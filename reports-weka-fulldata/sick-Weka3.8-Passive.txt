=== Run information ===

Scheme:       weka.classifiers.bayes.NaiveBayes 
Relation:     sick
Instances:    3772
Attributes:   30
              age
              sex
              on thyroxine
              query on thyroxine
              on antithyroid medication
              sick
              pregnant
              thyroid surgery
              I131 treatment
              query hypothyroid
              query hyperthyroid
              lithium
              goitre
              tumor
              hypopituitary
              psych
              TSH measured
              TSH
              T3 measured
              T3
              TT4 measured
              TT4
              T4U measured
              T4U
              FTI measured
              FTI
              TBG measured
              TBG
              referral source
              Class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

Naive Bayes Classifier

                               Class
Attribute                   negative     sick
                              (0.94)   (0.06)
==============================================
age
  mean                          50.99  64.0881
  std. dev.                   20.2327  16.3603
  weight sum                     3540      231
  precision                    4.9348   4.9348

sex
  F                            2345.0    137.0
  M                            1054.0     90.0
  [total]                      3399.0    227.0

on thyroxine
  f                            3090.0    220.0
  t                             453.0     13.0
  [total]                      3543.0    233.0

query on thyroxine
  f                            3496.0    228.0
  t                              47.0      5.0
  [total]                      3543.0    233.0

on antithyroid medication
  f                            3499.0    232.0
  t                              44.0      1.0
  [total]                      3543.0    233.0

sick
  f                            3420.0    207.0
  t                             123.0     26.0
  [total]                      3543.0    233.0

pregnant
  f                            3489.0    232.0
  t                              54.0      1.0
  [total]                      3543.0    233.0

thyroid surgery
  f                            3489.0    232.0
  t                              54.0      1.0
  [total]                      3543.0    233.0

I131 treatment
  f                            3484.0    231.0
  t                              59.0      2.0
  [total]                      3543.0    233.0

query hypothyroid
  f                            3336.0    204.0
  t                             207.0     29.0
  [total]                      3543.0    233.0

query hyperthyroid
  f                            3313.0    224.0
  t                             230.0      9.0
  [total]                      3543.0    233.0

lithium
  f                            3525.0    231.0
  t                              18.0      2.0
  [total]                      3543.0    233.0

goitre
  f                            3510.0    230.0
  t                              33.0      3.0
  [total]                      3543.0    233.0

tumor
  f                            3448.0    230.0
  t                              95.0      3.0
  [total]                      3543.0    233.0

hypopituitary
  f                            3542.0    231.0
  t                               1.0      2.0
  [total]                      3543.0    233.0

psych
  f                            3365.0    225.0
  t                             178.0      8.0
  [total]                      3543.0    233.0

TSH measured
  t                            3175.0    230.0
  f                             368.0      3.0
  [total]                      3543.0    233.0

TSH
  mean                         5.0964   4.1999
  std. dev.                    25.142  13.2259
  weight sum                     3174      229
  precision                    1.8531   1.8531

T3 measured
  t                            2776.0    229.0
  f                             767.0      4.0
  [total]                      3543.0    233.0

T3
  mean                          2.108   0.8921
  std. dev.                    0.7813   0.4392
  weight sum                     2775      228
  precision                    0.1551   0.1551

TT4 measured
  t                            3311.0    232.0
  f                             232.0      1.0
  [total]                      3543.0    233.0

TT4
  mean                       109.4196  92.6175
  std. dev.                   35.6301  31.3848
  weight sum                     3310      231
  precision                    1.7833   1.7833

T4U measured
  t                            3163.0    224.0
  f                             380.0      9.0
  [total]                      3543.0    233.0

T4U
  mean                         1.0063   0.8352
  std. dev.                     0.194   0.1351
  weight sum                     3162      223
  precision                    0.0143   0.0143

FTI measured
  t                            3165.0    224.0
  f                             378.0      9.0
  [total]                      3543.0    233.0

FTI
  mean                        110.446 110.8756
  std. dev.                   33.0156  34.1984
  weight sum                     3164      223
  precision                    1.6867   1.6867

TBG measured
  f                            3542.0    232.0
  [total]                      3542.0    232.0

TBG
  mean                              0        0
  std. dev.                    0.0017   0.0017
  weight sum                        0        0
  precision                      0.01     0.01

referral source
  SVHC                          378.0     10.0
  other                        2169.0     34.0
  SVI                           849.0    187.0
  STMW                          113.0      1.0
  SVHD                           37.0      4.0
  [total]                      3546.0    236.0



Time taken to build model: 0.01 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        3493               92.6034 %
Incorrectly Classified Instances       279                7.3966 %
Kappa statistic                          0.5249
Mean absolute error                      0.0888
Root mean squared error                  0.2294
Relative absolute error                 77.0863 %
Root relative squared error             95.6866 %
Total Number of Instances             3772     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.936    0.225    0.985      0.936    0.960      0.550    0.925     0.991     negative
                 0.775    0.064    0.441      0.775    0.562      0.550    0.925     0.660     sick
Weighted Avg.    0.926    0.215    0.951      0.926    0.935      0.550    0.925     0.971     

=== Confusion Matrix ===

    a    b   <-- classified as
 3314  227 |    a = negative
   52  179 |    b = sick

=== Run information ===

Scheme:       weka.classifiers.functions.SMO -C 1.0 -L 0.001 -P 1.0E-12 -N 0 -V -1 -W 1 -K "weka.classifiers.functions.supportVector.PolyKernel -E 1.0 -C 250007" -calibrator "weka.classifiers.functions.Logistic -R 1.0E-8 -M -1 -num-decimal-places 4"
Relation:     sick
Instances:    3772
Attributes:   30
              age
              sex
              on thyroxine
              query on thyroxine
              on antithyroid medication
              sick
              pregnant
              thyroid surgery
              I131 treatment
              query hypothyroid
              query hyperthyroid
              lithium
              goitre
              tumor
              hypopituitary
              psych
              TSH measured
              TSH
              T3 measured
              T3
              TT4 measured
              TT4
              T4U measured
              T4U
              FTI measured
              FTI
              TBG measured
              TBG
              referral source
              Class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

SMO

Kernel used:
  Linear Kernel: K(x,y) = <x,y>

Classifier for classes: negative, sick

BinarySMO

Machine linear: showing attribute weights, not support vectors.

         0.4351 * (normalized) age
 +       0.0021 * (normalized) sex=M
 +      -0.1412 * (normalized) on thyroxine=t
 +       0.0566 * (normalized) query on thyroxine=t
 +       0.1465 * (normalized) sick=t
 +      -0.012  * (normalized) pregnant=t
 +      -0.0774 * (normalized) thyroid surgery=t
 +       0.12   * (normalized) I131 treatment=t
 +       0.1979 * (normalized) query hypothyroid=t
 +       0.0546 * (normalized) query hyperthyroid=t
 +       0.134  * (normalized) lithium=t
 +       0.1166 * (normalized) goitre=t
 +       0.1995 * (normalized) tumor=t
 +       1      * (normalized) hypopituitary=t
 +       0.0865 * (normalized) psych=t
 +      -0.0047 * (normalized) TSH measured=f
 +      -1.5174 * (normalized) TSH
 +       0.1114 * (normalized) T3 measured=f
 +      -5.4137 * (normalized) T3
 +      -0.0748 * (normalized) TT4 measured=f
 +       0.5914 * (normalized) TT4
 +      -0.033  * (normalized) T4U measured=f
 +      -1.109  * (normalized) T4U
 +       0.0818 * (normalized) FTI measured=f
 +      -0.0547 * (normalized) FTI
 +      -0.0195 * (normalized) referral source=SVHC
 +      -0.0934 * (normalized) referral source=other
 +       0.1287 * (normalized) referral source=SVI
 +      -0.012  * (normalized) referral source=STMW
 +      -0.0037 * (normalized) referral source=SVHD
 -       0.3005

Number of kernel evaluations: 861855 (69.267% cached)



Time taken to build model: 0.4 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        3540               93.8494 %
Incorrectly Classified Instances       232                6.1506 %
Kappa statistic                         -0.0005
Mean absolute error                      0.0615
Root mean squared error                  0.248 
Relative absolute error                 53.3871 %
Root relative squared error            103.4332 %
Total Number of Instances             3772     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 1.000    1.000    0.939      1.000    0.968      -0.004   0.500     0.939     negative
                 0.000    0.000    0.000      0.000    0.000      -0.004   0.500     0.061     sick
Weighted Avg.    0.938    0.939    0.881      0.938    0.909      -0.004   0.500     0.885     

=== Confusion Matrix ===

    a    b   <-- classified as
 3540    1 |    a = negative
  231    0 |    b = sick


=== Run information ===

Scheme:       weka.classifiers.lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\""
Relation:     sick
Instances:    3772
Attributes:   30
              age
              sex
              on thyroxine
              query on thyroxine
              on antithyroid medication
              sick
              pregnant
              thyroid surgery
              I131 treatment
              query hypothyroid
              query hyperthyroid
              lithium
              goitre
              tumor
              hypopituitary
              psych
              TSH measured
              TSH
              T3 measured
              T3
              TT4 measured
              TT4
              T4U measured
              T4U
              FTI measured
              FTI
              TBG measured
              TBG
              referral source
              Class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

IB1 instance-based classifier
using 1 nearest neighbour(s) for classification


Time taken to build model: 0 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        3628               96.1824 %
Incorrectly Classified Instances       144                3.8176 %
Kappa statistic                          0.6465
Mean absolute error                      0.0384
Root mean squared error                  0.1953
Relative absolute error                 33.3689 %
Root relative squared error             81.4648 %
Total Number of Instances             3772     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.984    0.377    0.976      0.984    0.980      0.648    0.806     0.976     negative
                 0.623    0.016    0.716      0.623    0.667      0.648    0.806     0.475     sick
Weighted Avg.    0.962    0.355    0.960      0.962    0.961      0.648    0.806     0.945     

=== Confusion Matrix ===

    a    b   <-- classified as
 3484   57 |    a = negative
   87  144 |    b = sick

=== Run information ===

Scheme:       weka.classifiers.trees.J48 -C 0.25 -M 2
Relation:     sick
Instances:    3772
Attributes:   30
              age
              sex
              on thyroxine
              query on thyroxine
              on antithyroid medication
              sick
              pregnant
              thyroid surgery
              I131 treatment
              query hypothyroid
              query hyperthyroid
              lithium
              goitre
              tumor
              hypopituitary
              psych
              TSH measured
              TSH
              T3 measured
              T3
              TT4 measured
              TT4
              T4U measured
              T4U
              FTI measured
              FTI
              TBG measured
              TBG
              referral source
              Class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

J48 pruned tree
------------------

T3 <= 1.1
|   T3 measured = t
|   |   TT4 <= 53
|   |   |   TSH <= 19
|   |   |   |   FTI <= 78
|   |   |   |   |   T4U <= 0.67
|   |   |   |   |   |   T3 <= 0.5: sick (3.0/1.0)
|   |   |   |   |   |   T3 > 0.5: negative (2.0)
|   |   |   |   |   T4U > 0.67: sick (7.0)
|   |   |   |   FTI > 78: negative (8.0)
|   |   |   TSH > 19: negative (53.0)
|   |   TT4 > 53
|   |   |   TT4 <= 136
|   |   |   |   referral source = SVHC: sick (9.0)
|   |   |   |   referral source = other
|   |   |   |   |   sick = f
|   |   |   |   |   |   age <= 77
|   |   |   |   |   |   |   query hypothyroid = f
|   |   |   |   |   |   |   |   on thyroxine = f
|   |   |   |   |   |   |   |   |   T3 <= 0.5: sick (5.0/1.0)
|   |   |   |   |   |   |   |   |   T3 > 0.5: negative (21.0/1.0)
|   |   |   |   |   |   |   |   on thyroxine = t: sick (5.0/2.0)
|   |   |   |   |   |   |   query hypothyroid = t: sick (4.0/1.0)
|   |   |   |   |   |   age > 77: sick (8.0)
|   |   |   |   |   sick = t: sick (3.0)
|   |   |   |   referral source = SVI
|   |   |   |   |   FTI <= 151
|   |   |   |   |   |   FTI <= 69
|   |   |   |   |   |   |   FTI <= 61: negative (2.08/0.08)
|   |   |   |   |   |   |   FTI > 61: sick (6.22)
|   |   |   |   |   |   FTI > 69: sick (148.36)
|   |   |   |   |   FTI > 151
|   |   |   |   |   |   age <= 73: negative (5.28/1.28)
|   |   |   |   |   |   age > 73: sick (4.06)
|   |   |   |   referral source = STMW: sick (0.0)
|   |   |   |   referral source = SVHD: sick (3.0)
|   |   |   TT4 > 136
|   |   |   |   T4U measured = t: negative (12.0/1.0)
|   |   |   |   T4U measured = f: sick (2.0)
|   T3 measured = f: negative (79.64/0.31)
T3 > 1.1
|   FTI <= 171
|   |   TT4 <= 56
|   |   |   referral source = SVHC: negative (3.89)
|   |   |   referral source = other: negative (32.25/0.9)
|   |   |   referral source = SVI
|   |   |   |   TSH <= 14: sick (7.79/1.0)
|   |   |   |   TSH > 14: negative (4.98)
|   |   |   referral source = STMW: negative (2.03)
|   |   |   referral source = SVHD: negative (0.01)
|   |   TT4 > 56: negative (3187.63/1.0)
|   FTI > 171
|   |   T3 <= 2.6
|   |   |   on thyroxine = f
|   |   |   |   T3 measured = t
|   |   |   |   |   TSH <= 0.27: sick (19.02/3.02)
|   |   |   |   |   TSH > 0.27: negative (11.76)
|   |   |   |   T3 measured = f: negative (8.79)
|   |   |   on thyroxine = t: negative (19.64)
|   |   T3 > 2.6: negative (83.58)

Number of Leaves  : 	34

Size of the tree : 	61


Time taken to build model: 0.07 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        3727               98.807  %
Incorrectly Classified Instances        45                1.193  %
Kappa statistic                          0.8943
Mean absolute error                      0.0146
Root mean squared error                  0.1054
Relative absolute error                 12.685  %
Root relative squared error             43.9447 %
Total Number of Instances             3772     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.995    0.117    0.992      0.995    0.994      0.895    0.951     0.993     negative
                 0.883    0.005    0.919      0.883    0.901      0.895    0.951     0.864     sick
Weighted Avg.    0.988    0.110    0.988      0.988    0.988      0.895    0.951     0.985     

=== Confusion Matrix ===

    a    b   <-- classified as
 3523   18 |    a = negative
   27  204 |    b = sick


=== Run information ===

Scheme:       weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1
Relation:     sick
Instances:    3772
Attributes:   30
              age
              sex
              on thyroxine
              query on thyroxine
              on antithyroid medication
              sick
              pregnant
              thyroid surgery
              I131 treatment
              query hypothyroid
              query hyperthyroid
              lithium
              goitre
              tumor
              hypopituitary
              psych
              TSH measured
              TSH
              T3 measured
              T3
              TT4 measured
              TT4
              T4U measured
              T4U
              FTI measured
              FTI
              TBG measured
              TBG
              referral source
              Class
Test mode:    10-fold cross-validation

=== Classifier model (full training set) ===

RandomForest

Bagging with 100 iterations and base learner

weka.classifiers.trees.RandomTree -K 0 -M 1.0 -V 0.001 -S 1 -do-not-check-capabilities

Time taken to build model: 0.69 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances        3715               98.4889 %
Incorrectly Classified Instances        57                1.5111 %
Kappa statistic                          0.8567
Mean absolute error                      0.0323
Root mean squared error                  0.1083
Relative absolute error                 28.0513 %
Root relative squared error             45.1628 %
Total Number of Instances             3772     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.998    0.212    0.986      0.998    0.992      0.861    0.996     1.000     negative
                 0.788    0.002    0.958      0.788    0.865      0.861    0.996     0.966     sick
Weighted Avg.    0.985    0.199    0.985      0.985    0.984      0.861    0.996     0.998     

=== Confusion Matrix ===

    a    b   <-- classified as
 3533    8 |    a = negative
   49  182 |    b = sick



