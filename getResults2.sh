#!/bin/bash

function printRes(){
   grep -wn Correctly $1
   grep -wn Correctly $1 > $2.dat.txt
}

printRes $1 $2

#grep -wn Correctly General\ results-EntropySamplingQueryStrategy.* > EntropySampling.dat.txt
