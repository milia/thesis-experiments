from lxml import etree

root = etree.Element("experiment")
print root.tag

astring = "evaluation-method-type"

child2 = etree.SubElement(root,"process")
child3 = etree.SubElement(child2,"rand-gen-factory")
child4 = etree.SubElement(child2,"stratify")
child5 = etree.SubElement(child2, "file-dataset")

child6 = etree.SubElement(child2, "sampling-method")
child7 = etree.SubElement(child6, "percentage-to-select")

child8 = etree.SubElement(child2,"algorithm")

child9 = etree.SubElement(child8,"listener")
child10 = etree.SubElement(child9, "report-frequency")
child11 = etree.SubElement(child9, "report-on-file")
child12 = etree.SubElement(child9, "report-on-console")
child13 = etree.SubElement(child9, "report-title")
child14 = etree.SubElement(child9, "show-window")

child15 = etree.SubElement(child8, "stop-criterion")
child16 = etree.SubElement(child15, "max-iteration")
child17 = etree.SubElement(child8, "stop-criterion")

child18 = etree.SubElement(child8, "scenario")
child19 = etree.SubElement(child18, "batch-mode")
child20 = etree.SubElement(child19, "batch-size")

child21 = etree.SubElement(child18, "query-strategy")
child22 = etree.SubElement(child21, "wrapper-classifier")
child23 = etree.SubElement(child22, "classifier")
child24 = etree.SubElement(child18, "oracle")



print(etree.tostring(root, pretty_print=True))
