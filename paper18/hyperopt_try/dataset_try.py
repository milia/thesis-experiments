# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 10:35:56 2017

@author: 07sta
"""
import numpy as np
from hyperopt import fmin, tpe, hp, STATUS_OK,Trials
from sklearn.utils import shuffle
from sklearn.svm import SVC
from sklearn.preprocessing import normalize
from sklearn.cross_validation import cross_val_score
from sklearn.datasets import load_svmlight_file
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
X, y = load_svmlight_file("./solo_train_test_8_speakers.libsvm")
X = X.todense()
X = normalize(X)
#D = Dataset(X,y)

X1,  y1 = shuffle(X, y, random_state = 5) #anakateuo to train tou idiou fold pada
yy = np.concatenate([y1[:50]])
