# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 15:57:37 2016

@author: sotos
"""
from hyperopt import fmin, tpe, hp, STATUS_OK,Trials
from sklearn.svm import SVC
from sklearn.preprocessing import normalize
from sklearn.cross_validation import cross_val_score
from sklearn.datasets import load_svmlight_file
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
X, y = load_svmlight_file("./solo_train_test_8_speakers.libsvm")
X = X.todense()
X = normalize(X)


def hyperopt_train_test(params):
    t = params['type']
    del params['type']
    if t ==  'svm':
        clf = SVC(**params)
    elif t == 'randomforest':
        clf = RandomForestClassifier(**params)
    elif t == 'extra':
        clf = ExtraTreesClassifier(**params)
    else:
        return 0
    return cross_val_score(clf, X, y,cv=10).mean()

space = hp.choice('classifier_type', [
    {
        'type': 'svm',
        'C': hp.uniform('C', 0, 10.0),
        'kernel': hp.choice('kernel1', ['poly','rbf']),
        'degree': hp.choice('degree', [1, 2, 3]),  
        'gamma': hp.choice('gamma', [1,2,3,4,5])
    },
    {
        'type': 'randomforest',
        'max_depth': hp.choice('max_depth', range(1,20)),
        'max_features': hp.choice('max_features', range(1,5)),
        'n_estimators': hp.choice('n_estimators1', range(1,20)),
        'criterion': hp.choice('criterion', ["gini", "entropy"]),
    },
     {
        'type': 'extra',
        'max_depth': hp.choice('max_depth1', range(1,20)),
        #'max_features': hp.choice('max_features', range(1,5)),
        'n_estimators': hp.choice('n_estimators2', range(1,20)),
        #'criterion': hp.choice('criterion', ["gini", "entropy"]),
        }
])

count = 0
best = 0
def f(params):
    global best, count
    count += 1
    acc = hyperopt_train_test(params.copy())
    if acc > best:
        print 'new best:', acc, 'using', params
        best = acc
    if count % 50 == 0:
        print 'iters:', count, ', acc:', acc, 'using', params
    return {'loss': -acc, 'status': STATUS_OK}

trials = Trials()
best = fmin(f, space, algo=tpe.suggest, max_evals=25, trials=trials)
print 'best:'
print best
