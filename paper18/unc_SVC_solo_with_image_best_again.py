import copy
import os

import numpy as np
import matplotlib.pyplot as plt
try:
    from sklearn.model_selection import train_test_split
except ImportError:
    from sklearn.cross_validation import train_test_split

# libact classes
import xlwt
from libact.base.dataset import Dataset, import_libsvm_sparse
from sklearn.utils import shuffle
from libact.models import *
from libact.query_strategies import *
from libact.labelers import IdealLabeler
from sklearn.datasets import load_svmlight_file
from sklearn.preprocessing import normalize
#from sklearn.ensemble import ExtraTreesClassifier
#from sklearn.ensemble import RandomForestClassifier
#from sklearn.ensemble import GradientBoostingClassifier
from sklearn.datasets import load_iris
#from sklearn.linear_model import SGDClassifier
#from sklearn.naive_bayes import MultinomialNB
from sklearn.multiclass import OneVsRestClassifier
#from mlxtend.classifier import StackingClassifier
from libact.base.interfaces import QueryStrategy, ContinuousModel, ProbabilisticModel
from sklearn.svm import SVC
from libact.models import SVM
from libact.models import SklearnAdapter, SklearnProbaAdapter
from libact.utils import inherit_docstring_from, zip
from hyperopt import fmin, tpe, hp, STATUS_OK,Trials
from sklearn.cross_validation import cross_val_score
from sklearn.utils import shuffle
from hyperopt import space_eval


def run(trn_ds, tst_ds, lbr, model, qs, quota,posa,j):
    E_in, E_out, l = [], [], []
    counter = 0
    for _ in range(0,10):
    	counter = counter + 1
        # Standard usage of libact objects
        k = (posa/2 - j)/15
        l = []
        for i in range(0,k):
            ask_id = qs.make_query()
            l.append(ask_id)
            X, _ = zip(*trn_ds.data)
            lb = lbr.label(X[ask_id])
            trn_ds.update(ask_id, lb)

        model.train(trn_ds)
        #model.predict(tst_ds)
        #model.predict_proba(tst_ds)
        E_out = np.append(E_out, 1 - model.score(tst_ds))
        print  counter , '=', l, "error = ", E_out[counter - 1]
    return E_out, len(E_out) , trn_ds ,k ,j


def split_train_test(test_size, n_labeled,myfold,myflag,random_col):

    X, y = load_svmlight_file("/home/user/Downloads/kes 2017/text files kes2017/modApte/files/active learning/arffs/sinenosi_mfcc_autocolor.libsvm")
    X = X.todense()
    X = normalize(X)
    posa = len(y)
    if myflag == 0: #gia na fortono to length tou size
        return posa
    elif myflag == 1:
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state = myfold)
        X_train,  y_train = shuffle(X_train, y_train, random_state = random_col)
        return X_train,  y_train
    else:
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state = myfold)
        X_train, y_train = shuffle(X_train, y_train, random_state = random_col) #anakateuo to train tou idiou fold pada
     
        trn_ds = Dataset(X_train, np.concatenate([y_train[:n_labeled], [None] * (len(y_train) - n_labeled)]))  #instances tou L + U me i xoris label
        tst_ds = Dataset(X_test, y_test)
        fully_labeled_trn_ds = Dataset(X_train, y_train) #instances p periexoun tin alitheia gia ola ta L + U
    return trn_ds, tst_ds, y_train, fully_labeled_trn_ds, posa

def hyperopt_train_test(params):
    t = params['type']
    del params['type']
    if t ==  'svm':
        clf = SVC(**params)
    else:
        return 0
    return cross_val_score(clf, X_train_hyp, y_train_hyp,cv=10).mean()

def f(params):
    global best, count
    count += 1
    acc = hyperopt_train_test(params.copy())
    if acc > best:
        print 'new best:', acc, 'using', params
        best = acc
    if count % 50 == 0:
        print 'iters:', count, ', acc:', acc, 'using', params
    return {'loss': -acc, 'status': STATUS_OK}

def main():
 global count , best , X_train_hyp, y_train_hyp
 space = hp.choice('classifier_type', [
    {
        'type': 'svm',
        'C': hp.uniform('C', 0, 10.0),
        'kernel': hp.choice('kernel', ['poly','rbf','linear']),
        'degree': hp.choice('degree', [1, 2, 3]),  
        'gamma': hp.choice('gamma', [1,2,3,4,5])
    }
 ])
 book = xlwt.Workbook(encoding="utf-8")
 folds = [1,2,3,4,5]
 for fold in folds:
  message = "Sheet " + str(fold)
  sheet1 = book.add_sheet(message)#"Sheet 1")
  test_size = 0.15    # the percentage of samples in the dataset that will be randomly selected and assigned to the test set
  counter_j = -1
  counter_jj = -1
  #counter_initial_pop = 0
  #fold = 100
  SIZE = (1 - test_size) * split_train_test(test_size,1,fold,0,23) 
  for col in range(1,3):
   print '***********col*********** = ', col
   print '***********fold*********** = ', fold
   print 'SIZE = ', SIZE
   print
   print
   for j in range( int(0.3 * SIZE), int(0.4 * SIZE) , int((int(0.4 * SIZE) - int(0.3 * SIZE))/2)):
    print "j = ", j

    #counter_initial_pop += 1
    counter_j = counter_j + 1 #metrao to se poia periptosi eimai kathe fora apo tis esoterikes tou l h tou ll
    n_labeled = j      # number of samples that are initially labeled

    ll = ['poly' , 'rbf', 'linear']
    l = ['lc' , 'entropy' , 'sm']
    for jj in l:
    # Load dataset
        print '***********KERNEL*********** = ', jj
        X_train_hyp, y_train_hyp = split_train_test(test_size, n_labeled,fold,1,col)
        X_train_hyp = np.concatenate([X_train_hyp[:j]])
        y_train_hyp = np.concatenate([y_train_hyp[:j]])
        print 'mikos tous x_train_hyp pros hyperopt = ',len(X_train_hyp)
        count = 0
        best = 0
        trials = Trials()
        best = fmin(f, space, algo=tpe.suggest, max_evals=100, trials=trials)
        print 'best:'
        print best

        best_params = space_eval(space, best)
        del best_params['type']
        best_params['probability'] = True
        best_params['decision_function_shape'] = 'ovr'
        my_clf = SVC()
        my_clf.set_params(**best_params)
        print my_clf

        trn_ds, tst_ds, y_train, fully_labeled_trn_ds, posa_prin = split_train_test(test_size, n_labeled,fold,23,col)
        trn_ds2 = copy.deepcopy(trn_ds)
        #print len(trn_ds2)
        # trn_ds3 = copy.deepcopy(trn_ds)
        lbr = IdealLabeler(fully_labeled_trn_ds)
        posa = int(posa_prin - posa_prin * test_size)
        
        print "labeled instances = " , y_train[0:20]
        print "posa = ", posa
        quota = len(y_train) - n_labeled    # number of samples to query
        print "intial population of U: " , quota
    # Comparing UncertaintySampling strategy with RandomSampling.
    # model is the base learner, e.g. LogisticRegression, SVM ... etc.
        counter_jj = counter_jj + 1 #metrao to plithos ton periptoseon p eksetazei
        #VarianceReduction RandomSampling UncertaintySampling QUIRE
        qs1 = UncertaintySampling (trn_ds, kernel = jj , model =  SklearnProbaAdapter(my_clf))#SVC(kernel='poly', C = 0.688785056069401, degree= 3, gamma=3,decision_function_shape='ovr', probability = True)))#SVC(kernel='poly', C=3.3033000307257963, gamma=1, degree=1,decision_function_shape='ovr',probability = True)))
        model = SklearnProbaAdapter(my_clf)#SVC(kernel='poly', C = 0.688785056069401, degree= 3, gamma=3,decision_function_shape='ovr', probability = True))
        E_out_1, ttt , trn_ds_returned , aa , bb= run(trn_ds, tst_ds, lbr, model, qs1, quota,posa,j)
        print
        print "to size tou train meta to active = " , len(trn_ds_returned) , '  instances per iter    =  ', aa, '   arxikos plithismos    = ', bb
        print E_out_1 , jj , counter_jj , counter_j
    
        i = 4
        sheet1.write(1, counter_jj + counter_j, jj) #pirinas tou incertaintly
        sheet1.write(2, counter_jj + counter_j, j)  # plithos tou U
        sheet1.write(3, counter_jj + counter_j, aa)  #instanes inserted per iteratio
        for n in E_out_1:
            print i
            sheet1.write(i, counter_jj + counter_j, n)
            i = i+1 
        
        sheet1.write(i+2, counter_jj + counter_j, str(my_clf))
  #print "quota = ", quota 
 book.save("solo8_SVC_autocolor_best_hyperopt_UNCERT.xls")

if __name__ == '__main__':
    
    main()