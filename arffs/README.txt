Number of instances per data file:

1. krkopt, 28056, classification
2. kr-vs-kp, 3196, classification
3. Parkinsons, 5875, regression
4. Solar flare 1, 323, regression
5. Solar flare 2, 1066, regression
6. Tic Tac Toe, 958, classification

Notes:

1. Too big to run on Weka, will need the VM & AL.
2. Runs ok.
3. Haven't tried yet.
4. Too easy data.
5. Too easy data.
6. Runs ok.
