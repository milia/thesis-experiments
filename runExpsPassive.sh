#!/bin/bash

# bash for loop
for f in $( ls thesis-experiments/arffs/twentyUCI/ | sed 's/.arff//' ); do
	echo "starting experiment for $f"
	java -jar jclal-1.1.jar -d thesis-experiments/cfgs/passive/$f.cfgs
	sleep 2s
	#echo $f.cfgs
	echo "cleaning reports"
	cd reports/
	mkdir reports-pass-$f
	mv *.txt reports-pass-$f
	zip -r reports-pass-$f.zip reports-pass-$f/
	cd ..
	echo "finished cleaning"
	echo "sleeping 10s"
	sleep 10s
done 
