#!/bin/bash

testClassifiers(){
        cat $1/* |grep classifiers.trees;
        cat $1/* |grep classifiers.lazy;
        cat $1/* |grep classifiers.functions;
        cat $1/* |grep classifiers.bayes;
}

lsFiles(){
	for file in cfgs/final/*; 
	do
	  echo $file; 
	  ls -l $file;
	  testClassifiers $file;
	done
}

lsFiles
