#!/bin/bash
grep -wn Correctly General\ results-EntropySamplingQueryStrategy.* > EntropySampling.dat.txt 
grep -wn Correctly General\ results-KullbackLeiblerDivergenceQueryStrategy.* > KullbackLeibler.dat.txt 
grep -wn Correctly General\ results-LeastConfidentSamplingQueryStrategy.* > LeastConfidentSampling.dat.txt
grep -wn Correctly General\ results-MarginSamplingQueryStrategy.* > MarginSampling.dat.txt
grep -wn Correctly General\ results-RandomSelectionQueryStrategy.* > RandomSelection.dat.txt
grep -wn Correctly General\ results-VarianceReductionQueryStrategy.* > VarianceReduction.dat.txt
grep -wn Correctly General\ results-VoteEntropyQueryStrategy.* > VoteEntropy.dat.txt
