Wed Oct 25 15.46.07 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.07 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9030488830623409
Weighted AreaUnderROC: 0.8969298245614035
Root mean squared error: 0.3110855084191276
Relative absolute error: 19.35483870967742
Root relative squared error: 62.21710168382552
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7956420835574817
Weighted FMeasure: 0.903117288438708
Iteration time: 34.0
Weighted AreaUnderPRC: 0.8640318024236352
Mean absolute error: 0.0967741935483871
Coverage of cases: 90.3225806451613
Instances selection time: 12.0
Test time: 11.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.10936615732880588
Kappa statistic: 0.7956043956043956
Training time: 22.0
		
Time end:Wed Oct 25 15.46.07 EEST 2017