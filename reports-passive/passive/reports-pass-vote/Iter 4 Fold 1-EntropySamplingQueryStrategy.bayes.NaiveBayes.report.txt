Wed Oct 25 15.46.06 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.06 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9046709022561126
Weighted AreaUnderROC: 0.9704157782515991
Root mean squared error: 0.30507279188287084
Relative absolute error: 20.853819523510577
Root relative squared error: 61.01455837657417
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7951630856759044
Weighted FMeasure: 0.8999615639401037
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9722622326412015
Mean absolute error: 0.10426909761755289
Coverage of cases: 92.66055045871559
Instances selection time: 2.0
Test time: 14.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.08991412531053773
Kappa statistic: 0.7915869980879542
Training time: 4.0
		
Time end:Wed Oct 25 15.46.06 EEST 2017