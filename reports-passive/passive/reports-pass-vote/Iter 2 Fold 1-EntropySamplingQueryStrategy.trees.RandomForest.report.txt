Wed Oct 25 15.46.10 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.10 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 69.26605504587155
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.951069537304953
Weighted AreaUnderROC: 0.9844527363184079
Root mean squared error: 0.20875393717131585
Relative absolute error: 20.29369851849782
Root relative squared error: 41.750787434263174
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8956765253714111
Weighted FMeasure: 0.9497908110088517
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9800260686648741
Mean absolute error: 0.10146849259248909
Coverage of cases: 99.54128440366972
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.044957062655268866
Kappa statistic: 0.8946489763641156
Training time: 13.0
		
Time end:Wed Oct 25 15.46.10 EEST 2017