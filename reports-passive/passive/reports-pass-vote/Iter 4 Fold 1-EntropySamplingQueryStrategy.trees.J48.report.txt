Wed Oct 25 15.46.09 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.09 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 70.87155963302752
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9593495149616872
Weighted AreaUnderROC: 0.9650408670931058
Root mean squared error: 0.1969112586335136
Relative absolute error: 14.162352756223795
Root relative squared error: 39.38225172670272
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9138039551373668
Weighted FMeasure: 0.9588440549183241
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9483321163935471
Mean absolute error: 0.07081176378111897
Coverage of cases: 99.08256880733946
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03920600144754602
Kappa statistic: 0.9134233518665609
Training time: 1.0
		
Time end:Wed Oct 25 15.46.09 EEST 2017