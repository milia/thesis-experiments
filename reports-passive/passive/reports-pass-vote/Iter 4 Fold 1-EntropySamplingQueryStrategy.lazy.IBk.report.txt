Wed Oct 25 15.46.08 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.08 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 52.06422018348624
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9237863274704359
Weighted AreaUnderROC: 0.9431414356787491
Root mean squared error: 0.26498432223404983
Relative absolute error: 18.75177033017555
Root relative squared error: 52.996864446809965
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8381463301648796
Weighted FMeasure: 0.9224039806500435
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9221880900262259
Mean absolute error: 0.09375885165087774
Coverage of cases: 94.03669724770643
Instances selection time: 14.0
Test time: 12.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.0755364722912306
Kappa statistic: 0.8371847816536333
Training time: 0.0
		
Time end:Wed Oct 25 15.46.08 EEST 2017