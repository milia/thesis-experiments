Wed Oct 25 15.46.05 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.05 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.49384856043631
Incorrectly Classified Instances: 10.113304866190337
Correctly Classified Instances: 89.88669513380965
Weighted Precision: 0.9049896378415149
Weighted AreaUnderROC: 0.9709951183929976
Root mean squared error: 0.30107513276003267
Relative absolute error: 20.645696122583878
Root relative squared error: 60.21502655200654
Weighted TruePositiveRate: 0.8988669513380968
Weighted MatthewsCorrelation: 0.7954381332088503
Weighted FMeasure: 0.8997117891633766
Iteration time: 23.4
Weighted AreaUnderPRC: 0.9723514044523126
Mean absolute error: 0.10322848061291938
Coverage of cases: 93.01209148945165
Instances selection time: 13.6
Test time: 21.9
Accumulative iteration time: 23.4
Weighted Recall: 0.8988669513380968
Weighted FalsePositiveRate: 0.09013055811623602
Kappa statistic: 0.7913211962811035
Training time: 9.8
		
Time end:Wed Oct 25 15.46.06 EEST 2017