Wed Oct 25 15.46.08 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.08 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 51.84331797235023
Incorrectly Classified Instances: 8.755760368663594
Correctly Classified Instances: 91.2442396313364
Weighted Precision: 0.9133396081115769
Weighted AreaUnderROC: 0.935374149659864
Root mean squared error: 0.28305237741491274
Relative absolute error: 20.821301281369713
Root relative squared error: 56.61047548298255
Weighted TruePositiveRate: 0.9124423963133641
Weighted MatthewsCorrelation: 0.8170245549721808
Weighted FMeasure: 0.9127110675007154
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9137677946200317
Mean absolute error: 0.10410650640684857
Coverage of cases: 92.62672811059907
Instances selection time: 17.0
Test time: 15.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9124423963133641
Weighted FalsePositiveRate: 0.09038725846875252
Kappa statistic: 0.8166822284469344
Training time: 1.0
		
Time end:Wed Oct 25 15.46.08 EEST 2017