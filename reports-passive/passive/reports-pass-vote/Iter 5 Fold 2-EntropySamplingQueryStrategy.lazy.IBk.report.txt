Wed Oct 25 15.46.09 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.09 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9827694235588973
Root mean squared error: 0.20562085219496554
Relative absolute error: 14.893355083475445
Root relative squared error: 41.124170438993104
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9756419658130933
Mean absolute error: 0.07446677541737723
Coverage of cases: 97.6958525345622
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 2.0
		
Time end:Wed Oct 25 15.46.09 EEST 2017