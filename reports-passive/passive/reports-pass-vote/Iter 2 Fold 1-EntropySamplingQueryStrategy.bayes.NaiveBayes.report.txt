Wed Oct 25 15.46.05 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.05 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 52.293577981651374
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.9053880100830991
Weighted AreaUnderROC: 0.9756574271499645
Root mean squared error: 0.3101504629626329
Relative absolute error: 21.05530777312596
Root relative squared error: 62.03009259252658
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7923228054121031
Weighted FMeasure: 0.8956917649954197
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9773569996615842
Mean absolute error: 0.1052765388656298
Coverage of cases: 90.8256880733945
Instances selection time: 7.0
Test time: 30.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.08390550523920372
Kappa statistic: 0.7844553348809216
Training time: 0.0
		
Time end:Wed Oct 25 15.46.06 EEST 2017