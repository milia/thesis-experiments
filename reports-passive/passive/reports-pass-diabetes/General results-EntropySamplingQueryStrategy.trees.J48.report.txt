Wed Oct 25 15.35.08 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.08 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 82.109375
Incorrectly Classified Instances: 30.755208333333332
Correctly Classified Instances: 69.24479166666667
Weighted Precision: 0.7107600314281972
Weighted AreaUnderROC: 0.6593208955223882
Root mean squared error: 0.48864799023884664
Relative absolute error: 66.98104607179087
Root relative squared error: 97.72959804776934
Weighted TruePositiveRate: 0.6924479166666666
Weighted MatthewsCorrelation: 0.34750679775911064
Weighted FMeasure: 0.6876337467450564
Iteration time: 20.6
Weighted AreaUnderPRC: 0.6507743417165968
Mean absolute error: 0.33490523035895436
Coverage of cases: 90.15624999999999
Instances selection time: 8.1
Test time: 5.4
Accumulative iteration time: 20.6
Weighted Recall: 0.6924479166666666
Weighted FalsePositiveRate: 0.35841209577114425
Kappa statistic: 0.3327738410221547
Training time: 12.5
		
Time end:Wed Oct 25 15.35.09 EEST 2017