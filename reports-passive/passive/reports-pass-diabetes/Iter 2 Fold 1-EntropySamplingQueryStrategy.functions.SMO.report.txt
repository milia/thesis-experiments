Wed Oct 25 15.35.06 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.06 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7977240073145246
Weighted AreaUnderROC: 0.7329253731343285
Root mean squared error: 0.45357377202244253
Relative absolute error: 41.14583333333333
Root relative squared error: 90.7147544044885
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5304339929146479
Weighted FMeasure: 0.7812331109493439
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7153833509529562
Mean absolute error: 0.20572916666666666
Coverage of cases: 79.42708333333333
Instances selection time: 11.0
Test time: 5.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.3284200870646767
Kappa statistic: 0.5071163969584714
Training time: 31.0
		
Time end:Wed Oct 25 15.35.06 EEST 2017