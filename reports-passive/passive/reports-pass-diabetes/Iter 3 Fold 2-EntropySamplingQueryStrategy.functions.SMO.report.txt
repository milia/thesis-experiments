Wed Oct 25 15.35.06 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.06 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7590940238657815
Weighted AreaUnderROC: 0.702
Root mean squared error: 0.48680506023116343
Relative absolute error: 47.39583333333333
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45284467192495764
Weighted FMeasure: 0.7497593104153878
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6839992129573741
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333333
Instances selection time: 18.0
Test time: 15.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.35902083333333334
Kappa statistic: 0.436496162033155
Training time: 22.0
		
Time end:Wed Oct 25 15.35.06 EEST 2017