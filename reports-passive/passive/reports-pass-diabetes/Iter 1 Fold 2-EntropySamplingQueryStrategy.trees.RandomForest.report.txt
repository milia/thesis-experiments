Wed Oct 25 15.35.10 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.10 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 86.45833333333333
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.696781458878233
Weighted AreaUnderROC: 0.7588955223880597
Root mean squared error: 0.4425600241775119
Relative absolute error: 65.5729166666667
Root relative squared error: 88.51200483550238
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.307196697456082
Weighted FMeasure: 0.6818910256410255
Iteration time: 54.0
Weighted AreaUnderPRC: 0.7415105461287036
Mean absolute error: 0.3278645833333335
Coverage of cases: 97.39583333333333
Instances selection time: 18.0
Test time: 15.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.4541243781094528
Kappa statistic: 0.2836775483011327
Training time: 36.0
		
Time end:Wed Oct 25 15.35.10 EEST 2017