Wed Oct 25 15.34.22 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.22 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7705365519829575
Weighted AreaUnderROC: 0.7648908682940097
Root mean squared error: 0.4719465908407744
Relative absolute error: 47.32394366197189
Root relative squared error: 94.38931816815487
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5346016848812926
Weighted FMeasure: 0.7699786521780105
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7073009086571009
Mean absolute error: 0.23661971830985945
Coverage of cases: 77.10144927536231
Instances selection time: 47.0
Test time: 62.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.24123275616560388
Kappa statistic: 0.5334725526779754
Training time: 1.0
		
Time end:Wed Oct 25 15.34.23 EEST 2017