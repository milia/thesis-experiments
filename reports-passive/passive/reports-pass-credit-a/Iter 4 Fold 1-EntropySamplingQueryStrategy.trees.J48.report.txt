Wed Oct 25 15.34.25 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.25 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8821621621621623
Weighted AreaUnderROC: 0.875561683006536
Root mean squared error: 0.33270292125823603
Relative absolute error: 44.699165568730834
Root relative squared error: 66.5405842516472
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7482758802352505
Weighted FMeasure: 0.8669176976869286
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8299610645617262
Mean absolute error: 0.22349582784365415
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 7.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.11554330065359476
Kappa statistic: 0.7355
Training time: 4.0
		
Time end:Wed Oct 25 15.34.25 EEST 2017