Wed Oct 25 15.34.20 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.20 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8026488317527857
Weighted AreaUnderROC: 0.869113298113981
Root mean squared error: 0.4248962445397913
Relative absolute error: 43.39408798865777
Root relative squared error: 84.97924890795827
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5873257052662327
Weighted FMeasure: 0.7896473456686156
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8547667214418598
Mean absolute error: 0.21697043994328885
Coverage of cases: 87.2463768115942
Instances selection time: 27.0
Test time: 30.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.23259958040290385
Kappa statistic: 0.5742443467227505
Training time: 9.0
		
Time end:Wed Oct 25 15.34.20 EEST 2017