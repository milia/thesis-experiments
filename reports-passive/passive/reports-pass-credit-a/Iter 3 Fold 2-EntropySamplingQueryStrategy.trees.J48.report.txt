Wed Oct 25 15.34.25 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.25 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 96.66666666666667
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8688853239794365
Weighted AreaUnderROC: 0.8292309784456381
Root mean squared error: 0.37213021601910606
Relative absolute error: 44.27047473330789
Root relative squared error: 74.42604320382121
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.7101112920363145
Weighted FMeasure: 0.8400972721665326
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7969395999390095
Mean absolute error: 0.22135237366653948
Coverage of cases: 96.81159420289855
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.13482734732450188
Kappa statistic: 0.6863273436595969
Training time: 18.0
		
Time end:Wed Oct 25 15.34.25 EEST 2017