Wed Oct 25 15.34.18 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.18 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 57.971014492753625
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7903222416477984
Weighted AreaUnderROC: 0.8763810445675855
Root mean squared error: 0.43940643954428465
Relative absolute error: 44.05840397528366
Root relative squared error: 87.88128790885693
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5658479745864105
Weighted FMeasure: 0.7816215080865363
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8676564559322323
Mean absolute error: 0.2202920198764183
Coverage of cases: 85.79710144927536
Instances selection time: 84.0
Test time: 112.0
Accumulative iteration time: 178.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.23995986075589654
Kappa statistic: 0.5566322809211212
Training time: 94.0
		
Time end:Wed Oct 25 15.34.19 EEST 2017