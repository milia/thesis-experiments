Wed Oct 25 15.34.24 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.24 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 64.05797101449275
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7716963248333639
Weighted AreaUnderROC: 0.7831903594771242
Root mean squared error: 0.4400265788231593
Relative absolute error: 51.77519911215563
Root relative squared error: 88.00531576463186
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5171594638177609
Weighted FMeasure: 0.751298476635808
Iteration time: 66.0
Weighted AreaUnderPRC: 0.7685304824136849
Mean absolute error: 0.25887599556077817
Coverage of cases: 88.98550724637681
Instances selection time: 14.0
Test time: 4.0
Accumulative iteration time: 66.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.27667927678317705
Kappa statistic: 0.4976051371124796
Training time: 52.0
		
Time end:Wed Oct 25 15.34.24 EEST 2017