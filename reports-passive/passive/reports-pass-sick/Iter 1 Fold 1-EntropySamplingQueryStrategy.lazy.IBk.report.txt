Wed Oct 25 15.43.11 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.11 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.468716861081655
Correctly Classified Instances: 93.53128313891834
Weighted Precision: 0.9213111001104526
Weighted AreaUnderROC: 0.6246728696634178
Root mean squared error: 0.25367813328579053
Relative absolute error: 13.387374723823736
Root relative squared error: 50.73562665715811
Weighted TruePositiveRate: 0.9353128313891834
Weighted MatthewsCorrelation: 0.29791371809919026
Weighted FMeasure: 0.9263321088876698
Iteration time: 676.0
Weighted AreaUnderPRC: 0.9049866632481736
Mean absolute error: 0.06693687361911868
Coverage of cases: 93.53128313891834
Instances selection time: 671.0
Test time: 439.0
Accumulative iteration time: 676.0
Weighted Recall: 0.9353128313891834
Weighted FalsePositiveRate: 0.7115974163694205
Kappa statistic: 0.2836889589001859
Training time: 5.0
		
Time end:Wed Oct 25 15.43.12 EEST 2017