Wed Oct 25 15.43.19 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.19 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.256627783669141
Correctly Classified Instances: 93.74337221633085
Weighted Precision: 0.9278357447268987
Weighted AreaUnderROC: 0.6507254560184617
Root mean squared error: 0.24948558572899404
Relative absolute error: 12.97404318556285
Root relative squared error: 49.89711714579881
Weighted TruePositiveRate: 0.9374337221633086
Weighted MatthewsCorrelation: 0.36173201495127266
Weighted FMeasure: 0.9315743232502157
Iteration time: 407.0
Weighted AreaUnderPRC: 0.9102509880869486
Mean absolute error: 0.06487021592781425
Coverage of cases: 93.74337221633085
Instances selection time: 406.0
Test time: 474.0
Accumulative iteration time: 407.0
Weighted Recall: 0.9374337221633086
Weighted FalsePositiveRate: 0.6382807012711573
Kappa statistic: 0.3538132763456655
Training time: 1.0
		
Time end:Wed Oct 25 15.43.20 EEST 2017