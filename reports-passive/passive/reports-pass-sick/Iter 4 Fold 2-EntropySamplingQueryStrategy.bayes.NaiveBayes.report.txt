Wed Oct 25 15.43.07 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.07 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 58.08589607635207
Incorrectly Classified Instances: 6.044538706256628
Correctly Classified Instances: 93.95546129374337
Weighted Precision: 0.9547782197709641
Weighted AreaUnderROC: 0.8971530305537601
Root mean squared error: 0.21634711934910658
Relative absolute error: 14.641989439741982
Root relative squared error: 43.26942386982132
Weighted TruePositiveRate: 0.9395546129374337
Weighted MatthewsCorrelation: 0.5931156918214642
Weighted FMeasure: 0.9452436745209086
Iteration time: 44.0
Weighted AreaUnderPRC: 0.9689323814263774
Mean absolute error: 0.07320994719870991
Coverage of cases: 98.51537645811241
Instances selection time: 37.0
Test time: 68.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9395546129374337
Weighted FalsePositiveRate: 0.22146577599997025
Kappa statistic: 0.57832431160273
Training time: 7.0
		
Time end:Wed Oct 25 15.43.07 EEST 2017