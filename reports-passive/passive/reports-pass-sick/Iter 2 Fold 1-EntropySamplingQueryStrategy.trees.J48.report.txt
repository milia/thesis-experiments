Wed Oct 25 15.43.21 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.21 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 51.72322375397667
Incorrectly Classified Instances: 2.9692470837751856
Correctly Classified Instances: 97.03075291622481
Weighted Precision: 0.9682685125470023
Weighted AreaUnderROC: 0.7953403874008789
Root mean squared error: 0.1682535722188629
Relative absolute error: 9.166561108032099
Root relative squared error: 33.65071444377258
Weighted TruePositiveRate: 0.9703075291622482
Weighted MatthewsCorrelation: 0.7123428922478012
Weighted FMeasure: 0.9680410641416278
Iteration time: 62.0
Weighted AreaUnderPRC: 0.950687893573493
Mean absolute error: 0.04583280554016049
Coverage of cases: 97.5609756097561
Instances selection time: 11.0
Test time: 6.0
Accumulative iteration time: 62.0
Weighted Recall: 0.9703075291622482
Weighted FalsePositiveRate: 0.3596920576772114
Kappa statistic: 0.7019343113072832
Training time: 51.0
		
Time end:Wed Oct 25 15.43.22 EEST 2017