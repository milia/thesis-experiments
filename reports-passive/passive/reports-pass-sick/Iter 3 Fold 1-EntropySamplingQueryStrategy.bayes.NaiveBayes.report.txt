Wed Oct 25 15.43.07 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.07 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 62.67232237539767
Incorrectly Classified Instances: 7.688229056203605
Correctly Classified Instances: 92.31177094379639
Weighted Precision: 0.9540819653803871
Weighted AreaUnderROC: 0.9124812206558715
Root mean squared error: 0.24029837962754744
Relative absolute error: 19.9704946171881
Root relative squared error: 48.05967592550949
Weighted TruePositiveRate: 0.9231177094379639
Weighted MatthewsCorrelation: 0.5631795987745428
Weighted FMeasure: 0.9339920535418381
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9692624553403512
Mean absolute error: 0.09985247308594049
Coverage of cases: 99.04559915164369
Instances selection time: 25.0
Test time: 82.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9231177094379639
Weighted FalsePositiveRate: 0.16761234523694757
Kappa statistic: 0.529481598733676
Training time: 11.0
		
Time end:Wed Oct 25 15.43.07 EEST 2017