Wed Oct 25 15.45.49 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.49 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 39.657210401891255
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5419725099651093
Weighted AreaUnderROC: 0.7777282174336856
Root mean squared error: 0.39206332539816935
Relative absolute error: 56.499161463700496
Root relative squared error: 90.54314658320517
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.43184581214178025
Weighted FMeasure: 0.5375377321196223
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5317579871527601
Mean absolute error: 0.21187185548887685
Coverage of cases: 79.66903073286052
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1383194849546742
Kappa statistic: 0.44862799427958516
Training time: 14.0
		
Time end:Wed Oct 25 15.45.49 EEST 2017