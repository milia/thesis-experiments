Wed Oct 25 15.45.42 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.42 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.05200945626478
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.47875590026832754
Weighted AreaUnderROC: 0.7444669751480923
Root mean squared error: 0.4757697615278306
Relative absolute error: 68.72621853739874
Root relative squared error: 109.87431995615083
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.32095752210311357
Weighted FMeasure: 0.44447734813411377
Iteration time: 69.0
Weighted AreaUnderPRC: 0.4887803526675735
Mean absolute error: 0.2577233195152453
Coverage of cases: 64.06619385342789
Instances selection time: 65.0
Test time: 102.0
Accumulative iteration time: 69.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16554938504622643
Kappa statistic: 0.32815283220520125
Training time: 4.0
		
Time end:Wed Oct 25 15.45.42 EEST 2017