Wed Oct 25 15.45.45 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.45 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.613218091309085
Weighted AreaUnderROC: 0.8005317579620685
Root mean squared error: 0.3779272406323441
Relative absolute error: 78.9598108747044
Root relative squared error: 87.27855764527054
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.4974536152514009
Weighted FMeasure: 0.612836514703451
Iteration time: 112.0
Weighted AreaUnderPRC: 0.5412375032343499
Mean absolute error: 0.2960992907801415
Coverage of cases: 94.79905437352245
Instances selection time: 11.0
Test time: 10.0
Accumulative iteration time: 112.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12553343254870933
Kappa statistic: 0.504484271473766
Training time: 101.0
		
Time end:Wed Oct 25 15.45.45 EEST 2017