Wed Oct 25 15.45.47 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.47 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.5836706169039663
Weighted AreaUnderROC: 0.7243190701534918
Root mean squared error: 0.4435226582017142
Relative absolute error: 56.8987749838813
Root relative squared error: 102.42717044178325
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.44813691911051834
Weighted FMeasure: 0.5722141856437919
Iteration time: 69.0
Weighted AreaUnderPRC: 0.4657574784967656
Mean absolute error: 0.21337040618955488
Coverage of cases: 58.86524822695036
Instances selection time: 68.0
Test time: 79.0
Accumulative iteration time: 69.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.14001434196251983
Kappa statistic: 0.4497540407589599
Training time: 1.0
		
Time end:Wed Oct 25 15.45.47 EEST 2017