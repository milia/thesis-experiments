Wed Oct 25 15.36.05 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.05 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.863636363636363
Correctly Classified Instances: 76.13636363636364
Weighted Precision: 0.7627418202235721
Weighted AreaUnderROC: 0.6982722292456806
Root mean squared error: 0.47588607977857517
Relative absolute error: 50.55282555282554
Root relative squared error: 95.17721595571503
Weighted TruePositiveRate: 0.7613636363636364
Weighted MatthewsCorrelation: 0.45772938576167976
Weighted FMeasure: 0.7447433155080213
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6796405527120524
Mean absolute error: 0.2527641277641277
Coverage of cases: 76.13636363636364
Instances selection time: 16.0
Test time: 18.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7613636363636364
Weighted FalsePositiveRate: 0.36481917787227525
Kappa statistic: 0.43304187758858725
Training time: 6.0
		
Time end:Wed Oct 25 15.36.05 EEST 2017