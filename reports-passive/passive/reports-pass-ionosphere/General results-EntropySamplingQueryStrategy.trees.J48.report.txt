Wed Oct 25 15.36.06 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.06 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 56.00292207792207
Incorrectly Classified Instances: 15.831493506493507
Correctly Classified Instances: 84.1685064935065
Weighted Precision: 0.8476507531033264
Weighted AreaUnderROC: 0.8154559353239822
Root mean squared error: 0.38055494291411696
Relative absolute error: 34.606838983480316
Root relative squared error: 76.1109885828234
Weighted TruePositiveRate: 0.841685064935065
Weighted MatthewsCorrelation: 0.6555726569906126
Weighted FMeasure: 0.8374255182766452
Iteration time: 21.7
Weighted AreaUnderPRC: 0.7944584680475952
Mean absolute error: 0.17303419491740155
Coverage of cases: 87.18409090909091
Instances selection time: 6.0
Test time: 4.8
Accumulative iteration time: 21.7
Weighted Recall: 0.841685064935065
Weighted FalsePositiveRate: 0.22559958944693462
Kappa statistic: 0.6426785608412768
Training time: 15.7
		
Time end:Wed Oct 25 15.36.06 EEST 2017