Wed Oct 25 15.36.07 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.07 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 88.35227272727273
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8275920360631104
Weighted AreaUnderROC: 0.8851664559629161
Root mean squared error: 0.3601609488699587
Relative absolute error: 53.52272727272728
Root relative squared error: 72.03218977399175
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6216683538863422
Weighted FMeasure: 0.8266810872743076
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8712332003431654
Mean absolute error: 0.2676136363636364
Coverage of cases: 98.86363636363636
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.22847788887611895
Kappa statistic: 0.6184419713831479
Training time: 18.0
		
Time end:Wed Oct 25 15.36.07 EEST 2017