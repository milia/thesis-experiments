Wed Oct 25 15.36.07 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.07 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 84.0909090909091
Incorrectly Classified Instances: 10.227272727272727
Correctly Classified Instances: 89.77272727272727
Weighted Precision: 0.8984785875281743
Weighted AreaUnderROC: 0.9487287540384884
Root mean squared error: 0.291645020841807
Relative absolute error: 38.06818181818187
Root relative squared error: 58.3290041683614
Weighted TruePositiveRate: 0.8977272727272727
Weighted MatthewsCorrelation: 0.7750877934058509
Weighted FMeasure: 0.8960086523645846
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9420506147410073
Mean absolute error: 0.19034090909090934
Coverage of cases: 99.43181818181819
Instances selection time: 18.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8977272727272727
Weighted FalsePositiveRate: 0.14832426668709855
Kappa statistic: 0.7710651828298887
Training time: 13.0
		
Time end:Wed Oct 25 15.36.07 EEST 2017