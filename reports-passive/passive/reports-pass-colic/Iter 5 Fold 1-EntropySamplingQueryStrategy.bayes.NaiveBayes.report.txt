Wed Oct 25 15.33.59 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.59 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.708043581436619
Weighted AreaUnderROC: 0.7045677528882617
Root mean squared error: 0.519270754570208
Relative absolute error: 63.99033005746859
Root relative squared error: 103.8541509140416
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.3659764198558469
Weighted FMeasure: 0.6899475981150098
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6913005672094341
Mean absolute error: 0.31995165028734296
Coverage of cases: 79.8913043478261
Instances selection time: 13.0
Test time: 11.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.3064864626510274
Kappa statistic: 0.35865384615384627
Training time: 1.0
		
Time end:Wed Oct 25 15.33.59 EEST 2017