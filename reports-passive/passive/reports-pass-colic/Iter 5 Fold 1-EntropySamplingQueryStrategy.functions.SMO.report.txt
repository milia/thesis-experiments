Wed Oct 25 15.34.00 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.00 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7157611487014582
Weighted AreaUnderROC: 0.6937119675456389
Root mean squared error: 0.531609533071195
Relative absolute error: 56.52173913043478
Root relative squared error: 106.321906614239
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.3899033149020613
Weighted FMeasure: 0.716490013699618
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6597108342726874
Mean absolute error: 0.2826086956521739
Coverage of cases: 71.73913043478261
Instances selection time: 9.0
Test time: 21.0
Accumulative iteration time: 54.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.3299673692565482
Kappa statistic: 0.38979591836734684
Training time: 45.0
		
Time end:Wed Oct 25 15.34.01 EEST 2017