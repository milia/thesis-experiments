Wed Oct 25 15.34.00 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.00 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7686869151357689
Weighted AreaUnderROC: 0.7459432048681541
Root mean squared error: 0.47776654295295456
Relative absolute error: 45.65217391304348
Root relative squared error: 95.55330859059092
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.502310798467565
Weighted FMeasure: 0.7693488097580168
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7074158110400575
Mean absolute error: 0.22826086956521738
Coverage of cases: 77.17391304347827
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.27985272069847433
Kappa statistic: 0.5010330578512396
Training time: 39.0
		
Time end:Wed Oct 25 15.34.00 EEST 2017