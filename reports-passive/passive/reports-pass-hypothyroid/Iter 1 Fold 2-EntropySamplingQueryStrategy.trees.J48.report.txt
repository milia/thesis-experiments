Wed Oct 25 15.35.45 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.45 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 26.299045599151643
Incorrectly Classified Instances: 2.916224814422057
Correctly Classified Instances: 97.08377518557795
Weighted Precision: 0.9688831710495094
Weighted AreaUnderROC: 0.8953978031901829
Root mean squared error: 0.11829131213640835
Relative absolute error: 4.262104627075208
Root relative squared error: 27.318208361899764
Weighted TruePositiveRate: 0.9708377518557795
Weighted MatthewsCorrelation: 0.8416187714214748
Weighted FMeasure: 0.9691083372856862
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9533409277069139
Mean absolute error: 0.015982892351532028
Coverage of cases: 97.66702014846236
Instances selection time: 9.0
Test time: 18.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9708377518557795
Weighted FalsePositiveRate: 0.19637320887640244
Kappa statistic: 0.7830615241114791
Training time: 22.0
		
Time end:Wed Oct 25 15.35.45 EEST 2017