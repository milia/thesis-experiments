Wed Oct 25 15.35.28 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.28 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 29.838282078472957
Incorrectly Classified Instances: 6.256627783669141
Correctly Classified Instances: 93.74337221633085
Weighted Precision: 0.9230975242413946
Weighted AreaUnderROC: 0.885859206559402
Root mean squared error: 0.1583618192960196
Relative absolute error: 10.955440597412597
Root relative squared error: 36.57209559996632
Weighted TruePositiveRate: 0.9374337221633086
Weighted MatthewsCorrelation: 0.5235662659716476
Weighted FMeasure: 0.926112165526348
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9403788548604367
Mean absolute error: 0.04108290224029724
Coverage of cases: 97.29586426299046
Instances selection time: 77.0
Test time: 79.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9374337221633086
Weighted FalsePositiveRate: 0.5610538255540114
Kappa statistic: 0.43370281026392615
Training time: 11.0
		
Time end:Wed Oct 25 15.35.28 EEST 2017