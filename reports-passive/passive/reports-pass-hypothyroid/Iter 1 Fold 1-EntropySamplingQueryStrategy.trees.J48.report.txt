Wed Oct 25 15.35.45 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.45 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 29.40084835630965
Incorrectly Classified Instances: 1.4316012725344645
Correctly Classified Instances: 98.56839872746554
Weighted Precision: 0.9862819777308205
Weighted AreaUnderROC: 0.9844796130792048
Root mean squared error: 0.08243303077490517
Relative absolute error: 3.757733148488613
Root relative squared error: 19.037093003203285
Weighted TruePositiveRate: 0.9856839872746553
Weighted MatthewsCorrelation: 0.9373978586966144
Weighted FMeasure: 0.9858392533437891
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9805149680866716
Mean absolute error: 0.014091499306832298
Coverage of cases: 99.31071049840934
Instances selection time: 22.0
Test time: 13.0
Accumulative iteration time: 102.0
Weighted Recall: 0.9856839872746553
Weighted FalsePositiveRate: 0.01329940101077266
Kappa statistic: 0.9046811473752778
Training time: 80.0
		
Time end:Wed Oct 25 15.35.45 EEST 2017