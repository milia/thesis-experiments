Wed Oct 25 15.35.43 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.43 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 11.346765641569458
Correctly Classified Instances: 88.65323435843054
Weighted Precision: 0.8836738078678535
Weighted AreaUnderROC: 0.5606784718116671
Root mean squared error: 0.23697856359821096
Relative absolute error: 16.019224167151666
Root relative squared error: 54.72785499423918
Weighted TruePositiveRate: 0.8865323435843054
Weighted MatthewsCorrelation: 0.13646277782184466
Weighted FMeasure: 0.8813640973176148
Iteration time: 321.0
Weighted AreaUnderPRC: 0.8667497844881473
Mean absolute error: 0.060072090626818744
Coverage of cases: 88.65323435843054
Instances selection time: 320.0
Test time: 419.0
Accumulative iteration time: 321.0
Weighted Recall: 0.8865323435843054
Weighted FalsePositiveRate: 0.76660107996212
Kappa statistic: 0.12852978645304772
Training time: 1.0
		
Time end:Wed Oct 25 15.35.44 EEST 2017