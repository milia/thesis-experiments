Wed Oct 25 15.35.38 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.38 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 10.65747613997879
Correctly Classified Instances: 89.3425238600212
Weighted Precision: 0.8747614938324482
Weighted AreaUnderROC: 0.5692238324109942
Root mean squared error: 0.2296704372731094
Relative absolute error: 15.107605438468603
Root relative squared error: 53.040115513811514
Weighted TruePositiveRate: 0.8934252386002121
Weighted MatthewsCorrelation: 0.1556664093490099
Weighted FMeasure: 0.8836697293199341
Iteration time: 320.0
Weighted AreaUnderPRC: 0.8666914637956183
Mean absolute error: 0.05665352039425726
Coverage of cases: 89.3425238600212
Instances selection time: 319.0
Test time: 454.0
Accumulative iteration time: 320.0
Weighted Recall: 0.8934252386002121
Weighted FalsePositiveRate: 0.7601726821619593
Kappa statistic: 0.15162731990716966
Training time: 1.0
		
Time end:Wed Oct 25 15.35.39 EEST 2017