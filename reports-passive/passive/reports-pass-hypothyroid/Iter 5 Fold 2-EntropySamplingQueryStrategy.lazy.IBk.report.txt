Wed Oct 25 15.35.44 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.35.44 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.377518557794273
Correctly Classified Instances: 91.62248144220572
Weighted Precision: 0.8962345701520039
Weighted AreaUnderROC: 0.614767373577271
Root mean squared error: 0.20363798571840336
Relative absolute error: 12.10040800233392
Root relative squared error: 47.02817834870134
Weighted TruePositiveRate: 0.9162248144220573
Weighted MatthewsCorrelation: 0.30181293084984745
Weighted FMeasure: 0.9039307221468599
Iteration time: 355.0
Weighted AreaUnderPRC: 0.8801119517582011
Mean absolute error: 0.0453765300087522
Coverage of cases: 91.62248144220572
Instances selection time: 354.0
Test time: 476.0
Accumulative iteration time: 355.0
Weighted Recall: 0.9162248144220573
Weighted FalsePositiveRate: 0.6901301526118917
Kappa statistic: 0.2668105219387496
Training time: 1.0
		
Time end:Wed Oct 25 15.35.44 EEST 2017