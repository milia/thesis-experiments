Wed Oct 25 15.46.38 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.38 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 60.989898989899
Correctly Classified Instances: 39.01010101010101
Weighted Precision: 0.433316987970865
Weighted AreaUnderROC: 0.6645555555555556
Root mean squared error: 0.3171465242850592
Relative absolute error: 70.38000000000036
Root relative squared error: 110.31959056213566
Weighted TruePositiveRate: 0.39010101010101017
Weighted MatthewsCorrelation: 0.34349935415486543
Weighted FMeasure: 0.3922319931570696
Iteration time: 41.1
Weighted AreaUnderPRC: 0.22580134965911994
Mean absolute error: 0.11633057851239806
Coverage of cases: 69.69696969696969
Instances selection time: 40.9
Test time: 54.5
Accumulative iteration time: 41.1
Weighted Recall: 0.39010101010101017
Weighted FalsePositiveRate: 0.060989898989899
Kappa statistic: 0.32911111111111113
Training time: 0.2
		
Time end:Wed Oct 25 15.46.39 EEST 2017