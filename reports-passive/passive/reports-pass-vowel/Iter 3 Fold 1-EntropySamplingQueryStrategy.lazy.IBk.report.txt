Wed Oct 25 15.46.39 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.39 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.4336986998055263
Weighted AreaUnderROC: 0.6788888888888889
Root mean squared error: 0.3104249287084334
Relative absolute error: 67.80000000000034
Root relative squared error: 107.98147989354413
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.3615270044374023
Weighted FMeasure: 0.40987155708975215
Iteration time: 43.0
Weighted AreaUnderPRC: 0.24101757229190232
Mean absolute error: 0.11206611570248061
Coverage of cases: 71.71717171717172
Instances selection time: 43.0
Test time: 60.0
Accumulative iteration time: 43.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.05838383838383838
Kappa statistic: 0.35777777777777775
Training time: 0.0
		
Time end:Wed Oct 25 15.46.39 EEST 2017