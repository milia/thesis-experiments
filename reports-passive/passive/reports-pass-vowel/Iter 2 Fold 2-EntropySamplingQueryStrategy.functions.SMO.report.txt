Wed Oct 25 15.46.32 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.32 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 75.15151515151516
Correctly Classified Instances: 24.848484848484848
Weighted Precision: 0.23522560188570388
Weighted AreaUnderROC: 0.7086734006734007
Root mean squared error: 0.28109240350898845
Relative absolute error: 95.92323232323182
Root relative squared error: 97.77814508654578
Weighted TruePositiveRate: 0.24848484848484848
Weighted MatthewsCorrelation: 0.16237264467795706
Weighted FMeasure: 0.2189740743389824
Iteration time: 751.0
Weighted AreaUnderPRC: 0.21252594320416235
Mean absolute error: 0.15855079722848336
Coverage of cases: 95.75757575757575
Instances selection time: 27.0
Test time: 41.0
Accumulative iteration time: 751.0
Weighted Recall: 0.24848484848484848
Weighted FalsePositiveRate: 0.07515151515151515
Kappa statistic: 0.17333333333333334
Training time: 724.0
		
Time end:Wed Oct 25 15.46.32 EEST 2017