Wed Oct 25 15.46.40 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.46.40 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 16.510560146923904
Incorrectly Classified Instances: 55.15151515151515
Correctly Classified Instances: 44.84848484848485
Weighted Precision: 0.425101304246295
Weighted AreaUnderROC: 0.7475398428731761
Root mean squared error: 0.29065855139370433
Relative absolute error: 62.54801864801815
Root relative squared error: 101.10573481901321
Weighted TruePositiveRate: 0.4484848484848485
Weighted MatthewsCorrelation: 0.3803007694134468
Weighted FMeasure: 0.4196210391339869
Iteration time: 129.0
Weighted AreaUnderPRC: 0.31851394494249546
Mean absolute error: 0.10338515479011331
Coverage of cases: 59.19191919191919
Instances selection time: 23.0
Test time: 10.0
Accumulative iteration time: 129.0
Weighted Recall: 0.4484848484848485
Weighted FalsePositiveRate: 0.05515151515151516
Kappa statistic: 0.3933333333333333
Training time: 106.0
		
Time end:Wed Oct 25 15.46.40 EEST 2017