Wed Oct 25 15.39.39 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.39.39 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 78.79730769230571
Incorrectly Classified Instances: 27.34
Correctly Classified Instances: 72.66
Weighted Precision: 0.7481159290057231
Weighted AreaUnderROC: 0.9618304266411145
Root mean squared error: 0.18647876636486363
Relative absolute error: 96.32828799999955
Root relative squared error: 96.96895850973586
Weighted TruePositiveRate: 0.7266
Weighted MatthewsCorrelation: 0.722530793732515
Weighted FMeasure: 0.7286918029084469
Iteration time: 10896.0
Weighted AreaUnderPRC: 0.6489429358305967
Mean absolute error: 0.07124873372780038
Coverage of cases: 99.78
Instances selection time: 268.0
Test time: 496.0
Accumulative iteration time: 10896.0
Weighted Recall: 0.7266
Weighted FalsePositiveRate: 0.010919948845963525
Kappa statistic: 0.7156399681649895
Training time: 10628.0
		
Time end:Wed Oct 25 15.39.50 EEST 2017