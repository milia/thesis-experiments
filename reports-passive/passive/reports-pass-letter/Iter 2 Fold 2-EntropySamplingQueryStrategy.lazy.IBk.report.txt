Wed Oct 25 15.40.16 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.40.16 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 3.86538461538513
Incorrectly Classified Instances: 15.18
Correctly Classified Instances: 84.82
Weighted Precision: 0.8510594322214671
Weighted AreaUnderROC: 0.9231870370805917
Root mean squared error: 0.10701022852784788
Relative absolute error: 16.860003561214565
Root relative squared error: 55.645318834484776
Weighted TruePositiveRate: 0.8482
Weighted MatthewsCorrelation: 0.8431462121173012
Weighted FMeasure: 0.8486906852331892
Iteration time: 3607.0
Weighted AreaUnderPRC: 0.7369402339846564
Mean absolute error: 0.012470416835216134
Coverage of cases: 84.99
Instances selection time: 3597.0
Test time: 4596.0
Accumulative iteration time: 3607.0
Weighted Recall: 0.8482
Weighted FalsePositiveRate: 0.0060590518757867055
Kappa statistic: 0.8421210461267492
Training time: 10.0
		
Time end:Wed Oct 25 15.40.24 EEST 2017