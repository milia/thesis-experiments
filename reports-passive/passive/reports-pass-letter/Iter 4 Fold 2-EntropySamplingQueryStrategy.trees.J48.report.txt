Wed Oct 25 15.41.20 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.41.20 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 5.554230769231514
Incorrectly Classified Instances: 29.49
Correctly Classified Instances: 70.51
Weighted Precision: 0.7112517692321477
Weighted AreaUnderROC: 0.8735559661187086
Root mean squared error: 0.13988831309205432
Relative absolute error: 33.01105617353796
Root relative squared error: 72.74192280787332
Weighted TruePositiveRate: 0.7051
Weighted MatthewsCorrelation: 0.6954345995653319
Weighted FMeasure: 0.706041715264216
Iteration time: 224.0
Weighted AreaUnderPRC: 0.6007593678623158
Mean absolute error: 0.024416461666814614
Coverage of cases: 75.77
Instances selection time: 47.0
Test time: 185.0
Accumulative iteration time: 224.0
Weighted Recall: 0.7051
Weighted FalsePositiveRate: 0.011784511052645942
Kappa statistic: 0.6932901781832217
Training time: 177.0
		
Time end:Wed Oct 25 15.41.20 EEST 2017