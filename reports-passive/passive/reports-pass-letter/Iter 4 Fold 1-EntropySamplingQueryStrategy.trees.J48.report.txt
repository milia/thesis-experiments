Wed Oct 25 15.41.19 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.41.19 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 5.563846153846876
Incorrectly Classified Instances: 26.79
Correctly Classified Instances: 73.21
Weighted Precision: 0.739148645577106
Weighted AreaUnderROC: 0.881797153514379
Root mean squared error: 0.13361854939219728
Relative absolute error: 30.268370038413
Root relative squared error: 69.48164568394743
Weighted TruePositiveRate: 0.7321
Weighted MatthewsCorrelation: 0.7239589340523107
Weighted FMeasure: 0.7335335426994332
Iteration time: 205.0
Weighted AreaUnderPRC: 0.6309849826225552
Mean absolute error: 0.022387847661544957
Coverage of cases: 77.35
Instances selection time: 45.0
Test time: 260.0
Accumulative iteration time: 205.0
Weighted Recall: 0.7321
Weighted FalsePositiveRate: 0.010736365960861917
Kappa statistic: 0.7213665056059675
Training time: 160.0
		
Time end:Wed Oct 25 15.41.20 EEST 2017