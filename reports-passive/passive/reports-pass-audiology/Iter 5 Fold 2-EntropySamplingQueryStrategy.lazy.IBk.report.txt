Wed Oct 25 15.33.41 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.41 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.19616519174049
Incorrectly Classified Instances: 57.52212389380531
Correctly Classified Instances: 42.47787610619469
Weighted Precision: 0.30453380216671744
Weighted AreaUnderROC: 0.7277723923574813
Root mean squared error: 0.17254842109002802
Relative absolute error: 75.57863939261671
Root relative squared error: 86.34919900814536
Weighted TruePositiveRate: 0.4247787610619469
Weighted MatthewsCorrelation: 0.25775064067518355
Weighted FMeasure: 0.328268639478079
Iteration time: 9.0
Weighted AreaUnderPRC: 0.3928785036989736
Mean absolute error: 0.06035794118160356
Coverage of cases: 98.23008849557522
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 9.0
Weighted Recall: 0.4247787610619469
Weighted FalsePositiveRate: 0.1293622727850166
Kappa statistic: 0.2959838972491134
Training time: 0.0
		
Time end:Wed Oct 25 15.33.41 EEST 2017