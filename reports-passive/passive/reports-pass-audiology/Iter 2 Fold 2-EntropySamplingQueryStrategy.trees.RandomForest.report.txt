Wed Oct 25 15.33.42 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.42 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 14.085545722713865
Incorrectly Classified Instances: 46.902654867256636
Correctly Classified Instances: 53.097345132743364
Weighted Precision: 0.4091675286365552
Weighted AreaUnderROC: 0.8201144755298065
Root mean squared error: 0.16573810250571167
Relative absolute error: 64.01452606646919
Root relative squared error: 82.94107999418365
Weighted TruePositiveRate: 0.5309734513274337
Weighted MatthewsCorrelation: 0.3924733121290515
Weighted FMeasure: 0.44425852937045024
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5439768798279717
Mean absolute error: 0.05112271178919404
Coverage of cases: 74.33628318584071
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 36.0
Weighted Recall: 0.5309734513274337
Weighted FalsePositiveRate: 0.10484498714285638
Kappa statistic: 0.42612111920275975
Training time: 30.0
		
Time end:Wed Oct 25 15.33.42 EEST 2017