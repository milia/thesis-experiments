Wed Oct 25 15.33.41 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.41 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.41740412979357
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5149633784938863
Weighted AreaUnderROC: 0.7824460835143099
Root mean squared error: 0.16261883561333343
Relative absolute error: 70.0041530317667
Root relative squared error: 81.38009093413908
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4869235813125953
Weighted FMeasure: 0.5237783294817943
Iteration time: 34.0
Weighted AreaUnderPRC: 0.4547076382190571
Mean absolute error: 0.055906094435091556
Coverage of cases: 96.46017699115045
Instances selection time: 34.0
Test time: 25.0
Accumulative iteration time: 34.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08081000159536497
Kappa statistic: 0.5078583601590608
Training time: 0.0
		
Time end:Wed Oct 25 15.33.41 EEST 2017