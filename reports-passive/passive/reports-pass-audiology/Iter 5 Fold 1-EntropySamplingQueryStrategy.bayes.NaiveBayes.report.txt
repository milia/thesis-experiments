Wed Oct 25 15.33.26 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.26 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 5.199115044247787
Incorrectly Classified Instances: 70.79646017699115
Correctly Classified Instances: 29.20353982300885
Weighted Precision: 0.08606830730724535
Weighted AreaUnderROC: 0.8153890317109433
Root mean squared error: 0.23424281401031066
Relative absolute error: 72.69970245633374
Root relative squared error: 117.22320746505657
Weighted TruePositiveRate: 0.2920353982300885
Weighted MatthewsCorrelation: 0.06509122624943038
Weighted FMeasure: 0.13294670744403958
Iteration time: 59.0
Weighted AreaUnderPRC: 0.5056616883664868
Mean absolute error: 0.0580587901560998
Coverage of cases: 36.283185840707965
Instances selection time: 47.0
Test time: 68.0
Accumulative iteration time: 59.0
Weighted Recall: 0.2920353982300885
Weighted FalsePositiveRate: 0.21917349395905114
Kappa statistic: 0.07961718590918346
Training time: 12.0
		
Time end:Wed Oct 25 15.33.27 EEST 2017