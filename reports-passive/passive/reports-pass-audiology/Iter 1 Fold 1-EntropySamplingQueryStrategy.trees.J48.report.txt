Wed Oct 25 15.33.41 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.41 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 15.671091445427722
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.2632835024915421
Weighted AreaUnderROC: 0.7201824930834073
Root mean squared error: 0.18738197138455553
Relative absolute error: 67.36390453204608
Root relative squared error: 93.77242072346426
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.267321909234297
Weighted FMeasure: 0.32633253339646684
Iteration time: 64.0
Weighted AreaUnderPRC: 0.29310439995959586
Mean absolute error: 0.053797562647120205
Coverage of cases: 73.45132743362832
Instances selection time: 17.0
Test time: 10.0
Accumulative iteration time: 64.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.1494942535361661
Kappa statistic: 0.29644506929102227
Training time: 47.0
		
Time end:Wed Oct 25 15.33.41 EEST 2017