Wed Oct 25 15.42.48 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.48 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 15.052566481137752
Incorrectly Classified Instances: 9.523809523809524
Correctly Classified Instances: 90.47619047619048
Weighted Precision: 0.9063903722200096
Weighted AreaUnderROC: 0.953778477414841
Root mean squared error: 0.1628352531570963
Relative absolute error: 12.499204499204463
Root relative squared error: 46.53404961004434
Weighted TruePositiveRate: 0.9047619047619048
Weighted MatthewsCorrelation: 0.889494666874202
Weighted FMeasure: 0.9049138907930098
Iteration time: 46.0
Weighted AreaUnderPRC: 0.8527859354727615
Mean absolute error: 0.030610296732745755
Coverage of cases: 91.68831168831169
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9047619047619048
Weighted FalsePositiveRate: 0.015873015873015872
Kappa statistic: 0.8888888888888887
Training time: 39.0
		
Time end:Wed Oct 25 15.42.49 EEST 2017