Wed Oct 25 15.42.48 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.48 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 15.757575757575522
Incorrectly Classified Instances: 7.965367965367966
Correctly Classified Instances: 92.03463203463204
Weighted Precision: 0.9197837992924089
Weighted AreaUnderROC: 0.9528899383444839
Root mean squared error: 0.14822925552353478
Relative absolute error: 11.519730022403701
Root relative squared error: 42.36003811494985
Weighted TruePositiveRate: 0.9203463203463204
Weighted MatthewsCorrelation: 0.9066810233111094
Weighted FMeasure: 0.9196593296832798
Iteration time: 79.0
Weighted AreaUnderPRC: 0.8652207652321843
Mean absolute error: 0.028211583728335717
Coverage of cases: 92.64069264069263
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9203463203463204
Weighted FalsePositiveRate: 0.013275613275613277
Kappa statistic: 0.9070707070707069
Training time: 72.0
		
Time end:Wed Oct 25 15.42.48 EEST 2017