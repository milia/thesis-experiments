Wed Oct 25 15.42.48 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.48 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 16.3636363636361
Incorrectly Classified Instances: 9.523809523809524
Correctly Classified Instances: 90.47619047619048
Weighted Precision: 0.9064605928412025
Weighted AreaUnderROC: 0.9581455245091608
Root mean squared error: 0.15914160119552317
Relative absolute error: 12.475279106858034
Root relative squared error: 45.47850064082784
Weighted TruePositiveRate: 0.9047619047619048
Weighted MatthewsCorrelation: 0.8888389599307035
Weighted FMeasure: 0.9025939120980158
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8654264283835775
Mean absolute error: 0.030551703935162662
Coverage of cases: 93.24675324675324
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9047619047619048
Weighted FalsePositiveRate: 0.015873015873015872
Kappa statistic: 0.8888888888888887
Training time: 42.0
		
Time end:Wed Oct 25 15.42.48 EEST 2017