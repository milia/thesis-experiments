Wed Oct 25 15.47.39 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.39 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.52933333333472
Incorrectly Classified Instances: 21.176
Correctly Classified Instances: 78.824
Weighted Precision: 0.7889525590340799
Weighted AreaUnderROC: 0.9292815641826244
Root mean squared error: 0.32066144184995016
Relative absolute error: 50.85419999999906
Root relative squared error: 68.02256399914651
Weighted TruePositiveRate: 0.78824
Weighted MatthewsCorrelation: 0.6826495628762136
Weighted FMeasure: 0.7879963002048149
Iteration time: 219.1
Weighted AreaUnderPRC: 0.8387089208006895
Mean absolute error: 0.22601866666666362
Coverage of cases: 99.72
Instances selection time: 25.5
Test time: 32.0
Accumulative iteration time: 219.1
Weighted Recall: 0.78824
Weighted FalsePositiveRate: 0.1060139599855203
Kappa statistic: 0.6823556766743644
Training time: 193.6
		
Time end:Wed Oct 25 15.47.42 EEST 2017