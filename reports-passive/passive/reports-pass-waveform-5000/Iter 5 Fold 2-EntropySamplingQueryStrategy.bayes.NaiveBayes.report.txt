Wed Oct 25 15.47.02 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.02 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.02666666666796
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.839884876278365
Weighted AreaUnderROC: 0.9594105937712233
Root mean squared error: 0.3311340943730951
Relative absolute error: 29.83770510413495
Root relative squared error: 70.24414908398435
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7288734231951811
Weighted FMeasure: 0.7962995820544271
Iteration time: 164.0
Weighted AreaUnderPRC: 0.9254551971390003
Mean absolute error: 0.13261202268504485
Coverage of cases: 89.52
Instances selection time: 148.0
Test time: 155.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.0951665107811081
Kappa statistic: 0.7118817797631513
Training time: 16.0
		
Time end:Wed Oct 25 15.47.03 EEST 2017