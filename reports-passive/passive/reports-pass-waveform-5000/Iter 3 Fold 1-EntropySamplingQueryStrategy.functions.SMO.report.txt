Wed Oct 25 15.47.05 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.05 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 66.76000000000198
Incorrectly Classified Instances: 17.0
Correctly Classified Instances: 83.0
Weighted Precision: 0.8300460138363338
Weighted AreaUnderROC: 0.914353706674957
Root mean squared error: 0.33453118107451985
Relative absolute error: 58.55999999999907
Root relative squared error: 70.96477999684117
Weighted TruePositiveRate: 0.83
Weighted MatthewsCorrelation: 0.7450667734850306
Weighted FMeasure: 0.8297010426683646
Iteration time: 204.0
Weighted AreaUnderPRC: 0.7746813240357915
Mean absolute error: 0.26026666666666376
Coverage of cases: 99.92
Instances selection time: 16.0
Test time: 32.0
Accumulative iteration time: 204.0
Weighted Recall: 0.83
Weighted FalsePositiveRate: 0.08498692223440378
Kappa statistic: 0.7450162373660045
Training time: 188.0
		
Time end:Wed Oct 25 15.47.06 EEST 2017