Wed Oct 25 15.47.01 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.01 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.22666666666784
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8344615261449524
Weighted AreaUnderROC: 0.9499562299296531
Root mean squared error: 0.3367847646733525
Relative absolute error: 30.03232212201854
Root relative squared error: 71.44283727025278
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7200196394703214
Weighted FMeasure: 0.7912977545688329
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9090464915414496
Mean absolute error: 0.13347698720897191
Coverage of cases: 88.28
Instances selection time: 111.0
Test time: 163.0
Accumulative iteration time: 125.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.0981420639779238
Kappa statistic: 0.7028908955467418
Training time: 14.0
		
Time end:Wed Oct 25 15.47.02 EEST 2017