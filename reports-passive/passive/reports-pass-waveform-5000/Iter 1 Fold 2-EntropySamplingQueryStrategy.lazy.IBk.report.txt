Wed Oct 25 15.47.11 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.11 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7110899382823687
Weighted AreaUnderROC: 0.7833685441513302
Root mean squared error: 0.4374847746508457
Relative absolute error: 43.65805168986067
Root relative squared error: 92.80453524644426
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5666826354508104
Weighted FMeasure: 0.7110068927439108
Iteration time: 1415.0
Weighted AreaUnderPRC: 0.6022512608803426
Mean absolute error: 0.19403578528827053
Coverage of cases: 71.12
Instances selection time: 1414.0
Test time: 1475.0
Accumulative iteration time: 1415.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.1444629116973397
Kappa statistic: 0.566820411422214
Training time: 1.0
		
Time end:Wed Oct 25 15.47.14 EEST 2017