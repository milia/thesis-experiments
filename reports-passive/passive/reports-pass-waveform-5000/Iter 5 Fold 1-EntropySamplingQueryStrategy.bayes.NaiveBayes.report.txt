Wed Oct 25 15.47.02 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.47.02 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.53333333333454
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8324302484122805
Weighted AreaUnderROC: 0.9559809474179496
Root mean squared error: 0.32719714751344503
Relative absolute error: 29.558230101070475
Root relative squared error: 69.40899653749547
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7236587624653952
Weighted FMeasure: 0.7971424914746904
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9168591494254873
Mean absolute error: 0.13136991156031383
Coverage of cases: 90.36
Instances selection time: 209.0
Test time: 229.0
Accumulative iteration time: 218.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09604257822692149
Kappa statistic: 0.7094403257527413
Training time: 9.0
		
Time end:Wed Oct 25 15.47.02 EEST 2017