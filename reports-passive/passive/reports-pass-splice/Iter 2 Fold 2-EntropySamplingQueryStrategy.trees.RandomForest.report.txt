Wed Oct 25 15.45.25 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.25 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 96.5308254963429
Incorrectly Classified Instances: 28.087774294670847
Correctly Classified Instances: 71.91222570532915
Weighted Precision: 0.7336041092421501
Weighted AreaUnderROC: 0.8734115654501967
Root mean squared error: 0.378098886453948
Relative absolute error: 73.72602015543517
Root relative squared error: 80.20688597119982
Weighted TruePositiveRate: 0.7191222570532916
Weighted MatthewsCorrelation: 0.5274418309185018
Weighted FMeasure: 0.7026728535054485
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8109442178015939
Mean absolute error: 0.3276712006908304
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 19.0
Accumulative iteration time: 247.0
Weighted Recall: 0.7191222570532916
Weighted FalsePositiveRate: 0.250536764691607
Kappa statistic: 0.5053880145747732
Training time: 232.0
		
Time end:Wed Oct 25 15.45.26 EEST 2017