Wed Oct 25 15.45.22 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.22 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 48.87565308254956
Incorrectly Classified Instances: 16.38871473354232
Correctly Classified Instances: 83.61128526645767
Weighted Precision: 0.8397892077630692
Weighted AreaUnderROC: 0.9016764101204732
Root mean squared error: 0.30610446781480927
Relative absolute error: 29.578542380712246
Root relative squared error: 64.93456348300454
Weighted TruePositiveRate: 0.8361128526645768
Weighted MatthewsCorrelation: 0.7387920105796593
Weighted FMeasure: 0.8367053840773716
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8161954109223337
Mean absolute error: 0.13146018835872408
Coverage of cases: 92.60188087774296
Instances selection time: 10.5
Test time: 9.4
Accumulative iteration time: 68.0
Weighted Recall: 0.8361128526645768
Weighted FalsePositiveRate: 0.0930621344007695
Kappa statistic: 0.7357126526557393
Training time: 57.5
		
Time end:Wed Oct 25 15.45.24 EEST 2017