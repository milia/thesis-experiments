Wed Oct 25 15.45.04 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.04 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 67.08463949843151
Incorrectly Classified Instances: 8.652037617554859
Correctly Classified Instances: 91.34796238244515
Weighted Precision: 0.9148906820110267
Weighted AreaUnderROC: 0.9481557698414137
Root mean squared error: 0.3091544180456511
Relative absolute error: 55.04702194357364
Root relative squared error: 65.58155563015745
Weighted TruePositiveRate: 0.9134796238244514
Weighted MatthewsCorrelation: 0.8653178065799269
Weighted FMeasure: 0.9135858570968897
Iteration time: 930.0
Weighted AreaUnderPRC: 0.8798691755836148
Mean absolute error: 0.2446534308603328
Coverage of cases: 98.99686520376176
Instances selection time: 159.0
Test time: 209.0
Accumulative iteration time: 930.0
Weighted Recall: 0.9134796238244514
Weighted FalsePositiveRate: 0.045535173645535915
Kappa statistic: 0.8599656832899761
Training time: 771.0
		
Time end:Wed Oct 25 15.45.05 EEST 2017