Wed Oct 25 15.45.05 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.05 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 38.9550679205852
Incorrectly Classified Instances: 35.36050156739812
Correctly Classified Instances: 64.63949843260188
Weighted Precision: 0.7286150486198697
Weighted AreaUnderROC: 0.796592018494373
Root mean squared error: 0.451103554933281
Relative absolute error: 52.15400888588379
Root relative squared error: 95.6935148132033
Weighted TruePositiveRate: 0.6463949843260188
Weighted MatthewsCorrelation: 0.504726774184752
Weighted FMeasure: 0.6439806531670194
Iteration time: 970.0
Weighted AreaUnderPRC: 0.6643643996977753
Mean absolute error: 0.23179559504837766
Coverage of cases: 72.10031347962382
Instances selection time: 969.0
Test time: 1088.0
Accumulative iteration time: 970.0
Weighted Recall: 0.6463949843260188
Weighted FalsePositiveRate: 0.13440658932077554
Kappa statistic: 0.48031255885319607
Training time: 1.0
		
Time end:Wed Oct 25 15.45.08 EEST 2017