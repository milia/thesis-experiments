Wed Oct 25 15.45.26 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.26 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 95.31870428422157
Incorrectly Classified Instances: 26.018808777429467
Correctly Classified Instances: 73.98119122257053
Weighted Precision: 0.7579830201529852
Weighted AreaUnderROC: 0.8959391881398667
Root mean squared error: 0.3657743784392549
Relative absolute error: 69.95908017295284
Root relative squared error: 77.59246301360663
Weighted TruePositiveRate: 0.7398119122257053
Weighted MatthewsCorrelation: 0.5706143098023732
Weighted FMeasure: 0.7248304438008585
Iteration time: 163.0
Weighted AreaUnderPRC: 0.8451445106120465
Mean absolute error: 0.31092924521313076
Coverage of cases: 99.62382445141066
Instances selection time: 13.0
Test time: 21.0
Accumulative iteration time: 163.0
Weighted Recall: 0.7398119122257053
Weighted FalsePositiveRate: 0.2321055506193314
Kappa statistic: 0.5422463864832586
Training time: 150.0
		
Time end:Wed Oct 25 15.45.27 EEST 2017