Wed Oct 25 15.45.23 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.45.23 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 52.97805642633208
Incorrectly Classified Instances: 16.112852664576803
Correctly Classified Instances: 83.8871473354232
Weighted Precision: 0.8431675019818493
Weighted AreaUnderROC: 0.9075944875221866
Root mean squared error: 0.30757651581963613
Relative absolute error: 28.70345410490904
Root relative squared error: 65.2468320209381
Weighted TruePositiveRate: 0.838871473354232
Weighted MatthewsCorrelation: 0.742639638192508
Weighted FMeasure: 0.840357675519613
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8237493716049451
Mean absolute error: 0.12757090713293195
Coverage of cases: 94.60815047021944
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 35.0
Weighted Recall: 0.838871473354232
Weighted FalsePositiveRate: 0.09110361621137483
Kappa statistic: 0.7408184878997202
Training time: 30.0
		
Time end:Wed Oct 25 15.45.24 EEST 2017