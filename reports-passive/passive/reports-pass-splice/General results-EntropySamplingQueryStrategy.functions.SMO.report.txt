Wed Oct 25 15.44.48 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.44.48 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 67.04493207941373
Incorrectly Classified Instances: 9.92476489028213
Correctly Classified Instances: 90.07523510971787
Weighted Precision: 0.9024956254281747
Weighted AreaUnderROC: 0.9385991934674912
Root mean squared error: 0.3139965478870243
Relative absolute error: 55.708463949843235
Root relative squared error: 66.60872648402368
Weighted TruePositiveRate: 0.9007523510971787
Weighted MatthewsCorrelation: 0.8422171756538959
Weighted FMeasure: 0.9010011603458652
Iteration time: 1394.8
Weighted AreaUnderPRC: 0.8641032241456557
Mean absolute error: 0.24759317311042
Coverage of cases: 98.871473354232
Instances selection time: 223.1
Test time: 226.9
Accumulative iteration time: 1394.8
Weighted Recall: 0.9007523510971787
Weighted FalsePositiveRate: 0.05554758288208171
Kappa statistic: 0.8394751972694847
Training time: 1171.7
		
Time end:Wed Oct 25 15.45.05 EEST 2017