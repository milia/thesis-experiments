Wed Oct 25 15.36.27 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.27 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.257822277847309
Correctly Classified Instances: 93.74217772215269
Weighted Precision: 0.9374551176547007
Weighted AreaUnderROC: 0.9370119525039042
Root mean squared error: 0.25015639663712996
Relative absolute error: 12.51564455569462
Root relative squared error: 50.03127932742599
Weighted TruePositiveRate: 0.9374217772215269
Weighted MatthewsCorrelation: 0.8745869143845696
Weighted FMeasure: 0.9374016293768392
Iteration time: 362.0
Weighted AreaUnderPRC: 0.9099147954209579
Mean absolute error: 0.0625782227784731
Coverage of cases: 93.74217772215269
Instances selection time: 41.0
Test time: 57.0
Accumulative iteration time: 362.0
Weighted Recall: 0.9374217772215269
Weighted FalsePositiveRate: 0.06339787221371815
Kappa statistic: 0.8745180566788903
Training time: 321.0
		
Time end:Wed Oct 25 15.36.28 EEST 2017