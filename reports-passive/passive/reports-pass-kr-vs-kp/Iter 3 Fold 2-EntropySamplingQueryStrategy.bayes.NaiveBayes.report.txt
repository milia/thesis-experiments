Wed Oct 25 15.36.24 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.24 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 85.32540675844805
Incorrectly Classified Instances: 12.3279098873592
Correctly Classified Instances: 87.6720901126408
Weighted Precision: 0.876728457544824
Weighted AreaUnderROC: 0.9579739603362083
Root mean squared error: 0.29751430309229715
Relative absolute error: 42.61690162040412
Root relative squared error: 59.50286061845943
Weighted TruePositiveRate: 0.876720901126408
Weighted MatthewsCorrelation: 0.7529548047011045
Weighted FMeasure: 0.876724335538671
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9596788251014681
Mean absolute error: 0.21308450810202062
Coverage of cases: 99.93742177722153
Instances selection time: 26.0
Test time: 77.0
Accumulative iteration time: 27.0
Weighted Recall: 0.876720901126408
Weighted FalsePositiveRate: 0.12372414235038208
Kappa statistic: 0.7529542126468081
Training time: 1.0
		
Time end:Wed Oct 25 15.36.24 EEST 2017