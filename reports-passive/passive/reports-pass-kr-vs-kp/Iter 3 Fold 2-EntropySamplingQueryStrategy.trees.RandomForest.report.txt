Wed Oct 25 15.36.41 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.36.41 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 84.35544430538172
Incorrectly Classified Instances: 6.6958698372966206
Correctly Classified Instances: 93.30413016270337
Weighted Precision: 0.9333580605338941
Weighted AreaUnderROC: 0.9776316305789469
Root mean squared error: 0.25909226516448464
Relative absolute error: 35.80425026232282
Root relative squared error: 51.818453032896926
Weighted TruePositiveRate: 0.9330413016270338
Weighted MatthewsCorrelation: 0.8661995130238788
Weighted FMeasure: 0.933067735507162
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9723272489420718
Mean absolute error: 0.1790212513116141
Coverage of cases: 99.87484355444306
Instances selection time: 24.0
Test time: 29.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9330413016270338
Weighted FalsePositiveRate: 0.06615750696210415
Kappa statistic: 0.8659541008076339
Training time: 51.0
		
Time end:Wed Oct 25 15.36.41 EEST 2017