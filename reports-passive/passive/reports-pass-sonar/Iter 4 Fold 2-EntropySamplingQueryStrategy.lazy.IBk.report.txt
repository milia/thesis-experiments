Wed Oct 25 15.43.41 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.41 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6462904465124221
Weighted AreaUnderROC: 0.644345238095238
Root mean squared error: 0.5705196658565357
Relative absolute error: 73.77622377622382
Root relative squared error: 114.10393317130713
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.2878883338338082
Weighted FMeasure: 0.6447253052757641
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5953014621054405
Mean absolute error: 0.3688811188811191
Coverage of cases: 64.42307692307692
Instances selection time: 13.0
Test time: 7.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.3555402930402931
Kappa statistic: 0.28740740740740744
Training time: 0.0
		
Time end:Wed Oct 25 15.43.42 EEST 2017