Wed Oct 25 15.43.42 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.42 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5807763664906522
Weighted AreaUnderROC: 0.5788497217068646
Root mean squared error: 0.5889517253077057
Relative absolute error: 87.60683760683753
Root relative squared error: 117.79034506154113
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.15769944341372913
Weighted FMeasure: 0.5769230769230769
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5471544252450846
Mean absolute error: 0.43803418803418764
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 19.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.4192236335093478
Kappa statistic: 0.15665315149281228
Training time: 18.0
		
Time end:Wed Oct 25 15.43.42 EEST 2017