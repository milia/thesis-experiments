Wed Oct 25 15.43.43 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.43 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 50.0
Correctly Classified Instances: 50.0
Weighted Precision: 0.5033395176252319
Weighted AreaUnderROC: 0.501669758812616
Root mean squared error: 0.7071067811865476
Relative absolute error: 100.0
Root relative squared error: 141.4213562373095
Weighted TruePositiveRate: 0.5
Weighted MatthewsCorrelation: 0.003339517625231911
Weighted FMeasure: 0.5
Iteration time: 11.0
Weighted AreaUnderPRC: 0.50249908058974
Mean absolute error: 0.5
Coverage of cases: 50.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5
Weighted FalsePositiveRate: 0.49666048237476806
Kappa statistic: 0.0033173608551419396
Training time: 10.0
		
Time end:Wed Oct 25 15.43.43 EEST 2017