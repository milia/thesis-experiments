Wed Oct 25 15.44.13 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.44.13 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 20.234604105571847
Correctly Classified Instances: 79.76539589442815
Weighted Precision: 0.7492894755365426
Weighted AreaUnderROC: 0.9241445853409397
Root mean squared error: 0.21535704433732192
Relative absolute error: 95.36445484026049
Root relative squared error: 96.44427007056737
Weighted TruePositiveRate: 0.7976539589442815
Weighted MatthewsCorrelation: 0.749784830089698
Weighted FMeasure: 0.7621899625882255
Iteration time: 3998.0
Weighted AreaUnderPRC: 0.715054298434057
Mean absolute error: 0.09510028737532986
Coverage of cases: 91.20234604105572
Instances selection time: 37.0
Test time: 63.0
Accumulative iteration time: 3998.0
Weighted Recall: 0.7976539589442815
Weighted FalsePositiveRate: 0.022638934708602242
Kappa statistic: 0.7770544927371442
Training time: 3961.0
		
Time end:Wed Oct 25 15.44.17 EEST 2017