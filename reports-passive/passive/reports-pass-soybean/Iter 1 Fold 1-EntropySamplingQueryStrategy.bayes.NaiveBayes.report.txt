Wed Oct 25 15.43.58 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.43.58 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.418590335487858
Incorrectly Classified Instances: 28.362573099415204
Correctly Classified Instances: 71.6374269005848
Weighted Precision: 0.7059561857704273
Weighted AreaUnderROC: 0.9536972475718579
Root mean squared error: 0.1533886317435636
Relative absolute error: 32.641148517973356
Root relative squared error: 68.692689719773
Weighted TruePositiveRate: 0.716374269005848
Weighted MatthewsCorrelation: 0.6744822260577603
Weighted FMeasure: 0.6814520500408174
Iteration time: 227.0
Weighted AreaUnderPRC: 0.7995891238298211
Mean absolute error: 0.03255072982401804
Coverage of cases: 83.04093567251462
Instances selection time: 143.0
Test time: 327.0
Accumulative iteration time: 227.0
Weighted Recall: 0.716374269005848
Weighted FalsePositiveRate: 0.03582192617587109
Kappa statistic: 0.684279650532006
Training time: 84.0
		
Time end:Wed Oct 25 15.43.58 EEST 2017