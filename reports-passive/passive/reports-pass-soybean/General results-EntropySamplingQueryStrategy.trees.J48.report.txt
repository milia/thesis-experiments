Wed Oct 25 15.44.26 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.44.26 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.085201040879733
Incorrectly Classified Instances: 36.952118811202006
Correctly Classified Instances: 63.04788118879799
Weighted Precision: 0.6038842390476284
Weighted AreaUnderROC: 0.8481339223271416
Root mean squared error: 0.17806285851334763
Relative absolute error: 43.0237361584365
Root relative squared error: 79.74265466375728
Weighted TruePositiveRate: 0.6304788118879799
Weighted MatthewsCorrelation: 0.5764098164873186
Weighted FMeasure: 0.5892623815673405
Iteration time: 37.4
Weighted AreaUnderPRC: 0.5464185748159227
Mean absolute error: 0.042904556833898266
Coverage of cases: 74.87712438476446
Instances selection time: 16.2
Test time: 18.7
Accumulative iteration time: 37.4
Weighted Recall: 0.6304788118879799
Weighted FalsePositiveRate: 0.03354761407236796
Kappa statistic: 0.5931441336436956
Training time: 21.2
		
Time end:Wed Oct 25 15.44.27 EEST 2017