Wed Oct 25 15.44.27 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.44.27 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 13.582342954159621
Incorrectly Classified Instances: 42.22873900293255
Correctly Classified Instances: 57.77126099706745
Weighted Precision: 0.5030838263747824
Weighted AreaUnderROC: 0.8409224255511623
Root mean squared error: 0.1814109142536745
Relative absolute error: 49.37916387137268
Root relative squared error: 81.24202884501518
Weighted TruePositiveRate: 0.5777126099706745
Weighted MatthewsCorrelation: 0.4916843842283162
Weighted FMeasure: 0.5054368902999973
Iteration time: 9.0
Weighted AreaUnderPRC: 0.48549073815926813
Mean absolute error: 0.04924237948391779
Coverage of cases: 78.59237536656892
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 9.0
Weighted Recall: 0.5777126099706745
Weighted FalsePositiveRate: 0.04795907759464966
Kappa statistic: 0.5297227409854907
Training time: 2.0
		
Time end:Wed Oct 25 15.44.27 EEST 2017