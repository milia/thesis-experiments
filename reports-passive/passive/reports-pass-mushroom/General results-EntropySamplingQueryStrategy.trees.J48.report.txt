Wed Oct 25 15.42.15 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.15 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.043082225504676
Incorrectly Classified Instances: 0.6179222058099458
Correctly Classified Instances: 99.38207779419005
Weighted Precision: 0.9939445593065912
Weighted AreaUnderROC: 0.995987360036042
Root mean squared error: 0.06730129075781485
Relative absolute error: 1.8613822080974007
Root relative squared error: 13.460258151562968
Weighted TruePositiveRate: 0.9938207779419004
Weighted MatthewsCorrelation: 0.9877450288351856
Weighted FMeasure: 0.9938176532475828
Iteration time: 30.1
Weighted AreaUnderPRC: 0.9939429232798188
Mean absolute error: 0.009306911040487004
Coverage of cases: 99.43870014771048
Instances selection time: 9.5
Test time: 16.8
Accumulative iteration time: 30.1
Weighted Recall: 0.9938207779419004
Weighted FalsePositiveRate: 0.0065442930519880455
Kappa statistic: 0.9876166572292581
Training time: 20.6
		
Time end:Wed Oct 25 15.42.16 EEST 2017