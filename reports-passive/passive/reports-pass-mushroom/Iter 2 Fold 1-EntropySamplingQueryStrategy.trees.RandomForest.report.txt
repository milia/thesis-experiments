Wed Oct 25 15.42.17 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.17 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.64057114721812
Incorrectly Classified Instances: 0.29542097488921715
Correctly Classified Instances: 99.70457902511079
Weighted Precision: 0.997062543803559
Weighted AreaUnderROC: 0.9999130990340884
Root mean squared error: 0.0661437109481598
Relative absolute error: 3.411492114060836
Root relative squared error: 13.228742189631959
Weighted TruePositiveRate: 0.9970457902511078
Weighted MatthewsCorrelation: 0.994100083535708
Weighted FMeasure: 0.9970454502679525
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9999147728738907
Mean absolute error: 0.01705746057030418
Coverage of cases: 100.0
Instances selection time: 30.0
Test time: 48.0
Accumulative iteration time: 105.0
Weighted Recall: 0.9970457902511078
Weighted FalsePositiveRate: 0.0031744930090240697
Kappa statistic: 0.9940826793332744
Training time: 75.0
		
Time end:Wed Oct 25 15.42.17 EEST 2017