Wed Oct 25 15.42.04 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.42.04 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9993571027703445
Root mean squared error: 0.022803162500599997
Relative absolute error: 0.26543985613108256
Root relative squared error: 4.56063250012
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 717.0
Weighted AreaUnderPRC: 0.999286610010517
Mean absolute error: 0.0013271992806554128
Coverage of cases: 99.9507631708518
Instances selection time: 716.0
Test time: 934.0
Accumulative iteration time: 717.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 1.0
		
Time end:Wed Oct 25 15.42.06 EEST 2017