Wed Oct 25 15.34.43 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.43 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.2
Correctly Classified Instances: 63.8
Weighted Precision: 0.6842665750601168
Weighted AreaUnderROC: 0.6290476190476191
Root mean squared error: 0.6016643582596529
Relative absolute error: 72.39999999999999
Root relative squared error: 120.33287165193059
Weighted TruePositiveRate: 0.638
Weighted MatthewsCorrelation: 0.23918222049163881
Weighted FMeasure: 0.6515121242393971
Iteration time: 115.0
Weighted AreaUnderPRC: 0.6462137082658553
Mean absolute error: 0.362
Coverage of cases: 63.8
Instances selection time: 24.0
Test time: 33.0
Accumulative iteration time: 115.0
Weighted Recall: 0.638
Weighted FalsePositiveRate: 0.37990476190476186
Kappa statistic: 0.2304421768707484
Training time: 91.0
		
Time end:Wed Oct 25 15.34.43 EEST 2017