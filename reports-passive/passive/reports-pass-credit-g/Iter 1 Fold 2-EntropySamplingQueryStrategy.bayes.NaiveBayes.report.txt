Wed Oct 25 15.34.41 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.41 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 85.7
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.7234375
Weighted AreaUnderROC: 0.7414285714285714
Root mean squared error: 0.46040666009800707
Relative absolute error: 67.07491765410924
Root relative squared error: 92.08133201960142
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.33561084188120116
Weighted FMeasure: 0.7049645390070923
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7564598005565945
Mean absolute error: 0.3353745882705462
Coverage of cases: 94.2
Instances selection time: 28.0
Test time: 65.0
Accumulative iteration time: 38.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.33980952380952373
Kappa statistic: 0.32980599647266307
Training time: 10.0
		
Time end:Wed Oct 25 15.34.41 EEST 2017