Wed Oct 25 15.34.49 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.49 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 94.49999999999999
Incorrectly Classified Instances: 30.139999999999997
Correctly Classified Instances: 69.85999999999999
Weighted Precision: 0.6706602245405048
Weighted AreaUnderROC: 0.6658564761904763
Root mean squared error: 0.45084023124331096
Relative absolute error: 74.28436135089188
Root relative squared error: 90.1680462486622
Weighted TruePositiveRate: 0.6986000000000001
Weighted MatthewsCorrelation: 0.18932537934046806
Weighted FMeasure: 0.6627331559123113
Iteration time: 52.2
Weighted AreaUnderPRC: 0.7019830862760867
Mean absolute error: 0.37142180675445935
Coverage of cases: 98.72
Instances selection time: 14.6
Test time: 11.2
Accumulative iteration time: 52.2
Weighted Recall: 0.6986000000000001
Weighted FalsePositiveRate: 0.5508857142857144
Kappa statistic: 0.16857903311705175
Training time: 37.6
		
Time end:Wed Oct 25 15.34.50 EEST 2017