Wed Oct 25 15.34.42 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.42 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 74.4
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7077469793887704
Weighted AreaUnderROC: 0.7213142857142857
Root mean squared error: 0.4671193387768342
Relative absolute error: 59.83595564818056
Root relative squared error: 93.42386775536684
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.29244616114175315
Weighted FMeasure: 0.7088881262868909
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7468452332424327
Mean absolute error: 0.2991797782409028
Coverage of cases: 90.4
Instances selection time: 19.0
Test time: 37.0
Accumulative iteration time: 21.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4746666666666667
Kappa statistic: 0.2811839323467229
Training time: 2.0
		
Time end:Wed Oct 25 15.34.42 EEST 2017