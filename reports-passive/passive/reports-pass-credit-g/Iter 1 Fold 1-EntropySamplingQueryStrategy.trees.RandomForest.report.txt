Wed Oct 25 15.34.49 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.34.49 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 88.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7218584496870486
Weighted AreaUnderROC: 0.7014828571428572
Root mean squared error: 0.4416433469652845
Relative absolute error: 66.44276031175568
Root relative squared error: 88.3286693930569
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.22984063101331753
Weighted FMeasure: 0.6558341597015631
Iteration time: 127.0
Weighted AreaUnderPRC: 0.7243548158613279
Mean absolute error: 0.3322138015587784
Coverage of cases: 97.4
Instances selection time: 46.0
Test time: 24.0
Accumulative iteration time: 127.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.6050476190476191
Kappa statistic: 0.15640394088669937
Training time: 81.0
		
Time end:Wed Oct 25 15.34.49 EEST 2017