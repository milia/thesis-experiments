#!/bin/bash
function getResults{
	head $1 | grep "==>" && tail -n 25 $1 | grep "Correctly Classified Instances"
}

getResults General*Least*
