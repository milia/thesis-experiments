Wed Oct 25 15.33.09 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.09 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.98069784706754
Incorrectly Classified Instances: 9.33184855233853
Correctly Classified Instances: 90.66815144766147
Weighted Precision: 0.9012953837857106
Weighted AreaUnderROC: 0.9616427903285569
Root mean squared error: 0.15107489453653458
Relative absolute error: 19.478178267476302
Root relative squared error: 40.53764806527654
Weighted TruePositiveRate: 0.9066815144766147
Weighted MatthewsCorrelation: 0.7358025001231868
Weighted FMeasure: 0.894276005035241
Iteration time: 49.9
Weighted AreaUnderPRC: 0.9589494211914978
Mean absolute error: 0.05410605074298919
Coverage of cases: 98.41870824053451
Instances selection time: 13.8
Test time: 18.4
Accumulative iteration time: 49.9
Weighted Recall: 0.9066815144766147
Weighted FalsePositiveRate: 0.25198903001872985
Kappa statistic: 0.7293278070738262
Training time: 36.1
		
Time end:Wed Oct 25 15.33.11 EEST 2017