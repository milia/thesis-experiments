Wed Oct 25 15.33.05 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.05 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.8883672007280023
Weighted AreaUnderROC: 0.8549377427379425
Root mean squared error: 0.311404664174734
Relative absolute error: 78.38370983137168
Root relative squared error: 83.55863971262426
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7423986035626717
Weighted FMeasure: 0.8956891939860783
Iteration time: 175.0
Weighted AreaUnderPRC: 0.8589690639829562
Mean absolute error: 0.2177325273093636
Coverage of cases: 99.10913140311804
Instances selection time: 11.0
Test time: 21.0
Accumulative iteration time: 175.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.22245677580403758
Kappa statistic: 0.7561803396641371
Training time: 164.0
		
Time end:Wed Oct 25 15.33.05 EEST 2017