Wed Oct 25 15.33.07 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.07 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.29621380846323
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.9021663721418449
Weighted AreaUnderROC: 0.8808706522030924
Root mean squared error: 0.19874924285011583
Relative absolute error: 20.330319601657663
Root relative squared error: 53.33001809873703
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.7094560131169619
Weighted FMeasure: 0.8835826383392622
Iteration time: 48.0
Weighted AreaUnderPRC: 0.865680281887277
Mean absolute error: 0.05647311000460406
Coverage of cases: 87.75055679287306
Instances selection time: 47.0
Test time: 59.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.11393207323939245
Kappa statistic: 0.7171144412942712
Training time: 1.0
		
Time end:Wed Oct 25 15.33.07 EEST 2017