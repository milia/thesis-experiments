Wed Oct 25 15.33.07 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.07 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.33333333333331
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9128254426976044
Weighted AreaUnderROC: 0.8259901007669569
Root mean squared error: 0.17049817112666088
Relative absolute error: 16.581408979017844
Root relative squared error: 45.7494600814329
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7463556081518048
Weighted FMeasure: 0.9027386620131588
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8432438215906513
Mean absolute error: 0.046059469386160214
Coverage of cases: 92.87305122494432
Instances selection time: 47.0
Test time: 53.0
Accumulative iteration time: 47.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2567057672856855
Kappa statistic: 0.7418925171403333
Training time: 0.0
		
Time end:Wed Oct 25 15.33.07 EEST 2017