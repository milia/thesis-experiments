Wed Oct 25 15.33.06 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.06 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.22197475872306
Incorrectly Classified Instances: 13.808463251670378
Correctly Classified Instances: 86.19153674832963
Weighted Precision: 0.8239849237622066
Weighted AreaUnderROC: 0.7260624588825378
Root mean squared error: 0.20908808315947497
Relative absolute error: 21.835326915138502
Root relative squared error: 56.104220067566075
Weighted TruePositiveRate: 0.8619153674832962
Weighted MatthewsCorrelation: 0.582355439277843
Weighted FMeasure: 0.8338035993875543
Iteration time: 162.0
Weighted AreaUnderPRC: 0.7647191080861656
Mean absolute error: 0.060653685875384125
Coverage of cases: 90.20044543429844
Instances selection time: 161.0
Test time: 84.0
Accumulative iteration time: 162.0
Weighted Recall: 0.8619153674832962
Weighted FalsePositiveRate: 0.3932188440682796
Kappa statistic: 0.5776297622479479
Training time: 1.0
		
Time end:Wed Oct 25 15.33.06 EEST 2017