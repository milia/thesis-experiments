Wed Oct 25 15.33.01 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.01 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 8.106904231625833
Correctly Classified Instances: 91.89309576837418
Weighted Precision: 0.9129977019882884
Weighted AreaUnderROC: 0.891152936215566
Root mean squared error: 0.3131975611130343
Relative absolute error: 79.90620426344348
Root relative squared error: 84.03972444430717
Weighted TruePositiveRate: 0.9189309576837417
Weighted MatthewsCorrelation: 0.7841256358175805
Weighted FMeasure: 0.9134419243938288
Iteration time: 299.7
Weighted AreaUnderPRC: 0.88669607754603
Mean absolute error: 0.221961678509563
Coverage of cases: 99.28730512249442
Instances selection time: 48.5
Test time: 52.5
Accumulative iteration time: 299.7
Weighted Recall: 0.9189309576837417
Weighted FalsePositiveRate: 0.1631251479113869
Kappa statistic: 0.7879539844402847
Training time: 251.2
		
Time end:Wed Oct 25 15.33.06 EEST 2017