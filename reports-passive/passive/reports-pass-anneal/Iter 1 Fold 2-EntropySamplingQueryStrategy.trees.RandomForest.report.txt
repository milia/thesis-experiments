Wed Oct 25 15.33.09 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.09 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.394209354120274
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9159106958552881
Weighted AreaUnderROC: 0.9809941236524915
Root mean squared error: 0.13485470385295104
Relative absolute error: 16.935253522012804
Root relative squared error: 36.185314188096335
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7539585154171803
Weighted FMeasure: 0.8989271360955271
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9722062033143444
Mean absolute error: 0.04704237089447954
Coverage of cases: 99.10913140311804
Instances selection time: 16.0
Test time: 31.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.23655152675727992
Kappa statistic: 0.757310752002661
Training time: 32.0
		
Time end:Wed Oct 25 15.33.09 EEST 2017