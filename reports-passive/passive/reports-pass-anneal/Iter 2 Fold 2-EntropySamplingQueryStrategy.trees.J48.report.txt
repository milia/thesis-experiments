Wed Oct 25 15.33.08 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.08 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.343726800296864
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.910750757904324
Weighted AreaUnderROC: 0.9421004516636471
Root mean squared error: 0.1933752108263425
Relative absolute error: 19.308541042828
Root relative squared error: 51.88801398852687
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.6978000278982975
Weighted FMeasure: 0.8675939182122184
Iteration time: 34.0
Weighted AreaUnderPRC: 0.910902742046262
Mean absolute error: 0.05363483623007725
Coverage of cases: 93.98663697104676
Instances selection time: 16.0
Test time: 8.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.1598554635469267
Kappa statistic: 0.6503743635819108
Training time: 18.0
		
Time end:Wed Oct 25 15.33.08 EEST 2017