Wed Oct 25 15.33.06 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.06 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.22568671121007
Incorrectly Classified Instances: 11.024498886414253
Correctly Classified Instances: 88.97550111358574
Weighted Precision: 0.8907324600423028
Weighted AreaUnderROC: 0.8412314587453062
Root mean squared error: 0.18661323365411542
Relative absolute error: 18.691548979935327
Root relative squared error: 50.07358511419867
Weighted TruePositiveRate: 0.8897550111358574
Weighted MatthewsCorrelation: 0.709018971619285
Weighted FMeasure: 0.8861284247606299
Iteration time: 58.1
Weighted AreaUnderPRC: 0.8421385725399327
Mean absolute error: 0.051920969388708715
Coverage of cases: 91.11358574610247
Instances selection time: 56.7
Test time: 60.0
Accumulative iteration time: 58.1
Weighted Recall: 0.8897550111358574
Weighted FalsePositiveRate: 0.20726366705931798
Kappa statistic: 0.7121940403449886
Training time: 1.4
		
Time end:Wed Oct 25 15.33.08 EEST 2017