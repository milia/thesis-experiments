Wed Oct 25 15.33.08 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.33.08 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.941351150705238
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9060978664753042
Weighted AreaUnderROC: 0.8919877261182962
Root mean squared error: 0.1545115814804066
Relative absolute error: 12.642904932841173
Root relative squared error: 41.45980794014261
Weighted TruePositiveRate: 0.913140311804009
Weighted MatthewsCorrelation: 0.7687376647963035
Weighted FMeasure: 0.9003450654641243
Iteration time: 28.6
Weighted AreaUnderPRC: 0.8894922054539004
Mean absolute error: 0.03511918036900291
Coverage of cases: 95.32293986636971
Instances selection time: 8.3
Test time: 11.0
Accumulative iteration time: 28.6
Weighted Recall: 0.913140311804009
Weighted FalsePositiveRate: 0.18858298421148464
Kappa statistic: 0.7565864097817394
Training time: 20.3
		
Time end:Wed Oct 25 15.33.09 EEST 2017