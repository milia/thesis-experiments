Wed Oct 25 15.32.58 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Oct 25 15.32.58 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.211581291759437
Incorrectly Classified Instances: 12.783964365256125
Correctly Classified Instances: 87.21603563474386
Weighted Precision: 0.864638846791556
Weighted AreaUnderROC: 0.9166772583103047
Root mean squared error: 0.18409853111961544
Relative absolute error: 16.888703719113966
Root relative squared error: 49.39881961695871
Weighted TruePositiveRate: 0.8721603563474387
Weighted MatthewsCorrelation: 0.6650843869433214
Weighted FMeasure: 0.8583109100071695
Iteration time: 81.3
Weighted AreaUnderPRC: 0.8966746560992043
Mean absolute error: 0.046913065886427206
Coverage of cases: 93.11804008908686
Instances selection time: 64.3
Test time: 96.4
Accumulative iteration time: 81.3
Weighted Recall: 0.8721603563474387
Weighted FalsePositiveRate: 0.25437219228745256
Kappa statistic: 0.6465989306168872
Training time: 17.0
		
Time end:Wed Oct 25 15.33.01 EEST 2017