#!/usr/bin/python
import sys, getopt


def stripRes(inputfile, outputfile):
    #f = open('GeneralResults.dat.txt','r')
    f = open(str(inputfile),'r')
    #fout = open('final2.csv','a')
    fout = open(str(outputfile),'a')
    i = 1
    for line in f:
        print str(i) + " , " +  line[-18:]
        fout.write(str(i) + "," + line[-18:])
        i += 1
    f.close()
    fout.close()


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
       opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
       print 'test.py -i <inputfile> -o <outputfile>'
       sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
          print 'test.py -i <inputfile> -o <outputfile>'
          sys.exit()
       elif opt in ("-i", "--ifile"):
          inputfile = arg
       elif opt in ("-o", "--ofile"):
          outputfile = arg
#    print 'Input file is "', inputfile
#    print 'Output file is "', outputfile
    stripRes(inputfile, outputfile)	
	
if __name__ == "__main__":
   main(sys.argv[1:])


