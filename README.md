# Thesis Experiments

## Directories

* `arffs`: This directory contains the datasets that will be used in the subdirectory twentyUCI. 
* `cfgs`: This directory contains the cfg configure files of the JCLAL experiments to be run.
* `paper18`: It contains arff files and cfg files for sound data.
* `reports`: It contains the results of the experiments done for the thesis (with 16 datasets) in a compressed zip format.
* `analysis`: It contains the results of the Friedmann statistical analysis, along with the java files needed to run the analyses.

## Python files

* `makeCFGs.py`: It constructs the cfg files based on the two XML files. Sultan python package should be already installed before running this script (`pip install sultan`). 
* `makeCFGsOkean.py`: The same with the above to run in the Okeanos remote VM.
* `makeCFGsPaper.py`: The same with the above for the arff files from the paper18/ directory.
* `getPC.py`: It returns the percentage of correctly classified instances and only that.
* `makeCFGsPassive.py`: It creates the cfg files in the case of using the passive strategy.
* `makeCFGsSMO.py`: We've rerun the makeCFG script only for the case of the SVM learner for the classifier SMOsync to see if we'd get better results. 
* `getLatexTables.py`: It returns LaTeX tables. See the thesis for more information.
* `getPlots.py`: It returns the learning curves.

## Bash files

* `testFiles.sh`: This script was created to see if the CFGs which were made for each dataset by the script `makeCFGs.py` have the desired classifiers. It's for testing purposes.
* `runExps.sh` : This script was created to execute the experiments in all the datafiles provided in the arffs/ directory. It must be run inside JCLAL directory, where the repository thesis-experiments should be cloned.
* `runExpsPassive.sh`
* `runExpsSMO.sh`
* `getResults.sh`
* `getResults2.sh`
