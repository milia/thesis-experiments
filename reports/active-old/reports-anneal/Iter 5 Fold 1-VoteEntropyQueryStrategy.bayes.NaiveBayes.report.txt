Sat Oct 07 11.25.12 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.12 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.15590200445431
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.903334737855896
Weighted AreaUnderROC: 0.817241110670556
Root mean squared error: 0.16543099000670433
Relative absolute error: 13.565932367108903
Root relative squared error: 44.389792708809736
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.7565493184001295
Weighted FMeasure: 0.8993043217136708
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8320014678542801
Mean absolute error: 0.03768314546419102
Coverage of cases: 93.0957683741648
Instances selection time: 17.0
Test time: 28.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.23610970058048714
Kappa statistic: 0.7625165286380401
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.265033407572343
Incorrectly Classified Instances: 9.799554565701559
Correctly Classified Instances: 90.20044543429844
Weighted Precision: 0.8957185392778702
Weighted AreaUnderROC: 0.8961356032155751
Root mean squared error: 0.16399998513139386
Relative absolute error: 12.88222401395834
Root relative squared error: 44.005813807530394
Weighted TruePositiveRate: 0.9020044543429844
Weighted MatthewsCorrelation: 0.7310015454908959
Weighted FMeasure: 0.8810134841945204
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8764443539968849
Mean absolute error: 0.035783955594328364
Coverage of cases: 94.87750556792874
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9020044543429844
Weighted FalsePositiveRate: 0.24476146368877877
Kappa statistic: 0.724685749324117
Training time: 2.0
		
Time end:Sat Oct 07 11.25.12 EEST 2017