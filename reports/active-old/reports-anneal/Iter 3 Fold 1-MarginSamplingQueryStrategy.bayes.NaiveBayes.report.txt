Sat Oct 07 11.22.32 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.22.32 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.527097253155137
Incorrectly Classified Instances: 13.140311804008908
Correctly Classified Instances: 86.8596881959911
Weighted Precision: 0.8622855118318065
Weighted AreaUnderROC: 0.9626694428733573
Root mean squared error: 0.1685984336446927
Relative absolute error: 15.504035170567736
Root relative squared error: 45.239707023542685
Weighted TruePositiveRate: 0.8685968819599109
Weighted MatthewsCorrelation: 0.66014476855151
Weighted FMeasure: 0.8641128891236789
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9393723934354797
Mean absolute error: 0.04306676436268773
Coverage of cases: 97.32739420935413
Instances selection time: 18.0
Test time: 26.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8685968819599109
Weighted FalsePositiveRate: 0.1995564618127519
Kappa statistic: 0.6741654571843251
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 21.789161098738006
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.9119729571483298
Weighted AreaUnderROC: 0.9568802295334005
Root mean squared error: 0.15006977018312667
Relative absolute error: 14.428850446568877
Root relative squared error: 40.267944899669274
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7764452339674692
Weighted FMeasure: 0.9093302232799545
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9663040578239096
Mean absolute error: 0.040080140129357594
Coverage of cases: 97.32739420935413
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 37.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.0917040097555264
Kappa statistic: 0.7896487508930559
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 20.638455827765437
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.961283717003829
Weighted AreaUnderROC: 0.985817644994073
Root mean squared error: 0.11222121582506774
Relative absolute error: 8.581688632128753
Root relative squared error: 30.11211205230334
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9139372238898571
Weighted FMeasure: 0.9597561304562621
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9746332966609392
Mean absolute error: 0.023838023978135187
Coverage of cases: 97.7728285077951
Instances selection time: 17.0
Test time: 25.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.010006759653183851
Kappa statistic: 0.9093704732730166
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 20.527097253155198
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9641390889680261
Weighted AreaUnderROC: 0.9866755743757613
Root mean squared error: 0.10290203877186638
Relative absolute error: 7.708527637104967
Root relative squared error: 27.6115144460656
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9238351804992877
Weighted FMeasure: 0.9638028000354298
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9793115691030935
Mean absolute error: 0.02141257676973581
Coverage of cases: 97.7728285077951
Instances selection time: 15.0
Test time: 26.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.00962383464243092
Kappa statistic: 0.9194455076069277
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.304380103934655
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.939756139902493
Weighted AreaUnderROC: 0.9790881954803781
Root mean squared error: 0.14157361538187288
Relative absolute error: 11.192529033470022
Root relative squared error: 37.9881873377135
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8406451509298444
Weighted FMeasure: 0.9272532142847904
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9593975013904356
Mean absolute error: 0.031090358426305306
Coverage of cases: 97.32739420935413
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 83.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.014472266404471875
Kappa statistic: 0.8333183341957837
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.193021529324454
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9442107143036423
Weighted AreaUnderROC: 0.987134361026801
Root mean squared error: 0.12332439780456347
Relative absolute error: 9.231343471541157
Root relative squared error: 33.091408413027736
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8575165961812667
Weighted FMeasure: 0.9351244173109226
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9730730036619798
Mean absolute error: 0.02564262075428074
Coverage of cases: 97.55011135857461
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.013706416382966009
Kappa statistic: 0.8514703533867178
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.673348181143275
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9419200280019977
Weighted AreaUnderROC: 0.9820000126251353
Root mean squared error: 0.13572399816403286
Relative absolute error: 9.911080049424825
Root relative squared error: 36.41857032874027
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8490488383497647
Weighted FMeasure: 0.9311707197605122
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9663670544436533
Mean absolute error: 0.027530777915068683
Coverage of cases: 97.32739420935413
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 110.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.013914078060560869
Kappa statistic: 0.8423847808855169
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.4506310319228
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9442031582787002
Weighted AreaUnderROC: 0.9824432282516791
Root mean squared error: 0.13440848685999854
Relative absolute error: 9.759099057194572
Root relative squared error: 36.06558160462146
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8539364445512087
Weighted FMeasure: 0.9339230535409101
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9660666500198057
Mean absolute error: 0.027108608492206875
Coverage of cases: 97.32739420935413
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 122.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.013726435901997064
Kappa statistic: 0.846938775510204
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.45063103192279
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9376987935969211
Weighted AreaUnderROC: 0.9764035149749084
Root mean squared error: 0.15087640937029273
Relative absolute error: 11.37232001765661
Root relative squared error: 40.48438890636749
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.8290439431757299
Weighted FMeasure: 0.9221452623655629
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9583949960274187
Mean absolute error: 0.0315897778268236
Coverage of cases: 95.99109131403118
Instances selection time: 11.0
Test time: 26.0
Accumulative iteration time: 134.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.01505047426741393
Kappa statistic: 0.8200320655562483
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 19.30215293244244
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9376780371058474
Weighted AreaUnderROC: 0.9736466448322586
Root mean squared error: 0.15292446810204008
Relative absolute error: 11.513595180326508
Root relative squared error: 41.033940731899385
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.829097083098535
Weighted FMeasure: 0.9221364651114792
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9530421772790912
Mean absolute error: 0.03198220883423998
Coverage of cases: 95.7683741648107
Instances selection time: 10.0
Test time: 26.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.014875210934255862
Kappa statistic: 0.8200921576920508
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 19.042316258351864
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9347161983032677
Weighted AreaUnderROC: 0.9695509239050148
Root mean squared error: 0.16132764226833116
Relative absolute error: 12.359459635076881
Root relative squared error: 43.28874897141103
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.8172120456250515
Weighted FMeasure: 0.916319099623174
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9473278580650375
Mean absolute error: 0.03433183231965766
Coverage of cases: 94.43207126948775
Instances selection time: 8.0
Test time: 26.0
Accumulative iteration time: 153.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.015537230116964293
Kappa statistic: 0.8070455747531735
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.81959910913138
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9349545797207267
Weighted AreaUnderROC: 0.968573374208977
Root mean squared error: 0.16252014613279203
Relative absolute error: 12.356122020832279
Root relative squared error: 43.60873133593492
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.8174523787704937
Weighted FMeasure: 0.9164262401030385
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9457631032000029
Mean absolute error: 0.034322561168978215
Coverage of cases: 93.76391982182628
Instances selection time: 8.0
Test time: 25.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.015186703450648155
Kappa statistic: 0.8071730608289653
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.893838158871546
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.9313993502687677
Weighted AreaUnderROC: 0.9678044406183337
Root mean squared error: 0.16724601727748686
Relative absolute error: 12.851824795630106
Root relative squared error: 44.876815631828016
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.8022456941759785
Weighted FMeasure: 0.9088007208012449
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9447683220880645
Mean absolute error: 0.035699513321194386
Coverage of cases: 93.3184855233853
Instances selection time: 7.0
Test time: 26.0
Accumulative iteration time: 170.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.01595255347215402
Kappa statistic: 0.7902325076053891
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.968077208611717
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.9287553387494778
Weighted AreaUnderROC: 0.9685626135099689
Root mean squared error: 0.16999482689957363
Relative absolute error: 13.247963454727365
Root relative squared error: 45.61439865249099
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7880572971645255
Weighted FMeasure: 0.9015648315812682
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9452606706479466
Mean absolute error: 0.03679989848535343
Coverage of cases: 93.54120267260579
Instances selection time: 6.0
Test time: 26.0
Accumulative iteration time: 176.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.016367876827343742
Kappa statistic: 0.7738835076290074
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.41128433556048
Incorrectly Classified Instances: 14.031180400890868
Correctly Classified Instances: 85.96881959910912
Weighted Precision: 0.9241400929053251
Weighted AreaUnderROC: 0.944290824722167
Root mean squared error: 0.21201326877260918
Relative absolute error: 18.355041570397383
Root relative squared error: 56.88912973289878
Weighted TruePositiveRate: 0.8596881959910914
Weighted MatthewsCorrelation: 0.7385883531300219
Weighted FMeasure: 0.8755618352891321
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9332397953690179
Mean absolute error: 0.05098622658443667
Coverage of cases: 87.30512249443207
Instances selection time: 4.0
Test time: 26.0
Accumulative iteration time: 181.0
Weighted Recall: 0.8596881959910914
Weighted FalsePositiveRate: 0.017503380248628433
Kappa statistic: 0.7130262757431268
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.559762435040827
Incorrectly Classified Instances: 13.808463251670378
Correctly Classified Instances: 86.19153674832963
Weighted Precision: 0.9244189737240865
Weighted AreaUnderROC: 0.9488723437780777
Root mean squared error: 0.20755402647786583
Relative absolute error: 17.778708055545337
Root relative squared error: 55.69258946499618
Weighted TruePositiveRate: 0.8619153674832962
Weighted MatthewsCorrelation: 0.7415157836986438
Weighted FMeasure: 0.877189752142221
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9336765770089689
Mean absolute error: 0.04938530015429211
Coverage of cases: 89.08685968819599
Instances selection time: 4.0
Test time: 25.0
Accumulative iteration time: 186.0
Weighted Recall: 0.8619153674832962
Weighted FalsePositiveRate: 0.017399549409831003
Kappa statistic: 0.7166558097874766
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 18.70824053452116
Incorrectly Classified Instances: 13.808463251670378
Correctly Classified Instances: 86.19153674832963
Weighted Precision: 0.9236673517297127
Weighted AreaUnderROC: 0.9515934456244618
Root mean squared error: 0.20419868553271392
Relative absolute error: 17.42281554352215
Root relative squared error: 54.792257012070415
Weighted TruePositiveRate: 0.8619153674832962
Weighted MatthewsCorrelation: 0.7410476241211894
Weighted FMeasure: 0.876764094104269
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9319144454996392
Mean absolute error: 0.04839670984311661
Coverage of cases: 89.75501113585746
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 189.0
Weighted Recall: 0.8619153674832962
Weighted FalsePositiveRate: 0.01757481274298907
Kappa statistic: 0.7165692642896414
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 18.745360059391242
Incorrectly Classified Instances: 14.031180400890868
Correctly Classified Instances: 85.96881959910912
Weighted Precision: 0.9233788469739871
Weighted AreaUnderROC: 0.9472659578543656
Root mean squared error: 0.20912174653926358
Relative absolute error: 18.144043005928147
Root relative squared error: 56.11325290023324
Weighted TruePositiveRate: 0.8596881959910914
Weighted MatthewsCorrelation: 0.738113846577711
Weighted FMeasure: 0.8751283704211953
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9280028316965996
Mean absolute error: 0.050400119460911025
Coverage of cases: 88.8641425389755
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 192.0
Weighted Recall: 0.8596881959910914
Weighted FalsePositiveRate: 0.017678643581786502
Kappa statistic: 0.7129389080576417
Training time: 1.0
		
Time end:Sat Oct 07 11.22.33 EEST 2017