Sat Oct 07 11.22.41 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.22.41 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.8740067111366153
Weighted AreaUnderROC: 0.8533427136774849
Root mean squared error: 0.3164780026110676
Relative absolute error: 79.52911231307695
Root relative squared error: 84.91995926660479
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.746018300027618
Weighted FMeasure: 0.8917349838175487
Iteration time: 75.0
Weighted AreaUnderPRC: 0.8582620812101907
Mean absolute error: 0.220914200869656
Coverage of cases: 95.5456570155902
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.22265098863259195
Kappa statistic: 0.7598107125711542
Training time: 72.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.8875942701905158
Weighted AreaUnderROC: 0.8695528664538525
Root mean squared error: 0.3154470488024175
Relative absolute error: 79.30003181673584
Root relative squared error: 84.64332533086831
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.7858935617376053
Weighted FMeasure: 0.9072864130120735
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8754165609143548
Mean absolute error: 0.22027786615759737
Coverage of cases: 95.5456570155902
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 135.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.19359828916837604
Kappa statistic: 0.8046817650417336
Training time: 56.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8867762576448545
Weighted AreaUnderROC: 0.8868329729958825
Root mean squared error: 0.31563910912619086
Relative absolute error: 79.33821189945937
Root relative squared error: 84.69486052363686
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7781660640306646
Weighted FMeasure: 0.9039485319178138
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8816628646468434
Mean absolute error: 0.22038392194294046
Coverage of cases: 95.5456570155902
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 195.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.15311930514210928
Kappa statistic: 0.8033998712169994
Training time: 56.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8867762576448545
Weighted AreaUnderROC: 0.8868379778756402
Root mean squared error: 0.3156511090162907
Relative absolute error: 79.33821189945937
Root relative squared error: 84.69808043203514
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7781660640306646
Weighted FMeasure: 0.9039485319178138
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8816628646468434
Mean absolute error: 0.22038392194294046
Coverage of cases: 95.5456570155902
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 262.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.15311930514210928
Kappa statistic: 0.8033998712169994
Training time: 64.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8867762576448545
Weighted AreaUnderROC: 0.8868379778756402
Root mean squared error: 0.3156511090162907
Relative absolute error: 79.33821189945937
Root relative squared error: 84.69808043203514
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7781660640306646
Weighted FMeasure: 0.9039485319178138
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8816628646468434
Mean absolute error: 0.22038392194294046
Coverage of cases: 95.5456570155902
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 328.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.15311930514210928
Kappa statistic: 0.8033998712169994
Training time: 63.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8874468733447776
Weighted AreaUnderROC: 0.8902577422398217
Root mean squared error: 0.3156511090162907
Relative absolute error: 79.33821189945937
Root relative squared error: 84.69808043203514
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7824139437581616
Weighted FMeasure: 0.9041322263044829
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8822554974366382
Mean absolute error: 0.22038392194294046
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 392.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.14627977641374632
Kappa statistic: 0.8041364091247338
Training time: 62.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8874468733447776
Weighted AreaUnderROC: 0.8902627471195795
Root mean squared error: 0.3156511090162907
Relative absolute error: 79.33821189945937
Root relative squared error: 84.69808043203514
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7824139437581616
Weighted FMeasure: 0.9041322263044829
Iteration time: 63.0
Weighted AreaUnderPRC: 0.8822557146898686
Mean absolute error: 0.22038392194294046
Coverage of cases: 95.5456570155902
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 455.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.14627977641374632
Kappa statistic: 0.8041364091247338
Training time: 60.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8932845117209618
Weighted AreaUnderROC: 0.8914600350337011
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.800255393410651
Weighted FMeasure: 0.9121575762834168
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8896005789228427
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 523.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15200292845428728
Kappa statistic: 0.8238801286577233
Training time: 66.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8932845117209618
Weighted AreaUnderROC: 0.8914575325938222
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.800255393410651
Weighted FMeasure: 0.9121575762834168
Iteration time: 77.0
Weighted AreaUnderPRC: 0.8896003616696122
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 600.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15200292845428728
Kappa statistic: 0.8238801286577233
Training time: 75.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8937414167670892
Weighted AreaUnderROC: 0.8950671315794699
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8043198308900155
Weighted FMeasure: 0.912277535158117
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8900733054612466
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 672.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.14516339972592432
Kappa statistic: 0.824549977857087
Training time: 70.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8937414167670892
Weighted AreaUnderROC: 0.8950671315794699
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8043198308900155
Weighted FMeasure: 0.912277535158117
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8900733054612466
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 740.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.14516339972592432
Kappa statistic: 0.824549977857087
Training time: 66.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8937414167670892
Weighted AreaUnderROC: 0.8950646291395912
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8043198308900155
Weighted FMeasure: 0.912277535158117
Iteration time: 71.0
Weighted AreaUnderPRC: 0.8900730882080162
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 811.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.14516339972592432
Kappa statistic: 0.824549977857087
Training time: 69.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8937414167670892
Weighted AreaUnderROC: 0.8950671315794699
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8043198308900155
Weighted FMeasure: 0.912277535158117
Iteration time: 75.0
Weighted AreaUnderPRC: 0.8900733054612466
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 886.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.14516339972592432
Kappa statistic: 0.824549977857087
Training time: 73.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8937414167670892
Weighted AreaUnderROC: 0.8950621266997122
Root mean squared error: 0.31531493946929956
Relative absolute error: 79.26185173401234
Root relative squared error: 84.60787667695065
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8043198308900155
Weighted FMeasure: 0.912277535158117
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8900728730847194
Mean absolute error: 0.2201718103722543
Coverage of cases: 95.5456570155902
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1041.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.14516339972592432
Kappa statistic: 0.824549977857087
Training time: 154.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8970968765802696
Weighted AreaUnderROC: 0.874125287345005
Root mean squared error: 0.3151467202227955
Relative absolute error: 79.22367165128881
Root relative squared error: 84.56273871651382
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8120912003749845
Weighted FMeasure: 0.9159399486828367
Iteration time: 152.0
Weighted AreaUnderPRC: 0.885172228307213
Mean absolute error: 0.22006575458691116
Coverage of cases: 95.5456570155902
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1193.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19248191248055402
Kappa statistic: 0.8263391993811642
Training time: 150.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8921581346137563
Weighted AreaUnderROC: 0.8716606510545307
Root mean squared error: 0.3153149394692995
Relative absolute error: 79.26185173401232
Root relative squared error: 84.60787667695062
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.7987513746836673
Weighted FMeasure: 0.9115765495481306
Iteration time: 156.0
Weighted AreaUnderPRC: 0.880072659263963
Mean absolute error: 0.22017181037225428
Coverage of cases: 95.5456570155902
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1349.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.19304010082446502
Kappa statistic: 0.8154238263586285
Training time: 155.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8965055790407765
Weighted AreaUnderROC: 0.8781784718023855
Root mean squared error: 0.3150625769184804
Relative absolute error: 79.20458160992705
Root relative squared error: 84.54016069871781
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8114546568327804
Weighted FMeasure: 0.9160532507895763
Iteration time: 163.0
Weighted AreaUnderPRC: 0.887056694917097
Mean absolute error: 0.2200127266942396
Coverage of cases: 95.5456570155902
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1512.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1856423837521911
Kappa statistic: 0.8278374233128835
Training time: 162.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8965055790407765
Weighted AreaUnderROC: 0.8781784718023855
Root mean squared error: 0.3150625769184804
Relative absolute error: 79.20458160992705
Root relative squared error: 84.54016069871781
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8114546568327804
Weighted FMeasure: 0.9160532507895763
Iteration time: 165.0
Weighted AreaUnderPRC: 0.887056694917097
Mean absolute error: 0.2200127266942396
Coverage of cases: 95.5456570155902
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1677.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1856423837521911
Kappa statistic: 0.8278374233128835
Training time: 164.0
		
Time end:Sat Oct 07 11.22.43 EEST 2017