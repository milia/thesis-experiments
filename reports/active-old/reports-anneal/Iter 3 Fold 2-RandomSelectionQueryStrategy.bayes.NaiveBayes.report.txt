Sat Oct 07 11.23.55 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.55 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.74981440237563
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.9054404272667079
Weighted AreaUnderROC: 0.8796276437555817
Root mean squared error: 0.16185755294187196
Relative absolute error: 13.866050693767686
Root relative squared error: 43.43093892597582
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7493712654058534
Weighted FMeasure: 0.8903012400339847
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8696263028675316
Mean absolute error: 0.03851680748268763
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 2.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.23686821491419782
Kappa statistic: 0.7496235989516534
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.527097253155112
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.932548192767629
Weighted AreaUnderROC: 0.9669456234960455
Root mean squared error: 0.14392532400161434
Relative absolute error: 12.029018413994107
Root relative squared error: 38.619216978155194
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8153161816383875
Weighted FMeasure: 0.9212744938209999
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9553503360864071
Mean absolute error: 0.033413940038872186
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.1457215880698353
Kappa statistic: 0.8131405087655414
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 20.60133630289535
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9587525646642562
Weighted AreaUnderROC: 0.982892310177685
Root mean squared error: 0.11253697756682104
Relative absolute error: 9.29854599217486
Root relative squared error: 30.196839818613835
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8944350623421282
Weighted FMeasure: 0.9546326683409371
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9684939958975148
Mean absolute error: 0.02582929442270769
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 5.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.046268529142971665
Kappa statistic: 0.8904664324746292
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 20.081662954714197
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9648913335617415
Weighted AreaUnderROC: 0.9833622590896546
Root mean squared error: 0.10736301765541113
Relative absolute error: 8.281410345265146
Root relative squared error: 28.80852068964127
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9081640411809786
Weighted FMeasure: 0.9625879503708413
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9654643262113176
Mean absolute error: 0.023003917625736284
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.058831209911875584
Kappa statistic: 0.910459666965799
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.378619153674844
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.952337565923312
Weighted AreaUnderROC: 0.9850998552656969
Root mean squared error: 0.11212068819048823
Relative absolute error: 8.8678258103195
Root relative squared error: 30.0851376573589
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8710190891962656
Weighted FMeasure: 0.94795662582879
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9716856076998163
Mean absolute error: 0.024632849473109477
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.06025907911608989
Kappa statistic: 0.8746205958769395
Training time: 1.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.749814402375694
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9762903368067145
Weighted AreaUnderROC: 0.9865837090455245
Root mean squared error: 0.10078931297688805
Relative absolute error: 8.000251695838063
Root relative squared error: 27.044610626618937
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9365527486711577
Weighted FMeasure: 0.9738499524777418
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9787069371173504
Mean absolute error: 0.02222292137732773
Coverage of cases: 98.21826280623608
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.009487605459196696
Kappa statistic: 0.9353212330740421
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 20.41573867854495
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9543061363966057
Weighted AreaUnderROC: 0.9815897269526537
Root mean squared error: 0.1342984212255656
Relative absolute error: 10.441901996005308
Root relative squared error: 36.03604789575201
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8650895020892778
Weighted FMeasure: 0.939691284588374
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9640736070328302
Mean absolute error: 0.029005283322236678
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.013777848877326646
Kappa statistic: 0.8560403068819421
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 20.56421677802529
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9569433990541669
Weighted AreaUnderROC: 0.9841257979263194
Root mean squared error: 0.12951680111775904
Relative absolute error: 10.251105806728793
Root relative squared error: 34.753004583315764
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8739768030581878
Weighted FMeasure: 0.9437248981304516
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9676966227475351
Mean absolute error: 0.028475293907579698
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.013394923866573713
Kappa statistic: 0.8653766317634578
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 20.489977728285133
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9558467958856504
Weighted AreaUnderROC: 0.9843065810620995
Root mean squared error: 0.1321495199877872
Relative absolute error: 10.427871323347276
Root relative squared error: 35.4594371863993
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8695760896267426
Weighted FMeasure: 0.9417800303368434
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9685421450757457
Mean absolute error: 0.028966309231519924
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.013674018038529214
Kappa statistic: 0.8606679845531444
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 20.861172976985966
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9518211826039052
Weighted AreaUnderROC: 0.9855752940457725
Root mean squared error: 0.13180988441733946
Relative absolute error: 11.040496019397017
Root relative squared error: 35.36830339962753
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8564114918123834
Weighted FMeasure: 0.935695224471027
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9699907677102898
Mean absolute error: 0.03066804449832474
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 15.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.014160773888079582
Kappa statistic: 0.8468343491312653
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 20.564216778025305
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9497719800615123
Weighted AreaUnderROC: 0.9853165054000773
Root mean squared error: 0.13534756051899946
Relative absolute error: 10.991206905166043
Root relative squared error: 36.31756150911012
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8479973519446207
Weighted FMeasure: 0.9318329587481738
Iteration time: 2.0
Weighted AreaUnderPRC: 0.96976950892156
Mean absolute error: 0.030531130292127596
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.014718962231990583
Kappa statistic: 0.8377010663292971
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 20.304380103934736
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.953803051261004
Weighted AreaUnderROC: 0.9867058992071349
Root mean squared error: 0.13125656057621993
Relative absolute error: 10.607465370350948
Root relative squared error: 35.21983103294978
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8650110445252868
Weighted FMeasure: 0.9395399972097486
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9752492705259175
Mean absolute error: 0.029465181584307897
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.013427322211010506
Kappa statistic: 0.8561391463554182
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 20.193021529324483
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9443700007102551
Weighted AreaUnderROC: 0.9841268133290436
Root mean squared error: 0.15059113696946597
Relative absolute error: 12.051764125401263
Root relative squared error: 40.40784228872511
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.8275527223384586
Weighted FMeasure: 0.9220453383913956
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9690098826643649
Mean absolute error: 0.03347712257055873
Coverage of cases: 96.88195991091314
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.015763906425451946
Kappa statistic: 0.8155095060412225
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 20.452858203415065
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9434584578570656
Weighted AreaUnderROC: 0.9853494458334743
Root mean squared error: 0.15150243860007206
Relative absolute error: 12.656630270390862
Root relative squared error: 40.6523701760101
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.823608797370756
Weighted FMeasure: 0.9201398795417471
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9713100519957831
Mean absolute error: 0.035157306306640934
Coverage of cases: 96.88195991091314
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.016043000597407448
Kappa statistic: 0.8111357095417311
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 20.304380103934733
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9503499428101871
Weighted AreaUnderROC: 0.9858695370413492
Root mean squared error: 0.14030067899245208
Relative absolute error: 11.338691860818242
Root relative squared error: 37.64662266198013
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.848489319848715
Weighted FMeasure: 0.9352831778384564
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9800346828442734
Mean absolute error: 0.03149636628005036
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.01408934139371894
Kappa statistic: 0.8435575237153262
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 20.378619153674897
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9514391307465158
Weighted AreaUnderROC: 0.9861989239794428
Root mean squared error: 0.13855133297203961
Relative absolute error: 11.179689954323889
Root relative squared error: 37.177223867842805
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8504449250567017
Weighted FMeasure: 0.9373102069334193
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9799569601295485
Mean absolute error: 0.03105469431756605
Coverage of cases: 97.55011135857461
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 23.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.013810247221763439
Kappa statistic: 0.8486822889752633
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 20.193021529324493
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9503421927374339
Weighted AreaUnderROC: 0.9863138193581207
Root mean squared error: 0.13772817109543295
Relative absolute error: 10.958268932135734
Root relative squared error: 36.956346358333356
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.846284765639699
Weighted FMeasure: 0.9353397767958793
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9800451290451806
Mean absolute error: 0.03043963592259896
Coverage of cases: 97.10467706013362
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.01408934139371894
Kappa statistic: 0.8441495913111634
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 20.193021529324497
Incorrectly Classified Instances: 14.25389755011136
Correctly Classified Instances: 85.74610244988864
Weighted Precision: 0.9328251084744359
Weighted AreaUnderROC: 0.9684746144444449
Root mean squared error: 0.20711294014485637
Relative absolute error: 19.65200168682126
Root relative squared error: 55.57423358204955
Weighted TruePositiveRate: 0.8574610244988864
Weighted MatthewsCorrelation: 0.7372615624664972
Weighted FMeasure: 0.8782358759978331
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9685809163646711
Mean absolute error: 0.05458889357450296
Coverage of cases: 89.53229398663697
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8574610244988864
Weighted FalsePositiveRate: 0.01751575907403417
Kappa statistic: 0.7114801502038194
Training time: 1.0
		
Time end:Sat Oct 07 11.23.57 EEST 2017