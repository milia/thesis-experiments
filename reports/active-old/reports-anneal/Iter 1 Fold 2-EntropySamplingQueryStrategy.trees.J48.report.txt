Sat Oct 07 11.20.27 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.27 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.00074239049739
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9454628304528667
Weighted AreaUnderROC: 0.9432395934540005
Root mean squared error: 0.1337456505270072
Relative absolute error: 6.948775055679355
Root relative squared error: 35.88772395279842
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8646900536446315
Weighted FMeasure: 0.9432540988753229
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9257949299052084
Mean absolute error: 0.01930215293244246
Coverage of cases: 94.87750556792874
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.060740023593434854
Kappa statistic: 0.8584437788744862
Training time: 7.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 19.71046770601341
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8930689878772154
Weighted AreaUnderROC: 0.9629334784541576
Root mean squared error: 0.1363955877172492
Relative absolute error: 10.122667064502828
Root relative squared error: 36.59877671601673
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.7995848994944972
Weighted FMeasure: 0.91151316939415
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9384611556550496
Mean absolute error: 0.028118519623618686
Coverage of cases: 96.43652561247217
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15879233186936703
Kappa statistic: 0.8225109365941073
Training time: 3.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 18.59688195991096
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9756691976379451
Weighted AreaUnderROC: 0.9794069688156233
Root mean squared error: 0.08355439040357066
Relative absolute error: 3.4705512515851638
Root relative squared error: 22.41999561131292
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9308716270504727
Weighted FMeasure: 0.9730080796492764
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9766389031999255
Mean absolute error: 0.009640420143292026
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.030657119663634658
Kappa statistic: 0.9340417197140339
Training time: 8.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.740905716406818
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.8898259135990925
Weighted AreaUnderROC: 0.9551308963454802
Root mean squared error: 0.14622784549001572
Relative absolute error: 9.350297364105922
Root relative squared error: 39.23704832628154
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.7970741227180335
Weighted FMeasure: 0.9095469351549401
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9215904120232177
Mean absolute error: 0.025973048233627304
Coverage of cases: 93.3184855233853
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.15881235138839808
Kappa statistic: 0.8167033198572501
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9754019813234818
Weighted AreaUnderROC: 0.9790660534792451
Root mean squared error: 0.09225683805005278
Relative absolute error: 3.7751780513473534
Root relative squared error: 24.755107352292978
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9347986683119497
Weighted FMeasure: 0.9737346719035092
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9650581194655454
Mean absolute error: 0.010486605698186989
Coverage of cases: 97.55011135857461
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.02355851628234725
Kappa statistic: 0.934349526629382
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.740905716406818
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9759436365489033
Weighted AreaUnderROC: 0.9772596213551202
Root mean squared error: 0.0872546228892241
Relative absolute error: 3.0846325167038167
Root relative squared error: 23.412872175769817
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.9392209381075783
Weighted FMeasure: 0.975252389449421
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9690305815004733
Mean absolute error: 0.008568423657510517
Coverage of cases: 97.7728285077951
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.030131482167075514
Kappa statistic: 0.9391351497898874
Training time: 9.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.076790806233319
Relative absolute error: 2.726694241170883
Root relative squared error: 20.605135534166006
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007574150669919044
Coverage of cases: 98.88641425389756
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 72.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 8.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.076790806233319
Relative absolute error: 2.726694241170883
Root relative squared error: 20.605135534166006
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007574150669919044
Coverage of cases: 98.88641425389756
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 86.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 10.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 98.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 6.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 122.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 20.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 146.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 18.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 162.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 13.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 191.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 22.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 214.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 23.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 246.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 25.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 275.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 24.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 303.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 25.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 17.03786191536748
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9791336660195562
Weighted AreaUnderROC: 0.9833505930548639
Root mean squared error: 0.07490817607484966
Relative absolute error: 2.6503340757238565
Root relative squared error: 20.099972852866564
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9417150297340717
Weighted FMeasure: 0.9772214905438709
Iteration time: 53.0
Weighted AreaUnderPRC: 0.9814618800721845
Mean absolute error: 0.007362039099232861
Coverage of cases: 98.88641425389756
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 356.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.030111462648044457
Kappa statistic: 0.9446376168281916
Training time: 46.0
		
Time end:Sat Oct 07 11.20.29 EEST 2017