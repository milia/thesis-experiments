Sat Oct 07 11.20.58 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.58 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.33333333333331
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9128254426976044
Weighted AreaUnderROC: 0.8259901007669569
Root mean squared error: 0.17049817112666088
Relative absolute error: 16.581408979017844
Root relative squared error: 45.7494600814329
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7463556081518048
Weighted FMeasure: 0.9027386620131588
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8432438215906513
Mean absolute error: 0.046059469386160214
Coverage of cases: 92.87305122494432
Instances selection time: 24.0
Test time: 16.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2567057672856855
Kappa statistic: 0.7418925171403333
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.703786191536732
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9481135796775153
Weighted AreaUnderROC: 0.8882012553731188
Root mean squared error: 0.13328261215796497
Relative absolute error: 7.965059988642718
Root relative squared error: 35.76347772047413
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8492437290504051
Weighted FMeasure: 0.9389549256928891
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9003804142344811
Mean absolute error: 0.022125166635118442
Coverage of cases: 94.65478841870824
Instances selection time: 13.0
Test time: 45.0
Accumulative iteration time: 39.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.17796557250796163
Kappa statistic: 0.8459056901640469
Training time: 1.0
		
Time end:Sat Oct 07 11.20.59 EEST 2017