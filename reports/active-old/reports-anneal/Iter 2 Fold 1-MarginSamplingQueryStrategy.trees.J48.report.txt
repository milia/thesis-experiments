Sat Oct 07 11.23.31 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.31 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.380846325167134
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9602035961332454
Weighted AreaUnderROC: 0.9578348813920718
Root mean squared error: 0.1043702776242112
Relative absolute error: 6.555744792349071
Root relative squared error: 28.005484271791513
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9128015285680516
Weighted FMeasure: 0.964448974769066
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9517131349164334
Mean absolute error: 0.01821040220096946
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.06526275097284401
Kappa statistic: 0.9207144028354124
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9602035961332454
Weighted AreaUnderROC: 0.957637141867063
Root mean squared error: 0.1016092836894919
Relative absolute error: 5.256124721603613
Root relative squared error: 27.264631856971874
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9128015285680516
Weighted FMeasure: 0.964448974769066
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9515115324522786
Mean absolute error: 0.01460034644889878
Coverage of cases: 96.88195991091314
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.06526275097284401
Kappa statistic: 0.9207144028354124
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 18.337045285820384
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9560969689787202
Weighted AreaUnderROC: 0.969745597143668
Root mean squared error: 0.10784416350920997
Relative absolute error: 6.503340757238368
Root relative squared error: 28.937625669983635
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9043865699690159
Weighted FMeasure: 0.9600571539790769
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9534321757686283
Mean absolute error: 0.018064835436773063
Coverage of cases: 97.32739420935413
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07265420238095759
Kappa statistic: 0.9090172239108408
Training time: 3.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 31.143281365998416
Incorrectly Classified Instances: 11.135857461024498
Correctly Classified Instances: 88.8641425389755
Weighted Precision: 0.8930470771755498
Weighted AreaUnderROC: 0.7744321036955
Root mean squared error: 0.18326028801130997
Relative absolute error: 20.23452271559192
Root relative squared error: 49.173895388337705
Weighted TruePositiveRate: 0.888641425389755
Weighted MatthewsCorrelation: 0.6741297438654067
Weighted FMeasure: 0.8548440215110085
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8039446601406672
Mean absolute error: 0.056207007543310324
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.888641425389755
Weighted FalsePositiveRate: 0.34891635295440215
Kappa statistic: 0.6616988894079353
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 31.365998515218916
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.8829562771070214
Weighted AreaUnderROC: 0.7490288328779482
Root mean squared error: 0.19314567306735814
Relative absolute error: 21.7623959676477
Root relative squared error: 51.82642254462783
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.62096450769804
Weighted FMeasure: 0.8270003411446376
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7828966232528652
Mean absolute error: 0.060451099910131895
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.39162809035631296
Kappa statistic: 0.610792068479792
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 18.15144766146985
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.962437134037767
Weighted AreaUnderROC: 0.9740127120199247
Root mean squared error: 0.09723922533955637
Relative absolute error: 5.456912797670085
Root relative squared error: 26.0920221526403
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9221291653460968
Weighted FMeasure: 0.966536361686434
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9618927636078305
Mean absolute error: 0.015158091104638973
Coverage of cases: 97.32739420935413
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.07183571685757228
Kappa statistic: 0.9252442975884017
Training time: 5.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9668078676679338
Weighted AreaUnderROC: 0.9763466052644363
Root mean squared error: 0.0874302826666799
Relative absolute error: 4.318669359359761
Root relative squared error: 23.460006640166256
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.9412124786552714
Weighted FMeasure: 0.9710916432312815
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9687410145296367
Mean absolute error: 0.011996303775999218
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.04390688227188862
Kappa statistic: 0.9377041736563955
Training time: 3.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.778025241276893
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9821251211390043
Weighted AreaUnderROC: 0.980929209986283
Root mean squared error: 0.07475189517827834
Relative absolute error: 2.9464321869667383
Root relative squared error: 20.058038287868392
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9525219878768885
Weighted FMeasure: 0.9818968798160281
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9778130811166407
Mean absolute error: 0.008184533852685303
Coverage of cases: 98.44097995545657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.043088396748503316
Kappa statistic: 0.9547709587247223
Training time: 4.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9799159290467323
Weighted AreaUnderROC: 0.9806143530591902
Root mean squared error: 0.0797545573809975
Relative absolute error: 3.3446110964226676
Root relative squared error: 21.400393418318266
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9421784305020384
Weighted FMeasure: 0.9784604616925634
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9743637105155439
Mean absolute error: 0.009290586378951762
Coverage of cases: 98.44097995545657
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 35.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.04287562679780138
Kappa statistic: 0.9439723480452712
Training time: 4.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9883141480345249
Root mean squared error: 0.06466550941431823
Relative absolute error: 2.18899140948141
Root relative squared error: 17.351576982008265
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9827544540354234
Mean absolute error: 0.006080531693003856
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 7.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9844745429118972
Weighted AreaUnderROC: 0.9795718582627408
Root mean squared error: 0.07017135174139738
Relative absolute error: 2.4562519885460006
Root relative squared error: 18.828949508017626
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9540532952552635
Weighted FMeasure: 0.9826045425063651
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9763995114062198
Mean absolute error: 0.0068229221904054895
Coverage of cases: 98.88641425389756
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.03565317305868549
Kappa statistic: 0.9551291660420727
Training time: 9.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 17.594654788418662
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06577219637233789
Relative absolute error: 2.9462297168310796
Root relative squared error: 17.648532254161598
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.008183971435641807
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 5.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06436543582573825
Relative absolute error: 2.405345211581315
Root relative squared error: 17.27105878893021
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.006681514476614699
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 72.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 5.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06436543582573825
Relative absolute error: 2.405345211581315
Root relative squared error: 17.27105878893021
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.006681514476614699
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 5.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06436543582573825
Relative absolute error: 2.405345211581315
Root relative squared error: 17.27105878893021
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.006681514476614699
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 84.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 6.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06436543582573825
Relative absolute error: 2.405345211581315
Root relative squared error: 17.27105878893021
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.006681514476614699
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 5.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9865001576104536
Weighted AreaUnderROC: 0.9891779575385109
Root mean squared error: 0.06436543582573825
Relative absolute error: 2.405345211581315
Root relative squared error: 17.27105878893021
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9603507937472676
Weighted FMeasure: 0.9852035946783668
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9831391613387067
Mean absolute error: 0.006681514476614699
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.021519758096845993
Kappa statistic: 0.9612061517193712
Training time: 6.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 18.337045285820384
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9842925800023443
Weighted AreaUnderROC: 0.9882341885854911
Root mean squared error: 0.06968021294453397
Relative absolute error: 2.93755242530301
Root relative squared error: 18.69716313967674
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9543479537399645
Weighted FMeasure: 0.9829712155344571
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9814138476910302
Mean absolute error: 0.008159867848063836
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 108.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.028638380997164457
Kappa statistic: 0.9554949820344443
Training time: 10.0
		
Time end:Sat Oct 07 11.23.32 EEST 2017