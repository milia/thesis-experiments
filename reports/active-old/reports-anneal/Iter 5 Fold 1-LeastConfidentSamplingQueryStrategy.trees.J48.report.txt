Sat Oct 07 11.22.14 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.22.14 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 18.856718634001517
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9082267722699605
Weighted AreaUnderROC: 0.9272982805897487
Root mean squared error: 0.16072606925845115
Relative absolute error: 11.024498886414378
Root relative squared error: 43.12732999418853
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7547513780354652
Weighted FMeasure: 0.9030411707146266
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9045511739722492
Mean absolute error: 0.03062360801781741
Coverage of cases: 95.10022271714922
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.23638252908828225
Kappa statistic: 0.7577639751552795
Training time: 2.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 17.74313288789905
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.8643418710969243
Weighted AreaUnderROC: 0.9024027332473672
Root mean squared error: 0.16655323780376485
Relative absolute error: 12.437896179544415
Root relative squared error: 44.69092339222894
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7372373265397965
Weighted FMeasure: 0.8872826321231222
Iteration time: 2.0
Weighted AreaUnderPRC: 0.882773585522333
Mean absolute error: 0.034549711609845254
Coverage of cases: 92.20489977728285
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.24339732114980328
Kappa statistic: 0.7547719411262201
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9376154924151364
Weighted AreaUnderROC: 0.9374414820566919
Root mean squared error: 0.13197151612769442
Relative absolute error: 6.828507795100289
Root relative squared error: 35.411673738628316
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8508746828373759
Weighted FMeasure: 0.9415031317962315
Iteration time: 3.0
Weighted AreaUnderPRC: 0.925095954389176
Mean absolute error: 0.018968077208611725
Coverage of cases: 94.65478841870824
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.12960318558350528
Kappa statistic: 0.8598863592037341
Training time: 2.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 18.07720861172975
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9275516849687621
Weighted AreaUnderROC: 0.9220375250729929
Root mean squared error: 0.1429937786105889
Relative absolute error: 9.415795785506342
Root relative squared error: 38.36925711994005
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8038160287286418
Weighted FMeasure: 0.9237882370145833
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9076187102385853
Mean absolute error: 0.02615498829307291
Coverage of cases: 95.99109131403118
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.10942812011791883
Kappa statistic: 0.8138360791678354
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 17.557535263548584
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9467987421369078
Weighted AreaUnderROC: 0.9277182364132175
Root mean squared error: 0.12131202600496087
Relative absolute error: 5.659299868654038
Root relative squared error: 32.55143239623793
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8757983821641481
Weighted FMeasure: 0.9507831702443859
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9264673648868008
Mean absolute error: 0.015720277412927728
Coverage of cases: 95.99109131403118
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1079744883747548
Kappa statistic: 0.8841215562294342
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9487646936509558
Weighted AreaUnderROC: 0.9328142602350418
Root mean squared error: 0.11847881495654926
Relative absolute error: 5.5615653833917165
Root relative squared error: 31.79120209637571
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8820980090234075
Weighted FMeasure: 0.9530796788390443
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9304654008232571
Mean absolute error: 0.015448792731643504
Coverage of cases: 95.7683741648107
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.09401007108191298
Kappa statistic: 0.8911917759297994
Training time: 3.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.740905716406818
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9509657944609756
Weighted AreaUnderROC: 0.9339316186431347
Root mean squared error: 0.11557231768464606
Relative absolute error: 5.403638677580823
Root relative squared error: 31.011307039208514
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8879826483920981
Weighted FMeasure: 0.9552158140173423
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9326457308720837
Mean absolute error: 0.01501010743772436
Coverage of cases: 95.99109131403118
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.09373724257411789
Kappa statistic: 0.8965318585090449
Training time: 3.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 18.299925760950178
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9487646936509558
Weighted AreaUnderROC: 0.9358429272624963
Root mean squared error: 0.11707808617042614
Relative absolute error: 7.076041998091059
Root relative squared error: 31.41534712231826
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8820980090234075
Weighted FMeasure: 0.9530796788390443
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9321657221911206
Mean absolute error: 0.019655672216919412
Coverage of cases: 96.88195991091314
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.09401007108191298
Kappa statistic: 0.8911917759297994
Training time: 3.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.958157979259406
Weighted AreaUnderROC: 0.9390645984737032
Root mean squared error: 0.10511421416909882
Relative absolute error: 4.831738417484565
Root relative squared error: 28.205103394029322
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.906249737549219
Weighted FMeasure: 0.9617357994037691
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9409661466591024
Mean absolute error: 0.013421495604123658
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.09291875705073259
Kappa statistic: 0.9127952144188939
Training time: 3.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.188567186339935
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9515112923609155
Weighted AreaUnderROC: 0.9469503670616221
Root mean squared error: 0.11457371943279003
Relative absolute error: 5.959839257334364
Root relative squared error: 30.74335501040501
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8879871903248795
Weighted FMeasure: 0.955110183432422
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9398976385520166
Mean absolute error: 0.016555109048150846
Coverage of cases: 95.99109131403118
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.10742883135916459
Kappa statistic: 0.8949147694028008
Training time: 6.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 18.188567186339935
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.948435109077611
Root mean squared error: 0.10522458544208009
Relative absolute error: 4.923784456078493
Root relative squared error: 28.234719114327238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.013677179044662344
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 7.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.188567186339935
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.948435109077611
Root mean squared error: 0.10522458544208009
Relative absolute error: 4.923784456078493
Root relative squared error: 28.234719114327238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.013677179044662344
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 7.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.9486551428876843
Root mean squared error: 0.1050889619954726
Relative absolute error: 4.460706331530426
Root relative squared error: 28.19832752481238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.01239085092091773
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 65.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 6.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.9486551428876843
Root mean squared error: 0.1050889619954726
Relative absolute error: 4.460706331530426
Root relative squared error: 28.19832752481238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.01239085092091773
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 73.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 7.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.188567186339935
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.948435109077611
Root mean squared error: 0.10522458544208009
Relative absolute error: 4.923784456078493
Root relative squared error: 28.234719114327238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.013677179044662344
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 82.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 8.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.9486551428876843
Root mean squared error: 0.1050889619954726
Relative absolute error: 4.460706331530426
Root relative squared error: 28.19832752481238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.01239085092091773
Coverage of cases: 96.65924276169265
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 8.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.967306611936932
Weighted AreaUnderROC: 0.9486551428876843
Root mean squared error: 0.1050889619954726
Relative absolute error: 4.460706331530426
Root relative squared error: 28.19832752481238
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9179636229766098
Weighted FMeasure: 0.9664182922342157
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9442674498175563
Mean absolute error: 0.01239085092091773
Coverage of cases: 96.65924276169265
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 102.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07187575589563439
Kappa statistic: 0.914311886919681
Training time: 7.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 18.299925760950178
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9604681723831224
Weighted AreaUnderROC: 0.9464424984168298
Root mean squared error: 0.11389495431745629
Relative absolute error: 6.072846491167451
Root relative squared error: 30.561223217768003
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.900258804451309
Weighted FMeasure: 0.9599541508369912
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9381159386850646
Mean absolute error: 0.01686901803102053
Coverage of cases: 97.10467706013362
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.07269424141901969
Kappa statistic: 0.8983114824228089
Training time: 10.0
		
Time end:Sat Oct 07 11.22.15 EEST 2017