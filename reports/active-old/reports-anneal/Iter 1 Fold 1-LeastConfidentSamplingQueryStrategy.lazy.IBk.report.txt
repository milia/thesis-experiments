Sat Oct 07 11.21.51 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.51 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.22197475872306
Incorrectly Classified Instances: 13.808463251670378
Correctly Classified Instances: 86.19153674832963
Weighted Precision: 0.8239849237622066
Weighted AreaUnderROC: 0.7260624588825378
Root mean squared error: 0.20908808315947497
Relative absolute error: 21.835326915138502
Root relative squared error: 56.104220067566075
Weighted TruePositiveRate: 0.8619153674832962
Weighted MatthewsCorrelation: 0.582355439277843
Weighted FMeasure: 0.8338035993875543
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7647191080861656
Mean absolute error: 0.060653685875384125
Coverage of cases: 90.20044543429844
Instances selection time: 28.0
Test time: 28.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8619153674832962
Weighted FalsePositiveRate: 0.3932188440682796
Kappa statistic: 0.5776297622479479
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.8697847067557536
Weighted AreaUnderROC: 0.8362537938483895
Root mean squared error: 0.17311124986597
Relative absolute error: 15.853293723803153
Root relative squared error: 46.450622684431465
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7252594139158922
Weighted FMeasure: 0.8851424172019647
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8460294349709648
Mean absolute error: 0.04403692701056387
Coverage of cases: 90.64587973273942
Instances selection time: 26.0
Test time: 35.0
Accumulative iteration time: 55.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.2166487424200955
Kappa statistic: 0.7474453923314897
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.864802858982803
Weighted AreaUnderROC: 0.834616782823423
Root mean squared error: 0.18148671215416312
Relative absolute error: 16.189129603383613
Root relative squared error: 48.69799504675776
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.704756688342516
Weighted FMeasure: 0.8773297731478645
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8408343694538534
Mean absolute error: 0.04496980445384292
Coverage of cases: 89.75501113585746
Instances selection time: 31.0
Test time: 38.0
Accumulative iteration time: 86.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.21118466503247896
Kappa statistic: 0.7295711947626843
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.8662377721821242
Weighted AreaUnderROC: 0.8362269213465975
Root mean squared error: 0.17985908658353048
Relative absolute error: 15.42538219245949
Root relative squared error: 48.26125727661558
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7100219006571927
Weighted FMeasure: 0.8793153176552728
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8420553610868509
Mean absolute error: 0.042848283867942594
Coverage of cases: 89.97772828507794
Instances selection time: 29.0
Test time: 42.0
Accumulative iteration time: 115.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.21090557086052347
Kappa statistic: 0.7344347620361973
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.8662377721821242
Weighted AreaUnderROC: 0.83584910096866
Root mean squared error: 0.18013782527045075
Relative absolute error: 15.040701744001199
Root relative squared error: 48.33605071484511
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7100219006571927
Weighted FMeasure: 0.8793153176552728
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8426088449534991
Mean absolute error: 0.04177972706666958
Coverage of cases: 89.97772828507794
Instances selection time: 23.0
Test time: 25.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.21090557086052347
Kappa statistic: 0.7344347620361973
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.8723738077908375
Weighted AreaUnderROC: 0.8407783735496087
Root mean squared error: 0.17220564022031912
Relative absolute error: 13.69542359088586
Root relative squared error: 46.20762211698088
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7318823513779403
Weighted FMeasure: 0.8873707745212038
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8501137809632449
Mean absolute error: 0.0380428433080159
Coverage of cases: 90.86859688195992
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 152.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.20978919417270148
Kappa statistic: 0.7542681705933391
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.874016941107744
Weighted AreaUnderROC: 0.8423885120727834
Root mean squared error: 0.1702785375360123
Relative absolute error: 13.177881519167583
Root relative squared error: 45.690526204772986
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7375597477583847
Weighted FMeasure: 0.8894150094478308
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8515457470176834
Mean absolute error: 0.03660522644213181
Coverage of cases: 91.0913140311804
Instances selection time: 15.0
Test time: 27.0
Accumulative iteration time: 167.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.209510100000746
Kappa statistic: 0.7593235329590073
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.8792452878090764
Weighted AreaUnderROC: 0.8464632868864325
Root mean squared error: 0.1639378692104547
Relative absolute error: 12.182060239841809
Root relative squared error: 43.98914635692579
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7551451332805684
Weighted FMeasure: 0.8956276922278265
Iteration time: 20.0
Weighted AreaUnderPRC: 0.857552781327556
Mean absolute error: 0.03383905622178247
Coverage of cases: 91.75946547884188
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 187.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.20867281748487948
Kappa statistic: 0.7747298195180822
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.8810914705069293
Weighted AreaUnderROC: 0.844298760210304
Root mean squared error: 0.16183549044543383
Relative absolute error: 11.741205168036151
Root relative squared error: 43.425018936961145
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7610865045878147
Weighted FMeasure: 0.8975457147551493
Iteration time: 14.0
Weighted AreaUnderPRC: 0.85793914386413
Mean absolute error: 0.032614458800100096
Coverage of cases: 91.98218262806236
Instances selection time: 14.0
Test time: 32.0
Accumulative iteration time: 201.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.21523325204128693
Kappa statistic: 0.778043254376931
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.8830693589861707
Weighted AreaUnderROC: 0.8455310783555412
Root mean squared error: 0.15968198891477117
Relative absolute error: 11.325601157652853
Root relative squared error: 42.84717383949575
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7673845125451245
Weighted FMeasure: 0.8996751871322487
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8602612492371611
Mean absolute error: 0.031460003215702054
Coverage of cases: 92.20489977728285
Instances selection time: 14.0
Test time: 32.0
Accumulative iteration time: 215.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.21495415786933145
Kappa statistic: 0.7833399969668979
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.8830693589861707
Weighted AreaUnderROC: 0.8459218085167791
Root mean squared error: 0.15977175365425075
Relative absolute error: 11.189695337212061
Root relative squared error: 42.871260246630825
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7673845125451245
Weighted FMeasure: 0.8996751871322487
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8601534276172632
Mean absolute error: 0.031082487047810973
Coverage of cases: 92.20489977728285
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 227.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.21495415786933145
Kappa statistic: 0.7833399969668979
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8851132773133354
Weighted AreaUnderROC: 0.847180132890799
Root mean squared error: 0.1575575083344134
Relative absolute error: 10.810266711544378
Root relative squared error: 42.27711588014877
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7737937271719202
Weighted FMeasure: 0.9018207202694267
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8632849152712696
Mean absolute error: 0.03002851864317853
Coverage of cases: 92.42761692650335
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 239.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.2146750636973759
Kappa statistic: 0.7886795586993536
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.8872270228774481
Weighted AreaUnderROC: 0.8480217208747983
Root mean squared error: 0.15529792761410122
Relative absolute error: 10.446875340011598
Root relative squared error: 41.67080674919685
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7803180523111962
Weighted FMeasure: 0.9039828879992233
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8660016914765447
Mean absolute error: 0.029019098166698596
Coverage of cases: 92.65033407572383
Instances selection time: 10.0
Test time: 37.0
Accumulative iteration time: 250.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.21439596952542042
Kappa statistic: 0.794062460909811
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.8894146845149072
Weighted AreaUnderROC: 0.8492540390200356
Root mean squared error: 0.152992650009147
Relative absolute error: 10.092451896931431
Root relative squared error: 41.052235857394585
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.7869615928924465
Weighted FMeasure: 0.9061622860558247
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8685365866931187
Mean absolute error: 0.028034588602587028
Coverage of cases: 92.87305122494432
Instances selection time: 9.0
Test time: 39.0
Accumulative iteration time: 259.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.21411687535346494
Kappa statistic: 0.7994892334314863
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.8916806710918178
Weighted AreaUnderROC: 0.8504863571652728
Root mean squared error: 0.15064166509464688
Relative absolute error: 9.747326071578275
Root relative squared error: 40.421400407446725
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.7937286691395159
Weighted FMeasure: 0.9083595331635639
Iteration time: 7.0
Weighted AreaUnderPRC: 0.871150796163195
Mean absolute error: 0.02707590575438383
Coverage of cases: 93.0957683741648
Instances selection time: 7.0
Test time: 41.0
Accumulative iteration time: 266.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.21383778118150942
Kappa statistic: 0.8049604147691446
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.8916806710918178
Weighted AreaUnderROC: 0.8504863571652728
Root mean squared error: 0.15069002765332137
Relative absolute error: 9.673286456571333
Root relative squared error: 40.43437744369816
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.7937286691395159
Weighted FMeasure: 0.9083595331635639
Iteration time: 6.0
Weighted AreaUnderPRC: 0.871150796163195
Mean absolute error: 0.02687024015714232
Coverage of cases: 93.0957683741648
Instances selection time: 6.0
Test time: 42.0
Accumulative iteration time: 272.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.21383778118150942
Kappa statistic: 0.8049604147691446
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9071823407595366
Weighted AreaUnderROC: 0.8918585046487577
Root mean squared error: 0.1298745783089442
Relative absolute error: 7.494415944485595
Root relative squared error: 34.84900627775044
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8426484376744512
Weighted FMeasure: 0.9273131949971573
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9041479727989499
Mean absolute error: 0.020817822068015333
Coverage of cases: 94.87750556792874
Instances selection time: 5.0
Test time: 43.0
Accumulative iteration time: 277.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.14979019459767431
Kappa statistic: 0.8614067343953404
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9679837572233383
Weighted AreaUnderROC: 0.9286175327280614
Root mean squared error: 0.1049690051414988
Relative absolute error: 5.323868824626049
Root relative squared error: 28.16613972323009
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9088808538649907
Weighted FMeasure: 0.9612474169431079
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9371157879047906
Mean absolute error: 0.01478852451284999
Coverage of cases: 96.65924276169265
Instances selection time: 2.0
Test time: 44.0
Accumulative iteration time: 279.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.0928412113951266
Kappa statistic: 0.912633449649107
Training time: 0.0
		
Time end:Sat Oct 07 11.21.53 EEST 2017