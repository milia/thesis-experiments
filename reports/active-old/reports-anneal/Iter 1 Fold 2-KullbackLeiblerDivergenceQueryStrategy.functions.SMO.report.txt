Sat Oct 07 11.20.53 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.53 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9239285862995947
Weighted AreaUnderROC: 0.9155568457901988
Root mean squared error: 0.3129791550521431
Relative absolute error: 80.60579064588107
Root relative squared error: 83.98111994844506
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8073284886912842
Weighted FMeasure: 0.9249068425987657
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9009349040951071
Mean absolute error: 0.22390497401633408
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 99.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.12394605275838778
Kappa statistic: 0.8146623971180547
Training time: 90.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9493646212607368
Weighted AreaUnderROC: 0.9261299156082559
Root mean squared error: 0.3122931551688596
Relative absolute error: 80.44543429844225
Root relative squared error: 83.7970468638556
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8507475714859383
Weighted FMeasure: 0.9347735118221256
Iteration time: 253.0
Weighted AreaUnderPRC: 0.9179103524847391
Mean absolute error: 0.22345953971789292
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 352.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.12977218325250295
Kappa statistic: 0.8593101287307099
Training time: 251.0
		
Time end:Sat Oct 07 11.20.53 EEST 2017