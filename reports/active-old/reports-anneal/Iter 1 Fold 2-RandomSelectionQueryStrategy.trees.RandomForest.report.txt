Sat Oct 07 11.25.01 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.01 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 28.285077951002165
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9420743414484295
Weighted AreaUnderROC: 0.9475805919041528
Root mean squared error: 0.1345279757044823
Relative absolute error: 16.833421610247044
Root relative squared error: 36.09764382607968
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8642375630225518
Weighted FMeasure: 0.9464427506259596
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9451720864701725
Mean absolute error: 0.046759504472907984
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.10154294731378637
Kappa statistic: 0.8744790078275897
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 28.359317000742358
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9421070939703736
Weighted AreaUnderROC: 0.9875688752604938
Root mean squared error: 0.12017874712654854
Relative absolute error: 15.403161844697369
Root relative squared error: 32.24734176308657
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8644140771933038
Weighted FMeasure: 0.9462501644447078
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9797096045239952
Mean absolute error: 0.04278656067971449
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.10154294731378637
Kappa statistic: 0.8743352924713126
Training time: 4.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 26.91165553080918
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9411946301369989
Weighted AreaUnderROC: 0.9863274326091077
Root mean squared error: 0.1206195950481487
Relative absolute error: 14.636265499436854
Root relative squared error: 32.365633673539065
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8604635422001594
Weighted FMeasure: 0.9448149588558933
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9780170687120128
Mean absolute error: 0.040656293053990857
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.08814298402901595
Kappa statistic: 0.8711154930983701
Training time: 5.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 24.35040831477356
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9342627693511305
Weighted AreaUnderROC: 0.9833929395199618
Root mean squared error: 0.11182976518607737
Relative absolute error: 11.428204776613844
Root relative squared error: 30.00707482366916
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8443767886974428
Weighted FMeasure: 0.9377086304230557
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9786737843793784
Mean absolute error: 0.03174501326837147
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.12973834474310472
Kappa statistic: 0.8542699867577181
Training time: 5.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 23.496659242761655
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9512703004654107
Weighted AreaUnderROC: 0.9800724410054648
Root mean squared error: 0.112582567893666
Relative absolute error: 10.675402890007275
Root relative squared error: 30.20907298700685
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8887924369197797
Weighted FMeasure: 0.9555308583333931
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9732718244375472
Mean absolute error: 0.02965389691668658
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.07990798444087548
Kappa statistic: 0.8980960786786031
Training time: 5.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 24.35040831477357
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9506882547197514
Weighted AreaUnderROC: 0.9893136127762786
Root mean squared error: 0.11027354108400514
Relative absolute error: 11.448740040352037
Root relative squared error: 29.589496078014307
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8674366325158477
Weighted FMeasure: 0.948342234242791
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9820454322747545
Mean absolute error: 0.031802055667644234
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.10154294731378637
Kappa statistic: 0.8743608659153926
Training time: 6.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 25.4268745360059
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9293920272083237
Weighted AreaUnderROC: 0.9851573291791966
Root mean squared error: 0.12230195958099316
Relative absolute error: 13.714537709052701
Root relative squared error: 32.81705944854405
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8314000345480749
Weighted FMeasure: 0.932508324821228
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9762943892764627
Mean absolute error: 0.038095938080701566
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.14397559054374165
Kappa statistic: 0.8413469088624822
Training time: 7.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 24.573125463993993
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9401914042966285
Weighted AreaUnderROC: 0.9896572731819131
Root mean squared error: 0.11402426976672317
Relative absolute error: 12.133547275581563
Root relative squared error: 30.5959221939802
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8469871647521343
Weighted FMeasure: 0.9398345668474333
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9832513010747896
Mean absolute error: 0.03370429798772623
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1294792700901803
Kappa statistic: 0.854387193856372
Training time: 7.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 21.937639198218278
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9667970296475324
Weighted AreaUnderROC: 0.9951069747630802
Root mean squared error: 0.09050575374565992
Relative absolute error: 7.2988731521431784
Root relative squared error: 24.285242127618332
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9101018975206338
Weighted FMeasure: 0.9646411623877554
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9878105787651624
Mean absolute error: 0.02027464764484196
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.06539164446828305
Kappa statistic: 0.9154404379268783
Training time: 7.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.79138827023011
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9801091292387142
Weighted AreaUnderROC: 0.9952702512486262
Root mean squared error: 0.08963388605362411
Relative absolute error: 8.478232074805721
Root relative squared error: 24.05129547640498
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9469040358456939
Weighted FMeasure: 0.9798362441252523
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9895176981546617
Mean absolute error: 0.02355064465223788
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.03635896452309819
Kappa statistic: 0.9496912504357353
Training time: 8.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.754268745360015
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9738758648706252
Weighted AreaUnderROC: 0.9899736230467658
Root mean squared error: 0.09416369699274996
Relative absolute error: 8.750831932768843
Root relative squared error: 25.266771298617893
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9293690784444413
Weighted FMeasure: 0.9732678483255625
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9868567037329008
Mean absolute error: 0.02430786647991321
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.057196683918204684
Kappa statistic: 0.9321837633731908
Training time: 8.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 21.30660727542686
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9849606910934265
Weighted AreaUnderROC: 0.9963209435319781
Root mean squared error: 0.077733517392093
Relative absolute error: 6.416592696457211
Root relative squared error: 20.858091482265955
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595141913544695
Weighted FMeasure: 0.9843951617207368
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9943399977043514
Mean absolute error: 0.017823868601269853
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 85.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.015282189994098295
Kappa statistic: 0.9614317969862074
Training time: 10.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.121009651076452
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9606888247141441
Weighted AreaUnderROC: 0.9944255921100978
Root mean squared error: 0.08534389663937116
Relative absolute error: 6.748677164322395
Root relative squared error: 22.900170522042085
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9138651233076612
Weighted FMeasure: 0.9644876281847329
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9883870205445529
Mean absolute error: 0.01874632545645091
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 95.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.051433492839601615
Kappa statistic: 0.9218334203785222
Training time: 10.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 21.52932442464733
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9711187370271048
Weighted AreaUnderROC: 0.9952132542920232
Root mean squared error: 0.08629992374977893
Relative absolute error: 7.390982891317743
Root relative squared error: 23.156699514906624
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9235484235694257
Weighted FMeasure: 0.9710130287909858
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9934146315768888
Mean absolute error: 0.02053050803143797
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 107.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.050636249361797225
Kappa statistic: 0.9273733980340922
Training time: 11.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 22.048997772828507
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9766467379057795
Weighted AreaUnderROC: 0.9913811955821066
Root mean squared error: 0.0862386789261712
Relative absolute error: 7.69415081483566
Root relative squared error: 23.140265804243793
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.935486019870561
Weighted FMeasure: 0.9737893182851034
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9909802505900783
Mean absolute error: 0.021372641152321067
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 119.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.030077624138646224
Kappa statistic: 0.9393243243243243
Training time: 11.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 23.199703043800977
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9869942014839173
Weighted AreaUnderROC: 0.9956113413876789
Root mean squared error: 0.08211605726747467
Relative absolute error: 8.183155218021192
Root relative squared error: 22.034050331320792
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9651556739765145
Weighted FMeasure: 0.9865834453977803
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9909411920722017
Mean absolute error: 0.022730986716725306
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 132.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.015003095822142794
Kappa statistic: 0.9668226600985222
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 21.121009651076434
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9771661281117103
Weighted AreaUnderROC: 0.9959509018668101
Root mean squared error: 0.0798138175054763
Relative absolute error: 6.554009445157872
Root relative squared error: 21.416294578321025
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.940366732729549
Weighted FMeasure: 0.9764296283861342
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9927090435709993
Mean absolute error: 0.01820558179210502
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 145.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.05031711615177961
Kappa statistic: 0.9433667162786004
Training time: 12.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.306607275426842
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868927526467156
Weighted AreaUnderROC: 0.9977064971800247
Root mean squared error: 0.07448394192284663
Relative absolute error: 6.5161375197434905
Root relative squared error: 19.986138884595874
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9647021317258585
Weighted FMeasure: 0.9859870880346767
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9958111491693136
Mean absolute error: 0.018100381999287294
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 158.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665632369368251
Training time: 12.0
		
Time end:Sat Oct 07 11.25.02 EEST 2017