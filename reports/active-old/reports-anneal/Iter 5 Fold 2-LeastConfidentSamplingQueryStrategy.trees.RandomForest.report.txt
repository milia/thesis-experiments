Sat Oct 07 11.22.25 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.22.25 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.17149220489975
Incorrectly Classified Instances: 12.249443207126948
Correctly Classified Instances: 87.75055679287306
Weighted Precision: 0.869077830060563
Weighted AreaUnderROC: 0.9735961578578002
Root mean squared error: 0.1558038910917321
Relative absolute error: 20.57042692019544
Root relative squared error: 41.806570996810635
Weighted TruePositiveRate: 0.8775055679287305
Weighted MatthewsCorrelation: 0.6503718328835424
Weighted FMeasure: 0.8680327805171232
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9696135622940135
Mean absolute error: 0.0571400747783201
Coverage of cases: 99.77728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8775055679287305
Weighted FalsePositiveRate: 0.30138354271669054
Kappa statistic: 0.6553673104834207
Training time: 3.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 30.10393466963626
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9706115899833558
Weighted AreaUnderROC: 0.9967131631812899
Root mean squared error: 0.11087522174902571
Relative absolute error: 16.21190033670963
Root relative squared error: 29.7509439421423
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9153138598507649
Weighted FMeasure: 0.9681073238079931
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9938358699204277
Mean absolute error: 0.04503305649085963
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.03734775760944955
Kappa statistic: 0.9180158247108948
Training time: 4.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 25.946547884187066
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9673946499066207
Weighted AreaUnderROC: 0.9964152326878156
Root mean squared error: 0.11137538340276484
Relative absolute error: 13.931097375254105
Root relative squared error: 29.88515139704223
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.906078625991832
Weighted FMeasure: 0.9650735746707159
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9938829905869179
Mean absolute error: 0.03869749270903879
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07866402415158284
Kappa statistic: 0.9089964784271997
Training time: 5.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 27.20861172976983
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9798957794629088
Weighted AreaUnderROC: 0.9943744266439883
Root mean squared error: 0.10167442401049671
Relative absolute error: 13.242982479213426
Root relative squared error: 27.282110839273027
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9460371874546392
Weighted FMeasure: 0.9790447110628245
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9932999147971588
Mean absolute error: 0.03678606244225915
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.05038854864614025
Kappa statistic: 0.9488998482549318
Training time: 7.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 27.171492204899742
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9783881877423081
Weighted AreaUnderROC: 0.9915262320174957
Root mean squared error: 0.10873309326766534
Relative absolute error: 14.182113261386142
Root relative squared error: 29.176150554039076
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9411418156562974
Weighted FMeasure: 0.9770876163554337
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9913958649151727
Mean absolute error: 0.03939475905940556
Coverage of cases: 99.55456570155901
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 33.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.036813322028211755
Kappa statistic: 0.9442277594216579
Training time: 7.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 24.38752783964366
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9825215239686664
Weighted AreaUnderROC: 0.9973493080987608
Root mean squared error: 0.08202846426201703
Relative absolute error: 9.182484378162993
Root relative squared error: 22.010546661573972
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9536314976832471
Weighted FMeasure: 0.9822252530117801
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9963775426141799
Mean absolute error: 0.025506901050452505
Coverage of cases: 99.77728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.02223319025488401
Kappa statistic: 0.9557559185081171
Training time: 7.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 25.575352635486258
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9824059721423322
Weighted AreaUnderROC: 0.9994874831457311
Root mean squared error: 0.08208846659836033
Relative absolute error: 10.142946242357748
Root relative squared error: 22.026646977918666
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9525177974293297
Weighted FMeasure: 0.9815723391979808
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9981995543155037
Mean absolute error: 0.028174850673215686
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.050109454474184745
Kappa statistic: 0.9543664405315445
Training time: 7.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 22.902746844840376
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.995589327044849
Weighted AreaUnderROC: 0.9989933395305661
Root mean squared error: 0.07267027941470683
Relative absolute error: 7.301481667076334
Root relative squared error: 19.49948216582267
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882165994212805
Weighted FMeasure: 0.995397598939022
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9972999699205809
Mean absolute error: 0.02028189351965628
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 59.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.007397717072273965
Kappa statistic: 0.9888566252202615
Training time: 8.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 23.57089829250184
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9856394481196223
Weighted AreaUnderROC: 0.9972199296182939
Root mean squared error: 0.0770589267479083
Relative absolute error: 8.490843639783229
Root relative squared error: 20.677079817780076
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.959977990215128
Weighted FMeasure: 0.9848154052829219
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9960774743912458
Mean absolute error: 0.0235856767771754
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.015114567354565542
Kappa statistic: 0.9614469358716453
Training time: 9.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.011878247958425
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9911301600455793
Weighted AreaUnderROC: 0.9977996060740066
Root mean squared error: 0.07544677743844229
Relative absolute error: 7.221479736205236
Root relative squared error: 20.24449476427863
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.976357417557994
Weighted FMeasure: 0.9909166632951916
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9969673223544487
Mean absolute error: 0.020059665933903237
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.021634962872910897
Kappa statistic: 0.9775421397489122
Training time: 9.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.60579064587971
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9833447347581321
Weighted AreaUnderROC: 0.996154333709152
Root mean squared error: 0.07287916484289772
Relative absolute error: 7.3005267144322605
Root relative squared error: 19.555532007855945
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9538470536208509
Weighted FMeasure: 0.9823423355979362
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9964653031963205
Mean absolute error: 0.020279240873422744
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.015911810832369935
Kappa statistic: 0.9560633118868801
Training time: 10.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 23.979213066072745
Incorrectly Classified Instances: 0.6681514476614699
Correctly Classified Instances: 99.33184855233853
Weighted Precision: 0.9933751419753392
Weighted AreaUnderROC: 0.9939682311749284
Root mean squared error: 0.07488958331769728
Relative absolute error: 8.380854367569931
Root relative squared error: 20.09498389260074
Weighted TruePositiveRate: 0.9933184855233853
Weighted MatthewsCorrelation: 0.9823343274267384
Weighted FMeasure: 0.9931467509840141
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9946413533959826
Mean absolute error: 0.023280151021027352
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 103.0
Weighted Recall: 0.9933184855233853
Weighted FalsePositiveRate: 0.01451633997259243
Kappa statistic: 0.983217880992724
Training time: 12.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 22.568671121009647
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9846927537259794
Weighted AreaUnderROC: 0.9973928553497998
Root mean squared error: 0.08078508856681087
Relative absolute error: 7.847662348969908
Root relative squared error: 21.676913952447723
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9587116788766262
Weighted FMeasure: 0.9842322160912019
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9964100051775806
Mean absolute error: 0.02179906208047175
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 115.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.029311774117140362
Kappa statistic: 0.9608270807886932
Training time: 11.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 22.420193021529315
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9835616670463339
Weighted AreaUnderROC: 0.9981540880863515
Root mean squared error: 0.07975931626164938
Relative absolute error: 7.788748012759077
Root relative squared error: 21.401670359994394
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.953994356590909
Weighted FMeasure: 0.9823207858012047
Iteration time: 12.0
Weighted AreaUnderPRC: 0.997482912368595
Mean absolute error: 0.021635411146552777
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 127.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.015911810832369935
Kappa statistic: 0.9560471832019969
Training time: 11.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 22.08611729769862
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9823783815074691
Weighted AreaUnderROC: 0.9993650185268679
Root mean squared error: 0.07058816455188603
Relative absolute error: 6.606852925583803
Root relative squared error: 18.94079212139507
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9526394548230727
Weighted FMeasure: 0.981987425684748
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9976287814226457
Mean absolute error: 0.018352369237732605
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 140.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.03643039701745882
Kappa statistic: 0.9550674238823147
Training time: 12.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 21.677802524127706
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9920127516485436
Weighted AreaUnderROC: 0.9988012759656837
Root mean squared error: 0.07416908876908078
Relative absolute error: 6.7139745071271255
Root relative squared error: 19.9016549180258
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.977285812803449
Weighted FMeasure: 0.9914604722919306
Iteration time: 12.0
Weighted AreaUnderPRC: 0.996513966908298
Mean absolute error: 0.01864992918646405
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 152.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.0074377561103360764
Kappa statistic: 0.9778986488149444
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 21.492204899777274
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.995589327044849
Weighted AreaUnderROC: 0.9993485392220125
Root mean squared error: 0.06642343933775231
Relative absolute error: 6.002852655067578
Root relative squared error: 17.823279079025824
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882165994212805
Weighted FMeasure: 0.995397598939022
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9982202786191963
Mean absolute error: 0.016674590708520884
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 164.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.007397717072273965
Kappa statistic: 0.9888566252202615
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 22.568671121009636
Incorrectly Classified Instances: 0.6681514476614699
Correctly Classified Instances: 99.33184855233853
Weighted Precision: 0.9934616800432887
Weighted AreaUnderROC: 0.9972909804190864
Root mean squared error: 0.07847340313547983
Relative absolute error: 8.015001239054765
Root relative squared error: 21.056623660401467
Weighted TruePositiveRate: 0.9933184855233853
Weighted MatthewsCorrelation: 0.9824783650111125
Weighted FMeasure: 0.9931926967757398
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9970021143148327
Mean absolute error: 0.022263892330707462
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 178.0
Weighted Recall: 0.9933184855233853
Weighted FalsePositiveRate: 0.007501547911071397
Kappa statistic: 0.9833514609185742
Training time: 14.0
		
Time end:Sat Oct 07 11.22.26 EEST 2017