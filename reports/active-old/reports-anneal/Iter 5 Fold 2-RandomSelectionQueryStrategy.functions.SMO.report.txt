Sat Oct 07 11.24.34 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.24.34 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.8905868881847405
Weighted AreaUnderROC: 0.8710315873697829
Root mean squared error: 0.316000745522066
Relative absolute error: 81.2828507795114
Root relative squared error: 84.79189775135463
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7410410793312898
Weighted FMeasure: 0.8929566280599226
Iteration time: 89.0
Weighted AreaUnderPRC: 0.8595091532703679
Mean absolute error: 0.22578569660975167
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.18849552407610887
Kappa statistic: 0.7370099442911439
Training time: 88.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9315568242416638
Weighted AreaUnderROC: 0.8996676772168236
Root mean squared error: 0.31367417371474654
Relative absolute error: 80.76614699331992
Root relative squared error: 84.16761302547053
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8230564871444589
Weighted FMeasure: 0.932061736054668
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9009325434562141
Mean absolute error: 0.22435040831477532
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15040691396629852
Kappa statistic: 0.8301571773299421
Training time: 98.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9335923954426993
Weighted AreaUnderROC: 0.9002276340642886
Root mean squared error: 0.31370046991077
Relative absolute error: 80.76614699331992
Root relative squared error: 84.1746690352935
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8287352125341544
Weighted FMeasure: 0.9338721559597811
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9020227990738126
Mean absolute error: 0.22435040831477532
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 275.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.15030308312750107
Kappa statistic: 0.8353221644420575
Training time: 87.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9434746279782033
Weighted AreaUnderROC: 0.9140961274307837
Root mean squared error: 0.31345319857374887
Relative absolute error: 80.712694877507
Root relative squared error: 84.10831917307752
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8497902810802431
Weighted FMeasure: 0.9430628762477316
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9144758330364615
Mean absolute error: 0.224201930215295
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 366.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.12193242236502463
Kappa statistic: 0.8560029761522969
Training time: 90.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9452726035409776
Weighted AreaUnderROC: 0.9151867687298788
Root mean squared error: 0.3133689765802694
Relative absolute error: 80.69487750556937
Root relative squared error: 84.08572004076305
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8553512272994075
Weighted FMeasure: 0.9449814735605802
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9160595114898649
Mean absolute error: 0.2241524375154682
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 458.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.1218285915262272
Kappa statistic: 0.8611894732774277
Training time: 91.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9448621545085106
Weighted AreaUnderROC: 0.9171885652424419
Root mean squared error: 0.31361631432227716
Relative absolute error: 80.74832962138228
Root relative squared error: 84.15208772130673
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8471918110306729
Weighted FMeasure: 0.942745923413557
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9184813503532702
Mean absolute error: 0.22430091561494855
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 554.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.10783904644124725
Kappa statistic: 0.853332495759784
Training time: 95.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9474275820178593
Weighted AreaUnderROC: 0.9303699114834958
Root mean squared error: 0.3120817749123619
Relative absolute error: 80.42761692650474
Root relative squared error: 83.74032758913997
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8573540946999693
Weighted FMeasure: 0.9461684243905606
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9215575706467231
Mean absolute error: 0.2234100470180665
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 652.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.10779900740318514
Kappa statistic: 0.8634549348065736
Training time: 98.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9464035321772336
Weighted AreaUnderROC: 0.928364142000768
Root mean squared error: 0.3122086202400135
Relative absolute error: 80.46325167038
Root relative squared error: 83.77436376217084
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8524536452107199
Weighted FMeasure: 0.9446692237164904
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9213724630170992
Mean absolute error: 0.22350903241772002
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 755.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.10773521560244982
Kappa statistic: 0.8584007165112963
Training time: 102.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9533436125863743
Weighted AreaUnderROC: 0.9338702265785541
Root mean squared error: 0.31184380085704727
Relative absolute error: 80.37416481069181
Root relative squared error: 83.6764724493922
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8742813299558898
Weighted FMeasure: 0.9522639696359072
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9275652028617828
Mean absolute error: 0.22326156891858615
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 863.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.10731851721779516
Kappa statistic: 0.8790874817265524
Training time: 108.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.955194380210498
Weighted AreaUnderROC: 0.9350765851791221
Root mean squared error: 0.3117644358055341
Relative absolute error: 80.35634743875417
Root relative squared error: 83.65517657136566
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8799652595416501
Weighted FMeasure: 0.9542513726810562
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9294421373028436
Mean absolute error: 0.22321207621875935
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 971.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.10721468637899774
Kappa statistic: 0.8843677568890034
Training time: 108.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9572399898210483
Weighted AreaUnderROC: 0.9362420863446231
Root mean squared error: 0.3116850505451235
Relative absolute error: 80.33853006681653
Root relative squared error: 83.6338752707229
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8857876248605318
Weighted FMeasure: 0.9562806060726046
Iteration time: 118.0
Weighted AreaUnderPRC: 0.931434100128587
Mean absolute error: 0.2231625835189326
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 1089.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.1071108555402003
Kappa statistic: 0.8896920013447464
Training time: 117.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9546799154816974
Weighted AreaUnderROC: 0.9339726236184653
Root mean squared error: 0.31184909114232084
Relative absolute error: 80.37416481069181
Root relative squared error: 83.67789198189125
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8750184250864925
Weighted FMeasure: 0.9528910012089006
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9291756923173826
Mean absolute error: 0.22326156891858615
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 1199.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.10715089457826242
Kappa statistic: 0.8791370779603662
Training time: 110.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9546799154816974
Weighted AreaUnderROC: 0.9339948953333873
Root mean squared error: 0.31184380085704727
Relative absolute error: 80.37416481069181
Root relative squared error: 83.6764724493922
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8750184250864925
Weighted FMeasure: 0.9528910012089006
Iteration time: 116.0
Weighted AreaUnderPRC: 0.929192861466349
Mean absolute error: 0.22326156891858615
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 1315.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.10715089457826242
Kappa statistic: 0.8791370779603662
Training time: 115.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9526332535124002
Weighted AreaUnderROC: 0.9328071224529642
Root mean squared error: 0.31192843465467895
Relative absolute error: 80.39198218262945
Root relative squared error: 83.69918208035601
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8693236693029727
Weighted FMeasure: 0.9508552455164708
Iteration time: 268.0
Weighted AreaUnderPRC: 0.9271837756477246
Mean absolute error: 0.2233110616184129
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 1583.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.10725472541705985
Kappa statistic: 0.8739021650326797
Training time: 268.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9536171248748907
Weighted AreaUnderROC: 0.9339047602488828
Root mean squared error: 0.31185967144364
Relative absolute error: 80.37416481069181
Root relative squared error: 83.6807309746479
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8745025941949613
Weighted FMeasure: 0.9524842393439275
Iteration time: 270.0
Weighted AreaUnderPRC: 0.9281368530649693
Mean absolute error: 0.22326156891858615
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1853.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1072347058980288
Kappa statistic: 0.879112284930383
Training time: 269.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9536171248748907
Weighted AreaUnderROC: 0.9339047602488828
Root mean squared error: 0.31185967144364
Relative absolute error: 80.37416481069181
Root relative squared error: 83.6807309746479
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8745025941949613
Weighted FMeasure: 0.9524842393439275
Iteration time: 282.0
Weighted AreaUnderPRC: 0.9281368530649693
Mean absolute error: 0.22326156891858615
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 2135.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1072347058980288
Kappa statistic: 0.879112284930383
Training time: 278.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9526332535124002
Weighted AreaUnderROC: 0.9327811647432647
Root mean squared error: 0.3119390122648366
Relative absolute error: 80.39198218262945
Root relative squared error: 83.70202035099824
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8693236693029727
Weighted FMeasure: 0.9508552455164708
Iteration time: 268.0
Weighted AreaUnderPRC: 0.9271647464996368
Mean absolute error: 0.2233110616184129
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 2403.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.10725472541705985
Kappa statistic: 0.8739021650326797
Training time: 268.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9572399898210483
Weighted AreaUnderROC: 0.9361938569200018
Root mean squared error: 0.31170092921465853
Relative absolute error: 80.33853006681653
Root relative squared error: 83.6381359648596
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8857876248605318
Weighted FMeasure: 0.9562806060726046
Iteration time: 275.0
Weighted AreaUnderPRC: 0.9313979018315327
Mean absolute error: 0.2231625835189326
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2678.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.1071108555402003
Kappa statistic: 0.8896920013447464
Training time: 274.0
		
Time end:Sat Oct 07 11.24.38 EEST 2017