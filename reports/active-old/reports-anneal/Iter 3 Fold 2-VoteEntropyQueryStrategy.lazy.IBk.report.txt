Sat Oct 07 11.25.17 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.17 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.259094283593136
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.8857516055333678
Weighted AreaUnderROC: 0.8489903105737779
Root mean squared error: 0.18421817269110943
Relative absolute error: 18.3204680627688
Root relative squared error: 49.43092281937417
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7240770057997313
Weighted FMeasure: 0.8879172703437423
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8383044270280685
Mean absolute error: 0.050890189063246154
Coverage of cases: 91.53674832962137
Instances selection time: 12.0
Test time: 17.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.1965802356203143
Kappa statistic: 0.7204052774282267
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9285721632797129
Weighted AreaUnderROC: 0.8795700617126783
Root mean squared error: 0.14571928420897634
Relative absolute error: 9.14396934909287
Root relative squared error: 39.10058701486613
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.819899791484253
Weighted FMeasure: 0.9292807852744253
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8866183840251185
Mean absolute error: 0.025399914858591055
Coverage of cases: 93.54120267260579
Instances selection time: 7.0
Test time: 44.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.17888788887014437
Kappa statistic: 0.8242993428598416
Training time: 1.0
		
Time end:Sat Oct 07 11.25.17 EEST 2017