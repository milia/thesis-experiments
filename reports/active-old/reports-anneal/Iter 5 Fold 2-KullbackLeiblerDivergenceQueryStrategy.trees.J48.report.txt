Sat Oct 07 11.21.01 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.01 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.743132887899023
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9291689293036557
Weighted AreaUnderROC: 0.92211725186921
Root mean squared error: 0.15102279906305227
Relative absolute error: 9.977728285078047
Root relative squared error: 40.52366938287337
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8291385458388747
Weighted FMeasure: 0.9157206145988515
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9118716821675379
Mean absolute error: 0.0277159119029943
Coverage of cases: 92.65033407572383
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.08482784869727708
Kappa statistic: 0.8061432184049838
Training time: 5.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 20.304380103934644
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9636748113181003
Weighted AreaUnderROC: 0.9784244402756529
Root mean squared error: 0.1060419943389885
Relative absolute error: 5.938128686458362
Root relative squared error: 28.454052937395282
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9065980836819953
Weighted FMeasure: 0.9619215497287003
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9592658031430977
Mean absolute error: 0.01649480190682862
Coverage of cases: 97.7728285077951
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.05946083075014722
Kappa statistic: 0.904806445176095
Training time: 19.0
		
Time end:Sat Oct 07 11.21.01 EEST 2017