Sat Oct 07 11.25.17 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.17 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.29621380846323
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.9021663721418449
Weighted AreaUnderROC: 0.8808706522030924
Root mean squared error: 0.19874924285011583
Relative absolute error: 20.330319601657663
Root relative squared error: 53.33001809873703
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.7094560131169619
Weighted FMeasure: 0.8835826383392622
Iteration time: 14.0
Weighted AreaUnderPRC: 0.865680281887277
Mean absolute error: 0.05647311000460406
Coverage of cases: 87.75055679287306
Instances selection time: 13.0
Test time: 17.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.11393207323939245
Kappa statistic: 0.7171144412942712
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9567533341904694
Weighted AreaUnderROC: 0.9142646169683439
Root mean squared error: 0.12105436785060109
Relative absolute error: 6.777970103775568
Root relative squared error: 32.48229545846526
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8789660323655858
Weighted FMeasure: 0.952095548799177
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9203584305748022
Mean absolute error: 0.018827694732709725
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 43.0
Accumulative iteration time: 21.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1283428738833272
Kappa statistic: 0.8814709220981495
Training time: 1.0
		
Time end:Sat Oct 07 11.25.17 EEST 2017