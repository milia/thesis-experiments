Sat Oct 07 11.21.19 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.19 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 8.106904231625833
Correctly Classified Instances: 91.89309576837418
Weighted Precision: 0.9129977019882884
Weighted AreaUnderROC: 0.891152936215566
Root mean squared error: 0.3131975611130343
Relative absolute error: 79.90620426344348
Root relative squared error: 84.03972444430717
Weighted TruePositiveRate: 0.9189309576837417
Weighted MatthewsCorrelation: 0.7841256358175805
Weighted FMeasure: 0.9134419243938288
Iteration time: 89.0
Weighted AreaUnderPRC: 0.88669607754603
Mean absolute error: 0.221961678509563
Coverage of cases: 99.28730512249442
Instances selection time: 4.4
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9189309576837417
Weighted FalsePositiveRate: 0.1631251479113869
Kappa statistic: 0.7879539844402847
Training time: 84.6
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 7.371937639198218
Correctly Classified Instances: 92.62806236080179
Weighted Precision: 0.922469280971231
Weighted AreaUnderROC: 0.9020649223904404
Root mean squared error: 0.3127451184437312
Relative absolute error: 79.8028635062052
Root relative squared error: 83.91832133656915
Weighted TruePositiveRate: 0.9262806236080179
Weighted MatthewsCorrelation: 0.8038897879434568
Weighted FMeasure: 0.9212436273771474
Iteration time: 77.4
Weighted AreaUnderPRC: 0.8974412167055155
Mean absolute error: 0.22167462085056772
Coverage of cases: 99.28730512249442
Instances selection time: 4.1
Test time: 8.0
Accumulative iteration time: 166.4
Weighted Recall: 0.9262806236080179
Weighted FalsePositiveRate: 0.14238411139049328
Kappa statistic: 0.8106072011765504
Training time: 73.3
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 6.926503340757238
Correctly Classified Instances: 93.07349665924276
Weighted Precision: 0.9263014310518687
Weighted AreaUnderROC: 0.9044785610602721
Root mean squared error: 0.31255815557278943
Relative absolute error: 79.76099268215174
Root relative squared error: 83.86815393392583
Weighted TruePositiveRate: 0.9307349665924276
Weighted MatthewsCorrelation: 0.8127419047833044
Weighted FMeasure: 0.924242554605273
Iteration time: 81.9
Weighted AreaUnderPRC: 0.8994300942455926
Mean absolute error: 0.22155831300597484
Coverage of cases: 99.28730512249442
Instances selection time: 3.7
Test time: 7.7
Accumulative iteration time: 248.3
Weighted Recall: 0.9307349665924276
Weighted FalsePositiveRate: 0.1414678425076018
Kappa statistic: 0.8207853762254274
Training time: 78.2
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 6.057906458797327
Correctly Classified Instances: 93.94209354120268
Weighted Precision: 0.9276006731340356
Weighted AreaUnderROC: 0.9083119209761426
Root mean squared error: 0.31220495392116254
Relative absolute error: 79.681323576202
Root relative squared error: 83.77337998558946
Weighted TruePositiveRate: 0.9394209354120268
Weighted MatthewsCorrelation: 0.830227301169584
Weighted FMeasure: 0.9304875762775046
Iteration time: 84.8
Weighted AreaUnderPRC: 0.9049621406850961
Mean absolute error: 0.22133700993389221
Coverage of cases: 99.28730512249442
Instances selection time: 3.5
Test time: 8.3
Accumulative iteration time: 333.1
Weighted Recall: 0.9394209354120268
Weighted FalsePositiveRate: 0.1423281562477337
Kappa statistic: 0.8399187201788187
Training time: 81.3
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.657015590200445
Correctly Classified Instances: 94.34298440979956
Weighted Precision: 0.9299237643932233
Weighted AreaUnderROC: 0.9069564514405133
Root mean squared error: 0.31205444412837924
Relative absolute error: 79.6475978364629
Root relative squared error: 83.73299397023634
Weighted TruePositiveRate: 0.9434298440979957
Weighted MatthewsCorrelation: 0.8398823118589724
Weighted FMeasure: 0.9326329027709381
Iteration time: 80.8
Weighted AreaUnderPRC: 0.9063507858307938
Mean absolute error: 0.22124332732350585
Coverage of cases: 99.28730512249442
Instances selection time: 3.3
Test time: 7.5
Accumulative iteration time: 413.9
Weighted Recall: 0.9434298440979957
Weighted FalsePositiveRate: 0.15035558744251035
Kappa statistic: 0.8474491528147798
Training time: 77.5
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.931322959390074
Weighted AreaUnderROC: 0.907731825146034
Root mean squared error: 0.31204563753745507
Relative absolute error: 79.6454342984419
Root relative squared error: 83.73063091392156
Weighted TruePositiveRate: 0.9443207126948776
Weighted MatthewsCorrelation: 0.8430190423381484
Weighted FMeasure: 0.9335878006622045
Iteration time: 82.8
Weighted AreaUnderPRC: 0.9073812507212299
Mean absolute error: 0.22123731749566974
Coverage of cases: 99.28730512249442
Instances selection time: 2.9
Test time: 7.5
Accumulative iteration time: 496.7
Weighted Recall: 0.9443207126948776
Weighted FalsePositiveRate: 0.14893700415997962
Kappa statistic: 0.8498038041105183
Training time: 79.9
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.701559020044543
Correctly Classified Instances: 94.29844097995546
Weighted Precision: 0.929972435489201
Weighted AreaUnderROC: 0.9062480513916243
Root mean squared error: 0.31207187926852653
Relative absolute error: 79.65116131085043
Root relative squared error: 83.73767230926433
Weighted TruePositiveRate: 0.9429844097995547
Weighted MatthewsCorrelation: 0.8391904737975162
Weighted FMeasure: 0.9319252861375251
Iteration time: 84.9
Weighted AreaUnderPRC: 0.9055675402647733
Mean absolute error: 0.22125322586347118
Coverage of cases: 99.28730512249442
Instances selection time: 3.0
Test time: 7.6
Accumulative iteration time: 581.6
Weighted Recall: 0.9429844097995547
Weighted FalsePositiveRate: 0.15179683835588983
Kappa statistic: 0.8461264495460725
Training time: 81.9
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.857461024498886
Correctly Classified Instances: 94.14253897550111
Weighted Precision: 0.9286216051727058
Weighted AreaUnderROC: 0.900324202573984
Root mean squared error: 0.3121096913678606
Relative absolute error: 79.659688195992
Root relative squared error: 83.74781836020227
Weighted TruePositiveRate: 0.9414253897550111
Weighted MatthewsCorrelation: 0.8334371071980178
Weighted FMeasure: 0.9295569330085336
Iteration time: 86.4
Weighted AreaUnderPRC: 0.901659230322877
Mean absolute error: 0.22127691165553115
Coverage of cases: 99.28730512249442
Instances selection time: 2.7
Test time: 7.5
Accumulative iteration time: 668.0
Weighted Recall: 0.9414253897550111
Weighted FalsePositiveRate: 0.1643059997559227
Kappa statistic: 0.8404347671141682
Training time: 83.7
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.835189309576838
Correctly Classified Instances: 94.16481069042317
Weighted Precision: 0.928811376343693
Weighted AreaUnderROC: 0.9007041126491562
Root mean squared error: 0.31210060144156115
Relative absolute error: 79.65777919185584
Root relative squared error: 83.74537927702838
Weighted TruePositiveRate: 0.9416481069042316
Weighted MatthewsCorrelation: 0.8340596659570144
Weighted FMeasure: 0.9298183612278305
Iteration time: 93.4
Weighted AreaUnderPRC: 0.902026739277118
Mean absolute error: 0.22127160886626399
Coverage of cases: 99.28730512249442
Instances selection time: 2.7
Test time: 7.8
Accumulative iteration time: 761.4
Weighted Recall: 0.9416481069042316
Weighted FalsePositiveRate: 0.16359413746589085
Kappa statistic: 0.8411384644897604
Training time: 90.7
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.701559020044543
Correctly Classified Instances: 94.29844097995547
Weighted Precision: 0.9295952562472006
Weighted AreaUnderROC: 0.9032237648402301
Root mean squared error: 0.31206212581861087
Relative absolute error: 79.64912503977185
Root relative squared error: 83.73505518402116
Weighted TruePositiveRate: 0.9429844097995547
Weighted MatthewsCorrelation: 0.837908310744797
Weighted FMeasure: 0.9312649783108604
Iteration time: 86.6
Weighted AreaUnderPRC: 0.9038163094215591
Mean absolute error: 0.22124756955491956
Coverage of cases: 99.28730512249442
Instances selection time: 2.1
Test time: 7.6
Accumulative iteration time: 848.0
Weighted Recall: 0.9429844097995547
Weighted FalsePositiveRate: 0.15938315978826129
Kappa statistic: 0.8449721962913257
Training time: 84.5
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.679287305122495
Correctly Classified Instances: 94.3207126948775
Weighted Precision: 0.9302257858084071
Weighted AreaUnderROC: 0.9009552452948884
Root mean squared error: 0.31204271897797664
Relative absolute error: 79.64403436207537
Root relative squared error: 83.72984778223476
Weighted TruePositiveRate: 0.9432071269487752
Weighted MatthewsCorrelation: 0.8384714077787683
Weighted FMeasure: 0.9309983082755316
Iteration time: 113.9
Weighted AreaUnderPRC: 0.9026246583729465
Mean absolute error: 0.22123342878354046
Coverage of cases: 99.28730512249442
Instances selection time: 2.0
Test time: 7.1
Accumulative iteration time: 961.9
Weighted Recall: 0.9432071269487752
Weighted FalsePositiveRate: 0.16570132368497867
Kappa statistic: 0.8439085867691747
Training time: 111.9
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.634743875278397
Correctly Classified Instances: 94.3652561247216
Weighted Precision: 0.9306985303502465
Weighted AreaUnderROC: 0.9012066819273759
Root mean squared error: 0.31202738314176653
Relative absolute error: 79.64047088768784
Root relative squared error: 83.72573274556385
Weighted TruePositiveRate: 0.9436525612472162
Weighted MatthewsCorrelation: 0.839736034799287
Weighted FMeasure: 0.9314394691653906
Iteration time: 117.1
Weighted AreaUnderPRC: 0.9031517028641067
Mean absolute error: 0.22122353024357513
Coverage of cases: 99.28730512249442
Instances selection time: 2.3
Test time: 7.4
Accumulative iteration time: 1079.0
Weighted Recall: 0.9436525612472162
Weighted FalsePositiveRate: 0.16564675798341966
Kappa statistic: 0.8450150394844682
Training time: 114.8
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.634743875278397
Correctly Classified Instances: 94.3652561247216
Weighted Precision: 0.9306985303502465
Weighted AreaUnderROC: 0.9012049298823526
Root mean squared error: 0.3120268544449888
Relative absolute error: 79.64047088768784
Root relative squared error: 83.72559088133171
Weighted TruePositiveRate: 0.9436525612472162
Weighted MatthewsCorrelation: 0.839736034799287
Weighted FMeasure: 0.9314394691653906
Iteration time: 121.3
Weighted AreaUnderPRC: 0.9031580625890907
Mean absolute error: 0.22122353024357513
Coverage of cases: 99.28730512249442
Instances selection time: 1.8
Test time: 7.3
Accumulative iteration time: 1200.3
Weighted Recall: 0.9436525612472162
Weighted FalsePositiveRate: 0.16564675798341966
Kappa statistic: 0.8450150394844682
Training time: 119.5
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.590200445434299
Correctly Classified Instances: 94.4097995545657
Weighted Precision: 0.9312140737208932
Weighted AreaUnderROC: 0.9020042980788269
Root mean squared error: 0.3120085827543865
Relative absolute error: 79.63652561247306
Root relative squared error: 83.72068807226162
Weighted TruePositiveRate: 0.9440979955456571
Weighted MatthewsCorrelation: 0.8409556328678999
Weighted FMeasure: 0.9318060015848451
Iteration time: 228.2
Weighted AreaUnderPRC: 0.9039006761595282
Mean absolute error: 0.22121257114575635
Coverage of cases: 99.28730512249442
Instances selection time: 1.9
Test time: 5.5
Accumulative iteration time: 1428.5
Weighted Recall: 0.9440979955456571
Weighted FalsePositiveRate: 0.1642405597366718
Kappa statistic: 0.8464219582757602
Training time: 226.3
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.634743875278397
Correctly Classified Instances: 94.3652561247216
Weighted Precision: 0.9306941647996849
Weighted AreaUnderROC: 0.8991917550835886
Root mean squared error: 0.3119376070119941
Relative absolute error: 79.62048997772919
Root relative squared error: 83.70164328209248
Weighted TruePositiveRate: 0.9436525612472162
Weighted MatthewsCorrelation: 0.839072607474751
Weighted FMeasure: 0.9309083060313016
Iteration time: 217.7
Weighted AreaUnderPRC: 0.9020822936063752
Mean absolute error: 0.2211680277159122
Coverage of cases: 99.28730512249442
Instances selection time: 1.5
Test time: 6.0
Accumulative iteration time: 1646.2
Weighted Recall: 0.9436525612472162
Weighted FalsePositiveRate: 0.17183613337274586
Kappa statistic: 0.8438326625131924
Training time: 216.2
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 5.32293986636971
Correctly Classified Instances: 94.67706013363029
Weighted Precision: 0.9332846650285116
Weighted AreaUnderROC: 0.9075253389413795
Root mean squared error: 0.31172042573369363
Relative absolute error: 79.5737830098641
Root relative squared error: 83.64336742988606
Weighted TruePositiveRate: 0.9467706013363028
Weighted MatthewsCorrelation: 0.8477378090175707
Weighted FMeasure: 0.9346001378697173
Iteration time: 224.2
Weighted AreaUnderPRC: 0.9084977890402863
Mean absolute error: 0.22103828613850918
Coverage of cases: 99.28730512249442
Instances selection time: 1.4
Test time: 5.7
Accumulative iteration time: 1870.4
Weighted Recall: 0.9467706013363028
Weighted FalsePositiveRate: 0.1590991971543232
Kappa statistic: 0.8536992199291614
Training time: 222.8
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 76.66666666666644
Incorrectly Classified Instances: 4.810690423162583
Correctly Classified Instances: 95.18930957683742
Weighted Precision: 0.9381625722716846
Weighted AreaUnderROC: 0.9207304630644491
Root mean squared error: 0.3114423823383637
Relative absolute error: 79.51002227171583
Root relative squared error: 83.56876055796775
Weighted TruePositiveRate: 0.9518930957683741
Weighted MatthewsCorrelation: 0.8620556795327599
Weighted FMeasure: 0.9405401216517968
Iteration time: 233.1
Weighted AreaUnderPRC: 0.9197013849009744
Mean absolute error: 0.2208611729769862
Coverage of cases: 99.28730512249442
Instances selection time: 1.0
Test time: 5.5
Accumulative iteration time: 2103.5
Weighted Recall: 0.9518930957683741
Weighted FalsePositiveRate: 0.13509862645480564
Kappa statistic: 0.8699033380548681
Training time: 232.1
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 78.33333333333309
Incorrectly Classified Instances: 4.721603563474388
Correctly Classified Instances: 95.27839643652561
Weighted Precision: 0.9397496696769079
Weighted AreaUnderROC: 0.925356336142241
Root mean squared error: 0.31171851748873497
Relative absolute error: 79.75781100859152
Root relative squared error: 83.64285539403264
Weighted TruePositiveRate: 0.9527839643652563
Weighted MatthewsCorrelation: 0.8653092603871813
Weighted FMeasure: 0.942466864659799
Iteration time: 240.4
Weighted AreaUnderPRC: 0.9228706783844803
Mean absolute error: 0.22154947502386313
Coverage of cases: 99.37639198218262
Instances selection time: 0.9
Test time: 5.5
Accumulative iteration time: 2343.9
Weighted Recall: 0.9527839643652563
Weighted FalsePositiveRate: 0.12534091666663602
Kappa statistic: 0.8733602788930417
Training time: 239.5
		
Time end:Sat Oct 07 11.21.51 EEST 2017