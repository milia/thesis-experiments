Sat Oct 07 11.24.57 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.24.57 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.824053452115887
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9309569638303801
Weighted AreaUnderROC: 0.9580505086557751
Root mean squared error: 0.1433753525784473
Relative absolute error: 9.311697953123435
Root relative squared error: 38.471644159609134
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8526152826219733
Weighted FMeasure: 0.9311466339035276
Iteration time: 2.0
Weighted AreaUnderPRC: 0.925939694571677
Mean absolute error: 0.02586582764756484
Coverage of cases: 95.10022271714922
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.0556536087526009
Kappa statistic: 0.8374641021309457
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 17.557535263548605
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.961728872463839
Weighted AreaUnderROC: 0.9741679398990697
Root mean squared error: 0.12452254769691094
Relative absolute error: 7.087750556792944
Root relative squared error: 33.41290576581049
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8822799893493201
Weighted FMeasure: 0.944027747315413
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9526188654486059
Mean absolute error: 0.019688195991091315
Coverage of cases: 95.99109131403118
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.010915363879399573
Kappa statistic: 0.8388090467973736
Training time: 3.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 17.557535263548605
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.961728872463839
Weighted AreaUnderROC: 0.9741679398990697
Root mean squared error: 0.1246224346030222
Relative absolute error: 7.099109131403187
Root relative squared error: 33.439708235265726
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8822799893493201
Weighted FMeasure: 0.944027747315413
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9526188654486059
Mean absolute error: 0.01971974758723088
Coverage of cases: 95.99109131403118
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.010915363879399573
Kappa statistic: 0.8388090467973736
Training time: 3.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 17.557535263548605
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.961728872463839
Weighted AreaUnderROC: 0.9741679398990697
Root mean squared error: 0.12476118422815266
Relative absolute error: 7.109131403118111
Root relative squared error: 33.476938666503045
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8822799893493201
Weighted FMeasure: 0.944027747315413
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9526188654486059
Mean absolute error: 0.019747587230883445
Coverage of cases: 95.99109131403118
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.010915363879399573
Kappa statistic: 0.8388090467973736
Training time: 4.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 18.485523385300695
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.94627106061178
Weighted AreaUnderROC: 0.9592254834561829
Root mean squared error: 0.11916040897264189
Relative absolute error: 7.009863188036974
Root relative squared error: 31.974092962740535
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8749902205513717
Weighted FMeasure: 0.9500727621455676
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9463007223548229
Mean absolute error: 0.0194718421889914
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.11498928043627583
Kappa statistic: 0.883284160178843
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 17.334818114328137
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9663031158754073
Weighted AreaUnderROC: 0.9807389795482415
Root mean squared error: 0.10734443864399826
Relative absolute error: 4.917594654788467
Root relative squared error: 28.803535417744396
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.9053612216950789
Weighted FMeasure: 0.9580073964010645
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9654977654123242
Mean absolute error: 0.013659985152190052
Coverage of cases: 97.32739420935413
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.010715168689089023
Kappa statistic: 0.8890992919479666
Training time: 4.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 18.225686711210052
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.975671426699856
Weighted AreaUnderROC: 0.973574736244006
Root mean squared error: 0.0931797874963568
Relative absolute error: 3.685382721838037
Root relative squared error: 25.00276067650077
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9378100364577526
Weighted FMeasure: 0.9742048358040887
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9631305819077203
Mean absolute error: 0.01023717422732778
Coverage of cases: 98.21826280623608
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.029898692697342525
Kappa statistic: 0.9337359028913678
Training time: 5.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 18.708240534521174
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9624269109067219
Weighted AreaUnderROC: 0.9789148999713974
Root mean squared error: 0.09651499762289276
Relative absolute error: 4.93763919821831
Root relative squared error: 25.89769146396239
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9185144661861101
Weighted FMeasure: 0.9666689293953236
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9642089638375634
Mean absolute error: 0.01371566443949517
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.06515892013404657
Kappa statistic: 0.9261045701987592
Training time: 6.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 18.708240534521174
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9624269109067219
Weighted AreaUnderROC: 0.9789148999713974
Root mean squared error: 0.09676342056255068
Relative absolute error: 4.791388270230193
Root relative squared error: 25.964350333591838
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9185144661861101
Weighted FMeasure: 0.9666689293953236
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9642089638375634
Mean absolute error: 0.013309411861750404
Coverage of cases: 97.7728285077951
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.06515892013404657
Kappa statistic: 0.9261045701987592
Training time: 6.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.926503340757225
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9624269109067219
Weighted AreaUnderROC: 0.9789148999713974
Root mean squared error: 0.09700059497442023
Relative absolute error: 4.669687428000966
Root relative squared error: 26.027990906487503
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9185144661861101
Weighted FMeasure: 0.9666689293953236
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9642089638375634
Mean absolute error: 0.012971353966669222
Coverage of cases: 97.10467706013362
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.06515892013404657
Kappa statistic: 0.9261045701987592
Training time: 5.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.926503340757225
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9691325097871045
Weighted AreaUnderROC: 0.9804441550137553
Root mean squared error: 0.08583676130348628
Relative absolute error: 3.5084025106297174
Root relative squared error: 23.032419989162367
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9365007997287873
Weighted FMeasure: 0.9731982975210836
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9715985148634297
Mean absolute error: 0.009745562529526896
Coverage of cases: 97.7728285077951
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 56.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.06434043461066127
Kappa statistic: 0.9425176991716915
Training time: 8.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.926503340757225
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9691325097871045
Weighted AreaUnderROC: 0.9804441550137553
Root mean squared error: 0.08593201274353415
Relative absolute error: 3.4699331848552686
Root relative squared error: 23.057978632550583
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9365007997287873
Weighted FMeasure: 0.9731982975210836
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9715985148634297
Mean absolute error: 0.009638703291264539
Coverage of cases: 97.7728285077951
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.06434043461066127
Kappa statistic: 0.9425176991716915
Training time: 7.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.85226429101706
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.987565572416261
Weighted AreaUnderROC: 0.9837717279604624
Root mean squared error: 0.07078937672745131
Relative absolute error: 2.138084632516725
Root relative squared error: 18.99478301369082
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9728740897870191
Weighted FMeasure: 0.9841018033268002
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9786073180657897
Mean absolute error: 0.005939123979213066
Coverage of cases: 98.21826280623608
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.014610171903587304
Kappa statistic: 0.9553910732470628
Training time: 7.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.852264291017068
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9810206255446887
Weighted AreaUnderROC: 0.9694516736177586
Root mean squared error: 0.09164219825962261
Relative absolute error: 3.273942093541236
Root relative squared error: 24.59018218992361
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.9516181012844985
Weighted FMeasure: 0.9770915684903151
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9627946505402429
Mean absolute error: 0.00909428359317001
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.0359660406045427
Kappa statistic: 0.93814575010332
Training time: 8.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868102527131583
Weighted AreaUnderROC: 0.9787936032608429
Root mean squared error: 0.0665134672938844
Relative absolute error: 1.8835507476933047
Root relative squared error: 17.847436114600228
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9645033374682596
Weighted FMeasure: 0.9860330547257569
Iteration time: 9.0
Weighted AreaUnderPRC: 0.977339273979189
Mean absolute error: 0.005232085410259127
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.035865943009387426
Kappa statistic: 0.9660410180131349
Training time: 9.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868102527131583
Weighted AreaUnderROC: 0.9795562533867718
Root mean squared error: 0.06635066546069181
Relative absolute error: 2.1594326357599782
Root relative squared error: 17.803751798694602
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9645033374682596
Weighted FMeasure: 0.9860330547257569
Iteration time: 9.0
Weighted AreaUnderPRC: 0.978360003043033
Mean absolute error: 0.005998423988222102
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.035865943009387426
Kappa statistic: 0.9660410180131349
Training time: 8.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868102527131583
Weighted AreaUnderROC: 0.9772697673149267
Root mean squared error: 0.06630551732712794
Relative absolute error: 2.4441785860428733
Root relative squared error: 17.791637283209877
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9645033374682596
Weighted FMeasure: 0.9860330547257569
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9771720208899746
Mean absolute error: 0.006789384961230136
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 107.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.035865943009387426
Kappa statistic: 0.9660410180131349
Training time: 10.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868102527131583
Weighted AreaUnderROC: 0.9772697673149267
Root mean squared error: 0.06630348086090247
Relative absolute error: 2.4050853568617487
Root relative squared error: 17.791090841980193
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9645033374682596
Weighted FMeasure: 0.9860330547257569
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9771720208899746
Mean absolute error: 0.0066807926579492355
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 118.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.035865943009387426
Kappa statistic: 0.9660410180131349
Training time: 11.0
		
Time end:Sat Oct 07 11.24.58 EEST 2017