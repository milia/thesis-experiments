Sat Oct 07 11.21.48 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.48 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.9082097183118321
Weighted AreaUnderROC: 0.8988729415663013
Root mean squared error: 0.3144778414989872
Relative absolute error: 80.94432071269615
Root relative squared error: 84.38325972109719
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.756978578001933
Weighted FMeasure: 0.9019682630213192
Iteration time: 89.0
Weighted AreaUnderPRC: 0.884513791617787
Mean absolute error: 0.22484533531304263
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.14586746772297135
Kappa statistic: 0.7497088918068529
Training time: 85.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.930895821766384
Weighted AreaUnderROC: 0.9295881881608314
Root mean squared error: 0.31309509863655266
Relative absolute error: 80.6236080178187
Root relative squared error: 84.01223087680043
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8183351311283097
Weighted FMeasure: 0.9291899543805289
Iteration time: 87.0
Weighted AreaUnderPRC: 0.916862843491116
Mean absolute error: 0.22395446671616082
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 176.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.08855931227234394
Kappa statistic: 0.8269207603536753
Training time: 83.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.930895821766384
Weighted AreaUnderROC: 0.9295881881608314
Root mean squared error: 0.31308456008495383
Relative absolute error: 80.6236080178187
Root relative squared error: 84.00940308666931
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8183351311283097
Weighted FMeasure: 0.9291899543805289
Iteration time: 90.0
Weighted AreaUnderPRC: 0.916862843491116
Mean absolute error: 0.22395446671616082
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 266.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.08855931227234394
Kappa statistic: 0.8269207603536753
Training time: 87.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9556934484347778
Weighted AreaUnderROC: 0.9449028977970754
Root mean squared error: 0.31206062901167586
Relative absolute error: 80.3919821826294
Root relative squared error: 83.73465354857444
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8854545616899283
Weighted FMeasure: 0.954102894668887
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9384228329438041
Mean absolute error: 0.2233110616184128
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 355.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.08650845803534506
Kappa statistic: 0.8919415311344175
Training time: 85.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9618417687703731
Weighted AreaUnderROC: 0.9434485382676581
Root mean squared error: 0.3119760310761297
Relative absolute error: 80.37416481069177
Root relative squared error: 83.71195354041795
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8912044960646119
Weighted FMeasure: 0.9548841284151471
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9394139604234895
Mean absolute error: 0.223261568918586
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 448.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.09341941925806865
Kappa statistic: 0.8963606986227592
Training time: 90.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9618417687703731
Weighted AreaUnderROC: 0.9434485382676581
Root mean squared error: 0.3119813191191678
Relative absolute error: 80.37416481069177
Root relative squared error: 83.7133724712621
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8912044960646119
Weighted FMeasure: 0.9548841284151471
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9394139604234895
Mean absolute error: 0.223261568918586
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 542.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.09341941925806865
Kappa statistic: 0.8963606986227592
Training time: 91.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9618417687703731
Weighted AreaUnderROC: 0.9433965015505505
Root mean squared error: 0.3119813191191678
Relative absolute error: 80.37416481069177
Root relative squared error: 83.7133724712621
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8912044960646119
Weighted FMeasure: 0.9548841284151471
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9394061784080714
Mean absolute error: 0.223261568918586
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 641.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.09341941925806865
Kappa statistic: 0.8963606986227592
Training time: 96.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9455803836383398
Weighted AreaUnderROC: 0.9077932777029681
Root mean squared error: 0.31245687650985815
Relative absolute error: 80.48106904231754
Root relative squared error: 83.84097790959642
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8367335301855701
Weighted FMeasure: 0.9354187003237102
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9074512828259369
Mean absolute error: 0.2235585251175465
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 746.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.15696887605508594
Kappa statistic: 0.8393155369403281
Training time: 102.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9455803836383398
Weighted AreaUnderROC: 0.9077932777029681
Root mean squared error: 0.3124515965153242
Relative absolute error: 80.48106904231754
Root relative squared error: 83.83956113839258
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8367335301855701
Weighted FMeasure: 0.9354187003237102
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9074512828259369
Mean absolute error: 0.2235585251175465
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 847.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.15696887605508594
Kappa statistic: 0.8393155369403281
Training time: 99.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9619153674832962
Weighted AreaUnderROC: 0.9306694539796162
Root mean squared error: 0.31174856037065757
Relative absolute error: 80.32071269487881
Root relative squared error: 83.6509167451789
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8910645286771405
Weighted FMeasure: 0.9542457308252643
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9314748336785549
Mean absolute error: 0.22311309081910558
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 949.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12813521220573235
Kappa statistic: 0.8921968787515008
Training time: 100.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9619153674832962
Weighted AreaUnderROC: 0.9306694539796162
Root mean squared error: 0.31174326837937616
Relative absolute error: 80.32071269487881
Root relative squared error: 83.6494967549099
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8910645286771405
Weighted FMeasure: 0.9542457308252643
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9314748336785549
Mean absolute error: 0.22311309081910558
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1050.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12813521220573235
Kappa statistic: 0.8921968787515008
Training time: 99.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9619153674832962
Weighted AreaUnderROC: 0.9306694539796162
Root mean squared error: 0.31174326837937616
Relative absolute error: 80.32071269487881
Root relative squared error: 83.6494967549099
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8910645286771405
Weighted FMeasure: 0.9542457308252643
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9314748336785549
Mean absolute error: 0.22311309081910558
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1155.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12813521220573235
Kappa statistic: 0.8921968787515008
Training time: 103.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9619153674832962
Weighted AreaUnderROC: 0.9306694539796162
Root mean squared error: 0.31174326837937616
Relative absolute error: 80.32071269487881
Root relative squared error: 83.6494967549099
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8910645286771405
Weighted FMeasure: 0.9542457308252643
Iteration time: 107.0
Weighted AreaUnderPRC: 0.9314748336785549
Mean absolute error: 0.22311309081910558
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1262.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12813521220573235
Kappa statistic: 0.8921968787515008
Training time: 105.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9614609058999528
Weighted AreaUnderROC: 0.9336984865030601
Root mean squared error: 0.3117379762982591
Relative absolute error: 80.32071269487882
Root relative squared error: 83.64807674053549
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8906335891494246
Weighted FMeasure: 0.9543107740650966
Iteration time: 256.0
Weighted AreaUnderPRC: 0.9326517145653623
Mean absolute error: 0.2231130908191056
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1518.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.1212956834773694
Kappa statistic: 0.8930301506207482
Training time: 254.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9639309887028432
Weighted AreaUnderROC: 0.9349516193351403
Root mean squared error: 0.3116638777273185
Relative absolute error: 80.30289532294117
Root relative squared error: 83.62819400753644
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8970971492773555
Weighted FMeasure: 0.9565254868996899
Iteration time: 259.0
Weighted AreaUnderPRC: 0.9353572426537041
Mean absolute error: 0.2230635981192788
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1777.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.12101658930541388
Kappa statistic: 0.8985809571895512
Training time: 257.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9539636310237646
Weighted AreaUnderROC: 0.9135407925575203
Root mean squared error: 0.31205534231261145
Relative absolute error: 80.39198218262933
Root relative squared error: 83.73323497835628
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.866761258111944
Weighted FMeasure: 0.944888338788539
Iteration time: 270.0
Weighted AreaUnderPRC: 0.9160559461966147
Mean absolute error: 0.22331106161841258
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2047.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.15660970380700623
Kappa statistic: 0.8661553887428525
Training time: 269.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9559692138698961
Weighted AreaUnderROC: 0.9420717093292493
Root mean squared error: 0.3120341946206439
Relative absolute error: 80.39198218262939
Root relative squared error: 83.72756045713949
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8739418393784539
Weighted FMeasure: 0.9484214850857705
Iteration time: 266.0
Weighted AreaUnderPRC: 0.9350553377307975
Mean absolute error: 0.22331106161841277
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 2313.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.09425670177393516
Kappa statistic: 0.8804306474929622
Training time: 265.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9559692138698961
Weighted AreaUnderROC: 0.9420717093292493
Root mean squared error: 0.31205005552398035
Relative absolute error: 80.39198218262939
Root relative squared error: 83.73181638410487
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8739418393784539
Weighted FMeasure: 0.9484214850857705
Iteration time: 268.0
Weighted AreaUnderPRC: 0.9350553377307975
Mean absolute error: 0.22331106161841277
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 2581.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.09425670177393516
Kappa statistic: 0.8804306474929622
Training time: 267.0
		
Time end:Sat Oct 07 11.21.51 EEST 2017