Sat Oct 07 11.21.06 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.06 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.267260579064562
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9554920370765934
Weighted AreaUnderROC: 0.9766543215650879
Root mean squared error: 0.11605763976625251
Relative absolute error: 8.774558748907621
Root relative squared error: 31.141532619062964
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8833094123885586
Weighted FMeasure: 0.9505359645094003
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9570614589103557
Mean absolute error: 0.024373774302520926
Coverage of cases: 98.21826280623608
Instances selection time: 18.0
Test time: 27.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.05375379025015808
Kappa statistic: 0.8794851460989446
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.935412026726123
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9676518269662475
Weighted AreaUnderROC: 0.9812823180334307
Root mean squared error: 0.10326418631225767
Relative absolute error: 8.782420345456659
Root relative squared error: 27.708688828249514
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9145703186764779
Weighted FMeasure: 0.9649390131245235
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9752017172842365
Mean absolute error: 0.024395612070712697
Coverage of cases: 98.44097995545657
Instances selection time: 17.0
Test time: 29.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.05184399069559238
Kappa statistic: 0.9163354037267082
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 20.564216778025237
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9724877751951458
Weighted AreaUnderROC: 0.9801178831783707
Root mean squared error: 0.09887156863827318
Relative absolute error: 7.360462143768397
Root relative squared error: 26.530025822065948
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9279264033827453
Weighted FMeasure: 0.9689501641703114
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9692875102784648
Mean absolute error: 0.020445728177134234
Coverage of cases: 97.99554565701558
Instances selection time: 16.0
Test time: 31.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.017173192556182795
Kappa statistic: 0.9241901637762608
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 21.23236822568679
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9761875408897022
Weighted AreaUnderROC: 0.982750326812417
Root mean squared error: 0.09893281012794479
Relative absolute error: 8.184515947759266
Root relative squared error: 26.546458638139836
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.938472272437959
Weighted FMeasure: 0.9731903385601152
Iteration time: 16.0
Weighted AreaUnderPRC: 0.978689269337108
Mean absolute error: 0.02273476652155329
Coverage of cases: 98.66369710467706
Instances selection time: 16.0
Test time: 25.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.01679653320959026
Kappa statistic: 0.9345345856165632
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.08166295471424
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9583949858775139
Weighted AreaUnderROC: 0.979499886756459
Root mean squared error: 0.1244214737201939
Relative absolute error: 9.269898968529894
Root relative squared error: 33.38578477188703
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8801103895498932
Weighted FMeasure: 0.9490385140132505
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9692370211345914
Mean absolute error: 0.025749719357027225
Coverage of cases: 97.55011135857461
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 84.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.018380498613154772
Kappa statistic: 0.8748562867992892
Training time: 1.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 19.784706755753596
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.970963502477979
Weighted AreaUnderROC: 0.9798769130395242
Root mean squared error: 0.10373760903381801
Relative absolute error: 7.322416809745927
Root relative squared error: 27.835721474749736
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9228117584077044
Weighted FMeasure: 0.9668931979322042
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9643631980571472
Mean absolute error: 0.020340046693738484
Coverage of cases: 97.99554565701558
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.017446021063977895
Kappa statistic: 0.9190611818149044
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.227913882702346
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9716124055784042
Weighted AreaUnderROC: 0.9795920061610862
Root mean squared error: 0.10578865177608457
Relative absolute error: 6.74487362218569
Root relative squared error: 28.386073994325614
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9229820759010553
Weighted FMeasure: 0.9670291401249085
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9616567811964918
Mean absolute error: 0.01873576006162673
Coverage of cases: 97.7728285077951
Instances selection time: 13.0
Test time: 25.0
Accumulative iteration time: 111.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.017615018732975562
Kappa statistic: 0.9190329638623742
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.07943578322203
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9688018135308823
Weighted AreaUnderROC: 0.9799110088251236
Root mean squared error: 0.10593074913787723
Relative absolute error: 6.518008368549342
Root relative squared error: 28.424202717572626
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9130003602244805
Weighted FMeasure: 0.9629857899551657
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9616490426027502
Mean absolute error: 0.01810557880152577
Coverage of cases: 97.7728285077951
Instances selection time: 24.0
Test time: 26.0
Accumulative iteration time: 135.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.018160675748565766
Kappa statistic: 0.9088792856460701
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 18.485523385300667
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9688018135308823
Weighted AreaUnderROC: 0.9809481389462344
Root mean squared error: 0.10644404023128036
Relative absolute error: 6.383804715971386
Root relative squared error: 28.56193317082398
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9130003602244805
Weighted FMeasure: 0.9629857899551657
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9674636493683817
Mean absolute error: 0.017732790877698117
Coverage of cases: 97.32739420935413
Instances selection time: 10.0
Test time: 26.0
Accumulative iteration time: 146.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.018160675748565766
Kappa statistic: 0.9088792856460701
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.819599109131406
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.960214381536618
Weighted AreaUnderROC: 0.9776695828091453
Root mean squared error: 0.12235055948253194
Relative absolute error: 8.215409792264573
Root relative squared error: 32.83010017056891
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8800189516489896
Weighted FMeasure: 0.9491845189473906
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9608431514176158
Mean absolute error: 0.022820582756290255
Coverage of cases: 97.32739420935413
Instances selection time: 9.0
Test time: 28.0
Accumulative iteration time: 156.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.02007047530313146
Kappa statistic: 0.8744334005290203
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 19.042316258351942
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9634003096293788
Weighted AreaUnderROC: 0.9784687815116286
Root mean squared error: 0.11274258172554102
Relative absolute error: 7.376457307414691
Root relative squared error: 30.25200920365638
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8955886210122438
Weighted FMeasure: 0.9571762088535891
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9668489606802304
Mean absolute error: 0.020490159187262828
Coverage of cases: 97.55011135857461
Instances selection time: 9.0
Test time: 25.0
Accumulative iteration time: 165.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.018641165933955723
Kappa statistic: 0.8944063591359666
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.856718634001506
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9587517245888505
Weighted AreaUnderROC: 0.9768465873156464
Root mean squared error: 0.11874343063370507
Relative absolute error: 7.822346013170822
Root relative squared error: 31.862205933419613
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8814541458128342
Weighted FMeasure: 0.9509983679183288
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9637195417984477
Mean absolute error: 0.02172873892547429
Coverage of cases: 97.32739420935413
Instances selection time: 7.0
Test time: 26.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.019121656119345687
Kappa statistic: 0.8798906722493604
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.89383815887159
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9572846647653407
Weighted AreaUnderROC: 0.9761546949952129
Root mean squared error: 0.12522442607778841
Relative absolute error: 8.601895757704257
Root relative squared error: 33.60123949840004
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8768826941640373
Weighted FMeasure: 0.9489675799135419
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9620134834761304
Mean absolute error: 0.02389415488251159
Coverage of cases: 97.32739420935413
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 181.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.01922548695814312
Kappa statistic: 0.875136149799541
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.93095768374167
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9546854660648554
Weighted AreaUnderROC: 0.9756987215027408
Root mean squared error: 0.12648866000709535
Relative absolute error: 8.487118167812481
Root relative squared error: 33.940469059047075
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8679968821017976
Weighted FMeasure: 0.9450058457847412
Iteration time: 7.0
Weighted AreaUnderPRC: 0.961240834451413
Mean absolute error: 0.023575328243923326
Coverage of cases: 97.32739420935413
Instances selection time: 6.0
Test time: 26.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.01943314863573798
Kappa statistic: 0.8657327536631934
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.89383815887158
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9573118961130969
Weighted AreaUnderROC: 0.974411082277837
Root mean squared error: 0.13010155162699627
Relative absolute error: 8.603975005139294
Root relative squared error: 34.90990960993962
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.874569940923411
Weighted FMeasure: 0.9491441606456814
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9624201826951494
Mean absolute error: 0.023899930569831133
Coverage of cases: 96.65924276169265
Instances selection time: 5.0
Test time: 26.0
Accumulative iteration time: 194.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.018887491620147783
Kappa statistic: 0.875706475351219
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.04231625835189
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9522148657943792
Weighted AreaUnderROC: 0.9714709334057627
Root mean squared error: 0.139321960132611
Relative absolute error: 9.62242248503892
Root relative squared error: 37.38400483380424
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8613143231296819
Weighted FMeasure: 0.9404974791417902
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9580728034458528
Mean absolute error: 0.026728951347330065
Coverage of cases: 96.21380846325167
Instances selection time: 3.0
Test time: 30.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.01997880565132818
Kappa statistic: 0.8558157671399408
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 19.26503340757242
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9522148657943792
Weighted AreaUnderROC: 0.9740846594690548
Root mean squared error: 0.13672320869007668
Relative absolute error: 9.61455647338649
Root relative squared error: 36.68668664794835
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8613143231296819
Weighted FMeasure: 0.9404974791417902
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9579488289602015
Mean absolute error: 0.026707101314962203
Coverage of cases: 96.65924276169265
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 201.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.01997880565132818
Kappa statistic: 0.8558157671399408
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.116555308092096
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9560235284550188
Weighted AreaUnderROC: 0.9745849263967928
Root mean squared error: 0.1306445048608607
Relative absolute error: 9.245601992106055
Root relative squared error: 35.055599250682526
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8723556723472718
Weighted FMeasure: 0.9469649972120394
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9589391535975895
Mean absolute error: 0.025682227755849895
Coverage of cases: 97.10467706013362
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 204.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.01949831546593822
Kappa statistic: 0.8703735781511635
Training time: 1.0
		
Time end:Sat Oct 07 11.21.08 EEST 2017