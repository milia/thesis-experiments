Sat Oct 07 11.23.18 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.18 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.33333333333331
Incorrectly Classified Instances: 11.804008908685969
Correctly Classified Instances: 88.19599109131403
Weighted Precision: 0.8952753359935923
Weighted AreaUnderROC: 0.839998427286268
Root mean squared error: 0.19343127584672906
Relative absolute error: 19.585980541554566
Root relative squared error: 51.90305781213618
Weighted TruePositiveRate: 0.8819599109131403
Weighted MatthewsCorrelation: 0.7030973560717216
Weighted FMeasure: 0.8848644943456664
Iteration time: 13.0
Weighted AreaUnderPRC: 0.840444525212811
Mean absolute error: 0.054405501504317694
Coverage of cases: 91.0913140311804
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8819599109131403
Weighted FalsePositiveRate: 0.20196305634060469
Kappa statistic: 0.6978580769670267
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.8720492631803802
Weighted AreaUnderROC: 0.7977110391519828
Root mean squared error: 0.1994564321394988
Relative absolute error: 19.403118040089222
Root relative squared error: 53.51977689761944
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.6629668878321109
Weighted FMeasure: 0.8653952243608292
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8065787273729635
Mean absolute error: 0.05389755011135842
Coverage of cases: 87.52783964365256
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.27985631813256023
Kappa statistic: 0.654244932757625
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 11.58129175946548
Correctly Classified Instances: 88.41870824053453
Weighted Precision: 0.8804007198108784
Weighted AreaUnderROC: 0.8025940331538413
Root mean squared error: 0.1927768330480956
Relative absolute error: 17.724325661965025
Root relative squared error: 51.72745238192063
Weighted TruePositiveRate: 0.8841870824053452
Weighted MatthewsCorrelation: 0.6871873867498057
Weighted FMeasure: 0.8732222259013822
Iteration time: 14.0
Weighted AreaUnderPRC: 0.814143488174399
Mean absolute error: 0.04923423794990236
Coverage of cases: 88.41870824053453
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 40.0
Weighted Recall: 0.8841870824053452
Weighted FalsePositiveRate: 0.27899901609766264
Kappa statistic: 0.6734910779213513
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 11.35857461024499
Correctly Classified Instances: 88.64142538975501
Weighted Precision: 0.8832560049085303
Weighted AreaUnderROC: 0.8038471659859217
Root mean squared error: 0.1913227884555167
Relative absolute error: 16.973633163302114
Root relative squared error: 51.337291275761935
Weighted TruePositiveRate: 0.8864142538975501
Weighted MatthewsCorrelation: 0.6937968450262473
Weighted FMeasure: 0.8753879282494684
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8166043117024321
Mean absolute error: 0.04714898100917207
Coverage of cases: 88.64142538975501
Instances selection time: 14.0
Test time: 22.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8864142538975501
Weighted FalsePositiveRate: 0.27871992192570716
Kappa statistic: 0.6784570882947653
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.8949609626704835
Weighted AreaUnderROC: 0.7953951606686684
Root mean squared error: 0.1840075280880984
Relative absolute error: 15.551644481276357
Root relative squared error: 49.374400941203035
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7011964008733161
Weighted FMeasure: 0.8797490762345294
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8170104555638789
Mean absolute error: 0.04319901244798945
Coverage of cases: 89.53229398663697
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.3067828234154334
Kappa statistic: 0.6921966161026837
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.8924496524398575
Weighted AreaUnderROC: 0.793625388063511
Root mean squared error: 0.1861941494847383
Relative absolute error: 15.487097395922707
Root relative squared error: 49.96113303127613
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.6943840313526806
Weighted FMeasure: 0.877174569066982
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8146121597791048
Mean absolute error: 0.043019714988673755
Coverage of cases: 89.30957683741649
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.3139014463157519
Kappa statistic: 0.684090176189499
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.8924496524398575
Weighted AreaUnderROC: 0.793625388063511
Root mean squared error: 0.18640600780366418
Relative absolute error: 15.239754669095632
Root relative squared error: 50.01798058360217
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.6943840313526806
Weighted FMeasure: 0.877174569066982
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8146121597791048
Mean absolute error: 0.04233265185859856
Coverage of cases: 89.30957683741649
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.3139014463157519
Kappa statistic: 0.684090176189499
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.8924496524398575
Weighted AreaUnderROC: 0.7930499270586102
Root mean squared error: 0.18658660134696264
Relative absolute error: 15.037324322248832
Root relative squared error: 50.06643891629572
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.6943840313526806
Weighted FMeasure: 0.877174569066982
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8143397710539002
Mean absolute error: 0.04177034533957968
Coverage of cases: 89.30957683741649
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.3139014463157519
Kappa statistic: 0.684090176189499
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.913140311804009
Correctly Classified Instances: 89.08685968819599
Weighted Precision: 0.89053968840819
Weighted AreaUnderROC: 0.7890448788632367
Root mean squared error: 0.18867221149646338
Relative absolute error: 15.122450762042158
Root relative squared error: 50.62606684455738
Weighted TruePositiveRate: 0.89086859688196
Weighted MatthewsCorrelation: 0.687659267206691
Weighted FMeasure: 0.8747148977791787
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8109398574449193
Mean absolute error: 0.04200680767233891
Coverage of cases: 89.08685968819599
Instances selection time: 14.0
Test time: 30.0
Accumulative iteration time: 124.0
Weighted Recall: 0.89086859688196
Weighted FalsePositiveRate: 0.32102006921607035
Kappa statistic: 0.6761224790225233
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.8968655694713601
Weighted AreaUnderROC: 0.7915511445273973
Root mean squared error: 0.18492272445043625
Relative absolute error: 14.45218191346202
Root relative squared error: 49.61997389467679
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7026690955709408
Weighted FMeasure: 0.87907083475097
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8161505769214151
Mean absolute error: 0.04014494975961632
Coverage of cases: 89.53229398663697
Instances selection time: 13.0
Test time: 33.0
Accumulative iteration time: 137.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.3204618808721594
Kappa statistic: 0.6866480563062394
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.8968655694713601
Weighted AreaUnderROC: 0.7903738259473136
Root mean squared error: 0.18503960818133708
Relative absolute error: 14.3284542747267
Root relative squared error: 49.65133709080776
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7026690955709408
Weighted FMeasure: 0.87907083475097
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8155971642102663
Mean absolute error: 0.03980126187424044
Coverage of cases: 89.53229398663697
Instances selection time: 13.0
Test time: 35.0
Accumulative iteration time: 150.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.3204618808721594
Kappa statistic: 0.6866480563062394
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.9082320720388044
Weighted AreaUnderROC: 0.7915393271128148
Root mean squared error: 0.18316511616051334
Relative absolute error: 13.954073765877913
Root relative squared error: 49.14835810098661
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7113929431379141
Weighted FMeasure: 0.8806689248621775
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8177645741201807
Mean absolute error: 0.03876131601632715
Coverage of cases: 89.75501113585746
Instances selection time: 11.0
Test time: 36.0
Accumulative iteration time: 161.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.32035805003336193
Kappa statistic: 0.6918417283361186
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.9114310871704563
Weighted AreaUnderROC: 0.7926629226184329
Root mean squared error: 0.18125510007335732
Relative absolute error: 13.59248448769663
Root relative squared error: 48.63584700390673
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7189389368303591
Weighted FMeasure: 0.8826282375119134
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8197465606695419
Mean absolute error: 0.037756901354712484
Coverage of cases: 89.97772828507794
Instances selection time: 10.0
Test time: 38.0
Accumulative iteration time: 171.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.3203380305143309
Kappa statistic: 0.697012866268782
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.9114310871704563
Weighted AreaUnderROC: 0.7926629226184329
Root mean squared error: 0.18133472518181426
Relative absolute error: 13.504267913616694
Root relative squared error: 48.657212662533794
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7189389368303591
Weighted FMeasure: 0.8826282375119134
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8197465606695419
Mean absolute error: 0.037511855315601556
Coverage of cases: 89.97772828507794
Instances selection time: 9.0
Test time: 40.0
Accumulative iteration time: 180.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.3203380305143309
Kappa statistic: 0.697012866268782
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.799554565701559
Correctly Classified Instances: 90.20044543429844
Weighted Precision: 0.9131749310500017
Weighted AreaUnderROC: 0.7972703986535233
Root mean squared error: 0.179381713610485
Relative absolute error: 13.162477189453416
Root relative squared error: 48.13316466641348
Weighted TruePositiveRate: 0.9020044543429844
Weighted MatthewsCorrelation: 0.7254576871978685
Weighted FMeasure: 0.8852431924653983
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8235265482314191
Mean absolute error: 0.03656243663737024
Coverage of cases: 90.20044543429844
Instances selection time: 8.0
Test time: 44.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9020044543429844
Weighted FalsePositiveRate: 0.31321940761401246
Kappa statistic: 0.7051123218150608
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9348284363350524
Weighted AreaUnderROC: 0.852560983319277
Root mean squared error: 0.15306017647400746
Relative absolute error: 9.93302404551241
Root relative squared error: 41.070355109279575
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8008100156856055
Weighted FMeasure: 0.9136336562318063
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8697486119207593
Mean absolute error: 0.02759173345975642
Coverage of cases: 92.87305122494432
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 194.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.22779593281019084
Kappa statistic: 0.7968670474467003
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9500529409617
Weighted AreaUnderROC: 0.8894162294837274
Root mean squared error: 0.1326218755720027
Relative absolute error: 7.759309428342024
Root relative squared error: 35.58618348990219
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8502939303652617
Weighted FMeasure: 0.9328758419426149
Iteration time: 4.0
Weighted AreaUnderPRC: 0.90056144720426
Mean absolute error: 0.021553637300949852
Coverage of cases: 94.65478841870824
Instances selection time: 4.0
Test time: 43.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.17084694960764316
Kappa statistic: 0.8525209393989159
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9534934394828696
Weighted AreaUnderROC: 0.9019838139633162
Root mean squared error: 0.12701778511146694
Relative absolute error: 7.170316753278969
Root relative squared error: 34.08244822328426
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8619178688826348
Weighted FMeasure: 0.9374557826422908
Iteration time: 2.0
Weighted AreaUnderPRC: 0.909584467459015
Mean absolute error: 0.019917546536885825
Coverage of cases: 95.10022271714922
Instances selection time: 2.0
Test time: 45.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14977017507864326
Kappa statistic: 0.8669378738078559
Training time: 0.0
		
Time end:Sat Oct 07 11.23.20 EEST 2017