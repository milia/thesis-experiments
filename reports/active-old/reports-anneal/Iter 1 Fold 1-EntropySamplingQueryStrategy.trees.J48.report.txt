Sat Oct 07 11.20.25 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.25 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 30.920564216777848
Incorrectly Classified Instances: 14.922048997772828
Correctly Classified Instances: 85.07795100222717
Weighted Precision: 0.8217718265528939
Weighted AreaUnderROC: 0.6856160593952912
Root mean squared error: 0.21602134421699776
Relative absolute error: 22.11711478402354
Root relative squared error: 57.96460923121086
Weighted TruePositiveRate: 0.8507795100222717
Weighted MatthewsCorrelation: 0.5389469816601372
Weighted FMeasure: 0.7945866002495376
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7370919387174532
Mean absolute error: 0.06143642995562033
Coverage of cases: 94.65478841870824
Instances selection time: 18.0
Test time: 9.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8507795100222717
Weighted FalsePositiveRate: 0.47694773432133714
Kappa statistic: 0.5009787008161369
Training time: 72.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8883164322765397
Weighted AreaUnderROC: 0.8961929227939363
Root mean squared error: 0.14328333654065986
Relative absolute error: 7.913237145347226
Root relative squared error: 38.44695366574759
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.818918400102545
Weighted FMeasure: 0.9120982412836717
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8918801086868645
Mean absolute error: 0.021981214292630967
Coverage of cases: 93.76391982182628
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 108.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.15144474011037626
Kappa statistic: 0.8321136691416057
Training time: 14.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 17.037861915367472
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8878001406394767
Weighted AreaUnderROC: 0.8972113443146359
Root mean squared error: 0.13970005636864274
Relative absolute error: 7.438752783964438
Root relative squared error: 37.48545870010069
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8074010747456498
Weighted FMeasure: 0.9119702680393105
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9005316831815904
Mean absolute error: 0.02066320217767879
Coverage of cases: 94.65478841870824
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 136.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.17196332629546515
Kappa statistic: 0.8301264728137498
Training time: 21.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 163.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 20.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 189.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 21.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 213.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 17.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 10.0
Test time: 2.0
Accumulative iteration time: 237.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 14.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 9.0
Test time: 2.0
Accumulative iteration time: 261.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 15.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 277.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 11.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9026519493667163
Weighted AreaUnderROC: 0.8976272404675907
Root mean squared error: 0.1358299622971603
Relative absolute error: 6.800296956199026
Root relative squared error: 36.44700348932225
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8298633347992942
Weighted FMeasure: 0.9227363761230429
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9023589404076564
Mean absolute error: 0.01888971376721933
Coverage of cases: 94.65478841870824
Instances selection time: 10.0
Test time: 2.0
Accumulative iteration time: 301.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15060745759450977
Kappa statistic: 0.8503772226813467
Training time: 14.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.8942804824281971
Weighted AreaUnderROC: 0.8905697902150777
Root mean squared error: 0.13631638203069202
Relative absolute error: 7.734692662244161
Root relative squared error: 36.57752360009516
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8266817437889709
Weighted FMeasure: 0.918511354794404
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8969524386147321
Mean absolute error: 0.021485257395122453
Coverage of cases: 94.43207126948775
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 325.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.17112604377959864
Kappa statistic: 0.8465104195153967
Training time: 22.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.485523385300596
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.8733793969876069
Weighted AreaUnderROC: 0.9024548311590256
Root mean squared error: 0.1545738523489363
Relative absolute error: 11.262213742317373
Root relative squared error: 41.476516967548655
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8171804000208798
Weighted FMeasure: 0.8956098637613115
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8876428856952473
Mean absolute error: 0.03128392706199239
Coverage of cases: 94.87750556792874
Instances selection time: 10.0
Test time: 2.0
Accumulative iteration time: 361.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.1461491925444448
Kappa statistic: 0.7882018383244833
Training time: 26.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 17.03786191536747
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.906954215812254
Weighted AreaUnderROC: 0.9032691677774153
Root mean squared error: 0.13182223248736066
Relative absolute error: 7.017948382025485
Root relative squared error: 35.37161673450253
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8500353389600702
Weighted FMeasure: 0.9271630804101828
Iteration time: 40.0
Weighted AreaUnderPRC: 0.9048940717541638
Mean absolute error: 0.019494301061181708
Coverage of cases: 94.87750556792874
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 401.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.14320974052223578
Kappa statistic: 0.8612894560107455
Training time: 34.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.03786191536747
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.906954215812254
Weighted AreaUnderROC: 0.9032691677774153
Root mean squared error: 0.13291588795903372
Relative absolute error: 7.178304729464238
Root relative squared error: 35.6650752919376
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8500353389600702
Weighted FMeasure: 0.9271630804101828
Iteration time: 44.0
Weighted AreaUnderPRC: 0.9048940717541638
Mean absolute error: 0.019939735359622686
Coverage of cases: 94.87750556792874
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 445.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.14320974052223578
Kappa statistic: 0.8612894560107455
Training time: 36.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.926503340757215
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9037495616844878
Weighted AreaUnderROC: 0.900086469679898
Root mean squared error: 0.13587426534295816
Relative absolute error: 7.267391589152437
Root relative squared error: 36.45889124396396
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8380651361957472
Weighted FMeasure: 0.9231432664236972
Iteration time: 44.0
Weighted AreaUnderPRC: 0.9021098278735473
Mean absolute error: 0.02018719885875657
Coverage of cases: 94.43207126948775
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 489.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1432497795602979
Kappa statistic: 0.8505843516226073
Training time: 37.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.926503340757215
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9037495616844878
Weighted AreaUnderROC: 0.900086469679898
Root mean squared error: 0.13587426534295816
Relative absolute error: 7.267391589152437
Root relative squared error: 36.45889124396396
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8380651361957472
Weighted FMeasure: 0.9231432664236972
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9021098278735473
Mean absolute error: 0.02018719885875657
Coverage of cases: 94.43207126948775
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 532.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1432497795602979
Kappa statistic: 0.8505843516226073
Training time: 38.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.926503340757215
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9037495616844878
Weighted AreaUnderROC: 0.900086469679898
Root mean squared error: 0.13587426534295816
Relative absolute error: 7.267391589152437
Root relative squared error: 36.45889124396396
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8380651361957472
Weighted FMeasure: 0.9231432664236972
Iteration time: 40.0
Weighted AreaUnderPRC: 0.9021098278735473
Mean absolute error: 0.02018719885875657
Coverage of cases: 94.43207126948775
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 572.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1432497795602979
Kappa statistic: 0.8505843516226073
Training time: 40.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.933184855233925
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.890160668347167
Weighted AreaUnderROC: 0.8938865800077548
Root mean squared error: 0.1408392969666887
Relative absolute error: 8.967378488143677
Root relative squared error: 37.79114903049569
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.821879603482119
Weighted FMeasure: 0.914073690708299
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8928629884894737
Mean absolute error: 0.024909384689287745
Coverage of cases: 94.87750556792874
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 619.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17168423212350967
Kappa statistic: 0.8341541492243288
Training time: 43.0
		
Time end:Sat Oct 07 11.20.27 EEST 2017