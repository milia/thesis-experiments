Sat Oct 07 11.23.39 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.39 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.98069784706754
Incorrectly Classified Instances: 9.33184855233853
Correctly Classified Instances: 90.66815144766147
Weighted Precision: 0.9012953837857106
Weighted AreaUnderROC: 0.9616427903285569
Root mean squared error: 0.15107489453653458
Relative absolute error: 19.478178267476302
Root relative squared error: 40.53764806527654
Weighted TruePositiveRate: 0.9066815144766147
Weighted MatthewsCorrelation: 0.7358025001231868
Weighted FMeasure: 0.894276005035241
Iteration time: 5.8
Weighted AreaUnderPRC: 0.9589494211914978
Mean absolute error: 0.05410605074298919
Coverage of cases: 98.41870824053451
Instances selection time: 2.0
Test time: 3.3
Accumulative iteration time: 5.8
Weighted Recall: 0.9066815144766147
Weighted FalsePositiveRate: 0.25198903001872985
Kappa statistic: 0.7293278070738262
Training time: 3.8
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 26.967334818114317
Incorrectly Classified Instances: 4.120267260579064
Correctly Classified Instances: 95.87973273942092
Weighted Precision: 0.9533047666120487
Weighted AreaUnderROC: 0.9746250842649212
Root mean squared error: 0.12445737510133852
Relative absolute error: 15.194134468279426
Root relative squared error: 33.39541812333409
Weighted TruePositiveRate: 0.9587973273942092
Weighted MatthewsCorrelation: 0.8864237218106137
Weighted FMeasure: 0.9545780169474625
Iteration time: 6.5
Weighted AreaUnderPRC: 0.9716273086933895
Mean absolute error: 0.04220592907855354
Coverage of cases: 98.66369710467707
Instances selection time: 2.0
Test time: 3.5
Accumulative iteration time: 12.3
Weighted Recall: 0.9587973273942092
Weighted FalsePositiveRate: 0.09220374922061299
Kappa statistic: 0.8924363555853396
Training time: 4.5
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 27.04528582034149
Incorrectly Classified Instances: 3.1625835189309575
Correctly Classified Instances: 96.83741648106904
Weighted Precision: 0.9633723485530952
Weighted AreaUnderROC: 0.9778127924944405
Root mean squared error: 0.1095898757923717
Relative absolute error: 13.515435711096657
Root relative squared error: 29.406049430100353
Weighted TruePositiveRate: 0.9683741648106905
Weighted MatthewsCorrelation: 0.9127349609547251
Weighted FMeasure: 0.9640992524499845
Iteration time: 7.3
Weighted AreaUnderPRC: 0.9774714075785775
Mean absolute error: 0.03754287697526812
Coverage of cases: 98.97550111358574
Instances selection time: 1.5
Test time: 3.4
Accumulative iteration time: 19.6
Weighted Recall: 0.9683741648106905
Weighted FalsePositiveRate: 0.07010138241504069
Kappa statistic: 0.9181148147233902
Training time: 5.8
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 26.113585746102427
Incorrectly Classified Instances: 2.182628062360802
Correctly Classified Instances: 97.8173719376392
Weighted Precision: 0.9733549461509469
Weighted AreaUnderROC: 0.9797695068309318
Root mean squared error: 0.102505655512658
Relative absolute error: 12.30833163966147
Root relative squared error: 27.50515365653765
Weighted TruePositiveRate: 0.978173719376392
Weighted MatthewsCorrelation: 0.939223391612012
Weighted FMeasure: 0.9750482479782161
Iteration time: 7.7
Weighted AreaUnderPRC: 0.9808742815040915
Mean absolute error: 0.0341898101101704
Coverage of cases: 99.13140311804008
Instances selection time: 1.9
Test time: 3.1
Accumulative iteration time: 27.3
Weighted Recall: 0.978173719376392
Weighted FalsePositiveRate: 0.054620390757150916
Kappa statistic: 0.9435146962769236
Training time: 5.8
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 26.1915367483296
Incorrectly Classified Instances: 2.026726057906459
Correctly Classified Instances: 97.97327394209354
Weighted Precision: 0.9758307375718314
Weighted AreaUnderROC: 0.9796245989577802
Root mean squared error: 0.10076543546760723
Relative absolute error: 12.115933527388131
Root relative squared error: 27.038203618552707
Weighted TruePositiveRate: 0.9797327394209354
Weighted MatthewsCorrelation: 0.9438330865965977
Weighted FMeasure: 0.9768836399387404
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9811965520248135
Mean absolute error: 0.03365537090941115
Coverage of cases: 99.13140311804008
Instances selection time: 2.7
Test time: 3.3
Accumulative iteration time: 37.3
Weighted Recall: 0.9797327394209354
Weighted FalsePositiveRate: 0.050983738972220416
Kappa statistic: 0.9477131971338786
Training time: 7.3
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 24.38381588715665
Incorrectly Classified Instances: 1.915367483296214
Correctly Classified Instances: 98.08463251670379
Weighted Precision: 0.9767600550448862
Weighted AreaUnderROC: 0.982880201764939
Root mean squared error: 0.09010635938540264
Relative absolute error: 9.674611385865738
Root relative squared error: 24.178073374894492
Weighted TruePositiveRate: 0.9808463251670378
Weighted MatthewsCorrelation: 0.9467597759365477
Weighted FMeasure: 0.9780578384486198
Iteration time: 10.2
Weighted AreaUnderPRC: 0.9833340864068386
Mean absolute error: 0.026873920516293455
Coverage of cases: 99.13140311804008
Instances selection time: 1.4
Test time: 3.9
Accumulative iteration time: 47.5
Weighted Recall: 0.9808463251670378
Weighted FalsePositiveRate: 0.0460709152776242
Kappa statistic: 0.9507168403022869
Training time: 8.8
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 24.112843355605044
Incorrectly Classified Instances: 2.115812917594655
Correctly Classified Instances: 97.88418708240533
Weighted Precision: 0.9740381738064887
Weighted AreaUnderROC: 0.9828712867101158
Root mean squared error: 0.08943227763346884
Relative absolute error: 9.34354054515959
Root relative squared error: 23.99719826052856
Weighted TruePositiveRate: 0.9788418708240536
Weighted MatthewsCorrelation: 0.9412854002112541
Weighted FMeasure: 0.9757626841798931
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9833330036010353
Mean absolute error: 0.025954279292109715
Coverage of cases: 99.13140311804008
Instances selection time: 1.7
Test time: 3.4
Accumulative iteration time: 57.5
Weighted Recall: 0.9788418708240536
Weighted FalsePositiveRate: 0.04634487466559639
Kappa statistic: 0.9458162804090586
Training time: 8.3
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 23.054936896807714
Incorrectly Classified Instances: 1.4031180400890868
Correctly Classified Instances: 98.59688195991092
Weighted Precision: 0.9820397085009006
Weighted AreaUnderROC: 0.9863376033126231
Root mean squared error: 0.08124117408692559
Relative absolute error: 8.090952783416345
Root relative squared error: 21.799294539631312
Weighted TruePositiveRate: 0.9859688195991092
Weighted MatthewsCorrelation: 0.9606944780707366
Weighted FMeasure: 0.9835379862897
Iteration time: 10.2
Weighted AreaUnderPRC: 0.986711018510476
Mean absolute error: 0.022474868842822956
Coverage of cases: 99.28730512249444
Instances selection time: 1.5
Test time: 3.4
Accumulative iteration time: 67.7
Weighted Recall: 0.9859688195991092
Weighted FalsePositiveRate: 0.031016053121227554
Kappa statistic: 0.9639348766268597
Training time: 8.7
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 22.73570898292501
Incorrectly Classified Instances: 1.8708240534521157
Correctly Classified Instances: 98.12917594654789
Weighted Precision: 0.97645574317397
Weighted AreaUnderROC: 0.985111330195237
Root mean squared error: 0.0846280722579686
Relative absolute error: 8.055857791168847
Root relative squared error: 22.708094684829938
Weighted TruePositiveRate: 0.981291759465479
Weighted MatthewsCorrelation: 0.9476996823154502
Weighted FMeasure: 0.9782756722609742
Iteration time: 10.6
Weighted AreaUnderPRC: 0.9856254796071022
Mean absolute error: 0.02237738275324657
Coverage of cases: 99.30957683741647
Instances selection time: 1.8
Test time: 3.4
Accumulative iteration time: 78.3
Weighted Recall: 0.981291759465479
Weighted FalsePositiveRate: 0.04194392560718278
Kappa statistic: 0.9519797367235862
Training time: 8.8
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.639198218262802
Incorrectly Classified Instances: 1.469933184855234
Correctly Classified Instances: 98.53006681514476
Weighted Precision: 0.9857188045161684
Weighted AreaUnderROC: 0.9951106684848063
Root mean squared error: 0.07839045860138752
Relative absolute error: 7.4915503621138795
Root relative squared error: 21.034367306410378
Weighted TruePositiveRate: 0.9853006681514478
Weighted MatthewsCorrelation: 0.9597360923040356
Weighted FMeasure: 0.9832224492231546
Iteration time: 10.5
Weighted AreaUnderPRC: 0.9938691190437823
Mean absolute error: 0.02080986211698279
Coverage of cases: 99.73273942093542
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 88.8
Weighted Recall: 0.9853006681514478
Weighted FalsePositiveRate: 0.03107387390752177
Kappa statistic: 0.9623606819118106
Training time: 9.5
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 21.963622865627322
Incorrectly Classified Instances: 1.135857461024499
Correctly Classified Instances: 98.86414253897551
Weighted Precision: 0.9887496546674799
Weighted AreaUnderROC: 0.9953186507906666
Root mean squared error: 0.07284276808079906
Relative absolute error: 6.577825268288285
Root relative squared error: 19.54576573175033
Weighted TruePositiveRate: 0.9886414253897551
Weighted MatthewsCorrelation: 0.9699003144282317
Weighted FMeasure: 0.9882979246229601
Iteration time: 11.1
Weighted AreaUnderPRC: 0.9945076947595848
Mean absolute error: 0.01827173685635617
Coverage of cases: 99.66592427616926
Instances selection time: 1.1
Test time: 3.2
Accumulative iteration time: 99.9
Weighted Recall: 0.9886414253897551
Weighted FalsePositiveRate: 0.023185934741691807
Kappa statistic: 0.9714536251591092
Training time: 10.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 21.64810690423162
Incorrectly Classified Instances: 1.291759465478842
Correctly Classified Instances: 98.70824053452117
Weighted Precision: 0.9862498148445044
Weighted AreaUnderROC: 0.994063920286966
Root mean squared error: 0.07194639435700376
Relative absolute error: 6.253866021282338
Root relative squared error: 19.305243422192216
Weighted TruePositiveRate: 0.9870824053452116
Weighted MatthewsCorrelation: 0.9653798497209977
Weighted FMeasure: 0.9863713027283373
Iteration time: 11.1
Weighted AreaUnderPRC: 0.9934693267291432
Mean absolute error: 0.01737185005911743
Coverage of cases: 99.66592427616926
Instances selection time: 0.9
Test time: 3.1
Accumulative iteration time: 111.0
Weighted Recall: 0.9870824053452116
Weighted FalsePositiveRate: 0.02744571677048103
Kappa statistic: 0.9674110814730181
Training time: 10.2
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.729769858945804
Incorrectly Classified Instances: 1.2026726057906458
Correctly Classified Instances: 98.79732739420936
Weighted Precision: 0.9881090593180945
Weighted AreaUnderROC: 0.9951787967220846
Root mean squared error: 0.0715983804284345
Relative absolute error: 6.332302993320588
Root relative squared error: 19.211861486024503
Weighted TruePositiveRate: 0.9879732739420936
Weighted MatthewsCorrelation: 0.9681414342573286
Weighted FMeasure: 0.9876691182656414
Iteration time: 11.8
Weighted AreaUnderPRC: 0.9942036082394325
Mean absolute error: 0.017589730537001462
Coverage of cases: 99.77728285077953
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 122.8
Weighted Recall: 0.9879732739420936
Weighted FalsePositiveRate: 0.0232671567276143
Kappa statistic: 0.9697671410783972
Training time: 10.8
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 21.59242761692649
Incorrectly Classified Instances: 1.0690423162583518
Correctly Classified Instances: 98.93095768374164
Weighted Precision: 0.9894356092921477
Weighted AreaUnderROC: 0.9969786647523027
Root mean squared error: 0.07144738475777515
Relative absolute error: 6.352657207907286
Root relative squared error: 19.171345095956205
Weighted TruePositiveRate: 0.9893095768374165
Weighted MatthewsCorrelation: 0.9716720815816169
Weighted FMeasure: 0.9889199244921754
Iteration time: 14.4
Weighted AreaUnderPRC: 0.9951909464047232
Mean absolute error: 0.017646270021964505
Coverage of cases: 99.75501113585746
Instances selection time: 0.6
Test time: 3.4
Accumulative iteration time: 137.2
Weighted Recall: 0.9893095768374165
Weighted FalsePositiveRate: 0.021073886574171048
Kappa statistic: 0.9731312022662053
Training time: 13.8
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 21.718634001484777
Incorrectly Classified Instances: 1.135857461024499
Correctly Classified Instances: 98.86414253897551
Weighted Precision: 0.9878568326136827
Weighted AreaUnderROC: 0.9937417345162549
Root mean squared error: 0.07375139165371275
Relative absolute error: 6.415331974448063
Root relative squared error: 19.789575020749577
Weighted TruePositiveRate: 0.9886414253897551
Weighted MatthewsCorrelation: 0.9694067343492747
Weighted FMeasure: 0.9878426613076039
Iteration time: 15.0
Weighted AreaUnderPRC: 0.99236623126075
Mean absolute error: 0.017820366595688888
Coverage of cases: 99.62138084632517
Instances selection time: 1.2
Test time: 3.3
Accumulative iteration time: 152.2
Weighted Recall: 0.9886414253897551
Weighted FalsePositiveRate: 0.02591211196822828
Kappa statistic: 0.9712494849371268
Training time: 13.8
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 21.484780994803256
Incorrectly Classified Instances: 1.5144766146993318
Correctly Classified Instances: 98.48552338530067
Weighted Precision: 0.9841170607210271
Weighted AreaUnderROC: 0.9941642334600888
Root mean squared error: 0.07580306092678141
Relative absolute error: 6.546824949721712
Root relative squared error: 20.340095656181077
Weighted TruePositiveRate: 0.9848552338530068
Weighted MatthewsCorrelation: 0.9594482303441554
Weighted FMeasure: 0.9841345114703246
Iteration time: 13.6
Weighted AreaUnderPRC: 0.9932323209049272
Mean absolute error: 0.018185624860337903
Coverage of cases: 99.71046770601338
Instances selection time: 0.5
Test time: 3.1
Accumulative iteration time: 165.8
Weighted Recall: 0.9848552338530068
Weighted FalsePositiveRate: 0.028443052412541914
Kappa statistic: 0.9619421434781923
Training time: 13.1
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 21.51447661469933
Incorrectly Classified Instances: 1.469933184855234
Correctly Classified Instances: 98.53006681514476
Weighted Precision: 0.9855915082358193
Weighted AreaUnderROC: 0.9943566678865571
Root mean squared error: 0.07505480582146756
Relative absolute error: 6.404975761797497
Root relative squared error: 20.139317742581913
Weighted TruePositiveRate: 0.9853006681514478
Weighted MatthewsCorrelation: 0.9610206248952011
Weighted FMeasure: 0.9848777111709299
Iteration time: 13.3
Weighted AreaUnderPRC: 0.9929872506155023
Mean absolute error: 0.0177915993383262
Coverage of cases: 99.62138084632517
Instances selection time: 0.5
Test time: 3.0
Accumulative iteration time: 179.1
Weighted Recall: 0.9853006681514478
Weighted FalsePositiveRate: 0.03315876799046765
Kappa statistic: 0.9626950238839809
Training time: 12.8
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.262063845582766
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467707
Weighted Precision: 0.9867244441604225
Weighted AreaUnderROC: 0.9954973357331394
Root mean squared error: 0.07273638645087388
Relative absolute error: 6.0244560277437715
Root relative squared error: 19.517220545021935
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9647822492749251
Weighted FMeasure: 0.9864226791102014
Iteration time: 13.2
Weighted AreaUnderPRC: 0.9944619653763352
Mean absolute error: 0.016734600077065863
Coverage of cases: 99.59910913140311
Instances selection time: 0.6
Test time: 3.1
Accumulative iteration time: 192.3
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.024055863775398208
Kappa statistic: 0.9665000055708939
Training time: 12.6
		
Time end:Sat Oct 07 11.23.49 EEST 2017