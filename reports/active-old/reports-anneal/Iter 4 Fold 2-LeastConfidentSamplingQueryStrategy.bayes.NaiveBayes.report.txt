Sat Oct 07 11.21.14 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.14 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.529324424647328
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.8883794383773486
Weighted AreaUnderROC: 0.9477318302888142
Root mean squared error: 0.16389061662964818
Relative absolute error: 15.391838215796959
Root relative squared error: 43.97646715899031
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7199437344349178
Weighted FMeasure: 0.8868086950250028
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9395137714484475
Mean absolute error: 0.042755106154991125
Coverage of cases: 95.5456570155902
Instances selection time: 19.0
Test time: 25.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.26549478332137516
Kappa statistic: 0.7093033075299087
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 21.566443949517488
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9493080400572956
Weighted AreaUnderROC: 0.9749488834663924
Root mean squared error: 0.12307182083459921
Relative absolute error: 10.898875420812374
Root relative squared error: 33.02363490010081
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8877435246068031
Weighted FMeasure: 0.9502993455294758
Iteration time: 18.0
Weighted AreaUnderPRC: 0.968426691479965
Mean absolute error: 0.030274653946700737
Coverage of cases: 97.32739420935413
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 37.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.039113382281325576
Kappa statistic: 0.8862811312790208
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 21.900519673348242
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9527054248557066
Weighted AreaUnderROC: 0.9674609296931425
Root mean squared error: 0.13502083775594803
Relative absolute error: 10.938440260981329
Root relative squared error: 36.229892592152574
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8591043095431432
Weighted FMeasure: 0.9427347231800965
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9588517293314837
Mean absolute error: 0.030384556280503388
Coverage of cases: 96.21380846325167
Instances selection time: 17.0
Test time: 25.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.027058764543049455
Kappa statistic: 0.8604289710910785
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 21.93763919821831
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9589723099951208
Weighted AreaUnderROC: 0.9707172656968196
Root mean squared error: 0.1219519258409729
Relative absolute error: 9.656338212166052
Root relative squared error: 32.7231355400916
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8818801306646421
Weighted FMeasure: 0.9527616837407058
Iteration time: 16.0
Weighted AreaUnderPRC: 0.963876154151537
Mean absolute error: 0.026823161700460986
Coverage of cases: 96.65924276169265
Instances selection time: 16.0
Test time: 25.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.025525624335076283
Kappa statistic: 0.8843636959601044
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.63845582776547
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9634217688659301
Weighted AreaUnderROC: 0.9721058903370594
Root mean squared error: 0.11991500601758733
Relative absolute error: 8.902511001871485
Root relative squared error: 32.176572597314745
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8962861010185574
Weighted FMeasure: 0.9589557173806325
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9661506193923977
Mean absolute error: 0.024729197227420542
Coverage of cases: 96.65924276169265
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 84.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.024707138811690985
Kappa statistic: 0.8990939629068887
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.118782479584322
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9650054806571203
Weighted AreaUnderROC: 0.9756756309124376
Root mean squared error: 0.11402471310908498
Relative absolute error: 8.118877989472526
Root relative squared error: 30.596041155219197
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.9012175444554038
Weighted FMeasure: 0.9610467665465272
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9711538630723765
Mean absolute error: 0.02255243885964568
Coverage of cases: 97.32739420935413
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.02443431030389588
Kappa statistic: 0.9040723552242704
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.599109131403168
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.963648130714261
Weighted AreaUnderROC: 0.9751098722728966
Root mean squared error: 0.11650009434914037
Relative absolute error: 7.952602160773248
Root relative squared error: 31.26025564197819
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8964730225723311
Weighted FMeasure: 0.9590713194882001
Iteration time: 13.0
Weighted AreaUnderPRC: 0.970941019718664
Mean absolute error: 0.022090561557703246
Coverage of cases: 97.32739420935413
Instances selection time: 13.0
Test time: 29.0
Accumulative iteration time: 111.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.02453814114269331
Kappa statistic: 0.8991285634895297
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.636228656273275
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9650054806571203
Weighted AreaUnderROC: 0.9756354539084535
Root mean squared error: 0.11508157171567696
Relative absolute error: 8.099626417239381
Root relative squared error: 30.879626077652645
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.9012175444554038
Weighted FMeasure: 0.9610467665465272
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9725287240137996
Mean absolute error: 0.02249896227010917
Coverage of cases: 97.32739420935413
Instances selection time: 11.0
Test time: 26.0
Accumulative iteration time: 123.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.02443431030389588
Kappa statistic: 0.9040723552242704
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.26503340757245
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9628688008249208
Weighted AreaUnderROC: 0.9760949870034902
Root mean squared error: 0.11820477224463147
Relative absolute error: 7.9257029277665305
Root relative squared error: 31.71766872046532
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8921118374482797
Weighted FMeasure: 0.9573288461766456
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9729296385938258
Mean absolute error: 0.02201584146601792
Coverage of cases: 97.10467706013362
Instances selection time: 10.0
Test time: 27.0
Accumulative iteration time: 134.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.024472974312493078
Kappa statistic: 0.8942583957420753
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 19.00519673348187
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9596459004983673
Weighted AreaUnderROC: 0.9754482261641935
Root mean squared error: 0.1208326946463892
Relative absolute error: 7.878927724548335
Root relative squared error: 32.422814296056295
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.882435291932507
Weighted FMeasure: 0.9531118268904508
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9718026766797103
Mean absolute error: 0.02188591034596738
Coverage of cases: 96.88195991091314
Instances selection time: 9.0
Test time: 26.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.02501863132808328
Kappa statistic: 0.8844813472108525
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 18.819599109131453
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9596459004983673
Weighted AreaUnderROC: 0.9758973337811198
Root mean squared error: 0.12447742371767398
Relative absolute error: 8.024908674801186
Root relative squared error: 33.400797731611796
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.882435291932507
Weighted FMeasure: 0.9531118268904508
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9727860000597915
Mean absolute error: 0.022291412985558624
Coverage of cases: 96.88195991091314
Instances selection time: 8.0
Test time: 26.0
Accumulative iteration time: 153.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.02501863132808328
Kappa statistic: 0.8844813472108525
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.559762435040874
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9550626270517097
Weighted AreaUnderROC: 0.9746664689587609
Root mean squared error: 0.13106501640135126
Relative absolute error: 8.272382901347088
Root relative squared error: 35.168434337465726
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8649567350055439
Weighted FMeasure: 0.945667228210825
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9713396540662486
Mean absolute error: 0.022978841392630574
Coverage of cases: 96.65924276169265
Instances selection time: 7.0
Test time: 38.0
Accumulative iteration time: 161.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.02518114569450896
Kappa statistic: 0.8655735062123603
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.262806236080216
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9504615991605178
Weighted AreaUnderROC: 0.9733152905976122
Root mean squared error: 0.13547751987602735
Relative absolute error: 8.507843366868627
Root relative squared error: 36.352433263905304
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8512962227873252
Weighted FMeasure: 0.9393488951791908
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9696107214611766
Mean absolute error: 0.023632898241301506
Coverage of cases: 95.5456570155902
Instances selection time: 13.0
Test time: 50.0
Accumulative iteration time: 174.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.025999631217894257
Kappa statistic: 0.8515651717927089
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.114328136599884
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9527111558207381
Weighted AreaUnderROC: 0.9674328147786498
Root mean squared error: 0.14060087528373566
Relative absolute error: 8.828646186307738
Root relative squared error: 37.727173779648545
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.855911781029562
Weighted FMeasure: 0.9415044702546933
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9646263271510959
Mean absolute error: 0.024524017184187917
Coverage of cases: 94.65478841870824
Instances selection time: 11.0
Test time: 50.0
Accumulative iteration time: 185.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.025979611698863203
Kappa statistic: 0.8561292685159755
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.188567186340048
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9504615991605178
Weighted AreaUnderROC: 0.9675435459551713
Root mean squared error: 0.14178123248543337
Relative absolute error: 8.931105481931423
Root relative squared error: 38.043896852535845
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8512962227873252
Weighted FMeasure: 0.9393488951791908
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9663634735401724
Mean absolute error: 0.02480862633869815
Coverage of cases: 94.65478841870824
Instances selection time: 8.0
Test time: 48.0
Accumulative iteration time: 194.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.025999631217894257
Kappa statistic: 0.8515651717927089
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.22568671121013
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9504615991605178
Weighted AreaUnderROC: 0.9692779179704897
Root mean squared error: 0.14287131326025826
Relative absolute error: 9.116140016246126
Root relative squared error: 38.336396218152736
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8512962227873252
Weighted FMeasure: 0.9393488951791908
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9663316843373231
Mean absolute error: 0.025322611156238985
Coverage of cases: 94.65478841870824
Instances selection time: 4.0
Test time: 49.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.025999631217894257
Kappa statistic: 0.8515651717927089
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 18.856718634001545
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9490251549734515
Weighted AreaUnderROC: 0.9731082968369925
Root mean squared error: 0.140703963907001
Relative absolute error: 9.518622743177099
Root relative squared error: 37.75483535996792
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8468574064981126
Weighted FMeasure: 0.9372682712129851
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9669339263259373
Mean absolute error: 0.026440618731047232
Coverage of cases: 95.7683741648107
Instances selection time: 4.0
Test time: 50.0
Accumulative iteration time: 204.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.02627245972568936
Kappa statistic: 0.8469579049025734
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.153674832962203
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9490251549734515
Weighted AreaUnderROC: 0.9725179127536414
Root mean squared error: 0.14039355945810117
Relative absolute error: 9.764901825897807
Root relative squared error: 37.671545106176914
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8468574064981126
Weighted FMeasure: 0.9372682712129851
Iteration time: 9.0
Weighted AreaUnderPRC: 0.964989343263009
Mean absolute error: 0.027124727294160305
Coverage of cases: 95.99109131403118
Instances selection time: 8.0
Test time: 48.0
Accumulative iteration time: 213.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.02627245972568936
Kappa statistic: 0.8469579049025734
Training time: 1.0
		
Time end:Sat Oct 07 11.21.16 EEST 2017