Sat Oct 07 11.25.17 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.17 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.259094283593136
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.900215847057743
Weighted AreaUnderROC: 0.8547327489727893
Root mean squared error: 0.17452455920897134
Relative absolute error: 17.068563245045198
Root relative squared error: 46.82985337613385
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.7372514124891859
Weighted FMeasure: 0.9006288070727406
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8547259282583005
Mean absolute error: 0.04741267568068063
Coverage of cases: 92.65033407572383
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.19572308608833178
Kappa statistic: 0.7499287620133152
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9656484911063219
Weighted AreaUnderROC: 0.9405568017863886
Root mean squared error: 0.10830155105735305
Relative absolute error: 5.710059209046385
Root relative squared error: 29.06035562794882
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033775427276477
Weighted FMeasure: 0.9602915744358171
Iteration time: 6.0
Weighted AreaUnderPRC: 0.939065914486866
Mean absolute error: 0.015861275580684245
Coverage of cases: 96.43652561247217
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.08651478883500198
Kappa statistic: 0.9078265332306902
Training time: 0.0
		
Time end:Sat Oct 07 11.25.17 EEST 2017