Sat Oct 07 11.20.59 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.59 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.14773570898289
Incorrectly Classified Instances: 11.58129175946548
Correctly Classified Instances: 88.41870824053453
Weighted Precision: 0.8863453753193309
Weighted AreaUnderROC: 0.8458713640145644
Root mean squared error: 0.19161323268628663
Relative absolute error: 19.301580425354324
Root relative squared error: 51.41522564100284
Weighted TruePositiveRate: 0.8841870824053452
Weighted MatthewsCorrelation: 0.6932892720555097
Weighted FMeasure: 0.8833161666644299
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8470994970727652
Mean absolute error: 0.05361550118153925
Coverage of cases: 89.97772828507794
Instances selection time: 24.0
Test time: 17.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8841870824053452
Weighted FalsePositiveRate: 0.195981572961518
Kappa statistic: 0.7089395016019047
Training time: 1.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9273416765621666
Weighted AreaUnderROC: 0.8606360609844058
Root mean squared error: 0.14822520776763878
Relative absolute error: 9.405827411931192
Root relative squared error: 39.77299686569662
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8133331019970222
Weighted FMeasure: 0.9234692466818576
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8778251078925762
Mean absolute error: 0.02612729836647527
Coverage of cases: 93.3184855233853
Instances selection time: 12.0
Test time: 41.0
Accumulative iteration time: 38.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.20654389494803288
Kappa statistic: 0.8129660228550798
Training time: 1.0
		
Time end:Sat Oct 07 11.20.59 EEST 2017