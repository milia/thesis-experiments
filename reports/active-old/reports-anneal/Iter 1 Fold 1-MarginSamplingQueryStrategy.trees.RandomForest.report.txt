Sat Oct 07 11.23.39 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.39 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 25.501113585746147
Incorrectly Classified Instances: 16.92650334075724
Correctly Classified Instances: 83.07349665924276
Weighted Precision: 0.7881075592820647
Weighted AreaUnderROC: 0.8834724363406572
Root mean squared error: 0.20290406066815167
Relative absolute error: 22.605536185876026
Root relative squared error: 54.444872707767686
Weighted TruePositiveRate: 0.8307349665924276
Weighted MatthewsCorrelation: 0.4686908580939778
Weighted FMeasure: 0.7879386997560717
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8704706029371
Mean absolute error: 0.06279315607187723
Coverage of cases: 93.98663697104676
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8307349665924276
Weighted FalsePositiveRate: 0.5068176967823885
Kappa statistic: 0.43594829581143185
Training time: 3.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 25.68671121009653
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.8820987156317395
Weighted AreaUnderROC: 0.8999697369071823
Root mean squared error: 0.162523802323438
Relative absolute error: 18.36818443473421
Root relative squared error: 43.60971239483369
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7672740286691768
Weighted FMeasure: 0.8999307885597192
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9088455037670184
Mean absolute error: 0.05102273454092786
Coverage of cases: 95.5456570155902
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.2283741406731329
Kappa statistic: 0.7804063495612321
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 25.5382331106162
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9027407093817298
Weighted AreaUnderROC: 0.8780082529028799
Root mean squared error: 0.1420035379282624
Relative absolute error: 13.971081615881952
Root relative squared error: 38.103547662367916
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8301339700969347
Weighted FMeasure: 0.9227191800759541
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8963888194729713
Mean absolute error: 0.03880856004411615
Coverage of cases: 95.32293986636971
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1642865150512357
Kappa statistic: 0.8480109405042381
Training time: 9.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 23.051224944320733
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9068645573158527
Weighted AreaUnderROC: 0.8860553993414607
Root mean squared error: 0.13556122269527685
Relative absolute error: 11.971327221323868
Root relative squared error: 36.37489308715533
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8424035913733301
Weighted FMeasure: 0.9270801560857895
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9017601526594554
Mean absolute error: 0.033253686725899304
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.15004926925059878
Kappa statistic: 0.8612670947634273
Training time: 7.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 23.38530066815148
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9091750731472991
Weighted AreaUnderROC: 0.9058447640655246
Root mean squared error: 0.13492900486020082
Relative absolute error: 12.040521193878893
Root relative squared error: 36.2052512404572
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8486905212481941
Weighted FMeasure: 0.929493446175181
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9152605901064411
Mean absolute error: 0.03344589220521882
Coverage of cases: 95.5456570155902
Instances selection time: 9.0
Test time: 3.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14267157169735584
Kappa statistic: 0.8679800058806233
Training time: 8.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 21.343726800297024
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9088572897036149
Weighted AreaUnderROC: 0.905478994198858
Root mean squared error: 0.13190174559744186
Relative absolute error: 10.732463379234089
Root relative squared error: 35.39295234081181
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.848438882305289
Weighted FMeasure: 0.9294023588534264
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9142791347207426
Mean absolute error: 0.02981239827564995
Coverage of cases: 95.5456570155902
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14293064635028027
Kappa statistic: 0.8678987910559537
Training time: 10.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 22.271714922049046
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9110740298932841
Weighted AreaUnderROC: 0.8933871739899674
Root mean squared error: 0.13524531206438684
Relative absolute error: 11.400376634007038
Root relative squared error: 36.290125369697144
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8548374792669221
Weighted FMeasure: 0.9316007444962879
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9071623672107282
Mean absolute error: 0.03166771287224146
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 92.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1426515521783248
Kappa statistic: 0.8734090542935397
Training time: 11.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 21.232368225686738
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9056513240360647
Weighted AreaUnderROC: 0.9005370189205504
Root mean squared error: 0.13206227309423846
Relative absolute error: 10.435507618667316
Root relative squared error: 35.43602638822321
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8366183994594856
Weighted FMeasure: 0.9253761549163093
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9106397562204167
Mean absolute error: 0.028987521162964477
Coverage of cases: 95.5456570155902
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.1429706853883424
Kappa statistic: 0.8571807241690079
Training time: 12.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.524870081662982
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9067191236740944
Weighted AreaUnderROC: 0.9107570301604813
Root mean squared error: 0.13052581127021495
Relative absolute error: 8.66606163714752
Root relative squared error: 35.023750418221226
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8421488765860529
Weighted FMeasure: 0.9272217742187974
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9182483574905621
Mean absolute error: 0.024072393436520647
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 121.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.14320974052223578
Kappa statistic: 0.8624313955347153
Training time: 6.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 23.162583518930937
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9493173436693368
Weighted AreaUnderROC: 0.9921272692436025
Root mean squared error: 0.10565703383065617
Relative absolute error: 10.025180376583533
Root relative squared error: 28.350757193561204
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8447045379795107
Weighted FMeasure: 0.9282359203036162
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9898959163879606
Mean absolute error: 0.027847723268287314
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 131.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.1363902313129039
Kappa statistic: 0.8582478295185476
Training time: 9.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 24.016332590942877
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9820355290164361
Weighted AreaUnderROC: 0.9907102663076752
Root mean squared error: 0.08899567262738321
Relative absolute error: 9.473421070610323
Root relative squared error: 23.880044843777657
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9529407912905973
Weighted FMeasure: 0.982072937463354
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9895017070725713
Mean absolute error: 0.026315058529472857
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 143.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.02924034162277972
Kappa statistic: 0.9554353489987841
Training time: 11.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 22.086117297698607
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870998733569152
Weighted AreaUnderROC: 0.9946252985197264
Root mean squared error: 0.07365268798072146
Relative absolute error: 6.775956492323839
Root relative squared error: 19.763090046057084
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9653491685172266
Weighted FMeasure: 0.9867521345136884
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9932452695367149
Mean absolute error: 0.018822101367566034
Coverage of cases: 99.55456570155901
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 154.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.014744021169218349
Kappa statistic: 0.9668479732224164
Training time: 11.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.566443949517495
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9847485795227404
Weighted AreaUnderROC: 0.9923399482686833
Root mean squared error: 0.07900392921639912
Relative absolute error: 6.774180755717758
Root relative squared error: 21.19897874609412
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9594560469522763
Weighted FMeasure: 0.9844727635148902
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9905063185586652
Mean absolute error: 0.018817168765882474
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.015282189994098295
Kappa statistic: 0.961439367914806
Training time: 11.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 21.12100965107648
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9886491830685755
Weighted AreaUnderROC: 0.9976250151197593
Root mean squared error: 0.07602251493878633
Relative absolute error: 6.470406356820669
Root relative squared error: 20.39898134683444
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9706183056174977
Weighted FMeasure: 0.9887295034987393
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9947212561573948
Mean absolute error: 0.017973350991168344
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.021563530378550258
Kappa statistic: 0.9720458224380525
Training time: 12.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 21.12100965107652
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9910913140311804
Weighted AreaUnderROC: 0.9856436398492473
Root mean squared error: 0.07690109698279567
Relative absolute error: 6.125608782182125
Root relative squared error: 20.63472964774031
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.9766464065529488
Weighted FMeasure: 0.9910913140311804
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9834852189313141
Mean absolute error: 0.017015579950505733
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 193.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.01444490747823179
Kappa statistic: 0.9777259648774681
Training time: 13.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 20.304380103934697
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9886491830685755
Weighted AreaUnderROC: 0.9887395212728768
Root mean squared error: 0.07015397033174234
Relative absolute error: 5.139526526931325
Root relative squared error: 18.824285586393614
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9706183056174977
Weighted FMeasure: 0.9887295034987393
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9888368973908089
Mean absolute error: 0.014276462574809095
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 206.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.021563530378550258
Kappa statistic: 0.9720458224380525
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 20.452858203415033
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9823146459200436
Weighted AreaUnderROC: 0.9881496003055412
Root mean squared error: 0.07526035471734832
Relative absolute error: 5.2107871210845635
Root relative squared error: 20.194472299048638
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9535634880122902
Weighted FMeasure: 0.9822001729128255
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9861958463620112
Mean absolute error: 0.014474408669679199
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 219.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.022141738241492313
Kappa statistic: 0.9557722616233254
Training time: 12.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.933184855233872
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9822870848642238
Weighted AreaUnderROC: 0.997447627161469
Root mean squared error: 0.07493517084395178
Relative absolute error: 5.12253978721274
Root relative squared error: 20.107216309516474
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9533483288451591
Weighted FMeasure: 0.9821110390891029
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9963005470693717
Mean absolute error: 0.014229277186701915
Coverage of cases: 99.55456570155901
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 232.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.022400812894416762
Kappa statistic: 0.9557559185081171
Training time: 10.0
		
Time end:Sat Oct 07 11.23.40 EEST 2017