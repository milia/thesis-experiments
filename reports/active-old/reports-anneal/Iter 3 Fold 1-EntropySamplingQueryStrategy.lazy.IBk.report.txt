Sat Oct 07 11.20.16 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.16 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.29621380846323
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.9021663721418449
Weighted AreaUnderROC: 0.8808706522030924
Root mean squared error: 0.19874924285011583
Relative absolute error: 20.330319601657663
Root relative squared error: 53.33001809873703
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.7094560131169619
Weighted FMeasure: 0.8835826383392622
Iteration time: 14.0
Weighted AreaUnderPRC: 0.865680281887277
Mean absolute error: 0.05647311000460406
Coverage of cases: 87.75055679287306
Instances selection time: 13.0
Test time: 17.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.11393207323939245
Kappa statistic: 0.7171144412942712
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 12.694877505567929
Correctly Classified Instances: 87.30512249443207
Weighted Precision: 0.9015829716611815
Weighted AreaUnderROC: 0.8804951134556944
Root mean squared error: 0.20120493266217013
Relative absolute error: 19.628162652685784
Root relative squared error: 53.98894882090588
Weighted TruePositiveRate: 0.8730512249443207
Weighted MatthewsCorrelation: 0.7058780302611346
Weighted FMeasure: 0.8818912745294051
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8651641224353926
Mean absolute error: 0.05452267403523774
Coverage of cases: 87.30512249443207
Instances selection time: 14.0
Test time: 19.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8730512249443207
Weighted FalsePositiveRate: 0.11403590407818989
Kappa statistic: 0.7131022576956708
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.9168350857741916
Weighted AreaUnderROC: 0.890451437097698
Root mean squared error: 0.18139661022545428
Relative absolute error: 16.172684529032004
Root relative squared error: 48.67381816225815
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7497858017774062
Weighted FMeasure: 0.9034970183444002
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8805475598169981
Mean absolute error: 0.04492412369175512
Coverage of cases: 89.75501113585746
Instances selection time: 15.0
Test time: 22.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.11833118691451655
Kappa statistic: 0.7578379645913942
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9287139354478077
Weighted AreaUnderROC: 0.9035157519879854
Root mean squared error: 0.15180016830603857
Relative absolute error: 12.07112012735339
Root relative squared error: 40.73225943938556
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8102305146214661
Weighted FMeasure: 0.9283028265918605
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8960673302370868
Mean absolute error: 0.03353088924264797
Coverage of cases: 92.87305122494432
Instances selection time: 15.0
Test time: 23.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.12389234723287354
Kappa statistic: 0.820709276498041
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9340407290257865
Weighted AreaUnderROC: 0.9032201017463357
Root mean squared error: 0.14475369741884572
Relative absolute error: 10.902140600374496
Root relative squared error: 38.84149289075717
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8253429420458228
Weighted FMeasure: 0.934146625398336
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8996921809929905
Mean absolute error: 0.030283723889928855
Coverage of cases: 93.54120267260579
Instances selection time: 15.0
Test time: 25.0
Accumulative iteration time: 73.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.13024512011168615
Kappa statistic: 0.8343131266860081
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9364344044967653
Weighted AreaUnderROC: 0.9051723270780949
Root mean squared error: 0.1423991726849786
Relative absolute error: 10.309726743190092
Root relative squared error: 38.209707607601395
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8310930785130065
Weighted FMeasure: 0.9362566615040768
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9022692843772168
Mean absolute error: 0.028638129842194413
Coverage of cases: 93.76391982182628
Instances selection time: 15.0
Test time: 27.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.12996602593973064
Kappa statistic: 0.8394298559313375
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9600577798224097
Weighted AreaUnderROC: 0.9409963079973781
Root mean squared error: 0.11444299898285495
Relative absolute error: 7.448754044627555
Root relative squared error: 30.708279032952504
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8923665880034041
Weighted FMeasure: 0.9579442584544362
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9365150026766338
Mean absolute error: 0.020690983457298556
Coverage of cases: 95.99109131403118
Instances selection time: 15.0
Test time: 29.0
Accumulative iteration time: 103.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.07955745777455935
Kappa statistic: 0.898116632629907
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9620030784282498
Weighted AreaUnderROC: 0.9420078668714263
Root mean squared error: 0.11130228427767976
Relative absolute error: 6.966586708541247
Root relative squared error: 29.86553684350791
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8979390082235367
Weighted FMeasure: 0.9599923974929226
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9384652044614598
Mean absolute error: 0.019351629745947715
Coverage of cases: 96.21380846325167
Instances selection time: 15.0
Test time: 30.0
Accumulative iteration time: 118.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07945362693576191
Kappa statistic: 0.9033846387524682
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.968742486774994
Weighted AreaUnderROC: 0.9456484116708234
Root mean squared error: 0.10111958872134218
Relative absolute error: 5.993605460001423
Root relative squared error: 27.133232908529187
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.915610097151929
Weighted FMeasure: 0.9664715156592881
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9457968009320306
Mean absolute error: 0.016648904055559344
Coverage of cases: 96.88195991091314
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 133.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.07879160775305348
Kappa statistic: 0.9195113831340112
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.968742486774994
Weighted AreaUnderROC: 0.9453613417747607
Root mean squared error: 0.10115575736938454
Relative absolute error: 5.8346217976966175
Root relative squared error: 27.142937975210426
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.915610097151929
Weighted FMeasure: 0.9664715156592881
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9456502390604594
Mean absolute error: 0.016207282771379332
Coverage of cases: 96.88195991091314
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 146.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.07879160775305348
Kappa statistic: 0.9195113831340112
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.968742486774994
Weighted AreaUnderROC: 0.945504876722792
Root mean squared error: 0.10118867489829407
Relative absolute error: 5.690478492824976
Root relative squared error: 27.151770675085594
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.915610097151929
Weighted FMeasure: 0.9664715156592881
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9457235126162057
Mean absolute error: 0.015806884702291444
Coverage of cases: 96.88195991091314
Instances selection time: 13.0
Test time: 47.0
Accumulative iteration time: 159.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.07879160775305348
Kappa statistic: 0.9195113831340112
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9364137854428676
Root mean squared error: 0.1081756349526905
Relative absolute error: 6.088976724834252
Root relative squared error: 29.02656879161033
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9377401933359617
Mean absolute error: 0.01691382423565053
Coverage of cases: 96.43652561247217
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 170.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9364137854428676
Root mean squared error: 0.10821130587141453
Relative absolute error: 5.980743637382449
Root relative squared error: 29.036140303500744
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9377401933359617
Mean absolute error: 0.016613176770506638
Coverage of cases: 96.43652561247217
Instances selection time: 11.0
Test time: 38.0
Accumulative iteration time: 181.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9360739224062408
Root mean squared error: 0.10824493440813694
Relative absolute error: 5.888438598792623
Root relative squared error: 29.045163786792163
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9375668687203039
Mean absolute error: 0.016356773885534904
Coverage of cases: 96.43652561247217
Instances selection time: 10.0
Test time: 40.0
Accumulative iteration time: 191.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9360739224062408
Root mean squared error: 0.10827545582777874
Relative absolute error: 5.802448451756685
Root relative squared error: 29.053353543082828
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9375668687203039
Mean absolute error: 0.016117912365990632
Coverage of cases: 96.43652561247217
Instances selection time: 8.0
Test time: 49.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9364137854428676
Root mean squared error: 0.10830327209618805
Relative absolute error: 5.721809250327582
Root relative squared error: 29.060817431128058
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9377401933359617
Mean absolute error: 0.015893914584243125
Coverage of cases: 96.43652561247217
Instances selection time: 6.0
Test time: 43.0
Accumulative iteration time: 205.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9644454005534183
Weighted AreaUnderROC: 0.9364137854428676
Root mean squared error: 0.1083296116199734
Relative absolute error: 5.652136862801464
Root relative squared error: 29.067885067009538
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9033805775744805
Weighted FMeasure: 0.9618556091582589
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9377401933359617
Mean absolute error: 0.015700380174448355
Coverage of cases: 96.43652561247217
Instances selection time: 5.0
Test time: 44.0
Accumulative iteration time: 210.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.09302885355369041
Kappa statistic: 0.9073199680057795
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9730791581922172
Weighted AreaUnderROC: 0.954883037898779
Root mean squared error: 0.093872314608212
Relative absolute error: 4.53457449077989
Root relative squared error: 25.188585200305155
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9277787781196882
Weighted FMeasure: 0.9710002843230507
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9539007756924281
Mean absolute error: 0.012596040252166235
Coverage of cases: 97.32739420935413
Instances selection time: 3.0
Test time: 49.0
Accumulative iteration time: 213.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.06455436195241654
Kappa statistic: 0.9315218220177424
Training time: 0.0
		
Time end:Sat Oct 07 11.20.18 EEST 2017