Sat Oct 07 11.21.57 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.21.57 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.29621380846323
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.9021663721418449
Weighted AreaUnderROC: 0.8808706522030924
Root mean squared error: 0.19874924285011583
Relative absolute error: 20.330319601657663
Root relative squared error: 53.33001809873703
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.7094560131169619
Weighted FMeasure: 0.8835826383392622
Iteration time: 13.0
Weighted AreaUnderPRC: 0.865680281887277
Mean absolute error: 0.05647311000460406
Coverage of cases: 87.75055679287306
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.11393207323939245
Kappa statistic: 0.7171144412942712
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 12.694877505567929
Correctly Classified Instances: 87.30512249443207
Weighted Precision: 0.9015829716611815
Weighted AreaUnderROC: 0.8804951134556944
Root mean squared error: 0.20120493266217013
Relative absolute error: 19.628162652685784
Root relative squared error: 53.98894882090588
Weighted TruePositiveRate: 0.8730512249443207
Weighted MatthewsCorrelation: 0.7058780302611346
Weighted FMeasure: 0.8818912745294051
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8651641224353926
Mean absolute error: 0.05452267403523774
Coverage of cases: 87.30512249443207
Instances selection time: 14.0
Test time: 19.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8730512249443207
Weighted FalsePositiveRate: 0.11403590407818989
Kappa statistic: 0.7131022576956708
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.9168350857741916
Weighted AreaUnderROC: 0.890451437097698
Root mean squared error: 0.18139661022545428
Relative absolute error: 16.172684529032004
Root relative squared error: 48.67381816225815
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7497858017774062
Weighted FMeasure: 0.9034970183444002
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8805475598169981
Mean absolute error: 0.04492412369175512
Coverage of cases: 89.75501113585746
Instances selection time: 14.0
Test time: 22.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.11833118691451655
Kappa statistic: 0.7578379645913942
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9274984630898305
Weighted AreaUnderROC: 0.9018607246480332
Root mean squared error: 0.16093144920476526
Relative absolute error: 13.111456887783106
Root relative squared error: 43.182439216729364
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7937580249855636
Weighted FMeasure: 0.9215962381791571
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8934906880291429
Mean absolute error: 0.03642071357717493
Coverage of cases: 91.98218262806236
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 55.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.1169423518602261
Kappa statistic: 0.8031900645318397
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9298862569110725
Weighted AreaUnderROC: 0.9037453386368653
Root mean squared error: 0.15891732610922465
Relative absolute error: 12.443221925430556
Root relative squared error: 42.64199327792763
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7989984099212462
Weighted FMeasure: 0.9237370731673901
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8960428170609182
Mean absolute error: 0.03456450534841787
Coverage of cases: 92.20489977728285
Instances selection time: 15.0
Test time: 25.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.1166632576882706
Kappa statistic: 0.8079742906718151
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9358693149239072
Weighted AreaUnderROC: 0.8991753227936865
Root mean squared error: 0.15451762882722492
Relative absolute error: 11.611658386157382
Root relative squared error: 41.461430613570926
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8069728325667593
Weighted FMeasure: 0.9261976239831523
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8968929196829515
Mean absolute error: 0.03225460662821463
Coverage of cases: 92.65033407572383
Instances selection time: 17.0
Test time: 26.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.13004320145400997
Kappa statistic: 0.8146670335718216
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9445676771015104
Weighted AreaUnderROC: 0.9039660790455789
Root mean squared error: 0.13737153608279512
Relative absolute error: 9.53329860809121
Root relative squared error: 36.86065114256359
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8428024501514312
Weighted FMeasure: 0.9391615723048131
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9059695254344434
Mean absolute error: 0.02648138502247532
Coverage of cases: 94.20935412026726
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 104.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.1361559143107909
Kappa statistic: 0.8485469641930463
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9476133259321593
Weighted AreaUnderROC: 0.90472057907784
Root mean squared error: 0.13481889712464315
Relative absolute error: 9.058501242673529
Root relative squared error: 36.17570623467054
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8490632715545242
Weighted FMeasure: 0.9413715615987945
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9084065244615699
Mean absolute error: 0.025162503451870665
Coverage of cases: 94.43207126948775
Instances selection time: 16.0
Test time: 30.0
Accumulative iteration time: 120.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.1358768201388354
Kappa statistic: 0.8538183049434808
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9472046360556775
Weighted AreaUnderROC: 0.9012395796173617
Root mean squared error: 0.1349062667681202
Relative absolute error: 8.872065529998274
Root relative squared error: 36.19914997010869
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8484659924003668
Weighted FMeasure: 0.9407999591316323
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9062458557604972
Mean absolute error: 0.02464462647221718
Coverage of cases: 94.43207126948775
Instances selection time: 14.0
Test time: 37.0
Accumulative iteration time: 134.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.14289161220035645
Kappa statistic: 0.8526400084018169
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9465478841870826
Weighted AreaUnderROC: 0.8945659064394889
Root mean squared error: 0.1349837360611096
Relative absolute error: 8.710345628431613
Root relative squared error: 36.219937162743875
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8478484417779693
Weighted FMeasure: 0.9399210798136516
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9023835194756823
Mean absolute error: 0.024195404523420904
Coverage of cases: 94.43207126948775
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 147.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15692119632339852
Kappa statistic: 0.8501655187142934
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9426069822165345
Weighted AreaUnderROC: 0.8852993017578118
Root mean squared error: 0.140335407590902
Relative absolute error: 9.09644216205772
Root relative squared error: 37.655941322807806
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8352013496498593
Weighted FMeasure: 0.9346802231946754
Iteration time: 13.0
Weighted AreaUnderPRC: 0.894556778957214
Mean absolute error: 0.025267894894604525
Coverage of cases: 93.98663697104676
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 160.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8369073884733357
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9426069822165345
Weighted AreaUnderROC: 0.8849841891029466
Root mean squared error: 0.1404028698815673
Relative absolute error: 8.97914780097413
Root relative squared error: 37.67404335494926
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8352013496498593
Weighted FMeasure: 0.9346802231946754
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8943990044016189
Mean absolute error: 0.024942077224927896
Coverage of cases: 93.98663697104676
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 171.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8369073884733357
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9426069822165345
Weighted AreaUnderROC: 0.8849841891029466
Root mean squared error: 0.14046331226779693
Relative absolute error: 8.873885062571981
Root relative squared error: 37.69026174906907
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8352013496498593
Weighted FMeasure: 0.9346802231946754
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8943990044016189
Mean absolute error: 0.02464968072936637
Coverage of cases: 93.98663697104676
Instances selection time: 11.0
Test time: 38.0
Accumulative iteration time: 182.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8369073884733357
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9426069822165345
Weighted AreaUnderROC: 0.8856144144126772
Root mean squared error: 0.14051752028690234
Relative absolute error: 8.776751010212001
Root relative squared error: 37.70480728694651
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8352013496498593
Weighted FMeasure: 0.9346802231946754
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8947146008096531
Mean absolute error: 0.024379863917255315
Coverage of cases: 93.98663697104676
Instances selection time: 9.0
Test time: 41.0
Accumulative iteration time: 191.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8369073884733357
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.944829236588702
Weighted AreaUnderROC: 0.8867799155781784
Root mean squared error: 0.1379443284752979
Relative absolute error: 8.430525181406864
Root relative squared error: 37.0143474697593
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8415164805145072
Weighted FMeasure: 0.9367622284139819
Iteration time: 8.0
Weighted AreaUnderPRC: 0.896901147665097
Mean absolute error: 0.02341812550390772
Coverage of cases: 94.20935412026726
Instances selection time: 8.0
Test time: 42.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.17105461128523802
Kappa statistic: 0.8422645588433997
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9567533341904694
Weighted AreaUnderROC: 0.9145005399123626
Root mean squared error: 0.12105414788706743
Relative absolute error: 6.776291526449682
Root relative squared error: 32.4822364360556
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8789660323655858
Weighted FMeasure: 0.952095548799177
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9204776155979966
Mean absolute error: 0.018823032017915596
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 43.0
Accumulative iteration time: 205.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1283428738833272
Kappa statistic: 0.8814709220981495
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9709057554053756
Weighted AreaUnderROC: 0.9502082395258616
Root mean squared error: 0.09767487695979957
Relative absolute error: 4.863545430885716
Root relative squared error: 26.20891974912491
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9217016693930781
Weighted FMeasure: 0.9687464043112672
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9498287302073096
Mean absolute error: 0.013509848419126853
Coverage of cases: 97.10467706013362
Instances selection time: 4.0
Test time: 45.0
Accumulative iteration time: 209.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.071672984852735
Kappa statistic: 0.9255389718076285
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9730791581922172
Weighted AreaUnderROC: 0.954883037898779
Root mean squared error: 0.093872314608212
Relative absolute error: 4.53457449077989
Root relative squared error: 25.188585200305155
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9277787781196882
Weighted FMeasure: 0.9710002843230507
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9539007756924281
Mean absolute error: 0.012596040252166235
Coverage of cases: 97.32739420935413
Instances selection time: 2.0
Test time: 46.0
Accumulative iteration time: 211.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.06455436195241654
Kappa statistic: 0.9315218220177424
Training time: 0.0
		
Time end:Sat Oct 07 11.21.59 EEST 2017