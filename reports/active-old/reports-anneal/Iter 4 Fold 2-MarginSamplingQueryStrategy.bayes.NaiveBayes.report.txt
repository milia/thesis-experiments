Sat Oct 07 11.22.36 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.22.36 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.529324424647328
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.8883794383773486
Weighted AreaUnderROC: 0.9477318302888142
Root mean squared error: 0.16389061662964818
Relative absolute error: 15.391838215796959
Root relative squared error: 43.97646715899031
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7199437344349178
Weighted FMeasure: 0.8868086950250028
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9395137714484475
Mean absolute error: 0.042755106154991125
Coverage of cases: 95.5456570155902
Instances selection time: 18.0
Test time: 26.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.26549478332137516
Kappa statistic: 0.7093033075299087
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 21.566443949517488
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.947821294914452
Weighted AreaUnderROC: 0.9732371713036728
Root mean squared error: 0.12567871774966743
Relative absolute error: 11.202222837612062
Root relative squared error: 33.72313874559206
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8828206845918471
Weighted FMeasure: 0.9482933155210074
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9655270587121808
Mean absolute error: 0.031117285660033196
Coverage of cases: 97.10467706013362
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.03921721312012301
Kappa statistic: 0.881326813796749
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 21.71492204899782
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9512306760120529
Weighted AreaUnderROC: 0.965038343938016
Root mean squared error: 0.138982693853193
Relative absolute error: 11.218521017734501
Root relative squared error: 37.292970138213995
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8546658300368245
Weighted FMeasure: 0.9406897239139416
Iteration time: 17.0
Weighted AreaUnderPRC: 0.954576053607643
Mean absolute error: 0.031162558382595528
Coverage of cases: 95.99109131403118
Instances selection time: 16.0
Test time: 26.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.02716259538184689
Kappa statistic: 0.8557942670994827
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 21.45508537490727
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9604076638280754
Weighted AreaUnderROC: 0.9696155954580608
Root mean squared error: 0.12324361062406954
Relative absolute error: 9.706886738487706
Root relative squared error: 33.06973093775233
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8866191852327368
Weighted FMeasure: 0.9548136464093198
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9603684126783952
Mean absolute error: 0.026963574273576693
Coverage of cases: 96.65924276169265
Instances selection time: 15.0
Test time: 26.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.025252795827281186
Kappa statistic: 0.8892399859039117
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.93541202672609
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9634217688659301
Weighted AreaUnderROC: 0.9703115863912787
Root mean squared error: 0.11809084304960969
Relative absolute error: 8.733028343175647
Root relative squared error: 31.687098309502456
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8962861010185574
Weighted FMeasure: 0.9589557173806325
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9620623144555598
Mean absolute error: 0.024258412064376556
Coverage of cases: 96.65924276169265
Instances selection time: 14.0
Test time: 30.0
Accumulative iteration time: 82.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.024707138811690985
Kappa statistic: 0.8990939629068887
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.304380103934758
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.963648130714261
Weighted AreaUnderROC: 0.9748087743036988
Root mean squared error: 0.11943077233262328
Relative absolute error: 8.977133853085213
Root relative squared error: 32.04663906492577
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8964730225723311
Weighted FMeasure: 0.9590713194882001
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9695780850885665
Mean absolute error: 0.024936482925236456
Coverage of cases: 97.32739420935413
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 95.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.02453814114269331
Kappa statistic: 0.8991285634895297
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.71046770601341
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9601480073885904
Weighted AreaUnderROC: 0.9742603501834435
Root mean squared error: 0.12205561654869572
Relative absolute error: 8.500297743091838
Root relative squared error: 32.75095867662399
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8827439171522288
Weighted FMeasure: 0.9533265101282326
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9694835980601474
Mean absolute error: 0.023611938175254876
Coverage of cases: 97.32739420935413
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 108.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.024849633659085605
Kappa statistic: 0.8845205111118905
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.636228656273275
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9581957350945907
Weighted AreaUnderROC: 0.9744092037143248
Root mean squared error: 0.12126560690182273
Relative absolute error: 8.846744426749058
Root relative squared error: 32.53897684382935
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8739676089614921
Weighted FMeasure: 0.9496275095259338
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9705783801523553
Mean absolute error: 0.024574290074302698
Coverage of cases: 97.32739420935413
Instances selection time: 11.0
Test time: 26.0
Accumulative iteration time: 120.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.025057295336680472
Kappa statistic: 0.8749637399487137
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.26503340757245
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9573117691797529
Weighted AreaUnderROC: 0.9740869600940781
Root mean squared error: 0.12889489996892148
Relative absolute error: 8.771341364456147
Root relative squared error: 34.586130994025446
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8696814470348596
Weighted FMeasure: 0.9478119828577936
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9707722864891694
Mean absolute error: 0.024364837123489053
Coverage of cases: 96.88195991091314
Instances selection time: 10.0
Test time: 26.0
Accumulative iteration time: 131.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.0251611261754779
Kappa statistic: 0.8702387145251719
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 19.005196733481874
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.958951099279919
Weighted AreaUnderROC: 0.97352340948307
Root mean squared error: 0.1303668241514943
Relative absolute error: 8.627013799422011
Root relative squared error: 34.98108969762048
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8743930109879986
Weighted FMeasure: 0.949941605691589
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9703394073623677
Mean absolute error: 0.023963927220616455
Coverage of cases: 96.65924276169265
Instances selection time: 9.0
Test time: 31.0
Accumulative iteration time: 141.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.0248882976676828
Kappa statistic: 0.8750057996566603
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 18.63400148478104
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9557271338002608
Weighted AreaUnderROC: 0.9726065017857259
Root mean squared error: 0.1336760524952673
Relative absolute error: 8.57621064834097
Root relative squared error: 35.869048841189944
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8650318754214398
Weighted FMeasure: 0.9456962476287253
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9688999360694555
Mean absolute error: 0.023822807356502457
Coverage of cases: 96.43652561247217
Instances selection time: 8.0
Test time: 26.0
Accumulative iteration time: 150.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.025433954683273002
Kappa statistic: 0.865503813451923
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.374164810690463
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9550626270517097
Weighted AreaUnderROC: 0.9716432448627478
Root mean squared error: 0.13603490962880496
Relative absolute error: 8.545421760501904
Root relative squared error: 36.50199662916601
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8649567350055439
Weighted FMeasure: 0.945667228210825
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9681785502568446
Mean absolute error: 0.023737282668060612
Coverage of cases: 95.99109131403118
Instances selection time: 7.0
Test time: 52.0
Accumulative iteration time: 158.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.02518114569450896
Kappa statistic: 0.8655735062123603
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.151447661469973
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9534778547835205
Weighted AreaUnderROC: 0.9719374189774576
Root mean squared error: 0.13548100288918932
Relative absolute error: 8.418598090914225
Root relative squared error: 36.35336785440891
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8603442686350019
Weighted FMeasure: 0.94354790247624
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9691260166554325
Mean absolute error: 0.023384994696983727
Coverage of cases: 95.5456570155902
Instances selection time: 6.0
Test time: 26.0
Accumulative iteration time: 165.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.02545397420230406
Kappa statistic: 0.8608726588321703
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.114328136599884
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9527111558207381
Weighted AreaUnderROC: 0.9693418701717716
Root mean squared error: 0.14025137886275404
Relative absolute error: 8.799003285969048
Root relative squared error: 37.63339404902361
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.855911781029562
Weighted FMeasure: 0.9415044702546933
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9662502214341478
Mean absolute error: 0.024441675794358228
Coverage of cases: 94.65478841870824
Instances selection time: 5.0
Test time: 28.0
Accumulative iteration time: 171.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.025979611698863203
Kappa statistic: 0.8561292685159755
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.225686711210127
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9504615991605178
Weighted AreaUnderROC: 0.9680066120885745
Root mean squared error: 0.14175091400939793
Relative absolute error: 8.940546814292517
Root relative squared error: 38.03576155172911
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8512962227873252
Weighted FMeasure: 0.9393488951791908
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9667193181864209
Mean absolute error: 0.02483485226192341
Coverage of cases: 94.65478841870824
Instances selection time: 4.0
Test time: 25.0
Accumulative iteration time: 176.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.025999631217894257
Kappa statistic: 0.8515651717927089
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.22568671121013
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9504615991605178
Weighted AreaUnderROC: 0.9692214182254817
Root mean squared error: 0.1428566287481015
Relative absolute error: 9.1026632273863
Root relative squared error: 38.33245595006487
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8512962227873252
Weighted FMeasure: 0.9393488951791908
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9663963960692076
Mean absolute error: 0.025285175631628357
Coverage of cases: 94.65478841870824
Instances selection time: 3.0
Test time: 26.0
Accumulative iteration time: 180.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.025999631217894257
Kappa statistic: 0.8515651717927089
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 18.856718634001545
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9490251549734515
Weighted AreaUnderROC: 0.9731082968369925
Root mean squared error: 0.140703963907001
Relative absolute error: 9.518622743177103
Root relative squared error: 37.75483535996792
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8468574064981126
Weighted FMeasure: 0.9372682712129851
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9669339263259373
Mean absolute error: 0.026440618731047243
Coverage of cases: 95.7683741648107
Instances selection time: 2.0
Test time: 26.0
Accumulative iteration time: 183.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.02627245972568936
Kappa statistic: 0.8469579049025734
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.153674832962203
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9490251549734515
Weighted AreaUnderROC: 0.9725179127536414
Root mean squared error: 0.14039355945810114
Relative absolute error: 9.764901825897807
Root relative squared error: 37.67154510617691
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8468574064981126
Weighted FMeasure: 0.9372682712129851
Iteration time: 3.0
Weighted AreaUnderPRC: 0.964989343263009
Mean absolute error: 0.02712472729416031
Coverage of cases: 95.99109131403118
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 186.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.02627245972568936
Kappa statistic: 0.8469579049025734
Training time: 1.0
		
Time end:Sat Oct 07 11.22.38 EEST 2017