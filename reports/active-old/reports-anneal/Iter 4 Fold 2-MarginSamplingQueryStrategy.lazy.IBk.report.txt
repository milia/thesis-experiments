Sat Oct 07 11.23.25 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.25 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.259094283593136
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.8963009798255923
Weighted AreaUnderROC: 0.8720276561680567
Root mean squared error: 0.17452455920897156
Relative absolute error: 17.068563245045166
Root relative squared error: 46.829853376133904
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.7422489549821832
Weighted FMeasure: 0.8995749769244802
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8645054351255526
Mean absolute error: 0.04741267568068055
Coverage of cases: 92.65033407572383
Instances selection time: 11.0
Test time: 16.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.16132511645671732
Kappa statistic: 0.7588885419918827
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9107311118725374
Weighted AreaUnderROC: 0.884038061343929
Root mean squared error: 0.1581154286743648
Relative absolute error: 14.032581028925646
Root relative squared error: 42.426821616888105
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7842693536683892
Weighted FMeasure: 0.9159489181531292
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8808411490256446
Mean absolute error: 0.0389793917470153
Coverage of cases: 92.20489977728285
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.15914248839435652
Kappa statistic: 0.7978284082283774
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9173501272969017
Weighted AreaUnderROC: 0.8875008606045186
Root mean squared error: 0.1515631555789432
Relative absolute error: 12.577984747249868
Root relative squared error: 40.66866225106742
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8014479965094649
Weighted FMeasure: 0.9223422138778183
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8874511662490281
Mean absolute error: 0.03493884652013818
Coverage of cases: 92.87305122494432
Instances selection time: 14.0
Test time: 19.0
Accumulative iteration time: 39.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.1583240028709712
Kappa statistic: 0.8130432519648156
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9171119872178404
Weighted AreaUnderROC: 0.8846997791304466
Root mean squared error: 0.15179451909381064
Relative absolute error: 12.049992248384527
Root relative squared error: 40.730743596678245
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8005657954611953
Weighted FMeasure: 0.9218263384597419
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8853321659214427
Mean absolute error: 0.03347220068995669
Coverage of cases: 92.87305122494432
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.16533879493249223
Kappa statistic: 0.8115350813909258
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.924782511965177
Weighted AreaUnderROC: 0.8884497791304464
Root mean squared error: 0.14474718064127476
Relative absolute error: 10.875880694640824
Root relative squared error: 38.839744255840024
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8189843825019497
Weighted FMeasure: 0.9283647383021806
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8928106950650674
Mean absolute error: 0.030210779707335322
Coverage of cases: 93.54120267260579
Instances selection time: 15.0
Test time: 23.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.16452030940910695
Kappa statistic: 0.827211444040447
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9324172240905558
Weighted AreaUnderROC: 0.8914299973670546
Root mean squared error: 0.13725157297074383
Relative absolute error: 9.784906630118302
Root relative squared error: 36.8284616617629
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.837614961151023
Weighted FMeasure: 0.93437744793598
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8985229283326186
Mean absolute error: 0.027180296194772788
Coverage of cases: 94.20935412026726
Instances selection time: 15.0
Test time: 25.0
Accumulative iteration time: 83.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.16403981922371697
Kappa statistic: 0.843135674070491
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9324172240905558
Weighted AreaUnderROC: 0.8920307814614489
Root mean squared error: 0.13736762773815725
Relative absolute error: 9.51491638090017
Root relative squared error: 36.859602423648816
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.837614961151023
Weighted FMeasure: 0.93437744793598
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8988210553890664
Mean absolute error: 0.026430323280277987
Coverage of cases: 94.20935412026726
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.16403981922371697
Kappa statistic: 0.843135674070491
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9304522163400477
Weighted AreaUnderROC: 0.8874897973430549
Root mean squared error: 0.14007786170817052
Relative absolute error: 9.556923594782333
Root relative squared error: 37.58683451067428
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8313749234225896
Weighted FMeasure: 0.9319713985804116
Iteration time: 14.0
Weighted AreaUnderPRC: 0.894952160318905
Mean absolute error: 0.026547009985506218
Coverage of cases: 93.98663697104676
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 112.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8364585581696524
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9304522163400477
Weighted AreaUnderROC: 0.887176213988071
Root mean squared error: 0.14017365613398733
Relative absolute error: 9.375886657558436
Root relative squared error: 37.61253885243331
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8313749234225896
Weighted FMeasure: 0.9319713985804116
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8947967689970457
Mean absolute error: 0.02604412960432873
Coverage of cases: 93.98663697104676
Instances selection time: 14.0
Test time: 30.0
Accumulative iteration time: 126.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8364585581696524
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9304522163400477
Weighted AreaUnderROC: 0.887176213988071
Root mean squared error: 0.140258108655878
Relative absolute error: 9.218787442087654
Root relative squared error: 37.63519984201157
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8313749234225896
Weighted FMeasure: 0.9319713985804116
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8947967689970457
Mean absolute error: 0.025607742894687674
Coverage of cases: 93.98663697104676
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 139.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.17115844212403544
Kappa statistic: 0.8364585581696524
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9284980053354218
Weighted AreaUnderROC: 0.8819684892363491
Root mean squared error: 0.14290184187561744
Relative absolute error: 9.349300330999055
Root relative squared error: 38.34458790525699
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8251117304456009
Weighted FMeasure: 0.9295369052240333
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8906087586581117
Mean absolute error: 0.025970278697219337
Coverage of cases: 93.76391982182628
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 151.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1782770650243539
Kappa statistic: 0.8297284485677524
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9284980053354218
Weighted AreaUnderROC: 0.8819684892363491
Root mean squared error: 0.14297131688376769
Relative absolute error: 9.230814100524638
Root relative squared error: 38.363230006172344
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8251117304456009
Weighted FMeasure: 0.9295369052240333
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8906087586581117
Mean absolute error: 0.025641150279234852
Coverage of cases: 93.76391982182628
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1782770650243539
Kappa statistic: 0.8297284485677524
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9284980053354218
Weighted AreaUnderROC: 0.8826220385618903
Root mean squared error: 0.14303327672279217
Relative absolute error: 9.122523668453132
Root relative squared error: 38.37985557560438
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8251117304456009
Weighted FMeasure: 0.9295369052240333
Iteration time: 11.0
Weighted AreaUnderPRC: 0.890931981223572
Mean absolute error: 0.02534034352348067
Coverage of cases: 93.76391982182628
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1782770650243539
Kappa statistic: 0.8297284485677524
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9284980053354218
Weighted AreaUnderROC: 0.8822952638991198
Root mean squared error: 0.1430900170720591
Relative absolute error: 9.032030205122316
Root relative squared error: 38.39508060896774
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8251117304456009
Weighted FMeasure: 0.9295369052240333
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8907703503349288
Mean absolute error: 0.025088972792006184
Coverage of cases: 93.76391982182628
Instances selection time: 9.0
Test time: 38.0
Accumulative iteration time: 182.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1782770650243539
Kappa statistic: 0.8297284485677524
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9357791397986244
Weighted AreaUnderROC: 0.9041232399907558
Root mean squared error: 0.13254131342756043
Relative absolute error: 7.892184783389735
Root relative squared error: 35.56456639813587
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8498362017498307
Weighted FMeasure: 0.9392145340589911
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9077384065008057
Mean absolute error: 0.02192273550941571
Coverage of cases: 94.65478841870824
Instances selection time: 8.0
Test time: 40.0
Accumulative iteration time: 190.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.14295677903055665
Kappa statistic: 0.8574471181194024
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9396417836061489
Weighted AreaUnderROC: 0.9196570384578169
Root mean squared error: 0.12694907563617744
Relative absolute error: 7.294300489218356
Root relative squared error: 34.06401153639078
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8624852541321845
Weighted FMeasure: 0.9438863387246923
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9183541087138765
Mean absolute error: 0.020261945803384123
Coverage of cases: 95.10022271714922
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 196.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.11502794444487302
Kappa statistic: 0.8722964150431151
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9541490894798899
Weighted AreaUnderROC: 0.9516917633718375
Root mean squared error: 0.11488440278861464
Relative absolute error: 6.172536883538726
Root relative squared error: 30.8267201027772
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8922486053453729
Weighted FMeasure: 0.9565676159540663
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9434586638578314
Mean absolute error: 0.0171459357876074
Coverage of cases: 95.99109131403118
Instances selection time: 4.0
Test time: 43.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.05849428459751503
Kappa statistic: 0.9004863633565229
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9528627113273427
Weighted AreaUnderROC: 0.9505470768931794
Root mean squared error: 0.11805460651723546
Relative absolute error: 6.373492819291926
Root relative squared error: 31.67737502754354
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8872092597435022
Weighted FMeasure: 0.9546449358976207
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9418276622032519
Mean absolute error: 0.017704146720255172
Coverage of cases: 95.7683741648107
Instances selection time: 2.0
Test time: 45.0
Accumulative iteration time: 202.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.058598115436312465
Kappa statistic: 0.8953726529060426
Training time: 0.0
		
Time end:Sat Oct 07 11.23.26 EEST 2017