Sat Oct 07 11.20.54 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.54 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9468236292289745
Weighted AreaUnderROC: 0.9237506357846464
Root mean squared error: 0.3134058264873484
Relative absolute error: 80.69487750556931
Root relative squared error: 84.09560790842623
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8178481177097207
Weighted FMeasure: 0.9300337675373206
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9180873707671497
Mean absolute error: 0.22415243751546807
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.08060914987607765
Kappa statistic: 0.8197723838307518
Training time: 89.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9672777111529895
Weighted AreaUnderROC: 0.9493436337309555
Root mean squared error: 0.3115791720684814
Relative absolute error: 80.28507795100361
Root relative squared error: 83.60546509418779
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.909803950555605
Weighted FMeasure: 0.9643784259147004
Iteration time: 281.0
Weighted AreaUnderPRC: 0.9445806784554286
Mean absolute error: 0.22301410541945227
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 379.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.08573496732021388
Kappa statistic: 0.9135052526134643
Training time: 280.0
		
Time end:Sat Oct 07 11.20.55 EEST 2017