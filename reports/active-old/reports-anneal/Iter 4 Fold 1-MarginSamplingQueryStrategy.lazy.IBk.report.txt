Sat Oct 07 11.23.23 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.23 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.33333333333331
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9128254426976044
Weighted AreaUnderROC: 0.8259901007669569
Root mean squared error: 0.17049817112666088
Relative absolute error: 16.581408979017844
Root relative squared error: 45.7494600814329
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7463556081518048
Weighted FMeasure: 0.9027386620131588
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8432438215906513
Mean absolute error: 0.046059469386160214
Coverage of cases: 92.87305122494432
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2567057672856855
Kappa statistic: 0.7418925171403333
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9182505304876979
Weighted AreaUnderROC: 0.8119605166439148
Root mean squared error: 0.17096201952372375
Relative absolute error: 15.603369807301387
Root relative squared error: 45.87392366708333
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7480944004742486
Weighted FMeasure: 0.9000040999435688
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8364559913118582
Mean absolute error: 0.04334269390917009
Coverage of cases: 90.86859688195992
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2847649355317696
Kappa statistic: 0.732505085730892
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.9164659102834688
Weighted AreaUnderROC: 0.8083636943560926
Root mean squared error: 0.17340141646302315
Relative absolute error: 15.160828777755418
Root relative squared error: 46.52848255272873
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7415193593815181
Weighted FMeasure: 0.8972496959910925
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8332105824312247
Mean absolute error: 0.0421134132715424
Coverage of cases: 90.64587973273942
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.2918835584320881
Kappa statistic: 0.7248132150361896
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.9164659102834688
Weighted AreaUnderROC: 0.8083636943560926
Root mean squared error: 0.17372635996737534
Relative absolute error: 14.65294693735575
Root relative squared error: 46.61567404447816
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7415193593815181
Weighted FMeasure: 0.8972496959910925
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8332105824312247
Mean absolute error: 0.04070263038154335
Coverage of cases: 90.64587973273942
Instances selection time: 15.0
Test time: 22.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.2918835584320881
Kappa statistic: 0.7248132150361896
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.9111676741966274
Weighted AreaUnderROC: 0.7944241924781539
Root mean squared error: 0.1800691427625892
Relative absolute error: 15.035449762854464
Root relative squared error: 48.31762126407189
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7215446640753781
Weighted FMeasure: 0.8887246539335281
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8220246806068778
Mean absolute error: 0.04176513823015087
Coverage of cases: 89.97772828507794
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 71.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.31323942713304354
Kappa statistic: 0.7013392065275232
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.909419896834647
Weighted AreaUnderROC: 0.7897776918521744
Root mean squared error: 0.18229166892192092
Relative absolute error: 14.985887442179344
Root relative squared error: 48.913987612956355
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7147960480949631
Weighted FMeasure: 0.8857894765467149
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8183141874610477
Mean absolute error: 0.041627465117164426
Coverage of cases: 89.75501113585746
Instances selection time: 15.0
Test time: 26.0
Accumulative iteration time: 86.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.32035805003336193
Kappa statistic: 0.6933788598574823
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.9096962321768879
Weighted AreaUnderROC: 0.7868588156117379
Root mean squared error: 0.18249539815955307
Relative absolute error: 14.732405198622976
Root relative squared error: 48.96865390387832
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7153787591706339
Weighted FMeasure: 0.8849047298249484
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8171768760890163
Mean absolute error: 0.0409233477739523
Coverage of cases: 89.75501113585746
Instances selection time: 15.0
Test time: 28.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.3274566534146494
Kappa statistic: 0.6904840401618464
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.9096962321768879
Weighted AreaUnderROC: 0.7850467289719626
Root mean squared error: 0.18267070821532133
Relative absolute error: 14.533289105814493
Root relative squared error: 49.01569452809878
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7153787591706339
Weighted FMeasure: 0.8849047298249484
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8163236794613756
Mean absolute error: 0.040370247516150974
Coverage of cases: 89.75501113585746
Instances selection time: 15.0
Test time: 30.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.3274566534146494
Kappa statistic: 0.6904840401618464
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.703786191536732
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.9079702967462685
Weighted AreaUnderROC: 0.7851452511663705
Root mean squared error: 0.18332695314552772
Relative absolute error: 14.487107983505352
Root relative squared error: 49.19178352095851
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7088436800912303
Weighted FMeasure: 0.8823534736708563
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8163402875847763
Mean absolute error: 0.040241966620847794
Coverage of cases: 89.75501113585746
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 131.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.3345752763149678
Kappa statistic: 0.6822842173409012
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9256395037932432
Weighted AreaUnderROC: 0.8271028037383177
Root mean squared error: 0.16411962863960225
Relative absolute error: 11.854707430654091
Root relative squared error: 44.03791752962085
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7723219065266966
Weighted FMeasure: 0.9058072368038419
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8508382875294351
Mean absolute error: 0.032929742862927706
Coverage of cases: 91.75946547884188
Instances selection time: 13.0
Test time: 33.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.2633890473117832
Kappa statistic: 0.7610018558213808
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9238307349665924
Weighted AreaUnderROC: 0.822429906542056
Root mean squared error: 0.16641668750873545
Relative absolute error: 11.983239590804576
Root relative squared error: 44.65428310318493
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.7657016950445373
Weighted FMeasure: 0.9027449673512461
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8470503618533639
Mean absolute error: 0.03328677664112349
Coverage of cases: 91.53674832962137
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 156.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.27050767021210165
Kappa statistic: 0.7535069850763517
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9220314609894253
Weighted AreaUnderROC: 0.8187823440659923
Root mean squared error: 0.1686770956094841
Relative absolute error: 12.125186617391615
Root relative squared error: 45.260814243604756
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7590345598866003
Weighted FMeasure: 0.8996282044568231
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8437641297139766
Mean absolute error: 0.033681073937198595
Coverage of cases: 91.31403118040089
Instances selection time: 12.0
Test time: 37.0
Accumulative iteration time: 168.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.2776262931124201
Kappa statistic: 0.7459486122999696
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9220314609894253
Weighted AreaUnderROC: 0.8187823440659923
Root mean squared error: 0.1687574280065761
Relative absolute error: 12.023570931667495
Root relative squared error: 45.28236968768794
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7590345598866003
Weighted FMeasure: 0.8996282044568231
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8437641297139766
Mean absolute error: 0.03339880814352048
Coverage of cases: 91.31403118040089
Instances selection time: 10.0
Test time: 39.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.2776262931124201
Kappa statistic: 0.7459486122999696
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9220314609894253
Weighted AreaUnderROC: 0.8187823440659923
Root mean squared error: 0.16882956471903313
Relative absolute error: 11.933405148444994
Root relative squared error: 45.301725998815215
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7590345598866003
Weighted FMeasure: 0.8996282044568231
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8437641297139766
Mean absolute error: 0.03314834763456909
Coverage of cases: 91.31403118040089
Instances selection time: 10.0
Test time: 40.0
Accumulative iteration time: 189.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.2776262931124201
Kappa statistic: 0.7459486122999696
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9500529409617
Weighted AreaUnderROC: 0.8884798534556851
Root mean squared error: 0.13254331895989135
Relative absolute error: 7.908091098498566
Root relative squared error: 35.56510453893063
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8550876073958225
Weighted FMeasure: 0.9415615953649039
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9012924488455865
Mean absolute error: 0.021966919718051352
Coverage of cases: 94.65478841870824
Instances selection time: 8.0
Test time: 41.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.17084694960764316
Kappa statistic: 0.8530017597228094
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9679961071288203
Weighted AreaUnderROC: 0.9302983590895012
Root mean squared error: 0.10487442410881385
Relative absolute error: 5.463644057606178
Root relative squared error: 28.14076096901421
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9099032422370489
Weighted FMeasure: 0.9630361957646243
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9369331086943681
Mean absolute error: 0.015176789048905898
Coverage of cases: 96.65924276169265
Instances selection time: 7.0
Test time: 42.0
Accumulative iteration time: 205.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.10677934350477696
Kappa statistic: 0.9113057219990782
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9847224969847034
Weighted AreaUnderROC: 0.9674727061463326
Root mean squared error: 0.07176722970734777
Relative absolute error: 3.2863625670542045
Root relative squared error: 19.25716850189673
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9586230227531202
Weighted FMeasure: 0.9839312542714724
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9702829082993906
Mean absolute error: 0.00912878490848381
Coverage of cases: 98.44097995545657
Instances selection time: 5.0
Test time: 45.0
Accumulative iteration time: 210.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.049830360302229246
Kappa statistic: 0.9599367758218508
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9865182457841803
Weighted AreaUnderROC: 0.9756008246801035
Root mean squared error: 0.06647736062767685
Relative absolute error: 2.957705074708413
Root relative squared error: 17.83774767939051
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9644668094437193
Weighted FMeasure: 0.9863959254772943
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9761165837632642
Mean absolute error: 0.00821584742974551
Coverage of cases: 98.66369710467706
Instances selection time: 3.0
Test time: 46.0
Accumulative iteration time: 213.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.03569694534038975
Kappa statistic: 0.9660722381743994
Training time: 0.0
		
Time end:Sat Oct 07 11.23.25 EEST 2017