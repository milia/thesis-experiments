Sat Oct 07 11.20.21 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.20.21 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.259094283593136
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.8963009798255923
Weighted AreaUnderROC: 0.8720276561680567
Root mean squared error: 0.17452455920897156
Relative absolute error: 17.068563245045166
Root relative squared error: 46.829853376133904
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.7422489549821832
Weighted FMeasure: 0.8995749769244802
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8645054351255526
Mean absolute error: 0.04741267568068055
Coverage of cases: 92.65033407572383
Instances selection time: 16.0
Test time: 15.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.16132511645671732
Kappa statistic: 0.7588885419918827
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9107311118725374
Weighted AreaUnderROC: 0.884038061343929
Root mean squared error: 0.1581154286743648
Relative absolute error: 14.032581028925646
Root relative squared error: 42.426821616888105
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7842693536683892
Weighted FMeasure: 0.9159489181531292
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8808411490256446
Mean absolute error: 0.0389793917470153
Coverage of cases: 92.20489977728285
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.15914248839435652
Kappa statistic: 0.7978284082283774
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9173501272969017
Weighted AreaUnderROC: 0.8875008606045186
Root mean squared error: 0.1515631555789432
Relative absolute error: 12.577984747249868
Root relative squared error: 40.66866225106742
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8014479965094649
Weighted FMeasure: 0.9223422138778183
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8874511662490281
Mean absolute error: 0.03493884652013818
Coverage of cases: 92.87305122494432
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.1583240028709712
Kappa statistic: 0.8130432519648156
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9139878742885423
Weighted AreaUnderROC: 0.8851698582735165
Root mean squared error: 0.15642751566366261
Relative absolute error: 12.572273416496271
Root relative squared error: 41.97390702904374
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7906996478561579
Weighted FMeasure: 0.9187437715208727
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8848666096266732
Mean absolute error: 0.034922981712489296
Coverage of cases: 92.42761692650335
Instances selection time: 14.0
Test time: 42.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.15853166454856607
Kappa statistic: 0.8030091875709714
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9133403595651596
Weighted AreaUnderROC: 0.8823687767994443
Root mean squared error: 0.15664109246361343
Relative absolute error: 12.166367490695562
Root relative squared error: 42.03121569821656
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7895003041920708
Weighted FMeasure: 0.918161362791749
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8825506890837547
Mean absolute error: 0.03379546525193178
Coverage of cases: 92.42761692650335
Instances selection time: 14.0
Test time: 23.0
Accumulative iteration time: 71.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.1655464566100871
Kappa statistic: 0.8014333840610814
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.921015524467161
Weighted AreaUnderROC: 0.8861187767994442
Root mean squared error: 0.1497841719761595
Relative absolute error: 11.073346753469353
Root relative squared error: 40.191310859065595
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8075190673986329
Weighted FMeasure: 0.9247193416305511
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8900287704931572
Mean absolute error: 0.030759296537414566
Coverage of cases: 93.0957683741648
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 85.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.1647279710867018
Kappa statistic: 0.8168600826294046
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9155784025272369
Weighted AreaUnderROC: 0.8872329291264635
Root mean squared error: 0.15466484922788723
Relative absolute error: 11.327403981936472
Root relative squared error: 41.50093399239761
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7960104058645159
Weighted FMeasure: 0.9205270439666177
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8868894111524835
Mean absolute error: 0.031465011060934335
Coverage of cases: 92.65033407572383
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 99.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.15842783370976865
Kappa statistic: 0.8080052867546065
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9239847994269584
Weighted AreaUnderROC: 0.8913162044680437
Root mean squared error: 0.14515053921202486
Relative absolute error: 10.077797404193317
Root relative squared error: 38.947976717860556
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188104755953545
Weighted FMeasure: 0.9282409036879844
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8932497252812739
Mean absolute error: 0.027993881678314493
Coverage of cases: 93.54120267260579
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 113.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15784351268558125
Kappa statistic: 0.8284792201804652
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9239847994269584
Weighted AreaUnderROC: 0.8918906059468642
Root mean squared error: 0.14525190738130186
Relative absolute error: 9.89006382062857
Root relative squared error: 38.97517665193153
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188104755953545
Weighted FMeasure: 0.9282409036879844
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8935382570157138
Mean absolute error: 0.02747239950174575
Coverage of cases: 93.54120267260579
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 126.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15784351268558125
Kappa statistic: 0.8284792201804652
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9239847994269584
Weighted AreaUnderROC: 0.8918906059468642
Root mean squared error: 0.14534233365275126
Relative absolute error: 9.734437788073379
Root relative squared error: 38.99944056672105
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188104755953545
Weighted FMeasure: 0.9282409036879844
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8935382570157138
Mean absolute error: 0.02704010496687023
Coverage of cases: 93.54120267260579
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 139.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15784351268558125
Kappa statistic: 0.8284792201804652
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9239847994269584
Weighted AreaUnderROC: 0.8916034052074537
Root mean squared error: 0.14542299061421984
Relative absolute error: 9.602158499940387
Root relative squared error: 39.02108310056533
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188104755953545
Weighted FMeasure: 0.9282409036879844
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8933939751742799
Mean absolute error: 0.02667266249983414
Coverage of cases: 93.54120267260579
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 151.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15784351268558125
Kappa statistic: 0.8284792201804652
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9239847994269584
Weighted AreaUnderROC: 0.8916034052074537
Root mean squared error: 0.1454947169524792
Relative absolute error: 9.484581120438651
Root relative squared error: 39.04032929674034
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188104755953545
Weighted FMeasure: 0.9282409036879844
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8933939751742799
Mean absolute error: 0.02634605866788488
Coverage of cases: 93.54120267260579
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15784351268558125
Kappa statistic: 0.8284792201804652
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9238170574721691
Weighted AreaUnderROC: 0.894933963249208
Root mean squared error: 0.14555914351215524
Relative absolute error: 9.381044571865344
Root relative squared error: 39.05761675677938
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8191626878160486
Weighted FMeasure: 0.9284219742429795
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8949504357889093
Mean absolute error: 0.02605845714407014
Coverage of cases: 93.54120267260579
Instances selection time: 10.0
Test time: 37.0
Accumulative iteration time: 172.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15099771829305786
Kappa statistic: 0.8297930746003321
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9263910657372543
Weighted AreaUnderROC: 0.895530630864619
Root mean squared error: 0.14308978681972342
Relative absolute error: 9.03016412913611
Root relative squared error: 38.39501882578279
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8250383074152033
Weighted FMeasure: 0.9303526285143641
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8964106104776036
Mean absolute error: 0.02508378924760006
Coverage of cases: 93.76391982182628
Instances selection time: 8.0
Test time: 43.0
Accumulative iteration time: 180.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.15089388745426044
Kappa statistic: 0.8349676420010764
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9263910657372543
Weighted AreaUnderROC: 0.895530630864619
Root mean squared error: 0.14314107343514182
Relative absolute error: 8.947657159278828
Root relative squared error: 38.408780468792166
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8250383074152033
Weighted FMeasure: 0.9303526285143641
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8964106104776036
Mean absolute error: 0.02485460322021872
Coverage of cases: 93.76391982182628
Instances selection time: 7.0
Test time: 40.0
Accumulative iteration time: 187.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.15089388745426044
Kappa statistic: 0.8349676420010764
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.931171247535359
Weighted AreaUnderROC: 0.8982518750650956
Root mean squared error: 0.13798720690235594
Relative absolute error: 8.345425610688698
Root relative squared error: 37.025852959079714
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8373092113373981
Weighted FMeasure: 0.9346913574487395
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9012707163939779
Mean absolute error: 0.023181737807468378
Coverage of cases: 94.20935412026726
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 193.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.15034823043867027
Kappa statistic: 0.8455677112960196
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9396417836061489
Weighted AreaUnderROC: 0.9196570384578169
Root mean squared error: 0.12698446585054182
Relative absolute error: 7.226123166309012
Root relative squared error: 34.073507727397626
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8624852541321845
Weighted FMeasure: 0.9438863387246923
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9183541087138765
Mean absolute error: 0.020072564350858167
Coverage of cases: 95.10022271714922
Instances selection time: 5.0
Test time: 42.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.11502794444487302
Kappa statistic: 0.8722964150431151
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9528627113273427
Weighted AreaUnderROC: 0.9505470768931794
Root mean squared error: 0.11805460651723546
Relative absolute error: 6.373492819291926
Root relative squared error: 31.67737502754354
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8872092597435022
Weighted FMeasure: 0.9546449358976207
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9418276622032519
Mean absolute error: 0.017704146720255172
Coverage of cases: 95.7683741648107
Instances selection time: 2.0
Test time: 44.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.058598115436312465
Kappa statistic: 0.8953726529060426
Training time: 0.0
		
Time end:Sat Oct 07 11.20.22 EEST 2017