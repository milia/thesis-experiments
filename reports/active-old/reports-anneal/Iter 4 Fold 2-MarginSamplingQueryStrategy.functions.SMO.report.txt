Sat Oct 07 11.23.04 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.23.04 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9229332716827747
Weighted AreaUnderROC: 0.9065197223767151
Root mean squared error: 0.30988663398337973
Relative absolute error: 78.04008908686002
Root relative squared error: 83.15130946865243
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8194285596595761
Weighted FMeasure: 0.9282212470922448
Iteration time: 104.0
Weighted AreaUnderPRC: 0.9004295395493317
Mean absolute error: 0.21677802524127568
Coverage of cases: 99.10913140311804
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 104.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.14398292623153683
Kappa statistic: 0.8310869537016619
Training time: 97.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9279489109863338
Weighted AreaUnderROC: 0.9086425778392869
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.830973099181417
Weighted FMeasure: 0.9319719413089301
Iteration time: 98.0
Weighted AreaUnderPRC: 0.903547168149289
Mean absolute error: 0.21667196945593256
Coverage of cases: 99.10913140311804
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 202.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.14377526455394196
Kappa statistic: 0.8414112475962481
Training time: 90.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9356950029613164
Weighted AreaUnderROC: 0.9117630464298033
Root mean squared error: 0.3094585375429016
Relative absolute error: 77.94463888005123
Root relative squared error: 83.03643913963225
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.849340144781402
Weighted FMeasure: 0.9382553733280683
Iteration time: 101.0
Weighted AreaUnderPRC: 0.910176863018876
Mean absolute error: 0.2165128857779179
Coverage of cases: 99.10913140311804
Instances selection time: 13.0
Test time: 8.0
Accumulative iteration time: 303.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.14312577669955434
Kappa statistic: 0.8573376580393194
Training time: 88.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9325387557947552
Weighted AreaUnderROC: 0.9106329936099881
Root mean squared error: 0.30954420419523765
Relative absolute error: 77.96372892141298
Root relative squared error: 83.05942591459564
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8431333692069085
Weighted FMeasure: 0.9363200757852075
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9083455029765843
Mean absolute error: 0.21656591367058947
Coverage of cases: 99.10913140311804
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 402.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.14322960753835176
Kappa statistic: 0.8520242034354114
Training time: 91.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9299000462649151
Weighted AreaUnderROC: 0.9042550578839945
Root mean squared error: 0.30962984714580494
Relative absolute error: 77.98281896277474
Root relative squared error: 83.0824063297071
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8357043288291919
Weighted FMeasure: 0.9317995087557617
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9028917554934357
Mean absolute error: 0.21661894156326103
Coverage of cases: 99.10913140311804
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 515.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.15736302250019132
Kappa statistic: 0.8441201212428731
Training time: 104.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9369651291651873
Weighted AreaUnderROC: 0.9091364364271188
Root mean squared error: 0.3094585375429016
Relative absolute error: 77.94463888005124
Root relative squared error: 83.03643913963225
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8491233880301012
Weighted FMeasure: 0.9370415339655314
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9086111151834224
Mean absolute error: 0.21651288577791794
Coverage of cases: 99.10913140311804
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 628.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.1501405687610754
Kappa statistic: 0.8561108811472672
Training time: 106.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 105.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 733.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 99.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 109.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 842.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 100.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 112.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 954.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 104.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 90.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1044.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 87.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 142.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1186.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 140.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978994703511282
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 144.0
Weighted AreaUnderPRC: 0.8976556258288159
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1330.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 142.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 144.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1474.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 142.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8978744536084055
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 212.0
Weighted AreaUnderPRC: 0.8975841917144427
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 1686.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 206.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.8992131636567611
Root mean squared error: 0.30962984714580494
Relative absolute error: 77.98281896277476
Root relative squared error: 83.0824063297071
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 152.0
Weighted AreaUnderPRC: 0.8981350932553939
Mean absolute error: 0.21661894156326106
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1838.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 151.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9285627207931494
Weighted AreaUnderROC: 0.9000812158513593
Root mean squared error: 0.30962984714580494
Relative absolute error: 77.98281896277476
Root relative squared error: 83.0824063297071
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8282132172485668
Weighted FMeasure: 0.9264675034211419
Iteration time: 161.0
Weighted AreaUnderPRC: 0.8995572259375629
Mean absolute error: 0.21661894156326106
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1999.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1714964374620308
Kappa statistic: 0.8360094690564761
Training time: 160.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9442236394172338
Weighted AreaUnderROC: 0.9249519611947281
Root mean squared error: 0.3090298480646063
Relative absolute error: 77.84918867324242
Root relative squared error: 82.92140968186739
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8690370148463736
Weighted FMeasure: 0.9468996893618163
Iteration time: 167.0
Weighted AreaUnderPRC: 0.9212841389343484
Mean absolute error: 0.21624774631456015
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 2166.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.12193890566759663
Kappa statistic: 0.8766999686159641
Training time: 166.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9441929095813052
Weighted AreaUnderROC: 0.9327408752342927
Root mean squared error: 0.3090298480646063
Relative absolute error: 77.84918867324241
Root relative squared error: 82.92140968186739
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8704543187515102
Weighted FMeasure: 0.9485693755779393
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9261050364016268
Mean absolute error: 0.2162477463145601
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 2338.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.10089452948303353
Kappa statistic: 0.8797382786592522
Training time: 172.0
		
Time end:Sat Oct 07 11.23.08 EEST 2017