Sat Oct 07 11.19.12 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.19.12 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.267260579064562
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9554920370765934
Weighted AreaUnderROC: 0.9766543215650879
Root mean squared error: 0.11605763976625251
Relative absolute error: 8.774558748907621
Root relative squared error: 31.141532619062964
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8833094123885586
Weighted FMeasure: 0.9505359645094003
Iteration time: 35.0
Weighted AreaUnderPRC: 0.9570614589103557
Mean absolute error: 0.024373774302520926
Coverage of cases: 98.21826280623608
Instances selection time: 34.0
Test time: 33.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.05375379025015808
Kappa statistic: 0.8794851460989446
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.935412026726123
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9676518269662475
Weighted AreaUnderROC: 0.9812823180334307
Root mean squared error: 0.10326418631225767
Relative absolute error: 8.782420345456654
Root relative squared error: 27.708688828249514
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9145703186764779
Weighted FMeasure: 0.9649390131245235
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9752017172842365
Mean absolute error: 0.024395612070712683
Coverage of cases: 98.44097995545657
Instances selection time: 29.0
Test time: 30.0
Accumulative iteration time: 65.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.05184399069559238
Kappa statistic: 0.9163354037267082
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 20.304380103934662
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9695024536384206
Weighted AreaUnderROC: 0.9797535960617938
Root mean squared error: 0.10253833105894485
Relative absolute error: 7.416153818463934
Root relative squared error: 27.513921425661575
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9200413440800307
Weighted FMeasure: 0.9636255652291288
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9673855430182224
Mean absolute error: 0.020600427273510726
Coverage of cases: 97.7728285077951
Instances selection time: 19.0
Test time: 27.0
Accumulative iteration time: 85.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.017718849571772992
Kappa statistic: 0.9136185460404491
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 20.63845582776547
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9762009945538666
Weighted AreaUnderROC: 0.9827928588536688
Root mean squared error: 0.10332690421028566
Relative absolute error: 8.225776159548857
Root relative squared error: 27.72551780625709
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9409417892741928
Weighted FMeasure: 0.9719530204193965
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9771031192142473
Mean absolute error: 0.022849378220968823
Coverage of cases: 98.21826280623608
Instances selection time: 18.0
Test time: 26.0
Accumulative iteration time: 103.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.01679653320959026
Kappa statistic: 0.934264625144879
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.00742390497406
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9599867780230441
Weighted AreaUnderROC: 0.9792134155328165
Root mean squared error: 0.1174490033465601
Relative absolute error: 8.69106310137498
Root relative squared error: 31.514874644701194
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8906168326588646
Weighted FMeasure: 0.9513324204386554
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9639650395211491
Mean absolute error: 0.024141841948263593
Coverage of cases: 97.7728285077951
Instances selection time: 30.0
Test time: 54.0
Accumulative iteration time: 134.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.01851083227355525
Kappa statistic: 0.8838824953860983
Training time: 1.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 19.673348181143336
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9716261023767055
Weighted AreaUnderROC: 0.9809620548108933
Root mean squared error: 0.10449782435258274
Relative absolute error: 7.2276951793001665
Root relative squared error: 28.039708650385094
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9253041176185107
Weighted FMeasure: 0.965801955454174
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9697132503848255
Mean absolute error: 0.02007693105361137
Coverage of cases: 97.99554565701558
Instances selection time: 31.0
Test time: 52.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.017615018732975562
Kappa statistic: 0.9187026217951814
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.413511507052768
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9701984873465734
Weighted AreaUnderROC: 0.9803737519454536
Root mean squared error: 0.10780987897923865
Relative absolute error: 7.217321131519045
Root relative squared error: 28.928426165232533
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9202329572655565
Weighted FMeasure: 0.963777579189606
Iteration time: 19.0
Weighted AreaUnderPRC: 0.961898353135716
Mean absolute error: 0.02004811425421937
Coverage of cases: 97.7728285077951
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 185.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.017887847240770666
Kappa statistic: 0.9135884143041004
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.413511507052768
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9688156760986822
Weighted AreaUnderROC: 0.9800733615657594
Root mean squared error: 0.10924454063421053
Relative absolute error: 7.113691380823916
Root relative squared error: 29.313386283460076
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9152293005870205
Weighted FMeasure: 0.9617654362912536
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9606479376910076
Mean absolute error: 0.019760253835621792
Coverage of cases: 97.7728285077951
Instances selection time: 12.0
Test time: 27.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.018160675748565766
Kappa statistic: 0.9085101282512286
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 18.671121009651117
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9653516653168658
Weighted AreaUnderROC: 0.9797443596037934
Root mean squared error: 0.1122370015227089
Relative absolute error: 7.069515330806721
Root relative squared error: 30.11634779946309
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.9051667532906119
Weighted FMeasure: 0.9575784367001822
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9594956477918869
Mean absolute error: 0.019637542585574028
Coverage of cases: 97.55011135857461
Instances selection time: 12.0
Test time: 27.0
Accumulative iteration time: 211.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.018537335095158293
Kappa statistic: 0.8984948539472901
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.930957683741667
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9555378935644261
Weighted AreaUnderROC: 0.9794796369530461
Root mean squared error: 0.12349628708019986
Relative absolute error: 8.452453170657607
Root relative squared error: 33.13753114561887
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8702781707113949
Weighted FMeasure: 0.9446391832530704
Iteration time: 12.0
Weighted AreaUnderPRC: 0.964895581111896
Mean absolute error: 0.023479036585159787
Coverage of cases: 97.55011135857461
Instances selection time: 11.0
Test time: 27.0
Accumulative iteration time: 223.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.02010913931172866
Kappa statistic: 0.865027979466309
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 19.153674832962203
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9628474546936842
Weighted AreaUnderROC: 0.9800703315653464
Root mean squared error: 0.11598257187836371
Relative absolute error: 7.706172789616141
Root relative squared error: 31.12138979103336
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8930552736716341
Weighted FMeasure: 0.9572728839943783
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9708617256632853
Mean absolute error: 0.02140603552671129
Coverage of cases: 97.7728285077951
Instances selection time: 11.0
Test time: 26.0
Accumulative iteration time: 235.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.01796517525796505
Kappa statistic: 0.8949670748681241
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 19.00519673348186
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9573118961130969
Weighted AreaUnderROC: 0.9780060991641762
Root mean squared error: 0.12119721955541406
Relative absolute error: 7.997440135646317
Root relative squared error: 32.52062659318488
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.874569940923411
Weighted FMeasure: 0.9491441606456814
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9659676422376287
Mean absolute error: 0.022215111487906215
Coverage of cases: 97.32739420935413
Instances selection time: 8.0
Test time: 26.0
Accumulative iteration time: 244.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.018887491620147783
Kappa statistic: 0.875706475351219
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.819599109131424
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9546854660648554
Weighted AreaUnderROC: 0.9751249024341879
Root mean squared error: 0.12818806325823953
Relative absolute error: 8.450449617884743
Root relative squared error: 34.39646680193618
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8679968821017976
Weighted FMeasure: 0.9450058457847412
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9615355442087912
Mean absolute error: 0.023473471160790724
Coverage of cases: 97.10467706013362
Instances selection time: 7.0
Test time: 30.0
Accumulative iteration time: 253.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.01943314863573798
Kappa statistic: 0.8657327536631934
Training time: 2.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.893838158871592
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9546854660648554
Weighted AreaUnderROC: 0.9748926504359657
Root mean squared error: 0.1316181914460952
Relative absolute error: 9.003204930800878
Root relative squared error: 35.3168667778862
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8679968821017976
Weighted FMeasure: 0.9450058457847412
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9609150191675723
Mean absolute error: 0.025008902585557748
Coverage of cases: 97.10467706013362
Instances selection time: 6.0
Test time: 25.0
Accumulative iteration time: 261.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.01943314863573798
Kappa statistic: 0.8657327536631934
Training time: 2.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.89383815887158
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9573118961130969
Weighted AreaUnderROC: 0.974411082277837
Root mean squared error: 0.1301015516269963
Relative absolute error: 8.603975005139295
Root relative squared error: 34.90990960993962
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.874569940923411
Weighted FMeasure: 0.9491441606456814
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9624201826951494
Mean absolute error: 0.02389993056983114
Coverage of cases: 96.65924276169265
Instances selection time: 5.0
Test time: 29.0
Accumulative iteration time: 268.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.018887491620147783
Kappa statistic: 0.875706475351219
Training time: 2.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.04231625835189
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9522148657943792
Weighted AreaUnderROC: 0.9714709334057627
Root mean squared error: 0.13932196013261106
Relative absolute error: 9.622422485038923
Root relative squared error: 37.384004833804255
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8613143231296819
Weighted FMeasure: 0.9404974791417902
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9580728034458528
Mean absolute error: 0.026728951347330075
Coverage of cases: 96.21380846325167
Instances selection time: 6.0
Test time: 26.0
Accumulative iteration time: 275.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.01997880565132818
Kappa statistic: 0.8558157671399408
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 19.153674832962174
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9548142478686813
Weighted AreaUnderROC: 0.975308266482527
Root mean squared error: 0.12928673937260923
Relative absolute error: 8.869773138235082
Root relative squared error: 34.6912725391745
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8701259241989583
Weighted FMeasure: 0.9444663896911101
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9603090723991227
Mean absolute error: 0.02463825871731943
Coverage of cases: 97.10467706013362
Instances selection time: 2.0
Test time: 28.0
Accumulative iteration time: 279.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.019771143973733318
Kappa statistic: 0.8651184286539573
Training time: 2.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.190794357832257
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9562813981063304
Weighted AreaUnderROC: 0.975087247379933
Root mean squared error: 0.12731610738033758
Relative absolute error: 8.767833227094755
Root relative squared error: 34.16249648797187
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8746591499997727
Weighted FMeasure: 0.9465007416675832
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9596338922429277
Mean absolute error: 0.02435509229748519
Coverage of cases: 97.10467706013362
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 290.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.019667313134935884
Kappa statistic: 0.8698218676068098
Training time: 4.0
		
Time end:Sat Oct 07 11.19.13 EEST 2017