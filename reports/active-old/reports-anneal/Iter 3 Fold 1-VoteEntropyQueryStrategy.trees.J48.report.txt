Sat Oct 07 11.25.19 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.19 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 18.856718634001414
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9558649813438233
Weighted AreaUnderROC: 0.9789344753318913
Root mean squared error: 0.09654597419904531
Relative absolute error: 5.879732739420987
Root relative squared error: 25.906003351560862
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9000770931386607
Weighted FMeasure: 0.9595121506917188
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9713996615702016
Mean absolute error: 0.016332590942835915
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.08618932482532743
Kappa statistic: 0.9081000869876681
Training time: 2.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.18856718633997
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9605721227547508
Weighted AreaUnderROC: 0.9695994624207921
Root mean squared error: 0.10152803878272136
Relative absolute error: 5.091160369167217
Root relative squared error: 27.24283156084813
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9123333018295989
Weighted FMeasure: 0.9640829059113011
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9611885351644972
Mean absolute error: 0.014142112136575463
Coverage of cases: 97.10467706013362
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.07879160775305348
Kappa statistic: 0.9195875761142097
Training time: 12.0
		
Time end:Sat Oct 07 11.25.19 EEST 2017