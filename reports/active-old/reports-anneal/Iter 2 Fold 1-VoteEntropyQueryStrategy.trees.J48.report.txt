Sat Oct 07 11.25.18 EEST 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.18 EEST 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.380846325167134
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9602035961332454
Weighted AreaUnderROC: 0.9578348813920718
Root mean squared error: 0.1043702776242112
Relative absolute error: 6.555744792349071
Root relative squared error: 28.005484271791513
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9128015285680516
Weighted FMeasure: 0.964448974769066
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9517131349164334
Mean absolute error: 0.01821040220096946
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.06526275097284401
Kappa statistic: 0.9207144028354124
Training time: 2.0
		
Iteration: 2
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.114328136599767
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9445827790712379
Weighted AreaUnderROC: 0.9407434201418622
Root mean squared error: 0.12624092969919937
Relative absolute error: 7.962400918535399
Root relative squared error: 33.87399604202199
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8613116259181952
Weighted FMeasure: 0.9387563531494487
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9302553724906819
Mean absolute error: 0.02211778032926478
Coverage of cases: 95.32293986636971
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14976390941448287
Kappa statistic: 0.8669056024145085
Training time: 12.0
		
Time end:Sat Oct 07 11.25.18 EEST 2017