Sat Oct 07 11.34.48 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.48 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 98.84057971014492
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8345171700060245
Weighted AreaUnderROC: 0.8666768790849673
Root mean squared error: 0.36878147925526955
Relative absolute error: 43.56241234221599
Root relative squared error: 73.7562958510539
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6644617426715108
Weighted FMeasure: 0.8344721796537528
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8256028554256453
Mean absolute error: 0.21781206171107997
Coverage of cases: 98.84057971014492
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17281365444728616
Kappa statistic: 0.6641733695373737
Training time: 4.0
		
Time end:Sat Oct 07 11.34.48 EEST 2017