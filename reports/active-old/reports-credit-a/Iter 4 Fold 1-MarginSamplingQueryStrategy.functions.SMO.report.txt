Sat Oct 07 11.35.50 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.35.50 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8532736934977532
Weighted AreaUnderROC: 0.8481413398692811
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6928710658365983
Weighted FMeasure: 0.8409684583816984
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7967195592551919
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.14429703040636543
Kappa statistic: 0.6831321076098391
Training time: 12.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8366753781579156
Weighted AreaUnderROC: 0.8356311274509803
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6681985134197985
Weighted FMeasure: 0.8351602484472049
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7822564024869643
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.1635203537936914
Kappa statistic: 0.6672757728034109
Training time: 15.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8597898682716929
Weighted AreaUnderROC: 0.8591196895424836
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7149612269711613
Weighted FMeasure: 0.8582956521739129
Iteration time: 17.0
Weighted AreaUnderPRC: 0.80981404645389
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13973163540778633
Kappa statistic: 0.7139739099538094
Training time: 16.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8553126153391896
Weighted AreaUnderROC: 0.8538602941176471
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7068308723826378
Weighted FMeasure: 0.8551636162608165
Iteration time: 20.0
Weighted AreaUnderPRC: 0.804364964487565
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 66.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.14735187553282184
Kappa statistic: 0.7067822539520653
Training time: 18.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8519815445549279
Weighted AreaUnderROC: 0.8486008986928105
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.6998272627612473
Weighted FMeasure: 0.8518961607428315
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7993374233627369
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.15497211565785735
Kappa statistic: 0.6995235411650186
Training time: 20.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8547333392792886
Weighted AreaUnderROC: 0.843954248366013
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7011862265350106
Weighted FMeasure: 0.8507654491029443
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7975136867290066
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.1642654163114521
Kappa statistic: 0.6966954543103894
Training time: 20.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8408141062212258
Weighted AreaUnderROC: 0.8105596405228758
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6574884477845405
Weighted FMeasure: 0.8212142332862509
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7663766616114798
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 136.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20496767547598752
Kappa statistic: 0.6380612673101134
Training time: 25.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8387791340629132
Weighted AreaUnderROC: 0.7954963235294118
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6415819127608289
Weighted FMeasure: 0.8070627128831898
Iteration time: 53.0
Weighted AreaUnderPRC: 0.753841336329069
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 189.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.2235001065643649
Kappa statistic: 0.6112812929122213
Training time: 52.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8367328681338344
Weighted AreaUnderROC: 0.7922283496732027
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6362758622888584
Weighted FMeasure: 0.8037981009495253
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7505547966967755
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 227.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.2271375035521455
Kappa statistic: 0.604936314148301
Training time: 37.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8539639740853087
Weighted AreaUnderROC: 0.8393586601307189
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6968888743502082
Weighted FMeasure: 0.8472946255282872
Iteration time: 51.0
Weighted AreaUnderPRC: 0.7937624443217854
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 278.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.1705580420574027
Kappa statistic: 0.6897052718970528
Training time: 50.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 12.753623188405797
Correctly Classified Instances: 87.2463768115942
Weighted Precision: 0.8737973403463806
Weighted AreaUnderROC: 0.8661662581699346
Root mean squared error: 0.3571221526089609
Relative absolute error: 25.507246376811594
Root relative squared error: 71.42443052179217
Weighted TruePositiveRate: 0.8724637681159421
Weighted MatthewsCorrelation: 0.7417603880527684
Weighted FMeasure: 0.8716494922498138
Iteration time: 55.0
Weighted AreaUnderPRC: 0.822927318724608
Mean absolute error: 0.12753623188405797
Coverage of cases: 87.2463768115942
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 333.0
Weighted Recall: 0.8724637681159421
Weighted FalsePositiveRate: 0.14013125177607275
Kappa statistic: 0.7392021441088549
Training time: 54.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8553126153391896
Weighted AreaUnderROC: 0.8538602941176471
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7068308723826378
Weighted FMeasure: 0.8551636162608165
Iteration time: 65.0
Weighted AreaUnderPRC: 0.804364964487565
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 398.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.14735187553282184
Kappa statistic: 0.7067822539520653
Training time: 65.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8466966106945183
Weighted AreaUnderROC: 0.8448223039215688
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6852268935378812
Weighted FMeasure: 0.8410903926329775
Iteration time: 102.0
Weighted AreaUnderPRC: 0.7924812977942083
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 500.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15093510230179027
Kappa statistic: 0.6810549140234985
Training time: 101.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.8670853758169935
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8197257029657302
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 678.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 178.0
		
Time end:Sat Oct 07 11.35.52 EEST 2017