Sat Oct 07 11.36.56 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.56 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 92.30434782608697
Incorrectly Classified Instances: 20.666666666666664
Correctly Classified Instances: 79.33333333333334
Weighted Precision: 0.801874001575632
Weighted AreaUnderROC: 0.8782665245672006
Root mean squared error: 0.37771733132108865
Relative absolute error: 59.16828839079301
Root relative squared error: 75.54346626421774
Weighted TruePositiveRate: 0.7933333333333332
Weighted MatthewsCorrelation: 0.5917225116973348
Weighted FMeasure: 0.7926037977562793
Iteration time: 4.9
Weighted AreaUnderPRC: 0.8674884293254784
Mean absolute error: 0.2958414419539651
Coverage of cases: 99.21739130434784
Instances selection time: 1.1
Test time: 1.8
Accumulative iteration time: 4.9
Weighted Recall: 0.7933333333333332
Weighted FalsePositiveRate: 0.2047778045257449
Kappa statistic: 0.5849495729996622
Training time: 3.8
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 89.13043478260872
Incorrectly Classified Instances: 16.84057971014493
Correctly Classified Instances: 83.15942028985508
Weighted Precision: 0.8385459918135909
Weighted AreaUnderROC: 0.8931906079586422
Root mean squared error: 0.3613089328879282
Relative absolute error: 53.064406431580245
Root relative squared error: 72.26178657758564
Weighted TruePositiveRate: 0.8315942028985507
Weighted MatthewsCorrelation: 0.6672346391036329
Weighted FMeasure: 0.8314843620124952
Iteration time: 4.8
Weighted AreaUnderPRC: 0.8823431404001975
Mean absolute error: 0.2653220321579012
Coverage of cases: 98.63768115942028
Instances selection time: 0.9
Test time: 2.0
Accumulative iteration time: 9.7
Weighted Recall: 0.8315942028985507
Weighted FalsePositiveRate: 0.1654146332402892
Kappa statistic: 0.6614367130738359
Training time: 3.9
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 88.95652173913042
Incorrectly Classified Instances: 17.6231884057971
Correctly Classified Instances: 82.3768115942029
Weighted Precision: 0.827510230092383
Weighted AreaUnderROC: 0.8887838745964244
Root mean squared error: 0.36462818759569166
Relative absolute error: 53.65449314729142
Root relative squared error: 72.92563751913832
Weighted TruePositiveRate: 0.8237681159420289
Weighted MatthewsCorrelation: 0.6477229399827698
Weighted FMeasure: 0.8237255277997235
Iteration time: 5.4
Weighted AreaUnderPRC: 0.8748677657422348
Mean absolute error: 0.26827246573645713
Coverage of cases: 98.37681159420289
Instances selection time: 0.6
Test time: 1.7
Accumulative iteration time: 15.1
Weighted Recall: 0.8237681159420289
Weighted FalsePositiveRate: 0.1761752323067408
Kappa statistic: 0.6446937678125797
Training time: 4.8
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 88.33333333333334
Incorrectly Classified Instances: 16.84057971014493
Correctly Classified Instances: 83.15942028985508
Weighted Precision: 0.8352032101383216
Weighted AreaUnderROC: 0.8919599265045228
Root mean squared error: 0.361227660686655
Relative absolute error: 52.76480149421489
Root relative squared error: 72.245532137331
Weighted TruePositiveRate: 0.8315942028985507
Weighted MatthewsCorrelation: 0.6627473309835313
Weighted FMeasure: 0.8312380921036878
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8805237764129267
Mean absolute error: 0.26382400747107443
Coverage of cases: 98.55072463768116
Instances selection time: 0.8
Test time: 1.7
Accumulative iteration time: 21.1
Weighted Recall: 0.8315942028985507
Weighted FalsePositiveRate: 0.1716107748335281
Kappa statistic: 0.6591760901355902
Training time: 5.2
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 87.5072463768116
Incorrectly Classified Instances: 17.942028985507246
Correctly Classified Instances: 82.05797101449276
Weighted Precision: 0.8239606470285926
Weighted AreaUnderROC: 0.8856217326939675
Root mean squared error: 0.36303845390581946
Relative absolute error: 51.944441241341096
Root relative squared error: 72.60769078116388
Weighted TruePositiveRate: 0.8205797101449275
Weighted MatthewsCorrelation: 0.6404927248670291
Weighted FMeasure: 0.8202770224027649
Iteration time: 7.3
Weighted AreaUnderPRC: 0.8760890337477587
Mean absolute error: 0.25972220620670555
Coverage of cases: 98.81159420289853
Instances selection time: 0.7
Test time: 1.8
Accumulative iteration time: 28.4
Weighted Recall: 0.8205797101449275
Weighted FalsePositiveRate: 0.18257240981086403
Kappa statistic: 0.6373908687033578
Training time: 6.6
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 87.47826086956522
Incorrectly Classified Instances: 17.159420289855074
Correctly Classified Instances: 82.84057971014494
Weighted Precision: 0.8312128612481446
Weighted AreaUnderROC: 0.8920328009610321
Root mean squared error: 0.3575777710999872
Relative absolute error: 51.22050356666938
Root relative squared error: 71.51555421999743
Weighted TruePositiveRate: 0.8284057971014495
Weighted MatthewsCorrelation: 0.6556141860171957
Weighted FMeasure: 0.82813260282005
Iteration time: 8.2
Weighted AreaUnderPRC: 0.8790869286529007
Mean absolute error: 0.2561025178333468
Coverage of cases: 98.57971014492753
Instances selection time: 0.5
Test time: 1.6
Accumulative iteration time: 36.6
Weighted Recall: 0.8284057971014495
Weighted FalsePositiveRate: 0.17414818873524068
Kappa statistic: 0.65296454650501
Training time: 7.7
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 86.76811594202897
Incorrectly Classified Instances: 15.594202898550725
Correctly Classified Instances: 84.40579710144928
Weighted Precision: 0.8459484891914302
Weighted AreaUnderROC: 0.9005187500768057
Root mean squared error: 0.3494895839001438
Relative absolute error: 48.62282080800219
Root relative squared error: 69.89791678002874
Weighted TruePositiveRate: 0.8440579710144928
Weighted MatthewsCorrelation: 0.6866349518567777
Weighted FMeasure: 0.8441061352709207
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8883721279484016
Mean absolute error: 0.24311410404001094
Coverage of cases: 98.46376811594203
Instances selection time: 0.6
Test time: 1.6
Accumulative iteration time: 45.6
Weighted Recall: 0.8440579710144928
Weighted FalsePositiveRate: 0.15711089357571145
Kappa statistic: 0.6851666094497021
Training time: 8.4
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 87.14492753623188
Incorrectly Classified Instances: 16.985507246376812
Correctly Classified Instances: 83.01449275362319
Weighted Precision: 0.8325670088472075
Weighted AreaUnderROC: 0.8927923928244417
Root mean squared error: 0.3573680503159488
Relative absolute error: 50.59683203756945
Root relative squared error: 71.47361006318975
Weighted TruePositiveRate: 0.8301449275362319
Weighted MatthewsCorrelation: 0.6588271625290166
Weighted FMeasure: 0.8299987903195809
Iteration time: 11.2
Weighted AreaUnderPRC: 0.8802715314544265
Mean absolute error: 0.2529841601878472
Coverage of cases: 98.40579710144925
Instances selection time: 0.9
Test time: 1.9
Accumulative iteration time: 56.8
Weighted Recall: 0.8301449275362319
Weighted FalsePositiveRate: 0.1720936734593195
Kappa statistic: 0.6566490137948602
Training time: 10.3
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 87.34782608695653
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8331161861774568
Weighted AreaUnderROC: 0.9018248920147286
Root mean squared error: 0.35040213551363164
Relative absolute error: 49.90664112033302
Root relative squared error: 70.08042710272632
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6610917380194211
Weighted FMeasure: 0.8318590308004369
Iteration time: 10.7
Weighted AreaUnderPRC: 0.8904544788436353
Mean absolute error: 0.24953320560166517
Coverage of cases: 98.75362318840578
Instances selection time: 0.6
Test time: 1.6
Accumulative iteration time: 67.5
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.17102426109335497
Kappa statistic: 0.6600570697529049
Training time: 10.1
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 87.2608695652174
Incorrectly Classified Instances: 15.507246376811594
Correctly Classified Instances: 84.4927536231884
Weighted Precision: 0.8458961377341666
Weighted AreaUnderROC: 0.9081623435568174
Root mean squared error: 0.34450063030640615
Relative absolute error: 48.84445999365035
Root relative squared error: 68.90012606128121
Weighted TruePositiveRate: 0.8449275362318842
Weighted MatthewsCorrelation: 0.6871217724043135
Weighted FMeasure: 0.844874064490354
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8975609822856654
Mean absolute error: 0.24422229996825168
Coverage of cases: 98.78260869565219
Instances selection time: 0.4
Test time: 1.8
Accumulative iteration time: 79.5
Weighted Recall: 0.8449275362318842
Weighted FalsePositiveRate: 0.15832784003101757
Kappa statistic: 0.6862333598592356
Training time: 11.6
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 87.47826086956522
Incorrectly Classified Instances: 15.73913043478261
Correctly Classified Instances: 84.26086956521739
Weighted Precision: 0.8443053409005321
Weighted AreaUnderROC: 0.9061664386876161
Root mean squared error: 0.346738123557785
Relative absolute error: 49.533995141866065
Root relative squared error: 69.347624711557
Weighted TruePositiveRate: 0.8426086956521738
Weighted MatthewsCorrelation: 0.6833591579217764
Weighted FMeasure: 0.842577553380643
Iteration time: 12.6
Weighted AreaUnderPRC: 0.893740502048669
Mean absolute error: 0.24766997570933036
Coverage of cases: 98.66666666666667
Instances selection time: 0.5
Test time: 1.8
Accumulative iteration time: 92.1
Weighted Recall: 0.8426086956521738
Weighted FalsePositiveRate: 0.15961369702206982
Kappa statistic: 0.6819383290710767
Training time: 12.1
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 85.08695652173913
Incorrectly Classified Instances: 14.608695652173912
Correctly Classified Instances: 85.39130434782608
Weighted Precision: 0.8553945416597555
Weighted AreaUnderROC: 0.9109388221428747
Root mean squared error: 0.33843889345714595
Relative absolute error: 46.64178763254226
Root relative squared error: 67.68777869142919
Weighted TruePositiveRate: 0.8539130434782608
Weighted MatthewsCorrelation: 0.706077703853944
Weighted FMeasure: 0.853961908494629
Iteration time: 13.4
Weighted AreaUnderPRC: 0.8997288322477843
Mean absolute error: 0.23320893816271132
Coverage of cases: 98.60869565217392
Instances selection time: 0.2
Test time: 2.0
Accumulative iteration time: 105.5
Weighted Recall: 0.8539130434782608
Weighted FalsePositiveRate: 0.1478418305848852
Kappa statistic: 0.7049292324694196
Training time: 13.2
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 86.02898550724639
Incorrectly Classified Instances: 16.20289855072464
Correctly Classified Instances: 83.79710144927537
Weighted Precision: 0.8397946658339055
Weighted AreaUnderROC: 0.9045782050586297
Root mean squared error: 0.34743848657628673
Relative absolute error: 48.18566574680152
Root relative squared error: 69.48769731525735
Weighted TruePositiveRate: 0.8379710144927536
Weighted MatthewsCorrelation: 0.6736883497479196
Weighted FMeasure: 0.8376981315057238
Iteration time: 15.3
Weighted AreaUnderPRC: 0.8931424102484943
Mean absolute error: 0.24092832873400755
Coverage of cases: 98.57971014492753
Instances selection time: 0.8
Test time: 2.3
Accumulative iteration time: 120.8
Weighted Recall: 0.8379710144927536
Weighted FalsePositiveRate: 0.1668081235669084
Kappa statistic: 0.6717297186696207
Training time: 14.5
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 85.81159420289853
Incorrectly Classified Instances: 15.73913043478261
Correctly Classified Instances: 84.26086956521739
Weighted Precision: 0.8435346439471498
Weighted AreaUnderROC: 0.9022673650182215
Root mean squared error: 0.34854519906759496
Relative absolute error: 48.33200126499388
Root relative squared error: 69.70903981351898
Weighted TruePositiveRate: 0.842608695652174
Weighted MatthewsCorrelation: 0.6817941134916156
Weighted FMeasure: 0.8422069243822232
Iteration time: 15.7
Weighted AreaUnderPRC: 0.8904153997431526
Mean absolute error: 0.24166000632496942
Coverage of cases: 98.3768115942029
Instances selection time: 0.3
Test time: 2.0
Accumulative iteration time: 136.5
Weighted Recall: 0.842608695652174
Weighted FalsePositiveRate: 0.16405303740997806
Kappa statistic: 0.6803744854912257
Training time: 15.4
		
Time end:Sat Oct 07 11.37.02 EEST 2017