Sat Oct 07 11.36.24 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.24 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.769763696962888
Weighted AreaUnderROC: 0.8577038635502511
Root mean squared error: 0.452543829885716
Relative absolute error: 47.28674434403373
Root relative squared error: 90.5087659771432
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5280668629593211
Weighted FMeasure: 0.7650686546589032
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8452864744173583
Mean absolute error: 0.23643372172016863
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.25381855640807044
Kappa statistic: 0.5226234952262349
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7624123637139125
Weighted AreaUnderROC: 0.8689187269110542
Root mean squared error: 0.468920287314712
Relative absolute error: 50.002138040169456
Root relative squared error: 93.7840574629424
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5025377574554353
Weighted FMeasure: 0.7464479081214112
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8582742826210505
Mean absolute error: 0.25001069020084726
Coverage of cases: 83.76811594202898
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.2799712276214834
Kappa statistic: 0.4869035746155058
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 58.11594202898551
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7713643402296462
Weighted AreaUnderROC: 0.8666234488964669
Root mean squared error: 0.4638917104881074
Relative absolute error: 48.479094425573756
Root relative squared error: 92.77834209762148
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.524791812358165
Weighted FMeasure: 0.7598621089458794
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8540357485695177
Mean absolute error: 0.24239547212786877
Coverage of cases: 83.18840579710145
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2640940252912759
Kappa statistic: 0.5130430236813216
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 58.405797101449274
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.75228668851415
Weighted AreaUnderROC: 0.8658890428151937
Root mean squared error: 0.46858905580181076
Relative absolute error: 49.71313599230183
Root relative squared error: 93.71781116036215
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.4834410290188203
Weighted FMeasure: 0.7377808754865013
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8541245437433064
Mean absolute error: 0.24856567996150913
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.2882281898266553
Kappa statistic: 0.4691565253881661
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 59.42028985507246
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7511531190926276
Weighted AreaUnderROC: 0.8666718468788481
Root mean squared error: 0.467191028830871
Relative absolute error: 50.15193608199683
Root relative squared error: 93.43820576617419
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.4826969272011287
Weighted FMeasure: 0.7383283459069786
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8569893281605107
Mean absolute error: 0.25075968040998414
Coverage of cases: 84.92753623188406
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 6.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.28690057544757036
Kappa statistic: 0.469879518072289
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7589444843023123
Weighted AreaUnderROC: 0.8646676967888606
Root mean squared error: 0.46355847452718485
Relative absolute error: 49.23412575161951
Root relative squared error: 92.71169490543697
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5001938672099322
Weighted FMeasure: 0.7480034476592562
Iteration time: 1.0
Weighted AreaUnderPRC: 0.853233568603196
Mean absolute error: 0.24617062875809753
Coverage of cases: 84.6376811594203
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.27598838448422847
Kappa statistic: 0.4889957655915103
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 58.405797101449274
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7396997721952632
Weighted AreaUnderROC: 0.869058740646017
Root mean squared error: 0.4843406122432919
Relative absolute error: 52.36429210220869
Root relative squared error: 96.86812244865838
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.45033795224373363
Weighted FMeasure: 0.7166233595471849
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8550569614449822
Mean absolute error: 0.26182146051104344
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.3127078005115089
Kappa statistic: 0.42906939896482504
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 58.11594202898551
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7382194616977227
Weighted AreaUnderROC: 0.8653822700577816
Root mean squared error: 0.4863602837564668
Relative absolute error: 52.62760504722003
Root relative squared error: 97.27205675129336
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.4493163969586926
Weighted FMeasure: 0.7173276410890163
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8523172529565938
Mean absolute error: 0.26313802523610014
Coverage of cases: 82.31884057971014
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.31138018613242396
Kappa statistic: 0.4298523206751054
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 57.971014492753625
Incorrectly Classified Instances: 27.536231884057973
Correctly Classified Instances: 72.46376811594203
Weighted Precision: 0.7356593208343163
Weighted AreaUnderROC: 0.87072632495027
Root mean squared error: 0.48553129829839525
Relative absolute error: 52.5598417906074
Root relative squared error: 97.10625965967905
Weighted TruePositiveRate: 0.7246376811594203
Weighted MatthewsCorrelation: 0.4434578999421942
Weighted FMeasure: 0.7139677346380863
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8614412640268074
Mean absolute error: 0.262799208953037
Coverage of cases: 82.6086956521739
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7246376811594203
Weighted FalsePositiveRate: 0.31501758312020456
Kappa statistic: 0.4233915659471156
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 58.26086956521739
Incorrectly Classified Instances: 27.82608695652174
Correctly Classified Instances: 72.17391304347827
Weighted Precision: 0.7362575918249301
Weighted AreaUnderROC: 0.8769100182343469
Root mean squared error: 0.4821170935870484
Relative absolute error: 52.31635947727338
Root relative squared error: 96.42341871740967
Weighted TruePositiveRate: 0.7217391304347827
Weighted MatthewsCorrelation: 0.4397811558877692
Weighted FMeasure: 0.7090785331404748
Iteration time: 1.0
Weighted AreaUnderPRC: 0.872006787174059
Mean absolute error: 0.2615817973863669
Coverage of cases: 83.47826086956522
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7217391304347827
Weighted FalsePositiveRate: 0.32131020886615513
Kappa statistic: 0.415316174134096
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 58.405797101449274
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7479629930206134
Weighted AreaUnderROC: 0.8753670550345741
Root mean squared error: 0.4772918178824087
Relative absolute error: 51.39292984554511
Root relative squared error: 95.45836357648174
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.46428007479892425
Weighted FMeasure: 0.7219388860796069
Iteration time: 1.0
Weighted AreaUnderPRC: 0.868146823012101
Mean absolute error: 0.25696464922772555
Coverage of cases: 83.18840579710145
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.3080882352941176
Kappa statistic: 0.4404484874127353
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 58.69565217391305
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7438003220611915
Weighted AreaUnderROC: 0.8790432296106849
Root mean squared error: 0.4798180254167756
Relative absolute error: 52.118979153651445
Root relative squared error: 95.96360508335512
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.45727810966172894
Weighted FMeasure: 0.71928035982009
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8757293925735823
Mean absolute error: 0.26059489576825723
Coverage of cases: 83.47826086956522
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.3103980179028133
Kappa statistic: 0.4347550340891074
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 58.84057971014493
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7438003220611915
Weighted AreaUnderROC: 0.8746897792933599
Root mean squared error: 0.48169505671375673
Relative absolute error: 52.35046797078845
Root relative squared error: 96.33901134275135
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.45727810966172894
Weighted FMeasure: 0.71928035982009
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8707321405218535
Mean absolute error: 0.26175233985394225
Coverage of cases: 83.76811594202898
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.3103980179028133
Kappa statistic: 0.4347550340891074
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 57.391304347826086
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7587873784626674
Weighted AreaUnderROC: 0.8766831249407976
Root mean squared error: 0.47291000917885656
Relative absolute error: 49.900109481456056
Root relative squared error: 94.58200183577131
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.49199621510227853
Weighted FMeasure: 0.7393128610519915
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8729446320361623
Mean absolute error: 0.24950054740728028
Coverage of cases: 83.18840579710145
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.2885736359761296
Kappa statistic: 0.4733933364913943
Training time: 0.0
		
Time end:Sat Oct 07 11.36.24 EEST 2017