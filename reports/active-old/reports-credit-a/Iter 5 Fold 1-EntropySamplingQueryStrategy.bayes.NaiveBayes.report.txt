Sat Oct 07 11.34.03 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.03 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8026488317527857
Weighted AreaUnderROC: 0.869113298113981
Root mean squared error: 0.4248962445397913
Relative absolute error: 43.39408798865777
Root relative squared error: 84.97924890795827
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5873257052662327
Weighted FMeasure: 0.7896473456686156
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8547667214418598
Mean absolute error: 0.21697043994328885
Coverage of cases: 87.2463768115942
Instances selection time: 11.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.23259958040290385
Kappa statistic: 0.5742443467227505
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8018695259416363
Weighted AreaUnderROC: 0.8715802294677778
Root mean squared error: 0.43324613922531907
Relative absolute error: 44.246128997342645
Root relative squared error: 86.64922784506382
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5646992656761204
Weighted FMeasure: 0.7665970218552051
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8628944684338515
Mean absolute error: 0.22123064498671322
Coverage of cases: 86.66666666666667
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2629746458109763
Kappa statistic: 0.5322486926205695
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 60.72463768115942
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8126583771631579
Weighted AreaUnderROC: 0.8822630059825598
Root mean squared error: 0.4204641738961758
Relative absolute error: 42.96648489421531
Root relative squared error: 84.09283477923516
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5918158820606957
Weighted FMeasure: 0.7831591955960004
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8730230234873904
Mean absolute error: 0.21483242447107656
Coverage of cases: 87.82608695652173
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24499986696663228
Kappa statistic: 0.5640422619256555
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 61.594202898550726
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8238663929837655
Weighted AreaUnderROC: 0.8853719465146735
Root mean squared error: 0.4065632383635884
Relative absolute error: 41.580450284220625
Root relative squared error: 81.31264767271767
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6226950789526957
Weighted FMeasure: 0.8030676568369242
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8777235798047154
Mean absolute error: 0.2079022514211031
Coverage of cases: 88.69565217391305
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 24.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22217222795415376
Kappa statistic: 0.6024374061528791
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 61.73913043478261
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8152306152306152
Weighted AreaUnderROC: 0.8989709129932213
Root mean squared error: 0.40769182793083897
Relative absolute error: 42.58282347790321
Root relative squared error: 81.5383655861678
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6047639574854603
Weighted FMeasure: 0.7938912319644839
Iteration time: 8.0
Weighted AreaUnderPRC: 0.888611524550992
Mean absolute error: 0.21291411738951604
Coverage of cases: 89.85507246376811
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.23169919086149451
Kappa statistic: 0.5840976990408303
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8088432914519871
Weighted AreaUnderROC: 0.8951820241371802
Root mean squared error: 0.4115149647758224
Relative absolute error: 42.94695919212969
Root relative squared error: 82.30299295516448
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5922822893033304
Weighted FMeasure: 0.7879170647750486
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8865116384670226
Mean absolute error: 0.21473479596064846
Coverage of cases: 89.56521739130434
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.2376311979999665
Kappa statistic: 0.5720425598825934
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 61.30434782608695
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8126583771631579
Weighted AreaUnderROC: 0.894630477648916
Root mean squared error: 0.4228402575475536
Relative absolute error: 43.81882660650938
Root relative squared error: 84.56805150951072
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5918158820606957
Weighted FMeasure: 0.7831591955960004
Iteration time: 6.0
Weighted AreaUnderPRC: 0.887152503132714
Mean absolute error: 0.2190941330325469
Coverage of cases: 88.98550724637681
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24499986696663228
Kappa statistic: 0.5640422619256555
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.8083293865050046
Weighted AreaUnderROC: 0.8980074557811867
Root mean squared error: 0.4271344294581209
Relative absolute error: 43.43072231409472
Root relative squared error: 85.42688589162418
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5809779232583663
Weighted FMeasure: 0.7765726223869631
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8900102639241356
Mean absolute error: 0.2171536115704736
Coverage of cases: 88.69565217391305
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2521897785043699
Kappa statistic: 0.5513496415014761
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8104913442340052
Weighted AreaUnderROC: 0.8958656185608156
Root mean squared error: 0.4266637382945419
Relative absolute error: 43.05741296491281
Root relative squared error: 85.33274765890837
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5863979700458998
Weighted FMeasure: 0.7798721320318623
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8884678445288121
Mean absolute error: 0.21528706482456403
Coverage of cases: 88.40579710144928
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.24859482273550107
Kappa statistic: 0.557700075516763
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.8060691007831545
Weighted AreaUnderROC: 0.8921447245371671
Root mean squared error: 0.43037205856832744
Relative absolute error: 43.227946223151285
Root relative squared error: 86.07441171366548
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5791096477392731
Weighted FMeasure: 0.7771358399181116
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8863757177159167
Mean absolute error: 0.21613973111575643
Coverage of cases: 87.82608695652173
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.25093187410510426
Kappa statistic: 0.5519323247569237
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8065494912118409
Weighted AreaUnderROC: 0.8903616832367116
Root mean squared error: 0.4289311280262854
Relative absolute error: 42.690144061730564
Root relative squared error: 85.78622560525709
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5867744560531362
Weighted FMeasure: 0.7846907532149417
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8843538782675159
Mean absolute error: 0.21345072030865284
Coverage of cases: 87.82608695652173
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24122615376883533
Kappa statistic: 0.5657342657342658
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 58.84057971014493
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8047816756857623
Weighted AreaUnderROC: 0.8876078925248058
Root mean squared error: 0.42617174461302776
Relative absolute error: 42.1666529921966
Root relative squared error: 85.23434892260555
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5853377055052091
Weighted FMeasure: 0.7851647165493719
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8819599783763664
Mean absolute error: 0.210833264960983
Coverage of cases: 87.82608695652173
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.23996824936956965
Kappa statistic: 0.5662953521667772
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8047816756857623
Weighted AreaUnderROC: 0.8871395165271787
Root mean squared error: 0.4207268664251333
Relative absolute error: 41.22187011567312
Root relative squared error: 84.14537328502666
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5853377055052091
Weighted FMeasure: 0.7851647165493719
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8807277018955474
Mean absolute error: 0.2061093505783656
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.23996824936956965
Kappa statistic: 0.5662953521667772
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 58.69565217391305
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8065494912118409
Weighted AreaUnderROC: 0.8865843239392066
Root mean squared error: 0.4279944090419002
Relative absolute error: 42.5435571695578
Root relative squared error: 85.59888180838004
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5867744560531362
Weighted FMeasure: 0.7846907532149417
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8793868014869838
Mean absolute error: 0.212717785847789
Coverage of cases: 87.53623188405797
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24122615376883533
Kappa statistic: 0.5657342657342658
Training time: 1.0
		
Time end:Sat Oct 07 11.34.04 EEST 2017