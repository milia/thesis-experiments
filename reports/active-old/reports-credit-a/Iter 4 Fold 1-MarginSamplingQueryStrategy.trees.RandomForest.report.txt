Sat Oct 07 11.36.16 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.16 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 94.78260869565217
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8392654604374831
Weighted AreaUnderROC: 0.8984545206971678
Root mean squared error: 0.36457755200309616
Relative absolute error: 58.20281012362817
Root relative squared error: 72.91551040061923
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6736330697086078
Weighted FMeasure: 0.8380221191748617
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8886506208579277
Mean absolute error: 0.29101405061814084
Coverage of cases: 99.71014492753623
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16121057118499574
Kappa statistic: 0.6728971962616823
Training time: 3.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 91.15942028985508
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8496574590809193
Weighted AreaUnderROC: 0.9078499455337691
Root mean squared error: 0.34828441280833816
Relative absolute error: 51.970649749615404
Root relative squared error: 69.65688256166763
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6934281661691625
Weighted FMeasure: 0.8468155532623299
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8969728641231764
Mean absolute error: 0.259853248748077
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1489707658425689
Kappa statistic: 0.6914392750468282
Training time: 4.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 91.44927536231884
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8236378758117889
Weighted AreaUnderROC: 0.8930154459126647
Root mean squared error: 0.36472926326459876
Relative absolute error: 56.115269681328826
Root relative squared error: 72.94585265291975
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6426106638051122
Weighted FMeasure: 0.82335060586823
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8916500279291993
Mean absolute error: 0.28057634840664414
Coverage of cases: 99.71014492753623
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.17939755612389882
Kappa statistic: 0.6425113387351576
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 92.46376811594203
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8561788977004468
Weighted AreaUnderROC: 0.9235088389220424
Root mean squared error: 0.348868636060353
Relative absolute error: 54.54490293727231
Root relative squared error: 69.7737272120706
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7012068406794488
Weighted FMeasure: 0.8468292947081063
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9192321054919382
Mean absolute error: 0.27272451468636155
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1410050795680591
Kappa statistic: 0.6938570495755687
Training time: 6.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 94.4927536231884
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8132786311102216
Weighted AreaUnderROC: 0.893083232689211
Root mean squared error: 0.3813042307512722
Relative absolute error: 62.20377236357047
Root relative squared error: 76.26084615025444
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.6097578075590138
Weighted FMeasure: 0.7973913043478261
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8881120961023029
Mean absolute error: 0.3110188618178524
Coverage of cases: 99.71014492753623
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.1855818414322251
Kappa statistic: 0.5980225706581443
Training time: 8.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 90.8695652173913
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8474227559583656
Weighted AreaUnderROC: 0.9192378279814342
Root mean squared error: 0.3498397614797603
Relative absolute error: 53.03006232023204
Root relative squared error: 69.96795229595206
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6811843288798356
Weighted FMeasure: 0.835185493231942
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9145660220723031
Mean absolute error: 0.2651503116011602
Coverage of cases: 99.71014492753623
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.1502442100028417
Kappa statistic: 0.6716096387956515
Training time: 9.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 92.31884057971014
Incorrectly Classified Instances: 12.753623188405797
Correctly Classified Instances: 87.2463768115942
Weighted Precision: 0.877007273662792
Weighted AreaUnderROC: 0.9283613656815384
Root mean squared error: 0.3313743727908333
Relative absolute error: 50.1103329418097
Root relative squared error: 66.27487455816666
Weighted TruePositiveRate: 0.8724637681159421
Weighted MatthewsCorrelation: 0.7475787590293841
Weighted FMeasure: 0.8728598433702404
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9175793432604673
Mean absolute error: 0.2505516647090485
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 53.0
Weighted Recall: 0.8724637681159421
Weighted FalsePositiveRate: 0.12021703608979822
Kappa statistic: 0.7443411479385611
Training time: 10.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 87.2463768115942
Incorrectly Classified Instances: 12.173913043478262
Correctly Classified Instances: 87.82608695652173
Weighted Precision: 0.8810540184453227
Weighted AreaUnderROC: 0.9301096132897603
Root mean squared error: 0.32624555224128127
Relative absolute error: 46.58858793928694
Root relative squared error: 65.24911044825626
Weighted TruePositiveRate: 0.8782608695652174
Weighted MatthewsCorrelation: 0.7571716558149665
Weighted FMeasure: 0.8785942999285505
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9152859006504227
Mean absolute error: 0.2329429396964347
Coverage of cases: 98.84057971014492
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8782608695652174
Weighted FalsePositiveRate: 0.11692508525149191
Kappa statistic: 0.7553191489361704
Training time: 10.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 91.44927536231884
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.836472925952047
Weighted AreaUnderROC: 0.8951221642038457
Root mean squared error: 0.36627303674025663
Relative absolute error: 56.60011686256472
Root relative squared error: 73.25460734805132
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6611379103715649
Weighted FMeasure: 0.8265815264946109
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8808905238560971
Mean absolute error: 0.2830005843128236
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.16115640096618356
Kappa statistic: 0.6536492320048188
Training time: 12.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 86.52173913043478
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8736445941678072
Weighted AreaUnderROC: 0.9197861904423607
Root mean squared error: 0.32743026194697195
Relative absolute error: 45.027506220442426
Root relative squared error: 65.48605238939439
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7411830509925155
Weighted FMeasure: 0.8699616455733838
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9040513714959515
Mean absolute error: 0.22513753110221213
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.12385443307757887
Kappa statistic: 0.7383588654633702
Training time: 11.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 90.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8562927676597536
Weighted AreaUnderROC: 0.9122904234157432
Root mean squared error: 0.34701665504487744
Relative absolute error: 51.348107770065965
Root relative squared error: 69.40333100897548
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7061641311633703
Weighted FMeasure: 0.8526231983165018
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9002680191990564
Mean absolute error: 0.2567405388503298
Coverage of cases: 98.84057971014492
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.14169597186700766
Kappa statistic: 0.7034733808584862
Training time: 23.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 91.30434782608695
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8606486230568926
Weighted AreaUnderROC: 0.9114148195510087
Root mean squared error: 0.3412644857817138
Relative absolute error: 50.585137474311594
Root relative squared error: 68.25289715634277
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.713763950483755
Weighted FMeasure: 0.8555344367888915
Iteration time: 15.0
Weighted AreaUnderPRC: 0.889320942017379
Mean absolute error: 0.25292568737155796
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 129.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13673096050014208
Kappa statistic: 0.7098597234837017
Training time: 14.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 84.20289855072464
Incorrectly Classified Instances: 11.884057971014492
Correctly Classified Instances: 88.1159420289855
Weighted Precision: 0.8861863354037267
Weighted AreaUnderROC: 0.9326754463862841
Root mean squared error: 0.3166044990004932
Relative absolute error: 42.150737502270594
Root relative squared error: 63.32089980009864
Weighted TruePositiveRate: 0.881159420289855
Weighted MatthewsCorrelation: 0.7656683301820679
Weighted FMeasure: 0.8815343654717888
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9219982500573949
Mean absolute error: 0.21075368751135298
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 145.0
Weighted Recall: 0.881159420289855
Weighted FalsePositiveRate: 0.11063245950554136
Kappa statistic: 0.7619288058571068
Training time: 15.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 90.28985507246377
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8513233467299963
Weighted AreaUnderROC: 0.913016689163588
Root mean squared error: 0.34892597271962933
Relative absolute error: 52.401532194956665
Root relative squared error: 69.78519454392587
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6931099662218058
Weighted FMeasure: 0.8439712800923349
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9035225655794298
Mean absolute error: 0.26200766097478334
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 163.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1459700909349247
Kappa statistic: 0.6874685455460493
Training time: 18.0
		
Time end:Sat Oct 07 11.36.17 EEST 2017