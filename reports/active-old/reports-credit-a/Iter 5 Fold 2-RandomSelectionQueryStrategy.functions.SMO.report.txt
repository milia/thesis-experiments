Sat Oct 07 11.36.40 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.40 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8656232898560479
Weighted AreaUnderROC: 0.861825980392157
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7195243791879756
Weighted FMeasure: 0.8554846054121757
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8127654324726221
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13142050298380223
Kappa statistic: 0.7113743600040157
Training time: 18.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8656232898560479
Weighted AreaUnderROC: 0.861825980392157
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7195243791879756
Weighted FMeasure: 0.8554846054121757
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8127654324726221
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13142050298380223
Kappa statistic: 0.7113743600040157
Training time: 22.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8374812593703148
Weighted AreaUnderROC: 0.8356821895424836
Root mean squared error: 0.4100194410378433
Relative absolute error: 33.6231884057971
Root relative squared error: 82.00388820756866
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6670861828945415
Weighted FMeasure: 0.8324199466751142
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7819150328217533
Mean absolute error: 0.1681159420289855
Coverage of cases: 83.18840579710145
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.16051967888604718
Kappa statistic: 0.663437279241094
Training time: 21.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8366753781579156
Weighted AreaUnderROC: 0.8356311274509803
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6681985134197985
Weighted FMeasure: 0.8351602484472049
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7822564024869643
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 87.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.1635203537936914
Kappa statistic: 0.6672757728034109
Training time: 23.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8288830462052856
Weighted AreaUnderROC: 0.8264399509803921
Root mean squared error: 0.41353898577234915
Relative absolute error: 34.20289855072463
Root relative squared error: 82.70779715446983
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6533248552612317
Weighted FMeasure: 0.8289272523627477
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7728683635737532
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.1761056052855925
Kappa statistic: 0.6533135762096979
Training time: 26.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8288830462052856
Weighted AreaUnderROC: 0.8264399509803921
Root mean squared error: 0.41353898577234915
Relative absolute error: 34.20289855072463
Root relative squared error: 82.70779715446983
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6533248552612317
Weighted FMeasure: 0.8289272523627477
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7728683635737532
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 145.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.1761056052855925
Kappa statistic: 0.6533135762096979
Training time: 30.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8288830462052856
Weighted AreaUnderROC: 0.8264399509803921
Root mean squared error: 0.41353898577234915
Relative absolute error: 34.20289855072463
Root relative squared error: 82.70779715446983
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6533248552612317
Weighted FMeasure: 0.8289272523627477
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7728683635737532
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463769
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 179.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.1761056052855925
Kappa statistic: 0.6533135762096979
Training time: 34.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8560780474035654
Weighted AreaUnderROC: 0.855187908496732
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7079938154727557
Weighted FMeasure: 0.8553157792288226
Iteration time: 42.0
Weighted AreaUnderPRC: 0.8054263727028403
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 221.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.14469664677465188
Kappa statistic: 0.7075577255619978
Training time: 41.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8554380748644322
Weighted AreaUnderROC: 0.8545751633986928
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7051054599324447
Weighted FMeasure: 0.8525960984222418
Iteration time: 81.0
Weighted AreaUnderPRC: 0.8041165712580496
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 302.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.14302358624609265
Kappa statistic: 0.7030830759884574
Training time: 80.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8572422360248446
Weighted AreaUnderROC: 0.8559027777777779
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7073171983259131
Weighted FMeasure: 0.8526403082697859
Iteration time: 96.0
Weighted AreaUnderPRC: 0.8055428931496983
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 398.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.1403683574879227
Kappa statistic: 0.703862660944206
Training time: 96.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8572422360248446
Weighted AreaUnderROC: 0.8559027777777779
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7073171983259131
Weighted FMeasure: 0.8526403082697859
Iteration time: 113.0
Weighted AreaUnderPRC: 0.8055428931496983
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 511.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.1403683574879227
Kappa statistic: 0.703862660944206
Training time: 112.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8629389842893679
Weighted AreaUnderROC: 0.860498366013072
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7164504189642678
Weighted FMeasure: 0.8555289630484583
Iteration time: 163.0
Weighted AreaUnderPRC: 0.8110206774581289
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 674.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13407573174197213
Kappa statistic: 0.7106190236537493
Training time: 162.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.866479173000912
Weighted AreaUnderROC: 0.8637663398692811
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7230180667017827
Weighted FMeasure: 0.8584110844324404
Iteration time: 159.0
Weighted AreaUnderPRC: 0.8149670619235836
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 833.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13043833475419153
Kappa statistic: 0.7165920635718954
Training time: 159.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8597932170752478
Weighted AreaUnderROC: 0.8559538398692811
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7078470854246934
Weighted FMeasure: 0.8497039896286628
Iteration time: 130.0
Weighted AreaUnderPRC: 0.8057531716245175
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 963.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.13736768258027848
Kappa statistic: 0.6998293344041763
Training time: 130.0
		
Time end:Sat Oct 07 11.36.42 EEST 2017