Sat Oct 07 11.34.13 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.13 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8447248833210513
Weighted AreaUnderROC: 0.8434946895424836
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6828181846106068
Weighted FMeasure: 0.8410642334785802
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7910126949544142
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15359033105996023
Kappa statistic: 0.6802163911218969
Training time: 29.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 52.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 18.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8142887250987738
Weighted AreaUnderROC: 0.8114276960784312
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6237265338634693
Weighted FMeasure: 0.814363121139185
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7561292666498285
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.19163736146632565
Kappa statistic: 0.6236834032109623
Training time: 17.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.7952625088036019
Weighted AreaUnderROC: 0.7852328431372548
Root mean squared error: 0.4536486541909668
Relative absolute error: 41.15942028985507
Root relative squared error: 90.72973083819336
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5814765827530537
Weighted FMeasure: 0.7922420958099815
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7306319405044319
Mean absolute error: 0.20579710144927535
Coverage of cases: 79.42028985507247
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 89.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.22373721227621482
Kappa statistic: 0.5777524952164246
Training time: 17.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.825435486251506
Weighted AreaUnderROC: 0.7876838235294118
Root mean squared error: 0.4406845794337767
Relative absolute error: 38.84057971014493
Root relative squared error: 88.13691588675535
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6195692920292699
Weighted FMeasure: 0.798765164265013
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7431333818744895
Mean absolute error: 0.19420289855072465
Coverage of cases: 80.57971014492753
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.23042945439045184
Kappa statistic: 0.5938965898908978
Training time: 20.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8383304958966299
Weighted AreaUnderROC: 0.7843647875816994
Root mean squared error: 0.4406845794337767
Relative absolute error: 38.84057971014493
Root relative squared error: 88.13691588675535
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6304735228084292
Weighted FMeasure: 0.7961195863014318
Iteration time: 29.0
Weighted AreaUnderPRC: 0.744859213160139
Mean absolute error: 0.19420289855072465
Coverage of cases: 80.57971014492753
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 139.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.23706752628587668
Kappa statistic: 0.5910948362787243
Training time: 28.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.8217696032143139
Weighted AreaUnderROC: 0.7621527777777779
Root mean squared error: 0.4631336239393426
Relative absolute error: 42.89855072463768
Root relative squared error: 92.62672478786853
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5910758763850151
Weighted FMeasure: 0.7731648069958991
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7220173361709167
Mean absolute error: 0.2144927536231884
Coverage of cases: 78.55072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 167.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.26120169082125605
Kappa statistic: 0.5468100969219298
Training time: 27.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8176588628762542
Weighted AreaUnderROC: 0.7484170751633986
Root mean squared error: 0.47548602137364576
Relative absolute error: 45.21739130434783
Root relative squared error: 95.09720427472915
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5728113737218742
Weighted FMeasure: 0.7586214478095971
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7099907050212843
Mean absolute error: 0.22608695652173913
Coverage of cases: 77.3913043478261
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 201.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2770788931514635
Kappa statistic: 0.5203208556149732
Training time: 33.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.8153043478260869
Weighted AreaUnderROC: 0.7274816176470589
Root mean squared error: 0.49343516379516894
Relative absolute error: 48.69565217391305
Root relative squared error: 98.68703275903378
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.547974210427934
Weighted FMeasure: 0.7354275015602245
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6927863894139886
Mean absolute error: 0.24347826086956523
Coverage of cases: 75.65217391304348
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 231.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.30155850383631716
Kappa statistic: 0.47980613893376406
Training time: 29.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.839371783496007
Weighted AreaUnderROC: 0.7915645424836601
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.638525125374322
Weighted FMeasure: 0.8032850643536149
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7509348147576611
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 270.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.22846511793123048
Kappa statistic: 0.6043926964805506
Training time: 38.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8368064589332914
Weighted AreaUnderROC: 0.8164828431372548
Root mean squared error: 0.41353898577234915
Relative absolute error: 34.20289855072463
Root relative squared error: 82.70779715446983
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6577246976800077
Weighted FMeasure: 0.8257337968474688
Iteration time: 56.0
Weighted AreaUnderPRC: 0.7693771042109998
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 326.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.19601982097186701
Kappa statistic: 0.6462645326103956
Training time: 55.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8548567821352482
Weighted AreaUnderROC: 0.8532986111111113
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7020945085864515
Weighted FMeasure: 0.8497558142604472
Iteration time: 77.0
Weighted AreaUnderPRC: 0.8024407348830035
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 403.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.14267814009661833
Kappa statistic: 0.6982541124230498
Training time: 76.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8606663821128704
Weighted AreaUnderROC: 0.8578941993464053
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.711346663322964
Weighted FMeasure: 0.8526319450215195
Iteration time: 147.0
Weighted AreaUnderPRC: 0.8079367316073001
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 550.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13638551435066779
Kappa statistic: 0.7050243926972789
Training time: 146.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8606663821128704
Weighted AreaUnderROC: 0.8578941993464053
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.711346663322964
Weighted FMeasure: 0.8526319450215195
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8079367316073001
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 689.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13638551435066779
Kappa statistic: 0.7050243926972789
Training time: 138.0
		
Time end:Sat Oct 07 11.34.14 EEST 2017