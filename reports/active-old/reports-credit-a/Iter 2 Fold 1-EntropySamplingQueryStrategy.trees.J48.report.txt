Sat Oct 07 11.34.33 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.33 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 78.98550724637681
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.3964616393677095
Relative absolute error: 46.341677646025516
Root relative squared error: 79.29232787354191
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.2317083882301276
Coverage of cases: 96.52173913043478
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 5.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 78.98550724637681
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.40857232391441717
Relative absolute error: 43.29231610609794
Root relative squared error: 81.71446478288344
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.21646158053048972
Coverage of cases: 96.52173913043478
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 78.98550724637681
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.4154306376265166
Relative absolute error: 41.913837601747126
Root relative squared error: 83.08612752530333
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.2095691880087356
Coverage of cases: 96.52173913043478
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 2.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 78.98550724637681
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.41970192977882065
Relative absolute error: 41.12825307776223
Root relative squared error: 83.94038595576413
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.20564126538881117
Coverage of cases: 96.52173913043478
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.4225965708337037
Relative absolute error: 40.62075157111705
Root relative squared error: 84.51931416674074
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.20310375785558524
Coverage of cases: 80.8695652173913
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 3.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.42468235756441125
Relative absolute error: 40.26588209654573
Root relative squared error: 84.93647151288225
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.20132941048272865
Coverage of cases: 80.8695652173913
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 3.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.8190699891067539
Root mean squared error: 0.4262549461578519
Relative absolute error: 40.003788955195525
Root relative squared error: 85.25098923157039
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7730415533914179
Mean absolute error: 0.20001894477597762
Coverage of cases: 80.8695652173913
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 30.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 3.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 76.66666666666667
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8453335626188223
Weighted AreaUnderROC: 0.9097903050108932
Root mean squared error: 0.34649959406045366
Relative absolute error: 34.68745425267167
Root relative squared error: 69.29991881209074
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754796956079258
Weighted FMeasure: 0.8347406007141357
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8751839902712832
Mean absolute error: 0.17343727126335834
Coverage of cases: 95.65217391304348
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.18643524438761014
Kappa statistic: 0.6644784828592268
Training time: 8.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 97.2463768115942
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8501469926416009
Weighted AreaUnderROC: 0.8603111383442266
Root mean squared error: 0.35986843692820225
Relative absolute error: 49.752106376147076
Root relative squared error: 71.97368738564045
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6917209733818652
Weighted FMeasure: 0.8439794934326809
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8180365077688394
Mean absolute error: 0.24876053188073538
Coverage of cases: 98.84057971014492
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.14729770531400965
Kappa statistic: 0.6870590606732514
Training time: 5.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 88.40579710144928
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8032402073732721
Weighted AreaUnderROC: 0.8109851579520698
Root mean squared error: 0.4140750101870412
Relative absolute error: 46.014709111576146
Root relative squared error: 82.81500203740823
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5946479667218989
Weighted FMeasure: 0.797215672284628
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7865417953243296
Mean absolute error: 0.23007354555788073
Coverage of cases: 91.59420289855072
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.22177287581699348
Kappa statistic: 0.5879848382574382
Training time: 11.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 84.92753623188406
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8077650355360091
Weighted AreaUnderROC: 0.7968239379084967
Root mean squared error: 0.41177942378967797
Relative absolute error: 44.08754041984284
Root relative squared error: 82.3558847579356
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6057601895798246
Weighted FMeasure: 0.8036771477808854
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7785879579662869
Mean absolute error: 0.2204377020992142
Coverage of cases: 91.01449275362319
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.21317046746234727
Kappa statistic: 0.6010046087722025
Training time: 16.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 75.65217391304348
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8032402073732721
Weighted AreaUnderROC: 0.7446044389978214
Root mean squared error: 0.42551230137480617
Relative absolute error: 45.4033922076053
Root relative squared error: 85.10246027496123
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5946479667218989
Weighted FMeasure: 0.797215672284628
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7450964665302654
Mean absolute error: 0.22701696103802652
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.22177287581699348
Kappa statistic: 0.5879848382574382
Training time: 12.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 57.10144927536232
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8157902001380263
Weighted AreaUnderROC: 0.8132318899782136
Root mean squared error: 0.39954218897758415
Relative absolute error: 41.50248843832055
Root relative squared error: 79.90843779551683
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6232145452010962
Weighted FMeasure: 0.8128487994808566
Iteration time: 16.0
Weighted AreaUnderPRC: 0.792448556312994
Mean absolute error: 0.20751244219160273
Coverage of cases: 88.98550724637681
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 117.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.2022582764990054
Kappa statistic: 0.6196382428940569
Training time: 12.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 55.072463768115945
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8217253278122844
Weighted AreaUnderROC: 0.7851817810457516
Root mean squared error: 0.40046959768919416
Relative absolute error: 40.3938355778737
Root relative squared error: 80.09391953783883
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6351694530740114
Weighted FMeasure: 0.8186972744970797
Iteration time: 19.0
Weighted AreaUnderPRC: 0.774689095813544
Mean absolute error: 0.2019691778893685
Coverage of cases: 87.2463768115942
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 136.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19631109690252915
Kappa statistic: 0.6315245478036176
Training time: 11.0
		
Time end:Sat Oct 07 11.34.34 EEST 2017