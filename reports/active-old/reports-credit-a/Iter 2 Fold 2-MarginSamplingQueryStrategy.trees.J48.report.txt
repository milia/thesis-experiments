Sat Oct 07 11.36.07 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.07 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 76.81159420289855
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8186836581709146
Weighted AreaUnderROC: 0.7597062623240634
Root mean squared error: 0.4314921145093204
Relative absolute error: 51.85804533630585
Root relative squared error: 86.29842290186409
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.6109990138592402
Weighted FMeasure: 0.7905779555742709
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7547272930239096
Mean absolute error: 0.25929022668152923
Coverage of cases: 91.01449275362319
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.1846204558018808
Kappa statistic: 0.5896249793490831
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 89.27536231884058
Incorrectly Classified Instances: 31.594202898550726
Correctly Classified Instances: 68.40579710144928
Weighted Precision: 0.6829423036113296
Weighted AreaUnderROC: 0.6990888692459373
Root mean squared error: 0.49591734464654413
Relative absolute error: 72.82880296913949
Root relative squared error: 99.18346892930883
Weighted TruePositiveRate: 0.6840579710144927
Weighted MatthewsCorrelation: 0.3545067246472099
Weighted FMeasure: 0.6798063136671219
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6587794373841954
Mean absolute error: 0.3641440148456974
Coverage of cases: 95.94202898550725
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6840579710144927
Weighted FalsePositiveRate: 0.339018194037543
Kappa statistic: 0.35055178488161237
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 89.27536231884058
Incorrectly Classified Instances: 31.594202898550726
Correctly Classified Instances: 68.40579710144928
Weighted Precision: 0.6829423036113296
Weighted AreaUnderROC: 0.6990888692459373
Root mean squared error: 0.5025805136182011
Relative absolute error: 70.524908840077
Root relative squared error: 100.51610272364022
Weighted TruePositiveRate: 0.6840579710144927
Weighted MatthewsCorrelation: 0.3545067246472099
Weighted FMeasure: 0.6798063136671219
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6587794373841954
Mean absolute error: 0.352624544200385
Coverage of cases: 95.94202898550725
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6840579710144927
Weighted FalsePositiveRate: 0.339018194037543
Kappa statistic: 0.35055178488161237
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 89.27536231884058
Incorrectly Classified Instances: 31.594202898550726
Correctly Classified Instances: 68.40579710144928
Weighted Precision: 0.6829423036113296
Weighted AreaUnderROC: 0.6990888692459373
Root mean squared error: 0.5090973279256007
Relative absolute error: 69.10127107499493
Root relative squared error: 101.81946558512014
Weighted TruePositiveRate: 0.6840579710144927
Weighted MatthewsCorrelation: 0.3545067246472099
Weighted FMeasure: 0.6798063136671219
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6587794373841954
Mean absolute error: 0.34550635537497465
Coverage of cases: 95.94202898550725
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6840579710144927
Weighted FalsePositiveRate: 0.339018194037543
Kappa statistic: 0.35055178488161237
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 89.27536231884058
Incorrectly Classified Instances: 31.594202898550726
Correctly Classified Instances: 68.40579710144928
Weighted Precision: 0.6829423036113296
Weighted AreaUnderROC: 0.6990888692459373
Root mean squared error: 0.5141155645762374
Relative absolute error: 68.13722181026979
Root relative squared error: 102.82311291524748
Weighted TruePositiveRate: 0.6840579710144927
Weighted MatthewsCorrelation: 0.3545067246472099
Weighted FMeasure: 0.6798063136671219
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6587794373841954
Mean absolute error: 0.34068610905134894
Coverage of cases: 95.94202898550725
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6840579710144927
Weighted FalsePositiveRate: 0.339018194037543
Kappa statistic: 0.35055178488161237
Training time: 3.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 54.927536231884055
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7385827298870777
Weighted AreaUnderROC: 0.7265247841164072
Root mean squared error: 0.5022302049218514
Relative absolute error: 56.743807085876504
Root relative squared error: 100.44604098437028
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.4549839392999012
Weighted FMeasure: 0.7222012256912609
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7226738631891368
Mean absolute error: 0.28371903542938254
Coverage of cases: 76.52173913043478
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.30288327652315816
Kappa statistic: 0.43943602914198854
Training time: 2.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 53.91304347826087
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7843718071492398
Weighted AreaUnderROC: 0.6554701842659958
Root mean squared error: 0.47256056868370244
Relative absolute error: 49.914126528524235
Root relative squared error: 94.51211373674049
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5394558027122721
Weighted FMeasure: 0.7596536257810272
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7097845809540528
Mean absolute error: 0.2495706326426212
Coverage of cases: 79.1304347826087
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2674699911212545
Kappa statistic: 0.516230806983103
Training time: 3.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 53.91304347826087
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7843718071492398
Weighted AreaUnderROC: 0.6554701842659958
Root mean squared error: 0.47300771036977585
Relative absolute error: 49.5312252361579
Root relative squared error: 94.60154207395517
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5394558027122721
Weighted FMeasure: 0.7596536257810272
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7097845809540528
Mean absolute error: 0.2476561261807895
Coverage of cases: 79.1304347826087
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2674699911212545
Kappa statistic: 0.516230806983103
Training time: 3.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 53.91304347826087
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7843718071492398
Weighted AreaUnderROC: 0.6554701842659958
Root mean squared error: 0.4732180582464245
Relative absolute error: 49.25851213034527
Root relative squared error: 94.64361164928489
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5394558027122721
Weighted FMeasure: 0.7596536257810272
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7097845809540528
Mean absolute error: 0.24629256065172633
Coverage of cases: 79.1304347826087
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2674699911212545
Kappa statistic: 0.516230806983103
Training time: 3.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 80.43478260869566
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7883508110443422
Weighted AreaUnderROC: 0.6575440266539743
Root mean squared error: 0.46583134590325365
Relative absolute error: 52.38360589722518
Root relative squared error: 93.16626918065073
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5389190292518433
Weighted FMeasure: 0.7544721918217093
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7122666839267708
Mean absolute error: 0.2619180294861259
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2748386600879203
Kappa statistic: 0.5079499234060536
Training time: 7.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 83.91304347826087
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.778497157112627
Weighted AreaUnderROC: 0.7745461344937785
Root mean squared error: 0.43365866732831915
Relative absolute error: 50.16948435200873
Root relative squared error: 86.73173346566384
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310974235492204
Weighted FMeasure: 0.7575028756180497
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7830822486038773
Mean absolute error: 0.25084742176004365
Coverage of cases: 92.17391304347827
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.268549138091592
Kappa statistic: 0.5111348249741967
Training time: 8.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 95.94202898550725
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.843270159791899
Weighted AreaUnderROC: 0.8132521928333448
Root mean squared error: 0.370043247927606
Relative absolute error: 44.02988910832399
Root relative squared error: 74.0086495855212
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6827041541973072
Weighted FMeasure: 0.8432595060528094
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7971724097214391
Mean absolute error: 0.22014944554161997
Coverage of cases: 95.65217391304348
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 71.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1626800015372745
Kappa statistic: 0.6825153374233129
Training time: 8.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 95.94202898550725
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.843270159791899
Weighted AreaUnderROC: 0.8220235262120079
Root mean squared error: 0.3693245372667063
Relative absolute error: 42.3274267506072
Root relative squared error: 73.86490745334126
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6827041541973072
Weighted FMeasure: 0.8432595060528094
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8050679365969615
Mean absolute error: 0.211637133753036
Coverage of cases: 95.65217391304348
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1626800015372745
Kappa statistic: 0.6825153374233129
Training time: 4.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 95.94202898550725
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.843270159791899
Weighted AreaUnderROC: 0.828755014618889
Root mean squared error: 0.3690078315007812
Relative absolute error: 41.057674272065185
Root relative squared error: 73.80156630015624
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6827041541973072
Weighted FMeasure: 0.8432595060528094
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8123768098510472
Mean absolute error: 0.20528837136032593
Coverage of cases: 95.65217391304348
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1626800015372745
Kappa statistic: 0.6825153374233129
Training time: 4.0
		
Time end:Sat Oct 07 11.36.08 EEST 2017