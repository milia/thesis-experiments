Sat Oct 07 11.37.03 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.03 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8171722907203115
Weighted AreaUnderROC: 0.8120404411764705
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6287209528004792
Weighted FMeasure: 0.8167154451920468
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7579755522952346
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.1933104219948849
Kappa statistic: 0.6278317152103561
Training time: 14.0
		
Time end:Sat Oct 07 11.37.03 EEST 2017