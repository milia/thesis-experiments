Sat Oct 07 11.37.05 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.05 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 91.01449275362319
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8084831142154055
Weighted AreaUnderROC: 0.8744664381453066
Root mean squared error: 0.37617399479649066
Relative absolute error: 57.95682594628657
Root relative squared error: 75.23479895929813
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6119658269616332
Weighted FMeasure: 0.8085619686747845
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8583339211367251
Mean absolute error: 0.28978412973143286
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.1975845410628019
Kappa statistic: 0.6119235095613048
Training time: 4.0
		
Time end:Sat Oct 07 11.37.05 EEST 2017