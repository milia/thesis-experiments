Sat Oct 07 11.37.04 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.04 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7705365519829575
Weighted AreaUnderROC: 0.7648908682940097
Root mean squared error: 0.4719465908407744
Relative absolute error: 47.32394366197189
Root relative squared error: 94.38931816815487
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5346016848812926
Weighted FMeasure: 0.7699786521780105
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7073009086571009
Mean absolute error: 0.23661971830985945
Coverage of cases: 77.10144927536231
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.24123275616560388
Kappa statistic: 0.5334725526779754
Training time: 3.0
		
Time end:Sat Oct 07 11.37.04 EEST 2017