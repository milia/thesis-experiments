Sat Oct 07 11.37.04 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.04 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 98.98550724637681
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8590868884806422
Weighted AreaUnderROC: 0.9021384374787517
Root mean squared error: 0.3286759013753427
Relative absolute error: 42.95452273863074
Root relative squared error: 65.73518027506854
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7087498596326338
Weighted FMeasure: 0.8534443745377882
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8711037374450278
Mean absolute error: 0.2147726136931537
Coverage of cases: 99.42028985507247
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.16213712685372145
Kappa statistic: 0.7026579791084911
Training time: 8.0
		
Time end:Sat Oct 07 11.37.05 EEST 2017