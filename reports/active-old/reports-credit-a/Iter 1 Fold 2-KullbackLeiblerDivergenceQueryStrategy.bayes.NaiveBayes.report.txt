Sat Oct 07 11.34.44 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.44 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 61.594202898550726
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8128605839913999
Weighted AreaUnderROC: 0.8988880381322905
Root mean squared error: 0.41753147611013364
Relative absolute error: 43.027505655858455
Root relative squared error: 83.50629522202672
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5883692266882594
Weighted FMeasure: 0.7793068171796453
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8865173763469378
Mean absolute error: 0.21513752827929228
Coverage of cases: 88.40579710144928
Instances selection time: 43.0
Test time: 30.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.24985272713476675
Kappa statistic: 0.5571245186136072
Training time: 7.0
		
Time end:Sat Oct 07 11.34.45 EEST 2017