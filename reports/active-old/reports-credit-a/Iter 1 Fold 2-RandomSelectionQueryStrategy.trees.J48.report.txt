Sat Oct 07 11.36.50 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.50 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 56.666666666666664
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8419353630678201
Weighted AreaUnderROC: 0.886437908496732
Root mean squared error: 0.36685455931429256
Relative absolute error: 34.41091219096351
Root relative squared error: 73.3709118628585
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6727714441515101
Weighted FMeasure: 0.8355480582612322
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8484107922508735
Mean absolute error: 0.17205456095481753
Coverage of cases: 90.72463768115942
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.18245240125035522
Kappa statistic: 0.6658364466583645
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 75.3623188405797
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8513233467299963
Weighted AreaUnderROC: 0.8487540849673204
Root mean squared error: 0.3615040684425135
Relative absolute error: 45.420025549535225
Root relative squared error: 72.3008136885027
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6931099662218058
Weighted FMeasure: 0.8439712800923349
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7971037367688096
Mean absolute error: 0.22710012774767613
Coverage of cases: 95.3623188405797
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1459700909349247
Kappa statistic: 0.6874685455460493
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8349584764961584
Weighted AreaUnderROC: 0.8671023965141612
Root mean squared error: 0.3706688055171434
Relative absolute error: 36.126482213438784
Root relative squared error: 74.13376110342867
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6642336378092137
Weighted FMeasure: 0.834002779772954
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8270952479530824
Mean absolute error: 0.1806324110671939
Coverage of cases: 93.04347826086956
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17679649758454105
Kappa statistic: 0.6628259863175762
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 74.78260869565217
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8258529644268775
Weighted AreaUnderROC: 0.8879357298474946
Root mean squared error: 0.3579910496274465
Relative absolute error: 36.899972533242845
Root relative squared error: 71.5982099254893
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.638050250103982
Weighted FMeasure: 0.8173463592992216
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8514791330352767
Mean absolute error: 0.1844998626662142
Coverage of cases: 95.65217391304348
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.20294916879795397
Kappa statistic: 0.6290322580645161
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 82.6086956521739
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8381787439613527
Weighted AreaUnderROC: 0.8896377995642702
Root mean squared error: 0.3463265125819537
Relative absolute error: 39.76494292536277
Root relative squared error: 69.26530251639073
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6588442300029147
Weighted FMeasure: 0.8254162269801931
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8515595433720642
Mean absolute error: 0.19882471462681384
Coverage of cases: 96.23188405797102
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.19734743535095195
Kappa statistic: 0.6457843904985642
Training time: 2.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 98.69565217391305
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8381787439613527
Weighted AreaUnderROC: 0.8896377995642702
Root mean squared error: 0.3456757662098705
Relative absolute error: 39.77553031931926
Root relative squared error: 69.1351532419741
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6588442300029147
Weighted FMeasure: 0.8254162269801931
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8515595433720642
Mean absolute error: 0.1988776515965963
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.19734743535095195
Kappa statistic: 0.6457843904985642
Training time: 2.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 68.84057971014492
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8352946523254051
Weighted AreaUnderROC: 0.8162275326797387
Root mean squared error: 0.3797630905751901
Relative absolute error: 38.59947422191065
Root relative squared error: 75.95261811503802
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6643469561284228
Weighted FMeasure: 0.8338224471038351
Iteration time: 4.0
Weighted AreaUnderPRC: 0.78921678812722
Mean absolute error: 0.19299737110955323
Coverage of cases: 90.14492753623189
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17812411196362604
Kappa statistic: 0.6623744527427246
Training time: 3.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 57.971014492753625
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8244123153734138
Weighted AreaUnderROC: 0.8880208333333334
Root mean squared error: 0.353872586937118
Relative absolute error: 39.82132313472872
Root relative squared error: 70.7745173874236
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.640986603388155
Weighted FMeasure: 0.8217359414762329
Iteration time: 3.0
Weighted AreaUnderPRC: 0.853510593334152
Mean absolute error: 0.1991066156736436
Coverage of cases: 91.59420289855072
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.1926736999147485
Kappa statistic: 0.6377110984868046
Training time: 3.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 75.94202898550725
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8204313362717891
Weighted AreaUnderROC: 0.8986587690631809
Root mean squared error: 0.35361683871507005
Relative absolute error: 38.410910345693146
Root relative squared error: 70.72336774301401
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6346068826762286
Weighted FMeasure: 0.8193451820781014
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8683704425343133
Mean absolute error: 0.19205455172846572
Coverage of cases: 96.81159420289855
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19232825376527424
Kappa statistic: 0.6330039118797611
Training time: 3.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 75.65217391304348
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8462392366837165
Weighted AreaUnderROC: 0.8904547930283224
Root mean squared error: 0.34924511390999297
Relative absolute error: 36.469388923789566
Root relative squared error: 69.8490227819986
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6879104972277379
Weighted FMeasure: 0.8459536904772088
Iteration time: 4.0
Weighted AreaUnderPRC: 0.857909911625636
Mean absolute error: 0.18234694461894785
Coverage of cases: 95.94202898550725
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1622469096334186
Kappa statistic: 0.6873236546452572
Training time: 3.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8448502306959941
Weighted AreaUnderROC: 0.8615025871459696
Root mean squared error: 0.3614188444877712
Relative absolute error: 40.20753449252905
Root relative squared error: 72.28376889755424
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6825851393936753
Weighted FMeasure: 0.8422908545727136
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8252697727149059
Mean absolute error: 0.20103767246264526
Coverage of cases: 93.04347826086956
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.17119476413753906
Kappa statistic: 0.6795004128819158
Training time: 4.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 100.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8104288613247639
Weighted AreaUnderROC: 0.8818252995642702
Root mean squared error: 0.360053540345081
Relative absolute error: 42.95936283967277
Root relative squared error: 72.0107080690162
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6115715333516538
Weighted FMeasure: 0.8067419450975197
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8463109305631037
Mean absolute error: 0.21479681419836386
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20953307047456665
Kappa statistic: 0.6072241771889878
Training time: 5.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 74.20289855072464
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8104288613247639
Weighted AreaUnderROC: 0.8688214869281046
Root mean squared error: 0.3663280136164304
Relative absolute error: 42.10947857132461
Root relative squared error: 73.26560272328608
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6115715333516538
Weighted FMeasure: 0.8067419450975197
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8339123687260858
Mean absolute error: 0.21054739285662305
Coverage of cases: 94.4927536231884
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20953307047456665
Kappa statistic: 0.6072241771889878
Training time: 5.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 75.3623188405797
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8104288613247639
Weighted AreaUnderROC: 0.8818252995642702
Root mean squared error: 0.363544869907876
Relative absolute error: 42.876643674597744
Root relative squared error: 72.7089739815752
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6115715333516538
Weighted FMeasure: 0.8067419450975197
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8463109305631037
Mean absolute error: 0.21438321837298874
Coverage of cases: 95.3623188405797
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20953307047456665
Kappa statistic: 0.6072241771889878
Training time: 4.0
		
Time end:Sat Oct 07 11.36.51 EEST 2017