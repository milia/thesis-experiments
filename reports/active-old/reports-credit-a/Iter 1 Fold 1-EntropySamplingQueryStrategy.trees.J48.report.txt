Sat Oct 07 11.34.31 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.31 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 64.05797101449275
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7716963248333639
Weighted AreaUnderROC: 0.7831903594771242
Root mean squared error: 0.4400265788231593
Relative absolute error: 51.77519911215563
Root relative squared error: 88.00531576463186
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5171594638177609
Weighted FMeasure: 0.751298476635808
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7685304824136849
Mean absolute error: 0.25887599556077817
Coverage of cases: 88.98550724637681
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 57.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.27667927678317705
Kappa statistic: 0.4976051371124796
Training time: 49.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 63.91304347826087
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7798493861145529
Weighted AreaUnderROC: 0.8087043845315905
Root mean squared error: 0.4317520814514449
Relative absolute error: 48.45065562456864
Root relative squared error: 86.35041629028898
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310436832107464
Weighted FMeasure: 0.7567160941114337
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7868004457665077
Mean absolute error: 0.2422532781228432
Coverage of cases: 88.98550724637681
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27205971156578573
Kappa statistic: 0.5090391609128763
Training time: 15.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 51.594202898550726
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7677348430715111
Weighted AreaUnderROC: 0.7635825163398693
Root mean squared error: 0.47417838914457905
Relative absolute error: 48.016276677474316
Root relative squared error: 94.8356778289158
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5182936676005776
Weighted FMeasure: 0.7571302267526524
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7151561966178053
Mean absolute error: 0.2400813833873716
Coverage of cases: 77.97101449275362
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 100.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2664038078999716
Kappa statistic: 0.507366001462752
Training time: 18.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 54.78260869565217
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7926535706856073
Weighted AreaUnderROC: 0.8302525871459695
Root mean squared error: 0.4385193734924947
Relative absolute error: 42.769430849006106
Root relative squared error: 87.70387469849894
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5636842687476812
Weighted FMeasure: 0.7767533875902116
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7860353466184092
Mean absolute error: 0.21384715424503054
Coverage of cases: 83.47826086956522
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 116.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.24890771526001707
Kappa statistic: 0.5478848875609373
Training time: 7.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 52.608695652173914
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8264650283553874
Weighted AreaUnderROC: 0.7909858387799563
Root mean squared error: 0.4275557862226633
Relative absolute error: 38.78383397318656
Root relative squared error: 85.51115724453267
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6312190586476298
Weighted FMeasure: 0.8096933424778026
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7548571590160859
Mean absolute error: 0.1939191698659328
Coverage of cases: 81.73913043478261
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21553442028985506
Kappa statistic: 0.6144578313253012
Training time: 9.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 52.608695652173914
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8264650283553874
Weighted AreaUnderROC: 0.7909858387799563
Root mean squared error: 0.427973228623191
Relative absolute error: 38.31157438550526
Root relative squared error: 85.5946457246382
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6312190586476298
Weighted FMeasure: 0.8096933424778026
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7548571590160859
Mean absolute error: 0.1915578719275263
Coverage of cases: 81.73913043478261
Instances selection time: 10.0
Test time: 4.0
Accumulative iteration time: 148.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21553442028985506
Kappa statistic: 0.6144578313253012
Training time: 6.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 52.608695652173914
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8264650283553874
Weighted AreaUnderROC: 0.7909858387799563
Root mean squared error: 0.42844107459710795
Relative absolute error: 38.19372050445532
Root relative squared error: 85.6882149194216
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6312190586476298
Weighted FMeasure: 0.8096933424778026
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7548571590160859
Mean absolute error: 0.1909686025222766
Coverage of cases: 81.73913043478261
Instances selection time: 9.0
Test time: 1.0
Accumulative iteration time: 172.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21553442028985506
Kappa statistic: 0.6144578313253012
Training time: 15.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 60.289855072463766
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8234567901234568
Weighted AreaUnderROC: 0.8650088507625271
Root mean squared error: 0.37198929724669333
Relative absolute error: 40.95143268060627
Root relative squared error: 74.39785944933867
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6107047547706971
Weighted FMeasure: 0.7917241379310346
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8435206863691295
Mean absolute error: 0.20475716340303135
Coverage of cases: 92.46376811594203
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 187.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.23903186274509805
Kappa statistic: 0.5806247027112733
Training time: 11.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 54.63768115942029
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8588244205423421
Weighted AreaUnderROC: 0.8148828976034859
Root mean squared error: 0.3681420267744173
Relative absolute error: 34.52619853540163
Root relative squared error: 73.62840535488347
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7118741100660705
Weighted FMeasure: 0.8571456124225952
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8042636876146649
Mean absolute error: 0.17263099267700818
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 211.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.15433539357772094
Kappa statistic: 0.709760494463044
Training time: 23.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 54.63768115942029
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8588244205423421
Weighted AreaUnderROC: 0.8148828976034859
Root mean squared error: 0.36823891546848375
Relative absolute error: 34.01626695854809
Root relative squared error: 73.64778309369675
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7118741100660705
Weighted FMeasure: 0.8571456124225952
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8042636876146649
Mean absolute error: 0.17008133479274043
Coverage of cases: 88.1159420289855
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 229.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.15433539357772094
Kappa statistic: 0.709760494463044
Training time: 16.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 80.8695652173913
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8584167674713951
Weighted AreaUnderROC: 0.8500987200435729
Root mean squared error: 0.35252375472423286
Relative absolute error: 35.40349473122576
Root relative squared error: 70.50475094484658
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7116568061146984
Weighted FMeasure: 0.8573006352434166
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8237465147276157
Mean absolute error: 0.17701747365612883
Coverage of cases: 92.17391304347827
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 250.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.15300777919863598
Kappa statistic: 0.710148654904583
Training time: 14.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 81.15942028985508
Incorrectly Classified Instances: 12.753623188405797
Correctly Classified Instances: 87.2463768115942
Weighted Precision: 0.8728922390566816
Weighted AreaUnderROC: 0.867391748366013
Root mean squared error: 0.3316272921908634
Relative absolute error: 33.40415872973615
Root relative squared error: 66.32545843817267
Weighted TruePositiveRate: 0.8724637681159421
Weighted MatthewsCorrelation: 0.7411954151038025
Weighted FMeasure: 0.8719279199747088
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8438013111473663
Mean absolute error: 0.16702079364868078
Coverage of cases: 94.20289855072464
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 274.0
Weighted Recall: 0.8724637681159421
Weighted FalsePositiveRate: 0.13747602301790282
Kappa statistic: 0.7398992495116686
Training time: 17.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 91.30434782608695
Incorrectly Classified Instances: 12.173913043478262
Correctly Classified Instances: 87.82608695652173
Weighted Precision: 0.8782608695652174
Weighted AreaUnderROC: 0.8576899509803921
Root mean squared error: 0.3346587248946423
Relative absolute error: 37.56470965840361
Root relative squared error: 66.93174497892846
Weighted TruePositiveRate: 0.8782608695652174
Weighted MatthewsCorrelation: 0.7533700980392156
Weighted FMeasure: 0.8782608695652174
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8313235613965823
Mean absolute error: 0.18782354829201806
Coverage of cases: 95.07246376811594
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 293.0
Weighted Recall: 0.8782608695652174
Weighted FalsePositiveRate: 0.12489077152600173
Kappa statistic: 0.7533700980392157
Training time: 12.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 91.30434782608695
Incorrectly Classified Instances: 12.173913043478262
Correctly Classified Instances: 87.82608695652173
Weighted Precision: 0.8782608695652174
Weighted AreaUnderROC: 0.8576899509803921
Root mean squared error: 0.3339735626953716
Relative absolute error: 36.23620724294488
Root relative squared error: 66.79471253907433
Weighted TruePositiveRate: 0.8782608695652174
Weighted MatthewsCorrelation: 0.7533700980392156
Weighted FMeasure: 0.8782608695652174
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8313235613965823
Mean absolute error: 0.1811810362147244
Coverage of cases: 95.07246376811594
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 308.0
Weighted Recall: 0.8782608695652174
Weighted FalsePositiveRate: 0.12489077152600173
Kappa statistic: 0.7533700980392157
Training time: 12.0
		
Time end:Sat Oct 07 11.34.32 EEST 2017