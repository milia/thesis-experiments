Sat Oct 07 11.34.23 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.23 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7705365519829575
Weighted AreaUnderROC: 0.7648908682940097
Root mean squared error: 0.4719465908407744
Relative absolute error: 47.32394366197189
Root relative squared error: 94.38931816815487
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5346016848812926
Weighted FMeasure: 0.7699786521780105
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7073009086571009
Mean absolute error: 0.23661971830985945
Coverage of cases: 77.10144927536231
Instances selection time: 12.0
Test time: 8.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.24123275616560388
Kappa statistic: 0.5334725526779754
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7640225738659054
Weighted AreaUnderROC: 0.7417386278642824
Root mean squared error: 0.48810639072185796
Relative absolute error: 49.82322025800294
Root relative squared error: 97.62127814437159
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5087522659966739
Weighted FMeasure: 0.7504025764895331
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6891929622177078
Mean absolute error: 0.2491161012900147
Coverage of cases: 75.65217391304348
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.27304448340187015
Kappa statistic: 0.49531538434746253
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7761022442365316
Weighted AreaUnderROC: 0.7450873733596246
Root mean squared error: 0.4831974526532982
Relative absolute error: 48.481525003264125
Root relative squared error: 96.63949053065964
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5254627403597829
Weighted FMeasure: 0.7542262863003827
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6951189384210847
Mean absolute error: 0.24240762501632063
Coverage of cases: 76.23188405797102
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.27214409386046085
Kappa statistic: 0.5047789097783846
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7838485655131617
Weighted AreaUnderROC: 0.7425715645610934
Root mean squared error: 0.48384988487301644
Relative absolute error: 48.337205443079945
Root relative squared error: 96.7699769746033
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5315741909773352
Weighted FMeasure: 0.7517723078791458
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6954719339531655
Mean absolute error: 0.24168602721539972
Coverage of cases: 76.23188405797102
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2771757114575234
Kappa statistic: 0.502199542495161
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7838485655131617
Weighted AreaUnderROC: 0.7425715645610934
Root mean squared error: 0.48433141785255834
Relative absolute error: 48.231116229964464
Root relative squared error: 96.86628357051167
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5315741909773352
Weighted FMeasure: 0.7517723078791458
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6954719339531655
Mean absolute error: 0.24115558114982233
Coverage of cases: 76.23188405797102
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2771757114575234
Kappa statistic: 0.502199542495161
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.92753623188406
Correctly Classified Instances: 75.07246376811594
Weighted Precision: 0.7855579992105115
Weighted AreaUnderROC: 0.7270687427755489
Root mean squared error: 0.49638096212686295
Relative absolute error: 50.44156284430885
Root relative squared error: 99.27619242537259
Weighted TruePositiveRate: 0.7507246376811594
Weighted MatthewsCorrelation: 0.5179126244433668
Weighted FMeasure: 0.7350943440617353
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6842664967327167
Mean absolute error: 0.25220781422154426
Coverage of cases: 75.07246376811594
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7507246376811594
Weighted FalsePositiveRate: 0.29658715213006126
Kappa statistic: 0.4738055545702833
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7834636945093346
Weighted AreaUnderROC: 0.7238219895287958
Root mean squared error: 0.4995605669368636
Relative absolute error: 50.95379012064646
Root relative squared error: 99.91211338737273
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.5124368020420779
Weighted FMeasure: 0.7315876241106682
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6812675125194397
Mean absolute error: 0.2547689506032323
Coverage of cases: 74.78260869565217
Instances selection time: 11.0
Test time: 23.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.30018210789893013
Kappa statistic: 0.4673374860245967
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7813675006799375
Weighted AreaUnderROC: 0.7205752362820426
Root mean squared error: 0.5026700474533701
Relative absolute error: 51.478810357854265
Root relative squared error: 100.53400949067401
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5069494837893459
Weighted FMeasure: 0.7280644122383252
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6782804677577614
Mean absolute error: 0.2573940517892713
Coverage of cases: 74.4927536231884
Instances selection time: 13.0
Test time: 24.0
Accumulative iteration time: 75.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.3037770636677989
Kappa statistic: 0.4608609177439977
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7804729482169028
Weighted AreaUnderROC: 0.7244509417284286
Root mean squared error: 0.5000093293124684
Relative absolute error: 50.86391869000567
Root relative squared error: 100.00186586249369
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.5100508617254657
Weighted FMeasure: 0.7324339156132432
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6809754878707853
Mean absolute error: 0.25431959345002836
Coverage of cases: 74.78260869565217
Instances selection time: 13.0
Test time: 16.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.2989242034996644
Kappa statistic: 0.4680360845754391
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7960686438947309
Weighted AreaUnderROC: 0.7665567416876318
Root mean squared error: 0.46440819919924003
Relative absolute error: 43.92863329291535
Root relative squared error: 92.881639839848
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5673189529390705
Weighted FMeasure: 0.7759687303961781
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7170335582055808
Mean absolute error: 0.21964316646457674
Coverage of cases: 78.26086956521739
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 94.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.24949521227691043
Kappa statistic: 0.5479322815661198
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8075487012987013
Weighted AreaUnderROC: 0.7872951655674169
Root mean squared error: 0.4455755846230568
Relative absolute error: 40.442804428044276
Root relative squared error: 89.11511692461136
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5985751229198807
Weighted FMeasure: 0.7959518072289156
Iteration time: 5.0
Weighted AreaUnderPRC: 0.736289066306896
Mean absolute error: 0.20221402214022138
Coverage of cases: 80.0
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 99.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.22540966886516625
Kappa statistic: 0.5867689690489004
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8163996452976285
Weighted AreaUnderROC: 0.8009111307540627
Root mean squared error: 0.4325768381715088
Relative absolute error: 38.10946760296844
Root relative squared error: 86.51536763430177
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.620319145034854
Weighted FMeasure: 0.8087614520924765
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7495684510621201
Mean absolute error: 0.19054733801484217
Coverage of cases: 81.15942028985508
Instances selection time: 3.0
Test time: 20.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.20977194139042535
Kappa statistic: 0.6122187829635651
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8112403891950738
Weighted AreaUnderROC: 0.7995512341062079
Root mean squared error: 0.43598673580941755
Relative absolute error: 38.657905773801104
Root relative squared error: 87.19734716188351
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6127754894733722
Weighted FMeasure: 0.8065465743898805
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7465975605787636
Mean absolute error: 0.19328952886900552
Coverage of cases: 80.8695652173913
Instances selection time: 2.0
Test time: 22.0
Accumulative iteration time: 104.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20959318396149718
Kappa statistic: 0.6075085324232082
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8146351371597903
Weighted AreaUnderROC: 0.8085605493982458
Root mean squared error: 0.42941295276931857
Relative absolute error: 37.481500941372225
Root relative squared error: 85.88259055386371
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6233834206212556
Weighted FMeasure: 0.8135574550493995
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7542909727884725
Mean absolute error: 0.18740750470686113
Coverage of cases: 81.44927536231884
Instances selection time: 2.0
Test time: 21.0
Accumulative iteration time: 106.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.1973716548266969
Kappa statistic: 0.6218141956700466
Training time: 0.0
		
Time end:Sat Oct 07 11.34.24 EEST 2017