Sat Oct 07 11.35.44 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.35.44 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8202477948422141
Weighted AreaUnderROC: 0.8150540558917523
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.635203437805827
Weighted FMeasure: 0.8195668054207387
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7611767355200473
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19018174328895932
Kappa statistic: 0.6340962742481782
Training time: 26.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8103453113434613
Weighted AreaUnderROC: 0.7950465764601892
Root mean squared error: 0.4406845794337767
Relative absolute error: 38.84057971014493
Root relative squared error: 88.13691588675535
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6082295447051158
Weighted FMeasure: 0.8028771890799372
Iteration time: 33.0
Weighted AreaUnderPRC: 0.742999442710701
Mean absolute error: 0.19420289855072465
Coverage of cases: 80.57971014492753
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 66.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.2157039485288973
Kappa statistic: 0.6002870532085979
Training time: 27.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8420616525368801
Weighted AreaUnderROC: 0.8282450533759435
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.673463904083213
Weighted FMeasure: 0.8356202180599841
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7800699556887186
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 100.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.18119105266840296
Kappa statistic: 0.6665516051087331
Training time: 31.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8198698945981554
Weighted AreaUnderROC: 0.7990242741551642
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6230125428840673
Weighted FMeasure: 0.8077806879692686
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7494634413389784
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 140.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.21354565458822233
Kappa statistic: 0.6107243911330221
Training time: 34.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8243730308758663
Weighted AreaUnderROC: 0.7971374175562658
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6266882413931631
Weighted FMeasure: 0.8066631678994893
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7496660365469439
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 168.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.21731936778601926
Kappa statistic: 0.6092184368737477
Training time: 27.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8192476535431614
Weighted AreaUnderROC: 0.779016794723601
Root mean squared error: 0.45044261646145084
Relative absolute error: 40.57971014492754
Root relative squared error: 90.08852329229016
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.6045221163821184
Weighted FMeasure: 0.7891825512738894
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7339656184850673
Mean absolute error: 0.2028985507246377
Coverage of cases: 79.71014492753623
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 189.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.2390678598281603
Kappa statistic: 0.5761521990943873
Training time: 19.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8180825658005716
Weighted AreaUnderROC: 0.7680186305840755
Root mean squared error: 0.4599936987596142
Relative absolute error: 42.31884057971014
Root relative squared error: 91.99873975192284
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5927346501727319
Weighted FMeasure: 0.7781169140084869
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7250422098489367
Mean absolute error: 0.21159420289855072
Coverage of cases: 78.84057971014492
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 212.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.25236853593329805
Kappa statistic: 0.5559688993106366
Training time: 22.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.795439002386357
Weighted AreaUnderROC: 0.7471782144557012
Root mean squared error: 0.4815434123430768
Relative absolute error: 46.3768115942029
Root relative squared error: 96.30868246861536
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5483804206110338
Weighted FMeasure: 0.7565006587615284
Iteration time: 26.0
Weighted AreaUnderPRC: 0.70223406859954
Mean absolute error: 0.2318840579710145
Coverage of cases: 76.81159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 238.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.27375951311758273
Kappa statistic: 0.5130729332063088
Training time: 25.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8261228898824923
Weighted AreaUnderROC: 0.7965084653566329
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6281464686325311
Weighted FMeasure: 0.8062593666643184
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7498073744325735
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 267.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.21857727218528492
Kappa statistic: 0.608713859468514
Training time: 28.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8385922683945891
Weighted AreaUnderROC: 0.8256272523288231
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6670912586373973
Weighted FMeasure: 0.832807300053677
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7766649537707663
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 308.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18352810403800612
Kappa statistic: 0.6608137709781464
Training time: 41.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8390254148288174
Weighted AreaUnderROC: 0.8307608621744749
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6712687645546125
Weighted FMeasure: 0.8364967581604669
Iteration time: 53.0
Weighted AreaUnderPRC: 0.7805839109430791
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 361.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17615943507134038
Kappa statistic: 0.6682464454976303
Training time: 52.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8436645276035282
Weighted AreaUnderROC: 0.838512273067247
Root mean squared error: 0.39562828403747224
Relative absolute error: 31.30434782608696
Root relative squared error: 79.12565680749445
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6824992586848622
Weighted FMeasure: 0.8428485079470946
Iteration time: 53.0
Weighted AreaUnderPRC: 0.7881721930151803
Mean absolute error: 0.1565217391304348
Coverage of cases: 84.34782608695652
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 414.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.16645371473507145
Kappa statistic: 0.6813096582161552
Training time: 53.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8413662412048186
Weighted AreaUnderROC: 0.8402971374175564
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6787507168342687
Weighted FMeasure: 0.8407958714179405
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7877933575486464
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 482.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15998543530981502
Kappa statistic: 0.6784606781556605
Training time: 67.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8413662412048186
Weighted AreaUnderROC: 0.8402971374175564
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6787507168342687
Weighted FMeasure: 0.8407958714179405
Iteration time: 130.0
Weighted AreaUnderPRC: 0.7877933575486464
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 612.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15998543530981502
Kappa statistic: 0.6784606781556605
Training time: 129.0
		
Time end:Sat Oct 07 11.35.46 EEST 2017