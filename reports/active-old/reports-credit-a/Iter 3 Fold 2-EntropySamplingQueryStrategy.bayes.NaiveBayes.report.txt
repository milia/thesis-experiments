Sat Oct 07 11.34.00 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.00 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 26.3768115942029
Correctly Classified Instances: 73.6231884057971
Weighted Precision: 0.7397089553184788
Weighted AreaUnderROC: 0.8392113387788326
Root mean squared error: 0.48363513432760685
Relative absolute error: 52.10843239708255
Root relative squared error: 96.72702686552137
Weighted TruePositiveRate: 0.736231884057971
Weighted MatthewsCorrelation: 0.47200521578017723
Weighted FMeasure: 0.7369266611533785
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8307404490107112
Mean absolute error: 0.26054216198541275
Coverage of cases: 83.18840579710145
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 6.0
Weighted Recall: 0.736231884057971
Weighted FalsePositiveRate: 0.2617299462052478
Kappa statistic: 0.4706536950547135
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 57.82608695652174
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7622068192956662
Weighted AreaUnderROC: 0.850604907650207
Root mean squared error: 0.4580322972418192
Relative absolute error: 47.69247459745178
Root relative squared error: 91.60645944836384
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5165436947104642
Weighted FMeasure: 0.7605845387349693
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8403745893660317
Mean absolute error: 0.2384623729872589
Coverage of cases: 84.92753623188406
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.253275527871476
Kappa statistic: 0.5142180094786729
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7790471161180542
Weighted AreaUnderROC: 0.8747126232899053
Root mean squared error: 0.4374128102814274
Relative absolute error: 44.20055912001436
Root relative squared error: 87.48256205628549
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5391344300640966
Weighted FMeasure: 0.7654929794083664
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8641294769677992
Mean absolute error: 0.2210027956000718
Coverage of cases: 87.2463768115942
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2575855133560574
Kappa statistic: 0.525661776223046
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 59.42028985507246
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.7877662676912123
Weighted AreaUnderROC: 0.8787968462222958
Root mean squared error: 0.4279554335304992
Relative absolute error: 43.32028626098526
Root relative squared error: 85.59108670609984
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.557121902502926
Weighted FMeasure: 0.7746183058203518
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8628109838091406
Mean absolute error: 0.21660143130492632
Coverage of cases: 88.98550724637681
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.2480585504487166
Kappa statistic: 0.5439682760539863
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7960686438947309
Weighted AreaUnderROC: 0.8852280733910599
Root mean squared error: 0.429372344194771
Relative absolute error: 43.582486067136664
Root relative squared error: 85.8744688389542
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5673189529390705
Weighted FMeasure: 0.7759687303961781
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8756483980382979
Mean absolute error: 0.2179124303356833
Coverage of cases: 88.40579710144928
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 23.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.24949521227691043
Kappa statistic: 0.5479322815661198
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7951570048309178
Weighted AreaUnderROC: 0.8904105606814461
Root mean squared error: 0.43443299493388454
Relative absolute error: 45.589636725523164
Root relative squared error: 86.8865989867769
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5592165126708873
Weighted FMeasure: 0.7683862213873311
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8829599725779256
Mean absolute error: 0.22794818362761582
Coverage of cases: 88.1159420289855
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2592009326131794
Kappa statistic: 0.5340699815837938
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 61.15942028985507
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.791379279261007
Weighted AreaUnderROC: 0.8889902570303209
Root mean squared error: 0.43678822132569284
Relative absolute error: 45.48761498478013
Root relative squared error: 87.35764426513856
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5561817703387996
Weighted FMeasure: 0.7694780422541955
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8816918139158392
Mean absolute error: 0.22743807492390064
Coverage of cases: 88.69565217391305
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2566851238146481
Kappa statistic: 0.5352763150989277
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7843718071492398
Weighted AreaUnderROC: 0.8866210805659928
Root mean squared error: 0.4404537172327786
Relative absolute error: 45.77187145095821
Root relative squared error: 88.09074344655572
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5394558027122721
Weighted FMeasure: 0.7596536257810272
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8769470586350147
Mean absolute error: 0.22885935725479103
Coverage of cases: 87.2463768115942
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2674699911212545
Kappa statistic: 0.516230806983103
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 60.289855072463766
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7867047509780019
Weighted AreaUnderROC: 0.8872178583992832
Root mean squared error: 0.4406250228430723
Relative absolute error: 46.06041288285901
Root relative squared error: 88.12500456861446
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5450346065146009
Weighted FMeasure: 0.762940518862558
Iteration time: 4.0
Weighted AreaUnderPRC: 0.877390058886419
Mean absolute error: 0.23030206441429504
Coverage of cases: 86.95652173913044
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2638750353523857
Kappa statistic: 0.5225875387552769
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7832939963374745
Weighted AreaUnderROC: 0.8811359669998413
Root mean squared error: 0.44106457017452033
Relative absolute error: 45.942430867022345
Root relative squared error: 88.21291403490406
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5423556165748107
Weighted FMeasure: 0.7640203960173078
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8712655349016735
Mean absolute error: 0.22971215433511172
Coverage of cases: 86.66666666666667
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2613592265538544
Kappa statistic: 0.5238220032496462
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7790471161180542
Weighted AreaUnderROC: 0.8800104061656533
Root mean squared error: 0.4404753257169044
Relative absolute error: 45.545052597386174
Root relative squared error: 88.09506514338088
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5391344300640966
Weighted FMeasure: 0.7654929794083664
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8712004506585815
Mean absolute error: 0.22772526298693085
Coverage of cases: 86.66666666666667
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2575855133560574
Kappa statistic: 0.525661776223046
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 58.26086956521739
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7716452029060952
Weighted AreaUnderROC: 0.879810067768183
Root mean squared error: 0.4421604592786751
Relative absolute error: 45.521184534483034
Root relative squared error: 88.43209185573502
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5259940967194532
Weighted FMeasure: 0.7600202112557445
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8717922906486768
Mean absolute error: 0.22760592267241517
Coverage of cases: 85.21739130434783
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2622596160952637
Kappa statistic: 0.5142787617541238
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7728037578151995
Weighted AreaUnderROC: 0.8799005304582358
Root mean squared error: 0.4445107485176093
Relative absolute error: 46.47233653510744
Root relative squared error: 88.90214970352186
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.526818089289887
Weighted FMeasure: 0.7595560928111097
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8721740683636666
Mean absolute error: 0.2323616826755372
Coverage of cases: 86.95652173913044
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.26351752049452937
Kappa statistic: 0.5136532135957814
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7817670810612393
Weighted AreaUnderROC: 0.8891344257836405
Root mean squared error: 0.4323278807231355
Relative absolute error: 45.08772157717192
Root relative squared error: 86.4655761446271
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5411789904609113
Weighted FMeasure: 0.7645306148689407
Iteration time: 3.0
Weighted AreaUnderPRC: 0.887030063414808
Mean absolute error: 0.2254386078858596
Coverage of cases: 87.2463768115942
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2601013221545887
Kappa statistic: 0.5244368445848092
Training time: 1.0
		
Time end:Sat Oct 07 11.34.01 EEST 2017