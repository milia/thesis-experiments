Sat Oct 07 11.37.03 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.03 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8591839949590422
Weighted AreaUnderROC: 0.85851975249881
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7144934932717395
Weighted FMeasure: 0.8582235104669889
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8091062801932367
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14093150949513344
Kappa statistic: 0.7138964577656676
Training time: 27.0
		
Time end:Sat Oct 07 11.37.03 EEST 2017