Sat Oct 07 11.34.04 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.04 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 63.333333333333336
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7846052712040603
Weighted AreaUnderROC: 0.8900803376906319
Root mean squared error: 0.42656818229995636
Relative absolute error: 45.77270700284991
Root relative squared error: 85.31363645999127
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5423227125332433
Weighted FMeasure: 0.7632840922196245
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8799847907539383
Mean absolute error: 0.22886353501424958
Coverage of cases: 91.01449275362319
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.26478491759022454
Kappa statistic: 0.521816937733565
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 64.6376811594203
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8091971713258416
Weighted AreaUnderROC: 0.9004629629629628
Root mean squared error: 0.4023690252202306
Relative absolute error: 42.141225204620085
Root relative squared error: 80.47380504404612
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.5953286553682119
Weighted FMeasure: 0.7914166055006259
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8947984810819283
Mean absolute error: 0.21070612602310043
Coverage of cases: 93.33333333333333
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.23470357345836884
Kappa statistic: 0.5777381451951321
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 65.94202898550725
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.806849292245355
Weighted AreaUnderROC: 0.8964120370370372
Root mean squared error: 0.3996902053145776
Relative absolute error: 42.59788525980728
Root relative squared error: 79.93804106291552
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5897613225161564
Weighted FMeasure: 0.7882094291367081
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8871386941185145
Mean absolute error: 0.2129894262990364
Coverage of cases: 93.6231884057971
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.23834097044614946
Kappa statistic: 0.571413574090599
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 66.3768115942029
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7905590062111801
Weighted AreaUnderROC: 0.9004629629629628
Root mean squared error: 0.4171178048366665
Relative absolute error: 45.58592740115078
Root relative squared error: 83.4235609673333
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5507571540634773
Weighted FMeasure: 0.7654420851589708
Iteration time: 3.0
Weighted AreaUnderPRC: 0.888615409318815
Mean absolute error: 0.2279296370057539
Coverage of cases: 94.20289855072464
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2638027493606138
Kappa statistic: 0.5268987341772152
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 65.5072463768116
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.7991473893158532
Weighted AreaUnderROC: 0.9013139978213507
Root mean squared error: 0.41545818165815557
Relative absolute error: 45.46276206158729
Root relative squared error: 83.09163633163111
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5650843285539907
Weighted FMeasure: 0.7708869715487878
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8876614009054877
Mean absolute error: 0.22731381030793646
Coverage of cases: 92.17391304347827
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.2591831841432225
Kappa statistic: 0.5383965353332628
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 63.768115942028984
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7857260594843914
Weighted AreaUnderROC: 0.8991013071895425
Root mean squared error: 0.4293687040753784
Relative absolute error: 46.71005960224027
Root relative squared error: 85.87374081507568
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5318763223503581
Weighted FMeasure: 0.7508237154150198
Iteration time: 5.0
Weighted AreaUnderPRC: 0.884362938080563
Mean absolute error: 0.23355029801120136
Coverage of cases: 90.72463768115942
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.28100756606990623
Kappa statistic: 0.4998939329656342
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 64.05797101449275
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7969046341027017
Weighted AreaUnderROC: 0.9026416122004357
Root mean squared error: 0.426895155872414
Relative absolute error: 46.1289796760204
Root relative squared error: 85.3790311744828
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5595625397343743
Weighted FMeasure: 0.7675762118940529
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8905206953628264
Mean absolute error: 0.230644898380102
Coverage of cases: 91.01449275362319
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.26282058113100315
Kappa statistic: 0.532001479837218
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 63.91304347826087
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7789100793799975
Weighted AreaUnderROC: 0.8958333333333334
Root mean squared error: 0.4363461449801786
Relative absolute error: 47.47393591879621
Root relative squared error: 87.26922899603572
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5188982240965301
Weighted FMeasure: 0.7447462450592884
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8811578948964713
Mean absolute error: 0.23736967959398103
Coverage of cases: 90.43478260869566
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.2869547456663825
Kappa statistic: 0.4876962240135765
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7811803756883848
Weighted AreaUnderROC: 0.8957312091503269
Root mean squared error: 0.4404357190756001
Relative absolute error: 47.44417813312029
Root relative squared error: 88.08714381512002
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5244881490827233
Weighted FMeasure: 0.7481324380599816
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8806766266698544
Mean absolute error: 0.23722089066560145
Coverage of cases: 89.56521739130434
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.28331734867860187
Kappa statistic: 0.4941438338014733
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 61.73913043478261
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7750999181226494
Weighted AreaUnderROC: 0.8982162309368191
Root mean squared error: 0.43816922649574724
Relative absolute error: 46.803614716555856
Root relative squared error: 87.63384529914944
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5197445992591013
Weighted FMeasure: 0.7500981260522228
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8884033417086964
Mean absolute error: 0.2340180735827793
Coverage of cases: 89.56521739130434
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.279334505541347
Kappa statistic: 0.4962263155116905
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 61.73913043478261
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7716963248333639
Weighted AreaUnderROC: 0.8876752391777967
Root mean squared error: 0.4346507083436238
Relative absolute error: 46.06540502463087
Root relative squared error: 86.93014166872476
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5171594638177609
Weighted FMeasure: 0.751298476635808
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8681846075488825
Mean absolute error: 0.23032702512315434
Coverage of cases: 89.56521739130434
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.27667927678317705
Kappa statistic: 0.4976051371124796
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 61.01449275362319
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7839036234095341
Weighted AreaUnderROC: 0.8885301221938051
Root mean squared error: 0.4346205712464622
Relative absolute error: 45.48101144164313
Root relative squared error: 86.92411424929244
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.545600821435149
Weighted FMeasure: 0.7675785032721261
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8708590964677968
Mean absolute error: 0.22740505720821566
Coverage of cases: 88.98550724637681
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.25849229184427397
Kappa statistic: 0.5294796475031472
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7850153525857159
Weighted AreaUnderROC: 0.8891621080799469
Root mean squared error: 0.43006888427999795
Relative absolute error: 44.401669956775244
Root relative squared error: 86.0137768559996
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5502845381963992
Weighted FMeasure: 0.7712697722256497
Iteration time: 2.0
Weighted AreaUnderPRC: 0.872643247954421
Mean absolute error: 0.22200834978387624
Coverage of cases: 88.40579710144928
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.25352728047740836
Kappa statistic: 0.5364602418468305
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8021713901269868
Weighted AreaUnderROC: 0.8933829449654257
Root mean squared error: 0.42778339967258927
Relative absolute error: 43.995175668667244
Root relative squared error: 85.55667993451786
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5786259676912134
Weighted FMeasure: 0.7817622128108348
Iteration time: 8.0
Weighted AreaUnderPRC: 0.87695172709164
Mean absolute error: 0.21997587834333623
Coverage of cases: 88.98550724637681
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2456157644217107
Kappa statistic: 0.5587385019710907
Training time: 1.0
		
Time end:Sat Oct 07 11.34.05 EEST 2017