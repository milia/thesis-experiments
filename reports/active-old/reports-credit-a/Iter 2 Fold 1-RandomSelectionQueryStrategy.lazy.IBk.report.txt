Sat Oct 07 11.36.43 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.43 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.810369970294356
Weighted AreaUnderROC: 0.808874591503268
Root mean squared error: 0.43140916343579333
Relative absolute error: 40.00000000000008
Root relative squared error: 86.28183268715867
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6151579768519578
Weighted FMeasure: 0.8090974975989442
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7522490773246916
Mean absolute error: 0.2000000000000004
Coverage of cases: 80.8695652173913
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.1909464691673771
Kappa statistic: 0.6144859813084113
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8035836127199986
Weighted AreaUnderROC: 0.8016748366013072
Root mean squared error: 0.4391927909856939
Relative absolute error: 40.75171205606014
Root relative squared error: 87.83855819713878
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6019170712989217
Weighted FMeasure: 0.8031327969292095
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7447178914122428
Mean absolute error: 0.2037585602803007
Coverage of cases: 80.28985507246377
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.1995488775220233
Kappa statistic: 0.6017518842941536
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8286957118230568
Weighted AreaUnderROC: 0.8251123366013072
Root mean squared error: 0.4098954900766434
Relative absolute error: 35.38843191017094
Root relative squared error: 81.97909801532867
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6526732359749321
Weighted FMeasure: 0.8286641859573932
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7720030044149876
Mean absolute error: 0.17694215955085468
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.17876083404376242
Kappa statistic: 0.6523899789948254
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8200943359821421
Weighted AreaUnderROC: 0.8172998366013072
Root mean squared error: 0.4207430929382613
Relative absolute error: 36.920013275804784
Root relative squared error: 84.14861858765225
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6354872407653055
Weighted FMeasure: 0.8201642736035855
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7626828172112038
Mean absolute error: 0.18460006637902393
Coverage of cases: 82.02898550724638
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.1856901818698494
Kappa statistic: 0.6354432968606197
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8316168720187229
Weighted AreaUnderROC: 0.8283803104575164
Root mean squared error: 0.4073488658822998
Relative absolute error: 34.50235147326996
Root relative squared error: 81.46977317645995
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6586918116460445
Weighted FMeasure: 0.831637284382319
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7755640339641523
Mean absolute error: 0.1725117573663498
Coverage of cases: 83.18840579710145
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.17512343705598182
Kappa statistic: 0.6585091132500512
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8287447045707915
Weighted AreaUnderROC: 0.8257761437908495
Root mean squared error: 0.41115510674750605
Relative absolute error: 34.97245529282145
Root relative squared error: 82.2310213495012
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6529541229238118
Weighted FMeasure: 0.8288017477177445
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7724211784854508
Mean absolute error: 0.17486227646410724
Coverage of cases: 82.89855072463769
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.17743321966467748
Kappa statistic: 0.652852391916091
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8317055577488787
Weighted AreaUnderROC: 0.8290441176470589
Root mean squared error: 0.4079006952702455
Relative absolute error: 34.31823355338032
Root relative squared error: 81.5801390540491
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.659008654568978
Weighted FMeasure: 0.8317665785323863
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7759947384336335
Mean absolute error: 0.1715911677669016
Coverage of cases: 83.18840579710145
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.17379582267689686
Kappa statistic: 0.6589630841599345
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8491014851263298
Weighted AreaUnderROC: 0.8453329248366014
Root mean squared error: 0.386417429006602
Relative absolute error: 30.807060924513944
Root relative squared error: 77.2834858013204
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.693861914173872
Weighted FMeasure: 0.848927536231884
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7956724167202158
Mean absolute error: 0.15403530462256973
Coverage of cases: 84.92753623188406
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.15860951264563797
Kappa statistic: 0.6934276632830924
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8497977544394262
Weighted AreaUnderROC: 0.8433415032679739
Root mean squared error: 0.38657263578999107
Relative absolute error: 30.749733358428987
Root relative squared error: 77.31452715799821
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6939496957415776
Weighted FMeasure: 0.848483055936472
Iteration time: 1.0
Weighted AreaUnderPRC: 0.794719496829554
Mean absolute error: 0.15374866679214494
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.16259235578289286
Kappa statistic: 0.6921968293185093
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8389375778141248
Weighted AreaUnderROC: 0.8302696078431373
Root mean squared error: 0.40129955909253145
Relative absolute error: 33.00190542179116
Root relative squared error: 80.2599118185063
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.670660896490702
Weighted FMeasure: 0.8364497751124438
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7804235635764618
Mean absolute error: 0.16500952710895578
Coverage of cases: 83.76811594202898
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17714194373401534
Kappa statistic: 0.6676300578034682
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8428391731841514
Weighted AreaUnderROC: 0.8322099673202613
Root mean squared error: 0.39781575223076976
Relative absolute error: 32.386758650195176
Root relative squared error: 79.56315044615395
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6772442977786193
Weighted FMeasure: 0.8390607784443518
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7835714465595672
Mean absolute error: 0.1619337932509759
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.17615977550440465
Kappa statistic: 0.6729068624915964
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8368920901365827
Weighted AreaUnderROC: 0.8263378267973855
Root mean squared error: 0.4050847881764214
Relative absolute error: 33.50366054086372
Root relative squared error: 81.01695763528429
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6652733334004235
Weighted FMeasure: 0.8332084431150555
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7767064310231959
Mean absolute error: 0.16751830270431858
Coverage of cases: 83.47826086956522
Instances selection time: 0.0
Test time: 20.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18210695510088093
Kappa statistic: 0.6610125665821999
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8368920901365827
Weighted AreaUnderROC: 0.8263378267973855
Root mean squared error: 0.40517309764875375
Relative absolute error: 33.47406682510831
Root relative squared error: 81.03461952975076
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6652733334004235
Weighted FMeasure: 0.8332084431150555
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7767064310231959
Mean absolute error: 0.16737033412554153
Coverage of cases: 83.47826086956522
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18210695510088093
Kappa statistic: 0.6610125665821999
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8357277882797732
Weighted AreaUnderROC: 0.8276654411764705
Root mean squared error: 0.4052508189321978
Relative absolute error: 33.44804938920269
Root relative squared error: 81.05016378643955
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6645570174272217
Weighted FMeasure: 0.8336300185695076
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7771266540642723
Mean absolute error: 0.16724024694601344
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17945172634271098
Kappa statistic: 0.6619217081850534
Training time: 0.0
		
Time end:Sat Oct 07 11.36.44 EEST 2017