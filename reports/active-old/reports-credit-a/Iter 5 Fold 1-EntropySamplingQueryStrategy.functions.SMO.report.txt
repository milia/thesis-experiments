Sat Oct 07 11.34.18 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.18 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8591839949590422
Weighted AreaUnderROC: 0.85851975249881
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7144934932717395
Weighted FMeasure: 0.8582235104669889
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8091062801932367
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14093150949513344
Kappa statistic: 0.7138964577656676
Training time: 23.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.859569845329436
Weighted AreaUnderROC: 0.858417760250221
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.712776013464124
Weighted FMeasure: 0.8554831064532499
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8084500925764131
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 43.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13823694326767397
Kappa statistic: 0.7096937058229553
Training time: 18.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8771717171717172
Weighted AreaUnderROC: 0.8732916298361323
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7429799038026831
Weighted FMeasure: 0.8669873735674717
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8266674304481487
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 69.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12008340699440176
Kappa statistic: 0.7345709984947316
Training time: 24.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8771717171717172
Weighted AreaUnderROC: 0.8732916298361323
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7429799038026831
Weighted FMeasure: 0.8669873735674717
Iteration time: 41.0
Weighted AreaUnderPRC: 0.8266674304481487
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12008340699440176
Kappa statistic: 0.7345709984947316
Training time: 40.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8771717171717172
Weighted AreaUnderROC: 0.8732916298361323
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7429799038026831
Weighted FMeasure: 0.8669873735674717
Iteration time: 53.0
Weighted AreaUnderPRC: 0.8266674304481487
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 163.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12008340699440176
Kappa statistic: 0.7345709984947316
Training time: 52.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8765528893112642
Weighted AreaUnderROC: 0.8713027809886448
Root mean squared error: 0.3690960363617727
Relative absolute error: 27.246376811594203
Root relative squared error: 73.81920727235453
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7396941594880394
Weighted FMeasure: 0.8640337979479086
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8244805299892225
Mean absolute error: 0.13623188405797101
Coverage of cases: 86.3768115942029
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 271.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12116255396473924
Kappa statistic: 0.7293033505283717
Training time: 103.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8765528893112642
Weighted AreaUnderROC: 0.8713027809886448
Root mean squared error: 0.3690960363617727
Relative absolute error: 27.246376811594203
Root relative squared error: 73.81920727235453
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7396941594880394
Weighted FMeasure: 0.8640337979479086
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8244805299892225
Mean absolute error: 0.13623188405797101
Coverage of cases: 86.3768115942029
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 338.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12116255396473924
Kappa statistic: 0.7293033505283717
Training time: 66.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8735400187097674
Weighted AreaUnderROC: 0.8700448765893792
Root mean squared error: 0.3690960363617727
Relative absolute error: 27.246376811594203
Root relative squared error: 73.81920727235453
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7363442092795711
Weighted FMeasure: 0.864111901452055
Iteration time: 88.0
Weighted AreaUnderPRC: 0.8226368661927034
Mean absolute error: 0.13623188405797101
Coverage of cases: 86.3768115942029
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 426.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12367836276327059
Kappa statistic: 0.7286328719896908
Training time: 87.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8735400187097674
Weighted AreaUnderROC: 0.8700448765893792
Root mean squared error: 0.3690960363617727
Relative absolute error: 27.246376811594203
Root relative squared error: 73.81920727235453
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7363442092795711
Weighted FMeasure: 0.864111901452055
Iteration time: 183.0
Weighted AreaUnderPRC: 0.8226368661927034
Mean absolute error: 0.13623188405797101
Coverage of cases: 86.3768115942029
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 609.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12367836276327059
Kappa statistic: 0.7286328719896908
Training time: 182.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8744575936883628
Weighted AreaUnderROC: 0.8720337254368669
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7399283452375766
Weighted FMeasure: 0.8670370370370369
Iteration time: 204.0
Weighted AreaUnderPRC: 0.8249274288957289
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 813.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12259921579293306
Kappa statistic: 0.7339123436504478
Training time: 204.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8744575936883628
Weighted AreaUnderROC: 0.8720337254368669
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7399283452375766
Weighted FMeasure: 0.8670370370370369
Iteration time: 219.0
Weighted AreaUnderPRC: 0.8249274288957289
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1032.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12259921579293306
Kappa statistic: 0.7339123436504478
Training time: 218.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8744575936883628
Weighted AreaUnderROC: 0.8720337254368669
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7399283452375766
Weighted FMeasure: 0.8670370370370369
Iteration time: 179.0
Weighted AreaUnderPRC: 0.8249274288957289
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 1211.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12259921579293306
Kappa statistic: 0.7339123436504478
Training time: 179.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8744575936883628
Weighted AreaUnderROC: 0.8720337254368669
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7399283452375766
Weighted FMeasure: 0.8670370370370369
Iteration time: 136.0
Weighted AreaUnderPRC: 0.8249274288957289
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1347.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12259921579293306
Kappa statistic: 0.7339123436504478
Training time: 135.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8744575936883628
Weighted AreaUnderROC: 0.8720337254368669
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7399283452375766
Weighted FMeasure: 0.8670370370370369
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8249274288957289
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1513.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12259921579293306
Kappa statistic: 0.7339123436504478
Training time: 165.0
		
Time end:Sat Oct 07 11.34.20 EEST 2017