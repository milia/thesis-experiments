Sat Oct 07 11.36.57 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.57 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 92.7536231884058
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8398863872856938
Weighted AreaUnderROC: 0.8994913031637775
Root mean squared error: 0.352540216733865
Relative absolute error: 53.9559559347899
Root relative squared error: 70.508043346773
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6744771581046499
Weighted FMeasure: 0.8380794575297472
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8896496560800674
Mean absolute error: 0.2697797796739495
Coverage of cases: 99.71014492753623
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.15988295680591075
Kappa statistic: 0.6733285989652024
Training time: 3.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 92.46376811594203
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8463768115942029
Weighted AreaUnderROC: 0.9092644394714408
Root mean squared error: 0.3512734014296592
Relative absolute error: 55.12749927180318
Root relative squared error: 70.25468028593184
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6870913818361165
Weighted FMeasure: 0.8439069570509937
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8978528187747612
Mean absolute error: 0.2756374963590159
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15260816283034956
Kappa statistic: 0.6854103343465047
Training time: 4.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 90.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8562927676597536
Weighted AreaUnderROC: 0.8972274024344036
Root mean squared error: 0.34718668219894777
Relative absolute error: 49.86322914489359
Root relative squared error: 69.43733643978956
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7061641311633703
Weighted FMeasure: 0.8526231983165018
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8768364503122563
Mean absolute error: 0.24931614572446795
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.14169597186700766
Kappa statistic: 0.7034733808584862
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8756728639309865
Weighted AreaUnderROC: 0.9023195510088093
Root mean squared error: 0.34361245519494693
Relative absolute error: 47.607181005735896
Root relative squared error: 68.72249103898939
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7435721421361756
Weighted FMeasure: 0.8699830485178907
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8900337506169782
Mean absolute error: 0.23803590502867947
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.12119920431940892
Kappa statistic: 0.7390449296555897
Training time: 5.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 83.18840579710145
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8497977544394262
Weighted AreaUnderROC: 0.9304458830633703
Root mean squared error: 0.3251982807757049
Relative absolute error: 42.93545031047294
Root relative squared error: 65.03965615514097
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6939496957415776
Weighted FMeasure: 0.848483055936472
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9270747954501823
Mean absolute error: 0.2146772515523647
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.16259235578289286
Kappa statistic: 0.6921968293185093
Training time: 6.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 89.42028985507247
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8463768115942029
Weighted AreaUnderROC: 0.8976320510088094
Root mean squared error: 0.3577194940894147
Relative absolute error: 52.91325727978271
Root relative squared error: 71.54389881788295
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6870913818361165
Weighted FMeasure: 0.8439069570509937
Iteration time: 8.0
Weighted AreaUnderPRC: 0.885983888760027
Mean absolute error: 0.26456628639891355
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15260816283034956
Kappa statistic: 0.6854103343465047
Training time: 8.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 84.6376811594203
Incorrectly Classified Instances: 12.753623188405797
Correctly Classified Instances: 87.2463768115942
Weighted Precision: 0.8734217188423438
Weighted AreaUnderROC: 0.9120012195699536
Root mean squared error: 0.3334032547122794
Relative absolute error: 44.22513209831896
Root relative squared error: 66.68065094245588
Weighted TruePositiveRate: 0.8724637681159421
Weighted MatthewsCorrelation: 0.7431085173614994
Weighted FMeasure: 0.872677885721364
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8929503812351759
Mean absolute error: 0.2211256604915948
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8724637681159421
Weighted FalsePositiveRate: 0.12685510798522306
Kappa statistic: 0.7426507984945582
Training time: 8.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 90.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8030927052364019
Weighted AreaUnderROC: 0.880958280051151
Root mean squared error: 0.3687763229893278
Relative absolute error: 55.19384926282872
Root relative squared error: 73.75526459786556
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.5990146990954655
Weighted FMeasure: 0.8016401243860757
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8702551121465366
Mean absolute error: 0.2759692463141436
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 53.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.21149740693378802
Kappa statistic: 0.5969487681682301
Training time: 9.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 90.57971014492753
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8229027807291371
Weighted AreaUnderROC: 0.875234441602728
Root mean squared error: 0.37235251385964474
Relative absolute error: 56.80942756917764
Root relative squared error: 74.47050277192895
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6406737710410295
Weighted FMeasure: 0.8227014173416931
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8680497244846216
Mean absolute error: 0.2840471378458882
Coverage of cases: 99.71014492753623
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 65.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.18603562801932363
Kappa statistic: 0.6401272251577488
Training time: 11.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 11.304347826086957
Correctly Classified Instances: 88.69565217391305
Weighted Precision: 0.8868494983277594
Weighted AreaUnderROC: 0.9197605853935777
Root mean squared error: 0.32778365477412774
Relative absolute error: 45.974255401352245
Root relative squared error: 65.55673095482555
Weighted TruePositiveRate: 0.8869565217391304
Weighted MatthewsCorrelation: 0.7706496153003609
Weighted FMeasure: 0.8868350535761362
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9028299485179659
Mean absolute error: 0.22987127700676122
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8869565217391304
Weighted FalsePositiveRate: 0.11796142369991476
Kappa statistic: 0.7705295471987721
Training time: 13.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 88.84057971014492
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8432739103546556
Weighted AreaUnderROC: 0.9088926482428722
Root mean squared error: 0.34365662020431315
Relative absolute error: 50.19699523510982
Root relative squared error: 68.73132404086263
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6820632863267633
Weighted FMeasure: 0.8431170568561873
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8957401496087927
Mean absolute error: 0.2509849761755491
Coverage of cases: 98.55072463768116
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.16455669224211425
Kappa statistic: 0.6816364195632113
Training time: 12.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 84.92753623188406
Incorrectly Classified Instances: 11.884057971014492
Correctly Classified Instances: 88.1159420289855
Weighted Precision: 0.8810888354697668
Weighted AreaUnderROC: 0.9199901427962489
Root mean squared error: 0.32702428713946663
Relative absolute error: 44.638930208937
Root relative squared error: 65.40485742789332
Weighted TruePositiveRate: 0.881159420289855
Weighted MatthewsCorrelation: 0.7587697962441411
Weighted FMeasure: 0.8809361292246293
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9054439445039811
Mean absolute error: 0.223194651044685
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 103.0
Weighted Recall: 0.881159420289855
Weighted FalsePositiveRate: 0.125236217675476
Kappa statistic: 0.75844049387776
Training time: 13.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 86.08695652173913
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8658778864156438
Weighted AreaUnderROC: 0.9053351745287487
Root mean squared error: 0.3388857306938541
Relative absolute error: 46.065527865507704
Root relative squared error: 67.77714613877082
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7245619541700574
Weighted FMeasure: 0.8626490040882449
Iteration time: 22.0
Weighted AreaUnderPRC: 0.8926023469254369
Mean absolute error: 0.23032763932753852
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 125.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.1510434427394146
Kappa statistic: 0.7208593709652429
Training time: 21.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 86.52173913043478
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8325529698655597
Weighted AreaUnderROC: 0.8881485625651226
Root mean squared error: 0.3606813355917017
Relative absolute error: 50.839142571740105
Root relative squared error: 72.13626711834034
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6584920694943418
Weighted FMeasure: 0.8308106943292998
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8763094861309648
Mean absolute error: 0.2541957128587005
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 141.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.18176150895140664
Kappa statistic: 0.6562210081434904
Training time: 16.0
		
Time end:Sat Oct 07 11.36.58 EEST 2017