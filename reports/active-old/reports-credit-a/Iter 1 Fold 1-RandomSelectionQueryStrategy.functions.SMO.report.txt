Sat Oct 07 11.36.26 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.26 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8713365539452496
Weighted AreaUnderROC: 0.8674270755422588
Root mean squared error: 0.3730019232961255
Relative absolute error: 27.82608695652174
Root relative squared error: 74.6003846592251
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7313074053139799
Weighted FMeasure: 0.8612042158964921
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8195162080795351
Mean absolute error: 0.1391304347826087
Coverage of cases: 86.08695652173913
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.12601541413287373
Kappa statistic: 0.7230306071249374
Training time: 29.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8723447904426165
Weighted AreaUnderROC: 0.866067178894404
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7298304457613441
Weighted FMeasure: 0.8581930299000967
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8183107753310968
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 53.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1258366567039456
Kappa statistic: 0.7181325552313463
Training time: 16.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8669867520227617
Weighted AreaUnderROC: 0.8621914734480179
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7212997233858078
Weighted FMeasure: 0.8553795136330139
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8133360099155035
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 71.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13068951687208002
Kappa statistic: 0.7118468528664975
Training time: 17.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 92.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 21.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8808361113877773
Weighted AreaUnderROC: 0.8765383830828857
Root mean squared error: 0.3611575592573076
Relative absolute error: 26.08695652173913
Root relative squared error: 72.23151185146152
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7496440211506442
Weighted FMeasure: 0.8698613484932959
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8307302884650899
Mean absolute error: 0.13043478260869565
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.11648845122553295
Kappa statistic: 0.7405017801327158
Training time: 25.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8771717171717172
Weighted AreaUnderROC: 0.8732916298361323
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7429799038026831
Weighted FMeasure: 0.8669873735674717
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8266674304481487
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 153.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12008340699440176
Kappa statistic: 0.7345709984947316
Training time: 35.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.861155266409048
Weighted AreaUnderROC: 0.8604066090977086
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7170204617733981
Weighted FMeasure: 0.8583451252364346
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8109328034386681
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 188.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1371577962973365
Kappa statistic: 0.7149673742602303
Training time: 34.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8651321398124466
Weighted AreaUnderROC: 0.8629224178962398
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7217343081555156
Weighted FMeasure: 0.8583728174811635
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8138522554579488
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 233.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13212617870027385
Kappa statistic: 0.7163828537874338
Training time: 44.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8581927210880378
Weighted AreaUnderROC: 0.8564289114027334
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7087493749236508
Weighted FMeasure: 0.8525998279198409
Iteration time: 70.0
Weighted AreaUnderPRC: 0.8060525725455788
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 303.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13931609023801148
Kappa statistic: 0.7044397036837948
Training time: 69.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.849935404909227
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 127.0
Weighted AreaUnderPRC: 0.7983692923434037
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 430.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 126.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.849935404909227
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 141.0
Weighted AreaUnderPRC: 0.7983692923434037
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 571.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 141.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.849935404909227
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 255.0
Weighted AreaUnderPRC: 0.7983692923434037
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 826.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 254.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.849935404909227
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 137.0
Weighted AreaUnderPRC: 0.7983692923434037
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 963.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 137.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.849935404909227
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 170.0
Weighted AreaUnderPRC: 0.7983692923434037
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1133.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 169.0
		
Time end:Sat Oct 07 11.36.27 EEST 2017