Sat Oct 07 11.37.05 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.05 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 90.14492753623189
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.732205090731945
Weighted AreaUnderROC: 0.8123342625960428
Root mean squared error: 0.4276585612604975
Relative absolute error: 64.0495291984038
Root relative squared error: 85.5317122520995
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.45619693154841606
Weighted FMeasure: 0.7283082401321097
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8022753288866675
Mean absolute error: 0.320247645992019
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.2687411003140573
Kappa statistic: 0.4542241669471557
Training time: 3.0
		
Time end:Sat Oct 07 11.37.05 EEST 2017