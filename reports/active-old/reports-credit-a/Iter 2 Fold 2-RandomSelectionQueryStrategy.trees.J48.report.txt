Sat Oct 07 11.36.52 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.52 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 56.08695652173913
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8277664924925661
Weighted AreaUnderROC: 0.8835418508193378
Root mean squared error: 0.38100400249798494
Relative absolute error: 36.739130434782645
Root relative squared error: 76.200800499597
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6478005651799896
Weighted FMeasure: 0.8246026986506747
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8438735372759479
Mean absolute error: 0.18369565217391323
Coverage of cases: 90.14492753623189
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.18928135374754998
Kappa statistic: 0.6440975207179945
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.847105543273561
Weighted AreaUnderROC: 0.8667641259264296
Root mean squared error: 0.37243395033208254
Relative absolute error: 34.18274819809356
Root relative squared error: 74.4867900664165
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6883618481658159
Weighted FMeasure: 0.8439035916824197
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8428843697022119
Mean absolute error: 0.1709137409904678
Coverage of cases: 91.59420289855072
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15135886194388354
Kappa statistic: 0.6860782529572339
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 54.63768115942029
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8665484955913099
Weighted AreaUnderROC: 0.8581457809206501
Root mean squared error: 0.3491814770290757
Relative absolute error: 32.6418777143415
Root relative squared error: 69.83629540581514
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7299410688584402
Weighted FMeasure: 0.8665781590413943
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8308783628037832
Mean absolute error: 0.16320938857170747
Coverage of cases: 89.56521739130434
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.13769406858412092
Kappa statistic: 0.7298907308438575
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 77.97101449275362
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8665484955913099
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.3510229221394088
Relative absolute error: 34.20553359683798
Root relative squared error: 70.20458442788176
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7299410688584402
Weighted FMeasure: 0.8665781590413943
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8263075958361482
Mean absolute error: 0.17102766798418992
Coverage of cases: 92.7536231884058
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.13769406858412092
Kappa statistic: 0.7298907308438575
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.3471448828426704
Relative absolute error: 44.214160167555974
Root relative squared error: 69.42897656853408
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.22107080083777988
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.34874963183143953
Relative absolute error: 42.40623900359262
Root relative squared error: 69.74992636628791
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.21203119501796308
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 2.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.3493231957614801
Relative absolute error: 41.65486113828758
Root relative squared error: 69.86463915229602
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.2082743056914379
Coverage of cases: 96.23188405797102
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 3.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.34974185955539155
Relative absolute error: 41.10495944688207
Root relative squared error: 69.94837191107831
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.20552479723441033
Coverage of cases: 96.23188405797102
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 3.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.3473952508416369
Relative absolute error: 43.39700053775231
Root relative squared error: 69.47905016832738
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.21698500268876156
Coverage of cases: 96.23188405797102
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 24.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 3.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.3468758326060905
Relative absolute error: 44.941663621970555
Root relative squared error: 69.3751665212181
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.22470831810985278
Coverage of cases: 96.23188405797102
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 3.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8641173929619997
Weighted AreaUnderROC: 0.8609335690487523
Root mean squared error: 0.3466202782343564
Relative absolute error: 45.52606813476377
Root relative squared error: 69.32405564687129
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7180688361868957
Weighted FMeasure: 0.8554529022087866
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8115650545089809
Mean absolute error: 0.22763034067381885
Coverage of cases: 96.23188405797102
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13320532567061136
Kappa statistic: 0.7111326947551745
Training time: 3.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 11.884057971014492
Correctly Classified Instances: 88.1159420289855
Weighted Precision: 0.8815417291354323
Weighted AreaUnderROC: 0.8968858366764126
Root mean squared error: 0.3246156315307548
Relative absolute error: 36.668351274344765
Root relative squared error: 64.92312630615096
Weighted TruePositiveRate: 0.881159420289855
Weighted MatthewsCorrelation: 0.7592548687211349
Weighted FMeasure: 0.8807385801492329
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8660203024230577
Mean absolute error: 0.18334175637172384
Coverage of cases: 98.55072463768116
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 41.0
Weighted Recall: 0.881159420289855
Weighted FalsePositiveRate: 0.12726671613537083
Kappa statistic: 0.7581844602102743
Training time: 4.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8623431858246146
Weighted AreaUnderROC: 0.9051982049364248
Root mean squared error: 0.3255859741491107
Relative absolute error: 40.54217256991024
Root relative squared error: 65.11719482982214
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7206963833825233
Weighted FMeasure: 0.8611426839210193
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8721099680759089
Mean absolute error: 0.20271086284955123
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 53.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.1373365537262646
Kappa statistic: 0.7199106961639944
Training time: 9.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 95.94202898550725
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8699231662119882
Weighted AreaUnderROC: 0.8871285782280547
Root mean squared error: 0.3341351777576703
Relative absolute error: 37.86349422769046
Root relative squared error: 66.82703555153407
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7367056682872839
Weighted FMeasure: 0.869678091410988
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8568185760696507
Mean absolute error: 0.1893174711384523
Coverage of cases: 97.97101449275362
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1315833040167208
Kappa statistic: 0.7365920697670473
Training time: 6.0
		
Time end:Sat Oct 07 11.36.52 EEST 2017