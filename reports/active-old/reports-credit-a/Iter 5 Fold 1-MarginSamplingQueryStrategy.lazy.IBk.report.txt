Sat Oct 07 11.36.03 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.03 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.3768115942029
Correctly Classified Instances: 73.6231884057971
Weighted Precision: 0.7353632018720266
Weighted AreaUnderROC: 0.7297035425307676
Root mean squared error: 0.5064942265294221
Relative absolute error: 54.08450704225367
Root relative squared error: 101.29884530588443
Weighted TruePositiveRate: 0.736231884057971
Weighted MatthewsCorrelation: 0.46358676556509687
Weighted FMeasure: 0.7350387006101134
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6723918659133618
Mean absolute error: 0.27042253521126836
Coverage of cases: 73.6231884057971
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5.0
Weighted Recall: 0.736231884057971
Weighted FalsePositiveRate: 0.27682479899643575
Kappa statistic: 0.46260762397083244
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7505636070853463
Weighted AreaUnderROC: 0.7483511253144761
Root mean squared error: 0.4967417715502087
Relative absolute error: 51.524128045867265
Root relative squared error: 99.34835431004174
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.4943049359998725
Weighted FMeasure: 0.7484354857611625
Iteration time: 10.0
Weighted AreaUnderPRC: 0.688879978104931
Mean absolute error: 0.25762064022933634
Coverage of cases: 74.78260869565217
Instances selection time: 10.0
Test time: 8.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.25112383632756957
Kappa statistic: 0.4932894403646493
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.797101449275363
Correctly Classified Instances: 74.20289855072464
Weighted Precision: 0.7500260261314057
Weighted AreaUnderROC: 0.7462602842183994
Root mean squared error: 0.5033925410142874
Relative absolute error: 52.46637942290102
Root relative squared error: 100.67850820285749
Weighted TruePositiveRate: 0.7420289855072464
Weighted MatthewsCorrelation: 0.4898466496894673
Weighted FMeasure: 0.742728029231141
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6867622907674492
Mean absolute error: 0.2623318971145051
Coverage of cases: 74.20289855072464
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7420289855072464
Weighted FalsePositiveRate: 0.24950841707044755
Kappa statistic: 0.48549741114965067
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.797101449275363
Correctly Classified Instances: 74.20289855072464
Weighted Precision: 0.7577501135472149
Weighted AreaUnderROC: 0.7500339974161964
Root mean squared error: 0.5040741982926356
Relative absolute error: 52.333222701626234
Root relative squared error: 100.8148396585271
Weighted TruePositiveRate: 0.7420289855072464
Weighted MatthewsCorrelation: 0.4994740190973686
Weighted FMeasure: 0.7421807063885573
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6909811966837801
Mean absolute error: 0.2616661135081312
Coverage of cases: 74.20289855072464
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7420289855072464
Weighted FalsePositiveRate: 0.24196099067485366
Kappa statistic: 0.4892969412703957
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7563114134542707
Weighted AreaUnderROC: 0.7440674508737336
Root mean squared error: 0.5130092743432458
Relative absolute error: 53.95143487858718
Root relative squared error: 102.60185486864917
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.4907108294052479
Weighted FMeasure: 0.7326429980276135
Iteration time: 7.0
Weighted AreaUnderPRC: 0.685887892725592
Mean absolute error: 0.2697571743929359
Coverage of cases: 73.33333333333333
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.24519843158586613
Kappa statistic: 0.47499007542675653
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7601802757158006
Weighted AreaUnderROC: 0.7433365064255115
Root mean squared error: 0.5161846630693244
Relative absolute error: 54.452072209509296
Root relative squared error: 103.23693261386488
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.4926895602215488
Weighted FMeasure: 0.7288150638092663
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6860074999615781
Mean absolute error: 0.2722603610475465
Coverage of cases: 73.04347826086956
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.24376176975767233
Kappa statistic: 0.4715473935600758
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.768303374387066
Weighted AreaUnderROC: 0.7518188617665057
Root mean squared error: 0.5080997522757992
Relative absolute error: 52.67470976553613
Root relative squared error: 101.61995045515984
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.509296458504867
Weighted FMeasure: 0.7377037906300938
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6943120194088441
Mean absolute error: 0.26337354882768066
Coverage of cases: 73.91304347826087
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.2354927112495972
Kappa statistic: 0.4882824087807771
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.785158829753205
Weighted AreaUnderROC: 0.7767389678384443
Root mean squared error: 0.4792792152026669
Relative absolute error: 46.885088261556334
Root relative squared error: 95.85584304053339
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5531395435104038
Weighted FMeasure: 0.7681860776830296
Iteration time: 7.0
Weighted AreaUnderPRC: 0.718139937929463
Mean absolute error: 0.23442544130778167
Coverage of cases: 76.81159420289855
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.21463800635209695
Kappa statistic: 0.5412234042553191
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8000661110607316
Weighted AreaUnderROC: 0.796423471816142
Root mean squared error: 0.4548708648495551
Relative absolute error: 42.24355354790134
Root relative squared error: 90.97417296991101
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5897275168571133
Weighted FMeasure: 0.7918521791806526
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7382025465010592
Mean absolute error: 0.2112177677395067
Coverage of cases: 79.1304347826087
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.198457404193803
Kappa statistic: 0.5840310804474512
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8108200285822003
Weighted AreaUnderROC: 0.8088835248521112
Root mean squared error: 0.4389434335538713
Relative absolute error: 39.32790576823159
Root relative squared error: 87.78868671077426
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.614227285541712
Weighted FMeasure: 0.8063537209266811
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7516502542016559
Mean absolute error: 0.19663952884115796
Coverage of cases: 80.57971014492753
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 66.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.18803005174505288
Kappa statistic: 0.6112316463999193
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8167083618950531
Weighted AreaUnderROC: 0.8154790235942069
Root mean squared error: 0.4291291246004002
Relative absolute error: 37.56564522166945
Root relative squared error: 85.82582492008004
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6280819551503231
Weighted FMeasure: 0.8149161572591671
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7591635280262607
Mean absolute error: 0.18782822610834723
Coverage of cases: 81.44927536231884
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 71.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.18353470643477474
Kappa statistic: 0.627014426163046
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8244959042218022
Weighted AreaUnderROC: 0.8233324267355682
Root mean squared error: 0.41905632469537796
Relative absolute error: 35.806564071916256
Root relative squared error: 83.81126493907558
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6443687229397259
Weighted FMeasure: 0.8235027375201288
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7681665016353108
Mean absolute error: 0.17903282035958126
Coverage of cases: 82.31884057971014
Instances selection time: 4.0
Test time: 19.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.17652355232596526
Kappa statistic: 0.6438302841572595
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8182307608622144
Weighted AreaUnderROC: 0.8168389202420617
Root mean squared error: 0.425963263933167
Relative absolute error: 36.929959457570135
Root relative squared error: 85.1926527866334
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6319613672548047
Weighted FMeasure: 0.8176389072605501
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7610436567244794
Mean absolute error: 0.1846497972878507
Coverage of cases: 81.73913043478261
Instances selection time: 3.0
Test time: 20.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.1837134638637029
Kappa statistic: 0.6316913222510295
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8055231174608639
Weighted AreaUnderROC: 0.802594002855783
Root mean squared error: 0.4393615760111488
Relative absolute error: 39.21012303515918
Root relative squared error: 87.87231520222976
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6064262855611315
Weighted FMeasure: 0.8055986398965729
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7461884648641035
Mean absolute error: 0.1960506151757959
Coverage of cases: 80.57971014492753
Instances selection time: 2.0
Test time: 21.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.2006090957377094
Kappa statistic: 0.6063320673740145
Training time: 0.0
		
Time end:Sat Oct 07 11.36.04 EEST 2017