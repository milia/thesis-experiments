Sat Oct 07 11.34.31 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.34.31 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 80.05797101449276
Incorrectly Classified Instances: 18.347826086956523
Correctly Classified Instances: 81.65217391304347
Weighted Precision: 0.8275789625271992
Weighted AreaUnderROC: 0.8242005505729713
Root mean squared error: 0.39333385225780876
Relative absolute error: 45.81324522097965
Root relative squared error: 78.66677045156175
Weighted TruePositiveRate: 0.8165217391304347
Weighted MatthewsCorrelation: 0.6388789887353965
Weighted FMeasure: 0.8144334431074904
Iteration time: 11.9
Weighted AreaUnderPRC: 0.7951642157687393
Mean absolute error: 0.2290662261048982
Coverage of cases: 92.98550724637681
Instances selection time: 4.0
Test time: 4.3
Accumulative iteration time: 11.9
Weighted Recall: 0.8165217391304347
Weighted FalsePositiveRate: 0.18968074501820956
Kappa statistic: 0.6272105006180368
Training time: 7.9
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 82.53623188405797
Incorrectly Classified Instances: 20.811594202898554
Correctly Classified Instances: 79.18840579710145
Weighted Precision: 0.8005052654834458
Weighted AreaUnderROC: 0.7812623674628505
Root mean squared error: 0.4158289876923574
Relative absolute error: 52.621871398453
Root relative squared error: 83.16579753847147
Weighted TruePositiveRate: 0.7918840579710145
Weighted MatthewsCorrelation: 0.5837345463094054
Weighted FMeasure: 0.7879623752457582
Iteration time: 4.9
Weighted AreaUnderPRC: 0.7535706160068595
Mean absolute error: 0.26310935699226506
Coverage of cases: 93.53623188405797
Instances selection time: 2.3
Test time: 3.3
Accumulative iteration time: 16.8
Weighted Recall: 0.7918840579710145
Weighted FalsePositiveRate: 0.22718605210279383
Kappa statistic: 0.5717127466946497
Training time: 2.6
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 81.86956521739131
Incorrectly Classified Instances: 20.956521739130434
Correctly Classified Instances: 79.04347826086955
Weighted Precision: 0.8001645124832324
Weighted AreaUnderROC: 0.7836280797983428
Root mean squared error: 0.41874342247255925
Relative absolute error: 51.45746838887277
Root relative squared error: 83.74868449451185
Weighted TruePositiveRate: 0.7904347826086957
Weighted MatthewsCorrelation: 0.5818666502392814
Weighted FMeasure: 0.7863390499849461
Iteration time: 6.1
Weighted AreaUnderPRC: 0.7510049579355764
Mean absolute error: 0.25728734194436387
Coverage of cases: 92.81159420289855
Instances selection time: 2.8
Test time: 2.5
Accumulative iteration time: 22.9
Weighted Recall: 0.7904347826086957
Weighted FalsePositiveRate: 0.22801782768615864
Kappa statistic: 0.5688221135930177
Training time: 3.3
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 79.0
Incorrectly Classified Instances: 20.318840579710148
Correctly Classified Instances: 79.68115942028984
Weighted Precision: 0.8053876841791624
Weighted AreaUnderROC: 0.7989704214192684
Root mean squared error: 0.4156834415619559
Relative absolute error: 47.57309556596097
Root relative squared error: 83.1366883123912
Weighted TruePositiveRate: 0.7968115942028986
Weighted MatthewsCorrelation: 0.5930368476045504
Weighted FMeasure: 0.7926728536709222
Iteration time: 5.9
Weighted AreaUnderPRC: 0.7772012204784959
Mean absolute error: 0.23786547782980483
Coverage of cases: 91.53623188405797
Instances selection time: 3.0
Test time: 3.5
Accumulative iteration time: 28.8
Weighted Recall: 0.7968115942028986
Weighted FalsePositiveRate: 0.22672155299047514
Kappa statistic: 0.5805018889526113
Training time: 2.9
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 74.94202898550725
Incorrectly Classified Instances: 20.289855072463773
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.804759690720538
Weighted AreaUnderROC: 0.7760560307370343
Root mean squared error: 0.4243322169426942
Relative absolute error: 49.26468605258822
Root relative squared error: 84.86644338853885
Weighted TruePositiveRate: 0.7971014492753622
Weighted MatthewsCorrelation: 0.5930225477449389
Weighted FMeasure: 0.7933211822423435
Iteration time: 6.9
Weighted AreaUnderPRC: 0.7575697157772303
Mean absolute error: 0.24632343026294107
Coverage of cases: 88.4927536231884
Instances selection time: 3.5
Test time: 2.4
Accumulative iteration time: 35.7
Weighted Recall: 0.7971014492753622
Weighted FalsePositiveRate: 0.2253448263898127
Kappa statistic: 0.5816251347138692
Training time: 3.4
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 71.69565217391305
Incorrectly Classified Instances: 18.49275362318841
Correctly Classified Instances: 81.50724637681161
Weighted Precision: 0.8212067683562037
Weighted AreaUnderROC: 0.806271260492697
Root mean squared error: 0.40632343606990284
Relative absolute error: 44.48809756461823
Root relative squared error: 81.26468721398058
Weighted TruePositiveRate: 0.8150724637681159
Weighted MatthewsCorrelation: 0.6286642899630561
Weighted FMeasure: 0.812021393964694
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7872183551271756
Mean absolute error: 0.22244048782309114
Coverage of cases: 87.97101449275362
Instances selection time: 4.0
Test time: 2.5
Accumulative iteration time: 42.7
Weighted Recall: 0.8150724637681159
Weighted FalsePositiveRate: 0.20390539163226493
Kappa statistic: 0.6195504554633103
Training time: 3.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 70.57971014492753
Incorrectly Classified Instances: 18.405797101449277
Correctly Classified Instances: 81.59420289855073
Weighted Precision: 0.8228663026527121
Weighted AreaUnderROC: 0.7910488510079352
Root mean squared error: 0.40805200351388127
Relative absolute error: 43.25845563297905
Root relative squared error: 81.61040070277625
Weighted TruePositiveRate: 0.8159420289855073
Weighted MatthewsCorrelation: 0.6314286382364935
Weighted FMeasure: 0.8130017770328536
Iteration time: 8.9
Weighted AreaUnderPRC: 0.7761445838259384
Mean absolute error: 0.21629227816489527
Coverage of cases: 87.7391304347826
Instances selection time: 3.5
Test time: 3.1
Accumulative iteration time: 51.6
Weighted Recall: 0.8159420289855073
Weighted FalsePositiveRate: 0.202007311635137
Kappa statistic: 0.6217816980807325
Training time: 5.4
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 73.52173913043478
Incorrectly Classified Instances: 18.144927536231883
Correctly Classified Instances: 81.85507246376812
Weighted Precision: 0.8262504209550251
Weighted AreaUnderROC: 0.8102164263293767
Root mean squared error: 0.39117035046423976
Relative absolute error: 42.94318364570928
Root relative squared error: 78.23407009284794
Weighted TruePositiveRate: 0.8185507246376812
Weighted MatthewsCorrelation: 0.637384735172047
Weighted FMeasure: 0.8153833945913194
Iteration time: 8.8
Weighted AreaUnderPRC: 0.7969334251485692
Mean absolute error: 0.21471591822854644
Coverage of cases: 90.63768115942028
Instances selection time: 3.2
Test time: 3.5
Accumulative iteration time: 60.4
Weighted Recall: 0.8185507246376812
Weighted FalsePositiveRate: 0.1993765485417311
Kappa statistic: 0.6269179043878377
Training time: 5.6
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 76.94202898550725
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8331496549095567
Weighted AreaUnderROC: 0.8091685484449478
Root mean squared error: 0.385581897223463
Relative absolute error: 43.33967820695709
Root relative squared error: 77.11637944469261
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6566191914983946
Weighted FMeasure: 0.8274458634676517
Iteration time: 9.8
Weighted AreaUnderPRC: 0.7922836216045936
Mean absolute error: 0.21669839103478544
Coverage of cases: 91.47826086956522
Instances selection time: 3.3
Test time: 2.8
Accumulative iteration time: 70.2
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.1810769496856403
Kappa statistic: 0.6511872952880344
Training time: 6.5
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 78.71014492753623
Incorrectly Classified Instances: 17.217391304347828
Correctly Classified Instances: 82.78260869565219
Weighted Precision: 0.8322783942248131
Weighted AreaUnderROC: 0.8060514124306183
Root mean squared error: 0.38822463302057003
Relative absolute error: 42.29625636105399
Root relative squared error: 77.64492660411402
Weighted TruePositiveRate: 0.8278260869565217
Weighted MatthewsCorrelation: 0.65403848363329
Weighted FMeasure: 0.8258631673477643
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7917414703809929
Mean absolute error: 0.21148128180526998
Coverage of cases: 91.3913043478261
Instances selection time: 2.6
Test time: 2.6
Accumulative iteration time: 80.2
Weighted Recall: 0.8278260869565217
Weighted FalsePositiveRate: 0.18457001539018303
Kappa statistic: 0.6478886587450539
Training time: 7.4
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 83.78260869565217
Incorrectly Classified Instances: 17.420289855072465
Correctly Classified Instances: 82.57971014492753
Weighted Precision: 0.8323262415741135
Weighted AreaUnderROC: 0.8164218886262791
Root mean squared error: 0.384487320407955
Relative absolute error: 43.793643110269294
Root relative squared error: 76.89746408159101
Weighted TruePositiveRate: 0.8257971014492753
Weighted MatthewsCorrelation: 0.6527959697952062
Weighted FMeasure: 0.8241168965315522
Iteration time: 12.6
Weighted AreaUnderPRC: 0.7944054419758497
Mean absolute error: 0.21896821555134652
Coverage of cases: 93.42028985507247
Instances selection time: 3.8
Test time: 2.4
Accumulative iteration time: 92.8
Weighted Recall: 0.8257971014492753
Weighted FalsePositiveRate: 0.182668979880959
Kappa statistic: 0.6453059989907437
Training time: 8.8
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 81.59420289855073
Incorrectly Classified Instances: 16.724637681159418
Correctly Classified Instances: 83.27536231884059
Weighted Precision: 0.8353465698487504
Weighted AreaUnderROC: 0.8183738393520981
Root mean squared error: 0.3792340858560074
Relative absolute error: 41.206874099728324
Root relative squared error: 75.84681717120147
Weighted TruePositiveRate: 0.8327536231884058
Weighted MatthewsCorrelation: 0.6628136261426569
Weighted FMeasure: 0.8316344637323526
Iteration time: 13.3
Weighted AreaUnderPRC: 0.7996950207551755
Mean absolute error: 0.2060343704986416
Coverage of cases: 92.55072463768116
Instances selection time: 3.6
Test time: 2.6
Accumulative iteration time: 106.1
Weighted Recall: 0.8327536231884058
Weighted FalsePositiveRate: 0.17757805750362007
Kappa statistic: 0.6590477866084401
Training time: 9.7
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 82.15942028985508
Incorrectly Classified Instances: 15.91304347826087
Correctly Classified Instances: 84.08695652173914
Weighted Precision: 0.8431265762387253
Weighted AreaUnderROC: 0.8496289882663361
Root mean squared error: 0.3630748374964804
Relative absolute error: 39.82957561821131
Root relative squared error: 72.61496749929606
Weighted TruePositiveRate: 0.8408695652173913
Weighted MatthewsCorrelation: 0.67948053553382
Weighted FMeasure: 0.8401698760061459
Iteration time: 13.6
Weighted AreaUnderPRC: 0.8243843590867858
Mean absolute error: 0.19914787809105658
Coverage of cases: 94.23188405797102
Instances selection time: 3.3
Test time: 3.7
Accumulative iteration time: 119.7
Weighted Recall: 0.8408695652173913
Weighted FalsePositiveRate: 0.16609423078169733
Kappa statistic: 0.6766183656668007
Training time: 10.3
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 82.6231884057971
Incorrectly Classified Instances: 15.681159420289855
Correctly Classified Instances: 84.31884057971014
Weighted Precision: 0.8496306750976919
Weighted AreaUnderROC: 0.8459380303532116
Root mean squared error: 0.3618872395313759
Relative absolute error: 40.22772888219429
Root relative squared error: 72.37744790627518
Weighted TruePositiveRate: 0.8431884057971015
Weighted MatthewsCorrelation: 0.6889543724952436
Weighted FMeasure: 0.8423638868272508
Iteration time: 13.1
Weighted AreaUnderPRC: 0.8217372800943015
Mean absolute error: 0.2011386444109715
Coverage of cases: 94.28985507246378
Instances selection time: 3.1
Test time: 3.3
Accumulative iteration time: 132.8
Weighted Recall: 0.8431884057971015
Weighted FalsePositiveRate: 0.15986527724376037
Kappa statistic: 0.6825261949855252
Training time: 10.0
		
Time end:Sat Oct 07 11.34.40 EEST 2017