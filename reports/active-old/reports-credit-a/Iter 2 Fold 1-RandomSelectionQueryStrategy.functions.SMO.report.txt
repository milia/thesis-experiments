Sat Oct 07 11.36.29 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.36.29 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.872481771536592
Weighted AreaUnderROC: 0.866421568627451
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7297691588509918
Weighted FMeasure: 0.8582671047106049
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8186983526870105
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12512787723785168
Kappa statistic: 0.718066743383199
Training time: 16.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.872959814392763
Weighted AreaUnderROC: 0.8683619281045752
Root mean squared error: 0.3730019232961255
Relative absolute error: 27.82608695652174
Root relative squared error: 74.6003846592251
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.732891781251839
Weighted FMeasure: 0.8612299582564231
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8207833778136213
Mean absolute error: 0.1391304347826087
Coverage of cases: 86.08695652173913
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.12414570900824097
Kappa statistic: 0.7232805293763788
Training time: 15.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8656232898560479
Weighted AreaUnderROC: 0.861825980392157
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7195243791879756
Weighted FMeasure: 0.8554846054121757
Iteration time: 22.0
Weighted AreaUnderPRC: 0.8127654324726221
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 55.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13142050298380223
Kappa statistic: 0.7113743600040157
Training time: 21.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8692753623188405
Weighted AreaUnderROC: 0.8650939542483661
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7261941434040255
Weighted FMeasure: 0.858358102347872
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8167583840929776
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1277831059960216
Kappa statistic: 0.7173313268121395
Training time: 32.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8692753623188405
Weighted AreaUnderROC: 0.8650939542483661
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7261941434040255
Weighted FMeasure: 0.858358102347872
Iteration time: 37.0
Weighted AreaUnderPRC: 0.8167583840929776
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 125.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1277831059960216
Kappa statistic: 0.7173313268121395
Training time: 37.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8692753623188405
Weighted AreaUnderROC: 0.8650939542483661
Root mean squared error: 0.3768673314407159
Relative absolute error: 28.405797101449277
Root relative squared error: 75.37346628814318
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7261941434040255
Weighted FMeasure: 0.858358102347872
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8167583840929776
Mean absolute error: 0.14202898550724638
Coverage of cases: 85.79710144927536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 175.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1277831059960216
Kappa statistic: 0.7173313268121395
Training time: 49.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.872959814392763
Weighted AreaUnderROC: 0.8683619281045752
Root mean squared error: 0.3730019232961255
Relative absolute error: 27.82608695652174
Root relative squared error: 74.6003846592251
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.732891781251839
Weighted FMeasure: 0.8612299582564231
Iteration time: 37.0
Weighted AreaUnderPRC: 0.8207833778136213
Mean absolute error: 0.1391304347826087
Coverage of cases: 86.08695652173913
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 212.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.12414570900824097
Kappa statistic: 0.7232805293763788
Training time: 36.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8788033957687641
Weighted AreaUnderROC: 0.874234068627451
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7445749743269653
Weighted FMeasure: 0.8670120433290722
Iteration time: 41.0
Weighted AreaUnderPRC: 0.8279421653515326
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 253.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.11819852941176472
Kappa statistic: 0.7348105073190296
Training time: 41.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.858413166298207
Weighted AreaUnderROC: 0.8552900326797387
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7062656352788171
Weighted FMeasure: 0.8497321036451472
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8048735666556308
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 307.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.13869529695936345
Kappa statistic: 0.699437072778448
Training time: 53.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8576021080368906
Weighted AreaUnderROC: 0.8533496732026145
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7028340520777421
Weighted FMeasure: 0.8467954984579025
Iteration time: 51.0
Weighted AreaUnderPRC: 0.8027223717960958
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 358.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.13967746518897414
Kappa statistic: 0.6942563330825182
Training time: 50.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8576021080368906
Weighted AreaUnderROC: 0.8533496732026145
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7028340520777421
Weighted FMeasure: 0.8467954984579025
Iteration time: 91.0
Weighted AreaUnderPRC: 0.8027223717960958
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 449.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.13967746518897414
Kappa statistic: 0.6942563330825182
Training time: 90.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8576021080368906
Weighted AreaUnderROC: 0.8533496732026145
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7028340520777421
Weighted FMeasure: 0.8467954984579025
Iteration time: 117.0
Weighted AreaUnderPRC: 0.8027223717960958
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 566.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.13967746518897414
Kappa statistic: 0.6942563330825182
Training time: 116.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8539631442944476
Weighted AreaUnderROC: 0.8500816993464053
Root mean squared error: 0.39562828403747224
Relative absolute error: 31.30434782608696
Root relative squared error: 79.12565680749445
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6961697916614114
Weighted FMeasure: 0.8439233738451499
Iteration time: 122.0
Weighted AreaUnderPRC: 0.7988082191159163
Mean absolute error: 0.1565217391304348
Coverage of cases: 84.34782608695652
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 688.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.14331486217675474
Kappa statistic: 0.6882843088043369
Training time: 121.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8539631442944476
Weighted AreaUnderROC: 0.8500816993464053
Root mean squared error: 0.39562828403747224
Relative absolute error: 31.30434782608696
Root relative squared error: 79.12565680749445
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6961697916614114
Weighted FMeasure: 0.8439233738451499
Iteration time: 246.0
Weighted AreaUnderPRC: 0.7988082191159163
Mean absolute error: 0.1565217391304348
Coverage of cases: 84.34782608695652
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 934.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.14331486217675474
Kappa statistic: 0.6882843088043369
Training time: 246.0
		
Time end:Sat Oct 07 11.36.30 EEST 2017