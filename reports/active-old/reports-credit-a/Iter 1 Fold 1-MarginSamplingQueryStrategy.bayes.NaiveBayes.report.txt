Sat Oct 07 11.35.36 EEST 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.35.36 EEST 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 57.971014492753625
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7903222416477984
Weighted AreaUnderROC: 0.8763810445675855
Root mean squared error: 0.43940643954428465
Relative absolute error: 44.05840397528366
Root relative squared error: 87.88128790885693
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5658479745864105
Weighted FMeasure: 0.7816215080865363
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8676564559322323
Mean absolute error: 0.2202920198764183
Coverage of cases: 85.79710144927536
Instances selection time: 12.0
Test time: 8.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.23995986075589654
Kappa statistic: 0.5566322809211212
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 60.289855072463766
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8015430830039524
Weighted AreaUnderROC: 0.8810333191247514
Root mean squared error: 0.4206273455197124
Relative absolute error: 42.292724349954106
Root relative squared error: 84.12546910394248
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.5894972120480959
Weighted FMeasure: 0.7937781475958955
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8709602826654179
Mean absolute error: 0.21146362174977054
Coverage of cases: 88.69565217391305
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.22673788718385904
Kappa statistic: 0.581165452653486
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.7986808113010498
Weighted AreaUnderROC: 0.8927397106185467
Root mean squared error: 0.4197142321217921
Relative absolute error: 42.69650950772112
Root relative squared error: 83.94284642435842
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5796649317831007
Weighted FMeasure: 0.7867484917828166
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8829012988719279
Mean absolute error: 0.2134825475386056
Coverage of cases: 88.40579710144928
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.2366679099175902
Kappa statistic: 0.5674433183575384
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 63.47826086956522
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7869869450284965
Weighted AreaUnderROC: 0.9099605119825708
Root mean squared error: 0.41889505934183136
Relative absolute error: 44.26582018296889
Root relative squared error: 83.77901186836627
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5479562638922907
Weighted FMeasure: 0.7665499347066141
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9102046811371655
Mean absolute error: 0.22132910091484442
Coverage of cases: 92.17391304347827
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2611475206024439
Kappa statistic: 0.5281927203871238
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 64.05797101449275
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7882467838854272
Weighted AreaUnderROC: 0.9140454793028322
Root mean squared error: 0.4092540496430179
Relative absolute error: 42.858182050711186
Root relative squared error: 81.85080992860358
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5451734990314037
Weighted FMeasure: 0.7621415898569348
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9137426630457652
Mean absolute error: 0.21429091025355593
Coverage of cases: 92.46376811594203
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2674401463483944
Kappa statistic: 0.5205045653665488
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 63.91304347826087
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7903483499214249
Weighted AreaUnderROC: 0.9114923747276689
Root mean squared error: 0.41790662377101084
Relative absolute error: 44.92265036574891
Root relative squared error: 83.58132475420217
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5393384562146447
Weighted FMeasure: 0.7535177088121786
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9098733698648264
Mean absolute error: 0.22461325182874453
Coverage of cases: 91.8840579710145
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2786977834612106
Kappa statistic: 0.5056519662474129
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 63.6231884057971
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8019441647597254
Weighted AreaUnderROC: 0.919151688453159
Root mean squared error: 0.40782780861180784
Relative absolute error: 42.61477263953996
Root relative squared error: 81.56556172236156
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5599381231478938
Weighted FMeasure: 0.7623092154112697
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9163188134141631
Mean absolute error: 0.2130738631976998
Coverage of cases: 92.46376811594203
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.27044082125603863
Kappa statistic: 0.5236325013276686
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 62.028985507246375
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.8036762106751502
Weighted AreaUnderROC: 0.9123434095860566
Root mean squared error: 0.41715366924637803
Relative absolute error: 43.336523627717156
Root relative squared error: 83.43073384927561
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5687656826082319
Weighted FMeasure: 0.7696871720695424
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9094800178550748
Mean absolute error: 0.21668261813858578
Coverage of cases: 90.43478260869566
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.2618384129013924
Kappa statistic: 0.5371253045228259
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 61.44927536231884
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7925420985246764
Weighted AreaUnderROC: 0.9094839324618736
Root mean squared error: 0.4229928319559223
Relative absolute error: 43.77874203391404
Root relative squared error: 84.59856639118446
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5523347311861844
Weighted FMeasure: 0.7648576813263874
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9073802451963423
Mean absolute error: 0.2188937101695702
Coverage of cases: 89.85507246376811
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2651303637396988
Kappa statistic: 0.5262490757367697
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 59.85507246376812
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7887092423316339
Weighted AreaUnderROC: 0.9026075708061003
Root mean squared error: 0.4310462764333096
Relative absolute error: 44.29032951887264
Root relative squared error: 86.20925528666193
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5492990877808881
Weighted FMeasure: 0.7660060667340748
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9002787451723822
Mean absolute error: 0.2214516475943632
Coverage of cases: 88.98550724637681
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26247513498152886
Kappa statistic: 0.5275466132940061
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7846052712040603
Weighted AreaUnderROC: 0.901722494553377
Root mean squared error: 0.4340368306573756
Relative absolute error: 44.205540839142024
Root relative squared error: 86.80736613147512
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5423227125332433
Weighted FMeasure: 0.7632840922196245
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8993659350525921
Mean absolute error: 0.2210277041957101
Coverage of cases: 87.82608695652173
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.26478491759022454
Kappa statistic: 0.521816937733565
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 58.405797101449274
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7878048943033082
Weighted AreaUnderROC: 0.8996800108932463
Root mean squared error: 0.4298334040988215
Relative absolute error: 43.34371440476839
Root relative squared error: 85.96668081976429
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5523797946329901
Weighted FMeasure: 0.7703116344158664
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8966489912906697
Mean absolute error: 0.21671857202384195
Coverage of cases: 87.53623188405797
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2561825092355783
Kappa statistic: 0.5351950028869876
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 57.971014492753625
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7950850661625708
Weighted AreaUnderROC: 0.8986666133844843
Root mean squared error: 0.42598153787320014
Relative absolute error: 42.317120995693955
Root relative squared error: 85.19630757464003
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5693348372115877
Weighted FMeasure: 0.7799579272399593
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8934988759716879
Mean absolute error: 0.21158560497846976
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.24527031827223642
Kappa statistic: 0.5542168674698794
Training time: 4.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.806849292245355
Weighted AreaUnderROC: 0.9045819716775598
Root mean squared error: 0.41569259299996525
Relative absolute error: 41.540538713151705
Root relative squared error: 83.13851859999305
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5897613225161564
Weighted FMeasure: 0.7882094291367081
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9022272883751198
Mean absolute error: 0.20770269356575852
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.23834097044614946
Kappa statistic: 0.571413574090599
Training time: 0.0
		
Time end:Sat Oct 07 11.35.38 EEST 2017