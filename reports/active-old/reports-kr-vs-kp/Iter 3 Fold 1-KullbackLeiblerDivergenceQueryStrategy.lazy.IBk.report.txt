Sat Oct 07 13.06.30 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.06.30 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 58.479349186483105
Incorrectly Classified Instances: 14.643304130162702
Correctly Classified Instances: 85.3566958698373
Weighted Precision: 0.8535803444144753
Weighted AreaUnderROC: 0.8926560322422691
Root mean squared error: 0.35744506748644705
Relative absolute error: 33.46344425759556
Root relative squared error: 71.4890134972894
Weighted TruePositiveRate: 0.853566958698373
Weighted MatthewsCorrelation: 0.7064552084072547
Weighted FMeasure: 0.8534878246037385
Iteration time: 390.0
Weighted AreaUnderPRC: 0.8614677707579503
Mean absolute error: 0.16731722128797782
Coverage of cases: 90.61326658322903
Instances selection time: 389.0
Test time: 248.0
Accumulative iteration time: 390.0
Weighted Recall: 0.853566958698373
Weighted FalsePositiveRate: 0.14787496778848308
Kappa statistic: 0.70631275368511
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 59.386733416770966
Incorrectly Classified Instances: 15.707133917396746
Correctly Classified Instances: 84.29286608260325
Weighted Precision: 0.865481533546832
Weighted AreaUnderROC: 0.9157564001155097
Root mean squared error: 0.37872129441067837
Relative absolute error: 37.12712073567213
Root relative squared error: 75.74425888213567
Weighted TruePositiveRate: 0.8429286608260326
Weighted MatthewsCorrelation: 0.7091912519115218
Weighted FMeasure: 0.8414597802892335
Iteration time: 507.0
Weighted AreaUnderPRC: 0.8911710406854945
Mean absolute error: 0.18563560367836066
Coverage of cases: 90.11264080100125
Instances selection time: 506.0
Test time: 409.0
Accumulative iteration time: 897.0
Weighted Recall: 0.8429286608260326
Weighted FalsePositiveRate: 0.14707382636899086
Kappa statistic: 0.6885551402246514
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 59.04255319148936
Incorrectly Classified Instances: 18.961201501877348
Correctly Classified Instances: 81.03879849812266
Weighted Precision: 0.8532661092430236
Weighted AreaUnderROC: 0.9028368927894332
Root mean squared error: 0.42557095977051496
Relative absolute error: 44.31525668112346
Root relative squared error: 85.11419195410299
Weighted TruePositiveRate: 0.8103879849812266
Weighted MatthewsCorrelation: 0.6646023289096886
Weighted FMeasure: 0.806205478544012
Iteration time: 501.0
Weighted AreaUnderPRC: 0.8748555694706773
Mean absolute error: 0.2215762834056173
Coverage of cases: 86.60826032540676
Instances selection time: 500.0
Test time: 573.0
Accumulative iteration time: 1398.0
Weighted Recall: 0.8103879849812266
Weighted FalsePositiveRate: 0.17534523384182393
Kappa statistic: 0.6256604368740636
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 59.449311639549435
Incorrectly Classified Instances: 7.822277847309136
Correctly Classified Instances: 92.17772215269086
Weighted Precision: 0.9234385935963747
Weighted AreaUnderROC: 0.958961103368614
Root mean squared error: 0.2837634966024773
Relative absolute error: 24.30001148413151
Root relative squared error: 56.75269932049546
Weighted TruePositiveRate: 0.9217772215269087
Weighted MatthewsCorrelation: 0.8451187848770271
Weighted FMeasure: 0.9218103419105752
Iteration time: 358.0
Weighted AreaUnderPRC: 0.9462514868184012
Mean absolute error: 0.12150005742065755
Coverage of cases: 95.18147684605758
Instances selection time: 357.0
Test time: 718.0
Accumulative iteration time: 1756.0
Weighted Recall: 0.9217772215269087
Weighted FalsePositiveRate: 0.07594184794096065
Kappa statistic: 0.8436572999322189
Training time: 1.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 60.356695869837296
Incorrectly Classified Instances: 6.382978723404255
Correctly Classified Instances: 93.61702127659575
Weighted Precision: 0.9371318652911763
Weighted AreaUnderROC: 0.9726935728903788
Root mean squared error: 0.24129882287671894
Relative absolute error: 20.358737013140612
Root relative squared error: 48.25976457534379
Weighted TruePositiveRate: 0.9361702127659575
Weighted MatthewsCorrelation: 0.8729169714176925
Weighted FMeasure: 0.9360597069143711
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9650321197507443
Mean absolute error: 0.10179368506570306
Coverage of cases: 97.12140175219024
Instances selection time: 91.0
Test time: 975.0
Accumulative iteration time: 1848.0
Weighted Recall: 0.9361702127659575
Weighted FalsePositiveRate: 0.06627241372770115
Kappa statistic: 0.8718133430642239
Training time: 1.0
		
Time end:Sat Oct 07 13.06.35 EEST 2017