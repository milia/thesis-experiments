Sat Oct 07 13.35.13 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 13.35.13 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 85.32540675844805
Incorrectly Classified Instances: 12.3279098873592
Correctly Classified Instances: 87.6720901126408
Weighted Precision: 0.876728457544824
Weighted AreaUnderROC: 0.9579739603362083
Root mean squared error: 0.29751430309229715
Relative absolute error: 42.61690162040412
Root relative squared error: 59.50286061845943
Weighted TruePositiveRate: 0.876720901126408
Weighted MatthewsCorrelation: 0.7529548047011045
Weighted FMeasure: 0.876724335538671
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9596788251014681
Mean absolute error: 0.21308450810202062
Coverage of cases: 99.93742177722153
Instances selection time: 15.0
Test time: 23.0
Accumulative iteration time: 17.0
Weighted Recall: 0.876720901126408
Weighted FalsePositiveRate: 0.12372414235038208
Kappa statistic: 0.7529542126468081
Training time: 2.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 80.78848560700877
Incorrectly Classified Instances: 17.647058823529413
Correctly Classified Instances: 82.3529411764706
Weighted Precision: 0.866878687840564
Weighted AreaUnderROC: 0.9526906867784743
Root mean squared error: 0.3463047362212412
Relative absolute error: 45.90956252348951
Root relative squared error: 69.26094724424824
Weighted TruePositiveRate: 0.8235294117647058
Weighted MatthewsCorrelation: 0.6912336947009439
Weighted FMeasure: 0.8197773764294136
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9550608500342563
Mean absolute error: 0.22954781261744756
Coverage of cases: 99.93742177722153
Instances selection time: 12.0
Test time: 23.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8235294117647058
Weighted FalsePositiveRate: 0.16193202985748495
Kappa statistic: 0.6516553936536157
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 77.50312891113893
Incorrectly Classified Instances: 22.46558197747184
Correctly Classified Instances: 77.53441802252816
Weighted Precision: 0.8417308252876279
Weighted AreaUnderROC: 0.9456164996350681
Root mean squared error: 0.3858103177650908
Relative absolute error: 48.88794386712733
Root relative squared error: 77.16206355301816
Weighted TruePositiveRate: 0.7753441802252816
Weighted MatthewsCorrelation: 0.6175010455884736
Weighted FMeasure: 0.7666928633246936
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9482416379726636
Mean absolute error: 0.24443971933563666
Coverage of cases: 98.68585732165207
Instances selection time: 9.0
Test time: 23.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7753441802252816
Weighted FalsePositiveRate: 0.20596236718033611
Kappa statistic: 0.5584323819229461
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 82.04005006257822
Incorrectly Classified Instances: 12.891113892365457
Correctly Classified Instances: 87.10888610763455
Weighted Precision: 0.8792686016580199
Weighted AreaUnderROC: 0.9517316611861467
Root mean squared error: 0.31268182535297295
Relative absolute error: 42.52106780831399
Root relative squared error: 62.53636507059459
Weighted TruePositiveRate: 0.8710888610763454
Weighted MatthewsCorrelation: 0.750625670436075
Weighted FMeasure: 0.870862559745165
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9539517113154423
Mean absolute error: 0.21260533904156992
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 23.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8710888610763454
Weighted FalsePositiveRate: 0.12310697426020054
Kappa statistic: 0.7432779577531791
Training time: 2.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 82.7909887359199
Incorrectly Classified Instances: 12.891113892365457
Correctly Classified Instances: 87.10888610763455
Weighted Precision: 0.8715302625551626
Weighted AreaUnderROC: 0.9423219092614247
Root mean squared error: 0.3133891911184385
Relative absolute error: 42.800063989063595
Root relative squared error: 62.6778382236877
Weighted TruePositiveRate: 0.8710888610763454
Weighted MatthewsCorrelation: 0.7422558845474008
Weighted FMeasure: 0.8711444457380512
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9449510527514244
Mean absolute error: 0.21400031994531798
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8710888610763454
Weighted FalsePositiveRate: 0.1281924782195165
Kappa statistic: 0.7419740177489469
Training time: 2.0
		
Time end:Sat Oct 07 13.35.14 EEST 2017