Sat Oct 07 13.06.07 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.06.07 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.819774718397998
Correctly Classified Instances: 94.180225281602
Weighted Precision: 0.9428441904880608
Weighted AreaUnderROC: 0.9405643024847137
Root mean squared error: 0.2412420924796914
Relative absolute error: 11.639549436795996
Root relative squared error: 48.248418495938274
Weighted TruePositiveRate: 0.94180225281602
Weighted MatthewsCorrelation: 0.8842922724316015
Weighted FMeasure: 0.9416980306780549
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9159369720286517
Mean absolute error: 0.05819774718397998
Coverage of cases: 94.180225281602
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 100.0
Weighted Recall: 0.94180225281602
Weighted FalsePositiveRate: 0.06067364784659242
Kappa statistic: 0.8831174959614057
Training time: 92.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.819774718397998
Correctly Classified Instances: 94.180225281602
Weighted Precision: 0.9428441904880608
Weighted AreaUnderROC: 0.9405643024847137
Root mean squared error: 0.2412420924796914
Relative absolute error: 11.639549436795996
Root relative squared error: 48.248418495938274
Weighted TruePositiveRate: 0.94180225281602
Weighted MatthewsCorrelation: 0.8842922724316015
Weighted FMeasure: 0.9416980306780549
Iteration time: 104.0
Weighted AreaUnderPRC: 0.9159369720286517
Mean absolute error: 0.05819774718397998
Coverage of cases: 94.180225281602
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 204.0
Weighted Recall: 0.94180225281602
Weighted FalsePositiveRate: 0.06067364784659242
Kappa statistic: 0.8831174959614057
Training time: 97.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.192740926157697
Correctly Classified Instances: 95.8072590738423
Weighted Precision: 0.9583754152256719
Weighted AreaUnderROC: 0.9585138172184765
Root mean squared error: 0.2047618354615356
Relative absolute error: 8.385481852315396
Root relative squared error: 40.95236709230712
Weighted TruePositiveRate: 0.958072590738423
Weighted MatthewsCorrelation: 0.91633113335903
Weighted FMeasure: 0.9580885168896562
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9392893141699922
Mean absolute error: 0.041927409261576974
Coverage of cases: 95.8072590738423
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 377.0
Weighted Recall: 0.958072590738423
Weighted FalsePositiveRate: 0.04104495630146997
Kappa statistic: 0.9160715601537059
Training time: 169.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.067584480600751
Correctly Classified Instances: 95.93241551939924
Weighted Precision: 0.9595196137488251
Weighted AreaUnderROC: 0.9596579281077756
Root mean squared error: 0.20168253470741462
Relative absolute error: 8.1351689612015
Root relative squared error: 40.336506941482924
Weighted TruePositiveRate: 0.9593241551939925
Weighted MatthewsCorrelation: 0.918721279621187
Weighted FMeasure: 0.9593373120387579
Iteration time: 346.0
Weighted AreaUnderPRC: 0.9409546787914316
Mean absolute error: 0.040675844806007506
Coverage of cases: 95.93241551939924
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 723.0
Weighted Recall: 0.9593241551939925
Weighted FalsePositiveRate: 0.040008298978441374
Kappa statistic: 0.918559011198038
Training time: 343.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.693366708385482
Correctly Classified Instances: 95.30663329161452
Weighted Precision: 0.9530649977584382
Weighted AreaUnderROC: 0.9528937059776262
Root mean squared error: 0.21664179440693068
Relative absolute error: 9.386733416770964
Root relative squared error: 43.32835888138614
Weighted TruePositiveRate: 0.9530663329161452
Weighted MatthewsCorrelation: 0.9059431135138842
Weighted FMeasure: 0.9530622994138381
Iteration time: 589.0
Weighted AreaUnderPRC: 0.9317612762867243
Mean absolute error: 0.04693366708385482
Coverage of cases: 95.30663329161452
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1312.0
Weighted Recall: 0.9530663329161452
Weighted FalsePositiveRate: 0.04727892096089264
Kappa statistic: 0.905936700932863
Training time: 588.0
		
Time end:Sat Oct 07 13.06.08 EEST 2017