Sat Oct 07 13.05.51 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.05.51 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 86.95244055068837
Incorrectly Classified Instances: 13.329161451814768
Correctly Classified Instances: 86.67083854818523
Weighted Precision: 0.8679388185714316
Weighted AreaUnderROC: 0.9444989444440084
Root mean squared error: 0.3155616571848952
Relative absolute error: 46.09574415709531
Root relative squared error: 63.11233143697904
Weighted TruePositiveRate: 0.8667083854818524
Weighted MatthewsCorrelation: 0.7337044083729243
Weighted FMeasure: 0.8663604484239876
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9468677521473906
Mean absolute error: 0.23047872078547657
Coverage of cases: 100.0
Instances selection time: 30.0
Test time: 23.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8667083854818524
Weighted FalsePositiveRate: 0.13682869532088984
Kappa statistic: 0.7320720973178401
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 85.04380475594493
Incorrectly Classified Instances: 20.90112640801001
Correctly Classified Instances: 79.09887359198999
Weighted Precision: 0.8420211279439451
Weighted AreaUnderROC: 0.9488530148091757
Root mean squared error: 0.3677228946032487
Relative absolute error: 51.01706513480983
Root relative squared error: 73.54457892064974
Weighted TruePositiveRate: 0.7909887359198998
Weighted MatthewsCorrelation: 0.6260760884136365
Weighted FMeasure: 0.7804535462958262
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9507585928606963
Mean absolute error: 0.2550853256740492
Coverage of cases: 99.24906132665832
Instances selection time: 23.0
Test time: 27.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7909887359198998
Weighted FalsePositiveRate: 0.22760436442697485
Kappa statistic: 0.5735609243193921
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 82.3529411764706
Incorrectly Classified Instances: 26.408010012515646
Correctly Classified Instances: 73.59198998748435
Weighted Precision: 0.8196542279600816
Weighted AreaUnderROC: 0.9484621844123026
Root mean squared error: 0.41210009395597935
Relative absolute error: 55.62225020627189
Root relative squared error: 82.42001879119587
Weighted TruePositiveRate: 0.7359198998748435
Weighted MatthewsCorrelation: 0.5408545249988853
Weighted FMeasure: 0.7129917092128704
Iteration time: 18.0
Weighted AreaUnderPRC: 0.950208817413607
Mean absolute error: 0.2781112510313595
Coverage of cases: 98.56070087609513
Instances selection time: 16.0
Test time: 23.0
Accumulative iteration time: 73.0
Weighted Recall: 0.7359198998748435
Weighted FalsePositiveRate: 0.2885478026538203
Kappa statistic: 0.45808830265460837
Training time: 2.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 83.66708385481853
Incorrectly Classified Instances: 17.271589486858574
Correctly Classified Instances: 82.72841051314143
Weighted Precision: 0.853613817733273
Weighted AreaUnderROC: 0.9529433923764529
Root mean squared error: 0.3400307974656682
Relative absolute error: 46.04673459885195
Root relative squared error: 68.00615949313365
Weighted TruePositiveRate: 0.8272841051314143
Weighted MatthewsCorrelation: 0.6774266293605672
Weighted FMeasure: 0.8226402241825342
Iteration time: 11.0
Weighted AreaUnderPRC: 0.954848109383831
Mean absolute error: 0.23023367299425973
Coverage of cases: 99.93742177722153
Instances selection time: 9.0
Test time: 23.0
Accumulative iteration time: 84.0
Weighted Recall: 0.8272841051314143
Weighted FalsePositiveRate: 0.18607582706108053
Kappa statistic: 0.6494282566637151
Training time: 2.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 84.7622027534418
Incorrectly Classified Instances: 12.202753441802253
Correctly Classified Instances: 87.79724655819774
Weighted Precision: 0.8806755347715257
Weighted AreaUnderROC: 0.9591323251269414
Root mean squared error: 0.29981980412513176
Relative absolute error: 42.27530774635974
Root relative squared error: 59.963960825026355
Weighted TruePositiveRate: 0.8779724655819775
Weighted MatthewsCorrelation: 0.7576437548360555
Weighted FMeasure: 0.877462447354781
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9607426787326542
Mean absolute error: 0.2113765387317987
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 35.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8779724655819775
Weighted FalsePositiveRate: 0.126648900392566
Kappa statistic: 0.7544355429747651
Training time: 2.0
		
Time end:Sat Oct 07 13.05.52 EEST 2017