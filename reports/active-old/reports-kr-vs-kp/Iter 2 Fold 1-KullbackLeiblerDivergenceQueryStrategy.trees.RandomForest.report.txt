Sat Oct 07 13.07.05 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.07.05 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 84.19899874843554
Incorrectly Classified Instances: 7.071339173967459
Correctly Classified Instances: 92.92866082603254
Weighted Precision: 0.9320845204408001
Weighted AreaUnderROC: 0.9712080426303358
Root mean squared error: 0.25811539517105286
Relative absolute error: 34.47642886942
Root relative squared error: 51.623079034210576
Weighted TruePositiveRate: 0.9292866082603254
Weighted MatthewsCorrelation: 0.8608143688646892
Weighted FMeasure: 0.9290267502187131
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9638622668648025
Mean absolute error: 0.1723821443471
Coverage of cases: 99.7496871088861
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9292866082603254
Weighted FalsePositiveRate: 0.07478695749632261
Kappa statistic: 0.8577469737337579
Training time: 10.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 80.16270337922403
Incorrectly Classified Instances: 8.635794743429287
Correctly Classified Instances: 91.36420525657071
Weighted Precision: 0.9195538536670019
Weighted AreaUnderROC: 0.9766270865869833
Root mean squared error: 0.256399459433795
Relative absolute error: 32.3884021693782
Root relative squared error: 51.279891886758996
Weighted TruePositiveRate: 0.9136420525657072
Weighted MatthewsCorrelation: 0.8323500923230018
Weighted FMeasure: 0.9130721943152922
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9713339537867508
Mean absolute error: 0.16194201084689103
Coverage of cases: 99.87484355444306
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9136420525657072
Weighted FalsePositiveRate: 0.09224683513686886
Kappa statistic: 0.8259690613769125
Training time: 18.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 74.31163954943679
Incorrectly Classified Instances: 7.071339173967459
Correctly Classified Instances: 92.92866082603254
Weighted Precision: 0.9367993509862835
Weighted AreaUnderROC: 0.9924392368604862
Root mean squared error: 0.22434599997708576
Relative absolute error: 25.044399210570823
Root relative squared error: 44.869199995417155
Weighted TruePositiveRate: 0.9292866082603254
Weighted MatthewsCorrelation: 0.8653459342816678
Weighted FMeasure: 0.928749491562472
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9900066510184513
Mean absolute error: 0.1252219960528541
Coverage of cases: 99.87484355444306
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 76.0
Weighted Recall: 0.9292866082603254
Weighted FalsePositiveRate: 0.07704718147824083
Kappa statistic: 0.8574234942432172
Training time: 27.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 74.43679599499374
Incorrectly Classified Instances: 8.07259073842303
Correctly Classified Instances: 91.92740926157697
Weighted Precision: 0.9293771018795699
Weighted AreaUnderROC: 0.9841203569270373
Root mean squared error: 0.24166320026717938
Relative absolute error: 26.087543989460066
Root relative squared error: 48.332640053435874
Weighted TruePositiveRate: 0.9192740926157698
Weighted MatthewsCorrelation: 0.8477003991644037
Weighted FMeasure: 0.9184836204423573
Iteration time: 39.0
Weighted AreaUnderPRC: 0.9786774357882898
Mean absolute error: 0.13043771994730033
Coverage of cases: 99.43679599499374
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 115.0
Weighted Recall: 0.9192740926157698
Weighted FalsePositiveRate: 0.0881175328650222
Kappa statistic: 0.8370689450763116
Training time: 36.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 70.27534418022528
Incorrectly Classified Instances: 2.065081351689612
Correctly Classified Instances: 97.93491864831039
Weighted Precision: 0.979950869953498
Weighted AreaUnderROC: 0.9965178424278573
Root mean squared error: 0.15237508375630135
Relative absolute error: 15.486631016042718
Root relative squared error: 30.47501675126027
Weighted TruePositiveRate: 0.9793491864831039
Weighted MatthewsCorrelation: 0.9591830889101816
Weighted FMeasure: 0.9793254048673744
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9953332562704247
Mean absolute error: 0.07743315508021359
Coverage of cases: 99.87484355444306
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 163.0
Weighted Recall: 0.9793491864831039
Weighted FalsePositiveRate: 0.022373491738909436
Kappa statistic: 0.9585464287230313
Training time: 47.0
		
Time end:Sat Oct 07 13.07.05 EEST 2017