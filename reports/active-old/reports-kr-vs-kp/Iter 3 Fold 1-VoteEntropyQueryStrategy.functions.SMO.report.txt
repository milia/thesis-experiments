Sat Oct 07 13.35.22 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 13.35.22 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.569461827284105
Correctly Classified Instances: 94.4305381727159
Weighted Precision: 0.9446660586520389
Weighted AreaUnderROC: 0.9435116828003567
Root mean squared error: 0.2359970725937952
Relative absolute error: 11.13892365456821
Root relative squared error: 47.19941451875904
Weighted TruePositiveRate: 0.9443053817271589
Weighted MatthewsCorrelation: 0.8886811400245398
Weighted FMeasure: 0.9442533987789393
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9193494379529387
Mean absolute error: 0.05569461827284105
Coverage of cases: 94.4305381727159
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9443053817271589
Weighted FalsePositiveRate: 0.0572820161264458
Kappa statistic: 0.8882431427893402
Training time: 61.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.005006257822278
Correctly Classified Instances: 95.99499374217773
Weighted Precision: 0.9611905698820895
Weighted AreaUnderROC: 0.9609715369066004
Root mean squared error: 0.20012511730970398
Relative absolute error: 8.010012515644556
Root relative squared error: 40.0250234619408
Weighted TruePositiveRate: 0.9599499374217773
Weighted MatthewsCorrelation: 0.9210761457879738
Weighted FMeasure: 0.9599687645459146
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9426912637046383
Mean absolute error: 0.04005006257822278
Coverage of cases: 95.99499374217773
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 157.0
Weighted Recall: 0.9599499374217773
Weighted FalsePositiveRate: 0.03800686360857647
Kappa statistic: 0.9199218260629903
Training time: 88.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.569461827284105
Correctly Classified Instances: 94.4305381727159
Weighted Precision: 0.9486789629115789
Weighted AreaUnderROC: 0.9463131065827965
Root mean squared error: 0.2359970725937952
Relative absolute error: 11.13892365456821
Root relative squared error: 47.19941451875904
Weighted TruePositiveRate: 0.9443053817271589
Weighted MatthewsCorrelation: 0.8930039981022182
Weighted FMeasure: 0.9442936258028694
Iteration time: 164.0
Weighted AreaUnderPRC: 0.9226692603316178
Mean absolute error: 0.05569461827284105
Coverage of cases: 94.4305381727159
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 321.0
Weighted Recall: 0.9443053817271589
Weighted FalsePositiveRate: 0.05167916856156577
Kappa statistic: 0.8888666621865969
Training time: 161.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.067584480600751
Correctly Classified Instances: 95.93241551939924
Weighted Precision: 0.9596263834600152
Weighted AreaUnderROC: 0.9597677878639496
Root mean squared error: 0.20168253470741462
Relative absolute error: 8.1351689612015
Root relative squared error: 40.336506941482924
Weighted TruePositiveRate: 0.9593241551939925
Weighted MatthewsCorrelation: 0.9188371698177262
Weighted FMeasure: 0.9593396059377262
Iteration time: 372.0
Weighted AreaUnderPRC: 0.9410626398618877
Mean absolute error: 0.040675844806007506
Coverage of cases: 95.93241551939924
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 693.0
Weighted Recall: 0.9593241551939925
Weighted FalsePositiveRate: 0.039788579466093134
Kappa statistic: 0.918576886716282
Training time: 371.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.632040050062578
Correctly Classified Instances: 94.36795994993741
Weighted Precision: 0.9438086378026008
Weighted AreaUnderROC: 0.9431318819290118
Root mean squared error: 0.23731919539014493
Relative absolute error: 11.264080100125156
Root relative squared error: 47.46383907802898
Weighted TruePositiveRate: 0.9436795994993742
Weighted MatthewsCorrelation: 0.8872222726983813
Weighted FMeasure: 0.9436491633091302
Iteration time: 601.0
Weighted AreaUnderPRC: 0.9185134428901982
Mean absolute error: 0.056320400500625784
Coverage of cases: 94.36795994993741
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 1294.0
Weighted Recall: 0.9436795994993742
Weighted FalsePositiveRate: 0.0574158356413507
Kappa statistic: 0.8870433668019654
Training time: 601.0
		
Time end:Sat Oct 07 13.35.24 EEST 2017