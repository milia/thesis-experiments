Sat Oct 07 13.05.55 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.05.55 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.508135168961202
Correctly Classified Instances: 93.4918648310388
Weighted Precision: 0.9354251406313662
Weighted AreaUnderROC: 0.9339386757284905
Root mean squared error: 0.25511046958055644
Relative absolute error: 13.016270337922403
Root relative squared error: 51.02209391611129
Weighted TruePositiveRate: 0.934918648310388
Weighted MatthewsCorrelation: 0.869969086422972
Weighted FMeasure: 0.9348403418711891
Iteration time: 50.0
Weighted AreaUnderPRC: 0.9063713502447646
Mean absolute error: 0.06508135168961202
Coverage of cases: 93.4918648310388
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 50.0
Weighted Recall: 0.934918648310388
Weighted FalsePositiveRate: 0.06704129685340679
Kappa statistic: 0.8693510475217169
Training time: 42.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 50.0
Incorrectly Classified Instances: 8.32290362953692
Correctly Classified Instances: 91.67709637046308
Weighted Precision: 0.9167909534029155
Weighted AreaUnderROC: 0.9166864174665088
Root mean squared error: 0.28849443026749966
Relative absolute error: 16.64580725907384
Root relative squared error: 57.69888605349993
Weighted TruePositiveRate: 0.9167709637046307
Weighted MatthewsCorrelation: 0.8332374834985219
Weighted FMeasure: 0.9167777230109172
Iteration time: 174.0
Weighted AreaUnderPRC: 0.8821541070121434
Mean absolute error: 0.08322903629536921
Coverage of cases: 91.67709637046308
Instances selection time: 15.0
Test time: 12.0
Accumulative iteration time: 224.0
Weighted Recall: 0.9167709637046307
Weighted FalsePositiveRate: 0.08339812877161347
Kappa statistic: 0.8332315881816598
Training time: 159.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.321652065081351
Correctly Classified Instances: 92.67834793491865
Weighted Precision: 0.9270999298533894
Weighted AreaUnderROC: 0.9259282221925742
Root mean squared error: 0.2705855144881439
Relative absolute error: 14.643304130162704
Root relative squared error: 54.117102897628776
Weighted TruePositiveRate: 0.9267834793491865
Weighted MatthewsCorrelation: 0.8534822892362511
Weighted FMeasure: 0.9267136930071104
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8952851267672449
Mean absolute error: 0.07321652065081352
Coverage of cases: 92.67834793491865
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 402.0
Weighted Recall: 0.9267834793491865
Weighted FalsePositiveRate: 0.07492703496403805
Kappa statistic: 0.8530615131915027
Training time: 174.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.5682102628285355
Correctly Classified Instances: 95.43178973717147
Weighted Precision: 0.9549911520444242
Weighted AreaUnderROC: 0.9550443019596455
Root mean squared error: 0.21373371897827764
Relative absolute error: 9.136420525657071
Root relative squared error: 42.746743795655526
Weighted TruePositiveRate: 0.9543178973717147
Weighted MatthewsCorrelation: 0.909199255041884
Weighted FMeasure: 0.9543402214514078
Iteration time: 397.0
Weighted AreaUnderPRC: 0.934316585627543
Mean absolute error: 0.045682102628285357
Coverage of cases: 95.43178973717147
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 799.0
Weighted Recall: 0.9543178973717147
Weighted FalsePositiveRate: 0.04422929345242348
Kappa statistic: 0.9085997157403679
Training time: 394.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.256570713391739
Correctly Classified Instances: 94.74342928660826
Weighted Precision: 0.9474336482170612
Weighted AreaUnderROC: 0.9472143524222851
Root mean squared error: 0.22927212463340893
Relative absolute error: 10.513141426783479
Root relative squared error: 45.85442492668179
Weighted TruePositiveRate: 0.9474342928660826
Weighted MatthewsCorrelation: 0.8946421733244686
Weighted FMeasure: 0.9474280208669646
Iteration time: 598.0
Weighted AreaUnderPRC: 0.9238582185163067
Mean absolute error: 0.052565707133917394
Coverage of cases: 94.74342928660826
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 1397.0
Weighted Recall: 0.9474342928660826
Weighted FalsePositiveRate: 0.05300558802151225
Kappa statistic: 0.894630911468677
Training time: 598.0
		
Time end:Sat Oct 07 13.05.57 EEST 2017