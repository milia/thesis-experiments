Sat Oct 07 13.07.02 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.07.02 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 74.81226533166458
Incorrectly Classified Instances: 6.195244055068836
Correctly Classified Instances: 93.80475594493116
Weighted Precision: 0.9400111343612148
Weighted AreaUnderROC: 0.9759074249927405
Root mean squared error: 0.23099787810833705
Relative absolute error: 16.76640926859448
Root relative squared error: 46.19957562166741
Weighted TruePositiveRate: 0.9380475594493116
Weighted MatthewsCorrelation: 0.8779872003497665
Weighted FMeasure: 0.938073547239292
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9653648079172089
Mean absolute error: 0.0838320463429724
Coverage of cases: 99.43679599499374
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9380475594493116
Weighted FalsePositiveRate: 0.05932270247911049
Kappa statistic: 0.8761998369176496
Training time: 9.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 74.81226533166458
Incorrectly Classified Instances: 6.195244055068836
Correctly Classified Instances: 93.80475594493116
Weighted Precision: 0.9400111343612148
Weighted AreaUnderROC: 0.9759074249927405
Root mean squared error: 0.23099787810833705
Relative absolute error: 16.76640926859448
Root relative squared error: 46.19957562166741
Weighted TruePositiveRate: 0.9380475594493116
Weighted MatthewsCorrelation: 0.8779872003497665
Weighted FMeasure: 0.938073547239292
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9653648079172089
Mean absolute error: 0.0838320463429724
Coverage of cases: 99.43679599499374
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9380475594493116
Weighted FalsePositiveRate: 0.05932270247911049
Kappa statistic: 0.8761998369176496
Training time: 11.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 50.84480600750938
Incorrectly Classified Instances: 5.569461827284105
Correctly Classified Instances: 94.4305381727159
Weighted Precision: 0.9484656524870383
Weighted AreaUnderROC: 0.9786385289708919
Root mean squared error: 0.2290959565440065
Relative absolute error: 11.640409230024941
Root relative squared error: 45.8191913088013
Weighted TruePositiveRate: 0.9443053817271589
Weighted MatthewsCorrelation: 0.8927797562282329
Weighted FMeasure: 0.9443004744144834
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9686087533915076
Mean absolute error: 0.058202046150124705
Coverage of cases: 95.55694618272841
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9443053817271589
Weighted FalsePositiveRate: 0.051683286468135706
Kappa statistic: 0.8888551980681615
Training time: 8.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 51.15769712140175
Incorrectly Classified Instances: 1.3141426783479349
Correctly Classified Instances: 98.68585732165207
Weighted Precision: 0.9868632247255433
Weighted AreaUnderROC: 0.9965994616272043
Root mean squared error: 0.10048080398864849
Relative absolute error: 3.2400780160121676
Root relative squared error: 20.0961607977297
Weighted TruePositiveRate: 0.9868585732165206
Weighted MatthewsCorrelation: 0.9736661101547796
Weighted FMeasure: 0.986857412759144
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9951413232332147
Mean absolute error: 0.01620039008006084
Coverage of cases: 99.56195244055068
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.9868585732165206
Weighted FalsePositiveRate: 0.013364408204473972
Kappa statistic: 0.9736592166054157
Training time: 12.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 51.689612015018774
Incorrectly Classified Instances: 1.2515644555694618
Correctly Classified Instances: 98.74843554443054
Weighted Precision: 0.9874937736092755
Weighted AreaUnderROC: 0.9973261864213905
Root mean squared error: 0.10026905847209014
Relative absolute error: 3.1274335975524994
Root relative squared error: 20.053811694418027
Weighted TruePositiveRate: 0.9874843554443054
Weighted MatthewsCorrelation: 0.9749243942356285
Weighted FMeasure: 0.987482862111182
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9962652730166902
Mean absolute error: 0.015637167987762497
Coverage of cases: 99.68710888610764
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 65.0
Weighted Recall: 0.9874843554443054
Weighted FalsePositiveRate: 0.01279258564183954
Kappa statistic: 0.9749121217782564
Training time: 16.0
		
Time end:Sat Oct 07 13.07.03 EEST 2017