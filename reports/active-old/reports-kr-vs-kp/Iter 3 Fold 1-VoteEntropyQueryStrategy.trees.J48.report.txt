Sat Oct 07 13.36.13 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 13.36.13 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 60.85732165206508
Incorrectly Classified Instances: 4.5682102628285355
Correctly Classified Instances: 95.43178973717147
Weighted Precision: 0.9553848189336981
Weighted AreaUnderROC: 0.9670640450989993
Root mean squared error: 0.20376097215447264
Relative absolute error: 11.39526410177221
Root relative squared error: 40.75219443089453
Weighted TruePositiveRate: 0.9543178973717147
Weighted MatthewsCorrelation: 0.9096214262653028
Weighted FMeasure: 0.9543397494851437
Iteration time: 5.0
Weighted AreaUnderPRC: 0.95342204812016
Mean absolute error: 0.05697632050886105
Coverage of cases: 97.87234042553192
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9543178973717147
Weighted FalsePositiveRate: 0.04382534900203343
Kappa statistic: 0.9086458116343576
Training time: 3.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 51.9712140175219
Incorrectly Classified Instances: 4.25531914893617
Correctly Classified Instances: 95.74468085106383
Weighted Precision: 0.9593555062779987
Weighted AreaUnderROC: 0.986048595678431
Root mean squared error: 0.18392921229480105
Relative absolute error: 9.481597601278333
Root relative squared error: 36.78584245896021
Weighted TruePositiveRate: 0.9574468085106383
Weighted MatthewsCorrelation: 0.9167573810316895
Weighted FMeasure: 0.9574634750956386
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9795453037687846
Mean absolute error: 0.04740798800639166
Coverage of cases: 97.74718397997496
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9574468085106383
Weighted FalsePositiveRate: 0.03997031849845956
Kappa statistic: 0.9149635482043305
Training time: 6.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 51.56445556946183
Incorrectly Classified Instances: 5.006257822277847
Correctly Classified Instances: 94.99374217772215
Weighted Precision: 0.9533888860121144
Weighted AreaUnderROC: 0.9828791417128079
Root mean squared error: 0.2032600188176166
Relative absolute error: 10.280629973497021
Root relative squared error: 40.65200376352332
Weighted TruePositiveRate: 0.9499374217772215
Weighted MatthewsCorrelation: 0.9033211819606948
Weighted FMeasure: 0.9499400880172898
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9755090550177722
Mean absolute error: 0.05140314986748511
Coverage of cases: 97.4342928660826
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9499374217772215
Weighted FalsePositiveRate: 0.04651984170515353
Kappa statistic: 0.9000556636166339
Training time: 8.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 50.43804755944931
Incorrectly Classified Instances: 2.565707133917397
Correctly Classified Instances: 97.4342928660826
Weighted Precision: 0.9748425585655807
Weighted AreaUnderROC: 0.9812673421472246
Root mean squared error: 0.15847086985879064
Relative absolute error: 6.9626657504887595
Root relative squared error: 31.694173971758126
Weighted TruePositiveRate: 0.9743429286608261
Weighted MatthewsCorrelation: 0.949122932750622
Weighted FMeasure: 0.9743542409522569
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9727941366255012
Mean absolute error: 0.0348133287524438
Coverage of cases: 97.4342928660826
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9743429286608261
Weighted FalsePositiveRate: 0.024382478173048713
Kappa statistic: 0.9486577091799873
Training time: 12.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 50.12515644555695
Incorrectly Classified Instances: 0.688360450563204
Correctly Classified Instances: 99.31163954943679
Weighted Precision: 0.9931824282882122
Weighted AreaUnderROC: 0.998054697603174
Root mean squared error: 0.08257130349955724
Relative absolute error: 2.639843051056224
Root relative squared error: 16.51426069991145
Weighted TruePositiveRate: 0.9931163954943679
Weighted MatthewsCorrelation: 0.9862758885461592
Weighted FMeasure: 0.993117877561927
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9972729195628107
Mean absolute error: 0.01319921525528112
Coverage of cases: 99.31163954943679
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9931163954943679
Weighted FalsePositiveRate: 0.006415703691789081
Kappa statistic: 0.9862131386952913
Training time: 16.0
		
Time end:Sat Oct 07 13.36.13 EEST 2017