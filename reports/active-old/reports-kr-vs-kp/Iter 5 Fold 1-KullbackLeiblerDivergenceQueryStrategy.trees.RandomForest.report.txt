Sat Oct 07 13.07.07 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.07.07 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 83.22903629536921
Incorrectly Classified Instances: 4.881101376720901
Correctly Classified Instances: 95.1188986232791
Weighted Precision: 0.9534417990729879
Weighted AreaUnderROC: 0.9855863999899556
Root mean squared error: 0.23228953201903285
Relative absolute error: 31.430723589672
Root relative squared error: 46.45790640380657
Weighted TruePositiveRate: 0.951188986232791
Weighted MatthewsCorrelation: 0.904287907445396
Weighted FMeasure: 0.9510461633927505
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9817041714999949
Mean absolute error: 0.15715361794836
Coverage of cases: 99.93742177722153
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 18.0
Weighted Recall: 0.951188986232791
Weighted FalsePositiveRate: 0.052184629508746146
Kappa statistic: 0.9018776785180102
Training time: 10.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 77.65957446808511
Incorrectly Classified Instances: 3.942428035043805
Correctly Classified Instances: 96.0575719649562
Weighted Precision: 0.9608210878884849
Weighted AreaUnderROC: 0.992693541501877
Root mean squared error: 0.20966410584364292
Relative absolute error: 25.320400500625656
Root relative squared error: 41.93282116872859
Weighted TruePositiveRate: 0.9605757196495619
Weighted MatthewsCorrelation: 0.9212825166733265
Weighted FMeasure: 0.9605896452204509
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9906519328321135
Mean absolute error: 0.12660200250312828
Coverage of cases: 99.93742177722153
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9605757196495619
Weighted FalsePositiveRate: 0.03864206238689041
Kappa statistic: 0.9210735514785087
Training time: 18.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 77.09637046307886
Incorrectly Classified Instances: 3.504380475594493
Correctly Classified Instances: 96.49561952440551
Weighted Precision: 0.9654219024015026
Weighted AreaUnderROC: 0.9936069469032104
Root mean squared error: 0.2250223668091517
Relative absolute error: 27.945556664679167
Root relative squared error: 45.004473361830335
Weighted TruePositiveRate: 0.9649561952440551
Weighted MatthewsCorrelation: 0.9302903780994944
Weighted FMeasure: 0.9649713583020239
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9914475901174663
Mean absolute error: 0.13972778332339583
Coverage of cases: 99.87484355444306
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9649561952440551
Weighted FalsePositiveRate: 0.03386023431646206
Kappa statistic: 0.9298700973016351
Training time: 27.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 74.84355444305382
Incorrectly Classified Instances: 2.065081351689612
Correctly Classified Instances: 97.93491864831039
Weighted Precision: 0.9793999705448312
Weighted AreaUnderROC: 0.9947455648047007
Root mean squared error: 0.16699206849342746
Relative absolute error: 19.067186517709224
Root relative squared error: 33.398413698685495
Weighted TruePositiveRate: 0.9793491864831039
Weighted MatthewsCorrelation: 0.9586596146409117
Weighted FMeasure: 0.9793434225898503
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9928288777651914
Mean absolute error: 0.09533593258854613
Coverage of cases: 99.81226533166458
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 118.0
Weighted Recall: 0.9793491864831039
Weighted FalsePositiveRate: 0.02122458668650137
Kappa statistic: 0.9585985010857943
Training time: 39.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 70.99499374217773
Incorrectly Classified Instances: 1.9399249061326658
Correctly Classified Instances: 98.06007509386734
Weighted Precision: 0.9807145619904244
Weighted AreaUnderROC: 0.9962686918528005
Root mean squared error: 0.154298893852981
Relative absolute error: 15.879736033678382
Root relative squared error: 30.8597787705962
Weighted TruePositiveRate: 0.9806007509386734
Weighted MatthewsCorrelation: 0.9612270420075252
Weighted FMeasure: 0.980592531805501
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9952951737973481
Mean absolute error: 0.07939868016839191
Coverage of cases: 99.87484355444306
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 169.0
Weighted Recall: 0.9806007509386734
Weighted FalsePositiveRate: 0.02018792936347277
Kappa statistic: 0.9610991313306771
Training time: 50.0
		
Time end:Sat Oct 07 13.07.08 EEST 2017