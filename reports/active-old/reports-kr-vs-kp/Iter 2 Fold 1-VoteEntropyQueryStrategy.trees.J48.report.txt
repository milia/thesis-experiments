Sat Oct 07 13.36.13 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 13.36.13 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 51.470588235294116
Incorrectly Classified Instances: 3.566958698372966
Correctly Classified Instances: 96.43304130162703
Weighted Precision: 0.9664810726216808
Weighted AreaUnderROC: 0.9775578593795372
Root mean squared error: 0.18131885877391235
Relative absolute error: 8.421679059608907
Root relative squared error: 36.26377175478247
Weighted TruePositiveRate: 0.9643304130162703
Weighted MatthewsCorrelation: 0.9307752092976979
Weighted FMeasure: 0.9643444811147994
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9676321445135146
Mean absolute error: 0.04210839529804454
Coverage of cases: 97.24655819774718
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9643304130162703
Weighted FalsePositiveRate: 0.03281990846835438
Kappa statistic: 0.9287291494852216
Training time: 3.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 53.035043804755944
Incorrectly Classified Instances: 6.88360450563204
Correctly Classified Instances: 93.11639549436796
Weighted Precision: 0.9348422194425676
Weighted AreaUnderROC: 0.9735773538113813
Root mean squared error: 0.2340485971062601
Relative absolute error: 14.778524702269577
Root relative squared error: 46.80971942125202
Weighted TruePositiveRate: 0.9311639549436797
Weighted MatthewsCorrelation: 0.8654269471525413
Weighted FMeasure: 0.9308581257242643
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9618757868294601
Mean absolute error: 0.07389262351134789
Coverage of cases: 96.62077596996245
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9311639549436797
Weighted FalsePositiveRate: 0.07341052340570704
Kappa statistic: 0.8614528767956822
Training time: 7.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 52.15894868585732
Incorrectly Classified Instances: 5.882352941176471
Correctly Classified Instances: 94.11764705882354
Weighted Precision: 0.9466106960032317
Weighted AreaUnderROC: 0.9777681857778545
Root mean squared error: 0.22941384345618926
Relative absolute error: 12.968757685144375
Root relative squared error: 45.88276869123785
Weighted TruePositiveRate: 0.9411764705882353
Weighted MatthewsCorrelation: 0.887239005241318
Weighted FMeasure: 0.9408371255567307
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9673682587724948
Mean absolute error: 0.06484378842572187
Coverage of cases: 96.68335419274092
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9411764705882353
Weighted FalsePositiveRate: 0.06414835120446025
Kappa statistic: 0.8815110473050255
Training time: 11.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 51.22027534418022
Incorrectly Classified Instances: 2.3153942428035044
Correctly Classified Instances: 97.6846057571965
Weighted Precision: 0.9775281857878323
Weighted AreaUnderROC: 0.9887585248899318
Root mean squared error: 0.14987449187985988
Relative absolute error: 6.458785097823921
Root relative squared error: 29.974898375971975
Weighted TruePositiveRate: 0.976846057571965
Weighted MatthewsCorrelation: 0.9542404445775465
Weighted FMeasure: 0.9768169853409253
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9837164166104493
Mean absolute error: 0.03229392548911961
Coverage of cases: 97.74718397997496
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 37.0
Weighted Recall: 0.976846057571965
Weighted FalsePositiveRate: 0.02499981558673489
Kappa statistic: 0.953516491637434
Training time: 13.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 51.22027534418022
Incorrectly Classified Instances: 0.7509386733416771
Correctly Classified Instances: 99.24906132665832
Weighted Precision: 0.9925012892833022
Weighted AreaUnderROC: 0.9932381632540946
Root mean squared error: 0.08701083897892531
Relative absolute error: 2.562911361478969
Root relative squared error: 17.40216779578506
Weighted TruePositiveRate: 0.9924906132665833
Weighted MatthewsCorrelation: 0.9849596718495237
Weighted FMeasure: 0.9924897172667093
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9906709883200686
Mean absolute error: 0.012814556807394845
Coverage of cases: 99.31163954943679
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9924906132665833
Weighted FalsePositiveRate: 0.007765960344380452
Kappa statistic: 0.984947273066954
Training time: 15.0
		
Time end:Sat Oct 07 13.36.13 EEST 2017