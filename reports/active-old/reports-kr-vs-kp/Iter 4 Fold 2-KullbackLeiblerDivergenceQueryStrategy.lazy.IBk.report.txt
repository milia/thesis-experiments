Sat Oct 07 13.06.45 EEST 2017
Dataset: kr-vs-kp
Test set size: 1598
Initial Labelled set size: 319
Initial Unlabelled set size: 1279
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 13.06.45 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1279
	
Mean region size: 58.41677096370463
Incorrectly Classified Instances: 17.334167709637047
Correctly Classified Instances: 82.66583229036296
Weighted Precision: 0.8268140490090664
Weighted AreaUnderROC: 0.8727368890228132
Root mean squared error: 0.38537854665215726
Relative absolute error: 37.60271295333081
Root relative squared error: 77.07570933043145
Weighted TruePositiveRate: 0.8266583229036295
Weighted MatthewsCorrelation: 0.6525289131966127
Weighted FMeasure: 0.8264626851302078
Iteration time: 395.0
Weighted AreaUnderPRC: 0.8392181120777508
Mean absolute error: 0.18801356476665404
Coverage of cases: 88.67334167709637
Instances selection time: 393.0
Test time: 244.0
Accumulative iteration time: 395.0
Weighted Recall: 0.8266583229036295
Weighted FalsePositiveRate: 0.17560115816421687
Kappa statistic: 0.6520959316902482
Training time: 2.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 979
	
Mean region size: 58.79224030037547
Incorrectly Classified Instances: 18.773466833541928
Correctly Classified Instances: 81.22653316645807
Weighted Precision: 0.8392140890610574
Weighted AreaUnderROC: 0.9114185091717202
Root mean squared error: 0.36541422577617133
Relative absolute error: 34.72269929851296
Root relative squared error: 73.08284515523427
Weighted TruePositiveRate: 0.8122653316645807
Weighted MatthewsCorrelation: 0.647713201026448
Weighted FMeasure: 0.8069236155330616
Iteration time: 522.0
Weighted AreaUnderPRC: 0.888580204319306
Mean absolute error: 0.17361349649256477
Coverage of cases: 90.9261576971214
Instances selection time: 521.0
Test time: 412.0
Accumulative iteration time: 917.0
Weighted Recall: 0.8122653316645807
Weighted FalsePositiveRate: 0.20120025702272354
Kappa statistic: 0.6189528436260046
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 679
	
Mean region size: 58.88610763454318
Incorrectly Classified Instances: 22.715894868585732
Correctly Classified Instances: 77.28410513141426
Weighted Precision: 0.8260464020048384
Weighted AreaUnderROC: 0.910731100983088
Root mean squared error: 0.4013300296256343
Relative absolute error: 40.11706426719594
Root relative squared error: 80.26600592512686
Weighted TruePositiveRate: 0.7728410513141427
Weighted MatthewsCorrelation: 0.591137919282555
Weighted FMeasure: 0.7603872654005177
Iteration time: 516.0
Weighted AreaUnderPRC: 0.8871006580623493
Mean absolute error: 0.20058532133597973
Coverage of cases: 88.67334167709637
Instances selection time: 515.0
Test time: 577.0
Accumulative iteration time: 1433.0
Weighted Recall: 0.7728410513141427
Weighted FalsePositiveRate: 0.24610432551153866
Kappa statistic: 0.5364310704998394
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 379
	
Mean region size: 59.60575719649562
Incorrectly Classified Instances: 12.390488110137673
Correctly Classified Instances: 87.60951188986233
Weighted Precision: 0.8905733948682527
Weighted AreaUnderROC: 0.9542410574158474
Root mean squared error: 0.29525939766352327
Relative absolute error: 25.8243810211394
Root relative squared error: 59.05187953270465
Weighted TruePositiveRate: 0.8760951188986232
Weighted MatthewsCorrelation: 0.7649741580590691
Weighted FMeasure: 0.8743108553266995
Iteration time: 371.0
Weighted AreaUnderPRC: 0.9419360648766818
Mean absolute error: 0.129121905105697
Coverage of cases: 94.80600750938673
Instances selection time: 369.0
Test time: 726.0
Accumulative iteration time: 1804.0
Weighted Recall: 0.8760951188986232
Weighted FalsePositiveRate: 0.13306022744006238
Kappa statistic: 0.7495400904937258
Training time: 2.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 79
	
Mean region size: 60.88861076345432
Incorrectly Classified Instances: 8.010012515644556
Correctly Classified Instances: 91.98998748435544
Weighted Precision: 0.9214686396395575
Weighted AreaUnderROC: 0.9678424799427473
Root mean squared error: 0.253962724725231
Relative absolute error: 22.13609135955742
Root relative squared error: 50.7925449450462
Weighted TruePositiveRate: 0.9198998748435544
Weighted MatthewsCorrelation: 0.8408317171531821
Weighted FMeasure: 0.9196997128745338
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9591024187671465
Mean absolute error: 0.1106804567977871
Coverage of cases: 96.87108886107634
Instances selection time: 88.0
Test time: 870.0
Accumulative iteration time: 1894.0
Weighted Recall: 0.9198998748435544
Weighted FalsePositiveRate: 0.08326447112464476
Kappa statistic: 0.8390319757742115
Training time: 2.0
		
Time end:Sat Oct 07 13.06.50 EEST 2017