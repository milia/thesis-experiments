Sun Oct 08 06.07.28 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.28 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 9.137212532798292
Incorrectly Classified Instances: 27.565982404692082
Correctly Classified Instances: 72.43401759530792
Weighted Precision: 0.7510102488808061
Weighted AreaUnderROC: 0.9750641574871334
Root mean squared error: 0.14709425688422942
Relative absolute error: 30.242288233153875
Root relative squared error: 65.87385279374163
Weighted TruePositiveRate: 0.7243401759530792
Weighted MatthewsCorrelation: 0.6954457216894075
Weighted FMeasure: 0.7017963541993095
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8623254267385737
Mean absolute error: 0.03015851458153876
Coverage of cases: 87.3900293255132
Instances selection time: 103.0
Test time: 61.0
Accumulative iteration time: 108.0
Weighted Recall: 0.7243401759530792
Weighted FalsePositiveRate: 0.03225792477299648
Kappa statistic: 0.6950055662863831
Training time: 5.0
		
Time end:Sun Oct 08 06.07.28 EEST 2017