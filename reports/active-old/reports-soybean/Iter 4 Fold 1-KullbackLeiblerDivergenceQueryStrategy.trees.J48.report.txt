Sun Oct 08 06.07.43 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.43 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 8.818097876269642
Incorrectly Classified Instances: 37.134502923976605
Correctly Classified Instances: 62.865497076023395
Weighted Precision: 0.6406455655386456
Weighted AreaUnderROC: 0.8571421026320346
Root mean squared error: 0.18195308031443735
Relative absolute error: 42.26304596675031
Root relative squared error: 81.48482940047512
Weighted TruePositiveRate: 0.6286549707602339
Weighted MatthewsCorrelation: 0.5902770410057474
Weighted FMeasure: 0.5949877434882256
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5562316794361397
Mean absolute error: 0.0421459738172583
Coverage of cases: 75.43859649122807
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6286549707602339
Weighted FalsePositiveRate: 0.02853463959355033
Kappa statistic: 0.5932270058158592
Training time: 7.0
		
Time end:Sun Oct 08 06.07.43 EEST 2017