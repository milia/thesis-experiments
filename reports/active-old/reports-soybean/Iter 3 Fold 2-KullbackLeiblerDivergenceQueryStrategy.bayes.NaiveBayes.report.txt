Sun Oct 08 06.07.28 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.28 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 8.674178113906482
Incorrectly Classified Instances: 24.633431085043988
Correctly Classified Instances: 75.366568914956
Weighted Precision: 0.7131910699614049
Weighted AreaUnderROC: 0.945159475352551
Root mean squared error: 0.1462912849235301
Relative absolute error: 28.14407033714225
Root relative squared error: 65.51425441201665
Weighted TruePositiveRate: 0.7536656891495601
Weighted MatthewsCorrelation: 0.6984415910925187
Weighted FMeasure: 0.7072379488788256
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7826794547940917
Mean absolute error: 0.028066108923466202
Coverage of cases: 84.1642228739003
Instances selection time: 53.0
Test time: 38.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7536656891495601
Weighted FalsePositiveRate: 0.02917476108379816
Kappa statistic: 0.7269841875005957
Training time: 2.0
		
Time end:Sun Oct 08 06.07.28 EEST 2017