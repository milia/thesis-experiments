Sun Oct 08 06.18.33 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.33 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 30.483099243710438
Incorrectly Classified Instances: 37.24340175953079
Correctly Classified Instances: 62.75659824046921
Weighted Precision: 0.6320866556873908
Weighted AreaUnderROC: 0.9012533924883102
Root mean squared error: 0.17199886868278286
Relative absolute error: 64.64279026447927
Root relative squared error: 77.02699205460615
Weighted TruePositiveRate: 0.6275659824046921
Weighted MatthewsCorrelation: 0.5902156790790034
Weighted FMeasure: 0.6095515040027235
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6803078522501017
Mean absolute error: 0.0644637243634702
Coverage of cases: 88.26979472140762
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6275659824046921
Weighted FalsePositiveRate: 0.03562305245863731
Kappa statistic: 0.5888211613687289
Training time: 6.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 29.618768328445693
Incorrectly Classified Instances: 32.25806451612903
Correctly Classified Instances: 67.74193548387096
Weighted Precision: 0.67327939213742
Weighted AreaUnderROC: 0.909874764046093
Root mean squared error: 0.16459186394153646
Relative absolute error: 60.03634867514776
Root relative squared error: 73.70988131008923
Weighted TruePositiveRate: 0.6774193548387096
Weighted MatthewsCorrelation: 0.6376696636196288
Weighted FMeasure: 0.6574613267423521
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7043427895163216
Mean absolute error: 0.05987004300014788
Coverage of cases: 91.20234604105572
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 16.0
Weighted Recall: 0.6774193548387096
Weighted FalsePositiveRate: 0.0344105277027842
Kappa statistic: 0.6433868268938241
Training time: 8.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 26.60904460564899
Incorrectly Classified Instances: 29.912023460410555
Correctly Classified Instances: 70.08797653958945
Weighted Precision: 0.6610196167095057
Weighted AreaUnderROC: 0.9066240307406916
Root mean squared error: 0.1610244350682523
Relative absolute error: 57.16090311727426
Root relative squared error: 72.11226431654595
Weighted TruePositiveRate: 0.7008797653958945
Weighted MatthewsCorrelation: 0.652053050436586
Weighted FMeasure: 0.6715065078745321
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7233095826460809
Mean absolute error: 0.05700256266542635
Coverage of cases: 88.56304985337243
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7008797653958945
Weighted FalsePositiveRate: 0.029548203554879138
Kappa statistic: 0.6697806892623185
Training time: 9.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 22.858465812625422
Incorrectly Classified Instances: 21.700879765395893
Correctly Classified Instances: 78.2991202346041
Weighted Precision: 0.7596886605076884
Weighted AreaUnderROC: 0.9479564654506121
Root mean squared error: 0.14562501861664073
Relative absolute error: 48.951146457477094
Root relative squared error: 65.21587751035418
Weighted TruePositiveRate: 0.782991202346041
Weighted MatthewsCorrelation: 0.7473942787619604
Weighted FMeasure: 0.7562819773720791
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8157497829583219
Mean absolute error: 0.048815547713828004
Coverage of cases: 92.37536656891496
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 36.0
Weighted Recall: 0.782991202346041
Weighted FalsePositiveRate: 0.021736974723374432
Kappa statistic: 0.7604041056219675
Training time: 10.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 20.373514431239393
Incorrectly Classified Instances: 16.422287390029325
Correctly Classified Instances: 83.57771260997067
Weighted Precision: 0.7992736725010807
Weighted AreaUnderROC: 0.9580364574684019
Root mean squared error: 0.12988127957705292
Relative absolute error: 41.036507378318404
Root relative squared error: 58.165291240809054
Weighted TruePositiveRate: 0.8357771260997068
Weighted MatthewsCorrelation: 0.7993668360598811
Weighted FMeasure: 0.8078584555216528
Iteration time: 13.0
Weighted AreaUnderPRC: 0.864347236684191
Mean absolute error: 0.04092283284264475
Coverage of cases: 93.25513196480938
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8357771260997068
Weighted FalsePositiveRate: 0.018040521951735593
Kappa statistic: 0.8187667865650538
Training time: 12.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 20.697638524463677
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8282727670521212
Weighted AreaUnderROC: 0.9697899304107137
Root mean squared error: 0.12323889338945086
Relative absolute error: 37.91209356580974
Root relative squared error: 55.19060290701733
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.819549117087901
Weighted FMeasure: 0.829668899015056
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8782245153177113
Mean absolute error: 0.037807073916043286
Coverage of cases: 95.60117302052785
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 62.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.01616910655460114
Kappa statistic: 0.8290128479252209
Training time: 12.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 20.527859237536678
Incorrectly Classified Instances: 19.64809384164223
Correctly Classified Instances: 80.35190615835778
Weighted Precision: 0.8332215930426448
Weighted AreaUnderROC: 0.9715579766591113
Root mean squared error: 0.1315124572316556
Relative absolute error: 41.67641540298565
Root relative squared error: 58.89578853537242
Weighted TruePositiveRate: 0.8035190615835777
Weighted MatthewsCorrelation: 0.7874793257130736
Weighted FMeasure: 0.7934951238180208
Iteration time: 16.0
Weighted AreaUnderPRC: 0.879127847490425
Mean absolute error: 0.04156096826890572
Coverage of cases: 97.0674486803519
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8035190615835777
Weighted FalsePositiveRate: 0.021668326586593373
Kappa statistic: 0.7831715210355986
Training time: 16.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 22.009569377990477
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8928189159445326
Weighted AreaUnderROC: 0.989695434289775
Root mean squared error: 0.11732454378284085
Relative absolute error: 38.41070318733094
Root relative squared error: 52.54195432202746
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8741630449524033
Weighted FMeasure: 0.8836894747333215
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9428082210023144
Mean absolute error: 0.03830430234747717
Coverage of cases: 99.70674486803519
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 98.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.014192516826226702
Kappa statistic: 0.8741994986520362
Training time: 19.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 20.512424756906956
Incorrectly Classified Instances: 16.129032258064516
Correctly Classified Instances: 83.87096774193549
Weighted Precision: 0.8325271178021304
Weighted AreaUnderROC: 0.9857565843051876
Root mean squared error: 0.12068558415353961
Relative absolute error: 37.759906084755265
Root relative squared error: 54.04714346607057
Weighted TruePositiveRate: 0.8387096774193549
Weighted MatthewsCorrelation: 0.8173714915065944
Weighted FMeasure: 0.8278441584749957
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9123424959592094
Mean absolute error: 0.03765530800695849
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 119.0
Weighted Recall: 0.8387096774193549
Weighted FalsePositiveRate: 0.017648301648688424
Kappa statistic: 0.8222055798344821
Training time: 20.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 19.262231825899065
Incorrectly Classified Instances: 14.07624633431085
Correctly Classified Instances: 85.92375366568915
Weighted Precision: 0.8653581526445833
Weighted AreaUnderROC: 0.9863470555235693
Root mean squared error: 0.11574846868646353
Relative absolute error: 35.13322897421755
Root relative squared error: 51.83613384276595
Weighted TruePositiveRate: 0.8592375366568915
Weighted MatthewsCorrelation: 0.8437437390715748
Weighted FMeasure: 0.8543319605225325
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9262436976442259
Mean absolute error: 0.03503590701030036
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8592375366568915
Weighted FalsePositiveRate: 0.017824233595377232
Kappa statistic: 0.8448177784519701
Training time: 18.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 17.533569995369668
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9031876861768869
Weighted AreaUnderROC: 0.9836716564194303
Root mean squared error: 0.10887041033958203
Relative absolute error: 31.06598068165551
Root relative squared error: 48.75590343373076
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8849385889306355
Weighted FMeasure: 0.8931547761768381
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9303832898040679
Mean absolute error: 0.03097992533350715
Coverage of cases: 98.82697947214076
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 160.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.011755711207741661
Kappa statistic: 0.8841832161894428
Training time: 22.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 17.194011421515686
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8894000499202015
Weighted AreaUnderROC: 0.99164002320215
Root mean squared error: 0.10764660568686579
Relative absolute error: 31.03546778617613
Root relative squared error: 48.207841740168
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8718988923294695
Weighted FMeasure: 0.8806307295841879
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9370623057607609
Mean absolute error: 0.03094949696128395
Coverage of cases: 99.41348973607037
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 183.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.011162689933661103
Kappa statistic: 0.8681323863689953
Training time: 23.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 18.83006636826672
Incorrectly Classified Instances: 10.850439882697946
Correctly Classified Instances: 89.14956011730206
Weighted Precision: 0.9006418656389013
Weighted AreaUnderROC: 0.9881731022871636
Root mean squared error: 0.11508227425441256
Relative absolute error: 35.20620286618175
Root relative squared error: 51.53778912912104
Weighted TruePositiveRate: 0.8914956011730205
Weighted MatthewsCorrelation: 0.8810882872712031
Weighted FMeasure: 0.8880515099815102
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9196688632663278
Mean absolute error: 0.0351086787585195
Coverage of cases: 99.70674486803519
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 208.0
Weighted Recall: 0.8914956011730205
Weighted FalsePositiveRate: 0.010769672660229365
Kappa statistic: 0.8811466144165191
Training time: 25.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 15.064053094613378
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9135166532080978
Weighted AreaUnderROC: 0.990446236577087
Root mean squared error: 0.09833945090433004
Relative absolute error: 26.55051459136917
Root relative squared error: 44.03977864198825
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.8993527419513359
Weighted FMeasure: 0.9065908339028281
Iteration time: 26.0
Weighted AreaUnderPRC: 0.953606996491463
Mean absolute error: 0.026476967459537353
Coverage of cases: 98.53372434017595
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 234.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.01132123551706527
Kappa statistic: 0.9000529470718378
Training time: 26.0
		
Time end:Sun Oct 08 06.18.34 EEST 2017