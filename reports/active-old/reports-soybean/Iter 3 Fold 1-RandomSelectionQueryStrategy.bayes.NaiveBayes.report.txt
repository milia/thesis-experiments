Sun Oct 08 06.15.08 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.15.08 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.125884887657769
Incorrectly Classified Instances: 19.005847953216374
Correctly Classified Instances: 80.99415204678363
Weighted Precision: 0.8350580353302676
Weighted AreaUnderROC: 0.9599904642988745
Root mean squared error: 0.12874264524809925
Relative absolute error: 24.044899060862804
Root relative squared error: 57.655371739122266
Weighted TruePositiveRate: 0.8099415204678363
Weighted MatthewsCorrelation: 0.7923536136502292
Weighted FMeasure: 0.7927124176780928
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8669590946617395
Mean absolute error: 0.023978292692273365
Coverage of cases: 86.54970760233918
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8099415204678363
Weighted FalsePositiveRate: 0.023589889755171203
Kappa statistic: 0.7900096351854301
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 8.371806709756875
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8626612891891863
Weighted AreaUnderROC: 0.9695303535269454
Root mean squared error: 0.11505534214955115
Relative absolute error: 19.728362749808674
Root relative squared error: 51.52572800893426
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8302122193846281
Weighted FMeasure: 0.8332224387302443
Iteration time: 1.0
Weighted AreaUnderPRC: 0.896952056333078
Mean absolute error: 0.01967371354551575
Coverage of cases: 90.05847953216374
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.019406455213248665
Kappa statistic: 0.8291692191696904
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 7.77162203755003
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8672186418427019
Weighted AreaUnderROC: 0.9679719514623517
Root mean squared error: 0.1191049903017328
Relative absolute error: 19.307413952214233
Root relative squared error: 53.33929933315815
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8319598979715204
Weighted FMeasure: 0.8346619602665251
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8900281733190786
Mean absolute error: 0.019253930811072534
Coverage of cases: 89.47368421052632
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.019851612171959843
Kappa statistic: 0.8291547277936963
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 7.525392428439535
Incorrectly Classified Instances: 14.619883040935672
Correctly Classified Instances: 85.38011695906432
Weighted Precision: 0.8707866651618694
Weighted AreaUnderROC: 0.9713453238627809
Root mean squared error: 0.11564325987503749
Relative absolute error: 18.078591964946455
Root relative squared error: 51.789017728899324
Weighted TruePositiveRate: 0.8538011695906432
Weighted MatthewsCorrelation: 0.8401220709419072
Weighted FMeasure: 0.8440793880762799
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8993360366781309
Mean absolute error: 0.01802851276282765
Coverage of cases: 89.76608187134502
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8538011695906432
Weighted FalsePositiveRate: 0.01948582062235409
Kappa statistic: 0.838787227423141
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.340720221606662
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.8853273846554921
Weighted AreaUnderROC: 0.9729634687344083
Root mean squared error: 0.11240854655391473
Relative absolute error: 17.00774344704678
Root relative squared error: 50.3404021700111
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8545180289263709
Weighted FMeasure: 0.8580618437079248
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9005593444973248
Mean absolute error: 0.016960630584312728
Coverage of cases: 90.35087719298245
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.017955564269376084
Kappa statistic: 0.8519034529502579
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.063711911357365
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8898812778058764
Weighted AreaUnderROC: 0.9744044371596727
Root mean squared error: 0.10787719076728322
Relative absolute error: 15.553790638346268
Root relative squared error: 48.31110564703695
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8642631908990961
Weighted FMeasure: 0.8676393782307329
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9102486640836027
Mean absolute error: 0.015510705345719408
Coverage of cases: 90.35087719298245
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.016436835612489528
Kappa statistic: 0.8648450290746909
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.094490612496177
Incorrectly Classified Instances: 11.403508771929825
Correctly Classified Instances: 88.59649122807018
Weighted Precision: 0.9245188086345429
Weighted AreaUnderROC: 0.9799528995124354
Root mean squared error: 0.10310640312637821
Relative absolute error: 14.87316450846458
Root relative squared error: 46.174583328463186
Weighted TruePositiveRate: 0.8859649122807017
Weighted MatthewsCorrelation: 0.8801247673100493
Weighted FMeasure: 0.880211324411851
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9239851929138427
Mean absolute error: 0.014831964606779213
Coverage of cases: 92.10526315789474
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8859649122807017
Weighted FalsePositiveRate: 0.0158176656845054
Kappa statistic: 0.8745402725913105
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.171437365343204
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.9233490016244774
Weighted AreaUnderROC: 0.9811837723740542
Root mean squared error: 0.10029374496866415
Relative absolute error: 14.390097362745603
Root relative squared error: 44.91497854602632
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8850549044166007
Weighted FMeasure: 0.8865427890918086
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9282546634465008
Mean absolute error: 0.014350235597197957
Coverage of cases: 92.98245614035088
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.015150671775408978
Kappa statistic: 0.8778192484511173
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.017543859649144
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.9320021949693954
Weighted AreaUnderROC: 0.9847403265766617
Root mean squared error: 0.09930815790317894
Relative absolute error: 13.89724042838991
Root relative squared error: 44.47359885763857
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8949114346258465
Weighted FMeasure: 0.8960851294964146
Iteration time: 1.0
Weighted AreaUnderPRC: 0.933225694186986
Mean absolute error: 0.013858743917508064
Coverage of cases: 92.39766081871345
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.014045482306882247
Kappa statistic: 0.8875433338657096
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.186826715912613
Incorrectly Classified Instances: 9.649122807017545
Correctly Classified Instances: 90.35087719298245
Weighted Precision: 0.9242474588090865
Weighted AreaUnderROC: 0.9857042064026523
Root mean squared error: 0.0959665015090671
Relative absolute error: 13.392389520105938
Root relative squared error: 42.977090523079724
Weighted TruePositiveRate: 0.9035087719298246
Weighted MatthewsCorrelation: 0.8973524137765045
Weighted FMeasure: 0.9008519711912403
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9376645652182951
Mean absolute error: 0.013355291488194405
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9035087719298246
Weighted FalsePositiveRate: 0.012838598649142533
Kappa statistic: 0.8940351338409682
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.063711911357359
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.9271102653438638
Weighted AreaUnderROC: 0.9910556899084336
Root mean squared error: 0.09262262611769143
Relative absolute error: 12.383998206835251
Root relative squared error: 41.479588445446176
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.9009015494496911
Weighted FMeasure: 0.9046239490822565
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9491112297544669
Mean absolute error: 0.0123496935026613
Coverage of cases: 93.5672514619883
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.012384196878951289
Kappa statistic: 0.8972905502426023
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.079101261926769
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9281533384104054
Weighted AreaUnderROC: 0.9912100778994132
Root mean squared error: 0.09231489605206067
Relative absolute error: 12.336124594842147
Root relative squared error: 41.3417763685306
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9033852551167584
Weighted FMeasure: 0.9076314991986328
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9500193313335671
Mean absolute error: 0.012301952504551834
Coverage of cases: 93.5672514619883
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.012307500305742349
Kappa statistic: 0.9005011543442761
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 6.971375807940928
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.9254951778575079
Weighted AreaUnderROC: 0.992693371608628
Root mean squared error: 0.0927879574049701
Relative absolute error: 12.264229050092256
Root relative squared error: 41.55362946588498
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.9004055322874626
Weighted FMeasure: 0.9047431413531147
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9554821492191443
Mean absolute error: 0.012230256116435597
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.0123955718903263
Kappa statistic: 0.8973030797815439
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 6.971375807940929
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9275430194575884
Weighted AreaUnderROC: 0.9931825696291796
Root mean squared error: 0.09251641153744283
Relative absolute error: 12.100085244608223
Root relative squared error: 41.43202191380823
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9031190181457204
Weighted FMeasure: 0.9071399377075331
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9580039128391685
Mean absolute error: 0.012066567002933515
Coverage of cases: 93.5672514619883
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 15.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.011952545131510069
Kappa statistic: 0.9005338261921961
Training time: 1.0
		
Time end:Sun Oct 08 06.15.09 EEST 2017