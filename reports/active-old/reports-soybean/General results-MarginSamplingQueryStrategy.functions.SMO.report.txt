Sun Oct 08 06.11.37 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.11.37 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 75.79101713227317
Incorrectly Classified Instances: 21.90615835777126
Correctly Classified Instances: 78.09384164222874
Weighted Precision: 0.7560736495125059
Weighted AreaUnderROC: 0.9281141094480452
Root mean squared error: 0.2149748926548291
Relative absolute error: 95.20782869954208
Root relative squared error: 96.27312944135012
Weighted TruePositiveRate: 0.7809384164222875
Weighted MatthewsCorrelation: 0.7407958994001025
Weighted FMeasure: 0.7540542494577165
Iteration time: 1110.0
Weighted AreaUnderPRC: 0.7004620888016959
Mean absolute error: 0.09494409510203725
Coverage of cases: 93.70410385690522
Instances selection time: 22.1
Test time: 35.9
Accumulative iteration time: 1110.0
Weighted Recall: 0.7809384164222875
Weighted FalsePositiveRate: 0.026615035166401495
Kappa statistic: 0.757921740802915
Training time: 1087.9
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 76.84519215928349
Incorrectly Classified Instances: 19.036545420246608
Correctly Classified Instances: 80.9634545797534
Weighted Precision: 0.7857090903650142
Weighted AreaUnderROC: 0.9388640539486242
Root mean squared error: 0.21462116965964037
Relative absolute error: 95.09268406817
Root relative squared error: 96.11472015326238
Weighted TruePositiveRate: 0.8096345457975339
Weighted MatthewsCorrelation: 0.7732653378959299
Weighted FMeasure: 0.7860358759869481
Iteration time: 1143.5
Weighted AreaUnderPRC: 0.7374272816102114
Mean absolute error: 0.09482926943086291
Coverage of cases: 94.75810738968633
Instances selection time: 21.1
Test time: 32.9
Accumulative iteration time: 2253.5
Weighted Recall: 0.8096345457975339
Weighted FalsePositiveRate: 0.023910423689116313
Kappa statistic: 0.7897907767482047
Training time: 1122.4
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 77.37458581887104
Incorrectly Classified Instances: 16.25448028673835
Correctly Classified Instances: 83.74551971326166
Weighted Precision: 0.8227467101199023
Weighted AreaUnderROC: 0.948384481653562
Root mean squared error: 0.21434227389610064
Relative absolute error: 94.999686879363
Root relative squared error: 95.9898210656884
Weighted TruePositiveRate: 0.8374551971326165
Weighted MatthewsCorrelation: 0.8085194010265925
Weighted FMeasure: 0.818172903613009
Iteration time: 1184.1
Weighted AreaUnderPRC: 0.7750052100462597
Mean absolute error: 0.09473652985199717
Coverage of cases: 95.57853578227093
Instances selection time: 20.1
Test time: 33.5
Accumulative iteration time: 3437.6
Weighted Recall: 0.8374551971326165
Weighted FalsePositiveRate: 0.02076460377849508
Kappa statistic: 0.8206890220425249
Training time: 1164.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 78.42876084588136
Incorrectly Classified Instances: 15.110956766304813
Correctly Classified Instances: 84.88904323369519
Weighted Precision: 0.8372919401570311
Weighted AreaUnderROC: 0.9548922843818156
Root mean squared error: 0.2140921962330152
Relative absolute error: 94.92294912574955
Root relative squared error: 95.87782771180744
Weighted TruePositiveRate: 0.8488904323369519
Weighted MatthewsCorrelation: 0.8225419243205145
Weighted FMeasure: 0.8328458462081813
Iteration time: 1072.4
Weighted AreaUnderPRC: 0.7888726994070276
Mean absolute error: 0.09466000466833832
Coverage of cases: 96.51609473341222
Instances selection time: 19.3
Test time: 34.7
Accumulative iteration time: 4510.0
Weighted Recall: 0.8488904323369519
Weighted FalsePositiveRate: 0.019220760661313053
Kappa statistic: 0.8335934149815618
Training time: 1053.1
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 78.95353770029806
Incorrectly Classified Instances: 14.231877347327261
Correctly Classified Instances: 85.76812265267274
Weighted Precision: 0.8548944577460752
Weighted AreaUnderROC: 0.9621954489935328
Root mean squared error: 0.21382093497139554
Relative absolute error: 94.82503964138236
Root relative squared error: 95.75634761601657
Weighted TruePositiveRate: 0.8576812265267273
Weighted MatthewsCorrelation: 0.835335594467472
Weighted FMeasure: 0.8446266905104135
Iteration time: 1138.8
Weighted AreaUnderPRC: 0.8049392090066402
Mean absolute error: 0.09456236640137937
Coverage of cases: 97.68654284783318
Instances selection time: 18.0
Test time: 35.5
Accumulative iteration time: 5648.8
Weighted Recall: 0.8576812265267273
Weighted FalsePositiveRate: 0.017988782074756984
Kappa statistic: 0.8434049447804147
Training time: 1120.8
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 78.95970246653788
Incorrectly Classified Instances: 12.710466292809247
Correctly Classified Instances: 87.28953370719077
Weighted Precision: 0.8693742990755456
Weighted AreaUnderROC: 0.9677227364289328
Root mean squared error: 0.2136745200731603
Relative absolute error: 94.77262813112819
Root relative squared error: 95.69077800332431
Weighted TruePositiveRate: 0.8728953370719076
Weighted MatthewsCorrelation: 0.8524871885714272
Weighted FMeasure: 0.8615678292947073
Iteration time: 1161.5
Weighted AreaUnderPRC: 0.8235505998436569
Mean absolute error: 0.09451010007536412
Coverage of cases: 98.09624256143782
Instances selection time: 16.3
Test time: 36.5
Accumulative iteration time: 6810.3
Weighted Recall: 0.8728953370719076
Weighted FalsePositiveRate: 0.0165901950088306
Kappa statistic: 0.8601518349171812
Training time: 1145.2
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 78.9643192717087
Incorrectly Classified Instances: 11.802404349093653
Correctly Classified Instances: 88.19759565090635
Weighted Precision: 0.8790034589206416
Weighted AreaUnderROC: 0.9707020374768913
Root mean squared error: 0.213593584745911
Relative absolute error: 94.7428952163903
Root relative squared error: 95.65453238812512
Weighted TruePositiveRate: 0.8819759565090635
Weighted MatthewsCorrelation: 0.8633266366398822
Weighted FMeasure: 0.8716932685986871
Iteration time: 1183.6
Weighted AreaUnderPRC: 0.8344934809869995
Mean absolute error: 0.09448044952327093
Coverage of cases: 98.38949769340263
Instances selection time: 17.0
Test time: 41.5
Accumulative iteration time: 7993.9
Weighted Recall: 0.8819759565090635
Weighted FalsePositiveRate: 0.015167406210261303
Kappa statistic: 0.8702004382680284
Training time: 1166.6
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 78.96739262881655
Incorrectly Classified Instances: 11.450755432079712
Correctly Classified Instances: 88.54924456792028
Weighted Precision: 0.8835645878384957
Weighted AreaUnderROC: 0.9726914988969249
Root mean squared error: 0.21352246351518017
Relative absolute error: 94.71357129110335
Root relative squared error: 95.62268186191895
Weighted TruePositiveRate: 0.8854924456792028
Weighted MatthewsCorrelation: 0.8678404858829604
Weighted FMeasure: 0.8758853591253818
Iteration time: 1263.1
Weighted AreaUnderPRC: 0.8404551685285476
Mean absolute error: 0.09445120682769392
Coverage of cases: 98.594776285778
Instances selection time: 14.3
Test time: 40.0
Accumulative iteration time: 9257.0
Weighted Recall: 0.8854924456792028
Weighted FalsePositiveRate: 0.014621218360889343
Kappa statistic: 0.8740986792838645
Training time: 1248.8
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 78.96585820676565
Incorrectly Classified Instances: 10.688892318773474
Correctly Classified Instances: 89.31110768122653
Weighted Precision: 0.8905478030259282
Weighted AreaUnderROC: 0.9736889189733986
Root mean squared error: 0.21350616731009708
Relative absolute error: 94.70796438255358
Root relative squared error: 95.61538386240842
Weighted TruePositiveRate: 0.8931110768122652
Weighted MatthewsCorrelation: 0.8760004550365046
Weighted FMeasure: 0.8834395329745586
Iteration time: 1252.5
Weighted AreaUnderPRC: 0.8490368722185376
Mean absolute error: 0.0944456154507468
Coverage of cases: 98.594776285778
Instances selection time: 10.8
Test time: 36.6
Accumulative iteration time: 10509.5
Weighted Recall: 0.8931110768122652
Weighted FalsePositiveRate: 0.013952969955553025
Kappa statistic: 0.8824593126188587
Training time: 1241.7
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 78.97202748601151
Incorrectly Classified Instances: 10.220798820119702
Correctly Classified Instances: 89.77920117988029
Weighted Precision: 0.8974313383486583
Weighted AreaUnderROC: 0.9756462218590732
Root mean squared error: 0.21342456298147666
Relative absolute error: 94.67457223305128
Root relative squared error: 95.57883864545201
Weighted TruePositiveRate: 0.897792011798803
Weighted MatthewsCorrelation: 0.8821387828837807
Weighted FMeasure: 0.8892564530272864
Iteration time: 1224.6
Weighted AreaUnderPRC: 0.8560004662395851
Mean absolute error: 0.09441231580027357
Coverage of cases: 98.8871739465967
Instances selection time: 9.3
Test time: 36.7
Accumulative iteration time: 11734.1
Weighted Recall: 0.897792011798803
Weighted FalsePositiveRate: 0.013446360262940396
Kappa statistic: 0.8876207839323609
Training time: 1215.3
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 78.97663977817629
Incorrectly Classified Instances: 9.57615201248478
Correctly Classified Instances: 90.42384798751523
Weighted Precision: 0.9021902229765992
Weighted AreaUnderROC: 0.9771395228135871
Root mean squared error: 0.2134074942725021
Relative absolute error: 94.66806656770765
Root relative squared error: 95.57119469220609
Weighted TruePositiveRate: 0.9042384798751522
Weighted MatthewsCorrelation: 0.889038729761352
Weighted FMeasure: 0.8961970817752682
Iteration time: 1223.9
Weighted AreaUnderPRC: 0.8622639462726827
Mean absolute error: 0.09440582815616358
Coverage of cases: 98.8871739465967
Instances selection time: 7.2
Test time: 36.4
Accumulative iteration time: 12958.0
Weighted Recall: 0.9042384798751522
Weighted FalsePositiveRate: 0.012608042346621256
Kappa statistic: 0.8946934989762345
Training time: 1216.7
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 78.97663526517026
Incorrectly Classified Instances: 8.902694174341033
Correctly Classified Instances: 91.09730582565898
Weighted Precision: 0.9066760260370874
Weighted AreaUnderROC: 0.978652082766272
Root mean squared error: 0.21335805794102186
Relative absolute error: 94.64818964551672
Root relative squared error: 95.54905540756256
Weighted TruePositiveRate: 0.9109730582565897
Weighted MatthewsCorrelation: 0.8965275733907314
Weighted FMeasure: 0.9045266029584264
Iteration time: 1219.7
Weighted AreaUnderPRC: 0.8720163540816239
Mean absolute error: 0.09438600629469893
Coverage of cases: 98.8871739465967
Instances selection time: 5.4
Test time: 36.1
Accumulative iteration time: 14177.7
Weighted Recall: 0.9109730582565897
Weighted FalsePositiveRate: 0.011722016496040072
Kappa statistic: 0.9021814296387858
Training time: 1214.3
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 78.97047952494249
Incorrectly Classified Instances: 8.052940268559963
Correctly Classified Instances: 91.94705973144005
Weighted Precision: 0.9176156112114082
Weighted AreaUnderROC: 0.980933547091275
Root mean squared error: 0.213242936635872
Relative absolute error: 94.59986532409926
Root relative squared error: 95.4975001390599
Weighted TruePositiveRate: 0.9194705973144004
Weighted MatthewsCorrelation: 0.9071569792163228
Weighted FMeasure: 0.9150244882750215
Iteration time: 1248.7
Weighted AreaUnderPRC: 0.885175813206771
Mean absolute error: 0.0943378158356677
Coverage of cases: 99.18042907856152
Instances selection time: 3.9
Test time: 36.0
Accumulative iteration time: 15426.4
Weighted Recall: 0.9194705973144004
Weighted FalsePositiveRate: 0.010838630104591499
Kappa statistic: 0.9115695208929203
Training time: 1244.8
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 78.97508730410127
Incorrectly Classified Instances: 6.969268234123921
Correctly Classified Instances: 93.03073176587608
Weighted Precision: 0.9330110832023685
Weighted AreaUnderROC: 0.9865019073828609
Root mean squared error: 0.21300222725508128
Relative absolute error: 94.49830633569502
Root relative squared error: 95.38970222327339
Weighted TruePositiveRate: 0.9303073176587608
Weighted MatthewsCorrelation: 0.9217047992654315
Weighted FMeasure: 0.9297304315325962
Iteration time: 1315.4
Weighted AreaUnderPRC: 0.9037297118317424
Mean absolute error: 0.09423653817410113
Coverage of cases: 100.0
Instances selection time: 1.8
Test time: 38.4
Accumulative iteration time: 16741.8
Weighted Recall: 0.9303073176587608
Weighted FalsePositiveRate: 0.009380711218569855
Kappa statistic: 0.9235670035755523
Training time: 1313.6
		
Time end:Sun Oct 08 06.14.35 EEST 2017