Sun Oct 08 06.18.38 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.38 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 25.9453619385707
Incorrectly Classified Instances: 40.76246334310851
Correctly Classified Instances: 59.23753665689149
Weighted Precision: 0.5340863721576532
Weighted AreaUnderROC: 0.8978617238668748
Root mean squared error: 0.17262332371392156
Relative absolute error: 62.57341377315428
Root relative squared error: 77.30664443308017
Weighted TruePositiveRate: 0.592375366568915
Weighted MatthewsCorrelation: 0.523287325160369
Weighted FMeasure: 0.5372497974193495
Iteration time: 9.0
Weighted AreaUnderPRC: 0.662945409540867
Mean absolute error: 0.06240008021699651
Coverage of cases: 84.75073313782991
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.592375366568915
Weighted FalsePositiveRate: 0.037143221201333995
Kappa statistic: 0.5473955597994749
Training time: 8.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 27.45794104028393
Incorrectly Classified Instances: 27.85923753665689
Correctly Classified Instances: 72.14076246334311
Weighted Precision: 0.702059129587706
Weighted AreaUnderROC: 0.9345904190917157
Root mean squared error: 0.15409608929918894
Relative absolute error: 55.614141016453196
Root relative squared error: 69.00951347526308
Weighted TruePositiveRate: 0.7214076246334311
Weighted MatthewsCorrelation: 0.6752987191123326
Weighted FMeasure: 0.6853611975624201
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7979296661517254
Mean absolute error: 0.055460085224164346
Coverage of cases: 91.20234604105572
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7214076246334311
Weighted FalsePositiveRate: 0.028761814108943747
Kappa statistic: 0.6925916436549283
Training time: 21.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 24.509955240006136
Incorrectly Classified Instances: 28.1524926686217
Correctly Classified Instances: 71.8475073313783
Weighted Precision: 0.6742143150192877
Weighted AreaUnderROC: 0.9540219664022213
Root mean squared error: 0.15194531921749707
Relative absolute error: 53.07599786358311
Root relative squared error: 68.04632487255603
Weighted TruePositiveRate: 0.718475073313783
Weighted MatthewsCorrelation: 0.6633114640742421
Weighted FMeasure: 0.6770547375916423
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8054859432100885
Mean absolute error: 0.05292897293875371
Coverage of cases: 94.72140762463343
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 43.0
Weighted Recall: 0.718475073313783
Weighted FalsePositiveRate: 0.032441823731887365
Kappa statistic: 0.6874815034033738
Training time: 12.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 22.85846581262543
Incorrectly Classified Instances: 26.686217008797655
Correctly Classified Instances: 73.31378299120234
Weighted Precision: 0.765727056927703
Weighted AreaUnderROC: 0.9495186978459242
Root mean squared error: 0.14466479381903236
Relative absolute error: 47.36877708205764
Root relative squared error: 64.78585591531436
Weighted TruePositiveRate: 0.7331378299120235
Weighted MatthewsCorrelation: 0.7068874477389956
Weighted FMeasure: 0.7101934565729494
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8209368140000786
Mean absolute error: 0.04723756163307729
Coverage of cases: 92.96187683284458
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7331378299120235
Weighted FalsePositiveRate: 0.029077604132203862
Kappa statistic: 0.7044751102349457
Training time: 15.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 23.13628646396051
Incorrectly Classified Instances: 21.407624633431084
Correctly Classified Instances: 78.59237536656892
Weighted Precision: 0.7555256364406723
Weighted AreaUnderROC: 0.9722182504651575
Root mean squared error: 0.1363776988824367
Relative absolute error: 44.368542088002705
Root relative squared error: 61.07461059884549
Weighted TruePositiveRate: 0.7859237536656891
Weighted MatthewsCorrelation: 0.7450186280808488
Weighted FMeasure: 0.7586370348508981
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8674491773959093
Mean absolute error: 0.04424563753928287
Coverage of cases: 97.94721407624634
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 73.0
Weighted Recall: 0.7859237536656891
Weighted FalsePositiveRate: 0.027355645863233507
Kappa statistic: 0.7626389762953639
Training time: 14.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 22.07130730050937
Incorrectly Classified Instances: 17.59530791788856
Correctly Classified Instances: 82.40469208211144
Weighted Precision: 0.8378906143903584
Weighted AreaUnderROC: 0.977552171078336
Root mean squared error: 0.1312980144958702
Relative absolute error: 43.47528512578473
Root relative squared error: 58.79975372402739
Weighted TruePositiveRate: 0.8240469208211144
Weighted MatthewsCorrelation: 0.8101633896730186
Weighted FMeasure: 0.82038790654851
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8936575131130959
Mean absolute error: 0.04335485497308211
Coverage of cases: 97.65395894428153
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.8240469208211144
Weighted FalsePositiveRate: 0.01785654453113783
Kappa statistic: 0.8063966691900076
Training time: 16.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 21.160672943355493
Incorrectly Classified Instances: 20.821114369501466
Correctly Classified Instances: 79.17888563049853
Weighted Precision: 0.7892910227511608
Weighted AreaUnderROC: 0.9716653644886686
Root mean squared error: 0.1330392058971607
Relative absolute error: 42.04724246958451
Root relative squared error: 59.57951894725165
Weighted TruePositiveRate: 0.7917888563049853
Weighted MatthewsCorrelation: 0.7604265580739015
Weighted FMeasure: 0.7722788798712379
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8694460818512113
Mean absolute error: 0.041930768113713456
Coverage of cases: 97.0674486803519
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7917888563049853
Weighted FalsePositiveRate: 0.02483185586621331
Kappa statistic: 0.7698245947616105
Training time: 17.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 20.466121315017777
Incorrectly Classified Instances: 16.129032258064516
Correctly Classified Instances: 83.87096774193549
Weighted Precision: 0.8560542397243368
Weighted AreaUnderROC: 0.9740943414031367
Root mean squared error: 0.12267884250479782
Relative absolute error: 37.644905599304586
Root relative squared error: 54.9397929140638
Weighted TruePositiveRate: 0.8387096774193549
Weighted MatthewsCorrelation: 0.8231514832638143
Weighted FMeasure: 0.8299339414707936
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9069672780057646
Mean absolute error: 0.03754062608240934
Coverage of cases: 97.36070381231671
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 124.0
Weighted Recall: 0.8387096774193549
Weighted FalsePositiveRate: 0.01784714854390767
Kappa statistic: 0.8224983674203349
Training time: 18.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 20.697638524463663
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8228267594249999
Weighted AreaUnderROC: 0.9766647735105926
Root mean squared error: 0.12662317613212162
Relative absolute error: 40.23699210903301
Root relative squared error: 56.706200782320884
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.7993292160223086
Weighted FMeasure: 0.8097276069910415
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8763533351950656
Mean absolute error: 0.040125532297096984
Coverage of cases: 97.94721407624634
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 144.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.01877476571358181
Kappa statistic: 0.7997309790845711
Training time: 19.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 17.7342182435561
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9072067018131943
Weighted AreaUnderROC: 0.9877783971275829
Root mean squared error: 0.11156123155455804
Relative absolute error: 33.253811260696736
Root relative squared error: 49.96094545484185
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8848792107197986
Weighted FMeasure: 0.8934183422348264
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9518845703047987
Mean absolute error: 0.03316169544003028
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 167.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.01473636418600908
Kappa statistic: 0.8838270086117157
Training time: 22.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 18.351597468745194
Incorrectly Classified Instances: 14.956011730205278
Correctly Classified Instances: 85.04398826979472
Weighted Precision: 0.8590945150188924
Weighted AreaUnderROC: 0.983799569624044
Root mean squared error: 0.11819371887268282
Relative absolute error: 34.81021225109191
Root relative squared error: 52.931200735402385
Weighted TruePositiveRate: 0.8504398826979472
Weighted MatthewsCorrelation: 0.8335803165815017
Weighted FMeasure: 0.8422360471168494
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9115604094348722
Mean absolute error: 0.03471378507034127
Coverage of cases: 98.24046920821114
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 194.0
Weighted Recall: 0.8504398826979472
Weighted FalsePositiveRate: 0.017141898279833756
Kappa statistic: 0.8353982300884956
Training time: 26.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 18.922673252045072
Incorrectly Classified Instances: 14.956011730205278
Correctly Classified Instances: 85.04398826979472
Weighted Precision: 0.8456661852584512
Weighted AreaUnderROC: 0.9825731328086039
Root mean squared error: 0.1207440393211101
Relative absolute error: 37.076739935660825
Root relative squared error: 54.073321694814084
Weighted TruePositiveRate: 0.8504398826979472
Weighted MatthewsCorrelation: 0.8291326038887367
Weighted FMeasure: 0.8376581338411141
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8998532304465855
Mean absolute error: 0.036974034284869846
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 220.0
Weighted Recall: 0.8504398826979472
Weighted FalsePositiveRate: 0.016493373488894115
Kappa statistic: 0.8353000227289946
Training time: 25.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 19.57092143849363
Incorrectly Classified Instances: 16.422287390029325
Correctly Classified Instances: 83.57771260997067
Weighted Precision: 0.8449511100016712
Weighted AreaUnderROC: 0.9861929576331846
Root mean squared error: 0.11993095106910122
Relative absolute error: 37.229599387928744
Root relative squared error: 53.70919289090498
Weighted TruePositiveRate: 0.8357771260997068
Weighted MatthewsCorrelation: 0.8204753530450408
Weighted FMeasure: 0.8312238448436392
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9127710527371256
Mean absolute error: 0.037126470303751984
Coverage of cases: 98.82697947214076
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 247.0
Weighted Recall: 0.8357771260997068
Weighted FalsePositiveRate: 0.01964827712800375
Kappa statistic: 0.8187254967107449
Training time: 27.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 17.271183824664334
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8942482531114214
Weighted AreaUnderROC: 0.9841758820330944
Root mean squared error: 0.11044359716131323
Relative absolute error: 31.760229186841812
Root relative squared error: 49.46043044455307
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8748119112114084
Weighted FMeasure: 0.8816388776029276
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9122919401893139
Mean absolute error: 0.031672250712640314
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 276.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.012462636533868893
Kappa statistic: 0.8743480725623582
Training time: 28.0
		
Time end:Sun Oct 08 06.18.39 EEST 2017