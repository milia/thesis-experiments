Sun Oct 08 06.18.45 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.45 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.7808183937176342
Weighted AreaUnderROC: 0.9362271543499746
Root mean squared error: 0.21491673444950762
Relative absolute error: 95.25599128540198
Root relative squared error: 96.24708420248595
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.7766767089498855
Weighted FMeasure: 0.7888593096576111
Iteration time: 1540.0
Weighted AreaUnderPRC: 0.7294132387224455
Mean absolute error: 0.09499212427353189
Coverage of cases: 94.15204678362574
Instances selection time: 24.0
Test time: 40.0
Accumulative iteration time: 1540.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.022543177689420556
Kappa statistic: 0.7965938485357703
Training time: 1516.0
		
Time end:Sun Oct 08 06.18.46 EEST 2017