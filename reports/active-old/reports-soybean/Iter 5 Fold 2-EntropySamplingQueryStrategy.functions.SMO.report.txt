Sun Oct 08 06.06.36 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.06.36 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 73.6996450069453
Incorrectly Classified Instances: 29.032258064516128
Correctly Classified Instances: 70.96774193548387
Weighted Precision: 0.6658686073017689
Weighted AreaUnderROC: 0.9178193411974104
Root mean squared error: 0.21554839731159026
Relative absolute error: 95.44368415336079
Root relative squared error: 96.52996449484742
Weighted TruePositiveRate: 0.7096774193548387
Weighted MatthewsCorrelation: 0.6543572836809826
Weighted FMeasure: 0.6746466976295318
Iteration time: 818.0
Weighted AreaUnderPRC: 0.6190086607794231
Mean absolute error: 0.09517929721664872
Coverage of cases: 91.78885630498533
Instances selection time: 20.0
Test time: 34.0
Accumulative iteration time: 818.0
Weighted Recall: 0.7096774193548387
Weighted FalsePositiveRate: 0.03408447609934762
Kappa statistic: 0.6786693318103941
Training time: 798.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 19.35483870967742
Correctly Classified Instances: 80.64516129032258
Weighted Precision: 0.8023972495791257
Weighted AreaUnderROC: 0.956809390354241
Root mean squared error: 0.21407802799356565
Relative absolute error: 94.93863364831054
Root relative squared error: 95.87148268828567
Weighted TruePositiveRate: 0.8064516129032258
Weighted MatthewsCorrelation: 0.7786818103251958
Weighted FMeasure: 0.7949615457012801
Iteration time: 1032.0
Weighted AreaUnderPRC: 0.7494436680615952
Mean absolute error: 0.09467564574346841
Coverage of cases: 97.0674486803519
Instances selection time: 22.0
Test time: 35.0
Accumulative iteration time: 1850.0
Weighted Recall: 0.8064516129032258
Weighted FalsePositiveRate: 0.02562833813641702
Kappa statistic: 0.7863530215868314
Training time: 1010.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 79.0091063435711
Incorrectly Classified Instances: 17.59530791788856
Correctly Classified Instances: 82.40469208211144
Weighted Precision: 0.8454118278990786
Weighted AreaUnderROC: 0.9720145478311393
Root mean squared error: 0.21333688381231442
Relative absolute error: 94.62365591397798
Root relative squared error: 95.53957290656585
Weighted TruePositiveRate: 0.8240469208211144
Weighted MatthewsCorrelation: 0.8096287166813219
Weighted FMeasure: 0.8256214249565027
Iteration time: 1147.0
Weighted AreaUnderPRC: 0.7929828890376313
Mean absolute error: 0.09436154052363539
Coverage of cases: 100.0
Instances selection time: 23.0
Test time: 38.0
Accumulative iteration time: 2997.0
Weighted Recall: 0.8240469208211144
Weighted FalsePositiveRate: 0.023322314405457862
Kappa statistic: 0.8062536694380789
Training time: 1124.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 16.129032258064516
Correctly Classified Instances: 83.87096774193549
Weighted Precision: 0.8570092971281509
Weighted AreaUnderROC: 0.9736869054755807
Root mean squared error: 0.2132899679242299
Relative absolute error: 94.60917417906617
Root relative squared error: 95.5185623629129
Weighted TruePositiveRate: 0.8387096774193549
Weighted MatthewsCorrelation: 0.8247447283161804
Weighted FMeasure: 0.8398525421865655
Iteration time: 1157.0
Weighted AreaUnderPRC: 0.8043663672037837
Mean absolute error: 0.09434709890433275
Coverage of cases: 100.0
Instances selection time: 22.0
Test time: 37.0
Accumulative iteration time: 4154.0
Weighted Recall: 0.8387096774193549
Weighted FalsePositiveRate: 0.021659635203776312
Kappa statistic: 0.8224395508681575
Training time: 1135.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 79.03997530483056
Incorrectly Classified Instances: 16.422287390029325
Correctly Classified Instances: 83.57771260997067
Weighted Precision: 0.8511661297272137
Weighted AreaUnderROC: 0.9739699983830962
Root mean squared error: 0.2132889285313943
Relative absolute error: 94.61098439593019
Root relative squared error: 95.5180968871554
Weighted TruePositiveRate: 0.8357771260997068
Weighted MatthewsCorrelation: 0.8202004582300496
Weighted FMeasure: 0.8363091409668578
Iteration time: 1162.0
Weighted AreaUnderPRC: 0.8008721609458372
Mean absolute error: 0.09434890410674562
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 38.0
Accumulative iteration time: 5316.0
Weighted Recall: 0.8357771260997068
Weighted FalsePositiveRate: 0.022049074710996847
Kappa statistic: 0.8192248707802413
Training time: 1143.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 13.489736070381232
Correctly Classified Instances: 86.51026392961877
Weighted Precision: 0.8740590504330781
Weighted AreaUnderROC: 0.9769669143359422
Root mean squared error: 0.21322351083362198
Relative absolute error: 94.58383114297045
Root relative squared error: 95.48880059860926
Weighted TruePositiveRate: 0.8651026392961877
Weighted MatthewsCorrelation: 0.8500466604281152
Weighted FMeasure: 0.8626290452995627
Iteration time: 1198.0
Weighted AreaUnderPRC: 0.8268537687549891
Mean absolute error: 0.09432182607055309
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 38.0
Accumulative iteration time: 6514.0
Weighted Recall: 0.8651026392961877
Weighted FalsePositiveRate: 0.01823655202736602
Kappa statistic: 0.851576397562545
Training time: 1181.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 79.0091063435711
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9049236844535404
Weighted AreaUnderROC: 0.9773619009647162
Root mean squared error: 0.21314462648855362
Relative absolute error: 94.55486767314673
Root relative squared error: 95.45347348356863
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8833869873052652
Weighted FMeasure: 0.8923893578496092
Iteration time: 1221.0
Weighted AreaUnderPRC: 0.8534005612579668
Mean absolute error: 0.09429294283194771
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 37.0
Accumulative iteration time: 7735.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.013302297996963117
Kappa statistic: 0.8841843088418431
Training time: 1206.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9026637408170959
Weighted AreaUnderROC: 0.9816637195260975
Root mean squared error: 0.2131279347215804
Relative absolute error: 94.54762680569087
Root relative squared error: 95.44599833787802
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8828590895589057
Weighted FMeasure: 0.8925609131762274
Iteration time: 1232.0
Weighted AreaUnderPRC: 0.8571440162769155
Mean absolute error: 0.09428572202229643
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 36.0
Accumulative iteration time: 8967.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.012945067654838516
Kappa statistic: 0.88422035480859
Training time: 1219.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9035673108253752
Weighted AreaUnderROC: 0.9819252791743316
Root mean squared error: 0.21312637444510024
Relative absolute error: 94.54762680569084
Root relative squared error: 95.44529959256091
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8833205685296381
Weighted FMeasure: 0.8923534509417421
Iteration time: 1246.0
Weighted AreaUnderPRC: 0.8604620215077636
Mean absolute error: 0.0942857220222964
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 10213.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.012933615524742316
Kappa statistic: 0.8841843088418431
Training time: 1234.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 79.03997530483056
Incorrectly Classified Instances: 7.624633431085044
Correctly Classified Instances: 92.37536656891496
Weighted Precision: 0.9381314103747889
Weighted AreaUnderROC: 0.9852903570634562
Root mean squared error: 0.2130923674867642
Relative absolute error: 94.534955287643
Root relative squared error: 95.43007011035785
Weighted TruePositiveRate: 0.9237536656891495
Weighted MatthewsCorrelation: 0.9185909873164073
Weighted FMeasure: 0.9217786384859211
Iteration time: 1265.0
Weighted AreaUnderPRC: 0.8948978023128804
Mean absolute error: 0.09427308560540659
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 38.0
Accumulative iteration time: 11478.0
Weighted Recall: 0.9237536656891495
Weighted FalsePositiveRate: 0.008729506108698295
Kappa statistic: 0.9163592796294374
Training time: 1256.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 5.571847507331379
Correctly Classified Instances: 94.42815249266862
Weighted Precision: 0.9518100082871959
Weighted AreaUnderROC: 0.9870223192752917
Root mean squared error: 0.21301744881958146
Relative absolute error: 94.50418160095532
Root relative squared error: 95.39651896187635
Weighted TruePositiveRate: 0.9442815249266863
Weighted MatthewsCorrelation: 0.939671112167436
Weighted FMeasure: 0.944086533736452
Iteration time: 1278.0
Weighted AreaUnderPRC: 0.9159606416277354
Mean absolute error: 0.09424239716438841
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 36.0
Accumulative iteration time: 12756.0
Weighted Recall: 0.9442815249266863
Weighted FalsePositiveRate: 0.0063345523932105165
Kappa statistic: 0.9389712046569898
Training time: 1270.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 79.03997530483056
Incorrectly Classified Instances: 8.504398826979472
Correctly Classified Instances: 91.49560117302053
Weighted Precision: 0.9189126413780792
Weighted AreaUnderROC: 0.9816109446170708
Root mean squared error: 0.2130570420590097
Relative absolute error: 94.52228376959513
Root relative squared error: 95.41425017233263
Weighted TruePositiveRate: 0.9149560117302052
Weighted MatthewsCorrelation: 0.9050711022916877
Weighted FMeasure: 0.9151900980271116
Iteration time: 1280.0
Weighted AreaUnderPRC: 0.8757498580315022
Mean absolute error: 0.09426044918851674
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 41.0
Accumulative iteration time: 14036.0
Weighted Recall: 0.9149560117302052
Weighted FalsePositiveRate: 0.010504382537687134
Kappa statistic: 0.9068578048619678
Training time: 1274.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.937589919041532
Weighted AreaUnderROC: 0.9876331828982203
Root mean squared error: 0.21300064796236468
Relative absolute error: 94.49875095036339
Root relative squared error: 95.38899496183345
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.9274678925890125
Weighted FMeasure: 0.934652381251701
Iteration time: 1286.0
Weighted AreaUnderPRC: 0.9025052737817035
Mean absolute error: 0.09423698155714991
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 37.0
Accumulative iteration time: 15322.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.008432379892756553
Kappa statistic: 0.9292451050665861
Training time: 1283.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 6.744868035190616
Correctly Classified Instances: 93.25513196480938
Weighted Precision: 0.9349198585568743
Weighted AreaUnderROC: 0.9854766085386856
Root mean squared error: 0.21301715147079564
Relative absolute error: 94.5059918178193
Root relative squared error: 95.39638579889255
Weighted TruePositiveRate: 0.9325513196480938
Weighted MatthewsCorrelation: 0.9240039487358547
Weighted FMeasure: 0.9319135030071705
Iteration time: 1294.0
Weighted AreaUnderPRC: 0.8986688593043454
Mean absolute error: 0.09424420236680124
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 36.0
Accumulative iteration time: 16616.0
Weighted Recall: 0.9325513196480938
Weighted FalsePositiveRate: 0.009258341553803834
Kappa statistic: 0.9260045474701158
Training time: 1292.0
		
Time end:Sun Oct 08 06.06.54 EEST 2017