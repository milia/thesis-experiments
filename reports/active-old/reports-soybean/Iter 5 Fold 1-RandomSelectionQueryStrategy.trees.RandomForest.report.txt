Sun Oct 08 06.18.38 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.38 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 24.299784549092
Incorrectly Classified Instances: 36.8421052631579
Correctly Classified Instances: 63.1578947368421
Weighted Precision: 0.5815389803849682
Weighted AreaUnderROC: 0.9018304564699822
Root mean squared error: 0.16408938428077227
Relative absolute error: 56.78272479838501
Root relative squared error: 73.48485368558394
Weighted TruePositiveRate: 0.631578947368421
Weighted MatthewsCorrelation: 0.555132493180915
Weighted FMeasure: 0.5706281145073355
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7029917305866769
Mean absolute error: 0.056625431931907985
Coverage of cases: 86.25730994152046
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.631578947368421
Weighted FalsePositiveRate: 0.04479072423487564
Kappa statistic: 0.5887736308200288
Training time: 5.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 24.684518313327143
Incorrectly Classified Instances: 34.50292397660819
Correctly Classified Instances: 65.49707602339181
Weighted Precision: 0.6270379762887824
Weighted AreaUnderROC: 0.9247365239751975
Root mean squared error: 0.16113919672675975
Relative absolute error: 57.17105718398013
Root relative squared error: 72.16365852294815
Weighted TruePositiveRate: 0.6549707602339181
Weighted MatthewsCorrelation: 0.5925143197748088
Weighted FMeasure: 0.6036976611088671
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7314866654837316
Mean absolute error: 0.057012688604523624
Coverage of cases: 90.64327485380117
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6549707602339181
Weighted FalsePositiveRate: 0.040690450495581156
Kappa statistic: 0.6149530570185482
Training time: 8.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 22.853185595567876
Incorrectly Classified Instances: 36.54970760233918
Correctly Classified Instances: 63.45029239766082
Weighted Precision: 0.5514860669154114
Weighted AreaUnderROC: 0.9217260046495401
Root mean squared error: 0.1605187626010417
Relative absolute error: 54.82846087212492
Root relative squared error: 71.88580684381745
Weighted TruePositiveRate: 0.6345029239766082
Weighted MatthewsCorrelation: 0.5507454410019973
Weighted FMeasure: 0.5690814433271146
Iteration time: 10.0
Weighted AreaUnderPRC: 0.714580181993395
Mean absolute error: 0.05467658147912782
Coverage of cases: 89.76608187134502
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6345029239766082
Weighted FalsePositiveRate: 0.04326947739962832
Kappa statistic: 0.5908386133496679
Training time: 9.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 24.499846106494296
Incorrectly Classified Instances: 30.116959064327485
Correctly Classified Instances: 69.88304093567251
Weighted Precision: 0.6758229395665376
Weighted AreaUnderROC: 0.9391628394147287
Root mean squared error: 0.15440668639492786
Relative absolute error: 53.89799923191121
Root relative squared error: 69.14860950658517
Weighted TruePositiveRate: 0.6988304093567251
Weighted MatthewsCorrelation: 0.6535541192526646
Weighted FMeasure: 0.6693533618633155
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7790526743802528
Mean absolute error: 0.05374869729498118
Coverage of cases: 93.85964912280701
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6988304093567251
Weighted FalsePositiveRate: 0.03420208485567996
Kappa statistic: 0.6656605922551253
Training time: 10.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 24.176669744536767
Incorrectly Classified Instances: 25.43859649122807
Correctly Classified Instances: 74.56140350877193
Weighted Precision: 0.7735042464681215
Weighted AreaUnderROC: 0.962552986841743
Root mean squared error: 0.1466561654936119
Relative absolute error: 50.14610651896661
Root relative squared error: 65.67766044478776
Weighted TruePositiveRate: 0.7456140350877193
Weighted MatthewsCorrelation: 0.7191614349598153
Weighted FMeasure: 0.7263182986219274
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8248420304494553
Mean absolute error: 0.05000719763664304
Coverage of cases: 95.90643274853801
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7456140350877193
Weighted FalsePositiveRate: 0.028234767890280562
Kappa statistic: 0.7183693172675557
Training time: 13.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 23.114804555247797
Incorrectly Classified Instances: 24.56140350877193
Correctly Classified Instances: 75.43859649122807
Weighted Precision: 0.7293536493726247
Weighted AreaUnderROC: 0.9630974199473376
Root mean squared error: 0.14113882139369413
Relative absolute error: 47.68774453018872
Root relative squared error: 63.206804540899846
Weighted TruePositiveRate: 0.7543859649122807
Weighted MatthewsCorrelation: 0.710487870190834
Weighted FMeasure: 0.7219198636447568
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8381667658624394
Mean absolute error: 0.047555645514870064
Coverage of cases: 96.78362573099415
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7543859649122807
Weighted FalsePositiveRate: 0.02951689386906639
Kappa statistic: 0.7273089700996678
Training time: 12.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 21.42197599261316
Incorrectly Classified Instances: 20.760233918128655
Correctly Classified Instances: 79.23976608187135
Weighted Precision: 0.803579358269263
Weighted AreaUnderROC: 0.9779835131701051
Root mean squared error: 0.1296930409129404
Relative absolute error: 41.622592743271454
Root relative squared error: 58.08099151142127
Weighted TruePositiveRate: 0.7923976608187134
Weighted MatthewsCorrelation: 0.7704685001980954
Weighted FMeasure: 0.7797904552783974
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8778016710140963
Mean absolute error: 0.04150729470243173
Coverage of cases: 98.53801169590643
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 77.0
Weighted Recall: 0.7923976608187134
Weighted FalsePositiveRate: 0.022199042490622108
Kappa statistic: 0.77095910051313
Training time: 16.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 20.667897814712248
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.8060200293533626
Weighted AreaUnderROC: 0.9721277335745214
Root mean squared error: 0.12890287223264568
Relative absolute error: 41.29684667476213
Root relative squared error: 57.72712687774678
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.7880788406447069
Weighted FMeasure: 0.7983660119992236
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8801274546580488
Mean absolute error: 0.04118245097760249
Coverage of cases: 97.07602339181287
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.021866990739718838
Kappa statistic: 0.7964324181326883
Training time: 18.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 18.775007694675303
Incorrectly Classified Instances: 17.251461988304094
Correctly Classified Instances: 82.74853801169591
Weighted Precision: 0.8428470178149904
Weighted AreaUnderROC: 0.9846239446836177
Root mean squared error: 0.12297270792360551
Relative absolute error: 36.72999596916791
Root relative squared error: 55.071395926647384
Weighted TruePositiveRate: 0.827485380116959
Weighted MatthewsCorrelation: 0.8081258180775601
Weighted FMeasure: 0.8125377809051435
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9097771459360223
Mean absolute error: 0.0366282508279794
Coverage of cases: 98.24561403508773
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 116.0
Weighted Recall: 0.827485380116959
Weighted FalsePositiveRate: 0.019857417510965637
Kappa statistic: 0.8093825574365175
Training time: 19.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 19.88304093567253
Incorrectly Classified Instances: 16.666666666666668
Correctly Classified Instances: 83.33333333333333
Weighted Precision: 0.8605987792350438
Weighted AreaUnderROC: 0.9877232970239614
Root mean squared error: 0.12337704761102151
Relative absolute error: 38.94571131007443
Root relative squared error: 55.25247310540136
Weighted TruePositiveRate: 0.8333333333333334
Weighted MatthewsCorrelation: 0.8222468034168969
Weighted FMeasure: 0.8272097522491308
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9178808765647297
Mean absolute error: 0.03883782845326016
Coverage of cases: 99.41520467836257
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8333333333333334
Weighted FalsePositiveRate: 0.017137895490634165
Kappa statistic: 0.816537419063394
Training time: 21.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 19.313634964604535
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.83748387598502
Weighted AreaUnderROC: 0.9882556122757815
Root mean squared error: 0.11674860961553264
Relative absolute error: 36.160096702225744
Root relative squared error: 52.2840312503877
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.8165474539091614
Weighted FMeasure: 0.8257518123169432
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9157290563879719
Mean absolute error: 0.03605993022936671
Coverage of cases: 99.12280701754386
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.01764667657042238
Kappa statistic: 0.8196610169491525
Training time: 25.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 19.19052016004926
Incorrectly Classified Instances: 14.619883040935672
Correctly Classified Instances: 85.38011695906432
Weighted Precision: 0.8619453861798053
Weighted AreaUnderROC: 0.988080187413225
Root mean squared error: 0.1195522141332685
Relative absolute error: 37.13497240739617
Root relative squared error: 53.539581502349996
Weighted TruePositiveRate: 0.8538011695906432
Weighted MatthewsCorrelation: 0.838820581080984
Weighted FMeasure: 0.8490681442164209
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9178509924130952
Mean absolute error: 0.03703210544781922
Coverage of cases: 99.41520467836257
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 189.0
Weighted Recall: 0.8538011695906432
Weighted FalsePositiveRate: 0.01679513461955195
Kappa statistic: 0.8391224174914386
Training time: 24.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 17.728531855955687
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.9064276084754109
Weighted AreaUnderROC: 0.9818366958100732
Root mean squared error: 0.10786440149640174
Relative absolute error: 32.42105466177691
Root relative squared error: 48.305378172931356
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8903522765215197
Weighted FMeasure: 0.8979414248743848
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9309072217855008
Mean absolute error: 0.032331245646093604
Coverage of cases: 97.953216374269
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 216.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.010946782436392254
Kappa statistic: 0.8909295563267986
Training time: 27.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 17.128347183748865
Incorrectly Classified Instances: 10.526315789473685
Correctly Classified Instances: 89.47368421052632
Weighted Precision: 0.8994095605721962
Weighted AreaUnderROC: 0.9863166110512231
Root mean squared error: 0.10793040831494025
Relative absolute error: 32.00745476405067
Root relative squared error: 48.33493828996034
Weighted TruePositiveRate: 0.8947368421052632
Weighted MatthewsCorrelation: 0.8828674687747039
Weighted FMeasure: 0.893486441436257
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9403366726016457
Mean absolute error: 0.031918791454455246
Coverage of cases: 99.12280701754386
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 245.0
Weighted Recall: 0.8947368421052632
Weighted FalsePositiveRate: 0.013210316231809559
Kappa statistic: 0.8844995637775922
Training time: 29.0
		
Time end:Sun Oct 08 06.18.38 EEST 2017