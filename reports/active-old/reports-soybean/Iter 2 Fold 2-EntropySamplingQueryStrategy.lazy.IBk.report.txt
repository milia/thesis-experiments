Sun Oct 08 06.06.58 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.06.58 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 71.89381077326709
Incorrectly Classified Instances: 24.04692082111437
Correctly Classified Instances: 75.95307917888563
Weighted Precision: 0.770785198722806
Weighted AreaUnderROC: 0.8834324881365856
Root mean squared error: 0.14331187951218152
Relative absolute error: 40.32924399223529
Root relative squared error: 64.17997449084663
Weighted TruePositiveRate: 0.7595307917888563
Weighted MatthewsCorrelation: 0.7350110057324433
Weighted FMeasure: 0.7477894018419529
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6856906395809856
Mean absolute error: 0.040217528634916434
Coverage of cases: 95.01466275659824
Instances selection time: 11.0
Test time: 16.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7595307917888563
Weighted FalsePositiveRate: 0.024182592309285444
Kappa statistic: 0.7358636715724245
Training time: 1.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 64.42352214847952
Incorrectly Classified Instances: 17.302052785923753
Correctly Classified Instances: 82.69794721407625
Weighted Precision: 0.8504367448048462
Weighted AreaUnderROC: 0.9133740005784391
Root mean squared error: 0.12688840872278834
Relative absolute error: 33.09182061456836
Root relative squared error: 56.82498103250722
Weighted TruePositiveRate: 0.8269794721407625
Weighted MatthewsCorrelation: 0.8153844515427587
Weighted FMeasure: 0.8294644537617272
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7697611201279644
Mean absolute error: 0.0330001535214535
Coverage of cases: 93.841642228739
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8269794721407625
Weighted FalsePositiveRate: 0.01875880877704041
Kappa statistic: 0.8105733923359383
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 57.755826516437736
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8645731414655092
Weighted AreaUnderROC: 0.9197220675692293
Root mean squared error: 0.12356124505289243
Relative absolute error: 29.736894605209578
Root relative squared error: 55.33496303687665
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8342909107934396
Weighted FMeasure: 0.8473658415419552
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7823256960261793
Mean absolute error: 0.02965452093594333
Coverage of cases: 94.42815249266862
Instances selection time: 13.0
Test time: 44.0
Accumulative iteration time: 37.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.016661754646724775
Kappa statistic: 0.8298419214220482
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 49.62185522457166
Incorrectly Classified Instances: 13.782991202346041
Correctly Classified Instances: 86.21700879765396
Weighted Precision: 0.8734243571040714
Weighted AreaUnderROC: 0.9420743334383987
Root mean squared error: 0.11436587590827481
Relative absolute error: 25.9256988177415
Root relative squared error: 51.21696138101731
Weighted TruePositiveRate: 0.8621700879765396
Weighted MatthewsCorrelation: 0.8494704612853192
Weighted FMeasure: 0.8637420972775569
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8317219312292846
Mean absolute error: 0.02585388247752638
Coverage of cases: 97.0674486803519
Instances selection time: 13.0
Test time: 24.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8621700879765396
Weighted FalsePositiveRate: 0.017796213569125318
Kappa statistic: 0.8486391024309162
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 42.753511344343345
Incorrectly Classified Instances: 13.489736070381232
Correctly Classified Instances: 86.51026392961877
Weighted Precision: 0.8750932242421467
Weighted AreaUnderROC: 0.9479717546106784
Root mean squared error: 0.11247639229941071
Relative absolute error: 24.388010850086452
Root relative squared error: 50.37078582159719
Weighted TruePositiveRate: 0.8651026392961877
Weighted MatthewsCorrelation: 0.8522489721647688
Weighted FMeasure: 0.8667526073279974
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8397139737078199
Mean absolute error: 0.024320454033327422
Coverage of cases: 95.01466275659824
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 64.0
Weighted Recall: 0.8651026392961877
Weighted FalsePositiveRate: 0.017684712592327238
Kappa statistic: 0.8518105639058677
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 36.3790708442662
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.8817539445716254
Weighted AreaUnderROC: 0.9512599517865094
Root mean squared error: 0.108640891906792
Relative absolute error: 22.936537981662383
Root relative squared error: 48.65311720824977
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8614651902919686
Weighted FMeasure: 0.8750833919927163
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8486411876051849
Mean absolute error: 0.02287300186536989
Coverage of cases: 95.01466275659824
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 78.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.01600145320410798
Kappa statistic: 0.8616110612996082
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 32.18089211298041
Incorrectly Classified Instances: 12.316715542521994
Correctly Classified Instances: 87.68328445747801
Weighted Precision: 0.8849704256748353
Weighted AreaUnderROC: 0.9523572530637983
Root mean squared error: 0.1059054792847007
Relative absolute error: 21.295545740930212
Root relative squared error: 47.428105625781214
Weighted TruePositiveRate: 0.8768328445747801
Weighted MatthewsCorrelation: 0.8647506606630508
Weighted FMeasure: 0.8779290287801936
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8577603891814065
Mean absolute error: 0.02123655530951508
Coverage of cases: 95.01466275659824
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 91.0
Weighted Recall: 0.8768328445747801
Weighted FalsePositiveRate: 0.015555626145377699
Kappa statistic: 0.8648740447212001
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 28.78530637444047
Incorrectly Classified Instances: 12.316715542521994
Correctly Classified Instances: 87.68328445747801
Weighted Precision: 0.8829190683292646
Weighted AreaUnderROC: 0.9550930982760033
Root mean squared error: 0.1036689861343221
Relative absolute error: 20.029211892782946
Root relative squared error: 46.42652729306491
Weighted TruePositiveRate: 0.8768328445747801
Weighted MatthewsCorrelation: 0.8631395185679742
Weighted FMeasure: 0.8769013495224176
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8636504389752151
Mean absolute error: 0.01997372931136267
Coverage of cases: 95.60117302052785
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8768328445747801
Weighted FalsePositiveRate: 0.017064712424549492
Kappa statistic: 0.8646390563862163
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 24.37104491433871
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8874387418207251
Weighted AreaUnderROC: 0.9579924088827515
Root mean squared error: 0.10186342253416472
Relative absolute error: 19.009984335145766
Root relative squared error: 45.61793399155948
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.867317708653
Weighted FMeasure: 0.8801878149939921
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8724605458876612
Mean absolute error: 0.018957325098760486
Coverage of cases: 95.60117302052785
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 115.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.016238750763502206
Kappa statistic: 0.8679243500604595
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 20.836548850131233
Incorrectly Classified Instances: 12.316715542521994
Correctly Classified Instances: 87.68328445747801
Weighted Precision: 0.8850433154503085
Weighted AreaUnderROC: 0.9616626724516522
Root mean squared error: 0.10024052240405838
Relative absolute error: 18.052739692811887
Root relative squared error: 44.891143656341235
Weighted TruePositiveRate: 0.8768328445747801
Weighted MatthewsCorrelation: 0.8639139984529574
Weighted FMeasure: 0.8758405338096482
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8796959759580343
Mean absolute error: 0.01800273210363528
Coverage of cases: 95.89442815249267
Instances selection time: 20.0
Test time: 39.0
Accumulative iteration time: 135.0
Weighted Recall: 0.8768328445747801
Weighted FalsePositiveRate: 0.01597011713798329
Kappa statistic: 0.8647426029635366
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 17.888563049853435
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8947663777220616
Weighted AreaUnderROC: 0.9659437437716721
Root mean squared error: 0.09709792221523918
Relative absolute error: 16.988458675425125
Root relative squared error: 43.48377951709552
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8742274575801253
Weighted FMeasure: 0.884147301711375
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8900222006886707
Mean absolute error: 0.01694139923311108
Coverage of cases: 93.54838709677419
Instances selection time: 9.0
Test time: 41.0
Accumulative iteration time: 144.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.014734832033686975
Kappa statistic: 0.8743765586034912
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 14.73992900138911
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9018447392634567
Weighted AreaUnderROC: 0.9683311902472119
Root mean squared error: 0.09366896027259981
Relative absolute error: 15.999743464203759
Root relative squared error: 41.948172763783965
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8830060298525279
Weighted FMeasure: 0.8927918299969658
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8983419144095423
Mean absolute error: 0.015955422845189485
Coverage of cases: 94.13489736070382
Instances selection time: 7.0
Test time: 43.0
Accumulative iteration time: 151.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.013986711209658389
Kappa statistic: 0.8840924541128484
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 11.822812162370742
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9002891695260686
Weighted AreaUnderROC: 0.9676036349641899
Root mean squared error: 0.09505572024820516
Relative absolute error: 16.005539890611228
Root relative squared error: 42.56921143944876
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8824044522918497
Weighted FMeasure: 0.8925257923782552
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8950735650983
Mean absolute error: 0.015961203215014107
Coverage of cases: 93.841642228739
Instances selection time: 5.0
Test time: 46.0
Accumulative iteration time: 156.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.013986711209658389
Kappa statistic: 0.8840924541128484
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 8.93656428461182
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9015914660041869
Weighted AreaUnderROC: 0.967871607182052
Root mean squared error: 0.09462470295533247
Relative absolute error: 15.61684701639415
Root relative squared error: 42.37618711406943
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8828612155482395
Weighted FMeasure: 0.892171008493865
Iteration time: 7.0
Weighted AreaUnderPRC: 0.896971863209973
Mean absolute error: 0.015573587052359953
Coverage of cases: 93.841642228739
Instances selection time: 7.0
Test time: 48.0
Accumulative iteration time: 163.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.013731676939428327
Kappa statistic: 0.8841187131853194
Training time: 0.0
		
Time end:Sun Oct 08 06.06.59 EEST 2017