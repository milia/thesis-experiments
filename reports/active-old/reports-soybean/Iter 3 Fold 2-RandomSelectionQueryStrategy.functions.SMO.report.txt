Sun Oct 08 06.16.45 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.16.45 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 15.249266862170089
Correctly Classified Instances: 84.75073313782991
Weighted Precision: 0.8416403216543468
Weighted AreaUnderROC: 0.9636190286068687
Root mean squared error: 0.2138387282482465
Relative absolute error: 94.83907172079167
Root relative squared error: 95.76431605560646
Weighted TruePositiveRate: 0.8475073313782991
Weighted MatthewsCorrelation: 0.8231807166185107
Weighted FMeasure: 0.834293296751265
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.7898277482335946
Mean absolute error: 0.0945763596107626
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 1035.0
Weighted Recall: 0.8475073313782991
Weighted FalsePositiveRate: 0.01949888148700833
Kappa statistic: 0.8318715806840055
Training time: 1034.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 12.903225806451612
Correctly Classified Instances: 87.09677419354838
Weighted Precision: 0.8654663739270556
Weighted AreaUnderROC: 0.9671632551203687
Root mean squared error: 0.21367854391883045
Relative absolute error: 94.77028347996038
Root relative squared error: 95.69258001941219
Weighted TruePositiveRate: 0.8709677419354839
Weighted MatthewsCorrelation: 0.8498439564057035
Weighted FMeasure: 0.8600335344446577
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.8209967926443184
Mean absolute error: 0.0945077619190749
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 35.0
Accumulative iteration time: 2072.0
Weighted Recall: 0.8709677419354839
Weighted FalsePositiveRate: 0.017217067921355505
Kappa statistic: 0.8578655197893181
Training time: 1036.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.8947348397514786
Weighted AreaUnderROC: 0.9697128725526706
Root mean squared error: 0.21360262000157404
Relative absolute error: 94.7431302270007
Root relative squared error: 95.65857868547289
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8738204193087253
Weighted FMeasure: 0.8810590067850964
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.8378284974234959
Mean absolute error: 0.09448068388288242
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 35.0
Accumulative iteration time: 3113.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.01580745066673355
Kappa statistic: 0.8836883195634048
Training time: 1040.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 78.96280290168191
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.8946544169418069
Weighted AreaUnderROC: 0.9693090062107549
Root mean squared error: 0.2136021999113527
Relative absolute error: 94.74494044386469
Root relative squared error: 95.65839055466489
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.874374763240711
Weighted FMeasure: 0.881171661236423
Iteration time: 1050.0
Weighted AreaUnderPRC: 0.8397845263846174
Mean absolute error: 0.09448248908529526
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 35.0
Accumulative iteration time: 4163.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.01510436499385936
Kappa statistic: 0.8837246748818399
Training time: 1049.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 9.3841642228739
Correctly Classified Instances: 90.6158357771261
Weighted Precision: 0.9170100355627352
Weighted AreaUnderROC: 0.9772844693659555
Root mean squared error: 0.21328590931395572
Relative absolute error: 94.61279461279408
Root relative squared error: 95.51674477804308
Weighted TruePositiveRate: 0.906158357771261
Weighted MatthewsCorrelation: 0.8923603597131466
Weighted FMeasure: 0.8983848562581648
Iteration time: 1170.0
Weighted AreaUnderPRC: 0.8641147601692595
Mean absolute error: 0.09435070930915837
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 5333.0
Weighted Recall: 0.906158357771261
Weighted FalsePositiveRate: 0.01336655680443827
Kappa statistic: 0.8967995763032458
Training time: 1170.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 78.96280290168194
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9194742130843382
Weighted AreaUnderROC: 0.9785601714918155
Root mean squared error: 0.2132720500625841
Relative absolute error: 94.60917417906616
Root relative squared error: 95.5105381299793
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.8954860090861414
Weighted FMeasure: 0.9009707469538938
Iteration time: 1192.0
Weighted AreaUnderPRC: 0.87302848815288
Mean absolute error: 0.09434709890433274
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 6525.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.012920729745707984
Kappa statistic: 0.9000236440156996
Training time: 1192.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9023213983644326
Weighted AreaUnderROC: 0.9819878444765698
Root mean squared error: 0.21309719764762075
Relative absolute error: 94.53314507077906
Root relative squared error: 95.43223322203859
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.888107075158949
Weighted FMeasure: 0.8977810333976133
Iteration time: 1332.0
Weighted AreaUnderPRC: 0.8767851762217862
Mean absolute error: 0.09427128040299379
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 7857.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.012494963471468223
Kappa statistic: 0.89056792547217
Training time: 1332.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 7.9178885630498534
Correctly Classified Instances: 92.08211143695014
Weighted Precision: 0.9271052320905694
Weighted AreaUnderROC: 0.9856384000986569
Root mean squared error: 0.2130384851927736
Relative absolute error: 94.5078020346833
Root relative squared error: 95.40593977122899
Weighted TruePositiveRate: 0.9208211143695014
Weighted MatthewsCorrelation: 0.912943834644531
Weighted FMeasure: 0.9217249337464457
Iteration time: 1228.0
Weighted AreaUnderPRC: 0.9010687497601962
Mean absolute error: 0.09424600756921409
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 9085.0
Weighted Recall: 0.9208211143695014
Weighted FalsePositiveRate: 0.009674143751999844
Kappa statistic: 0.9132642487046632
Training time: 1227.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 7.331378299120234
Correctly Classified Instances: 92.66862170087977
Weighted Precision: 0.9312886096136053
Weighted AreaUnderROC: 0.9867519516024262
Root mean squared error: 0.21302446117479684
Relative absolute error: 94.50237138409133
Root relative squared error: 95.39965933502877
Weighted TruePositiveRate: 0.9266862170087976
Weighted MatthewsCorrelation: 0.9188099051394742
Weighted FMeasure: 0.927573150403409
Iteration time: 1243.0
Weighted AreaUnderPRC: 0.9082112806477312
Mean absolute error: 0.09424059196197558
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 10328.0
Weighted Recall: 0.9266862170087976
Weighted FalsePositiveRate: 0.009139719976663876
Kappa statistic: 0.9196883625846687
Training time: 1242.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 6.744868035190616
Correctly Classified Instances: 93.25513196480938
Weighted Precision: 0.9354069073643854
Weighted AreaUnderROC: 0.9873066903248445
Root mean squared error: 0.2130155903828609
Relative absolute error: 94.5005611672274
Root relative squared error: 95.39568669017827
Weighted TruePositiveRate: 0.9325513196480938
Weighted MatthewsCorrelation: 0.9251768414215387
Weighted FMeasure: 0.9330238340265774
Iteration time: 1255.0
Weighted AreaUnderPRC: 0.9069694280355699
Mean absolute error: 0.09423878675956278
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 11583.0
Weighted Recall: 0.9325513196480938
Weighted FalsePositiveRate: 0.008202257338818516
Kappa statistic: 0.9260659307510298
Training time: 1254.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 8.504398826979472
Correctly Classified Instances: 91.49560117302053
Weighted Precision: 0.9196699247176569
Weighted AreaUnderROC: 0.9827541842640356
Root mean squared error: 0.21304388642340696
Relative absolute error: 94.51323268527527
Root relative squared error: 95.40835862754048
Weighted TruePositiveRate: 0.9149560117302052
Weighted MatthewsCorrelation: 0.905558185886301
Weighted FMeasure: 0.9156413330817691
Iteration time: 1308.0
Weighted AreaUnderPRC: 0.8894636430601707
Mean absolute error: 0.09425142317645262
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 12891.0
Weighted Recall: 0.9149560117302052
Weighted FalsePositiveRate: 0.010900123951392619
Kappa statistic: 0.9067805397660323
Training time: 1308.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 7.9178885630498534
Correctly Classified Instances: 92.08211143695014
Weighted Precision: 0.9261824197308068
Weighted AreaUnderROC: 0.983888565432184
Root mean squared error: 0.21303526421024366
Relative absolute error: 94.50961225154727
Root relative squared error: 95.40449730478927
Weighted TruePositiveRate: 0.9208211143695014
Weighted MatthewsCorrelation: 0.9127930518135243
Weighted FMeasure: 0.9217247513952705
Iteration time: 1288.0
Weighted AreaUnderPRC: 0.898154132661618
Mean absolute error: 0.09424781277162693
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 14179.0
Weighted Recall: 0.9208211143695014
Weighted FalsePositiveRate: 0.009628335231615052
Kappa statistic: 0.9132389110338394
Training time: 1288.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 7.624633431085044
Correctly Classified Instances: 92.37536656891496
Weighted Precision: 0.9277321603128055
Weighted AreaUnderROC: 0.9846669982827294
Root mean squared error: 0.21302091785807997
Relative absolute error: 94.50599181781932
Root relative squared error: 95.3980725162858
Weighted TruePositiveRate: 0.9237536656891495
Weighted MatthewsCorrelation: 0.915582441676361
Weighted FMeasure: 0.9242663627339341
Iteration time: 1314.0
Weighted AreaUnderPRC: 0.9021871011359249
Mean absolute error: 0.09424420236680126
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 15493.0
Weighted Recall: 0.9237536656891495
Weighted FalsePositiveRate: 0.00921686456317336
Kappa statistic: 0.9164483480030908
Training time: 1313.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 7.038123167155425
Correctly Classified Instances: 92.96187683284458
Weighted Precision: 0.932295957975148
Weighted AreaUnderROC: 0.986978414733065
Root mean squared error: 0.21301073359167721
Relative absolute error: 94.50237138409133
Root relative squared error: 95.39351165252376
Weighted TruePositiveRate: 0.9296187683284457
Weighted MatthewsCorrelation: 0.9212782845123465
Weighted FMeasure: 0.9292252567583658
Iteration time: 1303.0
Weighted AreaUnderPRC: 0.9043357440044156
Mean absolute error: 0.09424059196197557
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 16796.0
Weighted Recall: 0.9296187683284457
Weighted FalsePositiveRate: 0.009051123260058193
Kappa statistic: 0.9228223045803038
Training time: 1303.0
		
Time end:Sun Oct 08 06.17.03 EEST 2017