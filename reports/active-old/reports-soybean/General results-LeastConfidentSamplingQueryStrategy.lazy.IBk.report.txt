Sun Oct 08 06.10.55 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.10.55 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 70.89263648909757
Incorrectly Classified Instances: 24.686079813414278
Correctly Classified Instances: 75.31392018658573
Weighted Precision: 0.7321959901713673
Weighted AreaUnderROC: 0.8792599367877469
Root mean squared error: 0.14454469045453572
Relative absolute error: 40.40874328876685
Root relative squared error: 64.73206950977774
Weighted TruePositiveRate: 0.7531392018658573
Weighted MatthewsCorrelation: 0.7112517464108936
Weighted FMeasure: 0.7248746537268045
Iteration time: 12.2
Weighted AreaUnderPRC: 0.6881785381765136
Mean absolute error: 0.04029680771179002
Coverage of cases: 94.7290391178337
Instances selection time: 12.2
Test time: 16.7
Accumulative iteration time: 12.2
Weighted Recall: 0.7531392018658573
Weighted FalsePositiveRate: 0.028440240796961953
Kappa statistic: 0.7272105598530096
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 63.49499372240844
Incorrectly Classified Instances: 19.88355541836017
Correctly Classified Instances: 80.11644458163983
Weighted Precision: 0.7973507120440584
Weighted AreaUnderROC: 0.9084347619947689
Root mean squared error: 0.131369427637745
Relative absolute error: 33.66135497115057
Root relative squared error: 58.831734978054875
Weighted TruePositiveRate: 0.8011644458163982
Weighted MatthewsCorrelation: 0.7719728365109548
Weighted FMeasure: 0.7841969263849184
Iteration time: 12.4
Weighted AreaUnderPRC: 0.7526858350612465
Mean absolute error: 0.03356811022053826
Coverage of cases: 95.13899607278215
Instances selection time: 12.2
Test time: 19.3
Accumulative iteration time: 24.6
Weighted Recall: 0.8011644458163982
Weighted FalsePositiveRate: 0.023969648743302787
Kappa statistic: 0.7807357217504988
Training time: 0.2
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 56.858031661445104
Incorrectly Classified Instances: 15.489616024420778
Correctly Classified Instances: 84.51038397557923
Weighted Precision: 0.846278871164108
Weighted AreaUnderROC: 0.9319107836435434
Root mean squared error: 0.11706915382602856
Relative absolute error: 27.866892566597993
Root relative squared error: 52.42758194083181
Weighted TruePositiveRate: 0.8451038397557923
Weighted MatthewsCorrelation: 0.8241482646633186
Weighted FMeasure: 0.8355354239279489
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8083924527037494
Mean absolute error: 0.027789698958380515
Coverage of cases: 96.42786095247897
Instances selection time: 12.7
Test time: 21.9
Accumulative iteration time: 37.6
Weighted Recall: 0.8451038397557923
Weighted FalsePositiveRate: 0.019268489501783413
Kappa statistic: 0.8295322533410004
Training time: 0.3
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 50.14625298648165
Incorrectly Classified Instances: 14.290013891032567
Correctly Classified Instances: 85.70998610896743
Weighted Precision: 0.8591256672721093
Weighted AreaUnderROC: 0.9401948159636516
Root mean squared error: 0.11241138495685013
Relative absolute error: 25.285043766923355
Root relative squared error: 50.341673304187886
Weighted TruePositiveRate: 0.8570998610896744
Weighted MatthewsCorrelation: 0.8379982146285616
Weighted FMeasure: 0.8491468547658225
Iteration time: 13.9
Weighted AreaUnderPRC: 0.8300866793823432
Mean absolute error: 0.025215002094439026
Coverage of cases: 96.16410282793984
Instances selection time: 13.8
Test time: 24.2
Accumulative iteration time: 51.5
Weighted Recall: 0.8570998610896744
Weighted FalsePositiveRate: 0.018134069458168575
Kappa statistic: 0.8428237185563121
Training time: 0.1
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 43.88061654883214
Incorrectly Classified Instances: 13.938622215362454
Correctly Classified Instances: 86.06137778463753
Weighted Precision: 0.8639679241573963
Weighted AreaUnderROC: 0.9430886216031794
Root mean squared error: 0.11041769083460648
Relative absolute error: 23.620139504670146
Root relative squared error: 49.44882870300275
Weighted TruePositiveRate: 0.8606137778463754
Weighted MatthewsCorrelation: 0.842464156540319
Weighted FMeasure: 0.8530310485749067
Iteration time: 13.9
Weighted AreaUnderPRC: 0.8380587339057337
Mean absolute error: 0.02355470975534994
Coverage of cases: 94.93354598617756
Instances selection time: 13.9
Test time: 27.3
Accumulative iteration time: 65.4
Weighted Recall: 0.8606137778463754
Weighted FalsePositiveRate: 0.017856035372684036
Kappa statistic: 0.8467001937827627
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 37.35023815132841
Incorrectly Classified Instances: 13.762754883298177
Correctly Classified Instances: 86.23724511670181
Weighted Precision: 0.8645604923829542
Weighted AreaUnderROC: 0.9446554205373692
Root mean squared error: 0.10927738120484791
Relative absolute error: 22.51679850268248
Root relative squared error: 48.93815894416149
Weighted TruePositiveRate: 0.8623724511670183
Weighted MatthewsCorrelation: 0.8440937963148544
Weighted FMeasure: 0.8545282144633181
Iteration time: 14.2
Weighted AreaUnderPRC: 0.843880217742439
Mean absolute error: 0.022454425099628156
Coverage of cases: 94.67030234432613
Instances selection time: 14.2
Test time: 29.6
Accumulative iteration time: 79.6
Weighted Recall: 0.8623724511670183
Weighted FalsePositiveRate: 0.017378542689150418
Kappa statistic: 0.8486698223460497
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 32.47142590230787
Incorrectly Classified Instances: 13.206513350825745
Correctly Classified Instances: 86.79348664917427
Weighted Precision: 0.8681562462217534
Weighted AreaUnderROC: 0.9473784552682802
Root mean squared error: 0.10757166628515827
Relative absolute error: 21.346500438705718
Root relative squared error: 48.17428130605521
Weighted TruePositiveRate: 0.8679348664917426
Weighted MatthewsCorrelation: 0.8497084961342868
Weighted FMeasure: 0.8602620100387114
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8476705985282889
Mean absolute error: 0.021287368858543286
Coverage of cases: 94.93448920443826
Instances selection time: 14.0
Test time: 32.0
Accumulative iteration time: 93.6
Weighted Recall: 0.8679348664917426
Weighted FalsePositiveRate: 0.016881938175822307
Kappa statistic: 0.8547607535185829
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 28.69206315681158
Incorrectly Classified Instances: 13.089811527842087
Correctly Classified Instances: 86.91018847215793
Weighted Precision: 0.8691807155473434
Weighted AreaUnderROC: 0.9496633458340995
Root mean squared error: 0.10672517760896845
Relative absolute error: 20.435646058644267
Root relative squared error: 47.795194646738636
Weighted TruePositiveRate: 0.869101884721579
Weighted MatthewsCorrelation: 0.8511327118745822
Weighted FMeasure: 0.861233304860358
Iteration time: 13.6
Weighted AreaUnderPRC: 0.851544018115187
Mean absolute error: 0.020379037620808865
Coverage of cases: 94.72886762360446
Instances selection time: 13.5
Test time: 34.4
Accumulative iteration time: 107.2
Weighted Recall: 0.869101884721579
Weighted FalsePositiveRate: 0.01668189712602027
Kappa statistic: 0.856001374744124
Training time: 0.1
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 24.69475832401404
Incorrectly Classified Instances: 12.737991116598925
Correctly Classified Instances: 87.26200888340107
Weighted Precision: 0.8753584053723845
Weighted AreaUnderROC: 0.9540666761591234
Root mean squared error: 0.10419380960701567
Relative absolute error: 19.270911791482725
Root relative squared error: 46.66156124282766
Weighted TruePositiveRate: 0.8726200888340108
Weighted MatthewsCorrelation: 0.855901916909723
Weighted FMeasure: 0.8653188473782982
Iteration time: 12.6
Weighted AreaUnderPRC: 0.8606277047378933
Mean absolute error: 0.019217529764359677
Coverage of cases: 94.93414621597984
Instances selection time: 12.5
Test time: 37.5
Accumulative iteration time: 119.8
Weighted Recall: 0.8726200888340108
Weighted FalsePositiveRate: 0.016312985686373137
Kappa statistic: 0.8598734197761931
Training time: 0.1
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 21.21318628154482
Incorrectly Classified Instances: 12.415324724323025
Correctly Classified Instances: 87.58467527567697
Weighted Precision: 0.8774410630451299
Weighted AreaUnderROC: 0.9554839938585399
Root mean squared error: 0.10269122196407099
Relative absolute error: 18.438279095358354
Root relative squared error: 45.988650965447235
Weighted TruePositiveRate: 0.8758467527567697
Weighted MatthewsCorrelation: 0.8590913626322145
Weighted FMeasure: 0.8685302957060994
Iteration time: 12.1
Weighted AreaUnderPRC: 0.8660366231703878
Mean absolute error: 0.018387203529997412
Coverage of cases: 94.55385776268629
Instances selection time: 11.9
Test time: 39.3
Accumulative iteration time: 131.9
Weighted Recall: 0.8758467527567697
Weighted FalsePositiveRate: 0.015819908552467742
Kappa statistic: 0.8634634617496199
Training time: 0.2
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 17.891789849166372
Incorrectly Classified Instances: 11.713141602785067
Correctly Classified Instances: 88.28685839721494
Weighted Precision: 0.8857558347759505
Weighted AreaUnderROC: 0.9598378527459548
Root mean squared error: 0.1002452858852068
Relative absolute error: 17.50076818200707
Root relative squared error: 44.893276906561894
Weighted TruePositiveRate: 0.8828685839721494
Weighted MatthewsCorrelation: 0.8676437896831114
Weighted FMeasure: 0.8769530768435692
Iteration time: 9.5
Weighted AreaUnderPRC: 0.8746816875653834
Mean absolute error: 0.017452289599785592
Coverage of cases: 92.41609644835452
Instances selection time: 9.5
Test time: 41.4
Accumulative iteration time: 141.4
Weighted Recall: 0.8828685839721494
Weighted FalsePositiveRate: 0.015098151232884608
Kappa statistic: 0.8712095772688133
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 14.791002690654196
Incorrectly Classified Instances: 10.951364236593438
Correctly Classified Instances: 89.04863576340655
Weighted Precision: 0.8944900507898528
Weighted AreaUnderROC: 0.9626413481284875
Root mean squared error: 0.09683912864532512
Relative absolute error: 16.48020938020868
Root relative squared error: 43.36788288122684
Weighted TruePositiveRate: 0.8904863576340656
Weighted MatthewsCorrelation: 0.8770585198414483
Weighted FMeasure: 0.8865161279071181
Iteration time: 8.1
Weighted AreaUnderPRC: 0.8840789640334007
Mean absolute error: 0.016434557830679163
Coverage of cases: 92.91420143712165
Instances selection time: 7.9
Test time: 43.8
Accumulative iteration time: 149.5
Weighted Recall: 0.8904863576340656
Weighted FalsePositiveRate: 0.014138710112707412
Kappa statistic: 0.8796584971345671
Training time: 0.2
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 11.791704011791593
Incorrectly Classified Instances: 10.101953319270807
Correctly Classified Instances: 89.8980466807292
Weighted Precision: 0.9059638178412254
Weighted AreaUnderROC: 0.9658857007143407
Root mean squared error: 0.09312149515763013
Relative absolute error: 15.444320816943868
Root relative squared error: 41.70299911012048
Weighted TruePositiveRate: 0.8989804668072919
Weighted MatthewsCorrelation: 0.8878895392803281
Weighted FMeasure: 0.897181116301288
Iteration time: 5.7
Weighted AreaUnderPRC: 0.8941228561273737
Mean absolute error: 0.015401538764819506
Coverage of cases: 93.5876592752654
Instances selection time: 5.6
Test time: 45.5
Accumulative iteration time: 155.2
Weighted Recall: 0.8989804668072919
Weighted FalsePositiveRate: 0.013141687037732133
Kappa statistic: 0.8890821070190812
Training time: 0.1
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 8.914251982789196
Incorrectly Classified Instances: 9.984994254943322
Correctly Classified Instances: 90.01500574505668
Weighted Precision: 0.907542592421828
Weighted AreaUnderROC: 0.9665898320067529
Root mean squared error: 0.09262017640942585
Relative absolute error: 15.161263380501495
Root relative squared error: 41.47849138207271
Weighted TruePositiveRate: 0.900150057450567
Weighted MatthewsCorrelation: 0.8893116903518343
Weighted FMeasure: 0.8981851236243642
Iteration time: 3.2
Weighted AreaUnderPRC: 0.8959823465427208
Mean absolute error: 0.015119265420998857
Coverage of cases: 93.76318361887122
Instances selection time: 3.1
Test time: 47.5
Accumulative iteration time: 158.4
Weighted Recall: 0.900150057450567
Weighted FalsePositiveRate: 0.012922980731085254
Kappa statistic: 0.8903681983115825
Training time: 0.1
		
Time end:Sun Oct 08 06.11.06 EEST 2017