Sun Oct 08 06.15.01 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.15.01 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 27.346875961834357
Incorrectly Classified Instances: 40.64327485380117
Correctly Classified Instances: 59.35672514619883
Weighted Precision: 0.5592884145975346
Weighted AreaUnderROC: 0.8908072844146633
Root mean squared error: 0.17283771021450756
Relative absolute error: 62.30528141769883
Root relative squared error: 77.40265406037433
Weighted TruePositiveRate: 0.5935672514619883
Weighted MatthewsCorrelation: 0.5283228544680237
Weighted FMeasure: 0.5439886588585962
Iteration time: 7.0
Weighted AreaUnderPRC: 0.672757631826561
Mean absolute error: 0.062132690610448134
Coverage of cases: 83.62573099415205
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5935672514619883
Weighted FalsePositiveRate: 0.04827940721629385
Kappa statistic: 0.5451866592679053
Training time: 5.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 27.300707910126135
Incorrectly Classified Instances: 32.45614035087719
Correctly Classified Instances: 67.54385964912281
Weighted Precision: 0.6926104608668949
Weighted AreaUnderROC: 0.9168670161425201
Root mean squared error: 0.16544334731367674
Relative absolute error: 59.6950326973623
Root relative squared error: 74.09120476554428
Weighted TruePositiveRate: 0.6754385964912281
Weighted MatthewsCorrelation: 0.6337879569569116
Weighted FMeasure: 0.63551318172447
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7263498252717356
Mean absolute error: 0.05952967249598509
Coverage of cases: 88.59649122807018
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6754385964912281
Weighted FalsePositiveRate: 0.034231774987693794
Kappa statistic: 0.6396206533192834
Training time: 8.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 26.531240381655827
Incorrectly Classified Instances: 31.871345029239766
Correctly Classified Instances: 68.12865497076024
Weighted Precision: 0.6824354484543381
Weighted AreaUnderROC: 0.9261442767238005
Root mean squared error: 0.1594262794319264
Relative absolute error: 56.36552957031865
Root relative squared error: 71.39655541424884
Weighted TruePositiveRate: 0.6812865497076024
Weighted MatthewsCorrelation: 0.6363932373020442
Weighted FMeasure: 0.6441757720175629
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7375116863460286
Mean absolute error: 0.05620939236929333
Coverage of cases: 91.2280701754386
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 28.0
Weighted Recall: 0.6812865497076024
Weighted FalsePositiveRate: 0.03520938529300005
Kappa statistic: 0.6454981170831907
Training time: 9.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 27.546937519236643
Incorrectly Classified Instances: 23.099415204678362
Correctly Classified Instances: 76.90058479532163
Weighted Precision: 0.7729045857748578
Weighted AreaUnderROC: 0.9628538898012217
Root mean squared error: 0.1491532641036663
Relative absolute error: 54.15100072419642
Root relative squared error: 66.79594683974636
Weighted TruePositiveRate: 0.7690058479532164
Weighted MatthewsCorrelation: 0.7440568098321299
Weighted FMeasure: 0.7581736107833416
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8218149881206045
Mean absolute error: 0.054000997952107714
Coverage of cases: 96.19883040935673
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7690058479532164
Weighted FalsePositiveRate: 0.025864435995354724
Kappa statistic: 0.7446868828137551
Training time: 11.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 27.54693751923666
Incorrectly Classified Instances: 17.251461988304094
Correctly Classified Instances: 82.74853801169591
Weighted Precision: 0.8459547390188493
Weighted AreaUnderROC: 0.9739652812771811
Root mean squared error: 0.1408029185272683
Relative absolute error: 51.30699667963215
Root relative squared error: 63.056375717609036
Weighted TruePositiveRate: 0.827485380116959
Weighted MatthewsCorrelation: 0.8142981656257011
Weighted FMeasure: 0.8261122934992289
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8885385855720372
Mean absolute error: 0.05116487203509069
Coverage of cases: 97.6608187134503
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 59.0
Weighted Recall: 0.827485380116959
Weighted FalsePositiveRate: 0.019848160371998283
Kappa statistic: 0.8101448048098907
Training time: 14.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 26.038781163434866
Incorrectly Classified Instances: 20.17543859649123
Correctly Classified Instances: 79.82456140350877
Weighted Precision: 0.8066505279574718
Weighted AreaUnderROC: 0.9810973958008142
Root mean squared error: 0.1359778137521518
Relative absolute error: 48.16190906345099
Root relative squared error: 60.89552832354274
Weighted TruePositiveRate: 0.7982456140350878
Weighted MatthewsCorrelation: 0.7738313524159846
Weighted FMeasure: 0.7857853677413696
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8632226760039144
Mean absolute error: 0.04802849657297094
Coverage of cases: 99.41520467836257
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7982456140350878
Weighted FalsePositiveRate: 0.022042395377742486
Kappa statistic: 0.7776542418874609
Training time: 14.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 24.269005847953206
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8643050652067814
Weighted AreaUnderROC: 0.9855553807948809
Root mean squared error: 0.1294424788279594
Relative absolute error: 44.41963724667639
Root relative squared error: 57.96878121680234
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8356225175126086
Weighted FMeasure: 0.8453553092547762
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9168241690201441
Mean absolute error: 0.044296591160120885
Coverage of cases: 98.53801169590643
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.016132159940195626
Kappa statistic: 0.8296396548807309
Training time: 16.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 21.775931055709478
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8916315893866152
Weighted AreaUnderROC: 0.9857483118717382
Root mean squared error: 0.11938509912165907
Relative absolute error: 39.869704200346824
Root relative squared error: 53.46474166898351
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8689011504727053
Weighted FMeasure: 0.877991971868624
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9119007086657364
Mean absolute error: 0.0397592618064404
Coverage of cases: 99.12280701754386
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.012581185128449506
Kappa statistic: 0.8653265577828197
Training time: 18.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 23.1763619575254
Incorrectly Classified Instances: 11.403508771929825
Correctly Classified Instances: 88.59649122807018
Weighted Precision: 0.8914420134978178
Weighted AreaUnderROC: 0.9884054066502616
Root mean squared error: 0.11959053074325003
Relative absolute error: 40.52274505807635
Root relative squared error: 53.55674099435835
Weighted TruePositiveRate: 0.8859649122807017
Weighted MatthewsCorrelation: 0.873995678152103
Weighted FMeasure: 0.8838111365542104
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9235993122073823
Mean absolute error: 0.0404104936867247
Coverage of cases: 99.41520467836257
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8859649122807017
Weighted FalsePositiveRate: 0.012655649736588111
Kappa statistic: 0.8749038659938849
Training time: 19.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 20.74484456755925
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.9054805054631843
Weighted AreaUnderROC: 0.9878542569606349
Root mean squared error: 0.1138776379216534
Relative absolute error: 36.344707097046324
Root relative squared error: 50.99831166660783
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.884029317480745
Weighted FMeasure: 0.8902731103480812
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9288613205450655
Mean absolute error: 0.036244029238052054
Coverage of cases: 98.83040935672514
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.01204606219823906
Kappa statistic: 0.8811295231653703
Training time: 24.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 21.529701446598946
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.848753359545167
Weighted AreaUnderROC: 0.9832168858779716
Root mean squared error: 0.12493908846469941
Relative absolute error: 40.72825077850941
Root relative squared error: 55.952008569481194
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8243481862720887
Weighted FMeasure: 0.8350860061351659
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9062220150925647
Mean absolute error: 0.04061543013923412
Coverage of cases: 98.83040935672514
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 191.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.019564317837629852
Kappa statistic: 0.8258688642063776
Training time: 25.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 18.605724838411817
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8925453085430234
Weighted AreaUnderROC: 0.9904485942144655
Root mean squared error: 0.113156012617526
Relative absolute error: 35.35733547877943
Root relative squared error: 50.67514310745912
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8678099114055063
Weighted FMeasure: 0.8739186279931249
Iteration time: 31.0
Weighted AreaUnderPRC: 0.940268833625195
Mean absolute error: 0.0352593927212208
Coverage of cases: 99.70760233918129
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 222.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.012938643372089111
Kappa statistic: 0.8651406896940221
Training time: 30.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 18.497999384426027
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8917847130127832
Weighted AreaUnderROC: 0.9824199940557502
Root mean squared error: 0.11227437601932329
Relative absolute error: 33.19779402239247
Root relative squared error: 50.280316003275914
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8659423535115249
Weighted FMeasure: 0.8724237227241987
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9284176650581771
Mean absolute error: 0.03310583337413129
Coverage of cases: 98.53801169590643
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 252.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.01363740982911828
Kappa statistic: 0.8618064952638701
Training time: 29.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 17.359187442289954
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.9048803439409313
Weighted AreaUnderROC: 0.9893606532298362
Root mean squared error: 0.10404368492210998
Relative absolute error: 29.9934119008487
Root relative squared error: 46.59433026043813
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8878704623117555
Weighted FMeasure: 0.8957828197860356
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9443670815916133
Mean absolute error: 0.029910327657356305
Coverage of cases: 99.41520467836257
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 284.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.011084915187786648
Kappa statistic: 0.8877752880621783
Training time: 31.0
		
Time end:Sun Oct 08 06.15.02 EEST 2017