Sun Oct 08 06.07.46 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.46 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 8.936564284611833
Incorrectly Classified Instances: 26.979472140762464
Correctly Classified Instances: 73.02052785923753
Weighted Precision: 0.6882988673098678
Weighted AreaUnderROC: 0.9621500355272148
Root mean squared error: 0.1507337980614795
Relative absolute error: 29.941370669973704
Root relative squared error: 67.50376415007439
Weighted TruePositiveRate: 0.7302052785923754
Weighted MatthewsCorrelation: 0.6714434821423486
Weighted FMeasure: 0.6763025323773166
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7992588231839564
Mean absolute error: 0.029858430585015593
Coverage of cases: 82.40469208211144
Instances selection time: 31.0
Test time: 39.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7302052785923754
Weighted FalsePositiveRate: 0.03220927785195916
Kappa statistic: 0.6993752156081108
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 8.48896434634975
Incorrectly Classified Instances: 21.994134897360702
Correctly Classified Instances: 78.00586510263929
Weighted Precision: 0.8022256673176614
Weighted AreaUnderROC: 0.9753369355960436
Root mean squared error: 0.13759232554332282
Relative absolute error: 26.230679255611108
Root relative squared error: 61.61856207290977
Weighted TruePositiveRate: 0.7800586510263929
Weighted MatthewsCorrelation: 0.7520044088867365
Weighted FMeasure: 0.7493662415312192
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8654350416938896
Mean absolute error: 0.026158018094238455
Coverage of cases: 85.92375366568915
Instances selection time: 24.0
Test time: 38.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7800586510263929
Weighted FalsePositiveRate: 0.026402672543366815
Kappa statistic: 0.7560615020697812
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 8.010495446828235
Incorrectly Classified Instances: 14.36950146627566
Correctly Classified Instances: 85.63049853372434
Weighted Precision: 0.8841968066592883
Weighted AreaUnderROC: 0.9872060886316067
Root mean squared error: 0.11059756867021646
Relative absolute error: 18.191883660366994
Root relative squared error: 49.52938416665456
Weighted TruePositiveRate: 0.8563049853372434
Weighted MatthewsCorrelation: 0.8417320390590947
Weighted FMeasure: 0.8455056080938388
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9275300246966965
Mean absolute error: 0.018141490630837052
Coverage of cases: 92.08211143695014
Instances selection time: 22.0
Test time: 39.0
Accumulative iteration time: 77.0
Weighted Recall: 0.8563049853372434
Weighted FalsePositiveRate: 0.019107520475659223
Kappa statistic: 0.841482619914997
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 8.319185059422775
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.9005901660412485
Weighted AreaUnderROC: 0.991397479670391
Root mean squared error: 0.10143439459933348
Relative absolute error: 17.07064990204657
Root relative squared error: 45.42580056893566
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8667470748205762
Weighted FMeasure: 0.8690423063978201
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9470871622098808
Mean absolute error: 0.017023362783204484
Coverage of cases: 95.30791788856305
Instances selection time: 40.0
Test time: 48.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.014700469890509563
Kappa statistic: 0.8675916279950753
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 8.720481555795677
Incorrectly Classified Instances: 8.211143695014663
Correctly Classified Instances: 91.78885630498533
Weighted Precision: 0.9276498253969985
Weighted AreaUnderROC: 0.9944589840055196
Root mean squared error: 0.08384777249012618
Relative absolute error: 14.128145317178705
Root relative squared error: 37.54990805959805
Weighted TruePositiveRate: 0.9178885630498533
Weighted MatthewsCorrelation: 0.9099188957077617
Weighted FMeasure: 0.9156359842791693
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9666796594656869
Mean absolute error: 0.014089009180566145
Coverage of cases: 97.94721407624634
Instances selection time: 19.0
Test time: 38.0
Accumulative iteration time: 137.0
Weighted Recall: 0.9178885630498533
Weighted FalsePositiveRate: 0.011023411001587264
Kappa statistic: 0.9097593709241442
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 8.55070226886869
Incorrectly Classified Instances: 7.624633431085044
Correctly Classified Instances: 92.37536656891496
Weighted Precision: 0.9294007887269916
Weighted AreaUnderROC: 0.9946085676761919
Root mean squared error: 0.08030658359842324
Relative absolute error: 13.509751289126717
Root relative squared error: 35.964042229700496
Weighted TruePositiveRate: 0.9237536656891495
Weighted MatthewsCorrelation: 0.9150150948476594
Weighted FMeasure: 0.9213147570738387
Iteration time: 17.0
Weighted AreaUnderPRC: 0.965998854901325
Mean absolute error: 0.01347232815536194
Coverage of cases: 98.53372434017595
Instances selection time: 16.0
Test time: 39.0
Accumulative iteration time: 154.0
Weighted Recall: 0.9237536656891495
Weighted FalsePositiveRate: 0.010470916572115092
Kappa statistic: 0.9162162162162162
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 7.856150640530968
Incorrectly Classified Instances: 7.331378299120234
Correctly Classified Instances: 92.66862170087977
Weighted Precision: 0.9363933868842704
Weighted AreaUnderROC: 0.9940482492239682
Root mean squared error: 0.0799475340137347
Relative absolute error: 11.997969997294172
Root relative squared error: 35.80324751216069
Weighted TruePositiveRate: 0.9266862170087976
Weighted MatthewsCorrelation: 0.9197396863137861
Weighted FMeasure: 0.9246252086712833
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9635496130467847
Mean absolute error: 0.011964734623340554
Coverage of cases: 97.36070381231671
Instances selection time: 14.0
Test time: 39.0
Accumulative iteration time: 168.0
Weighted Recall: 0.9266862170087976
Weighted FalsePositiveRate: 0.010047993773577204
Kappa statistic: 0.9194546536786311
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 7.593764469825609
Incorrectly Classified Instances: 7.038123167155425
Correctly Classified Instances: 92.96187683284458
Weighted Precision: 0.9390652669755053
Weighted AreaUnderROC: 0.9948975367202851
Root mean squared error: 0.08175329114623052
Relative absolute error: 11.584289429489376
Root relative squared error: 36.611927484084404
Weighted TruePositiveRate: 0.9296187683284457
Weighted MatthewsCorrelation: 0.9233009824497814
Weighted FMeasure: 0.9283902608234322
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9679857154662499
Mean absolute error: 0.01155219998508646
Coverage of cases: 97.0674486803519
Instances selection time: 31.0
Test time: 70.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9296187683284457
Weighted FalsePositiveRate: 0.009590714584750722
Kappa statistic: 0.922702758861698
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 7.547461027936426
Incorrectly Classified Instances: 8.211143695014663
Correctly Classified Instances: 91.78885630498533
Weighted Precision: 0.9338093959689958
Weighted AreaUnderROC: 0.9947253895920095
Root mean squared error: 0.08487698039761044
Relative absolute error: 12.028668223696267
Root relative squared error: 38.010822656998904
Weighted TruePositiveRate: 0.9178885630498533
Weighted MatthewsCorrelation: 0.9128417210738069
Weighted FMeasure: 0.9173553387112126
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9668718741389921
Mean absolute error: 0.011995347813104415
Coverage of cases: 97.0674486803519
Instances selection time: 18.0
Test time: 72.0
Accumulative iteration time: 219.0
Weighted Recall: 0.9178885630498533
Weighted FalsePositiveRate: 0.009956553581269649
Kappa statistic: 0.9099049793823187
Training time: 1.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 7.331378299120249
Incorrectly Classified Instances: 7.9178885630498534
Correctly Classified Instances: 92.08211143695014
Weighted Precision: 0.9324381131939876
Weighted AreaUnderROC: 0.9948633894434102
Root mean squared error: 0.0831315089987665
Relative absolute error: 11.32016065834838
Root relative squared error: 37.229140704088756
Weighted TruePositiveRate: 0.9208211143695014
Weighted MatthewsCorrelation: 0.9147931526642167
Weighted FMeasure: 0.9199134533447797
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9684367402876699
Mean absolute error: 0.011288802872591281
Coverage of cases: 96.7741935483871
Instances selection time: 19.0
Test time: 72.0
Accumulative iteration time: 239.0
Weighted Recall: 0.9208211143695014
Weighted FalsePositiveRate: 0.009727172602467356
Kappa statistic: 0.9131087202718007
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 7.346812779749976
Incorrectly Classified Instances: 7.9178885630498534
Correctly Classified Instances: 92.08211143695014
Weighted Precision: 0.9324381131939876
Weighted AreaUnderROC: 0.9946948651360122
Root mean squared error: 0.08352162915290853
Relative absolute error: 11.350928007566278
Root relative squared error: 37.40384988818729
Weighted TruePositiveRate: 0.9208211143695014
Weighted MatthewsCorrelation: 0.9147931526642167
Weighted FMeasure: 0.9199134533447797
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9678787757083134
Mean absolute error: 0.011319484993695004
Coverage of cases: 96.48093841642229
Instances selection time: 16.0
Test time: 76.0
Accumulative iteration time: 255.0
Weighted Recall: 0.9208211143695014
Weighted FalsePositiveRate: 0.009727172602467356
Kappa statistic: 0.9131087202718007
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 7.285074857231069
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9219278852828185
Weighted AreaUnderROC: 0.9945948120279015
Root mean squared error: 0.08461059032700709
Relative absolute error: 11.31581889246995
Root relative squared error: 37.89152404760146
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.902262193617311
Weighted FMeasure: 0.907216239743088
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9680108446499779
Mean absolute error: 0.011284473133765148
Coverage of cases: 96.7741935483871
Instances selection time: 9.0
Test time: 73.0
Accumulative iteration time: 265.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.010450241941110886
Kappa statistic: 0.9002293469746019
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 7.146164531563525
Incorrectly Classified Instances: 8.504398826979472
Correctly Classified Instances: 91.49560117302053
Weighted Precision: 0.9271062017037184
Weighted AreaUnderROC: 0.9944183413093174
Root mean squared error: 0.08467848569851225
Relative absolute error: 11.24658074120864
Root relative squared error: 37.92192992341636
Weighted TruePositiveRate: 0.9149560117302052
Weighted MatthewsCorrelation: 0.9086829170452858
Weighted FMeasure: 0.9132242272127616
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9668328571891603
Mean absolute error: 0.011215426777936691
Coverage of cases: 96.18768328445748
Instances selection time: 8.0
Test time: 75.0
Accumulative iteration time: 274.0
Weighted Recall: 0.9149560117302052
Weighted FalsePositiveRate: 0.009904366035678722
Kappa statistic: 0.906637084592145
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 7.1152955703040695
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9219278852828185
Weighted AreaUnderROC: 0.9947458611336758
Root mean squared error: 0.08658136586274096
Relative absolute error: 11.476539212853044
Root relative squared error: 38.7741049197603
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.9023803815926692
Weighted FMeasure: 0.9072334854338211
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9674148382760772
Mean absolute error: 0.01144474824550452
Coverage of cases: 95.89442815249267
Instances selection time: 2.0
Test time: 78.0
Accumulative iteration time: 277.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.010450241941110886
Kappa statistic: 0.9002086263699955
Training time: 1.0
		
Time end:Sun Oct 08 06.07.48 EEST 2017