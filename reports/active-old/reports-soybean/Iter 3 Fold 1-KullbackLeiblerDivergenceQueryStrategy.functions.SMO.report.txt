Sun Oct 08 06.07.34 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.34 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.863701641217682
Weighted AreaUnderROC: 0.9630072975591
Root mean squared error: 0.21351586243562523
Relative absolute error: 94.70435347628282
Root relative squared error: 95.61972567211184
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8317501072585077
Weighted FMeasure: 0.8425187626655766
Iteration time: 1288.0
Weighted AreaUnderPRC: 0.7838387136554141
Mean absolute error: 0.09444201454698646
Coverage of cases: 100.0
Instances selection time: 62.0
Test time: 38.0
Accumulative iteration time: 1288.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.0170397976953145
Kappa statistic: 0.8302538793628199
Training time: 1226.0
		
Time end:Sun Oct 08 06.07.35 EEST 2017