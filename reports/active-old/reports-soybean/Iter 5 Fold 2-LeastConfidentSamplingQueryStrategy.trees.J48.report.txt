Sun Oct 08 06.11.13 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.11.13 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 13.582342954159621
Incorrectly Classified Instances: 42.22873900293255
Correctly Classified Instances: 57.77126099706745
Weighted Precision: 0.5030838263747824
Weighted AreaUnderROC: 0.8409224255511623
Root mean squared error: 0.1814109142536745
Relative absolute error: 49.37916387137268
Root relative squared error: 81.24202884501518
Weighted TruePositiveRate: 0.5777126099706745
Weighted MatthewsCorrelation: 0.4916843842283162
Weighted FMeasure: 0.5054368902999973
Iteration time: 3.0
Weighted AreaUnderPRC: 0.48549073815926813
Mean absolute error: 0.04924237948391779
Coverage of cases: 78.59237536656892
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5777126099706745
Weighted FalsePositiveRate: 0.04795907759464966
Kappa statistic: 0.5297227409854907
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 12.82605340330301
Incorrectly Classified Instances: 27.85923753665689
Correctly Classified Instances: 72.14076246334311
Weighted Precision: 0.6499353317285134
Weighted AreaUnderROC: 0.88605047318535
Root mean squared error: 0.1614879153474606
Relative absolute error: 40.40913886565802
Root relative squared error: 72.31982668051643
Weighted TruePositiveRate: 0.7214076246334311
Weighted MatthewsCorrelation: 0.6583961996227453
Weighted FMeasure: 0.675836160765407
Iteration time: 3.0
Weighted AreaUnderPRC: 0.630842433493211
Mean absolute error: 0.040297202192900325
Coverage of cases: 81.2316715542522
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7214076246334311
Weighted FalsePositiveRate: 0.03123629581533292
Kappa statistic: 0.69094048732088
Training time: 2.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 11.91541904614909
Incorrectly Classified Instances: 31.085043988269796
Correctly Classified Instances: 68.91495601173021
Weighted Precision: 0.6277831297252267
Weighted AreaUnderROC: 0.8593336577433548
Root mean squared error: 0.16853800439583042
Relative absolute error: 41.872997753088555
Root relative squared error: 75.47709833742823
Weighted TruePositiveRate: 0.6891495601173021
Weighted MatthewsCorrelation: 0.6231476431870326
Weighted FMeasure: 0.6338892168202036
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5594981250071502
Mean absolute error: 0.04175700606956236
Coverage of cases: 76.53958944281526
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6891495601173021
Weighted FalsePositiveRate: 0.031440998803241464
Kappa statistic: 0.655890024942404
Training time: 2.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 10.788701960179042
Incorrectly Classified Instances: 24.926686217008797
Correctly Classified Instances: 75.0733137829912
Weighted Precision: 0.6978040715627934
Weighted AreaUnderROC: 0.90919540369081
Root mean squared error: 0.14831627080412518
Relative absolute error: 32.5121998788758
Root relative squared error: 66.42111253573465
Weighted TruePositiveRate: 0.750733137829912
Weighted MatthewsCorrelation: 0.6949568678855877
Weighted FMeasure: 0.7116314711122306
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6857942296722447
Mean absolute error: 0.03242213838336673
Coverage of cases: 85.63049853372434
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.750733137829912
Weighted FalsePositiveRate: 0.03128709789400821
Kappa statistic: 0.7235970056739618
Training time: 3.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 10.016977928692725
Incorrectly Classified Instances: 20.527859237536656
Correctly Classified Instances: 79.47214076246334
Weighted Precision: 0.7512208500931407
Weighted AreaUnderROC: 0.9142233834316345
Root mean squared error: 0.1386614969595407
Relative absolute error: 26.128967843438854
Root relative squared error: 62.09737370006014
Weighted TruePositiveRate: 0.7947214076246334
Weighted MatthewsCorrelation: 0.7484418579854671
Weighted FMeasure: 0.759589357832304
Iteration time: 4.0
Weighted AreaUnderPRC: 0.716789076259448
Mean absolute error: 0.02605658843113039
Coverage of cases: 85.63049853372434
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7947214076246334
Weighted FalsePositiveRate: 0.0242301364418619
Kappa statistic: 0.7728419029129909
Training time: 3.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 6.513350825744733
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8954491021371725
Weighted AreaUnderROC: 0.9581141192777552
Root mean squared error: 0.10508351442080638
Relative absolute error: 14.487124421075709
Root relative squared error: 47.06000157064861
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.870172966527878
Weighted FMeasure: 0.8749636551003203
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8483896990148034
Mean absolute error: 0.0144469938825133
Coverage of cases: 92.96187683284458
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.012446336366343189
Kappa statistic: 0.8679430627839541
Training time: 4.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 6.343571538817739
Incorrectly Classified Instances: 11.730205278592376
Correctly Classified Instances: 88.26979472140762
Weighted Precision: 0.8980072852032487
Weighted AreaUnderROC: 0.9584924873232321
Root mean squared error: 0.10338710163727662
Relative absolute error: 14.493931712243677
Root relative squared error: 46.30028974812917
Weighted TruePositiveRate: 0.8826979472140762
Weighted MatthewsCorrelation: 0.8736873795071047
Weighted FMeasure: 0.8787002930721298
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8517531188900047
Mean absolute error: 0.014453782316919031
Coverage of cases: 92.96187683284458
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8826979472140762
Weighted FalsePositiveRate: 0.011989057177516707
Kappa statistic: 0.8712077577497238
Training time: 4.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 6.683130112671728
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.895178667520621
Weighted AreaUnderROC: 0.9567650658068477
Root mean squared error: 0.10410405638407669
Relative absolute error: 15.414682172531641
Root relative squared error: 46.6213666715311
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8704901295621781
Weighted FMeasure: 0.8756666292990003
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8567470196385953
Mean absolute error: 0.015371982221915345
Coverage of cases: 92.66862170087977
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.012396536144006683
Kappa statistic: 0.867949299180173
Training time: 4.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 7.917888563049877
Incorrectly Classified Instances: 16.129032258064516
Correctly Classified Instances: 83.87096774193549
Weighted Precision: 0.8183856270806418
Weighted AreaUnderROC: 0.937080556380679
Root mean squared error: 0.12299985783368932
Relative absolute error: 23.02180160964933
Root relative squared error: 55.08355458748233
Weighted TruePositiveRate: 0.8387096774193549
Weighted MatthewsCorrelation: 0.8104304026246995
Weighted FMeasure: 0.8214973620633714
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7793460597175528
Mean absolute error: 0.022958029306021695
Coverage of cases: 89.44281524926686
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.8387096774193549
Weighted FalsePositiveRate: 0.016939082863406955
Kappa statistic: 0.8225504295500132
Training time: 4.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 7.609198950455343
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8500671739879871
Weighted AreaUnderROC: 0.9468434421441545
Root mean squared error: 0.11953824615939355
Relative absolute error: 21.143918486252446
Root relative squared error: 53.533326164620604
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8267650799873487
Weighted FMeasure: 0.8342126450761851
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8200673291162982
Mean absolute error: 0.021085348074933372
Coverage of cases: 90.9090909090909
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 44.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.015505772948760994
Kappa statistic: 0.8291954522686678
Training time: 5.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 7.825281679271513
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.8851487922309036
Weighted AreaUnderROC: 0.949592327298465
Root mean squared error: 0.1103720909376406
Relative absolute error: 18.38759061394849
Root relative squared error: 49.42840750530429
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8604830687898742
Weighted FMeasure: 0.8673437694859978
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8289057545252703
Mean absolute error: 0.018336655459893395
Coverage of cases: 91.49560117302053
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 49.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.014497411665917776
Kappa statistic: 0.8613218075548074
Training time: 5.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 7.748109276122883
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8937647282661944
Weighted AreaUnderROC: 0.9534237085401347
Root mean squared error: 0.1049648528614005
Relative absolute error: 16.43707971612046
Root relative squared error: 47.006860854877964
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8716860151448447
Weighted FMeasure: 0.8781414880586297
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8451484666942741
Mean absolute error: 0.01639154763934465
Coverage of cases: 92.08211143695014
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.013783935160272064
Kappa statistic: 0.8742102077106428
Training time: 6.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 7.825281679271539
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8937647282661944
Weighted AreaUnderROC: 0.9536968300885434
Root mean squared error: 0.10514230277018174
Relative absolute error: 17.276530107662193
Root relative squared error: 47.08632900963073
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8716860151448447
Weighted FMeasure: 0.8781414880586297
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8474265815289534
Mean absolute error: 0.017228672683541398
Coverage of cases: 92.37536656891496
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.013783935160272064
Kappa statistic: 0.8742102077106428
Training time: 5.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 8.350054020682228
Incorrectly Classified Instances: 13.489736070381232
Correctly Classified Instances: 86.51026392961877
Weighted Precision: 0.8776653859236758
Weighted AreaUnderROC: 0.947649122265566
Root mean squared error: 0.11198408166202954
Relative absolute error: 19.888144546763098
Root relative squared error: 50.150312234506934
Weighted TruePositiveRate: 0.8651026392961877
Weighted MatthewsCorrelation: 0.851066881955143
Weighted FMeasure: 0.8590250804037133
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8260214241918161
Mean absolute error: 0.019833052733614343
Coverage of cases: 90.6158357771261
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8651026392961877
Weighted FalsePositiveRate: 0.015389291061100782
Kappa statistic: 0.8518035636679705
Training time: 7.0
		
Time end:Sun Oct 08 06.11.13 EEST 2017