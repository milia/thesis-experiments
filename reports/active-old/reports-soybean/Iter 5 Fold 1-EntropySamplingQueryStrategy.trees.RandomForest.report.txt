Sun Oct 08 06.07.25 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.25 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 27.346875961834357
Incorrectly Classified Instances: 40.64327485380117
Correctly Classified Instances: 59.35672514619883
Weighted Precision: 0.5592884145975346
Weighted AreaUnderROC: 0.8908072844146633
Root mean squared error: 0.17283771021450756
Relative absolute error: 62.30528141769883
Root relative squared error: 77.40265406037433
Weighted TruePositiveRate: 0.5935672514619883
Weighted MatthewsCorrelation: 0.5283228544680237
Weighted FMeasure: 0.5439886588585962
Iteration time: 9.0
Weighted AreaUnderPRC: 0.672757631826561
Mean absolute error: 0.062132690610448134
Coverage of cases: 83.62573099415205
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 9.0
Weighted Recall: 0.5935672514619883
Weighted FalsePositiveRate: 0.04827940721629385
Kappa statistic: 0.5451866592679053
Training time: 6.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 25.8694983071714
Incorrectly Classified Instances: 30.994152046783626
Correctly Classified Instances: 69.00584795321637
Weighted Precision: 0.6690610540530303
Weighted AreaUnderROC: 0.9081491041158776
Root mean squared error: 0.16197396895256222
Relative absolute error: 57.90754377862968
Root relative squared error: 72.53749815396853
Weighted TruePositiveRate: 0.6900584795321637
Weighted MatthewsCorrelation: 0.64188938368596
Weighted FMeasure: 0.6527546689281657
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7516236641265251
Mean absolute error: 0.057747135070102124
Coverage of cases: 85.96491228070175
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6900584795321637
Weighted FalsePositiveRate: 0.03230673857191634
Kappa statistic: 0.6576576576576576
Training time: 11.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 25.377039088950433
Incorrectly Classified Instances: 28.07017543859649
Correctly Classified Instances: 71.9298245614035
Weighted Precision: 0.7095632229736841
Weighted AreaUnderROC: 0.9288578127913621
Root mean squared error: 0.1592165706615849
Relative absolute error: 56.27907932967956
Root relative squared error: 71.30264063497977
Weighted TruePositiveRate: 0.7192982456140351
Weighted MatthewsCorrelation: 0.6801133406793687
Weighted FMeasure: 0.6875446596695439
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7663106666170925
Mean absolute error: 0.05612318160300505
Coverage of cases: 89.47368421052632
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7192982456140351
Weighted FalsePositiveRate: 0.030389502729734785
Kappa statistic: 0.6889406815791718
Training time: 10.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 25.02308402585405
Incorrectly Classified Instances: 22.514619883040936
Correctly Classified Instances: 77.48538011695906
Weighted Precision: 0.8012804077486878
Weighted AreaUnderROC: 0.965490214038311
Root mean squared error: 0.1424015757807289
Relative absolute error: 49.64013470390576
Root relative squared error: 63.7723092612619
Weighted TruePositiveRate: 0.7748538011695907
Weighted MatthewsCorrelation: 0.7540342767682799
Weighted FMeasure: 0.7589338067141675
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8522648233096999
Mean absolute error: 0.04950262740555743
Coverage of cases: 96.19883040935673
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7748538011695907
Weighted FalsePositiveRate: 0.02241556266794855
Kappa statistic: 0.7520549106007965
Training time: 14.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 26.1311172668513
Incorrectly Classified Instances: 19.005847953216374
Correctly Classified Instances: 80.99415204678363
Weighted Precision: 0.8073807723694094
Weighted AreaUnderROC: 0.9713558488256765
Root mean squared error: 0.13640169876720595
Relative absolute error: 47.731784317114844
Root relative squared error: 61.08535857031524
Weighted TruePositiveRate: 0.8099415204678363
Weighted MatthewsCorrelation: 0.7834250729035807
Weighted FMeasure: 0.7955777741039903
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8716735442481282
Mean absolute error: 0.047599563307926585
Coverage of cases: 96.78362573099415
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8099415204678363
Weighted FalsePositiveRate: 0.02254777348703663
Kappa statistic: 0.7905023984318309
Training time: 16.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 24.299784549092
Incorrectly Classified Instances: 14.619883040935672
Correctly Classified Instances: 85.38011695906432
Weighted Precision: 0.8728923474104029
Weighted AreaUnderROC: 0.9840545306646444
Root mean squared error: 0.12848562884378656
Relative absolute error: 44.6006343682002
Root relative squared error: 57.54027097895733
Weighted TruePositiveRate: 0.8538011695906432
Weighted MatthewsCorrelation: 0.8388539879422272
Weighted FMeasure: 0.8432833349081071
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9076248559220099
Mean absolute error: 0.04447708690457676
Coverage of cases: 98.83040935672514
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 87.0
Weighted Recall: 0.8538011695906432
Weighted FalsePositiveRate: 0.015302840311217364
Kappa statistic: 0.8392494547642325
Training time: 17.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 23.714989227454577
Incorrectly Classified Instances: 13.742690058479532
Correctly Classified Instances: 86.25730994152046
Weighted Precision: 0.8769614517790103
Weighted AreaUnderROC: 0.9874299447841142
Root mean squared error: 0.12080573640379227
Relative absolute error: 41.062367002773996
Root relative squared error: 54.10095176432529
Weighted TruePositiveRate: 0.8625730994152047
Weighted MatthewsCorrelation: 0.8521062975316079
Weighted FMeasure: 0.8601816373506245
Iteration time: 29.0
Weighted AreaUnderPRC: 0.92808623994009
Mean absolute error: 0.0409486208337916
Coverage of cases: 98.83040935672514
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8625730994152047
Weighted FalsePositiveRate: 0.01396212378759687
Kappa statistic: 0.8491879568036178
Training time: 27.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 21.72976300400123
Incorrectly Classified Instances: 14.619883040935672
Correctly Classified Instances: 85.38011695906432
Weighted Precision: 0.8720768144522276
Weighted AreaUnderROC: 0.9762202025493049
Root mean squared error: 0.12155647621218671
Relative absolute error: 39.337887191296
Root relative squared error: 54.437158796959466
Weighted TruePositiveRate: 0.8538011695906432
Weighted MatthewsCorrelation: 0.8433414261359647
Weighted FMeasure: 0.8524929156586586
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8993720463247044
Mean absolute error: 0.039228917974699955
Coverage of cases: 97.6608187134503
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 141.0
Weighted Recall: 0.8538011695906432
Weighted FalsePositiveRate: 0.015676084104327345
Kappa statistic: 0.8395255210728328
Training time: 23.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 19.990766389658347
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8772151910730699
Weighted AreaUnderROC: 0.981948867219051
Root mean squared error: 0.11888896241094347
Relative absolute error: 36.61749224952482
Root relative squared error: 53.242554635039866
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8372532748518414
Weighted FMeasure: 0.8447436643586168
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9165122994331903
Mean absolute error: 0.036516058752989064
Coverage of cases: 97.953216374269
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 166.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.014892872831018679
Kappa statistic: 0.8272678806924998
Training time: 24.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 19.559864573715014
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8654140693614376
Weighted AreaUnderROC: 0.9892221620460412
Root mean squared error: 0.1159367670338323
Relative absolute error: 36.13694178698425
Root relative squared error: 51.920460300362706
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8442122759232062
Weighted FMeasure: 0.8563867646969127
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9247925553502605
Mean absolute error: 0.03603683945516467
Coverage of cases: 99.70760233918129
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 194.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.017048214950626677
Kappa statistic: 0.8456475543938169
Training time: 27.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 20.206217297630044
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.862026961346071
Weighted AreaUnderROC: 0.9846325591053371
Root mean squared error: 0.1209182081406336
Relative absolute error: 38.77602147141968
Root relative squared error: 54.151320465273095
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8331438592693132
Weighted FMeasure: 0.8448134901189055
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9120768910969071
Mean absolute error: 0.03866860866956014
Coverage of cases: 98.53801169590643
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 219.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.019129853456982066
Kappa statistic: 0.8326022703740658
Training time: 24.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 18.867343798091728
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8977840434419381
Weighted AreaUnderROC: 0.988389521468761
Root mean squared error: 0.1112615290099177
Relative absolute error: 33.82917464812168
Root relative squared error: 49.826728377127615
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8778022514995234
Weighted FMeasure: 0.8867040338315416
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9411983561670096
Mean absolute error: 0.03373546502305793
Coverage of cases: 99.41520467836257
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 248.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.013117244607351744
Kappa statistic: 0.8780119209649411
Training time: 28.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 18.744228993536485
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.8699106931345328
Weighted AreaUnderROC: 0.9899830445280606
Root mean squared error: 0.11188650446865525
Relative absolute error: 33.816178381063516
Root relative squared error: 50.10661382092833
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8509613394387496
Weighted FMeasure: 0.8606089066989682
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9429847527087882
Mean absolute error: 0.03372250475673954
Coverage of cases: 99.12280701754386
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 282.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.015628992432812225
Kappa statistic: 0.8519800908894178
Training time: 33.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 16.343490304709157
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8962518978428852
Weighted AreaUnderROC: 0.9890376719359157
Root mean squared error: 0.10579706936767552
Relative absolute error: 29.63227434686635
Root relative squared error: 47.37955594704618
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8765106062072906
Weighted FMeasure: 0.8842739672093798
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9441872261328416
Mean absolute error: 0.029550190484409915
Coverage of cases: 99.12280701754386
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 315.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.012554071420584708
Kappa statistic: 0.8780988828544897
Training time: 33.0
		
Time end:Sun Oct 08 06.07.26 EEST 2017