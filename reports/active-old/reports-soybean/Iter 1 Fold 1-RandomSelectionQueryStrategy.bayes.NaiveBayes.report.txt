Sun Oct 08 06.15.03 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.15.03 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.233918128654976
Incorrectly Classified Instances: 35.08771929824562
Correctly Classified Instances: 64.91228070175438
Weighted Precision: 0.6847426719889085
Weighted AreaUnderROC: 0.9557173751019745
Root mean squared error: 0.16956057751913214
Relative absolute error: 37.25219399592053
Root relative squared error: 75.93504165093388
Weighted TruePositiveRate: 0.6491228070175439
Weighted MatthewsCorrelation: 0.6086345205325748
Weighted FMeasure: 0.6042990932908405
Iteration time: 10.0
Weighted AreaUnderPRC: 0.785802612828573
Mean absolute error: 0.037149002322801966
Coverage of cases: 80.4093567251462
Instances selection time: 10.0
Test time: 75.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6491228070175439
Weighted FalsePositiveRate: 0.04372570911521267
Kappa statistic: 0.6088187355237197
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 10.080024622960925
Incorrectly Classified Instances: 26.608187134502923
Correctly Classified Instances: 73.39181286549707
Weighted Precision: 0.736648536389657
Weighted AreaUnderROC: 0.9666870303763213
Root mean squared error: 0.14584487172263638
Relative absolute error: 30.009847548829253
Root relative squared error: 65.31433527102666
Weighted TruePositiveRate: 0.7339181286549707
Weighted MatthewsCorrelation: 0.6932053682009927
Weighted FMeasure: 0.6951938611242493
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8300203089148046
Mean absolute error: 0.029926717777226112
Coverage of cases: 86.25730994152046
Instances selection time: 2.0
Test time: 71.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7339181286549707
Weighted FalsePositiveRate: 0.03160258696096459
Kappa statistic: 0.704920830567934
Training time: 1.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 9.387503847337648
Incorrectly Classified Instances: 21.05263157894737
Correctly Classified Instances: 78.94736842105263
Weighted Precision: 0.7939526687890648
Weighted AreaUnderROC: 0.9736272083202807
Root mean squared error: 0.13100573755315978
Relative absolute error: 24.874998999729875
Root relative squared error: 58.668862085516544
Weighted TruePositiveRate: 0.7894736842105263
Weighted MatthewsCorrelation: 0.7630219363289388
Weighted FMeasure: 0.7691283604658188
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8800742966922427
Mean absolute error: 0.024806093185326412
Coverage of cases: 89.76608187134502
Instances selection time: 5.0
Test time: 75.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7894736842105263
Weighted FalsePositiveRate: 0.02469552588052111
Kappa statistic: 0.7676104190260475
Training time: 1.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.81809787626963
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.8550527862028713
Weighted AreaUnderROC: 0.9811402206501204
Root mean squared error: 0.1221577372518898
Relative absolute error: 22.248751108291447
Root relative squared error: 54.70642411063643
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7999333575316173
Weighted FMeasure: 0.8001984519254318
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9125257261961932
Mean absolute error: 0.02218712021879499
Coverage of cases: 91.81286549707602
Instances selection time: 4.0
Test time: 76.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.021820435918982615
Kappa statistic: 0.7936846074088039
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 8.510310864881506
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8686730220051129
Weighted AreaUnderROC: 0.9832838327015518
Root mean squared error: 0.1132087880359933
Relative absolute error: 19.685205529426497
Root relative squared error: 50.69877774976864
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8336749714611889
Weighted FMeasure: 0.8378067087056479
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9226854749062802
Mean absolute error: 0.019630675874220502
Coverage of cases: 92.98245614035088
Instances selection time: 5.0
Test time: 70.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.018355587257558176
Kappa statistic: 0.8294248287284499
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 8.033240997229923
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.875693284720037
Weighted AreaUnderROC: 0.9842862743595916
Root mean squared error: 0.11155458141804094
Relative absolute error: 18.822565271928895
Root relative squared error: 49.95796729564471
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8360228093491266
Weighted FMeasure: 0.8400354212993627
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9275888542779167
Mean absolute error: 0.018770425201923718
Coverage of cases: 92.69005847953217
Instances selection time: 5.0
Test time: 71.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.018941170787418814
Kappa statistic: 0.8295291030668962
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.848568790397054
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8797017757323002
Weighted AreaUnderROC: 0.9840347809205415
Root mean squared error: 0.11171783284229202
Relative absolute error: 18.199256239572673
Root relative squared error: 50.03107688209137
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.837114555574829
Weighted FMeasure: 0.840745082732738
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9309563353766774
Mean absolute error: 0.01814884278738565
Coverage of cases: 92.69005847953217
Instances selection time: 5.0
Test time: 61.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.019931095502012164
Kappa statistic: 0.8293718406114976
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.771622037550025
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.9054708470384757
Weighted AreaUnderROC: 0.9874922584713568
Root mean squared error: 0.10687307850167507
Relative absolute error: 17.10214022592353
Root relative squared error: 47.861429738717014
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8632477628316482
Weighted FMeasure: 0.867089912179771
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9376016395299543
Mean absolute error: 0.017054765876267385
Coverage of cases: 92.98245614035088
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.0168638041973489
Kappa statistic: 0.8522525568421941
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.6792859341335875
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.9062975501571994
Weighted AreaUnderROC: 0.9885647661025487
Root mean squared error: 0.10426579869564666
Relative absolute error: 16.044277829147198
Root relative squared error: 46.69380042556451
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8671686594111054
Weighted FMeasure: 0.8703384111299971
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9454331850832395
Mean absolute error: 0.015999833846241117
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.015604759288760971
Kappa statistic: 0.8587864341885475
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.602339181286558
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.9116386634362065
Weighted AreaUnderROC: 0.9896172972054348
Root mean squared error: 0.10601706800767714
Relative absolute error: 16.16370668986048
Root relative squared error: 47.478078882837586
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8689209878897962
Weighted FMeasure: 0.8710073514643745
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9456261231909122
Mean absolute error: 0.016118931879085384
Coverage of cases: 92.69005847953217
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.01570633608461488
Kappa statistic: 0.8587718557310584
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.41766697445369
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.9100895545922717
Weighted AreaUnderROC: 0.990234699199946
Root mean squared error: 0.10505351827043796
Relative absolute error: 15.530342890161533
Root relative squared error: 47.04656826580311
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8727102308324293
Weighted FMeasure: 0.8763207323135677
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9452118965153218
Mean absolute error: 0.01548732254974571
Coverage of cases: 92.39766081871345
Instances selection time: 1.0
Test time: 39.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.014718705771128507
Kappa statistic: 0.8653518063705732
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.356109572176068
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.9159182639658022
Weighted AreaUnderROC: 0.9904884930711924
Root mean squared error: 0.10151888674843466
Relative absolute error: 14.75357770083784
Root relative squared error: 45.463639050940415
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8839355770653747
Weighted FMeasure: 0.8881220884587715
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9457480031567537
Mean absolute error: 0.014712709064547561
Coverage of cases: 92.98245614035088
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.013194617287867302
Kappa statistic: 0.8782496276102414
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.309941520467851
Incorrectly Classified Instances: 11.403508771929825
Correctly Classified Instances: 88.59649122807018
Weighted Precision: 0.9146201569350537
Weighted AreaUnderROC: 0.9915225728104436
Root mean squared error: 0.10020270423352509
Relative absolute error: 14.392101152729033
Root relative squared error: 44.87420738261166
Weighted TruePositiveRate: 0.8859649122807017
Weighted MatthewsCorrelation: 0.881165632598247
Weighted FMeasure: 0.8853308401160549
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505748374704339
Mean absolute error: 0.014352233836516614
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8859649122807017
Weighted FalsePositiveRate: 0.013637644046683535
Kappa statistic: 0.8750468410402458
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 7.186826715912605
Incorrectly Classified Instances: 11.695906432748538
Correctly Classified Instances: 88.30409356725146
Weighted Precision: 0.9134969857447305
Weighted AreaUnderROC: 0.9919691772519752
Root mean squared error: 0.10010867351726255
Relative absolute error: 14.058319970256845
Root relative squared error: 44.83209720310925
Weighted TruePositiveRate: 0.8830409356725146
Weighted MatthewsCorrelation: 0.8782258851725167
Weighted FMeasure: 0.8816067943911527
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9554099963160458
Mean absolute error: 0.01401937725565792
Coverage of cases: 93.27485380116958
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8830409356725146
Weighted FalsePositiveRate: 0.013370760457035202
Kappa statistic: 0.871928099985957
Training time: 0.0
		
Time end:Sun Oct 08 06.15.05 EEST 2017