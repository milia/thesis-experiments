Sun Oct 08 06.18.56 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.56 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 8.087667849976867
Incorrectly Classified Instances: 32.551319648093845
Correctly Classified Instances: 67.44868035190616
Weighted Precision: 0.6602807796523198
Weighted AreaUnderROC: 0.8278201316176755
Root mean squared error: 0.1750490892659057
Relative absolute error: 36.818905904927036
Root relative squared error: 78.39298543828542
Weighted TruePositiveRate: 0.6744868035190615
Weighted MatthewsCorrelation: 0.6227346496940603
Weighted FMeasure: 0.6329635054854997
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5585610273072226
Mean absolute error: 0.03671691447582784
Coverage of cases: 69.79472140762464
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6744868035190615
Weighted FalsePositiveRate: 0.03384395597264357
Kappa statistic: 0.6409981599863421
Training time: 2.0
		
Time end:Sun Oct 08 06.18.56 EEST 2017