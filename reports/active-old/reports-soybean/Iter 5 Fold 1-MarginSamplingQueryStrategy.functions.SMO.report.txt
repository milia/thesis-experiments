Sun Oct 08 06.14.03 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.14.03 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.6719216132081478
Weighted AreaUnderROC: 0.8981308242887049
Root mean squared error: 0.21618784314934567
Relative absolute error: 95.5929667040771
Root relative squared error: 96.81633027063039
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6678648219952337
Weighted FMeasure: 0.6873342010573581
Iteration time: 732.0
Weighted AreaUnderPRC: 0.6339597954767437
Mean absolute error: 0.0953281662422938
Coverage of cases: 88.30409356725146
Instances selection time: 18.0
Test time: 27.0
Accumulative iteration time: 732.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.037606156860589726
Kappa statistic: 0.7006991676575506
Training time: 714.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 20.760233918128655
Correctly Classified Instances: 79.23976608187135
Weighted Precision: 0.7362445582718488
Weighted AreaUnderROC: 0.9069566159451878
Root mean squared error: 0.2160258020289671
Relative absolute error: 95.52375607931093
Root relative squared error: 96.74376269976491
Weighted TruePositiveRate: 0.7923976608187134
Weighted MatthewsCorrelation: 0.7389137725764189
Weighted FMeasure: 0.7493714016506627
Iteration time: 726.0
Weighted AreaUnderPRC: 0.6864745021254104
Mean absolute error: 0.0952591473367098
Coverage of cases: 88.30409356725146
Instances selection time: 17.0
Test time: 27.0
Accumulative iteration time: 1458.0
Weighted Recall: 0.7923976608187134
Weighted FalsePositiveRate: 0.029743553215429056
Kappa statistic: 0.7692132225749425
Training time: 709.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.7546962261960543
Weighted AreaUnderROC: 0.9141907860655686
Root mean squared error: 0.2158623037684923
Relative absolute error: 95.45641601197086
Root relative squared error: 96.6705425715921
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7565481835112811
Weighted FMeasure: 0.763541988868106
Iteration time: 732.0
Weighted AreaUnderPRC: 0.712933833400801
Mean absolute error: 0.0951919938069524
Coverage of cases: 88.30409356725146
Instances selection time: 16.0
Test time: 27.0
Accumulative iteration time: 2190.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.027511690264890774
Kappa statistic: 0.7856980100258242
Training time: 716.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 73.69959987688499
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.7713035933336962
Weighted AreaUnderROC: 0.9282287826644168
Root mean squared error: 0.21534751372465158
Relative absolute error: 95.36302175190981
Root relative squared error: 96.44000193536361
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.768543560076253
Weighted FMeasure: 0.776493889693572
Iteration time: 837.0
Weighted AreaUnderPRC: 0.7158929543187975
Mean absolute error: 0.09509885825675302
Coverage of cases: 91.2280701754386
Instances selection time: 17.0
Test time: 29.0
Accumulative iteration time: 3027.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.026398933306129784
Kappa statistic: 0.7955962014628731
Training time: 820.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.796272926381313
Weighted AreaUnderROC: 0.9405876625966604
Root mean squared error: 0.21482449391863018
Relative absolute error: 95.2233115468399
Root relative squared error: 96.20577572939295
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7811813517721422
Weighted FMeasure: 0.788228514070283
Iteration time: 947.0
Weighted AreaUnderPRC: 0.7299805843502876
Mean absolute error: 0.09495953506056139
Coverage of cases: 94.15204678362574
Instances selection time: 16.0
Test time: 33.0
Accumulative iteration time: 3974.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.02493335289318095
Kappa statistic: 0.8054220123460303
Training time: 931.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8472503393138312
Weighted AreaUnderROC: 0.9603444137769126
Root mean squared error: 0.21412375151411164
Relative absolute error: 94.9696772796183
Root relative squared error: 95.89195924886295
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8281416532112389
Weighted FMeasure: 0.8334208376457819
Iteration time: 1056.0
Weighted AreaUnderPRC: 0.7861934210499327
Mean absolute error: 0.09470660338133763
Coverage of cases: 97.07602339181287
Instances selection time: 17.0
Test time: 35.0
Accumulative iteration time: 5030.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.019480531650886024
Kappa statistic: 0.8448202520158432
Training time: 1039.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8592972495959128
Weighted AreaUnderROC: 0.964792157337286
Root mean squared error: 0.21402969181341233
Relative absolute error: 94.92996895530956
Root relative squared error: 95.84983608913574
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8483640745812365
Weighted FMeasure: 0.8558375507529048
Iteration time: 1077.0
Weighted AreaUnderPRC: 0.8018916925261539
Mean absolute error: 0.09466700505238711
Coverage of cases: 97.07602339181287
Instances selection time: 14.0
Test time: 36.0
Accumulative iteration time: 6107.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.01741655381752458
Kappa statistic: 0.8612104681999642
Training time: 1063.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 11.988304093567251
Correctly Classified Instances: 88.01169590643275
Weighted Precision: 0.8639955929208474
Weighted AreaUnderROC: 0.9643837193558784
Root mean squared error: 0.21401237993195515
Relative absolute error: 94.92274925998072
Root relative squared error: 95.84208323491254
Weighted TruePositiveRate: 0.8801169590643275
Weighted MatthewsCorrelation: 0.8552000265702547
Weighted FMeasure: 0.8634029460956265
Iteration time: 1938.0
Weighted AreaUnderPRC: 0.8103841874617923
Mean absolute error: 0.0946598053562143
Coverage of cases: 97.07602339181287
Instances selection time: 25.0
Test time: 67.0
Accumulative iteration time: 8045.0
Weighted Recall: 0.8801169590643275
Weighted FalsePositiveRate: 0.016383499800896387
Kappa statistic: 0.867816742081448
Training time: 1913.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 78.9627577716216
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8613951241222745
Weighted AreaUnderROC: 0.9645111973944526
Root mean squared error: 0.21401926550707262
Relative absolute error: 94.92635910764514
Root relative squared error: 95.84516683158922
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8521585595074446
Weighted FMeasure: 0.8606114122040046
Iteration time: 1526.0
Weighted AreaUnderPRC: 0.8100799595680752
Mean absolute error: 0.0946634052043007
Coverage of cases: 97.07602339181287
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 9571.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.016950853378316547
Kappa statistic: 0.864516129032258
Training time: 1516.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8619536334824555
Weighted AreaUnderROC: 0.9649703777934578
Root mean squared error: 0.21403369990451204
Relative absolute error: 94.93177387914182
Root relative squared error: 95.85163104978666
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8516819839104101
Weighted FMeasure: 0.8596166945657043
Iteration time: 1118.0
Weighted AreaUnderPRC: 0.8147865213111647
Mean absolute error: 0.09466880497643033
Coverage of cases: 97.07602339181287
Instances selection time: 9.0
Test time: 35.0
Accumulative iteration time: 10689.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.01758193695367723
Kappa statistic: 0.8643920583820321
Training time: 1109.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8585741699238885
Weighted AreaUnderROC: 0.9647496304020101
Root mean squared error: 0.2140335277794428
Relative absolute error: 94.93177387914177
Root relative squared error: 95.85155396627766
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8480257957634519
Weighted FMeasure: 0.8563098100136244
Iteration time: 1107.0
Weighted AreaUnderPRC: 0.8133462580546121
Mean absolute error: 0.0946688049764303
Coverage of cases: 97.07602339181287
Instances selection time: 7.0
Test time: 35.0
Accumulative iteration time: 11796.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.018059088746618497
Kappa statistic: 0.8611606763531311
Training time: 1100.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.878602560295164
Weighted AreaUnderROC: 0.970883245806949
Root mean squared error: 0.21376984176398942
Relative absolute error: 94.82167352537675
Root relative squared error: 95.73346632551129
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8773817730081087
Weighted FMeasure: 0.8873350218079713
Iteration time: 1108.0
Weighted AreaUnderPRC: 0.8546447895324841
Mean absolute error: 0.09455900960979483
Coverage of cases: 97.07602339181287
Instances selection time: 5.0
Test time: 34.0
Accumulative iteration time: 12904.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.013655303880138222
Kappa statistic: 0.8906608492872456
Training time: 1103.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 9.649122807017545
Correctly Classified Instances: 90.35087719298245
Weighted Precision: 0.8817354632880108
Weighted AreaUnderROC: 0.9716225967263513
Root mean squared error: 0.2137592550581473
Relative absolute error: 94.81625875388008
Root relative squared error: 95.72872523556674
Weighted TruePositiveRate: 0.9035087719298246
Weighted MatthewsCorrelation: 0.8805738159564036
Weighted FMeasure: 0.890077338835581
Iteration time: 1126.0
Weighted AreaUnderPRC: 0.8611742987942578
Mean absolute error: 0.09455360983766518
Coverage of cases: 97.07602339181287
Instances selection time: 4.0
Test time: 33.0
Accumulative iteration time: 14030.0
Weighted Recall: 0.9035087719298246
Weighted FalsePositiveRate: 0.013578607306929287
Kappa statistic: 0.8938397719897283
Training time: 1122.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 6.432748538011696
Correctly Classified Instances: 93.5672514619883
Weighted Precision: 0.9363832554192175
Weighted AreaUnderROC: 0.988751440489148
Root mean squared error: 0.21298535014353645
Relative absolute error: 94.48956754024931
Root relative squared error: 95.3821440739273
Weighted TruePositiveRate: 0.935672514619883
Weighted MatthewsCorrelation: 0.9269987226039077
Weighted FMeasure: 0.9351798402847215
Iteration time: 1283.0
Weighted AreaUnderPRC: 0.9171556825537817
Mean absolute error: 0.09422782358584503
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 36.0
Accumulative iteration time: 15313.0
Weighted Recall: 0.935672514619883
Weighted FalsePositiveRate: 0.008795549627368603
Kappa statistic: 0.9294766046790641
Training time: 1281.0
		
Time end:Sun Oct 08 06.14.19 EEST 2017