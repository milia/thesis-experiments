Sun Oct 08 06.03.29 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.03.29 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.633733456448155
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.7254820365545388
Weighted AreaUnderROC: 0.9646798455675958
Root mean squared error: 0.14967014731037812
Relative absolute error: 30.808229080290534
Root relative squared error: 67.02742486609303
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6871652165537357
Weighted FMeasure: 0.6916262086195485
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8281349316969276
Mean absolute error: 0.030722887725497756
Coverage of cases: 87.42690058479532
Instances selection time: 27.0
Test time: 45.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.034696928779716606
Kappa statistic: 0.7013695769781988
Training time: 1.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 8.37180670975686
Incorrectly Classified Instances: 21.05263157894737
Correctly Classified Instances: 78.94736842105263
Weighted Precision: 0.8342364216641823
Weighted AreaUnderROC: 0.9820113001661565
Root mean squared error: 0.12838376877455443
Relative absolute error: 23.663228143062895
Root relative squared error: 57.494654546689425
Weighted TruePositiveRate: 0.7894736842105263
Weighted MatthewsCorrelation: 0.7700486945722378
Weighted FMeasure: 0.7690686669670449
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9056850007969689
Mean absolute error: 0.023597679034633567
Coverage of cases: 90.05847953216374
Instances selection time: 28.0
Test time: 39.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7894736842105263
Weighted FalsePositiveRate: 0.028407645834661384
Kappa statistic: 0.7672831747171844
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.110187750076957
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8749860896059726
Weighted AreaUnderROC: 0.9867573745321578
Root mean squared error: 0.11004044744799096
Relative absolute error: 18.983624275560736
Root relative squared error: 49.27988617700812
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.831048345103043
Weighted FMeasure: 0.8347836718033043
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9310426447399436
Mean absolute error: 0.018931038058731094
Coverage of cases: 95.02923976608187
Instances selection time: 24.0
Test time: 41.0
Accumulative iteration time: 81.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.019366625697871738
Kappa statistic: 0.8262522108907537
Training time: 1.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.033240997229926
Incorrectly Classified Instances: 13.157894736842104
Correctly Classified Instances: 86.84210526315789
Weighted Precision: 0.8926973320531845
Weighted AreaUnderROC: 0.986067524132063
Root mean squared error: 0.1010779070626548
Relative absolute error: 17.806251299108723
Root relative squared error: 45.26615322435947
Weighted TruePositiveRate: 0.868421052631579
Weighted MatthewsCorrelation: 0.8567192938748632
Weighted FMeasure: 0.8635061772712023
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9454645236107116
Mean absolute error: 0.017756926503266473
Coverage of cases: 94.44444444444444
Instances selection time: 21.0
Test time: 40.0
Accumulative iteration time: 103.0
Weighted Recall: 0.868421052631579
Weighted FalsePositiveRate: 0.01800922240047161
Kappa statistic: 0.8552442224667739
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.787011388119436
Incorrectly Classified Instances: 10.526315789473685
Correctly Classified Instances: 89.47368421052632
Weighted Precision: 0.908138694188496
Weighted AreaUnderROC: 0.9902997288328087
Root mean squared error: 0.09477243393028198
Relative absolute error: 15.096495811711309
Root relative squared error: 42.44234611104889
Weighted TruePositiveRate: 0.8947368421052632
Weighted MatthewsCorrelation: 0.8828552320586994
Weighted FMeasure: 0.88742453938985
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9571795827746281
Mean absolute error: 0.015054677263756562
Coverage of cases: 95.32163742690058
Instances selection time: 19.0
Test time: 43.0
Accumulative iteration time: 123.0
Weighted Recall: 0.8947368421052632
Weighted FalsePositiveRate: 0.013157534869437635
Kappa statistic: 0.8843932806880815
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.402277623884278
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9284798195394045
Weighted AreaUnderROC: 0.9900974739495488
Root mean squared error: 0.08516432805689964
Relative absolute error: 12.31493474037176
Root relative squared error: 38.13950679334517
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9103714748118588
Weighted FMeasure: 0.9169827563954995
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9575219733320165
Mean absolute error: 0.01228082134773926
Coverage of cases: 95.02923976608187
Instances selection time: 17.0
Test time: 46.0
Accumulative iteration time: 141.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.011194392560702426
Kappa statistic: 0.910192444761226
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.479224376731305
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9907892346110129
Root mean squared error: 0.0835642597079525
Relative absolute error: 11.756172570296002
Root relative squared error: 37.42294131274027
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9602261540792549
Mean absolute error: 0.011723606995309135
Coverage of cases: 95.6140350877193
Instances selection time: 14.0
Test time: 40.0
Accumulative iteration time: 156.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.417666974453682
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9923123228273654
Root mean squared error: 0.08287161931313053
Relative absolute error: 11.22368774775201
Root relative squared error: 37.11275319000879
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9633783154293868
Mean absolute error: 0.011192597199974402
Coverage of cases: 95.6140350877193
Instances selection time: 12.0
Test time: 44.0
Accumulative iteration time: 169.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.248384118190215
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9930078342852245
Root mean squared error: 0.08295361962091168
Relative absolute error: 10.892515868537057
Root relative squared error: 37.14947574001336
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9651213259026288
Mean absolute error: 0.01086234269438608
Coverage of cases: 95.6140350877193
Instances selection time: 15.0
Test time: 42.0
Accumulative iteration time: 185.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 1.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.156048014773778
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9928652535307224
Root mean squared error: 0.0837859423531063
Relative absolute error: 10.914631378294454
Root relative squared error: 37.52221840379136
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9635695086241817
Mean absolute error: 0.010884396942343595
Coverage of cases: 95.6140350877193
Instances selection time: 9.0
Test time: 48.0
Accumulative iteration time: 195.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.079101261926751
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9351142507352391
Weighted AreaUnderROC: 0.9929516236322409
Root mean squared error: 0.0848375482273698
Relative absolute error: 10.95850033767117
Root relative squared error: 37.99316357884867
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.916574750862787
Weighted FMeasure: 0.9227801257075224
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9630421951758468
Mean absolute error: 0.010928144381057218
Coverage of cases: 95.32163742690058
Instances selection time: 12.0
Test time: 50.0
Accumulative iteration time: 207.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.01136190589675002
Kappa statistic: 0.9165524878469941
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 6.909818405663284
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9351142507352391
Weighted AreaUnderROC: 0.9932665915500763
Root mean squared error: 0.08525310944398039
Relative absolute error: 10.857809218511372
Root relative squared error: 38.179266143215536
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.916574750862787
Weighted FMeasure: 0.9227801257075224
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9640019303610816
Mean absolute error: 0.010827732184665175
Coverage of cases: 94.73684210526316
Instances selection time: 6.0
Test time: 39.0
Accumulative iteration time: 214.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.01136190589675002
Kappa statistic: 0.9165524878469941
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 6.848261003385658
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9307731133389027
Weighted AreaUnderROC: 0.9937189644439698
Root mean squared error: 0.08623105547501386
Relative absolute error: 10.858123165239283
Root relative squared error: 38.61722391432842
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9130034816319632
Weighted FMeasure: 0.9197643567302259
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9645361554430247
Mean absolute error: 0.01082804526173456
Coverage of cases: 94.44444444444444
Instances selection time: 4.0
Test time: 41.0
Accumulative iteration time: 218.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.011449977481333971
Kappa statistic: 0.9133722348349813
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 6.802092951677439
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9307731133389027
Weighted AreaUnderROC: 0.993423513490624
Root mean squared error: 0.08678943817928353
Relative absolute error: 10.870776869737822
Root relative squared error: 38.867286838896455
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9130034816319632
Weighted FMeasure: 0.9197643567302259
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9628235572549654
Mean absolute error: 0.010840663914420085
Coverage of cases: 94.15204678362574
Instances selection time: 2.0
Test time: 40.0
Accumulative iteration time: 221.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.011449977481333971
Kappa statistic: 0.9133722348349813
Training time: 1.0
		
Time end:Sun Oct 08 06.03.31 EEST 2017