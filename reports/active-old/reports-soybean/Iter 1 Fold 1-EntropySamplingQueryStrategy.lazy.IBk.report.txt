Sun Oct 08 06.06.54 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.06.54 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 70.37550015389316
Incorrectly Classified Instances: 26.608187134502923
Correctly Classified Instances: 73.39181286549707
Weighted Precision: 0.7034989758027617
Weighted AreaUnderROC: 0.8697447464544567
Root mean squared error: 0.15140226405681603
Relative absolute error: 41.958683462924135
Root relative squared error: 67.80312614766127
Weighted TruePositiveRate: 0.7339181286549707
Weighted MatthewsCorrelation: 0.6882013514419414
Weighted FMeasure: 0.7043088410950753
Iteration time: 128.0
Weighted AreaUnderPRC: 0.6717898364016908
Mean absolute error: 0.041842454422861
Coverage of cases: 96.19883040935673
Instances selection time: 127.0
Test time: 56.0
Accumulative iteration time: 128.0
Weighted Recall: 0.7339181286549707
Weighted FalsePositiveRate: 0.02993761517159684
Kappa statistic: 0.7062909344859477
Training time: 1.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 63.881194213604
Incorrectly Classified Instances: 19.5906432748538
Correctly Classified Instances: 80.4093567251462
Weighted Precision: 0.7932585399076425
Weighted AreaUnderROC: 0.9047516757078765
Root mean squared error: 0.13132970524010903
Relative absolute error: 33.069780317663515
Root relative squared error: 58.813945926123736
Weighted TruePositiveRate: 0.804093567251462
Weighted MatthewsCorrelation: 0.7733897198269397
Weighted FMeasure: 0.7896067061997255
Iteration time: 39.0
Weighted AreaUnderPRC: 0.763387692104671
Mean absolute error: 0.03297817427800269
Coverage of cases: 97.07602339181287
Instances selection time: 37.0
Test time: 40.0
Accumulative iteration time: 167.0
Weighted Recall: 0.804093567251462
Weighted FalsePositiveRate: 0.02404523634853822
Kappa statistic: 0.7845983192013386
Training time: 2.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 56.94059710680211
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8637650742023028
Weighted AreaUnderROC: 0.9308363566734911
Root mean squared error: 0.11668523454470948
Relative absolute error: 27.495634638339858
Root relative squared error: 52.25564972024079
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8366696216036323
Weighted FMeasure: 0.8503853302967048
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8183376741436267
Mean absolute error: 0.027419469445436113
Coverage of cases: 97.6608187134503
Instances selection time: 32.0
Test time: 59.0
Accumulative iteration time: 199.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.017708100428119974
Kappa statistic: 0.8366395054790672
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 50.307787011388
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8704031841338342
Weighted AreaUnderROC: 0.9431054363800396
Root mean squared error: 0.11149810682662539
Relative absolute error: 25.00878044071376
Root relative squared error: 49.93267603682675
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8453383859503361
Weighted FMeasure: 0.8600629713811965
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8363122402619548
Mean absolute error: 0.024939504040601197
Coverage of cases: 97.6608187134503
Instances selection time: 31.0
Test time: 57.0
Accumulative iteration time: 230.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.01712305553426088
Kappa statistic: 0.8463396141641628
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 43.93659587565411
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8583733204693162
Weighted AreaUnderROC: 0.9498735249542078
Root mean squared error: 0.11053387579579636
Relative absolute error: 23.900912738923875
Root relative squared error: 49.50086031315794
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8335406622490533
Weighted FMeasure: 0.8487070291990925
Iteration time: 36.0
Weighted AreaUnderPRC: 0.838988799017858
Mean absolute error: 0.02383470522441183
Coverage of cases: 96.19883040935673
Instances selection time: 36.0
Test time: 62.0
Accumulative iteration time: 266.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.018909466087316962
Kappa statistic: 0.8365078174796595
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 36.88827331486622
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8655667384253555
Weighted AreaUnderROC: 0.9475445185261178
Root mean squared error: 0.10945843406609222
Relative absolute error: 22.479392145482944
Root relative squared error: 49.019240624589585
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8429745870499059
Weighted FMeasure: 0.857032734767361
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8437833500946534
Mean absolute error: 0.02241712236114662
Coverage of cases: 95.32163742690058
Instances selection time: 15.0
Test time: 50.0
Accumulative iteration time: 282.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.017534885765368217
Kappa statistic: 0.8461307738452309
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 30.101569713758096
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8577422241508929
Weighted AreaUnderROC: 0.94365083617155
Root mean squared error: 0.11216251722444387
Relative absolute error: 22.38002778273065
Root relative squared error: 50.23022179875934
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8317315221021236
Weighted FMeasure: 0.8460030444017093
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8393804842616938
Mean absolute error: 0.022318033245936578
Coverage of cases: 94.44444444444444
Instances selection time: 18.0
Test time: 35.0
Accumulative iteration time: 301.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.01858570744079357
Kappa statistic: 0.833242690771337
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 26.438904278239445
Incorrectly Classified Instances: 13.157894736842104
Correctly Classified Instances: 86.84210526315789
Weighted Precision: 0.8790060502284611
Weighted AreaUnderROC: 0.9471303384776975
Root mean squared error: 0.1093944216272388
Relative absolute error: 21.078174976171887
Root relative squared error: 48.990573659180335
Weighted TruePositiveRate: 0.868421052631579
Weighted MatthewsCorrelation: 0.8549318189991973
Weighted FMeasure: 0.8658090362772214
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8518157381073834
Mean absolute error: 0.021019786679839187
Coverage of cases: 94.73684210526316
Instances selection time: 26.0
Test time: 35.0
Accumulative iteration time: 328.0
Weighted Recall: 0.868421052631579
Weighted FalsePositiveRate: 0.016103430386544416
Kappa statistic: 0.8557137901615368
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 23.176361957525476
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.885892925536389
Weighted AreaUnderROC: 0.9506365957206663
Root mean squared error: 0.10657119169665046
Relative absolute error: 19.95708676202196
Root relative squared error: 47.726234474294046
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8636648762654809
Weighted FMeasure: 0.8741788823619311
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8581287568891345
Mean absolute error: 0.01990180397320767
Coverage of cases: 95.32163742690058
Instances selection time: 31.0
Test time: 74.0
Accumulative iteration time: 359.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.015839215632792572
Kappa statistic: 0.8652317911862116
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 20.42166820560177
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.8792437372785249
Weighted AreaUnderROC: 0.9520450174194379
Root mean squared error: 0.10583235526362211
Relative absolute error: 19.193564237851973
Root relative squared error: 47.395358181372096
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8568171549081777
Weighted FMeasure: 0.867553050521993
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8615611014370703
Mean absolute error: 0.019140396469880253
Coverage of cases: 94.73684210526316
Instances selection time: 13.0
Test time: 39.0
Accumulative iteration time: 372.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.016359015307175963
Kappa statistic: 0.8587678792656831
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 16.89750692520782
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.8792437372785249
Weighted AreaUnderROC: 0.9537247575565269
Root mean squared error: 0.10609968427980597
Relative absolute error: 18.796372512220294
Root relative squared error: 47.5150772828014
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8568171549081777
Weighted FMeasure: 0.867553050521993
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8626638659848578
Mean absolute error: 0.01874430499833619
Coverage of cases: 91.2280701754386
Instances selection time: 12.0
Test time: 73.0
Accumulative iteration time: 384.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.016359015307175963
Kappa statistic: 0.8587678792656831
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 13.881194213604196
Incorrectly Classified Instances: 11.988304093567251
Correctly Classified Instances: 88.01169590643275
Weighted Precision: 0.8887196577193576
Weighted AreaUnderROC: 0.954564845822838
Root mean squared error: 0.10427923345736856
Relative absolute error: 17.669298623514816
Root relative squared error: 46.69981697260539
Weighted TruePositiveRate: 0.8801169590643275
Weighted MatthewsCorrelation: 0.8670497596072868
Weighted FMeasure: 0.8780280994251551
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8647634038714346
Mean absolute error: 0.01762035319796507
Coverage of cases: 91.2280701754386
Instances selection time: 9.0
Test time: 44.0
Accumulative iteration time: 393.0
Weighted Recall: 0.8801169590643275
Weighted FalsePositiveRate: 0.015728470367816817
Kappa statistic: 0.8684442609723604
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 11.234225915666388
Incorrectly Classified Instances: 11.695906432748538
Correctly Classified Instances: 88.30409356725146
Weighted Precision: 0.8904234950573623
Weighted AreaUnderROC: 0.9566468287912435
Root mean squared error: 0.10206941188593079
Relative absolute error: 16.76173985005322
Root relative squared error: 45.71018308763396
Weighted TruePositiveRate: 0.8830409356725146
Weighted MatthewsCorrelation: 0.8696715952306705
Weighted FMeasure: 0.8805054668479051
Iteration time: 5.0
Weighted AreaUnderPRC: 0.870027558246277
Mean absolute error: 0.016715308437726353
Coverage of cases: 91.52046783625731
Instances selection time: 5.0
Test time: 47.0
Accumulative iteration time: 398.0
Weighted Recall: 0.8830409356725146
Weighted FalsePositiveRate: 0.015285443609000585
Kappa statistic: 0.871695069451609
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 8.618036318867347
Incorrectly Classified Instances: 11.988304093567251
Correctly Classified Instances: 88.01169590643275
Weighted Precision: 0.887765334504465
Weighted AreaUnderROC: 0.9565485081984622
Root mean squared error: 0.10239629314314767
Relative absolute error: 16.68715656401748
Root relative squared error: 45.85657172492716
Weighted TruePositiveRate: 0.8801169590643275
Weighted MatthewsCorrelation: 0.866300824045669
Weighted FMeasure: 0.8767139367405855
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8699960901169557
Mean absolute error: 0.01664093175359099
Coverage of cases: 91.52046783625731
Instances selection time: 6.0
Test time: 53.0
Accumulative iteration time: 404.0
Weighted Recall: 0.8801169590643275
Weighted FalsePositiveRate: 0.015373515193584536
Kappa statistic: 0.8684849792250912
Training time: 0.0
		
Time end:Sun Oct 08 06.06.56 EEST 2017