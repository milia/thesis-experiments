Sun Oct 08 06.07.43 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.43 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.556786703601121
Incorrectly Classified Instances: 35.08771929824562
Correctly Classified Instances: 64.91228070175438
Weighted Precision: 0.6182756846502959
Weighted AreaUnderROC: 0.8648904275912963
Root mean squared error: 0.17665014594757888
Relative absolute error: 42.397150539748694
Root relative squared error: 79.10999352817967
Weighted TruePositiveRate: 0.6491228070175439
Weighted MatthewsCorrelation: 0.598801853933033
Weighted FMeasure: 0.6078153912539709
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5649928276621184
Mean absolute error: 0.042279706909445054
Coverage of cases: 76.90058479532163
Instances selection time: 9.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6491228070175439
Weighted FalsePositiveRate: 0.02428241829333246
Kappa statistic: 0.6167959886831564
Training time: 9.0
		
Time end:Sun Oct 08 06.07.43 EEST 2017