Sun Oct 08 06.12.31 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.12.31 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 78.9473684210522
Incorrectly Classified Instances: 21.700879765395893
Correctly Classified Instances: 78.2991202346041
Weighted Precision: 0.7863828894525563
Weighted AreaUnderROC: 0.9596045607530308
Root mean squared error: 0.21376708362492464
Relative absolute error: 94.81553890155992
Root relative squared error: 95.73223113624839
Weighted TruePositiveRate: 0.782991202346041
Weighted MatthewsCorrelation: 0.7564806521918884
Weighted FMeasure: 0.7678219392073422
Iteration time: 1307.0
Weighted AreaUnderPRC: 0.7276845667973778
Mean absolute error: 0.09455289197939576
Coverage of cases: 100.0
Instances selection time: 28.0
Test time: 40.0
Accumulative iteration time: 1307.0
Weighted Recall: 0.782991202346041
Weighted FalsePositiveRate: 0.02444058387445624
Kappa statistic: 0.7607856892318484
Training time: 1279.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 78.96280290168194
Incorrectly Classified Instances: 21.114369501466275
Correctly Classified Instances: 78.88563049853373
Weighted Precision: 0.8058138143800676
Weighted AreaUnderROC: 0.9606720165238393
Root mean squared error: 0.21361102163250847
Relative absolute error: 94.75580174504856
Root relative squared error: 95.66234122393718
Weighted TruePositiveRate: 0.7888563049853372
Weighted MatthewsCorrelation: 0.7667048317263846
Weighted FMeasure: 0.7795767325482755
Iteration time: 1293.0
Weighted AreaUnderPRC: 0.7463426506340011
Mean absolute error: 0.09449332029977224
Coverage of cases: 100.0
Instances selection time: 26.0
Test time: 39.0
Accumulative iteration time: 2600.0
Weighted Recall: 0.7888563049853372
Weighted FalsePositiveRate: 0.02464835591001145
Kappa statistic: 0.7678912439259581
Training time: 1267.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 78.96280290168194
Incorrectly Classified Instances: 17.88856304985337
Correctly Classified Instances: 82.11143695014663
Weighted Precision: 0.8422969240610853
Weighted AreaUnderROC: 0.9678416212139818
Root mean squared error: 0.2135217497602764
Relative absolute error: 94.71778719090497
Root relative squared error: 95.62236221799499
Weighted TruePositiveRate: 0.8211143695014663
Weighted MatthewsCorrelation: 0.8043956492439996
Weighted FMeasure: 0.8117090538265892
Iteration time: 1283.0
Weighted AreaUnderPRC: 0.7931632032669922
Mean absolute error: 0.09445541104910275
Coverage of cases: 100.0
Instances selection time: 24.0
Test time: 39.0
Accumulative iteration time: 3883.0
Weighted Recall: 0.8211143695014663
Weighted FalsePositiveRate: 0.020799737933314303
Kappa statistic: 0.8035454562626321
Training time: 1259.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8774382637478793
Weighted AreaUnderROC: 0.9697978980881198
Root mean squared error: 0.21332879304259933
Relative absolute error: 94.63632743202588
Root relative squared error: 95.5359495824165
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8359007513166835
Weighted FMeasure: 0.8449325182826541
Iteration time: 1277.0
Weighted AreaUnderPRC: 0.8019646260815947
Mean absolute error: 0.09437417694052526
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 39.0
Accumulative iteration time: 5160.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.016530035790597836
Kappa statistic: 0.8303753273203376
Training time: 1256.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 78.99367186294137
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.8813844035621554
Weighted AreaUnderROC: 0.974062144108336
Root mean squared error: 0.21330528600744678
Relative absolute error: 94.627276347706
Root relative squared error: 95.52542232590727
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8441790491523643
Weighted FMeasure: 0.8526530715454181
Iteration time: 1280.0
Weighted AreaUnderPRC: 0.815019525797618
Mean absolute error: 0.09436515092846111
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 38.0
Accumulative iteration time: 6440.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.014846776402378577
Kappa statistic: 0.840023269342641
Training time: 1260.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8952754361289098
Weighted AreaUnderROC: 0.9772019026595823
Root mean squared error: 0.2131890489475735
Relative absolute error: 94.5729698417866
Root relative squared error: 95.47336738417495
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8681764703616065
Weighted FMeasure: 0.8764539703832779
Iteration time: 1289.0
Weighted AreaUnderPRC: 0.8417122618695829
Mean absolute error: 0.09431099485607611
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 39.0
Accumulative iteration time: 7729.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.014049405952927432
Kappa statistic: 0.8684413580246914
Training time: 1272.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 11.730205278592376
Correctly Classified Instances: 88.26979472140762
Weighted Precision: 0.9002870919009411
Weighted AreaUnderROC: 0.9771942203035437
Root mean squared error: 0.213178724151593
Relative absolute error: 94.56934940805864
Root relative squared error: 95.46874358644855
Weighted TruePositiveRate: 0.8826979472140762
Weighted MatthewsCorrelation: 0.8721600943637595
Weighted FMeasure: 0.8796507778819146
Iteration time: 1300.0
Weighted AreaUnderPRC: 0.8471242770374128
Mean absolute error: 0.09430738445125043
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 38.0
Accumulative iteration time: 9029.0
Weighted Recall: 0.8826979472140762
Weighted FalsePositiveRate: 0.013569222503908554
Kappa statistic: 0.8716971903189699
Training time: 1284.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 79.0091063435711
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.9016008934605354
Weighted AreaUnderROC: 0.9796288068375009
Root mean squared error: 0.21316307506622945
Relative absolute error: 94.56391875746672
Root relative squared error: 95.46173538934126
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.877344930422635
Weighted FMeasure: 0.8845584908531628
Iteration time: 1303.0
Weighted AreaUnderPRC: 0.8519940187174916
Mean absolute error: 0.09430196884401196
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 39.0
Accumulative iteration time: 10332.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.013057702988764986
Kappa statistic: 0.877984934086629
Training time: 1289.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 79.0091063435711
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.9005836542178945
Weighted AreaUnderROC: 0.9792705494033176
Root mean squared error: 0.21316156457156243
Relative absolute error: 94.56572897433067
Root relative squared error: 95.46105893802734
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.8771225483390575
Weighted FMeasure: 0.8849657627529571
Iteration time: 1305.0
Weighted AreaUnderPRC: 0.8533817090904061
Mean absolute error: 0.09430377404642476
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 39.0
Accumulative iteration time: 11637.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.013069155118861182
Kappa statistic: 0.8779837851580523
Training time: 1294.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 79.02454082420083
Incorrectly Classified Instances: 10.850439882697946
Correctly Classified Instances: 89.14956011730206
Weighted Precision: 0.9040098779598353
Weighted AreaUnderROC: 0.979738952035727
Root mean squared error: 0.21315108986324927
Relative absolute error: 94.56391875746668
Root relative squared error: 95.45636800441717
Weighted TruePositiveRate: 0.8914956011730205
Weighted MatthewsCorrelation: 0.880455234643039
Weighted FMeasure: 0.8878839186593952
Iteration time: 1323.0
Weighted AreaUnderPRC: 0.8578958966916201
Mean absolute error: 0.09430196884401193
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 38.0
Accumulative iteration time: 12960.0
Weighted Recall: 0.8914956011730205
Weighted FalsePositiveRate: 0.012611875930034697
Kappa statistic: 0.881234997882054
Training time: 1313.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 79.0091063435711
Incorrectly Classified Instances: 8.211143695014663
Correctly Classified Instances: 91.78885630498533
Weighted Precision: 0.9276258722425458
Weighted AreaUnderROC: 0.9850696889867466
Root mean squared error: 0.2131020029297822
Relative absolute error: 94.54219615509892
Root relative squared error: 95.43438519218644
Weighted TruePositiveRate: 0.9178885630498533
Weighted MatthewsCorrelation: 0.9106535735351391
Weighted FMeasure: 0.9165056637084218
Iteration time: 1346.0
Weighted AreaUnderPRC: 0.8843276911921453
Mean absolute error: 0.09428030641505793
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 37.0
Accumulative iteration time: 14306.0
Weighted Recall: 0.9178885630498533
Weighted FalsePositiveRate: 0.00858798027136594
Kappa statistic: 0.9101246281863021
Training time: 1338.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 6.744868035190616
Correctly Classified Instances: 93.25513196480938
Weighted Precision: 0.9452522550763018
Weighted AreaUnderROC: 0.9884355727506268
Root mean squared error: 0.2130792883477576
Relative absolute error: 94.533145070779
Root relative squared error: 95.42421282336488
Weighted TruePositiveRate: 0.9325513196480938
Weighted MatthewsCorrelation: 0.9285684952891147
Weighted FMeasure: 0.9313340557453132
Iteration time: 1233.0
Weighted AreaUnderPRC: 0.9095893604569291
Mean absolute error: 0.09427128040299373
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 37.0
Accumulative iteration time: 15539.0
Weighted Recall: 0.9325513196480938
Weighted FalsePositiveRate: 0.006358844977714518
Kappa statistic: 0.9261738017244625
Training time: 1227.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9479182108214366
Weighted AreaUnderROC: 0.988841139646311
Root mean squared error: 0.213069924380115
Relative absolute error: 94.52952463705103
Root relative squared error: 95.42001931751957
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.9315573910185283
Weighted FMeasure: 0.934230883848443
Iteration time: 1257.0
Weighted AreaUnderPRC: 0.9135224914051635
Mean absolute error: 0.09426766999816806
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 37.0
Accumulative iteration time: 16796.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.006270248261108836
Kappa statistic: 0.9293756589847868
Training time: 1253.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 78.97823738231166
Incorrectly Classified Instances: 4.69208211143695
Correctly Classified Instances: 95.30791788856305
Weighted Precision: 0.9605548410533747
Weighted AreaUnderROC: 0.992408468278721
Root mean squared error: 0.2129436688160437
Relative absolute error: 94.47521813113164
Root relative squared error: 95.36347774602527
Weighted TruePositiveRate: 0.9530791788856305
Weighted MatthewsCorrelation: 0.9497101614900084
Weighted FMeasure: 0.9540476262458305
Iteration time: 1290.0
Weighted AreaUnderPRC: 0.938156863085753
Mean absolute error: 0.09421351392578307
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 18086.0
Weighted Recall: 0.9530791788856305
Weighted FalsePositiveRate: 0.005369985489253939
Kappa statistic: 0.9486542443064183
Training time: 1289.0
		
Time end:Sun Oct 08 06.12.50 EEST 2017