Sun Oct 08 06.07.40 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.40 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 73.6996450069453
Incorrectly Classified Instances: 29.032258064516128
Correctly Classified Instances: 70.96774193548387
Weighted Precision: 0.6658686073017689
Weighted AreaUnderROC: 0.9178193411974104
Root mean squared error: 0.21554839731159026
Relative absolute error: 95.44368415336079
Root relative squared error: 96.52996449484742
Weighted TruePositiveRate: 0.7096774193548387
Weighted MatthewsCorrelation: 0.6543572836809826
Weighted FMeasure: 0.6746466976295318
Iteration time: 856.0
Weighted AreaUnderPRC: 0.6190086607794231
Mean absolute error: 0.09517929721664872
Coverage of cases: 91.78885630498533
Instances selection time: 42.0
Test time: 28.0
Accumulative iteration time: 856.0
Weighted Recall: 0.7096774193548387
Weighted FalsePositiveRate: 0.03408447609934762
Kappa statistic: 0.6786693318103941
Training time: 814.0
		
Time end:Sun Oct 08 06.07.41 EEST 2017