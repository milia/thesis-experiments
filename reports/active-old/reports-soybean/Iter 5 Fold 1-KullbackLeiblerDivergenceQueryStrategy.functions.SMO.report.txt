Sun Oct 08 06.07.40 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.40 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.6719216132081478
Weighted AreaUnderROC: 0.8981308242887049
Root mean squared error: 0.21618784314934567
Relative absolute error: 95.5929667040771
Root relative squared error: 96.81633027063039
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6678648219952337
Weighted FMeasure: 0.6873342010573581
Iteration time: 761.0
Weighted AreaUnderPRC: 0.6339597954767437
Mean absolute error: 0.0953281662422938
Coverage of cases: 88.30409356725146
Instances selection time: 37.0
Test time: 26.0
Accumulative iteration time: 761.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.037606156860589726
Kappa statistic: 0.7006991676575506
Training time: 724.0
		
Time end:Sun Oct 08 06.07.40 EEST 2017