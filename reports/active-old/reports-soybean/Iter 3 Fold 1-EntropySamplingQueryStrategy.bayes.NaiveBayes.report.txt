Sun Oct 08 06.03.26 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.03.26 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.403200984918461
Incorrectly Classified Instances: 27.485380116959064
Correctly Classified Instances: 72.51461988304094
Weighted Precision: 0.7521097798697284
Weighted AreaUnderROC: 0.9724277272196916
Root mean squared error: 0.14900990870737477
Relative absolute error: 32.339580808840125
Root relative squared error: 66.73174737694933
Weighted TruePositiveRate: 0.7251461988304093
Weighted MatthewsCorrelation: 0.6964366682760215
Weighted FMeasure: 0.6961037437692786
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8545994238357326
Mean absolute error: 0.03224999748250013
Coverage of cases: 87.42690058479532
Instances selection time: 53.0
Test time: 74.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7251461988304093
Weighted FalsePositiveRate: 0.028990681862797858
Kappa statistic: 0.6970456580125335
Training time: 1.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 8.80270852570024
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.8354826061546907
Weighted AreaUnderROC: 0.9827555396771476
Root mean squared error: 0.12728416993394362
Relative absolute error: 24.41812547447594
Root relative squared error: 57.00221647539494
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7888845090152569
Weighted FMeasure: 0.7912376643166719
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9070174408854962
Mean absolute error: 0.02435048523770475
Coverage of cases: 91.81286549707602
Instances selection time: 31.0
Test time: 40.0
Accumulative iteration time: 85.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.02131692675842961
Kappa statistic: 0.7878651178527123
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 7.78701138811943
Incorrectly Classified Instances: 17.251461988304094
Correctly Classified Instances: 82.74853801169591
Weighted Precision: 0.8506086202464244
Weighted AreaUnderROC: 0.9870091586679635
Root mean squared error: 0.12312668946014571
Relative absolute error: 21.416976113521574
Root relative squared error: 55.14035413946861
Weighted TruePositiveRate: 0.827485380116959
Weighted MatthewsCorrelation: 0.8096703017621331
Weighted FMeasure: 0.8120206695387536
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9253019139839426
Mean absolute error: 0.0213576493098832
Coverage of cases: 91.52046783625731
Instances selection time: 24.0
Test time: 43.0
Accumulative iteration time: 110.0
Weighted Recall: 0.827485380116959
Weighted FalsePositiveRate: 0.022000456718182265
Kappa statistic: 0.8097940330866757
Training time: 1.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 7.817790089258246
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8679556566904012
Weighted AreaUnderROC: 0.9862015491737228
Root mean squared error: 0.10285355011977194
Relative absolute error: 16.74063196120929
Root relative squared error: 46.06134708057388
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8565439301119085
Weighted FMeasure: 0.8634887717906234
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9304598130981698
Mean absolute error: 0.016694259019488636
Coverage of cases: 92.39766081871345
Instances selection time: 21.0
Test time: 44.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.014492092093428204
Kappa statistic: 0.8650367850867714
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.940904893813502
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.884356285371158
Weighted AreaUnderROC: 0.9907012513063912
Root mean squared error: 0.09787078171505503
Relative absolute error: 15.620472088511695
Root relative squared error: 43.82989250613753
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8783984616029771
Weighted FMeasure: 0.8834923838870756
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9417346119519363
Mean absolute error: 0.015577202082726482
Coverage of cases: 93.27485380116958
Instances selection time: 19.0
Test time: 41.0
Accumulative iteration time: 152.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.011017760706042672
Kappa statistic: 0.8876773515501839
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.7870113881194385
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9213563683042119
Weighted AreaUnderROC: 0.9933948840744886
Root mean squared error: 0.08957202484129821
Relative absolute error: 13.811148260004835
Root relative squared error: 40.11342457426467
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9008573559662933
Weighted FMeasure: 0.9042507961359262
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9620931702018487
Mean absolute error: 0.01377289023158389
Coverage of cases: 95.32163742690058
Instances selection time: 19.0
Test time: 42.0
Accumulative iteration time: 172.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.010688235325600575
Kappa statistic: 0.9005142256587343
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.463835026161914
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9333871373018154
Weighted AreaUnderROC: 0.994541437755082
Root mean squared error: 0.08323955653393135
Relative absolute error: 11.788487562379734
Root relative squared error: 37.27752809579894
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.9160030107723385
Weighted FMeasure: 0.9207873484881796
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9675497205591812
Mean absolute error: 0.01175583247217934
Coverage of cases: 95.6140350877193
Instances selection time: 15.0
Test time: 44.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.010291451919146908
Kappa statistic: 0.9166174360705545
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.186826715912602
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.932140000607764
Weighted AreaUnderROC: 0.9946065341088416
Root mean squared error: 0.08118652719075202
Relative absolute error: 11.014876478193885
Root relative squared error: 36.35811114779214
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.9163714219671751
Weighted FMeasure: 0.9211829905419586
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9702510068852446
Mean absolute error: 0.01098436435498569
Coverage of cases: 95.02923976608187
Instances selection time: 18.0
Test time: 44.0
Accumulative iteration time: 207.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.009581541570682344
Kappa statistic: 0.9166127444084962
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.3868882733148835
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9362562846152647
Weighted AreaUnderROC: 0.9960894234854111
Root mean squared error: 0.08178578726716712
Relative absolute error: 11.171773634397713
Root relative squared error: 36.62648036148621
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9199112202797294
Weighted FMeasure: 0.9243429754850712
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9763754858109355
Mean absolute error: 0.011140826893028288
Coverage of cases: 95.90643274853801
Instances selection time: 14.0
Test time: 39.0
Accumulative iteration time: 222.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.009351106950771286
Kappa statistic: 0.919831223628692
Training time: 1.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.171437365343196
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9362562846152647
Weighted AreaUnderROC: 0.9952389516413155
Root mean squared error: 0.08281427978636827
Relative absolute error: 11.04827062102386
Root relative squared error: 37.087074583479826
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9199112202797294
Weighted FMeasure: 0.9243429754850712
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9730228780269395
Mean absolute error: 0.011017665993264888
Coverage of cases: 94.73684210526316
Instances selection time: 9.0
Test time: 40.0
Accumulative iteration time: 232.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.009351106950771286
Kappa statistic: 0.919831223628692
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.171437365343196
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9323377574798152
Weighted AreaUnderROC: 0.9952875380793192
Root mean squared error: 0.08311184599595746
Relative absolute error: 10.940685770471415
Root relative squared error: 37.22033493709307
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.9165129248830945
Weighted FMeasure: 0.9212365012249841
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9730915266842501
Mean absolute error: 0.010910379161689042
Coverage of cases: 95.02923976608187
Instances selection time: 9.0
Test time: 41.0
Accumulative iteration time: 242.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.009450553546730248
Kappa statistic: 0.9166236907987885
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.140658664204383
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9323377574798152
Weighted AreaUnderROC: 0.9953470406973014
Root mean squared error: 0.08353238473670008
Relative absolute error: 10.99445523424052
Root relative squared error: 37.408666607469215
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.9165129248830945
Weighted FMeasure: 0.9212365012249841
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9729457830083943
Mean absolute error: 0.010963999679575133
Coverage of cases: 94.73684210526316
Instances selection time: 14.0
Test time: 40.0
Accumulative iteration time: 258.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.009450553546730248
Kappa statistic: 0.9166236907987885
Training time: 2.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.15604801477379
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9298415062911678
Weighted AreaUnderROC: 0.9950610356598344
Root mean squared error: 0.08385101559679099
Relative absolute error: 11.021298166439875
Root relative squared error: 37.551360433984094
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9131161680023991
Weighted FMeasure: 0.9178630548806237
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9706467910901628
Mean absolute error: 0.010990768254621579
Coverage of cases: 95.02923976608187
Instances selection time: 4.0
Test time: 40.0
Accumulative iteration time: 264.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.009904955316921493
Kappa statistic: 0.913416909675665
Training time: 2.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 7.0944906124961635
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9274287909692343
Weighted AreaUnderROC: 0.9944884934206254
Root mean squared error: 0.08557235870824725
Relative absolute error: 11.242913415697354
Root relative squared error: 38.322236912328414
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9097346205502774
Weighted FMeasure: 0.9144603622930351
Iteration time: 9.0
Weighted AreaUnderPRC: 0.968272405569626
Mean absolute error: 0.011211769611221835
Coverage of cases: 94.73684210526316
Instances selection time: 8.0
Test time: 43.0
Accumulative iteration time: 273.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010359357087112737
Kappa statistic: 0.9102101285525416
Training time: 1.0
		
Time end:Sun Oct 08 06.03.28 EEST 2017