Sun Oct 08 06.07.42 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.42 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 70.319493749035
Incorrectly Classified Instances: 25.806451612903224
Correctly Classified Instances: 74.19354838709677
Weighted Precision: 0.7460796784935859
Weighted AreaUnderROC: 0.8859979366436648
Root mean squared error: 0.14351134653201697
Relative absolute error: 40.01665516226502
Root relative squared error: 64.26930266300081
Weighted TruePositiveRate: 0.7419354838709677
Weighted MatthewsCorrelation: 0.7067334205504584
Weighted FMeasure: 0.7221961341891916
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7013165653198038
Mean absolute error: 0.039905805701982086
Coverage of cases: 97.65395894428153
Instances selection time: 22.0
Test time: 16.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7419354838709677
Weighted FalsePositiveRate: 0.03276169341686207
Kappa statistic: 0.7140188697226723
Training time: 0.0
		
Time end:Sun Oct 08 06.07.42 EEST 2017