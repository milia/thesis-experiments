Sun Oct 08 06.18.56 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.56 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 71.10665226115105
Incorrectly Classified Instances: 26.686217008797655
Correctly Classified Instances: 73.31378299120234
Weighted Precision: 0.6996434124157739
Weighted AreaUnderROC: 0.8748125328344621
Root mean squared error: 0.1511942706293806
Relative absolute error: 43.0321802317011
Root relative squared error: 67.70997955777278
Weighted TruePositiveRate: 0.7331378299120235
Weighted MatthewsCorrelation: 0.6852800810648367
Weighted FMeasure: 0.6999319121986068
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6649740687847709
Mean absolute error: 0.04291297751637821
Coverage of cases: 92.37536656891496
Instances selection time: 12.0
Test time: 16.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7331378299120235
Weighted FalsePositiveRate: 0.02933641165730614
Kappa statistic: 0.7051714473021634
Training time: 0.0
		
Time end:Sun Oct 08 06.18.56 EEST 2017