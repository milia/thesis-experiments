Sun Oct 08 06.18.16 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.16 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 70.3908895044626
Incorrectly Classified Instances: 27.77777777777778
Correctly Classified Instances: 72.22222222222223
Weighted Precision: 0.7452859586440417
Weighted AreaUnderROC: 0.8795356042727148
Root mean squared error: 0.1537531546037249
Relative absolute error: 43.18926941686386
Root relative squared error: 68.85593555777413
Weighted TruePositiveRate: 0.7222222222222222
Weighted MatthewsCorrelation: 0.6904838125120566
Weighted FMeasure: 0.70070859628817
Iteration time: 1.0
Weighted AreaUnderPRC: 0.651281182562595
Mean absolute error: 0.04306963155144357
Coverage of cases: 95.6140350877193
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7222222222222222
Weighted FalsePositiveRate: 0.0256987272047513
Kappa statistic: 0.6958776396585292
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 63.55801785164651
Incorrectly Classified Instances: 21.92982456140351
Correctly Classified Instances: 78.0701754385965
Weighted Precision: 0.8183187297117429
Weighted AreaUnderROC: 0.9036260500970594
Root mean squared error: 0.13986808578043236
Relative absolute error: 36.010616760814926
Root relative squared error: 62.63772555371918
Weighted TruePositiveRate: 0.7807017543859649
Weighted MatthewsCorrelation: 0.7628964418836809
Weighted FMeasure: 0.769393737181797
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7213241135675873
Mean absolute error: 0.035910864359815754
Coverage of cases: 94.44444444444444
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7807017543859649
Weighted FalsePositiveRate: 0.01938255251706425
Kappa statistic: 0.7602019352124526
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 55.0477069867651
Incorrectly Classified Instances: 21.637426900584796
Correctly Classified Instances: 78.3625730994152
Weighted Precision: 0.8202447522354495
Weighted AreaUnderROC: 0.9093234670893647
Root mean squared error: 0.13757044536877353
Relative absolute error: 33.118007981855776
Root relative squared error: 61.60876338037144
Weighted TruePositiveRate: 0.783625730994152
Weighted MatthewsCorrelation: 0.7653163820784255
Weighted FMeasure: 0.7710525430053059
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7350957799138458
Mean absolute error: 0.0330262683475573
Coverage of cases: 93.27485380116958
Instances selection time: 2.0
Test time: 24.0
Accumulative iteration time: 4.0
Weighted Recall: 0.783625730994152
Weighted FalsePositiveRate: 0.019660887460445812
Kappa statistic: 0.7631601111766192
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 48.799630655586206
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.8415396361928412
Weighted AreaUnderROC: 0.9278903857057709
Root mean squared error: 0.12684405607567098
Relative absolute error: 28.504187018992642
Root relative squared error: 56.80511839606506
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7953184212558044
Weighted FMeasure: 0.8035837004716485
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7850540822756573
Mean absolute error: 0.02842522805218128
Coverage of cases: 94.44444444444444
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.019379520026256567
Kappa statistic: 0.7888948121545409
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 42.24376731301946
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.849514130987862
Weighted AreaUnderROC: 0.9284770948120782
Root mean squared error: 0.12356907044044861
Relative absolute error: 26.427030490709143
Root relative squared error: 55.33846751379403
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.8046589987317876
Weighted FMeasure: 0.8132906337255098
Iteration time: 1.0
Weighted AreaUnderPRC: 0.800020494455179
Mean absolute error: 0.026353825420097992
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 27.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.018674873054964063
Kappa statistic: 0.7986468048520643
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 37.33456448137899
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.857066519104758
Weighted AreaUnderROC: 0.9520767705858
Root mean squared error: 0.11207089495894036
Relative absolute error: 23.014065092918244
Root relative squared error: 50.189190206104044
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8259452827611049
Weighted FMeasure: 0.8347689798970638
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8423037500957331
Mean absolute error: 0.02295031422008488
Coverage of cases: 96.78362573099415
Instances selection time: 1.0
Test time: 29.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.017515898608101224
Kappa statistic: 0.8269911753133606
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 33.10249307479225
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8614589683194699
Weighted AreaUnderROC: 0.9554336345414874
Root mean squared error: 0.11039131977280199
Relative absolute error: 21.946697707849257
Root relative squared error: 49.437018837137686
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8317564082749387
Weighted FMeasure: 0.8401245753400403
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8497353879297025
Mean absolute error: 0.021885903531373412
Coverage of cases: 97.36842105263158
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 7.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.01697342525332603
Kappa statistic: 0.8333801787621564
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 29.63988919667586
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.8700806975925994
Weighted AreaUnderROC: 0.9539252882408635
Root mean squared error: 0.10548017854599394
Relative absolute error: 20.050397665819517
Root relative squared error: 47.23764136940519
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8476200354996267
Weighted FMeasure: 0.8557191215856401
Iteration time: 1.0
Weighted AreaUnderPRC: 0.856010811654879
Mean absolute error: 0.01999485639804734
Coverage of cases: 97.07602339181287
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.016055991879823237
Kappa statistic: 0.8523717918641204
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 25.407817790089307
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8724008136214174
Weighted AreaUnderROC: 0.9566200629368811
Root mean squared error: 0.10344589127519986
Relative absolute error: 19.526520706317815
Root relative squared error: 46.32661776416722
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8441526316911964
Weighted FMeasure: 0.8533584631775237
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8663863982199567
Mean absolute error: 0.019472430621258936
Coverage of cases: 96.49122807017544
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.016564340200473395
Kappa statistic: 0.846067289298976
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 21.129578331794423
Incorrectly Classified Instances: 13.742690058479532
Correctly Classified Instances: 86.25730994152046
Weighted Precision: 0.872672183381436
Weighted AreaUnderROC: 0.9583907869816847
Root mean squared error: 0.10181853990847207
Relative absolute error: 18.72225249518826
Root relative squared error: 45.59783401253582
Weighted TruePositiveRate: 0.8625730994152047
Weighted MatthewsCorrelation: 0.8461767856020147
Weighted FMeasure: 0.8549478067248688
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8711556628281565
Mean absolute error: 0.018670390299910897
Coverage of cases: 96.49122807017544
Instances selection time: 1.0
Test time: 38.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8625730994152047
Weighted FalsePositiveRate: 0.016476268615889446
Kappa statistic: 0.8491752209732205
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 18.12865497076028
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.8762403846007162
Weighted AreaUnderROC: 0.9630529699757584
Root mean squared error: 0.0998030904300886
Relative absolute error: 17.90320627954404
Root relative squared error: 44.69524661677673
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8509111869401605
Weighted FMeasure: 0.8602947031768934
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8814275308366663
Mean absolute error: 0.017853612910348784
Coverage of cases: 93.27485380116958
Instances selection time: 3.0
Test time: 44.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.01593158871886109
Kappa statistic: 0.8524022629401333
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 14.9584487534626
Incorrectly Classified Instances: 13.157894736842104
Correctly Classified Instances: 86.84210526315789
Weighted Precision: 0.8741666669089564
Weighted AreaUnderROC: 0.9630547117271937
Root mean squared error: 0.09747673044947372
Relative absolute error: 17.45023622781657
Root relative squared error: 43.653422835520004
Weighted TruePositiveRate: 0.868421052631579
Weighted MatthewsCorrelation: 0.8519853445251132
Weighted FMeasure: 0.8622298293673187
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8905984554526878
Mean absolute error: 0.017401897623307537
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 42.0
Accumulative iteration time: 14.0
Weighted Recall: 0.868421052631579
Weighted FalsePositiveRate: 0.01620984731988443
Kappa statistic: 0.8555838111235186
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 12.157586949830714
Incorrectly Classified Instances: 13.157894736842104
Correctly Classified Instances: 86.84210526315789
Weighted Precision: 0.8760478866981537
Weighted AreaUnderROC: 0.9627436944556975
Root mean squared error: 0.09758808364980977
Relative absolute error: 17.26625209095929
Root relative squared error: 43.703290617460816
Weighted TruePositiveRate: 0.868421052631579
Weighted MatthewsCorrelation: 0.8522056527629118
Weighted FMeasure: 0.8620371551291348
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8920907518522606
Mean absolute error: 0.01721842313779889
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 45.0
Accumulative iteration time: 15.0
Weighted Recall: 0.868421052631579
Weighted FalsePositiveRate: 0.016576177505491726
Kappa statistic: 0.8555350085890493
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 9.12588488765774
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8817370967733286
Weighted AreaUnderROC: 0.9628489132084151
Root mean squared error: 0.09636117898566646
Relative absolute error: 16.566255228448412
Root relative squared error: 43.153840632466895
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8592484913243859
Weighted FMeasure: 0.8693865818432515
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8946087114268086
Mean absolute error: 0.016520365324768645
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 47.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.015667373965109237
Kappa statistic: 0.8620489104434209
Training time: 0.0
		
Time end:Sun Oct 08 06.18.17 EEST 2017