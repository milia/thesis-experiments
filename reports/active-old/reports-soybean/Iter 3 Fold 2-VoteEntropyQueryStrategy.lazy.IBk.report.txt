Sun Oct 08 06.18.56 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.56 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 70.18058342336748
Incorrectly Classified Instances: 23.46041055718475
Correctly Classified Instances: 76.53958944281526
Weighted Precision: 0.72279897641927
Weighted AreaUnderROC: 0.8923843814540297
Root mean squared error: 0.13926047613902479
Relative absolute error: 38.54387590712244
Root relative squared error: 62.365617118475214
Weighted TruePositiveRate: 0.7653958944281525
Weighted MatthewsCorrelation: 0.7137624307101704
Weighted FMeasure: 0.7256373127054292
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7127002255916375
Mean absolute error: 0.03843710616776787
Coverage of cases: 93.25513196480938
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7653958944281525
Weighted FalsePositiveRate: 0.02673821332219376
Kappa statistic: 0.7408297627756296
Training time: 1.0
		
Time end:Sun Oct 08 06.18.56 EEST 2017