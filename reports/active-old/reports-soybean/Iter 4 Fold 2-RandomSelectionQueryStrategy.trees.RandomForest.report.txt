Sun Oct 08 06.18.37 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.37 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 30.46766476308067
Incorrectly Classified Instances: 34.89736070381232
Correctly Classified Instances: 65.10263929618769
Weighted Precision: 0.6380360153028262
Weighted AreaUnderROC: 0.9102381407105529
Root mean squared error: 0.16760758418427138
Relative absolute error: 62.27968583836654
Root relative squared error: 75.06042425816217
Weighted TruePositiveRate: 0.6510263929618768
Weighted MatthewsCorrelation: 0.5970928646987945
Weighted FMeasure: 0.6097377480381646
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7157091043300907
Mean absolute error: 0.06210716593299765
Coverage of cases: 88.85630498533725
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6510263929618768
Weighted FalsePositiveRate: 0.03744767003056457
Kappa statistic: 0.6124555907858044
Training time: 6.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 25.327982713381665
Incorrectly Classified Instances: 29.032258064516128
Correctly Classified Instances: 70.96774193548387
Weighted Precision: 0.7375306206548984
Weighted AreaUnderROC: 0.9549473809487867
Root mean squared error: 0.1526499108545277
Relative absolute error: 53.099921431401576
Root relative squared error: 68.36186517141343
Weighted TruePositiveRate: 0.7096774193548387
Weighted MatthewsCorrelation: 0.6834827727925615
Weighted FMeasure: 0.6934229165375974
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8048783610605553
Mean absolute error: 0.05295283023630121
Coverage of cases: 95.01466275659824
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7096774193548387
Weighted FalsePositiveRate: 0.029656063037340065
Kappa statistic: 0.6794108429959261
Training time: 7.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 27.010341102021847
Incorrectly Classified Instances: 31.671554252199414
Correctly Classified Instances: 68.32844574780059
Weighted Precision: 0.710272340074393
Weighted AreaUnderROC: 0.9205208695741134
Root mean squared error: 0.16338663608262521
Relative absolute error: 58.269631645510714
Root relative squared error: 73.17013894187888
Weighted TruePositiveRate: 0.6832844574780058
Weighted MatthewsCorrelation: 0.6503885706310291
Weighted FMeasure: 0.6584150810276942
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7305535934111794
Mean absolute error: 0.05810821992350151
Coverage of cases: 91.49560117302053
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6832844574780058
Weighted FalsePositiveRate: 0.03350906826515097
Kappa statistic: 0.64877546349279
Training time: 10.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 24.463651798116956
Incorrectly Classified Instances: 23.46041055718475
Correctly Classified Instances: 76.53958944281526
Weighted Precision: 0.7649844568241174
Weighted AreaUnderROC: 0.9620169673592989
Root mean squared error: 0.1415372522840372
Relative absolute error: 48.17754531387409
Root relative squared error: 63.38523555768384
Weighted TruePositiveRate: 0.7653958944281525
Weighted MatthewsCorrelation: 0.7333845615191671
Weighted FMeasure: 0.740975657717342
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8458513321199952
Mean absolute error: 0.04804408950968096
Coverage of cases: 95.89442815249267
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7653958944281525
Weighted FalsePositiveRate: 0.02368905338715625
Kappa statistic: 0.7409774114832082
Training time: 10.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 22.19478314554714
Incorrectly Classified Instances: 19.64809384164223
Correctly Classified Instances: 80.35190615835778
Weighted Precision: 0.8100851775667293
Weighted AreaUnderROC: 0.9725103888295951
Root mean squared error: 0.13348734494560516
Relative absolute error: 43.66230191977399
Root relative squared error: 59.78021098196238
Weighted TruePositiveRate: 0.8035190615835777
Weighted MatthewsCorrelation: 0.7786062524831094
Weighted FMeasure: 0.7833322181969735
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8866052311015864
Mean absolute error: 0.04354135371501045
Coverage of cases: 96.48093841642229
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8035190615835777
Weighted FalsePositiveRate: 0.0181051959471875
Kappa statistic: 0.7837195652997083
Training time: 12.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 24.324741472449414
Incorrectly Classified Instances: 24.926686217008797
Correctly Classified Instances: 75.0733137829912
Weighted Precision: 0.7393770464764172
Weighted AreaUnderROC: 0.9624501427954893
Root mean squared error: 0.14282824026309535
Relative absolute error: 49.113142879991734
Root relative squared error: 63.963384248817974
Weighted TruePositiveRate: 0.750733137829912
Weighted MatthewsCorrelation: 0.718457749704646
Weighted FMeasure: 0.7324288681545276
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8474273658881514
Mean absolute error: 0.04897709539278997
Coverage of cases: 96.18768328445748
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 63.0
Weighted Recall: 0.750733137829912
Weighted FalsePositiveRate: 0.02370209142108533
Kappa statistic: 0.7254532365923428
Training time: 13.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 22.966507177033492
Incorrectly Classified Instances: 16.715542521994134
Correctly Classified Instances: 83.28445747800586
Weighted Precision: 0.8464803501669861
Weighted AreaUnderROC: 0.9786195544473739
Root mean squared error: 0.13186632734827314
Relative absolute error: 44.30389532841574
Root relative squared error: 59.054263709481454
Weighted TruePositiveRate: 0.8328445747800587
Weighted MatthewsCorrelation: 0.8173767386324944
Weighted FMeasure: 0.8282351874115657
Iteration time: 15.0
Weighted AreaUnderPRC: 0.885332959820906
Mean absolute error: 0.04418116985659226
Coverage of cases: 98.24046920821114
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8328445747800587
Weighted FalsePositiveRate: 0.01869147605172168
Kappa statistic: 0.8159845494049818
Training time: 15.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 21.97870041673095
Incorrectly Classified Instances: 16.715542521994134
Correctly Classified Instances: 83.28445747800586
Weighted Precision: 0.8416235617831272
Weighted AreaUnderROC: 0.9775725978767997
Root mean squared error: 0.1281268217374684
Relative absolute error: 41.86206854602863
Root relative squared error: 57.37958485154752
Weighted TruePositiveRate: 0.8328445747800587
Weighted MatthewsCorrelation: 0.8128299919685084
Weighted FMeasure: 0.8239131822609794
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8892285462190375
Mean absolute error: 0.041746107137314234
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8328445747800587
Weighted FalsePositiveRate: 0.021485589759783808
Kappa statistic: 0.8155094679891794
Training time: 17.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 19.74070072542058
Incorrectly Classified Instances: 17.008797653958943
Correctly Classified Instances: 82.99120234604105
Weighted Precision: 0.8343395130691746
Weighted AreaUnderROC: 0.9879297633691628
Root mean squared error: 0.12086533896867921
Relative absolute error: 38.778409123618665
Root relative squared error: 54.127643836936805
Weighted TruePositiveRate: 0.8299120234604106
Weighted MatthewsCorrelation: 0.8120944166647586
Weighted FMeasure: 0.8245877903722313
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9106416726151375
Mean absolute error: 0.03867098970776411
Coverage of cases: 99.70674486803519
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8299120234604106
Weighted FalsePositiveRate: 0.017673465560917676
Kappa statistic: 0.8131312654125606
Training time: 17.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 20.373514431239382
Incorrectly Classified Instances: 15.249266862170089
Correctly Classified Instances: 84.75073313782991
Weighted Precision: 0.8552840365786678
Weighted AreaUnderROC: 0.9786526730287297
Root mean squared error: 0.1250245766100245
Relative absolute error: 40.095921554025374
Root relative squared error: 55.990293092752474
Weighted TruePositiveRate: 0.8475073313782991
Weighted MatthewsCorrelation: 0.8316519265492591
Weighted FMeasure: 0.8451189848667948
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9043330433457635
Mean absolute error: 0.03998485251925003
Coverage of cases: 98.53372434017595
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 150.0
Weighted Recall: 0.8475073313782991
Weighted FalsePositiveRate: 0.017567253726252598
Kappa statistic: 0.8324925844055244
Training time: 31.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 20.12656274116378
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.818182578680475
Weighted AreaUnderROC: 0.9755642806653871
Root mean squared error: 0.1296283121069312
Relative absolute error: 40.67008739609294
Root relative squared error: 58.05200373223204
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.7951268176155674
Weighted FMeasure: 0.8058605527206993
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8752121475896918
Mean absolute error: 0.04055742787422046
Coverage of cases: 97.36070381231671
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 171.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.020903068118165048
Kappa statistic: 0.7992498694393012
Training time: 21.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 18.243556104337113
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8441060375960787
Weighted AreaUnderROC: 0.9863704797148459
Root mean squared error: 0.11628108014716572
Relative absolute error: 34.9052536506351
Root relative squared error: 52.07465552064607
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8256935222610529
Weighted FMeasure: 0.8374703156175908
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9247790755737634
Mean absolute error: 0.03480856319730955
Coverage of cases: 98.82697947214076
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 193.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.01730046791632724
Kappa statistic: 0.8289918153001846
Training time: 22.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 17.93486649174257
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.870270665007507
Weighted AreaUnderROC: 0.987842665444428
Root mean squared error: 0.11124730850407279
Relative absolute error: 32.60465635659082
Root relative squared error: 49.820359946921556
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8352943765154975
Weighted FMeasure: 0.8417950334714522
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9384028917782105
Mean absolute error: 0.032514338748955124
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 219.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.01891509905564461
Kappa statistic: 0.838353385098173
Training time: 25.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 16.082728816175354
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.887635553323018
Weighted AreaUnderROC: 0.9876419088562838
Root mean squared error: 0.10759109689455486
Relative absolute error: 29.93948629745809
Root relative squared error: 48.18298299931092
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8698389944173358
Weighted FMeasure: 0.8762341662460952
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9290850065752662
Mean absolute error: 0.02985655143236844
Coverage of cases: 97.94721407624634
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 246.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.013470747196092907
Kappa statistic: 0.8741363971910431
Training time: 26.0
		
Time end:Sun Oct 08 06.18.38 EEST 2017