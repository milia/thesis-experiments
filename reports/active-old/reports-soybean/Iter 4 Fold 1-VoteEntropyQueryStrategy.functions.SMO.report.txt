Sun Oct 08 06.18.51 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.18.51 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.8206744488369605
Weighted AreaUnderROC: 0.9378209379534502
Root mean squared error: 0.21458607612878355
Relative absolute error: 95.1180101670287
Root relative squared error: 96.0990040666208
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7978154263777604
Weighted FMeasure: 0.8052064257532847
Iteration time: 961.0
Weighted AreaUnderPRC: 0.7574223599967254
Mean absolute error: 0.09485452537432308
Coverage of cases: 94.73684210526316
Instances selection time: 23.0
Test time: 33.0
Accumulative iteration time: 961.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.022912922065524175
Kappa statistic: 0.8064899425693836
Training time: 938.0
		
Time end:Sun Oct 08 06.18.52 EEST 2017