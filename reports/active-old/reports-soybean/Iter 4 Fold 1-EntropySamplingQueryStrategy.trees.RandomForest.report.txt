Sun Oct 08 06.07.23 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.23 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 31.22499230532467
Incorrectly Classified Instances: 34.7953216374269
Correctly Classified Instances: 65.2046783625731
Weighted Precision: 0.657198191073999
Weighted AreaUnderROC: 0.9180643601612591
Root mean squared error: 0.16893898240506217
Relative absolute error: 63.65930221351771
Root relative squared error: 75.6566700414034
Weighted TruePositiveRate: 0.652046783625731
Weighted MatthewsCorrelation: 0.6099020507907156
Weighted FMeasure: 0.6233783304349957
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7045342565143907
Mean absolute error: 0.0634829606561401
Coverage of cases: 91.52046783625731
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 18.0
Weighted Recall: 0.652046783625731
Weighted FalsePositiveRate: 0.037445131291799515
Kappa statistic: 0.6147956044788127
Training time: 11.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 31.240381655894026
Incorrectly Classified Instances: 40.93567251461988
Correctly Classified Instances: 59.06432748538012
Weighted Precision: 0.6023987920348604
Weighted AreaUnderROC: 0.8970340459462729
Root mean squared error: 0.17613912968270112
Relative absolute error: 66.6637109464376
Root relative squared error: 78.88114291959161
Weighted TruePositiveRate: 0.5906432748538012
Weighted MatthewsCorrelation: 0.5515888909059703
Weighted FMeasure: 0.5680089463736054
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6501036089641006
Mean absolute error: 0.06647904692719599
Coverage of cases: 88.01169590643275
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 29.0
Weighted Recall: 0.5906432748538012
Weighted FalsePositiveRate: 0.037073338606581345
Kappa statistic: 0.548842424642174
Training time: 9.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 30.40935672514613
Incorrectly Classified Instances: 34.7953216374269
Correctly Classified Instances: 65.2046783625731
Weighted Precision: 0.677981872280118
Weighted AreaUnderROC: 0.9016991455225095
Root mean squared error: 0.17375949445589986
Relative absolute error: 66.07422465745799
Root relative squared error: 77.81546065603129
Weighted TruePositiveRate: 0.652046783625731
Weighted MatthewsCorrelation: 0.6238651617325061
Weighted FMeasure: 0.638273898796758
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6628503638757336
Mean absolute error: 0.06589119356422461
Coverage of cases: 89.18128654970761
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 42.0
Weighted Recall: 0.652046783625731
Weighted FalsePositiveRate: 0.03024906926468904
Kappa statistic: 0.6183214697690121
Training time: 10.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 26.86980609418279
Incorrectly Classified Instances: 16.95906432748538
Correctly Classified Instances: 83.04093567251462
Weighted Precision: 0.8797375610783148
Weighted AreaUnderROC: 0.9701364657658126
Root mean squared error: 0.14281219690141173
Relative absolute error: 52.463603197075194
Root relative squared error: 63.95619948125295
Weighted TruePositiveRate: 0.8304093567251462
Weighted MatthewsCorrelation: 0.8274677026109818
Weighted FMeasure: 0.8315878626004237
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8659736081750101
Mean absolute error: 0.05231827465636354
Coverage of cases: 97.6608187134503
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8304093567251462
Weighted FalsePositiveRate: 0.018427666254646105
Kappa statistic: 0.8136502419089671
Training time: 13.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 24.638350261618942
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.887978520339784
Weighted AreaUnderROC: 0.9852075004866331
Root mean squared error: 0.13477486502515862
Relative absolute error: 47.899353887900936
Root relative squared error: 60.35680662876757
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.8413521105552165
Weighted FMeasure: 0.846746862426656
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9108402534378754
Mean absolute error: 0.047766668697076145
Coverage of cases: 99.12280701754386
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.01367501134529377
Kappa statistic: 0.824374667376264
Training time: 16.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 24.63835026161892
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.851695634350498
Weighted AreaUnderROC: 0.9782009602667767
Root mean squared error: 0.13434261405183884
Relative absolute error: 47.2653440908321
Root relative squared error: 60.16322981897527
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.8099800760998611
Weighted FMeasure: 0.8171897438192368
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8831000316868697
Mean absolute error: 0.047134415159832986
Coverage of cases: 97.953216374269
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.015413988066633908
Kappa statistic: 0.7988254077926443
Training time: 18.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 24.31517389966143
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.8653525682325249
Weighted AreaUnderROC: 0.9845470765101467
Root mean squared error: 0.13194976224622695
Relative absolute error: 46.579789618331525
Root relative squared error: 59.091628718164266
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.8307841775955747
Weighted FMeasure: 0.8407258012914053
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9061894288347199
Mean absolute error: 0.04645075973019251
Coverage of cases: 99.12280701754386
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.015972045835246255
Kappa statistic: 0.8203730972322526
Training time: 21.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 22.345337026777464
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.9024134279256968
Weighted AreaUnderROC: 0.9889250693783709
Root mean squared error: 0.1183122179029399
Relative absolute error: 39.87577343344773
Root relative squared error: 52.984268665046294
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8814579472144561
Weighted FMeasure: 0.8860073996031649
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9363167681498112
Mean absolute error: 0.03976531422726125
Coverage of cases: 99.12280701754386
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 139.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.01078184329148238
Kappa statistic: 0.8780073405863081
Training time: 19.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 21.26808248691904
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8932590216756987
Weighted AreaUnderROC: 0.9809274189217062
Root mean squared error: 0.12059579637312931
Relative absolute error: 39.76893700336453
Root relative squared error: 54.00693341875323
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8659079273097534
Weighted FMeasure: 0.8736331759615549
Iteration time: 25.0
Weighted AreaUnderPRC: 0.926731029222358
Mean absolute error: 0.039658773742967744
Coverage of cases: 97.953216374269
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.013467234451715614
Kappa statistic: 0.8621097046413502
Training time: 23.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 18.605724838411845
Incorrectly Classified Instances: 9.649122807017545
Correctly Classified Instances: 90.35087719298245
Weighted Precision: 0.9075286947140493
Weighted AreaUnderROC: 0.9882454148963227
Root mean squared error: 0.1057710831530401
Relative absolute error: 32.26728120992745
Root relative squared error: 47.367918428941664
Weighted TruePositiveRate: 0.9035087719298246
Weighted MatthewsCorrelation: 0.8937016615555899
Weighted FMeasure: 0.9008019596924371
Iteration time: 25.0
Weighted AreaUnderPRC: 0.937917125386232
Mean absolute error: 0.03217789815948472
Coverage of cases: 98.53801169590643
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 189.0
Weighted Recall: 0.9035087719298246
Weighted FalsePositiveRate: 0.010271027237909895
Kappa statistic: 0.8941166536884669
Training time: 23.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 20.375500153893505
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8980924362959614
Weighted AreaUnderROC: 0.991188095959934
Root mean squared error: 0.11354848870400204
Relative absolute error: 36.44654792158307
Root relative squared error: 50.850907358852936
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8797606056108289
Weighted FMeasure: 0.8881560227649596
Iteration time: 25.0
Weighted AreaUnderPRC: 0.938632090474738
Mean absolute error: 0.03634558795504161
Coverage of cases: 99.12280701754386
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 214.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.012073100082975874
Kappa statistic: 0.8780165009996339
Training time: 24.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 18.497999384426002
Incorrectly Classified Instances: 11.403508771929825
Correctly Classified Instances: 88.59649122807018
Weighted Precision: 0.9000667805436375
Weighted AreaUnderROC: 0.9932734861628468
Root mean squared error: 0.1107079369894278
Relative absolute error: 33.922166093703055
Root relative squared error: 49.578810885051475
Weighted TruePositiveRate: 0.8859649122807017
Weighted MatthewsCorrelation: 0.8772345228583647
Weighted FMeasure: 0.8817113236436561
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9470893955091124
Mean absolute error: 0.033828198874607224
Coverage of cases: 99.70760233918129
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 240.0
Weighted Recall: 0.8859649122807017
Weighted FalsePositiveRate: 0.011805389159332305
Kappa statistic: 0.8746664160872016
Training time: 25.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 16.420437057556196
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.905957281262032
Weighted AreaUnderROC: 0.9916332784548999
Root mean squared error: 0.10167088036885885
Relative absolute error: 29.065126238966542
Root relative squared error: 45.53170700674981
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8858311931586673
Weighted FMeasure: 0.8940579378660506
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9497631309105323
Mean absolute error: 0.02898461342390041
Coverage of cases: 99.41520467836257
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 267.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.013718778332804344
Kappa statistic: 0.8874968279180804
Training time: 27.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 15.835641735918747
Incorrectly Classified Instances: 9.649122807017545
Correctly Classified Instances: 90.35087719298245
Weighted Precision: 0.9126841472050943
Weighted AreaUnderROC: 0.9822118001167459
Root mean squared error: 0.10402668810025573
Relative absolute error: 29.360555551927614
Root relative squared error: 46.586718500710006
Weighted TruePositiveRate: 0.9035087719298246
Weighted MatthewsCorrelation: 0.8960169532111426
Weighted FMeasure: 0.9033370714390853
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9304792468168273
Mean absolute error: 0.029279224373113668
Coverage of cases: 97.36842105263158
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 295.0
Weighted Recall: 0.9035087719298246
Weighted FalsePositiveRate: 0.010721399200663048
Kappa statistic: 0.8940749152955972
Training time: 27.0
		
Time end:Sun Oct 08 06.07.24 EEST 2017