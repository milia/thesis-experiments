Sun Oct 08 06.07.14 EEST 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.14 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 13.582342954159621
Incorrectly Classified Instances: 42.22873900293255
Correctly Classified Instances: 57.77126099706745
Weighted Precision: 0.5030838263747824
Weighted AreaUnderROC: 0.8409224255511623
Root mean squared error: 0.1814109142536745
Relative absolute error: 49.37916387137268
Root relative squared error: 81.24202884501518
Weighted TruePositiveRate: 0.5777126099706745
Weighted MatthewsCorrelation: 0.4916843842283162
Weighted FMeasure: 0.5054368902999973
Iteration time: 8.0
Weighted AreaUnderPRC: 0.48549073815926813
Mean absolute error: 0.04924237948391779
Coverage of cases: 78.59237536656892
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5777126099706745
Weighted FalsePositiveRate: 0.04795907759464966
Kappa statistic: 0.5297227409854907
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 15.758604722951116
Incorrectly Classified Instances: 33.13782991202346
Correctly Classified Instances: 66.86217008797654
Weighted Precision: 0.6873310333353226
Weighted AreaUnderROC: 0.8882386282836817
Root mean squared error: 0.16429141620080448
Relative absolute error: 42.637947492452966
Root relative squared error: 73.57533050800885
Weighted TruePositiveRate: 0.6686217008797654
Weighted MatthewsCorrelation: 0.6293383250583755
Weighted FMeasure: 0.6306064356220453
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5761555189423692
Mean absolute error: 0.04251983683457951
Coverage of cases: 87.68328445747801
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6686217008797654
Weighted FalsePositiveRate: 0.04045970037580464
Kappa statistic: 0.6325746378953592
Training time: 2.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 16.067294335545625
Incorrectly Classified Instances: 31.964809384164223
Correctly Classified Instances: 68.03519061583577
Weighted Precision: 0.6971777998521732
Weighted AreaUnderROC: 0.8671741574171252
Root mean squared error: 0.17316434785098453
Relative absolute error: 45.65395593693764
Root relative squared error: 77.54893359594529
Weighted TruePositiveRate: 0.6803519061583577
Weighted MatthewsCorrelation: 0.6370730529290062
Weighted FMeasure: 0.639606098167721
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5458196371838805
Mean absolute error: 0.04552749068503517
Coverage of cases: 83.87096774193549
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6803519061583577
Weighted FalsePositiveRate: 0.04279526413073448
Kappa statistic: 0.6446694199074605
Training time: 3.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 11.390646704738423
Incorrectly Classified Instances: 36.36363636363637
Correctly Classified Instances: 63.63636363636363
Weighted Precision: 0.6291562602867032
Weighted AreaUnderROC: 0.8486057703876599
Root mean squared error: 0.17999617942238674
Relative absolute error: 42.46808920376603
Root relative squared error: 80.6084620695851
Weighted TruePositiveRate: 0.6363636363636364
Weighted MatthewsCorrelation: 0.5849427517686323
Weighted FMeasure: 0.5943037874633138
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5164277474270201
Mean absolute error: 0.04235044906746788
Coverage of cases: 78.59237536656892
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6363636363636364
Weighted FalsePositiveRate: 0.041583797634730676
Kappa statistic: 0.5970573100306846
Training time: 2.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 13.165611977157004
Incorrectly Classified Instances: 41.64222873900293
Correctly Classified Instances: 58.35777126099707
Weighted Precision: 0.5565778964526714
Weighted AreaUnderROC: 0.8266067582439645
Root mean squared error: 0.19307403355744276
Relative absolute error: 51.108140018160995
Root relative squared error: 86.46517365302061
Weighted TruePositiveRate: 0.5835777126099707
Weighted MatthewsCorrelation: 0.5088414268464531
Weighted FMeasure: 0.5178159450642615
Iteration time: 3.0
Weighted AreaUnderPRC: 0.47706259546202723
Mean absolute error: 0.05096656622309728
Coverage of cases: 74.48680351906158
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.5835777126099707
Weighted FalsePositiveRate: 0.043879162459656874
Kappa statistic: 0.5422169699834555
Training time: 2.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 12.486494829449029
Incorrectly Classified Instances: 32.551319648093845
Correctly Classified Instances: 67.44868035190616
Weighted Precision: 0.6531326511607609
Weighted AreaUnderROC: 0.8805712032163067
Root mean squared error: 0.1743983096280208
Relative absolute error: 41.29097922743321
Root relative squared error: 78.10154399811462
Weighted TruePositiveRate: 0.6744868035190615
Weighted MatthewsCorrelation: 0.6187347084036723
Weighted FMeasure: 0.6262237066236164
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5609002948311322
Mean absolute error: 0.04117659978359027
Coverage of cases: 82.40469208211144
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6744868035190615
Weighted FalsePositiveRate: 0.03948136544498492
Kappa statistic: 0.6394318701417466
Training time: 2.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 12.640839635746271
Incorrectly Classified Instances: 27.565982404692082
Correctly Classified Instances: 72.43401759530792
Weighted Precision: 0.7260979443571312
Weighted AreaUnderROC: 0.9096136561924318
Root mean squared error: 0.15542056713964303
Relative absolute error: 35.47636584364388
Root relative squared error: 69.6026600748568
Weighted TruePositiveRate: 0.7243401759530792
Weighted MatthewsCorrelation: 0.6785593316528064
Weighted FMeasure: 0.6809552852785087
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6561994903670603
Mean absolute error: 0.035378093362082856
Coverage of cases: 87.3900293255132
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7243401759530792
Weighted FalsePositiveRate: 0.03448759935657426
Kappa statistic: 0.6950926022810505
Training time: 3.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 9.600246951690092
Incorrectly Classified Instances: 18.7683284457478
Correctly Classified Instances: 81.2316715542522
Weighted Precision: 0.7827900518478854
Weighted AreaUnderROC: 0.9389163335921097
Root mean squared error: 0.12743612348494274
Relative absolute error: 25.87280313266564
Root relative squared error: 57.070266486741616
Weighted TruePositiveRate: 0.8123167155425219
Weighted MatthewsCorrelation: 0.7691423507920743
Weighted FMeasure: 0.7792117902588422
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7814526830838192
Mean absolute error: 0.02580113331789394
Coverage of cases: 90.3225806451613
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8123167155425219
Weighted FalsePositiveRate: 0.024306471914284967
Kappa statistic: 0.7923066674280058
Training time: 4.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 9.2606883778361
Incorrectly Classified Instances: 15.835777126099707
Correctly Classified Instances: 84.1642228739003
Weighted Precision: 0.8317776835722773
Weighted AreaUnderROC: 0.9366297526318151
Root mean squared error: 0.12222665594025633
Relative absolute error: 23.227387138751503
Root relative squared error: 54.73728826283626
Weighted TruePositiveRate: 0.841642228739003
Weighted MatthewsCorrelation: 0.8109354800539593
Weighted FMeasure: 0.8179686050823265
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7900380936615939
Mean absolute error: 0.023163045346123585
Coverage of cases: 89.73607038123167
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.841642228739003
Weighted FalsePositiveRate: 0.01981384493669353
Kappa statistic: 0.8251066133520758
Training time: 4.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 8.411791943201129
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8928919575897774
Weighted AreaUnderROC: 0.952809730842867
Root mean squared error: 0.10409879519765189
Relative absolute error: 17.2470169139675
Root relative squared error: 46.619010531818994
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8725855134394964
Weighted FMeasure: 0.8807894226537925
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8536413022954202
Mean absolute error: 0.017199241243845857
Coverage of cases: 92.08211143695014
Instances selection time: 0.0
Test time: 12.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.014129417301535876
Kappa statistic: 0.8741971186136047
Training time: 6.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 8.689612594536223
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.8958245089094254
Weighted AreaUnderROC: 0.9581919813766622
Root mean squared error: 0.10253863662840548
Relative absolute error: 17.428811126308943
Root relative squared error: 45.92031801926005
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.8756417362264302
Weighted FMeasure: 0.8836789010042412
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8579182979639588
Mean absolute error: 0.017380531871111566
Coverage of cases: 93.25513196480938
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.014040820584930194
Kappa statistic: 0.8774089176072127
Training time: 6.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 8.597005710757857
Incorrectly Classified Instances: 10.850439882697946
Correctly Classified Instances: 89.14956011730206
Weighted Precision: 0.896383090113168
Weighted AreaUnderROC: 0.9633233592182734
Root mean squared error: 0.10098209378265877
Relative absolute error: 17.34467888351729
Root relative squared error: 45.22324475168461
Weighted TruePositiveRate: 0.8914956011730205
Weighted MatthewsCorrelation: 0.8788387397581863
Weighted FMeasure: 0.8882477512264905
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8670877952099086
Mean absolute error: 0.01729663268162404
Coverage of cases: 93.54838709677419
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8914956011730205
Weighted FalsePositiveRate: 0.013952223868324514
Kappa statistic: 0.8806485484282917
Training time: 6.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 8.550702268868681
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9052650372139087
Weighted AreaUnderROC: 0.9622623748396304
Root mean squared error: 0.09660259566604724
Relative absolute error: 15.641014893241955
Root relative squared error: 43.26195530126647
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.8885576257563476
Weighted FMeasure: 0.8969569298214212
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8744012262917928
Mean absolute error: 0.015597687982180482
Coverage of cases: 93.841642228739
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.012517074491017185
Kappa statistic: 0.8904210576059732
Training time: 6.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 7.933323043679625
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9052650372139087
Weighted AreaUnderROC: 0.9622623748396304
Root mean squared error: 0.09674401713151624
Relative absolute error: 15.6278636809163
Root relative squared error: 43.3252886835175
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.8885576257563476
Weighted FMeasure: 0.8969569298214212
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8744012262917928
Mean absolute error: 0.015584573199805867
Coverage of cases: 93.841642228739
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 76.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.012517074491017185
Kappa statistic: 0.8904210576059732
Training time: 7.0
		
Time end:Sun Oct 08 06.07.15 EEST 2017