Sun Oct 08 06.07.28 EEST 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.07.28 EEST 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.403200984918461
Incorrectly Classified Instances: 27.485380116959064
Correctly Classified Instances: 72.51461988304094
Weighted Precision: 0.7521097798697284
Weighted AreaUnderROC: 0.9724277272196916
Root mean squared error: 0.14900990870737477
Relative absolute error: 32.339580808840125
Root relative squared error: 66.73174737694933
Weighted TruePositiveRate: 0.7251461988304093
Weighted MatthewsCorrelation: 0.6964366682760215
Weighted FMeasure: 0.6961037437692786
Iteration time: 101.0
Weighted AreaUnderPRC: 0.8545994238357326
Mean absolute error: 0.03224999748250013
Coverage of cases: 87.42690058479532
Instances selection time: 100.0
Test time: 46.0
Accumulative iteration time: 101.0
Weighted Recall: 0.7251461988304093
Weighted FalsePositiveRate: 0.028990681862797858
Kappa statistic: 0.6970456580125335
Training time: 1.0
		
Time end:Sun Oct 08 06.07.28 EEST 2017