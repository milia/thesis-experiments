Sat Oct 07 11.27.10 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.10 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 41.66666666666662
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.38777983581441083
Weighted AreaUnderROC: 0.8025909811016542
Root mean squared error: 0.1936745771127835
Relative absolute error: 93.80073417010716
Root relative squared error: 96.92145831461703
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.3525599526287144
Weighted FMeasure: 0.40935396499872767
Iteration time: 699.0
Weighted AreaUnderPRC: 0.41131645856009635
Mean absolute error: 0.07491030853862717
Coverage of cases: 84.070796460177
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 699.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.11277340158295526
Kappa statistic: 0.3932885906040268
Training time: 695.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 54.166666666666586
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.6366764995083579
Weighted AreaUnderROC: 0.8859024911560794
Root mean squared error: 0.19338018001224738
Relative absolute error: 95.05354207162608
Root relative squared error: 96.7741318212129
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5934686191783952
Weighted FMeasure: 0.6236922543013121
Iteration time: 1617.0
Weighted AreaUnderPRC: 0.5766137868873898
Mean absolute error: 0.07591081484886797
Coverage of cases: 92.03539823008849
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 2316.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.059370460588899554
Kappa statistic: 0.5994776606659827
Training time: 1612.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 66.81415929203538
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6549879276684872
Weighted AreaUnderROC: 0.9150839348187525
Root mean squared error: 0.19368430404247403
Relative absolute error: 95.88126290836884
Root relative squared error: 96.92632600672502
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.6261543712199796
Weighted FMeasure: 0.6551222438878056
Iteration time: 981.0
Weighted AreaUnderPRC: 0.6253974473624972
Mean absolute error: 0.07657184190598891
Coverage of cases: 94.69026548672566
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 3297.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.051684627299813615
Kappa statistic: 0.6323324346936877
Training time: 976.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 75.0
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.7143064186741035
Weighted AreaUnderROC: 0.9363474448160355
Root mean squared error: 0.19358584295429646
Relative absolute error: 96.01983668932517
Root relative squared error: 96.87705267206397
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.7111368651475494
Weighted FMeasure: 0.7328464350356733
Iteration time: 1236.0
Weighted AreaUnderPRC: 0.7042204930461479
Mean absolute error: 0.07668250846716933
Coverage of cases: 96.46017699115045
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 4533.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.04047204772730256
Kappa statistic: 0.726519594154333
Training time: 1231.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 75.0
Incorrectly Classified Instances: 20.353982300884955
Correctly Classified Instances: 79.64601769911505
Weighted Precision: 0.7446762185700238
Weighted AreaUnderROC: 0.9463701163315185
Root mean squared error: 0.1934665188620249
Relative absolute error: 96.02340714306857
Root relative squared error: 96.81733876842503
Weighted TruePositiveRate: 0.7964601769911505
Weighted MatthewsCorrelation: 0.7420595035552726
Weighted FMeasure: 0.7623819441471886
Iteration time: 1367.0
Weighted AreaUnderPRC: 0.7287102528723836
Mean absolute error: 0.07668535987120051
Coverage of cases: 97.34513274336283
Instances selection time: 2.0
Test time: 18.0
Accumulative iteration time: 5900.0
Weighted Recall: 0.7964601769911505
Weighted FalsePositiveRate: 0.03367884381922184
Kappa statistic: 0.7599076212471132
Training time: 1365.0
		
Time end:Sat Oct 07 11.27.16 EEST 2017