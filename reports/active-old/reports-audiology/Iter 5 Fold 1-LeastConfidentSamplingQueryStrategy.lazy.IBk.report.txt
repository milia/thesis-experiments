Sat Oct 07 11.28.45 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.28.45 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 88.97492625368739
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.3848808969325971
Weighted AreaUnderROC: 0.6993085249141734
Root mean squared error: 0.1780735064217211
Relative absolute error: 76.0192273362881
Root relative squared error: 89.11414284147334
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.3236261237161699
Weighted FMeasure: 0.3682212124449985
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3527465494861667
Mean absolute error: 0.060709799608841125
Coverage of cases: 93.80530973451327
Instances selection time: 8.0
Test time: 9.0
Accumulative iteration time: 8.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.11280249495455763
Kappa statistic: 0.3345364741641337
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 79.75663716814161
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.567465445025819
Weighted AreaUnderROC: 0.7388536929689581
Root mean squared error: 0.16861910711145586
Relative absolute error: 62.39861429381646
Root relative squared error: 84.38283436361358
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.4599468804811759
Weighted FMeasure: 0.493503375743067
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4539500614538493
Mean absolute error: 0.04983222669297836
Coverage of cases: 88.49557522123894
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 16.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.0978579963859696
Kappa statistic: 0.4640994024471213
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 72.67699115044245
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.6542585527253154
Weighted AreaUnderROC: 0.8262012238847494
Root mean squared error: 0.15077245725190178
Relative absolute error: 50.68089356058574
Root relative squared error: 75.4517533915815
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5626593979602967
Weighted FMeasure: 0.5825958702064897
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5694791634671768
Mean absolute error: 0.04047432471852328
Coverage of cases: 88.49557522123894
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.08072905050241223
Kappa statistic: 0.5698106024555059
Training time: 1.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 69.39528023598821
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6966439551102712
Weighted AreaUnderROC: 0.8276141055867232
Root mean squared error: 0.14373583469423326
Relative absolute error: 44.48744961644615
Root relative squared error: 71.9303840406542
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6217285884573387
Weighted FMeasure: 0.6293956387813906
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5910136951425488
Mean absolute error: 0.03552817156868959
Coverage of cases: 86.72566371681415
Instances selection time: 2.0
Test time: 19.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.0657418584494948
Kappa statistic: 0.6363120030291556
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 60.7300884955752
Incorrectly Classified Instances: 27.43362831858407
Correctly Classified Instances: 72.56637168141593
Weighted Precision: 0.7313322197497107
Weighted AreaUnderROC: 0.8354419733953573
Root mean squared error: 0.13935812608730505
Relative absolute error: 40.14200823188055
Root relative squared error: 69.7396272124472
Weighted TruePositiveRate: 0.7256637168141593
Weighted MatthewsCorrelation: 0.6645497960170108
Weighted FMeasure: 0.6743599104348714
Iteration time: 2.0
Weighted AreaUnderPRC: 0.643269362260582
Mean absolute error: 0.03205785379629346
Coverage of cases: 86.72566371681415
Instances selection time: 1.0
Test time: 19.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7256637168141593
Weighted FalsePositiveRate: 0.05518491177438497
Kappa statistic: 0.6719423112942499
Training time: 1.0
		
Time end:Sat Oct 07 11.28.46 EEST 2017