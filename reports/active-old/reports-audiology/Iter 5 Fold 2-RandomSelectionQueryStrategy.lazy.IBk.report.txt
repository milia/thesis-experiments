Sat Oct 07 11.31.23 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.23 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 87.7949852507375
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.42666040940377226
Weighted AreaUnderROC: 0.7772727140876764
Root mean squared error: 0.17249979716184805
Relative absolute error: 73.781945300088
Root relative squared error: 86.32486591240072
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.32194275268267436
Weighted FMeasure: 0.3446879127153178
Iteration time: 1.0
Weighted AreaUnderPRC: 0.43921227581510686
Mean absolute error: 0.05892308131604236
Coverage of cases: 94.69026548672566
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.12095085044146325
Kappa statistic: 0.33711798656448105
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 80.27286135693215
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.5416182048040457
Weighted AreaUnderROC: 0.8077754352940361
Root mean squared error: 0.1656537218979392
Relative absolute error: 62.3644560835789
Root relative squared error: 82.89885301901373
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.4512746342582323
Weighted FMeasure: 0.47122579142689724
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5091149144147789
Mean absolute error: 0.04980494756674692
Coverage of cases: 93.80530973451327
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.08328285247513104
Kappa statistic: 0.4565800425413853
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 70.50147492625368
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.5926583484490372
Weighted AreaUnderROC: 0.784245950139043
Root mean squared error: 0.16535669026300925
Relative absolute error: 55.75474309090365
Root relative squared error: 82.75020811346053
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.4878692096282052
Weighted FMeasure: 0.5029235554289709
Iteration time: 0.0
Weighted AreaUnderPRC: 0.49733317617104716
Mean absolute error: 0.04452635732954101
Coverage of cases: 84.95575221238938
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.08167123606710248
Kappa statistic: 0.4881205509845613
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 62.721238938053084
Incorrectly Classified Instances: 34.51327433628319
Correctly Classified Instances: 65.48672566371681
Weighted Precision: 0.6597479216948243
Weighted AreaUnderROC: 0.8552284741307828
Root mean squared error: 0.15101713574042533
Relative absolute error: 47.729418382784004
Root relative squared error: 75.57419897158198
Weighted TruePositiveRate: 0.6548672566371682
Weighted MatthewsCorrelation: 0.5924588494486549
Weighted FMeasure: 0.6111622160120063
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6099269578864909
Mean absolute error: 0.038117243847362134
Coverage of cases: 89.38053097345133
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6548672566371682
Weighted FalsePositiveRate: 0.06288369239478434
Kappa statistic: 0.592057761732852
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 54.83038348082596
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.6948150033782516
Weighted AreaUnderROC: 0.8632078358811236
Root mean squared error: 0.1410719792222845
Relative absolute error: 42.13723291630997
Root relative squared error: 70.5972986097755
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6591569085497304
Weighted FMeasure: 0.6857845692698515
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6503129672122684
Mean absolute error: 0.033651262398441906
Coverage of cases: 89.38053097345133
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.04897773836208731
Kappa statistic: 0.6670962990241208
Training time: 0.0
		
Time end:Sat Oct 07 11.31.23 EEST 2017