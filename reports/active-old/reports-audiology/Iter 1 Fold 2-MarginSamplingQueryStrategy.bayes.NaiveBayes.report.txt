Sat Oct 07 11.28.52 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.28.52 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 8.222713864306789
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.3040884996637209
Weighted AreaUnderROC: 0.8450380706310103
Root mean squared error: 0.1920350022048468
Relative absolute error: 61.23484462865455
Root relative squared error: 96.10095831166235
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.27181091191306295
Weighted FMeasure: 0.33077402238409803
Iteration time: 23.0
Weighted AreaUnderPRC: 0.4423768081004401
Mean absolute error: 0.04890282730760614
Coverage of cases: 57.52212389380531
Instances selection time: 23.0
Test time: 30.0
Accumulative iteration time: 23.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.1605082762500842
Kappa statistic: 0.2958086239823097
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.112094395280236
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.3237833427477562
Weighted AreaUnderROC: 0.8992871224721334
Root mean squared error: 0.18325252067725453
Relative absolute error: 54.111111451936786
Root relative squared error: 91.70590073639922
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.3362420399763187
Weighted FMeasure: 0.37983029806038654
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5329318081398712
Mean absolute error: 0.04321373484008847
Coverage of cases: 61.06194690265487
Instances selection time: 18.0
Test time: 30.0
Accumulative iteration time: 42.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.1291738253197967
Kappa statistic: 0.3724090052563721
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.227138643067844
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.3833581879664332
Weighted AreaUnderROC: 0.9308765384768056
Root mean squared error: 0.17631787573994392
Relative absolute error: 49.812212062270554
Root relative squared error: 88.23556451448596
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.4057794236949556
Weighted FMeasure: 0.4396312848794573
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6522338204951768
Mean absolute error: 0.03978058602195224
Coverage of cases: 64.60176991150442
Instances selection time: 13.0
Test time: 30.0
Accumulative iteration time: 55.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.10026971921747434
Kappa statistic: 0.45289048126270937
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.264011799410033
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5217102517987473
Weighted AreaUnderROC: 0.9501854072928442
Root mean squared error: 0.16053811110468377
Relative absolute error: 42.352425990838896
Root relative squared error: 80.3388244099744
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5279654072629995
Weighted FMeasure: 0.5587292943045156
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7502776739498161
Mean absolute error: 0.033823117978795
Coverage of cases: 74.33628318584071
Instances selection time: 8.0
Test time: 30.0
Accumulative iteration time: 64.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.07222681865265461
Kappa statistic: 0.5593494388434468
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.264011799410033
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5524881852315481
Weighted AreaUnderROC: 0.9526196052815954
Root mean squared error: 0.15648424015154766
Relative absolute error: 40.70420159960126
Root relative squared error: 78.3101271464796
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5367005272860864
Weighted FMeasure: 0.5610323946462058
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7573055255259167
Mean absolute error: 0.03250682766634828
Coverage of cases: 77.87610619469027
Instances selection time: 3.0
Test time: 30.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.07169806782648144
Kappa statistic: 0.5596844706329595
Training time: 0.0
		
Time end:Sat Oct 07 11.28.52 EEST 2017