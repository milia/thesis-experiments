Sat Oct 07 11.27.21 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.21 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 12.205014749262528
Incorrectly Classified Instances: 58.4070796460177
Correctly Classified Instances: 41.5929203539823
Weighted Precision: 0.2842568630636986
Weighted AreaUnderROC: 0.7222388889347241
Root mean squared error: 0.19551216609112793
Relative absolute error: 66.93106816231094
Root relative squared error: 97.84105140845043
Weighted TruePositiveRate: 0.415929203539823
Weighted MatthewsCorrelation: 0.26329060617733213
Weighted FMeasure: 0.33442867924020014
Iteration time: 2.0
Weighted AreaUnderPRC: 0.31540502949205557
Mean absolute error: 0.0534518947129566
Coverage of cases: 65.48672566371681
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 2.0
Weighted Recall: 0.415929203539823
Weighted FalsePositiveRate: 0.11564738725659272
Kappa statistic: 0.28815500620406603
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 9.845132743362834
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.4633863360307921
Weighted AreaUnderROC: 0.8315698827760877
Root mean squared error: 0.16441589851794147
Relative absolute error: 49.805969328862766
Root relative squared error: 82.27940337872646
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.48123784966078836
Weighted FMeasure: 0.5116383775615198
Iteration time: 3.0
Weighted AreaUnderPRC: 0.46177113313483464
Mean absolute error: 0.039775600505688985
Coverage of cases: 78.76106194690266
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.06974495920069118
Kappa statistic: 0.5222660653889516
Training time: 2.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.849557522123888
Incorrectly Classified Instances: 32.743362831858406
Correctly Classified Instances: 67.2566371681416
Weighted Precision: 0.5482032716545991
Weighted AreaUnderROC: 0.8714968699614026
Root mean squared error: 0.15289747143249596
Relative absolute error: 40.1522294558078
Root relative squared error: 76.51518399973251
Weighted TruePositiveRate: 0.672566371681416
Weighted MatthewsCorrelation: 0.5613474072989081
Weighted FMeasure: 0.593297786654557
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5406522775731879
Mean absolute error: 0.032066016579290924
Coverage of cases: 86.72566371681415
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 12.0
Weighted Recall: 0.672566371681416
Weighted FalsePositiveRate: 0.0675614108438749
Kappa statistic: 0.6069749952998684
Training time: 4.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.78023598820059
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.537028536273721
Weighted AreaUnderROC: 0.8672793743223992
Root mean squared error: 0.15657076487510696
Relative absolute error: 40.43279536930944
Root relative squared error: 78.35342711136227
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.546906846794752
Weighted FMeasure: 0.5799394635617207
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5285564901863797
Mean absolute error: 0.032290079635212375
Coverage of cases: 76.99115044247787
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.06994780837793078
Kappa statistic: 0.5973368342085522
Training time: 3.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.222713864306776
Incorrectly Classified Instances: 25.663716814159294
Correctly Classified Instances: 74.33628318584071
Weighted Precision: 0.69668451451377
Weighted AreaUnderROC: 0.898973676920528
Root mean squared error: 0.13618381594537043
Relative absolute error: 33.44704233643421
Root relative squared error: 68.15109260617301
Weighted TruePositiveRate: 0.7433628318584071
Weighted MatthewsCorrelation: 0.6860711626779079
Weighted FMeasure: 0.7054568574829404
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6596910707492631
Mean absolute error: 0.026711179643680076
Coverage of cases: 85.84070796460178
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7433628318584071
Weighted FalsePositiveRate: 0.026130551878477522
Kappa statistic: 0.7019283245406586
Training time: 3.0
		
Time end:Sat Oct 07 11.27.21 EEST 2017