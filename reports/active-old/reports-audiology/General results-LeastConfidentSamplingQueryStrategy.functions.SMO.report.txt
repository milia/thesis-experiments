Sat Oct 07 11.27.44 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.44 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 40.00000000000001
Incorrectly Classified Instances: 46.19469026548673
Correctly Classified Instances: 53.80530973451327
Weighted Precision: 0.455207181015418
Weighted AreaUnderROC: 0.7907170602450662
Root mean squared error: 0.19397500698693754
Relative absolute error: 93.60823570580374
Root relative squared error: 97.07180381663576
Weighted TruePositiveRate: 0.5380530973451327
Weighted MatthewsCorrelation: 0.41708590030749393
Weighted FMeasure: 0.4639760668178837
Iteration time: 838.5
Weighted AreaUnderPRC: 0.4109695457350499
Mean absolute error: 0.07475657712616264
Coverage of cases: 82.0353982300885
Instances selection time: 5.3
Test time: 7.0
Accumulative iteration time: 838.5
Weighted Recall: 0.5380530973451327
Weighted FalsePositiveRate: 0.09335349390567932
Kappa statistic: 0.44069140569926396
Training time: 833.2
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 44.16666666666665
Incorrectly Classified Instances: 42.389380530973455
Correctly Classified Instances: 57.61061946902655
Weighted Precision: 0.49913016949470485
Weighted AreaUnderROC: 0.8225822059597151
Root mean squared error: 0.19413939536955765
Relative absolute error: 94.26030370393602
Root relative squared error: 97.15406945010736
Weighted TruePositiveRate: 0.5761061946902656
Weighted MatthewsCorrelation: 0.47045444087953403
Weighted FMeasure: 0.5083007755489789
Iteration time: 937.4
Weighted AreaUnderPRC: 0.4651070055405147
Mean absolute error: 0.07527732587467106
Coverage of cases: 83.09734513274336
Instances selection time: 4.4
Test time: 8.4
Accumulative iteration time: 1775.9
Weighted Recall: 0.5761061946902656
Weighted FalsePositiveRate: 0.07769860947095285
Kappa statistic: 0.4909190862188241
Training time: 933.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 48.75368731563424
Incorrectly Classified Instances: 37.43362831858407
Correctly Classified Instances: 62.56637168141593
Weighted Precision: 0.551898130019919
Weighted AreaUnderROC: 0.8487731122355745
Root mean squared error: 0.19403132727528197
Relative absolute error: 94.68872191867472
Root relative squared error: 97.09998843725256
Weighted TruePositiveRate: 0.6256637168141592
Weighted MatthewsCorrelation: 0.5297305968162818
Weighted FMeasure: 0.5592696442638083
Iteration time: 943.3
Weighted AreaUnderPRC: 0.5191870298670832
Mean absolute error: 0.07561946542116378
Coverage of cases: 85.84070796460176
Instances selection time: 4.2
Test time: 8.7
Accumulative iteration time: 2719.2
Weighted Recall: 0.6256637168141592
Weighted FalsePositiveRate: 0.06710344677742675
Kappa statistic: 0.5513767425005481
Training time: 939.1
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 52.92404129793512
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256636
Weighted Precision: 0.633490468211604
Weighted AreaUnderROC: 0.8810250068378895
Root mean squared error: 0.1937916306845031
Relative absolute error: 95.02053058854726
Root relative squared error: 96.98003597122602
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.6080523898486867
Weighted FMeasure: 0.6312178348001974
Iteration time: 1119.7
Weighted AreaUnderPRC: 0.59432261429319
Mean absolute error: 0.07588445151168699
Coverage of cases: 88.84955752212389
Instances selection time: 2.6
Test time: 10.2
Accumulative iteration time: 3838.9
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.05656223333014878
Kappa statistic: 0.6290856932795187
Training time: 1117.1
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 61.272123893805315
Incorrectly Classified Instances: 25.48672566371682
Correctly Classified Instances: 74.51327433628317
Weighted Precision: 0.6853692972864818
Weighted AreaUnderROC: 0.9138744553084089
Root mean squared error: 0.19354560095298043
Relative absolute error: 95.4662235779336
Root relative squared error: 96.85691418248425
Weighted TruePositiveRate: 0.7451327433628319
Weighted MatthewsCorrelation: 0.6769411458822651
Weighted FMeasure: 0.7014393441395171
Iteration time: 1840.9
Weighted AreaUnderPRC: 0.6675140629014612
Mean absolute error: 0.07624038688515525
Coverage of cases: 93.09734513274337
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 5679.8
Weighted Recall: 0.7451327433628319
Weighted FalsePositiveRate: 0.04263788873789964
Kappa statistic: 0.6979735201960995
Training time: 1837.9
		
Time end:Sat Oct 07 11.28.43 EEST 2017