Sat Oct 07 11.25.41 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.25.41 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.452802359882003
Incorrectly Classified Instances: 71.68141592920354
Correctly Classified Instances: 28.31858407079646
Weighted Precision: 0.29432972795804657
Weighted AreaUnderROC: 0.7997821906425161
Root mean squared error: 0.230841126175663
Relative absolute error: 73.92496442434461
Root relative squared error: 115.52088519550468
Weighted TruePositiveRate: 0.2831858407079646
Weighted MatthewsCorrelation: 0.10768157383876559
Weighted FMeasure: 0.16299381109375902
Iteration time: 34.0
Weighted AreaUnderPRC: 0.4398928030360764
Mean absolute error: 0.05903729797777516
Coverage of cases: 42.47787610619469
Instances selection time: 34.0
Test time: 36.0
Accumulative iteration time: 34.0
Weighted Recall: 0.2831858407079646
Weighted FalsePositiveRate: 0.23559826635237965
Kappa statistic: 0.05179736869367037
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.259587020648967
Incorrectly Classified Instances: 51.32743362831859
Correctly Classified Instances: 48.67256637168141
Weighted Precision: 0.3447832499679364
Weighted AreaUnderROC: 0.8929446770772731
Root mean squared error: 0.1855709616740333
Relative absolute error: 56.6512491685245
Root relative squared error: 92.86612881472541
Weighted TruePositiveRate: 0.48672566371681414
Weighted MatthewsCorrelation: 0.3345064471723375
Weighted FMeasure: 0.39470710364667855
Iteration time: 38.0
Weighted AreaUnderPRC: 0.5925976232077615
Mean absolute error: 0.04524231704430773
Coverage of cases: 67.2566371681416
Instances selection time: 38.0
Test time: 62.0
Accumulative iteration time: 72.0
Weighted Recall: 0.48672566371681414
Weighted FalsePositiveRate: 0.10731956725401175
Kappa statistic: 0.37449895018133234
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.665191740412984
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.4808484337687877
Weighted AreaUnderROC: 0.9280736923460081
Root mean squared error: 0.16613153173239908
Relative absolute error: 49.630037191353075
Root relative squared error: 83.13796558940521
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.44939917146191427
Weighted FMeasure: 0.48611111111111105
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6689376494806062
Mean absolute error: 0.03963509914587222
Coverage of cases: 73.45132743362832
Instances selection time: 27.0
Test time: 63.0
Accumulative iteration time: 99.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.09697313548070119
Kappa statistic: 0.4662103538031428
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 8.07522123893805
Incorrectly Classified Instances: 38.05309734513274
Correctly Classified Instances: 61.94690265486726
Weighted Precision: 0.5419222062152707
Weighted AreaUnderROC: 0.9388749566824325
Root mean squared error: 0.15767241900261678
Relative absolute error: 44.08122127558002
Root relative squared error: 78.9047329471013
Weighted TruePositiveRate: 0.6194690265486725
Weighted MatthewsCorrelation: 0.516290197180928
Weighted FMeasure: 0.5435008274245108
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6928797518365607
Mean absolute error: 0.035203753102025684
Coverage of cases: 76.10619469026548
Instances selection time: 19.0
Test time: 61.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6194690265486725
Weighted FalsePositiveRate: 0.07640615249687682
Kappa statistic: 0.5391691957511381
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.632743362831856
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5637968602049809
Weighted AreaUnderROC: 0.9391645223700453
Root mean squared error: 0.1557352531304546
Relative absolute error: 41.67947362861797
Root relative squared error: 77.93530813086468
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5403211526430135
Weighted FMeasure: 0.5661803226747562
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6956744709325325
Mean absolute error: 0.033285690745076826
Coverage of cases: 76.99115044247787
Instances selection time: 8.0
Test time: 59.0
Accumulative iteration time: 128.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.07332046872313333
Kappa statistic: 0.5600189933523266
Training time: 1.0
		
Time end:Sat Oct 07 11.25.42 EEST 2017