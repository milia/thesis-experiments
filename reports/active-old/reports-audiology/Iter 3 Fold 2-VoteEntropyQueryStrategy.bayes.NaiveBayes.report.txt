Sat Oct 07 11.31.28 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.28 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 7.411504424778759
Incorrectly Classified Instances: 71.68141592920354
Correctly Classified Instances: 28.31858407079646
Weighted Precision: 0.2687145592595057
Weighted AreaUnderROC: 0.8574584809605275
Root mean squared error: 0.22185447417641244
Relative absolute error: 73.0081528125745
Root relative squared error: 111.02365365320406
Weighted TruePositiveRate: 0.2831858407079646
Weighted MatthewsCorrelation: 0.1315667801233097
Weighted FMeasure: 0.17573849598836586
Iteration time: 25.0
Weighted AreaUnderPRC: 0.516315369444417
Mean absolute error: 0.058305122037819845
Coverage of cases: 42.47787610619469
Instances selection time: 22.0
Test time: 30.0
Accumulative iteration time: 25.0
Weighted Recall: 0.2831858407079646
Weighted FalsePositiveRate: 0.18093864969672865
Kappa statistic: 0.1076338110558643
Training time: 3.0
		
Time end:Sat Oct 07 11.31.28 EEST 2017