Sat Oct 07 11.27.38 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.38 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 12.094395280235993
Incorrectly Classified Instances: 51.32743362831859
Correctly Classified Instances: 48.67256637168141
Weighted Precision: 0.25326012806217746
Weighted AreaUnderROC: 0.764844361394617
Root mean squared error: 0.17764677520687408
Relative absolute error: 63.23644292093702
Root relative squared error: 88.90059177933651
Weighted TruePositiveRate: 0.48672566371681414
Weighted MatthewsCorrelation: 0.2990727411266837
Weighted FMeasure: 0.3301033121859889
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3138596013409896
Mean absolute error: 0.050501325943803764
Coverage of cases: 78.76106194690266
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.48672566371681414
Weighted FalsePositiveRate: 0.1309066759835908
Kappa statistic: 0.3508963058334158
Training time: 1.0
		
Time end:Sat Oct 07 11.27.38 EEST 2017