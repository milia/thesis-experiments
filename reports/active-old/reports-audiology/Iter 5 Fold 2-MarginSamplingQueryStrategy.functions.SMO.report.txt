Sat Oct 07 11.29.39 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.29.39 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 41.66666666666662
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.38777983581441083
Weighted AreaUnderROC: 0.8025909811016542
Root mean squared error: 0.1936745771127835
Relative absolute error: 93.80073417010716
Root relative squared error: 96.92145831461703
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.3525599526287144
Weighted FMeasure: 0.40935396499872767
Iteration time: 714.0
Weighted AreaUnderPRC: 0.41131645856009635
Mean absolute error: 0.07491030853862717
Coverage of cases: 84.070796460177
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 714.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.11277340158295526
Kappa statistic: 0.3932885906040268
Training time: 711.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 41.66666666666662
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.4478586311854046
Weighted AreaUnderROC: 0.8414673582202888
Root mean squared error: 0.19353027969005537
Relative absolute error: 93.73085282281144
Root relative squared error: 96.8492468924968
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.43880785300288955
Weighted FMeasure: 0.48313343976475576
Iteration time: 744.0
Weighted AreaUnderPRC: 0.4871178677258138
Mean absolute error: 0.07485450051821739
Coverage of cases: 84.070796460177
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1458.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.09341352646022594
Kappa statistic: 0.48175042996369194
Training time: 741.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 45.83333333333339
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.5305456920501168
Weighted AreaUnderROC: 0.8500696615058531
Root mean squared error: 0.19374213630132323
Relative absolute error: 94.2880128720839
Root relative squared error: 96.95526726968713
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.5056646233124944
Weighted FMeasure: 0.5470883099201682
Iteration time: 859.0
Weighted AreaUnderPRC: 0.5005878588667554
Mean absolute error: 0.07529945472423358
Coverage of cases: 84.95575221238938
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 2317.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.07089393517887595
Kappa statistic: 0.5353271028037382
Training time: 857.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6080288324293462
Weighted AreaUnderROC: 0.8800365945288915
Root mean squared error: 0.19385941327897988
Relative absolute error: 94.71774858462067
Root relative squared error: 97.01395672635543
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.600993998135501
Weighted FMeasure: 0.6256832987883392
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5830744100274527
Mean absolute error: 0.07564264643910669
Coverage of cases: 85.84070796460178
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 3347.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.05752215887263931
Kappa statistic: 0.6281147155618241
Training time: 1028.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 62.5
Incorrectly Classified Instances: 25.663716814159294
Correctly Classified Instances: 74.33628318584071
Weighted Precision: 0.7002668423022406
Weighted AreaUnderROC: 0.9202438190594173
Root mean squared error: 0.19351486877513324
Relative absolute error: 95.51936849160344
Root relative squared error: 96.84153473754861
Weighted TruePositiveRate: 0.7433628318584071
Weighted MatthewsCorrelation: 0.6807256785210049
Weighted FMeasure: 0.6985981070849779
Iteration time: 2354.0
Weighted AreaUnderPRC: 0.6766759691042961
Mean absolute error: 0.076282829003711
Coverage of cases: 93.80530973451327
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5701.0
Weighted Recall: 0.7433628318584071
Weighted FalsePositiveRate: 0.04349640542160994
Kappa statistic: 0.6949073643049996
Training time: 2353.0
		
Time end:Sat Oct 07 11.29.45 EEST 2017