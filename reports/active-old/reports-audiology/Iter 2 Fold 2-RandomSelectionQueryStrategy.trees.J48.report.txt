Sat Oct 07 11.31.24 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.24 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.563421828908553
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.424741578047148
Weighted AreaUnderROC: 0.7754605211347717
Root mean squared error: 0.17406160823444175
Relative absolute error: 47.710657945363536
Root relative squared error: 87.10645020200806
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.4597564831016994
Weighted FMeasure: 0.49308826866348987
Iteration time: 1.0
Weighted AreaUnderPRC: 0.43236593329476836
Mean absolute error: 0.03810226155358898
Coverage of cases: 69.02654867256638
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.09471459284022819
Kappa statistic: 0.5189163038219642
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 12.794985250737481
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.46793716545175773
Weighted AreaUnderROC: 0.8114222783510294
Root mean squared error: 0.1547136514832395
Relative absolute error: 46.892760952014484
Root relative squared error: 77.42406332557954
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5040684291023828
Weighted FMeasure: 0.5355063167751688
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4759546296227971
Mean absolute error: 0.03744907992695606
Coverage of cases: 76.10619469026548
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.08516900270400465
Kappa statistic: 0.5660105616898703
Training time: 2.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 6.637168141592915
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.6941308107008443
Weighted AreaUnderROC: 0.8578787862174229
Root mean squared error: 0.15681180730199212
Relative absolute error: 39.78453251250474
Root relative squared error: 78.47405308033387
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.6295383795068902
Weighted FMeasure: 0.6396178177905967
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5988085479700671
Mean absolute error: 0.03177236971484757
Coverage of cases: 76.10619469026548
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.02709269316444228
Kappa statistic: 0.614403735632184
Training time: 3.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 8.259587020648974
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.6595005965866182
Weighted AreaUnderROC: 0.8804409995636778
Root mean squared error: 0.1391125654065144
Relative absolute error: 35.47774785173784
Root relative squared error: 69.61674015291797
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6520882659434762
Weighted FMeasure: 0.676619665047664
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6234099186527996
Mean absolute error: 0.028332923631596223
Coverage of cases: 80.53097345132744
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.03616970228317346
Kappa statistic: 0.6685305710880924
Training time: 3.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.485250737463126
Incorrectly Classified Instances: 23.893805309734514
Correctly Classified Instances: 76.10619469026548
Weighted Precision: 0.7031913514214401
Weighted AreaUnderROC: 0.9087589536313957
Root mean squared error: 0.12657822499704355
Relative absolute error: 29.800452209070876
Root relative squared error: 63.344122602343425
Weighted TruePositiveRate: 0.7610619469026548
Weighted MatthewsCorrelation: 0.7027856025505813
Weighted FMeasure: 0.723990162716596
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6814997132664512
Mean absolute error: 0.023798972250299685
Coverage of cases: 85.84070796460178
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7610619469026548
Weighted FalsePositiveRate: 0.026488405593941354
Kappa statistic: 0.7224092439268492
Training time: 4.0
		
Time end:Sat Oct 07 11.31.24 EEST 2017