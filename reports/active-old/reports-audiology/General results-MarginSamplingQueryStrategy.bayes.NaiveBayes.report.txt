Sat Oct 07 11.28.51 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.28.51 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.692477876106194
Incorrectly Classified Instances: 64.60176991150442
Correctly Classified Instances: 35.39823008849557
Weighted Precision: 0.2263121506898813
Weighted AreaUnderROC: 0.8270362419197411
Root mean squared error: 0.21573476781448972
Relative absolute error: 68.30493795257254
Root relative squared error: 107.96114088618567
Weighted TruePositiveRate: 0.35398230088495575
Weighted MatthewsCorrelation: 0.16725176079258514
Weighted FMeasure: 0.2278743772918642
Iteration time: 23.8
Weighted AreaUnderPRC: 0.4784627593822971
Mean absolute error: 0.05454908239267943
Coverage of cases: 45.48672566371682
Instances selection time: 23.4
Test time: 31.1
Accumulative iteration time: 23.8
Weighted Recall: 0.35398230088495575
Weighted FalsePositiveRate: 0.1879496997085408
Kappa statistic: 0.1672079490089871
Training time: 0.4
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 7.879793510324481
Incorrectly Classified Instances: 54.424778761061944
Correctly Classified Instances: 45.575221238938056
Weighted Precision: 0.3176157469416714
Weighted AreaUnderROC: 0.8738730662500217
Root mean squared error: 0.19251255324447192
Relative absolute error: 58.70633290419174
Root relative squared error: 96.33994137216588
Weighted TruePositiveRate: 0.4557522123893805
Weighted MatthewsCorrelation: 0.2912329352453975
Weighted FMeasure: 0.3409776667950023
Iteration time: 18.1
Weighted AreaUnderPRC: 0.5688560057648205
Mean absolute error: 0.0468835297498753
Coverage of cases: 60.0
Instances selection time: 17.8
Test time: 30.4
Accumulative iteration time: 41.9
Weighted Recall: 0.4557522123893805
Weighted FalsePositiveRate: 0.1394229405953604
Kappa statistic: 0.3158200285033333
Training time: 0.3
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.662241887905606
Incorrectly Classified Instances: 44.15929203539823
Correctly Classified Instances: 55.840707964601776
Weighted Precision: 0.4290535545618429
Weighted AreaUnderROC: 0.9099538191897427
Root mean squared error: 0.17629799398786117
Relative absolute error: 51.02493220662427
Root relative squared error: 88.2256149979597
Weighted TruePositiveRate: 0.5584070796460177
Weighted MatthewsCorrelation: 0.4187543363411009
Weighted FMeasure: 0.45593155091930465
Iteration time: 13.1
Weighted AreaUnderPRC: 0.6375134364750135
Mean absolute error: 0.040749077803901294
Coverage of cases: 66.19469026548671
Instances selection time: 12.9
Test time: 30.9
Accumulative iteration time: 55.0
Weighted Recall: 0.5584070796460177
Weighted FalsePositiveRate: 0.10956923579392633
Kappa statistic: 0.4495706091975767
Training time: 0.2
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.662241887905603
Incorrectly Classified Instances: 39.557522123893804
Correctly Classified Instances: 60.4424778761062
Weighted Precision: 0.4943607879241275
Weighted AreaUnderROC: 0.936173667266921
Root mean squared error: 0.16480936973334384
Relative absolute error: 45.38653498465798
Root relative squared error: 82.4763099865535
Weighted TruePositiveRate: 0.6044247787610619
Weighted MatthewsCorrelation: 0.4825122408774507
Weighted FMeasure: 0.5134631616391261
Iteration time: 8.6
Weighted AreaUnderPRC: 0.6968364616256127
Mean absolute error: 0.036246191133581
Coverage of cases: 72.30088495575221
Instances selection time: 8.2
Test time: 30.9
Accumulative iteration time: 63.6
Weighted Recall: 0.6044247787610619
Weighted FalsePositiveRate: 0.09216960121026203
Kappa statistic: 0.5114829473218534
Training time: 0.4
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.735988200589968
Incorrectly Classified Instances: 34.7787610619469
Correctly Classified Instances: 65.22123893805309
Weighted Precision: 0.56785615643506
Weighted AreaUnderROC: 0.9524570502355438
Root mean squared error: 0.15208163185649098
Relative absolute error: 40.24845663073175
Root relative squared error: 76.1069096529599
Weighted TruePositiveRate: 0.6522123893805308
Weighted MatthewsCorrelation: 0.5525279316550941
Weighted FMeasure: 0.5811488946904783
Iteration time: 3.7
Weighted AreaUnderPRC: 0.7517598377458505
Mean absolute error: 0.032142864670376026
Coverage of cases: 79.20353982300885
Instances selection time: 3.2
Test time: 30.3
Accumulative iteration time: 67.3
Weighted Recall: 0.6522123893805308
Weighted FalsePositiveRate: 0.07541638242922172
Kappa statistic: 0.5754424246188085
Training time: 0.5
		
Time end:Sat Oct 07 11.28.55 EEST 2017