Sat Oct 07 11.31.22 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.22 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.2330383480827
Incorrectly Classified Instances: 55.75221238938053
Correctly Classified Instances: 44.24778761061947
Weighted Precision: 0.3883460482148307
Weighted AreaUnderROC: 0.7041512682030822
Root mean squared error: 0.18016106375249244
Relative absolute error: 76.16516443601068
Root relative squared error: 90.15882874620058
Weighted TruePositiveRate: 0.4424778761061947
Weighted MatthewsCorrelation: 0.3190558767190756
Weighted FMeasure: 0.34424618732896983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3467141512485996
Mean absolute error: 0.06082634659820292
Coverage of cases: 95.57522123893806
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1.0
Weighted Recall: 0.4424778761061947
Weighted FalsePositiveRate: 0.09699214737525777
Kappa statistic: 0.3358522250209908
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 81.01032448377582
Incorrectly Classified Instances: 48.67256637168141
Correctly Classified Instances: 51.32743362831859
Weighted Precision: 0.5849100997331086
Weighted AreaUnderROC: 0.7627923509539342
Root mean squared error: 0.17318266864438936
Relative absolute error: 64.63220296403624
Root relative squared error: 86.6665984253409
Weighted TruePositiveRate: 0.5132743362831859
Weighted MatthewsCorrelation: 0.4451666218945821
Weighted FMeasure: 0.4630919065774923
Iteration time: 0.0
Weighted AreaUnderPRC: 0.4962453797110006
Mean absolute error: 0.05161599542266778
Coverage of cases: 93.80530973451327
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 1.0
Weighted Recall: 0.5132743362831859
Weighted FalsePositiveRate: 0.08110068541323621
Kappa statistic: 0.42330889858030996
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 71.0914454277286
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.6552476815905506
Weighted AreaUnderROC: 0.7822406178731305
Root mean squared error: 0.16906567857148525
Relative absolute error: 57.4418535637919
Root relative squared error: 84.60631417078794
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.5203285366546573
Weighted FMeasure: 0.524563563879342
Iteration time: 1.0
Weighted AreaUnderPRC: 0.535060297738133
Mean absolute error: 0.045873702498861545
Coverage of cases: 90.26548672566372
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.06896751821632578
Kappa statistic: 0.48693476649369904
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 63.2005899705015
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.6647288028973293
Weighted AreaUnderROC: 0.8127526129946137
Root mean squared error: 0.15635268468066268
Relative absolute error: 50.14713725798893
Root relative squared error: 78.24429223785346
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5778363884167776
Weighted FMeasure: 0.5984059012141975
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6075132357994162
Mean absolute error: 0.0400480610046439
Coverage of cases: 85.84070796460178
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.07212144369315776
Kappa statistic: 0.5780039212025021
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 55.67846607669613
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6862621154656553
Weighted AreaUnderROC: 0.8472871932844469
Root mean squared error: 0.14854433510402826
Relative absolute error: 44.995515012805626
Root relative squared error: 74.33672398971409
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6339914872011794
Weighted FMeasure: 0.6605143419738958
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6448784725270928
Mean absolute error: 0.03593391823939335
Coverage of cases: 88.49557522123894
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.06121829705287831
Kappa statistic: 0.6424716173459891
Training time: 0.0
		
Time end:Sat Oct 07 11.31.22 EEST 2017