Sat Oct 07 11.29.50 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.29.50 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 18.646755162241885
Incorrectly Classified Instances: 54.86725663716815
Correctly Classified Instances: 45.13274336283185
Weighted Precision: 0.4003259396836255
Weighted AreaUnderROC: 0.783117430120641
Root mean squared error: 0.17432050295919155
Relative absolute error: 71.25275288773976
Root relative squared error: 87.23601007840891
Weighted TruePositiveRate: 0.4513274336283186
Weighted MatthewsCorrelation: 0.3291629330454856
Weighted FMeasure: 0.38679628291999907
Iteration time: 4.1
Weighted AreaUnderPRC: 0.4820719009791687
Mean absolute error: 0.056903240153403235
Coverage of cases: 75.39823008849558
Instances selection time: 1.4
Test time: 3.0
Accumulative iteration time: 4.1
Weighted Recall: 0.4513274336283186
Weighted FalsePositiveRate: 0.10959347588625039
Kappa statistic: 0.3382781203309143
Training time: 2.7
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 18.85324483775811
Incorrectly Classified Instances: 45.75221238938054
Correctly Classified Instances: 54.24778761061947
Weighted Precision: 0.4893000700172716
Weighted AreaUnderROC: 0.844441895173957
Root mean squared error: 0.16201809696420444
Relative absolute error: 65.14241842159063
Root relative squared error: 81.07946053232016
Weighted TruePositiveRate: 0.5424778761061948
Weighted MatthewsCorrelation: 0.43585790876392017
Weighted FMeasure: 0.4855913089271243
Iteration time: 6.9
Weighted AreaUnderPRC: 0.589597773741582
Mean absolute error: 0.052023459156131366
Coverage of cases: 83.6283185840708
Instances selection time: 1.7
Test time: 2.9
Accumulative iteration time: 11.0
Weighted Recall: 0.5424778761061948
Weighted FalsePositiveRate: 0.08968398527383309
Kappa statistic: 0.45080685938259357
Training time: 5.2
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 18.462389380530972
Incorrectly Classified Instances: 37.25663716814159
Correctly Classified Instances: 62.743362831858406
Weighted Precision: 0.5750817821266805
Weighted AreaUnderROC: 0.8867547549643346
Root mean squared error: 0.14961369512239678
Relative absolute error: 58.833197479739205
Root relative squared error: 74.87186873606488
Weighted TruePositiveRate: 0.6274336283185841
Weighted MatthewsCorrelation: 0.536400875212194
Weighted FMeasure: 0.5779845529581076
Iteration time: 7.9
Weighted AreaUnderPRC: 0.6922930181160761
Mean absolute error: 0.04698484520951392
Coverage of cases: 88.4070796460177
Instances selection time: 1.0
Test time: 2.8
Accumulative iteration time: 18.9
Weighted Recall: 0.6274336283185841
Weighted FalsePositiveRate: 0.07336717906971699
Kappa statistic: 0.5540250116825678
Training time: 6.9
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 18.252212389380535
Incorrectly Classified Instances: 33.53982300884956
Correctly Classified Instances: 66.46017699115045
Weighted Precision: 0.6270272750834339
Weighted AreaUnderROC: 0.9124075467499558
Root mean squared error: 0.14379523887009757
Relative absolute error: 56.21514310461306
Root relative squared error: 71.96011194526915
Weighted TruePositiveRate: 0.6646017699115043
Weighted MatthewsCorrelation: 0.5892124555311609
Weighted FMeasure: 0.6251260481070202
Iteration time: 10.1
Weighted AreaUnderPRC: 0.7431870134426096
Mean absolute error: 0.04489403789604512
Coverage of cases: 90.70796460176992
Instances selection time: 0.8
Test time: 3.7
Accumulative iteration time: 29.0
Weighted Recall: 0.6646017699115043
Weighted FalsePositiveRate: 0.05959952545413757
Kappa statistic: 0.6018339409571511
Training time: 9.3
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 17.433628318584073
Incorrectly Classified Instances: 27.256637168141594
Correctly Classified Instances: 72.74336283185842
Weighted Precision: 0.6819188204401904
Weighted AreaUnderROC: 0.9355671830916293
Root mean squared error: 0.13510923621037987
Relative absolute error: 51.805891391748574
Root relative squared error: 67.61333573305498
Weighted TruePositiveRate: 0.7274336283185839
Weighted MatthewsCorrelation: 0.6605024896169628
Weighted FMeasure: 0.69104450561823
Iteration time: 15.1
Weighted AreaUnderPRC: 0.7947771992910411
Mean absolute error: 0.041372760486465834
Coverage of cases: 92.38938053097345
Instances selection time: 1.4
Test time: 3.2
Accumulative iteration time: 44.1
Weighted Recall: 0.7274336283185839
Weighted FalsePositiveRate: 0.049366154935557814
Kappa statistic: 0.6765886689885767
Training time: 13.7
		
Time end:Sat Oct 07 11.29.52 EEST 2017