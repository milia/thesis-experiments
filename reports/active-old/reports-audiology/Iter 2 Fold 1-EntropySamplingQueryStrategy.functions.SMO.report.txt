Sat Oct 07 11.26.10 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.26.10 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 50.0
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.4563744081703477
Weighted AreaUnderROC: 0.8113860723653782
Root mean squared error: 0.19335334915351346
Relative absolute error: 94.4715000274834
Root relative squared error: 96.76070473132282
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3829648383799821
Weighted FMeasure: 0.4444510852112047
Iteration time: 1191.0
Weighted AreaUnderPRC: 0.3975161036184053
Mean absolute error: 0.07544598960528172
Coverage of cases: 90.26548672566372
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 1191.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.09550526453402272
Kappa statistic: 0.3973051370824366
Training time: 1186.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 58.333333333333414
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5733241786186551
Weighted AreaUnderROC: 0.8547656551983969
Root mean squared error: 0.1938635598310922
Relative absolute error: 95.50596383224328
Root relative squared error: 97.01603180447721
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5470396538036585
Weighted FMeasure: 0.5933096340960758
Iteration time: 939.0
Weighted AreaUnderPRC: 0.5259606379666225
Mean absolute error: 0.07627212389380524
Coverage of cases: 92.03539823008849
Instances selection time: 10.0
Test time: 10.0
Accumulative iteration time: 2130.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.06885123567387583
Kappa statistic: 0.5653030587352224
Training time: 929.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 70.83333333333331
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.6654666130329846
Weighted AreaUnderROC: 0.9061697949049927
Root mean squared error: 0.19386114840899807
Relative absolute error: 96.07165277564842
Root relative squared error: 97.0148250454412
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6449693341833891
Weighted FMeasure: 0.6757997982438687
Iteration time: 1286.0
Weighted AreaUnderPRC: 0.6060416055481785
Mean absolute error: 0.07672388936944129
Coverage of cases: 95.57522123893806
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 3416.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.049783592064608403
Kappa statistic: 0.6518207282913165
Training time: 1280.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 75.0
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.700888127636629
Weighted AreaUnderROC: 0.9260514584236745
Root mean squared error: 0.19344347634812256
Relative absolute error: 96.01325953769215
Root relative squared error: 96.80580749734168
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6678201916978755
Weighted FMeasure: 0.7017053696187371
Iteration time: 1700.0
Weighted AreaUnderPRC: 0.6747374206401724
Mean absolute error: 0.07667725588079564
Coverage of cases: 98.23008849557522
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 5116.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.04126101869840841
Kappa statistic: 0.6674636748206731
Training time: 1695.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 79.16666666666674
Incorrectly Classified Instances: 26.548672566371682
Correctly Classified Instances: 73.45132743362832
Weighted Precision: 0.7398608523521193
Weighted AreaUnderROC: 0.9250154763045878
Root mean squared error: 0.19348334016291366
Relative absolute error: 96.07387456714135
Root relative squared error: 96.82575672930146
Weighted TruePositiveRate: 0.7345132743362832
Weighted MatthewsCorrelation: 0.6975770461868522
Weighted FMeasure: 0.7231455365363836
Iteration time: 1717.0
Weighted AreaUnderPRC: 0.6790289154596997
Mean absolute error: 0.07672566371681411
Coverage of cases: 98.23008849557522
Instances selection time: 3.0
Test time: 23.0
Accumulative iteration time: 6833.0
Weighted Recall: 0.7345132743362832
Weighted FalsePositiveRate: 0.03505619014274433
Kappa statistic: 0.6899012074643249
Training time: 1714.0
		
Time end:Sat Oct 07 11.26.17 EEST 2017