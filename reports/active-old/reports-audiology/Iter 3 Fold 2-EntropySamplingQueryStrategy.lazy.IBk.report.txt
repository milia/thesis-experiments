Sat Oct 07 11.27.18 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.18 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.8230088495576
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.5541064563669412
Weighted AreaUnderROC: 0.7686583506864695
Root mean squared error: 0.16510349303731622
Relative absolute error: 71.83118873793732
Root relative squared error: 82.6234994626855
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.5017163330721913
Weighted FMeasure: 0.5426535439419354
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5041725815009053
Mean absolute error: 0.057365185450436
Coverage of cases: 94.69026548672566
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.056461151174699145
Kappa statistic: 0.5059209327746401
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 80.4203539823009
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.5986888448219729
Weighted AreaUnderROC: 0.8282340929569997
Root mean squared error: 0.15169502312876765
Relative absolute error: 58.00342788841299
Root relative squared error: 75.91343727136648
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5812927454250918
Weighted FMeasure: 0.6134074635057059
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5877747456151642
Mean absolute error: 0.046322181994218656
Coverage of cases: 96.46017699115045
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.053865679917600884
Kappa statistic: 0.6021126760563381
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 71.09144542772859
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.5772257925489936
Weighted AreaUnderROC: 0.8263326608456347
Root mean squared error: 0.15316271668132606
Relative absolute error: 50.79588826924097
Root relative squared error: 76.64792189807143
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5536526725792907
Weighted FMeasure: 0.5911477522739387
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5798330940322527
Mean absolute error: 0.040566160770574344
Coverage of cases: 92.03539823008849
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.06691527658767674
Kappa statistic: 0.5780433159073936
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 61.799410029498524
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.6189831684886343
Weighted AreaUnderROC: 0.8345029139126643
Root mean squared error: 0.1519184055010525
Relative absolute error: 46.68904857478728
Root relative squared error: 76.02522553808886
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5890821951449862
Weighted FMeasure: 0.6219926519384128
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5944681678660559
Mean absolute error: 0.03728639295903147
Coverage of cases: 88.49557522123894
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.05369112924798798
Kappa statistic: 0.6044219253800093
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 55.75221238938053
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.6961703973624849
Weighted AreaUnderROC: 0.869533004890952
Root mean squared error: 0.1375402160438144
Relative absolute error: 40.30994910249331
Root relative squared error: 68.82988213838259
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6621216276931109
Weighted FMeasure: 0.6906387333694541
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6785019281991648
Mean absolute error: 0.03219197324157449
Coverage of cases: 90.26548672566372
Instances selection time: 5.0
Test time: 20.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.04516986955775374
Kappa statistic: 0.6670349907918969
Training time: 0.0
		
Time end:Sat Oct 07 11.27.18 EEST 2017