Sat Oct 07 11.27.16 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.16 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 88.97492625368736
Incorrectly Classified Instances: 53.097345132743364
Correctly Classified Instances: 46.902654867256636
Weighted Precision: 0.37790267850520154
Weighted AreaUnderROC: 0.7166847172881974
Root mean squared error: 0.17428748666724225
Relative absolute error: 74.06226939464298
Root relative squared error: 87.21948758376051
Weighted TruePositiveRate: 0.4690265486725664
Weighted MatthewsCorrelation: 0.32367465821002084
Weighted FMeasure: 0.3952536207776078
Iteration time: 56.0
Weighted AreaUnderPRC: 0.3567436251751113
Mean absolute error: 0.059146951252666356
Coverage of cases: 96.46017699115045
Instances selection time: 55.0
Test time: 54.0
Accumulative iteration time: 56.0
Weighted Recall: 0.4690265486725664
Weighted FalsePositiveRate: 0.12716625077022944
Kappa statistic: 0.3412999125619353
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 79.97787610619473
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.504418295568738
Weighted AreaUnderROC: 0.743157241689657
Root mean squared error: 0.16992788176395043
Relative absolute error: 63.10546849036964
Root relative squared error: 85.03779047512792
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.42223239249210603
Weighted FMeasure: 0.4676682209587607
Iteration time: 17.0
Weighted AreaUnderPRC: 0.42813177627034
Mean absolute error: 0.05039672830828138
Coverage of cases: 85.84070796460178
Instances selection time: 14.0
Test time: 15.0
Accumulative iteration time: 73.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.09566590177793406
Kappa statistic: 0.44287475111406077
Training time: 3.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 68.62094395280235
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.6313405534888249
Weighted AreaUnderROC: 0.7772916628606765
Root mean squared error: 0.1591788734737237
Relative absolute error: 53.12865291837564
Root relative squared error: 79.6586148783329
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.5213054779798124
Weighted FMeasure: 0.5482205110523695
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5265507162767799
Mean absolute error: 0.04242913253898062
Coverage of cases: 84.95575221238938
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 79.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.08090751631136828
Kappa statistic: 0.5315620878085547
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 58.99705014749262
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.6352479518459647
Weighted AreaUnderROC: 0.8356024604859633
Root mean squared error: 0.14751435376819766
Relative absolute error: 45.68454696759689
Root relative squared error: 73.82128569836098
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5664706445278728
Weighted FMeasure: 0.5968592698886095
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6262448646918364
Mean absolute error: 0.03648418681440035
Coverage of cases: 88.49557522123894
Instances selection time: 5.0
Test time: 19.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.06859227899042815
Kappa statistic: 0.5769770706598034
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 55.641592920353965
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.7051110957409761
Weighted AreaUnderROC: 0.9075118023712833
Root mean squared error: 0.1273206968102842
Relative absolute error: 37.588812723991204
Root relative squared error: 63.71568118256365
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6516831624637464
Weighted FMeasure: 0.6790052304107483
Iteration time: 9.0
Weighted AreaUnderPRC: 0.785587858343192
Mean absolute error: 0.03001884349485413
Coverage of cases: 94.69026548672566
Instances selection time: 8.0
Test time: 32.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.0536909270960963
Kappa statistic: 0.6545622973598888
Training time: 1.0
		
Time end:Sat Oct 07 11.27.16 EEST 2017