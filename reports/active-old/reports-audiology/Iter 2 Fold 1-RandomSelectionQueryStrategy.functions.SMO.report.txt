Sat Oct 07 11.30.12 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.30.12 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 50.0
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.5280952214388152
Weighted AreaUnderROC: 0.8230857773355544
Root mean squared error: 0.1937597561309893
Relative absolute error: 94.6693783323256
Root relative squared error: 96.96408484198777
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.4418102061600256
Weighted FMeasure: 0.4909796483074846
Iteration time: 441.0
Weighted AreaUnderPRC: 0.4489598840430719
Mean absolute error: 0.07560401741817678
Coverage of cases: 88.49557522123894
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 441.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.06079219992864752
Kappa statistic: 0.41247833622183705
Training time: 441.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 58.333333333333414
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6588555913113435
Weighted AreaUnderROC: 0.8834133720829075
Root mean squared error: 0.19387950837047827
Relative absolute error: 95.51365909965344
Root relative squared error: 97.02401300530552
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.6260148592141669
Weighted FMeasure: 0.6511957857131382
Iteration time: 708.0
Weighted AreaUnderPRC: 0.5865194943928145
Mean absolute error: 0.07627826941986221
Coverage of cases: 91.15044247787611
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 1149.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.043642824064623754
Kappa statistic: 0.623646960865945
Training time: 708.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 66.740412979351
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6716086273608398
Weighted AreaUnderROC: 0.9169646335851177
Root mean squared error: 0.19357830008897062
Relative absolute error: 95.83173015262258
Root relative squared error: 96.87327796131903
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.6317210945997727
Weighted FMeasure: 0.6572909897941644
Iteration time: 992.0
Weighted AreaUnderPRC: 0.6291604100728855
Mean absolute error: 0.07653228449688618
Coverage of cases: 95.57522123893806
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 2141.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.05026613411271033
Kappa statistic: 0.6314759597465524
Training time: 991.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 75.0
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6910145179015559
Weighted AreaUnderROC: 0.9249238208384283
Root mean squared error: 0.1936124134919276
Relative absolute error: 96.03351716472147
Root relative squared error: 96.8903494882688
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6571295344640701
Weighted FMeasure: 0.6829909648682325
Iteration time: 1238.0
Weighted AreaUnderPRC: 0.6581626859075889
Mean absolute error: 0.07669343384682627
Coverage of cases: 96.46017699115045
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 3379.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.03658313717082533
Kappa statistic: 0.6463223787167449
Training time: 1237.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 75.0
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.7545714277138493
Weighted AreaUnderROC: 0.9405874497701119
Root mean squared error: 0.19350475022075392
Relative absolute error: 95.98221538198452
Root relative squared error: 96.83647106289861
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.7358273467205606
Weighted FMeasure: 0.7504182796188034
Iteration time: 1251.0
Weighted AreaUnderPRC: 0.7330870451831252
Mean absolute error: 0.07665246367311272
Coverage of cases: 96.46017699115045
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 4630.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.02581302558984526
Kappa statistic: 0.7293413173652694
Training time: 1250.0
		
Time end:Sat Oct 07 11.30.17 EEST 2017