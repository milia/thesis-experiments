Sat Oct 07 11.28.48 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.28.48 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 9.255162241887916
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.40993108215787777
Weighted AreaUnderROC: 0.7550656309606638
Root mean squared error: 0.1830507629746422
Relative absolute error: 55.814366472311896
Root relative squared error: 91.60493420245916
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.41815969430065825
Weighted FMeasure: 0.4521235599692871
Iteration time: 3.0
Weighted AreaUnderPRC: 0.38272011718833787
Mean absolute error: 0.04457397322441571
Coverage of cases: 63.716814159292035
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.0946248476499823
Kappa statistic: 0.45756528417818737
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 11.430678466076687
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.494016740049697
Weighted AreaUnderROC: 0.7863553708168082
Root mean squared error: 0.1628570886986698
Relative absolute error: 48.214348175871876
Root relative squared error: 81.49932102010602
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.4882719457270038
Weighted FMeasure: 0.5132340456147924
Iteration time: 9.0
Weighted AreaUnderPRC: 0.4554191297390249
Mean absolute error: 0.03850451416823098
Coverage of cases: 73.45132743362832
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.10157051236523246
Kappa statistic: 0.5157300087659491
Training time: 2.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.628318584070788
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5190084883510926
Weighted AreaUnderROC: 0.8146502046641839
Root mean squared error: 0.16465503589227973
Relative absolute error: 42.82965975924811
Root relative squared error: 82.39907599350074
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5218221581908323
Weighted FMeasure: 0.5418361821634945
Iteration time: 3.0
Weighted AreaUnderPRC: 0.4820320908772584
Mean absolute error: 0.03420424216884395
Coverage of cases: 75.22123893805309
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.08154104397144073
Kappa statistic: 0.5569051262433053
Training time: 2.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.264011799410027
Incorrectly Classified Instances: 32.743362831858406
Correctly Classified Instances: 67.2566371681416
Weighted Precision: 0.5999578592498946
Weighted AreaUnderROC: 0.8687629083350187
Root mean squared error: 0.14787648042476223
Relative absolute error: 36.38893370899481
Root relative squared error: 74.00250640462025
Weighted TruePositiveRate: 0.672566371681416
Weighted MatthewsCorrelation: 0.5846501700987181
Weighted FMeasure: 0.6147635255599857
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6005579484705975
Mean absolute error: 0.02906060678148889
Coverage of cases: 80.53097345132744
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 19.0
Weighted Recall: 0.672566371681416
Weighted FalsePositiveRate: 0.059181132657172175
Kappa statistic: 0.6093618611604223
Training time: 4.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 6.489675516224188
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6238125039866405
Weighted AreaUnderROC: 0.8852317539572307
Root mean squared error: 0.14556444263737914
Relative absolute error: 34.91452756554722
Root relative squared error: 72.84548271378665
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.6077652101359061
Weighted FMeasure: 0.6365352641458836
Iteration time: 12.0
Weighted AreaUnderPRC: 0.627026972281586
Mean absolute error: 0.02788312965304116
Coverage of cases: 83.1858407079646
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.055045458516994286
Kappa statistic: 0.6318532998231406
Training time: 8.0
		
Time end:Sat Oct 07 11.28.48 EEST 2017