Sat Oct 07 11.27.49 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.49 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 37.5
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.49034688670569104
Weighted AreaUnderROC: 0.8244267362604225
Root mean squared error: 0.19357466751419322
Relative absolute error: 93.19103838858395
Root relative squared error: 96.87146009523607
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4796841551124824
Weighted FMeasure: 0.5181535742844224
Iteration time: 669.0
Weighted AreaUnderPRC: 0.43778135317201533
Mean absolute error: 0.07442339871310534
Coverage of cases: 81.41592920353982
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 669.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.07855765763587454
Kappa statistic: 0.5079515335100341
Training time: 665.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 41.66666666666662
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.4891811965748592
Weighted AreaUnderROC: 0.8329440044820606
Root mean squared error: 0.19396173899937053
Relative absolute error: 93.94049686469842
Root relative squared error: 97.06516405666794
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.48636135292972765
Weighted FMeasure: 0.5314475851737146
Iteration time: 895.0
Weighted AreaUnderPRC: 0.4817902508673331
Mean absolute error: 0.07502192457944677
Coverage of cases: 82.30088495575221
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1564.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.07411529208354337
Kappa statistic: 0.5223558143903814
Training time: 892.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 45.83333333333339
Incorrectly Classified Instances: 32.743362831858406
Correctly Classified Instances: 67.2566371681416
Weighted Precision: 0.5844570952978033
Weighted AreaUnderROC: 0.8547371305578222
Root mean squared error: 0.19366502948381129
Relative absolute error: 94.25070248463862
Root relative squared error: 96.91668035079115
Weighted TruePositiveRate: 0.672566371681416
Weighted MatthewsCorrelation: 0.577938955478492
Weighted FMeasure: 0.6052427838997334
Iteration time: 1089.0
Weighted AreaUnderPRC: 0.5475712256641768
Mean absolute error: 0.07526965823426013
Coverage of cases: 85.84070796460178
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2653.0
Weighted Recall: 0.672566371681416
Weighted FalsePositiveRate: 0.0587915586730189
Kappa statistic: 0.608593896274106
Training time: 1086.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.548672566371682
Correctly Classified Instances: 73.45132743362832
Weighted Precision: 0.6644788593903638
Weighted AreaUnderROC: 0.8980016737800692
Root mean squared error: 0.19305266007163127
Relative absolute error: 94.32638927059857
Root relative squared error: 96.61022951278989
Weighted TruePositiveRate: 0.7345132743362832
Weighted MatthewsCorrelation: 0.6567981965880184
Weighted FMeasure: 0.6815927573263391
Iteration time: 1526.0
Weighted AreaUnderPRC: 0.6299078015763877
Mean absolute error: 0.07533010254249202
Coverage of cases: 89.38053097345133
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4179.0
Weighted Recall: 0.7345132743362832
Weighted FalsePositiveRate: 0.0505890060276782
Kappa statistic: 0.6835324869305452
Training time: 1524.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 50.18436578171091
Incorrectly Classified Instances: 23.893805309734514
Correctly Classified Instances: 76.10619469026548
Weighted Precision: 0.6949288564983516
Weighted AreaUnderROC: 0.9062224190282262
Root mean squared error: 0.1932434483285736
Relative absolute error: 94.72909002592343
Root relative squared error: 96.70570655664261
Weighted TruePositiveRate: 0.7610619469026548
Weighted MatthewsCorrelation: 0.6894210130641473
Weighted FMeasure: 0.7123684786259723
Iteration time: 2317.0
Weighted AreaUnderPRC: 0.6660028192995902
Mean absolute error: 0.0756517038401473
Coverage of cases: 90.26548672566372
Instances selection time: 5.0
Test time: 19.0
Accumulative iteration time: 6496.0
Weighted Recall: 0.7610619469026548
Weighted FalsePositiveRate: 0.046573325756710186
Kappa statistic: 0.7154183378416192
Training time: 2312.0
		
Time end:Sat Oct 07 11.27.56 EEST 2017