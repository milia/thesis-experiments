Sat Oct 07 11.31.28 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.28 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.268436578171098
Incorrectly Classified Instances: 62.83185840707964
Correctly Classified Instances: 37.16814159292036
Weighted Precision: 0.1751051791672711
Weighted AreaUnderROC: 0.794544735028287
Root mean squared error: 0.21238752519782528
Relative absolute error: 65.34216383519554
Root relative squared error: 106.28606488717607
Weighted TruePositiveRate: 0.37168141592920356
Weighted MatthewsCorrelation: 0.1572570789054859
Weighted FMeasure: 0.23804792216529524
Iteration time: 24.0
Weighted AreaUnderPRC: 0.43279744382932867
Mean absolute error: 0.05218297806282984
Coverage of cases: 49.557522123893804
Instances selection time: 23.0
Test time: 29.0
Accumulative iteration time: 24.0
Weighted Recall: 0.37168141592920356
Weighted FalsePositiveRate: 0.19083604407259575
Kappa statistic: 0.1772969647251846
Training time: 1.0
		
Time end:Sat Oct 07 11.31.28 EEST 2017