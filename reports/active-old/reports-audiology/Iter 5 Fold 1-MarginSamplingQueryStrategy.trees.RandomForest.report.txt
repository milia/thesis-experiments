Sat Oct 07 11.29.52 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.29.52 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 21.570796460177004
Incorrectly Classified Instances: 61.94690265486726
Correctly Classified Instances: 38.05309734513274
Weighted Precision: 0.2408840751992251
Weighted AreaUnderROC: 0.7525213298469277
Root mean squared error: 0.18013011114494254
Relative absolute error: 74.33159939970257
Root relative squared error: 90.14333899061621
Weighted TruePositiveRate: 0.3805309734513274
Weighted MatthewsCorrelation: 0.20736348595666831
Weighted FMeasure: 0.2771017699115044
Iteration time: 4.0
Weighted AreaUnderPRC: 0.42733696918854813
Mean absolute error: 0.059362041187262404
Coverage of cases: 69.91150442477876
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 4.0
Weighted Recall: 0.3805309734513274
Weighted FalsePositiveRate: 0.15066095777580046
Kappa statistic: 0.2279160566129819
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 20.169616519174053
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.5306920075832436
Weighted AreaUnderROC: 0.8480455133375406
Root mean squared error: 0.1620297333945544
Relative absolute error: 66.40729957635942
Root relative squared error: 81.08528380461486
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.4529884876054601
Weighted FMeasure: 0.511970080265102
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6102520805422378
Mean absolute error: 0.05303360730056475
Coverage of cases: 83.1858407079646
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.0951500829614533
Kappa statistic: 0.46713194378949346
Training time: 5.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 19.948377581120933
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5577115608089059
Weighted AreaUnderROC: 0.863581789973853
Root mean squared error: 0.1533518457118199
Relative absolute error: 61.45101489211212
Root relative squared error: 76.74256860761048
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.510207999610484
Weighted FMeasure: 0.5604470993580427
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6830348974009798
Mean absolute error: 0.04907546328189504
Coverage of cases: 85.84070796460178
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.06632296742809785
Kappa statistic: 0.517944913289437
Training time: 7.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 18.768436578171094
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6938053097345134
Weighted AreaUnderROC: 0.9143176299986348
Root mean squared error: 0.14827280917297103
Relative absolute error: 58.70141949116948
Root relative squared error: 74.20084302071638
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.6302726895499919
Weighted FMeasure: 0.6532695152151136
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7273139343706951
Mean absolute error: 0.04687960584364224
Coverage of cases: 92.03539823008849
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 28.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.054592622764630554
Kappa statistic: 0.6202744329319517
Training time: 9.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 18.51032448377582
Incorrectly Classified Instances: 27.43362831858407
Correctly Classified Instances: 72.56637168141593
Weighted Precision: 0.671906454949561
Weighted AreaUnderROC: 0.9211692997401946
Root mean squared error: 0.13780945014914836
Relative absolute error: 52.35649943186148
Root relative squared error: 68.96461619850554
Weighted TruePositiveRate: 0.7256637168141593
Weighted MatthewsCorrelation: 0.6522669941865139
Weighted FMeasure: 0.6844121265161524
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7598638204127504
Mean absolute error: 0.04181248218516711
Coverage of cases: 90.26548672566372
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7256637168141593
Weighted FalsePositiveRate: 0.0548884976326662
Kappa statistic: 0.6720651563377644
Training time: 12.0
		
Time end:Sat Oct 07 11.29.52 EEST 2017