Sat Oct 07 11.27.29 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.29 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 50.0
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.4563744081703477
Weighted AreaUnderROC: 0.8113860723653782
Root mean squared error: 0.19335334915351346
Relative absolute error: 94.4715000274834
Root relative squared error: 96.76070473132282
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3829648383799821
Weighted FMeasure: 0.4444510852112047
Iteration time: 1234.0
Weighted AreaUnderPRC: 0.3975161036184053
Mean absolute error: 0.07544598960528172
Coverage of cases: 90.26548672566372
Instances selection time: 17.0
Test time: 15.0
Accumulative iteration time: 1234.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.09550526453402272
Kappa statistic: 0.3973051370824366
Training time: 1217.0
		
Time end:Sat Oct 07 11.27.31 EEST 2017