Sat Oct 07 11.31.32 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.32 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 45.83333333333339
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5687274377539864
Weighted AreaUnderROC: 0.8049596534400018
Root mean squared error: 0.19422035793671438
Relative absolute error: 94.52120279361549
Root relative squared error: 97.19458591951036
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.5012970945049244
Weighted FMeasure: 0.5316370404032505
Iteration time: 377.0
Weighted AreaUnderPRC: 0.4779464022748916
Mean absolute error: 0.07548568278656786
Coverage of cases: 84.95575221238938
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 377.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.0741841067553217
Kappa statistic: 0.5111905209704721
Training time: 374.0
		
Time end:Sat Oct 07 11.31.33 EEST 2017