Sat Oct 07 11.29.55 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.29.55 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 8.628318584070794
Incorrectly Classified Instances: 60.176991150442475
Correctly Classified Instances: 39.823008849557525
Weighted Precision: 0.18041515750359335
Weighted AreaUnderROC: 0.8892625242955926
Root mean squared error: 0.20025653008304098
Relative absolute error: 63.37507470899402
Root relative squared error: 100.21529527528375
Weighted TruePositiveRate: 0.39823008849557523
Weighted MatthewsCorrelation: 0.21655882427483544
Weighted FMeasure: 0.24644470159382983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5829565399648652
Mean absolute error: 0.05061203883009927
Coverage of cases: 57.52212389380531
Instances selection time: 1.0
Test time: 57.0
Accumulative iteration time: 1.0
Weighted Recall: 0.39823008849557523
Weighted FalsePositiveRate: 0.12874598417423502
Kappa statistic: 0.26860841423948223
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.591445427728608
Incorrectly Classified Instances: 52.21238938053097
Correctly Classified Instances: 47.78761061946903
Weighted Precision: 0.38851292729481174
Weighted AreaUnderROC: 0.9203173227962232
Root mean squared error: 0.18395574954628338
Relative absolute error: 55.679876797323544
Root relative squared error: 92.0578207897758
Weighted TruePositiveRate: 0.4778761061946903
Weighted MatthewsCorrelation: 0.3376217544963617
Weighted FMeasure: 0.3788494835909834
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6340677370848418
Mean absolute error: 0.044466568275640224
Coverage of cases: 69.02654867256638
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 2.0
Weighted Recall: 0.4778761061946903
Weighted FalsePositiveRate: 0.1140444166518501
Kappa statistic: 0.35733564680933105
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.849557522123893
Incorrectly Classified Instances: 48.67256637168141
Correctly Classified Instances: 51.32743362831859
Weighted Precision: 0.46380757885182666
Weighted AreaUnderROC: 0.9366406839207884
Root mean squared error: 0.1770405290709956
Relative absolute error: 53.16651538655018
Root relative squared error: 88.59720524062352
Weighted TruePositiveRate: 0.5132743362831859
Weighted MatthewsCorrelation: 0.3998128633466368
Weighted FMeasure: 0.435580441914862
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6843736185039008
Mean absolute error: 0.04245936992675872
Coverage of cases: 76.10619469026548
Instances selection time: 0.0
Test time: 31.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5132743362831859
Weighted FalsePositiveRate: 0.104705877810018
Kappa statistic: 0.407418001525553
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 9.181415929203537
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6219010547334527
Weighted AreaUnderROC: 0.9596359232916722
Root mean squared error: 0.14456468521496466
Relative absolute error: 41.45872572818333
Root relative squared error: 72.34516951426511
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.6008356661105738
Weighted FMeasure: 0.62217525514894
Iteration time: 1.0
Weighted AreaUnderPRC: 0.767236698130316
Mean absolute error: 0.03310939901903522
Coverage of cases: 82.30088495575221
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.07097905144387427
Kappa statistic: 0.6226145038167938
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.407079646017698
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.5921453064046012
Weighted AreaUnderROC: 0.9566825499482335
Root mean squared error: 0.15037003315238195
Relative absolute error: 41.71339919625763
Root relative squared error: 75.25036645082835
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5584762643672279
Weighted FMeasure: 0.5824576153513572
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7489038465167565
Mean absolute error: 0.03331278408034456
Coverage of cases: 84.070796460177
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.0789834619545871
Kappa statistic: 0.5658855167114867
Training time: 1.0
		
Time end:Sat Oct 07 11.29.56 EEST 2017