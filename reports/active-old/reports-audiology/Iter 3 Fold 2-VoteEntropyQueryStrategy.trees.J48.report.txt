Sat Oct 07 11.31.36 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.36 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 11.578171091445414
Incorrectly Classified Instances: 41.5929203539823
Correctly Classified Instances: 58.4070796460177
Weighted Precision: 0.45536592218008143
Weighted AreaUnderROC: 0.758517135907783
Root mean squared error: 0.16280179599365935
Relative absolute error: 54.80569449788382
Root relative squared error: 81.47165063773745
Weighted TruePositiveRate: 0.584070796460177
Weighted MatthewsCorrelation: 0.4571839030747186
Weighted FMeasure: 0.4905271915891385
Iteration time: 8.0
Weighted AreaUnderPRC: 0.42916516152802664
Mean absolute error: 0.043768436578171054
Coverage of cases: 72.56637168141593
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.584070796460177
Weighted FalsePositiveRate: 0.09767541231198386
Kappa statistic: 0.4868103198376655
Training time: 7.0
		
Time end:Sat Oct 07 11.31.37 EEST 2017