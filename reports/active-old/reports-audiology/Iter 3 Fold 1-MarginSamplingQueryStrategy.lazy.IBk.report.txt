Sat Oct 07 11.29.46 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.29.46 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.23303834808269
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.39981732680847726
Weighted AreaUnderROC: 0.7592329340604417
Root mean squared error: 0.1725059511861159
Relative absolute error: 73.34897034913476
Root relative squared error: 86.32794559903505
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.3714232959284068
Weighted FMeasure: 0.4165037570719918
Iteration time: 4.0
Weighted AreaUnderPRC: 0.396898481623544
Mean absolute error: 0.05857730270937839
Coverage of cases: 93.80530973451327
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.08809916906363491
Kappa statistic: 0.40565417488494415
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 81.67404129793512
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.6175094816687736
Weighted AreaUnderROC: 0.8076648503176302
Root mean squared error: 0.159391692249182
Relative absolute error: 59.28981755280332
Root relative squared error: 79.765116755769
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.5379095496381663
Weighted FMeasure: 0.561406311240382
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5253316817618846
Mean absolute error: 0.04734950707341927
Coverage of cases: 90.26548672566372
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.05894124303532197
Kappa statistic: 0.5300803992237316
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 73.26696165191737
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6930162551746363
Weighted AreaUnderROC: 0.8534615965340226
Root mean squared error: 0.14002981233498804
Relative absolute error: 48.25301329650323
Root relative squared error: 70.07576224692517
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6499013190365429
Weighted FMeasure: 0.6735397707752129
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6729013763638871
Mean absolute error: 0.038535392563179616
Coverage of cases: 93.80530973451327
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.040957959442483434
Kappa statistic: 0.6472640470069776
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 61.76253687315632
Incorrectly Classified Instances: 24.778761061946902
Correctly Classified Instances: 75.22123893805309
Weighted Precision: 0.7031973261280914
Weighted AreaUnderROC: 0.8819360816785825
Root mean squared error: 0.13107757688847177
Relative absolute error: 40.43343077020912
Root relative squared error: 65.59575393821001
Weighted TruePositiveRate: 0.7522123893805309
Weighted MatthewsCorrelation: 0.6849695444544969
Weighted FMeasure: 0.7161813297322147
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7101918726813256
Mean absolute error: 0.03229058707343086
Coverage of cases: 92.92035398230088
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7522123893805309
Weighted FalsePositiveRate: 0.0490028120206286
Kappa statistic: 0.7052356996459848
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 55.789085545722706
Incorrectly Classified Instances: 23.893805309734514
Correctly Classified Instances: 76.10619469026548
Weighted Precision: 0.7114611388995769
Weighted AreaUnderROC: 0.8964427168661913
Root mean squared error: 0.12532472193465077
Relative absolute error: 36.209863900931275
Root relative squared error: 62.71682630656683
Weighted TruePositiveRate: 0.7610619469026548
Weighted MatthewsCorrelation: 0.6975046458124837
Weighted FMeasure: 0.7272748746051425
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7423937587819692
Mean absolute error: 0.028917599643104804
Coverage of cases: 93.80530973451327
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7610619469026548
Weighted FalsePositiveRate: 0.0458184546762006
Kappa statistic: 0.7163443659352919
Training time: 0.0
		
Time end:Sat Oct 07 11.29.46 EEST 2017