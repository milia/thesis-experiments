Sat Oct 07 11.27.31 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.27.31 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 29.166666666666707
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.45105070665228825
Weighted AreaUnderROC: 0.787205309898991
Root mean squared error: 0.19273878077239545
Relative absolute error: 91.16253504094999
Root relative squared error: 96.45315345319443
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.435584636156426
Weighted FMeasure: 0.4601332631360095
Iteration time: 615.0
Weighted AreaUnderPRC: 0.42175093423591065
Mean absolute error: 0.07280341340075851
Coverage of cases: 79.64601769911505
Instances selection time: 9.0
Test time: 10.0
Accumulative iteration time: 615.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.09504659798981088
Kappa statistic: 0.47921267402784445
Training time: 606.0
		
Time end:Sat Oct 07 11.27.31 EEST 2017