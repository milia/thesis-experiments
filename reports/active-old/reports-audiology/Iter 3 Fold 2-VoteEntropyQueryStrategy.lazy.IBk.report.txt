Sat Oct 07 11.31.36 EEST 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.31.36 EEST 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.8230088495576
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.5541064563669412
Weighted AreaUnderROC: 0.7686583506864695
Root mean squared error: 0.16510349303731622
Relative absolute error: 71.83118873793732
Root relative squared error: 82.6234994626855
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.5017163330721913
Weighted FMeasure: 0.5426535439419354
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5041725815009053
Mean absolute error: 0.057365185450436
Coverage of cases: 94.69026548672566
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.056461151174699145
Kappa statistic: 0.5059209327746401
Training time: 1.0
		
Time end:Sat Oct 07 11.31.36 EEST 2017