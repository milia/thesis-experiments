Sun Oct 08 09.52.05 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.05 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 44.2080378250591
Correctly Classified Instances: 55.7919621749409
Weighted Precision: 0.5583465349811292
Weighted AreaUnderROC: 0.7047706032614203
Root mean squared error: 0.4597611385226379
Relative absolute error: 60.810229959166115
Root relative squared error: 106.17728683558953
Weighted TruePositiveRate: 0.557919621749409
Weighted MatthewsCorrelation: 0.40990824532243614
Weighted FMeasure: 0.5483952221938915
Iteration time: 28.0
Weighted AreaUnderPRC: 0.4527863733353577
Mean absolute error: 0.2280383623468729
Coverage of cases: 55.7919621749409
Instances selection time: 28.0
Test time: 27.0
Accumulative iteration time: 28.0
Weighted Recall: 0.557919621749409
Weighted FalsePositiveRate: 0.14837841522656836
Kappa statistic: 0.4100242401640873
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.31678486997636
Correctly Classified Instances: 57.68321513002364
Weighted Precision: 0.5791219014095128
Weighted AreaUnderROC: 0.7172605442945218
Root mean squared error: 0.4516687772620395
Relative absolute error: 58.03636575898156
Root relative squared error: 104.30843605471506
Weighted TruePositiveRate: 0.5768321513002365
Weighted MatthewsCorrelation: 0.4357715090607614
Weighted FMeasure: 0.5740390514189737
Iteration time: 29.0
Weighted AreaUnderPRC: 0.479762705536057
Mean absolute error: 0.21763637159618085
Coverage of cases: 57.68321513002364
Instances selection time: 29.0
Test time: 38.0
Accumulative iteration time: 57.0
Weighted Recall: 0.5768321513002365
Weighted FalsePositiveRate: 0.14231106271119287
Kappa statistic: 0.4353185967424379
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5697062238084797
Weighted AreaUnderROC: 0.7063254792549707
Root mean squared error: 0.46170411967106406
Relative absolute error: 59.92169030732861
Root relative squared error: 106.62599911121922
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.41684670539889745
Weighted FMeasure: 0.5647005843784493
Iteration time: 22.0
Weighted AreaUnderPRC: 0.4724109819527246
Mean absolute error: 0.22470633865248227
Coverage of cases: 56.02836879432624
Instances selection time: 22.0
Test time: 35.0
Accumulative iteration time: 79.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14763272943332084
Kappa statistic: 0.4134722905577672
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5744655939224808
Weighted AreaUnderROC: 0.7064215048382242
Root mean squared error: 0.4626586268393654
Relative absolute error: 59.746981023577085
Root relative squared error: 106.8464330994441
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.4191062990353386
Weighted FMeasure: 0.5651527405000628
Iteration time: 31.0
Weighted AreaUnderPRC: 0.47612930162900174
Mean absolute error: 0.22405117883841408
Coverage of cases: 56.02836879432624
Instances selection time: 31.0
Test time: 42.0
Accumulative iteration time: 110.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14744067826681415
Kappa statistic: 0.41360343439763886
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.73522458628842
Correctly Classified Instances: 56.26477541371158
Weighted Precision: 0.5764280953572262
Weighted AreaUnderROC: 0.7080588790308034
Root mean squared error: 0.4621426631387096
Relative absolute error: 59.306165334534064
Root relative squared error: 106.72727638685781
Weighted TruePositiveRate: 0.5626477541371159
Weighted MatthewsCorrelation: 0.4222506236731446
Weighted FMeasure: 0.5652716050197827
Iteration time: 28.0
Weighted AreaUnderPRC: 0.4757056460291695
Mean absolute error: 0.22239812000450274
Coverage of cases: 56.26477541371158
Instances selection time: 28.0
Test time: 44.0
Accumulative iteration time: 138.0
Weighted Recall: 0.5626477541371159
Weighted FalsePositiveRate: 0.1465299960755091
Kappa statistic: 0.416821301616401
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.026004728132385
Correctly Classified Instances: 56.973995271867615
Weighted Precision: 0.5819735152576966
Weighted AreaUnderROC: 0.7128508273558747
Root mean squared error: 0.45895279619077434
Relative absolute error: 58.275069999832546
Root relative squared error: 105.99060817043001
Weighted TruePositiveRate: 0.5697399527186762
Weighted MatthewsCorrelation: 0.4312242878154191
Weighted FMeasure: 0.5691576728801846
Iteration time: 26.0
Weighted AreaUnderPRC: 0.47849599514602925
Mean absolute error: 0.21853151249937205
Coverage of cases: 56.973995271867615
Instances selection time: 26.0
Test time: 46.0
Accumulative iteration time: 164.0
Weighted Recall: 0.5697399527186762
Weighted FalsePositiveRate: 0.14403829800692666
Kappa statistic: 0.4263594772215848
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.5931972163863459
Weighted AreaUnderROC: 0.7272631055303505
Root mean squared error: 0.4479157669656726
Relative absolute error: 55.405528277868996
Root relative squared error: 103.44171545276352
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.45504790511096815
Weighted FMeasure: 0.5853559961186167
Iteration time: 14.0
Weighted AreaUnderPRC: 0.49458543481256667
Mean absolute error: 0.20777073104200874
Coverage of cases: 59.1016548463357
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 178.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.1364903374026563
Kappa statistic: 0.45512419585418146
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.5995679368986913
Weighted AreaUnderROC: 0.7338576574750827
Root mean squared error: 0.44307638054852
Relative absolute error: 54.09011101433658
Root relative squared error: 102.32410703250125
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.4663735846562238
Weighted FMeasure: 0.5909990944427759
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5033191067132793
Mean absolute error: 0.2028379163037622
Coverage of cases: 60.047281323877066
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 192.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.13275749828860534
Kappa statistic: 0.4681145510144863
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.189125295508276
Correctly Classified Instances: 59.810874704491724
Weighted Precision: 0.5975437760778382
Weighted AreaUnderROC: 0.7323782832803332
Root mean squared error: 0.44469466524938805
Relative absolute error: 54.3341213553982
Root relative squared error: 102.69783387556988
Weighted TruePositiveRate: 0.5981087470449172
Weighted MatthewsCorrelation: 0.4634225246924514
Weighted FMeasure: 0.5876388427600177
Iteration time: 16.0
Weighted AreaUnderPRC: 0.49870265792340535
Mean absolute error: 0.20375295508274327
Coverage of cases: 59.810874704491724
Instances selection time: 16.0
Test time: 41.0
Accumulative iteration time: 208.0
Weighted Recall: 0.5981087470449172
Weighted FalsePositiveRate: 0.13335218048425107
Kappa statistic: 0.46509465540967754
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6099829576046963
Weighted AreaUnderROC: 0.7448994110518248
Root mean squared error: 0.43436514627162265
Relative absolute error: 51.79422038742459
Root relative squared error: 100.31233365060501
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.48568484590269617
Weighted FMeasure: 0.6044927317629446
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5186260173821986
Mean absolute error: 0.19422832645284221
Coverage of cases: 61.702127659574465
Instances selection time: 16.0
Test time: 33.0
Accumulative iteration time: 224.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12722245449209532
Kappa statistic: 0.4901680691025154
Training time: 0.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.627093212411117
Weighted AreaUnderROC: 0.7600436195675147
Root mean squared error: 0.42096293959173997
Relative absolute error: 48.63518956308539
Root relative squared error: 97.21722659685891
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5138634464162343
Weighted FMeasure: 0.6286594492584974
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5444743470236603
Mean absolute error: 0.1823819608615702
Coverage of cases: 64.06619385342789
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 236.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12057469939924957
Kappa statistic: 0.5209548715885469
Training time: 0.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6373567493587454
Weighted AreaUnderROC: 0.7695573188901311
Root mean squared error: 0.4127556163286332
Relative absolute error: 46.72152118960622
Root relative squared error: 95.32182647874652
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5310421107748442
Weighted FMeasure: 0.6378446214571403
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5542697474571454
Mean absolute error: 0.17520570446102332
Coverage of cases: 65.48463356973996
Instances selection time: 10.0
Test time: 36.0
Accumulative iteration time: 247.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11573169791713708
Kappa statistic: 0.5398371197162634
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6407775561387741
Weighted AreaUnderROC: 0.7726775384718798
Root mean squared error: 0.41007912127579765
Relative absolute error: 46.05604566683979
Root relative squared error: 94.70371642305079
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.536745287892477
Weighted FMeasure: 0.6412991892977185
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5585729010501576
Mean absolute error: 0.17271017125064922
Coverage of cases: 65.95744680851064
Instances selection time: 10.0
Test time: 41.0
Accumulative iteration time: 257.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11421939114134669
Kappa statistic: 0.5460967540016095
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6364311687577701
Weighted AreaUnderROC: 0.7710550425406777
Root mean squared error: 0.4116424613539519
Relative absolute error: 46.32935699211085
Root relative squared error: 95.0647543490337
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5327279276746503
Weighted FMeasure: 0.6377378273677696
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5535455940471985
Mean absolute error: 0.17373508872041568
Coverage of cases: 65.72104018912529
Instances selection time: 20.0
Test time: 49.0
Accumulative iteration time: 277.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11510031680989756
Kappa statistic: 0.5428833340786119
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.656369062907724
Weighted AreaUnderROC: 0.780523915718888
Root mean squared error: 0.40316107858781286
Relative absolute error: 44.42474389282929
Root relative squared error: 93.10606289978145
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5543882292829007
Weighted FMeasure: 0.658643799666603
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5657672212416845
Mean absolute error: 0.16659278959810983
Coverage of cases: 67.13947990543736
Instances selection time: 7.0
Test time: 40.0
Accumulative iteration time: 284.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11034696761659751
Kappa statistic: 0.5617299134596032
Training time: 0.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6774527958401927
Weighted AreaUnderROC: 0.7915350729797187
Root mean squared error: 0.39298823982228775
Relative absolute error: 42.20954887767804
Root relative squared error: 90.75674641990202
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5785954773240839
Weighted FMeasure: 0.6816433100312734
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5823426724951659
Mean absolute error: 0.15828580829129263
Coverage of cases: 68.79432624113475
Instances selection time: 5.0
Test time: 42.0
Accumulative iteration time: 289.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10487311645191
Kappa statistic: 0.5836955630279669
Training time: 0.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6719513627168815
Weighted AreaUnderROC: 0.7851810028901908
Root mean squared error: 0.39899684079882847
Relative absolute error: 43.42851403760837
Root relative squared error: 92.14437337640555
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5676212559604539
Weighted FMeasure: 0.6746730120460583
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5780240624991635
Mean absolute error: 0.16285692764103138
Coverage of cases: 67.84869976359339
Instances selection time: 3.0
Test time: 47.0
Accumulative iteration time: 292.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10812499185555209
Kappa statistic: 0.5710514935055251
Training time: 0.0
		
Time end:Sun Oct 08 09.52.07 EEST 2017