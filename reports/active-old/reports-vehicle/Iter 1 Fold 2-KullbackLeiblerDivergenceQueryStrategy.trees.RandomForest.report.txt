Sun Oct 08 09.52.54 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.54 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 64.00709219858156
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6175890141150967
Weighted AreaUnderROC: 0.8331689860677621
Root mean squared error: 0.35480491093373434
Relative absolute error: 62.726556343577535
Root relative squared error: 81.93868433495709
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.49403595338630024
Weighted FMeasure: 0.6163575892563481
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6355790837170006
Mean absolute error: 0.23522458628841575
Coverage of cases: 95.74468085106383
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 23.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.12658675357535584
Kappa statistic: 0.49900929622787676
Training time: 17.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 51.41843971631206
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7327696834861528
Weighted AreaUnderROC: 0.9097093096709841
Root mean squared error: 0.2948333664088761
Relative absolute error: 44.63356973995272
Root relative squared error: 68.08884938489928
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6417888082693739
Weighted FMeasure: 0.7127239841717543
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7656074076327356
Mean absolute error: 0.1673758865248227
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08927741305361371
Kappa statistic: 0.6441541038525962
Training time: 83.0
		
Time end:Sun Oct 08 09.52.54 EEST 2017