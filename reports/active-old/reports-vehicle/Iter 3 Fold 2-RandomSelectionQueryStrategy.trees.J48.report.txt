Sun Oct 08 09.55.49 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.49 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 47.163120567375884
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.6072178821270675
Weighted AreaUnderROC: 0.8030779397061103
Root mean squared error: 0.39809502756489407
Relative absolute error: 55.174678224323635
Root relative squared error: 91.93610853105724
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.459085883855962
Weighted FMeasure: 0.5856674786658762
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5763991816778756
Mean absolute error: 0.20690504334121362
Coverage of cases: 81.56028368794327
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.13764741912468806
Kappa statistic: 0.4514600008943343
Training time: 12.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 43.2033096926714
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6150279044557087
Weighted AreaUnderROC: 0.7576775299906758
Root mean squared error: 0.41315611058447604
Relative absolute error: 53.32160497506075
Root relative squared error: 95.41431666531442
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.4915422228599699
Weighted FMeasure: 0.6167353136519806
Iteration time: 6.0
Weighted AreaUnderPRC: 0.560140581399049
Mean absolute error: 0.1999560186564778
Coverage of cases: 73.5224586288416
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.12656864918180993
Kappa statistic: 0.4960235903851309
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 37.64775413711584
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5906560902602241
Weighted AreaUnderROC: 0.7836742911871091
Root mean squared error: 0.4110265385345547
Relative absolute error: 56.85954444819669
Root relative squared error: 94.9225130668021
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.44945559563706666
Weighted FMeasure: 0.5855971353627115
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5480565587129972
Mean absolute error: 0.21322329168073756
Coverage of cases: 75.65011820330969
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.13905404513375041
Kappa statistic: 0.44784658302627056
Training time: 7.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 38.652482269503544
Incorrectly Classified Instances: 41.607565011820334
Correctly Classified Instances: 58.392434988179666
Weighted Precision: 0.5958715771323799
Weighted AreaUnderROC: 0.7535703867754969
Root mean squared error: 0.4289414635681335
Relative absolute error: 57.806831116523796
Root relative squared error: 99.05978778306158
Weighted TruePositiveRate: 0.5839243498817966
Weighted MatthewsCorrelation: 0.4491674103135741
Weighted FMeasure: 0.5866548310395276
Iteration time: 8.0
Weighted AreaUnderPRC: 0.548234685117414
Mean absolute error: 0.21677561668696424
Coverage of cases: 74.94089834515367
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 36.0
Weighted Recall: 0.5839243498817966
Weighted FalsePositiveRate: 0.13936096085163785
Kappa statistic: 0.4456465892760076
Training time: 8.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 36.170212765957444
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.6485785586567301
Weighted AreaUnderROC: 0.8032252915893429
Root mean squared error: 0.39482614858093806
Relative absolute error: 49.76790305395492
Root relative squared error: 91.18119326652311
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5212029720161664
Weighted FMeasure: 0.6423242458492932
Iteration time: 10.0
Weighted AreaUnderPRC: 0.591992457339127
Mean absolute error: 0.18662963645233097
Coverage of cases: 79.66903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.12184643459929607
Kappa statistic: 0.5173393592267765
Training time: 9.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.505910165484636
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.5998774083956242
Weighted AreaUnderROC: 0.7778044696228356
Root mean squared error: 0.4177517435715725
Relative absolute error: 53.22220201652823
Root relative squared error: 96.47563264219318
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.4553634451383856
Weighted FMeasure: 0.5923480640895438
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5546806090576261
Mean absolute error: 0.19958325756198086
Coverage of cases: 73.04964539007092
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 57.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.13869472682205236
Kappa statistic: 0.4507395412008775
Training time: 11.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 30.7919621749409
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.6167004269011213
Weighted AreaUnderROC: 0.7642713031341651
Root mean squared error: 0.4172818328997543
Relative absolute error: 51.07433674809558
Root relative squared error: 96.36711142104544
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.4907829959987784
Weighted FMeasure: 0.6159489702773862
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5355656978620753
Mean absolute error: 0.19152876280535844
Coverage of cases: 69.03073286052009
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 69.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.1274666909638421
Kappa statistic: 0.4922688098291236
Training time: 11.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 40.780141843971634
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6406370515988491
Weighted AreaUnderROC: 0.7550137869628495
Root mean squared error: 0.3980677421713505
Relative absolute error: 48.89506945410124
Root relative squared error: 91.92980723933431
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5364305862976547
Weighted FMeasure: 0.6482297125787339
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5748490833266446
Mean absolute error: 0.18335651045287965
Coverage of cases: 76.59574468085107
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 83.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11470035958035765
Kappa statistic: 0.5459377259614309
Training time: 13.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 38.94799054373522
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6567368182851693
Weighted AreaUnderROC: 0.8197298682596372
Root mean squared error: 0.3772078687540529
Relative absolute error: 46.85961267521552
Root relative squared error: 87.11242582623899
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5541736613319657
Weighted FMeasure: 0.6591281432688886
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6178170276382048
Mean absolute error: 0.1757235475320582
Coverage of cases: 83.451536643026
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 96.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11020258485191868
Kappa statistic: 0.5618834013889302
Training time: 13.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 34.692671394799056
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6589150175162154
Weighted AreaUnderROC: 0.7912926728138407
Root mean squared error: 0.38677945451148527
Relative absolute error: 45.733474102268524
Root relative squared error: 89.32288887168905
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5574942416020281
Weighted FMeasure: 0.664060393606982
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6151773941040957
Mean absolute error: 0.17150052788350695
Coverage of cases: 76.12293144208037
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 112.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10917096296684747
Kappa statistic: 0.5651811186675506
Training time: 15.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 32.505910165484636
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6876664120357913
Weighted AreaUnderROC: 0.8283959677910645
Root mean squared error: 0.363083127103954
Relative absolute error: 41.82347024415868
Root relative squared error: 83.85045646867158
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5868046067657047
Weighted FMeasure: 0.6889607943431474
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6533791743042663
Mean absolute error: 0.15683801341559506
Coverage of cases: 80.1418439716312
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 128.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10309074205994288
Kappa statistic: 0.5904276712696072
Training time: 16.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 36.82033096926714
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6810785010613156
Weighted AreaUnderROC: 0.8229806764749568
Root mean squared error: 0.37155501378261063
Relative absolute error: 43.60455381704145
Root relative squared error: 85.80695489045816
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.577753104112833
Weighted FMeasure: 0.680259573796106
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6348767770488535
Mean absolute error: 0.16351707681390543
Coverage of cases: 81.32387706855792
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 145.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10507696727781748
Kappa statistic: 0.5811226351175275
Training time: 17.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 30.91016548463357
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6988648774271286
Weighted AreaUnderROC: 0.825290108124888
Root mean squared error: 0.3688090734273286
Relative absolute error: 41.13308762707463
Root relative squared error: 85.17280712913785
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5936776112096381
Weighted FMeasure: 0.6928265278893395
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6348349593024125
Mean absolute error: 0.15424907860152987
Coverage of cases: 80.1418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 164.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10273278807613107
Kappa statistic: 0.593502536558475
Training time: 18.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 30.73286052009456
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7193133099748693
Weighted AreaUnderROC: 0.8294863744538821
Root mean squared error: 0.3581999392706129
Relative absolute error: 38.87392637922999
Root relative squared error: 82.72273254463839
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6240789539838478
Weighted FMeasure: 0.717279125516241
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6506921873734085
Mean absolute error: 0.14577722392211245
Coverage of cases: 79.90543735224587
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 183.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09450268857831087
Kappa statistic: 0.624980443285528
Training time: 19.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 36.5839243498818
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7055038853607157
Weighted AreaUnderROC: 0.8358604737279395
Root mean squared error: 0.35571974323942274
Relative absolute error: 40.859707685654335
Root relative squared error: 82.14995580613811
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6142242177899182
Weighted FMeasure: 0.7068041345001269
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6639880194668208
Mean absolute error: 0.15322390382120377
Coverage of cases: 83.451536643026
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 204.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09512954314390851
Kappa statistic: 0.6189955112887738
Training time: 21.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 31.44208037825059
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.7213372934506878
Weighted AreaUnderROC: 0.8365778728387966
Root mean squared error: 0.35736124563710037
Relative absolute error: 39.06996680755547
Root relative squared error: 82.52904454660795
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6184973916836041
Weighted FMeasure: 0.7141527563136378
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6636006855002591
Mean absolute error: 0.146512375528333
Coverage of cases: 80.61465721040189
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 226.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09759240699649926
Kappa statistic: 0.6151936111670359
Training time: 22.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 30.08274231678487
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6998306767371122
Weighted AreaUnderROC: 0.8374567814102896
Root mean squared error: 0.37045865547802215
Relative absolute error: 40.3643663927352
Root relative squared error: 85.55376178554518
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5945395645213172
Weighted FMeasure: 0.6962307283427951
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6530409638718605
Mean absolute error: 0.15136637397275698
Coverage of cases: 78.4869976359338
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 249.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10295678590908225
Kappa statistic: 0.5929263616492723
Training time: 22.0
		
Time end:Sun Oct 08 09.55.50 EEST 2017