Sun Oct 08 09.52.50 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.50 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.06382978723404
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6424808984868988
Weighted AreaUnderROC: 0.8179224824023698
Root mean squared error: 0.3708232062261038
Relative absolute error: 77.69897557131587
Root relative squared error: 85.63795117456047
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5288777501228472
Weighted FMeasure: 0.6086038327071875
Iteration time: 56.0
Weighted AreaUnderPRC: 0.580679356227237
Mean absolute error: 0.2913711583924345
Coverage of cases: 96.21749408983452
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 56.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11580193790082363
Kappa statistic: 0.5400633029231054
Training time: 54.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6303253701629961
Weighted AreaUnderROC: 0.8113976688801269
Root mean squared error: 0.37570527321913494
Relative absolute error: 78.43446283162595
Root relative squared error: 86.76541625161178
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5022753198556545
Weighted FMeasure: 0.6150764342468202
Iteration time: 120.0
Weighted AreaUnderPRC: 0.5471534796296547
Mean absolute error: 0.29412923561859733
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 176.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12470360575223355
Kappa statistic: 0.5026973131041051
Training time: 119.0
		
Time end:Sun Oct 08 09.52.50 EEST 2017