Sun Oct 08 09.55.48 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.48 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 33.21513002364066
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6589585634702272
Weighted AreaUnderROC: 0.771562883866426
Root mean squared error: 0.4083025751912438
Relative absolute error: 51.79931704754396
Root relative squared error: 94.29344067899281
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.5068548744232757
Weighted FMeasure: 0.6298087705316151
Iteration time: 5.0
Weighted AreaUnderPRC: 0.576552484611879
Mean absolute error: 0.19424743892828986
Coverage of cases: 69.50354609929079
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12875178744362048
Kappa statistic: 0.4892294390363888
Training time: 4.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 33.21513002364066
Incorrectly Classified Instances: 39.2434988179669
Correctly Classified Instances: 60.7565011820331
Weighted Precision: 0.6352382674322827
Weighted AreaUnderROC: 0.7442234607031933
Root mean squared error: 0.4247420762807444
Relative absolute error: 54.163383241397355
Root relative squared error: 98.08998083073934
Weighted TruePositiveRate: 0.607565011820331
Weighted MatthewsCorrelation: 0.48793273555432226
Weighted FMeasure: 0.6156047477144883
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5467656188869736
Mean absolute error: 0.20311268715524008
Coverage of cases: 66.903073286052
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.607565011820331
Weighted FalsePositiveRate: 0.13117038295015052
Kappa statistic: 0.4770816422278654
Training time: 6.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 33.74704491725768
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6799418242033436
Weighted AreaUnderROC: 0.8034773560740055
Root mean squared error: 0.38683547449120814
Relative absolute error: 46.12064583695792
Root relative squared error: 89.33582613183826
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.554991731619099
Weighted FMeasure: 0.6555027873414924
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5973179813248265
Mean absolute error: 0.1729524218885922
Coverage of cases: 77.30496453900709
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11422110341096892
Kappa statistic: 0.5457142217879969
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 34.988179669030735
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.626545391293863
Weighted AreaUnderROC: 0.7912463383887759
Root mean squared error: 0.4092596535174754
Relative absolute error: 50.94421207896381
Root relative squared error: 94.51446845070697
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.4913349039618794
Weighted FMeasure: 0.6139768569181411
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5857024864090101
Mean absolute error: 0.19104079529611429
Coverage of cases: 75.177304964539
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12857030041640888
Kappa statistic: 0.4863024414957421
Training time: 8.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 35.99290780141844
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7007862007310204
Weighted AreaUnderROC: 0.8325239421442131
Root mean squared error: 0.355456616739639
Relative absolute error: 41.64835164835167
Root relative squared error: 82.08918934394569
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6059192830613612
Weighted FMeasure: 0.7033743030696016
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6365097447586101
Mean absolute error: 0.15618131868131876
Coverage of cases: 82.97872340425532
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09815236269787207
Kappa statistic: 0.609074783490095
Training time: 7.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.09219858156028
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.7113225418739353
Weighted AreaUnderROC: 0.8078982124867614
Root mean squared error: 0.37417633433210185
Relative absolute error: 42.02534178893516
Root relative squared error: 86.41232294041058
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.6043945099670816
Weighted FMeasure: 0.6992850992939517
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6384526759036804
Mean absolute error: 0.15759503170850683
Coverage of cases: 77.06855791962175
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10063345182390508
Kappa statistic: 0.5995363295489277
Training time: 9.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 31.32387706855792
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6367171911771324
Weighted AreaUnderROC: 0.7560551868638734
Root mean squared error: 0.4155090301511096
Relative absolute error: 52.27213028631476
Root relative squared error: 95.9577001633854
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.4948670941008337
Weighted FMeasure: 0.6191490521186036
Iteration time: 11.0
Weighted AreaUnderPRC: 0.55474741151257
Mean absolute error: 0.19602048857368037
Coverage of cases: 68.32151300236407
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.1299427275203658
Kappa statistic: 0.4852938980874603
Training time: 10.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 32.269503546099294
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6908766646354821
Weighted AreaUnderROC: 0.7960562113621543
Root mean squared error: 0.38156594058845433
Relative absolute error: 43.81832509492071
Root relative squared error: 88.11887940493476
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5803651204901142
Weighted FMeasure: 0.6856489040865639
Iteration time: 11.0
Weighted AreaUnderPRC: 0.611213877659901
Mean absolute error: 0.16431871910595267
Coverage of cases: 75.177304964539
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10657788979806505
Kappa statistic: 0.5772019334049409
Training time: 11.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 34.57446808510638
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6834170065553045
Weighted AreaUnderROC: 0.8132807789206512
Root mean squared error: 0.38158288418783026
Relative absolute error: 44.91924195233875
Root relative squared error: 88.12279236159904
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5726027968038415
Weighted FMeasure: 0.6798023942275169
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6269323595323645
Mean absolute error: 0.1684471573212703
Coverage of cases: 79.90543735224587
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10829594115776649
Kappa statistic: 0.571009910440638
Training time: 11.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 36.28841607565012
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.7020627467439087
Weighted AreaUnderROC: 0.8509983893912084
Root mean squared error: 0.35694899945425934
Relative absolute error: 41.39425061410878
Root relative squared error: 82.43384036875368
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5917488997649869
Weighted FMeasure: 0.6872459609343712
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6637083939137169
Mean absolute error: 0.15522843980290793
Coverage of cases: 85.57919621749409
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 92.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10395480177013393
Kappa statistic: 0.5868124165802955
Training time: 13.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 35.1063829787234
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6922238977178506
Weighted AreaUnderROC: 0.8482404186888486
Root mean squared error: 0.3535604379661544
Relative absolute error: 40.5506147822933
Root relative squared error: 81.65128561382448
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5935482255328491
Weighted FMeasure: 0.6898806175112765
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6593360116273128
Mean absolute error: 0.15206480543359988
Coverage of cases: 84.86997635933807
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10179535816283566
Kappa statistic: 0.5962687067982014
Training time: 14.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 31.85579196217494
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6885967150703808
Weighted AreaUnderROC: 0.8064981005043687
Root mean squared error: 0.37708520707770327
Relative absolute error: 43.38380756198342
Root relative squared error: 87.0840983254951
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5707592456099008
Weighted FMeasure: 0.6727421904774512
Iteration time: 16.0
Weighted AreaUnderPRC: 0.614435572129135
Mean absolute error: 0.16268927835743782
Coverage of cases: 76.35933806146572
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 123.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.1100720599930974
Kappa statistic: 0.5644348935598683
Training time: 15.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 31.85579196217494
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6780117040977708
Weighted AreaUnderROC: 0.8208444404267475
Root mean squared error: 0.37283198410648083
Relative absolute error: 41.767737740673276
Root relative squared error: 86.1018585545516
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5809381974610403
Weighted FMeasure: 0.6808505194148836
Iteration time: 16.0
Weighted AreaUnderPRC: 0.630428414224771
Mean absolute error: 0.15662901652752478
Coverage of cases: 78.01418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10364624290467066
Kappa statistic: 0.5871110515021459
Training time: 15.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 32.15130023640662
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6719001126381821
Weighted AreaUnderROC: 0.8343213393851245
Root mean squared error: 0.3735992251175613
Relative absolute error: 42.53064041874128
Root relative squared error: 86.27904527626384
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5718816656112465
Weighted FMeasure: 0.67232366584423
Iteration time: 16.0
Weighted AreaUnderPRC: 0.637855370505183
Mean absolute error: 0.15948990157027979
Coverage of cases: 81.08747044917257
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 155.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10594756939306055
Kappa statistic: 0.5776901928937035
Training time: 16.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.97872340425532
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6548142429656467
Weighted AreaUnderROC: 0.8136580161968565
Root mean squared error: 0.3808966184001036
Relative absolute error: 45.46935953528155
Root relative squared error: 87.96430606668719
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5474686150473081
Weighted FMeasure: 0.6575442753957907
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6286915811153898
Mean absolute error: 0.17051009825730581
Coverage of cases: 81.08747044917257
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 174.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11267766790719165
Kappa statistic: 0.5522508218350962
Training time: 18.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 37.5886524822695
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.685013264125141
Weighted AreaUnderROC: 0.8368020687125514
Root mean squared error: 0.3468860053070814
Relative absolute error: 43.386819023342774
Root relative squared error: 80.10989141686296
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5818485952753139
Weighted FMeasure: 0.6745227138561093
Iteration time: 49.0
Weighted AreaUnderPRC: 0.6331378979394509
Mean absolute error: 0.16270057133753538
Coverage of cases: 87.23404255319149
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 223.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10475354896337652
Kappa statistic: 0.5838538017797785
Training time: 48.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 38.297872340425535
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6734887399583247
Weighted AreaUnderROC: 0.83110324688223
Root mean squared error: 0.3588476916078287
Relative absolute error: 44.38753301624944
Root relative squared error: 82.87232453914228
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5641049079294425
Weighted FMeasure: 0.6673992613062776
Iteration time: 55.0
Weighted AreaUnderPRC: 0.6315301013139744
Mean absolute error: 0.1664532488109354
Coverage of cases: 86.28841607565012
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 278.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10965682893209713
Kappa statistic: 0.5648342800912466
Training time: 54.0
		
Time end:Sun Oct 08 09.55.49 EEST 2017