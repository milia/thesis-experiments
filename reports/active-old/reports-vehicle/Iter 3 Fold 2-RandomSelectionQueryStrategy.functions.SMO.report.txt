Sun Oct 08 09.55.24 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.24 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.6250381807221901
Weighted AreaUnderROC: 0.8053345576019517
Root mean squared error: 0.378100960356817
Relative absolute error: 78.90727607039659
Root relative squared error: 87.31867649714573
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5110732955633984
Weighted FMeasure: 0.6261627439272307
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5421738814615599
Mean absolute error: 0.2959022852639872
Coverage of cases: 93.85342789598108
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.1207546002009188
Kappa statistic: 0.5180546143707135
Training time: 47.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.94562647754137
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.6325980596291384
Weighted AreaUnderROC: 0.8109418099689327
Root mean squared error: 0.37749259156313736
Relative absolute error: 78.95981087470444
Root relative squared error: 87.17817974242672
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5133931061290897
Weighted FMeasure: 0.6192956926830441
Iteration time: 47.0
Weighted AreaUnderPRC: 0.5494759337301343
Mean absolute error: 0.2960992907801417
Coverage of cases: 94.79905437352245
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.12080185555770416
Kappa statistic: 0.5180689696256636
Training time: 47.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6649571954842697
Weighted AreaUnderROC: 0.8265569629496405
Root mean squared error: 0.3694926558066279
Relative absolute error: 77.33123194116095
Root relative squared error: 85.33067371741855
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5579276711006234
Weighted FMeasure: 0.6602113672810556
Iteration time: 48.0
Weighted AreaUnderPRC: 0.5818727272543076
Mean absolute error: 0.2899921197793536
Coverage of cases: 96.45390070921985
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 143.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11010565555709509
Kappa statistic: 0.5619552098699209
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.656527336661461
Weighted AreaUnderROC: 0.8206707541302658
Root mean squared error: 0.3717959169155041
Relative absolute error: 77.75151037562385
Root relative squared error: 85.86258908590803
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5381200139013337
Weighted FMeasure: 0.6361246790744423
Iteration time: 50.0
Weighted AreaUnderPRC: 0.5709526148641506
Mean absolute error: 0.2915681639085894
Coverage of cases: 95.98108747044917
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 193.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11545837566774152
Kappa statistic: 0.5400907039610374
Training time: 49.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6892438249766466
Weighted AreaUnderROC: 0.8291778672400383
Root mean squared error: 0.3674431208652359
Relative absolute error: 76.91095350669809
Root relative squared error: 84.85735389736807
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5900178473720175
Weighted FMeasure: 0.6836862015909304
Iteration time: 51.0
Weighted AreaUnderPRC: 0.592662768362763
Mean absolute error: 0.28841607565011784
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 244.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10249409271095132
Kappa statistic: 0.5932480078716093
Training time: 50.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6562863763232667
Weighted AreaUnderROC: 0.8245341237219923
Root mean squared error: 0.3694482215985656
Relative absolute error: 77.22616233254527
Root relative squared error: 85.32041207662415
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.545032205540329
Weighted FMeasure: 0.6440049040607886
Iteration time: 52.0
Weighted AreaUnderPRC: 0.5734084531221783
Mean absolute error: 0.2895981087470448
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 296.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11343413531376531
Kappa statistic: 0.5493630335990464
Training time: 51.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6983531244993499
Weighted AreaUnderROC: 0.8402907473592702
Root mean squared error: 0.36439225827135874
Relative absolute error: 76.28053585500392
Root relative squared error: 84.15278736143384
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5950800266577566
Weighted FMeasure: 0.680329409776248
Iteration time: 53.0
Weighted AreaUnderPRC: 0.6072494846614603
Mean absolute error: 0.2860520094562647
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 349.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10153198953317201
Kappa statistic: 0.5966236301191266
Training time: 52.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6949969018830027
Weighted AreaUnderROC: 0.8387429041777249
Root mean squared error: 0.36569647416427237
Relative absolute error: 76.5432098765432
Root relative squared error: 84.45398312017588
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6006129528097415
Weighted FMeasure: 0.6870856890365976
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6085676840704952
Mean absolute error: 0.287037037037037
Coverage of cases: 96.6903073286052
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 403.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09898614182234294
Kappa statistic: 0.6061599195560687
Training time: 54.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 76.35933806146572
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6959606714967776
Weighted AreaUnderROC: 0.8475322976214554
Root mean squared error: 0.3626761865435737
Relative absolute error: 76.12293144208027
Root relative squared error: 83.75647757183968
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.5986500981051697
Weighted FMeasure: 0.6775610430362303
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6200610385928933
Mean absolute error: 0.285460992907801
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 457.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09983791652765431
Kappa statistic: 0.6057634953772741
Training time: 54.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.6967196928681488
Weighted AreaUnderROC: 0.8478867226200374
Root mean squared error: 0.3628572078608316
Relative absolute error: 76.0178618334646
Root relative squared error: 83.79828265433885
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6096096124961484
Weighted FMeasure: 0.6973479835675769
Iteration time: 55.0
Weighted AreaUnderPRC: 0.6182061026632472
Mean absolute error: 0.28506698187549223
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 512.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09619365688023454
Kappa statistic: 0.6185923469577853
Training time: 54.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7140595280106938
Weighted AreaUnderROC: 0.8504049261978457
Root mean squared error: 0.3613156358435332
Relative absolute error: 75.70265300761753
Root relative squared error: 83.44227184667389
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.628181338572239
Weighted FMeasure: 0.7113080559262074
Iteration time: 56.0
Weighted AreaUnderPRC: 0.629293590711984
Mean absolute error: 0.28388494877856574
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 568.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09215857534992648
Kappa statistic: 0.6344319942782215
Training time: 55.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.6965001017635293
Weighted AreaUnderROC: 0.8449582971967504
Root mean squared error: 0.36303813891563547
Relative absolute error: 76.01786183346469
Root relative squared error: 83.84006689161717
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6078540146944744
Weighted FMeasure: 0.694865966667978
Iteration time: 57.0
Weighted AreaUnderPRC: 0.6180284675078972
Mean absolute error: 0.28506698187549256
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 625.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09656872697752769
Kappa statistic: 0.6156550234601922
Training time: 56.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7168733269271194
Weighted AreaUnderROC: 0.8535785498280873
Root mean squared error: 0.3609974361905773
Relative absolute error: 75.59758339900182
Root relative squared error: 83.36878678455781
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6313575591558407
Weighted FMeasure: 0.7132891938543781
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6337030187780749
Mean absolute error: 0.28349093774625683
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 684.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09129203713908454
Kappa statistic: 0.6375834426034093
Training time: 58.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6872131141035124
Weighted AreaUnderROC: 0.842535836933706
Root mean squared error: 0.36430214023899965
Relative absolute error: 76.22800105069605
Root relative squared error: 84.1319754933373
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.5992812953111772
Weighted FMeasure: 0.6914488122736292
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6167479481418124
Mean absolute error: 0.2858550039401102
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 743.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09848263885903596
Kappa statistic: 0.6092495194958059
Training time: 58.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6793567929184605
Weighted AreaUnderROC: 0.8423808649141767
Root mean squared error: 0.3647074960251674
Relative absolute error: 76.33307065931176
Root relative squared error: 84.22558840224191
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5894051875597885
Weighted FMeasure: 0.6845882031906267
Iteration time: 61.0
Weighted AreaUnderPRC: 0.612161261188646
Mean absolute error: 0.2862490149724191
Coverage of cases: 96.45390070921985
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 804.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10084628678215445
Kappa statistic: 0.599807804049524
Training time: 61.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6775646839837247
Weighted AreaUnderROC: 0.8374237218221188
Root mean squared error: 0.36583112767267045
Relative absolute error: 76.5432098765432
Root relative squared error: 84.48508001590426
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5864901085063345
Weighted FMeasure: 0.682364843708997
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6087123078519046
Mean absolute error: 0.287037037037037
Coverage of cases: 96.45390070921985
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 869.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10159877958469152
Kappa statistic: 0.5966777161160565
Training time: 65.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6892934488679169
Weighted AreaUnderROC: 0.8412899125788478
Root mean squared error: 0.364797513920164
Relative absolute error: 76.38560546361964
Root relative squared error: 84.24637714460519
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6000171665755164
Weighted FMeasure: 0.6923608937925977
Iteration time: 63.0
Weighted AreaUnderPRC: 0.617359736775251
Mean absolute error: 0.28644602048857365
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 932.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09853504145284654
Kappa statistic: 0.6092174962562303
Training time: 62.0
		
Time end:Sun Oct 08 09.55.26 EEST 2017