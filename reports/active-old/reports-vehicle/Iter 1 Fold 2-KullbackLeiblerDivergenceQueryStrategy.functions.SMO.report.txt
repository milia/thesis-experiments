Sun Oct 08 09.52.48 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.48 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.35933806146572
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.6249044615882474
Weighted AreaUnderROC: 0.8027178078702943
Root mean squared error: 0.38017940548112644
Relative absolute error: 79.64276333070653
Root relative squared error: 87.7986728379521
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.5064882670417473
Weighted FMeasure: 0.6093702382143433
Iteration time: 51.0
Weighted AreaUnderPRC: 0.5453495266078588
Mean absolute error: 0.2986603624901495
Coverage of cases: 95.27186761229315
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 51.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.1217064748779498
Kappa statistic: 0.5122994413740265
Training time: 49.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.5179685594702913
Weighted AreaUnderROC: 0.8547230913970194
Root mean squared error: 0.3670855119037594
Relative absolute error: 76.75334909377447
Root relative squared error: 84.77476764529882
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5200564553200923
Weighted FMeasure: 0.5786354669370465
Iteration time: 68.0
Weighted AreaUnderPRC: 0.6176035357823547
Mean absolute error: 0.2878250591016543
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11107873933126601
Kappa statistic: 0.5565180536553447
Training time: 67.0
		
Time end:Sun Oct 08 09.52.49 EEST 2017