Sun Oct 08 09.52.19 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.19 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 34.86406619385343
Incorrectly Classified Instances: 39.12529550827422
Correctly Classified Instances: 60.87470449172578
Weighted Precision: 0.6010990511787423
Weighted AreaUnderROC: 0.7552457473176856
Root mean squared error: 0.4216875076813319
Relative absolute error: 54.40409423958306
Root relative squared error: 97.38455842948775
Weighted TruePositiveRate: 0.6087470449172576
Weighted MatthewsCorrelation: 0.4740229466378656
Weighted FMeasure: 0.5961929062733026
Iteration time: 20.7
Weighted AreaUnderPRC: 0.5210809419949145
Mean absolute error: 0.20401535339843643
Coverage of cases: 70.92198581560284
Instances selection time: 5.0
Test time: 2.8
Accumulative iteration time: 20.7
Weighted Recall: 0.6087470449172576
Weighted FalsePositiveRate: 0.13148370430132522
Kappa statistic: 0.4782056113655435
Training time: 15.7
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 37.659574468085104
Incorrectly Classified Instances: 38.156028368794324
Correctly Classified Instances: 61.84397163120568
Weighted Precision: 0.6183398988827636
Weighted AreaUnderROC: 0.7732577996689192
Root mean squared error: 0.4123663605828689
Relative absolute error: 52.62272333207127
Root relative squared error: 95.23193171490627
Weighted TruePositiveRate: 0.6184397163120566
Weighted MatthewsCorrelation: 0.48995130722162383
Weighted FMeasure: 0.6106804429464514
Iteration time: 14.8
Weighted AreaUnderPRC: 0.545457363339277
Mean absolute error: 0.19733521249526723
Coverage of cases: 75.13002364066193
Instances selection time: 3.1
Test time: 3.4
Accumulative iteration time: 35.5
Weighted Recall: 0.6184397163120566
Weighted FalsePositiveRate: 0.12812441881991246
Kappa statistic: 0.4911332977150388
Training time: 11.7
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 35.874704491725765
Incorrectly Classified Instances: 37.635933806146575
Correctly Classified Instances: 62.36406619385343
Weighted Precision: 0.6242674928638522
Weighted AreaUnderROC: 0.7682708356508671
Root mean squared error: 0.41559266630932257
Relative absolute error: 52.040182675354274
Root relative squared error: 95.97701510676868
Weighted TruePositiveRate: 0.6236406619385342
Weighted MatthewsCorrelation: 0.4973774831100535
Weighted FMeasure: 0.6160110294157366
Iteration time: 17.7
Weighted AreaUnderPRC: 0.5473122634457674
Mean absolute error: 0.1951506850325785
Coverage of cases: 72.67139479905437
Instances selection time: 2.5
Test time: 3.0
Accumulative iteration time: 53.2
Weighted Recall: 0.6236406619385342
Weighted FalsePositiveRate: 0.126117285172004
Kappa statistic: 0.4981269627501204
Training time: 15.2
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 36.43026004728132
Incorrectly Classified Instances: 35.1063829787234
Correctly Classified Instances: 64.89361702127658
Weighted Precision: 0.6457518366182484
Weighted AreaUnderROC: 0.7783913081170525
Root mean squared error: 0.40438988247498964
Relative absolute error: 49.61719475529562
Root relative squared error: 93.38984300179858
Weighted TruePositiveRate: 0.648936170212766
Weighted MatthewsCorrelation: 0.5294583796480474
Weighted FMeasure: 0.6411405802629275
Iteration time: 17.6
Weighted AreaUnderPRC: 0.5566432575243874
Mean absolute error: 0.18606448033235862
Coverage of cases: 74.08983451536643
Instances selection time: 4.0
Test time: 2.4
Accumulative iteration time: 70.8
Weighted Recall: 0.648936170212766
Weighted FalsePositiveRate: 0.11772073422591015
Kappa statistic: 0.5318948729066516
Training time: 13.6
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 35.325059101654844
Incorrectly Classified Instances: 36.193853427895974
Correctly Classified Instances: 63.80614657210401
Weighted Precision: 0.6499037323361736
Weighted AreaUnderROC: 0.7768338201219538
Root mean squared error: 0.4058820315709094
Relative absolute error: 50.31793400084583
Root relative squared error: 93.73444007467869
Weighted TruePositiveRate: 0.6380614657210402
Weighted MatthewsCorrelation: 0.5193906770113943
Weighted FMeasure: 0.6309074454862251
Iteration time: 20.3
Weighted AreaUnderPRC: 0.5638211731530133
Mean absolute error: 0.1886922525031719
Coverage of cases: 73.56973995271868
Instances selection time: 3.2
Test time: 1.9
Accumulative iteration time: 91.1
Weighted Recall: 0.6380614657210402
Weighted FalsePositiveRate: 0.12142471575441091
Kappa statistic: 0.5173990960471248
Training time: 17.1
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 33.611111111111114
Incorrectly Classified Instances: 34.58628841607565
Correctly Classified Instances: 65.41371158392435
Weighted Precision: 0.655859138408194
Weighted AreaUnderROC: 0.7836553872390525
Root mean squared error: 0.4001522077815154
Relative absolute error: 48.1489989291854
Root relative squared error: 92.4111939517924
Weighted TruePositiveRate: 0.6541371158392435
Weighted MatthewsCorrelation: 0.5384542885892935
Weighted FMeasure: 0.6470708418439494
Iteration time: 17.2
Weighted AreaUnderPRC: 0.570048828431694
Mean absolute error: 0.18055874598444527
Coverage of cases: 73.54609929078013
Instances selection time: 1.8
Test time: 2.3
Accumulative iteration time: 108.3
Weighted Recall: 0.6541371158392435
Weighted FalsePositiveRate: 0.1159677998139812
Kappa statistic: 0.5387474056441853
Training time: 15.4
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 34.47990543735225
Incorrectly Classified Instances: 34.704491725768314
Correctly Classified Instances: 65.29550827423166
Weighted Precision: 0.6562245736319231
Weighted AreaUnderROC: 0.7943446600866163
Root mean squared error: 0.38957291382074455
Relative absolute error: 47.973292211817196
Root relative squared error: 89.96801066535747
Weighted TruePositiveRate: 0.6529550827423167
Weighted MatthewsCorrelation: 0.5366818208516342
Weighted FMeasure: 0.6432469731951441
Iteration time: 15.1
Weighted AreaUnderPRC: 0.5889151751221446
Mean absolute error: 0.17989984579431445
Coverage of cases: 76.43026004728132
Instances selection time: 1.7
Test time: 2.3
Accumulative iteration time: 123.4
Weighted Recall: 0.6529550827423167
Weighted FalsePositiveRate: 0.11651455199981735
Kappa statistic: 0.5372152113203214
Training time: 13.4
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 34.27895981087471
Incorrectly Classified Instances: 34.349881796690305
Correctly Classified Instances: 65.65011820330969
Weighted Precision: 0.6611108581847002
Weighted AreaUnderROC: 0.7954931786808419
Root mean squared error: 0.38789082429816596
Relative absolute error: 47.01390840459138
Root relative squared error: 89.57954872989279
Weighted TruePositiveRate: 0.6565011820330969
Weighted MatthewsCorrelation: 0.5430564779852703
Weighted FMeasure: 0.6501397816651359
Iteration time: 18.9
Weighted AreaUnderPRC: 0.5866991597854307
Mean absolute error: 0.17630215651721765
Coverage of cases: 77.42316784869976
Instances selection time: 1.3
Test time: 2.4
Accumulative iteration time: 142.3
Weighted Recall: 0.6565011820330969
Weighted FalsePositiveRate: 0.11521569639921334
Kappa statistic: 0.5419711267617743
Training time: 17.6
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.70567375886525
Incorrectly Classified Instances: 33.54609929078014
Correctly Classified Instances: 66.45390070921987
Weighted Precision: 0.6662899936135482
Weighted AreaUnderROC: 0.8042690027718992
Root mean squared error: 0.3861043325174943
Relative absolute error: 46.45544524834446
Root relative squared error: 89.16697612570245
Weighted TruePositiveRate: 0.6645390070921986
Weighted MatthewsCorrelation: 0.5525704143215922
Weighted FMeasure: 0.6585694959739904
Iteration time: 23.3
Weighted AreaUnderPRC: 0.5965666295659119
Mean absolute error: 0.17420791968129173
Coverage of cases: 77.84869976359337
Instances selection time: 3.1
Test time: 1.8
Accumulative iteration time: 165.6
Weighted Recall: 0.6645390070921986
Weighted FalsePositiveRate: 0.11262369138740977
Kappa statistic: 0.5526237848368456
Training time: 20.2
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 34.62765957446808
Incorrectly Classified Instances: 33.94799054373523
Correctly Classified Instances: 66.05200945626477
Weighted Precision: 0.6611283070365557
Weighted AreaUnderROC: 0.8021725650300944
Root mean squared error: 0.3876361718261553
Relative absolute error: 46.54195359740551
Root relative squared error: 89.52073926058671
Weighted TruePositiveRate: 0.6605200945626477
Weighted MatthewsCorrelation: 0.5467245536995915
Weighted FMeasure: 0.6555487510576297
Iteration time: 24.4
Weighted AreaUnderPRC: 0.5979348314983499
Mean absolute error: 0.17453232599027063
Coverage of cases: 76.59574468085107
Instances selection time: 2.3
Test time: 2.6
Accumulative iteration time: 190.0
Weighted Recall: 0.6605200945626477
Weighted FalsePositiveRate: 0.11384057743858071
Kappa statistic: 0.5473083267866676
Training time: 22.1
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 32.630023640661946
Incorrectly Classified Instances: 31.82033096926714
Correctly Classified Instances: 68.17966903073287
Weighted Precision: 0.6807340557045564
Weighted AreaUnderROC: 0.8046309530770952
Root mean squared error: 0.3790234780934535
Relative absolute error: 43.96420804900682
Root relative squared error: 87.53172284257745
Weighted TruePositiveRate: 0.6817966903073286
Weighted MatthewsCorrelation: 0.5742666274752154
Weighted FMeasure: 0.6771335591234615
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6091061152427746
Mean absolute error: 0.16486578018377565
Coverage of cases: 76.73758865248227
Instances selection time: 2.6
Test time: 2.4
Accumulative iteration time: 215.0
Weighted Recall: 0.6817966903073286
Weighted FalsePositiveRate: 0.1070412107771411
Kappa statistic: 0.5755849051591946
Training time: 22.4
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 33.2033096926714
Incorrectly Classified Instances: 32.505910165484636
Correctly Classified Instances: 67.49408983451536
Weighted Precision: 0.6744125818606804
Weighted AreaUnderROC: 0.8064734235003395
Root mean squared error: 0.3790562409077383
Relative absolute error: 44.671066956786206
Root relative squared error: 87.53928909043614
Weighted TruePositiveRate: 0.6749408983451537
Weighted MatthewsCorrelation: 0.5653614181194817
Weighted FMeasure: 0.669699657745969
Iteration time: 25.1
Weighted AreaUnderPRC: 0.6137984310898716
Mean absolute error: 0.16751650108794827
Coverage of cases: 77.47044917257683
Instances selection time: 1.6
Test time: 2.7
Accumulative iteration time: 240.1
Weighted Recall: 0.6749408983451537
Weighted FalsePositiveRate: 0.10924336624488198
Kappa statistic: 0.5664478761121379
Training time: 23.5
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 33.71158392434988
Incorrectly Classified Instances: 31.347517730496456
Correctly Classified Instances: 68.65248226950355
Weighted Precision: 0.6845772112009285
Weighted AreaUnderROC: 0.8226600369127969
Root mean squared error: 0.36792962888359676
Relative absolute error: 43.076895332397854
Root relative squared error: 84.96970811151347
Weighted TruePositiveRate: 0.6865248226950353
Weighted MatthewsCorrelation: 0.5800178109493157
Weighted FMeasure: 0.6795168906471607
Iteration time: 28.2
Weighted AreaUnderPRC: 0.6301655503471404
Mean absolute error: 0.16153835749649195
Coverage of cases: 80.18912529550829
Instances selection time: 1.9
Test time: 2.6
Accumulative iteration time: 268.3
Weighted Recall: 0.6865248226950353
Weighted FalsePositiveRate: 0.1055342271571866
Kappa statistic: 0.5819135747296236
Training time: 26.3
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 33.2033096926714
Incorrectly Classified Instances: 31.08747044917258
Correctly Classified Instances: 68.91252955082743
Weighted Precision: 0.6867917768679621
Weighted AreaUnderROC: 0.8157352235793394
Root mean squared error: 0.37052533168306917
Relative absolute error: 43.28112683868734
Root relative squared error: 85.56915999551816
Weighted TruePositiveRate: 0.6891252955082742
Weighted MatthewsCorrelation: 0.5836254450029447
Weighted FMeasure: 0.6825832663173858
Iteration time: 28.4
Weighted AreaUnderPRC: 0.6190328917364682
Mean absolute error: 0.16230422564507754
Coverage of cases: 78.95981087470449
Instances selection time: 1.4
Test time: 2.3
Accumulative iteration time: 296.7
Weighted Recall: 0.6891252955082742
Weighted FalsePositiveRate: 0.10436686076083301
Kappa statistic: 0.5854263558984203
Training time: 27.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 33.150118203309695
Incorrectly Classified Instances: 30.260047281323875
Correctly Classified Instances: 69.73995271867614
Weighted Precision: 0.696386929835189
Weighted AreaUnderROC: 0.8259988128941849
Root mean squared error: 0.36447957115087826
Relative absolute error: 41.93753458472513
Root relative squared error: 84.17295140723158
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5949462802503488
Weighted FMeasure: 0.6917359117974324
Iteration time: 27.8
Weighted AreaUnderPRC: 0.6382828177591937
Mean absolute error: 0.15726575469271925
Coverage of cases: 80.49645390070923
Instances selection time: 1.2
Test time: 2.6
Accumulative iteration time: 324.5
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10192625788463192
Kappa statistic: 0.5963832677474198
Training time: 26.6
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 32.81914893617021
Incorrectly Classified Instances: 30.118203309692678
Correctly Classified Instances: 69.88179669030733
Weighted Precision: 0.6977478242996383
Weighted AreaUnderROC: 0.8290101575851144
Root mean squared error: 0.36138465576852163
Relative absolute error: 41.88410225559577
Root relative squared error: 83.4582113155825
Weighted TruePositiveRate: 0.6988179669030732
Weighted MatthewsCorrelation: 0.5968084280347218
Weighted FMeasure: 0.6925867146626027
Iteration time: 29.6
Weighted AreaUnderPRC: 0.636764641854466
Mean absolute error: 0.15706538345848414
Coverage of cases: 80.78014184397165
Instances selection time: 1.9
Test time: 1.8
Accumulative iteration time: 354.1
Weighted Recall: 0.6988179669030732
Weighted FalsePositiveRate: 0.10143016682600763
Kappa statistic: 0.5982774774900359
Training time: 27.7
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 33.51063829787235
Incorrectly Classified Instances: 30.047281323877066
Correctly Classified Instances: 69.95271867612294
Weighted Precision: 0.6991182936108309
Weighted AreaUnderROC: 0.830581109018304
Root mean squared error: 0.35777258569351833
Relative absolute error: 41.85646432069681
Root relative squared error: 82.62403946352852
Weighted TruePositiveRate: 0.6995271867612292
Weighted MatthewsCorrelation: 0.5979477204471866
Weighted FMeasure: 0.6931572521741762
Iteration time: 30.6
Weighted AreaUnderPRC: 0.6408462591931727
Mean absolute error: 0.15696174120261303
Coverage of cases: 81.65484633569741
Instances selection time: 1.0
Test time: 2.1
Accumulative iteration time: 384.7
Weighted Recall: 0.6995271867612292
Weighted FalsePositiveRate: 0.10125622202302535
Kappa statistic: 0.5991811275751394
Training time: 29.6
		
Time end:Sun Oct 08 09.52.31 EEST 2017