Sun Oct 08 09.54.30 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.54.30 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.5836706169039663
Weighted AreaUnderROC: 0.7243190701534918
Root mean squared error: 0.4435226582017142
Relative absolute error: 56.8987749838813
Root relative squared error: 102.42717044178325
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.44813691911051834
Weighted FMeasure: 0.5722141856437919
Iteration time: 11.0
Weighted AreaUnderPRC: 0.4657574784967656
Mean absolute error: 0.21337040618955488
Coverage of cases: 58.86524822695036
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.14001434196251983
Kappa statistic: 0.4497540407589599
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.6030739687888643
Weighted AreaUnderROC: 0.7263366320301026
Root mean squared error: 0.44404409667663014
Relative absolute error: 56.21515920964273
Root relative squared error: 102.54759149932666
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.45863287020427823
Weighted FMeasure: 0.5849612751604201
Iteration time: 12.0
Weighted AreaUnderPRC: 0.47731807019525857
Mean absolute error: 0.21080684703616023
Coverage of cases: 59.1016548463357
Instances selection time: 12.0
Test time: 16.0
Accumulative iteration time: 23.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.1383432844031519
Kappa statistic: 0.453427143785431
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6478943332358625
Weighted AreaUnderROC: 0.7422745863667923
Root mean squared error: 0.4322424310272349
Relative absolute error: 52.89844365642238
Root relative squared error: 99.82211356350093
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.49940686675767526
Weighted FMeasure: 0.6071328487382994
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5004233788049585
Mean absolute error: 0.19836916371158392
Coverage of cases: 61.46572104018912
Instances selection time: 16.0
Test time: 22.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.13010803766830653
Kappa statistic: 0.48505941133856617
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.6553635899167813
Weighted AreaUnderROC: 0.7392064920178137
Root mean squared error: 0.4357765409048929
Relative absolute error: 53.306497987349296
Root relative squared error: 100.63828127918556
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.49853949723241964
Weighted FMeasure: 0.5997503932464248
Iteration time: 16.0
Weighted AreaUnderPRC: 0.4990385342236858
Mean absolute error: 0.19989936745255985
Coverage of cases: 60.99290780141844
Instances selection time: 16.0
Test time: 25.0
Accumulative iteration time: 55.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.13151609397855707
Kappa statistic: 0.4788617764769129
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.6564310935373459
Weighted AreaUnderROC: 0.7344857624943287
Root mean squared error: 0.44040858349149886
Relative absolute error: 54.07519981988057
Root relative squared error: 101.70800569289547
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.4926033399556329
Weighted FMeasure: 0.5936375083999118
Iteration time: 20.0
Weighted AreaUnderPRC: 0.4971905106870185
Mean absolute error: 0.20278199932455213
Coverage of cases: 60.283687943262414
Instances selection time: 20.0
Test time: 32.0
Accumulative iteration time: 75.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.13386535444396663
Kappa statistic: 0.4694301137084792
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6666998998608421
Weighted AreaUnderROC: 0.7489532814442229
Root mean squared error: 0.4289864212656377
Relative absolute error: 51.179518132890905
Root relative squared error: 99.07017031856405
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.5172240510099326
Weighted FMeasure: 0.6177929249402236
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5209686097429004
Mean absolute error: 0.1919231929983409
Coverage of cases: 62.4113475177305
Instances selection time: 20.0
Test time: 36.0
Accumulative iteration time: 95.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.1262069122888591
Kappa statistic: 0.49841897233201576
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6727880099288179
Weighted AreaUnderROC: 0.7618664588423206
Root mean squared error: 0.418478072412022
Relative absolute error: 48.60429168939831
Root relative squared error: 96.64337110281464
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5366580468286474
Weighted FMeasure: 0.636617625084516
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5414910816123085
Mean absolute error: 0.18226609383524364
Coverage of cases: 64.30260047281324
Instances selection time: 19.0
Test time: 26.0
Accumulative iteration time: 114.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.11929308704349106
Kappa statistic: 0.524177387754494
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6768203373277023
Weighted AreaUnderROC: 0.7683571247310723
Root mean squared error: 0.4132410606028366
Relative absolute error: 47.27717639251799
Root relative squared error: 95.43393503170168
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5466826274130504
Weighted FMeasure: 0.6447164734499835
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5510283694707555
Mean absolute error: 0.17728941147194244
Coverage of cases: 65.2482269503546
Instances selection time: 13.0
Test time: 27.0
Accumulative iteration time: 127.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11576802004140135
Kappa statistic: 0.5370647488441695
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6762807197553236
Weighted AreaUnderROC: 0.771629000054325
Root mean squared error: 0.41070566605156317
Relative absolute error: 46.581001042222965
Root relative squared error: 94.84841074102982
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5504723340164209
Weighted FMeasure: 0.6467120156522473
Iteration time: 26.0
Weighted AreaUnderPRC: 0.5543951767017011
Mean absolute error: 0.17467875390833612
Coverage of cases: 65.72104018912529
Instances selection time: 26.0
Test time: 29.0
Accumulative iteration time: 153.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11395240178260285
Kappa statistic: 0.5435602818934787
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6761949965562059
Weighted AreaUnderROC: 0.7747344273754622
Root mean squared error: 0.4081049557031953
Relative absolute error: 45.89464027380841
Root relative squared error: 94.24780241314406
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.554050490663987
Weighted FMeasure: 0.6477196820807839
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5565866906545992
Mean absolute error: 0.17210490102678153
Coverage of cases: 66.19385342789599
Instances selection time: 12.0
Test time: 31.0
Accumulative iteration time: 165.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11246967952803523
Kappa statistic: 0.5497822931785196
Training time: 0.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6715122718213324
Weighted AreaUnderROC: 0.7777454990202884
Root mean squared error: 0.40544792616726166
Relative absolute error: 45.216049382715795
Root relative squared error: 93.63418772601763
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5558613979699284
Weighted FMeasure: 0.6482840855159705
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5575673224512533
Mean absolute error: 0.16956018518518423
Coverage of cases: 66.66666666666667
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 177.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11117566862608957
Kappa statistic: 0.5559534533975594
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6746666052475542
Weighted AreaUnderROC: 0.7808017795300709
Root mean squared error: 0.40274113855752175
Relative absolute error: 44.54371475648077
Root relative squared error: 93.00908190396864
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5613580190212744
Weighted FMeasure: 0.6509163425604841
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5607186518319675
Mean absolute error: 0.16703893033680287
Coverage of cases: 67.13947990543736
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 187.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10979123999423172
Kappa statistic: 0.5620694021346482
Training time: 0.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6748826541640841
Weighted AreaUnderROC: 0.780752632718716
Root mean squared error: 0.4028979962850818
Relative absolute error: 44.49922158796059
Root relative squared error: 93.04530664446115
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5614083137206692
Weighted FMeasure: 0.65089098813826
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5606210779893347
Mean absolute error: 0.16687208095485223
Coverage of cases: 67.13947990543736
Instances selection time: 8.0
Test time: 37.0
Accumulative iteration time: 195.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10988953361694148
Kappa statistic: 0.5620367818489247
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.669766801464855
Weighted AreaUnderROC: 0.7790318431648041
Root mean squared error: 0.4044839717980319
Relative absolute error: 44.77142831263657
Root relative squared error: 93.4115720001931
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5576130074832005
Weighted FMeasure: 0.6531734914549867
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5599151388125283
Mean absolute error: 0.16789285617238714
Coverage of cases: 66.903073286052
Instances selection time: 8.0
Test time: 38.0
Accumulative iteration time: 203.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11096704653091187
Kappa statistic: 0.558738059401219
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6728316315877961
Weighted AreaUnderROC: 0.782095999567384
Root mean squared error: 0.4017084395142803
Relative absolute error: 44.11296124987177
Root relative squared error: 92.77059027572571
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5636037619284301
Weighted FMeasure: 0.6655208913963725
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5639025546661455
Mean absolute error: 0.16542360468701914
Coverage of cases: 67.37588652482269
Instances selection time: 6.0
Test time: 40.0
Accumulative iteration time: 209.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10956686611345888
Kappa statistic: 0.5649056378760323
Training time: 0.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.667882530123619
Weighted AreaUnderROC: 0.7787527140822699
Root mean squared error: 0.40472031595696767
Relative absolute error: 44.70522288026143
Root relative squared error: 93.46615334570629
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5571862336597235
Weighted FMeasure: 0.6652011399363127
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5637152634868323
Mean absolute error: 0.16764458580098038
Coverage of cases: 66.903073286052
Instances selection time: 4.0
Test time: 46.0
Accumulative iteration time: 213.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11152530469598015
Kappa statistic: 0.5584024339318737
Training time: 0.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6714850132114877
Weighted AreaUnderROC: 0.7802356454722051
Root mean squared error: 0.40337304394183837
Relative absolute error: 44.36486966733073
Root relative squared error: 93.15501420146366
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5608348845142547
Weighted FMeasure: 0.6695089172845979
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5669350339015224
Mean absolute error: 0.1663682612524902
Coverage of cases: 67.13947990543736
Instances selection time: 3.0
Test time: 43.0
Accumulative iteration time: 216.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11092350810996345
Kappa statistic: 0.5614324288037235
Training time: 0.0
		
Time end:Sun Oct 08 09.54.32 EEST 2017