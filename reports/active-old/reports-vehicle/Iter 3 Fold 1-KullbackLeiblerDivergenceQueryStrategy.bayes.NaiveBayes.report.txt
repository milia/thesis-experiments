Sun Oct 08 09.52.47 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.47 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 43.73522458628842
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.49194286856345343
Weighted AreaUnderROC: 0.7879059809259094
Root mean squared error: 0.43753038867321387
Relative absolute error: 65.26574375022066
Root relative squared error: 101.04331507164865
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.34672086045963085
Weighted FMeasure: 0.468780914562974
Iteration time: 106.0
Weighted AreaUnderPRC: 0.550785284467025
Mean absolute error: 0.24474653906332747
Coverage of cases: 79.66903073286052
Instances selection time: 100.0
Test time: 63.0
Accumulative iteration time: 106.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.15846191046355482
Kappa statistic: 0.3563316508313539
Training time: 6.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 45.80378250591016
Incorrectly Classified Instances: 57.4468085106383
Correctly Classified Instances: 42.5531914893617
Weighted Precision: 0.48118298827518685
Weighted AreaUnderROC: 0.7676094276906783
Root mean squared error: 0.4727276385473321
Relative absolute error: 75.58881347868608
Root relative squared error: 109.17177174747131
Weighted TruePositiveRate: 0.425531914893617
Weighted MatthewsCorrelation: 0.25536283753806566
Weighted FMeasure: 0.3611039596175214
Iteration time: 10.0
Weighted AreaUnderPRC: 0.52619201588514
Mean absolute error: 0.2834580505450728
Coverage of cases: 75.65011820330969
Instances selection time: 8.0
Test time: 23.0
Accumulative iteration time: 116.0
Weighted Recall: 0.425531914893617
Weighted FalsePositiveRate: 0.1848327029530471
Kappa statistic: 0.24035000849893948
Training time: 2.0
		
Time end:Sun Oct 08 09.52.47 EEST 2017