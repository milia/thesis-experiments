Sun Oct 08 09.56.09 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.09 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.7339191655261085
Weighted AreaUnderROC: 0.8131209886227504
Root mean squared error: 0.3844307107510158
Relative absolute error: 80.27318098240083
Root relative squared error: 88.78046973474325
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5225588326749164
Weighted FMeasure: 0.5747472651897926
Iteration time: 94.0
Weighted AreaUnderPRC: 0.5570568936839263
Mean absolute error: 0.3010244286840031
Coverage of cases: 93.14420803782505
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 94.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12277249920110289
Kappa statistic: 0.5201182239537853
Training time: 90.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.7740213959959057
Weighted AreaUnderROC: 0.8539389420385352
Root mean squared error: 0.3672643599104258
Relative absolute error: 76.80588389808229
Root relative squared error: 84.81607082321598
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5598306350824743
Weighted FMeasure: 0.5937522892959611
Iteration time: 123.0
Weighted AreaUnderPRC: 0.6203242129831198
Mean absolute error: 0.2880220646178086
Coverage of cases: 96.45390070921985
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 217.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10688788032071447
Kappa statistic: 0.5694960330431165
Training time: 121.0
		
Time end:Sun Oct 08 09.56.09 EEST 2017