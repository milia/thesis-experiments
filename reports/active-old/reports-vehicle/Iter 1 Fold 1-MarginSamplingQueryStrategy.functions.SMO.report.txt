Sun Oct 08 09.54.13 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.54.13 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.4374922232874992
Weighted AreaUnderROC: 0.7917058680501837
Root mean squared error: 0.3918747246791956
Relative absolute error: 81.79669030732846
Root relative squared error: 90.49959111285763
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.4086816861412211
Weighted FMeasure: 0.4977448113586295
Iteration time: 51.0
Weighted AreaUnderPRC: 0.516688281198598
Mean absolute error: 0.30673758865248174
Coverage of cases: 93.3806146572104
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.13731150759315938
Kappa statistic: 0.44385515320334257
Training time: 50.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 43.026004728132385
Correctly Classified Instances: 56.973995271867615
Weighted Precision: 0.4408252315481774
Weighted AreaUnderROC: 0.7847027976271251
Root mean squared error: 0.3940471811628661
Relative absolute error: 82.26950354609919
Root relative squared error: 91.00129844711759
Weighted TruePositiveRate: 0.5697399527186762
Weighted MatthewsCorrelation: 0.3991791372761127
Weighted FMeasure: 0.4912414977085373
Iteration time: 42.0
Weighted AreaUnderPRC: 0.5087259460443642
Mean absolute error: 0.30851063829787195
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 93.0
Weighted Recall: 0.5697399527186762
Weighted FalsePositiveRate: 0.1419354152602622
Kappa statistic: 0.428221296316927
Training time: 41.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.4773176615697158
Weighted AreaUnderROC: 0.8079446098745552
Root mean squared error: 0.3868996743662253
Relative absolute error: 80.79852902547921
Root relative squared error: 89.3506524578875
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.42736220881007564
Weighted FMeasure: 0.5099103723585511
Iteration time: 46.0
Weighted AreaUnderPRC: 0.5483923685275415
Mean absolute error: 0.30299448384554706
Coverage of cases: 93.14420803782505
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 139.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.1389421992835056
Kappa statistic: 0.44345828780411656
Training time: 45.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 47.28132387706856
Correctly Classified Instances: 52.71867612293144
Weighted Precision: 0.5030273870660147
Weighted AreaUnderROC: 0.8044563828497435
Root mean squared error: 0.39195850334924537
Relative absolute error: 81.79669030732842
Root relative squared error: 90.51893896793986
Weighted TruePositiveRate: 0.5271867612293144
Weighted MatthewsCorrelation: 0.39361143567761586
Weighted FMeasure: 0.46468492820848556
Iteration time: 54.0
Weighted AreaUnderPRC: 0.551976533871841
Mean absolute error: 0.3067375886524816
Coverage of cases: 93.14420803782505
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 193.0
Weighted Recall: 0.5271867612293144
Weighted FalsePositiveRate: 0.15734652036917215
Kappa statistic: 0.3711673542200914
Training time: 53.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 56.973995271867615
Correctly Classified Instances: 43.026004728132385
Weighted Precision: 0.548392997861083
Weighted AreaUnderROC: 0.7865773010327328
Root mean squared error: 0.402701345972182
Relative absolute error: 84.05568689256613
Root relative squared error: 92.99989220002557
Weighted TruePositiveRate: 0.4302600472813239
Weighted MatthewsCorrelation: 0.2856697764210614
Weighted FMeasure: 0.32588817091864375
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5341981864864902
Mean absolute error: 0.315208825847123
Coverage of cases: 92.67139479905437
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 246.0
Weighted Recall: 0.4302600472813239
Weighted FalsePositiveRate: 0.19004362200957148
Kappa statistic: 0.24254380098970177
Training time: 52.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 51.30023640661938
Correctly Classified Instances: 48.69976359338062
Weighted Precision: 0.5064327041798882
Weighted AreaUnderROC: 0.790835172301172
Root mean squared error: 0.3986449335326593
Relative absolute error: 83.1625952193327
Root relative squared error: 92.06310387446453
Weighted TruePositiveRate: 0.48699763593380613
Weighted MatthewsCorrelation: 0.3632749779655842
Weighted FMeasure: 0.43653160652069084
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5371806971589826
Mean absolute error: 0.3118597320724976
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 300.0
Weighted Recall: 0.48699763593380613
Weighted FalsePositiveRate: 0.17143142281557766
Kappa statistic: 0.3166956987806512
Training time: 53.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 44.680851063829785
Correctly Classified Instances: 55.319148936170215
Weighted Precision: 0.5095167074210184
Weighted AreaUnderROC: 0.8045375366676413
Root mean squared error: 0.39170711358312843
Relative absolute error: 81.7441555030206
Root relative squared error: 90.46088298828421
Weighted TruePositiveRate: 0.5531914893617021
Weighted MatthewsCorrelation: 0.41612034228542294
Weighted FMeasure: 0.47892533759402656
Iteration time: 55.0
Weighted AreaUnderPRC: 0.554902828337746
Mean absolute error: 0.3065405831363272
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 355.0
Weighted Recall: 0.5531914893617021
Weighted FalsePositiveRate: 0.14770999146681474
Kappa statistic: 0.40653393907001606
Training time: 54.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 44.917257683215134
Correctly Classified Instances: 55.082742316784866
Weighted Precision: 0.4940750516124495
Weighted AreaUnderROC: 0.8015418309428508
Root mean squared error: 0.39212600699080125
Relative absolute error: 81.84922511163627
Root relative squared error: 90.55762227695688
Weighted TruePositiveRate: 0.5508274231678487
Weighted MatthewsCorrelation: 0.4062112391550303
Weighted FMeasure: 0.4730277326619556
Iteration time: 57.0
Weighted AreaUnderPRC: 0.5416857987600245
Mean absolute error: 0.30693459416863605
Coverage of cases: 92.19858156028369
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 412.0
Weighted Recall: 0.5508274231678487
Weighted FalsePositiveRate: 0.1475629170007499
Kappa statistic: 0.403871829105474
Training time: 56.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 50.118203309692674
Correctly Classified Instances: 49.881796690307326
Weighted Precision: 0.4657720984234896
Weighted AreaUnderROC: 0.7710876139197281
Root mean squared error: 0.40245666665077395
Relative absolute error: 83.9506172839505
Root relative squared error: 92.94338593119353
Weighted TruePositiveRate: 0.4988179669030733
Weighted MatthewsCorrelation: 0.3421470919301382
Weighted FMeasure: 0.4034477600090443
Iteration time: 63.0
Weighted AreaUnderPRC: 0.4851965083983076
Mean absolute error: 0.3148148148148144
Coverage of cases: 91.48936170212765
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 475.0
Weighted Recall: 0.4988179669030733
Weighted FalsePositiveRate: 0.16363477660102954
Kappa statistic: 0.3358071325408287
Training time: 62.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 50.118203309692674
Correctly Classified Instances: 49.881796690307326
Weighted Precision: 0.46263292853255555
Weighted AreaUnderROC: 0.7597882290932935
Root mean squared error: 0.40562598565878394
Relative absolute error: 84.63356973995268
Root relative squared error: 93.67530880416248
Weighted TruePositiveRate: 0.4988179669030733
Weighted MatthewsCorrelation: 0.341252527727787
Weighted FMeasure: 0.401334291485086
Iteration time: 63.0
Weighted AreaUnderPRC: 0.46776981508545207
Mean absolute error: 0.31737588652482257
Coverage of cases: 91.25295508274232
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 538.0
Weighted Recall: 0.4988179669030733
Weighted FalsePositiveRate: 0.1631073864983838
Kappa statistic: 0.3360726739666393
Training time: 62.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.0
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.49997827528940897
Weighted AreaUnderROC: 0.8013219344627428
Root mean squared error: 0.39354690946581133
Relative absolute error: 82.11189913317571
Root relative squared error: 90.8857656475326
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.45424748611835475
Weighted FMeasure: 0.5244301629193507
Iteration time: 66.0
Weighted AreaUnderPRC: 0.5540991759350268
Mean absolute error: 0.3079196217494089
Coverage of cases: 90.78014184397163
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 604.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.1310526680192497
Kappa statistic: 0.4694291058069113
Training time: 65.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.5161066010743395
Weighted AreaUnderROC: 0.8237614929862644
Root mean squared error: 0.39128777172907053
Relative absolute error: 81.58655109009725
Root relative squared error: 90.36404013535508
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5092453708719334
Weighted FMeasure: 0.5697687490644978
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6073146013199991
Mean absolute error: 0.30594956658786465
Coverage of cases: 90.30732860520095
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 669.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11290112467386398
Kappa statistic: 0.5413714744018179
Training time: 64.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5102813013690819
Weighted AreaUnderROC: 0.8268615407512729
Root mean squared error: 0.39065791495157887
Relative absolute error: 81.48148148148154
Root relative squared error: 90.21858094334081
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5142309546825544
Weighted FMeasure: 0.5740785097802733
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6039856544951877
Mean absolute error: 0.3055555555555558
Coverage of cases: 90.30732860520095
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 732.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11050927655470165
Kappa statistic: 0.5536383092562868
Training time: 63.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5096911919067332
Weighted AreaUnderROC: 0.8305249753788556
Root mean squared error: 0.38301885205519803
Relative absolute error: 79.90543735224578
Root relative squared error: 88.45441493550801
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.513894710254279
Weighted FMeasure: 0.5739127917053356
Iteration time: 60.0
Weighted AreaUnderPRC: 0.5935323534297298
Mean absolute error: 0.2996453900709217
Coverage of cases: 91.48936170212765
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 792.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11059801469829969
Kappa statistic: 0.5536084542839943
Training time: 60.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.5060891398647971
Weighted AreaUnderROC: 0.8350137415471487
Root mean squared error: 0.3768396779728316
Relative absolute error: 78.64460204885722
Root relative squared error: 87.02739580757849
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5100816855669812
Weighted FMeasure: 0.571181687493434
Iteration time: 61.0
Weighted AreaUnderPRC: 0.5989930762167202
Mean absolute error: 0.2949172576832146
Coverage of cases: 93.3806146572104
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 853.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11144880112354652
Kappa statistic: 0.5504448061358721
Training time: 60.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.7649842494083007
Weighted AreaUnderROC: 0.8413851880900867
Root mean squared error: 0.37250174748330894
Relative absolute error: 77.80404517993159
Root relative squared error: 86.02559367323776
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5649089233582795
Weighted FMeasure: 0.5996581298304254
Iteration time: 60.0
Weighted AreaUnderPRC: 0.6077203585099157
Mean absolute error: 0.29176516942474345
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 913.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10740752328675506
Kappa statistic: 0.5661538461538461
Training time: 59.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.756849780419297
Weighted AreaUnderROC: 0.8616641365037905
Root mean squared error: 0.3606334356581909
Relative absolute error: 75.49251379038603
Root relative squared error: 83.28472446241445
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6391580814032349
Weighted FMeasure: 0.6936479943678647
Iteration time: 61.0
Weighted AreaUnderPRC: 0.6449933747045498
Mean absolute error: 0.2830969267139476
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 974.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09085683350048282
Kappa statistic: 0.632023495297223
Training time: 60.0
		
Time end:Sun Oct 08 09.54.14 EEST 2017