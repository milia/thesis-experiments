Sun Oct 08 09.55.34 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.34 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 43.498817966903076
Correctly Classified Instances: 56.501182033096924
Weighted Precision: 0.5736948086384961
Weighted AreaUnderROC: 0.7092133025455782
Root mean squared error: 0.4560651176490609
Relative absolute error: 59.907586503331224
Root relative squared error: 105.32372737707347
Weighted TruePositiveRate: 0.5650118203309693
Weighted MatthewsCorrelation: 0.4225931649114132
Weighted FMeasure: 0.5666474159364929
Iteration time: 2.0
Weighted AreaUnderPRC: 0.4533583385719663
Mean absolute error: 0.22465344938749207
Coverage of cases: 56.501182033096924
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5650118203309693
Weighted FalsePositiveRate: 0.14658521523981297
Kappa statistic: 0.4194111460050576
Training time: 1.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.607565011820334
Correctly Classified Instances: 58.392434988179666
Weighted Precision: 0.586314502743094
Weighted AreaUnderROC: 0.7220356477314894
Root mean squared error: 0.4478726627886266
Relative absolute error: 57.12576248431228
Root relative squared error: 103.43176096947522
Weighted TruePositiveRate: 0.5839243498817966
Weighted MatthewsCorrelation: 0.44516312522032464
Weighted FMeasure: 0.5836035989175151
Iteration time: 1.0
Weighted AreaUnderPRC: 0.47266299991983396
Mean absolute error: 0.21422160931617107
Coverage of cases: 58.392434988179666
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5839243498817966
Weighted FalsePositiveRate: 0.13985305441881776
Kappa statistic: 0.44495224746326295
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6241671304171685
Weighted AreaUnderROC: 0.7506608114628904
Root mean squared error: 0.42556791040572495
Relative absolute error: 51.371650906225376
Root relative squared error: 98.28069905248474
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5001645246140117
Weighted FMeasure: 0.623969141683811
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5054821127032973
Mean absolute error: 0.19264369089834515
Coverage of cases: 62.64775413711584
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12515591844537752
Kappa statistic: 0.5018707609748826
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6201695439573444
Weighted AreaUnderROC: 0.7489747572109354
Root mean squared error: 0.4277857829150219
Relative absolute error: 51.46635997699844
Root relative squared error: 98.79289476859309
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.49623967175902317
Weighted FMeasure: 0.6204083362887796
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5045434967388508
Mean absolute error: 0.19299884991374416
Coverage of cases: 62.4113475177305
Instances selection time: 1.0
Test time: 19.0
Accumulative iteration time: 5.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.1261639607554344
Kappa statistic: 0.4985535988548082
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6191018307703909
Weighted AreaUnderROC: 0.7490557131725365
Root mean squared error: 0.42845620003348317
Relative absolute error: 51.305865135652276
Root relative squared error: 98.94772097011827
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.49580025124905985
Weighted FMeasure: 0.6214348445414339
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5075593818018709
Mean absolute error: 0.19239699425869602
Coverage of cases: 62.4113475177305
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 6.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.12600204883223173
Kappa statistic: 0.4987217804145456
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.2434988179669
Correctly Classified Instances: 60.7565011820331
Weighted Precision: 0.6118343516215857
Weighted AreaUnderROC: 0.7379957058201353
Root mean squared error: 0.4383235492490279
Relative absolute error: 53.3390339184817
Root relative squared error: 101.22648766043137
Weighted TruePositiveRate: 0.607565011820331
Weighted MatthewsCorrelation: 0.4776411026245149
Weighted FMeasure: 0.6091690277134427
Iteration time: 1.0
Weighted AreaUnderPRC: 0.502161589693404
Mean absolute error: 0.20002137719430635
Coverage of cases: 60.7565011820331
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 7.0
Weighted Recall: 0.607565011820331
Weighted FalsePositiveRate: 0.13157360018006062
Kappa statistic: 0.476785514697664
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.6137621219137588
Weighted AreaUnderROC: 0.7346210615950997
Root mean squared error: 0.4413978090570491
Relative absolute error: 53.859792689580054
Root relative squared error: 101.93645755151934
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.4741200527110286
Weighted FMeasure: 0.607826055938698
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5082994588892336
Mean absolute error: 0.2019742225859252
Coverage of cases: 60.283687943262414
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.13359475624242492
Kappa statistic: 0.47022118846867794
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.6165051419777815
Weighted AreaUnderROC: 0.7393335421846088
Root mean squared error: 0.4378030336844218
Relative absolute error: 52.851395628551366
Root relative squared error: 101.1062797398943
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.48171516841467893
Weighted FMeasure: 0.6128858990479747
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5130702723561863
Mean absolute error: 0.19819273360706763
Coverage of cases: 60.99290780141844
Instances selection time: 1.0
Test time: 27.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.13126199364496668
Kappa statistic: 0.4795961734903107
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6234201751152146
Weighted AreaUnderROC: 0.7424781974761828
Root mean squared error: 0.4354452537029561
Relative absolute error: 52.163247667709015
Root relative squared error: 100.56177377709864
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.48902484521733763
Weighted FMeasure: 0.618676151488886
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5183314958601428
Mean absolute error: 0.19561217875390882
Coverage of cases: 61.46572104018912
Instances selection time: 1.0
Test time: 29.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12970081544952544
Kappa statistic: 0.48583892617449664
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6358137792234059
Weighted AreaUnderROC: 0.7518933354944208
Root mean squared error: 0.427610951959931
Relative absolute error: 50.24169930489384
Root relative squared error: 98.75251928899932
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5070961635580589
Weighted FMeasure: 0.6319675864932444
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5320810400075963
Mean absolute error: 0.18840637239335192
Coverage of cases: 62.88416075650118
Instances selection time: 0.0
Test time: 31.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12505493657617023
Kappa statistic: 0.5047355546937924
Training time: 0.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.6260574837535242
Weighted AreaUnderROC: 0.7455933619573349
Root mean squared error: 0.433243973120162
Relative absolute error: 51.432667892478655
Root relative squared error: 100.05340980228343
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.49438230742184114
Weighted FMeasure: 0.6223261810631858
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5204353453785022
Mean absolute error: 0.19287250459679495
Coverage of cases: 61.938534278959814
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.12819861887492803
Kappa statistic: 0.49209841371646773
Training time: 0.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6211312032078815
Weighted AreaUnderROC: 0.7424680872749898
Root mean squared error: 0.4361201590301794
Relative absolute error: 52.010479670053975
Root relative squared error: 100.7176364860386
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.4880493156906922
Weighted FMeasure: 0.6174854462715557
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5156465719654855
Mean absolute error: 0.19503929876270243
Coverage of cases: 61.46572104018912
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12972103585191144
Kappa statistic: 0.48578140731625463
Training time: 0.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6276013454499205
Weighted AreaUnderROC: 0.7488072791031408
Root mean squared error: 0.4309050435928074
Relative absolute error: 50.726517903477124
Root relative squared error: 99.51325716538993
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.4993901827284557
Weighted FMeasure: 0.6255483732777788
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5234668055252727
Mean absolute error: 0.19022444213803924
Coverage of cases: 62.4113475177305
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 13.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.12649891697102353
Kappa statistic: 0.49842271293375395
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6312875025081073
Weighted AreaUnderROC: 0.7520162368284875
Root mean squared error: 0.4283350021943192
Relative absolute error: 50.06838582284907
Root relative squared error: 98.91973152809166
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5052871134876153
Weighted FMeasure: 0.6299020818985844
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5272078391345678
Mean absolute error: 0.18775644683568402
Coverage of cases: 62.88416075650118
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12480913390803706
Kappa statistic: 0.5047872578408125
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6384990370837816
Weighted AreaUnderROC: 0.7566844440263351
Root mean squared error: 0.42435436719144004
Relative absolute error: 49.10148353719138
Root relative squared error: 98.0004432519085
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5147381415605987
Weighted FMeasure: 0.6368839061947097
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5334203194011999
Mean absolute error: 0.18413056326446767
Coverage of cases: 63.593380614657214
Instances selection time: 0.0
Test time: 39.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12256491809390194
Kappa statistic: 0.5141992065149301
Training time: 1.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6410145658023559
Weighted AreaUnderROC: 0.7614456046752204
Root mean squared error: 0.4203175623550937
Relative absolute error: 48.136774633813566
Root relative squared error: 97.06818310833627
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5222310597899968
Weighted FMeasure: 0.6416522651766905
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5395523506337038
Mean absolute error: 0.18051290487680088
Coverage of cases: 64.30260047281324
Instances selection time: 0.0
Test time: 41.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.12013479537769191
Kappa statistic: 0.5236735150453038
Training time: 0.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6373235696628595
Weighted AreaUnderROC: 0.7598883466510853
Root mean squared error: 0.42181227052077563
Relative absolute error: 48.42241072946101
Root relative squared error: 97.41337117306281
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5184675625056843
Weighted FMeasure: 0.6386258163543266
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5388188217496368
Mean absolute error: 0.1815840402354788
Coverage of cases: 64.06619385342789
Instances selection time: 0.0
Test time: 43.0
Accumulative iteration time: 14.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12088524523210847
Kappa statistic: 0.5205833886349571
Training time: 0.0
		
Time end:Sun Oct 08 09.55.35 EEST 2017