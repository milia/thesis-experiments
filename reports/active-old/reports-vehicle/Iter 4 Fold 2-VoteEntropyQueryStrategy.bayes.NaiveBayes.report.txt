Sun Oct 08 09.56.07 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.07 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 39.0661938534279
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.5739326336579996
Weighted AreaUnderROC: 0.7961555271558377
Root mean squared error: 0.457102123284739
Relative absolute error: 66.914921866104
Root relative squared error: 105.56321357023744
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.3712357566571153
Weighted FMeasure: 0.48853729757368847
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5838045851154048
Mean absolute error: 0.25093095699789
Coverage of cases: 73.75886524822695
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.1592159111053139
Kappa statistic: 0.34288588717432783
Training time: 1.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 43.91252955082742
Incorrectly Classified Instances: 56.02836879432624
Correctly Classified Instances: 43.97163120567376
Weighted Precision: 0.6071560271140447
Weighted AreaUnderROC: 0.7808530006886493
Root mean squared error: 0.47828100172455806
Relative absolute error: 74.20025427727332
Root relative squared error: 110.45426603758298
Weighted TruePositiveRate: 0.4397163120567376
Weighted MatthewsCorrelation: 0.3000279480929934
Weighted FMeasure: 0.3575139050275196
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5438519716025622
Mean absolute error: 0.2782509535397749
Coverage of cases: 74.94089834515367
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 14.0
Weighted Recall: 0.4397163120567376
Weighted FalsePositiveRate: 0.17802761893649738
Kappa statistic: 0.26083846993246235
Training time: 2.0
		
Time end:Sun Oct 08 09.56.07 EEST 2017