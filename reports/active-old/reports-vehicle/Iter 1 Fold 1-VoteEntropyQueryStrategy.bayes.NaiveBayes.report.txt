Sun Oct 08 09.56.06 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.06 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 38.17966903073286
Incorrectly Classified Instances: 59.810874704491724
Correctly Classified Instances: 40.189125295508276
Weighted Precision: 0.42258359454081257
Weighted AreaUnderROC: 0.736997418355665
Root mean squared error: 0.49978642221250225
Relative absolute error: 78.12717723571389
Root relative squared error: 115.42073016068326
Weighted TruePositiveRate: 0.40189125295508277
Weighted MatthewsCorrelation: 0.21634258971334627
Weighted FMeasure: 0.3644925386086929
Iteration time: 21.0
Weighted AreaUnderPRC: 0.47423893674612705
Mean absolute error: 0.2929769146339271
Coverage of cases: 64.06619385342789
Instances selection time: 20.0
Test time: 17.0
Accumulative iteration time: 21.0
Weighted Recall: 0.40189125295508277
Weighted FalsePositiveRate: 0.1935243364612577
Kappa statistic: 0.2080586080586081
Training time: 1.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 43.61702127659574
Incorrectly Classified Instances: 57.21040189125296
Correctly Classified Instances: 42.78959810874704
Weighted Precision: 0.5330279304894214
Weighted AreaUnderROC: 0.7595358508697059
Root mean squared error: 0.47883571681506115
Relative absolute error: 77.52038425134816
Root relative squared error: 110.58237200031319
Weighted TruePositiveRate: 0.42789598108747046
Weighted MatthewsCorrelation: 0.27013953768549137
Weighted FMeasure: 0.35339989894414814
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5199532559378929
Mean absolute error: 0.2907014409425556
Coverage of cases: 68.79432624113475
Instances selection time: 2.0
Test time: 14.0
Accumulative iteration time: 25.0
Weighted Recall: 0.42789598108747046
Weighted FalsePositiveRate: 0.18327258545984085
Kappa statistic: 0.2440794866303842
Training time: 2.0
		
Time end:Sun Oct 08 09.56.06 EEST 2017