Sun Oct 08 09.54.16 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.54.16 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6435965452499278
Weighted AreaUnderROC: 0.8077732896482025
Root mean squared error: 0.37718803920088284
Relative absolute error: 78.90727607039655
Root relative squared error: 87.10784638709474
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.5199301462414682
Weighted FMeasure: 0.6158149940782246
Iteration time: 42.0
Weighted AreaUnderPRC: 0.5796853538179392
Mean absolute error: 0.29590228526398704
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12086792594233248
Kappa statistic: 0.5205905379711442
Training time: 41.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 76.71394799054373
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6390889529964919
Weighted AreaUnderROC: 0.8037528919291935
Root mean squared error: 0.37360193815802045
Relative absolute error: 78.27685841870223
Root relative squared error: 86.27967182611962
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5061195411285733
Weighted FMeasure: 0.6310792990508087
Iteration time: 41.0
Weighted AreaUnderPRC: 0.5789040808899738
Mean absolute error: 0.2935382190701334
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 83.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12568482945737053
Kappa statistic: 0.5020155131176002
Training time: 40.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 76.71394799054373
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.6043503365196882
Weighted AreaUnderROC: 0.7892843979392973
Root mean squared error: 0.3806972437308839
Relative absolute error: 79.74783293932214
Root relative squared error: 87.91826245910977
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.4272482320514162
Weighted FMeasure: 0.5625068889468233
Iteration time: 42.0
Weighted AreaUnderPRC: 0.5535718520870615
Mean absolute error: 0.299054373522458
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 125.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.14922674862637117
Kappa statistic: 0.4078982636924085
Training time: 41.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 76.53664302600473
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5940840789540554
Weighted AreaUnderROC: 0.7979564072604128
Root mean squared error: 0.3793147707355131
Relative absolute error: 79.38008930916725
Root relative squared error: 87.59899399669987
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.4503850844392812
Weighted FMeasure: 0.5775781625671991
Iteration time: 43.0
Weighted AreaUnderPRC: 0.5430081768338166
Mean absolute error: 0.2976753349093772
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 168.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.13978996227901433
Kappa statistic: 0.44772299979110086
Training time: 42.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.88652482269504
Incorrectly Classified Instances: 38.77068557919622
Correctly Classified Instances: 61.22931442080378
Weighted Precision: 0.6126869292993159
Weighted AreaUnderROC: 0.8109831657854066
Root mean squared error: 0.375268050595492
Relative absolute error: 78.48699763593372
Root relative squared error: 86.66444401182937
Weighted TruePositiveRate: 0.6122931442080378
Weighted MatthewsCorrelation: 0.48179937310926
Weighted FMeasure: 0.5935424094762392
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5721438172294423
Mean absolute error: 0.2943262411347514
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 212.0
Weighted Recall: 0.6122931442080378
Weighted FalsePositiveRate: 0.13071980874134354
Kappa statistic: 0.48286966633867073
Training time: 43.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 76.41843971631205
Incorrectly Classified Instances: 39.2434988179669
Correctly Classified Instances: 60.7565011820331
Weighted Precision: 0.7808219412752254
Weighted AreaUnderROC: 0.8536679998371077
Root mean squared error: 0.3705131718400314
Relative absolute error: 77.69897557131588
Root relative squared error: 85.56635180005769
Weighted TruePositiveRate: 0.607565011820331
Weighted MatthewsCorrelation: 0.4937370619311338
Weighted FMeasure: 0.5415920717807545
Iteration time: 46.0
Weighted AreaUnderPRC: 0.6173252715281575
Mean absolute error: 0.29137115839243455
Coverage of cases: 99.76359338061465
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 258.0
Weighted Recall: 0.607565011820331
Weighted FalsePositiveRate: 0.13195269841226184
Kappa statistic: 0.4766139190972041
Training time: 45.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 76.12293144208037
Incorrectly Classified Instances: 40.42553191489362
Correctly Classified Instances: 59.57446808510638
Weighted Precision: 0.5248790908659541
Weighted AreaUnderROC: 0.855072563887351
Root mean squared error: 0.3718842189989047
Relative absolute error: 77.96164959285518
Root relative squared error: 85.88298157855655
Weighted TruePositiveRate: 0.5957446808510638
Weighted MatthewsCorrelation: 0.4642740109399482
Weighted FMeasure: 0.5289821234201437
Iteration time: 47.0
Weighted AreaUnderPRC: 0.6132542322554917
Mean absolute error: 0.29235618597320695
Coverage of cases: 99.76359338061465
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 305.0
Weighted Recall: 0.5957446808510638
Weighted FalsePositiveRate: 0.13572608223194504
Kappa statistic: 0.46084525939177096
Training time: 46.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 76.4775413711584
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.5144028065262322
Weighted AreaUnderROC: 0.8507868580151031
Root mean squared error: 0.37033589280029416
Relative absolute error: 77.5939059627002
Root relative squared error: 85.5254109595321
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.48500420403202715
Weighted FMeasure: 0.5498801393380515
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5987598712585607
Mean absolute error: 0.29097714736012575
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 358.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12427078267850607
Kappa statistic: 0.5029672928472625
Training time: 52.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 76.77304964539007
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.5086802909457216
Weighted AreaUnderROC: 0.8451749164037122
Root mean squared error: 0.3719725001203853
Relative absolute error: 77.96164959285518
Root relative squared error: 85.90336923025703
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.47974033804991506
Weighted FMeasure: 0.5466863293911406
Iteration time: 55.0
Weighted AreaUnderPRC: 0.5910226643055769
Mean absolute error: 0.29235618597320695
Coverage of cases: 99.76359338061465
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 413.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.12473815687237085
Kappa statistic: 0.4999888484956621
Training time: 54.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 45.8628841607565
Correctly Classified Instances: 54.1371158392435
Weighted Precision: 0.4720638969156138
Weighted AreaUnderROC: 0.804803887486522
Root mean squared error: 0.3866449954525943
Relative absolute error: 80.85106382978712
Root relative squared error: 89.29183688215079
Weighted TruePositiveRate: 0.541371158392435
Weighted MatthewsCorrelation: 0.3882911743751346
Weighted FMeasure: 0.4523055242468956
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5164597574176066
Mean absolute error: 0.3031914893617017
Coverage of cases: 98.81796690307328
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 467.0
Weighted Recall: 0.541371158392435
Weighted FalsePositiveRate: 0.15028797638257108
Kappa statistic: 0.3916331205657985
Training time: 53.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 76.4775413711584
Incorrectly Classified Instances: 42.78959810874704
Correctly Classified Instances: 57.21040189125296
Weighted Precision: 0.4833377070800866
Weighted AreaUnderROC: 0.8109958524294465
Root mean squared error: 0.3839606680233209
Relative absolute error: 80.37825059101651
Root relative squared error: 88.67191801659716
Weighted TruePositiveRate: 0.5721040189125296
Weighted MatthewsCorrelation: 0.42196250201026025
Weighted FMeasure: 0.49104091490665985
Iteration time: 54.0
Weighted AreaUnderPRC: 0.5341453423560067
Mean absolute error: 0.3014184397163119
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 521.0
Weighted Recall: 0.5721040189125296
Weighted FalsePositiveRate: 0.14004151242357224
Kappa statistic: 0.4321473866898072
Training time: 54.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 76.4775413711584
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5233055094872856
Weighted AreaUnderROC: 0.8497869461982995
Root mean squared error: 0.37002545010761617
Relative absolute error: 77.54137115839244
Root relative squared error: 85.45371729065785
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5205117981651205
Weighted FMeasure: 0.5790828832532504
Iteration time: 53.0
Weighted AreaUnderPRC: 0.6126781075011308
Mean absolute error: 0.2907801418439716
Coverage of cases: 99.52718676122932
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 574.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11091610968413113
Kappa statistic: 0.5534989518754738
Training time: 52.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 76.18203309692672
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.770975029347724
Weighted AreaUnderROC: 0.8386406837707193
Root mean squared error: 0.3770138988226253
Relative absolute error: 78.85474126608882
Root relative squared error: 87.06763038938924
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5442090199811875
Weighted FMeasure: 0.5858431294012467
Iteration time: 56.0
Weighted AreaUnderPRC: 0.6012291944043757
Mean absolute error: 0.2957052797478331
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 630.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10835014445040106
Kappa statistic: 0.5658182467310295
Training time: 55.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5046528171079261
Weighted AreaUnderROC: 0.8349973682954993
Root mean squared error: 0.3787950425570872
Relative absolute error: 79.11741528762809
Root relative squared error: 87.47896791521201
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5109582426006624
Weighted FMeasure: 0.5724707776346315
Iteration time: 56.0
Weighted AreaUnderPRC: 0.5909886446100453
Mean absolute error: 0.29669030732860535
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 686.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11172315090808425
Kappa statistic: 0.553160149973219
Training time: 55.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.7595689649882336
Weighted AreaUnderROC: 0.8402970437968084
Root mean squared error: 0.3734261254715039
Relative absolute error: 78.06671920147092
Root relative squared error: 86.23906962536469
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.530445328512993
Weighted FMeasure: 0.5759628983703621
Iteration time: 60.0
Weighted AreaUnderPRC: 0.5982148322306916
Mean absolute error: 0.2927501970055159
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 746.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11172315090808425
Kappa statistic: 0.5531335555291037
Training time: 59.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 76.59574468085107
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6922534887879814
Weighted AreaUnderROC: 0.8460555357243452
Root mean squared error: 0.3681572967011015
Relative absolute error: 77.27869713685301
Root relative squared error: 85.02228574180234
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5475258301803325
Weighted FMeasure: 0.5931011990468443
Iteration time: 56.0
Weighted AreaUnderPRC: 0.6091188698135687
Mean absolute error: 0.28979511426319876
Coverage of cases: 98.10874704491725
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 802.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10754784672425521
Kappa statistic: 0.5689709032488397
Training time: 56.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6646416885416542
Weighted AreaUnderROC: 0.8330386435535324
Root mean squared error: 0.36542701829507057
Relative absolute error: 76.49067507223528
Root relative squared error: 84.39175495272853
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.572640348309924
Weighted FMeasure: 0.6716359622003399
Iteration time: 60.0
Weighted AreaUnderPRC: 0.6035359118232111
Mean absolute error: 0.2868400315208823
Coverage of cases: 98.58156028368795
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 862.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10439478912919008
Kappa statistic: 0.5842070773263434
Training time: 60.0
		
Time end:Sun Oct 08 09.54.17 EEST 2017