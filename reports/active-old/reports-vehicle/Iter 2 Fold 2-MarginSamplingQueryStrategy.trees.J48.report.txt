Sun Oct 08 09.54.46 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.54.46 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 32.860520094562645
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6281774474062158
Weighted AreaUnderROC: 0.7835851836869755
Root mean squared error: 0.42161054361335243
Relative absolute error: 51.25295508274223
Root relative squared error: 97.36678433934141
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5007905695592993
Weighted FMeasure: 0.6173756246800798
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5353309212003275
Mean absolute error: 0.19219858156028338
Coverage of cases: 75.41371158392435
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12746587289489683
Kappa statistic: 0.500676135047703
Training time: 3.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 37.05673758865248
Incorrectly Classified Instances: 39.479905437352244
Correctly Classified Instances: 60.520094562647756
Weighted Precision: 0.6198703918609991
Weighted AreaUnderROC: 0.8179910916713534
Root mean squared error: 0.4000630988587598
Relative absolute error: 51.12047526941149
Root relative squared error: 92.39061512757634
Weighted TruePositiveRate: 0.6052009456264775
Weighted MatthewsCorrelation: 0.4785347678945862
Weighted FMeasure: 0.6056287642411647
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5853754481119657
Mean absolute error: 0.1917017822602931
Coverage of cases: 82.26950354609929
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6052009456264775
Weighted FalsePositiveRate: 0.13369853039402954
Kappa statistic: 0.47257251651920706
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 38.652482269503544
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6614518930782713
Weighted AreaUnderROC: 0.8340639599446421
Root mean squared error: 0.38524568707268364
Relative absolute error: 46.21785228168208
Root relative squared error: 88.9686804542225
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5519560806681227
Weighted FMeasure: 0.6598258022930698
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6150186120935681
Mean absolute error: 0.1733169460563078
Coverage of cases: 86.05200945626477
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11287407079968716
Kappa statistic: 0.5551055116700606
Training time: 10.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 35.520094562647756
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.661581855381003
Weighted AreaUnderROC: 0.8113834546049586
Root mean squared error: 0.39776394838900797
Relative absolute error: 47.04432027127064
Root relative squared error: 91.85964907052887
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5420686603613255
Weighted FMeasure: 0.6558170310818325
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5788393421711315
Mean absolute error: 0.1764162010172649
Coverage of cases: 79.66903073286052
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11539809718113338
Kappa statistic: 0.539919393889729
Training time: 11.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 37.174940898345156
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6392340348075246
Weighted AreaUnderROC: 0.7756113528627995
Root mean squared error: 0.41476508494427855
Relative absolute error: 51.60220826887502
Root relative squared error: 95.78589337721489
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5104992811596432
Weighted FMeasure: 0.6320516808444304
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5686432838170634
Mean absolute error: 0.19350828100828132
Coverage of cases: 75.65011820330969
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12356518953657306
Kappa statistic: 0.5085754287714387
Training time: 10.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.09219858156028
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6927814055184545
Weighted AreaUnderROC: 0.7951457276264006
Root mean squared error: 0.39000395567592167
Relative absolute error: 46.229672612651314
Root relative squared error: 90.0675555178049
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5745429911873788
Weighted FMeasure: 0.679112528359864
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5994838445877528
Mean absolute error: 0.17336127229744244
Coverage of cases: 72.10401891252955
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10907123904294948
Kappa statistic: 0.5680810309232248
Training time: 9.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 37.765957446808514
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6250831407204731
Weighted AreaUnderROC: 0.7942950117192834
Root mean squared error: 0.39335246487557873
Relative absolute error: 50.75802264454754
Root relative squared error: 90.84086059292729
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.48971426302211624
Weighted FMeasure: 0.6100277729237427
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6146967155964734
Mean absolute error: 0.19034258491705328
Coverage of cases: 77.06855791962175
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 82.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12857986640596342
Kappa statistic: 0.48699805807906077
Training time: 11.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 35.815602836879435
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.66076083986268
Weighted AreaUnderROC: 0.7987660817745226
Root mean squared error: 0.3900081823114374
Relative absolute error: 47.26761024633365
Root relative squared error: 90.06853161746601
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5458721847663223
Weighted FMeasure: 0.646727832591466
Iteration time: 14.0
Weighted AreaUnderPRC: 0.61415745260683
Mean absolute error: 0.17725353842375122
Coverage of cases: 75.177304964539
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.1131985446521796
Kappa statistic: 0.5468464044993974
Training time: 13.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 35.520094562647756
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6720281446360218
Weighted AreaUnderROC: 0.7960742176267015
Root mean squared error: 0.3877885864630608
Relative absolute error: 48.08548163158093
Root relative squared error: 89.55593791324505
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5571655854785514
Weighted FMeasure: 0.6662797418613046
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6009751961273032
Mean absolute error: 0.18032055611842848
Coverage of cases: 75.65011820330969
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 110.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11112053737650662
Kappa statistic: 0.5559468413803372
Training time: 14.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 36.46572104018912
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6679086223402919
Weighted AreaUnderROC: 0.8229358341283454
Root mean squared error: 0.38233284059785727
Relative absolute error: 45.88936761513589
Root relative squared error: 88.29598737568287
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5552462498258011
Weighted FMeasure: 0.6595905367645906
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6318826327602358
Mean absolute error: 0.1720851285567596
Coverage of cases: 80.85106382978724
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 126.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11070509953516108
Kappa statistic: 0.5563465143265196
Training time: 16.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 34.810874704491724
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.667156029165581
Weighted AreaUnderROC: 0.8087332295067204
Root mean squared error: 0.37970204477486846
Relative absolute error: 47.65154414799807
Root relative squared error: 87.68843110504866
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5484209893809412
Weighted FMeasure: 0.6589843984259567
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5895332342199885
Mean absolute error: 0.17869329055499275
Coverage of cases: 77.54137115839244
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 142.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11429416195969587
Kappa statistic: 0.5461982030307093
Training time: 15.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 37.765957446808514
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6762065344992917
Weighted AreaUnderROC: 0.828295771278413
Root mean squared error: 0.3807236960107774
Relative absolute error: 45.35054473588755
Root relative squared error: 87.92437135147664
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5608087548547088
Weighted FMeasure: 0.6720622235130516
Iteration time: 18.0
Weighted AreaUnderPRC: 0.626732086685513
Mean absolute error: 0.17006454275957833
Coverage of cases: 82.50591016548464
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 160.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11166024912690535
Kappa statistic: 0.5582706766917293
Training time: 17.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 37.174940898345156
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6284441065635148
Weighted AreaUnderROC: 0.788181579119095
Root mean squared error: 0.40279035485180364
Relative absolute error: 50.63043726980794
Root relative squared error: 93.02044792026949
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5057194921114601
Weighted FMeasure: 0.6289628223763262
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5834515054589641
Mean absolute error: 0.18986413976177977
Coverage of cases: 77.77777777777777
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 179.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12413268792232511
Kappa statistic: 0.5081065366639087
Training time: 18.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 35.815602836879435
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6627045177722938
Weighted AreaUnderROC: 0.7780748672121484
Root mean squared error: 0.3982355371205923
Relative absolute error: 48.740415525285584
Root relative squared error: 91.96855782297968
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5460172028479976
Weighted FMeasure: 0.65939716281349
Iteration time: 20.0
Weighted AreaUnderPRC: 0.585693443780472
Mean absolute error: 0.18277655821982094
Coverage of cases: 74.70449172576832
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 199.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11548670856203572
Kappa statistic: 0.5454124811558726
Training time: 20.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.97872340425532
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.6862733507527734
Weighted AreaUnderROC: 0.7923751777076655
Root mean squared error: 0.38424484634767464
Relative absolute error: 44.768144189643245
Root relative squared error: 88.73754618942255
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5756825884233954
Weighted FMeasure: 0.6812200903456374
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6019514656201793
Mean absolute error: 0.16788054071116218
Coverage of cases: 75.41371158392435
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 219.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.1081611624111591
Kappa statistic: 0.5738051168761381
Training time: 20.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 30.08274231678487
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6822567563779405
Weighted AreaUnderROC: 0.7863730653052822
Root mean squared error: 0.38764643538689164
Relative absolute error: 45.359548189915586
Root relative squared error: 89.52310952840831
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5697635722635567
Weighted FMeasure: 0.6766420662802205
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5950083763462384
Mean absolute error: 0.17009830571218343
Coverage of cases: 73.5224586288416
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 240.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10970416315233485
Kappa statistic: 0.5674588365253996
Training time: 20.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 32.91962174940898
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6929629528566276
Weighted AreaUnderROC: 0.7995822425520155
Root mean squared error: 0.3792192252493286
Relative absolute error: 43.22712326327959
Root relative squared error: 87.57692871183248
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5905896981050103
Weighted FMeasure: 0.690435198927485
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6105843506214533
Mean absolute error: 0.16210171223729844
Coverage of cases: 75.88652482269504
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 261.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10387156775677413
Kappa statistic: 0.59288080458398
Training time: 21.0
		
Time end:Sun Oct 08 09.54.47 EEST 2017