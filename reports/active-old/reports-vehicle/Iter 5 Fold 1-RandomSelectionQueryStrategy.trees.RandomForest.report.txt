Sun Oct 08 09.56.04 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.04 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.01182033096927
Incorrectly Classified Instances: 40.42553191489362
Correctly Classified Instances: 59.57446808510638
Weighted Precision: 0.6052009358228202
Weighted AreaUnderROC: 0.8131632202511784
Root mean squared error: 0.368964431582571
Relative absolute error: 65.65799842395586
Root relative squared error: 85.20868555823786
Weighted TruePositiveRate: 0.5957446808510638
Weighted MatthewsCorrelation: 0.4640148417261459
Weighted FMeasure: 0.5992369394407553
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6093464514570252
Mean absolute error: 0.24621749408983448
Coverage of cases: 92.19858156028369
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 19.0
Weighted Recall: 0.5957446808510638
Weighted FalsePositiveRate: 0.13618866182842668
Kappa statistic: 0.46052356801909305
Training time: 19.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 60.520094562647756
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.629760458822844
Weighted AreaUnderROC: 0.8549504380918884
Root mean squared error: 0.34444253793417307
Relative absolute error: 57.49408983451528
Root relative squared error: 79.54559679866108
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.5091379414014765
Weighted FMeasure: 0.6257193735904939
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6796846374265644
Mean absolute error: 0.2156028368794323
Coverage of cases: 94.08983451536643
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.122079794939363
Kappa statistic: 0.5116783102200111
Training time: 10.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 62.825059101654844
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6923277032400397
Weighted AreaUnderROC: 0.8641907290584245
Root mean squared error: 0.331252369633346
Relative absolute error: 55.85500394011026
Root relative squared error: 76.49945791100549
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6021792150785544
Weighted FMeasure: 0.6959979312521993
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6972505529818853
Mean absolute error: 0.20945626477541346
Coverage of cases: 94.79905437352245
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 43.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.0979220917104243
Kappa statistic: 0.609234964128467
Training time: 12.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 64.53900709219859
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7016029641376542
Weighted AreaUnderROC: 0.8885980938005187
Root mean squared error: 0.3186666963411365
Relative absolute error: 55.09850275807715
Root relative squared error: 73.59292116572956
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6039830300108704
Weighted FMeasure: 0.701260216052628
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7369415701511443
Mean absolute error: 0.2066193853427893
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09923624785853052
Kappa statistic: 0.6058634107070978
Training time: 15.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 60.047281323877066
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7223300555180493
Weighted AreaUnderROC: 0.8912716885424902
Root mean squared error: 0.31508563885216134
Relative absolute error: 52.19858156028368
Root relative squared error: 72.76591136363223
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6281957785803476
Weighted FMeasure: 0.7205845817451789
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7461539785256229
Mean absolute error: 0.19574468085106378
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.0935152971599456
Kappa statistic: 0.6278712601859376
Training time: 17.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 63.593380614657214
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6932145023301595
Weighted AreaUnderROC: 0.8838994950921663
Root mean squared error: 0.32033153629358463
Relative absolute error: 54.814814814814724
Root relative squared error: 73.977399483611
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5939250763597675
Weighted FMeasure: 0.6951765004121974
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7248570361342949
Mean absolute error: 0.2055555555555552
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 98.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10164069600955412
Kappa statistic: 0.5963740458015268
Training time: 20.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 57.565011820330966
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6940941756171319
Weighted AreaUnderROC: 0.8836269319757606
Root mean squared error: 0.32121592275404554
Relative absolute error: 52.482269503546036
Root relative squared error: 74.1816397880169
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5943974660925532
Weighted FMeasure: 0.6954496262731323
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7250732414028158
Mean absolute error: 0.19680851063829763
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 121.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10165248579706129
Kappa statistic: 0.5963048292213747
Training time: 22.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 57.565011820330966
Incorrectly Classified Instances: 25.29550827423168
Correctly Classified Instances: 74.70449172576832
Weighted Precision: 0.7431181179324738
Weighted AreaUnderROC: 0.8995542007683903
Root mean squared error: 0.30352538538723267
Relative absolute error: 48.542159180457
Root relative squared error: 70.09618518368147
Weighted TruePositiveRate: 0.7470449172576832
Weighted MatthewsCorrelation: 0.660466615811307
Weighted FMeasure: 0.7441218634289618
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7708990313964414
Mean absolute error: 0.18203309692671377
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 146.0
Weighted Recall: 0.7470449172576832
Weighted FalsePositiveRate: 0.08504716304725632
Kappa statistic: 0.6624832214765101
Training time: 24.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 58.983451536643024
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6921028605536691
Weighted AreaUnderROC: 0.8878009061228809
Root mean squared error: 0.3138828740610765
Relative absolute error: 51.82033096926708
Root relative squared error: 72.48814473327037
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5955218192359734
Weighted FMeasure: 0.6953084849983051
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7399214697191943
Mean absolute error: 0.19432624113475155
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 185.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10090035635633056
Kappa statistic: 0.5995005032243635
Training time: 38.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 58.03782505910166
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7214501173485306
Weighted AreaUnderROC: 0.8949153941961319
Root mean squared error: 0.30984629759348054
Relative absolute error: 50.08668242710791
Root relative squared error: 71.55593732920195
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6316683371521391
Weighted FMeasure: 0.7194950236644763
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7507968222301685
Mean absolute error: 0.18782505910165465
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 214.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09207707830897419
Kappa statistic: 0.6341894360159542
Training time: 28.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 58.03782505910166
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7250578251689267
Weighted AreaUnderROC: 0.8957318526428488
Root mean squared error: 0.30693327910974255
Relative absolute error: 48.95193065405828
Root relative squared error: 70.88320452690576
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6374927345508539
Weighted FMeasure: 0.727239493421855
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7616950304448271
Mean absolute error: 0.18356973995271855
Coverage of cases: 97.87234042553192
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 247.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09037439995299125
Kappa statistic: 0.6406572525056821
Training time: 33.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 55.673758865248224
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.7004252281715448
Weighted AreaUnderROC: 0.9036810435581872
Root mean squared error: 0.30149352423172265
Relative absolute error: 47.02915681639079
Root relative squared error: 69.62694694964561
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.6011469001876585
Weighted FMeasure: 0.7012081261264801
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7722004245869064
Mean absolute error: 0.17635933806146545
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 281.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.10022517527169071
Kappa statistic: 0.6026895867965739
Training time: 33.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 55.673758865248224
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7016134433569945
Weighted AreaUnderROC: 0.8988839679116827
Root mean squared error: 0.30476902853882837
Relative absolute error: 48.79432624113472
Root relative squared error: 70.38339226702132
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6080641360717589
Weighted FMeasure: 0.7038053762906347
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7572148298972364
Mean absolute error: 0.1829787234042552
Coverage of cases: 98.3451536643026
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 317.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09785273731056028
Kappa statistic: 0.6121377932504866
Training time: 36.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 56.678486997635936
Incorrectly Classified Instances: 26.24113475177305
Correctly Classified Instances: 73.75886524822695
Weighted Precision: 0.7302317915811862
Weighted AreaUnderROC: 0.9076950507475292
Root mean squared error: 0.2941108329067409
Relative absolute error: 47.31284475965319
Root relative squared error: 67.92198742011676
Weighted TruePositiveRate: 0.7375886524822695
Weighted MatthewsCorrelation: 0.6461125982634828
Weighted FMeasure: 0.7316501058795895
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7795462155346515
Mean absolute error: 0.17742316784869946
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 356.0
Weighted Recall: 0.7375886524822695
Weighted FalsePositiveRate: 0.0880757425355983
Kappa statistic: 0.6500771346166745
Training time: 38.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 55.55555555555556
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.7246947160524919
Weighted AreaUnderROC: 0.9013547304458428
Root mean squared error: 0.2992109402432158
Relative absolute error: 46.68242710795895
Root relative squared error: 69.09980675756069
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6394478074012028
Weighted FMeasure: 0.7269259486354636
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7741667142607878
Mean absolute error: 0.1750591016548461
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 398.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08965000738431099
Kappa statistic: 0.6437775277047018
Training time: 41.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.56028368794326
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.7259211925920724
Weighted AreaUnderROC: 0.902365929932925
Root mean squared error: 0.2988156290120112
Relative absolute error: 47.69109535066975
Root relative squared error: 69.00851353926082
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.642079951836151
Weighted FMeasure: 0.7298707863013136
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7705437102951034
Mean absolute error: 0.17884160756501158
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 443.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.0889495344550346
Kappa statistic: 0.646890465684813
Training time: 44.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 54.787234042553195
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.7289211105467833
Weighted AreaUnderROC: 0.906879819903655
Root mean squared error: 0.29627314724385306
Relative absolute error: 46.39873916469657
Root relative squared error: 68.42135252595848
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.6435413271462094
Weighted FMeasure: 0.7310361544069723
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7744395996889031
Mean absolute error: 0.17399527186761216
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 487.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.08857287325221724
Kappa statistic: 0.647029898451062
Training time: 43.0
		
Time end:Sun Oct 08 09.56.05 EEST 2017