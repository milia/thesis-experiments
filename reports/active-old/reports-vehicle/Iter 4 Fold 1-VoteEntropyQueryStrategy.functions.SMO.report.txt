Sun Oct 08 09.56.10 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.10 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5506795848403413
Weighted AreaUnderROC: 0.7668404582872281
Root mean squared error: 0.3875356394127866
Relative absolute error: 81.11373785132638
Root relative squared error: 89.49752229421844
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.4080214756141045
Weighted FMeasure: 0.5536272619319976
Iteration time: 49.0
Weighted AreaUnderPRC: 0.495793595733729
Mean absolute error: 0.3041765169424739
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14755761934051104
Kappa statistic: 0.4139572300060334
Training time: 48.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.7694600891184568
Weighted AreaUnderROC: 0.8477132092446218
Root mean squared error: 0.36967033922916354
Relative absolute error: 77.27869713685304
Root relative squared error: 85.37170794615113
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5562185222485996
Weighted FMeasure: 0.5914291194113517
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6068800994887352
Mean absolute error: 0.28979511426319887
Coverage of cases: 95.74468085106383
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10835182403671521
Kappa statistic: 0.5658247054623349
Training time: 64.0
		
Time end:Sun Oct 08 09.56.10 EEST 2017