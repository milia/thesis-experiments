Sun Oct 08 09.52.53 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.53 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.997635933806144
Incorrectly Classified Instances: 39.479905437352244
Correctly Classified Instances: 60.520094562647756
Weighted Precision: 0.6117583328143608
Weighted AreaUnderROC: 0.7405202394927977
Root mean squared error: 0.43073630961385
Relative absolute error: 55.41384402460739
Root relative squared error: 99.47428972212091
Weighted TruePositiveRate: 0.6052009456264775
Weighted MatthewsCorrelation: 0.475124416943004
Weighted FMeasure: 0.6045229095265531
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5085194783522042
Mean absolute error: 0.20780191509227772
Coverage of cases: 70.2127659574468
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6052009456264775
Weighted FalsePositiveRate: 0.1326854549236279
Kappa statistic: 0.4737826180880939
Training time: 10.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 34.39716312056738
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.7104728769763907
Weighted AreaUnderROC: 0.8305984441101575
Root mean squared error: 0.36223194709854206
Relative absolute error: 43.02973516643386
Root relative squared error: 83.65388486657022
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.6034557092373074
Weighted FMeasure: 0.6908589116499867
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6239453647071456
Mean absolute error: 0.16136150687412698
Coverage of cases: 82.26950354609929
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10094079749572522
Kappa statistic: 0.59948258765815
Training time: 38.0
		
Time end:Sun Oct 08 09.52.53 EEST 2017