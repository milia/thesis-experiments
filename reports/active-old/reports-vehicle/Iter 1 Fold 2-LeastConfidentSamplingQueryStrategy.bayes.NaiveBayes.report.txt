Sun Oct 08 09.52.57 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.57 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.05200945626478
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.47875590026832754
Weighted AreaUnderROC: 0.7444669751480923
Root mean squared error: 0.4757697615278306
Relative absolute error: 68.72621853739874
Root relative squared error: 109.87431995615083
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.32095752210311357
Weighted FMeasure: 0.44447734813411377
Iteration time: 11.0
Weighted AreaUnderPRC: 0.4887803526675735
Mean absolute error: 0.2577233195152453
Coverage of cases: 64.06619385342789
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 11.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16554938504622643
Kappa statistic: 0.32815283220520125
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 36.702127659574465
Incorrectly Classified Instances: 50.8274231678487
Correctly Classified Instances: 49.1725768321513
Weighted Precision: 0.5251511474849949
Weighted AreaUnderROC: 0.7580020460660816
Root mean squared error: 0.479083340259854
Relative absolute error: 69.92182231892461
Root relative squared error: 110.63955818531672
Weighted TruePositiveRate: 0.491725768321513
Weighted MatthewsCorrelation: 0.3376233948521639
Weighted FMeasure: 0.44349194428776134
Iteration time: 10.0
Weighted AreaUnderPRC: 0.50594573385119
Mean absolute error: 0.2622068336959673
Coverage of cases: 65.95744680851064
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 21.0
Weighted Recall: 0.491725768321513
Weighted FalsePositiveRate: 0.1647291798854073
Kappa statistic: 0.3261736115227313
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 37.174940898345156
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.5322107781431936
Weighted AreaUnderROC: 0.7505681342217764
Root mean squared error: 0.47390475448529384
Relative absolute error: 70.45533096035695
Root relative squared error: 109.44361502893118
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.3266763655186488
Weighted FMeasure: 0.4454279269085967
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4885616851848871
Mean absolute error: 0.26420749110133857
Coverage of cases: 67.13947990543736
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 31.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.16901226297488092
Kappa statistic: 0.3078409595735229
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 36.347517730496456
Incorrectly Classified Instances: 50.8274231678487
Correctly Classified Instances: 49.1725768321513
Weighted Precision: 0.5321403913294138
Weighted AreaUnderROC: 0.7541730812581545
Root mean squared error: 0.47199980708413564
Relative absolute error: 69.30222738530155
Root relative squared error: 109.00368627099085
Weighted TruePositiveRate: 0.491725768321513
Weighted MatthewsCorrelation: 0.34278224331314433
Weighted FMeasure: 0.4593084217015932
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4975462368263833
Mean absolute error: 0.2598833526948808
Coverage of cases: 66.19385342789599
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 41.0
Weighted Recall: 0.491725768321513
Weighted FalsePositiveRate: 0.16393631933881786
Kappa statistic: 0.32675224304876965
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 35.520094562647756
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.5186786324601376
Weighted AreaUnderROC: 0.746052430148953
Root mean squared error: 0.4802015657914062
Relative absolute error: 70.16452477225377
Root relative squared error: 110.89780130997927
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.3228276850196452
Weighted FMeasure: 0.449961020482079
Iteration time: 10.0
Weighted AreaUnderPRC: 0.48077629599571076
Mean absolute error: 0.2631169678959516
Coverage of cases: 65.48463356973996
Instances selection time: 9.0
Test time: 18.0
Accumulative iteration time: 51.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16909381653193098
Kappa statistic: 0.3054289940828403
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 36.5839243498818
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5296428343236854
Weighted AreaUnderROC: 0.7506171833193407
Root mean squared error: 0.46879662370568476
Relative absolute error: 68.2372960885011
Root relative squared error: 108.26394275666593
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.35109021637143234
Weighted FMeasure: 0.46763355294065845
Iteration time: 9.0
Weighted AreaUnderPRC: 0.48909301007898465
Mean absolute error: 0.2558898603318791
Coverage of cases: 66.43026004728132
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 60.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.16104690510188593
Kappa statistic: 0.33921419105507467
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 36.22931442080378
Incorrectly Classified Instances: 50.8274231678487
Correctly Classified Instances: 49.1725768321513
Weighted Precision: 0.5313385160451118
Weighted AreaUnderROC: 0.7518386414751372
Root mean squared error: 0.46778131089928593
Relative absolute error: 67.91652193664994
Root relative squared error: 108.02946630783151
Weighted TruePositiveRate: 0.491725768321513
Weighted MatthewsCorrelation: 0.3434000671341787
Weighted FMeasure: 0.46976806344445177
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4925283464120026
Mean absolute error: 0.2546869572624373
Coverage of cases: 67.61229314420804
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 70.0
Weighted Recall: 0.491725768321513
Weighted FalsePositiveRate: 0.1639534354522992
Kappa statistic: 0.3269664833822996
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 36.702127659574465
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5329176639583146
Weighted AreaUnderROC: 0.757407339850746
Root mean squared error: 0.46130333484294034
Relative absolute error: 66.6560969660297
Root relative squared error: 106.53344181985747
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.35237435435864845
Weighted FMeasure: 0.47928102059596317
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5015852142249079
Mean absolute error: 0.24996036362261134
Coverage of cases: 69.26713947990544
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 78.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.16128287181129328
Kappa statistic: 0.33911144020733053
Training time: 1.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 37.5886524822695
Incorrectly Classified Instances: 49.645390070921984
Correctly Classified Instances: 50.354609929078016
Weighted Precision: 0.5271036163625584
Weighted AreaUnderROC: 0.7571701689086655
Root mean squared error: 0.4562158234057148
Relative absolute error: 66.28754970822801
Root relative squared error: 105.35853138074249
Weighted TruePositiveRate: 0.5035460992907801
Weighted MatthewsCorrelation: 0.35187731762855345
Weighted FMeasure: 0.47940764835218347
Iteration time: 8.0
Weighted AreaUnderPRC: 0.49966401460757587
Mean absolute error: 0.24857831140585504
Coverage of cases: 70.92198581560284
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 86.0
Weighted Recall: 0.5035460992907801
Weighted FalsePositiveRate: 0.16083964733459444
Kappa statistic: 0.34202924314474903
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 38.593380614657214
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.5391307747043915
Weighted AreaUnderROC: 0.7636519682788376
Root mean squared error: 0.4484567891367719
Relative absolute error: 65.42362372823455
Root relative squared error: 103.56665917121221
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.36759913221747886
Weighted FMeasure: 0.49529052904295495
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5139766573809952
Mean absolute error: 0.2453385889808795
Coverage of cases: 72.81323877068557
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 99.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.15713746180851726
Kappa statistic: 0.35752865430351705
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 38.593380614657214
Incorrectly Classified Instances: 49.881796690307326
Correctly Classified Instances: 50.118203309692674
Weighted Precision: 0.5222190824382449
Weighted AreaUnderROC: 0.7593856591213188
Root mean squared error: 0.4504183634854472
Relative absolute error: 65.68960674590706
Root relative squared error: 104.01966536250947
Weighted TruePositiveRate: 0.5011820330969267
Weighted MatthewsCorrelation: 0.3472083121723301
Weighted FMeasure: 0.4823174100651968
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5044000170533328
Mean absolute error: 0.24633602529715146
Coverage of cases: 72.57683215130024
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 105.0
Weighted Recall: 0.5011820330969267
Weighted FalsePositiveRate: 0.16196441560535021
Kappa statistic: 0.3387931992443604
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 39.0661938534279
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.5350443011797668
Weighted AreaUnderROC: 0.7624209923746667
Root mean squared error: 0.44810338787832554
Relative absolute error: 65.32523147476941
Root relative squared error: 103.48504464653382
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.3658137753022489
Weighted FMeasure: 0.49562949252713273
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5137612516134215
Mean absolute error: 0.2449696180303853
Coverage of cases: 72.81323877068557
Instances selection time: 4.0
Test time: 18.0
Accumulative iteration time: 110.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.1572436313240246
Kappa statistic: 0.35751913402336827
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 40.839243498817964
Incorrectly Classified Instances: 50.354609929078016
Correctly Classified Instances: 49.645390070921984
Weighted Precision: 0.5203581259176647
Weighted AreaUnderROC: 0.7555513638171941
Root mean squared error: 0.44783396958499494
Relative absolute error: 66.31817681992679
Root relative squared error: 103.4228251568622
Weighted TruePositiveRate: 0.49645390070921985
Weighted MatthewsCorrelation: 0.3416860820814389
Weighted FMeasure: 0.4819956888790927
Iteration time: 5.0
Weighted AreaUnderPRC: 0.4978121038992529
Mean absolute error: 0.2486931630747255
Coverage of cases: 74.94089834515367
Instances selection time: 3.0
Test time: 18.0
Accumulative iteration time: 115.0
Weighted Recall: 0.49645390070921985
Weighted FalsePositiveRate: 0.1636738643486414
Kappa statistic: 0.33225871000733703
Training time: 2.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 39.8936170212766
Incorrectly Classified Instances: 51.30023640661938
Correctly Classified Instances: 48.69976359338062
Weighted Precision: 0.5231892021604677
Weighted AreaUnderROC: 0.7679953618153191
Root mean squared error: 0.4602245640014939
Relative absolute error: 68.20794048674519
Root relative squared error: 106.28431036557627
Weighted TruePositiveRate: 0.48699763593380613
Weighted MatthewsCorrelation: 0.33516777207131704
Weighted FMeasure: 0.4654192849931047
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5171208856558395
Mean absolute error: 0.25577977682529446
Coverage of cases: 72.81323877068557
Instances selection time: 3.0
Test time: 17.0
Accumulative iteration time: 119.0
Weighted Recall: 0.48699763593380613
Weighted FalsePositiveRate: 0.16574679036059123
Kappa statistic: 0.32063532006542667
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 41.13475177304964
Incorrectly Classified Instances: 51.06382978723404
Correctly Classified Instances: 48.93617021276596
Weighted Precision: 0.5251543270793524
Weighted AreaUnderROC: 0.7661866050731475
Root mean squared error: 0.45454009692841624
Relative absolute error: 68.1274188734816
Root relative squared error: 104.97153892763988
Weighted TruePositiveRate: 0.48936170212765956
Weighted MatthewsCorrelation: 0.337923569705694
Weighted FMeasure: 0.4706480408791373
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5165162268464711
Mean absolute error: 0.25547782077555603
Coverage of cases: 74.23167848699764
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 123.0
Weighted Recall: 0.48936170212765956
Weighted FalsePositiveRate: 0.16515286966737205
Kappa statistic: 0.3235507514622048
Training time: 2.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 41.96217494089834
Incorrectly Classified Instances: 51.06382978723404
Correctly Classified Instances: 48.93617021276596
Weighted Precision: 0.5440937564735224
Weighted AreaUnderROC: 0.790113493355442
Root mean squared error: 0.45775455090836337
Relative absolute error: 69.66947328577218
Root relative squared error: 105.71388527588795
Weighted TruePositiveRate: 0.48936170212765956
Weighted MatthewsCorrelation: 0.3449053656743628
Weighted FMeasure: 0.46582433065510653
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5467998069690576
Mean absolute error: 0.2612605248216457
Coverage of cases: 74.23167848699764
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 127.0
Weighted Recall: 0.48936170212765956
Weighted FalsePositiveRate: 0.16466003722593708
Kappa statistic: 0.32373600183558215
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 43.794326241134755
Incorrectly Classified Instances: 54.60992907801418
Correctly Classified Instances: 45.39007092198582
Weighted Precision: 0.4959155103432037
Weighted AreaUnderROC: 0.7678077390221807
Root mean squared error: 0.45839808689262923
Relative absolute error: 71.37292437002499
Root relative squared error: 105.86250354538758
Weighted TruePositiveRate: 0.45390070921985815
Weighted MatthewsCorrelation: 0.2926672286101307
Weighted FMeasure: 0.4287928074371781
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5047490873431605
Mean absolute error: 0.2676484663875937
Coverage of cases: 74.23167848699764
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 130.0
Weighted Recall: 0.45390070921985815
Weighted FalsePositiveRate: 0.17634333241060435
Kappa statistic: 0.2770621703006044
Training time: 2.0
		
Time end:Sun Oct 08 09.52.58 EEST 2017