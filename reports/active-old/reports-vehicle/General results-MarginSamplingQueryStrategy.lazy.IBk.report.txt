Sun Oct 08 09.54.29 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.54.29 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.276595744680854
Correctly Classified Instances: 58.723404255319146
Weighted Precision: 0.5848598374306027
Weighted AreaUnderROC: 0.7241610305352424
Root mean squared error: 0.44418358872915337
Relative absolute error: 57.07930367504834
Root relative squared error: 102.57980580895631
Weighted TruePositiveRate: 0.5872340425531914
Weighted MatthewsCorrelation: 0.44752056755638125
Weighted FMeasure: 0.5787799526018512
Iteration time: 11.9
Weighted AreaUnderPRC: 0.4727842896550797
Mean absolute error: 0.21404738878143123
Coverage of cases: 58.723404255319146
Instances selection time: 11.8
Test time: 13.9
Accumulative iteration time: 11.9
Weighted Recall: 0.5872340425531914
Weighted FalsePositiveRate: 0.1389119814827066
Kappa statistic: 0.44912885589278184
Training time: 0.1
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.9692671394799
Correctly Classified Instances: 59.0307328605201
Weighted Precision: 0.5887188395447713
Weighted AreaUnderROC: 0.7262527057405659
Root mean squared error: 0.44435585250632936
Relative absolute error: 56.30621953710977
Root relative squared error: 102.61958842420594
Weighted TruePositiveRate: 0.590307328605201
Weighted MatthewsCorrelation: 0.4521730977877515
Weighted FMeasure: 0.5817518097706091
Iteration time: 11.9
Weighted AreaUnderPRC: 0.4761347396566726
Mean absolute error: 0.2111483232641617
Coverage of cases: 59.0307328605201
Instances selection time: 11.6
Test time: 16.1
Accumulative iteration time: 23.8
Weighted Recall: 0.590307328605201
Weighted FalsePositiveRate: 0.13780191712406903
Kappa statistic: 0.45328419866352193
Training time: 0.3
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.73286052009456
Correctly Classified Instances: 59.26713947990544
Weighted Precision: 0.5962669301977966
Weighted AreaUnderROC: 0.7278903713080181
Root mean squared error: 0.4442487244072826
Relative absolute error: 55.73827817178881
Root relative squared error: 102.594848249477
Weighted TruePositiveRate: 0.5926713947990544
Weighted MatthewsCorrelation: 0.4578807742536079
Weighted FMeasure: 0.582403047182806
Iteration time: 12.9
Weighted AreaUnderPRC: 0.47842902167530504
Mean absolute error: 0.20901854314420804
Coverage of cases: 59.26713947990544
Instances selection time: 12.9
Test time: 18.6
Accumulative iteration time: 36.7
Weighted Recall: 0.5926713947990544
Weighted FalsePositiveRate: 0.13689065218301805
Kappa statistic: 0.45648577310424177
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.48936170212766
Correctly Classified Instances: 58.51063829787235
Weighted Precision: 0.5958325904196231
Weighted AreaUnderROC: 0.7228340930712562
Root mean squared error: 0.44923052086681486
Relative absolute error: 56.52673950546309
Root relative squared error: 103.74534486026054
Weighted TruePositiveRate: 0.5851063829787234
Weighted MatthewsCorrelation: 0.4512850775560426
Weighted FMeasure: 0.572968675841394
Iteration time: 13.5
Weighted AreaUnderPRC: 0.47241446015472854
Mean absolute error: 0.21197527314548661
Coverage of cases: 58.51063829787235
Instances selection time: 13.5
Test time: 20.7
Accumulative iteration time: 50.2
Weighted Recall: 0.5851063829787234
Weighted FalsePositiveRate: 0.139438196836211
Kappa statistic: 0.44637215169192396
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.79669030732861
Correctly Classified Instances: 58.2033096926714
Weighted Precision: 0.5993668404116774
Weighted AreaUnderROC: 0.7207779417807854
Root mean squared error: 0.45155233877068246
Relative absolute error: 56.78299373334829
Root relative squared error: 104.28154573698346
Weighted TruePositiveRate: 0.582033096926714
Weighted MatthewsCorrelation: 0.4503137537424406
Weighted FMeasure: 0.5685121724541944
Iteration time: 14.2
Weighted AreaUnderPRC: 0.47096491806413976
Mean absolute error: 0.21293622650005614
Coverage of cases: 58.2033096926714
Instances selection time: 14.2
Test time: 23.0
Accumulative iteration time: 64.4
Weighted Recall: 0.582033096926714
Weighted FalsePositiveRate: 0.1404772133651432
Kappa statistic: 0.4422389345504164
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.40189125295508
Correctly Classified Instances: 59.59810874704492
Weighted Precision: 0.6110241187132461
Weighted AreaUnderROC: 0.7303016932865118
Root mean squared error: 0.4444343612521381
Relative absolute error: 54.850694968395395
Root relative squared error: 102.63771924241652
Weighted TruePositiveRate: 0.5959810874704491
Weighted MatthewsCorrelation: 0.4677541349803082
Weighted FMeasure: 0.5810749157555841
Iteration time: 14.4
Weighted AreaUnderPRC: 0.48328793791813807
Mean absolute error: 0.20569010613148278
Coverage of cases: 59.59810874704492
Instances selection time: 14.4
Test time: 25.3
Accumulative iteration time: 78.8
Weighted Recall: 0.5959810874704491
Weighted FalsePositiveRate: 0.1353777008974256
Kappa statistic: 0.46118355353829654
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.054373522458626
Correctly Classified Instances: 60.94562647754136
Weighted Precision: 0.6204281984404422
Weighted AreaUnderROC: 0.7395068440697912
Root mean squared error: 0.43752554790080433
Relative absolute error: 52.99418076013836
Root relative squared error: 101.04219714314716
Weighted TruePositiveRate: 0.6094562647754137
Weighted MatthewsCorrelation: 0.48323586415024994
Weighted FMeasure: 0.5912966416355887
Iteration time: 14.5
Weighted AreaUnderPRC: 0.49749125426117236
Mean absolute error: 0.1987281778505189
Coverage of cases: 60.94562647754136
Instances selection time: 14.5
Test time: 25.9
Accumulative iteration time: 93.3
Weighted Recall: 0.6094562647754137
Weighted FalsePositiveRate: 0.1304425766358315
Kappa statistic: 0.47951011575498226
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.53427895981087
Correctly Classified Instances: 61.46572104018911
Weighted Precision: 0.6242041403133493
Weighted AreaUnderROC: 0.7431293687551731
Root mean squared error: 0.43493255727418545
Relative absolute error: 52.23203793565889
Root relative squared error: 100.44337160863333
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.4891847090673444
Weighted FMeasure: 0.5926044233842042
Iteration time: 13.5
Weighted AreaUnderPRC: 0.5029260968246
Mean absolute error: 0.1958701422587208
Coverage of cases: 61.46572104018911
Instances selection time: 13.5
Test time: 27.9
Accumulative iteration time: 106.8
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12839847289154518
Kappa statistic: 0.4866646708289223
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.46335697399527
Correctly Classified Instances: 61.53664302600473
Weighted Precision: 0.6267549841880637
Weighted AreaUnderROC: 0.7436804034637295
Root mean squared error: 0.4348029332834181
Relative absolute error: 52.070210223950916
Root relative squared error: 100.41343623024815
Weighted TruePositiveRate: 0.6153664302600473
Weighted MatthewsCorrelation: 0.49077461705207837
Weighted FMeasure: 0.5916922438382726
Iteration time: 14.2
Weighted AreaUnderPRC: 0.505661199806194
Mean absolute error: 0.19526328833981593
Coverage of cases: 61.53664302600473
Instances selection time: 14.2
Test time: 29.9
Accumulative iteration time: 121.0
Weighted Recall: 0.6153664302600473
Weighted FalsePositiveRate: 0.12800562333258844
Kappa statistic: 0.4877274116412562
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 37.328605200945624
Correctly Classified Instances: 62.67139479905436
Weighted Precision: 0.6321781792997734
Weighted AreaUnderROC: 0.7512294583517719
Root mean squared error: 0.42870655584815953
Relative absolute error: 50.521153099749505
Root relative squared error: 99.0055381689169
Weighted TruePositiveRate: 0.6267139479905438
Weighted MatthewsCorrelation: 0.5030276079784337
Weighted FMeasure: 0.6031792519756043
Iteration time: 12.2
Weighted AreaUnderPRC: 0.5166636995360827
Mean absolute error: 0.18945432412406066
Coverage of cases: 62.67139479905436
Instances selection time: 12.1
Test time: 31.4
Accumulative iteration time: 133.2
Weighted Recall: 0.6267139479905438
Weighted FalsePositiveRate: 0.12425503128700004
Kappa statistic: 0.5028252613156011
Training time: 0.1
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.05200945626477
Correctly Classified Instances: 63.947990543735216
Weighted Precision: 0.6401668963371556
Weighted AreaUnderROC: 0.7596790045304844
Root mean squared error: 0.42147310018705986
Relative absolute error: 48.79060502582937
Root relative squared error: 97.33504313967407
Weighted TruePositiveRate: 0.6394799054373523
Weighted MatthewsCorrelation: 0.5174687449851552
Weighted FMeasure: 0.6154770664495142
Iteration time: 11.1
Weighted AreaUnderPRC: 0.5294298838056999
Mean absolute error: 0.18296476884686014
Coverage of cases: 63.947990543735216
Instances selection time: 11.0
Test time: 33.4
Accumulative iteration time: 144.3
Weighted Recall: 0.6394799054373523
Weighted FalsePositiveRate: 0.12012189637638312
Kappa statistic: 0.5197820923089891
Training time: 0.1
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.05910165484634
Correctly Classified Instances: 64.94089834515367
Weighted Precision: 0.6481526893850116
Weighted AreaUnderROC: 0.7662902693459054
Root mean squared error: 0.4158917980236822
Relative absolute error: 47.43708616049041
Root relative squared error: 96.04609661709218
Weighted TruePositiveRate: 0.6494089834515366
Weighted MatthewsCorrelation: 0.5296362925122188
Weighted FMeasure: 0.6252972560996176
Iteration time: 10.9
Weighted AreaUnderPRC: 0.5391691546456847
Mean absolute error: 0.17788907310183905
Coverage of cases: 64.94089834515367
Instances selection time: 10.5
Test time: 35.1
Accumulative iteration time: 155.2
Weighted Recall: 0.6494089834515366
Weighted FalsePositiveRate: 0.11682844475972601
Kappa statistic: 0.533000928831216
Training time: 0.4
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.77541371158392
Correctly Classified Instances: 65.22458628841609
Weighted Precision: 0.6514633186462581
Weighted AreaUnderROC: 0.7681123178717159
Root mean squared error: 0.41438397649847153
Relative absolute error: 47.02127659574474
Root relative squared error: 95.69788015170404
Weighted TruePositiveRate: 0.6522458628841609
Weighted MatthewsCorrelation: 0.5333440420845201
Weighted FMeasure: 0.6277979223888726
Iteration time: 8.9
Weighted AreaUnderPRC: 0.542347072989451
Mean absolute error: 0.17632978723404275
Coverage of cases: 65.22458628841609
Instances selection time: 8.9
Test time: 37.1
Accumulative iteration time: 164.1
Weighted Recall: 0.6522458628841609
Weighted FalsePositiveRate: 0.11602122714072886
Kappa statistic: 0.5367275274050919
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.39716312056738
Correctly Classified Instances: 65.60283687943262
Weighted Precision: 0.6521545125126568
Weighted AreaUnderROC: 0.7704897068104619
Root mean squared error: 0.41224915837537024
Relative absolute error: 46.48514986005823
Root relative squared error: 95.20486502448668
Weighted TruePositiveRate: 0.6560283687943264
Weighted MatthewsCorrelation: 0.5380410745731217
Weighted FMeasure: 0.6386138797709462
Iteration time: 7.7
Weighted AreaUnderPRC: 0.547646940258866
Mean absolute error: 0.17431931197521838
Coverage of cases: 65.60283687943262
Instances selection time: 7.7
Test time: 38.4
Accumulative iteration time: 171.8
Weighted Recall: 0.6560283687943264
Weighted FalsePositiveRate: 0.11504895517340268
Kappa statistic: 0.5415974030693549
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.64066193853428
Correctly Classified Instances: 66.35933806146572
Weighted Precision: 0.6593045695304183
Weighted AreaUnderROC: 0.7754106513165734
Root mean squared error: 0.40783080536944166
Relative absolute error: 45.453626614588856
Root relative squared error: 94.18449010554762
Weighted TruePositiveRate: 0.6635933806146572
Weighted MatthewsCorrelation: 0.5485774387115157
Weighted FMeasure: 0.6555416934348985
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5553291696900765
Mean absolute error: 0.17045109980470824
Coverage of cases: 66.35933806146572
Instances selection time: 6.0
Test time: 40.3
Accumulative iteration time: 177.8
Weighted Recall: 0.6635933806146572
Weighted FalsePositiveRate: 0.11277207798151016
Kappa statistic: 0.5514839808098495
Training time: 0.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.356973995271865
Correctly Classified Instances: 66.64302600472811
Weighted Precision: 0.6610948633988265
Weighted AreaUnderROC: 0.7771840785301298
Root mean squared error: 0.406153363219867
Relative absolute error: 45.048378055616595
Root relative squared error: 93.79710143490482
Weighted TruePositiveRate: 0.6664302600472812
Weighted MatthewsCorrelation: 0.5519494178984555
Weighted FMeasure: 0.6614710670904322
Iteration time: 5.2
Weighted AreaUnderPRC: 0.5606063838042831
Mean absolute error: 0.16893141770856224
Coverage of cases: 66.64302600472811
Instances selection time: 5.1
Test time: 42.5
Accumulative iteration time: 183.0
Weighted Recall: 0.6664302600472812
Weighted FalsePositiveRate: 0.1120621029870215
Kappa statistic: 0.5551254845975545
Training time: 0.1
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.3096926713948
Correctly Classified Instances: 66.6903073286052
Weighted Precision: 0.6628093524606613
Weighted AreaUnderROC: 0.7774226686274102
Root mean squared error: 0.405983935623384
Relative absolute error: 44.95789489948833
Root relative squared error: 93.7579738075298
Weighted TruePositiveRate: 0.6669030732860521
Weighted MatthewsCorrelation: 0.5530854497329359
Weighted FMeasure: 0.6637790958766743
Iteration time: 2.9
Weighted AreaUnderPRC: 0.562772316325257
Mean absolute error: 0.16859210587308127
Coverage of cases: 66.6903073286052
Instances selection time: 2.9
Test time: 43.2
Accumulative iteration time: 185.9
Weighted Recall: 0.6669030732860521
Weighted FalsePositiveRate: 0.11205773603123166
Kappa statistic: 0.5556644931753782
Training time: 0.0
		
Time end:Sun Oct 08 09.54.43 EEST 2017