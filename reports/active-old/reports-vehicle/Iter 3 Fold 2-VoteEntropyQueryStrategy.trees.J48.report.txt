Sun Oct 08 09.56.13 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.13 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.997635933806144
Incorrectly Classified Instances: 39.479905437352244
Correctly Classified Instances: 60.520094562647756
Weighted Precision: 0.6117583328143608
Weighted AreaUnderROC: 0.7405202394927977
Root mean squared error: 0.43073630961385
Relative absolute error: 55.41384402460739
Root relative squared error: 99.47428972212091
Weighted TruePositiveRate: 0.6052009456264775
Weighted MatthewsCorrelation: 0.475124416943004
Weighted FMeasure: 0.6045229095265531
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5085194783522042
Mean absolute error: 0.20780191509227772
Coverage of cases: 70.2127659574468
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6052009456264775
Weighted FalsePositiveRate: 0.1326854549236279
Kappa statistic: 0.4737826180880939
Training time: 5.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 35.69739952718676
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6960515635855777
Weighted AreaUnderROC: 0.8344647167470846
Root mean squared error: 0.3601402739234117
Relative absolute error: 45.37485628347607
Root relative squared error: 83.17083363828293
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5815280882935691
Weighted FMeasure: 0.6654534063156207
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6060544808089644
Mean absolute error: 0.17015571106303526
Coverage of cases: 85.34278959810875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10643633793009441
Kappa statistic: 0.5774320284485265
Training time: 17.0
		
Time end:Sun Oct 08 09.56.13 EEST 2017