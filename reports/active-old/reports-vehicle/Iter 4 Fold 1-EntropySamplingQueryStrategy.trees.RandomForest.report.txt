Sun Oct 08 09.52.40 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.40 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.01182033096927
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6149025652571751
Weighted AreaUnderROC: 0.8286249777538006
Root mean squared error: 0.36404608338426525
Relative absolute error: 62.3798266351458
Root relative squared error: 84.0728416957338
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.4983390856038692
Weighted FMeasure: 0.6178663994603519
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6202087179669236
Mean absolute error: 0.23392434988179672
Coverage of cases: 93.14420803782505
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12366580691942247
Kappa statistic: 0.5055467865864555
Training time: 9.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 64.06619385342789
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6512835447450317
Weighted AreaUnderROC: 0.8509679804666698
Root mean squared error: 0.3456415558152447
Relative absolute error: 58.69188337273447
Root relative squared error: 79.82249811722103
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5439365302070355
Weighted FMeasure: 0.6525961632274326
Iteration time: 27.0
Weighted AreaUnderPRC: 0.660561868556757
Mean absolute error: 0.22009456264775426
Coverage of cases: 96.21749408983452
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 37.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.1124487976863684
Kappa statistic: 0.5497856441096787
Training time: 25.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 63.47517730496454
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6550375700979857
Weighted AreaUnderROC: 0.85582199420734
Root mean squared error: 0.33792930842720625
Relative absolute error: 58.72340425531914
Root relative squared error: 78.04143087500464
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5518351166261517
Weighted FMeasure: 0.6593616910863549
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6789943054490337
Mean absolute error: 0.22021276595744677
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 53.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11032307653858552
Kappa statistic: 0.5590994371482175
Training time: 15.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 63.94799054373522
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6565815468395476
Weighted AreaUnderROC: 0.8621295668711386
Root mean squared error: 0.3371063065837454
Relative absolute error: 57.14736012608356
Root relative squared error: 77.85136674065838
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5352650051448544
Weighted FMeasure: 0.6487149262061548
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6875611885422566
Mean absolute error: 0.21430260047281338
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.11688660709112139
Kappa statistic: 0.5339849187503257
Training time: 16.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 61.87943262411348
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6942525513158943
Weighted AreaUnderROC: 0.8802783706512597
Root mean squared error: 0.3217674299238673
Relative absolute error: 54.18439716312051
Root relative squared error: 74.30900491319953
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5986058372863149
Weighted FMeasure: 0.6945568428639903
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7119803681702016
Mean absolute error: 0.2031914893617019
Coverage of cases: 99.29078014184397
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09941382804658686
Kappa statistic: 0.6031126666170229
Training time: 18.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 61.406619385342786
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6814569387386751
Weighted AreaUnderROC: 0.8763149810175246
Root mean squared error: 0.32482037225469473
Relative absolute error: 54.436564223798264
Root relative squared error: 75.01405174380899
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5780315031363931
Weighted FMeasure: 0.6824696787433486
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7151567209735378
Mean absolute error: 0.20413711583924352
Coverage of cases: 98.81796690307328
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10537234215870103
Kappa statistic: 0.5808761081725397
Training time: 21.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 63.652482269503544
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6805587745999371
Weighted AreaUnderROC: 0.8769084262170835
Root mean squared error: 0.3215836992916746
Relative absolute error: 54.21591804570526
Root relative squared error: 74.26657414121759
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5861245805033313
Weighted FMeasure: 0.6844175609224432
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7121062517352305
Mean absolute error: 0.20330969267139473
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 138.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10177860083393822
Kappa statistic: 0.5937325500882268
Training time: 24.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 59.810874704491724
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.717720170219388
Weighted AreaUnderROC: 0.8940808867785783
Root mean squared error: 0.3076833286560397
Relative absolute error: 50.559495665878636
Root relative squared error: 71.05642104988985
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6235609191175904
Weighted FMeasure: 0.7150780089829314
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7455325736574713
Mean absolute error: 0.18959810874704486
Coverage of cases: 97.87234042553192
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 167.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09446645283933071
Kappa statistic: 0.6249329399141631
Training time: 27.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 59.751773049645394
Incorrectly Classified Instances: 25.768321513002363
Correctly Classified Instances: 74.23167848699764
Weighted Precision: 0.7377557956850072
Weighted AreaUnderROC: 0.9156706679123927
Root mean squared error: 0.2928219268618111
Relative absolute error: 48.41607565011819
Root relative squared error: 67.62432731931662
Weighted TruePositiveRate: 0.7423167848699763
Weighted MatthewsCorrelation: 0.6533749008025761
Weighted FMeasure: 0.7311730904099554
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7758050415938403
Mean absolute error: 0.1815602836879432
Coverage of cases: 99.52718676122932
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 197.0
Weighted Recall: 0.7423167848699763
Weighted FalsePositiveRate: 0.08634105398972218
Kappa statistic: 0.6565253991075486
Training time: 28.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 56.973995271867615
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7186562621426377
Weighted AreaUnderROC: 0.8988734010263111
Root mean squared error: 0.3051178898621777
Relative absolute error: 48.00630417651691
Root relative squared error: 70.46395833859955
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6346378535684141
Weighted FMeasure: 0.7204672472345773
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7457635144477881
Mean absolute error: 0.1800236406619384
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 230.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.08988637783293622
Kappa statistic: 0.6409328508242864
Training time: 32.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 55.7919621749409
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.714080149080958
Weighted AreaUnderROC: 0.9039107846579938
Root mean squared error: 0.3005511092429031
Relative absolute error: 47.09219858156027
Root relative squared error: 69.40930553065229
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6304203478538765
Weighted FMeasure: 0.716736994029489
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7615873593178754
Mean absolute error: 0.176595744680851
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 264.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.0911205764895651
Kappa statistic: 0.6375645410044928
Training time: 33.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 55.082742316784866
Incorrectly Classified Instances: 25.768321513002363
Correctly Classified Instances: 74.23167848699764
Weighted Precision: 0.7317698676400892
Weighted AreaUnderROC: 0.914268194382244
Root mean squared error: 0.29034910103668865
Relative absolute error: 43.97163120567374
Root relative squared error: 67.05325265699922
Weighted TruePositiveRate: 0.7423167848699763
Weighted MatthewsCorrelation: 0.6510256483136704
Weighted FMeasure: 0.7355819793260451
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7863384902415534
Mean absolute error: 0.16489361702127653
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 299.0
Weighted Recall: 0.7423167848699763
Weighted FalsePositiveRate: 0.08631873859716406
Kappa statistic: 0.6564716576265124
Training time: 34.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 56.26477541371158
Incorrectly Classified Instances: 26.24113475177305
Correctly Classified Instances: 73.75886524822695
Weighted Precision: 0.7207355753686491
Weighted AreaUnderROC: 0.9009807447910191
Root mean squared error: 0.3011404642292655
Relative absolute error: 47.09219858156023
Root relative squared error: 69.54541123466213
Weighted TruePositiveRate: 0.7375886524822695
Weighted MatthewsCorrelation: 0.6418282867917637
Weighted FMeasure: 0.7248976436055773
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7529992418207037
Mean absolute error: 0.17659574468085085
Coverage of cases: 97.63593380614657
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 337.0
Weighted Recall: 0.7375886524822695
Weighted FalsePositiveRate: 0.08778274803412844
Kappa statistic: 0.650251772838329
Training time: 38.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 56.205673758865245
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.714183352726476
Weighted AreaUnderROC: 0.9055416667291406
Root mean squared error: 0.29847920281883367
Relative absolute error: 46.49330181245076
Root relative squared error: 68.93081923798341
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6263255064863459
Weighted FMeasure: 0.7163594087127028
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7607975560507811
Mean absolute error: 0.17434988179669034
Coverage of cases: 99.05437352245863
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 382.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09251381451817844
Kappa statistic: 0.6313574471888688
Training time: 44.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 55.20094562647754
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.693238301792159
Weighted AreaUnderROC: 0.9020173111852409
Root mean squared error: 0.30249162307921346
Relative absolute error: 47.31284475965325
Root relative squared error: 69.85744800495628
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6043658935786622
Weighted FMeasure: 0.6969139273390556
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7466158860335116
Mean absolute error: 0.17742316784869971
Coverage of cases: 98.81796690307328
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 427.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09706271599333176
Kappa statistic: 0.6125796747483172
Training time: 45.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 53.486997635933804
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7017810440721326
Weighted AreaUnderROC: 0.8966225844738862
Root mean squared error: 0.30145431576231524
Relative absolute error: 45.736800630417626
Root relative squared error: 69.61789214149886
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6123608783002126
Weighted FMeasure: 0.7038916753750066
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7421472653639738
Mean absolute error: 0.17151300236406608
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 472.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09550828882645518
Kappa statistic: 0.6188820301272553
Training time: 44.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 53.84160756501182
Incorrectly Classified Instances: 25.53191489361702
Correctly Classified Instances: 74.46808510638297
Weighted Precision: 0.7334089668299054
Weighted AreaUnderROC: 0.9057144230000062
Root mean squared error: 0.29336637467036686
Relative absolute error: 44.50748620961383
Root relative squared error: 67.75006215484837
Weighted TruePositiveRate: 0.7446808510638298
Weighted MatthewsCorrelation: 0.653728038203826
Weighted FMeasure: 0.7377711098798391
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7584159448805432
Mean absolute error: 0.16690307328605186
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 520.0
Weighted Recall: 0.7446808510638298
Weighted FalsePositiveRate: 0.0855379581391685
Kappa statistic: 0.6596892179794699
Training time: 47.0
		
Time end:Sun Oct 08 09.52.41 EEST 2017