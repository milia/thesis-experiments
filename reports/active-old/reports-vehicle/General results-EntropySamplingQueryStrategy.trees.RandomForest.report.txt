Sun Oct 08 09.52.31 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.31 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.96335697399527
Incorrectly Classified Instances: 35.29550827423168
Correctly Classified Instances: 64.70449172576832
Weighted Precision: 0.6459399024348584
Weighted AreaUnderROC: 0.8459886439322247
Root mean squared error: 0.3503562591456788
Relative absolute error: 61.784081954294706
Root relative squared error: 80.9113122120112
Weighted TruePositiveRate: 0.6470449172576831
Weighted MatthewsCorrelation: 0.5275977339417798
Weighted FMeasure: 0.6345351728677349
Iteration time: 24.2
Weighted AreaUnderPRC: 0.6602410865241122
Mean absolute error: 0.23169030732860513
Coverage of cases: 94.60992907801418
Instances selection time: 4.3
Test time: 3.8
Accumulative iteration time: 24.2
Weighted Recall: 0.6470449172576831
Weighted FalsePositiveRate: 0.11816178143081471
Kappa statistic: 0.5294754839327426
Training time: 19.9
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 63.4692671394799
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6570570891667665
Weighted AreaUnderROC: 0.8582798230419322
Root mean squared error: 0.33917678627399994
Relative absolute error: 58.36091410559493
Root relative squared error: 78.32952354326642
Weighted TruePositiveRate: 0.6595744680851066
Weighted MatthewsCorrelation: 0.5436208310076986
Weighted FMeasure: 0.649961870943003
Iteration time: 20.2
Weighted AreaUnderPRC: 0.6812726780994269
Mean absolute error: 0.218853427895981
Coverage of cases: 95.67375886524823
Instances selection time: 3.3
Test time: 3.7
Accumulative iteration time: 44.4
Weighted Recall: 0.6595744680851066
Weighted FalsePositiveRate: 0.11410096758121199
Kappa statistic: 0.5462365699252537
Training time: 16.9
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 63.44562647754136
Incorrectly Classified Instances: 31.702127659574465
Correctly Classified Instances: 68.29787234042553
Weighted Precision: 0.6829631223912191
Weighted AreaUnderROC: 0.8608921813079646
Root mean squared error: 0.3348679721988514
Relative absolute error: 57.361702127659555
Root relative squared error: 77.33444555679637
Weighted TruePositiveRate: 0.6829787234042553
Weighted MatthewsCorrelation: 0.5760572215219192
Weighted FMeasure: 0.6747070031529993
Iteration time: 21.8
Weighted AreaUnderPRC: 0.6900315685473829
Mean absolute error: 0.21510638297872334
Coverage of cases: 95.83924349881796
Instances selection time: 3.0
Test time: 3.5
Accumulative iteration time: 66.2
Weighted Recall: 0.6829787234042553
Weighted FalsePositiveRate: 0.10598490955609305
Kappa statistic: 0.5775309661709811
Training time: 18.8
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 63.30378250591017
Incorrectly Classified Instances: 31.962174940898347
Correctly Classified Instances: 68.03782505910165
Weighted Precision: 0.6810538128414284
Weighted AreaUnderROC: 0.8691836412607525
Root mean squared error: 0.33013793908337485
Relative absolute error: 56.49172576832151
Root relative squared error: 76.2420911997979
Weighted TruePositiveRate: 0.6803782505910165
Weighted MatthewsCorrelation: 0.5729842958161366
Weighted FMeasure: 0.6731953599942717
Iteration time: 23.2
Weighted AreaUnderPRC: 0.7008050722385092
Mean absolute error: 0.21184397163120564
Coverage of cases: 97.30496453900709
Instances selection time: 2.0
Test time: 3.1
Accumulative iteration time: 89.4
Weighted Recall: 0.6803782505910165
Weighted FalsePositiveRate: 0.10704928413931221
Kappa statistic: 0.5740118913319702
Training time: 21.2
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 61.89716312056737
Incorrectly Classified Instances: 30.472813238770687
Correctly Classified Instances: 69.52718676122932
Weighted Precision: 0.6930882094634165
Weighted AreaUnderROC: 0.8761511260148094
Root mean squared error: 0.32359120366665767
Relative absolute error: 55.051221434200144
Root relative squared error: 74.73018741773593
Weighted TruePositiveRate: 0.6952718676122932
Weighted MatthewsCorrelation: 0.5915850105543106
Weighted FMeasure: 0.6878582397392712
Iteration time: 23.9
Weighted AreaUnderPRC: 0.7134792737237399
Mean absolute error: 0.20644208037825057
Coverage of cases: 97.32860520094562
Instances selection time: 1.8
Test time: 2.8
Accumulative iteration time: 113.3
Weighted Recall: 0.6952718676122932
Weighted FalsePositiveRate: 0.10209412147847448
Kappa statistic: 0.5938331782008512
Training time: 22.1
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 61.78486997635933
Incorrectly Classified Instances: 30.827423167848696
Correctly Classified Instances: 69.17257683215131
Weighted Precision: 0.6902463698601536
Weighted AreaUnderROC: 0.877693339413339
Root mean squared error: 0.32214826688035375
Relative absolute error: 54.433412135539754
Root relative squared error: 74.39695544093746
Weighted TruePositiveRate: 0.691725768321513
Weighted MatthewsCorrelation: 0.5873578981562708
Weighted FMeasure: 0.6873827214625661
Iteration time: 26.9
Weighted AreaUnderPRC: 0.7127984873650224
Mean absolute error: 0.2041252955082741
Coverage of cases: 97.96690307328605
Instances selection time: 2.2
Test time: 3.0
Accumulative iteration time: 140.2
Weighted Recall: 0.691725768321513
Weighted FalsePositiveRate: 0.10346007997535434
Kappa statistic: 0.5889797643076332
Training time: 24.7
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 60.32505910165484
Incorrectly Classified Instances: 29.007092198581564
Correctly Classified Instances: 70.99290780141845
Weighted Precision: 0.7033152528154135
Weighted AreaUnderROC: 0.8887570416580133
Root mean squared error: 0.3122314413132423
Relative absolute error: 51.94011032308904
Root relative squared error: 72.10676267666612
Weighted TruePositiveRate: 0.7099290780141845
Weighted MatthewsCorrelation: 0.6093007705219036
Weighted FMeasure: 0.702914226505262
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7333008477668461
Mean absolute error: 0.19477541371158386
Coverage of cases: 98.15602836879432
Instances selection time: 1.6
Test time: 3.5
Accumulative iteration time: 171.2
Weighted Recall: 0.7099290780141845
Weighted FalsePositiveRate: 0.0973256402974339
Kappa statistic: 0.6133133546008926
Training time: 29.4
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 60.839243498817964
Incorrectly Classified Instances: 28.912529550827422
Correctly Classified Instances: 71.08747044917257
Weighted Precision: 0.7104212069802229
Weighted AreaUnderROC: 0.8896981317250239
Root mean squared error: 0.311571903570391
Relative absolute error: 52.091410559495635
Root relative squared error: 71.95444895931574
Weighted TruePositiveRate: 0.7108747044917257
Weighted MatthewsCorrelation: 0.6131988909432442
Weighted FMeasure: 0.7050659982148331
Iteration time: 37.5
Weighted AreaUnderPRC: 0.7350953471542677
Mean absolute error: 0.19534278959810863
Coverage of cases: 98.03782505910166
Instances selection time: 2.6
Test time: 3.4
Accumulative iteration time: 208.7
Weighted Recall: 0.7108747044917257
Weighted FalsePositiveRate: 0.09716377686248988
Kappa statistic: 0.6144827170245958
Training time: 34.9
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 60.25413711583924
Incorrectly Classified Instances: 28.888888888888886
Correctly Classified Instances: 71.1111111111111
Weighted Precision: 0.711550292248942
Weighted AreaUnderROC: 0.8886494211244397
Root mean squared error: 0.3120268305704045
Relative absolute error: 51.97163120567374
Root relative squared error: 72.05950984968351
Weighted TruePositiveRate: 0.7111111111111111
Weighted MatthewsCorrelation: 0.6138824484385152
Weighted FMeasure: 0.707796101867315
Iteration time: 42.2
Weighted AreaUnderPRC: 0.7299311329613541
Mean absolute error: 0.19489361702127644
Coverage of cases: 98.13238770685578
Instances selection time: 2.8
Test time: 3.1
Accumulative iteration time: 250.9
Weighted Recall: 0.7111111111111111
Weighted FalsePositiveRate: 0.09716575967694639
Kappa statistic: 0.6148072605495285
Training time: 39.4
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 58.628841607565015
Incorrectly Classified Instances: 28.15602836879433
Correctly Classified Instances: 71.84397163120568
Weighted Precision: 0.7128245862822815
Weighted AreaUnderROC: 0.897265369479717
Root mean squared error: 0.30553796923594356
Relative absolute error: 49.91331757289202
Root relative squared error: 70.56097151440946
Weighted TruePositiveRate: 0.7184397163120568
Weighted MatthewsCorrelation: 0.6210066903405028
Weighted FMeasure: 0.7121840699161878
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7482867116393228
Mean absolute error: 0.1871749408983451
Coverage of cases: 98.3451536643026
Instances selection time: 2.0
Test time: 3.6
Accumulative iteration time: 292.9
Weighted Recall: 0.7184397163120568
Weighted FalsePositiveRate: 0.09459955938142076
Kappa statistic: 0.6246323055668203
Training time: 40.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 57.4113475177305
Incorrectly Classified Instances: 28.912529550827422
Correctly Classified Instances: 71.08747044917257
Weighted Precision: 0.7059489381659165
Weighted AreaUnderROC: 0.8946312544667381
Root mean squared error: 0.3075960147195602
Relative absolute error: 49.78723404255315
Root relative squared error: 71.03625675999767
Weighted TruePositiveRate: 0.7108747044917257
Weighted MatthewsCorrelation: 0.6112358212174371
Weighted FMeasure: 0.7040609131410024
Iteration time: 39.7
Weighted AreaUnderPRC: 0.7399195553590379
Mean absolute error: 0.18670212765957436
Coverage of cases: 98.22695035460993
Instances selection time: 1.5
Test time: 3.0
Accumulative iteration time: 332.6
Weighted Recall: 0.7108747044917257
Weighted FalsePositiveRate: 0.09708007755792433
Kappa statistic: 0.6145826064363404
Training time: 38.2
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 56.01063829787233
Incorrectly Classified Instances: 28.226950354609926
Correctly Classified Instances: 71.77304964539007
Weighted Precision: 0.7136187326220216
Weighted AreaUnderROC: 0.8958991364109371
Root mean squared error: 0.30484186278545694
Relative absolute error: 48.31205673758861
Root relative squared error: 70.4002126157802
Weighted TruePositiveRate: 0.7177304964539009
Weighted MatthewsCorrelation: 0.6207319438989737
Weighted FMeasure: 0.7120863658899269
Iteration time: 41.4
Weighted AreaUnderPRC: 0.7437395101919874
Mean absolute error: 0.1811702127659573
Coverage of cases: 97.99054373522458
Instances selection time: 1.4
Test time: 4.1
Accumulative iteration time: 374.0
Weighted Recall: 0.7177304964539009
Weighted FalsePositiveRate: 0.09485364723237219
Kappa statistic: 0.6236714622108435
Training time: 40.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 56.631205673758856
Incorrectly Classified Instances: 27.28132387706856
Correctly Classified Instances: 72.71867612293144
Weighted Precision: 0.7195934662903889
Weighted AreaUnderROC: 0.8971811181839016
Root mean squared error: 0.3040239563615212
Relative absolute error: 48.37825059101651
Root relative squared error: 70.21132521816773
Weighted TruePositiveRate: 0.7271867612293146
Weighted MatthewsCorrelation: 0.6318772845612819
Weighted FMeasure: 0.7206662918190821
Iteration time: 46.6
Weighted AreaUnderPRC: 0.747064946999644
Mean absolute error: 0.18141843971631194
Coverage of cases: 98.03782505910166
Instances selection time: 2.2
Test time: 3.1
Accumulative iteration time: 420.6
Weighted Recall: 0.7271867612293146
Weighted FalsePositiveRate: 0.09159746796806725
Kappa statistic: 0.6363065741449794
Training time: 44.4
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 56.49527186761229
Incorrectly Classified Instances: 27.683215130023637
Correctly Classified Instances: 72.31678486997637
Weighted Precision: 0.7183528018805132
Weighted AreaUnderROC: 0.8990862449787234
Root mean squared error: 0.3034774388450511
Relative absolute error: 48.15445232466505
Root relative squared error: 70.08511240406736
Weighted TruePositiveRate: 0.7231678486997637
Weighted MatthewsCorrelation: 0.6277204007642087
Weighted FMeasure: 0.7187558973598664
Iteration time: 51.2
Weighted AreaUnderPRC: 0.7485705879105107
Mean absolute error: 0.1805791962174939
Coverage of cases: 98.3451536643026
Instances selection time: 2.8
Test time: 3.0
Accumulative iteration time: 471.8
Weighted Recall: 0.7231678486997637
Weighted FalsePositiveRate: 0.09305997982174308
Kappa statistic: 0.6308975221979121
Training time: 48.4
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 55.5791962174941
Incorrectly Classified Instances: 26.903073286052006
Correctly Classified Instances: 73.09692671394801
Weighted Precision: 0.7225077090886185
Weighted AreaUnderROC: 0.9059886378595425
Root mean squared error: 0.29864514085618243
Relative absolute error: 46.72970843183607
Root relative squared error: 68.96914098619627
Weighted TruePositiveRate: 0.7309692671394799
Weighted MatthewsCorrelation: 0.6365092881390634
Weighted FMeasure: 0.7243707331428674
Iteration time: 52.6
Weighted AreaUnderPRC: 0.7604935525027962
Mean absolute error: 0.17523640661938522
Coverage of cases: 98.53427895981086
Instances selection time: 1.2
Test time: 3.8
Accumulative iteration time: 524.4
Weighted Recall: 0.7309692671394799
Weighted FalsePositiveRate: 0.09034226838328692
Kappa statistic: 0.641354764754816
Training time: 51.4
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 55.41962174940899
Incorrectly Classified Instances: 28.321513002364064
Correctly Classified Instances: 71.67848699763593
Weighted Precision: 0.7083881970736456
Weighted AreaUnderROC: 0.8984465469598076
Root mean squared error: 0.3034061652976821
Relative absolute error: 47.48305752561068
Root relative squared error: 70.06865248336354
Weighted TruePositiveRate: 0.7167848699763594
Weighted MatthewsCorrelation: 0.6176565618840092
Weighted FMeasure: 0.7093315149398759
Iteration time: 54.1
Weighted AreaUnderPRC: 0.74892308183299
Mean absolute error: 0.17806146572104004
Coverage of cases: 98.03782505910166
Instances selection time: 1.6
Test time: 3.4
Accumulative iteration time: 578.5
Weighted Recall: 0.7167848699763594
Weighted FalsePositiveRate: 0.09501244942843921
Kappa statistic: 0.6224576763777707
Training time: 52.5
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 54.90543735224586
Incorrectly Classified Instances: 26.359338061465717
Correctly Classified Instances: 73.6406619385343
Weighted Precision: 0.7313879581924251
Weighted AreaUnderROC: 0.9069816157349587
Root mean squared error: 0.2960033932376061
Relative absolute error: 45.93223010244282
Root relative squared error: 68.35905550670984
Weighted TruePositiveRate: 0.7364066193853429
Weighted MatthewsCorrelation: 0.6453659970497359
Weighted FMeasure: 0.7322088478095143
Iteration time: 55.9
Weighted AreaUnderPRC: 0.7682780489584312
Mean absolute error: 0.1722458628841606
Coverage of cases: 98.34515366430261
Instances selection time: 1.3
Test time: 3.5
Accumulative iteration time: 634.4
Weighted Recall: 0.7364066193853429
Weighted FalsePositiveRate: 0.08855537813854338
Kappa statistic: 0.6485423626215543
Training time: 54.6
		
Time end:Sun Oct 08 09.52.45 EEST 2017