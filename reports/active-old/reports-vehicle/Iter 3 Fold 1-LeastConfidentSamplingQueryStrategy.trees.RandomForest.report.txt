Sun Oct 08 09.53.55 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.53.55 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 65.83924349881796
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6566067908552466
Weighted AreaUnderROC: 0.8656907066955597
Root mean squared error: 0.33663260834679115
Relative absolute error: 60.83530338849491
Root relative squared error: 77.7419708188103
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5440173406433018
Weighted FMeasure: 0.6527047970037235
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6962671285779507
Mean absolute error: 0.2281323877068559
Coverage of cases: 96.45390070921985
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11454268356308751
Kappa statistic: 0.5457481001991155
Training time: 8.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 66.01654846335697
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6915460047615382
Weighted AreaUnderROC: 0.8707273008514357
Root mean squared error: 0.33462515636435286
Relative absolute error: 59.3223010244287
Root relative squared error: 77.2783696418319
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.56602382459892
Weighted FMeasure: 0.6642876675369328
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7009257168037081
Mean absolute error: 0.2224586288416076
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11097120285477616
Kappa statistic: 0.5557947106182365
Training time: 11.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 62.88416075650118
Incorrectly Classified Instances: 24.34988179669031
Correctly Classified Instances: 75.65011820330969
Weighted Precision: 0.771265194572106
Weighted AreaUnderROC: 0.8971454304468338
Root mean squared error: 0.3084698758430075
Relative absolute error: 53.64854215918049
Root relative squared error: 71.23806634194032
Weighted TruePositiveRate: 0.7565011820330969
Weighted MatthewsCorrelation: 0.6810205673604959
Weighted FMeasure: 0.750673515875589
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7643911976749539
Mean absolute error: 0.2011820330969268
Coverage of cases: 97.16312056737588
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7565011820330969
Weighted FalsePositiveRate: 0.08129886759198841
Kappa statistic: 0.6754612697301283
Training time: 13.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 59.1016548463357
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.7331501219087425
Weighted AreaUnderROC: 0.8984229725651043
Root mean squared error: 0.3107795430041717
Relative absolute error: 51.12687155240352
Root relative squared error: 71.77146112483497
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.6451631528509629
Weighted FMeasure: 0.723653103566693
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7478282944363537
Mean absolute error: 0.19172576832151322
Coverage of cases: 96.92671394799055
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 54.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.08788265708115447
Kappa statistic: 0.6472559136902767
Training time: 15.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 58.628841607565015
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6885214270303308
Weighted AreaUnderROC: 0.8702624505203087
Root mean squared error: 0.3257288608338871
Relative absolute error: 53.459416863672175
Root relative squared error: 75.22385819410995
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.5990836683162298
Weighted FMeasure: 0.6755090048666382
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6779297248281443
Mean absolute error: 0.20047281323877067
Coverage of cases: 95.27186761229315
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 74.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09681965361374777
Kappa statistic: 0.6098482594465934
Training time: 18.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 58.628841607565015
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7080379703168359
Weighted AreaUnderROC: 0.8839585182538394
Root mean squared error: 0.31622776601683794
Relative absolute error: 51.44208037825064
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6062935507083076
Weighted FMeasure: 0.6769833272805014
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7140464554947044
Mean absolute error: 0.19290780141843988
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 95.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09696401420556133
Kappa statistic: 0.6096972944012858
Training time: 20.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 60.520094562647756
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.708723660298019
Weighted AreaUnderROC: 0.8861064462279776
Root mean squared error: 0.317477498690759
Relative absolute error: 52.23010244286846
Root relative squared error: 73.3182877323035
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6108730804863115
Weighted FMeasure: 0.6810354290460673
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7190814976485287
Mean absolute error: 0.19586288416075673
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09552022517098295
Kappa statistic: 0.6160553530243286
Training time: 23.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 57.68321513002364
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7202115592421771
Weighted AreaUnderROC: 0.9009368084887919
Root mean squared error: 0.30463325235390215
Relative absolute error: 50.023640661938536
Root relative squared error: 70.35203610025465
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6328298049582441
Weighted FMeasure: 0.7125478758131096
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7526672411952694
Mean absolute error: 0.1875886524822695
Coverage of cases: 97.63593380614657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 147.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09074448936757258
Kappa statistic: 0.6376995091869186
Training time: 27.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 56.442080378250594
Incorrectly Classified Instances: 24.58628841607565
Correctly Classified Instances: 75.41371158392435
Weighted Precision: 0.7471403287006123
Weighted AreaUnderROC: 0.9044310420946425
Root mean squared error: 0.29948734763725676
Relative absolute error: 47.62805358550042
Root relative squared error: 69.16364031090289
Weighted TruePositiveRate: 0.7541371158392435
Weighted MatthewsCorrelation: 0.668337334738966
Weighted FMeasure: 0.7406799217506749
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7594418749803353
Mean absolute error: 0.17860520094562654
Coverage of cases: 97.16312056737588
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 176.0
Weighted Recall: 0.7541371158392435
Weighted FalsePositiveRate: 0.08133243722547301
Kappa statistic: 0.6726201107340597
Training time: 28.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 57.97872340425532
Incorrectly Classified Instances: 25.768321513002363
Correctly Classified Instances: 74.23167848699764
Weighted Precision: 0.7380878653905316
Weighted AreaUnderROC: 0.9040437108379814
Root mean squared error: 0.2965522928782502
Relative absolute error: 48.19542947202524
Root relative squared error: 68.4858184488234
Weighted TruePositiveRate: 0.7423167848699763
Weighted MatthewsCorrelation: 0.6537136422648498
Weighted FMeasure: 0.7377447539779456
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7700809447217382
Mean absolute error: 0.18073286052009463
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 208.0
Weighted Recall: 0.7423167848699763
Weighted FalsePositiveRate: 0.08646103688211361
Kappa statistic: 0.6564562998286267
Training time: 31.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 59.33806146572104
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6839643614960526
Weighted AreaUnderROC: 0.890614910695223
Root mean squared error: 0.3101894487080993
Relative absolute error: 51.94641449960597
Root relative squared error: 71.6351846845611
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5728179168659084
Weighted FMeasure: 0.6806783520883458
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7302234913259125
Mean absolute error: 0.1947990543735224
Coverage of cases: 99.29078014184397
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 244.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10815516378216422
Kappa statistic: 0.5711378325791517
Training time: 35.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 58.51063829787234
Incorrectly Classified Instances: 25.29550827423168
Correctly Classified Instances: 74.70449172576832
Weighted Precision: 0.7431916841677946
Weighted AreaUnderROC: 0.8997256905403519
Root mean squared error: 0.3008066381743263
Relative absolute error: 49.07801418439712
Root relative squared error: 69.46831740958946
Weighted TruePositiveRate: 0.7470449172576832
Weighted MatthewsCorrelation: 0.6601073001735694
Weighted FMeasure: 0.7443347109936791
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7570906707284982
Mean absolute error: 0.1840425531914892
Coverage of cases: 98.81796690307328
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 282.0
Weighted Recall: 0.7470449172576832
Weighted FalsePositiveRate: 0.08524638064618563
Kappa statistic: 0.6626115347630656
Training time: 37.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 57.269503546099294
Incorrectly Classified Instances: 25.059101654846337
Correctly Classified Instances: 74.94089834515367
Weighted Precision: 0.7430837788647882
Weighted AreaUnderROC: 0.914012112813261
Root mean squared error: 0.2903083874277054
Relative absolute error: 47.02915681639087
Root relative squared error: 67.04385025175677
Weighted TruePositiveRate: 0.7494089834515366
Weighted MatthewsCorrelation: 0.6622885155629845
Weighted FMeasure: 0.7457147627562067
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7818232616204207
Mean absolute error: 0.17635933806146578
Coverage of cases: 98.58156028368795
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 322.0
Weighted Recall: 0.7494089834515366
Weighted FalsePositiveRate: 0.08402546000310315
Kappa statistic: 0.6659166095431108
Training time: 39.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 54.90543735224586
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7187983982691353
Weighted AreaUnderROC: 0.8972624857429884
Root mean squared error: 0.30007879185953334
Relative absolute error: 47.21828211189911
Root relative squared error: 69.30022850327971
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6239899612706954
Weighted FMeasure: 0.7187046719411387
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7463310620954974
Mean absolute error: 0.17706855791962167
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 364.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09468490135596327
Kappa statistic: 0.624801544413056
Training time: 41.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 55.378250591016545
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7227107064430375
Weighted AreaUnderROC: 0.91059987279839
Root mean squared error: 0.2913853760095827
Relative absolute error: 45.705279747832925
Root relative squared error: 67.29257011082116
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6345387148236681
Weighted FMeasure: 0.7251219890310433
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7754750678650745
Mean absolute error: 0.1713947990543735
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 408.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09116154871900162
Kappa statistic: 0.637391634924303
Training time: 43.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.442080378250594
Incorrectly Classified Instances: 24.822695035460992
Correctly Classified Instances: 75.177304964539
Weighted Precision: 0.747256103902013
Weighted AreaUnderROC: 0.924547361329707
Root mean squared error: 0.2811660959690173
Relative absolute error: 45.074862096138695
Root relative squared error: 64.93252847788332
Weighted TruePositiveRate: 0.75177304964539
Weighted MatthewsCorrelation: 0.6662723013832033
Weighted FMeasure: 0.7492952236232192
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8015880461990371
Mean absolute error: 0.1690307328605201
Coverage of cases: 99.52718676122932
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 453.0
Weighted Recall: 0.75177304964539
Weighted FalsePositiveRate: 0.08336206870597047
Kappa statistic: 0.6689820162919129
Training time: 44.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 53.84160756501182
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7100286986316119
Weighted AreaUnderROC: 0.9087025165058992
Root mean squared error: 0.29531407330375065
Relative absolute error: 45.92592592592595
Root relative squared error: 68.19986388696212
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6180948697096637
Weighted FMeasure: 0.7122612611865932
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7763545832361407
Mean absolute error: 0.1722222222222223
Coverage of cases: 98.81796690307328
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 499.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09523594225117554
Kappa statistic: 0.6217162872154116
Training time: 46.0
		
Time end:Sun Oct 08 09.53.56 EEST 2017