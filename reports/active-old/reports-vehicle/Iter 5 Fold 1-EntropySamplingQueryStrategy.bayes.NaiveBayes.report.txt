Sun Oct 08 09.51.32 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.51.32 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 41.48936170212766
Incorrectly Classified Instances: 52.95508274231678
Correctly Classified Instances: 47.04491725768322
Weighted Precision: 0.43639374309258294
Weighted AreaUnderROC: 0.7720609417213525
Root mean squared error: 0.4729200204842421
Relative absolute error: 71.91168520421145
Root relative squared error: 109.2162004526962
Weighted TruePositiveRate: 0.47044917257683216
Weighted MatthewsCorrelation: 0.28449521792763166
Weighted FMeasure: 0.41384826156563437
Iteration time: 14.0
Weighted AreaUnderPRC: 0.49554973465519064
Mean absolute error: 0.2696688195157929
Coverage of cases: 71.39479905437352
Instances selection time: 13.0
Test time: 19.0
Accumulative iteration time: 14.0
Weighted Recall: 0.47044917257683216
Weighted FalsePositiveRate: 0.171856124411839
Kappa statistic: 0.29789929976658885
Training time: 1.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 42.139479905437355
Incorrectly Classified Instances: 52.95508274231678
Correctly Classified Instances: 47.04491725768322
Weighted Precision: 0.45503090229446697
Weighted AreaUnderROC: 0.779833510962339
Root mean squared error: 0.4665978302574656
Relative absolute error: 70.72342400964439
Root relative squared error: 107.75615316097725
Weighted TruePositiveRate: 0.47044917257683216
Weighted MatthewsCorrelation: 0.2900611244993223
Weighted FMeasure: 0.41991673474787927
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5108051494705649
Mean absolute error: 0.26521284003616646
Coverage of cases: 73.5224586288416
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 28.0
Weighted Recall: 0.47044917257683216
Weighted FalsePositiveRate: 0.17194635078147621
Kappa statistic: 0.29789929976658885
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 38.2387706855792
Incorrectly Classified Instances: 47.990543735224584
Correctly Classified Instances: 52.009456264775416
Weighted Precision: 0.5482992701859295
Weighted AreaUnderROC: 0.791146445397335
Root mean squared error: 0.45706298684189234
Relative absolute error: 65.38999286238935
Root relative squared error: 105.55417539591238
Weighted TruePositiveRate: 0.5200945626477541
Weighted MatthewsCorrelation: 0.3746904728925928
Weighted FMeasure: 0.4656671786024444
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5461183822332916
Mean absolute error: 0.2452124732339601
Coverage of cases: 69.50354609929079
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 39.0
Weighted Recall: 0.5200945626477541
Weighted FalsePositiveRate: 0.15394967739109258
Kappa statistic: 0.36463458849122077
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 42.494089834515364
Incorrectly Classified Instances: 48.463356973995275
Correctly Classified Instances: 51.536643026004725
Weighted Precision: 0.5554077535339682
Weighted AreaUnderROC: 0.8165583860340894
Root mean squared error: 0.4367509806774482
Relative absolute error: 64.15992939323873
Root relative squared error: 100.8633185051831
Weighted TruePositiveRate: 0.5153664302600472
Weighted MatthewsCorrelation: 0.37562739624167585
Weighted FMeasure: 0.4933519561144623
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5860279478364007
Mean absolute error: 0.2405997352246452
Coverage of cases: 80.37825059101655
Instances selection time: 10.0
Test time: 31.0
Accumulative iteration time: 50.0
Weighted Recall: 0.5153664302600472
Weighted FalsePositiveRate: 0.15633303579777014
Kappa statistic: 0.3578093756942901
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 41.371158392434985
Incorrectly Classified Instances: 47.5177304964539
Correctly Classified Instances: 52.4822695035461
Weighted Precision: 0.5791915860218418
Weighted AreaUnderROC: 0.8019791406615586
Root mean squared error: 0.4457090914934247
Relative absolute error: 65.1554231766925
Root relative squared error: 102.93210558159691
Weighted TruePositiveRate: 0.524822695035461
Weighted MatthewsCorrelation: 0.3944453774179564
Weighted FMeasure: 0.5136714766930162
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5636290865410445
Mean absolute error: 0.24433283691259686
Coverage of cases: 77.30496453900709
Instances selection time: 12.0
Test time: 19.0
Accumulative iteration time: 63.0
Weighted Recall: 0.524822695035461
Weighted FalsePositiveRate: 0.15338964392012291
Kappa statistic: 0.3700833487682904
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 48.995271867612296
Incorrectly Classified Instances: 47.75413711583924
Correctly Classified Instances: 52.24586288416076
Weighted Precision: 0.6018190214051987
Weighted AreaUnderROC: 0.7899764568672955
Root mean squared error: 0.4312280153111931
Relative absolute error: 66.6486341386889
Root relative squared error: 99.58784428881016
Weighted TruePositiveRate: 0.5224586288416075
Weighted MatthewsCorrelation: 0.40026901482580823
Weighted FMeasure: 0.5122638994943635
Iteration time: 10.0
Weighted AreaUnderPRC: 0.578554077033518
Mean absolute error: 0.24993237802008336
Coverage of cases: 82.74231678486997
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 73.0
Weighted Recall: 0.5224586288416075
Weighted FalsePositiveRate: 0.1542312342893911
Kappa statistic: 0.3669822643019069
Training time: 1.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 48.226950354609926
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.6419838268545374
Weighted AreaUnderROC: 0.7974086374362467
Root mean squared error: 0.42560628905281156
Relative absolute error: 65.2704765809141
Root relative squared error: 98.28956222137538
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.44711058325921343
Weighted FMeasure: 0.5409560136620952
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5829696543041917
Mean absolute error: 0.2447642871784279
Coverage of cases: 82.50591016548464
Instances selection time: 11.0
Test time: 22.0
Accumulative iteration time: 85.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.14328132094903745
Kappa statistic: 0.41077620698853035
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 45.212765957446805
Incorrectly Classified Instances: 47.5177304964539
Correctly Classified Instances: 52.4822695035461
Weighted Precision: 0.6276039387753187
Weighted AreaUnderROC: 0.7957545493064699
Root mean squared error: 0.4384947814123136
Relative absolute error: 66.24878026289417
Root relative squared error: 101.26603203465815
Weighted TruePositiveRate: 0.524822695035461
Weighted MatthewsCorrelation: 0.4020572654675171
Weighted FMeasure: 0.46871437690436735
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5965674043725053
Mean absolute error: 0.24843292598585315
Coverage of cases: 78.72340425531915
Instances selection time: 13.0
Test time: 19.0
Accumulative iteration time: 99.0
Weighted Recall: 0.524822695035461
Weighted FalsePositiveRate: 0.15372727720473905
Kappa statistic: 0.3699059561128526
Training time: 1.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 44.089834515366434
Incorrectly Classified Instances: 45.8628841607565
Correctly Classified Instances: 54.1371158392435
Weighted Precision: 0.6299761091723267
Weighted AreaUnderROC: 0.7867812656141203
Root mean squared error: 0.4394519968963952
Relative absolute error: 66.63494497224146
Root relative squared error: 101.48709148162094
Weighted TruePositiveRate: 0.541371158392435
Weighted MatthewsCorrelation: 0.4281345785711324
Weighted FMeasure: 0.5141553941077663
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5760319046712448
Mean absolute error: 0.24988104364590547
Coverage of cases: 76.59574468085107
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 106.0
Weighted Recall: 0.541371158392435
Weighted FalsePositiveRate: 0.14772630263714917
Kappa statistic: 0.3921873611234557
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 43.61702127659574
Incorrectly Classified Instances: 47.5177304964539
Correctly Classified Instances: 52.4822695035461
Weighted Precision: 0.585936909658128
Weighted AreaUnderROC: 0.7767208958004572
Root mean squared error: 0.4465859313182629
Relative absolute error: 68.09745827987429
Root relative squared error: 103.13460306515954
Weighted TruePositiveRate: 0.524822695035461
Weighted MatthewsCorrelation: 0.39678354384670317
Weighted FMeasure: 0.4955404086629231
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5444923326586892
Mean absolute error: 0.2553654685495286
Coverage of cases: 75.65011820330969
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 113.0
Weighted Recall: 0.524822695035461
Weighted FalsePositiveRate: 0.153173182370886
Kappa statistic: 0.3704098633788737
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 43.61702127659574
Incorrectly Classified Instances: 47.75413711583924
Correctly Classified Instances: 52.24586288416076
Weighted Precision: 0.6066264197902388
Weighted AreaUnderROC: 0.7815184826867719
Root mean squared error: 0.4434289061225101
Relative absolute error: 67.34452207943028
Root relative squared error: 102.40551932651701
Weighted TruePositiveRate: 0.5224586288416075
Weighted MatthewsCorrelation: 0.4025671731853449
Weighted FMeasure: 0.49550755597040835
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5629922266533248
Mean absolute error: 0.25254195779786354
Coverage of cases: 76.12293144208037
Instances selection time: 6.0
Test time: 30.0
Accumulative iteration time: 120.0
Weighted Recall: 0.5224586288416075
Weighted FalsePositiveRate: 0.1536688248614657
Kappa statistic: 0.36746492948884035
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 44.0307328605201
Incorrectly Classified Instances: 47.04491725768322
Correctly Classified Instances: 52.95508274231678
Weighted Precision: 0.5989164613311075
Weighted AreaUnderROC: 0.7849347037728723
Root mean squared error: 0.43483357961217245
Relative absolute error: 65.90027760379941
Root relative squared error: 100.42051369671053
Weighted TruePositiveRate: 0.5295508274231678
Weighted MatthewsCorrelation: 0.40592283728720374
Weighted FMeasure: 0.5042435911900585
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5642200048572261
Mean absolute error: 0.2471260410142478
Coverage of cases: 78.01418439716312
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 126.0
Weighted Recall: 0.5295508274231678
Weighted FalsePositiveRate: 0.1518362895754276
Kappa statistic: 0.37654517579267793
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 45.685579196217496
Incorrectly Classified Instances: 50.354609929078016
Correctly Classified Instances: 49.645390070921984
Weighted Precision: 0.5702123296630858
Weighted AreaUnderROC: 0.7802995145375984
Root mean squared error: 0.44272129873736676
Relative absolute error: 68.73557575554631
Root relative squared error: 102.24210440079978
Weighted TruePositiveRate: 0.49645390070921985
Weighted MatthewsCorrelation: 0.3604243487059976
Weighted FMeasure: 0.46881652290546116
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5458663713059957
Mean absolute error: 0.25775840908329867
Coverage of cases: 79.43262411347517
Instances selection time: 4.0
Test time: 21.0
Accumulative iteration time: 131.0
Weighted Recall: 0.49645390070921985
Weighted FalsePositiveRate: 0.16281298298645702
Kappa statistic: 0.3328717929732331
Training time: 1.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 45.39007092198582
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.5550915052433613
Weighted AreaUnderROC: 0.7758831077336192
Root mean squared error: 0.44408713305674624
Relative absolute error: 69.24841256393638
Root relative squared error: 102.55753032558465
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.3524340603463598
Weighted FMeasure: 0.46135828946107066
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5274469217300957
Mean absolute error: 0.25968154711476144
Coverage of cases: 78.25059101654847
Instances selection time: 7.0
Test time: 31.0
Accumulative iteration time: 139.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16360368228537064
Kappa statistic: 0.32984393970801623
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 45.39007092198582
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.5478624893704587
Weighted AreaUnderROC: 0.7765969812349371
Root mean squared error: 0.445613024192131
Relative absolute error: 69.37403616870147
Root relative squared error: 102.90991978869202
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.3494219891807815
Weighted FMeasure: 0.46246042137736587
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5327726547321987
Mean absolute error: 0.2601526356326305
Coverage of cases: 78.25059101654847
Instances selection time: 3.0
Test time: 33.0
Accumulative iteration time: 143.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.16409864679426528
Kappa statistic: 0.3294169938513964
Training time: 1.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 44.73995271867612
Incorrectly Classified Instances: 50.59101654846336
Correctly Classified Instances: 49.40898345153664
Weighted Precision: 0.5530862694053033
Weighted AreaUnderROC: 0.7739505229584804
Root mean squared error: 0.4500280795075524
Relative absolute error: 70.29418047711891
Root relative squared error: 103.92953313863028
Weighted TruePositiveRate: 0.4940898345153664
Weighted MatthewsCorrelation: 0.35144715510741775
Weighted FMeasure: 0.4580833458300729
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5269272530652404
Mean absolute error: 0.2636031767891959
Coverage of cases: 77.06855791962175
Instances selection time: 6.0
Test time: 33.0
Accumulative iteration time: 151.0
Weighted Recall: 0.4940898345153664
Weighted FalsePositiveRate: 0.1637154259231582
Kappa statistic: 0.32970499377998935
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 45.62647754137116
Incorrectly Classified Instances: 50.354609929078016
Correctly Classified Instances: 49.645390070921984
Weighted Precision: 0.5476029149523457
Weighted AreaUnderROC: 0.7727334494054428
Root mean squared error: 0.4489993333190485
Relative absolute error: 70.53798645913298
Root relative squared error: 103.69195438308606
Weighted TruePositiveRate: 0.49645390070921985
Weighted MatthewsCorrelation: 0.3516678597939402
Weighted FMeasure: 0.46550047033833236
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5298987515871535
Mean absolute error: 0.2645174492217487
Coverage of cases: 77.54137115839244
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 154.0
Weighted Recall: 0.49645390070921985
Weighted FalsePositiveRate: 0.16341690604133566
Kappa statistic: 0.3324318717306581
Training time: 2.0
		
Time end:Sun Oct 08 09.51.33 EEST 2017