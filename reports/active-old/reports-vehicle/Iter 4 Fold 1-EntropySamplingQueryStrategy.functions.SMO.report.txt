Sun Oct 08 09.51.53 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.51.53 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5506795848403413
Weighted AreaUnderROC: 0.7668404582872281
Root mean squared error: 0.3875356394127866
Relative absolute error: 81.11373785132638
Root relative squared error: 89.49752229421844
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.4080214756141045
Weighted FMeasure: 0.5536272619319976
Iteration time: 111.0
Weighted AreaUnderPRC: 0.495793595733729
Mean absolute error: 0.3041765169424739
Coverage of cases: 95.74468085106383
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 111.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14755761934051104
Kappa statistic: 0.4139572300060334
Training time: 103.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.56479161965993
Weighted AreaUnderROC: 0.7818526303895945
Root mean squared error: 0.38528386473142073
Relative absolute error: 80.53585500393999
Root relative squared error: 88.97749720684205
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.4416905919927181
Weighted FMeasure: 0.568895222094932
Iteration time: 97.0
Weighted AreaUnderPRC: 0.5179341925475105
Mean absolute error: 0.30200945626477493
Coverage of cases: 95.27186761229315
Instances selection time: 11.0
Test time: 3.0
Accumulative iteration time: 208.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.13683610389392206
Kappa statistic: 0.4551404235041843
Training time: 86.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6300190967661272
Weighted AreaUnderROC: 0.8241874538443834
Root mean squared error: 0.3710887438227708
Relative absolute error: 77.64644076700806
Root relative squared error: 85.69927445572672
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5316714695257952
Weighted FMeasure: 0.6302739110449336
Iteration time: 103.0
Weighted AreaUnderPRC: 0.57529736647563
Mean absolute error: 0.29117415287628023
Coverage of cases: 95.98108747044917
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 311.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11398385735137342
Kappa statistic: 0.5462827092535623
Training time: 98.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.70921985815603
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6251496522632856
Weighted AreaUnderROC: 0.8227055753676318
Root mean squared error: 0.3724576722601196
Relative absolute error: 77.90911478854733
Root relative squared error: 86.0154149364486
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.525072738524562
Weighted FMeasure: 0.6236195340284346
Iteration time: 100.0
Weighted AreaUnderPRC: 0.566766765404636
Mean absolute error: 0.29215918045705247
Coverage of cases: 96.45390070921985
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 411.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11526028029994874
Kappa statistic: 0.5401523443608014
Training time: 95.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.82742316784869
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6276439673113676
Weighted AreaUnderROC: 0.815903926171774
Root mean squared error: 0.3734261254715042
Relative absolute error: 78.1192540057788
Root relative squared error: 86.23906962536478
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5263210825257655
Weighted FMeasure: 0.6256276792354678
Iteration time: 100.0
Weighted AreaUnderPRC: 0.56505376922676
Mean absolute error: 0.2929472025216705
Coverage of cases: 95.98108747044917
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 511.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.1157558188896682
Kappa statistic: 0.5399639467842616
Training time: 96.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.82742316784869
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6227840964473077
Weighted AreaUnderROC: 0.8176323998532066
Root mean squared error: 0.3717075938552449
Relative absolute error: 77.75151037562382
Root relative squared error: 85.8421917488615
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5240868112205319
Weighted FMeasure: 0.6236057897531426
Iteration time: 112.0
Weighted AreaUnderPRC: 0.5669333892665417
Mean absolute error: 0.2915681639085893
Coverage of cases: 96.6903073286052
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 623.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.115655290958563
Kappa statistic: 0.5400393243364018
Training time: 105.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.5038563658908433
Weighted AreaUnderROC: 0.8423454023296191
Root mean squared error: 0.3707789314714566
Relative absolute error: 77.59390596270016
Root relative squared error: 85.62772635795491
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.4941166084921335
Weighted FMeasure: 0.5650499477516562
Iteration time: 108.0
Weighted AreaUnderPRC: 0.59257571138822
Mean absolute error: 0.29097714736012564
Coverage of cases: 96.92671394799055
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 731.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11590165558757473
Kappa statistic: 0.5371302237639388
Training time: 102.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.88652482269504
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.5150863239644997
Weighted AreaUnderROC: 0.8484521261690267
Root mean squared error: 0.3702472214491368
Relative absolute error: 77.48883635408443
Root relative squared error: 85.50493318814806
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5041449450640574
Weighted FMeasure: 0.5716108559995986
Iteration time: 99.0
Weighted AreaUnderPRC: 0.6077721333305414
Mean absolute error: 0.2905831363278166
Coverage of cases: 96.92671394799055
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 830.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11413812466415879
Kappa statistic: 0.5434583578345626
Training time: 97.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.5257773568331141
Weighted AreaUnderROC: 0.8506702419900269
Root mean squared error: 0.36980354576975333
Relative absolute error: 77.33123194116088
Root relative squared error: 85.40247067897806
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5284540637482297
Weighted FMeasure: 0.5834938637355419
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6170209296318508
Mean absolute error: 0.2899921197793533
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 893.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10925449101699937
Kappa statistic: 0.5624832574336993
Training time: 62.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.94562647754137
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5092218971207261
Weighted AreaUnderROC: 0.8468669240522458
Root mean squared error: 0.37051317184003135
Relative absolute error: 77.48883635408446
Root relative squared error: 85.56635180005766
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5138748199735805
Weighted FMeasure: 0.5737002469133998
Iteration time: 122.0
Weighted AreaUnderPRC: 0.6028528493185465
Mean absolute error: 0.29058313632781674
Coverage of cases: 96.45390070921985
Instances selection time: 9.0
Test time: 2.0
Accumulative iteration time: 1015.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11121918170348594
Kappa statistic: 0.5532399143163156
Training time: 113.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.5189102206731266
Weighted AreaUnderROC: 0.847059850153708
Root mean squared error: 0.3721489995372547
Relative absolute error: 77.75151037562377
Root relative squared error: 85.94413002459358
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5287274716523602
Weighted FMeasure: 0.5836280752245503
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6102898393335616
Mean absolute error: 0.29156816390858914
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1078.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10717622428038032
Kappa statistic: 0.5691247323340471
Training time: 62.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.5072234697816264
Weighted AreaUnderROC: 0.8377214653523961
Root mean squared error: 0.374435921082079
Relative absolute error: 78.22432361439445
Root relative squared error: 86.47227193240153
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5089680528005358
Weighted FMeasure: 0.5688305137870875
Iteration time: 82.0
Weighted AreaUnderPRC: 0.5914407898991361
Mean absolute error: 0.2933412135539792
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1160.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11226944830548927
Kappa statistic: 0.54734143350797
Training time: 81.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.5155535072175276
Weighted AreaUnderROC: 0.8455149052621307
Root mean squared error: 0.37051317184003124
Relative absolute error: 77.48883635408448
Root relative squared error: 85.56635180005765
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.512405138732851
Weighted FMeasure: 0.5760636774226456
Iteration time: 82.0
Weighted AreaUnderPRC: 0.5991676483447225
Mean absolute error: 0.2905831363278168
Coverage of cases: 96.45390070921985
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 1242.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.10997487284566408
Kappa statistic: 0.5567454684631791
Training time: 75.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6927868146273202
Weighted AreaUnderROC: 0.847988013092964
Root mean squared error: 0.36645885650636323
Relative absolute error: 76.64827948515885
Root relative squared error: 84.6300477803485
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5829360308961713
Weighted FMeasure: 0.6510741404187703
Iteration time: 68.0
Weighted AreaUnderPRC: 0.6078834176858537
Mean absolute error: 0.2874310480693457
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1310.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.1023286827806818
Kappa statistic: 0.5879829283526158
Training time: 67.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6563332036215852
Weighted AreaUnderROC: 0.8301227898859538
Root mean squared error: 0.36824647129286625
Relative absolute error: 76.96348831100607
Root relative squared error: 85.04287973162646
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5545619130006971
Weighted FMeasure: 0.6571607403055724
Iteration time: 67.0
Weighted AreaUnderPRC: 0.5856981153956685
Mean absolute error: 0.28861308116627277
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1377.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10933522194760657
Kappa statistic: 0.5624279048306554
Training time: 66.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6577980572768316
Weighted AreaUnderROC: 0.8317093570871692
Root mean squared error: 0.36748779751697447
Relative absolute error: 76.80588389808251
Root relative squared error: 84.86767152813117
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5527998146190691
Weighted FMeasure: 0.6556463196036311
Iteration time: 71.0
Weighted AreaUnderPRC: 0.5933229388838855
Mean absolute error: 0.28802206461780944
Coverage of cases: 96.45390070921985
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1448.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11035112079171747
Kappa statistic: 0.5591617970000372
Training time: 70.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6685420366272808
Weighted AreaUnderROC: 0.8380712763105366
Root mean squared error: 0.3657862486775102
Relative absolute error: 76.49067507223539
Root relative squared error: 84.47471565592956
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5717323544687899
Weighted FMeasure: 0.6676934696389464
Iteration time: 71.0
Weighted AreaUnderPRC: 0.6023245642851491
Mean absolute error: 0.2868400315208827
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1519.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10544903378790085
Kappa statistic: 0.5808761081725397
Training time: 70.0
		
Time end:Sun Oct 08 09.51.55 EEST 2017