Sun Oct 08 09.55.47 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.47 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 40.13002364066194
Incorrectly Classified Instances: 43.498817966903076
Correctly Classified Instances: 56.501182033096924
Weighted Precision: 0.6051741193713168
Weighted AreaUnderROC: 0.7666877389351782
Root mean squared error: 0.4263507189168309
Relative absolute error: 58.424331119366684
Root relative squared error: 98.46148093432912
Weighted TruePositiveRate: 0.5650118203309693
Weighted MatthewsCorrelation: 0.4350981810793621
Weighted FMeasure: 0.5703574351550387
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5380104195166187
Mean absolute error: 0.21909124169762506
Coverage of cases: 75.65011820330969
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5650118203309693
Weighted FalsePositiveRate: 0.14881845141100344
Kappa statistic: 0.417904420013462
Training time: 9.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 35.22458628841608
Incorrectly Classified Instances: 38.77068557919622
Correctly Classified Instances: 61.22931442080378
Weighted Precision: 0.6105783338081908
Weighted AreaUnderROC: 0.7674949398052174
Root mean squared error: 0.41954545912686064
Relative absolute error: 52.74617763145093
Root relative squared error: 96.88987350567126
Weighted TruePositiveRate: 0.6122931442080378
Weighted MatthewsCorrelation: 0.48102500763100037
Weighted FMeasure: 0.6091732126941113
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5419703702931182
Mean absolute error: 0.19779816611794096
Coverage of cases: 73.75886524822695
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6122931442080378
Weighted FalsePositiveRate: 0.13111330321921266
Kappa statistic: 0.4822019033401754
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 30.08274231678487
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.654607041313529
Weighted AreaUnderROC: 0.7739730568388843
Root mean squared error: 0.4040943683129105
Relative absolute error: 46.075525035335936
Root relative squared error: 93.32159692938828
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5445767033692255
Weighted FMeasure: 0.653973000378408
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5677400985711709
Mean absolute error: 0.17278321888250975
Coverage of cases: 69.26713947990544
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11450612494288889
Kappa statistic: 0.5486333415414922
Training time: 5.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 31.14657210401891
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6602182478500702
Weighted AreaUnderROC: 0.7747857743231366
Root mean squared error: 0.40614862512696165
Relative absolute error: 46.953356598746616
Root relative squared error: 93.79600721921909
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5426262347195308
Weighted FMeasure: 0.6571015812969313
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5705211749773846
Mean absolute error: 0.17607508724529983
Coverage of cases: 69.50354609929079
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11630084417906711
Kappa statistic: 0.5424366080553835
Training time: 7.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 31.914893617021278
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6611935582593785
Weighted AreaUnderROC: 0.8266252058798782
Root mean squared error: 0.379847245132103
Relative absolute error: 44.960093754419894
Root relative squared error: 87.72196369118298
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5455820369110288
Weighted FMeasure: 0.6595746991766209
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6150355330923319
Mean absolute error: 0.1686003515790746
Coverage of cases: 80.61465721040189
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11482879244637148
Kappa statistic: 0.545751487762316
Training time: 8.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 31.56028368794326
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6503884832865025
Weighted AreaUnderROC: 0.8027562970612231
Root mean squared error: 0.3999356674711647
Relative absolute error: 47.42442597663913
Root relative squared error: 92.36118610920386
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5282026073806918
Weighted FMeasure: 0.6456481028177032
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5932162574044049
Mean absolute error: 0.17784159741239672
Coverage of cases: 77.77777777777777
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 52.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11955608829260846
Kappa statistic: 0.5267856476958303
Training time: 10.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 34.2789598108747
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6225396916855158
Weighted AreaUnderROC: 0.7554124705909272
Root mean squared error: 0.41910169789221285
Relative absolute error: 53.436906629786705
Root relative squared error: 96.78739123835933
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.487846439072234
Weighted FMeasure: 0.6156621041714828
Iteration time: 12.0
Weighted AreaUnderPRC: 0.531719504209119
Mean absolute error: 0.20038839986170015
Coverage of cases: 73.75886524822695
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.13017861062896505
Kappa statistic: 0.4859922469062174
Training time: 11.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 35.99290780141844
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7012739034626743
Weighted AreaUnderROC: 0.8164814490108881
Root mean squared error: 0.3659251923330932
Relative absolute error: 42.10059619043082
Root relative squared error: 84.50680331871078
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6037289872493036
Weighted FMeasure: 0.6981431106964989
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6299319225537598
Mean absolute error: 0.15787723571411558
Coverage of cases: 82.26950354609929
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.09912108625326813
Kappa statistic: 0.6057928874972042
Training time: 12.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 34.33806146572104
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6977821935632158
Weighted AreaUnderROC: 0.8409324125646577
Root mean squared error: 0.36387979049840813
Relative absolute error: 42.322386407081616
Root relative squared error: 84.03443799876824
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.593400021759023
Weighted FMeasure: 0.6933984812144043
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6515462202697159
Mean absolute error: 0.15870894902655608
Coverage of cases: 83.451536643026
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 88.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10305507953179201
Kappa statistic: 0.5930083386786401
Training time: 12.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 31.20567375886525
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7216414678321127
Weighted AreaUnderROC: 0.837545299548925
Root mean squared error: 0.3568539366775925
Relative absolute error: 39.37802109405504
Root relative squared error: 82.41188656087428
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6228422455272973
Weighted FMeasure: 0.7149512693288458
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6589446369674045
Mean absolute error: 0.14766757910270642
Coverage of cases: 79.90543735224587
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 102.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09598639911294303
Kappa statistic: 0.6214511041009464
Training time: 14.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 32.565011820330966
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.663695146793429
Weighted AreaUnderROC: 0.8166253288658418
Root mean squared error: 0.37187717114362573
Relative absolute error: 46.4230339160959
Root relative squared error: 85.88135394609954
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5446233070918013
Weighted FMeasure: 0.6516419301857242
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6151022776511913
Mean absolute error: 0.17408637718535963
Coverage of cases: 79.19621749408984
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 116.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11542134881170443
Kappa statistic: 0.5426924740161941
Training time: 14.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 36.87943262411348
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6540152510377925
Weighted AreaUnderROC: 0.8309656038619286
Root mean squared error: 0.35778251946590045
Relative absolute error: 47.45070501747108
Root relative squared error: 82.62633356999207
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5344338806572682
Weighted FMeasure: 0.629458501487206
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6258549150076536
Mean absolute error: 0.17794014381551654
Coverage of cases: 83.451536643026
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 131.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.11625577121911318
Kappa statistic: 0.534307307783861
Training time: 15.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 36.997635933806144
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6552072793248146
Weighted AreaUnderROC: 0.8346553819356984
Root mean squared error: 0.35366764305738585
Relative absolute error: 46.53422399028327
Root relative squared error: 81.67604356913688
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5388007899554067
Weighted FMeasure: 0.6458914370874762
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6375213019565834
Mean absolute error: 0.17450333996356227
Coverage of cases: 86.05200945626477
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 147.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11576249675655455
Kappa statistic: 0.5402345058626464
Training time: 15.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 36.87943262411348
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6897495225795802
Weighted AreaUnderROC: 0.8593108742344686
Root mean squared error: 0.33212501883062506
Relative absolute error: 42.92598166285216
Root relative squared error: 76.70098761058837
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5733521842878402
Weighted FMeasure: 0.6656765442370575
Iteration time: 17.0
Weighted AreaUnderPRC: 0.67538374351297
Mean absolute error: 0.1609724312356956
Coverage of cases: 88.17966903073287
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 164.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10810541892933322
Kappa statistic: 0.568711067449597
Training time: 17.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 37.11583924349882
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.683783792296268
Weighted AreaUnderROC: 0.8620862947421822
Root mean squared error: 0.33300778824777705
Relative absolute error: 43.4276359214224
Root relative squared error: 76.90485447483839
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5704506876962907
Weighted FMeasure: 0.6602981698494759
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6756440426939548
Mean absolute error: 0.162853634705334
Coverage of cases: 87.94326241134752
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 182.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10760607492298685
Kappa statistic: 0.5689612852839451
Training time: 17.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 38.002364066193856
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6872799451070116
Weighted AreaUnderROC: 0.8552256485230862
Root mean squared error: 0.34128743555761504
Relative absolute error: 42.75925290184003
Root relative squared error: 78.81695711609044
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5806381754314555
Weighted FMeasure: 0.6818367423980963
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6671198125882137
Mean absolute error: 0.16034719838190012
Coverage of cases: 86.7612293144208
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 201.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10513073419951421
Kappa statistic: 0.5810352916644946
Training time: 18.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 39.00709219858156
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.7011906749022573
Weighted AreaUnderROC: 0.8622571036528663
Root mean squared error: 0.33445338349183773
Relative absolute error: 41.31900421388735
Root relative squared error: 77.23870039615745
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5973068305483101
Weighted FMeasure: 0.6954392655713948
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6822058645506199
Mean absolute error: 0.15494626580207754
Coverage of cases: 87.70685579196217
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 221.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10141279688784195
Kappa statistic: 0.5966656982591012
Training time: 19.0
		
Time end:Sun Oct 08 09.55.48 EEST 2017