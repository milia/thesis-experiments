Sun Oct 08 09.56.12 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.12 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 42.31678486997636
Correctly Classified Instances: 57.68321513002364
Weighted Precision: 0.59012179242899
Weighted AreaUnderROC: 0.7172563167760982
Root mean squared error: 0.4498376039608989
Relative absolute error: 58.40318074360627
Root relative squared error: 103.88554469537651
Weighted TruePositiveRate: 0.5768321513002365
Weighted MatthewsCorrelation: 0.4403155589817752
Weighted FMeasure: 0.5815345495320174
Iteration time: 12.0
Weighted AreaUnderPRC: 0.46832049503405926
Mean absolute error: 0.21901192778852352
Coverage of cases: 57.68321513002364
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5768321513002365
Weighted FalsePositiveRate: 0.14231951774804008
Kappa statistic: 0.4355332900946034
Training time: 1.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6701583207966187
Weighted AreaUnderROC: 0.7819951323261618
Root mean squared error: 0.4018194118890793
Relative absolute error: 44.08130437961554
Root relative squared error: 92.79621824791083
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5624717078046328
Weighted FMeasure: 0.6717357874341487
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5556610163807991
Mean absolute error: 0.16530489142355825
Coverage of cases: 67.37588652482269
Instances selection time: 5.0
Test time: 42.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10976860059590357
Kappa statistic: 0.5647369362920543
Training time: 1.0
		
Time end:Sun Oct 08 09.56.12 EEST 2017