Sun Oct 08 09.53.46 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.53.46 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 30.49645390070922
Incorrectly Classified Instances: 37.5886524822695
Correctly Classified Instances: 62.4113475177305
Weighted Precision: 0.6032274607960918
Weighted AreaUnderROC: 0.7637772619152294
Root mean squared error: 0.42169304133648033
Relative absolute error: 51.27396900446548
Root relative squared error: 97.38583637240357
Weighted TruePositiveRate: 0.624113475177305
Weighted MatthewsCorrelation: 0.48839732549480774
Weighted FMeasure: 0.6104880184479684
Iteration time: 15.0
Weighted AreaUnderPRC: 0.526465381269522
Mean absolute error: 0.19227738376674555
Coverage of cases: 69.03073286052009
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.624113475177305
Weighted FalsePositiveRate: 0.12632201195053058
Kappa statistic: 0.49880769632025274
Training time: 8.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 40.543735224586285
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.5697935382190702
Weighted AreaUnderROC: 0.7852700955664682
Root mean squared error: 0.41305809589336445
Relative absolute error: 54.05130899220725
Root relative squared error: 95.3916811419953
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.45442976340389035
Weighted FMeasure: 0.5737037051469965
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5426856460601689
Mean absolute error: 0.20269240872077715
Coverage of cases: 80.1418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.13299237064151537
Kappa statistic: 0.47094295796667707
Training time: 6.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 42.02127659574468
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6288685193847503
Weighted AreaUnderROC: 0.7405393308469421
Root mean squared error: 0.41575523942692255
Relative absolute error: 53.26846343394798
Root relative squared error: 96.01455976005244
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.5012252519375229
Weighted FMeasure: 0.6076756377479404
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5455152943293948
Mean absolute error: 0.1997567378773049
Coverage of cases: 74.23167848699764
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12461293925506102
Kappa statistic: 0.5022973526454928
Training time: 8.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 41.13475177304964
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6568349510666032
Weighted AreaUnderROC: 0.8007884542777106
Root mean squared error: 0.3819548296878334
Relative absolute error: 47.87370797032633
Root relative squared error: 88.2086894954193
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5583294270657841
Weighted FMeasure: 0.6573001453408704
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6135955246334317
Mean absolute error: 0.17952640488872373
Coverage of cases: 80.85106382978724
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10802666000135759
Kappa statistic: 0.5686661258029221
Training time: 8.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 41.903073286052006
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.6385354937106549
Weighted AreaUnderROC: 0.7786076409901936
Root mean squared error: 0.4104097481677352
Relative absolute error: 53.60068487233241
Root relative squared error: 94.78007143307538
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.49505129954121624
Weighted FMeasure: 0.6013566064723762
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5692185698850399
Mean absolute error: 0.20100256827124652
Coverage of cases: 77.30496453900709
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.12971486317746753
Kappa statistic: 0.4858005816988589
Training time: 9.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.74231678486998
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6828328857153578
Weighted AreaUnderROC: 0.8145821520483736
Root mean squared error: 0.3779824141593958
Relative absolute error: 45.65724792675146
Root relative squared error: 87.29129942554871
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5579405513762901
Weighted FMeasure: 0.6494992489431068
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5866719726746021
Mean absolute error: 0.17121467972531795
Coverage of cases: 79.90543735224587
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 58.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.1129008691549759
Kappa statistic: 0.5496884468498514
Training time: 9.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 30.141843971631207
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6732666560427452
Weighted AreaUnderROC: 0.794619205625064
Root mean squared error: 0.397296815558935
Relative absolute error: 45.922423605638734
Root relative squared error: 91.7517693644529
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5492466885802997
Weighted FMeasure: 0.6547513239422978
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5824286045693168
Mean absolute error: 0.17220908852114528
Coverage of cases: 73.04964539007092
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11481341670859797
Kappa statistic: 0.5431761306083536
Training time: 10.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 45.09456264775414
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6656352417756183
Weighted AreaUnderROC: 0.812552185724906
Root mean squared error: 0.38324216702867686
Relative absolute error: 48.05791042104956
Root relative squared error: 88.50598731952886
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5516847716171107
Weighted FMeasure: 0.6504560017166264
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6234739835076805
Mean absolute error: 0.18021716407893584
Coverage of cases: 83.92434988179669
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 81.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11254806240818922
Kappa statistic: 0.5525843382917073
Training time: 12.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 42.08037825059102
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6582372900799103
Weighted AreaUnderROC: 0.7785597096269733
Root mean squared error: 0.3885321952395321
Relative absolute error: 48.72680615391415
Root relative squared error: 89.72766700415204
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5441997213435452
Weighted FMeasure: 0.6521473038650738
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6021551256084273
Mean absolute error: 0.18272552307717807
Coverage of cases: 77.06855791962175
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 94.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11424175936588529
Kappa statistic: 0.5464414957780458
Training time: 13.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 36.938534278959814
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6558783602996564
Weighted AreaUnderROC: 0.7576205525881907
Root mean squared error: 0.39786463443495146
Relative absolute error: 49.21844833586372
Root relative squared error: 91.88290151682051
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5430898322350083
Weighted FMeasure: 0.6556918581268159
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5902549896457263
Mean absolute error: 0.18456918125948896
Coverage of cases: 71.63120567375887
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 108.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11451301255562191
Kappa statistic: 0.5461914411729645
Training time: 14.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 37.234042553191486
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6873351631616398
Weighted AreaUnderROC: 0.812477368775031
Root mean squared error: 0.36861228500672394
Relative absolute error: 44.02574548775826
Root relative squared error: 85.12736079009406
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.588902580115507
Weighted FMeasure: 0.6843187705427186
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6390063149439169
Mean absolute error: 0.16509654557909345
Coverage of cases: 79.90543735224587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 124.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10220330482648063
Kappa statistic: 0.5935812547015932
Training time: 15.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 35.22458628841608
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6780538763169788
Weighted AreaUnderROC: 0.8347516825782572
Root mean squared error: 0.3594921554139005
Relative absolute error: 42.55487076930546
Root relative squared error: 83.02115707990971
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5782974812496023
Weighted FMeasure: 0.6734584864847185
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6446479549251126
Mean absolute error: 0.1595807653848955
Coverage of cases: 82.97872340425532
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.104506674262991
Kappa statistic: 0.5841699186749679
Training time: 15.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 37.88416075650118
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6827097634669599
Weighted AreaUnderROC: 0.8148436177322343
Root mean squared error: 0.37245000241435056
Relative absolute error: 44.14357981061978
Root relative squared error: 86.01364366144084
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5722184585022284
Weighted FMeasure: 0.6767619575992755
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6285731002563193
Mean absolute error: 0.16553842428982418
Coverage of cases: 80.1418439716312
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 155.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10831087034761486
Kappa statistic: 0.5711026615969581
Training time: 16.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 31.501182033096928
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6788000467364387
Weighted AreaUnderROC: 0.8142533908841965
Root mean squared error: 0.3773929357601135
Relative absolute error: 44.29022776702996
Root relative squared error: 87.15516522054587
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5642653867517228
Weighted FMeasure: 0.6705382420885533
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6205061355078834
Mean absolute error: 0.16608835412636236
Coverage of cases: 78.01418439716312
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 173.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11068239416353091
Kappa statistic: 0.5615730487886719
Training time: 17.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 31.501182033096928
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6788000467364387
Weighted AreaUnderROC: 0.8142533908841965
Root mean squared error: 0.3774896381475471
Relative absolute error: 44.085290205614115
Root relative squared error: 87.1774976803123
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5642653867517228
Weighted FMeasure: 0.6705382420885533
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6205061355078834
Mean absolute error: 0.16531983827105293
Coverage of cases: 78.01418439716312
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 191.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11068239416353091
Kappa statistic: 0.5615730487886719
Training time: 18.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 31.501182033096928
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6774096933193061
Weighted AreaUnderROC: 0.8178560496732449
Root mean squared error: 0.37611088751372723
Relative absolute error: 43.36579179878916
Root relative squared error: 86.8590888604798
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5657464497832771
Weighted FMeasure: 0.6705484087526222
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6184586412319873
Mean absolute error: 0.16262171924545935
Coverage of cases: 78.25059101654847
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 209.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10986174698158817
Kappa statistic: 0.5647596537403351
Training time: 18.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 31.796690307328607
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.680348028645901
Weighted AreaUnderROC: 0.8204589102655347
Root mean squared error: 0.36928479229482225
Relative absolute error: 43.79789110285564
Root relative squared error: 85.28266969562027
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5752328853781892
Weighted FMeasure: 0.6712695607665343
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6262643461165082
Mean absolute error: 0.16424209163570866
Coverage of cases: 78.4869976359338
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 229.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10646647717339845
Kappa statistic: 0.5777310924369747
Training time: 19.0
		
Time end:Sun Oct 08 09.53.47 EEST 2017