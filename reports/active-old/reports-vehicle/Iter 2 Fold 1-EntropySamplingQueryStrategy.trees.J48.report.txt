Sun Oct 08 09.52.22 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.52.22 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 39.716312056737586
Incorrectly Classified Instances: 40.189125295508276
Correctly Classified Instances: 59.810874704491724
Weighted Precision: 0.5729007731997722
Weighted AreaUnderROC: 0.7294204349681244
Root mean squared error: 0.42964908849574696
Relative absolute error: 59.43599172913601
Root relative squared error: 99.22320676003875
Weighted TruePositiveRate: 0.5981087470449172
Weighted MatthewsCorrelation: 0.4515501971719286
Weighted FMeasure: 0.5748101356972297
Iteration time: 16.0
Weighted AreaUnderPRC: 0.48658625642854125
Mean absolute error: 0.22288496898426005
Coverage of cases: 70.2127659574468
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 16.0
Weighted Recall: 0.5981087470449172
Weighted FalsePositiveRate: 0.13538145133699717
Kappa statistic: 0.46367040081146793
Training time: 13.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 43.853427895981085
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.5927407904646896
Weighted AreaUnderROC: 0.762282352644969
Root mean squared error: 0.4242762660828297
Relative absolute error: 58.67301860209661
Root relative squared error: 97.98240657347641
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.44608671312345655
Weighted FMeasure: 0.5859584953378941
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5371870745616013
Mean absolute error: 0.22002381975786228
Coverage of cases: 79.66903073286052
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 45.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.14079150616548136
Kappa statistic: 0.4414404332937937
Training time: 23.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 36.05200945626478
Incorrectly Classified Instances: 45.62647754137116
Correctly Classified Instances: 54.37352245862884
Weighted Precision: 0.5551338467040292
Weighted AreaUnderROC: 0.7119983176593083
Root mean squared error: 0.4549388310306141
Relative absolute error: 62.14402568017093
Root relative squared error: 105.0636226241355
Weighted TruePositiveRate: 0.5437352245862884
Weighted MatthewsCorrelation: 0.39564990379742926
Weighted FMeasure: 0.5438644072641585
Iteration time: 42.0
Weighted AreaUnderPRC: 0.4906677029796816
Mean absolute error: 0.23304009630064096
Coverage of cases: 65.48463356973996
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 87.0
Weighted Recall: 0.5437352245862884
Weighted FalsePositiveRate: 0.1537576201467553
Kappa statistic: 0.3907491846954082
Training time: 35.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 40.8983451536643
Incorrectly Classified Instances: 38.77068557919622
Correctly Classified Instances: 61.22931442080378
Weighted Precision: 0.6018943953509219
Weighted AreaUnderROC: 0.7523435059982074
Root mean squared error: 0.41693416506956266
Relative absolute error: 54.250613491331016
Root relative squared error: 96.28682097490555
Weighted TruePositiveRate: 0.6122931442080378
Weighted MatthewsCorrelation: 0.4775242015460854
Weighted FMeasure: 0.5993092729772582
Iteration time: 45.0
Weighted AreaUnderPRC: 0.5248704050164753
Mean absolute error: 0.2034398005924913
Coverage of cases: 76.12293144208037
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 132.0
Weighted Recall: 0.6122931442080378
Weighted FalsePositiveRate: 0.13110402709516347
Kappa statistic: 0.48207046386095376
Training time: 38.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 38.002364066193856
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6397016341026922
Weighted AreaUnderROC: 0.759652851611046
Root mean squared error: 0.41299502326192866
Relative absolute error: 54.59486910077939
Root relative squared error: 95.37711514170012
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.49843261176824494
Weighted FMeasure: 0.6256439283994637
Iteration time: 45.0
Weighted AreaUnderPRC: 0.5536217423832928
Mean absolute error: 0.2047307591279227
Coverage of cases: 71.63120567375887
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 177.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12899172054509528
Kappa statistic: 0.48882937854793107
Training time: 36.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 32.38770685579196
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6757922952654687
Weighted AreaUnderROC: 0.8062440688710157
Root mean squared error: 0.38606057414947864
Relative absolute error: 44.95279031449249
Root relative squared error: 89.15687056348119
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5652944854013943
Weighted FMeasure: 0.6728088912159876
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5828371716973562
Mean absolute error: 0.16857296367934685
Coverage of cases: 77.06855791962175
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 195.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10974080301452611
Kappa statistic: 0.5645453663849372
Training time: 13.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 35.69739952718676
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6360509741081213
Weighted AreaUnderROC: 0.818581461766296
Root mean squared error: 0.3793979723689828
Relative absolute error: 46.09423300352748
Root relative squared error: 87.61820859089217
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5325604857045484
Weighted FMeasure: 0.6407123174691329
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6201102363669608
Mean absolute error: 0.17285337376322804
Coverage of cases: 82.97872340425532
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 206.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.11462641150913491
Kappa statistic: 0.5431659230901006
Training time: 10.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 36.7612293144208
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6426097714612891
Weighted AreaUnderROC: 0.83432284073136
Root mean squared error: 0.380961483181541
Relative absolute error: 47.33968240466061
Root relative squared error: 87.97928594629671
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5250584447651834
Weighted FMeasure: 0.6422428265602559
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6287901049541542
Mean absolute error: 0.17752380901747727
Coverage of cases: 86.05200945626477
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 222.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11855952892435018
Kappa statistic: 0.527469335776045
Training time: 15.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 35.1063829787234
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6530260827717635
Weighted AreaUnderROC: 0.8400874222461966
Root mean squared error: 0.3726446587292182
Relative absolute error: 44.716458352718206
Root relative squared error: 86.05859761175614
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.546294568404709
Weighted FMeasure: 0.6454340925468272
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6504890932131959
Mean absolute error: 0.1676867188226933
Coverage of cases: 83.68794326241135
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 252.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11186521517659846
Kappa statistic: 0.5528441364113482
Training time: 24.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 36.170212765957444
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6708285962253618
Weighted AreaUnderROC: 0.8341626564303882
Root mean squared error: 0.3536014386616845
Relative absolute error: 43.753796157195644
Root relative squared error: 81.660754318865
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5747603620674139
Weighted FMeasure: 0.6641171841899475
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6420386176083015
Mean absolute error: 0.16407673558948366
Coverage of cases: 84.86997635933807
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 278.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10421830232667678
Kappa statistic: 0.5841699186749679
Training time: 22.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 35.46099290780142
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6826166703243484
Weighted AreaUnderROC: 0.8298864322646863
Root mean squared error: 0.3564633567117045
Relative absolute error: 42.61711041058072
Root relative squared error: 82.32168598149607
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.591220002230151
Weighted FMeasure: 0.6845808366528382
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6408739676255631
Mean absolute error: 0.15981416403967771
Coverage of cases: 83.92434988179669
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 304.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10040596592826077
Kappa statistic: 0.599807804049524
Training time: 22.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 33.096926713947994
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6785645787863538
Weighted AreaUnderROC: 0.8310461909529188
Root mean squared error: 0.355376301109889
Relative absolute error: 40.77085856398121
Root relative squared error: 82.07064124376318
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5915190151263607
Weighted FMeasure: 0.6842710792215245
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6511809809969863
Mean absolute error: 0.15289071961492953
Coverage of cases: 81.7966903073286
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 320.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09967722470226943
Kappa statistic: 0.6030062418996828
Training time: 15.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 35.520094562647756
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6617917050555042
Weighted AreaUnderROC: 0.8437364308142479
Root mean squared error: 0.35915114865257475
Relative absolute error: 43.03415607930188
Root relative squared error: 82.94240494173093
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5715702015211824
Weighted FMeasure: 0.6691754870659021
Iteration time: 18.0
Weighted AreaUnderPRC: 0.649152142275287
Mean absolute error: 0.16137808529738204
Coverage of cases: 84.16075650118204
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 338.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10438319070195785
Kappa statistic: 0.5840584028605482
Training time: 17.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 33.628841607565015
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6771534977375427
Weighted AreaUnderROC: 0.8200170011042226
Root mean squared error: 0.3583680010724513
Relative absolute error: 43.3001426138315
Root relative squared error: 82.76154475525115
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5823645374457376
Weighted FMeasure: 0.6801776151925968
Iteration time: 19.0
Weighted AreaUnderPRC: 0.620897845582674
Mean absolute error: 0.16237553480186812
Coverage of cases: 82.03309692671395
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 357.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10376295408384845
Kappa statistic: 0.5899573478092284
Training time: 18.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.328605200945624
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6871670237694174
Weighted AreaUnderROC: 0.8200135124336765
Root mean squared error: 0.3586910595205334
Relative absolute error: 42.2961455408265
Root relative squared error: 82.83615190803681
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5930148829733645
Weighted FMeasure: 0.6906480533728675
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6181381213172482
Mean absolute error: 0.15861054577809938
Coverage of cases: 81.32387706855792
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 377.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10147997705125548
Kappa statistic: 0.599416883533298
Training time: 20.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 32.860520094562645
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6662628620947422
Weighted AreaUnderROC: 0.8210313931493946
Root mean squared error: 0.3601462186731588
Relative absolute error: 44.15257624023298
Root relative squared error: 83.17220651942964
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5736813755482015
Weighted FMeasure: 0.6735362517719152
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6226441238284697
Mean absolute error: 0.1655721609008737
Coverage of cases: 81.7966903073286
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 398.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10434107031087167
Kappa statistic: 0.5840119202831067
Training time: 20.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 35.815602836879435
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6722794604458869
Weighted AreaUnderROC: 0.8157400720072812
Root mean squared error: 0.356719862952689
Relative absolute error: 43.577816510191745
Root relative squared error: 82.38092356040858
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.582585359322694
Weighted FMeasure: 0.6796182536682297
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6132606810578721
Mean absolute error: 0.16341681191321905
Coverage of cases: 82.03309692671395
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 420.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.1020667152534325
Kappa statistic: 0.5934510505140813
Training time: 22.0
		
Time end:Sun Oct 08 09.52.24 EEST 2017