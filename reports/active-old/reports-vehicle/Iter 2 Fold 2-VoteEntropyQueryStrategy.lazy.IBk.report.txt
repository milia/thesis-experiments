Sun Oct 08 09.56.11 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.11 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 41.843971631205676
Correctly Classified Instances: 58.156028368794324
Weighted Precision: 0.5697090154706326
Weighted AreaUnderROC: 0.7201966604230061
Root mean squared error: 0.44732232371936864
Relative absolute error: 57.80141843971625
Root relative squared error: 103.30466560556258
Weighted TruePositiveRate: 0.5815602836879432
Weighted MatthewsCorrelation: 0.434697668444269
Weighted FMeasure: 0.5618083772270441
Iteration time: 11.0
Weighted AreaUnderPRC: 0.4628630798339099
Mean absolute error: 0.21675531914893595
Coverage of cases: 58.156028368794324
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5815602836879432
Weighted FalsePositiveRate: 0.14116696284193098
Kappa statistic: 0.4414362662449082
Training time: 1.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6560531053973118
Weighted AreaUnderROC: 0.7681802272176467
Root mean squared error: 0.4147137138669091
Relative absolute error: 46.88893763252192
Root relative squared error: 95.77402973507577
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5373731360355114
Weighted FMeasure: 0.6470849149146001
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5511294151019727
Mean absolute error: 0.1758335161219572
Coverage of cases: 65.2482269503546
Instances selection time: 4.0
Test time: 42.0
Accumulative iteration time: 16.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11612181506825256
Kappa statistic: 0.5369130515732639
Training time: 1.0
		
Time end:Sun Oct 08 09.56.11 EEST 2017