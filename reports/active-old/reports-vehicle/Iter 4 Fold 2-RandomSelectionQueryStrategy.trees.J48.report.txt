Sun Oct 08 09.55.51 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.55.51 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 40.42553191489362
Incorrectly Classified Instances: 42.78959810874704
Correctly Classified Instances: 57.21040189125296
Weighted Precision: 0.5926473083379742
Weighted AreaUnderROC: 0.7700196234273593
Root mean squared error: 0.4299704746075032
Relative absolute error: 57.93554138707799
Root relative squared error: 99.29742770329327
Weighted TruePositiveRate: 0.5721040189125296
Weighted MatthewsCorrelation: 0.43823919033837966
Weighted FMeasure: 0.5455225440877971
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5149030555114262
Mean absolute error: 0.21725828020154245
Coverage of cases: 78.25059101654847
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5721040189125296
Weighted FalsePositiveRate: 0.1437107680193289
Kappa statistic: 0.42946033354695445
Training time: 3.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 32.91962174940898
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.663149174992053
Weighted AreaUnderROC: 0.8263108202982153
Root mean squared error: 0.37130486913063654
Relative absolute error: 45.594956658786366
Root relative squared error: 85.74918645759672
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.53431434903342
Weighted FMeasure: 0.636353106584725
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6187663107777382
Mean absolute error: 0.17098108747044888
Coverage of cases: 79.90543735224587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11855498169866295
Kappa statistic: 0.5277682678138165
Training time: 4.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 39.30260047281324
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.6171172102164382
Weighted AreaUnderROC: 0.8061857879542177
Root mean squared error: 0.3664431488395389
Relative absolute error: 50.1170046492929
Root relative squared error: 84.62642025008076
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.51042337993488
Weighted FMeasure: 0.613422747524046
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5859332887917552
Mean absolute error: 0.18793876743484839
Coverage of cases: 83.68794326241135
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12057369876779235
Kappa statistic: 0.5209869920879711
Training time: 5.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 37.94326241134752
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6460121749928933
Weighted AreaUnderROC: 0.7906539247279587
Root mean squared error: 0.37455538745514527
Relative absolute error: 51.16283768766035
Root relative squared error: 86.4998615094611
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5142198391514555
Weighted FMeasure: 0.6183430498173091
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5665374542561706
Mean absolute error: 0.1918606413287263
Coverage of cases: 81.32387706855792
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12428747712250404
Kappa statistic: 0.5078277083721797
Training time: 6.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 31.796690307328607
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6385261897166297
Weighted AreaUnderROC: 0.782415304558269
Root mean squared error: 0.400123227146988
Relative absolute error: 49.59732429069109
Root relative squared error: 92.40450116093412
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5099907217240369
Weighted FMeasure: 0.6328096657879198
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5795481023232651
Mean absolute error: 0.18598996609009158
Coverage of cases: 73.04964539007092
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12504746977001702
Kappa statistic: 0.507416226868613
Training time: 9.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 40.307328605200944
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6633607060392485
Weighted AreaUnderROC: 0.8130517079324716
Root mean squared error: 0.3677544429062611
Relative absolute error: 48.2546050394751
Root relative squared error: 84.92925064304427
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5487317715171363
Weighted FMeasure: 0.6572574769819681
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5991241376325097
Mean absolute error: 0.18095476889803164
Coverage of cases: 82.26950354609929
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11420336818196204
Kappa statistic: 0.5486468134639634
Training time: 10.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 39.479905437352244
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6619214473582149
Weighted AreaUnderROC: 0.8168398823273912
Root mean squared error: 0.3660462546504949
Relative absolute error: 49.12833760351494
Root relative squared error: 84.53476146332702
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5329276031160823
Weighted FMeasure: 0.6512129796711241
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6219000799107423
Mean absolute error: 0.18423126601318102
Coverage of cases: 83.68794326241135
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 52.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.12032017392378003
Kappa statistic: 0.5264925373134329
Training time: 10.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 41.78486997635934
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6751512799915067
Weighted AreaUnderROC: 0.8429900464050167
Root mean squared error: 0.3509920490405195
Relative absolute error: 46.188241755617646
Root relative squared error: 81.05814159878491
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5643693921317374
Weighted FMeasure: 0.6728578121068153
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6495539611951077
Mean absolute error: 0.1732059065835662
Coverage of cases: 87.47044917257683
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.11041864081877029
Kappa statistic: 0.5644543928371571
Training time: 10.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.333333333333336
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6904776732278746
Weighted AreaUnderROC: 0.8223983373949108
Root mean squared error: 0.36596361606799255
Relative absolute error: 43.00442793350597
Root relative squared error: 84.51567690018575
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5858736260491293
Weighted FMeasure: 0.6888423898966238
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6277991768762449
Mean absolute error: 0.16126660475064739
Coverage of cases: 81.32387706855792
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10504502168550353
Kappa statistic: 0.5864917504309476
Training time: 14.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 40.839243498817964
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6987545351736701
Weighted AreaUnderROC: 0.8510998858827835
Root mean squared error: 0.34328073784075747
Relative absolute error: 43.11813901352614
Root relative squared error: 79.27729055998988
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5939958313971587
Weighted FMeasure: 0.6964735635740132
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6873169053100485
Mean absolute error: 0.16169302130072302
Coverage of cases: 87.70685579196217
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.1028742440719676
Kappa statistic: 0.5930326218284333
Training time: 12.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 38.356973995271865
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6795870083697892
Weighted AreaUnderROC: 0.8140451493816678
Root mean squared error: 0.3669744748220783
Relative absolute error: 46.66054455915389
Root relative squared error: 84.74912472969937
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.557606998665088
Weighted FMeasure: 0.6642699399806004
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6187920960304615
Mean absolute error: 0.17497704209682707
Coverage of cases: 82.26950354609929
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11436565722711688
Kappa statistic: 0.5514349511227942
Training time: 14.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 39.53900709219858
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6673001524437865
Weighted AreaUnderROC: 0.8297201934390934
Root mean squared error: 0.35301477257409714
Relative absolute error: 46.69220522095361
Root relative squared error: 81.52526958942781
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5612141961236747
Weighted FMeasure: 0.6616358012375095
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6331445598008869
Mean absolute error: 0.17509576957857603
Coverage of cases: 84.63356973995272
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10980146849498003
Kappa statistic: 0.5649866978664421
Training time: 14.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 40.248226950354606
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6645539085829333
Weighted AreaUnderROC: 0.8264470808965164
Root mean squared error: 0.35714387338346476
Relative absolute error: 47.051438456148084
Root relative squared error: 82.4788445749476
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5561061554530593
Weighted FMeasure: 0.6453973908386438
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6147581825683847
Mean absolute error: 0.1764428942105553
Coverage of cases: 86.7612293144208
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 134.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11112698296811596
Kappa statistic: 0.5588531074708919
Training time: 15.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 41.371158392434985
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6924672614866337
Weighted AreaUnderROC: 0.8535836837238446
Root mean squared error: 0.3373093434640716
Relative absolute error: 44.21488329165843
Root relative squared error: 77.89825609966307
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5864241060309396
Weighted FMeasure: 0.6843744203893466
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6654172135909141
Mean absolute error: 0.16580581234371913
Coverage of cases: 89.36170212765957
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 151.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.1054368318484564
Kappa statistic: 0.5864022451447252
Training time: 17.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 42.494089834515364
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6881544974332766
Weighted AreaUnderROC: 0.8635849381138203
Root mean squared error: 0.3354562613246508
Relative absolute error: 44.77454633259044
Root relative squared error: 77.47030511085305
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5866322992670074
Weighted FMeasure: 0.6805605816216587
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6752790874487318
Mean absolute error: 0.16790454874721417
Coverage of cases: 91.01654846335697
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 168.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.1038442231425445
Kappa statistic: 0.5898441870352276
Training time: 17.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 40.8983451536643
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6900876967807402
Weighted AreaUnderROC: 0.8379078596420779
Root mean squared error: 0.3585804088691889
Relative absolute error: 43.74979028672167
Root relative squared error: 82.81059823470092
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5923682949193863
Weighted FMeasure: 0.693344220688452
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6487918865645927
Mean absolute error: 0.16406171357520627
Coverage of cases: 85.1063829787234
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 190.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.1016905616424899
Kappa statistic: 0.5964161660132083
Training time: 21.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 38.47517730496454
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6749385746903478
Weighted AreaUnderROC: 0.8418712586140908
Root mean squared error: 0.34758794355537403
Relative absolute error: 46.46376015705605
Root relative squared error: 80.27199711150547
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5676500400040119
Weighted FMeasure: 0.6487676764655029
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6368533065692542
Mean absolute error: 0.1742391005889602
Coverage of cases: 87.47044917257683
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 209.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10733717434120103
Kappa statistic: 0.57170935080405
Training time: 18.0
		
Time end:Sun Oct 08 09.55.51 EEST 2017