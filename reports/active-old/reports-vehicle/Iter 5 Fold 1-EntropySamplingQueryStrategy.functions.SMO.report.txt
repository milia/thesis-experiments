Sun Oct 08 09.51.57 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.51.57 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.06382978723404
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6424808984868988
Weighted AreaUnderROC: 0.8179224824023698
Root mean squared error: 0.3708232062261038
Relative absolute error: 77.69897557131587
Root relative squared error: 85.63795117456047
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5288777501228472
Weighted FMeasure: 0.6086038327071875
Iteration time: 67.0
Weighted AreaUnderPRC: 0.580679356227237
Mean absolute error: 0.2913711583924345
Coverage of cases: 96.21749408983452
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11580193790082363
Kappa statistic: 0.5400633029231054
Training time: 65.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.5968615247160821
Weighted AreaUnderROC: 0.8041986860421385
Root mean squared error: 0.38370403838474304
Relative absolute error: 80.01050696086153
Root relative squared error: 88.61265194023116
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.48691629039200546
Weighted FMeasure: 0.575979378363938
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5396656106842984
Mean absolute error: 0.30003940110323074
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 120.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.1242580254991147
Kappa statistic: 0.5054878775242747
Training time: 52.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.5705557237069278
Weighted AreaUnderROC: 0.7844746476253729
Root mean squared error: 0.3867723558717892
Relative absolute error: 80.74599422117143
Root relative squared error: 89.32124951107329
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.4439876657370278
Weighted FMeasure: 0.5471525021544403
Iteration time: 101.0
Weighted AreaUnderPRC: 0.49487491387635096
Mean absolute error: 0.30279747832939286
Coverage of cases: 95.0354609929078
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 221.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.13446486907013105
Kappa statistic: 0.4563910947354346
Training time: 93.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 44.680851063829785
Correctly Classified Instances: 55.319148936170215
Weighted Precision: 0.557612475069653
Weighted AreaUnderROC: 0.7716838828047426
Root mean squared error: 0.3907419545169171
Relative absolute error: 81.53401628578918
Root relative squared error: 90.23798904960906
Weighted TruePositiveRate: 0.5531914893617021
Weighted MatthewsCorrelation: 0.4079331826266815
Weighted FMeasure: 0.5202108362282332
Iteration time: 56.0
Weighted AreaUnderPRC: 0.46734733908955955
Mean absolute error: 0.30575256107170945
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 277.0
Weighted Recall: 0.5531914893617021
Weighted FalsePositiveRate: 0.14558755373411766
Kappa statistic: 0.40705332641103614
Training time: 55.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 46.335697399527184
Correctly Classified Instances: 53.664302600472816
Weighted Precision: 0.5470670231398059
Weighted AreaUnderROC: 0.7647752528444551
Root mean squared error: 0.3921678717223562
Relative absolute error: 81.79669030732848
Root relative squared error: 90.56729052256999
Weighted TruePositiveRate: 0.5366430260047281
Weighted MatthewsCorrelation: 0.38869879925080225
Weighted FMeasure: 0.4996717081391747
Iteration time: 56.0
Weighted AreaUnderPRC: 0.45643058461236824
Mean absolute error: 0.3067375886524818
Coverage of cases: 95.74468085106383
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 333.0
Weighted Recall: 0.5366430260047281
Weighted FalsePositiveRate: 0.1505034928323055
Kappa statistic: 0.38548430134305794
Training time: 55.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 49.40898345153664
Correctly Classified Instances: 50.59101654846336
Weighted Precision: 0.5144967294394173
Weighted AreaUnderROC: 0.7561955604168062
Root mean squared error: 0.3957516713076391
Relative absolute error: 82.53217756763844
Root relative squared error: 91.3949335846839
Weighted TruePositiveRate: 0.5059101654846335
Weighted MatthewsCorrelation: 0.3456565868232816
Weighted FMeasure: 0.4524682576423475
Iteration time: 59.0
Weighted AreaUnderPRC: 0.438975699129721
Mean absolute error: 0.3094956658786442
Coverage of cases: 95.50827423167848
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 392.0
Weighted Recall: 0.5059101654846335
Weighted FalsePositiveRate: 0.15990656669299927
Kappa statistic: 0.3453273104265402
Training time: 57.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 49.1725768321513
Correctly Classified Instances: 50.8274231678487
Weighted Precision: 0.41336196055893976
Weighted AreaUnderROC: 0.7677518998247619
Root mean squared error: 0.3979442181204387
Relative absolute error: 83.00499080640914
Root relative squared error: 91.90128058171616
Weighted TruePositiveRate: 0.508274231678487
Weighted MatthewsCorrelation: 0.33767150359991743
Weighted FMeasure: 0.4272528864155494
Iteration time: 56.0
Weighted AreaUnderPRC: 0.45542271519971256
Mean absolute error: 0.3112687155240343
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 448.0
Weighted Recall: 0.508274231678487
Weighted FalsePositiveRate: 0.15843522002669908
Kappa statistic: 0.34899001109877903
Training time: 55.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.0
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.4453773561194423
Weighted AreaUnderROC: 0.7521090016413998
Root mean squared error: 0.4056259856587839
Relative absolute error: 84.6335697399527
Root relative squared error: 93.67530880416247
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.31246430977201145
Weighted FMeasure: 0.3742690011833577
Iteration time: 75.0
Weighted AreaUnderPRC: 0.44306977174237094
Mean absolute error: 0.3173758865248226
Coverage of cases: 94.79905437352245
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 523.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.16931309777624548
Kappa statistic: 0.30564983030545023
Training time: 73.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.0
Incorrectly Classified Instances: 53.664302600472816
Correctly Classified Instances: 46.335697399527184
Weighted Precision: 0.4433916642500285
Weighted AreaUnderROC: 0.7394815264040094
Root mean squared error: 0.410374075764252
Relative absolute error: 85.68426582610981
Root relative squared error: 94.77183324437392
Weighted TruePositiveRate: 0.46335697399527187
Weighted MatthewsCorrelation: 0.291288187189443
Weighted FMeasure: 0.34422224281311464
Iteration time: 63.0
Weighted AreaUnderPRC: 0.43121034604376124
Mean absolute error: 0.32131599684791173
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 586.0
Weighted Recall: 0.46335697399527187
Weighted FalsePositiveRate: 0.17464187183319016
Kappa statistic: 0.28944388944388943
Training time: 62.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 50.8274231678487
Correctly Classified Instances: 49.1725768321513
Weighted Precision: 0.4491506873303564
Weighted AreaUnderROC: 0.7617677257346515
Root mean squared error: 0.4032309751653606
Relative absolute error: 84.16075650118198
Root relative squared error: 93.1222048229265
Weighted TruePositiveRate: 0.491725768321513
Weighted MatthewsCorrelation: 0.32882094992399025
Weighted FMeasure: 0.38805472852107237
Iteration time: 65.0
Weighted AreaUnderPRC: 0.45737984448775765
Mean absolute error: 0.3156028368794324
Coverage of cases: 96.21749408983452
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 651.0
Weighted Recall: 0.491725768321513
Weighted FalsePositiveRate: 0.16527465395642804
Kappa statistic: 0.326782145236509
Training time: 63.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 52.95508274231678
Correctly Classified Instances: 47.04491725768322
Weighted Precision: 0.4503569773773408
Weighted AreaUnderROC: 0.7667197478050801
Root mean squared error: 0.40294587671790466
Relative absolute error: 84.10822169687411
Root relative squared error: 93.05636415677282
Weighted TruePositiveRate: 0.47044917257683216
Weighted MatthewsCorrelation: 0.30142995018283397
Weighted FMeasure: 0.35262414742313436
Iteration time: 62.0
Weighted AreaUnderPRC: 0.46255580669526203
Mean absolute error: 0.31540583136327793
Coverage of cases: 97.39952718676123
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 713.0
Weighted Recall: 0.47044917257683216
Weighted FalsePositiveRate: 0.17262194141903792
Kappa statistic: 0.2986320838514834
Training time: 61.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 54.37352245862884
Correctly Classified Instances: 45.62647754137116
Weighted Precision: 0.42507580728299443
Weighted AreaUnderROC: 0.7454419486310381
Root mean squared error: 0.408489500700839
Relative absolute error: 85.31652219595479
Root relative squared error: 94.33660927630609
Weighted TruePositiveRate: 0.4562647754137116
Weighted MatthewsCorrelation: 0.27393910323306925
Weighted FMeasure: 0.3269037659134713
Iteration time: 67.0
Weighted AreaUnderPRC: 0.43328338136660954
Mean absolute error: 0.3199369582348305
Coverage of cases: 96.92671394799055
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 780.0
Weighted Recall: 0.4562647754137116
Weighted FalsePositiveRate: 0.17736498905087006
Kappa statistic: 0.279941382832275
Training time: 66.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 52.4822695035461
Correctly Classified Instances: 47.5177304964539
Weighted Precision: 0.5757017472351295
Weighted AreaUnderROC: 0.7062826876250736
Root mean squared error: 0.41518649621001125
Relative absolute error: 86.84003152088258
Root relative squared error: 95.88321414029902
Weighted TruePositiveRate: 0.475177304964539
Weighted MatthewsCorrelation: 0.321111671724215
Weighted FMeasure: 0.38657598636769686
Iteration time: 69.0
Weighted AreaUnderPRC: 0.40827926177690843
Mean absolute error: 0.3256501182033097
Coverage of cases: 90.30732860520095
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 849.0
Weighted Recall: 0.475177304964539
Weighted FalsePositiveRate: 0.17133987294738728
Kappa statistic: 0.3046626829864274
Training time: 68.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 48.69976359338062
Correctly Classified Instances: 51.30023640661938
Weighted Precision: 0.5679093293567526
Weighted AreaUnderROC: 0.7082928281504651
Root mean squared error: 0.41185161090978295
Relative absolute error: 86.10454426057271
Root relative squared error: 95.11305536997769
Weighted TruePositiveRate: 0.5130023640661938
Weighted MatthewsCorrelation: 0.35258857530554605
Weighted FMeasure: 0.44884958640759787
Iteration time: 64.0
Weighted AreaUnderPRC: 0.4155986760779524
Mean absolute error: 0.32289204097714763
Coverage of cases: 89.83451536643027
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 913.0
Weighted Recall: 0.5130023640661938
Weighted FalsePositiveRate: 0.1600077098043415
Kappa statistic: 0.3536907375541446
Training time: 63.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.6552549097734776
Weighted AreaUnderROC: 0.8236296581198904
Root mean squared error: 0.3723695061629535
Relative absolute error: 77.75151037562382
Root relative squared error: 85.99505384847569
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.5205072128626133
Weighted FMeasure: 0.621698875057385
Iteration time: 67.0
Weighted AreaUnderPRC: 0.5627629755247634
Mean absolute error: 0.2915681639085893
Coverage of cases: 98.10874704491725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 980.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.12085741463163313
Kappa statistic: 0.5126655666057174
Training time: 66.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.7006370271375357
Weighted AreaUnderROC: 0.8494548362203557
Root mean squared error: 0.36113384180141195
Relative absolute error: 75.59758339900182
Root relative squared error: 83.40028831101158
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5992681558628464
Weighted FMeasure: 0.6866546375518295
Iteration time: 78.0
Weighted AreaUnderPRC: 0.6212906345727275
Mean absolute error: 0.28349093774625683
Coverage of cases: 98.3451536643026
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1058.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.09952225104599022
Kappa statistic: 0.6001949869016433
Training time: 77.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7119689457605046
Weighted AreaUnderROC: 0.8525209499825872
Root mean squared error: 0.35958488163812635
Relative absolute error: 75.28237457315471
Root relative squared error: 83.04257128411679
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6182706405763488
Weighted FMeasure: 0.6977613282751813
Iteration time: 70.0
Weighted AreaUnderPRC: 0.6251723478634837
Mean absolute error: 0.2823089046493302
Coverage of cases: 97.87234042553192
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1128.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09437417001376552
Kappa statistic: 0.6220542794385913
Training time: 69.0
		
Time end:Sun Oct 08 09.51.59 EEST 2017