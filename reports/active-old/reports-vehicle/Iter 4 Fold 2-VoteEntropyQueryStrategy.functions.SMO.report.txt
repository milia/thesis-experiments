Sun Oct 08 09.56.10 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.10 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6589549784920528
Weighted AreaUnderROC: 0.830038286427116
Root mean squared error: 0.37483031797176825
Relative absolute error: 78.38192802731785
Root relative squared error: 86.56335399257337
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.4922773069230399
Weighted FMeasure: 0.5525747715344637
Iteration time: 50.0
Weighted AreaUnderPRC: 0.5866637214712648
Mean absolute error: 0.29393223010244196
Coverage of cases: 95.27186761229315
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12363743689963734
Kappa statistic: 0.508758347043453
Training time: 49.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.7329620542982361
Weighted AreaUnderROC: 0.8529967558341234
Root mean squared error: 0.36663801019905323
Relative absolute error: 76.64827948515877
Root relative squared error: 84.67142155342886
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5823925448905346
Weighted FMeasure: 0.6312621598623023
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6355814733601765
Mean absolute error: 0.2874310480693454
Coverage of cases: 97.39952718676123
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 115.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10502357640394233
Kappa statistic: 0.5787165727706509
Training time: 65.0
		
Time end:Sun Oct 08 09.56.10 EEST 2017