Sun Oct 08 09.53.09 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.53.09 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.67375886524823
Incorrectly Classified Instances: 38.65248226950354
Correctly Classified Instances: 61.34751773049645
Weighted Precision: 0.6107823062685481
Weighted AreaUnderROC: 0.7986829535108257
Root mean squared error: 0.38176243951998423
Relative absolute error: 79.83188862621475
Root relative squared error: 88.16425888934046
Weighted TruePositiveRate: 0.6134751773049645
Weighted MatthewsCorrelation: 0.4782563840479435
Weighted FMeasure: 0.5735354175625933
Iteration time: 62.3
Weighted AreaUnderPRC: 0.5411352830752627
Mean absolute error: 0.2993695823483053
Coverage of cases: 94.822695035461
Instances selection time: 2.7
Test time: 3.2
Accumulative iteration time: 62.3
Weighted Recall: 0.6134751773049645
Weighted FalsePositiveRate: 0.12883803988227138
Kappa statistic: 0.48535561698184376
Training time: 59.6
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.7033096926714
Incorrectly Classified Instances: 37.56501182033097
Correctly Classified Instances: 62.43498817966902
Weighted Precision: 0.6055921221195506
Weighted AreaUnderROC: 0.8075284421335478
Root mean squared error: 0.37811169818885626
Relative absolute error: 79.0858944050432
Root relative squared error: 87.32115629323309
Weighted TruePositiveRate: 0.6243498817966904
Weighted MatthewsCorrelation: 0.49319560328741663
Weighted FMeasure: 0.5903686174698606
Iteration time: 50.3
Weighted AreaUnderPRC: 0.5552233774969904
Mean absolute error: 0.29657210401891204
Coverage of cases: 95.69739952718676
Instances selection time: 1.2
Test time: 2.3
Accumulative iteration time: 112.6
Weighted Recall: 0.6243498817966904
Weighted FalsePositiveRate: 0.1251484007679785
Kappa statistic: 0.5000854143568201
Training time: 49.1
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.66193853427896
Incorrectly Classified Instances: 39.90543735224587
Correctly Classified Instances: 60.09456264775414
Weighted Precision: 0.6011479404082569
Weighted AreaUnderROC: 0.8107754976704358
Root mean squared error: 0.3792905540904146
Relative absolute error: 79.32755450485931
Root relative squared error: 87.59340140207328
Weighted TruePositiveRate: 0.6009456264775415
Weighted MatthewsCorrelation: 0.46854529208719053
Weighted FMeasure: 0.5543671545636084
Iteration time: 50.0
Weighted AreaUnderPRC: 0.5519374957133769
Mean absolute error: 0.2974783293932225
Coverage of cases: 95.91016548463358
Instances selection time: 1.1
Test time: 2.1
Accumulative iteration time: 162.6
Weighted Recall: 0.6009456264775415
Weighted FalsePositiveRate: 0.13254601923442316
Kappa statistic: 0.46912279010108265
Training time: 48.9
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.5614657210402
Incorrectly Classified Instances: 41.205673758865245
Correctly Classified Instances: 58.794326241134755
Weighted Precision: 0.5893178323081657
Weighted AreaUnderROC: 0.8037619072020655
Root mean squared error: 0.38269500738575185
Relative absolute error: 79.96322563698438
Root relative squared error: 88.37962621267586
Weighted TruePositiveRate: 0.5879432624113474
Weighted MatthewsCorrelation: 0.4563979832747845
Weighted FMeasure: 0.5419806148921317
Iteration time: 53.4
Weighted AreaUnderPRC: 0.5420271543464248
Mean absolute error: 0.2998620961386914
Coverage of cases: 95.34278959810875
Instances selection time: 1.1
Test time: 2.2
Accumulative iteration time: 216.0
Weighted Recall: 0.5879432624113474
Weighted FalsePositiveRate: 0.1366689446166494
Kappa statistic: 0.45205181646469805
Training time: 52.3
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.47281323877068
Incorrectly Classified Instances: 42.03309692671395
Correctly Classified Instances: 57.96690307328604
Weighted Precision: 0.6020827448629547
Weighted AreaUnderROC: 0.80178675577116
Root mean squared error: 0.3848064910646043
Relative absolute error: 80.39926451273956
Root relative squared error: 88.86725248082587
Weighted TruePositiveRate: 0.5796690307328605
Weighted MatthewsCorrelation: 0.448907839973295
Weighted FMeasure: 0.5254227151735973
Iteration time: 54.4
Weighted AreaUnderPRC: 0.5446589267448424
Mean absolute error: 0.30149724192277333
Coverage of cases: 94.91725768321513
Instances selection time: 1.3
Test time: 2.7
Accumulative iteration time: 270.4
Weighted Recall: 0.5796690307328605
Weighted FalsePositiveRate: 0.13975680085919062
Kappa statistic: 0.4410294121845822
Training time: 53.1
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.32505910165484
Incorrectly Classified Instances: 42.198581560283685
Correctly Classified Instances: 57.8014184397163
Weighted Precision: 0.6122011664794207
Weighted AreaUnderROC: 0.8050135459983586
Root mean squared error: 0.3847777652682293
Relative absolute error: 80.36249014972408
Root relative squared error: 88.86061854231794
Weighted TruePositiveRate: 0.5780141843971632
Weighted MatthewsCorrelation: 0.45243929891664336
Weighted FMeasure: 0.5246285613583814
Iteration time: 60.0
Weighted AreaUnderPRC: 0.5480787564447273
Mean absolute error: 0.3013593380614653
Coverage of cases: 95.2482269503546
Instances selection time: 1.2
Test time: 2.6
Accumulative iteration time: 330.4
Weighted Recall: 0.5780141843971632
Weighted FalsePositiveRate: 0.14072940834532546
Kappa statistic: 0.43837956549010115
Training time: 58.8
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.34278959810875
Incorrectly Classified Instances: 44.42080378250591
Correctly Classified Instances: 55.5791962174941
Weighted Precision: 0.5181748343362076
Weighted AreaUnderROC: 0.8036261084324771
Root mean squared error: 0.3885093598708927
Relative absolute error: 81.1452587339111
Root relative squared error: 89.72239340165964
Weighted TruePositiveRate: 0.555791962174941
Weighted MatthewsCorrelation: 0.4111754108358764
Weighted FMeasure: 0.4775450451357037
Iteration time: 59.0
Weighted AreaUnderPRC: 0.5442397616584925
Mean absolute error: 0.3042947202521667
Coverage of cases: 95.08274231678487
Instances selection time: 1.1
Test time: 2.1
Accumulative iteration time: 389.4
Weighted Recall: 0.555791962174941
Weighted FalsePositiveRate: 0.14794784761891228
Kappa statistic: 0.40891708103063806
Training time: 57.9
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.30732860520094
Incorrectly Classified Instances: 43.1678486997636
Correctly Classified Instances: 56.83215130023641
Weighted Precision: 0.49426932590196626
Weighted AreaUnderROC: 0.7975596674938094
Root mean squared error: 0.3908150110201437
Relative absolute error: 81.6233254531126
Root relative squared error: 90.25486072633063
Weighted TruePositiveRate: 0.5683215130023641
Weighted MatthewsCorrelation: 0.41661013408588676
Weighted FMeasure: 0.480456036663133
Iteration time: 57.9
Weighted AreaUnderPRC: 0.5316652069960105
Mean absolute error: 0.3060874704491722
Coverage of cases: 94.79905437352245
Instances selection time: 1.3
Test time: 2.5
Accumulative iteration time: 447.3
Weighted Recall: 0.5683215130023641
Weighted FalsePositiveRate: 0.1428853024639369
Kappa statistic: 0.42660835334634023
Training time: 56.6
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.40780141843972
Incorrectly Classified Instances: 43.64066193853428
Correctly Classified Instances: 56.359338061465714
Weighted Precision: 0.4736349800627182
Weighted AreaUnderROC: 0.7967939633120811
Root mean squared error: 0.39167836864910427
Relative absolute error: 81.81245074862088
Root relative squared error: 90.45424463012554
Weighted TruePositiveRate: 0.5635933806146574
Weighted MatthewsCorrelation: 0.4059381922343205
Weighted FMeasure: 0.4712157289428666
Iteration time: 62.6
Weighted AreaUnderPRC: 0.5286258084154817
Mean absolute error: 0.3067966903073282
Coverage of cases: 94.65721040189126
Instances selection time: 1.4
Test time: 2.3
Accumulative iteration time: 509.9
Weighted Recall: 0.5635933806146574
Weighted FalsePositiveRate: 0.14415733762574415
Kappa statistic: 0.4204850628910199
Training time: 61.2
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.34278959810874
Incorrectly Classified Instances: 42.90780141843971
Correctly Classified Instances: 57.092198581560275
Weighted Precision: 0.4852290990286031
Weighted AreaUnderROC: 0.7965333521071974
Root mean squared error: 0.3917636082239887
Relative absolute error: 81.81245074862089
Root relative squared error: 90.47392986672759
Weighted TruePositiveRate: 0.5709219858156027
Weighted MatthewsCorrelation: 0.4154764560415944
Weighted FMeasure: 0.4810854485016165
Iteration time: 65.3
Weighted AreaUnderPRC: 0.5293282173603677
Mean absolute error: 0.30679669030732837
Coverage of cases: 94.4917257683215
Instances selection time: 1.1
Test time: 2.5
Accumulative iteration time: 575.2
Weighted Recall: 0.5709219858156027
Weighted FalsePositiveRate: 0.14170865476620845
Kappa statistic: 0.4302185977642937
Training time: 64.2
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.36643026004728
Incorrectly Classified Instances: 41.32387706855791
Correctly Classified Instances: 58.67612293144208
Weighted Precision: 0.46691720774258155
Weighted AreaUnderROC: 0.7987571636005081
Root mean squared error: 0.39117083843631795
Relative absolute error: 81.71263462043598
Root relative squared error: 90.33703554813594
Weighted TruePositiveRate: 0.5867612293144207
Weighted MatthewsCorrelation: 0.4290135633780993
Weighted FMeasure: 0.4919157975164703
Iteration time: 71.1
Weighted AreaUnderPRC: 0.538989537482079
Mean absolute error: 0.3064223798266349
Coverage of cases: 93.52245862884162
Instances selection time: 2.1
Test time: 2.0
Accumulative iteration time: 646.3
Weighted Recall: 0.5867612293144207
Weighted FalsePositiveRate: 0.13626588663255307
Kappa statistic: 0.4512582444565244
Training time: 69.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.37234042553192
Incorrectly Classified Instances: 38.67612293144208
Correctly Classified Instances: 61.32387706855792
Weighted Precision: 0.4974908016674268
Weighted AreaUnderROC: 0.804586379995167
Root mean squared error: 0.38952518841372136
Relative absolute error: 81.36590491200417
Root relative squared error: 89.95698895472069
Weighted TruePositiveRate: 0.6132387706855792
Weighted MatthewsCorrelation: 0.4601838449454629
Weighted FMeasure: 0.5193247755083169
Iteration time: 69.1
Weighted AreaUnderPRC: 0.5522778319185933
Mean absolute error: 0.30512214342001565
Coverage of cases: 92.67139479905437
Instances selection time: 1.3
Test time: 2.8
Accumulative iteration time: 715.4
Weighted Recall: 0.6132387706855792
Weighted FalsePositiveRate: 0.12756747269059293
Kappa statistic: 0.4862536928576147
Training time: 67.8
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.37825059101655
Incorrectly Classified Instances: 36.7612293144208
Correctly Classified Instances: 63.2387706855792
Weighted Precision: 0.5102740499298098
Weighted AreaUnderROC: 0.8087848917427205
Root mean squared error: 0.38854475495817314
Relative absolute error: 81.16101917520356
Root relative squared error: 89.73056754692738
Weighted TruePositiveRate: 0.6323877068557919
Weighted MatthewsCorrelation: 0.4802382220529089
Weighted FMeasure: 0.5438237172451272
Iteration time: 73.1
Weighted AreaUnderPRC: 0.5624685270630739
Mean absolute error: 0.3043538219070133
Coverage of cases: 92.12765957446808
Instances selection time: 1.0
Test time: 2.4
Accumulative iteration time: 788.5
Weighted Recall: 0.6323877068557919
Weighted FalsePositiveRate: 0.12134296374596845
Kappa statistic: 0.5115574186191855
Training time: 72.1
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.28959810874704
Incorrectly Classified Instances: 36.09929078014185
Correctly Classified Instances: 63.900709219858165
Weighted Precision: 0.5350073341765829
Weighted AreaUnderROC: 0.8085840890485765
Root mean squared error: 0.386951189345527
Relative absolute error: 80.79852902547938
Root relative squared error: 89.36254933275436
Weighted TruePositiveRate: 0.6390070921985815
Weighted MatthewsCorrelation: 0.4874919358500698
Weighted FMeasure: 0.5518658278257529
Iteration time: 70.9
Weighted AreaUnderPRC: 0.5649988921346332
Mean absolute error: 0.30299448384554767
Coverage of cases: 92.24586288416076
Instances selection time: 1.0
Test time: 2.4
Accumulative iteration time: 859.4
Weighted Recall: 0.6390070921985815
Weighted FalsePositiveRate: 0.11936092441352977
Kappa statistic: 0.5201623111909288
Training time: 69.9
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.49054373522458
Incorrectly Classified Instances: 33.99527186761229
Correctly Classified Instances: 66.00472813238771
Weighted Precision: 0.5700867786835389
Weighted AreaUnderROC: 0.8337824268438698
Root mean squared error: 0.37534201333995565
Relative absolute error: 78.4292093511951
Root relative squared error: 86.68152497599982
Weighted TruePositiveRate: 0.660047281323877
Weighted MatthewsCorrelation: 0.5207034350265098
Weighted FMeasure: 0.5855323320479952
Iteration time: 69.1
Weighted AreaUnderPRC: 0.5977671673882494
Mean absolute error: 0.29410953506698156
Coverage of cases: 95.43735224586291
Instances selection time: 1.4
Test time: 2.4
Accumulative iteration time: 928.5
Weighted Recall: 0.660047281323877
Weighted FalsePositiveRate: 0.11267489408653437
Kappa statistic: 0.5478523266283417
Training time: 67.7
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.54964539007094
Incorrectly Classified Instances: 31.654846335697396
Correctly Classified Instances: 68.3451536643026
Weighted Precision: 0.7296401850940832
Weighted AreaUnderROC: 0.8500947714398212
Root mean squared error: 0.3669273483338694
Relative absolute error: 76.77436301549763
Root relative squared error: 84.73824133343803
Weighted TruePositiveRate: 0.683451536643026
Weighted MatthewsCorrelation: 0.5654589837679355
Weighted FMeasure: 0.6153808740520106
Iteration time: 70.9
Weighted AreaUnderPRC: 0.6216948859581883
Mean absolute error: 0.2879038613081161
Coverage of cases: 96.90307328605202
Instances selection time: 0.6
Test time: 2.0
Accumulative iteration time: 999.4
Weighted Recall: 0.683451536643026
Weighted FalsePositiveRate: 0.10502830790984
Kappa statistic: 0.5788314336223269
Training time: 70.3
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.45508274231679
Incorrectly Classified Instances: 29.38534278959811
Correctly Classified Instances: 70.61465721040189
Weighted Precision: 0.7046978679460192
Weighted AreaUnderROC: 0.8488045527426893
Root mean squared error: 0.36237827483879936
Relative absolute error: 75.87601786183336
Root relative squared error: 83.6876778106612
Weighted TruePositiveRate: 0.706146572104019
Weighted MatthewsCorrelation: 0.6035662936265132
Weighted FMeasure: 0.6818694058988362
Iteration time: 75.8
Weighted AreaUnderPRC: 0.6259701718952047
Mean absolute error: 0.2845350669818751
Coverage of cases: 97.42316784869976
Instances selection time: 0.9
Test time: 2.7
Accumulative iteration time: 1075.2
Weighted Recall: 0.706146572104019
Weighted FalsePositiveRate: 0.09792987847005197
Kappa statistic: 0.6086994924255842
Training time: 74.9
		
Time end:Sun Oct 08 09.53.27 EEST 2017