Sun Oct 08 09.56.12 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.12 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.6089375656723774
Weighted AreaUnderROC: 0.7455596398571276
Root mean squared error: 0.426666605420435
Relative absolute error: 52.98732000859656
Root relative squared error: 98.5344317974848
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.4873687630442058
Weighted FMeasure: 0.6078617389138415
Iteration time: 10.0
Weighted AreaUnderPRC: 0.4908781805310687
Mean absolute error: 0.19870245003223713
Coverage of cases: 61.938534278959814
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.12826606307534294
Kappa statistic: 0.4918748321246307
Training time: 0.0
		
Iteration: 2
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6271178756364139
Weighted AreaUnderROC: 0.7534676563749212
Root mean squared error: 0.42721901779729204
Relative absolute error: 49.69657088542818
Root relative squared error: 98.66200597127764
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.5052373725643954
Weighted FMeasure: 0.6272163518479446
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5299692824499986
Mean absolute error: 0.18636214082035568
Coverage of cases: 63.12056737588652
Instances selection time: 4.0
Test time: 42.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.1242703610090227
Kappa statistic: 0.5080001789415607
Training time: 1.0
		
Time end:Sun Oct 08 09.56.12 EEST 2017