Sun Oct 08 09.53.20 EEST 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.53.20 EEST 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 76.00472813238771
Incorrectly Classified Instances: 43.97163120567376
Correctly Classified Instances: 56.02836879432624
Weighted Precision: 0.5506795848403413
Weighted AreaUnderROC: 0.7668404582872281
Root mean squared error: 0.3875356394127866
Relative absolute error: 81.11373785132638
Root relative squared error: 89.49752229421844
Weighted TruePositiveRate: 0.5602836879432624
Weighted MatthewsCorrelation: 0.4080214756141045
Weighted FMeasure: 0.5536272619319976
Iteration time: 51.0
Weighted AreaUnderPRC: 0.495793595733729
Mean absolute error: 0.3041765169424739
Coverage of cases: 95.74468085106383
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.5602836879432624
Weighted FalsePositiveRate: 0.14755761934051104
Kappa statistic: 0.4139572300060334
Training time: 49.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 76.24113475177305
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6014955803527068
Weighted AreaUnderROC: 0.7950047268009071
Root mean squared error: 0.3825899874575366
Relative absolute error: 80.1155765694772
Root relative squared error: 88.35537289914572
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.4806208297618107
Weighted FMeasure: 0.5962988248847616
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5313640698974074
Mean absolute error: 0.3004334121355395
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 100.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.12881242594032274
Kappa statistic: 0.48944254868944553
Training time: 48.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 76.12293144208037
Incorrectly Classified Instances: 35.22458628841608
Correctly Classified Instances: 64.77541371158392
Weighted Precision: 0.6299628143105463
Weighted AreaUnderROC: 0.8248672656442888
Root mean squared error: 0.3730302439621056
Relative absolute error: 78.11925400577876
Root relative squared error: 86.14764470695738
Weighted TruePositiveRate: 0.6477541371158393
Weighted MatthewsCorrelation: 0.5211348054559939
Weighted FMeasure: 0.6217705314955674
Iteration time: 49.0
Weighted AreaUnderPRC: 0.5668824326245374
Mean absolute error: 0.29294720252167034
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 149.0
Weighted Recall: 0.6477541371158393
Weighted FalsePositiveRate: 0.11866820218205132
Kappa statistic: 0.5301156315000783
Training time: 48.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 39.2434988179669
Correctly Classified Instances: 60.7565011820331
Weighted Precision: 0.617281047353695
Weighted AreaUnderROC: 0.8102034218827757
Root mean squared error: 0.3790549957222309
Relative absolute error: 79.27501970055151
Root relative squared error: 87.53900152716099
Weighted TruePositiveRate: 0.607565011820331
Weighted MatthewsCorrelation: 0.4795486894618351
Weighted FMeasure: 0.5861367434372317
Iteration time: 52.0
Weighted AreaUnderPRC: 0.5419001056030002
Mean absolute error: 0.2972813238770682
Coverage of cases: 96.21749408983452
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 201.0
Weighted Recall: 0.607565011820331
Weighted FalsePositiveRate: 0.13294928040869164
Kappa statistic: 0.4758990281986594
Training time: 51.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 37.825059101654844
Correctly Classified Instances: 62.174940898345156
Weighted Precision: 0.6339058653594276
Weighted AreaUnderROC: 0.8076948988568546
Root mean squared error: 0.3840034229555868
Relative absolute error: 80.22064617809295
Root relative squared error: 88.6817918452583
Weighted TruePositiveRate: 0.6217494089834515
Weighted MatthewsCorrelation: 0.4973742682310886
Weighted FMeasure: 0.5815135649988207
Iteration time: 53.0
Weighted AreaUnderPRC: 0.547657994234218
Mean absolute error: 0.30082742316784855
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 254.0
Weighted Recall: 0.6217494089834515
Weighted FalsePositiveRate: 0.1286087272218302
Kappa statistic: 0.49500828222232174
Training time: 52.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.29550827423168
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.6103089937144527
Weighted AreaUnderROC: 0.7872658150824489
Root mean squared error: 0.3922934391107242
Relative absolute error: 81.90175991594423
Root relative squared error: 90.59628906876027
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.4541256083406518
Weighted FMeasure: 0.5343324749245784
Iteration time: 53.0
Weighted AreaUnderPRC: 0.5235607114490687
Mean absolute error: 0.30713159968479087
Coverage of cases: 93.3806146572104
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 307.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1410191542423516
Kappa statistic: 0.44734779200418073
Training time: 52.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.4634540926633125
Weighted AreaUnderROC: 0.8064455025224396
Root mean squared error: 0.387408529922871
Relative absolute error: 80.90359863409505
Root relative squared error: 89.46816761493072
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.42601781949391104
Weighted FMeasure: 0.5042948061448066
Iteration time: 56.0
Weighted AreaUnderPRC: 0.5409832195727035
Mean absolute error: 0.3033884948778564
Coverage of cases: 95.0354609929078
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 363.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1407366853459327
Kappa statistic: 0.44760348340012085
Training time: 55.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 38.53427895981088
Correctly Classified Instances: 61.46572104018912
Weighted Precision: 0.49170250486514466
Weighted AreaUnderROC: 0.8278996146104108
Root mean squared error: 0.38490017945975
Relative absolute error: 80.32571578670863
Root relative squared error: 88.88888888888879
Weighted TruePositiveRate: 0.6146572104018913
Weighted MatthewsCorrelation: 0.4625232806462235
Weighted FMeasure: 0.5305404708969015
Iteration time: 56.0
Weighted AreaUnderPRC: 0.5752974108269259
Mean absolute error: 0.30122143420015735
Coverage of cases: 93.85342789598108
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 419.0
Weighted Recall: 0.6146572104018913
Weighted FalsePositiveRate: 0.13046361058373446
Kappa statistic: 0.48556271823798014
Training time: 53.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 38.061465721040186
Correctly Classified Instances: 61.938534278959814
Weighted Precision: 0.4994228512120521
Weighted AreaUnderROC: 0.8318806707709312
Root mean squared error: 0.3853264728448745
Relative absolute error: 80.43078539532438
Root relative squared error: 88.98733712915092
Weighted TruePositiveRate: 0.6193853427895981
Weighted MatthewsCorrelation: 0.47002806149081844
Weighted FMeasure: 0.537133394774369
Iteration time: 55.0
Weighted AreaUnderPRC: 0.587736251173204
Mean absolute error: 0.3016154452324664
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 474.0
Weighted Recall: 0.6193853427895981
Weighted FalsePositiveRate: 0.12860966193040635
Kappa statistic: 0.49202264539372104
Training time: 54.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.5075035048347469
Weighted AreaUnderROC: 0.8347171649722267
Root mean squared error: 0.38485752415076313
Relative absolute error: 80.32571578670868
Root relative squared error: 88.8790380672384
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.48019800974118537
Weighted FMeasure: 0.5459143028428444
Iteration time: 59.0
Weighted AreaUnderPRC: 0.59040581119223
Mean absolute error: 0.30122143420015757
Coverage of cases: 94.32624113475177
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 533.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12593674568144966
Kappa statistic: 0.5017222097964661
Training time: 58.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 35.9338061465721
Correctly Classified Instances: 64.06619385342789
Weighted Precision: 0.5170374908169519
Weighted AreaUnderROC: 0.8373556307392479
Root mean squared error: 0.383447236991748
Relative absolute error: 80.01050696086156
Root relative squared error: 88.55334619888158
Weighted TruePositiveRate: 0.640661938534279
Weighted MatthewsCorrelation: 0.49745386376040657
Weighted FMeasure: 0.5588287238154523
Iteration time: 63.0
Weighted AreaUnderPRC: 0.5986894131301407
Mean absolute error: 0.30003940110323085
Coverage of cases: 93.85342789598108
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 596.0
Weighted Recall: 0.640661938534279
Weighted FalsePositiveRate: 0.12089398520289106
Kappa statistic: 0.5208156329651656
Training time: 62.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.11820330969267
Incorrectly Classified Instances: 35.22458628841608
Correctly Classified Instances: 64.77541371158392
Weighted Precision: 0.5100868528536506
Weighted AreaUnderROC: 0.826826278596622
Root mean squared error: 0.386814800026196
Relative absolute error: 80.69345941686367
Root relative squared error: 89.33105156866222
Weighted TruePositiveRate: 0.6477541371158393
Weighted MatthewsCorrelation: 0.5003282944198825
Weighted FMeasure: 0.5628414079113742
Iteration time: 64.0
Weighted AreaUnderPRC: 0.586155981502827
Mean absolute error: 0.30260047281323876
Coverage of cases: 91.48936170212765
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 660.0
Weighted Recall: 0.6477541371158393
Weighted FalsePositiveRate: 0.11790297396810288
Kappa statistic: 0.5306509986148966
Training time: 63.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.05910165484633
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.5131070154211033
Weighted AreaUnderROC: 0.8292411134301378
Root mean squared error: 0.3853690762473866
Relative absolute error: 80.37825059101657
Root relative squared error: 88.99717596351444
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.515443166597609
Weighted FMeasure: 0.5750647224832909
Iteration time: 63.0
Weighted AreaUnderPRC: 0.5936913297511925
Mean absolute error: 0.3014184397163121
Coverage of cases: 91.48936170212765
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 723.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11198600616625004
Kappa statistic: 0.5530304721509096
Training time: 62.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.0
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.5150306316791056
Weighted AreaUnderROC: 0.8287200495888337
Root mean squared error: 0.38592249249397903
Relative absolute error: 80.48332019963232
Root relative squared error: 89.12498197109205
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5224492093771489
Weighted FMeasure: 0.5798862134247137
Iteration time: 66.0
Weighted AreaUnderPRC: 0.5925963269815951
Mean absolute error: 0.3018124507486212
Coverage of cases: 91.25295508274232
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 789.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10920093087564788
Kappa statistic: 0.5626882656134948
Training time: 65.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.5150306316791056
Weighted AreaUnderROC: 0.8444109224842425
Root mean squared error: 0.3728981900631198
Relative absolute error: 77.9091147885473
Root relative squared error: 86.11714816530659
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5224492093771489
Weighted FMeasure: 0.5798862134247137
Iteration time: 65.0
Weighted AreaUnderPRC: 0.6075417845612262
Mean absolute error: 0.29215918045705236
Coverage of cases: 94.56264775413712
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 854.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.10920093087564788
Kappa statistic: 0.5626882656134948
Training time: 64.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.7722037904596454
Weighted AreaUnderROC: 0.8490420948123832
Root mean squared error: 0.36935933714603447
Relative absolute error: 77.22616233254517
Root relative squared error: 85.2998850915859
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5450941516013621
Weighted FMeasure: 0.5854205384159978
Iteration time: 65.0
Weighted AreaUnderPRC: 0.608657147170846
Mean absolute error: 0.2895981087470444
Coverage of cases: 95.98108747044917
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 919.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10838028369370512
Kappa statistic: 0.5658473095087575
Training time: 64.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.6758412190029629
Weighted AreaUnderROC: 0.8362801669017662
Root mean squared error: 0.36695131869441194
Relative absolute error: 76.75334909377462
Root relative squared error: 84.74377705108276
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5838996927145393
Weighted FMeasure: 0.679402596990675
Iteration time: 123.0
Weighted AreaUnderPRC: 0.6022380326394565
Mean absolute error: 0.2878250591016548
Coverage of cases: 96.21749408983452
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 1042.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.10196672871984408
Kappa statistic: 0.5936932241250931
Training time: 119.0
		
Time end:Sun Oct 08 09.53.22 EEST 2017