Sun Oct 08 10.41.04 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.41.04 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 42.38666666666713
Incorrectly Classified Instances: 31.76
Correctly Classified Instances: 68.24
Weighted Precision: 0.6821926276037966
Weighted AreaUnderROC: 0.7607693298439961
Root mean squared error: 0.44701004613592177
Relative absolute error: 49.2581333666335
Root relative squared error: 94.8251504643663
Weighted TruePositiveRate: 0.6824
Weighted MatthewsCorrelation: 0.5233313441993419
Weighted FMeasure: 0.6822112882859
Iteration time: 89.0
Weighted AreaUnderPRC: 0.589552167328094
Mean absolute error: 0.2189250371850388
Coverage of cases: 74.84
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 89.0
Weighted Recall: 0.6824
Weighted FalsePositiveRate: 0.1589727814561942
Kappa statistic: 0.5236131863870008
Training time: 86.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 43.54666666666683
Incorrectly Classified Instances: 30.08
Correctly Classified Instances: 69.92
Weighted Precision: 0.6989194406817861
Weighted AreaUnderROC: 0.7971233344136226
Root mean squared error: 0.42993995349329867
Relative absolute error: 46.74945151370855
Root relative squared error: 91.20403698544189
Weighted TruePositiveRate: 0.6992
Weighted MatthewsCorrelation: 0.5485895297741662
Weighted FMeasure: 0.6989128202168507
Iteration time: 85.0
Weighted AreaUnderPRC: 0.63644804811792
Mean absolute error: 0.20777534006092788
Coverage of cases: 78.28
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 174.0
Weighted Recall: 0.6992
Weighted FalsePositiveRate: 0.15047012559259948
Kappa statistic: 0.5488419396532899
Training time: 82.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 45.07999999999986
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7111014472229215
Weighted AreaUnderROC: 0.8128200168527469
Root mean squared error: 0.4221352564883255
Relative absolute error: 44.86441750010275
Root relative squared error: 89.54841073224505
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5666434435605815
Weighted FMeasure: 0.7110829401431756
Iteration time: 89.0
Weighted AreaUnderPRC: 0.6513891375688133
Mean absolute error: 0.19939741111156872
Coverage of cases: 80.88
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 263.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14451572009490232
Kappa statistic: 0.5667949401649013
Training time: 86.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 39.026666666667694
Incorrectly Classified Instances: 29.16
Correctly Classified Instances: 70.84
Weighted Precision: 0.708449055294901
Weighted AreaUnderROC: 0.8241161116699363
Root mean squared error: 0.42163222098182696
Relative absolute error: 43.58798791208787
Root relative squared error: 89.44170078689821
Weighted TruePositiveRate: 0.7084
Weighted MatthewsCorrelation: 0.5624861775091792
Weighted FMeasure: 0.7083985402040862
Iteration time: 90.0
Weighted AreaUnderPRC: 0.6694139706640317
Mean absolute error: 0.1937243907203914
Coverage of cases: 78.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 353.0
Weighted Recall: 0.7084
Weighted FalsePositiveRate: 0.14591414473135547
Kappa statistic: 0.5625963608017219
Training time: 88.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 39.17333333333434
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7134472853280511
Weighted AreaUnderROC: 0.8133062801750093
Root mean squared error: 0.42011103014997025
Relative absolute error: 43.297766483516384
Root relative squared error: 89.11900748109282
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.5684584197299868
Weighted FMeasure: 0.7123714912227713
Iteration time: 97.0
Weighted AreaUnderPRC: 0.6550215204803836
Mean absolute error: 0.19243451770451817
Coverage of cases: 78.6
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 450.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.1442636892296184
Kappa statistic: 0.5679126146471862
Training time: 94.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 36.61333333333435
Incorrectly Classified Instances: 26.48
Correctly Classified Instances: 73.52
Weighted Precision: 0.736265187171547
Weighted AreaUnderROC: 0.8100218795009243
Root mean squared error: 0.41229334158536945
Relative absolute error: 41.0056403260402
Root relative squared error: 87.46062530192269
Weighted TruePositiveRate: 0.7352
Weighted MatthewsCorrelation: 0.6030272519980883
Weighted FMeasure: 0.7355320361563041
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6441876082378328
Mean absolute error: 0.18224729033795728
Coverage of cases: 76.4
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 550.0
Weighted Recall: 0.7352
Weighted FalsePositiveRate: 0.13272374170325815
Kappa statistic: 0.6027120245507164
Training time: 97.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 38.60000000000099
Incorrectly Classified Instances: 26.44
Correctly Classified Instances: 73.56
Weighted Precision: 0.7356669311457472
Weighted AreaUnderROC: 0.8205834867918096
Root mean squared error: 0.40984892115977606
Relative absolute error: 41.053633699633934
Root relative squared error: 86.94208542422031
Weighted TruePositiveRate: 0.7356
Weighted MatthewsCorrelation: 0.6032507865913139
Weighted FMeasure: 0.7355925003144171
Iteration time: 98.0
Weighted AreaUnderPRC: 0.6569481022943118
Mean absolute error: 0.18246059422059613
Coverage of cases: 77.92
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 648.0
Weighted Recall: 0.7356
Weighted FalsePositiveRate: 0.13237095133282017
Kappa statistic: 0.6033887045103045
Training time: 95.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 39.02666666666771
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.7410971113198183
Weighted AreaUnderROC: 0.8281517058498492
Root mean squared error: 0.4048158700808727
Relative absolute error: 40.52875010175008
Root relative squared error: 85.87441405983506
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.6098589566352915
Weighted FMeasure: 0.7399333414701293
Iteration time: 107.0
Weighted AreaUnderPRC: 0.6725451966677
Mean absolute error: 0.18012777823000117
Coverage of cases: 78.68
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 755.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.1305736099965887
Kappa statistic: 0.6092791578579424
Training time: 104.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 37.88000000000094
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7123994924243845
Weighted AreaUnderROC: 0.8000898509452481
Root mean squared error: 0.4270928071485184
Relative absolute error: 44.13482602035616
Root relative squared error: 90.6000660392145
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5688601147046287
Weighted FMeasure: 0.712387150068619
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6255602140462454
Mean absolute error: 0.19615478231269495
Coverage of cases: 75.84
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 863.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.14382815314518999
Kappa statistic: 0.569197656435251
Training time: 105.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 35.76000000000094
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7266641167334102
Weighted AreaUnderROC: 0.7994296629251547
Root mean squared error: 0.42305373165704585
Relative absolute error: 42.1992198581562
Root relative squared error: 89.74324873829113
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5874062155325298
Weighted FMeasure: 0.7245699773960342
Iteration time: 118.0
Weighted AreaUnderPRC: 0.62762996797027
Mean absolute error: 0.18755208825847286
Coverage of cases: 75.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 981.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13828583175022593
Kappa statistic: 0.5864439073884048
Training time: 115.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 40.60000000000075
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.733546337725932
Weighted AreaUnderROC: 0.8360272712877856
Root mean squared error: 0.41039380033165695
Relative absolute error: 41.15915399796283
Root relative squared error: 87.0576717514296
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5979852302013358
Weighted FMeasure: 0.7320819728740364
Iteration time: 112.0
Weighted AreaUnderPRC: 0.6840615909771152
Mean absolute error: 0.1829295733242801
Coverage of cases: 81.36
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1093.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13463995450571226
Kappa statistic: 0.5972694508651926
Training time: 109.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 37.65333333333446
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7254259349593496
Weighted AreaUnderROC: 0.8043952393231346
Root mean squared error: 0.4191569404686371
Relative absolute error: 42.72929670329681
Root relative squared error: 88.91661449603359
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5869262729185972
Weighted FMeasure: 0.7245206524501877
Iteration time: 131.0
Weighted AreaUnderPRC: 0.6370221314653266
Mean absolute error: 0.1899079853479867
Coverage of cases: 76.08
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1224.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.1381219159968455
Kappa statistic: 0.5864703170914399
Training time: 128.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 37.280000000001166
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7339864784835619
Weighted AreaUnderROC: 0.8076901079657438
Root mean squared error: 0.4144199415417579
Relative absolute error: 41.435498207056
Root relative squared error: 87.91174527693268
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5994695395921406
Weighted FMeasure: 0.733117851858648
Iteration time: 133.0
Weighted AreaUnderPRC: 0.6426803270250843
Mean absolute error: 0.18415776980913864
Coverage of cases: 77.16
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1357.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13397719089286705
Kappa statistic: 0.5990912093905801
Training time: 130.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 39.040000000000965
Incorrectly Classified Instances: 24.72
Correctly Classified Instances: 75.28
Weighted Precision: 0.7558187177542017
Weighted AreaUnderROC: 0.8286100444902759
Root mean squared error: 0.395938313661868
Relative absolute error: 38.62634798534783
Root relative squared error: 83.99119995656173
Weighted TruePositiveRate: 0.7528
Weighted MatthewsCorrelation: 0.6304178662277738
Weighted FMeasure: 0.7532133409240727
Iteration time: 133.0
Weighted AreaUnderPRC: 0.6787546330484365
Mean absolute error: 0.17167265771265783
Coverage of cases: 80.76
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1490.0
Weighted Recall: 0.7528
Weighted FalsePositiveRate: 0.1240706632315011
Kappa statistic: 0.6290416452975447
Training time: 130.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 36.133333333334306
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7318325367823317
Weighted AreaUnderROC: 0.8078034751688072
Root mean squared error: 0.4189033249249421
Relative absolute error: 41.617560150375965
Root relative squared error: 88.86281451480527
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5923148739944093
Weighted FMeasure: 0.7274378397981709
Iteration time: 155.0
Weighted AreaUnderPRC: 0.641532435958935
Mean absolute error: 0.1849669340016718
Coverage of cases: 75.28
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1645.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13727726211516006
Kappa statistic: 0.5899470137095782
Training time: 152.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 36.26666666666771
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.7244284387554246
Weighted AreaUnderROC: 0.7991662996271968
Root mean squared error: 0.4245962394869341
Relative absolute error: 42.860395850173184
Root relative squared error: 90.07046406225533
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.5825435752903099
Weighted FMeasure: 0.721490190843425
Iteration time: 145.0
Weighted AreaUnderPRC: 0.6278486819640214
Mean absolute error: 0.19049064822299283
Coverage of cases: 74.12
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1790.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.14020386248828198
Kappa statistic: 0.5809927757871933
Training time: 142.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 35.56000000000097
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7203429273753716
Weighted AreaUnderROC: 0.7967510564177581
Root mean squared error: 0.4286045844517605
Relative absolute error: 43.13927443609036
Root relative squared error: 90.92076243404443
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5781685336041078
Weighted FMeasure: 0.7188826859889559
Iteration time: 151.0
Weighted AreaUnderPRC: 0.6239589437425171
Mean absolute error: 0.19173010860484696
Coverage of cases: 73.64
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1941.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.1412660093813357
Kappa statistic: 0.5774530888899454
Training time: 148.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 36.16000000000105
Incorrectly Classified Instances: 26.32
Correctly Classified Instances: 73.68
Weighted Precision: 0.7400650100660138
Weighted AreaUnderROC: 0.8120177864842378
Root mean squared error: 0.41370120077158223
Relative absolute error: 40.31406808688387
Root relative squared error: 87.75927733518076
Weighted TruePositiveRate: 0.7368
Weighted MatthewsCorrelation: 0.6064721264454765
Weighted FMeasure: 0.7373100172720458
Iteration time: 151.0
Weighted AreaUnderPRC: 0.6481909291093997
Mean absolute error: 0.17917363594170696
Coverage of cases: 75.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2092.0
Weighted Recall: 0.7368
Weighted FalsePositiveRate: 0.1321588162836515
Kappa statistic: 0.6050057892464566
Training time: 149.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 36.06666666666761
Incorrectly Classified Instances: 27.52
Correctly Classified Instances: 72.48
Weighted Precision: 0.729314408462307
Weighted AreaUnderROC: 0.8110991146824698
Root mean squared error: 0.41911305789866626
Relative absolute error: 41.56173597455182
Root relative squared error: 88.9073055971929
Weighted TruePositiveRate: 0.7248
Weighted MatthewsCorrelation: 0.58900018770978
Weighted FMeasure: 0.7255322948744595
Iteration time: 163.0
Weighted AreaUnderPRC: 0.6446860975121818
Mean absolute error: 0.18471882655356453
Coverage of cases: 75.76
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2255.0
Weighted Recall: 0.7248
Weighted FalsePositiveRate: 0.13825618202514997
Kappa statistic: 0.5869655947143122
Training time: 160.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 36.65333333333436
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7261074272223268
Weighted AreaUnderROC: 0.8051538732732022
Root mean squared error: 0.4230378690036342
Relative absolute error: 42.290602607918494
Root relative squared error: 89.73988376135263
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5854532659987124
Weighted FMeasure: 0.7233089967963751
Iteration time: 166.0
Weighted AreaUnderPRC: 0.6367987539478931
Mean absolute error: 0.18795823381297197
Coverage of cases: 75.48
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2421.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13914898069214837
Kappa statistic: 0.5840074453663112
Training time: 163.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 37.8266666666676
Incorrectly Classified Instances: 25.64
Correctly Classified Instances: 74.36
Weighted Precision: 0.7467169616348563
Weighted AreaUnderROC: 0.80987624009302
Root mean squared error: 0.4055268914490568
Relative absolute error: 40.51507639176503
Root relative squared error: 86.0252444691385
Weighted TruePositiveRate: 0.7436
Weighted MatthewsCorrelation: 0.6167375985303228
Weighted FMeasure: 0.7437432322056565
Iteration time: 170.0
Weighted AreaUnderPRC: 0.6474562305011199
Mean absolute error: 0.1800670061856232
Coverage of cases: 77.6
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 2591.0
Weighted Recall: 0.7436
Weighted FalsePositiveRate: 0.12871824851982888
Kappa statistic: 0.6152056326691739
Training time: 165.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 37.2800000000009
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7403525801295326
Weighted AreaUnderROC: 0.8033962478541878
Root mean squared error: 0.41027509214110225
Relative absolute error: 40.69686555656434
Root relative squared error: 87.0324899414725
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6071261711499089
Weighted FMeasure: 0.7373390144169294
Iteration time: 165.0
Weighted AreaUnderPRC: 0.6438132238617791
Mean absolute error: 0.1808749580291757
Coverage of cases: 77.32
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2756.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.13191945593597484
Kappa statistic: 0.6056081678128241
Training time: 162.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 38.400000000001036
Incorrectly Classified Instances: 26.48
Correctly Classified Instances: 73.52
Weighted Precision: 0.7378923692365678
Weighted AreaUnderROC: 0.8254998967816948
Root mean squared error: 0.4057140856478665
Relative absolute error: 39.94329692797165
Root relative squared error: 86.06495435535165
Weighted TruePositiveRate: 0.7352
Weighted MatthewsCorrelation: 0.603722894161044
Weighted FMeasure: 0.7357307471112066
Iteration time: 175.0
Weighted AreaUnderPRC: 0.665490966557882
Mean absolute error: 0.17752576412431928
Coverage of cases: 79.92
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2931.0
Weighted Recall: 0.7352
Weighted FalsePositiveRate: 0.1329065470517574
Kappa statistic: 0.6026519322160209
Training time: 172.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 37.90666666666747
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.7306423934491333
Weighted AreaUnderROC: 0.8226696882531105
Root mean squared error: 0.4164908878334961
Relative absolute error: 41.06739678742315
Root relative squared error: 88.35105932684105
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5945669471416773
Weighted FMeasure: 0.7299104228791217
Iteration time: 174.0
Weighted AreaUnderPRC: 0.6631632273377047
Mean absolute error: 0.1825217634996593
Coverage of cases: 78.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3105.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.1355814062790846
Kappa statistic: 0.594306268520279
Training time: 172.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 37.280000000000754
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7380626793555184
Weighted AreaUnderROC: 0.815336285323949
Root mean squared error: 0.41321732180768395
Relative absolute error: 40.40139179241809
Root relative squared error: 87.65663110618694
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6046441298273346
Weighted FMeasure: 0.736459818249612
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6533128697525533
Mean absolute error: 0.17956174129963678
Coverage of cases: 77.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3286.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13247624617256296
Kappa statistic: 0.6038573252542636
Training time: 179.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 38.09333333333411
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7389840982390593
Weighted AreaUnderROC: 0.812649591996842
Root mean squared error: 0.4113497026475132
Relative absolute error: 40.340758963116286
Root relative squared error: 87.26044925433774
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6071990436103659
Weighted FMeasure: 0.7382104248377005
Iteration time: 188.0
Weighted AreaUnderPRC: 0.6474866349765779
Mean absolute error: 0.17929226205829543
Coverage of cases: 78.16
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 3474.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13136294073023266
Kappa statistic: 0.6069008761249237
Training time: 185.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 37.12000000000084
Incorrectly Classified Instances: 25.76
Correctly Classified Instances: 74.24
Weighted Precision: 0.7438567044642604
Weighted AreaUnderROC: 0.8197752812042236
Root mean squared error: 0.40733758462239744
Relative absolute error: 39.615439186401666
Root relative squared error: 86.4093504955937
Weighted TruePositiveRate: 0.7424
Weighted MatthewsCorrelation: 0.6141126679382678
Weighted FMeasure: 0.7425833590167596
Iteration time: 191.0
Weighted AreaUnderPRC: 0.6556502625122086
Mean absolute error: 0.17606861860623046
Coverage of cases: 77.68
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 3665.0
Weighted Recall: 0.7424
Weighted FalsePositiveRate: 0.12914001642819325
Kappa statistic: 0.6134817563388991
Training time: 188.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 39.400000000000595
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.734535698063344
Weighted AreaUnderROC: 0.8070524235582099
Root mean squared error: 0.41108579576153453
Relative absolute error: 40.804585993572175
Root relative squared error: 87.20446614973454
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6014961994126719
Weighted FMeasure: 0.7342952970365669
Iteration time: 199.0
Weighted AreaUnderPRC: 0.6400073866235375
Mean absolute error: 0.18135371552698829
Coverage of cases: 79.32
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 3864.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13301720231862688
Kappa statistic: 0.6015679979415947
Training time: 196.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 37.22666666666753
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7375005877770167
Weighted AreaUnderROC: 0.8094107020975473
Root mean squared error: 0.4102064426844509
Relative absolute error: 40.547632976001275
Root relative squared error: 87.01792719257561
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6045122961496688
Weighted FMeasure: 0.7359835456138666
Iteration time: 202.0
Weighted AreaUnderPRC: 0.6439743037701889
Mean absolute error: 0.18021170211556206
Coverage of cases: 78.04
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4066.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.1323622571173233
Kappa statistic: 0.6039073460063777
Training time: 199.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 36.77333333333422
Incorrectly Classified Instances: 25.76
Correctly Classified Instances: 74.24
Weighted Precision: 0.7421382854108748
Weighted AreaUnderROC: 0.8010067230434764
Root mean squared error: 0.4088059137458182
Relative absolute error: 39.700136438857605
Root relative squared error: 86.72083013964907
Weighted TruePositiveRate: 0.7424
Weighted MatthewsCorrelation: 0.6134213775351595
Weighted FMeasure: 0.7420185462670343
Iteration time: 206.0
Weighted AreaUnderPRC: 0.6296815845792486
Mean absolute error: 0.17644505083936796
Coverage of cases: 77.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4272.0
Weighted Recall: 0.7424
Weighted FalsePositiveRate: 0.12887545495583794
Kappa statistic: 0.613637029027138
Training time: 204.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.37333333333405
Incorrectly Classified Instances: 24.64
Correctly Classified Instances: 75.36
Weighted Precision: 0.7534805299073576
Weighted AreaUnderROC: 0.8193749480595754
Root mean squared error: 0.39898467070485905
Relative absolute error: 37.9549694277086
Root relative squared error: 84.63742987346605
Weighted TruePositiveRate: 0.7536
Weighted MatthewsCorrelation: 0.630241361773853
Weighted FMeasure: 0.7534294271602613
Iteration time: 207.0
Weighted AreaUnderPRC: 0.6598280786872325
Mean absolute error: 0.168688753012039
Coverage of cases: 78.92
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4479.0
Weighted Recall: 0.7536
Weighted FalsePositiveRate: 0.1233382549799287
Kappa statistic: 0.6303845057184798
Training time: 204.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 37.360000000000966
Incorrectly Classified Instances: 26.08
Correctly Classified Instances: 73.92
Weighted Precision: 0.7402277562751899
Weighted AreaUnderROC: 0.8028800412128421
Root mean squared error: 0.4096243916144177
Relative absolute error: 40.33873633097957
Root relative squared error: 86.89445551499041
Weighted TruePositiveRate: 0.7392
Weighted MatthewsCorrelation: 0.6090287476334814
Weighted FMeasure: 0.7394251286392755
Iteration time: 216.0
Weighted AreaUnderPRC: 0.6320117992068129
Mean absolute error: 0.17928327258213225
Coverage of cases: 77.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4695.0
Weighted Recall: 0.7392
Weighted FalsePositiveRate: 0.13075209196658333
Kappa statistic: 0.6087015179980497
Training time: 213.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 36.760000000000886
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.73926174564003
Weighted AreaUnderROC: 0.8165405641772014
Root mean squared error: 0.40697467497099427
Relative absolute error: 39.8271231431172
Root relative squared error: 86.33236573295413
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6072967758812967
Weighted FMeasure: 0.7382516873583436
Iteration time: 219.0
Weighted AreaUnderPRC: 0.6550586259457581
Mean absolute error: 0.17700943619163284
Coverage of cases: 78.6
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4914.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13137365911955884
Kappa statistic: 0.6069146533095265
Training time: 217.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 36.73333333333417
Incorrectly Classified Instances: 25.68
Correctly Classified Instances: 74.32
Weighted Precision: 0.7454466254344532
Weighted AreaUnderROC: 0.8126941060492112
Root mean squared error: 0.4045099965446932
Relative absolute error: 39.499740077569285
Root relative squared error: 85.80952848434964
Weighted TruePositiveRate: 0.7432
Weighted MatthewsCorrelation: 0.6155660620610797
Weighted FMeasure: 0.7436858320371353
Iteration time: 230.0
Weighted AreaUnderPRC: 0.6513750692092437
Mean absolute error: 0.1755544003447532
Coverage of cases: 77.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 5144.0
Weighted Recall: 0.7432
Weighted FalsePositiveRate: 0.12885655964286233
Kappa statistic: 0.6146562205290037
Training time: 227.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 39.25333333333411
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7353045003307299
Weighted AreaUnderROC: 0.8193395320314758
Root mean squared error: 0.4103623358736516
Relative absolute error: 41.105459914986874
Root relative squared error: 87.050997131943
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6018294226049987
Weighted FMeasure: 0.7345434868434548
Iteration time: 217.0
Weighted AreaUnderPRC: 0.6537178607181627
Mean absolute error: 0.18269093295549807
Coverage of cases: 80.44
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5361.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13310086883013308
Kappa statistic: 0.6015044885958267
Training time: 215.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 37.0800000000009
Incorrectly Classified Instances: 26.68
Correctly Classified Instances: 73.32
Weighted Precision: 0.7344439710665347
Weighted AreaUnderROC: 0.8111470575561385
Root mean squared error: 0.4124019180892872
Relative absolute error: 41.00217796009971
Root relative squared error: 87.48365785658203
Weighted TruePositiveRate: 0.7332
Weighted MatthewsCorrelation: 0.6001632096456148
Weighted FMeasure: 0.7334461711328173
Iteration time: 219.0
Weighted AreaUnderPRC: 0.6425942924413518
Mean absolute error: 0.18223190204488846
Coverage of cases: 77.96
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5580.0
Weighted Recall: 0.7332
Weighted FalsePositiveRate: 0.1337595006140579
Kappa statistic: 0.5996769567094141
Training time: 217.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 36.840000000001005
Incorrectly Classified Instances: 25.64
Correctly Classified Instances: 74.36
Weighted Precision: 0.7443644721194902
Weighted AreaUnderROC: 0.8237302946719467
Root mean squared error: 0.4059293766833381
Relative absolute error: 39.394790727989495
Root relative squared error: 86.11062448068483
Weighted TruePositiveRate: 0.7436
Weighted MatthewsCorrelation: 0.615530613590905
Weighted FMeasure: 0.7438058910457955
Iteration time: 240.0
Weighted AreaUnderPRC: 0.6630971011358519
Mean absolute error: 0.17508795879106526
Coverage of cases: 78.16
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5820.0
Weighted Recall: 0.7436
Weighted FalsePositiveRate: 0.12850440606830776
Kappa statistic: 0.6153131530974434
Training time: 238.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 36.640000000000875
Incorrectly Classified Instances: 25.76
Correctly Classified Instances: 74.24
Weighted Precision: 0.7445489850966074
Weighted AreaUnderROC: 0.8298751937489062
Root mean squared error: 0.4062102552115065
Relative absolute error: 39.382429162377534
Root relative squared error: 86.1702078142721
Weighted TruePositiveRate: 0.7424
Weighted MatthewsCorrelation: 0.6142744310736089
Weighted FMeasure: 0.7428804490048078
Iteration time: 237.0
Weighted AreaUnderPRC: 0.673891658002315
Mean absolute error: 0.17503301849945654
Coverage of cases: 77.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6057.0
Weighted Recall: 0.7424
Weighted FalsePositiveRate: 0.1292793261121475
Kappa statistic: 0.6134624544798021
Training time: 235.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 36.133333333334434
Incorrectly Classified Instances: 26.48
Correctly Classified Instances: 73.52
Weighted Precision: 0.7360255010591887
Weighted AreaUnderROC: 0.8065759510416048
Root mean squared error: 0.41318284628702784
Relative absolute error: 40.69853327096543
Root relative squared error: 87.64931774385468
Weighted TruePositiveRate: 0.7352
Weighted MatthewsCorrelation: 0.603054659877962
Weighted FMeasure: 0.7351463057703337
Iteration time: 264.0
Weighted AreaUnderPRC: 0.6340397592459447
Mean absolute error: 0.18088237009318053
Coverage of cases: 76.76
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 6321.0
Weighted Recall: 0.7352
Weighted FalsePositiveRate: 0.13262475839307514
Kappa statistic: 0.6027515039035964
Training time: 261.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 36.18666666666769
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7409388579764031
Weighted AreaUnderROC: 0.8159835086011613
Root mean squared error: 0.40918503047174787
Relative absolute error: 40.06523137367595
Root relative squared error: 86.8012529419789
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6107363545256677
Weighted FMeasure: 0.7403115210175564
Iteration time: 254.0
Weighted AreaUnderPRC: 0.6473985346665455
Mean absolute error: 0.17806769499411615
Coverage of cases: 77.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6575.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.13002709862088432
Kappa statistic: 0.610536377222583
Training time: 252.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 36.640000000001066
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7339302405239522
Weighted AreaUnderROC: 0.8140588177060305
Root mean squared error: 0.4143579890699734
Relative absolute error: 40.70623160173153
Root relative squared error: 87.89860317305966
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5995060384019032
Weighted FMeasure: 0.7330234751966967
Iteration time: 244.0
Weighted AreaUnderPRC: 0.6492389364664354
Mean absolute error: 0.1809165848965854
Coverage of cases: 76.96
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6819.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13395044114557564
Kappa statistic: 0.5990850496654401
Training time: 242.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 36.54666666666776
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7354483694942212
Weighted AreaUnderROC: 0.8143472014044403
Root mean squared error: 0.41348375089213407
Relative absolute error: 40.607564854187856
Root relative squared error: 87.71314924988295
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6019069156992508
Weighted FMeasure: 0.7345581014220283
Iteration time: 270.0
Weighted AreaUnderPRC: 0.6464480383107842
Mean absolute error: 0.18047806601861352
Coverage of cases: 77.88
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7089.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13313349057854185
Kappa statistic: 0.6014819111199031
Training time: 268.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 39.57333333333419
Incorrectly Classified Instances: 25.04
Correctly Classified Instances: 74.96
Weighted Precision: 0.7496633890491852
Weighted AreaUnderROC: 0.8306718778229628
Root mean squared error: 0.3967093549679561
Relative absolute error: 38.953256250334455
Root relative squared error: 84.1547625173947
Weighted TruePositiveRate: 0.7496
Weighted MatthewsCorrelation: 0.624298158044683
Weighted FMeasure: 0.7495049565513172
Iteration time: 266.0
Weighted AreaUnderPRC: 0.6759482400863454
Mean absolute error: 0.17312558333482062
Coverage of cases: 80.16
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 7355.0
Weighted Recall: 0.7496
Weighted FalsePositiveRate: 0.12533371846333624
Kappa statistic: 0.6243997596158462
Training time: 264.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 37.933333333334126
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7288345123054827
Weighted AreaUnderROC: 0.8018790730439338
Root mean squared error: 0.41959932097599684
Relative absolute error: 42.17486669883189
Root relative squared error: 89.01045757301922
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5890772274271568
Weighted FMeasure: 0.7260122256335825
Iteration time: 278.0
Weighted AreaUnderPRC: 0.6327575863239282
Mean absolute error: 0.18744385199480929
Coverage of cases: 77.12
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 7633.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.13801483352641813
Kappa statistic: 0.5876114889638351
Training time: 276.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 42.2400000000005
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7315573291320415
Weighted AreaUnderROC: 0.8150053774661256
Root mean squared error: 0.40854166242738765
Relative absolute error: 43.30531223133195
Root relative squared error: 86.66477396988914
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5923815002806335
Weighted FMeasure: 0.7262952182957262
Iteration time: 274.0
Weighted AreaUnderPRC: 0.6548899379985768
Mean absolute error: 0.19246805436147627
Coverage of cases: 80.08
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7907.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13714397995913322
Kappa statistic: 0.5900287304024129
Training time: 272.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 40.24000000000084
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7223173633431499
Weighted AreaUnderROC: 0.8036568818992514
Root mean squared error: 0.4175890911397536
Relative absolute error: 43.2022139033108
Root relative squared error: 88.5840234283339
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5804300277496811
Weighted FMeasure: 0.7198987474341466
Iteration time: 283.0
Weighted AreaUnderPRC: 0.6390332578224168
Mean absolute error: 0.19200983957027112
Coverage of cases: 77.84
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 8190.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14072953785844325
Kappa statistic: 0.5792092639751747
Training time: 281.0
		
Time end:Sun Oct 08 10.41.16 EEST 2017