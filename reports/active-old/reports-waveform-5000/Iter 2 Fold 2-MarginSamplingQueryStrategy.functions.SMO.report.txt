Sun Oct 08 11.25.24 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.25.24 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.56
Correctly Classified Instances: 83.44
Weighted Precision: 0.8361323758479117
Weighted AreaUnderROC: 0.9154996727309259
Root mean squared error: 0.33319997332266443
Relative absolute error: 58.319999999999084
Root relative squared error: 70.68238818828966
Weighted TruePositiveRate: 0.8344
Weighted MatthewsCorrelation: 0.7527288106737003
Weighted FMeasure: 0.833611257352218
Iteration time: 51.0
Weighted AreaUnderPRC: 0.7784617296262603
Mean absolute error: 0.25919999999999715
Coverage of cases: 99.96
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8344
Weighted FalsePositiveRate: 0.08280033507496182
Kappa statistic: 0.7516343539361636
Training time: 44.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.04
Correctly Classified Instances: 82.96
Weighted Precision: 0.8324139958660799
Weighted AreaUnderROC: 0.9128970486516413
Root mean squared error: 0.3347525343582351
Relative absolute error: 58.55999999999909
Root relative squared error: 71.01173611922707
Weighted TruePositiveRate: 0.8296
Weighted MatthewsCorrelation: 0.7462301421271582
Weighted FMeasure: 0.8286273783831655
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7730544380938074
Mean absolute error: 0.26026666666666387
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8296
Weighted FalsePositiveRate: 0.0851387471983729
Kappa statistic: 0.7444539917606208
Training time: 44.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.8281026907569773
Weighted AreaUnderROC: 0.9096986143826372
Root mean squared error: 0.3367381662250282
Relative absolute error: 58.85999999999914
Root relative squared error: 71.43295224661192
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7382776972606351
Weighted FMeasure: 0.8224622693698275
Iteration time: 51.0
Weighted AreaUnderPRC: 0.766441217386583
Mean absolute error: 0.2615999999999974
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 152.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08808407779579075
Kappa statistic: 0.7354667470303893
Training time: 45.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.36
Correctly Classified Instances: 82.64
Weighted Precision: 0.8316238723638657
Weighted AreaUnderROC: 0.911097869715396
Root mean squared error: 0.3356806242270822
Relative absolute error: 58.69999999999915
Root relative squared error: 71.20861371117077
Weighted TruePositiveRate: 0.8264
Weighted MatthewsCorrelation: 0.7429883983114265
Weighted FMeasure: 0.8250401172710732
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7700866327406307
Mean absolute error: 0.26088888888888634
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 202.0
Weighted Recall: 0.8264
Weighted FalsePositiveRate: 0.08665877963617334
Kappa statistic: 0.7396739950636431
Training time: 44.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.48
Correctly Classified Instances: 82.52
Weighted Precision: 0.8317745377503124
Weighted AreaUnderROC: 0.9099509492465512
Root mean squared error: 0.3360775924517287
Relative absolute error: 58.75999999999914
Root relative squared error: 71.2928233882397
Weighted TruePositiveRate: 0.8252
Weighted MatthewsCorrelation: 0.7420483788634717
Weighted FMeasure: 0.8236156027702087
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7687204295566431
Mean absolute error: 0.261155555555553
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 254.0
Weighted Recall: 0.8252
Weighted FalsePositiveRate: 0.0872382329493752
Kappa statistic: 0.7378876503697165
Training time: 46.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 17.48
Correctly Classified Instances: 82.52
Weighted Precision: 0.8325298652115107
Weighted AreaUnderROC: 0.9101909070803016
Root mean squared error: 0.3361657439529041
Relative absolute error: 58.7799999999992
Root relative squared error: 71.31152314551558
Weighted TruePositiveRate: 0.8252
Weighted MatthewsCorrelation: 0.7424992649998081
Weighted FMeasure: 0.8235710192197723
Iteration time: 56.0
Weighted AreaUnderPRC: 0.7690238074403274
Mean absolute error: 0.2612444444444421
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 310.0
Weighted Recall: 0.8252
Weighted FalsePositiveRate: 0.0872382329493752
Kappa statistic: 0.7378816760697368
Training time: 50.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8291314155213658
Weighted AreaUnderROC: 0.9071231768373289
Root mean squared error: 0.3379239449825873
Relative absolute error: 59.03999999999922
Root relative squared error: 71.68449390674901
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7359173768865114
Weighted FMeasure: 0.8179652468424845
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7629558425699352
Mean absolute error: 0.26239999999999775
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 365.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08978810628439017
Kappa statistic: 0.7301050215340206
Training time: 49.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8292196092621078
Weighted AreaUnderROC: 0.9065965376550968
Root mean squared error: 0.3384058488243379
Relative absolute error: 59.11999999999921
Root relative squared error: 71.78672114906352
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7345495457409082
Weighted FMeasure: 0.8162369912392947
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7621495680643631
Mean absolute error: 0.2627555555555533
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 420.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.09056528822879346
Kappa statistic: 0.7277121608067205
Training time: 49.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8265868802338644
Weighted AreaUnderROC: 0.9052107359145284
Root mean squared error: 0.33932394660669446
Relative absolute error: 59.25999999999926
Root relative squared error: 71.98147909937252
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7304325418104282
Weighted FMeasure: 0.8134200948255179
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7590224418505181
Mean absolute error: 0.2633777777777757
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 475.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09197685380800842
Kappa statistic: 0.7235075887270745
Training time: 50.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8268149595770514
Weighted AreaUnderROC: 0.9045977473508768
Root mean squared error: 0.33962942768280585
Relative absolute error: 59.29999999999923
Root relative squared error: 72.04628142150528
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7298684708228278
Weighted FMeasure: 0.8125577019401425
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7581308614812581
Mean absolute error: 0.2635555555555534
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 533.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09235171219980759
Kappa statistic: 0.7223194470666497
Training time: 52.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.8261936171492363
Weighted AreaUnderROC: 0.9041805022655144
Root mean squared error: 0.3398910500821227
Relative absolute error: 59.33999999999922
Root relative squared error: 72.10177991330545
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7288136292722239
Weighted FMeasure: 0.811676447298172
Iteration time: 61.0
Weighted AreaUnderPRC: 0.757240538446363
Mean absolute error: 0.2637333333333311
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 594.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.09274030317200925
Kappa statistic: 0.7211237764230723
Training time: 55.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8273383393391356
Weighted AreaUnderROC: 0.9042112698227991
Root mean squared error: 0.340021785794406
Relative absolute error: 59.359999999999225
Root relative squared error: 72.12951314591508
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.7291959890770322
Weighted FMeasure: 0.8111528815899383
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7574363244088348
Mean absolute error: 0.26382222222222
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 653.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.09288996777180211
Kappa statistic: 0.7205367804781628
Training time: 54.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8264591353316011
Weighted AreaUnderROC: 0.9032654997828083
Root mean squared error: 0.34041369167746993
Relative absolute error: 59.419999999999234
Root relative squared error: 72.2126489381655
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.727274875358145
Weighted FMeasure: 0.8094290455419169
Iteration time: 60.0
Weighted AreaUnderPRC: 0.7560865022026697
Mean absolute error: 0.2640888888888867
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 713.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09366714971620538
Kappa statistic: 0.7181533696623646
Training time: 55.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8250082027171455
Weighted AreaUnderROC: 0.9028109976286002
Root mean squared error: 0.34080514689210645
Relative absolute error: 59.4799999999992
Root relative squared error: 72.2956891292056
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7253178372234742
Weighted FMeasure: 0.8082531909383067
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7547561200939392
Mean absolute error: 0.26435555555555323
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 778.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09426720190001092
Kappa statistic: 0.7163517631526428
Training time: 60.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8248439202984044
Weighted AreaUnderROC: 0.9023742209109616
Root mean squared error: 0.3411527299981451
Relative absolute error: 59.53999999999922
Root relative squared error: 72.36942264059734
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7245080426344856
Weighted FMeasure: 0.8074298764981751
Iteration time: 64.0
Weighted AreaUnderPRC: 0.7539242712370731
Mean absolute error: 0.26462222222222
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 842.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09464892658201134
Kappa statistic: 0.7151550012545174
Training time: 58.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8263517575346309
Weighted AreaUnderROC: 0.9034571837657771
Root mean squared error: 0.340631222335287
Relative absolute error: 59.45999999999921
Root relative squared error: 72.25879415914302
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7268780695995902
Weighted FMeasure: 0.8089750494337085
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7559789935981978
Mean absolute error: 0.26426666666666443
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 911.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09385114576700437
Kappa statistic: 0.7175575576920171
Training time: 63.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8242598153785179
Weighted AreaUnderROC: 0.9025393705852465
Root mean squared error: 0.3414131851041566
Relative absolute error: 59.5799999999992
Root relative squared error: 72.42467351209396
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7234622199858924
Weighted FMeasure: 0.8065432045395419
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7536676344990652
Mean absolute error: 0.2647999999999977
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 976.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09505125013461542
Kappa statistic: 0.7139608684076076
Training time: 59.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.824471881319901
Weighted AreaUnderROC: 0.9027062449270765
Root mean squared error: 0.3414131851041566
Relative absolute error: 59.5799999999992
Root relative squared error: 72.42467351209396
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7236067797256237
Weighted FMeasure: 0.8065058471034017
Iteration time: 66.0
Weighted AreaUnderPRC: 0.753900033940948
Mean absolute error: 0.2647999999999977
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1042.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09504438384441419
Kappa statistic: 0.7139621720069589
Training time: 61.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8226277180396119
Weighted AreaUnderROC: 0.9016159084716288
Root mean squared error: 0.3420634552351501
Relative absolute error: 59.67999999999919
Root relative squared error: 72.56261663786253
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.720672043328738
Weighted FMeasure: 0.804540467384211
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7516944025025104
Mean absolute error: 0.2652444444444421
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1110.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09604675958082383
Kappa statistic: 0.7109651891750827
Training time: 63.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8226198813990752
Weighted AreaUnderROC: 0.9010527573214531
Root mean squared error: 0.3423232174205941
Relative absolute error: 59.71999999999921
Root relative squared error: 72.6177205187095
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7199524914278432
Weighted FMeasure: 0.80368365234788
Iteration time: 67.0
Weighted AreaUnderPRC: 0.7508804857727155
Mean absolute error: 0.26542222222221995
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1177.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09644908313342791
Kappa statistic: 0.7097632291599205
Training time: 62.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8197377986934704
Weighted AreaUnderROC: 0.8994307442841748
Root mean squared error: 0.3432308369121422
Relative absolute error: 59.859999999999204
Root relative squared error: 72.81025568787274
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7157042679951785
Weighted FMeasure: 0.8008944859963614
Iteration time: 68.0
Weighted AreaUnderPRC: 0.747486230840005
Mean absolute error: 0.26604444444444214
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1245.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09783318355183797
Kappa statistic: 0.7055682736202604
Training time: 63.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8198418477759333
Weighted AreaUnderROC: 0.8989983655338252
Root mean squared error: 0.34361908281420606
Relative absolute error: 59.91999999999922
Root relative squared error: 72.89261508090792
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7147434344757858
Weighted FMeasure: 0.7995279743446195
Iteration time: 72.0
Weighted AreaUnderPRC: 0.7466567654051796
Mean absolute error: 0.2663111111111089
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1317.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.0984126368650398
Kappa statistic: 0.7037733518345064
Training time: 67.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8183508324416171
Weighted AreaUnderROC: 0.8976888953295494
Root mean squared error: 0.3444372759110709
Relative absolute error: 60.03999999999922
Root relative squared error: 73.06618004704185
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7117272281844672
Weighted FMeasure: 0.7970244906892393
Iteration time: 67.0
Weighted AreaUnderPRC: 0.7440045378571963
Mean absolute error: 0.2668444444444422
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1384.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.09958527607184596
Kappa statistic: 0.700185021819335
Training time: 62.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8201167928568166
Weighted AreaUnderROC: 0.8982232738327126
Root mean squared error: 0.3439207488382805
Relative absolute error: 59.95999999999922
Root relative squared error: 72.95660810829091
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.714240263416941
Weighted FMeasure: 0.7986085450420815
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7455950191595774
Mean absolute error: 0.26648888888888667
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1457.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09878062896663778
Kappa statistic: 0.7025848970212198
Training time: 68.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8183589114349463
Weighted AreaUnderROC: 0.8966529155352345
Root mean squared error: 0.3448671241700715
Relative absolute error: 60.099999999999206
Root relative squared error: 73.15736463268803
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.7106670681604799
Weighted FMeasure: 0.7957617347929087
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7425509632278137
Mean absolute error: 0.26711111111110886
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1526.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.1001578630948466
Kappa statistic: 0.698398502041896
Training time: 64.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8176763768997556
Weighted AreaUnderROC: 0.8964529856436715
Root mean squared error: 0.3449959742116194
Relative absolute error: 60.1199999999992
Root relative squared error: 73.18469785412844
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.709922506294159
Weighted FMeasure: 0.7952977758605415
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7419861277180093
Mean absolute error: 0.2671999999999977
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1600.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10036932430645049
Kappa statistic: 0.6977961423917421
Training time: 70.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.24
Correctly Classified Instances: 79.76
Weighted Precision: 0.8169008257434384
Weighted AreaUnderROC: 0.8959884319633759
Root mean squared error: 0.34525353003264453
Relative absolute error: 60.1599999999992
Root relative squared error: 73.23933369440272
Weighted TruePositiveRate: 0.7976
Weighted MatthewsCorrelation: 0.7087608023837851
Weighted FMeasure: 0.7944438940154775
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7410199386901675
Mean absolute error: 0.2673777777777755
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 1673.0
Weighted Recall: 0.7976
Weighted FalsePositiveRate: 0.10076478156885335
Kappa statistic: 0.6965983033489311
Training time: 69.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.24
Correctly Classified Instances: 79.76
Weighted Precision: 0.8169008257434384
Weighted AreaUnderROC: 0.8960185572695581
Root mean squared error: 0.34525353003264453
Relative absolute error: 60.1599999999992
Root relative squared error: 73.23933369440272
Weighted TruePositiveRate: 0.7976
Weighted MatthewsCorrelation: 0.7087608023837851
Weighted FMeasure: 0.7944438940154775
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7410453978387656
Mean absolute error: 0.2673777777777755
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 1749.0
Weighted Recall: 0.7976
Weighted FalsePositiveRate: 0.10076478156885335
Kappa statistic: 0.6965983033489311
Training time: 72.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8176852162225108
Weighted AreaUnderROC: 0.8962911217422165
Root mean squared error: 0.3449959742116194
Relative absolute error: 60.11999999999922
Root relative squared error: 73.18469785412844
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.7099606713642724
Weighted FMeasure: 0.7952223188665711
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7418038360328217
Mean absolute error: 0.2671999999999978
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1828.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10036932430645049
Kappa statistic: 0.6977975195412278
Training time: 75.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8179454264468815
Weighted AreaUnderROC: 0.896392974700973
Root mean squared error: 0.3449959742116194
Relative absolute error: 60.11999999999919
Root relative squared error: 73.18469785412844
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.7101273491617851
Weighted FMeasure: 0.79518848445389
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7419947201548391
Mean absolute error: 0.26719999999999766
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1909.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10036932430645049
Kappa statistic: 0.6977975195412278
Training time: 76.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.24
Correctly Classified Instances: 79.76
Weighted Precision: 0.8181958524374177
Weighted AreaUnderROC: 0.8960283600191098
Root mean squared error: 0.34525353003264453
Relative absolute error: 60.1599999999992
Root relative squared error: 73.23933369440272
Weighted TruePositiveRate: 0.7976
Weighted MatthewsCorrelation: 0.7095580377020582
Weighted FMeasure: 0.7943427850488247
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7415079629326666
Mean absolute error: 0.2673777777777755
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1985.0
Weighted Recall: 0.7976
Weighted FalsePositiveRate: 0.10076478156885335
Kappa statistic: 0.6965983033489311
Training time: 72.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.8160424476032686
Weighted AreaUnderROC: 0.894881642575625
Root mean squared error: 0.3460250472629626
Relative absolute error: 60.27999999999921
Root relative squared error: 73.40299721401077
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.7060667766962501
Weighted FMeasure: 0.7920342290510118
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7389975694516501
Mean absolute error: 0.2679111111111089
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2066.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.10194428706586073
Kappa statistic: 0.6930020537682918
Training time: 77.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.8158292595520423
Weighted AreaUnderROC: 0.8945758818304228
Root mean squared error: 0.3460250472629626
Relative absolute error: 60.27999999999921
Root relative squared error: 73.40299721401077
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.7059512056146319
Weighted FMeasure: 0.7920040716878093
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7386557998446355
Mean absolute error: 0.2679111111111089
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2151.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.10195115335606196
Kappa statistic: 0.693000654772041
Training time: 80.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8177973521292224
Weighted AreaUnderROC: 0.8950797628021981
Root mean squared error: 0.34576806613040156
Relative absolute error: 60.23999999999929
Root relative squared error: 73.3484832835695
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7077466602882969
Weighted FMeasure: 0.7928596044521817
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7400867042601112
Mean absolute error: 0.26773333333333144
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2235.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10159002754466522
Kappa statistic: 0.6941803601907739
Training time: 79.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.8165501227997627
Weighted AreaUnderROC: 0.8943733251130589
Root mean squared error: 0.3461534662865944
Relative absolute error: 60.29999999999932
Root relative squared error: 73.43023900274378
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.7058339348093932
Weighted FMeasure: 0.7915523335689375
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7385845517749247
Mean absolute error: 0.26799999999999824
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2320.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10225187634028178
Kappa statistic: 0.6923575867497633
Training time: 81.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.44
Correctly Classified Instances: 79.56
Weighted Precision: 0.8170980627767194
Weighted AreaUnderROC: 0.894989227657995
Root mean squared error: 0.3458965805619458
Relative absolute error: 60.2599999999993
Root relative squared error: 73.37574531137707
Weighted TruePositiveRate: 0.7956
Weighted MatthewsCorrelation: 0.7067657350652347
Weighted FMeasure: 0.7926119321317837
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7396806676512343
Mean absolute error: 0.2678222222222204
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2410.0
Weighted Recall: 0.7956
Weighted FalsePositiveRate: 0.10186328536808015
Kappa statistic: 0.6935527823248441
Training time: 85.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.820699047956012
Weighted AreaUnderROC: 0.8977607459248568
Root mean squared error: 0.34422215049135213
Relative absolute error: 59.99999999999937
Root relative squared error: 73.02054505411523
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.713167958962014
Weighted FMeasure: 0.7974022001164475
Iteration time: 83.0
Weighted AreaUnderPRC: 0.7451029632745704
Mean absolute error: 0.2666666666666651
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2493.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09951800695446782
Kappa statistic: 0.7007232780672985
Training time: 79.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8205874362922428
Weighted AreaUnderROC: 0.8978785126314442
Root mean squared error: 0.34422215049135213
Relative absolute error: 59.999999999999396
Root relative squared error: 73.02054505411523
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7130839361147827
Weighted FMeasure: 0.7972881169776042
Iteration time: 88.0
Weighted AreaUnderPRC: 0.7451009350035012
Mean absolute error: 0.2666666666666652
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2581.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.0995386058250715
Kappa statistic: 0.7007137287830625
Training time: 84.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8227527909996785
Weighted AreaUnderROC: 0.8994526667586992
Root mean squared error: 0.34353284386622635
Relative absolute error: 59.89999999999942
Root relative squared error: 72.87432103743225
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7160171403035347
Weighted FMeasure: 0.799310441225554
Iteration time: 80.0
Weighted AreaUnderPRC: 0.7479606344035779
Mean absolute error: 0.2662222222222209
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2661.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09858429412007044
Kappa statistic: 0.703692329470137
Training time: 76.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8234630187011442
Weighted AreaUnderROC: 0.8994311491061897
Root mean squared error: 0.34366219417286203
Relative absolute error: 59.91999999999949
Root relative squared error: 72.90176038112345
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7153492400618213
Weighted FMeasure: 0.7982747416175225
Iteration time: 97.0
Weighted AreaUnderPRC: 0.7477410354756792
Mean absolute error: 0.2663111111111101
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2758.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.0990827457354917
Kappa statistic: 0.702454724527065
Training time: 92.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.824429231772672
Weighted AreaUnderROC: 0.9001734530428869
Root mean squared error: 0.3432739970301265
Relative absolute error: 59.85999999999952
Root relative squared error: 72.8194113315038
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7169215895830533
Weighted FMeasure: 0.7995087250071796
Iteration time: 96.0
Weighted AreaUnderPRC: 0.7491409184899048
Mean absolute error: 0.2660444444444436
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2854.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09850329242228986
Kappa statistic: 0.7042436063226837
Training time: 92.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8249497371665336
Weighted AreaUnderROC: 0.9007455931788105
Root mean squared error: 0.3428853603908639
Relative absolute error: 59.79999999999957
Root relative squared error: 72.73696905059177
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7182535782333571
Weighted FMeasure: 0.8003098142891185
Iteration time: 97.0
Weighted AreaUnderPRC: 0.7499426350301843
Mean absolute error: 0.2657777777777771
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2951.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09796503685029535
Kappa statistic: 0.7060178741132539
Training time: 93.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8236795516182117
Weighted AreaUnderROC: 0.900008880932023
Root mean squared error: 0.3434034448370141
Relative absolute error: 59.87999999999956
Root relative squared error: 72.84687135812179
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7159760877304108
Weighted FMeasure: 0.7985648041399304
Iteration time: 108.0
Weighted AreaUnderPRC: 0.7482756256170847
Mean absolute error: 0.2661333333333326
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3059.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09879028282610723
Kappa statistic: 0.7036099112845666
Training time: 104.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8239536526471724
Weighted AreaUnderROC: 0.8999336218379831
Root mean squared error: 0.34357596604600665
Relative absolute error: 59.89999999999958
Root relative squared error: 72.88346863315492
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7156246512238408
Weighted FMeasure: 0.7981023435138477
Iteration time: 100.0
Weighted AreaUnderPRC: 0.7479652551211715
Mean absolute error: 0.2662222222222216
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3159.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09902234290831478
Kappa statistic: 0.7029990971172553
Training time: 96.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8260846102785047
Weighted AreaUnderROC: 0.9008506208210493
Root mean squared error: 0.3433171517222362
Relative absolute error: 59.8599999999996
Root relative squared error: 72.82856582413302
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7173011519885136
Weighted FMeasure: 0.798736359609146
Iteration time: 107.0
Weighted AreaUnderPRC: 0.7497616237655341
Mean absolute error: 0.2660444444444439
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3266.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09868181596752172
Kappa statistic: 0.704174819360624
Training time: 103.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8273702264434633
Weighted AreaUnderROC: 0.9012778397350046
Root mean squared error: 0.34305814214049957
Relative absolute error: 59.819999999999595
Root relative squared error: 72.77362159464155
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7185604807033322
Weighted FMeasure: 0.7995047425579836
Iteration time: 117.0
Weighted AreaUnderPRC: 0.7508195731655832
Mean absolute error: 0.26586666666666614
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3383.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09832755644632621
Kappa statistic: 0.7053547660636861
Training time: 113.0
		
Time end:Sun Oct 08 11.25.31 EEST 2017