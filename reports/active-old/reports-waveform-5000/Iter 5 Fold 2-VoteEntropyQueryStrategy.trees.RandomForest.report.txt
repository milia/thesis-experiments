Sun Oct 08 12.23.47 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.23.47 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.53333333333482
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.7840779897999082
Weighted AreaUnderROC: 0.9275335519219446
Root mean squared error: 0.3223827952398602
Relative absolute error: 51.64199999999901
Root relative squared error: 68.38771819559365
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.674946247660138
Weighted FMeasure: 0.7834730100111755
Iteration time: 94.0
Weighted AreaUnderPRC: 0.8334175485773816
Mean absolute error: 0.22951999999999667
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10873013421459109
Kappa statistic: 0.6747293772423869
Training time: 86.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 69.24000000000129
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7847640382174552
Weighted AreaUnderROC: 0.9328035937042567
Root mean squared error: 0.33481935427928783
Relative absolute error: 51.629999999999285
Root relative squared error: 71.02591076501548
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.6370187819862069
Weighted FMeasure: 0.739419078543843
Iteration time: 151.0
Weighted AreaUnderPRC: 0.8471297060443367
Mean absolute error: 0.22946666666666457
Coverage of cases: 99.48
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 245.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.1310639527252137
Kappa statistic: 0.6115842648095825
Training time: 143.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 68.53333333333454
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7686116764643144
Weighted AreaUnderROC: 0.9228834242957803
Root mean squared error: 0.35732338294603605
Relative absolute error: 54.00599999999947
Root relative squared error: 75.79973614729772
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.5892615196546642
Weighted FMeasure: 0.6948552469820688
Iteration time: 210.0
Weighted AreaUnderPRC: 0.8244273738133248
Mean absolute error: 0.24002666666666542
Coverage of cases: 98.6
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 455.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.15247692464271914
Kappa statistic: 0.5487638294861539
Training time: 203.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 67.42666666666783
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7725245891665374
Weighted AreaUnderROC: 0.9322647323589932
Root mean squared error: 0.33855378696252414
Relative absolute error: 50.79599999999929
Root relative squared error: 71.81810356727581
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.6102901591804827
Weighted FMeasure: 0.713730553176532
Iteration time: 279.0
Weighted AreaUnderPRC: 0.8467902751455036
Mean absolute error: 0.2257599999999979
Coverage of cases: 99.64
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 734.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.14083588289815516
Kappa statistic: 0.5821424895313165
Training time: 275.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 67.29333333333445
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7722183388600774
Weighted AreaUnderROC: 0.9308955398000675
Root mean squared error: 0.3426212680691802
Relative absolute error: 51.65999999999926
Root relative squared error: 72.6809466091352
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.6097299804789532
Weighted FMeasure: 0.7063285513280493
Iteration time: 351.0
Weighted AreaUnderPRC: 0.8423799961278124
Mean absolute error: 0.22959999999999778
Coverage of cases: 99.24
Instances selection time: 4.0
Test time: 16.0
Accumulative iteration time: 1085.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13924527690826194
Kappa statistic: 0.5853849766589768
Training time: 347.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 66.66666666666809
Incorrectly Classified Instances: 23.72
Correctly Classified Instances: 76.28
Weighted Precision: 0.7841004999297746
Weighted AreaUnderROC: 0.9374385131376711
Root mean squared error: 0.318249378108844
Relative absolute error: 47.67599999999886
Root relative squared error: 67.5108880107494
Weighted TruePositiveRate: 0.7628
Weighted MatthewsCorrelation: 0.6551456811365457
Weighted FMeasure: 0.7578841259728206
Iteration time: 657.0
Weighted AreaUnderPRC: 0.8585874376788791
Mean absolute error: 0.21189333333332924
Coverage of cases: 99.68
Instances selection time: 2.0
Test time: 16.0
Accumulative iteration time: 1742.0
Weighted Recall: 0.7628
Weighted FalsePositiveRate: 0.11931122684794208
Kappa statistic: 0.6439297284210609
Training time: 655.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 66.16000000000139
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8094636786975656
Weighted AreaUnderROC: 0.9466350452095008
Root mean squared error: 0.2995329698046597
Relative absolute error: 44.7119999999987
Root relative squared error: 63.54053824134593
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7095046874168284
Weighted FMeasure: 0.8046500876708343
Iteration time: 512.0
Weighted AreaUnderPRC: 0.8785949052988202
Mean absolute error: 0.19871999999999515
Coverage of cases: 99.84
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 2254.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.097898538932069
Kappa statistic: 0.7071133524141782
Training time: 510.0
		
Time end:Sun Oct 08 12.23.50 EEST 2017