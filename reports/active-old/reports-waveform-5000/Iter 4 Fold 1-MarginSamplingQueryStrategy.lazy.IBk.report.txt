Sun Oct 08 11.40.52 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.40.52 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7028250058351176
Weighted AreaUnderROC: 0.7763944557982472
Root mean squared error: 0.444398110783266
Relative absolute error: 45.02982107355844
Root relative squared error: 94.27107530440117
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5532641527245132
Weighted FMeasure: 0.7019127565808656
Iteration time: 863.0
Weighted AreaUnderPRC: 0.5927310474964158
Mean absolute error: 0.2001325381047051
Coverage of cases: 70.2
Instances selection time: 863.0
Test time: 1079.0
Accumulative iteration time: 863.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.1492110884035056
Kappa statistic: 0.5529563685415696
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.48
Correctly Classified Instances: 70.52
Weighted Precision: 0.7066068776900781
Weighted AreaUnderROC: 0.7787600894302659
Root mean squared error: 0.4420557690111942
Relative absolute error: 44.539961759080946
Root relative squared error: 93.77418957913463
Weighted TruePositiveRate: 0.7052
Weighted MatthewsCorrelation: 0.5583191849476206
Weighted FMeasure: 0.7051722550490722
Iteration time: 883.0
Weighted AreaUnderPRC: 0.5963250582875412
Mean absolute error: 0.19795538559591622
Coverage of cases: 70.52
Instances selection time: 883.0
Test time: 1117.0
Accumulative iteration time: 1746.0
Weighted Recall: 0.7052
Weighted FalsePositiveRate: 0.1476798211394681
Kappa statistic: 0.5577169215465433
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.72
Correctly Classified Instances: 70.28
Weighted Precision: 0.7070419666097825
Weighted AreaUnderROC: 0.7768368436010854
Root mean squared error: 0.44389802563901143
Relative absolute error: 44.886187845304306
Root relative squared error: 94.16499122539925
Weighted TruePositiveRate: 0.7028
Weighted MatthewsCorrelation: 0.5560933797042972
Weighted FMeasure: 0.7026684770488042
Iteration time: 904.0
Weighted AreaUnderPRC: 0.5944400419736693
Mean absolute error: 0.1994941682013534
Coverage of cases: 70.28
Instances selection time: 904.0
Test time: 1154.0
Accumulative iteration time: 2650.0
Weighted Recall: 0.7028
Weighted FalsePositiveRate: 0.1491263127978291
Kappa statistic: 0.5539738468993318
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.708865174339301
Weighted AreaUnderROC: 0.777396987664118
Root mean squared error: 0.4433434098270318
Relative absolute error: 44.75595026642912
Root relative squared error: 94.04733944491804
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5578293188925666
Weighted FMeasure: 0.7033856163473944
Iteration time: 924.0
Weighted AreaUnderPRC: 0.5955262395481672
Mean absolute error: 0.19891533451746368
Coverage of cases: 70.36
Instances selection time: 924.0
Test time: 1192.0
Accumulative iteration time: 3574.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14880602467176396
Kappa statistic: 0.5551259575898754
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.56
Correctly Classified Instances: 70.44
Weighted Precision: 0.710554511837759
Weighted AreaUnderROC: 0.7779672522022959
Root mean squared error: 0.4427849204303923
Relative absolute error: 44.62641509433871
Root relative squared error: 93.92886595304266
Weighted TruePositiveRate: 0.7044
Weighted MatthewsCorrelation: 0.5595156843767615
Weighted FMeasure: 0.7040718307752307
Iteration time: 945.0
Weighted AreaUnderPRC: 0.5965798047015786
Mean absolute error: 0.1983396226415063
Coverage of cases: 70.44
Instances selection time: 945.0
Test time: 1230.0
Accumulative iteration time: 4519.0
Weighted Recall: 0.7044
Weighted FalsePositiveRate: 0.14846549559540814
Kappa statistic: 0.5562902551012812
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.88
Correctly Classified Instances: 70.12
Weighted Precision: 0.7095683798076163
Weighted AreaUnderROC: 0.7755004527005264
Root mean squared error: 0.44521282061255835
Relative absolute error: 45.09452736318456
Root relative squared error: 94.44390135789875
Weighted TruePositiveRate: 0.7012
Weighted MatthewsCorrelation: 0.5559070059659706
Weighted FMeasure: 0.7006319638626365
Iteration time: 964.0
Weighted AreaUnderPRC: 0.5936960152930014
Mean absolute error: 0.20042012161415454
Coverage of cases: 70.12
Instances selection time: 964.0
Test time: 1268.0
Accumulative iteration time: 5483.0
Weighted Recall: 0.7012
Weighted FalsePositiveRate: 0.1501990945989472
Kappa statistic: 0.5514129950190629
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.16
Correctly Classified Instances: 69.84
Weighted Precision: 0.7082811500686783
Weighted AreaUnderROC: 0.7733511119313251
Root mean squared error: 0.4473294102717448
Relative absolute error: 45.50369181380491
Root relative squared error: 94.89289782819877
Weighted TruePositiveRate: 0.6984
Weighted MatthewsCorrelation: 0.5525718561282165
Weighted FMeasure: 0.6976326697866052
Iteration time: 983.0
Weighted AreaUnderPRC: 0.5910678638402467
Mean absolute error: 0.20223863028357833
Coverage of cases: 69.84
Instances selection time: 983.0
Test time: 1304.0
Accumulative iteration time: 6466.0
Weighted Recall: 0.6984
Weighted FalsePositiveRate: 0.1516977761373501
Kappa statistic: 0.5471547625625516
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.7095602162749068
Weighted AreaUnderROC: 0.773005078171542
Root mean squared error: 0.4476592720781254
Relative absolute error: 45.55520995334505
Root relative squared error: 94.96287208424262
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.5529846594514795
Weighted FMeasure: 0.6969703569123005
Iteration time: 1001.0
Weighted AreaUnderPRC: 0.591029726921943
Mean absolute error: 0.20246759979264561
Coverage of cases: 69.8
Instances selection time: 1001.0
Test time: 1343.0
Accumulative iteration time: 7467.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.15198984365691612
Kappa statistic: 0.5464996984673491
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.7057619449245701
Weighted AreaUnderROC: 0.7690683451057797
Root mean squared error: 0.45152835246395323
Relative absolute error: 46.32398190045309
Root relative squared error: 95.78362797757504
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.545947954657387
Weighted FMeasure: 0.6916017415248207
Iteration time: 1018.0
Weighted AreaUnderPRC: 0.5858775005325445
Mean absolute error: 0.2058843640020147
Coverage of cases: 69.28
Instances selection time: 1018.0
Test time: 1393.0
Accumulative iteration time: 8485.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.1546633097884406
Kappa statistic: 0.538655170209811
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.7079737747944639
Weighted AreaUnderROC: 0.7705632031371402
Root mean squared error: 0.4500858248903612
Relative absolute error: 46.01815519765768
Root relative squared error: 95.47762166877439
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.5491350095924562
Weighted FMeasure: 0.6934619225587314
Iteration time: 1040.0
Weighted AreaUnderPRC: 0.5879929498912196
Mean absolute error: 0.20452513421181287
Coverage of cases: 69.48
Instances selection time: 1040.0
Test time: 1420.0
Accumulative iteration time: 9525.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.15367359372571945
Kappa statistic: 0.5416485849135945
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.7086769501262877
Weighted AreaUnderROC: 0.7699282467903016
Root mean squared error: 0.45070333047978006
Relative absolute error: 46.13086770981284
Root relative squared error: 95.60861438568399
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5488689934770519
Weighted FMeasure: 0.6923258205997617
Iteration time: 1051.0
Weighted AreaUnderPRC: 0.5874636612076681
Mean absolute error: 0.20502607871028025
Coverage of cases: 69.4
Instances selection time: 1051.0
Test time: 1459.0
Accumulative iteration time: 10576.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.15414350641939673
Kappa statistic: 0.5404056157747691
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.7052190323635725
Weighted AreaUnderROC: 0.766294168464885
Root mean squared error: 0.45425111451372896
Relative absolute error: 46.84149377593518
Root relative squared error: 96.36121303026118
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5424224396052807
Weighted FMeasure: 0.6874585815496191
Iteration time: 1067.0
Weighted AreaUnderPRC: 0.5827928821437804
Mean absolute error: 0.20818441678193508
Coverage of cases: 68.92
Instances selection time: 1067.0
Test time: 1778.0
Accumulative iteration time: 11643.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.15661166307023017
Kappa statistic: 0.53316353383549
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.7071033077657998
Weighted AreaUnderROC: 0.7671808198966983
Root mean squared error: 0.45339854947971997
Relative absolute error: 46.65625841184266
Root relative squared error: 96.1803566751761
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.544693432375241
Weighted FMeasure: 0.6884239149970195
Iteration time: 1083.0
Weighted AreaUnderPRC: 0.5841629201612475
Mean absolute error: 0.20736114849707946
Coverage of cases: 69.04
Instances selection time: 1082.0
Test time: 1535.0
Accumulative iteration time: 12726.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.15603836020660347
Kappa statistic: 0.5349469911671172
Training time: 1.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.7077302953072895
Weighted AreaUnderROC: 0.7671674450230525
Root mean squared error: 0.453422432184197
Relative absolute error: 46.65058977719326
Root relative squared error: 96.18542296186271
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.5451064144177842
Weighted FMeasure: 0.688271220531166
Iteration time: 1345.0
Weighted AreaUnderPRC: 0.584292739596536
Mean absolute error: 0.20733595456530435
Coverage of cases: 69.04
Instances selection time: 1345.0
Test time: 1570.0
Accumulative iteration time: 14071.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.1560651099538949
Kappa statistic: 0.5349304486683388
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.7058981635012738
Weighted AreaUnderROC: 0.7638176057676143
Root mean squared error: 0.45665583548194766
Relative absolute error: 47.302681992336296
Root relative squared error: 96.87133138130785
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.5400201872037642
Weighted FMeasure: 0.6832466935693161
Iteration time: 1107.0
Weighted AreaUnderPRC: 0.5802135019976608
Mean absolute error: 0.21023414218816233
Coverage of cases: 68.6
Instances selection time: 1107.0
Test time: 1609.0
Accumulative iteration time: 15178.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.15836478846477145
Kappa statistic: 0.5282653771064604
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.7057040582321938
Weighted AreaUnderROC: 0.7625939692122307
Root mean squared error: 0.45783954546305344
Relative absolute error: 47.53673723536816
Root relative squared error: 97.12243418768729
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5384374269713955
Weighted FMeasure: 0.6813716576403448
Iteration time: 1120.0
Weighted AreaUnderPRC: 0.5788607872380482
Mean absolute error: 0.21127438771274834
Coverage of cases: 68.44
Instances selection time: 1120.0
Test time: 1650.0
Accumulative iteration time: 16298.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.1592120615755387
Kappa statistic: 0.5258349631629656
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.88
Correctly Classified Instances: 68.12
Weighted Precision: 0.7044625210071171
Weighted AreaUnderROC: 0.7601553485763294
Root mean squared error: 0.46017557464316705
Relative absolute error: 48.010206561361535
Root relative squared error: 97.61798080997967
Weighted TruePositiveRate: 0.6812
Weighted MatthewsCorrelation: 0.5348452214011581
Weighted FMeasure: 0.6777514142121785
Iteration time: 1132.0
Weighted AreaUnderPRC: 0.5759657109267621
Mean absolute error: 0.21337869582827448
Coverage of cases: 68.12
Instances selection time: 1132.0
Test time: 2158.0
Accumulative iteration time: 17430.0
Weighted Recall: 0.6812
Weighted FalsePositiveRate: 0.1608893028473413
Kappa statistic: 0.5209839098675633
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.7047379737130339
Weighted AreaUnderROC: 0.7592282152439348
Root mean squared error: 0.4610606839724549
Relative absolute error: 48.185053380783565
Root relative squared error: 97.80574085262896
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.5339973124707673
Weighted FMeasure: 0.6762642387728433
Iteration time: 1141.0
Weighted AreaUnderPRC: 0.5750601300391176
Mean absolute error: 0.21415579280348349
Coverage of cases: 68.0
Instances selection time: 1141.0
Test time: 1718.0
Accumulative iteration time: 18571.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.16154356951213059
Kappa statistic: 0.5191489320778633
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7044065789321172
Weighted AreaUnderROC: 0.7580027534086275
Root mean squared error: 0.46223087124081413
Relative absolute error: 48.41993047508814
Root relative squared error: 98.05397505844346
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5324492935808581
Weighted FMeasure: 0.6743208030649884
Iteration time: 1152.0
Weighted AreaUnderPRC: 0.5736607226129788
Mean absolute error: 0.21519969100039274
Coverage of cases: 67.84
Instances selection time: 1152.0
Test time: 1968.0
Accumulative iteration time: 19723.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16239449318274493
Kappa statistic: 0.5167146986535816
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7051953752946216
Weighted AreaUnderROC: 0.7579893785349819
Root mean squared error: 0.4622490014096971
Relative absolute error: 48.415855039638316
Root relative squared error: 98.0578210480518
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5329927637577424
Weighted FMeasure: 0.6740989823878241
Iteration time: 1162.0
Weighted AreaUnderPRC: 0.573815725884159
Mean absolute error: 0.2151815779539491
Coverage of cases: 67.84
Instances selection time: 1162.0
Test time: 2023.0
Accumulative iteration time: 20885.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16242124293003635
Kappa statistic: 0.5166975002440557
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7057459373852102
Weighted AreaUnderROC: 0.7579821551786927
Root mean squared error: 0.46226633059096495
Relative absolute error: 48.4119601328905
Root relative squared error: 98.06149712252787
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5333302893392733
Weighted FMeasure: 0.6739818203023368
Iteration time: 1168.0
Weighted AreaUnderPRC: 0.5739372193887279
Mean absolute error: 0.21516426725729212
Coverage of cases: 67.84
Instances selection time: 1168.0
Test time: 1830.0
Accumulative iteration time: 22053.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16243568964261468
Kappa statistic: 0.5166884357248519
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.48
Correctly Classified Instances: 67.52
Weighted Precision: 0.7037383514326835
Weighted AreaUnderROC: 0.7555601638149375
Root mean squared error: 0.46457710766259946
Relative absolute error: 48.88667388949112
Root relative squared error: 98.55168696367681
Weighted TruePositiveRate: 0.6752
Weighted MatthewsCorrelation: 0.5292196387164758
Weighted FMeasure: 0.6705081767529787
Iteration time: 1190.0
Weighted AreaUnderPRC: 0.5708958275938231
Mean absolute error: 0.21727410617551712
Coverage of cases: 67.52
Instances selection time: 1190.0
Test time: 1872.0
Accumulative iteration time: 23243.0
Weighted Recall: 0.6752
Weighted FalsePositiveRate: 0.16407967237012513
Kappa statistic: 0.5118565945288502
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.44
Correctly Classified Instances: 67.56
Weighted Precision: 0.7051353955631383
Weighted AreaUnderROC: 0.7558454747238486
Root mean squared error: 0.46430690150845383
Relative absolute error: 48.823329798515104
Root relative squared error: 98.49436758250242
Weighted TruePositiveRate: 0.6756
Weighted MatthewsCorrelation: 0.5305041187029947
Weighted FMeasure: 0.6706297340714659
Iteration time: 1187.0
Weighted AreaUnderPRC: 0.5715029905289664
Mean absolute error: 0.21699257688229037
Coverage of cases: 67.56
Instances selection time: 1187.0
Test time: 1914.0
Accumulative iteration time: 24430.0
Weighted Recall: 0.6756
Weighted FalsePositiveRate: 0.163909050552303
Kappa statistic: 0.512438059794452
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.56
Correctly Classified Instances: 67.44
Weighted Precision: 0.7045769847668082
Weighted AreaUnderROC: 0.7549349706635999
Root mean squared error: 0.46518018354195323
Relative absolute error: 48.99937694704014
Root relative squared error: 98.67961867683515
Weighted TruePositiveRate: 0.6744
Weighted MatthewsCorrelation: 0.5290823632730906
Weighted FMeasure: 0.6692678967994488
Iteration time: 1189.0
Weighted AreaUnderPRC: 0.5704010140316804
Mean absolute error: 0.21777500865351276
Coverage of cases: 67.44
Instances selection time: 1189.0
Test time: 1944.0
Accumulative iteration time: 25619.0
Weighted Recall: 0.6744
Weighted FalsePositiveRate: 0.16453005867280016
Kappa statistic: 0.5106229685442392
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.88
Correctly Classified Instances: 67.12
Weighted Precision: 0.7024643737486095
Weighted AreaUnderROC: 0.7525133365794889
Root mean squared error: 0.46747523703517885
Relative absolute error: 49.474669379449956
Root relative squared error: 99.16647304330886
Weighted TruePositiveRate: 0.6712
Weighted MatthewsCorrelation: 0.5249414228031779
Weighted FMeasure: 0.6659058823189058
Iteration time: 1198.0
Weighted AreaUnderPRC: 0.567402069442698
Mean absolute error: 0.21988741946422305
Coverage of cases: 67.12
Instances selection time: 1198.0
Test time: 1992.0
Accumulative iteration time: 26817.0
Weighted Recall: 0.6712
Weighted FalsePositiveRate: 0.16617332684102215
Kappa statistic: 0.505791736068257
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.7005870884190085
Weighted AreaUnderROC: 0.7500884480968776
Root mean squared error: 0.4697587757870972
Relative absolute error: 49.95014955134667
Root relative squared error: 99.65088476428399
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.5209160055505574
Weighted FMeasure: 0.6625066579809156
Iteration time: 1207.0
Weighted AreaUnderPRC: 0.5644762346939193
Mean absolute error: 0.22200066467265292
Coverage of cases: 66.8
Instances selection time: 1207.0
Test time: 2032.0
Accumulative iteration time: 28024.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.16782310380624493
Kappa statistic: 0.5009574402074863
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.48
Correctly Classified Instances: 66.52
Weighted Precision: 0.6986013528229952
Weighted AreaUnderROC: 0.7479723658719682
Root mean squared error: 0.4717492613930009
Relative absolute error: 50.365982404691664
Root relative squared error: 100.07313052522059
Weighted TruePositiveRate: 0.6652
Weighted MatthewsCorrelation: 0.5171842989210335
Weighted FMeasure: 0.6595398914481331
Iteration time: 1211.0
Weighted AreaUnderPRC: 0.5618263997875667
Mean absolute error: 0.22384881068751955
Coverage of cases: 66.52
Instances selection time: 1211.0
Test time: 2070.0
Accumulative iteration time: 29235.0
Weighted Recall: 0.6652
Weighted FalsePositiveRate: 0.16925526825606357
Kappa statistic: 0.4967326706479101
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.88
Correctly Classified Instances: 66.12
Weighted Precision: 0.6957299781298153
Weighted AreaUnderROC: 0.744949391264955
Root mean squared error: 0.47457227134292607
Relative absolute error: 50.96145733461277
Root relative squared error: 100.67198136890535
Weighted TruePositiveRate: 0.6612
Weighted MatthewsCorrelation: 0.5118422311776981
Weighted FMeasure: 0.6553705451952427
Iteration time: 1216.0
Weighted AreaUnderPRC: 0.5581004993576493
Mean absolute error: 0.22649536593161337
Coverage of cases: 66.12
Instances selection time: 1216.0
Test time: 2273.0
Accumulative iteration time: 30451.0
Weighted Recall: 0.6612
Weighted FalsePositiveRate: 0.1713012174700902
Kappa statistic: 0.49069762519578375
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6954947493219249
Weighted AreaUnderROC: 0.7440352755265618
Root mean squared error: 0.47542479706209567
Relative absolute error: 51.13828786453575
Root relative squared error: 100.85282938405358
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.510625156611264
Weighted FMeasure: 0.6539335807050093
Iteration time: 1219.0
Weighted AreaUnderPRC: 0.5570976420429664
Mean absolute error: 0.2272812793979377
Coverage of cases: 66.0
Instances selection time: 1219.0
Test time: 2556.0
Accumulative iteration time: 31670.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.17192944894687645
Kappa statistic: 0.4888770014373982
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.12
Correctly Classified Instances: 65.88
Weighted Precision: 0.6955939237199212
Weighted AreaUnderROC: 0.7431175481100241
Root mean squared error: 0.4762754143225872
Relative absolute error: 51.31523545706492
Root relative squared error: 101.03327255397994
Weighted TruePositiveRate: 0.6588
Weighted MatthewsCorrelation: 0.5096135130516266
Weighted FMeasure: 0.6524048146201099
Iteration time: 1218.0
Weighted AreaUnderPRC: 0.5561596874038394
Mean absolute error: 0.2280677131425118
Coverage of cases: 65.88
Instances selection time: 1218.0
Test time: 2174.0
Accumulative iteration time: 32888.0
Weighted Recall: 0.6588
Weighted FalsePositiveRate: 0.17256490377995193
Kappa statistic: 0.4870513232512359
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.52
Correctly Classified Instances: 65.48
Weighted Precision: 0.6943045955589914
Weighted AreaUnderROC: 0.740076872391932
Root mean squared error: 0.4790710260108573
Relative absolute error: 51.91115140525726
Root relative squared error: 101.626311348682
Weighted TruePositiveRate: 0.6548
Weighted MatthewsCorrelation: 0.505264439712519
Weighted FMeasure: 0.6476729498841239
Iteration time: 1222.0
Weighted AreaUnderPRC: 0.5527044457129627
Mean absolute error: 0.23071622846781112
Coverage of cases: 65.48
Instances selection time: 1222.0
Test time: 2219.0
Accumulative iteration time: 34110.0
Weighted Recall: 0.6548
Weighted FalsePositiveRate: 0.17464625521613594
Kappa statistic: 0.48098966790555586
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.6
Correctly Classified Instances: 65.4
Weighted Precision: 0.6940983666499156
Weighted AreaUnderROC: 0.7394686657923849
Root mean squared error: 0.4796374109238763
Relative absolute error: 52.02849510240388
Root relative squared error: 101.74645973250924
Weighted TruePositiveRate: 0.654
Weighted MatthewsCorrelation: 0.504415215600058
Weighted FMeasure: 0.6466881415888218
Iteration time: 1223.0
Weighted AreaUnderPRC: 0.5520086411313828
Mean absolute error: 0.23123775601068497
Coverage of cases: 65.4
Instances selection time: 1223.0
Test time: 2250.0
Accumulative iteration time: 35333.0
Weighted Recall: 0.654
Weighted FalsePositiveRate: 0.1750626684152304
Kappa statistic: 0.47977685133285386
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.48
Correctly Classified Instances: 65.52
Weighted Precision: 0.6966288137334018
Weighted AreaUnderROC: 0.7403574997837659
Root mean squared error: 0.47881611600853613
Relative absolute error: 51.846719160104655
Root relative squared error: 101.5722367713119
Weighted TruePositiveRate: 0.6552
Weighted MatthewsCorrelation: 0.5070518233445028
Weighted FMeasure: 0.647586635705126
Iteration time: 1221.0
Weighted AreaUnderPRC: 0.553486082840584
Mean absolute error: 0.23042986293379955
Coverage of cases: 65.52
Instances selection time: 1221.0
Test time: 2287.0
Accumulative iteration time: 36554.0
Weighted Recall: 0.6552
Weighted FalsePositiveRate: 0.17448500043246823
Kappa statistic: 0.4815638823960746
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.56
Correctly Classified Instances: 65.44
Weighted Precision: 0.696073903716351
Weighted AreaUnderROC: 0.7397529048623632
Root mean squared error: 0.4793820539466755
Relative absolute error: 51.96423043852047
Root relative squared error: 101.69229033744864
Weighted TruePositiveRate: 0.6544
Weighted MatthewsCorrelation: 0.5059936300499634
Weighted FMeasure: 0.6467203078789014
Iteration time: 1220.0
Weighted AreaUnderPRC: 0.5527403985025944
Mean absolute error: 0.23095213528231426
Coverage of cases: 65.44
Instances selection time: 1220.0
Test time: 2341.0
Accumulative iteration time: 37774.0
Weighted Recall: 0.6544
Weighted FalsePositiveRate: 0.17489419027527356
Kappa statistic: 0.4803562638962599
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 34.52
Correctly Classified Instances: 65.48
Weighted Precision: 0.6969175591216715
Weighted AreaUnderROC: 0.7400483362464196
Root mean squared error: 0.4791149772361648
Relative absolute error: 51.90228233305043
Root relative squared error: 101.6356348115189
Weighted TruePositiveRate: 0.6548
Weighted MatthewsCorrelation: 0.5069004380983774
Weighted FMeasure: 0.6469669359410825
Iteration time: 1217.0
Weighted AreaUnderPRC: 0.5532059508643147
Mean absolute error: 0.23067681036911408
Coverage of cases: 65.48
Instances selection time: 1217.0
Test time: 2383.0
Accumulative iteration time: 38991.0
Weighted Recall: 0.6548
Weighted FalsePositiveRate: 0.1747033275071608
Kappa statistic: 0.4809504609015561
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.8
Correctly Classified Instances: 66.2
Weighted Precision: 0.7018921759353338
Weighted AreaUnderROC: 0.7455012790144682
Root mean squared error: 0.47410208477887655
Relative absolute error: 50.822942643391386
Root relative squared error: 100.57223973654669
Weighted TruePositiveRate: 0.662
Weighted MatthewsCorrelation: 0.5161313699444604
Weighted FMeasure: 0.6544339785146778
Iteration time: 1221.0
Weighted AreaUnderPRC: 0.5597571037846869
Mean absolute error: 0.22587974508174058
Coverage of cases: 66.2
Instances selection time: 1221.0
Test time: 2416.0
Accumulative iteration time: 40212.0
Weighted Recall: 0.662
Weighted FalsePositiveRate: 0.17099744197106356
Kappa statistic: 0.4918293374080001
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.48
Correctly Classified Instances: 66.52
Weighted Precision: 0.7047833294535638
Weighted AreaUnderROC: 0.7479301753365709
Root mean squared error: 0.4718620889710925
Relative absolute error: 50.342109566637916
Root relative squared error: 100.09706486889263
Weighted TruePositiveRate: 0.6652
Weighted MatthewsCorrelation: 0.5203599419152145
Weighted FMeasure: 0.6574643902043128
Iteration time: 1223.0
Weighted AreaUnderPRC: 0.5627856372229859
Mean absolute error: 0.22374270918505845
Coverage of cases: 66.52
Instances selection time: 1223.0
Test time: 2454.0
Accumulative iteration time: 41435.0
Weighted Recall: 0.6652
Weighted FalsePositiveRate: 0.16933964932685822
Kappa statistic: 0.4966694793842554
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.24
Correctly Classified Instances: 66.76
Weighted Precision: 0.706362462817722
Weighted AreaUnderROC: 0.7497501116181354
Root mean squared error: 0.4701770510997915
Relative absolute error: 49.98101367658869
Root relative squared error: 99.73961435728671
Weighted TruePositiveRate: 0.6676
Weighted MatthewsCorrelation: 0.5233127708286238
Weighted FMeasure: 0.6600652301157481
Iteration time: 1228.0
Weighted AreaUnderPRC: 0.5650508835199507
Mean absolute error: 0.2221378385626174
Coverage of cases: 66.76
Instances selection time: 1228.0
Test time: 2496.0
Accumulative iteration time: 42663.0
Weighted Recall: 0.6676
Weighted FalsePositiveRate: 0.16809977676372914
Kappa statistic: 0.5003011422237957
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.88
Correctly Classified Instances: 67.12
Weighted Precision: 0.7099880190326663
Weighted AreaUnderROC: 0.7524661052473702
Root mean squared error: 0.4676329636611525
Relative absolute error: 49.440380047505435
Root relative squared error: 99.19993191334875
Weighted TruePositiveRate: 0.6712
Weighted MatthewsCorrelation: 0.528493562981349
Weighted FMeasure: 0.6636517413344049
Iteration time: 1227.0
Weighted AreaUnderPRC: 0.5687262696892506
Mean absolute error: 0.21973502243335852
Coverage of cases: 67.12
Instances selection time: 1227.0
Test time: 2534.0
Accumulative iteration time: 43890.0
Weighted Recall: 0.6712
Weighted FalsePositiveRate: 0.1662677895052596
Kappa statistic: 0.5057308761142703
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.712997381068589
Weighted AreaUnderROC: 0.7567206932070473
Root mean squared error: 0.46364217074592295
Relative absolute error: 48.60046765393526
Root relative squared error: 98.35335689354774
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5353103444551024
Weighted FMeasure: 0.6687631237382271
Iteration time: 1227.0
Weighted AreaUnderPRC: 0.5735803609132573
Mean absolute error: 0.21600207846193553
Coverage of cases: 67.68
Instances selection time: 1227.0
Test time: 2577.0
Accumulative iteration time: 45117.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16335861358590556
Kappa statistic: 0.5141981600135062
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.7123611725123733
Weighted AreaUnderROC: 0.7561284013203576
Root mean squared error: 0.4642239374459147
Relative absolute error: 48.71834228702942
Root relative squared error: 98.47676824713756
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5340071312012771
Weighted FMeasure: 0.6678384731193022
Iteration time: 1228.0
Weighted AreaUnderPRC: 0.572736866216819
Mean absolute error: 0.21652596572013177
Coverage of cases: 67.6
Instances selection time: 1228.0
Test time: 2610.0
Accumulative iteration time: 46345.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.16374319735928472
Kappa statistic: 0.5130050498370191
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7129195906547988
Weighted AreaUnderROC: 0.7573629117918774
Root mean squared error: 0.4630843322792981
Relative absolute error: 48.47709750567019
Root relative squared error: 98.2350214847806
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5355412041377725
Weighted FMeasure: 0.669180879507525
Iteration time: 1228.0
Weighted AreaUnderPRC: 0.5740018669126196
Mean absolute error: 0.2154537666918685
Coverage of cases: 67.76
Instances selection time: 1228.0
Test time: 2650.0
Accumulative iteration time: 47573.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16287417641624494
Kappa statistic: 0.5154463208308063
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.7146768007517438
Weighted AreaUnderROC: 0.7582748838524056
Root mean squared error: 0.4622294993877012
Relative absolute error: 48.295755770661856
Root relative squared error: 98.05368404445176
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5374353969120678
Weighted FMeasure: 0.6698835263680909
Iteration time: 1235.0
Weighted AreaUnderPRC: 0.575226722654234
Mean absolute error: 0.21464780342516482
Coverage of cases: 67.88
Instances selection time: 1235.0
Test time: 2689.0
Accumulative iteration time: 48808.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.16225023229518912
Kappa statistic: 0.5172603182450831
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7136819262565175
Weighted AreaUnderROC: 0.7579982643000629
Root mean squared error: 0.4625247845683685
Relative absolute error: 48.3539251650777
Root relative squared error: 98.11632349054187
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.536459570241726
Weighted FMeasure: 0.6687441289065544
Iteration time: 1218.0
Weighted AreaUnderPRC: 0.5743559276439908
Mean absolute error: 0.21490633406701298
Coverage of cases: 67.84
Instances selection time: 1218.0
Test time: 2728.0
Accumulative iteration time: 50026.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.1624034713998742
Kappa statistic: 0.5166742572052807
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7128465613192402
Weighted AreaUnderROC: 0.7574095840915179
Root mean squared error: 0.4631070569041745
Relative absolute error: 48.47201735357899
Root relative squared error: 98.23984210568561
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5351315918183104
Weighted FMeasure: 0.667548429729289
Iteration time: 1214.0
Weighted AreaUnderPRC: 0.5733730419893027
Mean absolute error: 0.21543118823812984
Coverage of cases: 67.76
Instances selection time: 1214.0
Test time: 2765.0
Accumulative iteration time: 51240.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16278083181696423
Kappa statistic: 0.5154812749743554
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.7130471036067911
Weighted AreaUnderROC: 0.7577245418665766
Root mean squared error: 0.46282682109210393
Relative absolute error: 48.410548823948474
Root relative squared error: 98.18039511277168
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.5354484968333136
Weighted FMeasure: 0.6679183826371956
Iteration time: 1207.0
Weighted AreaUnderPRC: 0.573719650559042
Mean absolute error: 0.21515799477310535
Coverage of cases: 67.8
Instances selection time: 1207.0
Test time: 2810.0
Accumulative iteration time: 52447.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.16255091626684698
Kappa statistic: 0.5161007991490107
Training time: 0.0
		
Time end:Sun Oct 08 11.43.19 EEST 2017