Sun Oct 08 11.23.14 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.23.14 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.133333333334356
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.841292016764236
Weighted AreaUnderROC: 0.9545876650833277
Root mean squared error: 0.3315731038286152
Relative absolute error: 29.383559486363655
Root relative squared error: 70.33727705288536
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7283245366860054
Weighted FMeasure: 0.7942388843498575
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9157420603037623
Mean absolute error: 0.1305935977171724
Coverage of cases: 88.48
Instances selection time: 104.0
Test time: 137.0
Accumulative iteration time: 108.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09573616308488768
Kappa statistic: 0.710097843175871
Training time: 4.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 39.386666666667665
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8431372234907718
Weighted AreaUnderROC: 0.9554936073842913
Root mean squared error: 0.3293756811063393
Relative absolute error: 29.15857928987106
Root relative squared error: 69.87113330046894
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7329027844778915
Weighted FMeasure: 0.7986109588535237
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9170089453746956
Mean absolute error: 0.12959368573276087
Coverage of cases: 89.04
Instances selection time: 95.0
Test time: 150.0
Accumulative iteration time: 208.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09396418411800168
Kappa statistic: 0.7154753610048621
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 39.65333333333427
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8436500395897804
Weighted AreaUnderROC: 0.9552184892615061
Root mean squared error: 0.32771801477122875
Relative absolute error: 29.01709283694993
Root relative squared error: 69.51948916851855
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7331370387577768
Weighted FMeasure: 0.798642279682793
Iteration time: 99.0
Weighted AreaUnderPRC: 0.916365667215125
Mean absolute error: 0.12896485705311142
Coverage of cases: 89.2
Instances selection time: 94.0
Test time: 128.0
Accumulative iteration time: 307.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.0939605335581546
Kappa statistic: 0.7154760427233986
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 39.94666666666755
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8482234660716971
Weighted AreaUnderROC: 0.9561207999582302
Root mean squared error: 0.32522570409084084
Relative absolute error: 28.79448823800958
Root relative squared error: 68.99079023364075
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7410916046684713
Weighted FMeasure: 0.8051148686015195
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9181848714183761
Mean absolute error: 0.12797550328004317
Coverage of cases: 89.44
Instances selection time: 93.0
Test time: 141.0
Accumulative iteration time: 405.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09119582476458374
Kappa statistic: 0.7238470584739777
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.1733333333342
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8493024919567171
Weighted AreaUnderROC: 0.9566909183012067
Root mean squared error: 0.3239964261765004
Relative absolute error: 28.76657088336518
Root relative squared error: 68.73002100888286
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7432357880464854
Weighted FMeasure: 0.8069224431160832
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9191290546229733
Mean absolute error: 0.1278514261482903
Coverage of cases: 89.96
Instances selection time: 92.0
Test time: 128.0
Accumulative iteration time: 502.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09040419482626451
Kappa statistic: 0.7262396372417608
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 40.49333333333417
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.849932409261994
Weighted AreaUnderROC: 0.957205853170968
Root mean squared error: 0.32139129210916983
Relative absolute error: 28.53402536971481
Root relative squared error: 68.17738861941
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7459939040951896
Weighted FMeasure: 0.8102433532118796
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9201297132531461
Mean absolute error: 0.12681789053206644
Coverage of cases: 90.64
Instances selection time: 94.0
Test time: 128.0
Accumulative iteration time: 601.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.08923584126673864
Kappa statistic: 0.729818068713196
Training time: 5.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.46666666666751
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8491193474594138
Weighted AreaUnderROC: 0.9563731012918948
Root mean squared error: 0.32258879809269636
Relative absolute error: 28.722109254052608
Root relative squared error: 68.43141799984892
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7443882863735113
Weighted FMeasure: 0.8088040177307163
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9186539048289549
Mean absolute error: 0.12765381890690108
Coverage of cases: 90.28
Instances selection time: 93.0
Test time: 131.0
Accumulative iteration time: 704.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08982867052136756
Kappa statistic: 0.7280246297061325
Training time: 10.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 40.62666666666751
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.8500111892551944
Weighted AreaUnderROC: 0.9581431635221809
Root mean squared error: 0.320401745572551
Relative absolute error: 28.568853246769834
Root relative squared error: 67.96747409950714
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7455951760036527
Weighted FMeasure: 0.8095326343860039
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9215590308014927
Mean absolute error: 0.1269726810967554
Coverage of cases: 90.76
Instances selection time: 88.0
Test time: 131.0
Accumulative iteration time: 798.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.0894217021198325
Kappa statistic: 0.7292250949389535
Training time: 6.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 40.64000000000085
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8489628572482206
Weighted AreaUnderROC: 0.9579115385817327
Root mean squared error: 0.3200650864275204
Relative absolute error: 28.57054008139205
Root relative squared error: 67.89605791018727
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7442891541612662
Weighted FMeasure: 0.8087807644931713
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9210822657189616
Mean absolute error: 0.1269801781395208
Coverage of cases: 90.72
Instances selection time: 87.0
Test time: 131.0
Accumulative iteration time: 891.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08982509772492547
Kappa statistic: 0.7280254117719228
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 40.786666666667486
Incorrectly Classified Instances: 17.88
Correctly Classified Instances: 82.12
Weighted Precision: 0.8504706711089957
Weighted AreaUnderROC: 0.9585177182957545
Root mean squared error: 0.318924590652733
Relative absolute error: 28.500100237388363
Root relative squared error: 67.65412222130723
Weighted TruePositiveRate: 0.8212
Weighted MatthewsCorrelation: 0.7478826532141327
Weighted FMeasure: 0.8122096292874338
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9220791439934459
Mean absolute error: 0.1266671121661711
Coverage of cases: 90.92
Instances selection time: 86.0
Test time: 132.0
Accumulative iteration time: 984.0
Weighted Recall: 0.8212
Weighted FalsePositiveRate: 0.08845072012542014
Kappa statistic: 0.7322070452911574
Training time: 7.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 41.06666666666745
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.849983188841395
Weighted AreaUnderROC: 0.9586065010033795
Root mean squared error: 0.31808105660452696
Relative absolute error: 28.473108403695356
Root relative squared error: 67.47518162761277
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7463730628627138
Weighted FMeasure: 0.8110783338979135
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9223384138992072
Mean absolute error: 0.12654714846086884
Coverage of cases: 91.36
Instances selection time: 86.0
Test time: 132.0
Accumulative iteration time: 1076.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08903775514233671
Kappa statistic: 0.7304124904566021
Training time: 6.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 41.226666666667406
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8490000229445315
Weighted AreaUnderROC: 0.958703428534266
Root mean squared error: 0.315969825530038
Relative absolute error: 28.32987875106768
Root relative squared error: 67.02732188478589
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7459027826892672
Weighted FMeasure: 0.811362673390671
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9224302936584651
Mean absolute error: 0.12591057222696805
Coverage of cases: 91.36
Instances selection time: 84.0
Test time: 131.0
Accumulative iteration time: 1167.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08905220185491503
Kappa statistic: 0.73040615929665
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 41.20000000000073
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.8509372595965488
Weighted AreaUnderROC: 0.9580147465679223
Root mean squared error: 0.31945147126484214
Relative absolute error: 28.774003824739495
Root relative squared error: 67.76589047741666
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7477493222923312
Weighted FMeasure: 0.8115485240310927
Iteration time: 234.0
Weighted AreaUnderPRC: 0.9212037602338485
Mean absolute error: 0.12788446144328725
Coverage of cases: 91.2
Instances selection time: 216.0
Test time: 217.0
Accumulative iteration time: 1401.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08864515568997496
Kappa statistic: 0.7316119434602218
Training time: 18.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 41.36000000000071
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8476707516524282
Weighted AreaUnderROC: 0.9582823797134395
Root mean squared error: 0.3169374130471912
Relative absolute error: 28.540217802639056
Root relative squared error: 67.23257819321704
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7437076666505925
Weighted FMeasure: 0.8093117109763416
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9216760077863588
Mean absolute error: 0.12684541245617417
Coverage of cases: 91.48
Instances selection time: 82.0
Test time: 131.0
Accumulative iteration time: 1490.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08985176970881185
Kappa statistic: 0.7280140713777346
Training time: 7.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 41.44000000000068
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8472862409636702
Weighted AreaUnderROC: 0.9590755208819248
Root mean squared error: 0.31676143091423525
Relative absolute error: 28.597214307546675
Root relative squared error: 67.1952467453428
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7431077651279927
Weighted FMeasure: 0.8089316191932304
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9231059881916431
Mean absolute error: 0.1270987302557636
Coverage of cases: 91.76
Instances selection time: 82.0
Test time: 130.0
Accumulative iteration time: 1579.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09004842671463693
Kappa statistic: 0.7274151140710834
Training time: 7.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.493333333334
Incorrectly Classified Instances: 18.12
Correctly Classified Instances: 81.88
Weighted Precision: 0.8479475312862509
Weighted AreaUnderROC: 0.959477494645713
Root mean squared error: 0.317026849724118
Relative absolute error: 28.68518876025393
Root relative squared error: 67.25155057743956
Weighted TruePositiveRate: 0.8188
Weighted MatthewsCorrelation: 0.7442564282476835
Weighted FMeasure: 0.8096552387301612
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9237723054873088
Mean absolute error: 0.12748972782335138
Coverage of cases: 91.68
Instances selection time: 82.0
Test time: 130.0
Accumulative iteration time: 1668.0
Weighted Recall: 0.8188
Weighted FalsePositiveRate: 0.08965296902512153
Kappa statistic: 0.728613289393848
Training time: 7.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 41.54666666666733
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8493389848356926
Weighted AreaUnderROC: 0.9592541875928855
Root mean squared error: 0.31710625211858967
Relative absolute error: 28.71468479798451
Root relative squared error: 67.26839436891157
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7461703973724602
Weighted FMeasure: 0.8108680173771556
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9232850888235605
Mean absolute error: 0.1276208213243762
Coverage of cases: 91.72
Instances selection time: 80.0
Test time: 130.0
Accumulative iteration time: 1755.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08905363097349187
Kappa statistic: 0.7304101647589291
Training time: 7.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 41.6800000000006
Incorrectly Classified Instances: 18.12
Correctly Classified Instances: 81.88
Weighted Precision: 0.848880434640276
Weighted AreaUnderROC: 0.9593029833169985
Root mean squared error: 0.31574176998615855
Relative absolute error: 28.61448097588488
Root relative squared error: 66.9789439983166
Weighted TruePositiveRate: 0.8188
Weighted MatthewsCorrelation: 0.7446845945230384
Weighted FMeasure: 0.8096113675726999
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9234151340825468
Mean absolute error: 0.1271754710039334
Coverage of cases: 92.0
Instances selection time: 79.0
Test time: 131.0
Accumulative iteration time: 1841.0
Weighted Recall: 0.8188
Weighted FalsePositiveRate: 0.08963995143112004
Kappa statistic: 0.7286169312362032
Training time: 7.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 41.74666666666723
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8465759950078091
Weighted AreaUnderROC: 0.9586144391225243
Root mean squared error: 0.31589351010664085
Relative absolute error: 28.707870438954604
Root relative squared error: 67.01113293876793
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7420290997440901
Weighted FMeasure: 0.8078422077260385
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9223571927378216
Mean absolute error: 0.12759053528424327
Coverage of cases: 91.76
Instances selection time: 80.0
Test time: 127.0
Accumulative iteration time: 1929.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09045396599759517
Kappa statistic: 0.7262185133317806
Training time: 8.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 41.64000000000059
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8472499008899462
Weighted AreaUnderROC: 0.9574965825316393
Root mean squared error: 0.3186459033098801
Relative absolute error: 29.031766806149555
Root relative squared error: 67.59500370831859
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7423606916322611
Weighted FMeasure: 0.8078062808971461
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9206674128271439
Mean absolute error: 0.12903007469399863
Coverage of cases: 91.6
Instances selection time: 77.0
Test time: 129.0
Accumulative iteration time: 2014.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09045031543774808
Kappa statistic: 0.7262204815356383
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 41.61333333333395
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8458207504292419
Weighted AreaUnderROC: 0.9575050156587502
Root mean squared error: 0.31891908262856805
Relative absolute error: 29.072124167444453
Root relative squared error: 67.65295379293585
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7409032889859052
Weighted FMeasure: 0.8068902967485221
Iteration time: 85.0
Weighted AreaUnderPRC: 0.920667992417202
Mean absolute error: 0.12920944074419818
Coverage of cases: 91.64
Instances selection time: 76.0
Test time: 130.0
Accumulative iteration time: 2099.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.0908595052805534
Kappa statistic: 0.725020872054069
Training time: 9.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 41.62666666666727
Incorrectly Classified Instances: 18.32
Correctly Classified Instances: 81.68
Weighted Precision: 0.8465193204882256
Weighted AreaUnderROC: 0.9571277181470709
Root mean squared error: 0.3193292060648574
Relative absolute error: 29.147942256814993
Root relative squared error: 67.73995411181295
Weighted TruePositiveRate: 0.8168
Weighted MatthewsCorrelation: 0.7416294389728905
Weighted FMeasure: 0.8072757011466327
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9200312290840109
Mean absolute error: 0.12954641003028947
Coverage of cases: 91.56
Instances selection time: 75.0
Test time: 128.0
Accumulative iteration time: 2183.0
Weighted Recall: 0.8168
Weighted FalsePositiveRate: 0.09065419579986232
Kappa statistic: 0.7256214016265258
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 41.65333333333392
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8478252446341772
Weighted AreaUnderROC: 0.956941578823569
Root mean squared error: 0.3189197507513795
Relative absolute error: 29.119411833702742
Root relative squared error: 67.65309552318705
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7439164525950821
Weighted FMeasure: 0.8089640038823294
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9197665553751961
Mean absolute error: 0.12941960814979056
Coverage of cases: 91.6
Instances selection time: 74.0
Test time: 128.0
Accumulative iteration time: 2266.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08986470953940832
Kappa statistic: 0.7280148535042471
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 41.546666666667285
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8459711536134651
Weighted AreaUnderROC: 0.9560125186885337
Root mean squared error: 0.32170720367035793
Relative absolute error: 29.421968382163072
Root relative squared error: 68.2444035815614
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7394004463396934
Weighted FMeasure: 0.8045665847485357
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9184412778727586
Mean absolute error: 0.13076430392072538
Coverage of cases: 91.28
Instances selection time: 73.0
Test time: 129.0
Accumulative iteration time: 2348.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09163883218415957
Kappa statistic: 0.722636636501354
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 41.56000000000062
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8469099317848758
Weighted AreaUnderROC: 0.9561374556160183
Root mean squared error: 0.3208874896999082
Relative absolute error: 29.371537475110784
Root relative squared error: 68.0705159894199
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7406413529393207
Weighted FMeasure: 0.8056774735562975
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9187183969435451
Mean absolute error: 0.13054016655604855
Coverage of cases: 91.32
Instances selection time: 73.0
Test time: 128.0
Accumulative iteration time: 2430.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09123686569764339
Kappa statistic: 0.7238321033378335
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 41.58666666666728
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8483254514881519
Weighted AreaUnderROC: 0.9565441432007643
Root mean squared error: 0.32066110801630765
Relative absolute error: 29.353055046840765
Root relative squared error: 68.02249318233677
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7429302971666254
Weighted FMeasure: 0.8078379398954965
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9193662747288527
Mean absolute error: 0.13045802243040402
Coverage of cases: 91.36
Instances selection time: 72.0
Test time: 129.0
Accumulative iteration time: 2511.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09043944152161182
Kappa statistic: 0.7262217936558222
Training time: 9.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 41.70666666666723
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8473964718905103
Weighted AreaUnderROC: 0.9564908374352148
Root mean squared error: 0.319717492707444
Relative absolute error: 29.271235242524217
Root relative squared error: 67.8223221472181
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7416796101083759
Weighted FMeasure: 0.8066269796560218
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9191566813072797
Mean absolute error: 0.1300943788556638
Coverage of cases: 91.6
Instances selection time: 71.0
Test time: 128.0
Accumulative iteration time: 2592.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.09084069344883958
Kappa statistic: 0.7250278566550448
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 41.69333333333388
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8466770666919626
Weighted AreaUnderROC: 0.9561750334723073
Root mean squared error: 0.3199761949293304
Relative absolute error: 29.350809543523447
Root relative squared error: 67.87720117583926
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7405774874895414
Weighted FMeasure: 0.8061761004933865
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9190528459524103
Mean absolute error: 0.13044804241566038
Coverage of cases: 91.32
Instances selection time: 71.0
Test time: 128.0
Accumulative iteration time: 2673.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09124909096895144
Kappa statistic: 0.7238244266211686
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 41.50666666666727
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8445807461716373
Weighted AreaUnderROC: 0.9553588649559395
Root mean squared error: 0.323252432955023
Relative absolute error: 29.697807429973096
Root relative squared error: 68.57219621326381
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7346642166760738
Weighted FMeasure: 0.8006753781719106
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9174780003278443
Mean absolute error: 0.1319902552443255
Coverage of cases: 91.16
Instances selection time: 68.0
Test time: 127.0
Accumulative iteration time: 2751.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09360016703717647
Kappa statistic: 0.7166521777885978
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 41.54666666666727
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.844648430654313
Weighted AreaUnderROC: 0.9563452674819548
Root mean squared error: 0.3218309769777364
Relative absolute error: 29.568129310828898
Root relative squared error: 68.27065986505455
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7350841793450108
Weighted FMeasure: 0.8011132591458593
Iteration time: 78.0
Weighted AreaUnderPRC: 0.919035502648417
Mean absolute error: 0.13141390804812905
Coverage of cases: 91.28
Instances selection time: 68.0
Test time: 128.0
Accumulative iteration time: 2829.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09339993723490929
Kappa statistic: 0.7172504087888159
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.50666666666728
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8456316641065028
Weighted AreaUnderROC: 0.9563678505298325
Root mean squared error: 0.3230774677694422
Relative absolute error: 29.66822055857283
Root relative squared error: 68.53508049250509
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7359374205020053
Weighted FMeasure: 0.8009229405745054
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9190715237334193
Mean absolute error: 0.1318587580381021
Coverage of cases: 91.2
Instances selection time: 68.0
Test time: 128.0
Accumulative iteration time: 2907.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09319391319492981
Kappa statistic: 0.7178579729383264
Training time: 10.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 41.46666666666728
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8462049247759658
Weighted AreaUnderROC: 0.9565890182519421
Root mean squared error: 0.3228822079740274
Relative absolute error: 29.637162442498383
Root relative squared error: 68.49365963487581
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7365854661630501
Weighted FMeasure: 0.8014031711777954
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9191931622050472
Mean absolute error: 0.1317207219666601
Coverage of cases: 91.0
Instances selection time: 65.0
Test time: 130.0
Accumulative iteration time: 2983.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09298717459566191
Kappa statistic: 0.7184574053609545
Training time: 11.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 41.53333333333394
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.847475160774026
Weighted AreaUnderROC: 0.9571084420934043
Root mean squared error: 0.3223305767722856
Relative absolute error: 29.6022136929387
Root relative squared error: 68.3766409858361
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7392091990422055
Weighted FMeasure: 0.8038313901069907
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9200617630023288
Mean absolute error: 0.1315653941908393
Coverage of cases: 91.12
Instances selection time: 64.0
Test time: 130.0
Accumulative iteration time: 3058.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.09199817309222917
Kappa statistic: 0.7214463988590444
Training time: 11.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 41.440000000000644
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8459700358482156
Weighted AreaUnderROC: 0.9573416216895227
Root mean squared error: 0.32342505377291225
Relative absolute error: 29.719690508343735
Root relative squared error: 68.60881461853485
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7356253856904384
Weighted FMeasure: 0.800554821055717
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9203645056257075
Mean absolute error: 0.13208751337041721
Coverage of cases: 91.08
Instances selection time: 64.0
Test time: 133.0
Accumulative iteration time: 3133.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09337397981031136
Kappa statistic: 0.7172621971410895
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 41.46666666666729
Incorrectly Classified Instances: 18.68
Correctly Classified Instances: 81.32
Weighted Precision: 0.847238578206981
Weighted AreaUnderROC: 0.9574814637683603
Root mean squared error: 0.3237385730509058
Relative absolute error: 29.744257302184447
Root relative squared error: 68.67532210078544
Weighted TruePositiveRate: 0.8132
Weighted MatthewsCorrelation: 0.7382448280588051
Weighted FMeasure: 0.8027233705377821
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9204981913034695
Mean absolute error: 0.13219669912082038
Coverage of cases: 91.2
Instances selection time: 62.0
Test time: 131.0
Accumulative iteration time: 3206.0
Weighted Recall: 0.8132
Weighted FalsePositiveRate: 0.09238283462901337
Kappa statistic: 0.7202542176319154
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.453333333333994
Incorrectly Classified Instances: 18.56
Correctly Classified Instances: 81.44
Weighted Precision: 0.8477338802588241
Weighted AreaUnderROC: 0.957802458772072
Root mean squared error: 0.3230204217457025
Relative absolute error: 29.66965893585931
Root relative squared error: 68.52297920343727
Weighted TruePositiveRate: 0.8144
Weighted MatthewsCorrelation: 0.7397258540601189
Weighted FMeasure: 0.8035214421237019
Iteration time: 72.0
Weighted AreaUnderPRC: 0.920771336604776
Mean absolute error: 0.131865150826042
Coverage of cases: 91.28
Instances selection time: 61.0
Test time: 130.0
Accumulative iteration time: 3278.0
Weighted Recall: 0.8144
Weighted FalsePositiveRate: 0.09179365593423154
Kappa statistic: 0.7220534309149426
Training time: 11.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 41.400000000000674
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8463653489605169
Weighted AreaUnderROC: 0.958107833894754
Root mean squared error: 0.3225668327318434
Relative absolute error: 29.642907374006164
Root relative squared error: 68.42675844316581
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.7382022988726696
Weighted FMeasure: 0.8031541040586663
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9211523173135556
Mean absolute error: 0.13174625499558357
Coverage of cases: 91.16
Instances selection time: 60.0
Test time: 130.0
Accumulative iteration time: 3350.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.0921949078614593
Kappa statistic: 0.7208507032884985
Training time: 12.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 41.37333333333401
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.846741926992401
Weighted AreaUnderROC: 0.958211864577332
Root mean squared error: 0.3227648622312867
Relative absolute error: 29.67037307861641
Root relative squared error: 68.46876684374523
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7388063840916544
Weighted FMeasure: 0.8032532350307021
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9211486397393999
Mean absolute error: 0.13186832479385133
Coverage of cases: 91.16
Instances selection time: 60.0
Test time: 131.0
Accumulative iteration time: 3422.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.09199539261848055
Kappa statistic: 0.7214526728323266
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 41.360000000000696
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8454862942720196
Weighted AreaUnderROC: 0.9581390763961585
Root mean squared error: 0.3230729730738187
Relative absolute error: 29.715097944693206
Root relative squared error: 68.53412702357868
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7361679719042624
Weighted FMeasure: 0.8008459627572972
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9209609367857303
Mean absolute error: 0.13206710197641486
Coverage of cases: 91.12
Instances selection time: 59.0
Test time: 130.0
Accumulative iteration time: 3493.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.0929851086812017
Kappa statistic: 0.7184638814318425
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.360000000000696
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8448043541275408
Weighted AreaUnderROC: 0.9581519223590672
Root mean squared error: 0.3236185223166551
Relative absolute error: 29.764095012068008
Root relative squared error: 68.6498554943029
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7350145377460412
Weighted FMeasure: 0.8001096953478267
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9210349611352405
Mean absolute error: 0.13228486672030287
Coverage of cases: 91.08
Instances selection time: 58.0
Test time: 134.0
Accumulative iteration time: 3563.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09337985181142869
Kappa statistic: 0.7172658553953822
Training time: 12.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.42666666666735
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8454214435831938
Weighted AreaUnderROC: 0.9581016766391495
Root mean squared error: 0.32368042349121806
Relative absolute error: 29.812679812708023
Root relative squared error: 68.66298671639197
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7353015072077141
Weighted FMeasure: 0.7998967710809677
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9210641134550999
Mean absolute error: 0.13250079916759183
Coverage of cases: 91.04
Instances selection time: 57.0
Test time: 141.0
Accumulative iteration time: 3632.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09337334301442796
Kappa statistic: 0.7172697845260316
Training time: 12.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.493333333334
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8453544017948731
Weighted AreaUnderROC: 0.9583207050796183
Root mean squared error: 0.32339155270111647
Relative absolute error: 29.784558391481358
Root relative squared error: 68.6017079680217
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7352645795894619
Weighted FMeasure: 0.8000198283919648
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9214277814725227
Mean absolute error: 0.1323758150732511
Coverage of cases: 91.28
Instances selection time: 57.0
Test time: 129.0
Accumulative iteration time: 3701.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09337334301442796
Kappa statistic: 0.7172684296657299
Training time: 12.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 41.360000000000696
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8433822530615657
Weighted AreaUnderROC: 0.9575514693654592
Root mean squared error: 0.3249489500752055
Relative absolute error: 29.94133465238379
Root relative squared error: 68.93208184128785
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7319032432917334
Weighted FMeasure: 0.7970711164601016
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9202833789089152
Mean absolute error: 0.1330725984550397
Coverage of cases: 91.0
Instances selection time: 57.0
Test time: 127.0
Accumulative iteration time: 3771.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09456765399855176
Kappa statistic: 0.7136806957535132
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 41.32000000000069
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8443288297713407
Weighted AreaUnderROC: 0.9574859367588177
Root mean squared error: 0.3267024175636111
Relative absolute error: 30.09938617724968
Root relative squared error: 69.30404846678037
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7327909677933502
Weighted FMeasure: 0.7973182977446686
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9200804823050872
Mean absolute error: 0.13377504967666587
Coverage of cases: 90.88
Instances selection time: 55.0
Test time: 127.0
Accumulative iteration time: 3838.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09436448819572595
Kappa statistic: 0.7142817438373509
Training time: 12.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 41.346666666667375
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8426606483411236
Weighted AreaUnderROC: 0.957736733930837
Root mean squared error: 0.32648634828275674
Relative absolute error: 30.080801120425665
Root relative squared error: 69.2582132506709
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7303484448444665
Weighted FMeasure: 0.7954575534710335
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9204812106718513
Mean absolute error: 0.1336924494241147
Coverage of cases: 90.96
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 3904.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09516048325318069
Kappa statistic: 0.711888959009885
Training time: 13.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.360000000000696
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8428339322684034
Weighted AreaUnderROC: 0.9577558893057214
Root mean squared error: 0.32619134242880593
Relative absolute error: 30.0590179662714
Root relative squared error: 69.19563305872539
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7308224147349907
Weighted FMeasure: 0.79608361951525
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9205274976263521
Mean absolute error: 0.1335956354056513
Coverage of cases: 91.0
Instances selection time: 53.0
Test time: 128.0
Accumulative iteration time: 3970.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09496239712877877
Kappa statistic: 0.7124854625462
Training time: 13.0
		
Time end:Sun Oct 08 11.23.27 EEST 2017