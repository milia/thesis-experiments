Sun Oct 08 10.49.51 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.49.51 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 34.10666666666764
Incorrectly Classified Instances: 29.08
Correctly Classified Instances: 70.92
Weighted Precision: 0.7091960916224295
Weighted AreaUnderROC: 0.7560714081893881
Root mean squared error: 0.4362681774706522
Relative absolute error: 45.099384112192205
Root relative squared error: 92.54645601161809
Weighted TruePositiveRate: 0.7092
Weighted MatthewsCorrelation: 0.563802549706184
Weighted FMeasure: 0.7087559696705348
Iteration time: 80.0
Weighted AreaUnderPRC: 0.5809223263845014
Mean absolute error: 0.2004417071652996
Coverage of cases: 71.72
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7092
Weighted FalsePositiveRate: 0.1454034908219383
Kappa statistic: 0.5638782228019583
Training time: 76.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 37.97333333333433
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7126428444676176
Weighted AreaUnderROC: 0.8014237763006018
Root mean squared error: 0.43096384770812884
Relative absolute error: 45.39481345586944
Root relative squared error: 91.42123774819912
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5601053871194364
Weighted FMeasure: 0.7012971578947763
Iteration time: 148.0
Weighted AreaUnderPRC: 0.624446247795972
Mean absolute error: 0.2017547264705318
Coverage of cases: 76.84
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 228.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14909864083592383
Kappa statistic: 0.554990769511643
Training time: 145.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.240000000000926
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.7068370572805884
Weighted AreaUnderROC: 0.7828509438980725
Root mean squared error: 0.4440434102353667
Relative absolute error: 46.549979964308555
Root relative squared error: 94.1958319555881
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5479993841788446
Weighted FMeasure: 0.6900405991652632
Iteration time: 193.0
Weighted AreaUnderPRC: 0.6029931772169308
Mean absolute error: 0.20688879984137235
Coverage of cases: 73.08
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 421.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.1539471223377767
Kappa statistic: 0.5404936239736189
Training time: 190.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 36.880000000001026
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7282550810886831
Weighted AreaUnderROC: 0.8122973341447601
Root mean squared error: 0.4265624672484508
Relative absolute error: 44.147610288809695
Root relative squared error: 90.48756395731304
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.568027678061661
Weighted FMeasure: 0.6970162685436393
Iteration time: 240.0
Weighted AreaUnderPRC: 0.6544611726540616
Mean absolute error: 0.19621160128359957
Coverage of cases: 75.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 661.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14941449018518027
Kappa statistic: 0.5548139927549433
Training time: 238.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 36.74666666666755
Incorrectly Classified Instances: 27.84
Correctly Classified Instances: 72.16
Weighted Precision: 0.7424617053404099
Weighted AreaUnderROC: 0.7805221734916387
Root mean squared error: 0.4234459731800046
Relative absolute error: 43.46305249551591
Root relative squared error: 89.82645573051525
Weighted TruePositiveRate: 0.7216
Weighted MatthewsCorrelation: 0.5931284658493969
Weighted FMeasure: 0.7155704777512069
Iteration time: 296.0
Weighted AreaUnderPRC: 0.6252459561777332
Mean absolute error: 0.19316912220229382
Coverage of cases: 75.52
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 957.0
Weighted Recall: 0.7216
Weighted FalsePositiveRate: 0.14006352662783478
Kappa statistic: 0.5819889111748058
Training time: 295.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 37.426666666667494
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7309840909340093
Weighted AreaUnderROC: 0.8043907891246636
Root mean squared error: 0.4128675400739
Relative absolute error: 42.06339705049459
Root relative squared error: 87.58243119541879
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5896616916350417
Weighted FMeasure: 0.7222433809577942
Iteration time: 350.0
Weighted AreaUnderPRC: 0.6465244984614533
Mean absolute error: 0.1869484313355324
Coverage of cases: 77.08
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 1307.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13865743355418697
Kappa statistic: 0.58570591748847
Training time: 348.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 36.77333333333433
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7490846740210031
Weighted AreaUnderROC: 0.8332726726709363
Root mean squared error: 0.4040069242710281
Relative absolute error: 38.96197635444197
Root relative squared error: 85.70281073950899
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.6184852009453055
Weighted FMeasure: 0.7434696411655195
Iteration time: 405.0
Weighted AreaUnderPRC: 0.6821363949420307
Mean absolute error: 0.17316433935307626
Coverage of cases: 78.36
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 1712.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.12857874586848092
Kappa statistic: 0.6157745672540993
Training time: 404.0
		
Time end:Sun Oct 08 10.49.53 EEST 2017