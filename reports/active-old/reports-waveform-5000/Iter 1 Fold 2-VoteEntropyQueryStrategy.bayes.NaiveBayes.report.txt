Sun Oct 08 12.18.05 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.18.05 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.89333333333448
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8332471311850611
Weighted AreaUnderROC: 0.9458768011182656
Root mean squared error: 0.3366390142219849
Relative absolute error: 30.463274437665877
Root relative squared error: 71.41191893049587
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7225086768066672
Weighted FMeasure: 0.7933306010095732
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9011067952120695
Mean absolute error: 0.1353923308340712
Coverage of cases: 88.52
Instances selection time: 99.0
Test time: 131.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09681289388112355
Kappa statistic: 0.70707596640371
Training time: 4.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.720000000001235
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8392132777125307
Weighted AreaUnderROC: 0.9547735348601555
Root mean squared error: 0.3219680648682945
Relative absolute error: 28.856678350773517
Root relative squared error: 68.29974059816422
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7383389460944829
Weighted FMeasure: 0.8091119426116838
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9158796769526252
Mean absolute error: 0.12825190378121623
Coverage of cases: 90.72
Instances selection time: 80.0
Test time: 130.0
Accumulative iteration time: 191.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09054050800428746
Kappa statistic: 0.7261874775767316
Training time: 8.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.54666666666768
Incorrectly Classified Instances: 17.88
Correctly Classified Instances: 82.12
Weighted Precision: 0.8363531022334526
Weighted AreaUnderROC: 0.9549792635144854
Root mean squared error: 0.3134070504884948
Relative absolute error: 28.13729973102281
Root relative squared error: 66.48367520162665
Weighted TruePositiveRate: 0.8212
Weighted MatthewsCorrelation: 0.7407651635733213
Weighted FMeasure: 0.8150342693037185
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9161657150877381
Mean absolute error: 0.12505466547121308
Coverage of cases: 91.88
Instances selection time: 67.0
Test time: 128.0
Accumulative iteration time: 268.0
Weighted Recall: 0.8212
Weighted FalsePositiveRate: 0.08867308233549272
Kappa statistic: 0.7321265484523748
Training time: 10.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 40.85333333333442
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8372856779795628
Weighted AreaUnderROC: 0.9546866901662437
Root mean squared error: 0.31670123650501586
Relative absolute error: 28.39232822812806
Root relative squared error: 67.18247758285823
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7401223790753378
Weighted FMeasure: 0.8134729282226193
Iteration time: 65.0
Weighted AreaUnderPRC: 0.9163619519551277
Mean absolute error: 0.12618812545834754
Coverage of cases: 91.12
Instances selection time: 52.0
Test time: 128.0
Accumulative iteration time: 333.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.0892319367780909
Kappa statistic: 0.7303422496167863
Training time: 13.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 40.56000000000107
Incorrectly Classified Instances: 18.76
Correctly Classified Instances: 81.24
Weighted Precision: 0.833464240201137
Weighted AreaUnderROC: 0.9543549981894678
Root mean squared error: 0.3210391087178625
Relative absolute error: 28.84988153467502
Root relative squared error: 68.10267924014559
Weighted TruePositiveRate: 0.8124
Weighted MatthewsCorrelation: 0.7309124922416868
Weighted FMeasure: 0.8048249629088738
Iteration time: 54.0
Weighted AreaUnderPRC: 0.9158633878634371
Mean absolute error: 0.12822169570966735
Coverage of cases: 90.36
Instances selection time: 38.0
Test time: 130.0
Accumulative iteration time: 387.0
Weighted Recall: 0.8124
Weighted FalsePositiveRate: 0.09296818190031447
Kappa statistic: 0.7189835761778602
Training time: 16.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 40.06666666666785
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8357809918653697
Weighted AreaUnderROC: 0.9542340530410076
Root mean squared error: 0.32749778856024736
Relative absolute error: 29.424811584286235
Root relative squared error: 69.47277213436455
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.728288242191679
Weighted FMeasure: 0.7987081759024298
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9154086973014127
Mean absolute error: 0.1307769403746061
Coverage of cases: 89.4
Instances selection time: 24.0
Test time: 129.0
Accumulative iteration time: 430.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09465161151831024
Kappa statistic: 0.7136470084738918
Training time: 19.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 39.506666666667876
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8338415198354234
Weighted AreaUnderROC: 0.9540015900882572
Root mean squared error: 0.33637676330807004
Relative absolute error: 30.33203706575647
Root relative squared error: 71.3562871106154
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7199441763497587
Weighted FMeasure: 0.7893538614865598
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9146048809542026
Mean absolute error: 0.13480905362558496
Coverage of cases: 88.28
Instances selection time: 10.0
Test time: 131.0
Accumulative iteration time: 462.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09816266284852747
Kappa statistic: 0.7029057732116843
Training time: 22.0
		
Time end:Sun Oct 08 12.18.07 EEST 2017