Sun Oct 08 12.18.17 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.18.17 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.53333333333454
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8324302484122805
Weighted AreaUnderROC: 0.9559809474179496
Root mean squared error: 0.32719714751344503
Relative absolute error: 29.558230101070475
Root relative squared error: 69.40899653749547
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7236587624653952
Weighted FMeasure: 0.7971424914746904
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9168591494254873
Mean absolute error: 0.13136991156031383
Coverage of cases: 90.36
Instances selection time: 100.0
Test time: 136.0
Accumulative iteration time: 105.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09604257822692149
Kappa statistic: 0.7094403257527413
Training time: 5.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.92000000000097
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.834285353201997
Weighted AreaUnderROC: 0.9564275198020193
Root mean squared error: 0.3060769141504678
Relative absolute error: 27.552210839815036
Root relative squared error: 64.9287184681344
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7417764707210245
Weighted FMeasure: 0.8198116381088973
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9179172957291979
Mean absolute error: 0.12245427039917851
Coverage of cases: 92.88
Instances selection time: 81.0
Test time: 131.0
Accumulative iteration time: 193.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08757597232090004
Kappa statistic: 0.7356643779157838
Training time: 7.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 42.46666666666743
Incorrectly Classified Instances: 17.0
Correctly Classified Instances: 83.0
Weighted Precision: 0.8370137809359116
Weighted AreaUnderROC: 0.9582960699523192
Root mean squared error: 0.2979296860543153
Relative absolute error: 26.941238564349117
Root relative squared error: 63.20043039773551
Weighted TruePositiveRate: 0.83
Weighted MatthewsCorrelation: 0.7493117775341086
Weighted FMeasure: 0.8273728457979412
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9211628150226469
Mean absolute error: 0.11973883806377442
Coverage of cases: 93.84
Instances selection time: 67.0
Test time: 130.0
Accumulative iteration time: 270.0
Weighted Recall: 0.83
Weighted FalsePositiveRate: 0.0844947097040918
Kappa statistic: 0.7452083584128243
Training time: 10.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.65333333333432
Incorrectly Classified Instances: 18.24
Correctly Classified Instances: 81.76
Weighted Precision: 0.8344258138889104
Weighted AreaUnderROC: 0.957772079351186
Root mean squared error: 0.31265779684511846
Relative absolute error: 28.24424930120372
Root relative squared error: 66.32473450200861
Weighted TruePositiveRate: 0.8176
Weighted MatthewsCorrelation: 0.7362167797022922
Weighted FMeasure: 0.8123275975820107
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9204170826152956
Mean absolute error: 0.12552999689423935
Coverage of cases: 92.44
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 336.0
Weighted Recall: 0.8176
Weighted FalsePositiveRate: 0.09043890743590319
Kappa statistic: 0.7267269002128893
Training time: 13.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 40.9466666666677
Incorrectly Classified Instances: 18.56
Correctly Classified Instances: 81.44
Weighted Precision: 0.8344279389648301
Weighted AreaUnderROC: 0.9562226541319527
Root mean squared error: 0.32181166118578464
Relative absolute error: 29.14275911518972
Root relative squared error: 68.26656236681262
Weighted TruePositiveRate: 0.8144
Weighted MatthewsCorrelation: 0.7331751992791904
Weighted FMeasure: 0.8079615051786961
Iteration time: 55.0
Weighted AreaUnderPRC: 0.9180658154423914
Mean absolute error: 0.12952337384528825
Coverage of cases: 91.12
Instances selection time: 38.0
Test time: 134.0
Accumulative iteration time: 391.0
Weighted Recall: 0.8144
Weighted FalsePositiveRate: 0.0919795387443073
Kappa statistic: 0.721963027314496
Training time: 17.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 40.25333333333444
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8371724164373591
Weighted AreaUnderROC: 0.955448188202846
Root mean squared error: 0.3288309633000064
Relative absolute error: 29.760819278181653
Root relative squared error: 69.75558120406161
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7316989121941275
Weighted FMeasure: 0.8028803277357758
Iteration time: 44.0
Weighted AreaUnderPRC: 0.9167942714584763
Mean absolute error: 0.13227030790303018
Coverage of cases: 89.8
Instances selection time: 24.0
Test time: 131.0
Accumulative iteration time: 435.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.0932743773901014
Kappa statistic: 0.7178211913769031
Training time: 20.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 39.9200000000012
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8370786908346252
Weighted AreaUnderROC: 0.9558881781997456
Root mean squared error: 0.33243882388398893
Relative absolute error: 30.01987590906955
Root relative squared error: 70.52092400941453
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7262464267470389
Weighted FMeasure: 0.7962792166826941
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9177559153526358
Mean absolute error: 0.13342167070697641
Coverage of cases: 89.12
Instances selection time: 10.0
Test time: 132.0
Accumulative iteration time: 467.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09578305298390903
Kappa statistic: 0.7100684597854171
Training time: 22.0
		
Time end:Sun Oct 08 12.18.19 EEST 2017