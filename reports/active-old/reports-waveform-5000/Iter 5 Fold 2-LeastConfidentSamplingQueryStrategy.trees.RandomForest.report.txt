Sun Oct 08 11.22.34 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.22.34 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.53333333333482
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.7840779897999082
Weighted AreaUnderROC: 0.9275335519219446
Root mean squared error: 0.3223827952398602
Relative absolute error: 51.64199999999901
Root relative squared error: 68.38771819559365
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.674946247660138
Weighted FMeasure: 0.7834730100111755
Iteration time: 96.0
Weighted AreaUnderPRC: 0.8334175485773816
Mean absolute error: 0.22951999999999667
Coverage of cases: 99.56
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10873013421459109
Kappa statistic: 0.6747293772423869
Training time: 87.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 71.85333333333496
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.7932268091487947
Weighted AreaUnderROC: 0.9330658910466886
Root mean squared error: 0.3181069002709608
Relative absolute error: 50.77799999999901
Root relative squared error: 67.48066389714859
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.689699309574286
Weighted FMeasure: 0.7916873153381004
Iteration time: 103.0
Weighted AreaUnderPRC: 0.8481491802776785
Mean absolute error: 0.22567999999999666
Coverage of cases: 99.92
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 199.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10353385784991953
Kappa statistic: 0.6892821289476765
Training time: 94.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 71.36000000000145
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.7922516401012676
Weighted AreaUnderROC: 0.9320440899570249
Root mean squared error: 0.31677541992185193
Relative absolute error: 50.06999999999898
Root relative squared error: 67.19821426198715
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.6885037612822124
Weighted FMeasure: 0.7920176599637003
Iteration time: 102.0
Weighted AreaUnderPRC: 0.847386087980206
Mean absolute error: 0.22253333333332984
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 301.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10382870584323418
Kappa statistic: 0.6886417967252075
Training time: 94.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 71.24000000000134
Incorrectly Classified Instances: 20.56
Correctly Classified Instances: 79.44
Weighted Precision: 0.7962178082051383
Weighted AreaUnderROC: 0.9331880401816784
Root mean squared error: 0.316109685605064
Relative absolute error: 49.697999999998956
Root relative squared error: 67.05699068702633
Weighted TruePositiveRate: 0.7944
Weighted MatthewsCorrelation: 0.6922402430064467
Weighted FMeasure: 0.7946202020537849
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8471102848459399
Mean absolute error: 0.22087999999999638
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 409.0
Weighted Recall: 0.7944
Weighted FalsePositiveRate: 0.1031791243219869
Kappa statistic: 0.6915148087295788
Training time: 100.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 70.61333333333462
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8006851063977478
Weighted AreaUnderROC: 0.9368177963042006
Root mean squared error: 0.3126019833590298
Relative absolute error: 49.271999999998954
Root relative squared error: 66.31289467366011
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.700113734635216
Weighted FMeasure: 0.8001631717043103
Iteration time: 112.0
Weighted AreaUnderPRC: 0.854272690474995
Mean absolute error: 0.21898666666666305
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 521.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.10027454681198346
Kappa statistic: 0.6999453660522509
Training time: 104.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 71.97333333333486
Incorrectly Classified Instances: 21.72
Correctly Classified Instances: 78.28
Weighted Precision: 0.7838143449847568
Weighted AreaUnderROC: 0.9314445486289169
Root mean squared error: 0.319144690279084
Relative absolute error: 51.23999999999895
Root relative squared error: 67.70081240280606
Weighted TruePositiveRate: 0.7828
Weighted MatthewsCorrelation: 0.6745451320481991
Weighted FMeasure: 0.7827640461451824
Iteration time: 122.0
Weighted AreaUnderPRC: 0.8428809748166259
Mean absolute error: 0.22773333333332976
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 643.0
Weighted Recall: 0.7828
Weighted FalsePositiveRate: 0.10889726099937616
Kappa statistic: 0.6741262743283042
Training time: 113.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 71.29333333333481
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8013081639119649
Weighted AreaUnderROC: 0.9390233574849881
Root mean squared error: 0.3105650763795983
Relative absolute error: 49.03799999999887
Root relative squared error: 65.88080145231946
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7003926947283455
Weighted FMeasure: 0.8002654970702641
Iteration time: 119.0
Weighted AreaUnderPRC: 0.8597245793142599
Mean absolute error: 0.21794666666666268
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 762.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.10031510647596979
Kappa statistic: 0.6999348498546005
Training time: 111.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 69.45333333333463
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.7880097860469769
Weighted AreaUnderROC: 0.9340426302776028
Root mean squared error: 0.31418465907806375
Relative absolute error: 48.79799999999888
Root relative squared error: 66.64863089366457
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.6800784315240087
Weighted FMeasure: 0.7867788649932069
Iteration time: 127.0
Weighted AreaUnderPRC: 0.8514924365431101
Mean absolute error: 0.21687999999999605
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 889.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10721267686724847
Kappa statistic: 0.6795004143687526
Training time: 119.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 71.50666666666804
Incorrectly Classified Instances: 21.28
Correctly Classified Instances: 78.72
Weighted Precision: 0.7896302821203429
Weighted AreaUnderROC: 0.9321098049484013
Root mean squared error: 0.3177126584405038
Relative absolute error: 50.30999999999911
Root relative squared error: 67.39703257562553
Weighted TruePositiveRate: 0.7872
Weighted MatthewsCorrelation: 0.6817361878014588
Weighted FMeasure: 0.7874937064438531
Iteration time: 133.0
Weighted AreaUnderPRC: 0.8450581482345864
Mean absolute error: 0.22359999999999708
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1022.0
Weighted Recall: 0.7872
Weighted FalsePositiveRate: 0.1068394482790789
Kappa statistic: 0.68068315558029
Training time: 125.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 70.58666666666817
Incorrectly Classified Instances: 21.44
Correctly Classified Instances: 78.56
Weighted Precision: 0.7921902658685247
Weighted AreaUnderROC: 0.9366184384671413
Root mean squared error: 0.31528611344829727
Relative absolute error: 50.20199999999886
Root relative squared error: 66.88228464997249
Weighted TruePositiveRate: 0.7856
Weighted MatthewsCorrelation: 0.6816767912853594
Weighted FMeasure: 0.7858452987298651
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8541567795365194
Mean absolute error: 0.223119999999996
Coverage of cases: 99.96
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1161.0
Weighted Recall: 0.7856
Weighted FalsePositiveRate: 0.1078978611310237
Kappa statistic: 0.6781570987255501
Training time: 130.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 70.89333333333478
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.798006610947787
Weighted AreaUnderROC: 0.9376933425287003
Root mean squared error: 0.31394266992557573
Relative absolute error: 49.865999999998984
Root relative squared error: 66.59729724245521
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.6888629954514177
Weighted FMeasure: 0.7906109869224062
Iteration time: 143.0
Weighted AreaUnderPRC: 0.8570191449559609
Mean absolute error: 0.2216266666666632
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1304.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.10577670061291597
Kappa statistic: 0.6847510289726415
Training time: 135.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 68.38666666666809
Incorrectly Classified Instances: 22.0
Correctly Classified Instances: 78.0
Weighted Precision: 0.7898096935557218
Weighted AreaUnderROC: 0.931931138827924
Root mean squared error: 0.31793081008294727
Relative absolute error: 48.899999999999
Root relative squared error: 67.44330952733516
Weighted TruePositiveRate: 0.78
Weighted MatthewsCorrelation: 0.6747449463382608
Weighted FMeasure: 0.7808734828352273
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8458565060760637
Mean absolute error: 0.21733333333332988
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1449.0
Weighted Recall: 0.78
Weighted FalsePositiveRate: 0.11085632695263511
Kappa statistic: 0.6697152152471948
Training time: 137.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 70.56000000000157
Incorrectly Classified Instances: 22.16
Correctly Classified Instances: 77.84
Weighted Precision: 0.7923445591676528
Weighted AreaUnderROC: 0.9351237771614699
Root mean squared error: 0.31789306378088683
Relative absolute error: 50.33999999999903
Root relative squared error: 67.43530232748967
Weighted TruePositiveRate: 0.7784
Weighted MatthewsCorrelation: 0.6748931802278779
Weighted FMeasure: 0.7783906742260939
Iteration time: 151.0
Weighted AreaUnderPRC: 0.8501467183832089
Mean absolute error: 0.22373333333333006
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1600.0
Weighted Recall: 0.7784
Weighted FalsePositiveRate: 0.11179655584565798
Kappa statistic: 0.667234490364216
Training time: 142.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.34666666666817
Incorrectly Classified Instances: 23.4
Correctly Classified Instances: 76.6
Weighted Precision: 0.7801391655566682
Weighted AreaUnderROC: 0.927756090157364
Root mean squared error: 0.3243907109233132
Relative absolute error: 51.125999999999
Root relative squared error: 68.81366143433978
Weighted TruePositiveRate: 0.766
Weighted MatthewsCorrelation: 0.6560388559082692
Weighted FMeasure: 0.7669535279573991
Iteration time: 152.0
Weighted AreaUnderPRC: 0.8349287835403156
Mean absolute error: 0.22722666666666333
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1752.0
Weighted Recall: 0.766
Weighted FalsePositiveRate: 0.11804890086403852
Kappa statistic: 0.6486272075220927
Training time: 144.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 69.28000000000151
Incorrectly Classified Instances: 22.12
Correctly Classified Instances: 77.88
Weighted Precision: 0.79353336950723
Weighted AreaUnderROC: 0.936701001138864
Root mean squared error: 0.3161729484527928
Relative absolute error: 49.289999999998955
Root relative squared error: 67.0704107636142
Weighted TruePositiveRate: 0.7788
Weighted MatthewsCorrelation: 0.6760351002282442
Weighted FMeasure: 0.7787584353773503
Iteration time: 158.0
Weighted AreaUnderPRC: 0.8530259660849367
Mean absolute error: 0.21906666666666305
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1910.0
Weighted Recall: 0.7788
Weighted FalsePositiveRate: 0.111647045064225
Kappa statistic: 0.6678153567578972
Training time: 149.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 70.88000000000152
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.7910831655508644
Weighted AreaUnderROC: 0.9303563014773435
Root mean squared error: 0.32537158245099707
Relative absolute error: 50.79599999999915
Root relative squared error: 69.0217357069492
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.6602777782277328
Weighted FMeasure: 0.7632169626920378
Iteration time: 161.0
Weighted AreaUnderPRC: 0.8405760940489712
Mean absolute error: 0.22575999999999727
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2071.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.11964414700391321
Kappa statistic: 0.6448365118161871
Training time: 153.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 70.78666666666824
Incorrectly Classified Instances: 23.48
Correctly Classified Instances: 76.52
Weighted Precision: 0.790648817355963
Weighted AreaUnderROC: 0.9336151932127533
Root mean squared error: 0.32409463638470226
Relative absolute error: 51.67199999999911
Root relative squared error: 68.75085454014325
Weighted TruePositiveRate: 0.7652
Weighted MatthewsCorrelation: 0.6613767638746193
Weighted FMeasure: 0.7650297410134311
Iteration time: 170.0
Weighted AreaUnderPRC: 0.8462363071020307
Mean absolute error: 0.22965333333333046
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2241.0
Weighted Recall: 0.7652
Weighted FalsePositiveRate: 0.1187910796554336
Kappa statistic: 0.6472683063542661
Training time: 161.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 69.2266666666682
Incorrectly Classified Instances: 24.44
Correctly Classified Instances: 75.56
Weighted Precision: 0.7795034981178504
Weighted AreaUnderROC: 0.9323808408705254
Root mean squared error: 0.3245879439124818
Relative absolute error: 50.35199999999909
Root relative squared error: 68.85550086957423
Weighted TruePositiveRate: 0.7556
Weighted MatthewsCorrelation: 0.6469460116943916
Weighted FMeasure: 0.7539059284026307
Iteration time: 173.0
Weighted AreaUnderPRC: 0.845790628266161
Mean absolute error: 0.22378666666666366
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2414.0
Weighted Recall: 0.7556
Weighted FalsePositiveRate: 0.12355974888046792
Kappa statistic: 0.6328207968377638
Training time: 165.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 70.48000000000148
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.7973227324949742
Weighted AreaUnderROC: 0.9362518046579326
Root mean squared error: 0.3231263942587573
Relative absolute error: 51.07199999999905
Root relative squared error: 68.5454593682174
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.6637844502763439
Weighted FMeasure: 0.7638122508470968
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8534747227751268
Mean absolute error: 0.2269866666666635
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2592.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.11980908837020215
Kappa statistic: 0.6447806679902544
Training time: 170.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 70.14666666666805
Incorrectly Classified Instances: 23.48
Correctly Classified Instances: 76.52
Weighted Precision: 0.7946325323133555
Weighted AreaUnderROC: 0.9318479720843683
Root mean squared error: 0.3255886975925284
Relative absolute error: 50.91599999999903
Root relative squared error: 69.06779278361174
Weighted TruePositiveRate: 0.7652
Weighted MatthewsCorrelation: 0.6640491125079787
Weighted FMeasure: 0.7648795511652466
Iteration time: 187.0
Weighted AreaUnderPRC: 0.8446912309278112
Mean absolute error: 0.22629333333333007
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2779.0
Weighted Recall: 0.7652
Weighted FalsePositiveRate: 0.11889458361156216
Kappa statistic: 0.6472160720424447
Training time: 179.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 71.0533333333349
Incorrectly Classified Instances: 24.76
Correctly Classified Instances: 75.24
Weighted Precision: 0.7834916699869671
Weighted AreaUnderROC: 0.9293207737208575
Root mean squared error: 0.33045826766275005
Relative absolute error: 52.56599999999922
Root relative squared error: 70.10078458904677
Weighted TruePositiveRate: 0.7524
Weighted MatthewsCorrelation: 0.6460721685904646
Weighted FMeasure: 0.75124299486324
Iteration time: 190.0
Weighted AreaUnderPRC: 0.836489668467467
Mean absolute error: 0.23362666666666432
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2969.0
Weighted Recall: 0.7524
Weighted FalsePositiveRate: 0.1253427155496693
Kappa statistic: 0.6279513256953764
Training time: 182.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 69.96000000000151
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7880142051688407
Weighted AreaUnderROC: 0.929703186683337
Root mean squared error: 0.3309944611822568
Relative absolute error: 51.57599999999918
Root relative squared error: 70.21452841114821
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6484393362808144
Weighted FMeasure: 0.7504959939046779
Iteration time: 196.0
Weighted AreaUnderPRC: 0.8397204339816411
Mean absolute error: 0.22922666666666414
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3165.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12566510561296879
Kappa statistic: 0.6272961562451459
Training time: 188.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 70.0000000000016
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7818594033472321
Weighted AreaUnderROC: 0.926230819166932
Root mean squared error: 0.33510197452914614
Relative absolute error: 52.21199999999913
Root relative squared error: 71.08586357356812
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.6361564933022412
Weighted FMeasure: 0.7414286672146345
Iteration time: 196.0
Weighted AreaUnderPRC: 0.8330563360778964
Mean absolute error: 0.23205333333333056
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 3361.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.13037285471969018
Kappa statistic: 0.6134388146386813
Training time: 188.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 68.17333333333461
Incorrectly Classified Instances: 24.84
Correctly Classified Instances: 75.16
Weighted Precision: 0.7897004382457188
Weighted AreaUnderROC: 0.935423270005996
Root mean squared error: 0.3261656021103377
Relative absolute error: 49.73999999999914
Root relative squared error: 69.19017271260377
Weighted TruePositiveRate: 0.7516
Weighted MatthewsCorrelation: 0.6487020952364337
Weighted FMeasure: 0.7506002171867789
Iteration time: 257.0
Weighted AreaUnderPRC: 0.852763709965075
Mean absolute error: 0.22106666666666389
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3618.0
Weighted Recall: 0.7516
Weighted FalsePositiveRate: 0.1259324115362986
Kappa statistic: 0.6266824538108209
Training time: 249.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 68.17333333333474
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7895286987317708
Weighted AreaUnderROC: 0.9280172023445481
Root mean squared error: 0.3311434734371181
Relative absolute error: 49.85999999999923
Root relative squared error: 70.2461386839159
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.6481289360257704
Weighted FMeasure: 0.749327701534823
Iteration time: 204.0
Weighted AreaUnderPRC: 0.837982429859406
Mean absolute error: 0.22159999999999763
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 3822.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.12634310826108577
Kappa statistic: 0.6254666337061274
Training time: 196.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 69.13333333333478
Incorrectly Classified Instances: 25.64
Correctly Classified Instances: 74.36
Weighted Precision: 0.7856486801667087
Weighted AreaUnderROC: 0.9313066337278855
Root mean squared error: 0.3302201286011086
Relative absolute error: 50.79599999999916
Root relative squared error: 70.05026766544114
Weighted TruePositiveRate: 0.7436
Weighted MatthewsCorrelation: 0.6387518479603371
Weighted FMeasure: 0.7427960522104405
Iteration time: 212.0
Weighted AreaUnderPRC: 0.8440430635608971
Mean absolute error: 0.22575999999999735
Coverage of cases: 99.64
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 4034.0
Weighted Recall: 0.7436
Weighted FalsePositiveRate: 0.13004383635535405
Kappa statistic: 0.6146248718823093
Training time: 204.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 68.17333333333475
Incorrectly Classified Instances: 25.32
Correctly Classified Instances: 74.68
Weighted Precision: 0.7873187192882201
Weighted AreaUnderROC: 0.9285653620322669
Root mean squared error: 0.3322047561369336
Relative absolute error: 50.309999999999285
Root relative squared error: 70.47127074205456
Weighted TruePositiveRate: 0.7468
Weighted MatthewsCorrelation: 0.6428691808806225
Weighted FMeasure: 0.7457191687253775
Iteration time: 209.0
Weighted AreaUnderPRC: 0.8378617893216801
Mean absolute error: 0.22359999999999786
Coverage of cases: 99.52
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 4243.0
Weighted Recall: 0.7468
Weighted FalsePositiveRate: 0.1283948517128247
Kappa statistic: 0.6194473582071679
Training time: 201.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 69.8533333333348
Incorrectly Classified Instances: 24.76
Correctly Classified Instances: 75.24
Weighted Precision: 0.7959461484503539
Weighted AreaUnderROC: 0.9323115431898193
Root mean squared error: 0.3320160638684007
Relative absolute error: 51.42599999999917
Root relative squared error: 70.43124306726344
Weighted TruePositiveRate: 0.7524
Weighted MatthewsCorrelation: 0.6532894742619252
Weighted FMeasure: 0.7509183974674972
Iteration time: 224.0
Weighted AreaUnderPRC: 0.8438582363895679
Mean absolute error: 0.2285599999999974
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 4467.0
Weighted Recall: 0.7524
Weighted FalsePositiveRate: 0.1256403800395067
Kappa statistic: 0.6278339333684455
Training time: 216.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 69.02666666666816
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7812011191745274
Weighted AreaUnderROC: 0.923826945292705
Root mean squared error: 0.3378401989106676
Relative absolute error: 51.76799999999926
Root relative squared error: 71.66672868214336
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6314738138302207
Weighted FMeasure: 0.7349838490792849
Iteration time: 227.0
Weighted AreaUnderPRC: 0.8299567548941622
Mean absolute error: 0.2300799999999978
Coverage of cases: 99.44
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4694.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13308243763266048
Kappa statistic: 0.6055613919642273
Training time: 220.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 68.72000000000152
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.77834556429486
Weighted AreaUnderROC: 0.926515538111945
Root mean squared error: 0.3374215958312871
Relative absolute error: 51.5879999999992
Root relative squared error: 71.57792955932672
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.624129190665511
Weighted FMeasure: 0.7287917134041543
Iteration time: 231.0
Weighted AreaUnderPRC: 0.8341780583372637
Mean absolute error: 0.2292799999999975
Coverage of cases: 99.44
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4925.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.1361608062512598
Kappa statistic: 0.5965165972987899
Training time: 224.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 68.78666666666815
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7855704593848318
Weighted AreaUnderROC: 0.9273439390194653
Root mean squared error: 0.33419355669032974
Relative absolute error: 51.40799999999929
Root relative squared error: 70.89315904937474
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.6370031098938749
Weighted FMeasure: 0.7393535422029549
Iteration time: 235.0
Weighted AreaUnderPRC: 0.8324794715262372
Mean absolute error: 0.22847999999999793
Coverage of cases: 99.08
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 5160.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.13105109065802226
Kappa statistic: 0.6116089250585592
Training time: 225.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 68.18666666666816
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7847172497476268
Weighted AreaUnderROC: 0.9309622567779292
Root mean squared error: 0.33198795158860694
Relative absolute error: 50.423999999999204
Root relative squared error: 70.4252795521604
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6335767494286445
Weighted FMeasure: 0.735307780752081
Iteration time: 237.0
Weighted AreaUnderPRC: 0.8432504022961755
Mean absolute error: 0.2241066666666642
Coverage of cases: 99.56
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5397.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13291197134164842
Kappa statistic: 0.6061554713002185
Training time: 228.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 68.76000000000147
Incorrectly Classified Instances: 24.52
Correctly Classified Instances: 75.48
Weighted Precision: 0.7887388456916582
Weighted AreaUnderROC: 0.929551529059241
Root mean squared error: 0.3276502200009424
Relative absolute error: 49.9079999999991
Root relative squared error: 69.505107725979
Weighted TruePositiveRate: 0.7548
Weighted MatthewsCorrelation: 0.6510361067101612
Weighted FMeasure: 0.753832293149983
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8397820734465818
Mean absolute error: 0.22181333333333036
Coverage of cases: 99.36
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5644.0
Weighted Recall: 0.7548
Weighted FalsePositiveRate: 0.12420547685657037
Kappa statistic: 0.6315466290062843
Training time: 238.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 70.49333333333485
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7810056132392148
Weighted AreaUnderROC: 0.93006895288589
Root mean squared error: 0.3317830616532427
Relative absolute error: 51.905999999999196
Root relative squared error: 70.38181583335252
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6376070551666285
Weighted FMeasure: 0.7420413419914667
Iteration time: 250.0
Weighted AreaUnderPRC: 0.839081204834162
Mean absolute error: 0.23069333333333084
Coverage of cases: 99.52
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 5894.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.1289512024209704
Kappa statistic: 0.6171252828584645
Training time: 244.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 69.66666666666816
Incorrectly Classified Instances: 24.12
Correctly Classified Instances: 75.88
Weighted Precision: 0.7880242386028249
Weighted AreaUnderROC: 0.9342981802822337
Root mean squared error: 0.3240493789532685
Relative absolute error: 50.88599999999904
Root relative squared error: 68.74125398914347
Weighted TruePositiveRate: 0.7588
Weighted MatthewsCorrelation: 0.6544715336744483
Weighted FMeasure: 0.7572334386183077
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8478245915394442
Mean absolute error: 0.2261599999999968
Coverage of cases: 99.64
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 6148.0
Weighted Recall: 0.7588
Weighted FalsePositiveRate: 0.12205046273891797
Kappa statistic: 0.6376092160483707
Training time: 248.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 67.33333333333475
Incorrectly Classified Instances: 24.16
Correctly Classified Instances: 75.84
Weighted Precision: 0.7925383410895752
Weighted AreaUnderROC: 0.9356159392712321
Root mean squared error: 0.3226184536982749
Relative absolute error: 48.67799999999916
Root relative squared error: 68.43770890379035
Weighted TruePositiveRate: 0.7584
Weighted MatthewsCorrelation: 0.6568506796725668
Weighted FMeasure: 0.756673108277348
Iteration time: 255.0
Weighted AreaUnderPRC: 0.8549057671051673
Mean absolute error: 0.21634666666666394
Coverage of cases: 99.52
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 6403.0
Weighted Recall: 0.7584
Weighted FalsePositiveRate: 0.12239015775194942
Kappa statistic: 0.6369497625026327
Training time: 250.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 69.3333333333349
Incorrectly Classified Instances: 24.68
Correctly Classified Instances: 75.32
Weighted Precision: 0.7877779256360078
Weighted AreaUnderROC: 0.9321203308138544
Root mean squared error: 0.3270514740322475
Relative absolute error: 50.52599999999904
Root relative squared error: 69.37809452557731
Weighted TruePositiveRate: 0.7532
Weighted MatthewsCorrelation: 0.6486692438970334
Weighted FMeasure: 0.7510604687032939
Iteration time: 261.0
Weighted AreaUnderPRC: 0.8456610125319639
Mean absolute error: 0.22455999999999676
Coverage of cases: 99.84
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6664.0
Weighted Recall: 0.7532
Weighted FalsePositiveRate: 0.12496400377740752
Kappa statistic: 0.629165368444942
Training time: 255.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 69.6400000000015
Incorrectly Classified Instances: 24.32
Correctly Classified Instances: 75.68
Weighted Precision: 0.788590257926104
Weighted AreaUnderROC: 0.9296921126667524
Root mean squared error: 0.3288768766575104
Relative absolute error: 51.263999999999086
Root relative squared error: 69.76532089799306
Weighted TruePositiveRate: 0.7568
Weighted MatthewsCorrelation: 0.6524058141498191
Weighted FMeasure: 0.7550736300110676
Iteration time: 265.0
Weighted AreaUnderPRC: 0.8388805572447052
Mean absolute error: 0.22783999999999702
Coverage of cases: 99.6
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 6929.0
Weighted Recall: 0.7568
Weighted FalsePositiveRate: 0.12305549560031588
Kappa statistic: 0.6346132766150574
Training time: 260.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 68.96000000000151
Incorrectly Classified Instances: 26.52
Correctly Classified Instances: 73.48
Weighted Precision: 0.7698591670898062
Weighted AreaUnderROC: 0.9260610485805202
Root mean squared error: 0.33057525618230926
Relative absolute error: 51.3479999999991
Root relative squared error: 70.12560160169716
Weighted TruePositiveRate: 0.7348
Weighted MatthewsCorrelation: 0.6212596772823051
Weighted FMeasure: 0.7321476965322363
Iteration time: 273.0
Weighted AreaUnderPRC: 0.8315877988814198
Mean absolute error: 0.2282133333333304
Coverage of cases: 99.52
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 7202.0
Weighted Recall: 0.7348
Weighted FalsePositiveRate: 0.1341997881697198
Kappa statistic: 0.6015027254088113
Training time: 268.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 68.88000000000147
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.79078713230585
Weighted AreaUnderROC: 0.938015255204703
Root mean squared error: 0.3184797220127722
Relative absolute error: 48.95399999999898
Root relative squared error: 67.55975133169117
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.6600134896711909
Weighted FMeasure: 0.7608990987859495
Iteration time: 293.0
Weighted AreaUnderPRC: 0.8558731922914264
Mean absolute error: 0.21757333333332982
Coverage of cases: 99.6
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 7495.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.11949213077957758
Kappa statistic: 0.6448942157293475
Training time: 287.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 68.89333333333474
Incorrectly Classified Instances: 23.96
Correctly Classified Instances: 76.04
Weighted Precision: 0.7877985871858324
Weighted AreaUnderROC: 0.9383445002205059
Root mean squared error: 0.3190276894983663
Relative absolute error: 49.211999999999016
Root relative squared error: 67.67599278917118
Weighted TruePositiveRate: 0.7604
Weighted MatthewsCorrelation: 0.6554532288624213
Weighted FMeasure: 0.7579612864149984
Iteration time: 286.0
Weighted AreaUnderPRC: 0.8593927549866439
Mean absolute error: 0.21871999999999667
Coverage of cases: 99.64
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7781.0
Weighted Recall: 0.7604
Weighted FalsePositiveRate: 0.12110071128493063
Kappa statistic: 0.6400740474039905
Training time: 280.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 68.74666666666802
Incorrectly Classified Instances: 23.24
Correctly Classified Instances: 76.76
Weighted Precision: 0.7921974380269738
Weighted AreaUnderROC: 0.9350935944491264
Root mean squared error: 0.3202165933655935
Relative absolute error: 49.03199999999896
Root relative squared error: 67.92819738517976
Weighted TruePositiveRate: 0.7676
Weighted MatthewsCorrelation: 0.6646644263694013
Weighted FMeasure: 0.765982757697405
Iteration time: 296.0
Weighted AreaUnderPRC: 0.8515597496048285
Mean absolute error: 0.2179199999999964
Coverage of cases: 99.76
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8077.0
Weighted Recall: 0.7676
Weighted FalsePositiveRate: 0.11739490351223847
Kappa statistic: 0.6509249227829106
Training time: 290.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 68.80000000000142
Incorrectly Classified Instances: 22.44
Correctly Classified Instances: 77.56
Weighted Precision: 0.8020885537515673
Weighted AreaUnderROC: 0.9378364969304434
Root mean squared error: 0.31801048200753657
Relative absolute error: 49.16999999999889
Root relative squared error: 67.46021049477933
Weighted TruePositiveRate: 0.7756
Weighted MatthewsCorrelation: 0.6776429956083729
Weighted FMeasure: 0.7743106953566172
Iteration time: 288.0
Weighted AreaUnderPRC: 0.8555349266170238
Mean absolute error: 0.21853333333332944
Coverage of cases: 99.72
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8365.0
Weighted Recall: 0.7756
Weighted FalsePositiveRate: 0.11348938322912656
Kappa statistic: 0.6629218343974026
Training time: 282.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 69.61333333333481
Incorrectly Classified Instances: 23.4
Correctly Classified Instances: 76.6
Weighted Precision: 0.7940224438801733
Weighted AreaUnderROC: 0.9352845401121649
Root mean squared error: 0.3228498102833565
Relative absolute error: 50.38799999999897
Root relative squared error: 68.48678704684536
Weighted TruePositiveRate: 0.766
Weighted MatthewsCorrelation: 0.6640622588238441
Weighted FMeasure: 0.7642656406372884
Iteration time: 300.0
Weighted AreaUnderPRC: 0.8503530193347965
Mean absolute error: 0.22394666666666316
Coverage of cases: 99.72
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8665.0
Weighted Recall: 0.766
Weighted FalsePositiveRate: 0.11830432859245452
Kappa statistic: 0.6484885367605281
Training time: 294.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 69.6400000000015
Incorrectly Classified Instances: 24.72
Correctly Classified Instances: 75.28
Weighted Precision: 0.781376755755361
Weighted AreaUnderROC: 0.9288469792629054
Root mean squared error: 0.32804064788782805
Relative absolute error: 50.93999999999908
Root relative squared error: 69.58792998789335
Weighted TruePositiveRate: 0.7528
Weighted MatthewsCorrelation: 0.6442291058675873
Weighted FMeasure: 0.7497545399552485
Iteration time: 312.0
Weighted AreaUnderPRC: 0.8358930399729511
Mean absolute error: 0.226399999999997
Coverage of cases: 99.68
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8977.0
Weighted Recall: 0.7528
Weighted FalsePositiveRate: 0.12485370702264584
Kappa statistic: 0.6286848522165713
Training time: 306.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 68.1200000000015
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.7889150120064559
Weighted AreaUnderROC: 0.9318396939132843
Root mean squared error: 0.3223993382954324
Relative absolute error: 49.457999999999004
Root relative squared error: 68.39122750762665
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.6594085137470094
Weighted FMeasure: 0.761802217730785
Iteration time: 307.0
Weighted AreaUnderPRC: 0.8446880946777306
Mean absolute error: 0.21981333333332995
Coverage of cases: 99.48
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9284.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.11924054516059286
Kappa statistic: 0.6455127927824001
Training time: 302.0
		
Time end:Sun Oct 08 11.22.48 EEST 2017