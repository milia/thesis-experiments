Sun Oct 08 11.53.48 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.53.48 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.5200000000016
Incorrectly Classified Instances: 21.24
Correctly Classified Instances: 78.76
Weighted Precision: 0.7878745437864889
Weighted AreaUnderROC: 0.926182392085408
Root mean squared error: 0.32332439850197775
Relative absolute error: 51.49799999999914
Root relative squared error: 68.58746241114285
Weighted TruePositiveRate: 0.7876
Weighted MatthewsCorrelation: 0.6813655382205834
Weighted FMeasure: 0.7876061333024962
Iteration time: 98.0
Weighted AreaUnderPRC: 0.8325501782897005
Mean absolute error: 0.22887999999999725
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7876
Weighted FalsePositiveRate: 0.10641522523961544
Kappa statistic: 0.681361253528429
Training time: 89.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 70.9333333333347
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.7927889238830826
Weighted AreaUnderROC: 0.92537942476389
Root mean squared error: 0.32265977954082264
Relative absolute error: 50.597999999999146
Root relative squared error: 68.44647543884149
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.6891354012174584
Weighted FMeasure: 0.7925205633049014
Iteration time: 102.0
Weighted AreaUnderPRC: 0.8330643550980061
Mean absolute error: 0.22487999999999725
Coverage of cases: 99.32
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 200.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10369369464956985
Kappa statistic: 0.6892132270850552
Training time: 94.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 71.8000000000015
Incorrectly Classified Instances: 20.68
Correctly Classified Instances: 79.32
Weighted Precision: 0.7934250359824173
Weighted AreaUnderROC: 0.9274473757400236
Root mean squared error: 0.3226060549132108
Relative absolute error: 51.73799999999911
Root relative squared error: 68.43507872429116
Weighted TruePositiveRate: 0.7932
Weighted MatthewsCorrelation: 0.6897921124011408
Weighted FMeasure: 0.7931374762098847
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8365496484531517
Mean absolute error: 0.2299466666666638
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 308.0
Weighted Recall: 0.7932
Weighted FalsePositiveRate: 0.1035234311791733
Kappa statistic: 0.6897962279221316
Training time: 99.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 71.93333333333491
Incorrectly Classified Instances: 23.08
Correctly Classified Instances: 76.92
Weighted Precision: 0.7696658973286036
Weighted AreaUnderROC: 0.9184853863661497
Root mean squared error: 0.3315981503768265
Relative absolute error: 52.81199999999925
Root relative squared error: 70.34259022811099
Weighted TruePositiveRate: 0.7692
Weighted MatthewsCorrelation: 0.6543552762829428
Weighted FMeasure: 0.7679343776693244
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8177342434598942
Mean absolute error: 0.2347199999999978
Coverage of cases: 99.56
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 418.0
Weighted Recall: 0.7692
Weighted FalsePositiveRate: 0.11534595131005203
Kappa statistic: 0.6538936425360755
Training time: 101.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 71.10666666666816
Incorrectly Classified Instances: 23.68
Correctly Classified Instances: 76.32
Weighted Precision: 0.7673383606910787
Weighted AreaUnderROC: 0.9130425731675498
Root mean squared error: 0.3375282704209125
Relative absolute error: 53.189999999999316
Root relative squared error: 71.60055865703804
Weighted TruePositiveRate: 0.7632
Weighted MatthewsCorrelation: 0.6478719101993738
Weighted FMeasure: 0.760591640193215
Iteration time: 118.0
Weighted AreaUnderPRC: 0.8049777171752399
Mean absolute error: 0.23639999999999806
Coverage of cases: 99.28
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 536.0
Weighted Recall: 0.7632
Weighted FalsePositiveRate: 0.1183805436800858
Kappa statistic: 0.6449041709124211
Training time: 108.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 72.41333333333493
Incorrectly Classified Instances: 21.28
Correctly Classified Instances: 78.72
Weighted Precision: 0.7954298934214297
Weighted AreaUnderROC: 0.9266670426020782
Root mean squared error: 0.32855238040024304
Relative absolute error: 52.53599999999927
Root relative squared error: 69.69648484679803
Weighted TruePositiveRate: 0.7872
Weighted MatthewsCorrelation: 0.6861877562983596
Weighted FMeasure: 0.785198545175047
Iteration time: 121.0
Weighted AreaUnderPRC: 0.8326523605310893
Mean absolute error: 0.23349333333333117
Coverage of cases: 99.48
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 657.0
Weighted Recall: 0.7872
Weighted FalsePositiveRate: 0.10610484839592618
Kappa statistic: 0.6809445193705059
Training time: 112.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 71.18666666666809
Incorrectly Classified Instances: 22.56
Correctly Classified Instances: 77.44
Weighted Precision: 0.7811539518831627
Weighted AreaUnderROC: 0.9200904396666308
Root mean squared error: 0.33189154855163044
Relative absolute error: 51.365999999999225
Root relative squared error: 70.40482937980849
Weighted TruePositiveRate: 0.7744
Weighted MatthewsCorrelation: 0.6665447068317706
Weighted FMeasure: 0.7709636179015081
Iteration time: 121.0
Weighted AreaUnderPRC: 0.8219313053667251
Mean absolute error: 0.22829333333333096
Coverage of cases: 99.44
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 778.0
Weighted Recall: 0.7744
Weighted FalsePositiveRate: 0.11249396120618312
Kappa statistic: 0.6618148693046385
Training time: 112.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 71.00000000000148
Incorrectly Classified Instances: 22.64
Correctly Classified Instances: 77.36
Weighted Precision: 0.7802262405042754
Weighted AreaUnderROC: 0.9181662522559376
Root mean squared error: 0.33323865322018
Relative absolute error: 52.22399999999926
Root relative squared error: 70.69059343363833
Weighted TruePositiveRate: 0.7736
Weighted MatthewsCorrelation: 0.6647799825939548
Weighted FMeasure: 0.7713057624629155
Iteration time: 130.0
Weighted AreaUnderPRC: 0.8187564290752019
Mean absolute error: 0.23210666666666444
Coverage of cases: 99.32
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 908.0
Weighted Recall: 0.7736
Weighted FalsePositiveRate: 0.11314347120603134
Kappa statistic: 0.6604887618181101
Training time: 122.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 69.44000000000136
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.792986467755185
Weighted AreaUnderROC: 0.9245141436182169
Root mean squared error: 0.32761766334148207
Relative absolute error: 50.60399999999913
Root relative squared error: 69.49820141557583
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.6841323439374518
Weighted FMeasure: 0.7851636062488236
Iteration time: 131.0
Weighted AreaUnderPRC: 0.8296135508321039
Mean absolute error: 0.22490666666666384
Coverage of cases: 99.24
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1039.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10652229831356685
Kappa statistic: 0.6802865016841045
Training time: 123.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 69.86666666666794
Incorrectly Classified Instances: 22.88
Correctly Classified Instances: 77.12
Weighted Precision: 0.7864447797436356
Weighted AreaUnderROC: 0.9152399066618194
Root mean squared error: 0.33921871017579897
Relative absolute error: 52.025999999999264
Root relative squared error: 71.9591550811983
Weighted TruePositiveRate: 0.7712
Weighted MatthewsCorrelation: 0.666211909021697
Weighted FMeasure: 0.7683039614593264
Iteration time: 134.0
Weighted AreaUnderPRC: 0.8151915861213967
Mean absolute error: 0.23122666666666447
Coverage of cases: 99.08
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1173.0
Weighted Recall: 0.7712
Weighted FalsePositiveRate: 0.11424058122062401
Kappa statistic: 0.6569053437832448
Training time: 126.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 70.94666666666811
Incorrectly Classified Instances: 24.72
Correctly Classified Instances: 75.28
Weighted Precision: 0.7703594728395784
Weighted AreaUnderROC: 0.9124136416562647
Root mean squared error: 0.34337151891209555
Relative absolute error: 53.73599999999925
Root relative squared error: 72.84009884672012
Weighted TruePositiveRate: 0.7528
Weighted MatthewsCorrelation: 0.6390971069209014
Weighted FMeasure: 0.7485648807694958
Iteration time: 141.0
Weighted AreaUnderPRC: 0.8032527414511244
Mean absolute error: 0.23882666666666447
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1314.0
Weighted Recall: 0.7528
Weighted FalsePositiveRate: 0.12384420373078071
Kappa statistic: 0.6291515226870457
Training time: 133.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 70.8800000000013
Incorrectly Classified Instances: 21.96
Correctly Classified Instances: 78.04
Weighted Precision: 0.7966934255737484
Weighted AreaUnderROC: 0.9268162015538053
Root mean squared error: 0.32835346807975
Relative absolute error: 50.849999999999206
Root relative squared error: 69.65428917159338
Weighted TruePositiveRate: 0.7804
Weighted MatthewsCorrelation: 0.6796748884827223
Weighted FMeasure: 0.7778321824482196
Iteration time: 147.0
Weighted AreaUnderPRC: 0.8362433953542244
Mean absolute error: 0.22599999999999754
Coverage of cases: 99.4
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 1461.0
Weighted Recall: 0.7804
Weighted FalsePositiveRate: 0.10994000915023516
Kappa statistic: 0.6705734613980503
Training time: 138.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 70.45333333333471
Incorrectly Classified Instances: 22.04
Correctly Classified Instances: 77.96
Weighted Precision: 0.7909444690090303
Weighted AreaUnderROC: 0.9256303228385099
Root mean squared error: 0.3296907642018491
Relative absolute error: 51.47399999999915
Root relative squared error: 69.9379725185106
Weighted TruePositiveRate: 0.7796
Weighted MatthewsCorrelation: 0.6759630880127708
Weighted FMeasure: 0.7773948328824242
Iteration time: 151.0
Weighted AreaUnderPRC: 0.830921110234622
Mean absolute error: 0.22877333333333064
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1612.0
Weighted Recall: 0.7796
Weighted FalsePositiveRate: 0.11033546641263801
Kappa statistic: 0.6694080399964673
Training time: 143.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.17333333333455
Incorrectly Classified Instances: 22.4
Correctly Classified Instances: 77.6
Weighted Precision: 0.789591460063764
Weighted AreaUnderROC: 0.9258276697762529
Root mean squared error: 0.330853441874192
Relative absolute error: 51.401999999999134
Root relative squared error: 70.18461369844496
Weighted TruePositiveRate: 0.776
Weighted MatthewsCorrelation: 0.6716682667060929
Weighted FMeasure: 0.7732719851337623
Iteration time: 153.0
Weighted AreaUnderPRC: 0.8328064225763983
Mean absolute error: 0.22845333333333057
Coverage of cases: 99.24
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1765.0
Weighted Recall: 0.776
Weighted FalsePositiveRate: 0.11216995441506074
Kappa statistic: 0.6639606699568117
Training time: 145.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 71.0133333333346
Incorrectly Classified Instances: 23.72
Correctly Classified Instances: 76.28
Weighted Precision: 0.7786210337425192
Weighted AreaUnderROC: 0.9127086526098046
Root mean squared error: 0.3395565735877694
Relative absolute error: 52.33799999999927
Root relative squared error: 72.03082673411403
Weighted TruePositiveRate: 0.7628
Weighted MatthewsCorrelation: 0.6534476594419368
Weighted FMeasure: 0.7586446490886051
Iteration time: 162.0
Weighted AreaUnderPRC: 0.8088947761911357
Mean absolute error: 0.23261333333333115
Coverage of cases: 98.88
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1927.0
Weighted Recall: 0.7628
Weighted FalsePositiveRate: 0.11878426101732402
Kappa statistic: 0.6441843156446336
Training time: 153.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 70.02666666666808
Incorrectly Classified Instances: 23.52
Correctly Classified Instances: 76.48
Weighted Precision: 0.7805433355435057
Weighted AreaUnderROC: 0.9190801760083148
Root mean squared error: 0.33530483642997577
Relative absolute error: 51.74399999999919
Root relative squared error: 71.12889708128444
Weighted TruePositiveRate: 0.7648
Weighted MatthewsCorrelation: 0.6558625952514356
Weighted FMeasure: 0.761436754260105
Iteration time: 161.0
Weighted AreaUnderPRC: 0.8207502945977818
Mean absolute error: 0.2299733333333308
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2088.0
Weighted Recall: 0.7648
Weighted FalsePositiveRate: 0.11787114705353033
Kappa statistic: 0.6471216892452774
Training time: 153.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 69.88000000000144
Incorrectly Classified Instances: 23.28
Correctly Classified Instances: 76.72
Weighted Precision: 0.7854384213179597
Weighted AreaUnderROC: 0.9194370710285197
Root mean squared error: 0.33661451741321663
Relative absolute error: 51.88199999999921
Root relative squared error: 71.40672237261663
Weighted TruePositiveRate: 0.7672
Weighted MatthewsCorrelation: 0.6609974348968819
Weighted FMeasure: 0.763145711972665
Iteration time: 176.0
Weighted AreaUnderPRC: 0.8203466217147175
Mean absolute error: 0.23058666666666422
Coverage of cases: 99.04
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2264.0
Weighted Recall: 0.7672
Weighted FalsePositiveRate: 0.11663671123491312
Kappa statistic: 0.6507447897363614
Training time: 168.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 69.38666666666788
Incorrectly Classified Instances: 24.64
Correctly Classified Instances: 75.36
Weighted Precision: 0.771520111640222
Weighted AreaUnderROC: 0.9165250636215995
Root mean squared error: 0.33883722739195327
Relative absolute error: 52.24799999999918
Root relative squared error: 71.87823036218934
Weighted TruePositiveRate: 0.7536
Weighted MatthewsCorrelation: 0.6402019994629607
Weighted FMeasure: 0.7483869578476533
Iteration time: 176.0
Weighted AreaUnderPRC: 0.8122336613479206
Mean absolute error: 0.23221333333333077
Coverage of cases: 98.88
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2440.0
Weighted Recall: 0.7536
Weighted FalsePositiveRate: 0.12359293856260357
Kappa statistic: 0.630260623044463
Training time: 168.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 70.56000000000134
Incorrectly Classified Instances: 24.96
Correctly Classified Instances: 75.04
Weighted Precision: 0.7740177178714803
Weighted AreaUnderROC: 0.9195158891014628
Root mean squared error: 0.339741077881377
Relative absolute error: 52.85999999999923
Root relative squared error: 72.06996600526443
Weighted TruePositiveRate: 0.7504
Weighted MatthewsCorrelation: 0.6382388147114298
Weighted FMeasure: 0.7434972554963557
Iteration time: 180.0
Weighted AreaUnderPRC: 0.818826178099551
Mean absolute error: 0.23493333333333102
Coverage of cases: 99.28
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2620.0
Weighted Recall: 0.7504
Weighted FalsePositiveRate: 0.125264029384831
Kappa statistic: 0.6254331829223462
Training time: 172.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 70.9066666666679
Incorrectly Classified Instances: 25.28
Correctly Classified Instances: 74.72
Weighted Precision: 0.7754779459253658
Weighted AreaUnderROC: 0.9147261016601244
Root mean squared error: 0.34420923869065423
Relative absolute error: 53.5319999999992
Root relative squared error: 73.017806047566
Weighted TruePositiveRate: 0.7472
Weighted MatthewsCorrelation: 0.6353490184257741
Weighted FMeasure: 0.7384625053103439
Iteration time: 181.0
Weighted AreaUnderPRC: 0.8096942153002162
Mean absolute error: 0.23791999999999758
Coverage of cases: 99.28
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2801.0
Weighted Recall: 0.7472
Weighted FalsePositiveRate: 0.12705184714047923
Kappa statistic: 0.620534100655011
Training time: 173.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 71.0533333333347
Incorrectly Classified Instances: 24.84
Correctly Classified Instances: 75.16
Weighted Precision: 0.7817207401649982
Weighted AreaUnderROC: 0.9172130505770936
Root mean squared error: 0.3422630567268398
Relative absolute error: 53.525999999999186
Root relative squared error: 72.60495850835517
Weighted TruePositiveRate: 0.7516
Weighted MatthewsCorrelation: 0.6429617104820183
Weighted FMeasure: 0.7414633714959077
Iteration time: 195.0
Weighted AreaUnderPRC: 0.812590727595105
Mean absolute error: 0.23789333333333085
Coverage of cases: 99.32
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2996.0
Weighted Recall: 0.7516
Weighted FalsePositiveRate: 0.12481503558545241
Kappa statistic: 0.6271217134772025
Training time: 187.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 71.26666666666786
Incorrectly Classified Instances: 24.24
Correctly Classified Instances: 75.76
Weighted Precision: 0.7849540414038192
Weighted AreaUnderROC: 0.917133204584369
Root mean squared error: 0.34175624841885943
Relative absolute error: 52.871999999999204
Root relative squared error: 72.49744823095477
Weighted TruePositiveRate: 0.7576
Weighted MatthewsCorrelation: 0.6505362765496358
Weighted FMeasure: 0.7503213259648077
Iteration time: 193.0
Weighted AreaUnderPRC: 0.8144651143590229
Mean absolute error: 0.23498666666666423
Coverage of cases: 99.16
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3189.0
Weighted Recall: 0.7576
Weighted FalsePositiveRate: 0.12177357692521741
Kappa statistic: 0.6361782433628946
Training time: 185.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 71.22666666666807
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7737311341265739
Weighted AreaUnderROC: 0.9131903694136405
Root mean squared error: 0.34880749227427194
Relative absolute error: 54.53999999999918
Root relative squared error: 73.99324293474342
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6244606884879303
Weighted FMeasure: 0.7258123597254998
Iteration time: 209.0
Weighted AreaUnderPRC: 0.803638704203781
Mean absolute error: 0.2423999999999975
Coverage of cases: 99.04
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3398.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13194839267374064
Kappa statistic: 0.60608086247154
Training time: 201.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.05333333333462
Incorrectly Classified Instances: 25.68
Correctly Classified Instances: 74.32
Weighted Precision: 0.77468715399426
Weighted AreaUnderROC: 0.9188953401740857
Root mean squared error: 0.3405877273185275
Relative absolute error: 52.44599999999913
Root relative squared error: 72.24956747275348
Weighted TruePositiveRate: 0.7432
Weighted MatthewsCorrelation: 0.630650315234841
Weighted FMeasure: 0.7324831279325162
Iteration time: 207.0
Weighted AreaUnderPRC: 0.8164599467966612
Mean absolute error: 0.23309333333333057
Coverage of cases: 99.0
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 3605.0
Weighted Recall: 0.7432
Weighted FalsePositiveRate: 0.12929005248014014
Kappa statistic: 0.6144348852589449
Training time: 199.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 68.92000000000115
Incorrectly Classified Instances: 25.36
Correctly Classified Instances: 74.64
Weighted Precision: 0.7771402326136224
Weighted AreaUnderROC: 0.919551109628635
Root mean squared error: 0.34135416603092217
Relative absolute error: 51.947999999999226
Root relative squared error: 72.41215367602294
Weighted TruePositiveRate: 0.7464
Weighted MatthewsCorrelation: 0.635207505436583
Weighted FMeasure: 0.7366568847024894
Iteration time: 207.0
Weighted AreaUnderPRC: 0.8198444897506509
Mean absolute error: 0.23087999999999764
Coverage of cases: 99.0
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3812.0
Weighted Recall: 0.7464
Weighted FalsePositiveRate: 0.1275296998852968
Kappa statistic: 0.6192794027995093
Training time: 199.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 71.69333333333468
Incorrectly Classified Instances: 25.16
Correctly Classified Instances: 74.84
Weighted Precision: 0.7824297131899233
Weighted AreaUnderROC: 0.9203778554190569
Root mean squared error: 0.34067384206402773
Relative absolute error: 53.39999999999914
Root relative squared error: 72.2678351689045
Weighted TruePositiveRate: 0.7484
Weighted MatthewsCorrelation: 0.6393820551599759
Weighted FMeasure: 0.7398583008350833
Iteration time: 217.0
Weighted AreaUnderPRC: 0.8219672770531464
Mean absolute error: 0.23733333333333062
Coverage of cases: 99.52
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4029.0
Weighted Recall: 0.7484
Weighted FalsePositiveRate: 0.12680884204713747
Kappa statistic: 0.6221888205371668
Training time: 210.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 70.96000000000122
Incorrectly Classified Instances: 25.68
Correctly Classified Instances: 74.32
Weighted Precision: 0.7715209436834776
Weighted AreaUnderROC: 0.9144734580299819
Root mean squared error: 0.34464281026399823
Relative absolute error: 53.81999999999923
Root relative squared error: 73.10978046745838
Weighted TruePositiveRate: 0.7432
Weighted MatthewsCorrelation: 0.6293549589156366
Weighted FMeasure: 0.7323882178585738
Iteration time: 219.0
Weighted AreaUnderPRC: 0.8053043214878312
Mean absolute error: 0.2391999999999977
Coverage of cases: 99.16
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4248.0
Weighted Recall: 0.7432
Weighted FalsePositiveRate: 0.12923512215853036
Kappa statistic: 0.6144489634922804
Training time: 212.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 71.06666666666804
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7756740759783848
Weighted AreaUnderROC: 0.9176554319944549
Root mean squared error: 0.3449444399706517
Relative absolute error: 53.843999999999106
Root relative squared error: 73.17376579075297
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6251337695630483
Weighted FMeasure: 0.7254331400218812
Iteration time: 225.0
Weighted AreaUnderPRC: 0.8123604497223738
Mean absolute error: 0.23930666666666384
Coverage of cases: 99.12
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 4473.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13218184654058232
Kappa statistic: 0.605953182432624
Training time: 217.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 70.21333333333455
Incorrectly Classified Instances: 26.96
Correctly Classified Instances: 73.04
Weighted Precision: 0.7702608328555088
Weighted AreaUnderROC: 0.918608522078014
Root mean squared error: 0.3449753614390448
Relative absolute error: 53.63999999999913
Root relative squared error: 73.18032522474846
Weighted TruePositiveRate: 0.7304
Weighted MatthewsCorrelation: 0.6150583804626832
Weighted FMeasure: 0.7179840750669795
Iteration time: 231.0
Weighted AreaUnderPRC: 0.8143463316842272
Mean absolute error: 0.23839999999999723
Coverage of cases: 98.96
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 4704.0
Weighted Recall: 0.7304
Weighted FalsePositiveRate: 0.13580275851401916
Kappa statistic: 0.5951224388190358
Training time: 224.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 71.05333333333458
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.778065939910447
Weighted AreaUnderROC: 0.9173857549775184
Root mean squared error: 0.3467025622441613
Relative absolute error: 54.13199999999916
Root relative squared error: 73.54671984527909
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.6283235641593021
Weighted FMeasure: 0.7270301614631252
Iteration time: 241.0
Weighted AreaUnderPRC: 0.8148765110523732
Mean absolute error: 0.24058666666666403
Coverage of cases: 99.36
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 4945.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.13122066854538003
Kappa statistic: 0.6089458766676566
Training time: 232.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 71.21333333333462
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7867448070948742
Weighted AreaUnderROC: 0.9209837719206314
Root mean squared error: 0.33920691816844317
Relative absolute error: 53.0399999999992
Root relative squared error: 71.95665361868878
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6452238807143559
Weighted FMeasure: 0.7427399077182119
Iteration time: 244.0
Weighted AreaUnderPRC: 0.8211461951353441
Mean absolute error: 0.23573333333333088
Coverage of cases: 98.92
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5189.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12498122033491595
Kappa statistic: 0.6275913038004428
Training time: 235.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 71.60000000000122
Incorrectly Classified Instances: 25.76
Correctly Classified Instances: 74.24
Weighted Precision: 0.7788356025575862
Weighted AreaUnderROC: 0.9217112693891387
Root mean squared error: 0.3419941519967846
Relative absolute error: 53.84399999999904
Root relative squared error: 72.54791520092061
Weighted TruePositiveRate: 0.7424
Weighted MatthewsCorrelation: 0.6315680448992181
Weighted FMeasure: 0.731677956844499
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8227697853247314
Mean absolute error: 0.23930666666666356
Coverage of cases: 99.52
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 5436.0
Weighted Recall: 0.7424
Weighted FalsePositiveRate: 0.12987776586817737
Kappa statistic: 0.6131419290954032
Training time: 239.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 70.34666666666786
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7633305581685506
Weighted AreaUnderROC: 0.9150425302351668
Root mean squared error: 0.3476166470889057
Relative absolute error: 54.05399999999908
Root relative squared error: 73.74062652296868
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.6078184180697406
Weighted FMeasure: 0.7129325263756662
Iteration time: 251.0
Weighted AreaUnderPRC: 0.8105043936431957
Mean absolute error: 0.240239999999997
Coverage of cases: 99.28
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 5687.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.1378349751476433
Kappa statistic: 0.5891172291795969
Training time: 245.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 70.93333333333452
Incorrectly Classified Instances: 26.16
Correctly Classified Instances: 73.84
Weighted Precision: 0.7735893779303064
Weighted AreaUnderROC: 0.92218706304194
Root mean squared error: 0.3426679636810729
Relative absolute error: 53.27399999999921
Root relative squared error: 72.69085224428149
Weighted TruePositiveRate: 0.7384
Weighted MatthewsCorrelation: 0.6251178035901793
Weighted FMeasure: 0.728525462780109
Iteration time: 257.0
Weighted AreaUnderPRC: 0.8231452416328637
Mean absolute error: 0.23677333333333095
Coverage of cases: 99.2
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 5944.0
Weighted Recall: 0.7384
Weighted FalsePositiveRate: 0.13194431395280762
Kappa statistic: 0.6070971472128889
Training time: 252.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 70.20000000000122
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.773954896883967
Weighted AreaUnderROC: 0.9167881388811995
Root mean squared error: 0.344696968364968
Relative absolute error: 53.465999999999234
Root relative squared error: 73.12126913559395
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6227821825693016
Weighted FMeasure: 0.724240353046
Iteration time: 274.0
Weighted AreaUnderPRC: 0.8104087052324327
Mean absolute error: 0.23762666666666438
Coverage of cases: 98.8
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 6218.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13297276106538805
Kappa statistic: 0.6035269283308844
Training time: 269.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 69.14666666666787
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7844562529069111
Weighted AreaUnderROC: 0.9220577361046162
Root mean squared error: 0.33799802760765674
Relative absolute error: 51.59399999999907
Root relative squared error: 71.70020920471542
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.6429552313530994
Weighted FMeasure: 0.7425300415180476
Iteration time: 267.0
Weighted AreaUnderPRC: 0.8228392979997993
Mean absolute error: 0.2293066666666636
Coverage of cases: 99.08
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6485.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.12554007477751414
Kappa statistic: 0.6257876179549865
Training time: 261.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 71.69333333333456
Incorrectly Classified Instances: 25.52
Correctly Classified Instances: 74.48
Weighted Precision: 0.7801998438985561
Weighted AreaUnderROC: 0.9219022668153639
Root mean squared error: 0.3406855832973658
Relative absolute error: 53.95799999999905
Root relative squared error: 72.27032586061836
Weighted TruePositiveRate: 0.7448
Weighted MatthewsCorrelation: 0.6345400901392625
Weighted FMeasure: 0.7370028081898492
Iteration time: 268.0
Weighted AreaUnderPRC: 0.8239200724172832
Mean absolute error: 0.23981333333333027
Coverage of cases: 99.32
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6753.0
Weighted Recall: 0.7448
Weighted FalsePositiveRate: 0.12852660311613934
Kappa statistic: 0.6167951824307198
Training time: 262.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 70.6533333333346
Incorrectly Classified Instances: 23.92
Correctly Classified Instances: 76.08
Weighted Precision: 0.7904939796768111
Weighted AreaUnderROC: 0.9274674796595187
Root mean squared error: 0.3335825734856859
Relative absolute error: 52.007999999999264
Root relative squared error: 70.76354993921632
Weighted TruePositiveRate: 0.7608
Weighted MatthewsCorrelation: 0.6563218379027483
Weighted FMeasure: 0.7529818364712624
Iteration time: 268.0
Weighted AreaUnderPRC: 0.833035994623775
Mean absolute error: 0.23114666666666447
Coverage of cases: 99.48
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 7021.0
Weighted Recall: 0.7608
Weighted FalsePositiveRate: 0.1205487949660697
Kappa statistic: 0.6408008866812093
Training time: 262.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 70.36000000000124
Incorrectly Classified Instances: 23.28
Correctly Classified Instances: 76.72
Weighted Precision: 0.7946053823430484
Weighted AreaUnderROC: 0.9264290296630965
Root mean squared error: 0.33434911893608
Relative absolute error: 52.13399999999912
Root relative squared error: 70.92615878503473
Weighted TruePositiveRate: 0.7672
Weighted MatthewsCorrelation: 0.6648781915878175
Weighted FMeasure: 0.7600330026896273
Iteration time: 270.0
Weighted AreaUnderPRC: 0.830772071048781
Mean absolute error: 0.23170666666666384
Coverage of cases: 99.16
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 7291.0
Weighted Recall: 0.7672
Weighted FalsePositiveRate: 0.11727527622362713
Kappa statistic: 0.6504355021067567
Training time: 265.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 71.0666666666681
Incorrectly Classified Instances: 24.68
Correctly Classified Instances: 75.32
Weighted Precision: 0.7770750660150408
Weighted AreaUnderROC: 0.9274481215636929
Root mean squared error: 0.3327621773379093
Relative absolute error: 52.66199999999904
Root relative squared error: 70.58951763541067
Weighted TruePositiveRate: 0.7532
Weighted MatthewsCorrelation: 0.6418761107682531
Weighted FMeasure: 0.7485917551024676
Iteration time: 282.0
Weighted AreaUnderPRC: 0.835852449773115
Mean absolute error: 0.23405333333333014
Coverage of cases: 99.84
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 7573.0
Weighted Recall: 0.7532
Weighted FalsePositiveRate: 0.1243193715392994
Kappa statistic: 0.6294220401271664
Training time: 277.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 71.09333333333478
Incorrectly Classified Instances: 24.52
Correctly Classified Instances: 75.48
Weighted Precision: 0.7754318508419294
Weighted AreaUnderROC: 0.9263865457573193
Root mean squared error: 0.33261389026918203
Relative absolute error: 52.22399999999901
Root relative squared error: 70.55806119785288
Weighted TruePositiveRate: 0.7548
Weighted MatthewsCorrelation: 0.642764830378029
Weighted FMeasure: 0.7503835920232214
Iteration time: 296.0
Weighted AreaUnderPRC: 0.8322935335026462
Mean absolute error: 0.23210666666666335
Coverage of cases: 99.6
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 7869.0
Weighted Recall: 0.7548
Weighted FalsePositiveRate: 0.12341173008107283
Kappa statistic: 0.6318631400476692
Training time: 287.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 69.76000000000127
Incorrectly Classified Instances: 24.04
Correctly Classified Instances: 75.96
Weighted Precision: 0.7857294651148837
Weighted AreaUnderROC: 0.9217217728616747
Root mean squared error: 0.3345484518969802
Relative absolute error: 51.71399999999915
Root relative squared error: 70.96844369154469
Weighted TruePositiveRate: 0.7596
Weighted MatthewsCorrelation: 0.6526441839505911
Weighted FMeasure: 0.7537238840829894
Iteration time: 307.0
Weighted AreaUnderPRC: 0.8240102726960198
Mean absolute error: 0.22983999999999732
Coverage of cases: 99.08
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 8176.0
Weighted Recall: 0.7596
Weighted FalsePositiveRate: 0.12108018424786296
Kappa statistic: 0.6390400796442149
Training time: 300.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 71.30666666666818
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.7818736518037269
Weighted AreaUnderROC: 0.9246260528733256
Root mean squared error: 0.33079298662456413
Relative absolute error: 52.20599999999907
Root relative squared error: 70.17178920335391
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.6545702998993572
Weighted FMeasure: 0.7613069480041191
Iteration time: 303.0
Weighted AreaUnderPRC: 0.8287156259684659
Mean absolute error: 0.23202666666666363
Coverage of cases: 99.48
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 8479.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.11902736874363513
Kappa statistic: 0.6450881886790458
Training time: 298.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 69.76000000000144
Incorrectly Classified Instances: 21.92
Correctly Classified Instances: 78.08
Weighted Precision: 0.7990651883278123
Weighted AreaUnderROC: 0.9324260550416278
Root mean squared error: 0.3230892549538998
Relative absolute error: 50.3999999999991
Root relative squared error: 68.5375809319234
Weighted TruePositiveRate: 0.7808
Weighted MatthewsCorrelation: 0.6804650641095207
Weighted FMeasure: 0.7787344839393457
Iteration time: 304.0
Weighted AreaUnderPRC: 0.843580945726488
Mean absolute error: 0.2239999999999971
Coverage of cases: 99.44
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 8783.0
Weighted Recall: 0.7808
Weighted FalsePositiveRate: 0.11046324099016241
Kappa statistic: 0.6708943530035215
Training time: 299.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 71.34666666666809
Incorrectly Classified Instances: 22.4
Correctly Classified Instances: 77.6
Weighted Precision: 0.7916623657948886
Weighted AreaUnderROC: 0.9314395612512839
Root mean squared error: 0.3251379194536772
Relative absolute error: 51.923999999999054
Root relative squared error: 68.97216829997403
Weighted TruePositiveRate: 0.776
Weighted MatthewsCorrelation: 0.6720120699461669
Weighted FMeasure: 0.7738498170560145
Iteration time: 313.0
Weighted AreaUnderPRC: 0.8413600183842124
Mean absolute error: 0.23077333333333022
Coverage of cases: 99.6
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9096.0
Weighted Recall: 0.776
Weighted FalsePositiveRate: 0.11281538569397594
Kappa statistic: 0.6637060791131449
Training time: 308.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 71.17333333333488
Incorrectly Classified Instances: 22.56
Correctly Classified Instances: 77.44
Weighted Precision: 0.7880554123652864
Weighted AreaUnderROC: 0.9280049263102508
Root mean squared error: 0.3256583076375185
Relative absolute error: 51.88199999999902
Root relative squared error: 69.0825593040671
Weighted TruePositiveRate: 0.7744
Weighted MatthewsCorrelation: 0.6684680480101426
Weighted FMeasure: 0.773296092356792
Iteration time: 320.0
Weighted AreaUnderPRC: 0.8335932383498658
Mean absolute error: 0.23058666666666336
Coverage of cases: 99.68
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9416.0
Weighted Recall: 0.7744
Weighted FalsePositiveRate: 0.11365436425019027
Kappa statistic: 0.6613039796782387
Training time: 315.0
		
Time end:Sun Oct 08 11.54.02 EEST 2017