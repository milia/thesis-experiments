Sun Oct 08 11.21.39 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.21.39 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.5200000000016
Incorrectly Classified Instances: 21.24
Correctly Classified Instances: 78.76
Weighted Precision: 0.7878745437864889
Weighted AreaUnderROC: 0.926182392085408
Root mean squared error: 0.32332439850197775
Relative absolute error: 51.49799999999914
Root relative squared error: 68.58746241114285
Weighted TruePositiveRate: 0.7876
Weighted MatthewsCorrelation: 0.6813655382205834
Weighted FMeasure: 0.7876061333024962
Iteration time: 99.0
Weighted AreaUnderPRC: 0.8325501782897005
Mean absolute error: 0.22887999999999725
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 99.0
Weighted Recall: 0.7876
Weighted FalsePositiveRate: 0.10641522523961544
Kappa statistic: 0.681361253528429
Training time: 90.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 71.41333333333476
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.7833828187571324
Weighted AreaUnderROC: 0.9247252508906302
Root mean squared error: 0.3231759066927262
Relative absolute error: 51.311999999999074
Root relative squared error: 68.55596254156113
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6746486138066686
Weighted FMeasure: 0.7832453042491626
Iteration time: 103.0
Weighted AreaUnderPRC: 0.830117246048549
Mean absolute error: 0.22805333333333028
Coverage of cases: 99.48
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 202.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10864517050444106
Kappa statistic: 0.674775282921502
Training time: 94.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 70.81333333333491
Incorrectly Classified Instances: 21.28
Correctly Classified Instances: 78.72
Weighted Precision: 0.7881324316585806
Weighted AreaUnderROC: 0.9307383444001065
Root mean squared error: 0.3198124450361481
Relative absolute error: 51.089999999998994
Root relative squared error: 67.84246457787293
Weighted TruePositiveRate: 0.7872
Weighted MatthewsCorrelation: 0.6810348490325611
Weighted FMeasure: 0.7874212351464946
Iteration time: 106.0
Weighted AreaUnderPRC: 0.8411515816917315
Mean absolute error: 0.22706666666666325
Coverage of cases: 99.8
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 308.0
Weighted Recall: 0.7872
Weighted FalsePositiveRate: 0.10670908193363404
Kappa statistic: 0.6807204099838007
Training time: 96.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 70.73333333333483
Incorrectly Classified Instances: 21.72
Correctly Classified Instances: 78.28
Weighted Precision: 0.7824269295127151
Weighted AreaUnderROC: 0.929173717142091
Root mean squared error: 0.3184671620957681
Relative absolute error: 49.79999999999904
Root relative squared error: 67.55708697094576
Weighted TruePositiveRate: 0.7828
Weighted MatthewsCorrelation: 0.673997999955577
Weighted FMeasure: 0.7823263778433275
Iteration time: 109.0
Weighted AreaUnderPRC: 0.840284423900932
Mean absolute error: 0.2213333333333301
Coverage of cases: 99.56
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 417.0
Weighted Recall: 0.7828
Weighted FalsePositiveRate: 0.10863691042960573
Kappa statistic: 0.6742465436538428
Training time: 100.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 72.58666666666817
Incorrectly Classified Instances: 21.96
Correctly Classified Instances: 78.04
Weighted Precision: 0.7824519464841129
Weighted AreaUnderROC: 0.9244399727950354
Root mean squared error: 0.3255764119219931
Relative absolute error: 52.385999999999086
Root relative squared error: 69.06518659932766
Weighted TruePositiveRate: 0.7804
Weighted MatthewsCorrelation: 0.6714605130257281
Weighted FMeasure: 0.7804287201354245
Iteration time: 116.0
Weighted AreaUnderPRC: 0.8264217515895518
Mean absolute error: 0.23282666666666368
Coverage of cases: 99.6
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 533.0
Weighted Recall: 0.7804
Weighted FalsePositiveRate: 0.11020092817788174
Kappa statistic: 0.67047728574121
Training time: 107.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 71.5200000000014
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.7951376157195408
Weighted AreaUnderROC: 0.9311988059159956
Root mean squared error: 0.3191697562948787
Relative absolute error: 50.663999999999035
Root relative squared error: 67.70612970772979
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.6927528053510272
Weighted FMeasure: 0.7949795931456136
Iteration time: 119.0
Weighted AreaUnderPRC: 0.8411842149915865
Mean absolute error: 0.2251733333333301
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 652.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.1023905959289404
Kappa statistic: 0.6928312820622348
Training time: 111.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 71.4133333333348
Incorrectly Classified Instances: 21.08
Correctly Classified Instances: 78.92
Weighted Precision: 0.7894219115057933
Weighted AreaUnderROC: 0.9330314664010737
Root mean squared error: 0.31613499226332503
Relative absolute error: 50.237999999999104
Root relative squared error: 67.06235903992601
Weighted TruePositiveRate: 0.7892
Weighted MatthewsCorrelation: 0.6838240626182663
Weighted FMeasure: 0.7889559591540446
Iteration time: 123.0
Weighted AreaUnderPRC: 0.8479099781120601
Mean absolute error: 0.22327999999999706
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 775.0
Weighted Recall: 0.7892
Weighted FalsePositiveRate: 0.10553504894219377
Kappa statistic: 0.6838048061669463
Training time: 115.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 71.0133333333347
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8008481615338379
Weighted AreaUnderROC: 0.9318146672631908
Root mean squared error: 0.3172254718650422
Relative absolute error: 50.093999999998985
Root relative squared error: 67.29368469626195
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7008818111784909
Weighted FMeasure: 0.7997909123551954
Iteration time: 129.0
Weighted AreaUnderPRC: 0.8427165326617989
Mean absolute error: 0.22263999999999656
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 904.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09984072259392542
Kappa statistic: 0.7006509611803686
Training time: 121.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 70.49333333333468
Incorrectly Classified Instances: 20.88
Correctly Classified Instances: 79.12
Weighted Precision: 0.7932153058550004
Weighted AreaUnderROC: 0.9258239053868575
Root mean squared error: 0.3239835386764776
Relative absolute error: 50.53199999999918
Root relative squared error: 68.72728715728526
Weighted TruePositiveRate: 0.7912
Weighted MatthewsCorrelation: 0.6882670393063861
Weighted FMeasure: 0.7897304853854794
Iteration time: 130.0
Weighted AreaUnderPRC: 0.8313842719435248
Mean absolute error: 0.22458666666666408
Coverage of cases: 99.64
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1034.0
Weighted Recall: 0.7912
Weighted FalsePositiveRate: 0.10424428901733268
Kappa statistic: 0.6869332412125401
Training time: 122.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 70.46666666666799
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.7934213635903863
Weighted AreaUnderROC: 0.9288600009685082
Root mean squared error: 0.3205952796491747
Relative absolute error: 50.26199999999911
Root relative squared error: 68.00852887689854
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.6889453257528104
Weighted FMeasure: 0.7912611210893891
Iteration time: 135.0
Weighted AreaUnderPRC: 0.8384242059720791
Mean absolute error: 0.22338666666666376
Coverage of cases: 99.4
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1169.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10380076772352125
Kappa statistic: 0.6881128283032332
Training time: 126.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 71.22666666666808
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.7965953366136406
Weighted AreaUnderROC: 0.9331205313641294
Root mean squared error: 0.31666596491150156
Relative absolute error: 49.75799999999905
Root relative squared error: 67.17499534797106
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.6945854285907326
Weighted FMeasure: 0.7947966275636001
Iteration time: 138.0
Weighted AreaUnderPRC: 0.8480297336891283
Mean absolute error: 0.2211466666666635
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1307.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10199513866653753
Kappa statistic: 0.6940730064177485
Training time: 130.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 69.62666666666814
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.7974005979497536
Weighted AreaUnderROC: 0.9325693947762709
Root mean squared error: 0.3186806133628661
Relative absolute error: 49.27199999999903
Root relative squared error: 67.60236682247113
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.693970921430002
Weighted FMeasure: 0.7935716041420575
Iteration time: 148.0
Weighted AreaUnderPRC: 0.8475573704064012
Mean absolute error: 0.21898666666666342
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1455.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10254712681893448
Kappa statistic: 0.692272032962524
Training time: 140.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 70.97333333333489
Incorrectly Classified Instances: 22.0
Correctly Classified Instances: 78.0
Weighted Precision: 0.7803721616138725
Weighted AreaUnderROC: 0.9260227929755683
Root mean squared error: 0.3232584105634366
Relative absolute error: 50.81399999999912
Root relative squared error: 68.57346425549717
Weighted TruePositiveRate: 0.78
Weighted MatthewsCorrelation: 0.6704329583115134
Weighted FMeasure: 0.778940441302792
Iteration time: 151.0
Weighted AreaUnderPRC: 0.8331808176474573
Mean absolute error: 0.22583999999999715
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1606.0
Weighted Recall: 0.78
Weighted FalsePositiveRate: 0.10988368504399122
Kappa statistic: 0.670107307495018
Training time: 143.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.36000000000143
Incorrectly Classified Instances: 22.04
Correctly Classified Instances: 77.96
Weighted Precision: 0.7812820087420117
Weighted AreaUnderROC: 0.9228109095232554
Root mean squared error: 0.32706370429423337
Relative absolute error: 50.66999999999928
Root relative squared error: 69.38068895593308
Weighted TruePositiveRate: 0.7796
Weighted MatthewsCorrelation: 0.6707214122202663
Weighted FMeasure: 0.7779279629653459
Iteration time: 157.0
Weighted AreaUnderPRC: 0.8274702603074842
Mean absolute error: 0.22519999999999782
Coverage of cases: 99.48
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1763.0
Weighted Recall: 0.7796
Weighted FalsePositiveRate: 0.11005394851438778
Kappa statistic: 0.6695466684066943
Training time: 148.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 70.26666666666821
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.7784732841215662
Weighted AreaUnderROC: 0.9212078764299049
Root mean squared error: 0.3286700473118885
Relative absolute error: 51.17399999999913
Root relative squared error: 69.72144576814176
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.6642540909427267
Weighted FMeasure: 0.7716647536290704
Iteration time: 160.0
Weighted AreaUnderPRC: 0.824535430871074
Mean absolute error: 0.22743999999999723
Coverage of cases: 99.08
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1923.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.11284961451201275
Kappa statistic: 0.6611349496176643
Training time: 152.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 69.6800000000014
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.7893022453410748
Weighted AreaUnderROC: 0.9269930323778108
Root mean squared error: 0.3222586952537759
Relative absolute error: 49.84199999999911
Root relative squared error: 68.36139261308205
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.6820198894624689
Weighted FMeasure: 0.7852932391620884
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8357823536680778
Mean absolute error: 0.22151999999999708
Coverage of cases: 99.24
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2089.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10644676912135337
Kappa statistic: 0.6803200206514467
Training time: 158.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 69.93333333333466
Incorrectly Classified Instances: 21.48
Correctly Classified Instances: 78.52
Weighted Precision: 0.7867268806146641
Weighted AreaUnderROC: 0.9259625983776762
Root mean squared error: 0.324629019035574
Relative absolute error: 50.18399999999916
Root relative squared error: 68.8642142189972
Weighted TruePositiveRate: 0.7852
Weighted MatthewsCorrelation: 0.6791323278347825
Weighted FMeasure: 0.7833955954158162
Iteration time: 167.0
Weighted AreaUnderPRC: 0.8336821845973991
Mean absolute error: 0.22303999999999732
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2256.0
Weighted Recall: 0.7852
Weighted FalsePositiveRate: 0.10728574767756768
Kappa statistic: 0.6779238576013209
Training time: 159.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 69.41333333333463
Incorrectly Classified Instances: 20.96
Correctly Classified Instances: 79.04
Weighted Precision: 0.7964280777645004
Weighted AreaUnderROC: 0.9213288061834195
Root mean squared error: 0.3285239717280903
Relative absolute error: 50.231999999999125
Root relative squared error: 69.69045845738091
Weighted TruePositiveRate: 0.7904
Weighted MatthewsCorrelation: 0.6897067484234949
Weighted FMeasure: 0.787871265926159
Iteration time: 173.0
Weighted AreaUnderPRC: 0.8254324770514713
Mean absolute error: 0.2232533333333305
Coverage of cases: 99.32
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2429.0
Weighted Recall: 0.7904
Weighted FalsePositiveRate: 0.10442002499329633
Kappa statistic: 0.6858082211756624
Training time: 165.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 69.54666666666806
Incorrectly Classified Instances: 20.84
Correctly Classified Instances: 79.16
Weighted Precision: 0.7979359870257895
Weighted AreaUnderROC: 0.9236645562822581
Root mean squared error: 0.32574427597938344
Relative absolute error: 49.973999999999116
Root relative squared error: 69.10079594331711
Weighted TruePositiveRate: 0.7916
Weighted MatthewsCorrelation: 0.6917199753904966
Weighted FMeasure: 0.7892440299207788
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8298556607013284
Mean absolute error: 0.2221066666666638
Coverage of cases: 98.84
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2607.0
Weighted Recall: 0.7916
Weighted FalsePositiveRate: 0.10390236829190554
Kappa statistic: 0.6875785551041868
Training time: 169.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 69.42666666666798
Incorrectly Classified Instances: 22.52
Correctly Classified Instances: 77.48
Weighted Precision: 0.7817260141044011
Weighted AreaUnderROC: 0.9205814880479571
Root mean squared error: 0.3298929119173867
Relative absolute error: 50.51999999999918
Root relative squared error: 69.980854524648
Weighted TruePositiveRate: 0.7748
Weighted MatthewsCorrelation: 0.6669999374786915
Weighted FMeasure: 0.7721184426619133
Iteration time: 184.0
Weighted AreaUnderPRC: 0.8233724836803811
Mean absolute error: 0.22453333333333075
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2791.0
Weighted Recall: 0.7748
Weighted FalsePositiveRate: 0.11244042466920744
Kappa statistic: 0.6623221583360005
Training time: 176.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 70.57333333333487
Incorrectly Classified Instances: 21.76
Correctly Classified Instances: 78.24
Weighted Precision: 0.7894323210025068
Weighted AreaUnderROC: 0.9224248172301963
Root mean squared error: 0.3310508520856968
Relative absolute error: 51.73799999999919
Root relative squared error: 70.2264907282141
Weighted TruePositiveRate: 0.7824
Weighted MatthewsCorrelation: 0.678384796213536
Weighted FMeasure: 0.7799216164185032
Iteration time: 185.0
Weighted AreaUnderPRC: 0.8267192169848031
Mean absolute error: 0.22994666666666413
Coverage of cases: 99.44
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2976.0
Weighted Recall: 0.7824
Weighted FalsePositiveRate: 0.1085325222919532
Kappa statistic: 0.6737775172262261
Training time: 177.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 69.78666666666803
Incorrectly Classified Instances: 21.28
Correctly Classified Instances: 78.72
Weighted Precision: 0.79409551301801
Weighted AreaUnderROC: 0.9237094556766513
Root mean squared error: 0.3286132478562998
Relative absolute error: 50.7899999999992
Root relative squared error: 69.70939678407743
Weighted TruePositiveRate: 0.7872
Weighted MatthewsCorrelation: 0.6854580539422688
Weighted FMeasure: 0.7848984886763458
Iteration time: 192.0
Weighted AreaUnderPRC: 0.829852389939975
Mean absolute error: 0.22573333333333084
Coverage of cases: 99.24
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3168.0
Weighted Recall: 0.7872
Weighted FalsePositiveRate: 0.10620784274894458
Kappa statistic: 0.6809401566080052
Training time: 184.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 70.25333333333475
Incorrectly Classified Instances: 21.84
Correctly Classified Instances: 78.16
Weighted Precision: 0.7890095922654584
Weighted AreaUnderROC: 0.918999712049472
Root mean squared error: 0.33070833070849465
Relative absolute error: 51.09599999999919
Root relative squared error: 70.15383097165781
Weighted TruePositiveRate: 0.7816
Weighted MatthewsCorrelation: 0.677289994452575
Weighted FMeasure: 0.7794901978346367
Iteration time: 199.0
Weighted AreaUnderPRC: 0.8205117987196617
Mean absolute error: 0.2270933333333308
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3367.0
Weighted Recall: 0.7816
Weighted FalsePositiveRate: 0.1089623110053622
Kappa statistic: 0.6725527999614259
Training time: 191.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.42666666666825
Incorrectly Classified Instances: 21.52
Correctly Classified Instances: 78.48
Weighted Precision: 0.7893381460568181
Weighted AreaUnderROC: 0.9285127937778401
Root mean squared error: 0.32334089338240635
Relative absolute error: 50.039999999999225
Root relative squared error: 68.59096150368464
Weighted TruePositiveRate: 0.7848
Weighted MatthewsCorrelation: 0.6806533153148312
Weighted FMeasure: 0.7819753075917206
Iteration time: 199.0
Weighted AreaUnderPRC: 0.8378681750426283
Mean absolute error: 0.2223999999999976
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3566.0
Weighted Recall: 0.7848
Weighted FalsePositiveRate: 0.10753154034017771
Kappa statistic: 0.6772917265996313
Training time: 191.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 70.74666666666813
Incorrectly Classified Instances: 22.12
Correctly Classified Instances: 77.88
Weighted Precision: 0.7861614375994458
Weighted AreaUnderROC: 0.9222828474288636
Root mean squared error: 0.32994140894003077
Relative absolute error: 51.16199999999921
Root relative squared error: 69.99114229672169
Weighted TruePositiveRate: 0.7788
Weighted MatthewsCorrelation: 0.6729452347671883
Weighted FMeasure: 0.7767229557290131
Iteration time: 204.0
Weighted AreaUnderPRC: 0.8286022786458458
Mean absolute error: 0.2273866666666642
Coverage of cases: 99.36
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 3770.0
Weighted Recall: 0.7788
Weighted FalsePositiveRate: 0.11051806867880289
Kappa statistic: 0.6683048687327016
Training time: 196.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 68.70666666666797
Incorrectly Classified Instances: 21.8
Correctly Classified Instances: 78.2
Weighted Precision: 0.7889792482126191
Weighted AreaUnderROC: 0.9280388173068097
Root mean squared error: 0.32298193963956023
Relative absolute error: 49.14599999999892
Root relative squared error: 68.514815915975
Weighted TruePositiveRate: 0.782
Weighted MatthewsCorrelation: 0.67759653649279
Weighted FMeasure: 0.7798250910141215
Iteration time: 210.0
Weighted AreaUnderPRC: 0.8403968633804183
Mean absolute error: 0.2184266666666629
Coverage of cases: 99.36
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3980.0
Weighted Recall: 0.782
Weighted FalsePositiveRate: 0.10887444301738039
Kappa statistic: 0.6731346423781116
Training time: 202.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 68.65333333333466
Incorrectly Classified Instances: 21.44
Correctly Classified Instances: 78.56
Weighted Precision: 0.7924912293996427
Weighted AreaUnderROC: 0.9250955776754499
Root mean squared error: 0.3254289476982634
Relative absolute error: 49.14599999999911
Root relative squared error: 69.03390471355314
Weighted TruePositiveRate: 0.7856
Weighted MatthewsCorrelation: 0.6830980360346485
Weighted FMeasure: 0.7829364599839156
Iteration time: 207.0
Weighted AreaUnderPRC: 0.8312998848921059
Mean absolute error: 0.21842666666666374
Coverage of cases: 99.28
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4187.0
Weighted Recall: 0.7856
Weighted FalsePositiveRate: 0.10713608307777484
Kappa statistic: 0.6784913856085545
Training time: 200.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 68.28000000000141
Incorrectly Classified Instances: 21.88
Correctly Classified Instances: 78.12
Weighted Precision: 0.7873586221899135
Weighted AreaUnderROC: 0.9216915694243375
Root mean squared error: 0.3285889022268798
Relative absolute error: 49.88399999999914
Root relative squared error: 69.70423229618089
Weighted TruePositiveRate: 0.7812
Weighted MatthewsCorrelation: 0.6758264342133801
Weighted FMeasure: 0.7790041861973844
Iteration time: 223.0
Weighted AreaUnderPRC: 0.8250374454714267
Mean absolute error: 0.2217066666666639
Coverage of cases: 99.2
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4410.0
Weighted Recall: 0.7812
Weighted FalsePositiveRate: 0.10940722608380776
Kappa statistic: 0.6718648395077133
Training time: 216.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 69.3733333333348
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.7971353094841462
Weighted AreaUnderROC: 0.9280304515633819
Root mean squared error: 0.32301083573155753
Relative absolute error: 49.61999999999914
Root relative squared error: 68.52094570275533
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.6878328822281323
Weighted FMeasure: 0.786492722422782
Iteration time: 224.0
Weighted AreaUnderPRC: 0.8387141948454482
Mean absolute error: 0.22053333333333053
Coverage of cases: 99.52
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 4634.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.10586184330258439
Kappa statistic: 0.6826308736686095
Training time: 217.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 68.92000000000134
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.8012247225702778
Weighted AreaUnderROC: 0.9296263243447571
Root mean squared error: 0.32216765821540694
Relative absolute error: 49.21199999999906
Root relative squared error: 68.34208074093108
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.6960565274269674
Weighted FMeasure: 0.7933633014864632
Iteration time: 229.0
Weighted AreaUnderPRC: 0.8428121716267731
Mean absolute error: 0.21871999999999683
Coverage of cases: 99.4
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4863.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10265012117195288
Kappa statistic: 0.6922313431600124
Training time: 222.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 69.50666666666798
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.808019623868309
Weighted AreaUnderROC: 0.9332559784066013
Root mean squared error: 0.31905694371590276
Relative absolute error: 48.96599999999899
Root relative squared error: 67.68219854585068
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7005573685957343
Weighted FMeasure: 0.7938200612939276
Iteration time: 240.0
Weighted AreaUnderPRC: 0.8520788974711871
Mean absolute error: 0.2176266666666632
Coverage of cases: 99.6
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 5103.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10229038914519024
Kappa statistic: 0.6939237013002602
Training time: 229.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 68.96000000000134
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.800551461279471
Weighted AreaUnderROC: 0.9312776883875131
Root mean squared error: 0.3181194744117364
Relative absolute error: 48.29999999999898
Root relative squared error: 67.4833312752116
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.6933539200374088
Weighted FMeasure: 0.7909269012066364
Iteration time: 240.0
Weighted AreaUnderPRC: 0.8473782920843957
Mean absolute error: 0.21466666666666315
Coverage of cases: 99.32
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 5343.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10387769070036881
Kappa statistic: 0.6886217715657322
Training time: 230.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 69.38666666666803
Incorrectly Classified Instances: 23.32
Correctly Classified Instances: 76.68
Weighted Precision: 0.777050273438035
Weighted AreaUnderROC: 0.921606262201217
Root mean squared error: 0.3294358814701266
Relative absolute error: 50.71199999999915
Root relative squared error: 69.8839037261081
Weighted TruePositiveRate: 0.7668
Weighted MatthewsCorrelation: 0.6560592377053576
Weighted FMeasure: 0.7637128910709918
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8259676907868773
Mean absolute error: 0.22538666666666396
Coverage of cases: 99.12
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5590.0
Weighted Recall: 0.7668
Weighted FalsePositiveRate: 0.11691683534852927
Kappa statistic: 0.6501319296682364
Training time: 238.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 68.56000000000128
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7863557631033722
Weighted AreaUnderROC: 0.9252402737553764
Root mean squared error: 0.3255026881609421
Relative absolute error: 49.409999999999044
Root relative squared error: 69.04954742791553
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.665849099005168
Weighted FMeasure: 0.768678554758074
Iteration time: 251.0
Weighted AreaUnderPRC: 0.8333572289227231
Mean absolute error: 0.21959999999999677
Coverage of cases: 99.16
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 5841.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.11437382830371552
Kappa statistic: 0.6579053629396037
Training time: 246.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 68.74666666666808
Incorrectly Classified Instances: 22.48
Correctly Classified Instances: 77.52
Weighted Precision: 0.7899276365057127
Weighted AreaUnderROC: 0.9316237897440753
Root mean squared error: 0.3212766201681443
Relative absolute error: 48.743999999999
Root relative squared error: 68.1530630272767
Weighted TruePositiveRate: 0.7752
Weighted MatthewsCorrelation: 0.6704689957324459
Weighted FMeasure: 0.7729466318336935
Iteration time: 248.0
Weighted AreaUnderPRC: 0.8481409190802304
Mean absolute error: 0.21663999999999659
Coverage of cases: 99.52
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 6089.0
Weighted Recall: 0.7752
Weighted FalsePositiveRate: 0.11282633070511017
Kappa statistic: 0.6626728411542014
Training time: 239.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 69.28000000000148
Incorrectly Classified Instances: 22.48
Correctly Classified Instances: 77.52
Weighted Precision: 0.7855033231016427
Weighted AreaUnderROC: 0.9258019847915286
Root mean squared error: 0.3259775043363152
Relative absolute error: 50.189999999999046
Root relative squared error: 69.15027114914255
Weighted TruePositiveRate: 0.7752
Weighted MatthewsCorrelation: 0.668441063098609
Weighted FMeasure: 0.7724994042476208
Iteration time: 269.0
Weighted AreaUnderPRC: 0.8317450152597144
Mean absolute error: 0.22306666666666347
Coverage of cases: 99.48
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 6358.0
Weighted Recall: 0.7752
Weighted FalsePositiveRate: 0.11279886554430527
Kappa statistic: 0.6626836123536065
Training time: 264.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 69.40000000000148
Incorrectly Classified Instances: 22.84
Correctly Classified Instances: 77.16
Weighted Precision: 0.7825969714264503
Weighted AreaUnderROC: 0.9228924019800936
Root mean squared error: 0.32812599206199117
Relative absolute error: 50.507999999999164
Root relative squared error: 69.606034221179
Weighted TruePositiveRate: 0.7716
Weighted MatthewsCorrelation: 0.6631908414817967
Weighted FMeasure: 0.7690273637485858
Iteration time: 264.0
Weighted AreaUnderPRC: 0.826445075539438
Mean absolute error: 0.22447999999999735
Coverage of cases: 99.2
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 6622.0
Weighted Recall: 0.7716
Weighted FalsePositiveRate: 0.1147432141899476
Kappa statistic: 0.6572285868717228
Training time: 259.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 68.8400000000015
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.8000541137781695
Weighted AreaUnderROC: 0.9326580225373328
Root mean squared error: 0.3196039215445671
Relative absolute error: 49.21199999999895
Root relative squared error: 67.79823006539286
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.6882693569701787
Weighted FMeasure: 0.7859194302634204
Iteration time: 266.0
Weighted AreaUnderPRC: 0.8462265343924048
Mean absolute error: 0.2187199999999964
Coverage of cases: 99.52
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6888.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.1065058807968655
Kappa statistic: 0.6818655691148853
Training time: 260.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 69.0266666666681
Incorrectly Classified Instances: 22.12
Correctly Classified Instances: 77.88
Weighted Precision: 0.7955595947206102
Weighted AreaUnderROC: 0.9275619243970855
Root mean squared error: 0.3254289476982641
Relative absolute error: 50.02799999999911
Root relative squared error: 69.0339047135533
Weighted TruePositiveRate: 0.7788
Weighted MatthewsCorrelation: 0.6768829684644343
Weighted FMeasure: 0.7752840660965481
Iteration time: 271.0
Weighted AreaUnderPRC: 0.8355156110838782
Mean absolute error: 0.22234666666666375
Coverage of cases: 99.4
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 7159.0
Weighted Recall: 0.7788
Weighted FalsePositiveRate: 0.11121843027932792
Kappa statistic: 0.668003697226096
Training time: 266.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 68.09333333333481
Incorrectly Classified Instances: 22.4
Correctly Classified Instances: 77.6
Weighted Precision: 0.7876568576669539
Weighted AreaUnderROC: 0.9294764343138154
Root mean squared error: 0.32129737004836995
Relative absolute error: 48.70199999999899
Root relative squared error: 68.15746474158162
Weighted TruePositiveRate: 0.776
Weighted MatthewsCorrelation: 0.6701845820043804
Weighted FMeasure: 0.7733670708922274
Iteration time: 272.0
Weighted AreaUnderPRC: 0.840552631270098
Mean absolute error: 0.21645333333332983
Coverage of cases: 99.52
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 7431.0
Weighted Recall: 0.776
Weighted FalsePositiveRate: 0.11249267005451834
Kappa statistic: 0.6638594932681862
Training time: 267.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 68.1466666666681
Incorrectly Classified Instances: 21.76
Correctly Classified Instances: 78.24
Weighted Precision: 0.7993873977423278
Weighted AreaUnderROC: 0.9328959397096355
Root mean squared error: 0.3205952796491751
Relative absolute error: 48.44999999999901
Root relative squared error: 68.00852887689862
Weighted TruePositiveRate: 0.7824
Weighted MatthewsCorrelation: 0.6823256278177264
Weighted FMeasure: 0.7792962970719914
Iteration time: 285.0
Weighted AreaUnderPRC: 0.846960147568238
Mean absolute error: 0.21533333333332993
Coverage of cases: 99.44
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 7716.0
Weighted Recall: 0.7824
Weighted FalsePositiveRate: 0.10948693662992356
Kappa statistic: 0.6733636720840263
Training time: 279.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 68.82666666666815
Incorrectly Classified Instances: 23.4
Correctly Classified Instances: 76.6
Weighted Precision: 0.7881368897882122
Weighted AreaUnderROC: 0.9259655625912947
Root mean squared error: 0.3292982437446835
Relative absolute error: 49.90799999999906
Root relative squared error: 69.85470635540574
Weighted TruePositiveRate: 0.766
Weighted MatthewsCorrelation: 0.6602214507720331
Weighted FMeasure: 0.7612793310910225
Iteration time: 292.0
Weighted AreaUnderPRC: 0.8345070934553233
Mean absolute error: 0.22181333333333023
Coverage of cases: 99.4
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 8008.0
Weighted Recall: 0.766
Weighted FalsePositiveRate: 0.11779979921521917
Kappa statistic: 0.6487105937355431
Training time: 287.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 67.24000000000146
Incorrectly Classified Instances: 22.64
Correctly Classified Instances: 77.36
Weighted Precision: 0.7928877436988112
Weighted AreaUnderROC: 0.9311306498184769
Root mean squared error: 0.3223207512194433
Relative absolute error: 48.431999999999
Root relative squared error: 68.374556671323
Weighted TruePositiveRate: 0.7736
Weighted MatthewsCorrelation: 0.6702517109076808
Weighted FMeasure: 0.7697024877739665
Iteration time: 289.0
Weighted AreaUnderPRC: 0.8457701817460246
Mean absolute error: 0.2152533333333299
Coverage of cases: 99.64
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 8297.0
Weighted Recall: 0.7736
Weighted FalsePositiveRate: 0.11400175748118455
Kappa statistic: 0.6601184421174501
Training time: 284.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 70.10666666666827
Incorrectly Classified Instances: 21.96
Correctly Classified Instances: 78.04
Weighted Precision: 0.7997101565874478
Weighted AreaUnderROC: 0.930546195533153
Root mean squared error: 0.3232089107682513
Relative absolute error: 50.45999999999915
Root relative squared error: 68.56296376324431
Weighted TruePositiveRate: 0.7804
Weighted MatthewsCorrelation: 0.680480045512245
Weighted FMeasure: 0.7772900417561527
Iteration time: 299.0
Weighted AreaUnderPRC: 0.8406722438119046
Mean absolute error: 0.22426666666666392
Coverage of cases: 99.52
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 8596.0
Weighted Recall: 0.7804
Weighted FalsePositiveRate: 0.1106541033311626
Kappa statistic: 0.6702953001045446
Training time: 294.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 67.74666666666808
Incorrectly Classified Instances: 23.24
Correctly Classified Instances: 76.76
Weighted Precision: 0.7847451586562091
Weighted AreaUnderROC: 0.9284633623428071
Root mean squared error: 0.324624911757143
Relative absolute error: 48.87599999999901
Root relative squared error: 68.86334293366797
Weighted TruePositiveRate: 0.7676
Weighted MatthewsCorrelation: 0.6603458949413861
Weighted FMeasure: 0.7632596336135646
Iteration time: 310.0
Weighted AreaUnderPRC: 0.83935546567223
Mean absolute error: 0.21722666666666327
Coverage of cases: 99.36
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 8906.0
Weighted Recall: 0.7676
Weighted FalsePositiveRate: 0.11696768694920606
Kappa statistic: 0.6511141648129479
Training time: 305.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 69.72000000000143
Incorrectly Classified Instances: 21.6
Correctly Classified Instances: 78.4
Weighted Precision: 0.8031717248315886
Weighted AreaUnderROC: 0.9304108434623161
Root mean squared error: 0.32359645651129426
Relative absolute error: 49.66199999999904
Root relative squared error: 68.64517463012201
Weighted TruePositiveRate: 0.784
Weighted MatthewsCorrelation: 0.6858680272500361
Weighted FMeasure: 0.7809736596043304
Iteration time: 316.0
Weighted AreaUnderPRC: 0.8393744878599245
Mean absolute error: 0.22071999999999675
Coverage of cases: 99.36
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 9222.0
Weighted Recall: 0.784
Weighted FalsePositiveRate: 0.10895007484256318
Kappa statistic: 0.6756751302644833
Training time: 310.0
		
Time end:Sun Oct 08 11.21.53 EEST 2017