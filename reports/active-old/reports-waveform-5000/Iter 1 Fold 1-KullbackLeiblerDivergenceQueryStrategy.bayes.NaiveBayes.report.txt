Sun Oct 08 10.43.35 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.43.35 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.89333333333457
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.8295082477480763
Weighted AreaUnderROC: 0.953380326964151
Root mean squared error: 0.34277346046380947
Relative absolute error: 31.596317034601974
Root relative squared error: 72.71323149142142
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.7056916177764803
Weighted FMeasure: 0.7768862607335308
Iteration time: 596.0
Weighted AreaUnderPRC: 0.9159133360616557
Mean absolute error: 0.14042807570934276
Coverage of cases: 88.48
Instances selection time: 512.0
Test time: 168.0
Accumulative iteration time: 596.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.10384197388102107
Kappa statistic: 0.6855713545801654
Training time: 84.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.973333333334466
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8415962414638105
Weighted AreaUnderROC: 0.9563910891505182
Root mean squared error: 0.3197939785418585
Relative absolute error: 28.80513480182721
Root relative squared error: 67.83854724287185
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7367672372246823
Weighted FMeasure: 0.8075811032974849
Iteration time: 229.0
Weighted AreaUnderPRC: 0.9211194415179201
Mean absolute error: 0.12802282134145487
Coverage of cases: 91.0
Instances selection time: 207.0
Test time: 174.0
Accumulative iteration time: 825.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09164271603422174
Kappa statistic: 0.7226048637492607
Training time: 22.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.37333333333426
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.845420817132174
Weighted AreaUnderROC: 0.9558871421558799
Root mean squared error: 0.30960499027759797
Relative absolute error: 27.767162735230407
Root relative squared error: 65.67713643434524
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7474344773357234
Weighted FMeasure: 0.81821161612723
Iteration time: 286.0
Weighted AreaUnderPRC: 0.920721430792733
Mean absolute error: 0.12340961215658015
Coverage of cases: 91.92
Instances selection time: 260.0
Test time: 259.0
Accumulative iteration time: 1111.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08734030894301761
Kappa statistic: 0.7357429422392795
Training time: 26.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.106666666667685
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.846897528957529
Weighted AreaUnderROC: 0.9554900502103445
Root mean squared error: 0.3141863123697388
Relative absolute error: 28.106644765961175
Root relative squared error: 66.648981609791
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7455287828433129
Weighted FMeasure: 0.8138935354208167
Iteration time: 234.0
Weighted AreaUnderPRC: 0.9205592567708994
Mean absolute error: 0.12491842118205025
Coverage of cases: 91.64
Instances selection time: 201.0
Test time: 220.0
Accumulative iteration time: 1345.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08869571918399907
Kappa statistic: 0.7315775951653289
Training time: 33.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 41.066666666667665
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.8467247622896257
Weighted AreaUnderROC: 0.9558354619551664
Root mean squared error: 0.3176407071631174
Relative absolute error: 28.487676752843115
Root relative squared error: 67.38176940477904
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7440381609512294
Weighted FMeasure: 0.8119644923627377
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9211893514131206
Mean absolute error: 0.1266118966793033
Coverage of cases: 91.12
Instances selection time: 103.0
Test time: 131.0
Accumulative iteration time: 1484.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.0894973529523561
Kappa statistic: 0.7291834329283229
Training time: 36.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 40.80000000000109
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.8452296460637666
Weighted AreaUnderROC: 0.9567999875865411
Root mean squared error: 0.3239369322061714
Relative absolute error: 29.230030558064378
Root relative squared error: 68.71740043192507
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7379033211058801
Weighted FMeasure: 0.8051793116439305
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9224112771276941
Mean absolute error: 0.1299112469247312
Coverage of cases: 90.68
Instances selection time: 61.0
Test time: 133.0
Accumulative iteration time: 1564.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.092016984923943
Kappa statistic: 0.7214307794426698
Training time: 19.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 40.493333333334526
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8407838834951455
Weighted AreaUnderROC: 0.9583412301573453
Root mean squared error: 0.3288095116739714
Relative absolute error: 29.772206507709356
Root relative squared error: 69.75103062699056
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7278697148800513
Weighted FMeasure: 0.796414258412033
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9248586563840571
Mean absolute error: 0.1323209178120422
Coverage of cases: 90.24
Instances selection time: 18.0
Test time: 134.0
Accumulative iteration time: 1605.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09573894355863631
Kappa statistic: 0.7100782549106993
Training time: 23.0
		
Time end:Sun Oct 08 10.43.38 EEST 2017