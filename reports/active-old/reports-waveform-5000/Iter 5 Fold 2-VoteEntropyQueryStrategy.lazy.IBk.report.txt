Sun Oct 08 12.22.37 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.22.37 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.32
Correctly Classified Instances: 69.68
Weighted Precision: 0.697031147139408
Weighted AreaUnderROC: 0.7727027189321356
Root mean squared error: 0.4482584874713087
Relative absolute error: 45.805168986083295
Root relative squared error: 95.08998486461601
Weighted TruePositiveRate: 0.6968
Weighted MatthewsCorrelation: 0.5459141772676834
Weighted FMeasure: 0.6942765756143302
Iteration time: 868.0
Weighted AreaUnderPRC: 0.587058746947305
Mean absolute error: 0.2035785288270378
Coverage of cases: 69.68
Instances selection time: 867.0
Test time: 1085.0
Accumulative iteration time: 868.0
Weighted Recall: 0.6968
Weighted FalsePositiveRate: 0.151394562135729
Kappa statistic: 0.5454436422077765
Training time: 1.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.72
Correctly Classified Instances: 68.28
Weighted Precision: 0.7002428897403906
Weighted AreaUnderROC: 0.761445067177248
Root mean squared error: 0.45899861764545696
Relative absolute error: 47.77584059775959
Root relative squared error: 97.36831052770596
Weighted TruePositiveRate: 0.6828
Weighted MatthewsCorrelation: 0.5334499575096676
Weighted FMeasure: 0.6812429420563899
Iteration time: 1131.0
Weighted AreaUnderPRC: 0.5768152349542607
Mean absolute error: 0.21233706932337695
Coverage of cases: 68.28
Instances selection time: 1130.0
Test time: 1668.0
Accumulative iteration time: 1999.0
Weighted Recall: 0.6828
Weighted FalsePositiveRate: 0.15990986564550388
Kappa statistic: 0.5235213419177202
Training time: 1.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.7078509915353419
Weighted AreaUnderROC: 0.7515113131998833
Root mean squared error: 0.4684050339465831
Relative absolute error: 49.637352674523065
Root relative squared error: 99.36371275366294
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.52722323241247
Weighted FMeasure: 0.6639429209448839
Iteration time: 1217.0
Weighted AreaUnderPRC: 0.5680365613201694
Mean absolute error: 0.22061045633121468
Coverage of cases: 67.0
Instances selection time: 1216.0
Test time: 2205.0
Accumulative iteration time: 3216.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.16697737360023363
Kappa statistic: 0.5038821074751316
Training time: 1.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.68
Correctly Classified Instances: 67.32
Weighted Precision: 0.7012218450972296
Weighted AreaUnderROC: 0.7541978733803153
Root mean squared error: 0.46626368488423314
Relative absolute error: 49.129009265858585
Root relative squared error: 98.90946402080041
Weighted TruePositiveRate: 0.6732
Weighted MatthewsCorrelation: 0.5250175291879932
Weighted FMeasure: 0.6664826557707745
Iteration time: 1190.0
Weighted AreaUnderPRC: 0.5679491853350923
Mean absolute error: 0.21835115229270585
Coverage of cases: 67.32
Instances selection time: 1190.0
Test time: 2766.0
Accumulative iteration time: 4406.0
Weighted Recall: 0.6732
Weighted FalsePositiveRate: 0.1648042532393694
Kappa statistic: 0.5090028563172883
Training time: 0.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.70421969419247
Weighted AreaUnderROC: 0.7565193862164163
Root mean squared error: 0.4643492054631768
Relative absolute error: 48.690546095126486
Root relative squared error: 98.50334160647911
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5284056257951547
Weighted FMeasure: 0.6643135304216942
Iteration time: 1020.0
Weighted AreaUnderPRC: 0.5694128418103087
Mean absolute error: 0.21640242708945207
Coverage of cases: 67.6
Instances selection time: 1019.0
Test time: 3329.0
Accumulative iteration time: 5426.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.16296122756716722
Kappa statistic: 0.5134328573385138
Training time: 1.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.726425404694314
Weighted AreaUnderROC: 0.7806663906134911
Root mean squared error: 0.44088048552188935
Relative absolute error: 43.88417373939145
Root relative squared error: 93.52487430160342
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5712927018118875
Weighted FMeasure: 0.7039247304385386
Iteration time: 750.0
Weighted AreaUnderPRC: 0.6029066494954922
Mean absolute error: 0.195040772175074
Coverage of cases: 70.8
Instances selection time: 749.0
Test time: 3907.0
Accumulative iteration time: 6176.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14666721877301778
Kappa statistic: 0.5616839215094789
Training time: 1.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.76
Correctly Classified Instances: 72.24
Weighted Precision: 0.7256830765506873
Weighted AreaUnderROC: 0.7916060863820372
Root mean squared error: 0.4299139063787934
Relative absolute error: 41.716022579243116
Root relative squared error: 91.19851155805279
Weighted TruePositiveRate: 0.7224
Weighted MatthewsCorrelation: 0.5850638899246341
Weighted FMeasure: 0.7223817204686017
Iteration time: 371.0
Weighted AreaUnderPRC: 0.6156055612247138
Mean absolute error: 0.18540454479663693
Coverage of cases: 72.24
Instances selection time: 370.0
Test time: 4490.0
Accumulative iteration time: 6547.0
Weighted Recall: 0.7224
Weighted FalsePositiveRate: 0.13918782723592546
Kappa statistic: 0.5834491752773845
Training time: 1.0
		
Time end:Sun Oct 08 12.23.04 EEST 2017