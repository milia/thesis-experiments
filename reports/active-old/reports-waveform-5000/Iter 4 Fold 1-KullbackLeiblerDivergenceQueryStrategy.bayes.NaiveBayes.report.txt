Sun Oct 08 10.43.49 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.43.49 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.720000000001114
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.8318848178174714
Weighted AreaUnderROC: 0.9512885163921561
Root mean squared error: 0.3430606344481288
Relative absolute error: 31.546803794710172
Root relative squared error: 72.77415029292918
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.7137029300028715
Weighted FMeasure: 0.7833958030012987
Iteration time: 205.0
Weighted AreaUnderPRC: 0.9101891570885844
Mean absolute error: 0.1402080168653792
Coverage of cases: 87.68
Instances selection time: 200.0
Test time: 136.0
Accumulative iteration time: 205.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.100727291213535
Kappa statistic: 0.6951311546970407
Training time: 5.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.76000000000112
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8391311764746648
Weighted AreaUnderROC: 0.955383997837182
Root mean squared error: 0.3240556130329005
Relative absolute error: 29.333736190139412
Root relative squared error: 68.74257643713814
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7329901978089386
Weighted FMeasure: 0.8029258098953082
Iteration time: 168.0
Weighted AreaUnderPRC: 0.9178427410612323
Mean absolute error: 0.13037216084506467
Coverage of cases: 90.36
Instances selection time: 161.0
Test time: 129.0
Accumulative iteration time: 373.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09304813073674896
Kappa statistic: 0.7184271797570472
Training time: 7.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.440000000000985
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8419393381208123
Weighted AreaUnderROC: 0.9554686220665404
Root mean squared error: 0.3154696661197833
Relative absolute error: 28.454248795401693
Root relative squared error: 66.92122205158628
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7419759405108115
Weighted FMeasure: 0.8124416189094269
Iteration time: 143.0
Weighted AreaUnderPRC: 0.9181229884069011
Mean absolute error: 0.12646332797956367
Coverage of cases: 91.76
Instances selection time: 132.0
Test time: 132.0
Accumulative iteration time: 516.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.08933355098515486
Kappa statistic: 0.7297700205063211
Training time: 11.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 40.80000000000109
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8363571058870882
Weighted AreaUnderROC: 0.9551637401376907
Root mean squared error: 0.3216102277522577
Relative absolute error: 28.995386326984352
Root relative squared error: 68.22383188277126
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.731415725069452
Weighted FMeasure: 0.8030218675458987
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9178916756624238
Mean absolute error: 0.12886838367548661
Coverage of cases: 90.88
Instances selection time: 104.0
Test time: 131.0
Accumulative iteration time: 634.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09330892786307801
Kappa statistic: 0.717806990046675
Training time: 14.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 40.61333333333442
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8334977317911846
Weighted AreaUnderROC: 0.9540202179354333
Root mean squared error: 0.3300647457736907
Relative absolute error: 29.988578492306306
Root relative squared error: 70.017305990157
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7241577674116153
Weighted FMeasure: 0.7959313686645307
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9161289644823472
Mean absolute error: 0.13328257107691754
Coverage of cases: 89.84
Instances selection time: 76.0
Test time: 130.0
Accumulative iteration time: 726.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09627077493154729
Kappa statistic: 0.708839489321239
Training time: 16.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 40.41333333333448
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8374634467095561
Weighted AreaUnderROC: 0.9561284668866591
Root mean squared error: 0.333336742197586
Relative absolute error: 30.38745059753131
Root relative squared error: 70.71140124796335
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7232517382772543
Weighted FMeasure: 0.7914666796935972
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9197317104752265
Mean absolute error: 0.13505533598902866
Coverage of cases: 89.48
Instances selection time: 48.0
Test time: 129.0
Accumulative iteration time: 793.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09735181141971985
Kappa statistic: 0.7052980227653685
Training time: 19.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 40.04000000000119
Incorrectly Classified Instances: 20.64
Correctly Classified Instances: 79.36
Weighted Precision: 0.8319990886621433
Weighted AreaUnderROC: 0.9564300421330423
Root mean squared error: 0.3407192584618092
Relative absolute error: 31.269262230931847
Root relative squared error: 72.27746944175901
Weighted TruePositiveRate: 0.7936
Weighted MatthewsCorrelation: 0.7106957919391819
Weighted FMeasure: 0.7797641725612723
Iteration time: 42.0
Weighted AreaUnderPRC: 0.9200409860019432
Mean absolute error: 0.1389744988041422
Coverage of cases: 88.4
Instances selection time: 18.0
Test time: 136.0
Accumulative iteration time: 835.0
Weighted Recall: 0.7936
Weighted FalsePositiveRate: 0.10206999491413507
Kappa statistic: 0.6909615599302196
Training time: 24.0
		
Time end:Sun Oct 08 10.43.51 EEST 2017