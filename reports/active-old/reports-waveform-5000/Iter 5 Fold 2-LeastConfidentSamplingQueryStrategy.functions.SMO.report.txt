Sun Oct 08 10.54.05 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.54.05 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 14.72
Correctly Classified Instances: 85.28
Weighted Precision: 0.8540934381943507
Weighted AreaUnderROC: 0.9252437082957884
Root mean squared error: 0.32678002568270154
Relative absolute error: 57.37999999999874
Root relative squared error: 69.32051163496556
Weighted TruePositiveRate: 0.8528
Weighted MatthewsCorrelation: 0.7799897538371806
Weighted FMeasure: 0.8522858932944891
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8008365100212188
Mean absolute error: 0.2550222222222178
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8528
Weighted FalsePositiveRate: 0.07346797195825347
Kappa statistic: 0.779263324937342
Training time: 47.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 66.73333333333531
Incorrectly Classified Instances: 14.84
Correctly Classified Instances: 85.16
Weighted Precision: 0.8526199763229609
Weighted AreaUnderROC: 0.9252438204224963
Root mean squared error: 0.3270972219273568
Relative absolute error: 57.43999999999872
Root relative squared error: 69.38779911963437
Weighted TruePositiveRate: 0.8516
Weighted MatthewsCorrelation: 0.7778972741889583
Weighted FMeasure: 0.85145930672999
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8003849613703817
Mean absolute error: 0.2552888888888844
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8516
Weighted FalsePositiveRate: 0.07424503567995848
Kappa statistic: 0.7774131059163235
Training time: 50.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 66.73333333333531
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8437643194323755
Weighted AreaUnderROC: 0.9201177733370381
Root mean squared error: 0.33074551031358623
Relative absolute error: 57.97999999999869
Root relative squared error: 70.16171795692244
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7625659517064538
Weighted FMeasure: 0.8408393761927891
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7885546818953464
Mean absolute error: 0.25768888888888425
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 168.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.07993023383746209
Kappa statistic: 0.7611309381768457
Training time: 51.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8445504502919577
Weighted AreaUnderROC: 0.9200113847188868
Root mean squared error: 0.33096939547447995
Relative absolute error: 57.99999999999871
Root relative squared error: 70.20921117156493
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7629851634237093
Weighted FMeasure: 0.84078119108933
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7885690199820621
Mean absolute error: 0.2577777777777732
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 223.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.08000397428233554
Kappa statistic: 0.7611057036433421
Training time: 49.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 66.72000000000199
Incorrectly Classified Instances: 17.12
Correctly Classified Instances: 82.88
Weighted Precision: 0.8357119918610473
Weighted AreaUnderROC: 0.9142259834402586
Root mean squared error: 0.3350621832834447
Relative absolute error: 58.61999999999879
Root relative squared error: 71.07742257566792
Weighted TruePositiveRate: 0.8288
Weighted MatthewsCorrelation: 0.7464815568625648
Weighted FMeasure: 0.8293097211430555
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7762427392912459
Mean absolute error: 0.2605333333333292
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 281.0
Weighted Recall: 0.8288
Weighted FalsePositiveRate: 0.08622421484415477
Kappa statistic: 0.74303690038141
Training time: 52.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 17.4
Correctly Classified Instances: 82.6
Weighted Precision: 0.8362091567687825
Weighted AreaUnderROC: 0.9131771240477897
Root mean squared error: 0.3358571124749364
Relative absolute error: 58.719999999998784
Root relative squared error: 71.246052522228
Weighted TruePositiveRate: 0.826
Weighted MatthewsCorrelation: 0.7441411038674444
Weighted FMeasure: 0.8262799646698866
Iteration time: 62.0
Weighted AreaUnderPRC: 0.7745041749320886
Mean absolute error: 0.2609777777777736
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 343.0
Weighted Recall: 0.826
Weighted FalsePositiveRate: 0.08777131619871657
Kappa statistic: 0.7387822907123712
Training time: 56.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 17.6
Correctly Classified Instances: 82.4
Weighted Precision: 0.8358545826676025
Weighted AreaUnderROC: 0.9120913732803738
Root mean squared error: 0.33651811904330875
Relative absolute error: 58.81999999999876
Root relative squared error: 71.38627319029948
Weighted TruePositiveRate: 0.824
Weighted MatthewsCorrelation: 0.7420797389940782
Weighted FMeasure: 0.824014759650814
Iteration time: 62.0
Weighted AreaUnderPRC: 0.7726018206931429
Mean absolute error: 0.26142222222221795
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 405.0
Weighted Recall: 0.824
Weighted FalsePositiveRate: 0.08882897846859884
Kappa statistic: 0.7357593507559204
Training time: 56.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8338145772208012
Weighted AreaUnderROC: 0.9098962189142776
Root mean squared error: 0.3379677828169784
Relative absolute error: 59.03999999999884
Root relative squared error: 71.69379331574017
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7367451824625533
Weighted FMeasure: 0.8199048000499257
Iteration time: 61.0
Weighted AreaUnderPRC: 0.7686528723347464
Mean absolute error: 0.2623999999999961
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 466.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.0911330220986109
Kappa statistic: 0.7291227517188393
Training time: 55.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 17.8
Correctly Classified Instances: 82.2
Weighted Precision: 0.835394963742432
Weighted AreaUnderROC: 0.910964576424357
Root mean squared error: 0.33717782977071753
Relative absolute error: 58.919999999998815
Root relative squared error: 71.52621896899115
Weighted TruePositiveRate: 0.822
Weighted MatthewsCorrelation: 0.7399416361618202
Weighted FMeasure: 0.8221395712142007
Iteration time: 64.0
Weighted AreaUnderPRC: 0.7708271714600833
Mean absolute error: 0.26186666666666264
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 530.0
Weighted Recall: 0.822
Weighted FalsePositiveRate: 0.08989822921390578
Kappa statistic: 0.7327345305545091
Training time: 58.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.8349387296110595
Weighted AreaUnderROC: 0.910425681738984
Root mean squared error: 0.3375730373645941
Relative absolute error: 58.97999999999884
Root relative squared error: 71.61005515987311
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.738549048735817
Weighted FMeasure: 0.8209935637226439
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7698675529776144
Mean absolute error: 0.2621333333333294
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 593.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.09053154036911598
Kappa statistic: 0.7309233030509934
Training time: 57.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.48
Correctly Classified Instances: 81.52
Weighted Precision: 0.8319698839875437
Weighted AreaUnderROC: 0.9075993299384872
Root mean squared error: 0.3393676035958242
Relative absolute error: 59.25999999999882
Root relative squared error: 71.99074014529047
Weighted TruePositiveRate: 0.8152
Weighted MatthewsCorrelation: 0.731610694861919
Weighted FMeasure: 0.8153191320802543
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7645968348455994
Mean absolute error: 0.26337777777777377
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 656.0
Weighted Recall: 0.8152
Weighted FalsePositiveRate: 0.09342912781304537
Kappa statistic: 0.7224841203251189
Training time: 57.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 17.96
Correctly Classified Instances: 82.04
Weighted Precision: 0.8352117204412112
Weighted AreaUnderROC: 0.909969696808813
Root mean squared error: 0.3377046704510414
Relative absolute error: 58.99999999999879
Root relative squared error: 71.63797875428973
Weighted TruePositiveRate: 0.8204
Weighted MatthewsCorrelation: 0.7384201774921093
Weighted FMeasure: 0.8202683221314839
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7692388159611752
Mean absolute error: 0.2622222222222181
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 725.0
Weighted Recall: 0.8204
Weighted FalsePositiveRate: 0.09075494712223249
Kappa statistic: 0.7303110228672227
Training time: 64.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.8319637481167305
Weighted AreaUnderROC: 0.9070720677643997
Root mean squared error: 0.3398038649966606
Relative absolute error: 59.31999999999883
Root relative squared error: 72.08328516376088
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7305857737294719
Weighted FMeasure: 0.8140823272434043
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7636567039992058
Mean absolute error: 0.26364444444444046
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 793.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.0940798216813926
Kappa statistic: 0.7206630078526735
Training time: 62.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8303815403503509
Weighted AreaUnderROC: 0.9047729968829455
Root mean squared error: 0.341326388818248
Relative absolute error: 59.559999999998865
Root relative squared error: 72.40626123938964
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7251861301472264
Weighted FMeasure: 0.8092657631589926
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7596968364797502
Mean absolute error: 0.2647111111111073
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 862.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09658266599509498
Kappa statistic: 0.7134216707528611
Training time: 64.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8288818509540575
Weighted AreaUnderROC: 0.903071685768262
Root mean squared error: 0.3422799374131614
Relative absolute error: 59.69999999999887
Root relative squared error: 72.60853944268588
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7216831140957712
Weighted FMeasure: 0.8065736294593416
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7566915534436763
Mean absolute error: 0.2653333333333296
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 925.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09802276836049117
Kappa statistic: 0.7092045181446768
Training time: 57.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8330551388175486
Weighted AreaUnderROC: 0.9057815109740305
Root mean squared error: 0.3405877273185311
Relative absolute error: 59.439999999998875
Root relative squared error: 72.24956747275426
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7289775098067361
Weighted FMeasure: 0.8116506260966913
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7621724082681435
Mean absolute error: 0.264177777777774
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 990.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09538327533254726
Kappa statistic: 0.7170239814665728
Training time: 59.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8294964288350165
Weighted AreaUnderROC: 0.9025411163777327
Root mean squared error: 0.3423232174205941
Relative absolute error: 59.69999999999887
Root relative squared error: 72.6177205187095
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7220766816190384
Weighted FMeasure: 0.8063983935419015
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7562031709724942
Mean absolute error: 0.2653333333333296
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 1061.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09803650051378109
Kappa statistic: 0.7091973896019561
Training time: 65.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8333761766734544
Weighted AreaUnderROC: 0.9044587735488585
Root mean squared error: 0.34110930147909724
Relative absolute error: 59.51999999999889
Root relative squared error: 72.36021006050267
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7277406475387765
Weighted FMeasure: 0.8098407225382238
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7605290649280271
Mean absolute error: 0.2645333333333296
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 1134.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09624070780016239
Kappa statistic: 0.7145998004241468
Training time: 67.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8312496951387526
Weighted AreaUnderROC: 0.9024513331277753
Root mean squared error: 0.34245302462392735
Relative absolute error: 59.71999999999889
Root relative squared error: 72.64525678482666
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7227619105364416
Weighted FMeasure: 0.8057669712404814
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7566416264604979
Mean absolute error: 0.2654222222222185
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1208.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09829459492976654
Kappa statistic: 0.7085748456708524
Training time: 69.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8332804297611656
Weighted AreaUnderROC: 0.9018782965027183
Root mean squared error: 0.34284215134773444
Relative absolute error: 59.77999999999898
Root relative squared error: 72.72780302837013
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7229909536275207
Weighted FMeasure: 0.804602589433907
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7565567086533334
Mean absolute error: 0.2656888888888856
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1283.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09898577069869507
Kappa statistic: 0.7067386647282572
Training time: 70.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8345351358733304
Weighted AreaUnderROC: 0.9026351775524477
Root mean squared error: 0.342323217420594
Relative absolute error: 59.699999999999
Root relative squared error: 72.6177205187095
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7252988125753554
Weighted FMeasure: 0.8061001149710403
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7580594970646235
Mean absolute error: 0.26533333333333015
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1362.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09817318525079675
Kappa statistic: 0.7091430054902262
Training time: 74.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8364464676242495
Weighted AreaUnderROC: 0.9030095138794579
Root mean squared error: 0.34206345523515
Relative absolute error: 59.65999999999901
Root relative squared error: 72.56261663786252
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7271708529939748
Weighted FMeasure: 0.8068562520094889
Iteration time: 86.0
Weighted AreaUnderPRC: 0.7593599463529536
Mean absolute error: 0.26515555555555237
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1448.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.097797968511572
Kappa statistic: 0.7103357028914506
Training time: 81.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8352712953824437
Weighted AreaUnderROC: 0.9017521450943689
Root mean squared error: 0.34284215134773444
Relative absolute error: 59.779999999999
Root relative squared error: 72.72780302837013
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7242740506261162
Weighted FMeasure: 0.8043961950754195
Iteration time: 84.0
Weighted AreaUnderPRC: 0.757074972291189
Mean absolute error: 0.2656888888888857
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 1532.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.0990320468369887
Kappa statistic: 0.7067190661935068
Training time: 78.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8326919650920362
Weighted AreaUnderROC: 0.900615034282671
Root mean squared error: 0.3437484006696832
Relative absolute error: 59.91999999999898
Root relative squared error: 72.92004754066883
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7201386568737105
Weighted FMeasure: 0.8016369831105122
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7543283895450833
Mean absolute error: 0.26631111111110783
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1613.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.10044397033651668
Kappa statistic: 0.7025105942287776
Training time: 76.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8334496976300466
Weighted AreaUnderROC: 0.9010273318265964
Root mean squared error: 0.34348971627282715
Relative absolute error: 59.879999999998965
Root relative squared error: 72.86517229330761
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7213373586615206
Weighted FMeasure: 0.802426172197005
Iteration time: 83.0
Weighted AreaUnderPRC: 0.7552101635575934
Mean absolute error: 0.26613333333333
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1696.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.10004128929071213
Kappa statistic: 0.7037127140780813
Training time: 78.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.830437262967819
Weighted AreaUnderROC: 0.89817568001471
Root mean squared error: 0.3452535300326445
Relative absolute error: 60.159999999999016
Root relative squared error: 73.2393336944027
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.714388308101035
Weighted FMeasure: 0.7967894944202025
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7500049324097857
Mean absolute error: 0.26737777777777466
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1780.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10291284154663852
Kappa statistic: 0.6952766071003557
Training time: 79.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.8288232205362316
Weighted AreaUnderROC: 0.896745444596923
Root mean squared error: 0.34602504726296257
Relative absolute error: 60.279999999999035
Root relative squared error: 73.40299721401077
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.711231310467327
Weighted FMeasure: 0.7943725826236047
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7473764023376258
Mean absolute error: 0.2679111111111081
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1870.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10413461683734213
Kappa statistic: 0.6916634450605564
Training time: 85.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.6
Correctly Classified Instances: 79.4
Weighted Precision: 0.8282593279673319
Weighted AreaUnderROC: 0.8965794465318908
Root mean squared error: 0.3462818376858845
Relative absolute error: 60.31999999999907
Root relative squared error: 73.45747068882832
Weighted TruePositiveRate: 0.794
Weighted MatthewsCorrelation: 0.7101732395337235
Weighted FMeasure: 0.7936395245749437
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7468495153017213
Mean absolute error: 0.268088888888886
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1960.0
Weighted Recall: 0.794
Weighted FalsePositiveRate: 0.10454309212085905
Kappa statistic: 0.6904592694886844
Training time: 85.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.88
Correctly Classified Instances: 79.12
Weighted Precision: 0.8268883690374611
Weighted AreaUnderROC: 0.895314605617085
Root mean squared error: 0.3471364338465582
Relative absolute error: 60.45999999999907
Root relative squared error: 73.63875791094482
Weighted TruePositiveRate: 0.7912
Weighted MatthewsCorrelation: 0.706794367930338
Weighted FMeasure: 0.7908401173789343
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7445306679351026
Mean absolute error: 0.26871111111110824
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 2050.0
Weighted Recall: 0.7912
Weighted FalsePositiveRate: 0.1059817653676784
Kappa statistic: 0.6862395485311683
Training time: 85.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.96
Correctly Classified Instances: 79.04
Weighted Precision: 0.8264282428950149
Weighted AreaUnderROC: 0.8951012113544304
Root mean squared error: 0.34739240274001915
Relative absolute error: 60.49999999999907
Root relative squared error: 73.69305711304654
Weighted TruePositiveRate: 0.7904
Weighted MatthewsCorrelation: 0.7057831201423145
Weighted FMeasure: 0.7900186162797598
Iteration time: 94.0
Weighted AreaUnderPRC: 0.7439653414240938
Mean absolute error: 0.268888888888886
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2144.0
Weighted Recall: 0.7904
Weighted FalsePositiveRate: 0.10639095521048374
Kappa statistic: 0.6850343721268402
Training time: 89.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 66.72000000000197
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.825311934381408
Weighted AreaUnderROC: 0.8942863781245501
Root mean squared error: 0.34794635601866686
Relative absolute error: 60.599999999999106
Root relative squared error: 73.81056834898423
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.7029254207028168
Weighted FMeasure: 0.7876195859131865
Iteration time: 95.0
Weighted AreaUnderPRC: 0.742269087206626
Mean absolute error: 0.26933333333333065
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2239.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.10762503353590043
Kappa statistic: 0.681416865430484
Training time: 90.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.8258462075952905
Weighted AreaUnderROC: 0.894470423186238
Root mean squared error: 0.3478611898551053
Relative absolute error: 60.579999999999124
Root relative squared error: 73.79250187744962
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.7036253885442831
Weighted FMeasure: 0.7880061422266142
Iteration time: 98.0
Weighted AreaUnderPRC: 0.742658110055482
Mean absolute error: 0.2692444444444418
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2337.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.10742694741149852
Kappa statistic: 0.6820167423895607
Training time: 94.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.4
Correctly Classified Instances: 78.6
Weighted Precision: 0.8245022235097472
Weighted AreaUnderROC: 0.8927888492134812
Root mean squared error: 0.348796873924153
Relative absolute error: 60.71999999999912
Root relative squared error: 73.99099044253119
Weighted TruePositiveRate: 0.786
Weighted MatthewsCorrelation: 0.7006083484843675
Weighted FMeasure: 0.7855978308475925
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7399693986663124
Mean absolute error: 0.26986666666666403
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2427.0
Weighted Recall: 0.786
Weighted FalsePositiveRate: 0.1086552314992029
Kappa statistic: 0.6784013118821999
Training time: 85.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.8256908679869197
Weighted AreaUnderROC: 0.8928475794799199
Root mean squared error: 0.34871191548325703
Relative absolute error: 60.699999999999115
Root relative squared error: 73.97296803562821
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.7017223706895318
Weighted FMeasure: 0.7858968249957723
Iteration time: 100.0
Weighted AreaUnderPRC: 0.7404297107867658
Mean absolute error: 0.2697777777777751
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2527.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10847016296880249
Kappa statistic: 0.6789951755499642
Training time: 95.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.8262602419934588
Weighted AreaUnderROC: 0.8930324942459745
Root mean squared error: 0.34866942849979343
Relative absolute error: 60.69999999999914
Root relative squared error: 73.96395518539244
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.7019993344249283
Weighted FMeasure: 0.7860417546579644
Iteration time: 92.0
Weighted AreaUnderPRC: 0.7410148439026129
Mean absolute error: 0.26977777777777523
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2619.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10848389512209237
Kappa statistic: 0.6789928599355677
Training time: 88.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 21.4
Correctly Classified Instances: 78.6
Weighted Precision: 0.8258051419735816
Weighted AreaUnderROC: 0.8927028533169767
Root mean squared error: 0.3489667287547338
Relative absolute error: 60.73999999999911
Root relative squared error: 74.02702209328747
Weighted TruePositiveRate: 0.786
Weighted MatthewsCorrelation: 0.7013812516217988
Weighted FMeasure: 0.7855106089455381
Iteration time: 96.0
Weighted AreaUnderPRC: 0.7403774111625163
Mean absolute error: 0.26995555555555284
Coverage of cases: 99.92
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 2715.0
Weighted Recall: 0.786
Weighted FalsePositiveRate: 0.1086819812464943
Kappa statistic: 0.6783906403140159
Training time: 91.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.8265565114431193
Weighted AreaUnderROC: 0.8949895738479692
Root mean squared error: 0.3476907949441444
Relative absolute error: 60.53999999999918
Root relative squared error: 73.7563556583436
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.7056086303782849
Weighted FMeasure: 0.7896223867179096
Iteration time: 245.0
Weighted AreaUnderPRC: 0.7438355676782769
Mean absolute error: 0.2690666666666643
Coverage of cases: 99.92
Instances selection time: 10.0
Test time: 34.0
Accumulative iteration time: 2960.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.10660285125158057
Kappa statistic: 0.6844287402828096
Training time: 235.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.8251707863033367
Weighted AreaUnderROC: 0.8955231855628873
Root mean squared error: 0.3476481831669216
Relative absolute error: 60.539999999999246
Root relative squared error: 73.74731633535379
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.7047935748140308
Weighted FMeasure: 0.789575578473553
Iteration time: 139.0
Weighted AreaUnderPRC: 0.7438638178778012
Mean absolute error: 0.26906666666666457
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3099.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.1065624471144043
Kappa statistic: 0.6844433075053686
Training time: 135.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 20.92
Correctly Classified Instances: 79.08
Weighted Precision: 0.825235089894276
Weighted AreaUnderROC: 0.895824583083255
Root mean squared error: 0.3474350458878801
Relative absolute error: 60.499999999999275
Root relative squared error: 73.70210309075364
Weighted TruePositiveRate: 0.7908
Weighted MatthewsCorrelation: 0.7055406919155485
Weighted FMeasure: 0.7902382267933941
Iteration time: 107.0
Weighted AreaUnderPRC: 0.7441552422720825
Mean absolute error: 0.26888888888888696
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3206.0
Weighted Recall: 0.7908
Weighted FalsePositiveRate: 0.10612729984700102
Kappa statistic: 0.6856564623020958
Training time: 103.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8312009840449714
Weighted AreaUnderROC: 0.9007172809775206
Root mean squared error: 0.3443512418705354
Relative absolute error: 60.019999999999285
Root relative squared error: 73.0479294709992
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7177777518464141
Weighted FMeasure: 0.7999992456651597
Iteration time: 115.0
Weighted AreaUnderPRC: 0.7536102394233092
Mean absolute error: 0.26675555555555364
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3321.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.10122424508962634
Kappa statistic: 0.7001181738723241
Training time: 111.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8289164359047582
Weighted AreaUnderROC: 0.8998048000280859
Root mean squared error: 0.34495302954546553
Relative absolute error: 60.119999999999315
Root relative squared error: 73.17558791473247
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.7146858634490442
Weighted FMeasure: 0.7976702967969234
Iteration time: 122.0
Weighted AreaUnderPRC: 0.7512080453785586
Mean absolute error: 0.2671999999999982
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3443.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10221110291519382
Kappa statistic: 0.6971143652887293
Training time: 118.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 66.68000000000198
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8288861989400821
Weighted AreaUnderROC: 0.9004401703610263
Root mean squared error: 0.3444802848737049
Relative absolute error: 60.03999999999937
Root relative squared error: 73.07530362578096
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7160997710763608
Weighted FMeasure: 0.7989168677496222
Iteration time: 106.0
Weighted AreaUnderPRC: 0.7518575092254614
Mean absolute error: 0.2668444444444429
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3549.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.10133930149840542
Kappa statistic: 0.6995400318439494
Training time: 102.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8285543581613478
Weighted AreaUnderROC: 0.8994536212610154
Root mean squared error: 0.3449530295454656
Relative absolute error: 60.11999999999942
Root relative squared error: 73.17558791473249
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.7143794606980093
Weighted FMeasure: 0.7968398722292829
Iteration time: 115.0
Weighted AreaUnderPRC: 0.7501333938371756
Mean absolute error: 0.26719999999999866
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 3664.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10213894711570727
Kappa statistic: 0.6971330033204799
Training time: 111.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8302944833798128
Weighted AreaUnderROC: 0.9018102209997262
Root mean squared error: 0.3435328438662264
Relative absolute error: 59.89999999999942
Root relative squared error: 72.87432103743227
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7190540581805018
Weighted FMeasure: 0.8009327112608323
Iteration time: 123.0
Weighted AreaUnderPRC: 0.7541072885135702
Mean absolute error: 0.2662222222222209
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3787.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.10007998030767917
Kappa statistic: 0.7031642451118241
Training time: 119.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8333554662342758
Weighted AreaUnderROC: 0.9036992918065967
Root mean squared error: 0.34223665193244307
Relative absolute error: 59.69999999999948
Root relative squared error: 72.59935720560303
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.72500999131887
Weighted FMeasure: 0.8047409276975339
Iteration time: 120.0
Weighted AreaUnderPRC: 0.7579572355180162
Mean absolute error: 0.26533333333333226
Coverage of cases: 99.92
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3907.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09782149825667234
Kappa statistic: 0.7097873408156525
Training time: 116.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.69333333333532
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8306362653221133
Weighted AreaUnderROC: 0.9029450335043111
Root mean squared error: 0.3426260243948129
Relative absolute error: 59.75999999999951
Root relative squared error: 72.68195557816772
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7218440381799694
Weighted FMeasure: 0.8028862897819414
Iteration time: 134.0
Weighted AreaUnderPRC: 0.7554923972674827
Mean absolute error: 0.26559999999999906
Coverage of cases: 99.96
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 4041.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09855970646381382
Kappa statistic: 0.7074044576420189
Training time: 131.0
		
Time end:Sun Oct 08 10.54.13 EEST 2017