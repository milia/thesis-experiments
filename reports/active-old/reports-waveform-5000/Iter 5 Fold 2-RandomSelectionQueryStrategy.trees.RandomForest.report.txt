Sun Oct 08 12.17.49 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 12.17.49 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 70.08000000000153
Incorrectly Classified Instances: 21.72
Correctly Classified Instances: 78.28
Weighted Precision: 0.7840367186147187
Weighted AreaUnderROC: 0.9321280097843783
Root mean squared error: 0.31756154259187713
Relative absolute error: 49.90799999999879
Root relative squared error: 67.36497606323293
Weighted TruePositiveRate: 0.7828
Weighted MatthewsCorrelation: 0.6745434159924995
Weighted FMeasure: 0.7830672894804301
Iteration time: 95.0
Weighted AreaUnderPRC: 0.8460009070332101
Mean absolute error: 0.221813333333329
Coverage of cases: 99.72
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 95.0
Weighted Recall: 0.7828
Weighted FalsePositiveRate: 0.10895782832343805
Kappa statistic: 0.6741110975074001
Training time: 85.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 70.52000000000163
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.7873062214018777
Weighted AreaUnderROC: 0.928297962730986
Root mean squared error: 0.32184468303826164
Relative absolute error: 50.70599999999898
Root relative squared error: 68.2735673595568
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6766565466058699
Weighted FMeasure: 0.7834322485401922
Iteration time: 101.0
Weighted AreaUnderPRC: 0.836605172594122
Mean absolute error: 0.22535999999999654
Coverage of cases: 99.72
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 196.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10897945665167465
Kappa statistic: 0.6746151814230483
Training time: 91.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 69.33333333333468
Incorrectly Classified Instances: 20.92
Correctly Classified Instances: 79.08
Weighted Precision: 0.7968512050673803
Weighted AreaUnderROC: 0.938277423088519
Root mean squared error: 0.31097695520193397
Relative absolute error: 48.15599999999885
Root relative squared error: 65.96817414480967
Weighted TruePositiveRate: 0.7908
Weighted MatthewsCorrelation: 0.6892657751532867
Weighted FMeasure: 0.790927802188738
Iteration time: 103.0
Weighted AreaUnderPRC: 0.8587387637111684
Mean absolute error: 0.21402666666666256
Coverage of cases: 99.6
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 299.0
Weighted Recall: 0.7908
Weighted FalsePositiveRate: 0.10525701674790801
Kappa statistic: 0.685970683231723
Training time: 94.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 70.48000000000151
Incorrectly Classified Instances: 22.28
Correctly Classified Instances: 77.72
Weighted Precision: 0.7816152905097753
Weighted AreaUnderROC: 0.9329389828462638
Root mean squared error: 0.31715821498635743
Relative absolute error: 50.02199999999892
Root relative squared error: 67.27941735776211
Weighted TruePositiveRate: 0.7772
Weighted MatthewsCorrelation: 0.6677746084208338
Weighted FMeasure: 0.7776369300095525
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8475468060030212
Mean absolute error: 0.22231999999999624
Coverage of cases: 99.92
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 409.0
Weighted Recall: 0.7772
Weighted FalsePositiveRate: 0.11199424418224835
Kappa statistic: 0.6656074969238291
Training time: 100.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 70.20000000000172
Incorrectly Classified Instances: 20.84
Correctly Classified Instances: 79.16
Weighted Precision: 0.7967742282569988
Weighted AreaUnderROC: 0.9379694119877273
Root mean squared error: 0.31283222340417405
Relative absolute error: 49.41599999999883
Root relative squared error: 66.36173596282677
Weighted TruePositiveRate: 0.7916
Weighted MatthewsCorrelation: 0.6897722185108265
Weighted FMeasure: 0.7920950186835989
Iteration time: 112.0
Weighted AreaUnderPRC: 0.8581789134347464
Mean absolute error: 0.2196266666666625
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 521.0
Weighted Recall: 0.7916
Weighted FalsePositiveRate: 0.1048151273932889
Kappa statistic: 0.6872076201747122
Training time: 103.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 70.34666666666826
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.7938533719525803
Weighted AreaUnderROC: 0.9332427341897302
Root mean squared error: 0.3184378536962364
Relative absolute error: 50.34599999999893
Root relative squared error: 67.55086972052938
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.683566185113253
Weighted FMeasure: 0.7870419128829327
Iteration time: 117.0
Weighted AreaUnderPRC: 0.848366041456837
Mean absolute error: 0.22375999999999627
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 638.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10729042963700644
Kappa statistic: 0.6799762906261995
Training time: 108.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 70.82666666666829
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.8026389378511625
Weighted AreaUnderROC: 0.9387419676457784
Root mean squared error: 0.3121794355815243
Relative absolute error: 49.373999999998816
Root relative squared error: 66.22325875400529
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.6975757990284938
Weighted FMeasure: 0.7966395935741141
Iteration time: 120.0
Weighted AreaUnderPRC: 0.8592352249686588
Mean absolute error: 0.21943999999999578
Coverage of cases: 99.76
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 758.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10244087437421454
Kappa statistic: 0.6943995750172793
Training time: 111.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 71.24000000000163
Incorrectly Classified Instances: 22.52
Correctly Classified Instances: 77.48
Weighted Precision: 0.7846822369535337
Weighted AreaUnderROC: 0.9291658762185896
Root mean squared error: 0.3220269139890828
Relative absolute error: 50.80799999999903
Root relative squared error: 68.3122243818771
Weighted TruePositiveRate: 0.7748
Weighted MatthewsCorrelation: 0.6671510056997975
Weighted FMeasure: 0.7744036191642422
Iteration time: 126.0
Weighted AreaUnderPRC: 0.8386874608719216
Mean absolute error: 0.2258133333333301
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 884.0
Weighted Recall: 0.7748
Weighted FalsePositiveRate: 0.11341740336294999
Kappa statistic: 0.6619091336658754
Training time: 118.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 70.3200000000014
Incorrectly Classified Instances: 21.88
Correctly Classified Instances: 78.12
Weighted Precision: 0.7836464293034683
Weighted AreaUnderROC: 0.9307339176610377
Root mean squared error: 0.3181571938523458
Relative absolute error: 49.56599999999889
Root relative squared error: 67.49133277688284
Weighted TruePositiveRate: 0.7812
Weighted MatthewsCorrelation: 0.6727829447513151
Weighted FMeasure: 0.7814117335296283
Iteration time: 125.0
Weighted AreaUnderPRC: 0.844431644124725
Mean absolute error: 0.22029333333332943
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1009.0
Weighted Recall: 0.7812
Weighted FalsePositiveRate: 0.10982161406124387
Kappa statistic: 0.671679072836107
Training time: 117.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 69.18666666666806
Incorrectly Classified Instances: 21.76
Correctly Classified Instances: 78.24
Weighted Precision: 0.7867200357491062
Weighted AreaUnderROC: 0.9370497111587188
Root mean squared error: 0.31200854689147944
Relative absolute error: 47.98199999999882
Root relative squared error: 66.18700778853766
Weighted TruePositiveRate: 0.7824
Weighted MatthewsCorrelation: 0.6756812500002739
Weighted FMeasure: 0.782387652504367
Iteration time: 130.0
Weighted AreaUnderPRC: 0.858412971426255
Mean absolute error: 0.2132533333333291
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1139.0
Weighted Recall: 0.7824
Weighted FalsePositiveRate: 0.10936110442449512
Kappa statistic: 0.6734041469988723
Training time: 122.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 71.90666666666843
Incorrectly Classified Instances: 21.04
Correctly Classified Instances: 78.96
Weighted Precision: 0.7917907498540099
Weighted AreaUnderROC: 0.9326040446812005
Root mean squared error: 0.31712878561660984
Relative absolute error: 50.471999999998985
Root relative squared error: 67.27317444568774
Weighted TruePositiveRate: 0.7896
Weighted MatthewsCorrelation: 0.6852028972638967
Weighted FMeasure: 0.7898948756001746
Iteration time: 140.0
Weighted AreaUnderPRC: 0.8481752797808662
Mean absolute error: 0.22431999999999652
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1279.0
Weighted Recall: 0.7896
Weighted FalsePositiveRate: 0.10560243395310354
Kappa statistic: 0.6843052410611569
Training time: 132.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 68.94666666666818
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8031338506239059
Weighted AreaUnderROC: 0.9386988449939697
Root mean squared error: 0.3088948041000355
Relative absolute error: 47.6759999999988
Root relative squared error: 65.52648319572742
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7029206164560119
Weighted FMeasure: 0.801874574530304
Iteration time: 142.0
Weighted AreaUnderPRC: 0.8609987181702007
Mean absolute error: 0.21189333333332902
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1421.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09954269187503267
Kappa statistic: 0.7023170774450931
Training time: 134.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 68.16000000000135
Incorrectly Classified Instances: 20.84
Correctly Classified Instances: 79.16
Weighted Precision: 0.7957642532630181
Weighted AreaUnderROC: 0.9403925278429797
Root mean squared error: 0.3080519436718409
Relative absolute error: 47.23199999999875
Root relative squared error: 65.34768549841637
Weighted TruePositiveRate: 0.7916
Weighted MatthewsCorrelation: 0.6893246292421238
Weighted FMeasure: 0.7918421852749395
Iteration time: 147.0
Weighted AreaUnderPRC: 0.8653664544656254
Mean absolute error: 0.20991999999999544
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1568.0
Weighted Recall: 0.7916
Weighted FalsePositiveRate: 0.10472908391370238
Kappa statistic: 0.6872285011317406
Training time: 139.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 68.84000000000144
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.7910400831451065
Weighted AreaUnderROC: 0.935487777711005
Root mean squared error: 0.31313043075796015
Relative absolute error: 48.40199999999886
Root relative squared error: 66.42499529544534
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.6833095392833148
Weighted FMeasure: 0.7882886440359987
Iteration time: 149.0
Weighted AreaUnderPRC: 0.8538056498554524
Mean absolute error: 0.21511999999999593
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1717.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.106491773609839
Kappa statistic: 0.6818469301950555
Training time: 141.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 68.74666666666815
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.7928738225838168
Weighted AreaUnderROC: 0.9365408032823912
Root mean squared error: 0.31257639066314535
Relative absolute error: 48.50999999999887
Root relative squared error: 66.3074656430175
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.6841748837239416
Weighted FMeasure: 0.7885407439454628
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8562631981568142
Mean absolute error: 0.215599999999996
Coverage of cases: 99.8
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 1872.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.10660734731046555
Kappa statistic: 0.6818130078684642
Training time: 148.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 69.14666666666824
Incorrectly Classified Instances: 20.08
Correctly Classified Instances: 79.92
Weighted Precision: 0.8041430388395439
Weighted AreaUnderROC: 0.9414876377859531
Root mean squared error: 0.30759280442385595
Relative absolute error: 47.891999999998724
Root relative squared error: 65.25028735568866
Weighted TruePositiveRate: 0.7992
Weighted MatthewsCorrelation: 0.7010831256137191
Weighted FMeasure: 0.7994273160621761
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8651707706383164
Mean absolute error: 0.21285333333332865
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2027.0
Weighted Recall: 0.7992
Weighted FalsePositiveRate: 0.10099069041537555
Kappa statistic: 0.6986262158210911
Training time: 147.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 69.8666666666683
Incorrectly Classified Instances: 21.96
Correctly Classified Instances: 78.04
Weighted Precision: 0.7835474184632394
Weighted AreaUnderROC: 0.9321160517976441
Root mean squared error: 0.3175321506031565
Relative absolute error: 49.94999999999888
Root relative squared error: 67.35874108087185
Weighted TruePositiveRate: 0.7804
Weighted MatthewsCorrelation: 0.6719725648846638
Weighted FMeasure: 0.7806867119647114
Iteration time: 161.0
Weighted AreaUnderPRC: 0.8456648586909873
Mean absolute error: 0.22199999999999603
Coverage of cases: 99.76
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 2188.0
Weighted Recall: 0.7804
Weighted FalsePositiveRate: 0.11029811331532693
Kappa statistic: 0.6704359034450432
Training time: 154.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 68.28000000000146
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.7907633215379953
Weighted AreaUnderROC: 0.9350617807146838
Root mean squared error: 0.313424100328398
Relative absolute error: 48.15599999999887
Root relative squared error: 66.48729201885077
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.681966021584108
Weighted FMeasure: 0.787094441988545
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8522691720172517
Mean absolute error: 0.21402666666666265
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2354.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10716120154649503
Kappa statistic: 0.6800256267655622
Training time: 158.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 69.44000000000159
Incorrectly Classified Instances: 21.04
Correctly Classified Instances: 78.96
Weighted Precision: 0.7923217155737253
Weighted AreaUnderROC: 0.9380512443575649
Root mean squared error: 0.31127265647124513
Relative absolute error: 48.42599999999883
Root relative squared error: 66.03090185663028
Weighted TruePositiveRate: 0.7896
Weighted MatthewsCorrelation: 0.6854940376700133
Weighted FMeasure: 0.789989633037188
Iteration time: 170.0
Weighted AreaUnderPRC: 0.8601058389603815
Mean absolute error: 0.21522666666666246
Coverage of cases: 99.72
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 2524.0
Weighted Recall: 0.7896
Weighted FalsePositiveRate: 0.10566902880509282
Kappa statistic: 0.6842641578112566
Training time: 163.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 69.4133333333349
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8098096800765355
Weighted AreaUnderROC: 0.9411214999289312
Root mean squared error: 0.3068637048158881
Relative absolute error: 47.753999999998854
Root relative squared error: 65.0956219726023
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7135247895535154
Weighted FMeasure: 0.8089035041104667
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8658817217068973
Mean absolute error: 0.2122399999999959
Coverage of cases: 99.6
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 2702.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09591141686990737
Kappa statistic: 0.7131222905660685
Training time: 172.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 70.01333333333493
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.7886498474826612
Weighted AreaUnderROC: 0.9356396727595642
Root mean squared error: 0.3136707403206943
Relative absolute error: 49.27199999999887
Root relative squared error: 66.53961226217011
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.6804494054717255
Weighted FMeasure: 0.786754054241763
Iteration time: 194.0
Weighted AreaUnderPRC: 0.8535407295780028
Mean absolute error: 0.21898666666666267
Coverage of cases: 99.72
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 2896.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.1072414925290001
Kappa statistic: 0.6794754826354942
Training time: 187.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 67.84000000000157
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8007006213445409
Weighted AreaUnderROC: 0.9390949720544589
Root mean squared error: 0.3081558047481813
Relative absolute error: 47.11799999999878
Root relative squared error: 65.36971775983085
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.698912262889804
Weighted FMeasure: 0.7990790308574751
Iteration time: 180.0
Weighted AreaUnderPRC: 0.8615120271616113
Mean absolute error: 0.20941333333332887
Coverage of cases: 99.72
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 3076.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.1009574736117143
Kappa statistic: 0.6981079349958562
Training time: 174.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 70.09333333333488
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.7970936510475711
Weighted AreaUnderROC: 0.9378444716967163
Root mean squared error: 0.31104126200018195
Relative absolute error: 48.569999999998906
Root relative squared error: 65.98181567674493
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.6906160565337507
Weighted FMeasure: 0.793016258722044
Iteration time: 188.0
Weighted AreaUnderPRC: 0.8593443395887395
Mean absolute error: 0.21586666666666282
Coverage of cases: 99.76
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 3264.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10438276059963424
Kappa statistic: 0.6884348455429253
Training time: 182.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.06666666666837
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.7940677102802958
Weighted AreaUnderROC: 0.9344047841095715
Root mean squared error: 0.3141719274537416
Relative absolute error: 49.343999999998864
Root relative squared error: 66.64593010829647
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.6887592371396352
Weighted FMeasure: 0.7923628606451599
Iteration time: 189.0
Weighted AreaUnderPRC: 0.8513468144229537
Mean absolute error: 0.21930666666666265
Coverage of cases: 99.8
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 3453.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10439296169711297
Kappa statistic: 0.6878958321129262
Training time: 183.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 67.94666666666802
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.797594408923829
Weighted AreaUnderROC: 0.9376624830671914
Root mean squared error: 0.3093261493418652
Relative absolute error: 47.10599999999876
Root relative squared error: 65.61798533938652
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.6949710463196792
Weighted FMeasure: 0.796629990433249
Iteration time: 196.0
Weighted AreaUnderPRC: 0.8587432413115222
Mean absolute error: 0.20935999999999544
Coverage of cases: 99.6
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 3649.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10212733405322201
Kappa statistic: 0.6945153440921548
Training time: 190.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 67.25333333333462
Incorrectly Classified Instances: 20.04
Correctly Classified Instances: 79.96
Weighted Precision: 0.8027071715406587
Weighted AreaUnderROC: 0.9378525434341278
Root mean squared error: 0.308972490684849
Relative absolute error: 46.571999999998766
Root relative squared error: 65.54296300900609
Weighted TruePositiveRate: 0.7996
Weighted MatthewsCorrelation: 0.7007574977287695
Weighted FMeasure: 0.7997662194961644
Iteration time: 195.0
Weighted AreaUnderPRC: 0.8578242336787174
Mean absolute error: 0.20698666666666218
Coverage of cases: 99.56
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 3844.0
Weighted Recall: 0.7996
Weighted FalsePositiveRate: 0.1006574264359398
Kappa statistic: 0.6992707586012165
Training time: 189.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 69.2000000000014
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8054594729891711
Weighted AreaUnderROC: 0.9366314070473871
Root mean squared error: 0.31221787264664924
Relative absolute error: 48.50399999999886
Root relative squared error: 66.23141248682492
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.706971921269508
Weighted FMeasure: 0.8045445151871664
Iteration time: 206.0
Weighted AreaUnderPRC: 0.855133984698078
Mean absolute error: 0.21557333333332926
Coverage of cases: 99.6
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 4050.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09804401033662977
Kappa statistic: 0.706535296902261
Training time: 200.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 68.94666666666808
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.7976028615525144
Weighted AreaUnderROC: 0.9363849948452541
Root mean squared error: 0.30969662574848833
Relative absolute error: 47.47199999999889
Root relative squared error: 65.69657525320437
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.6949306034362916
Weighted FMeasure: 0.7967040638295736
Iteration time: 213.0
Weighted AreaUnderPRC: 0.8553952033590391
Mean absolute error: 0.21098666666666271
Coverage of cases: 99.56
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 4263.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10213670108737641
Kappa statistic: 0.6945213575782949
Training time: 207.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 70.34666666666813
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.7994115692788745
Weighted AreaUnderROC: 0.9380420992209414
Root mean squared error: 0.3127085970463006
Relative absolute error: 49.45799999999889
Root relative squared error: 66.33551085203108
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.6947700069603735
Weighted FMeasure: 0.7953884483323315
Iteration time: 218.0
Weighted AreaUnderPRC: 0.8564859253269201
Mean absolute error: 0.21981333333332945
Coverage of cases: 99.76
Instances selection time: 5.0
Test time: 20.0
Accumulative iteration time: 4481.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.1029397999970844
Kappa statistic: 0.6926273303290857
Training time: 213.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 68.53333333333487
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.7885830577432167
Weighted AreaUnderROC: 0.9329575555015912
Root mean squared error: 0.314065810513231
Relative absolute error: 48.24599999999897
Root relative squared error: 66.62341930582633
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.6808421796639561
Weighted FMeasure: 0.7870818631778964
Iteration time: 220.0
Weighted AreaUnderPRC: 0.8506233129858323
Mean absolute error: 0.2144266666666631
Coverage of cases: 99.8
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 4701.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10698554179087987
Kappa statistic: 0.6800926134884959
Training time: 214.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 68.97333333333499
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8048893864540178
Weighted AreaUnderROC: 0.9390562308655797
Root mean squared error: 0.308462855678496
Relative absolute error: 47.495999999998816
Root relative squared error: 65.43485309832941
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7066255238624383
Weighted FMeasure: 0.8045606154616386
Iteration time: 277.0
Weighted AreaUnderPRC: 0.8609438394007902
Mean absolute error: 0.21109333333332905
Coverage of cases: 99.8
Instances selection time: 13.0
Test time: 44.0
Accumulative iteration time: 4978.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09805281833830579
Kappa statistic: 0.7065502309191638
Training time: 264.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 68.04000000000148
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8055472960603923
Weighted AreaUnderROC: 0.9423767841142233
Root mean squared error: 0.3046046618159335
Relative absolute error: 46.61399999999876
Root relative squared error: 64.61640658532433
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7057793964853427
Weighted FMeasure: 0.8034181626534606
Iteration time: 329.0
Weighted AreaUnderPRC: 0.8687919344190285
Mean absolute error: 0.2071733333333288
Coverage of cases: 99.68
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 5307.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09878281359902373
Kappa statistic: 0.7046930752686933
Training time: 324.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 70.80000000000165
Incorrectly Classified Instances: 21.56
Correctly Classified Instances: 78.44
Weighted Precision: 0.7861245544530411
Weighted AreaUnderROC: 0.9291265648335179
Root mean squared error: 0.3195079550392013
Relative absolute error: 50.29799999999898
Root relative squared error: 67.77787249537958
Weighted TruePositiveRate: 0.7844
Weighted MatthewsCorrelation: 0.677190157389815
Weighted FMeasure: 0.7846990792147422
Iteration time: 240.0
Weighted AreaUnderPRC: 0.8389873887231919
Mean absolute error: 0.22354666666666317
Coverage of cases: 99.8
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 5547.0
Weighted Recall: 0.7844
Weighted FalsePositiveRate: 0.1081965209288523
Kappa statistic: 0.6764932686591961
Training time: 235.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 68.1733333333348
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8072567165859673
Weighted AreaUnderROC: 0.9418249612629374
Root mean squared error: 0.3051688057452781
Relative absolute error: 46.81199999999883
Root relative squared error: 64.73607958472577
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7089764335294845
Weighted FMeasure: 0.8059312476747557
Iteration time: 236.0
Weighted AreaUnderPRC: 0.8667034894696295
Mean absolute error: 0.2080533333333291
Coverage of cases: 99.72
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 5783.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.0975814347854208
Kappa statistic: 0.7083032033349947
Training time: 231.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 68.57333333333486
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.801379638961039
Weighted AreaUnderROC: 0.9385268005941898
Root mean squared error: 0.3083590980226343
Relative absolute error: 47.37599999999882
Root relative squared error: 65.41284277571145
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7012767310481614
Weighted FMeasure: 0.8009461064243415
Iteration time: 261.0
Weighted AreaUnderPRC: 0.8605805622771286
Mean absolute error: 0.21055999999999575
Coverage of cases: 99.72
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 6044.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09986964432490861
Kappa statistic: 0.7011414237190489
Training time: 256.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 67.57333333333474
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.801002770575206
Weighted AreaUnderROC: 0.9395582328554105
Root mean squared error: 0.3078484475625397
Relative absolute error: 47.153999999998824
Root relative squared error: 65.30451745476678
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7003019133783668
Weighted FMeasure: 0.8001881466345023
Iteration time: 256.0
Weighted AreaUnderPRC: 0.8624586678511532
Mean absolute error: 0.2095733333333291
Coverage of cases: 99.6
Instances selection time: 4.0
Test time: 15.0
Accumulative iteration time: 6300.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.10029391767617563
Kappa statistic: 0.6999236125548121
Training time: 252.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 68.02666666666808
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.798536507267748
Weighted AreaUnderROC: 0.9376305297676552
Root mean squared error: 0.30950282712763616
Relative absolute error: 47.33399999999876
Root relative squared error: 65.65546435750763
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.6975431240927735
Weighted FMeasure: 0.7984049749279425
Iteration time: 253.0
Weighted AreaUnderPRC: 0.8576451152573518
Mean absolute error: 0.2103733333333288
Coverage of cases: 99.56
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 6553.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10093377936901751
Kappa statistic: 0.6975797741352942
Training time: 248.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 68.78666666666803
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8064417275670674
Weighted AreaUnderROC: 0.9433556796954999
Root mean squared error: 0.3045389958609556
Relative absolute error: 47.13599999999875
Root relative squared error: 64.60247673270695
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7081876127052902
Weighted FMeasure: 0.8054707200726315
Iteration time: 253.0
Weighted AreaUnderPRC: 0.8710915343448163
Mean absolute error: 0.20949333333332879
Coverage of cases: 99.88
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 6806.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09773403709422253
Kappa statistic: 0.7077168980684589
Training time: 249.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 68.65333333333481
Incorrectly Classified Instances: 21.96
Correctly Classified Instances: 78.04
Weighted Precision: 0.7824644018850347
Weighted AreaUnderROC: 0.9336768783074807
Root mean squared error: 0.31394266992557607
Relative absolute error: 48.10199999999888
Root relative squared error: 66.5972972424553
Weighted TruePositiveRate: 0.7804
Weighted MatthewsCorrelation: 0.671518369013711
Weighted FMeasure: 0.780194329305197
Iteration time: 266.0
Weighted AreaUnderPRC: 0.8505646150958018
Mean absolute error: 0.21378666666666268
Coverage of cases: 99.68
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 7072.0
Weighted Recall: 0.7804
Weighted FalsePositiveRate: 0.1101642868154649
Kappa statistic: 0.6704952438479241
Training time: 262.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 68.2000000000015
Incorrectly Classified Instances: 20.68
Correctly Classified Instances: 79.32
Weighted Precision: 0.795728587948119
Weighted AreaUnderROC: 0.9362080493692003
Root mean squared error: 0.31146107300913084
Relative absolute error: 47.699999999998866
Root relative squared error: 66.07087104011828
Weighted TruePositiveRate: 0.7932
Weighted MatthewsCorrelation: 0.6908797347412216
Weighted FMeasure: 0.7933042025500314
Iteration time: 268.0
Weighted AreaUnderPRC: 0.8546669798733372
Mean absolute error: 0.21199999999999594
Coverage of cases: 99.76
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 7340.0
Weighted Recall: 0.7932
Weighted FalsePositiveRate: 0.10384283578433565
Kappa statistic: 0.689670654728891
Training time: 264.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 68.22666666666808
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.7964926481155581
Weighted AreaUnderROC: 0.935821739682322
Root mean squared error: 0.3106680972785359
Relative absolute error: 47.237999999998884
Root relative squared error: 65.90265548519227
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.6940671409210643
Weighted FMeasure: 0.7961265944317203
Iteration time: 270.0
Weighted AreaUnderPRC: 0.8566932057643422
Mean absolute error: 0.2099466666666627
Coverage of cases: 99.64
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 7610.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10223294566444169
Kappa statistic: 0.6939430979007617
Training time: 266.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 68.30666666666826
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8065333718040077
Weighted AreaUnderROC: 0.9412322004770858
Root mean squared error: 0.30604574821421576
Relative absolute error: 46.769999999998745
Root relative squared error: 64.92210717467465
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7074552118016402
Weighted FMeasure: 0.8046422333174352
Iteration time: 278.0
Weighted AreaUnderPRC: 0.8660479737113759
Mean absolute error: 0.20786666666666206
Coverage of cases: 99.76
Instances selection time: 4.0
Test time: 15.0
Accumulative iteration time: 7888.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09820307970180134
Kappa statistic: 0.7064900629179665
Training time: 274.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 67.48000000000143
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.7993789709087811
Weighted AreaUnderROC: 0.9423410554093226
Root mean squared error: 0.30504644455120666
Relative absolute error: 46.37999999999878
Root relative squared error: 64.71012285570116
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.6966660427878862
Weighted FMeasure: 0.7974607396061568
Iteration time: 283.0
Weighted AreaUnderPRC: 0.8685437891706799
Mean absolute error: 0.2061333333333289
Coverage of cases: 99.84
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 8171.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10181061872359894
Kappa statistic: 0.6956847010195223
Training time: 280.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 67.82666666666805
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8111950912054281
Weighted AreaUnderROC: 0.942533052040114
Root mean squared error: 0.3038069562512781
Relative absolute error: 46.19399999999876
Root relative squared error: 64.44718768107691
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7162039269340428
Weighted FMeasure: 0.8109034654876263
Iteration time: 337.0
Weighted AreaUnderPRC: 0.8690128430378432
Mean absolute error: 0.2053066666666621
Coverage of cases: 99.68
Instances selection time: 9.0
Test time: 34.0
Accumulative iteration time: 8508.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09481827461446268
Kappa statistic: 0.7161628968138715
Training time: 328.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 68.14666666666822
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8000890645943978
Weighted AreaUnderROC: 0.9380253351749643
Root mean squared error: 0.30811686094726976
Relative absolute error: 46.90799999999877
Root relative squared error: 65.36145653211794
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.6986315813034771
Weighted FMeasure: 0.7990246694209734
Iteration time: 297.0
Weighted AreaUnderPRC: 0.8606547812963987
Mean absolute error: 0.20847999999999553
Coverage of cases: 99.6
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 8805.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.10093810274752212
Kappa statistic: 0.6981086597560957
Training time: 294.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 67.3200000000013
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8114465478840867
Weighted AreaUnderROC: 0.9431586327549305
Root mean squared error: 0.30188297953566806
Relative absolute error: 45.36599999999886
Root relative squared error: 64.03905058634105
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.71637486807181
Weighted FMeasure: 0.8108718878080856
Iteration time: 292.0
Weighted AreaUnderPRC: 0.8709503558465738
Mean absolute error: 0.20162666666666254
Coverage of cases: 99.6
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 9097.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09484081476942863
Kappa statistic: 0.7161408210383701
Training time: 288.0
		
Time end:Sun Oct 08 12.18.03 EEST 2017