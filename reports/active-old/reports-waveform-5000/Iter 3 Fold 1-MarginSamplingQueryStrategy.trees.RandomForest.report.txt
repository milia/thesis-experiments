Sun Oct 08 11.53.34 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.53.34 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 72.586666666668
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.7949986287074973
Weighted AreaUnderROC: 0.9310477850129195
Root mean squared error: 0.3199499960931382
Relative absolute error: 51.647999999999236
Root relative squared error: 67.87164356342006
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.6921273845780264
Weighted FMeasure: 0.7948519734374039
Iteration time: 96.0
Weighted AreaUnderPRC: 0.8416651718900549
Mean absolute error: 0.22954666666666435
Coverage of cases: 99.92
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10278251254474066
Kappa statistic: 0.6921714335090295
Training time: 87.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 71.22666666666802
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.7904775897625456
Weighted AreaUnderROC: 0.9299701682420255
Root mean squared error: 0.3200499960943589
Relative absolute error: 50.80199999999905
Root relative squared error: 67.89285676711461
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.6850583764760986
Weighted FMeasure: 0.7900521899349605
Iteration time: 102.0
Weighted AreaUnderPRC: 0.839313293116181
Mean absolute error: 0.2257866666666635
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 198.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.1052193988023371
Kappa statistic: 0.6849600277283183
Training time: 93.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 72.53333333333478
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8001251271241817
Weighted AreaUnderROC: 0.9337466788856824
Root mean squared error: 0.31762451626619204
Relative absolute error: 51.515999999999195
Root relative squared error: 67.37833479687622
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6998641840635164
Weighted FMeasure: 0.8000422094358873
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8468165264293839
Mean absolute error: 0.2289599999999975
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 306.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.10019779260314629
Kappa statistic: 0.6999783504377676
Training time: 99.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 72.02666666666825
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.7945651198239284
Weighted AreaUnderROC: 0.9286274463617236
Root mean squared error: 0.32319653463488646
Relative absolute error: 51.84599999999919
Root relative squared error: 68.56033838889617
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.6904700048631843
Weighted FMeasure: 0.7916788793779489
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8382286498095162
Mean absolute error: 0.23042666666666414
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 416.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10350084728448565
Kappa statistic: 0.6892866020382798
Training time: 101.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 70.02666666666775
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.7986071288347807
Weighted AreaUnderROC: 0.9290310815060989
Root mean squared error: 0.32073665209950536
Relative absolute error: 50.231999999999125
Root relative squared error: 68.03851850238907
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.6960065812726992
Weighted FMeasure: 0.7952394601835978
Iteration time: 115.0
Weighted AreaUnderPRC: 0.8395548117703869
Mean absolute error: 0.22325333333333047
Coverage of cases: 99.64
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 531.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10194850500683313
Kappa statistic: 0.6946169548666659
Training time: 106.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 70.58666666666794
Incorrectly Classified Instances: 21.4
Correctly Classified Instances: 78.6
Weighted Precision: 0.790447558965157
Weighted AreaUnderROC: 0.9293807736827706
Root mean squared error: 0.3230603658760997
Relative absolute error: 50.36999999999912
Root relative squared error: 68.53145263307901
Weighted TruePositiveRate: 0.786
Weighted MatthewsCorrelation: 0.6820252448031059
Weighted FMeasure: 0.7843202571537213
Iteration time: 120.0
Weighted AreaUnderPRC: 0.8397761076334392
Mean absolute error: 0.22386666666666383
Coverage of cases: 99.44
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 651.0
Weighted Recall: 0.786
Weighted FalsePositiveRate: 0.10690069985751185
Kappa statistic: 0.6790782022236821
Training time: 111.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 70.80000000000146
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.7810711399855512
Weighted AreaUnderROC: 0.9207887881508908
Root mean squared error: 0.33028472565348743
Relative absolute error: 52.00799999999924
Root relative squared error: 70.06397076957566
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.6653681929185618
Weighted FMeasure: 0.7720507084156719
Iteration time: 124.0
Weighted AreaUnderPRC: 0.8211314453694655
Mean absolute error: 0.2311466666666644
Coverage of cases: 99.4
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 775.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.11310197306080723
Kappa statistic: 0.6610271178305736
Training time: 115.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 70.45333333333461
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7788686795353799
Weighted AreaUnderROC: 0.9196587341202201
Root mean squared error: 0.33199598391145163
Relative absolute error: 51.875999999999095
Root relative squared error: 70.42698346514605
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.6625336743878492
Weighted FMeasure: 0.7696416161363013
Iteration time: 128.0
Weighted AreaUnderPRC: 0.819136566405532
Mean absolute error: 0.23055999999999705
Coverage of cases: 99.48
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 903.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.11400207284749982
Kappa statistic: 0.6580788880389518
Training time: 120.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 72.226666666668
Incorrectly Classified Instances: 22.76
Correctly Classified Instances: 77.24
Weighted Precision: 0.7811031814214776
Weighted AreaUnderROC: 0.9187524122472635
Root mean squared error: 0.3359642838160023
Relative absolute error: 53.71199999999925
Root relative squared error: 71.26878699683296
Weighted TruePositiveRate: 0.7724
Weighted MatthewsCorrelation: 0.6640647850554062
Weighted FMeasure: 0.7702116100554369
Iteration time: 135.0
Weighted AreaUnderPRC: 0.8158805783145141
Mean absolute error: 0.2387199999999978
Coverage of cases: 99.56
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1038.0
Weighted Recall: 0.7724
Weighted FalsePositiveRate: 0.11379597104411528
Kappa statistic: 0.6586497552116803
Training time: 127.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 71.53333333333474
Incorrectly Classified Instances: 24.24
Correctly Classified Instances: 75.76
Weighted Precision: 0.7681731736764874
Weighted AreaUnderROC: 0.9109479104670656
Root mean squared error: 0.34175624841885904
Relative absolute error: 53.729999999999336
Root relative squared error: 72.49744823095469
Weighted TruePositiveRate: 0.7576
Weighted MatthewsCorrelation: 0.6438934920364798
Weighted FMeasure: 0.7527899172408022
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8027436070825025
Mean absolute error: 0.23879999999999818
Coverage of cases: 99.36
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1177.0
Weighted Recall: 0.7576
Weighted FalsePositiveRate: 0.12114762003854701
Kappa statistic: 0.636471309787872
Training time: 131.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 70.34666666666806
Incorrectly Classified Instances: 23.48
Correctly Classified Instances: 76.52
Weighted Precision: 0.7783898319264566
Weighted AreaUnderROC: 0.9198660072768851
Root mean squared error: 0.3346998655512124
Relative absolute error: 52.00199999999928
Root relative squared error: 71.00056337804624
Weighted TruePositiveRate: 0.7652
Weighted MatthewsCorrelation: 0.6558739297147255
Weighted FMeasure: 0.7619646672417841
Iteration time: 144.0
Weighted AreaUnderPRC: 0.8207967946605882
Mean absolute error: 0.23111999999999788
Coverage of cases: 99.52
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 1321.0
Weighted Recall: 0.7652
Weighted FalsePositiveRate: 0.1174655065085516
Kappa statistic: 0.6478149889940684
Training time: 134.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 71.76000000000123
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7747146784268544
Weighted AreaUnderROC: 0.9139044714372628
Root mean squared error: 0.3414205617709619
Relative absolute error: 53.82599999999921
Root relative squared error: 72.42623833943014
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.6491317259880641
Weighted FMeasure: 0.7564535143780002
Iteration time: 149.0
Weighted AreaUnderPRC: 0.8091496829542216
Mean absolute error: 0.2392266666666643
Coverage of cases: 99.28
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1470.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.12001459093777353
Kappa statistic: 0.6400443465365068
Training time: 140.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 70.78666666666797
Incorrectly Classified Instances: 22.72
Correctly Classified Instances: 77.28
Weighted Precision: 0.7856615413814342
Weighted AreaUnderROC: 0.9227036297342889
Root mean squared error: 0.33120789040520426
Relative absolute error: 51.47399999999906
Root relative squared error: 70.25980358640308
Weighted TruePositiveRate: 0.7728
Weighted MatthewsCorrelation: 0.6673713206741098
Weighted FMeasure: 0.7685278545031904
Iteration time: 152.0
Weighted AreaUnderPRC: 0.8287589533808633
Mean absolute error: 0.22877333333333022
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1622.0
Weighted Recall: 0.7728
Weighted FalsePositiveRate: 0.11371917509464723
Kappa statistic: 0.6592007633902901
Training time: 144.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 71.17333333333465
Incorrectly Classified Instances: 22.28
Correctly Classified Instances: 77.72
Weighted Precision: 0.7944704663033088
Weighted AreaUnderROC: 0.9213867676246835
Root mean squared error: 0.3334546445920337
Relative absolute error: 52.10399999999926
Root relative squared error: 70.73641212275298
Weighted TruePositiveRate: 0.7772
Weighted MatthewsCorrelation: 0.6756618727853826
Weighted FMeasure: 0.7739147645031064
Iteration time: 157.0
Weighted AreaUnderPRC: 0.8257485649862709
Mean absolute error: 0.23157333333333113
Coverage of cases: 99.36
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1779.0
Weighted Recall: 0.7772
Weighted FalsePositiveRate: 0.11159428625328732
Kappa statistic: 0.66575374031766
Training time: 149.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 71.25333333333458
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.800152488522981
Weighted AreaUnderROC: 0.9306976877789405
Root mean squared error: 0.3264066992776537
Relative absolute error: 50.867999999999114
Root relative squared error: 69.24131714518397
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6845817619713489
Weighted FMeasure: 0.7800145269935475
Iteration time: 158.0
Weighted AreaUnderPRC: 0.8451284775770215
Mean absolute error: 0.22607999999999714
Coverage of cases: 99.44
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1937.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10853838002624891
Kappa statistic: 0.6747641980429205
Training time: 150.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 70.54666666666805
Incorrectly Classified Instances: 22.44
Correctly Classified Instances: 77.56
Weighted Precision: 0.7914554648430517
Weighted AreaUnderROC: 0.9260221986141165
Root mean squared error: 0.33057122278464085
Relative absolute error: 51.46799999999911
Root relative squared error: 70.12474598884438
Weighted TruePositiveRate: 0.7756
Weighted MatthewsCorrelation: 0.6723346836245478
Weighted FMeasure: 0.7725949757611287
Iteration time: 168.0
Weighted AreaUnderPRC: 0.8311919421465802
Mean absolute error: 0.22874666666666377
Coverage of cases: 99.44
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2105.0
Weighted Recall: 0.7756
Weighted FalsePositiveRate: 0.11246981599332787
Kappa statistic: 0.663341448344696
Training time: 160.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 71.68000000000141
Incorrectly Classified Instances: 24.52
Correctly Classified Instances: 75.48
Weighted Precision: 0.7731364865626892
Weighted AreaUnderROC: 0.9122433376904342
Root mean squared error: 0.3435229249991906
Relative absolute error: 54.07199999999919
Root relative squared error: 72.87221692798946
Weighted TruePositiveRate: 0.7548
Weighted MatthewsCorrelation: 0.6429092891108574
Weighted FMeasure: 0.7493476277755396
Iteration time: 174.0
Weighted AreaUnderPRC: 0.801156313820365
Mean absolute error: 0.24031999999999756
Coverage of cases: 99.2
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2279.0
Weighted Recall: 0.7548
Weighted FalsePositiveRate: 0.12286943329922041
Kappa statistic: 0.6321194194376336
Training time: 165.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 70.58666666666811
Incorrectly Classified Instances: 22.92
Correctly Classified Instances: 77.08
Weighted Precision: 0.7909780509619855
Weighted AreaUnderROC: 0.9278560922908464
Root mean squared error: 0.33077686335856804
Relative absolute error: 51.389999999999226
Root relative squared error: 70.16836894213769
Weighted TruePositiveRate: 0.7708
Weighted MatthewsCorrelation: 0.6677004841775802
Weighted FMeasure: 0.7642907306633752
Iteration time: 182.0
Weighted AreaUnderPRC: 0.8336183462600802
Mean absolute error: 0.22839999999999763
Coverage of cases: 99.44
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2461.0
Weighted Recall: 0.7708
Weighted FalsePositiveRate: 0.11490320721788726
Kappa statistic: 0.6560596172933945
Training time: 174.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 71.56000000000127
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7887145373328782
Weighted AreaUnderROC: 0.9224260641251485
Root mean squared error: 0.33516165253998403
Relative absolute error: 52.92599999999923
Root relative squared error: 71.09852319141348
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.6672329759547551
Weighted FMeasure: 0.7665616213171057
Iteration time: 183.0
Weighted AreaUnderPRC: 0.8222495186038528
Mean absolute error: 0.23522666666666434
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 2644.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.11448690780497195
Kappa statistic: 0.6578054692531611
Training time: 175.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 70.0000000000013
Incorrectly Classified Instances: 24.16
Correctly Classified Instances: 75.84
Weighted Precision: 0.7833719283317716
Weighted AreaUnderROC: 0.9222668058261808
Root mean squared error: 0.33663036107873495
Relative absolute error: 52.45199999999917
Root relative squared error: 71.41008332161469
Weighted TruePositiveRate: 0.7584
Weighted MatthewsCorrelation: 0.6505663004907947
Weighted FMeasure: 0.7531730130535327
Iteration time: 186.0
Weighted AreaUnderPRC: 0.823435255035049
Mean absolute error: 0.23311999999999738
Coverage of cases: 99.36
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 2830.0
Weighted Recall: 0.7584
Weighted FalsePositiveRate: 0.12134534765084115
Kappa statistic: 0.6373900343254758
Training time: 179.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 70.26666666666803
Incorrectly Classified Instances: 22.96
Correctly Classified Instances: 77.04
Weighted Precision: 0.7914090558803214
Weighted AreaUnderROC: 0.9224213276549943
Root mean squared error: 0.33558307466259346
Relative absolute error: 52.49399999999919
Root relative squared error: 71.18792032360523
Weighted TruePositiveRate: 0.7704
Weighted MatthewsCorrelation: 0.6667870948355902
Weighted FMeasure: 0.7642816978863056
Iteration time: 188.0
Weighted AreaUnderPRC: 0.8200715764882821
Mean absolute error: 0.23330666666666416
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3018.0
Weighted Recall: 0.7704
Weighted FalsePositiveRate: 0.115378950172051
Kappa statistic: 0.6553580889497956
Training time: 180.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 70.28000000000135
Incorrectly Classified Instances: 23.96
Correctly Classified Instances: 76.04
Weighted Precision: 0.7844557274438112
Weighted AreaUnderROC: 0.925744529914203
Root mean squared error: 0.3346918981192497
Relative absolute error: 51.899999999999054
Root relative squared error: 70.9988732304954
Weighted TruePositiveRate: 0.7604
Weighted MatthewsCorrelation: 0.6530556866043083
Weighted FMeasure: 0.7537923189699042
Iteration time: 194.0
Weighted AreaUnderPRC: 0.8311664371489583
Mean absolute error: 0.23066666666666355
Coverage of cases: 99.52
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3212.0
Weighted Recall: 0.7604
Weighted FalsePositiveRate: 0.12049596688498278
Kappa statistic: 0.6403086788051378
Training time: 186.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 70.7333333333347
Incorrectly Classified Instances: 25.08
Correctly Classified Instances: 74.92
Weighted Precision: 0.7778316492853488
Weighted AreaUnderROC: 0.9130407264898193
Root mean squared error: 0.3445189883494569
Relative absolute error: 53.68199999999927
Root relative squared error: 73.08351387282886
Weighted TruePositiveRate: 0.7492
Weighted MatthewsCorrelation: 0.6383381872692474
Weighted FMeasure: 0.7413356377124004
Iteration time: 201.0
Weighted AreaUnderPRC: 0.8006957542769434
Mean absolute error: 0.23858666666666453
Coverage of cases: 99.12
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3413.0
Weighted Recall: 0.7492
Weighted FalsePositiveRate: 0.12615009191669038
Kappa statistic: 0.6234673861502296
Training time: 193.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.98666666666799
Incorrectly Classified Instances: 25.36
Correctly Classified Instances: 74.64
Weighted Precision: 0.7754245072885984
Weighted AreaUnderROC: 0.9216068461073481
Root mean squared error: 0.34046634684405
Relative absolute error: 53.35199999999915
Root relative squared error: 72.22381878577148
Weighted TruePositiveRate: 0.7464
Weighted MatthewsCorrelation: 0.6343995518051722
Weighted FMeasure: 0.737682472645141
Iteration time: 203.0
Weighted AreaUnderPRC: 0.819768220890036
Mean absolute error: 0.23711999999999733
Coverage of cases: 99.32
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3616.0
Weighted Recall: 0.7464
Weighted FalsePositiveRate: 0.12754899782221682
Kappa statistic: 0.619258916908123
Training time: 195.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 69.6133333333347
Incorrectly Classified Instances: 23.12
Correctly Classified Instances: 76.88
Weighted Precision: 0.7910654764188689
Weighted AreaUnderROC: 0.9281458743651939
Root mean squared error: 0.3317589486359027
Relative absolute error: 51.533999999999075
Root relative squared error: 70.37670068992973
Weighted TruePositiveRate: 0.7688
Weighted MatthewsCorrelation: 0.6650271706438813
Weighted FMeasure: 0.7610643931388247
Iteration time: 212.0
Weighted AreaUnderPRC: 0.834416732214156
Mean absolute error: 0.229039999999997
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3828.0
Weighted Recall: 0.7688
Weighted FalsePositiveRate: 0.11637957617368257
Kappa statistic: 0.6528583570807046
Training time: 204.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 68.84000000000114
Incorrectly Classified Instances: 24.72
Correctly Classified Instances: 75.28
Weighted Precision: 0.7852650410777383
Weighted AreaUnderROC: 0.9213827586667815
Root mean squared error: 0.33942696808984735
Relative absolute error: 51.68399999999928
Root relative squared error: 72.0033332561761
Weighted TruePositiveRate: 0.7528
Weighted MatthewsCorrelation: 0.6455112891337194
Weighted FMeasure: 0.7435696396966067
Iteration time: 223.0
Weighted AreaUnderPRC: 0.8240836874111259
Mean absolute error: 0.22970666666666453
Coverage of cases: 98.96
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 4051.0
Weighted Recall: 0.7528
Weighted FalsePositiveRate: 0.12442581820667482
Kappa statistic: 0.6288202682872687
Training time: 215.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 70.24000000000119
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7868260229520679
Weighted AreaUnderROC: 0.9197051669232619
Root mean squared error: 0.3409868032637037
Relative absolute error: 53.039999999999196
Root relative squared error: 72.33422426486625
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.6440233664749069
Weighted FMeasure: 0.7403698405700329
Iteration time: 219.0
Weighted AreaUnderPRC: 0.8185839182641804
Mean absolute error: 0.23573333333333085
Coverage of cases: 99.16
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 4270.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.12556952395614362
Kappa statistic: 0.6257718830341911
Training time: 211.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 70.4000000000012
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.768899578227862
Weighted AreaUnderROC: 0.9142158342420765
Root mean squared error: 0.34334821974199825
Relative absolute error: 53.63399999999909
Root relative squared error: 72.83515634636856
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.627567968624321
Weighted FMeasure: 0.7348214235674061
Iteration time: 221.0
Weighted AreaUnderPRC: 0.8025383474081883
Mean absolute error: 0.23837333333333044
Coverage of cases: 98.84
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4491.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.1295770413134022
Kappa statistic: 0.6137512716773839
Training time: 214.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 69.61333333333462
Incorrectly Classified Instances: 24.04
Correctly Classified Instances: 75.96
Weighted Precision: 0.7888066789613587
Weighted AreaUnderROC: 0.9268074676398305
Root mean squared error: 0.3327942307192235
Relative absolute error: 51.46199999999913
Root relative squared error: 70.59631718439684
Weighted TruePositiveRate: 0.7596
Weighted MatthewsCorrelation: 0.6542543813986372
Weighted FMeasure: 0.7516460151125857
Iteration time: 228.0
Weighted AreaUnderPRC: 0.8318211088871856
Mean absolute error: 0.22871999999999718
Coverage of cases: 99.24
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 4719.0
Weighted Recall: 0.7596
Weighted FalsePositiveRate: 0.12118424267612672
Kappa statistic: 0.638971733829538
Training time: 220.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 69.33333333333452
Incorrectly Classified Instances: 25.16
Correctly Classified Instances: 74.84
Weighted Precision: 0.7859725673249551
Weighted AreaUnderROC: 0.9251180321967247
Root mean squared error: 0.3384001576043757
Relative absolute error: 51.86999999999914
Root relative squared error: 71.78551385899497
Weighted TruePositiveRate: 0.7484
Weighted MatthewsCorrelation: 0.6412280984857096
Weighted FMeasure: 0.7391512966455338
Iteration time: 230.0
Weighted AreaUnderPRC: 0.8291133916628746
Mean absolute error: 0.23053333333333056
Coverage of cases: 99.2
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 4949.0
Weighted Recall: 0.7484
Weighted FalsePositiveRate: 0.12697211644429132
Kappa statistic: 0.6220705964915654
Training time: 223.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 70.38666666666788
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7865674119941897
Weighted AreaUnderROC: 0.9237213594275662
Root mean squared error: 0.3404389323603667
Relative absolute error: 53.03399999999907
Root relative squared error: 72.21800329557693
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.6388692482491632
Weighted FMeasure: 0.7358595987278317
Iteration time: 248.0
Weighted AreaUnderPRC: 0.8278308439023117
Mean absolute error: 0.23570666666666362
Coverage of cases: 99.36
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5197.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.12826040882357925
Kappa statistic: 0.6184448219537356
Training time: 239.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 70.70666666666786
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7755818052888881
Weighted AreaUnderROC: 0.9199302697821273
Root mean squared error: 0.34466215341983747
Relative absolute error: 54.16799999999907
Root relative squared error: 73.11388377045742
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6215832820556522
Weighted FMeasure: 0.7238205611527777
Iteration time: 242.0
Weighted AreaUnderPRC: 0.8179581466077045
Mean absolute error: 0.24074666666666367
Coverage of cases: 99.16
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5439.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13397744212465493
Kappa statistic: 0.6010422890366902
Training time: 233.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 71.22666666666788
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7785138643075028
Weighted AreaUnderROC: 0.9236234497923962
Root mean squared error: 0.343600543266935
Relative absolute error: 54.071999999999214
Root relative squared error: 72.88868224902927
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.62522763826434
Weighted FMeasure: 0.7244710725612967
Iteration time: 251.0
Weighted AreaUnderPRC: 0.8231116987314188
Mean absolute error: 0.24031999999999762
Coverage of cases: 99.4
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 5690.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13342957367592198
Kappa statistic: 0.6033331554342637
Training time: 245.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 71.12000000000124
Incorrectly Classified Instances: 24.44
Correctly Classified Instances: 75.56
Weighted Precision: 0.7856854223165759
Weighted AreaUnderROC: 0.927739240187836
Root mean squared error: 0.33377836958077367
Relative absolute error: 52.12799999999909
Root relative squared error: 70.80508456318626
Weighted TruePositiveRate: 0.7556
Weighted MatthewsCorrelation: 0.6488436143278528
Weighted FMeasure: 0.7472112014750526
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8340675205056319
Mean absolute error: 0.23167999999999705
Coverage of cases: 99.32
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 5944.0
Weighted Recall: 0.7556
Weighted FalsePositiveRate: 0.12326130675658023
Kappa statistic: 0.632919271077403
Training time: 248.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 68.92000000000131
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7908579108652806
Weighted AreaUnderROC: 0.92231636912608
Root mean squared error: 0.34045851435967905
Relative absolute error: 52.21799999999918
Root relative squared error: 72.22215726492782
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.64610919072935
Weighted FMeasure: 0.7403097793561308
Iteration time: 265.0
Weighted AreaUnderPRC: 0.8234503562532058
Mean absolute error: 0.23207999999999743
Coverage of cases: 99.32
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6209.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.12578717249432192
Kappa statistic: 0.6256522543786068
Training time: 259.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 70.28000000000115
Incorrectly Classified Instances: 24.08
Correctly Classified Instances: 75.92
Weighted Precision: 0.7888509194087101
Weighted AreaUnderROC: 0.928398933437228
Root mean squared error: 0.3322489026819093
Relative absolute error: 51.77399999999904
Root relative squared error: 70.48063563845005
Weighted TruePositiveRate: 0.7592
Weighted MatthewsCorrelation: 0.6539243893551686
Weighted FMeasure: 0.7529678134355773
Iteration time: 267.0
Weighted AreaUnderPRC: 0.8349124151351028
Mean absolute error: 0.2301066666666635
Coverage of cases: 99.32
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 6476.0
Weighted Recall: 0.7592
Weighted FalsePositiveRate: 0.12149877258725367
Kappa statistic: 0.6383235708734402
Training time: 262.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 69.92000000000144
Incorrectly Classified Instances: 23.88
Correctly Classified Instances: 76.12
Weighted Precision: 0.7867220465987589
Weighted AreaUnderROC: 0.9281028481546962
Root mean squared error: 0.33099848942253407
Relative absolute error: 51.413999999999035
Root relative squared error: 70.21538292995311
Weighted TruePositiveRate: 0.7612
Weighted MatthewsCorrelation: 0.6546871127348586
Weighted FMeasure: 0.7562482462328125
Iteration time: 266.0
Weighted AreaUnderPRC: 0.8341070916862802
Mean absolute error: 0.22850666666666347
Coverage of cases: 99.64
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6742.0
Weighted Recall: 0.7612
Weighted FalsePositiveRate: 0.12034204924378339
Kappa statistic: 0.6414042250150646
Training time: 260.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 68.57333333333467
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7982329026509671
Weighted AreaUnderROC: 0.9287093415374348
Root mean squared error: 0.32940754899263114
Relative absolute error: 50.579999999999124
Root relative squared error: 69.87789350001864
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.6713664243120735
Weighted FMeasure: 0.7675876785775937
Iteration time: 270.0
Weighted AreaUnderPRC: 0.835624243039803
Mean absolute error: 0.22479999999999717
Coverage of cases: 99.24
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 7012.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.1150869907208074
Kappa statistic: 0.6575758871908391
Training time: 264.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 70.01333333333467
Incorrectly Classified Instances: 22.88
Correctly Classified Instances: 77.12
Weighted Precision: 0.7926368017001624
Weighted AreaUnderROC: 0.9317415149269346
Root mean squared error: 0.32623917606565794
Relative absolute error: 50.753999999999024
Root relative squared error: 69.20578010542147
Weighted TruePositiveRate: 0.7712
Weighted MatthewsCorrelation: 0.667898206813681
Weighted FMeasure: 0.767265751214173
Iteration time: 282.0
Weighted AreaUnderPRC: 0.8430480609422222
Mean absolute error: 0.22557333333333004
Coverage of cases: 99.68
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 7294.0
Weighted Recall: 0.7712
Weighted FalsePositiveRate: 0.11531980624870916
Kappa statistic: 0.6564196428013669
Training time: 277.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 70.08000000000129
Incorrectly Classified Instances: 20.88
Correctly Classified Instances: 79.12
Weighted Precision: 0.8117168464130958
Weighted AreaUnderROC: 0.9393279993426539
Root mean squared error: 0.31810690027096133
Relative absolute error: 49.679999999999
Root relative squared error: 67.4806638971487
Weighted TruePositiveRate: 0.7912
Weighted MatthewsCorrelation: 0.6975385732866556
Weighted FMeasure: 0.7881856252135139
Iteration time: 294.0
Weighted AreaUnderPRC: 0.8578299241213615
Mean absolute error: 0.22079999999999655
Coverage of cases: 99.76
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 7588.0
Weighted Recall: 0.7912
Weighted FalsePositiveRate: 0.10541113490947795
Kappa statistic: 0.6864355628685884
Training time: 289.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 70.46666666666806
Incorrectly Classified Instances: 23.76
Correctly Classified Instances: 76.24
Weighted Precision: 0.7839014030237678
Weighted AreaUnderROC: 0.9301030536101053
Root mean squared error: 0.32708001059883324
Relative absolute error: 51.72599999999903
Root relative squared error: 69.38414804550068
Weighted TruePositiveRate: 0.7624
Weighted MatthewsCorrelation: 0.6543494039747665
Weighted FMeasure: 0.7605356982909182
Iteration time: 352.0
Weighted AreaUnderPRC: 0.8392792863080125
Mean absolute error: 0.22989333333333012
Coverage of cases: 99.64
Instances selection time: 13.0
Test time: 35.0
Accumulative iteration time: 7940.0
Weighted Recall: 0.7624
Weighted FalsePositiveRate: 0.11988606025298018
Kappa statistic: 0.6431873533103563
Training time: 339.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 70.16000000000139
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.8036276488185318
Weighted AreaUnderROC: 0.931496661869494
Root mean squared error: 0.3240946363847028
Relative absolute error: 50.585999999999004
Root relative squared error: 68.75085454014337
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6852996550940025
Weighted FMeasure: 0.7796499876507098
Iteration time: 330.0
Weighted AreaUnderPRC: 0.8419056412869446
Mean absolute error: 0.2248266666666633
Coverage of cases: 99.4
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 8270.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10927665456108189
Kappa statistic: 0.6744690000792806
Training time: 325.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 69.28000000000125
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.792574403087536
Weighted AreaUnderROC: 0.9325954693294876
Root mean squared error: 0.3196790056707917
Relative absolute error: 49.55999999999901
Root relative squared error: 67.81415781383673
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6796802591393417
Weighted FMeasure: 0.7817082785050868
Iteration time: 308.0
Weighted AreaUnderPRC: 0.8453551047180026
Mean absolute error: 0.22026666666666328
Coverage of cases: 99.6
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 8578.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10900867581909456
Kappa statistic: 0.6745736073671733
Training time: 303.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 70.52000000000133
Incorrectly Classified Instances: 23.2
Correctly Classified Instances: 76.8
Weighted Precision: 0.7828221148897615
Weighted AreaUnderROC: 0.9297133751508291
Root mean squared error: 0.32483021205957113
Relative absolute error: 51.191999999999
Root relative squared error: 68.90689370447595
Weighted TruePositiveRate: 0.768
Weighted MatthewsCorrelation: 0.6594454028498572
Weighted FMeasure: 0.7666981729668915
Iteration time: 309.0
Weighted AreaUnderPRC: 0.8395634739268435
Mean absolute error: 0.2275199999999966
Coverage of cases: 99.76
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 8887.0
Weighted Recall: 0.768
Weighted FalsePositiveRate: 0.11685551639528725
Kappa statistic: 0.6516759192752938
Training time: 304.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 68.94666666666808
Incorrectly Classified Instances: 21.48
Correctly Classified Instances: 78.52
Weighted Precision: 0.7956042981416714
Weighted AreaUnderROC: 0.9365779386227406
Root mean squared error: 0.31648486009075727
Relative absolute error: 49.09799999999892
Root relative squared error: 67.1365772139149
Weighted TruePositiveRate: 0.7852
Weighted MatthewsCorrelation: 0.6831739701322853
Weighted FMeasure: 0.7838400353519097
Iteration time: 310.0
Weighted AreaUnderPRC: 0.8540387635614577
Mean absolute error: 0.21821333333332954
Coverage of cases: 99.72
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 9197.0
Weighted Recall: 0.7852
Weighted FalsePositiveRate: 0.10808619140424612
Kappa statistic: 0.6775501400525239
Training time: 304.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 70.49333333333479
Incorrectly Classified Instances: 20.88
Correctly Classified Instances: 79.12
Weighted Precision: 0.8010184441762614
Weighted AreaUnderROC: 0.9340010437509636
Root mean squared error: 0.3196039215445674
Relative absolute error: 50.78399999999895
Root relative squared error: 67.79823006539291
Weighted TruePositiveRate: 0.7912
Weighted MatthewsCorrelation: 0.6917109337506863
Weighted FMeasure: 0.7907420773015483
Iteration time: 323.0
Weighted AreaUnderPRC: 0.8462609699985693
Mean absolute error: 0.22570666666666306
Coverage of cases: 99.76
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9520.0
Weighted Recall: 0.7912
Weighted FalsePositiveRate: 0.10510259650350431
Kappa statistic: 0.6865617869580881
Training time: 318.0
		
Time end:Sun Oct 08 11.53.48 EEST 2017