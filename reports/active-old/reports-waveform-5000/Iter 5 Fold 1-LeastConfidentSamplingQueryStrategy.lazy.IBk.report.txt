Sun Oct 08 11.13.45 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.13.45 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7071662415691966
Weighted AreaUnderROC: 0.7810188273677318
Root mean squared error: 0.4399017364441522
Relative absolute error: 44.135188866799034
Root relative squared error: 93.31725026861902
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5617779231122522
Weighted FMeasure: 0.7067484328850145
Iteration time: 867.0
Weighted AreaUnderPRC: 0.598992950688409
Mean absolute error: 0.1961563949635522
Coverage of cases: 70.8
Instances selection time: 867.0
Test time: 1088.0
Accumulative iteration time: 867.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14596234526453647
Kappa statistic: 0.5621304500963072
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.16
Correctly Classified Instances: 70.84
Weighted Precision: 0.7076834063001868
Weighted AreaUnderROC: 0.7812558950715215
Root mean squared error: 0.43965009573117686
Relative absolute error: 44.062715105160756
Root relative squared error: 93.26386921224876
Weighted TruePositiveRate: 0.7084
Weighted MatthewsCorrelation: 0.562248384878973
Weighted FMeasure: 0.7077210681080587
Iteration time: 880.0
Weighted AreaUnderPRC: 0.5994871853231567
Mean absolute error: 0.19583428935627095
Coverage of cases: 70.84
Instances selection time: 880.0
Test time: 1140.0
Accumulative iteration time: 1747.0
Weighted Recall: 0.7084
Weighted FalsePositiveRate: 0.14588820985695708
Kappa statistic: 0.5626525166857964
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7083631231114564
Weighted AreaUnderROC: 0.7815135616459148
Root mean squared error: 0.4393945341190849
Relative absolute error: 43.9911602209949
Root relative squared error: 93.20965640757241
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.5628676086707749
Weighted FMeasure: 0.7084568961030209
Iteration time: 908.0
Weighted AreaUnderPRC: 0.6000548690563757
Mean absolute error: 0.19551626764886712
Coverage of cases: 70.88
Instances selection time: 908.0
Test time: 1181.0
Accumulative iteration time: 2655.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14577287670817035
Kappa statistic: 0.5632026557278532
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7110833790820112
Weighted AreaUnderROC: 0.7832895818140161
Root mean squared error: 0.4376227948749847
Relative absolute error: 43.62202486678438
Root relative squared error: 92.83381375737314
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5665773580511666
Weighted FMeasure: 0.7110129103659396
Iteration time: 921.0
Weighted AreaUnderPRC: 0.6026778380599176
Mean absolute error: 0.19387566607459814
Coverage of cases: 71.12
Instances selection time: 921.0
Test time: 1203.0
Accumulative iteration time: 3576.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14462083637196788
Kappa statistic: 0.5667710256461953
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.92
Correctly Classified Instances: 71.08
Weighted Precision: 0.7112101147845059
Weighted AreaUnderROC: 0.7829460866121075
Root mean squared error: 0.4379654946419548
Relative absolute error: 43.67135506003288
Root relative squared error: 92.90651135611381
Weighted TruePositiveRate: 0.7108
Weighted MatthewsCorrelation: 0.5661848094216464
Weighted FMeasure: 0.7107373974046979
Iteration time: 939.0
Weighted AreaUnderPRC: 0.6024205738054296
Mean absolute error: 0.19409491137792484
Coverage of cases: 71.08
Instances selection time: 939.0
Test time: 1242.0
Accumulative iteration time: 4515.0
Weighted Recall: 0.7108
Weighted FalsePositiveRate: 0.14490782677578526
Kappa statistic: 0.5661215200605485
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7184821614036194
Weighted AreaUnderROC: 0.787707316791115
Root mean squared error: 0.4331291458266129
Relative absolute error: 42.70646766169233
Root relative squared error: 91.88056684306028
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5762136735964335
Weighted FMeasure: 0.7171910341391092
Iteration time: 959.0
Weighted AreaUnderPRC: 0.6095027497982628
Mean absolute error: 0.1898065229408557
Coverage of cases: 71.72
Instances selection time: 959.0
Test time: 1279.0
Accumulative iteration time: 5474.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14178536641776968
Kappa statistic: 0.5756690684477601
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.64
Correctly Classified Instances: 71.36
Weighted Precision: 0.716502523216547
Weighted AreaUnderROC: 0.784935142468294
Root mean squared error: 0.43591176183765157
Relative absolute error: 43.234670947031624
Root relative squared error: 92.4708488383134
Weighted TruePositiveRate: 0.7136
Weighted MatthewsCorrelation: 0.5716097157287119
Weighted FMeasure: 0.7136039826923847
Iteration time: 978.0
Weighted AreaUnderPRC: 0.6060030380451399
Mean absolute error: 0.19215409309791923
Coverage of cases: 71.36
Instances selection time: 978.0
Test time: 1316.0
Accumulative iteration time: 6452.0
Weighted Recall: 0.7136
Weighted FalsePositiveRate: 0.143729715063412
Kappa statistic: 0.570190940873641
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7194758053682259
Weighted AreaUnderROC: 0.7867145957814958
Root mean squared error: 0.43411382313379404
Relative absolute error: 42.867807153967334
Root relative squared error: 92.08944844341677
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.5755354628603008
Weighted FMeasure: 0.7159245878478093
Iteration time: 994.0
Weighted AreaUnderPRC: 0.6087437880222466
Mean absolute error: 0.19052358735096683
Coverage of cases: 71.6
Instances selection time: 993.0
Test time: 1353.0
Accumulative iteration time: 7446.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.14257080843700828
Kappa statistic: 0.5737654687115038
Training time: 1.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7161180426283849
Weighted AreaUnderROC: 0.783059561092275
Root mean squared error: 0.4377975464654663
Relative absolute error: 43.576470588236276
Root relative squared error: 92.87088416776893
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5690662735813954
Weighted FMeasure: 0.711124429133565
Iteration time: 1011.0
Weighted AreaUnderPRC: 0.6039651719065743
Mean absolute error: 0.19367320261438437
Coverage of cases: 71.12
Instances selection time: 1011.0
Test time: 1391.0
Accumulative iteration time: 8457.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14508087781545
Kappa statistic: 0.56650810389684
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7181031092147779
Weighted AreaUnderROC: 0.783626391417571
Root mean squared error: 0.43721956627394976
Relative absolute error: 43.44948755490506
Root relative squared error: 92.74827605392508
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.5709428693320229
Weighted FMeasure: 0.7117314682546024
Iteration time: 1027.0
Weighted AreaUnderPRC: 0.6050958779343952
Mean absolute error: 0.19310883357735673
Coverage of cases: 71.2
Instances selection time: 1027.0
Test time: 1428.0
Accumulative iteration time: 9484.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.14474721716485817
Kappa statistic: 0.5676694573603194
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.36
Correctly Classified Instances: 70.64
Weighted Precision: 0.7150890684222332
Weighted AreaUnderROC: 0.7793530292265448
Root mean squared error: 0.4414771826737424
Relative absolute error: 44.27880512090759
Root relative squared error: 93.65145288232043
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.5639596988359586
Weighted FMeasure: 0.7057245673525042
Iteration time: 1082.0
Weighted AreaUnderPRC: 0.599774025313977
Mean absolute error: 0.19679468942625689
Coverage of cases: 70.64
Instances selection time: 1082.0
Test time: 1465.0
Accumulative iteration time: 10566.0
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.14769394154691012
Kappa statistic: 0.5591906115047167
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.52
Correctly Classified Instances: 70.48
Weighted Precision: 0.7140688975525891
Weighted AreaUnderROC: 0.7781369730935382
Root mean squared error: 0.4427044483218293
Relative absolute error: 44.5112033195038
Root relative squared error: 93.91179524094429
Weighted TruePositiveRate: 0.7048
Weighted MatthewsCorrelation: 0.5618672225301202
Weighted FMeasure: 0.7041422060127298
Iteration time: 1056.0
Weighted AreaUnderPRC: 0.5982131099699857
Mean absolute error: 0.19782757030890671
Coverage of cases: 70.48
Instances selection time: 1056.0
Test time: 1503.0
Accumulative iteration time: 11622.0
Weighted Recall: 0.7048
Weighted FalsePositiveRate: 0.14852605381292322
Kappa statistic: 0.556772196524085
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.52
Correctly Classified Instances: 70.48
Weighted Precision: 0.7150162828068145
Weighted AreaUnderROC: 0.7781163742229347
Root mean squared error: 0.44272904405165875
Relative absolute error: 44.504979811573214
Root relative squared error: 93.91701278314946
Weighted TruePositiveRate: 0.7048
Weighted MatthewsCorrelation: 0.5624082046633981
Weighted FMeasure: 0.7040417927106238
Iteration time: 1072.0
Weighted AreaUnderPRC: 0.5984186275467658
Mean absolute error: 0.19779991027365965
Coverage of cases: 70.48
Instances selection time: 1072.0
Test time: 1541.0
Accumulative iteration time: 12694.0
Weighted Recall: 0.7048
Weighted FalsePositiveRate: 0.14856725155413059
Kappa statistic: 0.5567479183368701
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7139978212334348
Weighted AreaUnderROC: 0.7759728268372205
Root mean squared error: 0.4448471366272416
Relative absolute error: 44.91743119265807
Root relative squared error: 94.36632807016213
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5592060040692142
Weighted FMeasure: 0.7010669726760501
Iteration time: 1085.0
Weighted AreaUnderPRC: 0.5959060250483347
Mean absolute error: 0.1996330275229257
Coverage of cases: 70.2
Instances selection time: 1085.0
Test time: 1578.0
Accumulative iteration time: 13779.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.15005434632555903
Kappa statistic: 0.5524986911337556
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7144978433378371
Weighted AreaUnderROC: 0.7756533636510161
Root mean squared error: 0.44516783219703826
Relative absolute error: 44.97164750957751
Root relative squared error: 94.43435787379202
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5591456157550443
Weighted FMeasure: 0.7004560833443882
Iteration time: 1097.0
Weighted AreaUnderPRC: 0.5956889765956801
Mean absolute error: 0.19987398893145653
Coverage of cases: 70.16
Instances selection time: 1097.0
Test time: 1618.0
Accumulative iteration time: 14876.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.1502932726979678
Kappa statistic: 0.5518755151028764
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.96
Correctly Classified Instances: 70.04
Weighted Precision: 0.7138998863770757
Weighted AreaUnderROC: 0.774739604978711
Root mean squared error: 0.4460832134070339
Relative absolute error: 45.145703611458345
Root relative squared error: 94.62853955207963
Weighted TruePositiveRate: 0.7004
Weighted MatthewsCorrelation: 0.5576902618991098
Weighted FMeasure: 0.6991628450490219
Iteration time: 1120.0
Weighted AreaUnderPRC: 0.5945601723460286
Mean absolute error: 0.20064757160648247
Coverage of cases: 70.04
Instances selection time: 1120.0
Test time: 1657.0
Accumulative iteration time: 15996.0
Weighted Recall: 0.7004
Weighted FalsePositiveRate: 0.15092079004257825
Kappa statistic: 0.5500590269693058
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.88
Correctly Classified Instances: 70.12
Weighted Precision: 0.7158550890731956
Weighted AreaUnderROC: 0.7753201678844093
Root mean squared error: 0.4455073835953616
Relative absolute error: 45.02114216281959
Root relative squared error: 94.50638760268676
Weighted TruePositiveRate: 0.7012
Weighted MatthewsCorrelation: 0.5595862617972727
Weighted FMeasure: 0.6996998564985409
Iteration time: 1120.0
Weighted AreaUnderPRC: 0.595640601355591
Mean absolute error: 0.20009396516808797
Coverage of cases: 70.12
Instances selection time: 1120.0
Test time: 1691.0
Accumulative iteration time: 17116.0
Weighted Recall: 0.7012
Weighted FalsePositiveRate: 0.15055966423118147
Kappa statistic: 0.5512358855575814
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7168083475718311
Weighted AreaUnderROC: 0.7756087327647081
Root mean squared error: 0.445228255905606
Relative absolute error: 44.956583629893856
Root relative squared error: 94.44717567801383
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5605389802923816
Weighted FMeasure: 0.6999934998189542
Iteration time: 1127.0
Weighted AreaUnderPRC: 0.5961981817195615
Mean absolute error: 0.19980703835508476
Coverage of cases: 70.16
Instances selection time: 1127.0
Test time: 1729.0
Accumulative iteration time: 18243.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.15038253447058372
Kappa statistic: 0.5518223168935831
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7177575680638789
Weighted AreaUnderROC: 0.775897297645007
Root mean squared error: 0.4449480171057519
Relative absolute error: 44.89223638470609
Root relative squared error: 94.38772805129531
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5614974979907632
Weighted FMeasure: 0.7002462416901151
Iteration time: 1138.0
Weighted AreaUnderPRC: 0.5967589009149213
Mean absolute error: 0.19952105059869465
Coverage of cases: 70.2
Instances selection time: 1138.0
Test time: 1764.0
Accumulative iteration time: 19381.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.15020540470998597
Kappa statistic: 0.5524087857168777
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7171643885613056
Weighted AreaUnderROC: 0.7743720777610978
Root mean squared error: 0.44645611590230516
Relative absolute error: 45.18686296715846
Root relative squared error: 94.70764411701794
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.5592918442013349
Weighted FMeasure: 0.6981097798993063
Iteration time: 1152.0
Weighted AreaUnderPRC: 0.5949317445878045
Mean absolute error: 0.20083050207626077
Coverage of cases: 70.0
Instances selection time: 1152.0
Test time: 1806.0
Accumulative iteration time: 20533.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.15125584447780416
Kappa statistic: 0.5493780696365895
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.12
Correctly Classified Instances: 69.88
Weighted Precision: 0.7175123419547658
Weighted AreaUnderROC: 0.7734445865083902
Root mean squared error: 0.4473648881196309
Relative absolute error: 45.36212624584741
Root relative squared error: 94.90042381624542
Weighted TruePositiveRate: 0.6988
Weighted MatthewsCorrelation: 0.5584107515326847
Weighted FMeasure: 0.6966648321196872
Iteration time: 1154.0
Weighted AreaUnderPRC: 0.5939987609526095
Mean absolute error: 0.20160944998154504
Coverage of cases: 69.88
Instances selection time: 1154.0
Test time: 1836.0
Accumulative iteration time: 21687.0
Weighted Recall: 0.6988
Weighted FalsePositiveRate: 0.15191082698321953
Kappa statistic: 0.5475445911930186
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.16
Correctly Classified Instances: 69.84
Weighted Precision: 0.7186799907689587
Weighted AreaUnderROC: 0.7731216901770853
Root mean squared error: 0.44767788995368424
Relative absolute error: 45.41798483206998
Root relative squared error: 94.96682153206031
Weighted TruePositiveRate: 0.6984
Weighted MatthewsCorrelation: 0.5587268622677022
Weighted FMeasure: 0.6960606792794298
Iteration time: 1165.0
Weighted AreaUnderPRC: 0.5939021925350106
Mean absolute error: 0.20185771036475644
Coverage of cases: 69.84
Instances selection time: 1165.0
Test time: 1873.0
Accumulative iteration time: 22852.0
Weighted Recall: 0.6984
Weighted FalsePositiveRate: 0.15215661964582952
Kappa statistic: 0.5469168230624866
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.32
Correctly Classified Instances: 69.68
Weighted Precision: 0.7187900951505639
Weighted AreaUnderROC: 0.7718884683185756
Root mean squared error: 0.448879191252453
Relative absolute error: 45.65344644750767
Root relative squared error: 95.2216560204426
Weighted TruePositiveRate: 0.6968
Weighted MatthewsCorrelation: 0.5574023901913673
Weighted FMeasure: 0.6941542171586079
Iteration time: 1171.0
Weighted AreaUnderPRC: 0.5926076524909859
Mean absolute error: 0.20290420643336837
Coverage of cases: 69.68
Instances selection time: 1171.0
Test time: 1911.0
Accumulative iteration time: 24023.0
Weighted Recall: 0.6968
Weighted FalsePositiveRate: 0.15302306336284874
Kappa statistic: 0.5444757528890333
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.64
Correctly Classified Instances: 69.36
Weighted Precision: 0.7182111942008232
Weighted AreaUnderROC: 0.7694391903270595
Root mean squared error: 0.451256555131928
Relative absolute error: 46.1283489096571
Root relative squared error: 95.72597105660002
Weighted TruePositiveRate: 0.6936
Weighted MatthewsCorrelation: 0.5542473788059534
Weighted FMeasure: 0.6904620179121621
Iteration time: 1178.0
Weighted AreaUnderPRC: 0.5898418238195812
Mean absolute error: 0.2050148840429214
Coverage of cases: 69.36
Instances selection time: 1178.0
Test time: 1953.0
Accumulative iteration time: 25201.0
Weighted Recall: 0.6936
Weighted FalsePositiveRate: 0.15472161934588108
Kappa statistic: 0.539613438767866
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.84
Correctly Classified Instances: 69.16
Weighted Precision: 0.7176328757724451
Weighted AreaUnderROC: 0.7679139704431504
Root mean squared error: 0.452741209562609
Relative absolute error: 46.42400813835066
Root relative squared error: 96.04091382129596
Weighted TruePositiveRate: 0.6916
Weighted MatthewsCorrelation: 0.5521261570697725
Weighted FMeasure: 0.688194999279651
Iteration time: 1186.0
Weighted AreaUnderPRC: 0.5880621735018583
Mean absolute error: 0.20632892505933723
Coverage of cases: 69.16
Instances selection time: 1186.0
Test time: 1992.0
Accumulative iteration time: 26387.0
Weighted Recall: 0.6916
Weighted FalsePositiveRate: 0.15577205911369926
Kappa statistic: 0.5365807860503483
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.7180250772545184
Weighted AreaUnderROC: 0.7676013735471472
Root mean squared error: 0.45304845433869034
Relative absolute error: 46.48055832502561
Root relative squared error: 96.10609028069133
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.5519909185792441
Weighted FMeasure: 0.6877251706226776
Iteration time: 1188.0
Weighted AreaUnderPRC: 0.5878624480792584
Mean absolute error: 0.206580259222337
Coverage of cases: 69.12
Instances selection time: 1188.0
Test time: 2032.0
Accumulative iteration time: 27575.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.1559972529057056
Kappa statistic: 0.5359670050901775
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.92
Correctly Classified Instances: 69.08
Weighted Precision: 0.718220797375053
Weighted AreaUnderROC: 0.7672922097962446
Root mean squared error: 0.4533549909662752
Relative absolute error: 46.53724340175841
Root relative squared error: 96.17111651910552
Weighted TruePositiveRate: 0.6908
Weighted MatthewsCorrelation: 0.5517635337739806
Weighted FMeasure: 0.687216367901964
Iteration time: 1191.0
Weighted AreaUnderPRC: 0.5875710182907796
Mean absolute error: 0.206832192896705
Coverage of cases: 69.08
Instances selection time: 1191.0
Test time: 2063.0
Accumulative iteration time: 28766.0
Weighted Recall: 0.6908
Weighted FalsePositiveRate: 0.1562155804075107
Kappa statistic: 0.5353553131308467
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.7143395349588137
Weighted AreaUnderROC: 0.763060045346426
Root mean squared error: 0.45745476478072133
Relative absolute error: 47.371812080538284
Root relative squared error: 97.0408098787633
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5443197490661452
Weighted FMeasure: 0.6814978605543116
Iteration time: 1194.0
Weighted AreaUnderPRC: 0.5821066324618864
Mean absolute error: 0.21054138702461556
Coverage of cases: 68.52
Instances selection time: 1194.0
Test time: 2107.0
Accumulative iteration time: 29960.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15907990930714797
Kappa statistic: 0.5269097577056602
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.7149882803215866
Weighted AreaUnderROC: 0.7630497459111242
Root mean squared error: 0.45746710334587415
Relative absolute error: 47.368955785514174
Root relative squared error: 97.04342728369019
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5447604346874491
Weighted FMeasure: 0.6813705315816265
Iteration time: 1213.0
Weighted AreaUnderPRC: 0.5822526611151037
Mean absolute error: 0.21052869238006397
Coverage of cases: 68.52
Instances selection time: 1212.0
Test time: 2143.0
Accumulative iteration time: 31173.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15910050817775162
Kappa statistic: 0.526896789198194
Training time: 1.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.72
Correctly Classified Instances: 68.28
Weighted Precision: 0.7137582897542482
Weighted AreaUnderROC: 0.7612290948567149
Root mean squared error: 0.45921954620616107
Relative absolute error: 47.72520775623412
Root relative squared error: 97.41517655273545
Weighted TruePositiveRate: 0.6828
Weighted MatthewsCorrelation: 0.5418716607909583
Weighted FMeasure: 0.6787666836958107
Iteration time: 1201.0
Weighted AreaUnderPRC: 0.5800225353449495
Mean absolute error: 0.21211203447215266
Coverage of cases: 68.28
Instances selection time: 1201.0
Test time: 2181.0
Accumulative iteration time: 32374.0
Weighted Recall: 0.6828
Weighted FalsePositiveRate: 0.16034181028657005
Kappa statistic: 0.5232681226647351
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.76
Correctly Classified Instances: 68.24
Weighted Precision: 0.7136856231884058
Weighted AreaUnderROC: 0.7609233642509131
Root mean squared error: 0.4595205036709609
Relative absolute error: 47.782411604713765
Root relative squared error: 97.47901927199806
Weighted TruePositiveRate: 0.6824
Weighted MatthewsCorrelation: 0.541485307935559
Weighted FMeasure: 0.678258408302334
Iteration time: 1205.0
Weighted AreaUnderPRC: 0.5796838990250329
Mean absolute error: 0.21236627379872886
Coverage of cases: 68.24
Instances selection time: 1205.0
Test time: 2215.0
Accumulative iteration time: 33579.0
Weighted Recall: 0.6824
Weighted FalsePositiveRate: 0.16055327149817392
Kappa statistic: 0.5226604047406916
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.84
Correctly Classified Instances: 68.16
Weighted Precision: 0.7136180297936678
Weighted AreaUnderROC: 0.7603119030393091
Root mean squared error: 0.46010998686119264
Relative absolute error: 47.8995547640247
Root relative squared error: 97.60406754036056
Weighted TruePositiveRate: 0.6816
Weighted MatthewsCorrelation: 0.5407461395438843
Weighted FMeasure: 0.6772648638921033
Iteration time: 1206.0
Weighted AreaUnderPRC: 0.579012851959652
Mean absolute error: 0.21288691006233298
Coverage of cases: 68.16
Instances selection time: 1206.0
Test time: 2257.0
Accumulative iteration time: 34785.0
Weighted Recall: 0.6816
Weighted FalsePositiveRate: 0.16097619392138168
Kappa statistic: 0.5214449189113665
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7112648347642297
Weighted AreaUnderROC: 0.7572820621420946
Root mean squared error: 0.4630018733404657
Relative absolute error: 48.495538057741896
Root relative squared error: 98.21752930233525
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5357397188154411
Weighted FMeasure: 0.6728636648552094
Iteration time: 1209.0
Weighted AreaUnderPRC: 0.5752601079444297
Mean absolute error: 0.2155357247010761
Coverage of cases: 67.76
Instances selection time: 1209.0
Test time: 2300.0
Accumulative iteration time: 35994.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16303587571581074
Kappa statistic: 0.5154019217981354
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.7109756533388435
Weighted AreaUnderROC: 0.7566740340755912
Root mean squared error: 0.46358639041607885
Relative absolute error: 48.612897678416935
Root relative squared error: 98.34152409870087
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5348509583097419
Weighted FMeasure: 0.6719020518201283
Iteration time: 1205.0
Weighted AreaUnderPRC: 0.5745444136649257
Mean absolute error: 0.21605732301518737
Coverage of cases: 67.68
Instances selection time: 1205.0
Test time: 2327.0
Accumulative iteration time: 37199.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.1634519318488173
Kappa statistic: 0.5141905657250861
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.44
Correctly Classified Instances: 67.56
Weighted Precision: 0.7079291261978429
Weighted AreaUnderROC: 0.7557946068542922
Root mean squared error: 0.46445630535122545
Relative absolute error: 48.79019442096294
Root relative squared error: 98.52606092361016
Weighted TruePositiveRate: 0.6756
Weighted MatthewsCorrelation: 0.5318994248891711
Weighted FMeasure: 0.6712526156698017
Iteration time: 1203.0
Weighted AreaUnderPRC: 0.5729392388024053
Mean absolute error: 0.21684530853761408
Coverage of cases: 67.56
Instances selection time: 1203.0
Test time: 2365.0
Accumulative iteration time: 38402.0
Weighted Recall: 0.6756
Weighted FalsePositiveRate: 0.16401078629141547
Kappa statistic: 0.5124135541095902
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.28
Correctly Classified Instances: 67.72
Weighted Precision: 0.7090574374628391
Weighted AreaUnderROC: 0.7570037966970975
Root mean squared error: 0.4633192458945335
Relative absolute error: 48.548628428928055
Root relative squared error: 98.28485418787841
Weighted TruePositiveRate: 0.6772
Weighted MatthewsCorrelation: 0.5340313186001507
Weighted FMeasure: 0.6730178286426781
Iteration time: 1205.0
Weighted AreaUnderPRC: 0.5744736454244519
Mean absolute error: 0.21577168190634793
Coverage of cases: 67.72
Instances selection time: 1205.0
Test time: 2402.0
Accumulative iteration time: 39607.0
Weighted Recall: 0.6772
Weighted FalsePositiveRate: 0.16319240660580484
Kappa statistic: 0.5148272869310223
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.7146519637204221
Weighted AreaUnderROC: 0.7624382846995202
Root mean squared error: 0.4581323271821254
Relative absolute error: 47.46917416189507
Root relative squared error: 97.18454256937625
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5438198869536285
Weighted FMeasure: 0.680973211886697
Iteration time: 1206.0
Weighted AreaUnderPRC: 0.5816796869719988
Mean absolute error: 0.2109741073862013
Coverage of cases: 68.44
Instances selection time: 1206.0
Test time: 2440.0
Accumulative iteration time: 40813.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.15952343060095941
Kappa statistic: 0.5256858258332779
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.7165047985510383
Weighted AreaUnderROC: 0.765169261281134
Root mean squared error: 0.45552089216000924
Relative absolute error: 46.9283990345931
Root relative squared error: 96.63057354554634
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.5480741926993185
Weighted FMeasure: 0.6851721345956037
Iteration time: 1206.0
Weighted AreaUnderPRC: 0.5851432389666618
Mean absolute error: 0.20857066237597033
Coverage of cases: 68.8
Instances selection time: 1206.0
Test time: 2493.0
Accumulative iteration time: 42019.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.1576614774377318
Kappa statistic: 0.531132670459122
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.7164352565658033
Weighted AreaUnderROC: 0.7676254055628515
Root mean squared error: 0.453187517474471
Relative absolute error: 46.447505938241825
Root relative squared error: 96.13559002658842
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.550833376463434
Weighted FMeasure: 0.6889904658226171
Iteration time: 1215.0
Weighted AreaUnderPRC: 0.5877246427375723
Mean absolute error: 0.2064333597255202
Coverage of cases: 69.12
Instances selection time: 1215.0
Test time: 2520.0
Accumulative iteration time: 43234.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.15594918887429704
Kappa statistic: 0.5360030388194764
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.48
Correctly Classified Instances: 69.52
Weighted Precision: 0.7197278175606625
Weighted AreaUnderROC: 0.7706483801698647
Root mean squared error: 0.45025112837401254
Relative absolute error: 45.84692127825274
Root relative squared error: 95.51268783304748
Weighted TruePositiveRate: 0.6952
Weighted MatthewsCorrelation: 0.5561956507567558
Weighted FMeasure: 0.6934522529968056
Iteration time: 1208.0
Weighted AreaUnderPRC: 0.5919823782132595
Mean absolute error: 0.20376409457001315
Coverage of cases: 69.52
Instances selection time: 1207.0
Test time: 2559.0
Accumulative iteration time: 44442.0
Weighted Recall: 0.6952
Weighted FalsePositiveRate: 0.1539032396602704
Kappa statistic: 0.5420447401855141
Training time: 1.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.44
Correctly Classified Instances: 69.56
Weighted Precision: 0.7188858903931551
Weighted AreaUnderROC: 0.7709747096462705
Root mean squared error: 0.4499636450333395
Relative absolute error: 45.785111281657194
Root relative squared error: 95.45170340714705
Weighted TruePositiveRate: 0.6956
Weighted MatthewsCorrelation: 0.5559437144876869
Weighted FMeasure: 0.6940869468882386
Iteration time: 1202.0
Weighted AreaUnderPRC: 0.592125616814355
Mean absolute error: 0.20348938347403295
Coverage of cases: 69.56
Instances selection time: 1202.0
Test time: 2596.0
Accumulative iteration time: 45644.0
Weighted Recall: 0.6956
Weighted FalsePositiveRate: 0.15365058070745918
Kappa statistic: 0.5426770639026173
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.68
Correctly Classified Instances: 69.32
Weighted Precision: 0.7154069395465487
Weighted AreaUnderROC: 0.7692055557683705
Root mean squared error: 0.4517418306610565
Relative absolute error: 46.142403628119276
Root relative squared error: 95.8289135418172
Weighted TruePositiveRate: 0.6932
Weighted MatthewsCorrelation: 0.5517627828610173
Weighted FMeasure: 0.6912438730922337
Iteration time: 1201.0
Weighted AreaUnderPRC: 0.5889582209824681
Mean absolute error: 0.20507734945830886
Coverage of cases: 69.32
Instances selection time: 1201.0
Test time: 2635.0
Accumulative iteration time: 46845.0
Weighted Recall: 0.6932
Weighted FalsePositiveRate: 0.15478888846325922
Kappa statistic: 0.5390924146750091
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.7108814981314102
Weighted AreaUnderROC: 0.7662375114829669
Root mean squared error: 0.454684796848263
Relative absolute error: 46.73924050632829
Root relative squared error: 96.45321094615012
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5453688208659729
Weighted FMeasure: 0.6867178396721145
Iteration time: 1196.0
Weighted AreaUnderPRC: 0.584278518683227
Mean absolute error: 0.20772995780590445
Coverage of cases: 68.92
Instances selection time: 1196.0
Test time: 2670.0
Accumulative iteration time: 48041.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.1567249770340662
Kappa statistic: 0.5331087768819983
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.7117344491481392
Weighted AreaUnderROC: 0.7677764639472785
Root mean squared error: 0.4532269024506328
Relative absolute error: 46.43815113719815
Root relative squared error: 96.14394484170467
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.5475897042668474
Weighted FMeasure: 0.6886986248173591
Iteration time: 1200.0
Weighted AreaUnderPRC: 0.5860317855454655
Mean absolute error: 0.20639178283199275
Coverage of cases: 69.12
Instances selection time: 1200.0
Test time: 2710.0
Accumulative iteration time: 49241.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.15564707210544312
Kappa statistic: 0.5361555902384065
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7118517069415938
Weighted AreaUnderROC: 0.7684016577392847
Root mean squared error: 0.4526466286734669
Relative absolute error: 46.31670281995639
Root relative squared error: 96.02085018487104
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5483579112859939
Weighted FMeasure: 0.6893597074236589
Iteration time: 1186.0
Weighted AreaUnderPRC: 0.5866109023815762
Mean absolute error: 0.20585201253314048
Coverage of cases: 69.2
Instances selection time: 1186.0
Test time: 2743.0
Accumulative iteration time: 50427.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.15519668452143043
Kappa statistic: 0.5373826093351142
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.7139495100174376
Weighted AreaUnderROC: 0.7705246062543952
Root mean squared error: 0.45059140582529716
Relative absolute error: 45.89593727726275
Root relative squared error: 95.58487158103394
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.5521840366520994
Weighted FMeasure: 0.6919315171419428
Iteration time: 1182.0
Weighted AreaUnderPRC: 0.5892860495275197
Mean absolute error: 0.20398194345450207
Coverage of cases: 69.48
Instances selection time: 1182.0
Test time: 2778.0
Accumulative iteration time: 51609.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.15375078749120935
Kappa statistic: 0.5416091525332491
Training time: 0.0
		
Time end:Sun Oct 08 11.16.09 EEST 2017