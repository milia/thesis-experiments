Sun Oct 08 12.18.32 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.18.32 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.88
Correctly Classified Instances: 83.12
Weighted Precision: 0.8325268140432943
Weighted AreaUnderROC: 0.9140321744962854
Root mean squared error: 0.3341323756304475
Relative absolute error: 58.459999999998715
Root relative squared error: 70.88018058667788
Weighted TruePositiveRate: 0.8312
Weighted MatthewsCorrelation: 0.7476452341186224
Weighted FMeasure: 0.8306612829677302
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7748539652138313
Mean absolute error: 0.25982222222221774
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8312
Weighted FalsePositiveRate: 0.08418304170873775
Kappa statistic: 0.7468869493951439
Training time: 57.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8210242227928899
Weighted AreaUnderROC: 0.9067127611087541
Root mean squared error: 0.34119615298947464
Relative absolute error: 59.539999999999075
Root relative squared error: 72.3786340480859
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.7206516266294317
Weighted FMeasure: 0.8094511223684553
Iteration time: 86.0
Weighted AreaUnderPRC: 0.7576059916155368
Mean absolute error: 0.26462222222221937
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 149.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.09606885486903093
Kappa statistic: 0.714109718443719
Training time: 81.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.48
Correctly Classified Instances: 78.52
Weighted Precision: 0.8134544028357135
Weighted AreaUnderROC: 0.8943262219286182
Root mean squared error: 0.349051625174614
Relative absolute error: 60.75999999999919
Root relative squared error: 74.0450313435462
Weighted TruePositiveRate: 0.7852
Weighted MatthewsCorrelation: 0.6939183659285288
Weighted FMeasure: 0.7841771635963977
Iteration time: 97.0
Weighted AreaUnderPRC: 0.7363875229448571
Mean absolute error: 0.2700444444444421
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 246.0
Weighted Recall: 0.7852
Weighted FalsePositiveRate: 0.10881006410223977
Kappa statistic: 0.6772721721830488
Training time: 92.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8242729697314723
Weighted AreaUnderROC: 0.9002402203428664
Root mean squared error: 0.3446952491909588
Relative absolute error: 60.079999999999565
Root relative squared error: 73.12090444371397
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.7115219010563784
Weighted FMeasure: 0.7958076516986567
Iteration time: 147.0
Weighted AreaUnderPRC: 0.7482139767500948
Mean absolute error: 0.26702222222222155
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 393.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.10164784806851256
Kappa statistic: 0.6978339439530625
Training time: 143.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.824424347203769
Weighted AreaUnderROC: 0.8984163107639579
Root mean squared error: 0.34551089386266864
Relative absolute error: 60.19999999999969
Root relative squared error: 73.29392880723537
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.7089159929158554
Weighted FMeasure: 0.7905276503609435
Iteration time: 150.0
Weighted AreaUnderPRC: 0.7444823394862191
Mean absolute error: 0.26755555555555544
Coverage of cases: 99.96
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 543.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10261449856928195
Kappa statistic: 0.6943063384409599
Training time: 147.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 17.0
Correctly Classified Instances: 83.0
Weighted Precision: 0.8429481781796679
Weighted AreaUnderROC: 0.9151717267129769
Root mean squared error: 0.3345311810745202
Relative absolute error: 58.519999999999435
Root relative squared error: 70.96477999684126
Weighted TruePositiveRate: 0.83
Weighted MatthewsCorrelation: 0.7517154576298694
Weighted FMeasure: 0.8284224528859626
Iteration time: 223.0
Weighted AreaUnderPRC: 0.7790903929349329
Mean absolute error: 0.2600888888888876
Coverage of cases: 99.96
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 766.0
Weighted Recall: 0.83
Weighted FalsePositiveRate: 0.08550405436367198
Kappa statistic: 0.7448631037469463
Training time: 221.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.850686719808646
Weighted AreaUnderROC: 0.9245318024156332
Root mean squared error: 0.32795663669997227
Relative absolute error: 57.539999999999175
Root relative squared error: 69.57010852370483
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.774874101546105
Weighted FMeasure: 0.8496181127997438
Iteration time: 242.0
Weighted AreaUnderPRC: 0.7978362572952572
Mean absolute error: 0.25573333333333087
Coverage of cases: 99.96
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 1008.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07534157741091711
Kappa statistic: 0.7743735926852878
Training time: 241.0
		
Time end:Sun Oct 08 12.18.33 EEST 2017