Sun Oct 08 10.39.13 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.39.13 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 35.00000000000113
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.717914419585461
Weighted AreaUnderROC: 0.7812722407589443
Root mean squared error: 0.431107154368824
Relative absolute error: 42.97227272727304
Root relative squared error: 91.45163768166914
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5774080631255885
Weighted FMeasure: 0.7176436425027155
Iteration time: 374.0
Weighted AreaUnderPRC: 0.6028609393698968
Mean absolute error: 0.19098787878788104
Coverage of cases: 72.68
Instances selection time: 28.0
Test time: 14.0
Accumulative iteration time: 374.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14081634335580587
Kappa statistic: 0.5776899351514103
Training time: 346.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 35.97333333333452
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7241911367372347
Weighted AreaUnderROC: 0.7930216609448008
Root mean squared error: 0.4237829473269757
Relative absolute error: 41.91354545454569
Root relative squared error: 89.89793874183759
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5868023373414061
Weighted FMeasure: 0.7229228947111492
Iteration time: 279.0
Weighted AreaUnderPRC: 0.620071973459688
Mean absolute error: 0.1862824242424262
Coverage of cases: 74.76
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 653.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13765931014071922
Kappa statistic: 0.5867673426969015
Training time: 269.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 35.21333333333447
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7184301375084641
Weighted AreaUnderROC: 0.7865411315452256
Root mean squared error: 0.4286475408610607
Relative absolute error: 42.68490993788839
Root relative squared error: 90.92987486453792
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.5773568428491336
Weighted FMeasure: 0.7170809981918759
Iteration time: 273.0
Weighted AreaUnderPRC: 0.6125073709644993
Mean absolute error: 0.18971071083506041
Coverage of cases: 73.44
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 926.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.14103505924752352
Kappa statistic: 0.5770860037902692
Training time: 269.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 36.74666666666782
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7231320671460718
Weighted AreaUnderROC: 0.7918885432730642
Root mean squared error: 0.42418002457357346
Relative absolute error: 42.18271428571436
Root relative squared error: 89.98217154595484
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5843906618000846
Weighted FMeasure: 0.7222315994081249
Iteration time: 262.0
Weighted AreaUnderPRC: 0.6206410867943852
Mean absolute error: 0.18747873015873134
Coverage of cases: 75.52
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 1188.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13865866255058398
Kappa statistic: 0.5842627929477532
Training time: 256.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 37.133333333334406
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7136069520513715
Weighted AreaUnderROC: 0.778447453933149
Root mean squared error: 0.433464062728959
Relative absolute error: 43.96623076923091
Root relative squared error: 91.95161344689515
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5698122125446492
Weighted FMeasure: 0.7117477927481322
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6016875981288529
Mean absolute error: 0.19540547008547166
Coverage of cases: 74.24
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 1385.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.143571281609554
Kappa statistic: 0.5693291323529552
Training time: 185.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 37.00000000000111
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7329917358232318
Weighted AreaUnderROC: 0.7945999955038927
Root mean squared error: 0.41622359484297594
Relative absolute error: 41.33133542319832
Root relative squared error: 88.29435792099292
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5993438933756886
Weighted FMeasure: 0.7323757847643562
Iteration time: 190.0
Weighted AreaUnderPRC: 0.6348712086743358
Mean absolute error: 0.1836948241031045
Coverage of cases: 76.08
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 1575.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.1336138016371388
Kappa statistic: 0.5992355237631736
Training time: 183.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 37.253333333334474
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7198859867302504
Weighted AreaUnderROC: 0.783690885551408
Root mean squared error: 0.42689652189590827
Relative absolute error: 43.5364782803412
Root relative squared error: 90.55842764926425
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5792483582398988
Weighted FMeasure: 0.718629989442484
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6175053616689365
Mean absolute error: 0.19349545902373957
Coverage of cases: 74.8
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 1724.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.14036777498873276
Kappa statistic: 0.5788817979995564
Training time: 144.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 35.9600000000011
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.729303125725922
Weighted AreaUnderROC: 0.7879027334873713
Root mean squared error: 0.4181892718743132
Relative absolute error: 41.687229110905974
Root relative squared error: 88.71134098853727
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5938832997973167
Weighted FMeasure: 0.7288360291521958
Iteration time: 162.0
Weighted AreaUnderPRC: 0.6249121573896408
Mean absolute error: 0.18527657382624965
Coverage of cases: 74.84
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 1886.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.1353683978908878
Kappa statistic: 0.5938638770894114
Training time: 154.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 36.373333333334436
Incorrectly Classified Instances: 27.76
Correctly Classified Instances: 72.24
Weighted Precision: 0.7223003565054442
Weighted AreaUnderROC: 0.7935446926641964
Root mean squared error: 0.4208671379060196
Relative absolute error: 42.51322911090596
Root relative squared error: 89.27940215757589
Weighted TruePositiveRate: 0.7224
Weighted MatthewsCorrelation: 0.583572119584419
Weighted FMeasure: 0.7219635392228051
Iteration time: 277.0
Weighted AreaUnderPRC: 0.6309090036468485
Mean absolute error: 0.1889476849373607
Coverage of cases: 75.12
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2163.0
Weighted Recall: 0.7224
Weighted FalsePositiveRate: 0.13880745877272851
Kappa statistic: 0.5836564894875064
Training time: 270.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 35.82666666666775
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7255877876262758
Weighted AreaUnderROC: 0.7815350399736044
Root mean squared error: 0.4225417451211509
Relative absolute error: 42.307229110905986
Root relative squared error: 89.63463999286887
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5884251643246714
Weighted FMeasure: 0.725185774704108
Iteration time: 308.0
Weighted AreaUnderPRC: 0.6162442616591821
Mean absolute error: 0.18803212938180525
Coverage of cases: 74.16
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 2471.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13718371699550877
Kappa statistic: 0.5884629487073658
Training time: 303.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 35.34666666666777
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.730045464747415
Weighted AreaUnderROC: 0.7802984447623136
Root mean squared error: 0.4214102958888626
Relative absolute error: 41.724229110905966
Root relative squared error: 89.39462336545304
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.5950091315619306
Weighted FMeasure: 0.7297451713432056
Iteration time: 337.0
Weighted AreaUnderPRC: 0.6143089279854882
Mean absolute error: 0.18544101827069404
Coverage of cases: 73.72
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 2808.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.13500834242352977
Kappa statistic: 0.5950411438197879
Training time: 329.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 38.13333333333418
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7342015693181568
Weighted AreaUnderROC: 0.8071125989738921
Root mean squared error: 0.413570227317926
Relative absolute error: 40.61718512755298
Root relative squared error: 87.73149367001001
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.601525419585281
Weighted FMeasure: 0.7339502822607434
Iteration time: 179.0
Weighted AreaUnderPRC: 0.6506351703730394
Mean absolute error: 0.18052082278912518
Coverage of cases: 77.48
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 2987.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.1327764545930043
Kappa statistic: 0.6016704883903745
Training time: 171.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 37.053333333334294
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7097207486668741
Weighted AreaUnderROC: 0.7931226133491661
Root mean squared error: 0.4297255965776903
Relative absolute error: 43.91230054644782
Root relative squared error: 91.15856501685563
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.5649156834653467
Weighted FMeasure: 0.709428441969474
Iteration time: 247.0
Weighted AreaUnderPRC: 0.6271000272147883
Mean absolute error: 0.19516578020643566
Coverage of cases: 75.24
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3234.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.1449828940554619
Kappa statistic: 0.5650792599556655
Training time: 245.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 39.60000000000108
Incorrectly Classified Instances: 27.96
Correctly Classified Instances: 72.04
Weighted Precision: 0.72017870339498
Weighted AreaUnderROC: 0.8020028176513958
Root mean squared error: 0.42235039465874125
Relative absolute error: 43.27790159174397
Root relative squared error: 89.59404843000294
Weighted TruePositiveRate: 0.7204
Weighted MatthewsCorrelation: 0.580517782921546
Weighted FMeasure: 0.7199213303277702
Iteration time: 361.0
Weighted AreaUnderPRC: 0.637723089540226
Mean absolute error: 0.19234622929664077
Coverage of cases: 77.56
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 3595.0
Weighted Recall: 0.7204
Weighted FalsePositiveRate: 0.1397805066816011
Kappa statistic: 0.5806758138128627
Training time: 354.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 39.666666666667744
Incorrectly Classified Instances: 27.96
Correctly Classified Instances: 72.04
Weighted Precision: 0.7200629398234379
Weighted AreaUnderROC: 0.8049488069374232
Root mean squared error: 0.4206754015550705
Relative absolute error: 42.872555284800185
Root relative squared error: 89.23872873538906
Weighted TruePositiveRate: 0.7204
Weighted MatthewsCorrelation: 0.5803411514024758
Weighted FMeasure: 0.7200974036093069
Iteration time: 377.0
Weighted AreaUnderPRC: 0.6458465507925777
Mean absolute error: 0.19054469015466838
Coverage of cases: 77.56
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 3972.0
Weighted Recall: 0.7204
Weighted FalsePositiveRate: 0.13990616197567046
Kappa statistic: 0.5806285172608264
Training time: 370.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 36.18666666666767
Incorrectly Classified Instances: 27.52
Correctly Classified Instances: 72.48
Weighted Precision: 0.7249383717701002
Weighted AreaUnderROC: 0.8025128023763475
Root mean squared error: 0.4209739396634748
Relative absolute error: 41.96893225806427
Root relative squared error: 89.30205823165765
Weighted TruePositiveRate: 0.7248
Weighted MatthewsCorrelation: 0.5870183978734693
Weighted FMeasure: 0.724820479212246
Iteration time: 197.0
Weighted AreaUnderPRC: 0.640210707454067
Mean absolute error: 0.18652858781361986
Coverage of cases: 74.88
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4169.0
Weighted Recall: 0.7248
Weighted FalsePositiveRate: 0.13786310702157165
Kappa statistic: 0.5871638685817783
Training time: 194.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 36.80000000000103
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7274855330700407
Weighted AreaUnderROC: 0.7981515329940692
Root mean squared error: 0.4192953755404385
Relative absolute error: 42.08734159872361
Root relative squared error: 88.94598100944103
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5907169153006003
Weighted FMeasure: 0.7271973113345073
Iteration time: 261.0
Weighted AreaUnderPRC: 0.6348496114128648
Mean absolute error: 0.1870548515498836
Coverage of cases: 75.8
Instances selection time: 9.0
Test time: 10.0
Accumulative iteration time: 4430.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13665006196913906
Kappa statistic: 0.5907628085240386
Training time: 252.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 36.573333333334375
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7279669804855455
Weighted AreaUnderROC: 0.7991980787608988
Root mean squared error: 0.42063354384783436
Relative absolute error: 41.737713852331915
Root relative squared error: 89.22984937479958
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5914382347263156
Weighted FMeasure: 0.7275701234379759
Iteration time: 161.0
Weighted AreaUnderPRC: 0.6392806842347464
Mean absolute error: 0.18550095045480938
Coverage of cases: 76.16
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4591.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13634799061953523
Kappa statistic: 0.5913885588796487
Training time: 159.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 36.66666666666777
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.7299737133046106
Weighted AreaUnderROC: 0.7999282480856672
Root mean squared error: 0.41505444997985735
Relative absolute error: 41.433274546477705
Root relative squared error: 88.04634484272273
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5943764989636772
Weighted FMeasure: 0.7297210048981182
Iteration time: 158.0
Weighted AreaUnderPRC: 0.6333747948441061
Mean absolute error: 0.1841478868732351
Coverage of cases: 76.28
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4749.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.13540375837241428
Kappa statistic: 0.5943611884785137
Training time: 155.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 35.8933333333345
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.712283650964783
Weighted AreaUnderROC: 0.7859736107287743
Root mean squared error: 0.43023807679817305
Relative absolute error: 43.726557263760434
Root relative squared error: 91.26727848859382
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5675091268653719
Weighted FMeasure: 0.7115805726941388
Iteration time: 168.0
Weighted AreaUnderPRC: 0.6170840694298588
Mean absolute error: 0.19434025450560286
Coverage of cases: 74.48
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 4917.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14449559293079084
Kappa statistic: 0.5673357580133498
Training time: 165.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 36.986666666667766
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.726106095005833
Weighted AreaUnderROC: 0.7910161072064895
Root mean squared error: 0.4219033378564136
Relative absolute error: 42.086838649041766
Root relative squared error: 89.49921336105251
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5888167628649966
Weighted FMeasure: 0.726046210082421
Iteration time: 166.0
Weighted AreaUnderPRC: 0.6247054692406666
Mean absolute error: 0.1870526162179643
Coverage of cases: 75.36
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5083.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.13723844834122742
Kappa statistic: 0.5889658019547227
Training time: 164.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 35.773333333334385
Incorrectly Classified Instances: 28.92
Correctly Classified Instances: 71.08
Weighted Precision: 0.7107764013874993
Weighted AreaUnderROC: 0.7915210805458764
Root mean squared error: 0.4322879143861033
Relative absolute error: 43.6471839410596
Root relative squared error: 91.70211470622078
Weighted TruePositiveRate: 0.7108
Weighted MatthewsCorrelation: 0.5660978654509707
Weighted FMeasure: 0.7104557538416809
Iteration time: 172.0
Weighted AreaUnderPRC: 0.6209985117013844
Mean absolute error: 0.19398748418248804
Coverage of cases: 74.92
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5255.0
Weighted Recall: 0.7108
Weighted FalsePositiveRate: 0.14474436205325278
Kappa statistic: 0.5662170042934317
Training time: 170.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 35.760000000001085
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7178743065537055
Weighted AreaUnderROC: 0.7934911077364905
Root mean squared error: 0.4283698183743044
Relative absolute error: 42.870833291708934
Root relative squared error: 90.87096102843589
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.5767557480971267
Weighted FMeasure: 0.7177734726817847
Iteration time: 172.0
Weighted AreaUnderPRC: 0.6257235068957187
Mean absolute error: 0.1905370368520406
Coverage of cases: 74.92
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 5427.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.14120905659434677
Kappa statistic: 0.5770062264683463
Training time: 169.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 35.45333333333443
Incorrectly Classified Instances: 26.96
Correctly Classified Instances: 73.04
Weighted Precision: 0.7304395182019565
Weighted AreaUnderROC: 0.8125994874073087
Root mean squared error: 0.4203067387678467
Relative absolute error: 40.758058210331946
Root relative squared error: 89.16052354834393
Weighted TruePositiveRate: 0.7304
Weighted MatthewsCorrelation: 0.595386191222525
Weighted FMeasure: 0.7304163534852067
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6465184883456317
Mean absolute error: 0.18114692537925392
Coverage of cases: 75.44
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 5608.0
Weighted Recall: 0.7304
Weighted FalsePositiveRate: 0.13503346578480338
Kappa statistic: 0.5955735343320868
Training time: 178.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 35.84000000000105
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7210627458406786
Weighted AreaUnderROC: 0.8182030434552414
Root mean squared error: 0.4254929213672034
Relative absolute error: 42.11545415368954
Root relative squared error: 90.26067901368698
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5802278982908139
Weighted FMeasure: 0.7201505457354828
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6555704234809012
Mean absolute error: 0.18717979623862108
Coverage of cases: 75.92
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5789.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.14034150079265792
Kappa statistic: 0.5799257308692177
Training time: 179.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 35.70666666666779
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7226689170139751
Weighted AreaUnderROC: 0.8116815201345595
Root mean squared error: 0.4232023470362437
Relative absolute error: 41.89750389610399
Root relative squared error: 89.77477482101695
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.5830115038997703
Weighted FMeasure: 0.7221909209236407
Iteration time: 193.0
Weighted AreaUnderPRC: 0.6457856530613127
Mean absolute error: 0.18621112842712972
Coverage of cases: 75.72
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5982.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.1393314660162411
Kappa statistic: 0.5829360724411837
Training time: 191.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 36.42666666666771
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.7307184305333141
Weighted AreaUnderROC: 0.812319326672286
Root mean squared error: 0.41821753249766636
Relative absolute error: 40.840731601731555
Root relative squared error: 88.71733597206135
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.5960701902179868
Weighted FMeasure: 0.7306198891876057
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6480277371540115
Mean absolute error: 0.18151436267436333
Coverage of cases: 76.24
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6179.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.13468297461904072
Kappa statistic: 0.5962210611094525
Training time: 195.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 35.240000000001004
Incorrectly Classified Instances: 26.96
Correctly Classified Instances: 73.04
Weighted Precision: 0.7306234494369296
Weighted AreaUnderROC: 0.8091919572793982
Root mean squared error: 0.41946667547745825
Relative absolute error: 40.82068831168831
Root relative squared error: 88.98231921356607
Weighted TruePositiveRate: 0.7304
Weighted MatthewsCorrelation: 0.5955089255192123
Weighted FMeasure: 0.7304082300686935
Iteration time: 204.0
Weighted AreaUnderPRC: 0.642932849260241
Mean absolute error: 0.18142528138528222
Coverage of cases: 75.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6383.0
Weighted Recall: 0.7304
Weighted FalsePositiveRate: 0.13501187347934093
Kappa statistic: 0.5955717870878673
Training time: 202.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 36.22666666666771
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7244242733203695
Weighted AreaUnderROC: 0.7967229379574661
Root mean squared error: 0.42293760351888404
Relative absolute error: 41.97142590406026
Root relative squared error: 89.71861424009688
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5865137367942891
Weighted FMeasure: 0.7243005233238786
Iteration time: 205.0
Weighted AreaUnderPRC: 0.631755128328327
Mean absolute error: 0.18653967068471314
Coverage of cases: 75.48
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 6588.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13787846556087935
Kappa statistic: 0.5866156093945893
Training time: 202.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 37.786666666667685
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7414528792733669
Weighted AreaUnderROC: 0.8119609934638925
Root mean squared error: 0.4062633934535783
Relative absolute error: 40.32927277555726
Root relative squared error: 86.1814801376649
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.6128100525260363
Weighted FMeasure: 0.741182491476088
Iteration time: 208.0
Weighted AreaUnderPRC: 0.6558397926657473
Mean absolute error: 0.17924121233581086
Coverage of cases: 78.36
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6796.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.12899686463480772
Kappa statistic: 0.6130844404517158
Training time: 206.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.18666666666764
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7403979844676667
Weighted AreaUnderROC: 0.814332122899276
Root mean squared error: 0.40464611782708554
Relative absolute error: 39.552979036775724
Root relative squared error: 85.83840416890267
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6104114408490441
Weighted FMeasure: 0.7403554319715785
Iteration time: 222.0
Weighted AreaUnderPRC: 0.6604616022899147
Mean absolute error: 0.17579101794122626
Coverage of cases: 78.32
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7018.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.12997796424543703
Kappa statistic: 0.6105941433359157
Training time: 220.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 38.30666666666766
Incorrectly Classified Instances: 25.88
Correctly Classified Instances: 74.12
Weighted Precision: 0.7406538824105193
Weighted AreaUnderROC: 0.8242384733155497
Root mean squared error: 0.4005718928494396
Relative absolute error: 39.86340582567319
Root relative squared error: 84.97413053597076
Weighted TruePositiveRate: 0.7412
Weighted MatthewsCorrelation: 0.6116346421373154
Weighted FMeasure: 0.7403048221814259
Iteration time: 217.0
Weighted AreaUnderPRC: 0.6746035350229175
Mean absolute error: 0.17717069255854834
Coverage of cases: 80.12
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7235.0
Weighted Recall: 0.7412
Weighted FalsePositiveRate: 0.1293865280866108
Kappa statistic: 0.6118914073357442
Training time: 215.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 36.40000000000094
Incorrectly Classified Instances: 26.16
Correctly Classified Instances: 73.84
Weighted Precision: 0.7380506535702198
Weighted AreaUnderROC: 0.8095848402497039
Root mean squared error: 0.41176350168612497
Relative absolute error: 40.0142852450602
Root relative squared error: 87.348229286213
Weighted TruePositiveRate: 0.7384
Weighted MatthewsCorrelation: 0.6073027830271589
Weighted FMeasure: 0.7381385961768906
Iteration time: 246.0
Weighted AreaUnderPRC: 0.6534008132337289
Mean absolute error: 0.17784126775582393
Coverage of cases: 77.12
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7481.0
Weighted Recall: 0.7384
Weighted FalsePositiveRate: 0.1309293420854421
Kappa statistic: 0.6076229148217743
Training time: 244.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 37.06666666666773
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7369714590844978
Weighted AreaUnderROC: 0.8099970352936853
Root mean squared error: 0.4083283292323778
Relative absolute error: 40.51284397168144
Root relative squared error: 86.61951916523604
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6061320398763669
Weighted FMeasure: 0.7368268994678556
Iteration time: 272.0
Weighted AreaUnderPRC: 0.6518286384166747
Mean absolute error: 0.18005708431858503
Coverage of cases: 77.6
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 7753.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13123653485410075
Kappa statistic: 0.60647454946135
Training time: 269.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 35.58666666666762
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7367004231707016
Weighted AreaUnderROC: 0.8009098666839046
Root mean squared error: 0.4130261291773994
Relative absolute error: 40.10333087518888
Root relative squared error: 87.61607302457082
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6054631227919995
Weighted FMeasure: 0.7368066879896867
Iteration time: 305.0
Weighted AreaUnderPRC: 0.6353870488483399
Mean absolute error: 0.1782370261119514
Coverage of cases: 76.72
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 8058.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.13151201198322318
Kappa statistic: 0.6058358847010568
Training time: 299.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 35.40000000000095
Incorrectly Classified Instances: 25.92
Correctly Classified Instances: 74.08
Weighted Precision: 0.7403189146147517
Weighted AreaUnderROC: 0.8076607183628867
Root mean squared error: 0.41000990536144133
Relative absolute error: 39.50648018944331
Root relative squared error: 86.97623533041872
Weighted TruePositiveRate: 0.7408
Weighted MatthewsCorrelation: 0.6108887162275172
Weighted FMeasure: 0.7404004437411787
Iteration time: 346.0
Weighted AreaUnderPRC: 0.6456434460837549
Mean absolute error: 0.17558435639752662
Coverage of cases: 76.52
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 8404.0
Weighted Recall: 0.7408
Weighted FalsePositiveRate: 0.12970677447204507
Kappa statistic: 0.6112297953484844
Training time: 344.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 34.760000000001085
Incorrectly Classified Instances: 26.08
Correctly Classified Instances: 73.92
Weighted Precision: 0.7389029972352288
Weighted AreaUnderROC: 0.7997847628764985
Root mean squared error: 0.41251649450164724
Relative absolute error: 39.823102714459225
Root relative squared error: 87.50796318402516
Weighted TruePositiveRate: 0.7392
Weighted MatthewsCorrelation: 0.6087090087748347
Weighted FMeasure: 0.7386045973244354
Iteration time: 305.0
Weighted AreaUnderPRC: 0.6353303628043669
Mean absolute error: 0.1769915676198196
Coverage of cases: 75.76
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 8709.0
Weighted Recall: 0.7392
Weighted FalsePositiveRate: 0.1303733859121783
Kappa statistic: 0.6088772858483164
Training time: 303.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 34.3333333333343
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7425707840932376
Weighted AreaUnderROC: 0.806847094629064
Root mean squared error: 0.4092736092233866
Relative absolute error: 39.35731701927993
Root relative squared error: 86.82004333276474
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.6140548804532214
Weighted FMeasure: 0.742470605566727
Iteration time: 263.0
Weighted AreaUnderPRC: 0.6472557591260085
Mean absolute error: 0.1749214089745783
Coverage of cases: 75.4
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 8972.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.12862458389614168
Kappa statistic: 0.6142477098432466
Training time: 261.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 35.72000000000089
Incorrectly Classified Instances: 25.44
Correctly Classified Instances: 74.56
Weighted Precision: 0.7453992423369776
Weighted AreaUnderROC: 0.8118255245793152
Root mean squared error: 0.4055786040596443
Relative absolute error: 39.36238641946566
Root relative squared error: 86.03621437042429
Weighted TruePositiveRate: 0.7456
Weighted MatthewsCorrelation: 0.6181286390934216
Weighted FMeasure: 0.7454753195550153
Iteration time: 258.0
Weighted AreaUnderPRC: 0.65055317948699
Mean absolute error: 0.17494393964207042
Coverage of cases: 77.32
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 9230.0
Weighted Recall: 0.7456
Weighted FalsePositiveRate: 0.1273702228798034
Kappa statistic: 0.6184010379491769
Training time: 255.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 35.013333333334316
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7457564946040649
Weighted AreaUnderROC: 0.8095087803799417
Root mean squared error: 0.4057944036967761
Relative absolute error: 39.23491427336554
Root relative squared error: 86.08199238646235
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.6188292251348714
Weighted FMeasure: 0.7456764091517296
Iteration time: 258.0
Weighted AreaUnderPRC: 0.6475871502121734
Mean absolute error: 0.17437739677051434
Coverage of cases: 76.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 9488.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.12704783281650392
Kappa statistic: 0.6190427281676087
Training time: 256.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 34.33333333333431
Incorrectly Classified Instances: 25.16
Correctly Classified Instances: 74.84
Weighted Precision: 0.7480645739190759
Weighted AreaUnderROC: 0.8079164532997817
Root mean squared error: 0.40362306488820304
Relative absolute error: 38.74764916347541
Root relative squared error: 85.62138186772367
Weighted TruePositiveRate: 0.7484
Weighted MatthewsCorrelation: 0.6224147791008562
Weighted FMeasure: 0.7479342875603409
Iteration time: 277.0
Weighted AreaUnderPRC: 0.6456048908991308
Mean absolute error: 0.17221177405989152
Coverage of cases: 76.28
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 9765.0
Weighted Recall: 0.7484
Weighted FalsePositiveRate: 0.12583399544137788
Kappa statistic: 0.6226577182754125
Training time: 275.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 37.57333333333434
Incorrectly Classified Instances: 26.44
Correctly Classified Instances: 73.56
Weighted Precision: 0.736651917555479
Weighted AreaUnderROC: 0.8106919281226143
Root mean squared error: 0.4040511721592934
Relative absolute error: 40.514122211207706
Root relative squared error: 85.71219713406266
Weighted TruePositiveRate: 0.7356
Weighted MatthewsCorrelation: 0.6036294839405606
Weighted FMeasure: 0.7358575729872844
Iteration time: 267.0
Weighted AreaUnderPRC: 0.6506641444685909
Mean absolute error: 0.1800627653831462
Coverage of cases: 79.12
Instances selection time: 2.0
Test time: 12.0
Accumulative iteration time: 10032.0
Weighted Recall: 0.7356
Weighted FalsePositiveRate: 0.13249533303512276
Kappa statistic: 0.60333958068493
Training time: 265.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 35.56000000000096
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7374068436239515
Weighted AreaUnderROC: 0.8049724862343393
Root mean squared error: 0.4110763544028693
Relative absolute error: 39.94537370282608
Root relative squared error: 87.20246333511382
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6065562859351575
Weighted FMeasure: 0.7376051979345293
Iteration time: 276.0
Weighted AreaUnderPRC: 0.6381322641503014
Mean absolute error: 0.17753499423478342
Coverage of cases: 76.56
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 10308.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13118592961944572
Kappa statistic: 0.6070246474141141
Training time: 274.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 36.12000000000089
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7417804879061558
Weighted AreaUnderROC: 0.8004806912148525
Root mean squared error: 0.40882126435477595
Relative absolute error: 40.117028984252514
Root relative squared error: 86.72408649555588
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.6127112775668302
Weighted FMeasure: 0.7418510102693581
Iteration time: 275.0
Weighted AreaUnderPRC: 0.6324195508085216
Mean absolute error: 0.17829790659667868
Coverage of cases: 77.36
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 10583.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.12919705269644402
Kappa statistic: 0.6129928790689748
Training time: 273.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 36.65333333333399
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7288233063740909
Weighted AreaUnderROC: 0.8010874942881051
Root mean squared error: 0.4198901526011625
Relative absolute error: 41.72478904237294
Root relative squared error: 89.07215227732068
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5916778999947016
Weighted FMeasure: 0.7279201918030803
Iteration time: 280.0
Weighted AreaUnderPRC: 0.630908754029925
Mean absolute error: 0.18544350685499172
Coverage of cases: 75.48
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 10863.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13658191849453696
Kappa statistic: 0.5912910545434992
Training time: 278.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 37.826666666667585
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.735643999776844
Weighted AreaUnderROC: 0.8248786504877862
Root mean squared error: 0.40682279274875816
Relative absolute error: 40.28597254535911
Root relative squared error: 86.30014664816869
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6036853465491641
Weighted FMeasure: 0.735747737744065
Iteration time: 296.0
Weighted AreaUnderPRC: 0.6641825989992544
Mean absolute error: 0.17904876686826357
Coverage of cases: 78.48
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 11159.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.1321474668162987
Kappa statistic: 0.6040181834850143
Training time: 294.0
		
Time end:Sun Oct 08 10.39.28 EEST 2017