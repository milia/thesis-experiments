Sun Oct 08 10.41.16 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.41.16 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.92000000000154
Incorrectly Classified Instances: 22.24
Correctly Classified Instances: 77.76
Weighted Precision: 0.7774382297077796
Weighted AreaUnderROC: 0.9237522055509231
Root mean squared error: 0.32663129060149604
Relative absolute error: 52.10999999999915
Root relative squared error: 69.28896015960933
Weighted TruePositiveRate: 0.7776
Weighted MatthewsCorrelation: 0.666490193300409
Weighted FMeasure: 0.7765678788887083
Iteration time: 422.0
Weighted AreaUnderPRC: 0.8280819472991683
Mean absolute error: 0.2315999999999973
Coverage of cases: 99.8
Instances selection time: 60.0
Test time: 65.0
Accumulative iteration time: 422.0
Weighted Recall: 0.7776
Weighted FalsePositiveRate: 0.11112256798735266
Kappa statistic: 0.6665054376408355
Training time: 362.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 72.02666666666813
Incorrectly Classified Instances: 21.76
Correctly Classified Instances: 78.24
Weighted Precision: 0.7824255095713
Weighted AreaUnderROC: 0.9250930015325405
Root mean squared error: 0.3252937134344878
Relative absolute error: 52.30799999999909
Root relative squared error: 69.00521719406379
Weighted TruePositiveRate: 0.7824
Weighted MatthewsCorrelation: 0.6735487411849498
Weighted FMeasure: 0.7822405596892406
Iteration time: 352.0
Weighted AreaUnderPRC: 0.828170232935424
Mean absolute error: 0.23247999999999705
Coverage of cases: 99.68
Instances selection time: 33.0
Test time: 24.0
Accumulative iteration time: 774.0
Weighted Recall: 0.7824
Weighted FalsePositiveRate: 0.1088453101274061
Kappa statistic: 0.6736215670868468
Training time: 319.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 70.96000000000143
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.788300686801773
Weighted AreaUnderROC: 0.9328461720680472
Root mean squared error: 0.3191279785080991
Relative absolute error: 51.23999999999904
Root relative squared error: 67.69726730082934
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.682470271408973
Weighted FMeasure: 0.7883040359826858
Iteration time: 321.0
Weighted AreaUnderPRC: 0.8467033380132165
Mean absolute error: 0.22773333333333012
Coverage of cases: 99.8
Instances selection time: 22.0
Test time: 26.0
Accumulative iteration time: 1095.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.1058637033777195
Kappa statistic: 0.6826052814481167
Training time: 299.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 72.0533333333349
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.7888256799470472
Weighted AreaUnderROC: 0.9283584230011059
Root mean squared error: 0.32290349435499394
Relative absolute error: 51.875999999999
Root relative squared error: 68.49817515817432
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.6827961881034533
Weighted FMeasure: 0.7880017730651439
Iteration time: 193.0
Weighted AreaUnderPRC: 0.835929921241894
Mean absolute error: 0.23055999999999663
Coverage of cases: 99.64
Instances selection time: 9.0
Test time: 19.0
Accumulative iteration time: 1288.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.10594665532993731
Kappa statistic: 0.6825918743519834
Training time: 184.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 71.28000000000146
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.7954260717740934
Weighted AreaUnderROC: 0.9332471834760008
Root mean squared error: 0.31777140630753065
Relative absolute error: 50.417999999999
Root relative squared error: 67.409494880172
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.6930445889002959
Weighted FMeasure: 0.7944881805108898
Iteration time: 150.0
Weighted AreaUnderPRC: 0.8464515987648669
Mean absolute error: 0.22407999999999662
Coverage of cases: 99.64
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1438.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.10230184543896305
Kappa statistic: 0.6928858936732575
Training time: 142.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 71.10666666666816
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8008040668520471
Weighted AreaUnderROC: 0.9305156238191412
Root mean squared error: 0.3194996087634521
Relative absolute error: 51.215999999999056
Root relative squared error: 67.77610198292561
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7010462794938103
Weighted FMeasure: 0.8007959628330065
Iteration time: 176.0
Weighted AreaUnderPRC: 0.8384468723840041
Mean absolute error: 0.22762666666666356
Coverage of cases: 99.64
Instances selection time: 23.0
Test time: 28.0
Accumulative iteration time: 1614.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09975613653874227
Kappa statistic: 0.7011831706361701
Training time: 153.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 72.61333333333474
Incorrectly Classified Instances: 21.12
Correctly Classified Instances: 78.88
Weighted Precision: 0.7889154103684836
Weighted AreaUnderROC: 0.92872192179212
Root mean squared error: 0.3208654962233641
Relative absolute error: 51.23999999999906
Root relative squared error: 68.06585046849803
Weighted TruePositiveRate: 0.7888
Weighted MatthewsCorrelation: 0.6832721082431511
Weighted FMeasure: 0.7883886873351861
Iteration time: 205.0
Weighted AreaUnderPRC: 0.8374427071992595
Mean absolute error: 0.2277333333333302
Coverage of cases: 99.8
Instances selection time: 18.0
Test time: 15.0
Accumulative iteration time: 1819.0
Weighted Recall: 0.7888
Weighted FalsePositiveRate: 0.10561735296396878
Kappa statistic: 0.6832422174772546
Training time: 187.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 73.17333333333472
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8020670649509785
Weighted AreaUnderROC: 0.9342789051206001
Root mean squared error: 0.3164806471176385
Relative absolute error: 50.783999999999025
Root relative squared error: 67.13568350735653
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.702994379417174
Weighted FMeasure: 0.8018074906328732
Iteration time: 215.0
Weighted AreaUnderPRC: 0.8481827966104567
Mean absolute error: 0.22570666666666342
Coverage of cases: 99.64
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2034.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09901868463152011
Kappa statistic: 0.7030261812118644
Training time: 206.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 71.6533333333347
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.7920278884377647
Weighted AreaUnderROC: 0.9337957914578261
Root mean squared error: 0.3161392098427511
Relative absolute error: 50.555999999998896
Root relative squared error: 67.06325372362971
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.6877832819659109
Weighted FMeasure: 0.7920133622991874
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8475168976672474
Mean absolute error: 0.22469333333332947
Coverage of cases: 99.76
Instances selection time: 28.0
Test time: 26.0
Accumulative iteration time: 2189.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.1042304750623094
Kappa statistic: 0.6879801805010656
Training time: 127.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 71.57333333333479
Incorrectly Classified Instances: 20.08
Correctly Classified Instances: 79.92
Weighted Precision: 0.8001138716520527
Weighted AreaUnderROC: 0.9349400365889609
Root mean squared error: 0.3158565075051207
Relative absolute error: 50.44199999999902
Root relative squared error: 67.00328350163097
Weighted TruePositiveRate: 0.7992
Weighted MatthewsCorrelation: 0.6990602373141318
Weighted FMeasure: 0.7993849247892792
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8512805445315662
Mean absolute error: 0.22418666666666337
Coverage of cases: 99.92
Instances selection time: 25.0
Test time: 27.0
Accumulative iteration time: 2355.0
Weighted Recall: 0.7992
Weighted FalsePositiveRate: 0.10064912675532482
Kappa statistic: 0.6987421102839122
Training time: 141.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 70.8133333333348
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8101788596649935
Weighted AreaUnderROC: 0.9370011484799524
Root mean squared error: 0.31243559336285454
Relative absolute error: 49.28399999999888
Root relative squared error: 66.27759802527498
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7151018782885341
Weighted FMeasure: 0.809705927737351
Iteration time: 214.0
Weighted AreaUnderPRC: 0.8540445179777287
Mean absolute error: 0.21903999999999604
Coverage of cases: 99.76
Instances selection time: 19.0
Test time: 27.0
Accumulative iteration time: 2569.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09499568026013151
Kappa statistic: 0.7150310046266967
Training time: 195.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 71.76000000000133
Incorrectly Classified Instances: 20.2
Correctly Classified Instances: 79.8
Weighted Precision: 0.7976453629499237
Weighted AreaUnderROC: 0.9341034493434076
Root mean squared error: 0.3155228465050756
Relative absolute error: 50.5019999999989
Root relative squared error: 66.93250331490619
Weighted TruePositiveRate: 0.798
Weighted MatthewsCorrelation: 0.6967568619981823
Weighted FMeasure: 0.7976455089058292
Iteration time: 148.0
Weighted AreaUnderPRC: 0.8498326525563525
Mean absolute error: 0.2244533333333295
Coverage of cases: 99.68
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2717.0
Weighted Recall: 0.798
Weighted FalsePositiveRate: 0.10107193496524089
Kappa statistic: 0.6970312179033074
Training time: 139.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 69.92000000000144
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.7884165051645278
Weighted AreaUnderROC: 0.9345611913817811
Root mean squared error: 0.3148163485801401
Relative absolute error: 49.607999999998896
Root relative squared error: 66.78263247282135
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.6820238358908757
Weighted FMeasure: 0.7880491955658137
Iteration time: 151.0
Weighted AreaUnderPRC: 0.849015347744421
Mean absolute error: 0.2204799999999961
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 2868.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.10621728607085092
Kappa statistic: 0.681962089881114
Training time: 142.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.10666666666808
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8077604857447849
Weighted AreaUnderROC: 0.9416186038856292
Root mean squared error: 0.3078051331605748
Relative absolute error: 48.38999999999877
Root relative squared error: 65.29532908256105
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7113259638991162
Weighted FMeasure: 0.8076627888349946
Iteration time: 157.0
Weighted AreaUnderPRC: 0.8652356989139398
Mean absolute error: 0.2150666666666622
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3025.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09636628779575383
Kappa statistic: 0.7113705828898081
Training time: 149.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 70.94666666666808
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8023835496301547
Weighted AreaUnderROC: 0.9357717177575349
Root mean squared error: 0.31375574363932535
Relative absolute error: 49.64999999999897
Root relative squared error: 66.55764418907833
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.703502578876078
Weighted FMeasure: 0.8022262610025018
Iteration time: 184.0
Weighted AreaUnderPRC: 0.8514071321733281
Mean absolute error: 0.2206666666666631
Coverage of cases: 99.72
Instances selection time: 15.0
Test time: 16.0
Accumulative iteration time: 3209.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09893061126024749
Kappa statistic: 0.7035895663527356
Training time: 169.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 70.41333333333458
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8063116584564861
Weighted AreaUnderROC: 0.9400320150231156
Root mean squared error: 0.3102944837838177
Relative absolute error: 49.21199999999891
Root relative squared error: 65.82340009449486
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7086299883753027
Weighted FMeasure: 0.8055718549557565
Iteration time: 189.0
Weighted AreaUnderPRC: 0.8615350646310348
Mean absolute error: 0.21871999999999617
Coverage of cases: 99.92
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 3398.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.0973489360289959
Kappa statistic: 0.7083891754061911
Training time: 180.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 70.2533333333347
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8103807950560119
Weighted AreaUnderROC: 0.9400139347575468
Root mean squared error: 0.3087005021051939
Relative absolute error: 48.40199999999882
Root relative squared error: 65.48526551828226
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7150888436446012
Weighted FMeasure: 0.809893098561949
Iteration time: 196.0
Weighted AreaUnderPRC: 0.863190922012257
Mean absolute error: 0.2151199999999958
Coverage of cases: 99.76
Instances selection time: 15.0
Test time: 27.0
Accumulative iteration time: 3594.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09511498228401019
Kappa statistic: 0.7150015959910625
Training time: 181.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 68.66666666666792
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8038390925096759
Weighted AreaUnderROC: 0.9414165981754244
Root mean squared error: 0.3059891065599104
Relative absolute error: 46.87799999999874
Root relative squared error: 64.91009166531757
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7037830062966043
Weighted FMeasure: 0.8019881356837704
Iteration time: 178.0
Weighted AreaUnderPRC: 0.8645149238958784
Mean absolute error: 0.20834666666666204
Coverage of cases: 99.88
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 3772.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09927053956349403
Kappa statistic: 0.7029410635069999
Training time: 169.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 69.9200000000014
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8070187552345823
Weighted AreaUnderROC: 0.9359259710712114
Root mean squared error: 0.3116622103068198
Relative absolute error: 48.72599999999881
Root relative squared error: 66.1135387042619
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7100027438737456
Weighted FMeasure: 0.8059603949033445
Iteration time: 179.0
Weighted AreaUnderPRC: 0.8529837993244787
Mean absolute error: 0.21655999999999573
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 3951.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09669542566412591
Kappa statistic: 0.7096707474322657
Training time: 170.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 68.33333333333469
Incorrectly Classified Instances: 20.08
Correctly Classified Instances: 79.92
Weighted Precision: 0.8006422515739374
Weighted AreaUnderROC: 0.938293937740171
Root mean squared error: 0.3095028271276357
Relative absolute error: 47.579999999998776
Root relative squared error: 65.65546435750753
Weighted TruePositiveRate: 0.7992
Weighted MatthewsCorrelation: 0.6993307713233375
Weighted FMeasure: 0.7994041754015339
Iteration time: 188.0
Weighted AreaUnderPRC: 0.858870579959057
Mean absolute error: 0.2114666666666622
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 4139.0
Weighted Recall: 0.7992
Weighted FalsePositiveRate: 0.10072929823379399
Kappa statistic: 0.6987084073558363
Training time: 180.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 69.74666666666808
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8031408876816098
Weighted AreaUnderROC: 0.938296683329429
Root mean squared error: 0.3116878780660747
Relative absolute error: 48.99599999999894
Root relative squared error: 66.11898365825
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7046637006465793
Weighted FMeasure: 0.8030634165899293
Iteration time: 208.0
Weighted AreaUnderPRC: 0.8577093165767082
Mean absolute error: 0.21775999999999635
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 4347.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.0985134835018646
Kappa statistic: 0.7048006612465189
Training time: 200.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 69.52000000000129
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8015809898784181
Weighted AreaUnderROC: 0.94040024864987
Root mean squared error: 0.30740201690945207
Relative absolute error: 47.66399999999878
Root relative squared error: 65.20981521212843
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.702355047087559
Weighted FMeasure: 0.8014649688263191
Iteration time: 190.0
Weighted AreaUnderPRC: 0.8634723487629431
Mean absolute error: 0.21183999999999556
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 4537.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09920970292644292
Kappa statistic: 0.7024173788250766
Training time: 182.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 68.46666666666796
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8083955384868423
Weighted AreaUnderROC: 0.9426200991532145
Root mean squared error: 0.30385522868629267
Relative absolute error: 46.66799999999867
Root relative squared error: 64.45742781091985
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7125949020580827
Weighted FMeasure: 0.8081309940119761
Iteration time: 202.0
Weighted AreaUnderPRC: 0.8680579430765276
Mean absolute error: 0.2074133333333284
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 4739.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09577278572246742
Kappa statistic: 0.7126392649708344
Training time: 194.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 69.61333333333474
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8105649421996939
Weighted AreaUnderROC: 0.946158228919027
Root mean squared error: 0.3031611232771542
Relative absolute error: 47.615999999998785
Root relative squared error: 64.31018581842184
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7134854025679591
Weighted FMeasure: 0.8086059400768217
Iteration time: 204.0
Weighted AreaUnderPRC: 0.8751290629041929
Mean absolute error: 0.21162666666666227
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 4943.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09617760472828048
Kappa statistic: 0.7125030790380054
Training time: 196.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 69.57333333333479
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8020857327949362
Weighted AreaUnderROC: 0.9398642041484724
Root mean squared error: 0.3072762058257448
Relative absolute error: 47.66399999999876
Root relative squared error: 65.18312665099708
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7026056803358278
Weighted FMeasure: 0.8014717586648921
Iteration time: 202.0
Weighted AreaUnderPRC: 0.8620822818188252
Mean absolute error: 0.21183999999999545
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 5145.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09925248403169953
Kappa statistic: 0.702406380407204
Training time: 194.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 67.61333333333457
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.802886364717
Weighted AreaUnderROC: 0.9413382014835446
Root mean squared error: 0.305518139123248
Relative absolute error: 46.60199999999866
Root relative squared error: 64.81018438486296
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7040828395498524
Weighted FMeasure: 0.8028003105026027
Iteration time: 208.0
Weighted AreaUnderPRC: 0.8663641957212862
Mean absolute error: 0.20711999999999503
Coverage of cases: 99.8
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 5353.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09876721279871456
Kappa statistic: 0.7041807835837015
Training time: 201.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 68.21333333333462
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.809596442745274
Weighted AreaUnderROC: 0.9412298938453141
Root mean squared error: 0.3056272239182872
Relative absolute error: 46.91999999999872
Root relative squared error: 64.83332476435193
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7138305093020016
Weighted FMeasure: 0.8092956067178899
Iteration time: 213.0
Weighted AreaUnderPRC: 0.8653293222320128
Mean absolute error: 0.20853333333332863
Coverage of cases: 99.64
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 5566.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09560212216401441
Kappa statistic: 0.7137592851207156
Training time: 205.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 69.2400000000014
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.812492777493378
Weighted AreaUnderROC: 0.9439549640561643
Root mean squared error: 0.3033765536974347
Relative absolute error: 47.07599999999868
Root relative squared error: 64.35588551173811
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.718102334777652
Weighted FMeasure: 0.8120685786896144
Iteration time: 229.0
Weighted AreaUnderPRC: 0.8709085210178892
Mean absolute error: 0.2092266666666618
Coverage of cases: 99.64
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 5795.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09421258329264233
Kappa statistic: 0.7179499981756766
Training time: 222.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 68.21333333333467
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8170588980198925
Weighted AreaUnderROC: 0.9455608507165476
Root mean squared error: 0.3018476436880018
Relative absolute error: 46.511999999998665
Root relative squared error: 64.0315547210899
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7239565889610834
Weighted FMeasure: 0.8158241240025008
Iteration time: 231.0
Weighted AreaUnderPRC: 0.8751123631768285
Mean absolute error: 0.20671999999999502
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 6026.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09253458572092044
Kappa statistic: 0.7233083154434053
Training time: 223.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 68.80000000000143
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8036198275268047
Weighted AreaUnderROC: 0.9404738823577306
Root mean squared error: 0.3066811590778484
Relative absolute error: 47.33399999999883
Root relative squared error: 65.05689817382891
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7039984615164916
Weighted FMeasure: 0.802499087156031
Iteration time: 232.0
Weighted AreaUnderPRC: 0.8630442023177828
Mean absolute error: 0.21037333333332914
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 6258.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.0991185740505757
Kappa statistic: 0.7035325002796636
Training time: 224.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 69.81333333333461
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8103031305750408
Weighted AreaUnderROC: 0.9440321807554223
Root mean squared error: 0.30501147519396554
Relative absolute error: 47.6939999999987
Root relative squared error: 64.70270473480949
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7150251612172212
Weighted FMeasure: 0.8099850334606067
Iteration time: 250.0
Weighted AreaUnderPRC: 0.8703372967954401
Mean absolute error: 0.21197333333332857
Coverage of cases: 99.84
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 6508.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09515538642118647
Kappa statistic: 0.7149768333170121
Training time: 240.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 68.6533333333347
Incorrectly Classified Instances: 18.56
Correctly Classified Instances: 81.44
Weighted Precision: 0.8150110865299984
Weighted AreaUnderROC: 0.9444806764531116
Root mean squared error: 0.30212580161250574
Relative absolute error: 46.58399999999872
Root relative squared error: 64.09056092748716
Weighted TruePositiveRate: 0.8144
Weighted MatthewsCorrelation: 0.7217003977277939
Weighted FMeasure: 0.81454576084514
Iteration time: 252.0
Weighted AreaUnderPRC: 0.8705503529206838
Mean absolute error: 0.20703999999999528
Coverage of cases: 99.72
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 6760.0
Weighted Recall: 0.8144
Weighted FalsePositiveRate: 0.0930443074965215
Kappa statistic: 0.7215545131452674
Training time: 242.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 68.28000000000138
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8023647595840477
Weighted AreaUnderROC: 0.9416191812823678
Root mean squared error: 0.30500710374240875
Relative absolute error: 46.4939999999987
Root relative squared error: 64.70177740989766
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.702610372223125
Weighted FMeasure: 0.8017020167359522
Iteration time: 241.0
Weighted AreaUnderPRC: 0.8663085086482237
Mean absolute error: 0.2066399999999952
Coverage of cases: 99.84
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 7001.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09942949514248645
Kappa statistic: 0.7023502329589507
Training time: 231.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 68.70666666666796
Incorrectly Classified Instances: 18.96
Correctly Classified Instances: 81.04
Weighted Precision: 0.8118722630121813
Weighted AreaUnderROC: 0.9453693986004978
Root mean squared error: 0.3018299742128548
Relative absolute error: 46.355999999998666
Root relative squared error: 64.02780645938097
Weighted TruePositiveRate: 0.8104
Weighted MatthewsCorrelation: 0.716157864473969
Weighted FMeasure: 0.8106165750711875
Iteration time: 255.0
Weighted AreaUnderPRC: 0.8738904976168839
Mean absolute error: 0.2060266666666617
Coverage of cases: 99.92
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 7256.0
Weighted Recall: 0.8104
Weighted FalsePositiveRate: 0.09513288228899466
Kappa statistic: 0.715508598602559
Training time: 245.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 68.94666666666814
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8097501424048318
Weighted AreaUnderROC: 0.9447844714170098
Root mean squared error: 0.3034908016178839
Relative absolute error: 47.21999999999869
Root relative squared error: 64.38012115552392
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7126653354091688
Weighted FMeasure: 0.8082690517180987
Iteration time: 252.0
Weighted AreaUnderPRC: 0.8727200334054621
Mean absolute error: 0.20986666666666184
Coverage of cases: 99.8
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7508.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09631647488379226
Kappa statistic: 0.7119208588983567
Training time: 246.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 68.85333333333465
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8108871122582177
Weighted AreaUnderROC: 0.9463509396589432
Root mean squared error: 0.3017990501420872
Relative absolute error: 46.93199999999867
Root relative squared error: 64.02124647333846
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7139716435569775
Weighted FMeasure: 0.8091995710207068
Iteration time: 256.0
Weighted AreaUnderPRC: 0.8755519817799914
Mean absolute error: 0.20858666666666176
Coverage of cases: 99.92
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7764.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.0960148430626309
Kappa statistic: 0.7130970821853214
Training time: 250.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 68.40000000000136
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8079370988341988
Weighted AreaUnderROC: 0.9440520297935313
Root mean squared error: 0.30409647591951083
Relative absolute error: 46.937999999998745
Root relative squared error: 64.50860407728518
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.709379279640156
Weighted FMeasure: 0.8058644447765646
Iteration time: 259.0
Weighted AreaUnderPRC: 0.8715201313915623
Mean absolute error: 0.20861333333332874
Coverage of cases: 99.92
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8023.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09759746614338599
Kappa statistic: 0.7082882160443882
Training time: 253.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 68.88000000000135
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8031710678896237
Weighted AreaUnderROC: 0.9425967334270338
Root mean squared error: 0.3075104334273326
Relative absolute error: 48.19799999999882
Root relative squared error: 65.23281382862422
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7023389705179903
Weighted FMeasure: 0.8006682338985652
Iteration time: 267.0
Weighted AreaUnderPRC: 0.8668684082310607
Mean absolute error: 0.2142133333333291
Coverage of cases: 99.96
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 8290.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09996735404332484
Kappa statistic: 0.7010830158490827
Training time: 258.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 68.73333333333467
Incorrectly Classified Instances: 18.72
Correctly Classified Instances: 81.28
Weighted Precision: 0.8129390404964438
Weighted AreaUnderROC: 0.9437828281414717
Root mean squared error: 0.30419292124132735
Relative absolute error: 47.14799999999877
Root relative squared error: 64.52906321960623
Weighted TruePositiveRate: 0.8128
Weighted MatthewsCorrelation: 0.7191469761501587
Weighted FMeasure: 0.8127765430402355
Iteration time: 273.0
Weighted AreaUnderPRC: 0.8702501855466688
Mean absolute error: 0.20954666666666216
Coverage of cases: 99.96
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 8563.0
Weighted Recall: 0.8128
Weighted FalsePositiveRate: 0.09376505522712096
Kappa statistic: 0.7191694905734558
Training time: 267.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 68.54666666666809
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.7946802848052513
Weighted AreaUnderROC: 0.9399899605582972
Root mean squared error: 0.3092744196771953
Relative absolute error: 48.34799999999881
Root relative squared error: 65.60701182038355
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.689084769691259
Weighted FMeasure: 0.7924049701160991
Iteration time: 277.0
Weighted AreaUnderPRC: 0.8626545459791032
Mean absolute error: 0.21487999999999574
Coverage of cases: 99.84
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8840.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.1044438362447572
Kappa statistic: 0.6878724521988666
Training time: 271.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 69.36000000000122
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8155017485188637
Weighted AreaUnderROC: 0.9467882967997054
Root mean squared error: 0.30156922919953105
Relative absolute error: 47.02199999999875
Root relative squared error: 63.97249408925643
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.7212503077802453
Weighted FMeasure: 0.8136412225462183
Iteration time: 276.0
Weighted AreaUnderPRC: 0.8759521057718519
Mean absolute error: 0.20898666666666207
Coverage of cases: 99.84
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 9116.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.09353160290333579
Kappa statistic: 0.7203045007687424
Training time: 270.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 66.85333333333453
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.800197748557878
Weighted AreaUnderROC: 0.9388594277839631
Root mean squared error: 0.307007057465024
Relative absolute error: 46.049999999998725
Root relative squared error: 65.12603166169382
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.6984123178792097
Weighted FMeasure: 0.7984311714180902
Iteration time: 284.0
Weighted AreaUnderPRC: 0.8601185669303233
Mean absolute error: 0.20466666666666194
Coverage of cases: 99.6
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9400.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10113634439873408
Kappa statistic: 0.6974885789934149
Training time: 279.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 68.28000000000131
Incorrectly Classified Instances: 20.32
Correctly Classified Instances: 79.68
Weighted Precision: 0.7995864539030706
Weighted AreaUnderROC: 0.9433580515828651
Root mean squared error: 0.3049109159519643
Relative absolute error: 46.91999999999871
Root relative squared error: 64.68137289823046
Weighted TruePositiveRate: 0.7968
Weighted MatthewsCorrelation: 0.6964197263165812
Weighted FMeasure: 0.7970279314067277
Iteration time: 303.0
Weighted AreaUnderPRC: 0.8711535219903505
Mean absolute error: 0.20853333333332857
Coverage of cases: 99.92
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 9703.0
Weighted Recall: 0.7968
Weighted FalsePositiveRate: 0.10198012247646432
Kappa statistic: 0.6950872554635802
Training time: 297.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 67.0133333333346
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8080903608148771
Weighted AreaUnderROC: 0.9458195571745966
Root mean squared error: 0.30080780131727397
Relative absolute error: 45.701999999998655
Root relative squared error: 63.810970843577884
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7119193297301918
Weighted FMeasure: 0.8080019248576588
Iteration time: 305.0
Weighted AreaUnderPRC: 0.8753519596748284
Mean absolute error: 0.20311999999999497
Coverage of cases: 99.8
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 10008.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09614359560192577
Kappa statistic: 0.7119758981431566
Training time: 299.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 68.37333333333473
Incorrectly Classified Instances: 18.68
Correctly Classified Instances: 81.32
Weighted Precision: 0.8144855661566156
Weighted AreaUnderROC: 0.9430272081169427
Root mean squared error: 0.30410962935537855
Relative absolute error: 46.82399999999874
Root relative squared error: 64.51139434239457
Weighted TruePositiveRate: 0.8132
Weighted MatthewsCorrelation: 0.7202626313519896
Weighted FMeasure: 0.8134160289719744
Iteration time: 308.0
Weighted AreaUnderPRC: 0.8676180910909559
Mean absolute error: 0.20810666666666205
Coverage of cases: 99.76
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 10316.0
Weighted Recall: 0.8132
Weighted FalsePositiveRate: 0.09371444999246592
Kappa statistic: 0.719715869915514
Training time: 301.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.40000000000111
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.7950531017988118
Weighted AreaUnderROC: 0.9396804863663436
Root mean squared error: 0.30709824703721844
Relative absolute error: 46.1219999999987
Root relative squared error: 65.14537589115547
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.6894092895655045
Weighted FMeasure: 0.7920636018586708
Iteration time: 300.0
Weighted AreaUnderPRC: 0.8615455936773709
Mean absolute error: 0.20498666666666185
Coverage of cases: 99.68
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 10616.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10444169256689197
Kappa statistic: 0.6878598615634477
Training time: 294.0
		
Time end:Sun Oct 08 10.41.31 EEST 2017