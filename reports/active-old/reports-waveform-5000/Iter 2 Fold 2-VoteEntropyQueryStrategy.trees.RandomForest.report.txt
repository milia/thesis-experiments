Sun Oct 08 12.23.32 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 12.23.32 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 72.80000000000146
Incorrectly Classified Instances: 21.68
Correctly Classified Instances: 78.32
Weighted Precision: 0.7856504800142817
Weighted AreaUnderROC: 0.9270757631520173
Root mean squared error: 0.3234563339927032
Relative absolute error: 51.46799999999908
Root relative squared error: 68.6154501551942
Weighted TruePositiveRate: 0.7832
Weighted MatthewsCorrelation: 0.6759342052361617
Weighted FMeasure: 0.7829868916696673
Iteration time: 94.0
Weighted AreaUnderPRC: 0.8324283154421331
Mean absolute error: 0.22874666666666363
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7832
Weighted FalsePositiveRate: 0.10869323453584961
Kappa statistic: 0.6747322662471233
Training time: 86.0
		
Iteration: 2
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 67.80000000000106
Incorrectly Classified Instances: 21.88
Correctly Classified Instances: 78.12
Weighted Precision: 0.7943878584946824
Weighted AreaUnderROC: 0.9173559215970847
Root mean squared error: 0.3371883746513207
Relative absolute error: 49.77599999999927
Root relative squared error: 71.52845587596553
Weighted TruePositiveRate: 0.7812
Weighted MatthewsCorrelation: 0.680492612448007
Weighted FMeasure: 0.7776188878587438
Iteration time: 153.0
Weighted AreaUnderPRC: 0.8164164906380763
Mean absolute error: 0.22122666666666446
Coverage of cases: 98.48
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 247.0
Weighted Recall: 0.7812
Weighted FalsePositiveRate: 0.10923556882877712
Kappa statistic: 0.6719366318315747
Training time: 145.0
		
Iteration: 3
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 67.86666666666765
Incorrectly Classified Instances: 25.92
Correctly Classified Instances: 74.08
Weighted Precision: 0.7649646803405341
Weighted AreaUnderROC: 0.9034910639931019
Root mean squared error: 0.3580502757993625
Relative absolute error: 52.77599999999936
Root relative squared error: 75.95393340703266
Weighted TruePositiveRate: 0.7408
Weighted MatthewsCorrelation: 0.6276322390312986
Weighted FMeasure: 0.733737998322046
Iteration time: 203.0
Weighted AreaUnderPRC: 0.7904709759687086
Mean absolute error: 0.23455999999999827
Coverage of cases: 97.6
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 450.0
Weighted Recall: 0.7408
Weighted FalsePositiveRate: 0.12921302687032327
Kappa statistic: 0.6114574948889872
Training time: 196.0
		
Iteration: 4
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.8800000000011
Incorrectly Classified Instances: 26.08
Correctly Classified Instances: 73.92
Weighted Precision: 0.7784154263254821
Weighted AreaUnderROC: 0.9195315325342899
Root mean squared error: 0.34717910843444066
Relative absolute error: 50.96999999999938
Root relative squared error: 73.64781055808763
Weighted TruePositiveRate: 0.7392
Weighted MatthewsCorrelation: 0.6290680050132842
Weighted FMeasure: 0.7270807814162308
Iteration time: 274.0
Weighted AreaUnderPRC: 0.8231519496421658
Mean absolute error: 0.22653333333333167
Coverage of cases: 98.88
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 724.0
Weighted Recall: 0.7392
Weighted FalsePositiveRate: 0.13082102992907485
Kappa statistic: 0.6086042708237536
Training time: 269.0
		
Iteration: 5
Labeled set size: 1700
Unlabelled set size: 800
	
Mean region size: 65.77333333333426
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.7827313794694014
Weighted AreaUnderROC: 0.9218518861699183
Root mean squared error: 0.35017709805182834
Relative absolute error: 51.26399999999941
Root relative squared error: 74.28378019460213
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.6206265975125167
Weighted FMeasure: 0.7119459821873128
Iteration time: 338.0
Weighted AreaUnderPRC: 0.8269844235976749
Mean absolute error: 0.22783999999999846
Coverage of cases: 98.56
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 1062.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.13570115794563486
Kappa statistic: 0.5956862304067024
Training time: 334.0
		
Iteration: 6
Labeled set size: 2000
Unlabelled set size: 500
	
Mean region size: 66.6133333333346
Incorrectly Classified Instances: 22.24
Correctly Classified Instances: 77.76
Weighted Precision: 0.8026708393709959
Weighted AreaUnderROC: 0.9321351410227591
Root mean squared error: 0.32640669927765376
Relative absolute error: 48.3059999999991
Root relative squared error: 69.24131714518397
Weighted TruePositiveRate: 0.7776
Weighted MatthewsCorrelation: 0.6793306807013648
Weighted FMeasure: 0.7727747140216177
Iteration time: 422.0
Weighted AreaUnderPRC: 0.8461992765921347
Mean absolute error: 0.21469333333333038
Coverage of cases: 99.2
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 1484.0
Weighted Recall: 0.7776
Weighted FalsePositiveRate: 0.11221672729480453
Kappa statistic: 0.6660212948666752
Training time: 419.0
		
Iteration: 7
Labeled set size: 2300
Unlabelled set size: 200
	
Mean region size: 66.06666666666783
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8053925769891277
Weighted AreaUnderROC: 0.9418922350208266
Root mean squared error: 0.30391226804238425
Relative absolute error: 44.80799999999888
Root relative squared error: 64.46952768556592
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7045431647617346
Weighted FMeasure: 0.8020942403196741
Iteration time: 495.0
Weighted AreaUnderPRC: 0.8685668159545258
Mean absolute error: 0.19914666666666261
Coverage of cases: 99.72
Instances selection time: 2.0
Test time: 14.0
Accumulative iteration time: 1979.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09942745403018712
Kappa statistic: 0.7028785129663818
Training time: 493.0
		
Time end:Sun Oct 08 12.23.35 EEST 2017