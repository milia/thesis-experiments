Sun Oct 08 11.21.53 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 11.21.53 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.72000000000142
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.798492499532758
Weighted AreaUnderROC: 0.9326357748147984
Root mean squared error: 0.3167838379715718
Relative absolute error: 49.99199999999897
Root relative squared error: 67.19999999999952
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.6975188186673288
Weighted FMeasure: 0.7983132234985709
Iteration time: 98.0
Weighted AreaUnderPRC: 0.8439564839188428
Mean absolute error: 0.22218666666666312
Coverage of cases: 99.76
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.10094822608159582
Kappa statistic: 0.697588339006352
Training time: 89.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 71.53333333333488
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8013773878992956
Weighted AreaUnderROC: 0.9334938188430353
Root mean squared error: 0.318567627566475
Relative absolute error: 50.95799999999901
Root relative squared error: 67.57839891562934
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7015840279740909
Weighted FMeasure: 0.800183449811651
Iteration time: 105.0
Weighted AreaUnderPRC: 0.8471957297878479
Mean absolute error: 0.22647999999999666
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 203.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09966922297305728
Kappa statistic: 0.7012227548749886
Training time: 96.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 71.92000000000148
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.7935068488983227
Weighted AreaUnderROC: 0.931434444965874
Root mean squared error: 0.3200541620830226
Relative absolute error: 51.347999999999004
Root relative squared error: 67.89374050676496
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.6894387264259138
Weighted FMeasure: 0.7928363757096024
Iteration time: 106.0
Weighted AreaUnderPRC: 0.8421054437968433
Mean absolute error: 0.22821333333333
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 309.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10384874952608389
Kappa statistic: 0.6891195192611462
Training time: 97.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 70.53333333333488
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.7966367226096412
Weighted AreaUnderROC: 0.9344450351413172
Root mean squared error: 0.31576784721268325
Relative absolute error: 49.58399999999891
Root relative squared error: 66.98447581342964
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.6934060495637107
Weighted FMeasure: 0.7951766506371687
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8494598379822897
Mean absolute error: 0.22037333333332954
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 419.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.10268174732172984
Kappa statistic: 0.6927173532593326
Training time: 102.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 70.81333333333481
Incorrectly Classified Instances: 20.84
Correctly Classified Instances: 79.16
Weighted Precision: 0.7929249385187426
Weighted AreaUnderROC: 0.9333821169951326
Root mean squared error: 0.316640700689808
Relative absolute error: 49.847999999998905
Root relative squared error: 67.16963599722679
Weighted TruePositiveRate: 0.7916
Weighted MatthewsCorrelation: 0.6880772740579482
Weighted FMeasure: 0.7910501237368353
Iteration time: 115.0
Weighted AreaUnderPRC: 0.8460967951729688
Mean absolute error: 0.22154666666666284
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 534.0
Weighted Recall: 0.7916
Weighted FalsePositiveRate: 0.10447611091677173
Kappa statistic: 0.6873294114879375
Training time: 106.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 70.25333333333472
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.7941313840346156
Weighted AreaUnderROC: 0.9348291111810084
Root mean squared error: 0.3150999841320198
Relative absolute error: 49.58399999999894
Root relative squared error: 66.84280065945725
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.6897049847046847
Weighted FMeasure: 0.7928481491662472
Iteration time: 121.0
Weighted AreaUnderPRC: 0.8511134546280009
Mean absolute error: 0.22037333333332962
Coverage of cases: 99.8
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 655.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10394050947997771
Kappa statistic: 0.689101756154945
Training time: 112.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 71.02666666666808
Incorrectly Classified Instances: 20.56
Correctly Classified Instances: 79.44
Weighted Precision: 0.7975703102128626
Weighted AreaUnderROC: 0.9363805485936572
Root mean squared error: 0.31445190411253543
Relative absolute error: 49.925999999998886
Root relative squared error: 66.70532212649859
Weighted TruePositiveRate: 0.7944
Weighted MatthewsCorrelation: 0.6929861992680056
Weighted FMeasure: 0.7946759244006193
Iteration time: 123.0
Weighted AreaUnderPRC: 0.852364384669475
Mean absolute error: 0.22189333333332942
Coverage of cases: 99.68
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 778.0
Weighted Recall: 0.7944
Weighted FalsePositiveRate: 0.10329485354942353
Kappa statistic: 0.691449624889186
Training time: 114.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 70.38666666666799
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.7948654648956357
Weighted AreaUnderROC: 0.9356103902317314
Root mean squared error: 0.3136792416891269
Relative absolute error: 49.09199999999891
Root relative squared error: 66.54141567475052
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.6922044071714718
Weighted FMeasure: 0.7945689979308899
Iteration time: 131.0
Weighted AreaUnderPRC: 0.8513160601910907
Mean absolute error: 0.21818666666666287
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 909.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10261994814653208
Kappa statistic: 0.6922281672781306
Training time: 123.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 71.46666666666826
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8054982203869182
Weighted AreaUnderROC: 0.9387623201124208
Root mean squared error: 0.31205982332452337
Relative absolute error: 49.7519999999989
Root relative squared error: 66.19788516259378
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.707458473375383
Weighted FMeasure: 0.8046603313613204
Iteration time: 134.0
Weighted AreaUnderPRC: 0.8568587649430508
Mean absolute error: 0.22111999999999615
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 1043.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09785179621334521
Kappa statistic: 0.707134444974163
Training time: 125.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 71.29333333333496
Incorrectly Classified Instances: 21.84
Correctly Classified Instances: 78.16
Weighted Precision: 0.7857849225438537
Weighted AreaUnderROC: 0.932974206197037
Root mean squared error: 0.31855088552171
Relative absolute error: 51.08399999999892
Root relative squared error: 67.57484739161407
Weighted TruePositiveRate: 0.7816
Weighted MatthewsCorrelation: 0.67436380257389
Weighted FMeasure: 0.7818583339756883
Iteration time: 138.0
Weighted AreaUnderPRC: 0.8442281169915106
Mean absolute error: 0.22703999999999627
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1181.0
Weighted Recall: 0.7816
Weighted FalsePositiveRate: 0.10976600691156996
Kappa statistic: 0.6722042141330172
Training time: 130.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 71.28000000000151
Incorrectly Classified Instances: 20.64
Correctly Classified Instances: 79.36
Weighted Precision: 0.7985001930654114
Weighted AreaUnderROC: 0.9381825373524764
Root mean squared error: 0.31392143390769905
Relative absolute error: 50.33999999999892
Root relative squared error: 66.59279240278143
Weighted TruePositiveRate: 0.7936
Weighted MatthewsCorrelation: 0.6926596538372854
Weighted FMeasure: 0.7940503901789498
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8574220178636044
Mean absolute error: 0.22373333333332956
Coverage of cases: 99.84
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1326.0
Weighted Recall: 0.7936
Weighted FalsePositiveRate: 0.10381397838195312
Kappa statistic: 0.6902087224736041
Training time: 137.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 71.45333333333483
Incorrectly Classified Instances: 21.64
Correctly Classified Instances: 78.36
Weighted Precision: 0.7879270327672072
Weighted AreaUnderROC: 0.9320061980539158
Root mean squared error: 0.3192240592436591
Relative absolute error: 51.0299999999989
Root relative squared error: 67.7176491027261
Weighted TruePositiveRate: 0.7836
Weighted MatthewsCorrelation: 0.677380443831089
Weighted FMeasure: 0.7838989454371736
Iteration time: 150.0
Weighted AreaUnderPRC: 0.8437848768582858
Mean absolute error: 0.22679999999999614
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1476.0
Weighted Recall: 0.7836
Weighted FalsePositiveRate: 0.10878279964584955
Kappa statistic: 0.6752092828909134
Training time: 142.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 69.2933333333347
Incorrectly Classified Instances: 21.08
Correctly Classified Instances: 78.92
Weighted Precision: 0.7963549651931485
Weighted AreaUnderROC: 0.9384803944525909
Root mean squared error: 0.3115894735064058
Relative absolute error: 48.44399999999881
Root relative squared error: 66.09810889881754
Weighted TruePositiveRate: 0.7892
Weighted MatthewsCorrelation: 0.6874211937067868
Weighted FMeasure: 0.7894092775526159
Iteration time: 152.0
Weighted AreaUnderPRC: 0.8596993181894779
Mean absolute error: 0.2153066666666624
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1628.0
Weighted Recall: 0.7892
Weighted FalsePositiveRate: 0.10612167257181226
Kappa statistic: 0.6835493204296715
Training time: 144.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.34666666666821
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.7962998491539757
Weighted AreaUnderROC: 0.93606318333443
Root mean squared error: 0.31538759223110296
Relative absolute error: 49.9799999999989
Root relative squared error: 66.90381155061303
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.6864904744608248
Weighted FMeasure: 0.7888752368849388
Iteration time: 157.0
Weighted AreaUnderPRC: 0.8519979378859216
Mean absolute error: 0.22213333333332952
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1785.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.10656690143265837
Kappa statistic: 0.6823447067939561
Training time: 149.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 70.16000000000152
Incorrectly Classified Instances: 22.48
Correctly Classified Instances: 77.52
Weighted Precision: 0.7876329887105211
Weighted AreaUnderROC: 0.9342429638019092
Root mean squared error: 0.318801923875415
Relative absolute error: 50.591999999998954
Root relative squared error: 67.62810066828688
Weighted TruePositiveRate: 0.7752
Weighted MatthewsCorrelation: 0.6694280113974684
Weighted FMeasure: 0.7750727325960595
Iteration time: 163.0
Weighted AreaUnderPRC: 0.8476908568013101
Mean absolute error: 0.22485333333332974
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 1948.0
Weighted Recall: 0.7752
Weighted FalsePositiveRate: 0.11336338085866296
Kappa statistic: 0.6624353761959517
Training time: 154.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 72.50666666666818
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.7858891483354017
Weighted AreaUnderROC: 0.9304796233002067
Root mean squared error: 0.32379417330561366
Relative absolute error: 52.409999999998945
Root relative squared error: 68.68711669592732
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.6672417256585194
Weighted FMeasure: 0.7739521808317328
Iteration time: 170.0
Weighted AreaUnderPRC: 0.8375818407290382
Mean absolute error: 0.23293333333332975
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2118.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.11393803507746145
Kappa statistic: 0.6606468011906792
Training time: 162.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 70.90666666666823
Incorrectly Classified Instances: 22.4
Correctly Classified Instances: 77.6
Weighted Precision: 0.7882397982449233
Weighted AreaUnderROC: 0.9364592556480995
Root mean squared error: 0.3172296749465059
Relative absolute error: 50.537999999998995
Root relative squared error: 67.2945763044834
Weighted TruePositiveRate: 0.776
Weighted MatthewsCorrelation: 0.6705666908330576
Weighted FMeasure: 0.7758325953367445
Iteration time: 171.0
Weighted AreaUnderPRC: 0.8531504252550838
Mean absolute error: 0.22461333333332992
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2289.0
Weighted Recall: 0.776
Weighted FalsePositiveRate: 0.112951332778704
Kappa statistic: 0.6636379668164438
Training time: 163.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 70.92000000000158
Incorrectly Classified Instances: 22.64
Correctly Classified Instances: 77.36
Weighted Precision: 0.7970142314605949
Weighted AreaUnderROC: 0.939057148087707
Root mean squared error: 0.31842947937232885
Relative absolute error: 50.87999999999901
Root relative squared error: 67.54909325816251
Weighted TruePositiveRate: 0.7736
Weighted MatthewsCorrelation: 0.6731467995594084
Weighted FMeasure: 0.7735504701462597
Iteration time: 177.0
Weighted AreaUnderPRC: 0.8586975783687785
Mean absolute error: 0.22613333333333
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2466.0
Weighted Recall: 0.7736
Weighted FalsePositiveRate: 0.11452212837596258
Kappa statistic: 0.6599028596040547
Training time: 169.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 70.17333333333494
Incorrectly Classified Instances: 23.96
Correctly Classified Instances: 76.04
Weighted Precision: 0.7814108731936954
Weighted AreaUnderROC: 0.9333468924640838
Root mean squared error: 0.32282916018641505
Relative absolute error: 50.8199999999989
Root relative squared error: 68.48240649977153
Weighted TruePositiveRate: 0.7604
Weighted MatthewsCorrelation: 0.65195557973018
Weighted FMeasure: 0.7596564952558355
Iteration time: 181.0
Weighted AreaUnderPRC: 0.8488153892155271
Mean absolute error: 0.22586666666666283
Coverage of cases: 99.92
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 2647.0
Weighted Recall: 0.7604
Weighted FalsePositiveRate: 0.12106262104107282
Kappa statistic: 0.6400934242965659
Training time: 173.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 68.92000000000154
Incorrectly Classified Instances: 22.0
Correctly Classified Instances: 78.0
Weighted Precision: 0.7995724303467194
Weighted AreaUnderROC: 0.939633801810845
Root mean squared error: 0.3160759402422135
Relative absolute error: 49.24199999999896
Root relative squared error: 67.04983221455478
Weighted TruePositiveRate: 0.78
Weighted MatthewsCorrelation: 0.6808647801542761
Weighted FMeasure: 0.7795065467985871
Iteration time: 199.0
Weighted AreaUnderPRC: 0.8605897286654607
Mean absolute error: 0.21885333333332974
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 2846.0
Weighted Recall: 0.78
Weighted FalsePositiveRate: 0.11119082278320674
Kappa statistic: 0.6695496358076495
Training time: 191.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 70.60000000000143
Incorrectly Classified Instances: 24.08
Correctly Classified Instances: 75.92
Weighted Precision: 0.7903647930889002
Weighted AreaUnderROC: 0.9335148147455191
Root mean squared error: 0.3267455687024171
Relative absolute error: 51.383999999999055
Root relative squared error: 69.31320220564005
Weighted TruePositiveRate: 0.7592
Weighted MatthewsCorrelation: 0.6563335222159811
Weighted FMeasure: 0.7573131916111575
Iteration time: 192.0
Weighted AreaUnderPRC: 0.8446807011047829
Mean absolute error: 0.2283733333333302
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3038.0
Weighted Recall: 0.7592
Weighted FalsePositiveRate: 0.12193588759921217
Kappa statistic: 0.6381635731455342
Training time: 184.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 69.13333333333483
Incorrectly Classified Instances: 22.76
Correctly Classified Instances: 77.24
Weighted Precision: 0.8062620360667846
Weighted AreaUnderROC: 0.9366208453146868
Root mean squared error: 0.3221511032626344
Relative absolute error: 49.70399999999905
Root relative squared error: 68.3385689051208
Weighted TruePositiveRate: 0.7724
Weighted MatthewsCorrelation: 0.6773162251608805
Weighted FMeasure: 0.7721642106871934
Iteration time: 196.0
Weighted AreaUnderPRC: 0.8545194967389276
Mean absolute error: 0.22090666666666348
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 3234.0
Weighted Recall: 0.7724
Weighted FalsePositiveRate: 0.11539110757837147
Kappa statistic: 0.6579978025306961
Training time: 188.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 68.76000000000145
Incorrectly Classified Instances: 22.92
Correctly Classified Instances: 77.08
Weighted Precision: 0.8017423588955089
Weighted AreaUnderROC: 0.934613750782291
Root mean squared error: 0.3216001658374354
Relative absolute error: 49.205999999999094
Root relative squared error: 68.22169742831049
Weighted TruePositiveRate: 0.7708
Weighted MatthewsCorrelation: 0.6736707469782476
Weighted FMeasure: 0.7698852881626154
Iteration time: 201.0
Weighted AreaUnderPRC: 0.8502083755693639
Mean absolute error: 0.2186933333333303
Coverage of cases: 99.36
Instances selection time: 9.0
Test time: 19.0
Accumulative iteration time: 3435.0
Weighted Recall: 0.7708
Weighted FalsePositiveRate: 0.1161112185130875
Kappa statistic: 0.6556084731134197
Training time: 192.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.42666666666834
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7736915237879621
Weighted AreaUnderROC: 0.9257961760206744
Root mean squared error: 0.33318663438579404
Relative absolute error: 52.253999999999095
Root relative squared error: 70.6795585724752
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.6331905280151503
Weighted FMeasure: 0.7416639685469206
Iteration time: 206.0
Weighted AreaUnderPRC: 0.832037001924809
Mean absolute error: 0.23223999999999706
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 3641.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.1295400700621321
Kappa statistic: 0.6153117465519912
Training time: 198.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 70.22666666666808
Incorrectly Classified Instances: 24.52
Correctly Classified Instances: 75.48
Weighted Precision: 0.7891743486479523
Weighted AreaUnderROC: 0.9283408755480963
Root mean squared error: 0.3309662621275258
Relative absolute error: 51.713999999999125
Root relative squared error: 70.2085464883012
Weighted TruePositiveRate: 0.7548
Weighted MatthewsCorrelation: 0.6518340371692397
Weighted FMeasure: 0.75289393176913
Iteration time: 210.0
Weighted AreaUnderPRC: 0.8345110099374926
Mean absolute error: 0.22983999999999719
Coverage of cases: 99.48
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 3851.0
Weighted Recall: 0.7548
Weighted FalsePositiveRate: 0.12421412933143636
Kappa statistic: 0.6315216460895852
Training time: 202.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 69.62666666666826
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7874910259769902
Weighted AreaUnderROC: 0.9278113190552947
Root mean squared error: 0.33309858400579595
Relative absolute error: 51.233999999999135
Root relative squared error: 70.66088026624038
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.6471300742396026
Weighted FMeasure: 0.7483222701520286
Iteration time: 214.0
Weighted AreaUnderPRC: 0.8347347631626609
Mean absolute error: 0.2277066666666639
Coverage of cases: 99.36
Instances selection time: 7.0
Test time: 20.0
Accumulative iteration time: 4065.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.12628659500253928
Kappa statistic: 0.625474198959648
Training time: 207.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 70.06666666666811
Incorrectly Classified Instances: 24.08
Correctly Classified Instances: 75.92
Weighted Precision: 0.795763426461841
Weighted AreaUnderROC: 0.9338990351291239
Root mean squared error: 0.3275973137862995
Relative absolute error: 50.99999999999913
Root relative squared error: 69.49388462303673
Weighted TruePositiveRate: 0.7592
Weighted MatthewsCorrelation: 0.6596960869648126
Weighted FMeasure: 0.7575801619264854
Iteration time: 218.0
Weighted AreaUnderPRC: 0.8477041732009
Mean absolute error: 0.22666666666666388
Coverage of cases: 99.44
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4283.0
Weighted Recall: 0.7592
Weighted FalsePositiveRate: 0.12206113938761325
Kappa statistic: 0.6381136318386905
Training time: 211.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 69.72000000000156
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.7865161211989786
Weighted AreaUnderROC: 0.927447234890745
Root mean squared error: 0.3336305341741563
Relative absolute error: 52.29599999999917
Root relative squared error: 70.77372393763066
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.6438292096988394
Weighted FMeasure: 0.746334400514586
Iteration time: 224.0
Weighted AreaUnderPRC: 0.8334437921260436
Mean absolute error: 0.23242666666666406
Coverage of cases: 99.6
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 4507.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.12774709384503616
Kappa statistic: 0.6212547505475574
Training time: 216.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 68.62666666666814
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7963955563793635
Weighted AreaUnderROC: 0.9321517305169197
Root mean squared error: 0.329099782234304
Relative absolute error: 50.27399999999926
Root relative squared error: 69.81260631146756
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.6569793148224411
Weighted FMeasure: 0.7541173784662614
Iteration time: 232.0
Weighted AreaUnderPRC: 0.8420517439376208
Mean absolute error: 0.22343999999999778
Coverage of cases: 99.36
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 4739.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.12376497487989718
Kappa statistic: 0.6332653571634699
Training time: 225.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 69.69333333333498
Incorrectly Classified Instances: 26.32
Correctly Classified Instances: 73.68
Weighted Precision: 0.7827573296799103
Weighted AreaUnderROC: 0.9268392903338271
Root mean squared error: 0.3371132351797136
Relative absolute error: 52.42799999999911
Root relative squared error: 71.5125163869931
Weighted TruePositiveRate: 0.7368
Weighted MatthewsCorrelation: 0.631232772145989
Weighted FMeasure: 0.7334392235532005
Iteration time: 233.0
Weighted AreaUnderPRC: 0.832253752839406
Mean absolute error: 0.23301333333333046
Coverage of cases: 99.36
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 4972.0
Weighted Recall: 0.7368
Weighted FalsePositiveRate: 0.13350877689838772
Kappa statistic: 0.6043417063768091
Training time: 225.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 68.18666666666809
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.7981259568899712
Weighted AreaUnderROC: 0.9392010145506321
Root mean squared error: 0.3216001658374357
Relative absolute error: 49.48199999999914
Root relative squared error: 68.22169742831055
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.6652748614813033
Weighted FMeasure: 0.7627881294353086
Iteration time: 241.0
Weighted AreaUnderPRC: 0.8608659567880228
Mean absolute error: 0.2199199999999972
Coverage of cases: 99.68
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 5213.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.11960584480397128
Kappa statistic: 0.645354261038499
Training time: 231.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 70.13333333333478
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.783810567687086
Weighted AreaUnderROC: 0.9301366113724272
Root mean squared error: 0.3287592837725898
Relative absolute error: 51.347999999999246
Root relative squared error: 69.74037568008906
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6458576500711313
Weighted FMeasure: 0.7500875945915118
Iteration time: 244.0
Weighted AreaUnderPRC: 0.8395217097074783
Mean absolute error: 0.22821333333333105
Coverage of cases: 99.68
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5457.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12557525604672512
Kappa statistic: 0.6273330752715058
Training time: 235.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 70.26666666666804
Incorrectly Classified Instances: 24.72
Correctly Classified Instances: 75.28
Weighted Precision: 0.7857468611282795
Weighted AreaUnderROC: 0.927544198148559
Root mean squared error: 0.33178306165324273
Relative absolute error: 51.53399999999914
Root relative squared error: 70.38181583335253
Weighted TruePositiveRate: 0.7528
Weighted MatthewsCorrelation: 0.647667941240268
Weighted FMeasure: 0.7514664638457119
Iteration time: 249.0
Weighted AreaUnderPRC: 0.8341681333551602
Mean absolute error: 0.22903999999999725
Coverage of cases: 99.44
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 5706.0
Weighted Recall: 0.7528
Weighted FalsePositiveRate: 0.1252051189859243
Kappa statistic: 0.6285287961111173
Training time: 240.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 69.58666666666818
Incorrectly Classified Instances: 24.16
Correctly Classified Instances: 75.84
Weighted Precision: 0.7984665899698177
Weighted AreaUnderROC: 0.9294763871045643
Root mean squared error: 0.33128034452207744
Relative absolute error: 51.239999999999185
Root relative squared error: 70.27517342561283
Weighted TruePositiveRate: 0.7584
Weighted MatthewsCorrelation: 0.659831516263137
Weighted FMeasure: 0.7571599198690777
Iteration time: 251.0
Weighted AreaUnderPRC: 0.8391451602181725
Mean absolute error: 0.22773333333333076
Coverage of cases: 99.48
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 5957.0
Weighted Recall: 0.7584
Weighted FalsePositiveRate: 0.1225547103012132
Kappa statistic: 0.6368924922833642
Training time: 246.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 69.04000000000148
Incorrectly Classified Instances: 24.28
Correctly Classified Instances: 75.72
Weighted Precision: 0.7914638897358295
Weighted AreaUnderROC: 0.9318103546793396
Root mean squared error: 0.32743447181586244
Relative absolute error: 50.52599999999911
Root relative squared error: 69.45934062456938
Weighted TruePositiveRate: 0.7572
Weighted MatthewsCorrelation: 0.6550626215376804
Weighted FMeasure: 0.7552767097881786
Iteration time: 260.0
Weighted AreaUnderPRC: 0.8426229482943274
Mean absolute error: 0.22455999999999707
Coverage of cases: 99.52
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 6217.0
Weighted Recall: 0.7572
Weighted FalsePositiveRate: 0.12300942570024961
Kappa statistic: 0.6351298649150977
Training time: 254.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 69.53333333333474
Incorrectly Classified Instances: 23.8
Correctly Classified Instances: 76.2
Weighted Precision: 0.7926579573044016
Weighted AreaUnderROC: 0.9332033977422043
Root mean squared error: 0.3262105659437362
Relative absolute error: 50.405999999999175
Root relative squared error: 69.19971098205502
Weighted TruePositiveRate: 0.762
Weighted MatthewsCorrelation: 0.6602537665602874
Weighted FMeasure: 0.7591060660435412
Iteration time: 263.0
Weighted AreaUnderPRC: 0.8466581506245888
Mean absolute error: 0.22402666666666404
Coverage of cases: 99.64
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6480.0
Weighted Recall: 0.762
Weighted FalsePositiveRate: 0.12049570747041098
Kappa statistic: 0.6423775079854999
Training time: 257.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 69.1866666666681
Incorrectly Classified Instances: 24.56
Correctly Classified Instances: 75.44
Weighted Precision: 0.7850553503498772
Weighted AreaUnderROC: 0.9240372000581658
Root mean squared error: 0.33379834231663413
Relative absolute error: 51.24599999999937
Root relative squared error: 70.80932142027599
Weighted TruePositiveRate: 0.7544
Weighted MatthewsCorrelation: 0.6481728639952135
Weighted FMeasure: 0.7528375441552672
Iteration time: 266.0
Weighted AreaUnderPRC: 0.8285365263762875
Mean absolute error: 0.22775999999999827
Coverage of cases: 99.4
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 6746.0
Weighted Recall: 0.7544
Weighted FalsePositiveRate: 0.12430561997234096
Kappa statistic: 0.6309812293253326
Training time: 261.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 69.33333333333486
Incorrectly Classified Instances: 23.84
Correctly Classified Instances: 76.16
Weighted Precision: 0.7919822322622102
Weighted AreaUnderROC: 0.9281898910317649
Root mean squared error: 0.32830067519475625
Relative absolute error: 50.93399999999912
Root relative squared error: 69.64309010950014
Weighted TruePositiveRate: 0.7616
Weighted MatthewsCorrelation: 0.6586576793857732
Weighted FMeasure: 0.7605375946672606
Iteration time: 272.0
Weighted AreaUnderPRC: 0.8363921825099494
Mean absolute error: 0.22637333333333048
Coverage of cases: 99.56
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7018.0
Weighted Recall: 0.7616
Weighted FalsePositiveRate: 0.12069593727267813
Kappa statistic: 0.6418126300232174
Training time: 266.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 68.17333333333488
Incorrectly Classified Instances: 23.48
Correctly Classified Instances: 76.52
Weighted Precision: 0.7960148877508032
Weighted AreaUnderROC: 0.9350958188668342
Root mean squared error: 0.32288697712976816
Relative absolute error: 49.48199999999914
Root relative squared error: 68.49467132558526
Weighted TruePositiveRate: 0.7652
Weighted MatthewsCorrelation: 0.6645976196769043
Weighted FMeasure: 0.7629028868061188
Iteration time: 276.0
Weighted AreaUnderPRC: 0.8506288686946134
Mean absolute error: 0.2199199999999972
Coverage of cases: 99.76
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 7294.0
Weighted Recall: 0.7652
Weighted FalsePositiveRate: 0.11884871097893683
Kappa statistic: 0.6472138670072004
Training time: 270.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 69.16000000000166
Incorrectly Classified Instances: 22.44
Correctly Classified Instances: 77.56
Weighted Precision: 0.8019769601058827
Weighted AreaUnderROC: 0.9313965563640806
Root mean squared error: 0.32329140621633107
Relative absolute error: 49.691999999999155
Root relative squared error: 68.58046369047058
Weighted TruePositiveRate: 0.7756
Weighted MatthewsCorrelation: 0.6774062028199894
Weighted FMeasure: 0.7747643251140391
Iteration time: 278.0
Weighted AreaUnderPRC: 0.8405217051589178
Mean absolute error: 0.2208533333333306
Coverage of cases: 99.44
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7572.0
Weighted Recall: 0.7756
Weighted FalsePositiveRate: 0.11356837356787716
Kappa statistic: 0.6628959080250242
Training time: 272.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 68.12000000000137
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.7969374529748148
Weighted AreaUnderROC: 0.9316248553801122
Root mean squared error: 0.32469472842451
Relative absolute error: 49.72199999999904
Root relative squared error: 68.87815328534849
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.669583797203209
Weighted FMeasure: 0.7668212740509811
Iteration time: 294.0
Weighted AreaUnderPRC: 0.840920211749092
Mean absolute error: 0.22098666666666344
Coverage of cases: 99.52
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 7866.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.11633792874965686
Kappa statistic: 0.6544695416403262
Training time: 288.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 69.18666666666812
Incorrectly Classified Instances: 23.84
Correctly Classified Instances: 76.16
Weighted Precision: 0.7903217127646825
Weighted AreaUnderROC: 0.930923013096961
Root mean squared error: 0.3269984709444367
Relative absolute error: 50.28599999999909
Root relative squared error: 69.36685087273285
Weighted TruePositiveRate: 0.7616
Weighted MatthewsCorrelation: 0.658152725173587
Weighted FMeasure: 0.7582903402540676
Iteration time: 298.0
Weighted AreaUnderPRC: 0.8405022449700058
Mean absolute error: 0.22349333333333035
Coverage of cases: 99.48
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8164.0
Weighted Recall: 0.7616
Weighted FalsePositiveRate: 0.12060044899553218
Kappa statistic: 0.6418172796959919
Training time: 292.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 68.28000000000151
Incorrectly Classified Instances: 23.12
Correctly Classified Instances: 76.88
Weighted Precision: 0.7989796172934543
Weighted AreaUnderROC: 0.9316302654745529
Root mean squared error: 0.3249369169546599
Relative absolute error: 49.445999999998996
Root relative squared error: 68.92952923094686
Weighted TruePositiveRate: 0.7688
Weighted MatthewsCorrelation: 0.6692206860058153
Weighted FMeasure: 0.765766219784664
Iteration time: 311.0
Weighted AreaUnderPRC: 0.8451912919478362
Mean absolute error: 0.21975999999999657
Coverage of cases: 99.48
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 8475.0
Weighted Recall: 0.7688
Weighted FalsePositiveRate: 0.11700950036417812
Kappa statistic: 0.6526510775508753
Training time: 306.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 69.96000000000147
Incorrectly Classified Instances: 24.12
Correctly Classified Instances: 75.88
Weighted Precision: 0.7844458409866993
Weighted AreaUnderROC: 0.9287438994783294
Root mean squared error: 0.3287673949770558
Relative absolute error: 51.56399999999907
Root relative squared error: 69.74209632639351
Weighted TruePositiveRate: 0.7588
Weighted MatthewsCorrelation: 0.6518014236118552
Weighted FMeasure: 0.756185541072522
Iteration time: 306.0
Weighted AreaUnderPRC: 0.8368638553491532
Mean absolute error: 0.22917333333333026
Coverage of cases: 99.68
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 8781.0
Weighted Recall: 0.7588
Weighted FalsePositiveRate: 0.12189449958975829
Kappa statistic: 0.6376682709409713
Training time: 300.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 69.466666666668
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.8018228146857981
Weighted AreaUnderROC: 0.9320877299228004
Root mean squared error: 0.3249984615348195
Relative absolute error: 50.14199999999906
Root relative squared error: 68.9425848079397
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.6754058869926183
Weighted FMeasure: 0.7717492582300474
Iteration time: 311.0
Weighted AreaUnderPRC: 0.8426175642607745
Mean absolute error: 0.22285333333333018
Coverage of cases: 99.44
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9092.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.11427666273695347
Kappa statistic: 0.6605164668140125
Training time: 306.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 68.01333333333486
Incorrectly Classified Instances: 22.36
Correctly Classified Instances: 77.64
Weighted Precision: 0.8063162980392157
Weighted AreaUnderROC: 0.9346517148052557
Root mean squared error: 0.3234316001877357
Relative absolute error: 49.247999999999045
Root relative squared error: 68.61020332282908
Weighted TruePositiveRate: 0.7764
Weighted MatthewsCorrelation: 0.6798103115883214
Weighted FMeasure: 0.7734639690758672
Iteration time: 315.0
Weighted AreaUnderPRC: 0.8500664508298683
Mean absolute error: 0.2188799999999968
Coverage of cases: 99.64
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9407.0
Weighted Recall: 0.7764
Weighted FalsePositiveRate: 0.11308735366784854
Kappa statistic: 0.6641270909440492
Training time: 310.0
		
Time end:Sun Oct 08 11.22.07 EEST 2017