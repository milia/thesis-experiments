Sun Oct 08 10.40.04 EEST 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.40.04 EEST 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 36.693333333334294
Incorrectly Classified Instances: 29.44
Correctly Classified Instances: 70.56
Weighted Precision: 0.7075308805412464
Weighted AreaUnderROC: 0.7907973942855068
Root mean squared error: 0.4292974967249255
Relative absolute error: 44.906031079427
Root relative squared error: 91.06775132418113
Weighted TruePositiveRate: 0.7056
Weighted MatthewsCorrelation: 0.5591148712129792
Weighted FMeasure: 0.7057235196644348
Iteration time: 88.0
Weighted AreaUnderPRC: 0.61616853788173
Mean absolute error: 0.19958236035300983
Coverage of cases: 75.28
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7056
Weighted FalsePositiveRate: 0.14755522748108138
Kappa statistic: 0.5582784382261998
Training time: 85.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 37.16000000000089
Incorrectly Classified Instances: 30.76
Correctly Classified Instances: 69.24
Weighted Precision: 0.6972297253199681
Weighted AreaUnderROC: 0.7885870929387296
Root mean squared error: 0.43838940408085214
Relative absolute error: 46.586088509316674
Root relative squared error: 92.99643612776981
Weighted TruePositiveRate: 0.6924
Weighted MatthewsCorrelation: 0.5404606414827364
Weighted FMeasure: 0.6932369057324711
Iteration time: 82.0
Weighted AreaUnderPRC: 0.6137106967941285
Mean absolute error: 0.20704928226363062
Coverage of cases: 74.48
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 170.0
Weighted Recall: 0.6924
Weighted FalsePositiveRate: 0.15434964099262574
Kappa statistic: 0.5384166960383308
Training time: 80.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 35.73333333333422
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7277400104707191
Weighted AreaUnderROC: 0.8020316975431279
Root mean squared error: 0.4194156022106362
Relative absolute error: 42.00616339410932
Root relative squared error: 88.9714849375739
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5899597697429606
Weighted FMeasure: 0.726794943759751
Iteration time: 89.0
Weighted AreaUnderPRC: 0.6314702118933188
Mean absolute error: 0.18669405952937562
Coverage of cases: 75.36
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 259.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.1371302117830692
Kappa statistic: 0.5894984583386547
Training time: 86.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 35.82666666666764
Incorrectly Classified Instances: 32.28
Correctly Classified Instances: 67.72
Weighted Precision: 0.6767839959791738
Weighted AreaUnderROC: 0.759729726118981
Root mean squared error: 0.4551241881217571
Relative absolute error: 49.26269820788522
Root relative squared error: 96.54641991087469
Weighted TruePositiveRate: 0.6772
Weighted MatthewsCorrelation: 0.5154397899189862
Weighted FMeasure: 0.6769303432103592
Iteration time: 92.0
Weighted AreaUnderPRC: 0.5730465718140489
Mean absolute error: 0.21894532536837977
Coverage of cases: 70.32
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 351.0
Weighted Recall: 0.6772
Weighted FalsePositiveRate: 0.16157314393872863
Kappa statistic: 0.5158192122936562
Training time: 89.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 36.09333333333429
Incorrectly Classified Instances: 29.24
Correctly Classified Instances: 70.76
Weighted Precision: 0.7072389611647187
Weighted AreaUnderROC: 0.7853711396823538
Root mean squared error: 0.4327388204561771
Relative absolute error: 45.319880026066926
Root relative squared error: 91.79776632816899
Weighted TruePositiveRate: 0.7076
Weighted MatthewsCorrelation: 0.5612302742544152
Weighted FMeasure: 0.7071411628235608
Iteration time: 97.0
Weighted AreaUnderPRC: 0.6054099152068971
Mean absolute error: 0.20142168900474286
Coverage of cases: 73.12
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 448.0
Weighted Recall: 0.7076
Weighted FalsePositiveRate: 0.1462457880426302
Kappa statistic: 0.5614365586484711
Training time: 94.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 37.80000000000113
Incorrectly Classified Instances: 30.68
Correctly Classified Instances: 69.32
Weighted Precision: 0.6931866048233898
Weighted AreaUnderROC: 0.7830946199505741
Root mean squared error: 0.4416164037165707
Relative absolute error: 47.57178214497895
Root relative squared error: 93.68098612536075
Weighted TruePositiveRate: 0.6932
Weighted MatthewsCorrelation: 0.5396024794894719
Weighted FMeasure: 0.6931682488211321
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6008895180639542
Mean absolute error: 0.21143014286657408
Coverage of cases: 74.04
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 556.0
Weighted Recall: 0.6932
Weighted FalsePositiveRate: 0.15359818761912447
Kappa statistic: 0.539774742837887
Training time: 105.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 36.093333333334215
Incorrectly Classified Instances: 29.28
Correctly Classified Instances: 70.72
Weighted Precision: 0.7072807826593902
Weighted AreaUnderROC: 0.7908645264235246
Root mean squared error: 0.43197010459365515
Relative absolute error: 45.3418757197756
Root relative squared error: 91.63469706841052
Weighted TruePositiveRate: 0.7072
Weighted MatthewsCorrelation: 0.5607021249556569
Weighted FMeasure: 0.7070811316826321
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6149611421312631
Mean absolute error: 0.20151944764344804
Coverage of cases: 74.08
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 659.0
Weighted Recall: 0.7072
Weighted FalsePositiveRate: 0.14659127729337396
Kappa statistic: 0.5607600818762409
Training time: 100.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 38.72000000000098
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7152431149541548
Weighted AreaUnderROC: 0.8056081800727217
Root mean squared error: 0.4226726131442001
Relative absolute error: 44.08188404100612
Root relative squared error: 89.66240129283044
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.5713102040322204
Weighted FMeasure: 0.7143150809124962
Iteration time: 112.0
Weighted AreaUnderPRC: 0.6414406752242117
Mean absolute error: 0.19591948462669478
Coverage of cases: 78.36
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 771.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.14337137183169335
Kappa statistic: 0.5708771678805341
Training time: 109.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 35.880000000000905
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7211624267261157
Weighted AreaUnderROC: 0.7986040786990612
Root mean squared error: 0.4237148955208377
Relative absolute error: 42.93939610389588
Root relative squared error: 89.88350277375993
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5801838767539772
Weighted FMeasure: 0.7203927741394107
Iteration time: 109.0
Weighted AreaUnderPRC: 0.6241838593188593
Mean absolute error: 0.19084176046176035
Coverage of cases: 75.04
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 880.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.14040428955799011
Kappa statistic: 0.5799003187476324
Training time: 107.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 36.52000000000094
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7020188950637463
Weighted AreaUnderROC: 0.7874405069061818
Root mean squared error: 0.43275386682937866
Relative absolute error: 45.63970343835444
Root relative squared error: 91.80095814592592
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5527941484445741
Weighted FMeasure: 0.7019522223218768
Iteration time: 123.0
Weighted AreaUnderPRC: 0.6129001695993255
Mean absolute error: 0.20284312639268734
Coverage of cases: 74.36
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1003.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.14925402503557228
Kappa statistic: 0.5529520764625968
Training time: 120.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 38.30666666666786
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7192699351989037
Weighted AreaUnderROC: 0.8033330894714426
Root mean squared error: 0.4157234568311438
Relative absolute error: 44.43296393341607
Root relative squared error: 88.18826262708421
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.579368920396531
Weighted FMeasure: 0.7172520258284049
Iteration time: 121.0
Weighted AreaUnderPRC: 0.6244815962960772
Mean absolute error: 0.19747983970407235
Coverage of cases: 77.4
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1124.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.14026975421669644
Kappa statistic: 0.5789500958698243
Training time: 118.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 39.92000000000134
Incorrectly Classified Instances: 29.32
Correctly Classified Instances: 70.68
Weighted Precision: 0.7066965049427187
Weighted AreaUnderROC: 0.7911459978064775
Root mean squared error: 0.4242386136653788
Relative absolute error: 46.57019516620857
Root relative squared error: 89.99460016919056
Weighted TruePositiveRate: 0.7068
Weighted MatthewsCorrelation: 0.5608320662107311
Weighted FMeasure: 0.7041307663007353
Iteration time: 130.0
Weighted AreaUnderPRC: 0.6128300176045239
Mean absolute error: 0.20697864518315018
Coverage of cases: 77.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1254.0
Weighted Recall: 0.7068
Weighted FalsePositiveRate: 0.1464516982964305
Kappa statistic: 0.5604048161880416
Training time: 127.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 37.50666666666778
Incorrectly Classified Instances: 29.28
Correctly Classified Instances: 70.72
Weighted Precision: 0.7069742228725121
Weighted AreaUnderROC: 0.7897163877720164
Root mean squared error: 0.42636278775029746
Relative absolute error: 45.53371257950706
Root relative squared error: 90.44520553915058
Weighted TruePositiveRate: 0.7072
Weighted MatthewsCorrelation: 0.5613535908711226
Weighted FMeasure: 0.7045657566925148
Iteration time: 146.0
Weighted AreaUnderPRC: 0.6156185307795898
Mean absolute error: 0.2023720559089212
Coverage of cases: 76.36
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 1400.0
Weighted Recall: 0.7072
Weighted FalsePositiveRate: 0.14630560478462953
Kappa statistic: 0.5609847376224085
Training time: 138.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 36.893333333334446
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.712044621644022
Weighted AreaUnderROC: 0.7879948675195328
Root mean squared error: 0.42679365780868517
Relative absolute error: 45.01897411890542
Root relative squared error: 90.53660688117944
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5691026348603977
Weighted FMeasure: 0.7112659517249392
Iteration time: 140.0
Weighted AreaUnderPRC: 0.613229710867343
Mean absolute error: 0.20008432941735838
Coverage of cases: 75.76
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1540.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.1436235222010132
Kappa statistic: 0.5693049517693528
Training time: 138.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 36.86666666666775
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7190869303128685
Weighted AreaUnderROC: 0.8020076240116694
Root mean squared error: 0.4225420607077594
Relative absolute error: 43.27608016290407
Root relative squared error: 89.63470693889815
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5790646266485078
Weighted FMeasure: 0.7192023559407354
Iteration time: 133.0
Weighted AreaUnderPRC: 0.6274788281021303
Mean absolute error: 0.19233813405735234
Coverage of cases: 75.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1673.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.1403363850914599
Kappa statistic: 0.5794154775104275
Training time: 130.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 37.4133333333344
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7190636650869766
Weighted AreaUnderROC: 0.7893838282215438
Root mean squared error: 0.4239620821838663
Relative absolute error: 43.74004374833807
Root relative squared error: 89.93593898345385
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5792403740510612
Weighted FMeasure: 0.7186573101303069
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6097323712103078
Mean absolute error: 0.194400194437059
Coverage of cases: 76.16
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1822.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14027987183291338
Kappa statistic: 0.5794518115368187
Training time: 146.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 37.746666666667615
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7217449569694075
Weighted AreaUnderROC: 0.812844179279528
Root mean squared error: 0.41834227123516593
Relative absolute error: 42.616721424725554
Root relative squared error: 88.74379705421012
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.582787557496586
Weighted FMeasure: 0.7216046341118858
Iteration time: 142.0
Weighted AreaUnderPRC: 0.6511199527039253
Mean absolute error: 0.1894076507765589
Coverage of cases: 77.48
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1964.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.1391665246499522
Kappa statistic: 0.5830027355020551
Training time: 140.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 36.98666666666761
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32
Weighted Precision: 0.7231223501142309
Weighted AreaUnderROC: 0.8132595401843984
Root mean squared error: 0.4187776664809324
Relative absolute error: 42.67194883318352
Root relative squared error: 88.83615833344349
Weighted TruePositiveRate: 0.7232
Weighted MatthewsCorrelation: 0.5847226860524344
Weighted FMeasure: 0.7226640226321417
Iteration time: 147.0
Weighted AreaUnderPRC: 0.6479709910591147
Mean absolute error: 0.18965310592526097
Coverage of cases: 76.84
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2111.0
Weighted Recall: 0.7232
Weighted FalsePositiveRate: 0.13856567971634068
Kappa statistic: 0.5848075066802793
Training time: 145.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 37.253333333334275
Incorrectly Classified Instances: 29.04
Correctly Classified Instances: 70.96
Weighted Precision: 0.7094435890590044
Weighted AreaUnderROC: 0.8028656154137811
Root mean squared error: 0.430518198140193
Relative absolute error: 44.77766129000128
Root relative squared error: 91.32670119874302
Weighted TruePositiveRate: 0.7096
Weighted MatthewsCorrelation: 0.5643112080028989
Weighted FMeasure: 0.7090941785708829
Iteration time: 144.0
Weighted AreaUnderPRC: 0.6346631395360238
Mean absolute error: 0.19901182795556216
Coverage of cases: 75.36
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2255.0
Weighted Recall: 0.7096
Weighted FalsePositiveRate: 0.14532679866081877
Kappa statistic: 0.564404530192886
Training time: 141.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 37.56000000000088
Incorrectly Classified Instances: 28.24
Correctly Classified Instances: 71.76
Weighted Precision: 0.7209874722782436
Weighted AreaUnderROC: 0.8191758951774116
Root mean squared error: 0.4218753997275113
Relative absolute error: 42.88735968103031
Root relative squared error: 89.49328678893238
Weighted TruePositiveRate: 0.7176
Weighted MatthewsCorrelation: 0.5779052217073248
Weighted FMeasure: 0.717533973214989
Iteration time: 158.0
Weighted AreaUnderPRC: 0.6599972584065096
Mean absolute error: 0.19061048747124673
Coverage of cases: 76.96
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2413.0
Weighted Recall: 0.7176
Weighted FalsePositiveRate: 0.14155622294252562
Kappa statistic: 0.5762852241415155
Training time: 156.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 37.986666666667645
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7153995576126659
Weighted AreaUnderROC: 0.8079203126854765
Root mean squared error: 0.42827430843774456
Relative absolute error: 44.137605838999185
Root relative squared error: 90.85070031129226
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5692313018296478
Weighted FMeasure: 0.7112683532686331
Iteration time: 161.0
Weighted AreaUnderPRC: 0.643950588473588
Mean absolute error: 0.1961671370622195
Coverage of cases: 75.6
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2574.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14448195322954913
Kappa statistic: 0.5673301497793803
Training time: 159.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 38.453333333334385
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7201328524911389
Weighted AreaUnderROC: 0.8044390986019307
Root mean squared error: 0.42214538154445824
Relative absolute error: 43.31429416289407
Root relative squared error: 89.55055858100044
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5796560294977933
Weighted FMeasure: 0.7191899788111963
Iteration time: 166.0
Weighted AreaUnderPRC: 0.6346699228053624
Mean absolute error: 0.1925079740573079
Coverage of cases: 76.6
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2740.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14029273390010483
Kappa statistic: 0.5794294062959119
Training time: 164.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 39.09333333333426
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7128722366522366
Weighted AreaUnderROC: 0.8040997862099092
Root mean squared error: 0.42600359735325316
Relative absolute error: 43.938748280238066
Root relative squared error: 90.36900974950444
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5691509322246696
Weighted FMeasure: 0.7123059253153274
Iteration time: 188.0
Weighted AreaUnderPRC: 0.6385318919664674
Mean absolute error: 0.19528332568994788
Coverage of cases: 77.08
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 2928.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.14380632754951247
Kappa statistic: 0.569205307390613
Training time: 186.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 37.49333333333444
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7279839501208533
Weighted AreaUnderROC: 0.8175573944900418
Root mean squared error: 0.4152010180719693
Relative absolute error: 41.60134265666936
Root relative squared error: 88.07743663027412
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5902513604584719
Weighted FMeasure: 0.7262256047747006
Iteration time: 174.0
Weighted AreaUnderPRC: 0.6597164826145873
Mean absolute error: 0.1848948562518647
Coverage of cases: 77.72
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3102.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13709069242063457
Kappa statistic: 0.5895193487616448
Training time: 172.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 35.85333333333426
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.7314033117073911
Weighted AreaUnderROC: 0.8271286051935407
Root mean squared error: 0.41190092494131253
Relative absolute error: 40.95595341270569
Root relative squared error: 87.37738116090375
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5945291904087471
Weighted FMeasure: 0.7296384375209234
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6739592540249296
Mean absolute error: 0.18202645961202613
Coverage of cases: 77.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3283.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13585450644012673
Kappa statistic: 0.5936571623657148
Training time: 179.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 35.96000000000078
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.738824950666142
Weighted AreaUnderROC: 0.8198491276338784
Root mean squared error: 0.4093296925882651
Relative absolute error: 39.829647084758584
Root relative squared error: 86.83194041104994
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6068229407173298
Weighted FMeasure: 0.7375673161753222
Iteration time: 176.0
Weighted AreaUnderPRC: 0.6645954802996146
Mean absolute error: 0.17702065371003897
Coverage of cases: 77.16
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 3459.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13151824574937795
Kappa statistic: 0.6063063954085725
Training time: 173.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 36.90666666666748
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7180450711092844
Weighted AreaUnderROC: 0.8134244120839464
Root mean squared error: 0.4240498288179012
Relative absolute error: 42.81759149706004
Root relative squared error: 89.95455285543956
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5761393457291424
Weighted FMeasure: 0.7169092514156089
Iteration time: 188.0
Weighted AreaUnderPRC: 0.6523124390350072
Mean absolute error: 0.1903004066536011
Coverage of cases: 76.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3647.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.1416577027248249
Kappa statistic: 0.5757093036207418
Training time: 186.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 36.0666666666676
Incorrectly Classified Instances: 28.12
Correctly Classified Instances: 71.88
Weighted Precision: 0.7199491219939459
Weighted AreaUnderROC: 0.8004638078722086
Root mean squared error: 0.42589379143742195
Relative absolute error: 43.00632160957826
Root relative squared error: 90.34571639719485
Weighted TruePositiveRate: 0.7188
Weighted MatthewsCorrelation: 0.5785838678371799
Weighted FMeasure: 0.7187464002790958
Iteration time: 193.0
Weighted AreaUnderPRC: 0.6262970877785868
Mean absolute error: 0.19113920715368204
Coverage of cases: 74.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3840.0
Weighted Recall: 0.7188
Weighted FalsePositiveRate: 0.1409267956373776
Kappa statistic: 0.5780923966848505
Training time: 191.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 37.62666666666773
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7273046175979636
Weighted AreaUnderROC: 0.7980930867831789
Root mean squared error: 0.42130163085021405
Relative absolute error: 42.13635460226059
Root relative squared error: 89.37157202974117
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.590342237275228
Weighted FMeasure: 0.7265812478085987
Iteration time: 187.0
Weighted AreaUnderPRC: 0.6266592612087919
Mean absolute error: 0.18727268712115905
Coverage of cases: 76.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4027.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13686894807630998
Kappa statistic: 0.5901078562460841
Training time: 185.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 37.37333333333412
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7219664607612806
Weighted AreaUnderROC: 0.7992858845876948
Root mean squared error: 0.4237466794658527
Relative absolute error: 42.978621931848224
Root relative squared error: 89.89024516667583
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5801102770944991
Weighted FMeasure: 0.7187639548121353
Iteration time: 195.0
Weighted AreaUnderPRC: 0.6344014053278595
Mean absolute error: 0.1910160974748819
Coverage of cases: 75.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4222.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.140789991396326
Kappa statistic: 0.5786340829565049
Training time: 193.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.76000000000087
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.7320288614821524
Weighted AreaUnderROC: 0.7839425792292107
Root mean squared error: 0.41910734533170413
Relative absolute error: 42.1716903067405
Root relative squared error: 88.90609377874183
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5953814173467835
Weighted FMeasure: 0.7295634951219035
Iteration time: 194.0
Weighted AreaUnderPRC: 0.6147390972411859
Mean absolute error: 0.18742973469662533
Coverage of cases: 75.64
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4416.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.13565276975587773
Kappa statistic: 0.5942443196605675
Training time: 192.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 38.50666666666756
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7379953843924252
Weighted AreaUnderROC: 0.8129981829467535
Root mean squared error: 0.40650561215315784
Relative absolute error: 40.51669615483639
Root relative squared error: 86.23286248316576
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6060725007299965
Weighted FMeasure: 0.7370425286350563
Iteration time: 213.0
Weighted AreaUnderPRC: 0.6504995024158127
Mean absolute error: 0.18007420513260702
Coverage of cases: 79.64
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4629.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.1316411623103296
Kappa statistic: 0.6057382428383581
Training time: 210.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 37.800000000000956
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7378038285846418
Weighted AreaUnderROC: 0.8190661939574433
Root mean squared error: 0.4110382809499634
Relative absolute error: 40.75642359348428
Root relative squared error: 87.19438673609393
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.6024806191745995
Weighted FMeasure: 0.7345988498153785
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6567042215704453
Mean absolute error: 0.18113966041548654
Coverage of cases: 78.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4826.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.1335996332083361
Kappa statistic: 0.6007966298351032
Training time: 195.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 39.10666666666732
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7282758196408377
Weighted AreaUnderROC: 0.8206218630949196
Root mean squared error: 0.41775779723420137
Relative absolute error: 41.76304629554838
Root relative squared error: 88.61981139535735
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5901874465066123
Weighted FMeasure: 0.7267853157866839
Iteration time: 205.0
Weighted AreaUnderPRC: 0.6575127564700847
Mean absolute error: 0.18561353909132697
Coverage of cases: 78.8
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 5031.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13725142419459804
Kappa statistic: 0.5894519399476227
Training time: 202.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 37.38666666666771
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7320510344028056
Weighted AreaUnderROC: 0.808798737221321
Root mean squared error: 0.41272198831380275
Relative absolute error: 41.37785375411735
Root relative squared error: 87.55155500444528
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.597460567910225
Weighted FMeasure: 0.7316889097709159
Iteration time: 220.0
Weighted AreaUnderPRC: 0.6492796258510537
Mean absolute error: 0.18390157224052242
Coverage of cases: 77.48
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5251.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13440944390034257
Kappa statistic: 0.5973322873974488
Training time: 218.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 39.773333333334186
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7267040792235462
Weighted AreaUnderROC: 0.8161371754491584
Root mean squared error: 0.41357680300196464
Relative absolute error: 42.655089133177185
Root relative squared error: 87.73288858324241
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5857607951149865
Weighted FMeasure: 0.7232414053721448
Iteration time: 210.0
Weighted AreaUnderPRC: 0.6563843847231363
Mean absolute error: 0.18957817392523282
Coverage of cases: 79.28
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5461.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13912064629947007
Kappa statistic: 0.5840226272482586
Training time: 208.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 37.26666666666774
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7258261211308393
Weighted AreaUnderROC: 0.8144426148657598
Root mean squared error: 0.4184803967680436
Relative absolute error: 42.11574031532851
Root relative squared error: 88.77309790449597
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5871143376487419
Weighted FMeasure: 0.7243627565362656
Iteration time: 216.0
Weighted AreaUnderPRC: 0.6549030867857196
Mean absolute error: 0.1871810680681276
Coverage of cases: 76.76
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 5677.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13807461424564338
Kappa statistic: 0.5865519077179058
Training time: 214.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 38.6533333333343
Incorrectly Classified Instances: 26.08
Correctly Classified Instances: 73.92
Weighted Precision: 0.7392311474302694
Weighted AreaUnderROC: 0.819702699158457
Root mean squared error: 0.40627348644988565
Relative absolute error: 40.38296852415387
Root relative squared error: 86.18362118550432
Weighted TruePositiveRate: 0.7392
Weighted MatthewsCorrelation: 0.6088263714916162
Weighted FMeasure: 0.7385730131463629
Iteration time: 221.0
Weighted AreaUnderPRC: 0.661904042508185
Mean absolute error: 0.17947986010735134
Coverage of cases: 78.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5898.0
Weighted Recall: 0.7392
Weighted FalsePositiveRate: 0.13049023320457162
Kappa statistic: 0.6088525076393744
Training time: 219.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 37.680000000001115
Incorrectly Classified Instances: 25.88
Correctly Classified Instances: 74.12
Weighted Precision: 0.7420269094733876
Weighted AreaUnderROC: 0.8229603532776975
Root mean squared error: 0.405682051150947
Relative absolute error: 40.05387895948828
Root relative squared error: 86.05815881235054
Weighted TruePositiveRate: 0.7412
Weighted MatthewsCorrelation: 0.6121276199638399
Weighted FMeasure: 0.740957116579224
Iteration time: 222.0
Weighted AreaUnderPRC: 0.6621378226120621
Mean absolute error: 0.17801723981994877
Coverage of cases: 78.08
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6120.0
Weighted Recall: 0.7412
Weighted FalsePositiveRate: 0.12956187678860584
Kappa statistic: 0.6118001242239602
Training time: 220.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 36.25333333333436
Incorrectly Classified Instances: 25.44
Correctly Classified Instances: 74.56
Weighted Precision: 0.7465717313323724
Weighted AreaUnderROC: 0.817408221873144
Root mean squared error: 0.404345208093999
Relative absolute error: 38.86949134942145
Root relative squared error: 85.77457157506551
Weighted TruePositiveRate: 0.7456
Weighted MatthewsCorrelation: 0.6187240716129078
Weighted FMeasure: 0.7455454653952458
Iteration time: 236.0
Weighted AreaUnderPRC: 0.6549398820262234
Mean absolute error: 0.17275329488631838
Coverage of cases: 77.72
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6356.0
Weighted Recall: 0.7456
Weighted FalsePositiveRate: 0.12742555499863134
Kappa statistic: 0.6183644010313283
Training time: 234.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 36.70666666666765
Incorrectly Classified Instances: 25.56
Correctly Classified Instances: 74.44
Weighted Precision: 0.7458654046996613
Weighted AreaUnderROC: 0.8282835031060537
Root mean squared error: 0.4040788728224859
Relative absolute error: 39.129994107609605
Root relative squared error: 85.71807333209868
Weighted TruePositiveRate: 0.7444
Weighted MatthewsCorrelation: 0.6171419743757253
Weighted FMeasure: 0.7443891540267421
Iteration time: 249.0
Weighted AreaUnderPRC: 0.6696203558635078
Mean absolute error: 0.17391108492271015
Coverage of cases: 78.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6605.0
Weighted Recall: 0.7444
Weighted FalsePositiveRate: 0.12807037686586117
Kappa statistic: 0.6165504276392851
Training time: 247.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.45333333333406
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.7498310917125988
Weighted AreaUnderROC: 0.8350905886864988
Root mean squared error: 0.3997687663529242
Relative absolute error: 38.8659759987136
Root relative squared error: 84.80376167841976
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.6227498367512609
Weighted FMeasure: 0.7478791269453421
Iteration time: 249.0
Weighted AreaUnderPRC: 0.6780634490271703
Mean absolute error: 0.1727376711053946
Coverage of cases: 83.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6854.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.1263295127654021
Kappa statistic: 0.6219121021255022
Training time: 247.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 36.49333333333434
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7458393115950505
Weighted AreaUnderROC: 0.8146205534900559
Root mean squared error: 0.40596273585142045
Relative absolute error: 38.916206407196796
Root relative squared error: 86.11770102887456
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6178761256537584
Weighted FMeasure: 0.7453116833979124
Iteration time: 247.0
Weighted AreaUnderPRC: 0.6504056925869298
Mean absolute error: 0.1729609173653199
Coverage of cases: 78.16
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7101.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.12770698189227617
Kappa statistic: 0.6177214035205637
Training time: 245.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 39.26666666666766
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7533523771605036
Weighted AreaUnderROC: 0.8204410798959818
Root mean squared error: 0.3997893832435928
Relative absolute error: 38.18575423566588
Root relative squared error: 84.8081351813794
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6285339776773506
Weighted FMeasure: 0.7519762323925095
Iteration time: 246.0
Weighted AreaUnderPRC: 0.661415196218616
Mean absolute error: 0.16971446326962694
Coverage of cases: 80.12
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7347.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12429157923035809
Kappa statistic: 0.6279015278603329
Training time: 244.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 39.37333333333427
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7474770631444055
Weighted AreaUnderROC: 0.8232304132410936
Root mean squared error: 0.4033958462740723
Relative absolute error: 39.108018941548195
Root relative squared error: 85.5731815208646
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.6196215286771721
Weighted FMeasure: 0.745931743485401
Iteration time: 252.0
Weighted AreaUnderPRC: 0.6641635042100875
Mean absolute error: 0.17381341751799279
Coverage of cases: 80.28
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7599.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.12732367171066378
Kappa statistic: 0.6188722459768514
Training time: 250.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 35.906666666667675
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7324743935072212
Weighted AreaUnderROC: 0.8152156676583471
Root mean squared error: 0.4144388010542621
Relative absolute error: 40.632932145641604
Root relative squared error: 87.91574598368716
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5989784853868987
Weighted FMeasure: 0.7324949204593413
Iteration time: 262.0
Weighted AreaUnderPRC: 0.6512106711552826
Mean absolute error: 0.18059080953618575
Coverage of cases: 76.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7861.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13366071457131581
Kappa statistic: 0.5992370625564547
Training time: 260.0
		
Time end:Sun Oct 08 10.40.15 EEST 2017