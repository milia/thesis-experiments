Sun Oct 08 06.02.50 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.50 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.67307692307693
Correctly Classified Instances: 64.32692307692307
Weighted Precision: 0.6604713007040184
Weighted AreaUnderROC: 0.6370230364873223
Root mean squared error: 0.5696132964879774
Relative absolute error: 73.951048951049
Root relative squared error: 113.92265929759549
Weighted TruePositiveRate: 0.6432692307692307
Weighted MatthewsCorrelation: 0.29579093917809873
Weighted FMeasure: 0.6294377848558252
Iteration time: 1.9
Weighted AreaUnderPRC: 0.5950567003579283
Mean absolute error: 0.3697552447552449
Coverage of cases: 64.32692307692307
Instances selection time: 1.8
Test time: 6.7
Accumulative iteration time: 1.9
Weighted Recall: 0.6432692307692307
Weighted FalsePositiveRate: 0.3692231577945864
Kappa statistic: 0.2767760719254001
Training time: 0.1
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.423076923076923
Correctly Classified Instances: 70.57692307692308
Weighted Precision: 0.7123761495611576
Weighted AreaUnderROC: 0.6991798082869511
Root mean squared error: 0.5278470583783592
Relative absolute error: 60.80586080586088
Root relative squared error: 105.56941167567189
Weighted TruePositiveRate: 0.7057692307692308
Weighted MatthewsCorrelation: 0.41207315267983036
Weighted FMeasure: 0.7003804728338497
Iteration time: 0.9
Weighted AreaUnderPRC: 0.646296519995807
Mean absolute error: 0.3040293040293044
Coverage of cases: 70.57692307692308
Instances selection time: 0.2
Test time: 10.7
Accumulative iteration time: 2.8
Weighted Recall: 0.7057692307692308
Weighted FalsePositiveRate: 0.30740961419532853
Kappa statistic: 0.40251976319848504
Training time: 0.7
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.75
Correctly Classified Instances: 76.25000000000001
Weighted Precision: 0.7670838128035271
Weighted AreaUnderROC: 0.7569302721088434
Root mean squared error: 0.4766273846778672
Relative absolute error: 49.193548387096754
Root relative squared error: 95.32547693557345
Weighted TruePositiveRate: 0.7625
Weighted MatthewsCorrelation: 0.5249736453165169
Weighted FMeasure: 0.7593331208422364
Iteration time: 1.4
Weighted AreaUnderPRC: 0.7015304663827538
Mean absolute error: 0.2459677419354837
Coverage of cases: 76.25000000000001
Instances selection time: 1.0
Test time: 13.3
Accumulative iteration time: 4.2
Weighted Recall: 0.7625
Weighted FalsePositiveRate: 0.2486394557823129
Kappa statistic: 0.5181169686535131
Training time: 0.4
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.673076923076927
Correctly Classified Instances: 79.32692307692307
Weighted Precision: 0.8006192306742725
Weighted AreaUnderROC: 0.7877060142238714
Root mean squared error: 0.4479271955622194
Relative absolute error: 42.77673545966225
Root relative squared error: 89.58543911244385
Weighted TruePositiveRate: 0.7932692307692308
Weighted MatthewsCorrelation: 0.5900095926991051
Weighted FMeasure: 0.7908167761108109
Iteration time: 0.7
Weighted AreaUnderPRC: 0.7332556814791242
Mean absolute error: 0.21388367729831131
Coverage of cases: 79.32692307692307
Instances selection time: 0.3
Test time: 16.4
Accumulative iteration time: 4.9
Weighted Recall: 0.7932692307692308
Weighted FalsePositiveRate: 0.21785720232148803
Kappa statistic: 0.5808752270185605
Training time: 0.4
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.615384615384613
Correctly Classified Instances: 80.3846153846154
Weighted Precision: 0.8083687991895909
Weighted AreaUnderROC: 0.7989842300556587
Root mean squared error: 0.43704666663242875
Relative absolute error: 40.42232277526403
Root relative squared error: 87.40933332648575
Weighted TruePositiveRate: 0.8038461538461539
Weighted MatthewsCorrelation: 0.6087618945738049
Weighted FMeasure: 0.8020912636073246
Iteration time: 1.4
Weighted AreaUnderPRC: 0.7446273003809417
Mean absolute error: 0.2021116138763201
Coverage of cases: 80.3846153846154
Instances selection time: 1.0
Test time: 19.9
Accumulative iteration time: 6.3
Weighted Recall: 0.8038461538461539
Weighted FalsePositiveRate: 0.2058776937348366
Kappa statistic: 0.6026875136872174
Training time: 0.4
		
Time end:Sun Oct 08 06.02.53 EEST 2017