Sun Oct 08 06.02.58 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.58 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7772750125691302
Weighted AreaUnderROC: 0.7440476190476191
Root mean squared error: 0.5188745216627708
Relative absolute error: 53.84615384615385
Root relative squared error: 103.77490433255416
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.5114621455194985
Weighted FMeasure: 0.724714565794026
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6892717639323974
Mean absolute error: 0.2692307692307692
Coverage of cases: 73.07692307692308
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.24267399267399264
Kappa statistic: 0.47398843930635826
Training time: 10.0
		
Time end:Sun Oct 08 06.02.58 EEST 2017