Sun Oct 08 06.02.10 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.10 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7211355171705915
Weighted AreaUnderROC: 0.7626488095238096
Root mean squared error: 0.43765107281767446
Relative absolute error: 75.3846153846154
Root relative squared error: 87.5302145635349
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43686194171286236
Weighted FMeasure: 0.7194514886822578
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7490429929283687
Mean absolute error: 0.37692307692307697
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.28960622710622713
Kappa statistic: 0.4347826086956522
Training time: 7.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 98.07692307692308
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.7190244205169579
Weighted AreaUnderROC: 0.8158482142857142
Root mean squared error: 0.42562171720184355
Relative absolute error: 75.3846153846154
Root relative squared error: 85.1243434403687
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.405983503508594
Weighted FMeasure: 0.6764915309094831
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7950027456053557
Mean absolute error: 0.37692307692307697
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.2928113553113553
Kappa statistic: 0.37916063675832123
Training time: 11.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 98.07692307692308
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7482517482517482
Weighted AreaUnderROC: 0.8149181547619049
Root mean squared error: 0.4215356541175463
Relative absolute error: 74.23076923076924
Root relative squared error: 84.30713082350925
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.4804999801042981
Weighted FMeasure: 0.7295726495726497
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7940330090373654
Mean absolute error: 0.3711538461538462
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.25457875457875456
Kappa statistic: 0.4678362573099414
Training time: 16.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 96.63461538461539
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7894736842105263
Weighted AreaUnderROC: 0.8534226190476191
Root mean squared error: 0.40096038553606606
Relative absolute error: 69.23076923076927
Root relative squared error: 80.19207710721321
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5422808355407928
Weighted FMeasure: 0.7458006718924971
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8353013286607992
Mean absolute error: 0.3461538461538463
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 74.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.22619047619047622
Kappa statistic: 0.5101449275362319
Training time: 19.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 96.63461538461539
Incorrectly Classified Instances: 29.807692307692307
Correctly Classified Instances: 70.1923076923077
Weighted Precision: 0.7264361654605557
Weighted AreaUnderROC: 0.8722098214285714
Root mean squared error: 0.3934560868860618
Relative absolute error: 67.30769230769232
Root relative squared error: 78.69121737721237
Weighted TruePositiveRate: 0.7019230769230769
Weighted MatthewsCorrelation: 0.43112330121425735
Weighted FMeasure: 0.6990162247894206
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8600576722694501
Mean absolute error: 0.3365384615384616
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7019230769230769
Weighted FalsePositiveRate: 0.2793040293040293
Kappa statistic: 0.4133915574963609
Training time: 26.0
		
Time end:Sun Oct 08 06.02.10 EEST 2017