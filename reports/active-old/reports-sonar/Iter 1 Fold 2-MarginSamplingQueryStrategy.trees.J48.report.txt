Sun Oct 08 06.02.40 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.40 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 80.76923076923077
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7331730769230769
Weighted AreaUnderROC: 0.7232142857142858
Root mean squared error: 0.4882279915161611
Relative absolute error: 59.82905982905981
Root relative squared error: 97.64559830323222
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.457453159194625
Weighted FMeasure: 0.7275058275058275
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6659116124260355
Mean absolute error: 0.299145299145299
Coverage of cases: 90.38461538461539
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.28434065934065933
Kappa statistic: 0.4518072289156625
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 40.38461538461539
Correctly Classified Instances: 59.61538461538461
Weighted Precision: 0.6120760959470636
Weighted AreaUnderROC: 0.6041666666666666
Root mean squared error: 0.6354889093022426
Relative absolute error: 80.76923076923077
Root relative squared error: 127.0977818604485
Weighted TruePositiveRate: 0.5961538461538461
Weighted MatthewsCorrelation: 0.21166687833365083
Weighted FMeasure: 0.5930069930069931
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5658681682254882
Mean absolute error: 0.40384615384615385
Coverage of cases: 59.61538461538461
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5961538461538461
Weighted FalsePositiveRate: 0.38782051282051283
Kappa statistic: 0.20408163265306117
Training time: 5.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 59.61538461538461
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.7120412267471091
Weighted AreaUnderROC: 0.6867559523809523
Root mean squared error: 0.5651941652604391
Relative absolute error: 69.23076923076924
Root relative squared error: 113.03883305208782
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.38983395237766655
Weighted FMeasure: 0.6657248298927461
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6359664287761007
Mean absolute error: 0.3461538461538462
Coverage of cases: 71.15384615384616
Instances selection time: 3.0
Test time: 0.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.30105311355311354
Kappa statistic: 0.3612716763005781
Training time: 7.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 59.61538461538461
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6728701406120762
Weighted AreaUnderROC: 0.652529761904762
Root mean squared error: 0.5847053462046862
Relative absolute error: 70.76923076923077
Root relative squared error: 116.94106924093724
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.3295955676909706
Weighted FMeasure: 0.6511488511488512
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6039398979845629
Mean absolute error: 0.35384615384615387
Coverage of cases: 69.23076923076923
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.32944139194139194
Kappa statistic: 0.3177842565597668
Training time: 12.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 67.3076923076923
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.6893106893106894
Weighted AreaUnderROC: 0.7276785714285714
Root mean squared error: 0.546204054382053
Relative absolute error: 64.89621489621484
Root relative squared error: 109.24081087641059
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.37154034757878523
Weighted FMeasure: 0.6828976848394325
Iteration time: 27.0
Weighted AreaUnderPRC: 0.680476944633608
Mean absolute error: 0.3244810744810742
Coverage of cases: 83.65384615384616
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.3106684981684982
Kappa statistic: 0.36818851251840945
Training time: 24.0
		
Time end:Sun Oct 08 06.02.40 EEST 2017