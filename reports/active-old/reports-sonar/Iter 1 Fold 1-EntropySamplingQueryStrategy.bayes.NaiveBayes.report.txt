Sun Oct 08 06.01.50 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.01.50 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 54.80769230769231
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.6985844017094016
Weighted AreaUnderROC: 0.7052750820607963
Root mean squared error: 0.558706509506785
Relative absolute error: 65.98271324571712
Root relative squared error: 111.741301901357
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.3724151621225528
Weighted FMeasure: 0.6706317159073064
Iteration time: 262.0
Weighted AreaUnderPRC: 0.6915010747490752
Mean absolute error: 0.32991356622858564
Coverage of cases: 72.11538461538461
Instances selection time: 115.0
Test time: 167.0
Accumulative iteration time: 262.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.3383509347795062
Kappa statistic: 0.35098335854765506
Training time: 147.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 55.28846153846154
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7164694280078895
Weighted AreaUnderROC: 0.759686741829599
Root mean squared error: 0.5327499415697788
Relative absolute error: 60.32459666738019
Root relative squared error: 106.54998831395577
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4227600216066948
Weighted FMeasure: 0.7071678321678323
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7647611936622243
Mean absolute error: 0.30162298333690096
Coverage of cases: 75.0
Instances selection time: 45.0
Test time: 61.0
Accumulative iteration time: 330.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.30151990866276585
Kappa statistic: 0.4146341463414635
Training time: 23.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 57.69230769230769
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.721663752913753
Weighted AreaUnderROC: 0.7838697017268447
Root mean squared error: 0.5144631302830847
Relative absolute error: 57.346182270060254
Root relative squared error: 102.89262605661693
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43938604901963735
Weighted FMeasure: 0.719719854712842
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7944795539807032
Mean absolute error: 0.28673091135030127
Coverage of cases: 79.8076923076923
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 374.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2862744398458684
Kappa statistic: 0.4373134328358209
Training time: 29.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 59.61538461538461
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7503766478342749
Weighted AreaUnderROC: 0.8041583416583417
Root mean squared error: 0.4802845545927602
Relative absolute error: 53.02764160842147
Root relative squared error: 96.05691091855203
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.49758400114469625
Weighted FMeasure: 0.7490668159761105
Iteration time: 36.0
Weighted AreaUnderPRC: 0.807664467733801
Mean absolute error: 0.26513820804210736
Coverage of cases: 81.73076923076923
Instances selection time: 14.0
Test time: 33.0
Accumulative iteration time: 410.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.25612244897959185
Kappa statistic: 0.4960864703689899
Training time: 22.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 59.13461538461539
Incorrectly Classified Instances: 29.807692307692307
Correctly Classified Instances: 70.1923076923077
Weighted Precision: 0.7023290598290598
Weighted AreaUnderROC: 0.7815238333095477
Root mean squared error: 0.5215410012787978
Relative absolute error: 60.10360378403081
Root relative squared error: 104.30820025575956
Weighted TruePositiveRate: 0.7019230769230769
Weighted MatthewsCorrelation: 0.4025951682663876
Weighted FMeasure: 0.7020611905933006
Iteration time: 21.0
Weighted AreaUnderPRC: 0.777820438529073
Mean absolute error: 0.30051801892015406
Coverage of cases: 78.84615384615384
Instances selection time: 8.0
Test time: 31.0
Accumulative iteration time: 431.0
Weighted Recall: 0.7019230769230769
Weighted FalsePositiveRate: 0.29895461681175967
Kappa statistic: 0.4025203854707189
Training time: 13.0
		
Time end:Sun Oct 08 06.01.51 EEST 2017