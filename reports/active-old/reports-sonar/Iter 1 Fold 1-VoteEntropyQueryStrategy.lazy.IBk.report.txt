Sun Oct 08 06.02.59 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.59 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7498133631952221
Weighted AreaUnderROC: 0.7480519480519481
Root mean squared error: 0.47889335240239755
Relative absolute error: 54.5454545454546
Root relative squared error: 95.7786704804795
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.497583150661188
Weighted FMeasure: 0.7496279761904762
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6869685958727866
Mean absolute error: 0.272727272727273
Coverage of cases: 75.0
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 9.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.2538961038961039
Kappa statistic: 0.4972108590554109
Training time: 3.0
		
Time end:Sun Oct 08 06.02.59 EEST 2017