Sun Oct 08 06.02.15 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.15 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7211355171705915
Weighted AreaUnderROC: 0.7626488095238096
Root mean squared error: 0.43765107281767446
Relative absolute error: 75.3846153846154
Root relative squared error: 87.5302145635349
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43686194171286236
Weighted FMeasure: 0.7194514886822578
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7490429929283687
Mean absolute error: 0.37692307692307697
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.28960622710622713
Kappa statistic: 0.4347826086956522
Training time: 7.0
		
Time end:Sun Oct 08 06.02.15 EEST 2017