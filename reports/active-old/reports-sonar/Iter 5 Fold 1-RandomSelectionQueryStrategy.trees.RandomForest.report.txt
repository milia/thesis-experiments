Sun Oct 08 06.02.57 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.57 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 99.51923076923077
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5814000814000814
Weighted AreaUnderROC: 0.7098330241187383
Root mean squared error: 0.4849662954188751
Relative absolute error: 86.5384615384616
Root relative squared error: 96.99325908377502
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.14406143934002155
Weighted FMeasure: 0.550944669365722
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6784111586800881
Mean absolute error: 0.432692307692308
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.450392464678179
Kappa statistic: 0.12970711297071114
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 100.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6725517129928895
Weighted AreaUnderROC: 0.7480519480519481
Root mean squared error: 0.45
Relative absolute error: 78.84615384615388
Root relative squared error: 90.0
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.327725495744935
Weighted FMeasure: 0.6532437442075997
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7387906348781322
Mean absolute error: 0.39423076923076944
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.35548380191237333
Kappa statistic: 0.3132075471698113
Training time: 3.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 92.78846153846153
Incorrectly Classified Instances: 29.807692307692307
Correctly Classified Instances: 70.1923076923077
Weighted Precision: 0.7111142533936653
Weighted AreaUnderROC: 0.8007421150278293
Root mean squared error: 0.42933311434936267
Relative absolute error: 69.03846153846158
Root relative squared error: 85.86662286987253
Weighted TruePositiveRate: 0.7019230769230769
Weighted MatthewsCorrelation: 0.40645774853280275
Weighted FMeasure: 0.6948809917963433
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7845656740301787
Mean absolute error: 0.3451923076923079
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7019230769230769
Weighted FalsePositiveRate: 0.3145390323961752
Kappa statistic: 0.39307228915662634
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 94.23076923076923
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7209249084249085
Weighted AreaUnderROC: 0.8020408163265305
Root mean squared error: 0.42369709790041127
Relative absolute error: 67.11538461538464
Root relative squared error: 84.73941958008226
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4399037715332818
Weighted FMeasure: 0.7209725586014246
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7812447793203214
Mean absolute error: 0.3355769230769232
Coverage of cases: 99.03846153846153
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2818217496788925
Kappa statistic: 0.4398216939078752
Training time: 10.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 95.1923076923077
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7569033530571991
Weighted AreaUnderROC: 0.7998144712430426
Root mean squared error: 0.4313440532888939
Relative absolute error: 70.57692307692307
Root relative squared error: 86.26881065777879
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5023383786150137
Weighted FMeasure: 0.7462121212121212
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7640839332245957
Mean absolute error: 0.35288461538461535
Coverage of cases: 99.03846153846153
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.26280148423005567
Kappa statistic: 0.49268292682926834
Training time: 13.0
		
Time end:Sun Oct 08 06.02.57 EEST 2017