Sun Oct 08 06.02.01 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.01 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7209249084249085
Weighted AreaUnderROC: 0.7196660482374768
Root mean squared error: 0.5055320159979813
Relative absolute error: 59.79020979020985
Root relative squared error: 101.10640319959626
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4399037715332818
Weighted FMeasure: 0.7209725586014246
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6593494646379262
Mean absolute error: 0.29895104895104924
Coverage of cases: 72.11538461538461
Instances selection time: 12.0
Test time: 8.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2818217496788925
Kappa statistic: 0.4398216939078752
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.7917986425339366
Weighted AreaUnderROC: 0.7512059369202226
Root mean squared error: 0.4978148658655981
Relative absolute error: 54.21245421245421
Root relative squared error: 99.56297317311962
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.5346268930586049
Weighted FMeasure: 0.7326630738725914
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6980677552085924
Mean absolute error: 0.27106227106227104
Coverage of cases: 74.03846153846153
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.23797274154417009
Kappa statistic: 0.49093546047860775
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.798072674122166
Weighted AreaUnderROC: 0.7432282003710575
Root mean squared error: 0.5106917381246516
Relative absolute error: 55.33498759305211
Root relative squared error: 102.13834762493033
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.5308612556246101
Weighted FMeasure: 0.719683861696824
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6930924415670668
Mean absolute error: 0.27667493796526055
Coverage of cases: 73.07692307692308
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.24431283002711576
Kappa statistic: 0.4737983375496927
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7698102274373461
Weighted AreaUnderROC: 0.7662337662337664
Root mean squared error: 0.4746466411394863
Relative absolute error: 47.467166979362055
Root relative squared error: 94.92932822789726
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5364635925188874
Weighted FMeasure: 0.7683693685933328
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7060468413956028
Mean absolute error: 0.23733583489681026
Coverage of cases: 76.92307692307692
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.23676323676323674
Kappa statistic: 0.5348490495713754
Training time: 1.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7853365384615384
Weighted AreaUnderROC: 0.7730983302411872
Root mean squared error: 0.46573985561403725
Relative absolute error: 45.32428355957785
Root relative squared error: 93.14797112280745
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5604133115633189
Weighted FMeasure: 0.7760271783735101
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7160133136094674
Mean absolute error: 0.22662141779788925
Coverage of cases: 77.88461538461539
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.23264949336377905
Kappa statistic: 0.5517241379310346
Training time: 0.0
		
Time end:Sun Oct 08 06.02.01 EEST 2017