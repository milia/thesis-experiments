Sun Oct 08 06.02.10 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.10 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 99.51923076923077
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6791105399976368
Weighted AreaUnderROC: 0.7491651205936919
Root mean squared error: 0.44978627403211424
Relative absolute error: 82.30769230769232
Root relative squared error: 89.95725480642285
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3450216287518032
Weighted FMeasure: 0.6606171992769932
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7308452633021798
Mean absolute error: 0.4115384615384616
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.32431497074354215
Kappa statistic: 0.33430870519385514
Training time: 7.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6668447293447293
Weighted AreaUnderROC: 0.7543599257884973
Root mean squared error: 0.4474285503768256
Relative absolute error: 80.76923076923076
Root relative squared error: 89.48571007536512
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.32993526681131213
Weighted FMeasure: 0.6636171272093601
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7410792089051423
Mean absolute error: 0.4038461538461538
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.333220351077494
Kappa statistic: 0.3284132841328412
Training time: 9.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7230665839536807
Weighted AreaUnderROC: 0.7925788497217069
Root mean squared error: 0.4310095483504173
Relative absolute error: 73.8461538461539
Root relative squared error: 86.20190967008347
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.44014794214945563
Weighted FMeasure: 0.7187705456936226
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7724440059928399
Mean absolute error: 0.3692307692307695
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2885007849293564
Kappa statistic: 0.43605086013462974
Training time: 14.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 21.153846153846153
Correctly Classified Instances: 78.84615384615384
Weighted Precision: 0.793727700870558
Weighted AreaUnderROC: 0.8803339517625232
Root mean squared error: 0.38804242432615915
Relative absolute error: 68.0769230769231
Root relative squared error: 77.60848486523183
Weighted TruePositiveRate: 0.7884615384615384
Weighted MatthewsCorrelation: 0.5821892393320964
Weighted FMeasure: 0.7884615384615384
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8578065177432364
Mean absolute error: 0.34038461538461545
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7884615384615384
Weighted FalsePositiveRate: 0.20627229912944195
Kappa statistic: 0.5783265757464061
Training time: 13.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7691159158124443
Weighted AreaUnderROC: 0.8556586270871984
Root mean squared error: 0.3940665699326211
Relative absolute error: 64.80769230769235
Root relative squared error: 78.81331398652422
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5362881975338608
Weighted FMeasure: 0.7688873626373627
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8420880363729315
Mean absolute error: 0.3240384615384618
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 87.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.23453689167974884
Kappa statistic: 0.5358869468203794
Training time: 25.0
		
Time end:Sun Oct 08 06.02.10 EEST 2017