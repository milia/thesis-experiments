Sun Oct 08 06.02.39 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.39 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 72.25961538461539
Incorrectly Classified Instances: 35.19230769230769
Correctly Classified Instances: 64.8076923076923
Weighted Precision: 0.6533947264999709
Weighted AreaUnderROC: 0.6411521625695733
Root mean squared error: 0.5672189818744895
Relative absolute error: 73.99030136530138
Root relative squared error: 113.44379637489787
Weighted TruePositiveRate: 0.6480769230769231
Weighted MatthewsCorrelation: 0.29592934827244466
Weighted FMeasure: 0.6432649799483229
Iteration time: 4.8
Weighted AreaUnderPRC: 0.599707964360835
Mean absolute error: 0.36995150682650685
Coverage of cases: 79.23076923076924
Instances selection time: 2.4
Test time: 2.6
Accumulative iteration time: 4.8
Weighted Recall: 0.6480769230769231
Weighted FalsePositiveRate: 0.3628388278388278
Kappa statistic: 0.28847354931747693
Training time: 2.4
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 57.25961538461538
Incorrectly Classified Instances: 38.75
Correctly Classified Instances: 61.25
Weighted Precision: 0.6244387214175606
Weighted AreaUnderROC: 0.6121069109461966
Root mean squared error: 0.6090870143977751
Relative absolute error: 78.94133923228749
Root relative squared error: 121.81740287955502
Weighted TruePositiveRate: 0.6125
Weighted MatthewsCorrelation: 0.23711924469656212
Weighted FMeasure: 0.6077293094350878
Iteration time: 6.5
Weighted AreaUnderPRC: 0.5750140325451891
Mean absolute error: 0.3947066961614375
Coverage of cases: 65.96153846153847
Instances selection time: 1.5
Test time: 1.4
Accumulative iteration time: 11.3
Weighted Recall: 0.6125
Weighted FalsePositiveRate: 0.38069959802102654
Kappa statistic: 0.2296119647528448
Training time: 5.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 54.61538461538462
Incorrectly Classified Instances: 37.59615384615385
Correctly Classified Instances: 62.40384615384615
Weighted Precision: 0.6480341852149136
Weighted AreaUnderROC: 0.6285990646258504
Root mean squared error: 0.6065924055866928
Relative absolute error: 76.33859124431866
Root relative squared error: 121.31848111733852
Weighted TruePositiveRate: 0.6240384615384615
Weighted MatthewsCorrelation: 0.27160171605054606
Weighted FMeasure: 0.612952974676295
Iteration time: 9.7
Weighted AreaUnderPRC: 0.5892876533513492
Mean absolute error: 0.3816929562215934
Coverage of cases: 65.0
Instances selection time: 1.9
Test time: 1.7
Accumulative iteration time: 21.0
Weighted Recall: 0.6240384615384615
Weighted FalsePositiveRate: 0.36836050853908
Kappa statistic: 0.2525634965969499
Training time: 7.8
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 56.49038461538462
Incorrectly Classified Instances: 32.692307692307686
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.6845680543003438
Weighted AreaUnderROC: 0.6817501546072975
Root mean squared error: 0.5616506448982106
Relative absolute error: 66.05446024563672
Root relative squared error: 112.33012897964211
Weighted TruePositiveRate: 0.673076923076923
Weighted MatthewsCorrelation: 0.3561831883466885
Weighted FMeasure: 0.6693958349264584
Iteration time: 15.3
Weighted AreaUnderPRC: 0.6349035986404431
Mean absolute error: 0.3302723012281836
Coverage of cases: 71.53846153846153
Instances selection time: 2.3
Test time: 1.3
Accumulative iteration time: 36.3
Weighted Recall: 0.673076923076923
Weighted FalsePositiveRate: 0.3260956305599163
Kappa statistic: 0.3466918689046331
Training time: 13.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 55.04807692307693
Incorrectly Classified Instances: 28.84615384615385
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7152379350868971
Weighted AreaUnderROC: 0.7087184021335807
Root mean squared error: 0.5251976170009881
Relative absolute error: 58.87562571031522
Root relative squared error: 105.03952340019762
Weighted TruePositiveRate: 0.7115384615384615
Weighted MatthewsCorrelation: 0.42294531254111495
Weighted FMeasure: 0.7094743149533717
Iteration time: 23.4
Weighted AreaUnderPRC: 0.661218840727197
Mean absolute error: 0.2943781285515761
Coverage of cases: 75.28846153846153
Instances selection time: 2.8
Test time: 1.9
Accumulative iteration time: 59.7
Weighted Recall: 0.7115384615384615
Weighted FalsePositiveRate: 0.2951663217734647
Kappa statistic: 0.41822141639585597
Training time: 20.6
		
Time end:Sun Oct 08 06.02.42 EEST 2017