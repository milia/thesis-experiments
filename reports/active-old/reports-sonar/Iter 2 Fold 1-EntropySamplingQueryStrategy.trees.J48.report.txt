Sun Oct 08 06.02.04 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.04 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5807763664906522
Weighted AreaUnderROC: 0.5788497217068646
Root mean squared error: 0.5889517253077057
Relative absolute error: 87.60683760683753
Root relative squared error: 117.79034506154113
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.15769944341372913
Weighted FMeasure: 0.5769230769230769
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5471544252450846
Mean absolute error: 0.43803418803418764
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.4192236335093478
Kappa statistic: 0.15665315149281228
Training time: 3.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 73.5576923076923
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5807763664906522
Weighted AreaUnderROC: 0.5788497217068646
Root mean squared error: 0.6146580790242808
Relative absolute error: 86.87002652519892
Root relative squared error: 122.93161580485616
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.15769944341372913
Weighted FMeasure: 0.5769230769230769
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5471544252450846
Mean absolute error: 0.43435013262599464
Coverage of cases: 75.96153846153847
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.4192236335093478
Kappa statistic: 0.15665315149281228
Training time: 8.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.5933499596355669
Weighted AreaUnderROC: 0.5901669758812615
Root mean squared error: 0.6430097498961725
Relative absolute error: 82.6923076923077
Root relative squared error: 128.60194997923452
Weighted TruePositiveRate: 0.5865384615384615
Weighted MatthewsCorrelation: 0.18124413918724375
Weighted FMeasure: 0.5855054772191812
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5547826585967373
Mean absolute error: 0.41346153846153844
Coverage of cases: 58.65384615384615
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 44.0
Weighted Recall: 0.5865384615384615
Weighted FalsePositiveRate: 0.40620450977593836
Kappa statistic: 0.17854518736223368
Training time: 24.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 52.88461538461539
Incorrectly Classified Instances: 47.11538461538461
Correctly Classified Instances: 52.88461538461539
Weighted Precision: 0.5360576923076923
Weighted AreaUnderROC: 0.5448979591836735
Root mean squared error: 0.6768138023688074
Relative absolute error: 93.46153846153847
Root relative squared error: 135.36276047376148
Weighted TruePositiveRate: 0.5288461538461539
Weighted MatthewsCorrelation: 0.06748249899619002
Weighted FMeasure: 0.5264447580961342
Iteration time: 39.0
Weighted AreaUnderPRC: 0.5293337945792399
Mean absolute error: 0.46730769230769237
Coverage of cases: 56.73076923076923
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 83.0
Weighted Recall: 0.5288461538461539
Weighted FalsePositiveRate: 0.46205580134151564
Kappa statistic: 0.06598240469208218
Training time: 34.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 51.92307692307692
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.653582216487287
Weighted AreaUnderROC: 0.62152133580705
Root mean squared error: 0.5767422678475064
Relative absolute error: 70.40830546265327
Root relative squared error: 115.34845356950127
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30277314380982334
Weighted FMeasure: 0.6515107830700034
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5893942562009504
Mean absolute error: 0.3520415273132664
Coverage of cases: 67.3076923076923
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 127.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3551448551448551
Kappa statistic: 0.30070974971983566
Training time: 43.0
		
Time end:Sun Oct 08 06.02.04 EEST 2017