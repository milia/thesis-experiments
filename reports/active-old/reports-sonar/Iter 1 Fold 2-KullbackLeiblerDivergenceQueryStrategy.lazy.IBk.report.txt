Sun Oct 08 06.02.13 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.13 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26923076923077
Correctly Classified Instances: 81.73076923076923
Weighted Precision: 0.8191752753350977
Weighted AreaUnderROC: 0.8184523809523809
Root mean squared error: 0.410061009834385
Relative absolute error: 42.307692307692356
Root relative squared error: 82.012201966877
Weighted TruePositiveRate: 0.8173076923076923
Weighted MatthewsCorrelation: 0.6351350870147934
Weighted FMeasure: 0.8175616432497167
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7614950077384978
Mean absolute error: 0.2115384615384618
Coverage of cases: 81.73076923076923
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8173076923076923
Weighted FalsePositiveRate: 0.18040293040293037
Kappa statistic: 0.634074074074074
Training time: 5.0
		
Time end:Sun Oct 08 06.02.13 EEST 2017