Sun Oct 08 06.02.14 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.14 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7309430682312038
Weighted AreaUnderROC: 0.7497217068645639
Root mean squared error: 0.4496793729553741
Relative absolute error: 82.88461538461533
Root relative squared error: 89.93587459107482
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.4587044097705052
Weighted FMeasure: 0.7297642633588881
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7122956086241653
Mean absolute error: 0.41442307692307667
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.2754816611959469
Kappa statistic: 0.4573238911666044
Training time: 8.0
		
Time end:Sun Oct 08 06.02.14 EEST 2017