Sun Oct 08 06.03.01 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.03.01 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 99.03846153846153
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.618790064102564
Weighted AreaUnderROC: 0.649165120593692
Root mean squared error: 0.5000961446023693
Relative absolute error: 89.80769230769229
Root relative squared error: 100.01922892047386
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.21189138534559035
Weighted FMeasure: 0.5711814308992992
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6249305643487403
Mean absolute error: 0.44903846153846144
Coverage of cases: 99.03846153846153
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.3906200941915227
Kappa statistic: 0.191027496382055
Training time: 3.0
		
Time end:Sun Oct 08 06.03.01 EEST 2017