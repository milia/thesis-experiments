Sun Oct 08 06.02.42 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.42 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7471475892528523
Weighted AreaUnderROC: 0.8327752976190477
Root mean squared error: 0.4203935884748591
Relative absolute error: 73.46153846153845
Root relative squared error: 84.07871769497181
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.46217116665408475
Weighted FMeasure: 0.7066930829528814
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8141097989571229
Mean absolute error: 0.3673076923076923
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2651098901098901
Kappa statistic: 0.4347826086956522
Training time: 3.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.7124125874125874
Weighted AreaUnderROC: 0.8028273809523809
Root mean squared error: 0.47251373606407543
Relative absolute error: 83.07692307692307
Root relative squared error: 94.50274721281508
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.2878538665448989
Weighted FMeasure: 0.5216189039718452
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7723077663652862
Mean absolute error: 0.4153846153846153
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 0.0
Accumulative iteration time: 17.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.3685897435897436
Kappa statistic: 0.19662921348314602
Training time: 5.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.7363997504842575
Weighted AreaUnderROC: 0.8128720238095238
Root mean squared error: 0.4312325804731576
Relative absolute error: 74.23076923076924
Root relative squared error: 86.24651609463152
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.4239755882991488
Weighted FMeasure: 0.6723560642926143
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7905829759191357
Mean absolute error: 0.3711538461538462
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.2868589743589744
Kappa statistic: 0.3827338129496404
Training time: 13.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.59615384615384
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.801196516026863
Weighted AreaUnderROC: 0.8993675595238096
Root mean squared error: 0.3787631693572362
Relative absolute error: 65.76923076923077
Root relative squared error: 75.75263387144724
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5814783086247064
Weighted FMeasure: 0.7775140252204472
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8806564903244052
Mean absolute error: 0.3288461538461539
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 0.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.204441391941392
Kappa statistic: 0.5635036496350364
Training time: 17.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 93.75
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7631184407796103
Weighted AreaUnderROC: 0.8614211309523809
Root mean squared error: 0.3968626966596885
Relative absolute error: 64.99999999999999
Root relative squared error: 79.3725393319377
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5138198631008718
Weighted FMeasure: 0.7495375508694045
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8518240698576295
Mean absolute error: 0.32499999999999996
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 87.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.2380952380952381
Kappa statistic: 0.5043988269794722
Training time: 24.0
		
Time end:Sun Oct 08 06.02.42 EEST 2017