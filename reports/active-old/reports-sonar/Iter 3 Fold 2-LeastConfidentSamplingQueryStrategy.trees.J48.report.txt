Sun Oct 08 06.02.28 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.28 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 84.61538461538461
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7270299145299146
Weighted AreaUnderROC: 0.6979166666666666
Root mean squared error: 0.5102943405248993
Relative absolute error: 61.26373626373639
Root relative squared error: 102.05886810497987
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.427548865519963
Weighted FMeasure: 0.7007211538461539
Iteration time: 3.0
Weighted AreaUnderPRC: 0.646760457758054
Mean absolute error: 0.30631868131868195
Coverage of cases: 93.26923076923077
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.31570512820512825
Kappa statistic: 0.40548780487804886
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 68.26923076923077
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6134615384615384
Weighted AreaUnderROC: 0.5803571428571429
Root mean squared error: 0.5921232533086247
Relative absolute error: 80.85826210826208
Root relative squared error: 118.42465066172494
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.21957751641342
Weighted FMeasure: 0.6107226107226107
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5565458579881658
Mean absolute error: 0.4042913105413104
Coverage of cases: 73.07692307692308
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.4010989010989011
Kappa statistic: 0.21686746987951808
Training time: 5.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 53.36538461538461
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6654998719371638
Weighted AreaUnderROC: 0.6724330357142858
Root mean squared error: 0.5699002761767402
Relative absolute error: 67.94871794871796
Root relative squared error: 113.98005523534805
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.32647130640947325
Weighted FMeasure: 0.6639293428284253
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6210183779285712
Mean absolute error: 0.33974358974358976
Coverage of cases: 69.23076923076923
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.33608058608058605
Kappa statistic: 0.32592592592592584
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.681877444589309
Weighted AreaUnderROC: 0.6785714285714286
Root mean squared error: 0.563300712149108
Relative absolute error: 63.46153846153846
Root relative squared error: 112.6601424298216
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.35935553470461673
Weighted FMeasure: 0.6817132376739671
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6238090462340788
Mean absolute error: 0.3173076923076923
Coverage of cases: 68.26923076923077
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.3255494505494506
Kappa statistic: 0.3587443946188341
Training time: 10.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 62.01923076923077
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7223264540337712
Weighted AreaUnderROC: 0.7001488095238096
Root mean squared error: 0.5216001477665123
Relative absolute error: 57.585470085470135
Root relative squared error: 104.32002955330246
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43719546038628915
Weighted FMeasure: 0.7183893436371955
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6548893681173724
Mean absolute error: 0.2879273504273507
Coverage of cases: 77.88461538461539
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2925824175824176
Kappa statistic: 0.43308270676691724
Training time: 14.0
		
Time end:Sun Oct 08 06.02.28 EEST 2017