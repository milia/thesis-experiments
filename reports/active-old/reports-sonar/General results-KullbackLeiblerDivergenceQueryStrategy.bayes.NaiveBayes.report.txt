Sun Oct 08 06.02.11 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.11 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 55.625
Incorrectly Classified Instances: 37.59615384615384
Correctly Classified Instances: 62.40384615384615
Weighted Precision: 0.639223840325456
Weighted AreaUnderROC: 0.6687879568348317
Root mean squared error: 0.5985475929108747
Relative absolute error: 75.66385375436549
Root relative squared error: 119.70951858217492
Weighted TruePositiveRate: 0.6240384615384615
Weighted MatthewsCorrelation: 0.26521079591880226
Weighted FMeasure: 0.6158525251799213
Iteration time: 27.6
Weighted AreaUnderPRC: 0.6586330287572035
Mean absolute error: 0.3783192687718274
Coverage of cases: 67.59615384615384
Instances selection time: 22.3
Test time: 15.3
Accumulative iteration time: 27.6
Weighted Recall: 0.6240384615384615
Weighted FalsePositiveRate: 0.36577547452547454
Kappa statistic: 0.2559860382088997
Training time: 5.3
		
Time end:Sun Oct 08 06.02.12 EEST 2017