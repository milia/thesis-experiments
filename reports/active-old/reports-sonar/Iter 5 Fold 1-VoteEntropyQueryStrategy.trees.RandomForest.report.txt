Sun Oct 08 06.03.01 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.03.01 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 98.5576923076923
Incorrectly Classified Instances: 48.07692307692308
Correctly Classified Instances: 51.92307692307692
Weighted Precision: 0.5615384615384615
Weighted AreaUnderROC: 0.6411830357142858
Root mean squared error: 0.5093359175662726
Relative absolute error: 91.53846153846158
Root relative squared error: 101.86718351325452
Weighted TruePositiveRate: 0.5192307692307693
Weighted MatthewsCorrelation: 0.09507985365695812
Weighted FMeasure: 0.48317307692307687
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6203123332687025
Mean absolute error: 0.4576923076923079
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5192307692307693
Weighted FalsePositiveRate: 0.4388736263736263
Kappa statistic: 0.07670454545454553
Training time: 6.0
		
Time end:Sun Oct 08 06.03.01 EEST 2017