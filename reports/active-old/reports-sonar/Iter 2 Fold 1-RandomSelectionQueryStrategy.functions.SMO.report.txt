Sun Oct 08 06.02.47 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.47 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.53846153846154
Correctly Classified Instances: 63.46153846153846
Weighted Precision: 0.6461827646038173
Weighted AreaUnderROC: 0.6175595238095238
Root mean squared error: 0.6044705248269888
Relative absolute error: 73.07692307692307
Root relative squared error: 120.89410496539776
Weighted TruePositiveRate: 0.6346153846153846
Weighted MatthewsCorrelation: 0.2642509968391208
Weighted FMeasure: 0.6142191142191143
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5787443831472172
Mean absolute error: 0.36538461538461536
Coverage of cases: 63.46153846153846
Instances selection time: 6.0
Test time: 0.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6346153846153846
Weighted FalsePositiveRate: 0.39949633699633696
Kappa statistic: 0.24233128834355824
Training time: 19.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.76923076923077
Correctly Classified Instances: 69.23076923076923
Weighted Precision: 0.6921304501949663
Weighted AreaUnderROC: 0.6860119047619049
Root mean squared error: 0.5547001962252291
Relative absolute error: 61.53846153846154
Root relative squared error: 110.94003924504582
Weighted TruePositiveRate: 0.6923076923076923
Weighted MatthewsCorrelation: 0.3779765684529479
Weighted FMeasure: 0.6898739678400694
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6309194320980922
Mean absolute error: 0.3076923076923077
Coverage of cases: 69.23076923076923
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 42.0
Weighted Recall: 0.6923076923076923
Weighted FalsePositiveRate: 0.3202838827838828
Kappa statistic: 0.37537537537537535
Training time: 12.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.6747638326585695
Weighted AreaUnderROC: 0.6636904761904762
Root mean squared error: 0.5717718748968657
Relative absolute error: 65.38461538461539
Root relative squared error: 114.35437497937313
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.3389255222129955
Weighted FMeasure: 0.6674682542010029
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6128672272396969
Mean absolute error: 0.3269230769230769
Coverage of cases: 67.3076923076923
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.3456959706959707
Kappa statistic: 0.3323262839879155
Training time: 14.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7242603550295859
Weighted AreaUnderROC: 0.7127976190476191
Root mean squared error: 0.5280588545286916
Relative absolute error: 55.769230769230774
Root relative squared error: 105.61177090573833
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43825049008927763
Weighted FMeasure: 0.7171010807374445
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6564595660749506
Mean absolute error: 0.27884615384615385
Coverage of cases: 72.11538461538461
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 82.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.295558608058608
Kappa statistic: 0.43137254901960786
Training time: 16.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7792778649921508
Weighted AreaUnderROC: 0.7782738095238096
Root mean squared error: 0.4702699715629801
Relative absolute error: 44.230769230769226
Root relative squared error: 94.05399431259602
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5558243599778628
Weighted FMeasure: 0.7789899336291088
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7180503562371694
Mean absolute error: 0.22115384615384615
Coverage of cases: 77.88461538461539
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.22229853479853479
Kappa statistic: 0.5557206537890045
Training time: 20.0
		
Time end:Sun Oct 08 06.02.48 EEST 2017