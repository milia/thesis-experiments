Sun Oct 08 06.02.12 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.12 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6624534443825332
Weighted AreaUnderROC: 0.6577380952380951
Root mean squared error: 0.5801193511153214
Relative absolute error: 67.3076923076923
Root relative squared error: 116.02387022306428
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3193611435969891
Weighted FMeasure: 0.6614069690992769
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6067079566962261
Mean absolute error: 0.33653846153846156
Coverage of cases: 66.34615384615384
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.34798534798534797
Kappa statistic: 0.3178410794602699
Training time: 20.0
		
Time end:Sun Oct 08 06.02.12 EEST 2017