Sun Oct 08 06.02.15 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.15 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 99.51923076923077
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6791105399976368
Weighted AreaUnderROC: 0.7491651205936919
Root mean squared error: 0.44978627403211424
Relative absolute error: 82.30769230769232
Root relative squared error: 89.95725480642285
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.3450216287518032
Weighted FMeasure: 0.6606171992769932
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7308452633021798
Mean absolute error: 0.4115384615384616
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.32431497074354215
Kappa statistic: 0.33430870519385514
Training time: 6.0
		
Time end:Sun Oct 08 06.02.15 EEST 2017