Sun Oct 08 06.02.00 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.00 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26923076923077
Correctly Classified Instances: 81.73076923076923
Weighted Precision: 0.8191752753350977
Weighted AreaUnderROC: 0.8184523809523809
Root mean squared error: 0.410061009834385
Relative absolute error: 42.307692307692356
Root relative squared error: 82.012201966877
Weighted TruePositiveRate: 0.8173076923076923
Weighted MatthewsCorrelation: 0.6351350870147934
Weighted FMeasure: 0.8175616432497167
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7614950077384978
Mean absolute error: 0.2115384615384618
Coverage of cases: 81.73076923076923
Instances selection time: 12.0
Test time: 9.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8173076923076923
Weighted FalsePositiveRate: 0.18040293040293037
Kappa statistic: 0.634074074074074
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.78125
Weighted AreaUnderROC: 0.7604166666666666
Root mean squared error: 0.48853058401626637
Relative absolute error: 52.38095238095241
Root relative squared error: 97.70611680325328
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.5336953523937291
Weighted FMeasure: 0.7470238095238094
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7031827847633136
Mean absolute error: 0.26190476190476203
Coverage of cases: 75.0
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 21.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.22916666666666669
Kappa statistic: 0.5087209302325582
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.8165680473372781
Weighted AreaUnderROC: 0.7901785714285714
Root mean squared error: 0.46290387425275126
Relative absolute error: 46.02977667493796
Root relative squared error: 92.58077485055026
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5976143046671968
Weighted FMeasure: 0.7756941707570493
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7359467455621302
Mean absolute error: 0.2301488833746898
Coverage of cases: 77.88461538461539
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.19848901098901098
Kappa statistic: 0.5660377358490567
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.423076923076923
Correctly Classified Instances: 85.57692307692308
Weighted Precision: 0.8556866798748098
Weighted AreaUnderROC: 0.8541666666666666
Root mean squared error: 0.37531542279847707
Relative absolute error: 30.581613508442736
Root relative squared error: 75.06308455969541
Weighted TruePositiveRate: 0.8557692307692307
Weighted MatthewsCorrelation: 0.7095221453585937
Weighted FMeasure: 0.8556483107018737
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8041047949093431
Mean absolute error: 0.15290806754221367
Coverage of cases: 85.57692307692308
Instances selection time: 6.0
Test time: 22.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8557692307692307
Weighted FalsePositiveRate: 0.1474358974358974
Kappa statistic: 0.7093889716840536
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.576923076923077
Correctly Classified Instances: 89.42307692307692
Weighted Precision: 0.8971817355347664
Weighted AreaUnderROC: 0.8898809523809523
Root mean squared error: 0.32216679090446865
Relative absolute error: 22.6998491704374
Root relative squared error: 64.43335818089373
Weighted TruePositiveRate: 0.8942307692307693
Weighted MatthewsCorrelation: 0.7893643360604823
Weighted FMeasure: 0.8935850474312012
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8515978361648322
Mean absolute error: 0.113499245852187
Coverage of cases: 89.42307692307692
Instances selection time: 7.0
Test time: 26.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8942307692307693
Weighted FalsePositiveRate: 0.11446886446886445
Kappa statistic: 0.7856071964017992
Training time: 1.0
		
Time end:Sun Oct 08 06.02.00 EEST 2017