Sun Oct 08 06.01.54 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.01.54 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 61.05769230769231
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6718299504094106
Weighted AreaUnderROC: 0.688290281147424
Root mean squared error: 0.5635760667679719
Relative absolute error: 69.78298867695946
Root relative squared error: 112.71521335359438
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.33638315544628367
Weighted FMeasure: 0.6626207372714267
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6707504291261148
Mean absolute error: 0.3489149433847973
Coverage of cases: 74.03846153846153
Instances selection time: 13.0
Test time: 11.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.32876766091051807
Kappa statistic: 0.33137398971344595
Training time: 1.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 63.46153846153846
Incorrectly Classified Instances: 45.19230769230769
Correctly Classified Instances: 54.80769230769231
Weighted Precision: 0.568947963800905
Weighted AreaUnderROC: 0.5718442272013701
Root mean squared error: 0.6417057399798473
Relative absolute error: 91.22578402689409
Root relative squared error: 128.34114799596946
Weighted TruePositiveRate: 0.5480769230769231
Weighted MatthewsCorrelation: 0.12398289839025253
Weighted FMeasure: 0.534635721185622
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5863058493403559
Mean absolute error: 0.45612892013447043
Coverage of cases: 69.23076923076923
Instances selection time: 8.0
Test time: 11.0
Accumulative iteration time: 23.0
Weighted Recall: 0.5480769230769231
Weighted FalsePositiveRate: 0.43156486370772085
Kappa statistic: 0.11385061638868753
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 62.5
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7115384615384616
Weighted AreaUnderROC: 0.746491008991009
Root mean squared error: 0.5292127931900585
Relative absolute error: 64.14114685501625
Root relative squared error: 105.84255863801171
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.42115027829313545
Weighted FMeasure: 0.7115384615384616
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7423751025916183
Mean absolute error: 0.32070573427508126
Coverage of cases: 79.8076923076923
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2903881832453261
Kappa statistic: 0.42115027829313556
Training time: 2.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 62.98076923076923
Incorrectly Classified Instances: 29.807692307692307
Correctly Classified Instances: 70.1923076923077
Weighted Precision: 0.7249098557692307
Weighted AreaUnderROC: 0.8018552875695732
Root mean squared error: 0.5098728541698687
Relative absolute error: 61.33386954876872
Root relative squared error: 101.97457083397374
Weighted TruePositiveRate: 0.7019230769230769
Weighted MatthewsCorrelation: 0.4294471572305868
Weighted FMeasure: 0.6981745548350113
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7983334226741781
Mean absolute error: 0.3066693477438436
Coverage of cases: 80.76923076923077
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7019230769230769
Weighted FalsePositiveRate: 0.2833702012273441
Kappa statistic: 0.4116788321167882
Training time: 4.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 62.5
Incorrectly Classified Instances: 30.76923076923077
Correctly Classified Instances: 69.23076923076923
Weighted Precision: 0.7119032728788827
Weighted AreaUnderROC: 0.8026188097616669
Root mean squared error: 0.5336610209942535
Relative absolute error: 63.73522530895325
Root relative squared error: 106.7322041988507
Weighted TruePositiveRate: 0.6923076923076923
Weighted MatthewsCorrelation: 0.40668498104161555
Weighted FMeasure: 0.689102564102564
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8151017379209491
Mean absolute error: 0.31867612654476624
Coverage of cases: 79.8076923076923
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6923076923076923
Weighted FalsePositiveRate: 0.2941629798772656
Kappa statistic: 0.39203507489952494
Training time: 2.0
		
Time end:Sun Oct 08 06.01.55 EEST 2017