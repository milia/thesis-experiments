Sun Oct 08 06.03.00 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.03.00 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.753125
Weighted AreaUnderROC: 0.7425595238095238
Root mean squared error: 0.4582575694955842
Relative absolute error: 60.0000000000001
Root relative squared error: 91.65151389911685
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4970990996581592
Weighted FMeasure: 0.746969696969697
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6849320451183433
Mean absolute error: 0.3000000000000005
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.26488095238095244
Kappa statistic: 0.4909638554216867
Training time: 8.0
		
Time end:Sun Oct 08 06.03.00 EEST 2017