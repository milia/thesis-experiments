Sun Oct 08 06.02.56 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.56 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 92.78846153846153
Incorrectly Classified Instances: 42.30769230769231
Correctly Classified Instances: 57.69230769230769
Weighted Precision: 0.5825508944209953
Weighted AreaUnderROC: 0.6649350649350649
Root mean squared error: 0.5082019586436387
Relative absolute error: 83.84615384615385
Root relative squared error: 101.64039172872774
Weighted TruePositiveRate: 0.5769230769230769
Weighted MatthewsCorrelation: 0.16040264617424987
Weighted FMeasure: 0.5762969919462706
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6506027010561506
Mean absolute error: 0.41923076923076924
Coverage of cases: 97.11538461538461
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5769230769230769
Weighted FalsePositiveRate: 0.41699728842585987
Kappa statistic: 0.15851415961750634
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 95.1923076923077
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6143288659491482
Weighted AreaUnderROC: 0.6432282003710574
Root mean squared error: 0.5077287282973668
Relative absolute error: 85.57692307692307
Root relative squared error: 101.54574565947337
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.22454107683784416
Weighted FMeasure: 0.6127897589666707
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6220622398671772
Mean absolute error: 0.42788461538461536
Coverage of cases: 98.07692307692308
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.3938632795775653
Kappa statistic: 0.22301083302203967
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 94.71153846153847
Incorrectly Classified Instances: 39.42307692307692
Correctly Classified Instances: 60.57692307692308
Weighted Precision: 0.6107772435897436
Weighted AreaUnderROC: 0.6948051948051948
Root mean squared error: 0.4714951667914447
Relative absolute error: 80.38461538461547
Root relative squared error: 94.29903335828894
Weighted TruePositiveRate: 0.6057692307692307
Weighted MatthewsCorrelation: 0.21697956298601062
Weighted FMeasure: 0.605514065222803
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6957781735929607
Mean absolute error: 0.4019230769230773
Coverage of cases: 99.03846153846153
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6057692307692307
Weighted FalsePositiveRate: 0.3890716426430712
Kappa statistic: 0.215022091310751
Training time: 6.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 96.15384615384616
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6458949704142013
Weighted AreaUnderROC: 0.7525046382189239
Root mean squared error: 0.4474285503768256
Relative absolute error: 75.76923076923082
Root relative squared error: 89.48571007536512
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.2889427981985323
Weighted FMeasure: 0.6445270515549039
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7501110617958453
Mean absolute error: 0.37884615384615405
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.354805908377337
Kappa statistic: 0.28846153846153855
Training time: 8.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 92.78846153846153
Incorrectly Classified Instances: 30.76923076923077
Correctly Classified Instances: 69.23076923076923
Weighted Precision: 0.6942136454331576
Weighted AreaUnderROC: 0.7808905380333951
Root mean squared error: 0.43367836362553647
Relative absolute error: 68.46153846153847
Root relative squared error: 86.73567272510729
Weighted TruePositiveRate: 0.6923076923076923
Weighted MatthewsCorrelation: 0.3816698750316
Weighted FMeasure: 0.689062726350862
Iteration time: 11.0
Weighted AreaUnderPRC: 0.767252198530479
Mean absolute error: 0.34230769230769237
Coverage of cases: 99.03846153846153
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6923076923076923
Weighted FalsePositiveRate: 0.31865277579563295
Kappa statistic: 0.37701235492324964
Training time: 11.0
		
Time end:Sun Oct 08 06.02.57 EEST 2017