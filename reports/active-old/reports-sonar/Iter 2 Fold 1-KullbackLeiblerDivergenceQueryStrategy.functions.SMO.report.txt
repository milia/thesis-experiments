Sun Oct 08 06.02.12 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.12 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6670430217894953
Weighted AreaUnderROC: 0.6593692022263451
Root mean squared error: 0.5883484054145521
Relative absolute error: 69.23076923076923
Root relative squared error: 117.66968108291043
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.32308339196601027
Weighted FMeasure: 0.6515341801056086
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6070924423680369
Mean absolute error: 0.34615384615384615
Coverage of cases: 65.38461538461539
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.33510774939346366
Kappa statistic: 0.314536799707067
Training time: 15.0
		
Time end:Sun Oct 08 06.02.12 EEST 2017