Sun Oct 08 06.02.14 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.14 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 57.21153846153846
Incorrectly Classified Instances: 41.34615384615385
Correctly Classified Instances: 58.65384615384615
Weighted Precision: 0.5859257164404224
Weighted AreaUnderROC: 0.5695732838589982
Root mean squared error: 0.6304251719561151
Relative absolute error: 85.89743589743587
Root relative squared error: 126.08503439122303
Weighted TruePositiveRate: 0.5865384615384616
Weighted MatthewsCorrelation: 0.16351748504193211
Weighted FMeasure: 0.576770407975573
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5415025236942999
Mean absolute error: 0.42948717948717935
Coverage of cases: 63.46153846153846
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5865384615384616
Weighted FalsePositiveRate: 0.43069430569430567
Kappa statistic: 0.1581325301204819
Training time: 7.0
		
Time end:Sun Oct 08 06.02.14 EEST 2017