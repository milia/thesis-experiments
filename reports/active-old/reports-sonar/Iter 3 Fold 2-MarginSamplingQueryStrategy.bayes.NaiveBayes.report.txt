Sun Oct 08 06.02.33 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.33 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 57.21153846153846
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6153846153846154
Weighted AreaUnderROC: 0.6446171016483516
Root mean squared error: 0.6064046914204985
Relative absolute error: 78.89847096397969
Root relative squared error: 121.28093828409969
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.22619047619047622
Weighted FMeasure: 0.6153846153846154
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6298040316487713
Mean absolute error: 0.3944923548198984
Coverage of cases: 67.3076923076923
Instances selection time: 15.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.38919413919413925
Kappa statistic: 0.22619047619047625
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 56.25
Incorrectly Classified Instances: 38.46153846153846
Correctly Classified Instances: 61.53846153846154
Weighted Precision: 0.6203418803418803
Weighted AreaUnderROC: 0.6617874313186813
Root mean squared error: 0.6117502414603099
Relative absolute error: 79.08409423658026
Root relative squared error: 122.35004829206197
Weighted TruePositiveRate: 0.6153846153846154
Weighted MatthewsCorrelation: 0.23459597901201695
Weighted FMeasure: 0.6158114915051651
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6545605924905531
Mean absolute error: 0.39542047118290125
Coverage of cases: 68.26923076923077
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6153846153846154
Weighted FalsePositiveRate: 0.3802655677655678
Kappa statistic: 0.23303834808259594
Training time: 1.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 58.65384615384615
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.652782701169798
Weighted AreaUnderROC: 0.6752661401098901
Root mean squared error: 0.5814680438728317
Relative absolute error: 73.38764964552857
Root relative squared error: 116.29360877456634
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.29935744221473476
Weighted FMeasure: 0.6511082138200782
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6541816730262423
Mean absolute error: 0.3669382482276428
Coverage of cases: 71.15384615384616
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.35920329670329676
Kappa statistic: 0.2972972972972973
Training time: 1.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 60.09615384615385
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.682718365645195
Weighted AreaUnderROC: 0.7619619963369964
Root mean squared error: 0.5416151105824818
Relative absolute error: 64.4304194148954
Root relative squared error: 108.32302211649636
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.3582573911498758
Weighted FMeasure: 0.6795464944837053
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7756162737541685
Mean absolute error: 0.322152097074477
Coverage of cases: 77.88461538461539
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 36.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.33150183150183155
Kappa statistic: 0.3548872180451128
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 63.94230769230769
Incorrectly Classified Instances: 25.96153846153846
Correctly Classified Instances: 74.03846153846153
Weighted Precision: 0.740859140859141
Weighted AreaUnderROC: 0.8146033653846154
Root mean squared error: 0.4674479312252099
Relative absolute error: 53.13271220710143
Root relative squared error: 93.48958624504198
Weighted TruePositiveRate: 0.7403846153846154
Weighted MatthewsCorrelation: 0.47854396768147545
Weighted FMeasure: 0.7405534003472147
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7971125401290793
Mean absolute error: 0.26566356103550715
Coverage of cases: 85.57692307692308
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7403846153846154
Weighted FalsePositiveRate: 0.2612179487179487
Kappa statistic: 0.47845468053491835
Training time: 2.0
		
Time end:Sun Oct 08 06.02.33 EEST 2017