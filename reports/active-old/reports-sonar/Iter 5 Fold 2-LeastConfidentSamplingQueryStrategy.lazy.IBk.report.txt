Sun Oct 08 06.02.25 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.25 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.6732088917563565
Weighted AreaUnderROC: 0.6686456400742113
Root mean squared error: 0.5470547622544537
Relative absolute error: 68.53146853146855
Root relative squared error: 109.41095245089075
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.34188917729581303
Weighted FMeasure: 0.67087129512167
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6148311505864146
Mean absolute error: 0.3426573426573427
Coverage of cases: 67.3076923076923
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.3357856429285001
Kappa statistic: 0.3395592080687338
Training time: 1.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6814015939015938
Weighted AreaUnderROC: 0.6517625231910947
Root mean squared error: 0.5666389626003993
Relative absolute error: 68.86446886446885
Root relative squared error: 113.32779252007985
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.33442399582306603
Weighted FMeasure: 0.6477755636572239
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6051596900154592
Mean absolute error: 0.34432234432234426
Coverage of cases: 66.34615384615384
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3599364920793492
Kappa statistic: 0.3100833965125095
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.73076923076923
Correctly Classified Instances: 68.26923076923077
Weighted Precision: 0.7577152014652015
Weighted AreaUnderROC: 0.6654916512059368
Root mean squared error: 0.5543754197106401
Relative absolute error: 64.64019851116626
Root relative squared error: 110.87508394212801
Weighted TruePositiveRate: 0.6826923076923077
Weighted MatthewsCorrelation: 0.4192092227886528
Weighted FMeasure: 0.6491121484083636
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6279224781628627
Mean absolute error: 0.3232009925558313
Coverage of cases: 68.26923076923077
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6826923076923077
Weighted FalsePositiveRate: 0.35170900528043386
Kappa statistic: 0.3420245398773007
Training time: 1.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7956600831600832
Weighted AreaUnderROC: 0.7482374768089054
Root mean squared error: 0.48442780368636545
Relative absolute error: 49.34333958724203
Root relative squared error: 96.8855607372731
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5470162670064332
Weighted FMeasure: 0.7484111168980172
Iteration time: 2.0
Weighted AreaUnderPRC: 0.699927534783304
Mean absolute error: 0.24671669793621012
Coverage of cases: 75.96153846153847
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.26314043099757384
Kappa statistic: 0.5072024260803639
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.307692307692307
Correctly Classified Instances: 82.6923076923077
Weighted Precision: 0.8334152297566931
Weighted AreaUnderROC: 0.8218923933209648
Root mean squared error: 0.4120429283420307
Relative absolute error: 35.89743589743591
Root relative squared error: 82.40858566840615
Weighted TruePositiveRate: 0.8269230769230769
Weighted MatthewsCorrelation: 0.6575940746572255
Weighted FMeasure: 0.8250977835723597
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7698223706666484
Mean absolute error: 0.17948717948717957
Coverage of cases: 82.6923076923077
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8269230769230769
Weighted FalsePositiveRate: 0.18313829028114742
Kappa statistic: 0.6495694496443278
Training time: 0.0
		
Time end:Sun Oct 08 06.02.26 EEST 2017