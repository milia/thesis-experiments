Sun Oct 08 06.02.37 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.37 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 40.38461538461539
Correctly Classified Instances: 59.61538461538461
Weighted Precision: 0.6043386933217442
Weighted AreaUnderROC: 0.6003710575139146
Root mean squared error: 0.6076174642283964
Relative absolute error: 82.51748251748255
Root relative squared error: 121.52349284567927
Weighted TruePositiveRate: 0.5961538461538461
Weighted MatthewsCorrelation: 0.2022486435907443
Weighted FMeasure: 0.5946581196581197
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5618747840961009
Mean absolute error: 0.4125874125874127
Coverage of cases: 59.61538461538461
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5961538461538461
Weighted FalsePositiveRate: 0.39541173112601685
Kappa statistic: 0.19853211009174312
Training time: 0.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.6820958646616541
Weighted AreaUnderROC: 0.6387755102040816
Root mean squared error: 0.5980884136704984
Relative absolute error: 76.19047619047616
Root relative squared error: 119.61768273409967
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.31234632557204534
Weighted FMeasure: 0.6045060240963855
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5951298827690528
Mean absolute error: 0.3809523809523808
Coverage of cases: 62.5
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.3474489795918367
Kappa statistic: 0.26945244956772335
Training time: 0.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.7397273612463486
Weighted AreaUnderROC: 0.6693877551020407
Root mean squared error: 0.5790058192571909
Relative absolute error: 70.22332506203475
Root relative squared error: 115.80116385143818
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.39573808037821073
Weighted FMeasure: 0.6295072115384616
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6250980263650663
Mean absolute error: 0.35111662531017374
Coverage of cases: 65.38461538461539
Instances selection time: 10.0
Test time: 10.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3150706436420722
Kappa statistic: 0.3278276481149012
Training time: 0.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.192307692307693
Correctly Classified Instances: 79.8076923076923
Weighted Precision: 0.8019586894586894
Weighted AreaUnderROC: 0.8001855287569574
Root mean squared error: 0.4440122124748343
Relative absolute error: 41.83864915572228
Root relative squared error: 88.80244249496685
Weighted TruePositiveRate: 0.7980769230769231
Weighted MatthewsCorrelation: 0.5998149007873067
Weighted FMeasure: 0.7981702763256162
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7409025038351961
Mean absolute error: 0.2091932457786114
Coverage of cases: 79.8076923076923
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7980769230769231
Weighted FalsePositiveRate: 0.19770586556300843
Kappa statistic: 0.5970479704797048
Training time: 0.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.346153846153847
Correctly Classified Instances: 83.65384615384616
Weighted Precision: 0.8371223042324991
Weighted AreaUnderROC: 0.8343228200371058
Root mean squared error: 0.4004404164598452
Relative absolute error: 34.012066365007584
Root relative squared error: 80.08808329196904
Weighted TruePositiveRate: 0.8365384615384616
Weighted MatthewsCorrelation: 0.6720204502374758
Weighted FMeasure: 0.836127333309448
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7807963159692343
Mean absolute error: 0.17006033182503794
Coverage of cases: 83.65384615384616
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8365384615384616
Weighted FalsePositiveRate: 0.16789282146425005
Kappa statistic: 0.6708860759493671
Training time: 0.0
		
Time end:Sun Oct 08 06.02.38 EEST 2017