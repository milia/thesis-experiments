Sun Oct 08 06.02.56 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.56 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 100.0
Incorrectly Classified Instances: 45.19230769230769
Correctly Classified Instances: 54.80769230769231
Weighted Precision: 0.5577574967405476
Weighted AreaUnderROC: 0.5617559523809523
Root mean squared error: 0.5222915484080337
Relative absolute error: 95.57692307692308
Root relative squared error: 104.45830968160675
Weighted TruePositiveRate: 0.5480769230769231
Weighted MatthewsCorrelation: 0.10780666041138504
Weighted FMeasure: 0.5466969414410887
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5484672894213717
Mean absolute error: 0.4778846153846154
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 0.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5480769230769231
Weighted FalsePositiveRate: 0.440934065934066
Kappa statistic: 0.10541727672035149
Training time: 2.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 21.153846153846153
Correctly Classified Instances: 78.84615384615384
Weighted Precision: 0.7895156695156695
Weighted AreaUnderROC: 0.8463541666666666
Root mean squared error: 0.40251612485617355
Relative absolute error: 68.65384615384617
Root relative squared error: 80.50322497123472
Weighted TruePositiveRate: 0.7884615384615384
Weighted MatthewsCorrelation: 0.5760964547890037
Weighted FMeasure: 0.7886970172684458
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8247281024197874
Mean absolute error: 0.3432692307692309
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7884615384615384
Weighted FalsePositiveRate: 0.21108058608058608
Kappa statistic: 0.5756676557863502
Training time: 4.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 95.67307692307692
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7703133903133903
Weighted AreaUnderROC: 0.8567708333333334
Root mean squared error: 0.39370039370059046
Relative absolute error: 64.2307692307692
Root relative squared error: 78.7400787401181
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5374920531794312
Weighted FMeasure: 0.769487655201941
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8252857714087015
Mean absolute error: 0.321153846153846
Coverage of cases: 98.07692307692308
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.23054029304029303
Kappa statistic: 0.5370919881305639
Training time: 11.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 97.11538461538461
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7637952132957684
Weighted AreaUnderROC: 0.8392857142857142
Root mean squared error: 0.403232133945391
Relative absolute error: 68.26923076923077
Root relative squared error: 80.64642678907819
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5223540902551571
Weighted FMeasure: 0.7599490358813021
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8236863729222696
Mean absolute error: 0.34134615384615385
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.23580586080586083
Kappa statistic: 0.519940915805022
Training time: 13.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 99.51923076923077
Incorrectly Classified Instances: 20.192307692307693
Correctly Classified Instances: 79.8076923076923
Weighted Precision: 0.7999658499103561
Weighted AreaUnderROC: 0.871279761904762
Root mean squared error: 0.3884139346308066
Relative absolute error: 67.5
Root relative squared error: 77.68278692616131
Weighted TruePositiveRate: 0.7980769230769231
Weighted MatthewsCorrelation: 0.5965521144391284
Weighted FMeasure: 0.7983576056970552
Iteration time: 24.0
Weighted AreaUnderPRC: 0.844480360193807
Mean absolute error: 0.3375
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.7980769230769231
Weighted FalsePositiveRate: 0.19986263736263735
Kappa statistic: 0.5955555555555556
Training time: 16.0
		
Time end:Sun Oct 08 06.02.56 EEST 2017