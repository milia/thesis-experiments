Sun Oct 08 06.02.13 EEST 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 06.02.13 EEST 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.61538461538461
Correctly Classified Instances: 65.38461538461539
Weighted Precision: 0.6538461538461539
Weighted AreaUnderROC: 0.6517857142857142
Root mean squared error: 0.5628067433744858
Relative absolute error: 72.02797202797207
Root relative squared error: 112.56134867489716
Weighted TruePositiveRate: 0.6538461538461539
Weighted MatthewsCorrelation: 0.30357142857142855
Weighted FMeasure: 0.6538461538461539
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6013049450549451
Mean absolute error: 0.3601398601398604
Coverage of cases: 65.38461538461539
Instances selection time: 10.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6538461538461539
Weighted FalsePositiveRate: 0.3502747252747253
Kappa statistic: 0.30357142857142855
Training time: 5.0
		
Time end:Sun Oct 08 06.02.13 EEST 2017