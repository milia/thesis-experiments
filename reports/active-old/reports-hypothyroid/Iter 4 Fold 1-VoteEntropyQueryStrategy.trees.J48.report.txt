Sat Oct 07 12.53.19 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.53.19 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.145811240721102
Incorrectly Classified Instances: 2.8101802757158008
Correctly Classified Instances: 97.1898197242842
Weighted Precision: 0.9802663636657112
Weighted AreaUnderROC: 0.983161545206467
Root mean squared error: 0.11281274875752734
Relative absolute error: 4.043016015444436
Root relative squared error: 26.052988345272013
Weighted TruePositiveRate: 0.971898197242842
Weighted MatthewsCorrelation: 0.8542577041232559
Weighted FMeasure: 0.9740085195655708
Iteration time: 5.0
Weighted AreaUnderPRC: 0.977558696518475
Mean absolute error: 0.015161310057916632
Coverage of cases: 97.4019088016967
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.971898197242842
Weighted FalsePositiveRate: 0.014183649103915416
Kappa statistic: 0.8285154521031124
Training time: 3.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 27.332979851537647
Incorrectly Classified Instances: 1.4846235418875928
Correctly Classified Instances: 98.51537645811241
Weighted Precision: 0.9869937577567895
Weighted AreaUnderROC: 0.9889753665496682
Root mean squared error: 0.07967400854185468
Relative absolute error: 3.160837084835192
Root relative squared error: 18.399924111622536
Weighted TruePositiveRate: 0.985153764581124
Weighted MatthewsCorrelation: 0.9245753844242912
Weighted FMeasure: 0.9854407580517921
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9858763777453089
Mean absolute error: 0.011853139068131971
Coverage of cases: 99.78791092258749
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.985153764581124
Weighted FalsePositiveRate: 0.00709695349446519
Kappa statistic: 0.9031465273753013
Training time: 5.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 25.861611876988334
Incorrectly Classified Instances: 1.007423117709438
Correctly Classified Instances: 98.99257688229056
Weighted Precision: 0.9896522023570133
Weighted AreaUnderROC: 0.9827986842021523
Root mean squared error: 0.0675556730392552
Relative absolute error: 2.125892781968107
Root relative squared error: 15.601314405800135
Weighted TruePositiveRate: 0.9899257688229056
Weighted MatthewsCorrelation: 0.945643091971669
Weighted FMeasure: 0.9897420796921689
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9895420452553324
Mean absolute error: 0.007972097932380401
Coverage of cases: 99.41675503711559
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9899257688229056
Weighted FalsePositiveRate: 0.03217333752854739
Kappa statistic: 0.9311845385096961
Training time: 6.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 25.83510074231177
Incorrectly Classified Instances: 1.007423117709438
Correctly Classified Instances: 98.99257688229056
Weighted Precision: 0.9896522023570133
Weighted AreaUnderROC: 0.9822318944263085
Root mean squared error: 0.06764350786936778
Relative absolute error: 2.097634683557715
Root relative squared error: 15.621598990924022
Weighted TruePositiveRate: 0.9899257688229056
Weighted MatthewsCorrelation: 0.945643091971669
Weighted FMeasure: 0.9897420796921689
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9894388283823706
Mean absolute error: 0.007866130063341432
Coverage of cases: 99.41675503711559
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9899257688229056
Weighted FalsePositiveRate: 0.03217333752854739
Kappa statistic: 0.9311845385096961
Training time: 7.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.861611876988334
Incorrectly Classified Instances: 0.5832449628844114
Correctly Classified Instances: 99.41675503711559
Weighted Precision: 0.9937753209015969
Weighted AreaUnderROC: 0.9951708069793299
Root mean squared error: 0.0509690581324467
Relative absolute error: 1.6012922930810172
Root relative squared error: 11.770799773243914
Weighted TruePositiveRate: 0.9941675503711559
Weighted MatthewsCorrelation: 0.975061244669086
Weighted FMeasure: 0.993922673724526
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9947616711801193
Mean absolute error: 0.006004846099053814
Coverage of cases: 99.84093319194062
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9941675503711559
Weighted FalsePositiveRate: 0.006593026210430394
Kappa statistic: 0.9601594696635083
Training time: 9.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.145811240721102
Incorrectly Classified Instances: 0.5302226935312832
Correctly Classified Instances: 99.46977730646871
Weighted Precision: 0.9942962859057582
Weighted AreaUnderROC: 0.9960327177255233
Root mean squared error: 0.049616617395954125
Relative absolute error: 0.8797748153328846
Root relative squared error: 11.458466963933114
Weighted TruePositiveRate: 0.9946977730646872
Weighted MatthewsCorrelation: 0.9756150946700572
Weighted FMeasure: 0.9944641377579969
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9959274567723454
Mean absolute error: 0.0032991555574983173
Coverage of cases: 99.68186638388123
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 47.0
Weighted Recall: 0.9946977730646872
Weighted FalsePositiveRate: 0.0065642774115077926
Kappa statistic: 0.9637848134322629
Training time: 12.0
		
Time end:Sat Oct 07 12.53.19 EEST 2017