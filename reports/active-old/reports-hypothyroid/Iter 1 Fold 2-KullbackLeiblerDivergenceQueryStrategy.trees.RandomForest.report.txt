Sat Oct 07 12.05.20 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.05.20 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 28.724814422057264
Incorrectly Classified Instances: 5.567338282078473
Correctly Classified Instances: 94.43266171792153
Weighted Precision: 0.9381350917313985
Weighted AreaUnderROC: 0.9463746416866773
Root mean squared error: 0.140331324880357
Relative absolute error: 8.236264553924785
Root relative squared error: 32.408131278164376
Weighted TruePositiveRate: 0.9443266171792153
Weighted MatthewsCorrelation: 0.5338094472448897
Weighted FMeasure: 0.9287702552881278
Iteration time: 46.0
Weighted AreaUnderPRC: 0.9701227798795854
Mean absolute error: 0.03088599207721795
Coverage of cases: 98.83351007423117
Instances selection time: 19.0
Test time: 23.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9443266171792153
Weighted FalsePositiveRate: 0.6257328342116809
Kappa statistic: 0.4456590199728473
Training time: 27.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 27.942735949098623
Incorrectly Classified Instances: 6.30965005302227
Correctly Classified Instances: 93.69034994697773
Weighted Precision: 0.9216343375272874
Weighted AreaUnderROC: 0.9013260296479411
Root mean squared error: 0.15230280526786127
Relative absolute error: 8.471806615672957
Root relative squared error: 35.172826247893944
Weighted TruePositiveRate: 0.9369034994697774
Weighted MatthewsCorrelation: 0.4515369226205701
Weighted FMeasure: 0.9167852655665353
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9514841183825097
Mean absolute error: 0.03176927480877359
Coverage of cases: 98.03817603393425
Instances selection time: 15.0
Test time: 24.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9369034994697774
Weighted FalsePositiveRate: 0.7016343421606083
Kappa statistic: 0.334274620543594
Training time: 36.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 27.87645811240721
Incorrectly Classified Instances: 5.938494167550371
Correctly Classified Instances: 94.06150583244963
Weighted Precision: 0.9326754311484932
Weighted AreaUnderROC: 0.9010311672061438
Root mean squared error: 0.14084381237220597
Relative absolute error: 7.673058659832251
Root relative squared error: 32.52648519471451
Weighted TruePositiveRate: 0.9406150583244963
Weighted MatthewsCorrelation: 0.48175924532032877
Weighted FMeasure: 0.921099961759336
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9601450650899511
Mean absolute error: 0.028773969974370945
Coverage of cases: 98.1442205726405
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 176.0
Weighted Recall: 0.9406150583244963
Weighted FalsePositiveRate: 0.6825620698400322
Kappa statistic: 0.38001860836437223
Training time: 67.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 28.19459172852598
Incorrectly Classified Instances: 5.9915164369035
Correctly Classified Instances: 94.0084835630965
Weighted Precision: 0.9246163927852948
Weighted AreaUnderROC: 0.9399789430666847
Root mean squared error: 0.14171599211584507
Relative absolute error: 7.8001038106651945
Root relative squared error: 32.72790647862322
Weighted TruePositiveRate: 0.940084835630965
Weighted MatthewsCorrelation: 0.4715067040087401
Weighted FMeasure: 0.9182735217970629
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9676311431097041
Mean absolute error: 0.029250389289994478
Coverage of cases: 98.83351007423117
Instances selection time: 10.0
Test time: 21.0
Accumulative iteration time: 286.0
Weighted Recall: 0.940084835630965
Weighted FalsePositiveRate: 0.682575916787415
Kappa statistic: 0.37801916858313567
Training time: 100.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 27.584835630965006
Incorrectly Classified Instances: 5.673382820784729
Correctly Classified Instances: 94.32661717921528
Weighted Precision: 0.9377559013830992
Weighted AreaUnderROC: 0.9241070847476458
Root mean squared error: 0.1344614685710538
Relative absolute error: 7.004824576567265
Root relative squared error: 31.052546030052127
Weighted TruePositiveRate: 0.9432661717921527
Weighted MatthewsCorrelation: 0.5026100825168547
Weighted FMeasure: 0.9245715942201141
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9696232040687933
Mean absolute error: 0.026268092162127245
Coverage of cases: 98.83351007423117
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 388.0
Weighted Recall: 0.9432661717921527
Weighted FalsePositiveRate: 0.6509517061136832
Kappa statistic: 0.42767441860465083
Training time: 94.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 29.48038176033934
Incorrectly Classified Instances: 2.0678685047720045
Correctly Classified Instances: 97.93213149522799
Weighted Precision: 0.9781038215165574
Weighted AreaUnderROC: 0.9974853701400201
Root mean squared error: 0.08880241316894334
Relative absolute error: 5.616673916915325
Root relative squared error: 20.508038859111124
Weighted TruePositiveRate: 0.9793213149522799
Weighted MatthewsCorrelation: 0.8578070505094715
Weighted FMeasure: 0.9781740797938262
Iteration time: 160.0
Weighted AreaUnderPRC: 0.996196659670813
Mean absolute error: 0.021062527188432465
Coverage of cases: 99.94697773064688
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 548.0
Weighted Recall: 0.9793213149522799
Weighted FalsePositiveRate: 0.18978680488453986
Kappa statistic: 0.8460288918241703
Training time: 159.0
		
Time end:Sat Oct 07 12.05.21 EEST 2017