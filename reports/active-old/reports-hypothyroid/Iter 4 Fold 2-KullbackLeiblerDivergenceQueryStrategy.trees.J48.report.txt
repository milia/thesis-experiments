Sat Oct 07 12.05.17 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.05.17 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 27.187168610816542
Incorrectly Classified Instances: 1.378579003181336
Correctly Classified Instances: 98.62142099681867
Weighted Precision: 0.9850421508846403
Weighted AreaUnderROC: 0.9731259998612714
Root mean squared error: 0.0734706373086145
Relative absolute error: 2.4429602169605897
Root relative squared error: 16.96731689106478
Weighted TruePositiveRate: 0.9862142099681867
Weighted MatthewsCorrelation: 0.9304985947135088
Weighted FMeasure: 0.9854603859321622
Iteration time: 5.0
Weighted AreaUnderPRC: 0.983913266999797
Mean absolute error: 0.009161100813602212
Coverage of cases: 99.31071049840934
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9862142099681867
Weighted FalsePositiveRate: 0.07612727872367991
Kappa statistic: 0.903032076773853
Training time: 3.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 26.59066808059385
Incorrectly Classified Instances: 1.8027571580063626
Correctly Classified Instances: 98.19724284199364
Weighted Precision: 0.9821521469711656
Weighted AreaUnderROC: 0.9858313288446339
Root mean squared error: 0.08387340488177335
Relative absolute error: 3.090020966889529
Root relative squared error: 19.369733154536927
Weighted TruePositiveRate: 0.9819724284199364
Weighted MatthewsCorrelation: 0.9070878649748346
Weighted FMeasure: 0.9814659673663462
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9848294438350548
Mean absolute error: 0.011587578625835733
Coverage of cases: 99.31071049840934
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9819724284199364
Weighted FalsePositiveRate: 0.09533261280395744
Kappa statistic: 0.8731343283582089
Training time: 7.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 26.564156945917286
Incorrectly Classified Instances: 1.855779427359491
Correctly Classified Instances: 98.1442205726405
Weighted Precision: 0.9814406464540115
Weighted AreaUnderROC: 0.9875394371380686
Root mean squared error: 0.08653708213417581
Relative absolute error: 3.2087376699085013
Root relative squared error: 19.984883066020462
Weighted TruePositiveRate: 0.9814422057264051
Weighted MatthewsCorrelation: 0.9034608091949046
Weighted FMeasure: 0.9809440376669011
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9840625646276244
Mean absolute error: 0.01203276626215688
Coverage of cases: 99.15164369034994
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9814422057264051
Weighted FalsePositiveRate: 0.0953464597513402
Kappa statistic: 0.8698387033166382
Training time: 10.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 26.444856839872745
Incorrectly Classified Instances: 1.5906680805938493
Correctly Classified Instances: 98.40933191940616
Weighted Precision: 0.983484512849825
Weighted AreaUnderROC: 0.9857250571522819
Root mean squared error: 0.0857633861681089
Relative absolute error: 2.8566614857835018
Root relative squared error: 19.8062056363086
Weighted TruePositiveRate: 0.9840933191940615
Weighted MatthewsCorrelation: 0.901360745614733
Weighted FMeasure: 0.9833834862622142
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9878629273600505
Mean absolute error: 0.010712480571688133
Coverage of cases: 99.31071049840934
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9840933191940615
Weighted FalsePositiveRate: 0.12666933548846337
Kappa statistic: 0.8850938867023286
Training time: 9.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.808589607635206
Incorrectly Classified Instances: 0.3181336161187699
Correctly Classified Instances: 99.68186638388123
Weighted Precision: 0.9963730459208174
Weighted AreaUnderROC: 0.9920920021539151
Root mean squared error: 0.04165955654598353
Relative absolute error: 1.0366302089752046
Root relative squared error: 9.62086247445761
Weighted TruePositiveRate: 0.9968186638388123
Weighted MatthewsCorrelation: 0.9869746994381693
Weighted FMeasure: 0.9965290977886122
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9966449433082963
Mean absolute error: 0.003887363283657017
Coverage of cases: 99.68186638388123
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9968186638388123
Weighted FalsePositiveRate: 0.01275317994561414
Kappa statistic: 0.978059009553134
Training time: 12.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.848356309650054
Incorrectly Classified Instances: 0.3181336161187699
Correctly Classified Instances: 99.68186638388123
Weighted Precision: 0.9963730459208174
Weighted AreaUnderROC: 0.9958336239801565
Root mean squared error: 0.03962243562836356
Relative absolute error: 1.0002406288175119
Root relative squared error: 9.150409550393729
Weighted TruePositiveRate: 0.9968186638388123
Weighted MatthewsCorrelation: 0.9869746994381693
Weighted FMeasure: 0.9965290977886122
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9977089515048594
Mean absolute error: 0.003750902358065669
Coverage of cases: 99.73488865323436
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 62.0
Weighted Recall: 0.9968186638388123
Weighted FalsePositiveRate: 0.01275317994561414
Kappa statistic: 0.978059009553134
Training time: 14.0
		
Time end:Sat Oct 07 12.05.18 EEST 2017