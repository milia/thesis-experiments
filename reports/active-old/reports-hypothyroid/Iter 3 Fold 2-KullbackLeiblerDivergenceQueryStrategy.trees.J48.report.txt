Sat Oct 07 12.05.17 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.05.17 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 27.200424178154826
Incorrectly Classified Instances: 1.696712619300106
Correctly Classified Instances: 98.3032873806999
Weighted Precision: 0.985151881174944
Weighted AreaUnderROC: 0.9855610153401337
Root mean squared error: 0.08665917135518693
Relative absolute error: 3.0890556810350884
Root relative squared error: 20.013078363866835
Weighted TruePositiveRate: 0.9830328738069989
Weighted MatthewsCorrelation: 0.9246613754620381
Weighted FMeasure: 0.9832091696761064
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9799241633724879
Mean absolute error: 0.011583958803881581
Coverage of cases: 99.36373276776246
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9830328738069989
Weighted FalsePositiveRate: 0.013455943162982244
Kappa statistic: 0.8889430711789674
Training time: 3.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 27.266702014846235
Incorrectly Classified Instances: 1.2195121951219512
Correctly Classified Instances: 98.78048780487805
Weighted Precision: 0.9886756272351163
Weighted AreaUnderROC: 0.989769045150732
Root mean squared error: 0.06794119928062956
Relative absolute error: 2.2812450481709363
Root relative squared error: 15.690347877494993
Weighted TruePositiveRate: 0.9878048780487805
Weighted MatthewsCorrelation: 0.9483854930824535
Weighted FMeasure: 0.987957954200939
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9900102239248932
Mean absolute error: 0.008554668930641012
Coverage of cases: 99.78791092258749
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9878048780487805
Weighted FalsePositiveRate: 0.013033283605740499
Kappa statistic: 0.9186094292077656
Training time: 5.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 26.352067868504772
Incorrectly Classified Instances: 2.3329798515376456
Correctly Classified Instances: 97.66702014846236
Weighted Precision: 0.9754399370112163
Weighted AreaUnderROC: 0.9323535233445407
Root mean squared error: 0.10034253364536809
Relative absolute error: 3.669279427696416
Root relative squared error: 23.173115524528942
Weighted TruePositiveRate: 0.9766702014846236
Weighted MatthewsCorrelation: 0.83721530279676
Weighted FMeasure: 0.9758954556453485
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9695089809195201
Mean absolute error: 0.01375979785386156
Coverage of cases: 98.46235418875928
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9766702014846236
Weighted FalsePositiveRate: 0.17733495987553452
Kappa statistic: 0.8321313916158752
Training time: 16.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 26.352067868504772
Incorrectly Classified Instances: 2.3329798515376456
Correctly Classified Instances: 97.66702014846236
Weighted Precision: 0.9754399370112163
Weighted AreaUnderROC: 0.9323535233445407
Root mean squared error: 0.1003180112565497
Relative absolute error: 3.6217688042878287
Root relative squared error: 23.16745232141475
Weighted TruePositiveRate: 0.9766702014846236
Weighted MatthewsCorrelation: 0.83721530279676
Weighted FMeasure: 0.9758954556453485
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9695089809195201
Mean absolute error: 0.013581633016079358
Coverage of cases: 98.46235418875928
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 51.0
Weighted Recall: 0.9766702014846236
Weighted FalsePositiveRate: 0.17733495987553452
Kappa statistic: 0.8321313916158752
Training time: 21.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 26.352067868504772
Incorrectly Classified Instances: 2.3329798515376456
Correctly Classified Instances: 97.66702014846236
Weighted Precision: 0.9754399370112163
Weighted AreaUnderROC: 0.9323535233445407
Root mean squared error: 0.10016082847651481
Relative absolute error: 3.588107374699565
Root relative squared error: 23.131152513268702
Weighted TruePositiveRate: 0.9766702014846236
Weighted MatthewsCorrelation: 0.83721530279676
Weighted FMeasure: 0.9758954556453485
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9695089809195201
Mean absolute error: 0.01345540265512337
Coverage of cases: 98.46235418875928
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9766702014846236
Weighted FalsePositiveRate: 0.17733495987553452
Kappa statistic: 0.8321313916158752
Training time: 26.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.38441145281018
Incorrectly Classified Instances: 1.007423117709438
Correctly Classified Instances: 98.99257688229056
Weighted Precision: 0.9893935554481234
Weighted AreaUnderROC: 0.9936966914434365
Root mean squared error: 0.06616018492827104
Relative absolute error: 1.5838631013554911
Root relative squared error: 15.279040231189084
Weighted TruePositiveRate: 0.9899257688229056
Weighted MatthewsCorrelation: 0.9544784233843503
Weighted FMeasure: 0.9895981572544761
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9905915479380877
Mean absolute error: 0.0059394866300830915
Coverage of cases: 99.2576882290562
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 91.0
Weighted Recall: 0.9899257688229056
Weighted FalsePositiveRate: 0.038228681229605634
Kappa statistic: 0.9305201969182564
Training time: 12.0
		
Time end:Sat Oct 07 12.05.17 EEST 2017