Sat Oct 07 12.52.38 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.52.38 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 11.187698833510074
Correctly Classified Instances: 88.81230116648993
Weighted Precision: 0.892571405186613
Weighted AreaUnderROC: 0.6293875857328923
Root mean squared error: 0.23531212931359638
Relative absolute error: 15.807423703259381
Root relative squared error: 54.34300848111556
Weighted TruePositiveRate: 0.8881230116648993
Weighted MatthewsCorrelation: 0.2540697646303361
Weighted FMeasure: 0.8900138826586996
Iteration time: 375.0
Weighted AreaUnderPRC: 0.8806195097146741
Mean absolute error: 0.059277838887222684
Coverage of cases: 88.81230116648993
Instances selection time: 375.0
Test time: 464.0
Accumulative iteration time: 375.0
Weighted Recall: 0.8881230116648993
Weighted FalsePositiveRate: 0.6332888693788322
Kappa statistic: 0.23255266791634874
Training time: 0.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.21845174973489
Correctly Classified Instances: 91.78154825026512
Weighted Precision: 0.9016248475492001
Weighted AreaUnderROC: 0.6268980134560623
Root mean squared error: 0.20213195312935237
Relative absolute error: 11.478665830098608
Root relative squared error: 46.68037502042257
Weighted TruePositiveRate: 0.9178154825026511
Weighted MatthewsCorrelation: 0.32292179381105157
Weighted FMeasure: 0.9072218855870954
Iteration time: 523.0
Weighted AreaUnderPRC: 0.8830845961040151
Mean absolute error: 0.043044996862869785
Coverage of cases: 91.78154825026512
Instances selection time: 522.0
Test time: 794.0
Accumulative iteration time: 898.0
Weighted Recall: 0.9178154825026511
Weighted FalsePositiveRate: 0.6698563950527765
Kappa statistic: 0.288945860352888
Training time: 1.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.741251325556734
Correctly Classified Instances: 92.25874867444327
Weighted Precision: 0.9017131544990324
Weighted AreaUnderROC: 0.6256801307018833
Root mean squared error: 0.19634540215198182
Relative absolute error: 10.684344214158772
Root relative squared error: 45.344028314636816
Weighted TruePositiveRate: 0.9225874867444327
Weighted MatthewsCorrelation: 0.33854609539680414
Weighted FMeasure: 0.9089791467475323
Iteration time: 541.0
Weighted AreaUnderPRC: 0.8828842826860555
Mean absolute error: 0.04006629080309539
Coverage of cases: 92.25874867444327
Instances selection time: 540.0
Test time: 1110.0
Accumulative iteration time: 1439.0
Weighted Recall: 0.9225874867444327
Weighted FalsePositiveRate: 0.6822728163258412
Kappa statistic: 0.2935060936497756
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.158006362672323
Correctly Classified Instances: 92.84199363732768
Weighted Precision: 0.9055545905908834
Weighted AreaUnderROC: 0.6296041347553933
Root mean squared error: 0.18889171499730625
Relative absolute error: 9.823629784503742
Root relative squared error: 43.62267300055394
Weighted TruePositiveRate: 0.9284199363732768
Weighted MatthewsCorrelation: 0.37611133262841495
Weighted FMeasure: 0.9125357303792362
Iteration time: 469.0
Weighted AreaUnderPRC: 0.8838000899109116
Mean absolute error: 0.03683861169188903
Coverage of cases: 92.84199363732768
Instances selection time: 468.0
Test time: 1453.0
Accumulative iteration time: 1908.0
Weighted Recall: 0.9284199363732768
Weighted FalsePositiveRate: 0.6819565795376926
Kappa statistic: 0.3149494438555052
Training time: 1.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.0
Incorrectly Classified Instances: 6.786850477200424
Correctly Classified Instances: 93.21314952279958
Weighted Precision: 0.9084253225721318
Weighted AreaUnderROC: 0.6286654329322178
Root mean squared error: 0.18398255753710147
Relative absolute error: 9.276847111974346
Root relative squared error: 42.48895164809654
Weighted TruePositiveRate: 0.9321314952279958
Weighted MatthewsCorrelation: 0.40273071397207216
Weighted FMeasure: 0.9141594550662356
Iteration time: 299.0
Weighted AreaUnderPRC: 0.8838432141401708
Mean absolute error: 0.034788176669903796
Coverage of cases: 93.21314952279958
Instances selection time: 298.0
Test time: 1785.0
Accumulative iteration time: 2207.0
Weighted Recall: 0.9321314952279958
Weighted FalsePositiveRate: 0.6880929181769184
Kappa statistic: 0.3235407851598624
Training time: 1.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.16542948038176
Correctly Classified Instances: 91.83457051961824
Weighted Precision: 0.9048149380961715
Weighted AreaUnderROC: 0.6688426492931105
Root mean squared error: 0.20184440224003192
Relative absolute error: 11.074547197388329
Root relative squared error: 46.61396798708062
Weighted TruePositiveRate: 0.9183457051961824
Weighted MatthewsCorrelation: 0.3801833897511353
Weighted FMeasure: 0.9108500526470907
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8888655351229502
Mean absolute error: 0.041529551990206234
Coverage of cases: 91.83457051961824
Instances selection time: 12.0
Test time: 2110.0
Accumulative iteration time: 2220.0
Weighted Recall: 0.9183457051961824
Weighted FalsePositiveRate: 0.5936247064501395
Kappa statistic: 0.3468794833394051
Training time: 1.0
		
Time end:Sat Oct 07 12.52.48 EEST 2017