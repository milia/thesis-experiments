Sat Oct 07 12.53.20 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.53.20 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 30.29427359490986
Incorrectly Classified Instances: 4.480381760339342
Correctly Classified Instances: 95.51961823966064
Weighted Precision: 0.9508998994154739
Weighted AreaUnderROC: 0.9863709956260953
Root mean squared error: 0.11827106368764466
Relative absolute error: 7.976878094529252
Root relative squared error: 27.313532182962007
Weighted TruePositiveRate: 0.9551961823966065
Weighted MatthewsCorrelation: 0.6479388389952427
Weighted FMeasure: 0.9467399765215114
Iteration time: 20.9
Weighted AreaUnderPRC: 0.9836501853381833
Mean absolute error: 0.029913292854484692
Coverage of cases: 99.64475079533403
Instances selection time: 6.9
Test time: 12.8
Accumulative iteration time: 20.9
Weighted Recall: 0.9551961823966065
Weighted FalsePositiveRate: 0.46082866026474456
Kappa statistic: 0.595235820518995
Training time: 14.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 29.21129374337221
Incorrectly Classified Instances: 5.615058324496287
Correctly Classified Instances: 94.38494167550371
Weighted Precision: 0.9383306591742244
Weighted AreaUnderROC: 0.9733846174925418
Root mean squared error: 0.1326996066534576
Relative absolute error: 8.048541994696292
Root relative squared error: 30.64566144909248
Weighted TruePositiveRate: 0.9438494167550371
Weighted MatthewsCorrelation: 0.5156827192469925
Weighted FMeasure: 0.9277175765805901
Iteration time: 26.3
Weighted AreaUnderPRC: 0.977414555563383
Mean absolute error: 0.030182032480111098
Coverage of cases: 99.37433722163308
Instances selection time: 5.5
Test time: 11.8
Accumulative iteration time: 47.2
Weighted Recall: 0.9438494167550371
Weighted FalsePositiveRate: 0.631837155816087
Kappa statistic: 0.4273548860221935
Training time: 20.8
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 28.533934252386
Incorrectly Classified Instances: 5.774125132555673
Correctly Classified Instances: 94.22587486744432
Weighted Precision: 0.9372499329764361
Weighted AreaUnderROC: 0.970430670575517
Root mean squared error: 0.13534995933569677
Relative absolute error: 7.822408972481706
Root relative squared error: 31.257734182907768
Weighted TruePositiveRate: 0.9422587486744431
Weighted MatthewsCorrelation: 0.486570203190099
Weighted FMeasure: 0.9242310099350842
Iteration time: 31.3
Weighted AreaUnderPRC: 0.9771607221101398
Mean absolute error: 0.029334033646806396
Coverage of cases: 99.32661717921528
Instances selection time: 4.4
Test time: 11.7
Accumulative iteration time: 78.5
Weighted Recall: 0.9422587486744431
Weighted FalsePositiveRate: 0.6620682653668825
Kappa statistic: 0.3942458577592497
Training time: 26.9
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 28.48621420996819
Incorrectly Classified Instances: 6.155885471898197
Correctly Classified Instances: 93.8441145281018
Weighted Precision: 0.9316864568352585
Weighted AreaUnderROC: 0.9686372024041541
Root mean squared error: 0.13992919747588567
Relative absolute error: 8.022579709273604
Root relative squared error: 32.315263932076356
Weighted TruePositiveRate: 0.9384411452810179
Weighted MatthewsCorrelation: 0.43820223239780026
Weighted FMeasure: 0.9182152764280884
Iteration time: 38.3
Weighted AreaUnderPRC: 0.9757934166200171
Mean absolute error: 0.03008467390977601
Coverage of cases: 99.25238600212089
Instances selection time: 3.1
Test time: 12.3
Accumulative iteration time: 116.8
Weighted Recall: 0.9384411452810179
Weighted FalsePositiveRate: 0.7059400079402397
Kappa statistic: 0.3393254850564261
Training time: 35.2
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 28.195917285259803
Incorrectly Classified Instances: 5.6839872746553555
Correctly Classified Instances: 94.31601272534465
Weighted Precision: 0.937973339975489
Weighted AreaUnderROC: 0.9757137251572209
Root mean squared error: 0.13247554703392733
Relative absolute error: 7.39900416109175
Root relative squared error: 30.593917096432346
Weighted TruePositiveRate: 0.9431601272534463
Weighted MatthewsCorrelation: 0.4969662603947674
Weighted FMeasure: 0.9253153198020758
Iteration time: 43.3
Weighted AreaUnderPRC: 0.9803969275620682
Mean absolute error: 0.027746265604094062
Coverage of cases: 99.34782608695653
Instances selection time: 1.7
Test time: 11.7
Accumulative iteration time: 160.1
Weighted Recall: 0.9431601272534463
Weighted FalsePositiveRate: 0.6550198009157179
Kappa statistic: 0.40529301652645167
Training time: 41.6
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 29.023064687168606
Incorrectly Classified Instances: 1.5429480381760339
Correctly Classified Instances: 98.45705196182396
Weighted Precision: 0.9837589483877496
Weighted AreaUnderROC: 0.9968567125095099
Root mean squared error: 0.08138067569712257
Relative absolute error: 4.878959828722994
Root relative squared error: 18.794062008226938
Weighted TruePositiveRate: 0.9845705196182397
Weighted MatthewsCorrelation: 0.89932059813532
Weighted FMeasure: 0.9838833231184078
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9957078460871811
Mean absolute error: 0.01829609935771123
Coverage of cases: 99.8727465535525
Instances selection time: 0.4
Test time: 15.5
Accumulative iteration time: 227.1
Weighted Recall: 0.9845705196182397
Weighted FalsePositiveRate: 0.11881894974237613
Kappa statistic: 0.8883765811758758
Training time: 66.6
		
Time end:Sat Oct 07 12.53.26 EEST 2017