Sat Oct 07 12.03.14 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.03.14 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 10.52492046659597
Correctly Classified Instances: 89.47507953340404
Weighted Precision: 0.8876701311547969
Weighted AreaUnderROC: 0.6025926110142504
Root mean squared error: 0.22793951458618705
Relative absolute error: 14.933153385352146
Root relative squared error: 52.640376042115086
Weighted TruePositiveRate: 0.8947507953340402
Weighted MatthewsCorrelation: 0.22352003196986647
Weighted FMeasure: 0.8898965549402101
Iteration time: 683.5
Weighted AreaUnderPRC: 0.875196696160226
Mean absolute error: 0.055999325195070546
Coverage of cases: 89.47507953340404
Instances selection time: 681.0
Test time: 454.4
Accumulative iteration time: 683.5
Weighted Recall: 0.8947507953340402
Weighted FalsePositiveRate: 0.6936057911011063
Kappa statistic: 0.20389374257282117
Training time: 2.5
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.605514316012725
Correctly Classified Instances: 91.39448568398728
Weighted Precision: 0.8894015085006153
Weighted AreaUnderROC: 0.5928139421648593
Root mean squared error: 0.20656021504528227
Relative absolute error: 11.991363237287914
Root relative squared error: 47.70303830410428
Weighted TruePositiveRate: 0.9139448568398727
Weighted MatthewsCorrelation: 0.2540885095007742
Weighted FMeasure: 0.8985987092757479
Iteration time: 992.9
Weighted AreaUnderPRC: 0.8741752057123818
Mean absolute error: 0.04496761213982967
Coverage of cases: 91.39448568398728
Instances selection time: 991.1
Test time: 794.0
Accumulative iteration time: 1676.4
Weighted Recall: 0.9139448568398727
Weighted FalsePositiveRate: 0.7356219588319981
Kappa statistic: 0.21710358445368533
Training time: 1.8
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.027571580063626
Correctly Classified Instances: 91.97242841993638
Weighted Precision: 0.8903164866626858
Weighted AreaUnderROC: 0.58622506883654
Root mean squared error: 0.19974086276907438
Relative absolute error: 11.064715356917384
Root relative squared error: 46.12817635515728
Weighted TruePositiveRate: 0.9197242841993635
Weighted MatthewsCorrelation: 0.26038135040395455
Weighted FMeasure: 0.9005548939910586
Iteration time: 1049.5
Weighted AreaUnderPRC: 0.8735184444384965
Mean absolute error: 0.04149268258844018
Coverage of cases: 91.97242841993638
Instances selection time: 1048.2
Test time: 1082.9
Accumulative iteration time: 2725.9
Weighted Recall: 0.9197242841993635
Weighted FalsePositiveRate: 0.7586976649794194
Kappa statistic: 0.21309312109839335
Training time: 1.3
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.677624602332981
Correctly Classified Instances: 92.32237539766702
Weighted Precision: 0.89178167023869
Weighted AreaUnderROC: 0.5835021129248228
Root mean squared error: 0.19544980597757852
Relative absolute error: 10.514780067668685
Root relative squared error: 45.13719923768603
Weighted TruePositiveRate: 0.9232237539766702
Weighted MatthewsCorrelation: 0.27228742817917695
Weighted FMeasure: 0.9018618774534873
Iteration time: 869.0
Weighted AreaUnderPRC: 0.8732744895557681
Mean absolute error: 0.03943042525375756
Coverage of cases: 92.32237539766702
Instances selection time: 866.5
Test time: 1396.4
Accumulative iteration time: 3594.9
Weighted Recall: 0.9232237539766702
Weighted FalsePositiveRate: 0.7686238215445002
Kappa statistic: 0.21414359955390153
Training time: 2.5
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.428419936373277
Correctly Classified Instances: 92.57158006362673
Weighted Precision: 0.8944660401944974
Weighted AreaUnderROC: 0.5822749697547434
Root mean squared error: 0.19234903566265538
Relative absolute error: 10.130306180726635
Root relative squared error: 44.42110700727961
Weighted TruePositiveRate: 0.9257158006362672
Weighted MatthewsCorrelation: 0.28490530074313464
Weighted FMeasure: 0.9030403768540032
Iteration time: 556.6
Weighted AreaUnderPRC: 0.8735896635301412
Mean absolute error: 0.03798864817772488
Coverage of cases: 92.57158006362673
Instances selection time: 554.2
Test time: 1731.8
Accumulative iteration time: 4151.5
Weighted Recall: 0.9257158006362672
Weighted FalsePositiveRate: 0.7748451025935098
Kappa statistic: 0.21548970032171946
Training time: 2.4
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.97136797454931
Correctly Classified Instances: 91.0286320254507
Weighted Precision: 0.9011238758358642
Weighted AreaUnderROC: 0.6475344522586183
Root mean squared error: 0.21141973811644982
Relative absolute error: 12.147086838966366
Root relative squared error: 48.825297085413
Weighted TruePositiveRate: 0.910286320254507
Weighted MatthewsCorrelation: 0.32340250938970566
Weighted FMeasure: 0.9047887148847977
Iteration time: 25.9
Weighted AreaUnderPRC: 0.8865139764045574
Mean absolute error: 0.045551575646123874
Coverage of cases: 91.0286320254507
Instances selection time: 23.2
Test time: 2056.4
Accumulative iteration time: 4177.4
Weighted Recall: 0.910286320254507
Weighted FalsePositiveRate: 0.6266030547661445
Kappa statistic: 0.3039190878933438
Training time: 2.7
		
Time end:Sat Oct 07 12.05.14 EEST 2017