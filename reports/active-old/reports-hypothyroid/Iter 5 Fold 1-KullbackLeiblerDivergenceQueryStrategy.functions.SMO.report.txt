Sat Oct 07 12.03.09 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.03.09 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 75.0
Incorrectly Classified Instances: 7.4761399787910925
Correctly Classified Instances: 92.5238600212089
Weighted Precision: 0.8788779225638212
Weighted AreaUnderROC: 0.5145158539551488
Root mean squared error: 0.3245801958269141
Relative absolute error: 68.83468834688331
Root relative squared error: 74.95858537371613
Weighted TruePositiveRate: 0.925238600212089
Weighted MatthewsCorrelation: 0.15465276995777924
Weighted FMeasure: 0.8911008853091638
Iteration time: 122.0
Weighted AreaUnderPRC: 0.8596273838998879
Mean absolute error: 0.2581300813008124
Coverage of cases: 99.94697773064688
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 122.0
Weighted Recall: 0.925238600212089
Weighted FalsePositiveRate: 0.8976523933155374
Kappa statistic: 0.05109796393168771
Training time: 112.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 75.0
Incorrectly Classified Instances: 7.52916224814422
Correctly Classified Instances: 92.47083775185578
Weighted Precision: 0.8784244135590585
Weighted AreaUnderROC: 0.5142201197216368
Root mean squared error: 0.3247049580278733
Relative absolute error: 68.85825379992914
Root relative squared error: 74.98739796983952
Weighted TruePositiveRate: 0.9247083775185578
Weighted MatthewsCorrelation: 0.13389765903760548
Weighted FMeasure: 0.8899374384040415
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8590672058543446
Mean absolute error: 0.2582184517497343
Coverage of cases: 99.94697773064688
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 288.0
Weighted Recall: 0.9247083775185578
Weighted FalsePositiveRate: 0.904018722346144
Kappa statistic: 0.03855653522502121
Training time: 158.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 75.0
Incorrectly Classified Instances: 7.52916224814422
Correctly Classified Instances: 92.47083775185578
Weighted Precision: 0.8784244135590585
Weighted AreaUnderROC: 0.5142201197216368
Root mean squared error: 0.3247049580278733
Relative absolute error: 68.85825379992914
Root relative squared error: 74.98739796983952
Weighted TruePositiveRate: 0.9247083775185578
Weighted MatthewsCorrelation: 0.13389765903760548
Weighted FMeasure: 0.8899374384040415
Iteration time: 275.0
Weighted AreaUnderPRC: 0.8590672058543446
Mean absolute error: 0.2582184517497343
Coverage of cases: 99.94697773064688
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 563.0
Weighted Recall: 0.9247083775185578
Weighted FalsePositiveRate: 0.904018722346144
Kappa statistic: 0.03855653522502121
Training time: 269.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 75.0
Incorrectly Classified Instances: 7.52916224814422
Correctly Classified Instances: 92.47083775185578
Weighted Precision: 0.8784244135590585
Weighted AreaUnderROC: 0.5107718438595679
Root mean squared error: 0.3247843272025618
Relative absolute error: 68.87003652645207
Root relative squared error: 75.00572749558822
Weighted TruePositiveRate: 0.9247083775185578
Weighted MatthewsCorrelation: 0.13389765903760548
Weighted FMeasure: 0.8899374384040415
Iteration time: 298.0
Weighted AreaUnderPRC: 0.8584571003642625
Mean absolute error: 0.25826263697419527
Coverage of cases: 99.94697773064688
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 861.0
Weighted Recall: 0.9247083775185578
Weighted FalsePositiveRate: 0.904018722346144
Kappa statistic: 0.03855653522502121
Training time: 294.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 75.0
Incorrectly Classified Instances: 7.52916224814422
Correctly Classified Instances: 92.47083775185578
Weighted Precision: 0.8784244135590585
Weighted AreaUnderROC: 0.5142201197216368
Root mean squared error: 0.3247049580278733
Relative absolute error: 68.85825379992914
Root relative squared error: 74.98739796983952
Weighted TruePositiveRate: 0.9247083775185578
Weighted MatthewsCorrelation: 0.13389765903760548
Weighted FMeasure: 0.8899374384040415
Iteration time: 489.0
Weighted AreaUnderPRC: 0.8590672058543446
Mean absolute error: 0.2582184517497343
Coverage of cases: 99.94697773064688
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 1350.0
Weighted Recall: 0.9247083775185578
Weighted FalsePositiveRate: 0.904018722346144
Kappa statistic: 0.03855653522502121
Training time: 486.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 75.0
Incorrectly Classified Instances: 6.574761399787911
Correctly Classified Instances: 93.42523860021208
Weighted Precision: 0.8860080976361119
Weighted AreaUnderROC: 0.5827056150290172
Root mean squared error: 0.3214912195040773
Relative absolute error: 68.30446565335193
Root relative squared error: 74.24521684911203
Weighted TruePositiveRate: 0.9342523860021209
Weighted MatthewsCorrelation: 0.35521866325244705
Weighted FMeasure: 0.9070210160536333
Iteration time: 1021.0
Weighted AreaUnderPRC: 0.8801231608684124
Mean absolute error: 0.25614174620006974
Coverage of cases: 99.94697773064688
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2371.0
Weighted Recall: 0.9342523860021209
Weighted FalsePositiveRate: 0.7830720218611897
Kappa statistic: 0.2514723203769146
Training time: 1021.0
		
Time end:Sat Oct 07 12.03.12 EEST 2017