Sat Oct 07 12.04.51 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.04.51 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 11.346765641569458
Correctly Classified Instances: 88.65323435843054
Weighted Precision: 0.8836738078678535
Weighted AreaUnderROC: 0.5606784718116671
Root mean squared error: 0.23697856359821096
Relative absolute error: 16.019224167151666
Root relative squared error: 54.72785499423918
Weighted TruePositiveRate: 0.8865323435843054
Weighted MatthewsCorrelation: 0.13646277782184466
Weighted FMeasure: 0.8813640973176148
Iteration time: 620.0
Weighted AreaUnderPRC: 0.8667497844881473
Mean absolute error: 0.060072090626818744
Coverage of cases: 88.65323435843054
Instances selection time: 619.0
Test time: 406.0
Accumulative iteration time: 620.0
Weighted Recall: 0.8865323435843054
Weighted FalsePositiveRate: 0.76660107996212
Kappa statistic: 0.12852978645304772
Training time: 1.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 25.0
Incorrectly Classified Instances: 10.710498409331919
Correctly Classified Instances: 89.28950159066808
Weighted Precision: 0.8783520288463132
Weighted AreaUnderROC: 0.5431494908432978
Root mean squared error: 0.2307474556182522
Relative absolute error: 14.782913421469859
Root relative squared error: 53.28884224640765
Weighted TruePositiveRate: 0.8928950159066809
Weighted MatthewsCorrelation: 0.10601456757186627
Weighted FMeasure: 0.8815863279875457
Iteration time: 850.0
Weighted AreaUnderPRC: 0.863634766277709
Mean absolute error: 0.05543592533051197
Coverage of cases: 89.28950159066808
Instances selection time: 848.0
Test time: 689.0
Accumulative iteration time: 1470.0
Weighted Recall: 0.8928950159066809
Weighted FalsePositiveRate: 0.8106191559968371
Kappa statistic: 0.09364889041148113
Training time: 2.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 25.0
Incorrectly Classified Instances: 9.75609756097561
Correctly Classified Instances: 90.2439024390244
Weighted Precision: 0.8803807948718227
Weighted AreaUnderROC: 0.5469704945551231
Root mean squared error: 0.22041930360889464
Relative absolute error: 13.359816420208569
Root relative squared error: 50.90365770927407
Weighted TruePositiveRate: 0.9024390243902439
Weighted MatthewsCorrelation: 0.11619685525723805
Weighted FMeasure: 0.8862733461754857
Iteration time: 916.0
Weighted AreaUnderPRC: 0.8644890036023823
Mean absolute error: 0.05009931157578214
Coverage of cases: 90.2439024390244
Instances selection time: 915.0
Test time: 981.0
Accumulative iteration time: 2386.0
Weighted Recall: 0.9024390243902439
Weighted FalsePositiveRate: 0.822792035781949
Kappa statistic: 0.10041942959648727
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 25.0
Incorrectly Classified Instances: 9.066808059384941
Correctly Classified Instances: 90.93319194061506
Weighted Precision: 0.8785427025059107
Weighted AreaUnderROC: 0.5404798289396331
Root mean squared error: 0.2125894907563196
Relative absolute error: 12.361413324387023
Root relative squared error: 49.09543988601864
Weighted TruePositiveRate: 0.9093319194061505
Weighted MatthewsCorrelation: 0.1154026206619587
Weighted FMeasure: 0.8881174099711798
Iteration time: 795.0
Weighted AreaUnderPRC: 0.8630949473431381
Mean absolute error: 0.04635529996645134
Coverage of cases: 90.93319194061506
Instances selection time: 790.0
Test time: 1265.0
Accumulative iteration time: 3181.0
Weighted Recall: 0.9093319194061505
Weighted FalsePositiveRate: 0.8414310420910073
Kappa statistic: 0.09018946275629333
Training time: 5.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.642629904559914
Correctly Classified Instances: 91.35737009544009
Weighted Precision: 0.8819979990265535
Weighted AreaUnderROC: 0.5445363453372505
Root mean squared error: 0.2076174422236679
Relative absolute error: 11.744952834382515
Root relative squared error: 47.947194462518496
Weighted TruePositiveRate: 0.9135737009544008
Weighted MatthewsCorrelation: 0.13380360490411783
Weighted FMeasure: 0.890532295905436
Iteration time: 502.0
Weighted AreaUnderPRC: 0.8639825488735063
Mean absolute error: 0.04404357312893443
Coverage of cases: 91.35737009544009
Instances selection time: 499.0
Test time: 1555.0
Accumulative iteration time: 3683.0
Weighted Recall: 0.9135737009544008
Weighted FalsePositiveRate: 0.841216249401977
Kappa statistic: 0.09920474457474034
Training time: 3.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.0
Incorrectly Classified Instances: 10.07423117709438
Correctly Classified Instances: 89.92576882290562
Weighted Precision: 0.8987507293374392
Weighted AreaUnderROC: 0.653695445817649
Root mean squared error: 0.22419829062969682
Relative absolute error: 13.613851026123227
Root relative squared error: 51.77637737876377
Weighted TruePositiveRate: 0.8992576882290562
Weighted MatthewsCorrelation: 0.3018718821786194
Weighted FMeasure: 0.8986755434862806
Iteration time: 30.0
Weighted AreaUnderPRC: 0.8859395127647921
Mean absolute error: 0.0510519413479621
Coverage of cases: 89.92576882290562
Instances selection time: 24.0
Test time: 1862.0
Accumulative iteration time: 3713.0
Weighted Recall: 0.8992576882290562
Weighted FalsePositiveRate: 0.6073652190794225
Kappa statistic: 0.2816015910056856
Training time: 6.0
		
Time end:Sat Oct 07 12.05.02 EEST 2017