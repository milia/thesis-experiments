Sat Oct 07 12.04.13 EEST 2017
Dataset: hypothyroid
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.04.13 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 25.0
Incorrectly Classified Instances: 10.445387062566278
Correctly Classified Instances: 89.55461293743372
Weighted Precision: 0.884643612910567
Weighted AreaUnderROC: 0.5992239049542719
Root mean squared error: 0.22737449013100033
Relative absolute error: 14.826309671405214
Root relative squared error: 52.509889233594784
Weighted TruePositiveRate: 0.8955461293743372
Weighted MatthewsCorrelation: 0.21242386097845584
Weighted FMeasure: 0.8898642293447109
Iteration time: 677.0
Weighted AreaUnderPRC: 0.8753323972712983
Mean absolute error: 0.05559866126776956
Coverage of cases: 89.55461293743372
Instances selection time: 676.0
Test time: 468.0
Accumulative iteration time: 677.0
Weighted Recall: 0.8955461293743372
Weighted FalsePositiveRate: 0.7036532207034735
Kappa statistic: 0.20872244950984634
Training time: 1.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 25.0
Incorrectly Classified Instances: 8.907741251325557
Correctly Classified Instances: 91.09225874867444
Weighted Precision: 0.882320381050668
Weighted AreaUnderROC: 0.5797124406816511
Root mean squared error: 0.21043650546142947
Relative absolute error: 12.391854464780266
Root relative squared error: 48.59822923019219
Weighted TruePositiveRate: 0.9109225874867445
Weighted MatthewsCorrelation: 0.21110893640662712
Weighted FMeasure: 0.8950547625921771
Iteration time: 953.0
Weighted AreaUnderPRC: 0.8720706863822887
Mean absolute error: 0.046469454242926
Coverage of cases: 91.09225874867444
Instances selection time: 952.0
Test time: 787.0
Accumulative iteration time: 1630.0
Weighted Recall: 0.9109225874867445
Weighted FalsePositiveRate: 0.7595220088283103
Kappa statistic: 0.19475245184622322
Training time: 1.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.582184517497349
Correctly Classified Instances: 92.41781548250265
Weighted Precision: 0.888343702029972
Weighted AreaUnderROC: 0.5802146600041762
Root mean squared error: 0.19431783833144817
Relative absolute error: 10.47281430146054
Root relative squared error: 44.87578250760311
Weighted TruePositiveRate: 0.9241781548250265
Weighted MatthewsCorrelation: 0.26838748565483145
Weighted FMeasure: 0.9016211366294301
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.873377103163625
Mean absolute error: 0.03927305363047702
Coverage of cases: 92.41781548250265
Instances selection time: 1033.0
Test time: 1104.0
Accumulative iteration time: 2664.0
Weighted Recall: 0.9241781548250265
Weighted FalsePositiveRate: 0.7777786325441423
Kappa statistic: 0.21165139357799548
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.317073170731708
Correctly Classified Instances: 92.6829268292683
Weighted Precision: 0.8877112036854143
Weighted AreaUnderROC: 0.5787602797106101
Root mean squared error: 0.19097887681051998
Relative absolute error: 10.035230544623621
Root relative squared error: 44.10468237443443
Weighted TruePositiveRate: 0.926829268292683
Weighted MatthewsCorrelation: 0.28659501394546977
Weighted FMeasure: 0.9023320672786311
Iteration time: 959.0
Weighted AreaUnderPRC: 0.8735548665392929
Mean absolute error: 0.03763211454233858
Coverage of cases: 92.6829268292683
Instances selection time: 952.0
Test time: 1428.0
Accumulative iteration time: 3623.0
Weighted Recall: 0.926829268292683
Weighted FalsePositiveRate: 0.7839401339771083
Kappa statistic: 0.21263329854335902
Training time: 7.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 25.0
Incorrectly Classified Instances: 7.317073170731708
Correctly Classified Instances: 92.6829268292683
Weighted Precision: 0.8877112036854143
Weighted AreaUnderROC: 0.5780287084553279
Root mean squared error: 0.191034002602741
Relative absolute error: 9.982498078715166
Root relative squared error: 44.117413130825675
Weighted TruePositiveRate: 0.926829268292683
Weighted MatthewsCorrelation: 0.28659501394546977
Weighted FMeasure: 0.9023320672786311
Iteration time: 599.0
Weighted AreaUnderPRC: 0.8734434640247101
Mean absolute error: 0.037434367795181875
Coverage of cases: 92.6829268292683
Instances selection time: 597.0
Test time: 1754.0
Accumulative iteration time: 4222.0
Weighted Recall: 0.926829268292683
Weighted FalsePositiveRate: 0.7839401339771083
Kappa statistic: 0.21263329854335902
Training time: 2.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 25.0
Incorrectly Classified Instances: 9.172852598091199
Correctly Classified Instances: 90.8271474019088
Weighted Precision: 0.8968859128429336
Weighted AreaUnderROC: 0.6235219978401515
Root mean squared error: 0.21393356462590427
Relative absolute error: 12.415481019033562
Root relative squared error: 49.40584045018481
Weighted TruePositiveRate: 0.908271474019088
Weighted MatthewsCorrelation: 0.28523500667934665
Weighted FMeasure: 0.9006868222483235
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8810486932383428
Mean absolute error: 0.04655805382137586
Coverage of cases: 90.8271474019088
Instances selection time: 38.0
Test time: 2339.0
Accumulative iteration time: 4267.0
Weighted Recall: 0.908271474019088
Weighted FalsePositiveRate: 0.671690353867613
Kappa statistic: 0.25717264899667147
Training time: 7.0
		
Time end:Sat Oct 07 12.04.26 EEST 2017