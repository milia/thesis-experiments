Sun Oct 08 04.52.45 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.45 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 10.64935064935065
Correctly Classified Instances: 89.35064935064935
Weighted Precision: 0.8961100551357132
Weighted AreaUnderROC: 0.9384782893873803
Root mean squared error: 0.17215396721350731
Relative absolute error: 14.986203436598293
Root relative squared error: 49.1970939680403
Weighted TruePositiveRate: 0.8935064935064935
Weighted MatthewsCorrelation: 0.876844347368174
Weighted FMeasure: 0.8941656449163439
Iteration time: 159.0
Weighted AreaUnderPRC: 0.8269981949938073
Mean absolute error: 0.036700906375342915
Coverage of cases: 89.35064935064935
Instances selection time: 158.0
Test time: 102.0
Accumulative iteration time: 159.0
Weighted Recall: 0.8935064935064935
Weighted FalsePositiveRate: 0.01774891774891775
Kappa statistic: 0.8757575757575756
Training time: 1.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 10.476190476190476
Correctly Classified Instances: 89.52380952380952
Weighted Precision: 0.8972864689055521
Weighted AreaUnderROC: 0.9401128164764527
Root mean squared error: 0.17193878122315046
Relative absolute error: 13.351442271062647
Root relative squared error: 49.13559944915382
Weighted TruePositiveRate: 0.8952380952380953
Weighted MatthewsCorrelation: 0.8785625861183016
Weighted FMeasure: 0.8955212190363354
Iteration time: 174.0
Weighted AreaUnderPRC: 0.82965681996209
Mean absolute error: 0.032697409643418864
Coverage of cases: 89.52380952380952
Instances selection time: 173.0
Test time: 148.0
Accumulative iteration time: 333.0
Weighted Recall: 0.8952380952380953
Weighted FalsePositiveRate: 0.01746031746031746
Kappa statistic: 0.8777777777777777
Training time: 1.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 6.147186147186147
Correctly Classified Instances: 93.85281385281385
Weighted Precision: 0.9409633764324291
Weighted AreaUnderROC: 0.9632017141108049
Root mean squared error: 0.13202308608763139
Relative absolute error: 7.931588334370321
Root relative squared error: 37.72873943792717
Weighted TruePositiveRate: 0.9385281385281385
Weighted MatthewsCorrelation: 0.9290884523897862
Weighted FMeasure: 0.9384508748251589
Iteration time: 119.0
Weighted AreaUnderPRC: 0.8936816784121533
Mean absolute error: 0.01942429796172332
Coverage of cases: 93.85281385281385
Instances selection time: 118.0
Test time: 192.0
Accumulative iteration time: 452.0
Weighted Recall: 0.9385281385281385
Weighted FalsePositiveRate: 0.010245310245310247
Kappa statistic: 0.9282828282828283
Training time: 1.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 4.675324675324675
Correctly Classified Instances: 95.32467532467533
Weighted Precision: 0.9531528270144083
Weighted AreaUnderROC: 0.9726087716996809
Root mean squared error: 0.11525031195982724
Relative absolute error: 6.021374879054781
Root relative squared error: 32.9355199831184
Weighted TruePositiveRate: 0.9532467532467532
Weighted MatthewsCorrelation: 0.9453537933695249
Weighted FMeasure: 0.9530097766086529
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9181766072309977
Mean absolute error: 0.014746224193603608
Coverage of cases: 95.32467532467533
Instances selection time: 11.0
Test time: 234.0
Accumulative iteration time: 464.0
Weighted Recall: 0.9532467532467532
Weighted FalsePositiveRate: 0.0077922077922077905
Kappa statistic: 0.9454545454545454
Training time: 1.0
		
Time end:Sun Oct 08 04.52.46 EEST 2017