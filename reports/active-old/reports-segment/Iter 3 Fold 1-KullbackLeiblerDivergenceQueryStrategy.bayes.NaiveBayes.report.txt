Sun Oct 08 04.52.17 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.17 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 16.771799628942222
Incorrectly Classified Instances: 19.047619047619047
Correctly Classified Instances: 80.95238095238095
Weighted Precision: 0.8420544780883457
Weighted AreaUnderROC: 0.9746486510122874
Root mean squared error: 0.2114972096175917
Relative absolute error: 21.36403808215464
Root relative squared error: 60.44036198498124
Weighted TruePositiveRate: 0.8095238095238095
Weighted MatthewsCorrelation: 0.7823012089064749
Weighted FMeasure: 0.7945337930002137
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8945685765165098
Mean absolute error: 0.05232009326241975
Coverage of cases: 90.73593073593074
Instances selection time: 143.0
Test time: 150.0
Accumulative iteration time: 145.0
Weighted Recall: 0.8095238095238095
Weighted FalsePositiveRate: 0.031746031746031744
Kappa statistic: 0.7777777777777778
Training time: 2.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 15.918367346938528
Incorrectly Classified Instances: 19.307359307359306
Correctly Classified Instances: 80.6926406926407
Weighted Precision: 0.8436263429432794
Weighted AreaUnderROC: 0.971260658533386
Root mean squared error: 0.22614669045800836
Relative absolute error: 23.133875708885277
Root relative squared error: 64.6267998414795
Weighted TruePositiveRate: 0.8069264069264069
Weighted MatthewsCorrelation: 0.7778577888333744
Weighted FMeasure: 0.7865571608567986
Iteration time: 85.0
Weighted AreaUnderPRC: 0.8709790617238864
Mean absolute error: 0.05665438949114786
Coverage of cases: 84.58874458874459
Instances selection time: 82.0
Test time: 87.0
Accumulative iteration time: 230.0
Weighted Recall: 0.8069264069264069
Weighted FalsePositiveRate: 0.032178932178932175
Kappa statistic: 0.7747474747474746
Training time: 3.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 15.720470006184053
Incorrectly Classified Instances: 19.653679653679653
Correctly Classified Instances: 80.34632034632034
Weighted Precision: 0.8380355631971794
Weighted AreaUnderROC: 0.9704949932222658
Root mean squared error: 0.22568119498147474
Relative absolute error: 22.738911191046633
Root relative squared error: 64.49377342872003
Weighted TruePositiveRate: 0.8034632034632034
Weighted MatthewsCorrelation: 0.7732590544476243
Weighted FMeasure: 0.7831611499147627
Iteration time: 46.0
Weighted AreaUnderPRC: 0.865849748573688
Mean absolute error: 0.055687129447461375
Coverage of cases: 84.41558441558442
Instances selection time: 42.0
Test time: 90.0
Accumulative iteration time: 276.0
Weighted Recall: 0.8034632034632034
Weighted FalsePositiveRate: 0.03275613275613276
Kappa statistic: 0.7707070707070707
Training time: 4.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 16.326530612244657
Incorrectly Classified Instances: 19.913419913419915
Correctly Classified Instances: 80.08658008658008
Weighted Precision: 0.8380131745210465
Weighted AreaUnderROC: 0.9740342822161004
Root mean squared error: 0.2232925502975085
Relative absolute error: 22.882288973764737
Root relative squared error: 63.81116135259166
Weighted TruePositiveRate: 0.8008658008658008
Weighted MatthewsCorrelation: 0.7704185706610731
Weighted FMeasure: 0.7802749368065391
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8803684823733038
Mean absolute error: 0.05603825871126082
Coverage of cases: 87.0995670995671
Instances selection time: 4.0
Test time: 84.0
Accumulative iteration time: 288.0
Weighted Recall: 0.8008658008658008
Weighted FalsePositiveRate: 0.03318903318903319
Kappa statistic: 0.7676767676767675
Training time: 8.0
		
Time end:Sun Oct 08 04.52.18 EEST 2017