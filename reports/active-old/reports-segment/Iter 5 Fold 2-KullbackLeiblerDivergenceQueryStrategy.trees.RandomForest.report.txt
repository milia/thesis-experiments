Sun Oct 08 04.52.55 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.55 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 25.91218305504011
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9195874714358684
Weighted AreaUnderROC: 0.9834942498578862
Root mean squared error: 0.1451504241815163
Relative absolute error: 20.28282828282837
Root relative squared error: 41.48018877254593
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.9020243439210004
Weighted FMeasure: 0.9149368883456775
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9403580172927436
Mean absolute error: 0.049672232529375804
Coverage of cases: 98.44155844155844
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 33.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.014285714285714285
Kappa statistic: 0.8999999999999998
Training time: 28.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 22.98082869511423
Incorrectly Classified Instances: 8.311688311688311
Correctly Classified Instances: 91.68831168831169
Weighted Precision: 0.9245666538537005
Weighted AreaUnderROC: 0.9920989111898202
Root mean squared error: 0.1297826545227286
Relative absolute error: 16.08080808080814
Root relative squared error: 37.08848278853619
Weighted TruePositiveRate: 0.9168831168831169
Weighted MatthewsCorrelation: 0.9059090730124119
Weighted FMeasure: 0.9175380044926471
Iteration time: 53.0
Weighted AreaUnderPRC: 0.966314414629953
Mean absolute error: 0.039381570810142555
Coverage of cases: 99.39393939393939
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 86.0
Weighted Recall: 0.9168831168831169
Weighted FalsePositiveRate: 0.013852813852813851
Kappa statistic: 0.903030303030303
Training time: 48.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 22.36239950525645
Incorrectly Classified Instances: 5.2813852813852815
Correctly Classified Instances: 94.71861471861472
Weighted Precision: 0.9530620870121386
Weighted AreaUnderROC: 0.9932052122961214
Root mean squared error: 0.1135365052896872
Relative absolute error: 13.353535353535317
Root relative squared error: 32.44575893282923
Weighted TruePositiveRate: 0.9471861471861471
Weighted MatthewsCorrelation: 0.9405423548610763
Weighted FMeasure: 0.9476217493980599
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9797950048772817
Mean absolute error: 0.03270253555967847
Coverage of cases: 99.13419913419914
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 180.0
Weighted Recall: 0.9471861471861471
Weighted FalsePositiveRate: 0.008802308802308801
Kappa statistic: 0.9383838383838383
Training time: 91.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 19.64131106988232
Incorrectly Classified Instances: 2.7705627705627704
Correctly Classified Instances: 97.22943722943722
Weighted Precision: 0.9726628313277145
Weighted AreaUnderROC: 0.9975451484542394
Root mean squared error: 0.08355135364022778
Relative absolute error: 8.161616161616106
Root relative squared error: 23.876788102695205
Weighted TruePositiveRate: 0.9722943722943723
Weighted MatthewsCorrelation: 0.9677458805011465
Weighted FMeasure: 0.9721045298826181
Iteration time: 133.0
Weighted AreaUnderPRC: 0.9901654302299399
Mean absolute error: 0.019987631416202797
Coverage of cases: 99.74025974025975
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 313.0
Weighted Recall: 0.9722943722943723
Weighted FalsePositiveRate: 0.004617604617604618
Kappa statistic: 0.9676767676767676
Training time: 133.0
		
Time end:Sun Oct 08 04.52.56 EEST 2017