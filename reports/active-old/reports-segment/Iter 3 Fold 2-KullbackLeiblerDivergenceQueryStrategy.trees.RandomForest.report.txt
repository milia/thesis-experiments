Sun Oct 08 04.52.53 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.53 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 24.192949907235334
Incorrectly Classified Instances: 6.753246753246753
Correctly Classified Instances: 93.24675324675324
Weighted Precision: 0.9367800135590193
Weighted AreaUnderROC: 0.9888272333726879
Root mean squared error: 0.13271346324751293
Relative absolute error: 17.1616161616162
Root relative squared error: 37.92603114463503
Weighted TruePositiveRate: 0.9324675324675324
Weighted MatthewsCorrelation: 0.9228735242894922
Weighted FMeasure: 0.9330605218196166
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9509309809394658
Mean absolute error: 0.04202844774273373
Coverage of cases: 99.22077922077922
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9324675324675324
Weighted FalsePositiveRate: 0.011255411255411256
Kappa statistic: 0.9212121212121211
Training time: 27.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 22.226345083487626
Incorrectly Classified Instances: 7.532467532467533
Correctly Classified Instances: 92.46753246753246
Weighted Precision: 0.9372200234194809
Weighted AreaUnderROC: 0.9861248852157943
Root mean squared error: 0.1352704064809129
Relative absolute error: 15.242424242424228
Root relative squared error: 38.656738537329105
Weighted TruePositiveRate: 0.9246753246753247
Weighted MatthewsCorrelation: 0.916269222753705
Weighted FMeasure: 0.9242415435610274
Iteration time: 61.0
Weighted AreaUnderPRC: 0.9503447457746077
Mean absolute error: 0.037328385899814596
Coverage of cases: 98.26839826839827
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 93.0
Weighted Recall: 0.9246753246753247
Weighted FalsePositiveRate: 0.012554112554112554
Kappa statistic: 0.912121212121212
Training time: 57.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 21.32343846629527
Incorrectly Classified Instances: 6.147186147186147
Correctly Classified Instances: 93.85281385281385
Weighted Precision: 0.9442182062784407
Weighted AreaUnderROC: 0.99169487078578
Root mean squared error: 0.12335246972501344
Relative absolute error: 13.141414141414096
Root relative squared error: 35.25090442281244
Weighted TruePositiveRate: 0.9385281385281385
Weighted MatthewsCorrelation: 0.9300340853263872
Weighted FMeasure: 0.9378944346440442
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9685441943860394
Mean absolute error: 0.03218305504019792
Coverage of cases: 99.04761904761905
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9385281385281385
Weighted FalsePositiveRate: 0.010245310245310247
Kappa statistic: 0.9282828282828283
Training time: 84.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 19.369202226344783
Incorrectly Classified Instances: 3.3766233766233764
Correctly Classified Instances: 96.62337662337663
Weighted Precision: 0.9662446779599603
Weighted AreaUnderROC: 0.9966517993790719
Root mean squared error: 0.09195196508903614
Relative absolute error: 8.686868686868628
Root relative squared error: 26.277462786674217
Weighted TruePositiveRate: 0.9662337662337662
Weighted MatthewsCorrelation: 0.9605642009533026
Weighted FMeasure: 0.9660756878232021
Iteration time: 119.0
Weighted AreaUnderPRC: 0.986365251685907
Mean absolute error: 0.021273964131106937
Coverage of cases: 99.65367965367966
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 298.0
Weighted Recall: 0.9662337662337662
Weighted FalsePositiveRate: 0.005627705627705628
Kappa statistic: 0.9606060606060605
Training time: 119.0
		
Time end:Sun Oct 08 04.52.54 EEST 2017