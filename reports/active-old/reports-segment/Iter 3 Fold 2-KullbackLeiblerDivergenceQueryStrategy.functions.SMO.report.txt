Sun Oct 08 04.52.28 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.28 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 72.12121212121073
Incorrectly Classified Instances: 21.471861471861473
Correctly Classified Instances: 78.52813852813853
Weighted Precision: 0.8711943195570888
Weighted AreaUnderROC: 0.9569570160479249
Root mean squared error: 0.3070258549470763
Relative absolute error: 84.82443482443588
Root relative squared error: 87.73994628724473
Weighted TruePositiveRate: 0.7852813852813852
Weighted MatthewsCorrelation: 0.7680802116359913
Weighted FMeasure: 0.7672873491725039
Iteration time: 179.0
Weighted AreaUnderPRC: 0.7959660270499082
Mean absolute error: 0.20773330977412957
Coverage of cases: 99.82683982683983
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 179.0
Weighted Recall: 0.7852813852813852
Weighted FalsePositiveRate: 0.03578643578643579
Kappa statistic: 0.7494949494949493
Training time: 171.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 71.6388373531216
Incorrectly Classified Instances: 18.614718614718615
Correctly Classified Instances: 81.38528138528139
Weighted Precision: 0.8833981898138452
Weighted AreaUnderROC: 0.9620726747999474
Root mean squared error: 0.3066922478258316
Relative absolute error: 84.73304473304577
Root relative squared error: 87.64461011139043
Weighted TruePositiveRate: 0.8138528138528138
Weighted MatthewsCorrelation: 0.7847569409113405
Weighted FMeasure: 0.7784870528736658
Iteration time: 186.0
Weighted AreaUnderPRC: 0.8307887588153032
Mean absolute error: 0.20750949730541912
Coverage of cases: 99.82683982683983
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 365.0
Weighted Recall: 0.8138528138528138
Weighted FalsePositiveRate: 0.031024531024531028
Kappa statistic: 0.7828282828282828
Training time: 179.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 72.71490414347386
Incorrectly Classified Instances: 8.744588744588745
Correctly Classified Instances: 91.25541125541126
Weighted Precision: 0.913067384545696
Weighted AreaUnderROC: 0.9713450522541431
Root mean squared error: 0.30381825960783043
Relative absolute error: 84.06445406445532
Root relative squared error: 86.82329956762185
Weighted TruePositiveRate: 0.9125541125541126
Weighted MatthewsCorrelation: 0.8980993602107128
Weighted FMeasure: 0.9123619972715062
Iteration time: 201.0
Weighted AreaUnderPRC: 0.8708535722403546
Mean absolute error: 0.2058721324027486
Coverage of cases: 99.74025974025975
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 566.0
Weighted Recall: 0.9125541125541126
Weighted FalsePositiveRate: 0.014574314574314576
Kappa statistic: 0.897979797979798
Training time: 199.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 72.69016697587946
Incorrectly Classified Instances: 7.532467532467533
Correctly Classified Instances: 92.46753246753246
Weighted Precision: 0.9247108331708961
Weighted AreaUnderROC: 0.9750802396256941
Root mean squared error: 0.30352455916510146
Relative absolute error: 83.99230399230528
Root relative squared error: 86.73936767506504
Weighted TruePositiveRate: 0.9246753246753247
Weighted MatthewsCorrelation: 0.9120963828863806
Weighted FMeasure: 0.9245426766440634
Iteration time: 213.0
Weighted AreaUnderPRC: 0.8892480035267863
Mean absolute error: 0.2056954383485036
Coverage of cases: 99.74025974025975
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 779.0
Weighted Recall: 0.9246753246753247
Weighted FalsePositiveRate: 0.012554112554112554
Kappa statistic: 0.912121212121212
Training time: 212.0
		
Time end:Sun Oct 08 04.52.29 EEST 2017