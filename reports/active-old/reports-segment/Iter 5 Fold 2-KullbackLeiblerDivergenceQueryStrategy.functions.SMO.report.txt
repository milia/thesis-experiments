Sun Oct 08 04.52.32 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.32 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 72.54174397031377
Incorrectly Classified Instances: 12.034632034632034
Correctly Classified Instances: 87.96536796536796
Weighted Precision: 0.8926335073760239
Weighted AreaUnderROC: 0.9682583409856137
Root mean squared error: 0.30460375936263645
Relative absolute error: 84.22318422318548
Root relative squared error: 87.04777482006338
Weighted TruePositiveRate: 0.8796536796536797
Weighted MatthewsCorrelation: 0.8650373445861522
Weighted FMeasure: 0.8830523206492042
Iteration time: 179.0
Weighted AreaUnderPRC: 0.8484056153500849
Mean absolute error: 0.2062608593220878
Coverage of cases: 99.82683982683983
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 179.0
Weighted Recall: 0.8796536796536797
Weighted FalsePositiveRate: 0.02005772005772006
Kappa statistic: 0.8595959595959595
Training time: 171.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 72.28200371057372
Incorrectly Classified Instances: 18.095238095238095
Correctly Classified Instances: 81.9047619047619
Weighted Precision: 0.8280324789423519
Weighted AreaUnderROC: 0.9594678385587476
Root mean squared error: 0.30594969815881135
Relative absolute error: 84.58393458393572
Root relative squared error: 87.43240886888856
Weighted TruePositiveRate: 0.819047619047619
Weighted MatthewsCorrelation: 0.7882376574096936
Weighted FMeasure: 0.8057038290878726
Iteration time: 182.0
Weighted AreaUnderPRC: 0.7905165302483403
Mean absolute error: 0.20714432959331283
Coverage of cases: 99.82683982683983
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 361.0
Weighted Recall: 0.819047619047619
Weighted FalsePositiveRate: 0.03015873015873016
Kappa statistic: 0.7888888888888889
Training time: 176.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 72.57884972170531
Incorrectly Classified Instances: 8.13852813852814
Correctly Classified Instances: 91.86147186147186
Weighted Precision: 0.9181938057344995
Weighted AreaUnderROC: 0.9747737111373475
Root mean squared error: 0.303274042800184
Relative absolute error: 83.92015392015523
Root relative squared error: 86.6677766606675
Weighted TruePositiveRate: 0.9186147186147187
Weighted MatthewsCorrelation: 0.9042704410135971
Weighted FMeasure: 0.9164248665110775
Iteration time: 201.0
Weighted AreaUnderPRC: 0.8760925036620012
Mean absolute error: 0.2055187442942586
Coverage of cases: 99.82683982683983
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 562.0
Weighted Recall: 0.9186147186147187
Weighted FalsePositiveRate: 0.013564213564213563
Kappa statistic: 0.9050505050505051
Training time: 199.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 72.66542980828541
Incorrectly Classified Instances: 7.792207792207792
Correctly Classified Instances: 92.20779220779221
Weighted Precision: 0.9218536168807848
Weighted AreaUnderROC: 0.9755201364292273
Root mean squared error: 0.30326664433848216
Relative absolute error: 83.9153439153452
Root relative squared error: 86.6656623741282
Weighted TruePositiveRate: 0.922077922077922
Weighted MatthewsCorrelation: 0.9083183193243116
Weighted FMeasure: 0.9196813191355628
Iteration time: 212.0
Weighted AreaUnderPRC: 0.8812219075071221
Mean absolute error: 0.20550696469064222
Coverage of cases: 99.82683982683983
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 774.0
Weighted Recall: 0.922077922077922
Weighted FalsePositiveRate: 0.012987012987012988
Kappa statistic: 0.9090909090909091
Training time: 212.0
		
Time end:Sun Oct 08 04.52.33 EEST 2017