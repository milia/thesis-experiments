Sun Oct 08 05.15.00 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 05.15.00 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 15.769944341372733
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8375931221651067
Weighted AreaUnderROC: 0.9666373693646421
Root mean squared error: 0.22093641021726684
Relative absolute error: 21.801090399755328
Root relative squared error: 63.137838240695224
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.7888113336672087
Weighted FMeasure: 0.8021280043074235
Iteration time: 82.0
Weighted AreaUnderPRC: 0.8580250473379626
Mean absolute error: 0.053390425468788784
Coverage of cases: 84.32900432900433
Instances selection time: 80.0
Test time: 92.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.030303030303030297
Kappa statistic: 0.7878787878787878
Training time: 2.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 15.49783549783529
Incorrectly Classified Instances: 18.095238095238095
Correctly Classified Instances: 81.9047619047619
Weighted Precision: 0.8491836516975002
Weighted AreaUnderROC: 0.9697739297739298
Root mean squared error: 0.22200383465580825
Relative absolute error: 21.784996824854264
Root relative squared error: 63.44288018225894
Weighted TruePositiveRate: 0.819047619047619
Weighted MatthewsCorrelation: 0.7906995053225326
Weighted FMeasure: 0.7973262563708989
Iteration time: 44.0
Weighted AreaUnderPRC: 0.8659577166923115
Mean absolute error: 0.05335101263229639
Coverage of cases: 83.8961038961039
Instances selection time: 42.0
Test time: 85.0
Accumulative iteration time: 126.0
Weighted Recall: 0.819047619047619
Weighted FalsePositiveRate: 0.030158730158730156
Kappa statistic: 0.7888888888888889
Training time: 2.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 15.708101422386918
Incorrectly Classified Instances: 19.393939393939394
Correctly Classified Instances: 80.60606060606061
Weighted Precision: 0.8330651565071259
Weighted AreaUnderROC: 0.9692596965324237
Root mean squared error: 0.22728944639748036
Relative absolute error: 22.91758707530096
Root relative squared error: 64.95336955257429
Weighted TruePositiveRate: 0.806060606060606
Weighted MatthewsCorrelation: 0.7733825729138774
Weighted FMeasure: 0.7829169102472145
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8653108724381714
Mean absolute error: 0.05612470304155361
Coverage of cases: 83.98268398268398
Instances selection time: 22.0
Test time: 85.0
Accumulative iteration time: 152.0
Weighted Recall: 0.806060606060606
Weighted FalsePositiveRate: 0.03232323232323232
Kappa statistic: 0.7737373737373736
Training time: 4.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 15.78231292516985
Incorrectly Classified Instances: 19.134199134199132
Correctly Classified Instances: 80.86580086580086
Weighted Precision: 0.8416519356094567
Weighted AreaUnderROC: 0.9707901526083345
Root mean squared error: 0.2238979703464461
Relative absolute error: 22.629769610565717
Root relative squared error: 63.984174542585585
Weighted TruePositiveRate: 0.8086580086580086
Weighted MatthewsCorrelation: 0.7785873511491394
Weighted FMeasure: 0.7874689279328598
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8771055107856801
Mean absolute error: 0.05541984394424281
Coverage of cases: 84.15584415584415
Instances selection time: 2.0
Test time: 85.0
Accumulative iteration time: 159.0
Weighted Recall: 0.8086580086580086
Weighted FalsePositiveRate: 0.03189033189033189
Kappa statistic: 0.7767676767676768
Training time: 5.0
		
Time end:Sun Oct 08 05.15.01 EEST 2017