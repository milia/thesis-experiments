Sun Oct 08 05.15.33 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 05.15.33 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 24.41558441558419
Incorrectly Classified Instances: 8.225108225108226
Correctly Classified Instances: 91.77489177489177
Weighted Precision: 0.9169175792376593
Weighted AreaUnderROC: 0.9867720494993223
Root mean squared error: 0.1351423354647958
Relative absolute error: 17.727272727272748
Root relative squared error: 38.620139195957655
Weighted TruePositiveRate: 0.9177489177489178
Weighted MatthewsCorrelation: 0.9034899842687183
Weighted FMeasure: 0.9168110663975663
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9484656394572283
Mean absolute error: 0.043413729128015076
Coverage of cases: 98.87445887445888
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9177489177489178
Weighted FalsePositiveRate: 0.01370851370851371
Kappa statistic: 0.9040404040404041
Training time: 29.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 22.980828695114194
Incorrectly Classified Instances: 6.32034632034632
Correctly Classified Instances: 93.67965367965368
Weighted Precision: 0.9394416675734107
Weighted AreaUnderROC: 0.9935519699156061
Root mean squared error: 0.12278967240257724
Relative absolute error: 15.030303030303003
Root relative squared error: 35.09007169147888
Weighted TruePositiveRate: 0.9367965367965368
Weighted MatthewsCorrelation: 0.9271359876015183
Weighted FMeasure: 0.9366170925924416
Iteration time: 54.0
Weighted AreaUnderPRC: 0.9711627463441802
Mean absolute error: 0.036808905380334045
Coverage of cases: 99.56709956709956
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 86.0
Weighted Recall: 0.9367965367965368
Weighted FalsePositiveRate: 0.010533910533910533
Kappa statistic: 0.9262626262626262
Training time: 52.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 21.24922696351246
Incorrectly Classified Instances: 6.0606060606060606
Correctly Classified Instances: 93.93939393939394
Weighted Precision: 0.9462210508514165
Weighted AreaUnderROC: 0.9911941930123749
Root mean squared error: 0.11647258350667707
Relative absolute error: 12.424242424242358
Root relative squared error: 33.284813171757236
Weighted TruePositiveRate: 0.9393939393939394
Weighted MatthewsCorrelation: 0.9305542398026277
Weighted FMeasure: 0.9362404560591487
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9720522703043987
Mean absolute error: 0.03042671614100182
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 174.0
Weighted Recall: 0.9393939393939394
Weighted FalsePositiveRate: 0.0101010101010101
Kappa statistic: 0.9292929292929294
Training time: 87.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 20.111317254174196
Incorrectly Classified Instances: 2.7705627705627704
Correctly Classified Instances: 97.22943722943722
Weighted Precision: 0.972796216978661
Weighted AreaUnderROC: 0.9976037430582884
Root mean squared error: 0.08676128225740251
Relative absolute error: 8.979797979797919
Root relative squared error: 24.794101612026036
Weighted TruePositiveRate: 0.9722943722943723
Weighted MatthewsCorrelation: 0.9678557717094891
Weighted FMeasure: 0.9723194332735994
Iteration time: 130.0
Weighted AreaUnderPRC: 0.9911742746195163
Mean absolute error: 0.021991341991341933
Coverage of cases: 99.74025974025975
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 304.0
Weighted Recall: 0.9722943722943723
Weighted FalsePositiveRate: 0.004617604617604618
Kappa statistic: 0.9676767676767676
Training time: 130.0
		
Time end:Sun Oct 08 05.15.34 EEST 2017