Sun Oct 08 04.52.36 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.36 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 10.735930735930737
Correctly Classified Instances: 89.26406926406926
Weighted Precision: 0.896317741458109
Weighted AreaUnderROC: 0.9379986007258734
Root mean squared error: 0.17284969300807246
Relative absolute error: 15.08298841924847
Root relative squared error: 49.39591417442427
Weighted TruePositiveRate: 0.8926406926406927
Weighted MatthewsCorrelation: 0.8759247141543941
Weighted FMeasure: 0.8923092311995656
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8249560144076121
Mean absolute error: 0.03693793082264947
Coverage of cases: 89.26406926406926
Instances selection time: 144.0
Test time: 95.0
Accumulative iteration time: 145.0
Weighted Recall: 0.8926406926406927
Weighted FalsePositiveRate: 0.017893217893217895
Kappa statistic: 0.8747474747474748
Training time: 1.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 9.177489177489177
Correctly Classified Instances: 90.82251082251082
Weighted Precision: 0.9139297235207525
Weighted AreaUnderROC: 0.9464900083081902
Root mean squared error: 0.16094688356567052
Relative absolute error: 11.85954853531464
Root relative squared error: 45.99440304981988
Weighted TruePositiveRate: 0.9082251082251083
Weighted MatthewsCorrelation: 0.8944718501244389
Weighted FMeasure: 0.906586203031101
Iteration time: 163.0
Weighted AreaUnderPRC: 0.8485601082733153
Mean absolute error: 0.029043792331382916
Coverage of cases: 90.82251082251082
Instances selection time: 162.0
Test time: 139.0
Accumulative iteration time: 308.0
Weighted Recall: 0.9082251082251083
Weighted FalsePositiveRate: 0.015295815295815297
Kappa statistic: 0.892929292929293
Training time: 1.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 9.264069264069263
Correctly Classified Instances: 90.73593073593074
Weighted Precision: 0.9121508325498203
Weighted AreaUnderROC: 0.9468485723031176
Root mean squared error: 0.1620434274275294
Relative absolute error: 11.540997517773102
Root relative squared error: 46.30776655973565
Weighted TruePositiveRate: 0.9073593073593074
Weighted MatthewsCorrelation: 0.8928503194312946
Weighted FMeasure: 0.9047876669685109
Iteration time: 115.0
Weighted AreaUnderPRC: 0.8463186954523422
Mean absolute error: 0.02826366739046486
Coverage of cases: 90.73593073593074
Instances selection time: 114.0
Test time: 181.0
Accumulative iteration time: 423.0
Weighted Recall: 0.9073593073593074
Weighted FalsePositiveRate: 0.01544011544011544
Kappa statistic: 0.8919191919191919
Training time: 1.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 14.285714285714091
Incorrectly Classified Instances: 5.021645021645021
Correctly Classified Instances: 94.97835497835497
Weighted Precision: 0.950333526762911
Weighted AreaUnderROC: 0.9704932441296078
Root mean squared error: 0.11944061979708885
Relative absolute error: 6.425319128526106
Root relative squared error: 34.13300019087396
Weighted TruePositiveRate: 0.9497835497835497
Weighted MatthewsCorrelation: 0.9416190628722041
Weighted FMeasure: 0.9498435242888192
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9119419244393505
Mean absolute error: 0.015735475416798695
Coverage of cases: 94.97835497835497
Instances selection time: 14.0
Test time: 228.0
Accumulative iteration time: 438.0
Weighted Recall: 0.9497835497835497
Weighted FalsePositiveRate: 0.008369408369408369
Kappa statistic: 0.9414141414141413
Training time: 1.0
		
Time end:Sun Oct 08 04.52.37 EEST 2017