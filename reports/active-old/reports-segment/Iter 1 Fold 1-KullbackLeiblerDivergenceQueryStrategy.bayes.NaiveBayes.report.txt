Sun Oct 08 04.52.12 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 04.52.12 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 15.769944341372733
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8375931221651067
Weighted AreaUnderROC: 0.9666373693646421
Root mean squared error: 0.22093641021726684
Relative absolute error: 21.801090399755328
Root relative squared error: 63.137838240695224
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.7888113336672087
Weighted FMeasure: 0.8021280043074235
Iteration time: 630.0
Weighted AreaUnderPRC: 0.8580250473379626
Mean absolute error: 0.053390425468788784
Coverage of cases: 84.32900432900433
Instances selection time: 597.0
Test time: 188.0
Accumulative iteration time: 630.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.030303030303030297
Kappa statistic: 0.7878787878787878
Training time: 33.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 15.435992578849525
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8468497153692762
Weighted AreaUnderROC: 0.9649748567930386
Root mean squared error: 0.22208114948807015
Relative absolute error: 21.751849493846773
Root relative squared error: 63.46497473592784
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.7888858455220265
Weighted FMeasure: 0.7956745344311871
Iteration time: 182.0
Weighted AreaUnderPRC: 0.8522577096666755
Mean absolute error: 0.053269835495135184
Coverage of cases: 83.8961038961039
Instances selection time: 166.0
Test time: 153.0
Accumulative iteration time: 812.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.030303030303030297
Kappa statistic: 0.7878787878787878
Training time: 16.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 15.411255411255198
Incorrectly Classified Instances: 18.614718614718615
Correctly Classified Instances: 81.38528138528139
Weighted Precision: 0.8361818233737213
Weighted AreaUnderROC: 0.9636617254799074
Root mean squared error: 0.22408282737668292
Relative absolute error: 22.12028477481657
Root relative squared error: 64.03700183918767
Weighted TruePositiveRate: 0.8138528138528138
Weighted MatthewsCorrelation: 0.7822338660470971
Weighted FMeasure: 0.7914589754609797
Iteration time: 99.0
Weighted AreaUnderPRC: 0.8450611440245916
Mean absolute error: 0.05417212597914285
Coverage of cases: 83.8961038961039
Instances selection time: 86.0
Test time: 170.0
Accumulative iteration time: 911.0
Weighted Recall: 0.8138528138528138
Weighted FalsePositiveRate: 0.03102453102453102
Kappa statistic: 0.7828282828282828
Training time: 13.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 15.757575757575541
Incorrectly Classified Instances: 19.134199134199132
Correctly Classified Instances: 80.86580086580086
Weighted Precision: 0.8416519356094567
Weighted AreaUnderROC: 0.9702365647820193
Root mean squared error: 0.22466577506422122
Relative absolute error: 22.717270583503886
Root relative squared error: 64.20359301699483
Weighted TruePositiveRate: 0.8086580086580086
Weighted MatthewsCorrelation: 0.7785873511491394
Weighted FMeasure: 0.7874689279328598
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8744941972266603
Mean absolute error: 0.05563413204123424
Coverage of cases: 83.8961038961039
Instances selection time: 4.0
Test time: 166.0
Accumulative iteration time: 929.0
Weighted Recall: 0.8086580086580086
Weighted FalsePositiveRate: 0.03189033189033189
Kappa statistic: 0.7767676767676768
Training time: 14.0
		
Time end:Sun Oct 08 04.52.14 EEST 2017