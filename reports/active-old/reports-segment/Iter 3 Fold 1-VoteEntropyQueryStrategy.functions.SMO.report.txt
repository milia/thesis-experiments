Sun Oct 08 05.15.11 EEST 2017
Dataset: segment
Test set size: 1155
Initial Labelled set size: 231
Initial Unlabelled set size: 924
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 05.15.11 EEST 2017
		
Iteration: 1
Labeled set size: 231
Unlabelled set size: 924
	
Mean region size: 72.03463203463036
Incorrectly Classified Instances: 10.995670995670995
Correctly Classified Instances: 89.00432900432901
Weighted Precision: 0.8917342580317728
Weighted AreaUnderROC: 0.9606204906204908
Root mean squared error: 0.30476576991982834
Relative absolute error: 84.25685425685553
Root relative squared error: 87.09407319317087
Weighted TruePositiveRate: 0.8900432900432901
Weighted MatthewsCorrelation: 0.8706860801123572
Weighted FMeasure: 0.8847367883907316
Iteration time: 153.0
Weighted AreaUnderPRC: 0.8365003520960113
Mean absolute error: 0.20634331654740218
Coverage of cases: 99.74025974025975
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 153.0
Weighted Recall: 0.8900432900432901
Weighted FalsePositiveRate: 0.018326118326118326
Kappa statistic: 0.8717171717171717
Training time: 150.0
		
Iteration: 2
Labeled set size: 531
Unlabelled set size: 624
	
Mean region size: 73.14780457637399
Incorrectly Classified Instances: 16.450216450216452
Correctly Classified Instances: 83.54978354978356
Weighted Precision: 0.9141817478026208
Weighted AreaUnderROC: 0.9666185666185665
Root mean squared error: 0.30571401324202435
Relative absolute error: 84.5069745069756
Root relative squared error: 87.36505629383207
Weighted TruePositiveRate: 0.8354978354978355
Weighted MatthewsCorrelation: 0.822182000975273
Weighted FMeasure: 0.8129843654492948
Iteration time: 156.0
Weighted AreaUnderPRC: 0.8486479365640143
Mean absolute error: 0.20695585593545132
Coverage of cases: 99.74025974025975
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 309.0
Weighted Recall: 0.8354978354978355
Weighted FalsePositiveRate: 0.02741702741702742
Kappa statistic: 0.8080808080808081
Training time: 153.0
		
Iteration: 3
Labeled set size: 831
Unlabelled set size: 324
	
Mean region size: 73.48175633889717
Incorrectly Classified Instances: 18.095238095238095
Correctly Classified Instances: 81.9047619047619
Weighted Precision: 0.902695868440479
Weighted AreaUnderROC: 0.9626218898946173
Root mean squared error: 0.306145810968653
Relative absolute error: 84.62722462722569
Root relative squared error: 87.4884527724377
Weighted TruePositiveRate: 0.819047619047619
Weighted MatthewsCorrelation: 0.7998205513869803
Weighted FMeasure: 0.789737081966093
Iteration time: 166.0
Weighted AreaUnderPRC: 0.8306626833931439
Mean absolute error: 0.2072503460258597
Coverage of cases: 99.74025974025975
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 475.0
Weighted Recall: 0.819047619047619
Weighted FalsePositiveRate: 0.030158730158730156
Kappa statistic: 0.7888888888888889
Training time: 164.0
		
Iteration: 4
Labeled set size: 1131
Unlabelled set size: 24
	
Mean region size: 75.01546072974418
Incorrectly Classified Instances: 7.792207792207792
Correctly Classified Instances: 92.20779220779221
Weighted Precision: 0.9229969841029939
Weighted AreaUnderROC: 0.9739275875639511
Root mean squared error: 0.30292426804611416
Relative absolute error: 83.89610389610519
Root relative squared error: 86.56782019888999
Weighted TruePositiveRate: 0.922077922077922
Weighted MatthewsCorrelation: 0.9092628438107524
Weighted FMeasure: 0.9215430134892552
Iteration time: 190.0
Weighted AreaUnderPRC: 0.8817752856372549
Mean absolute error: 0.20545984627617683
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 665.0
Weighted Recall: 0.922077922077922
Weighted FalsePositiveRate: 0.012987012987012988
Kappa statistic: 0.9090909090909091
Training time: 190.0
		
Time end:Sun Oct 08 05.15.12 EEST 2017