Sun Oct 08 10.07.46 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.07.46 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 74.34343434343434
Correctly Classified Instances: 25.656565656565657
Weighted Precision: 0.24349761015533988
Weighted AreaUnderROC: 0.6979057239057238
Root mean squared error: 0.28330190627524704
Relative absolute error: 96.67474747474688
Root relative squared error: 98.54672182270595
Weighted TruePositiveRate: 0.25656565656565655
Weighted MatthewsCorrelation: 0.177892850183683
Weighted FMeasure: 0.24111232464924678
Iteration time: 447.0
Weighted AreaUnderPRC: 0.2181890072915433
Mean absolute error: 0.1597929710326405
Coverage of cases: 94.14141414141415
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 447.0
Weighted Recall: 0.25656565656565655
Weighted FalsePositiveRate: 0.07434343434343434
Kappa statistic: 0.1822222222222222
Training time: 446.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.46097337006391
Incorrectly Classified Instances: 70.9090909090909
Correctly Classified Instances: 29.09090909090909
Weighted Precision: 0.25939023505469716
Weighted AreaUnderROC: 0.713544332210999
Root mean squared error: 0.28185811446750897
Relative absolute error: 96.21010101010033
Root relative squared error: 98.04449805895608
Weighted TruePositiveRate: 0.2909090909090909
Weighted MatthewsCorrelation: 0.2058378680008795
Weighted FMeasure: 0.2642836556263944
Iteration time: 402.0
Weighted AreaUnderPRC: 0.2319246857612139
Mean absolute error: 0.15902496034727429
Coverage of cases: 94.14141414141415
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 849.0
Weighted Recall: 0.2909090909090909
Weighted FalsePositiveRate: 0.07090909090909091
Kappa statistic: 0.22
Training time: 401.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.24058769513273
Incorrectly Classified Instances: 69.29292929292929
Correctly Classified Instances: 30.707070707070706
Weighted Precision: 0.3602253732871742
Weighted AreaUnderROC: 0.7393243546576881
Root mean squared error: 0.28005916656527957
Relative absolute error: 95.59999999999944
Root relative squared error: 97.41873305502314
Weighted TruePositiveRate: 0.30707070707070705
Weighted MatthewsCorrelation: 0.22638692962753798
Weighted FMeasure: 0.2640688150068382
Iteration time: 397.0
Weighted AreaUnderPRC: 0.24528061401718815
Mean absolute error: 0.15801652892561993
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1246.0
Weighted Recall: 0.30707070707070705
Weighted FalsePositiveRate: 0.06929292929292928
Kappa statistic: 0.23777777777777775
Training time: 396.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.33241505968742
Incorrectly Classified Instances: 69.6969696969697
Correctly Classified Instances: 30.303030303030305
Weighted Precision: 0.28000433587624857
Weighted AreaUnderROC: 0.7766127946127948
Root mean squared error: 0.2774947962467189
Relative absolute error: 94.73131313131256
Root relative squared error: 96.52671544823698
Weighted TruePositiveRate: 0.30303030303030304
Weighted MatthewsCorrelation: 0.22359722460094866
Weighted FMeasure: 0.282465411137498
Iteration time: 401.0
Weighted AreaUnderPRC: 0.2702887979037649
Mean absolute error: 0.15658068286167467
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1647.0
Weighted Recall: 0.30303030303030304
Weighted FalsePositiveRate: 0.0696969696969697
Kappa statistic: 0.23333333333333334
Training time: 400.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.33241505968745
Incorrectly Classified Instances: 68.28282828282828
Correctly Classified Instances: 31.717171717171716
Weighted Precision: 0.2917069033369974
Weighted AreaUnderROC: 0.7850864197530865
Root mean squared error: 0.27713662925538085
Relative absolute error: 94.61414141414085
Root relative squared error: 96.402126865952
Weighted TruePositiveRate: 0.31717171717171716
Weighted MatthewsCorrelation: 0.2350149345894319
Weighted FMeasure: 0.2866461946587329
Iteration time: 405.0
Weighted AreaUnderPRC: 0.2867530983469571
Mean absolute error: 0.1563870106018867
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2052.0
Weighted Recall: 0.31717171717171716
Weighted FalsePositiveRate: 0.06828282828282829
Kappa statistic: 0.24888888888888888
Training time: 404.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.49770431588583
Incorrectly Classified Instances: 65.05050505050505
Correctly Classified Instances: 34.94949494949495
Weighted Precision: 0.4000876124224476
Weighted AreaUnderROC: 0.7938855218855219
Root mean squared error: 0.2761149755002938
Relative absolute error: 94.27070707070654
Root relative squared error: 96.04674405287672
Weighted TruePositiveRate: 0.34949494949494947
Weighted MatthewsCorrelation: 0.29274429055286877
Weighted FMeasure: 0.33594107088394526
Iteration time: 410.0
Weighted AreaUnderPRC: 0.3070651219607719
Mean absolute error: 0.15581935053009444
Coverage of cases: 97.37373737373737
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2462.0
Weighted Recall: 0.34949494949494947
Weighted FalsePositiveRate: 0.06505050505050505
Kappa statistic: 0.28444444444444444
Training time: 410.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.40587695133114
Incorrectly Classified Instances: 62.42424242424242
Correctly Classified Instances: 37.57575757575758
Weighted Precision: 0.38855915950496883
Weighted AreaUnderROC: 0.8121459034792369
Root mean squared error: 0.27474877604127057
Relative absolute error: 93.80202020201968
Root relative squared error: 95.57151082973037
Weighted TruePositiveRate: 0.37575757575757573
Weighted MatthewsCorrelation: 0.31415719433928196
Weighted FMeasure: 0.3644958430522506
Iteration time: 417.0
Weighted AreaUnderPRC: 0.3259994249725529
Mean absolute error: 0.15504466149094262
Coverage of cases: 97.97979797979798
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2879.0
Weighted Recall: 0.37575757575757573
Weighted FalsePositiveRate: 0.062424242424242424
Kappa statistic: 0.3133333333333333
Training time: 417.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.36914600550932
Incorrectly Classified Instances: 62.02020202020202
Correctly Classified Instances: 37.97979797979798
Weighted Precision: 0.3895238399409231
Weighted AreaUnderROC: 0.8190303030303031
Root mean squared error: 0.2742320951989419
Relative absolute error: 93.63232323232266
Root relative squared error: 95.39178311836575
Weighted TruePositiveRate: 0.3797979797979798
Weighted MatthewsCorrelation: 0.31961072548600555
Weighted FMeasure: 0.37480135791384556
Iteration time: 422.0
Weighted AreaUnderPRC: 0.33571952663468313
Mean absolute error: 0.15476417063193929
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 3301.0
Weighted Recall: 0.3797979797979798
Weighted FalsePositiveRate: 0.06202020202020202
Kappa statistic: 0.31777777777777777
Training time: 421.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.69972451790605
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.42307315803007667
Weighted AreaUnderROC: 0.8277239057239056
Root mean squared error: 0.27367406683576456
Relative absolute error: 93.45454545454488
Root relative squared error: 95.19767264943798
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.3596547363997946
Weighted FMeasure: 0.41463755173885586
Iteration time: 429.0
Weighted AreaUnderPRC: 0.3616952181437537
Mean absolute error: 0.15447032306536443
Coverage of cases: 98.38383838383838
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 3730.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.05838383838383838
Kappa statistic: 0.35777777777777775
Training time: 429.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.77318640954977
Incorrectly Classified Instances: 55.35353535353536
Correctly Classified Instances: 44.64646464646464
Weighted Precision: 0.45737463457620764
Weighted AreaUnderROC: 0.8378540965207631
Root mean squared error: 0.27292143240357986
Relative absolute error: 93.19999999999932
Root relative squared error: 94.93586835380916
Weighted TruePositiveRate: 0.44646464646464645
Weighted MatthewsCorrelation: 0.39307811635853873
Weighted FMeasure: 0.44197882447514414
Iteration time: 439.0
Weighted AreaUnderPRC: 0.3820779488123932
Mean absolute error: 0.15404958677685937
Coverage of cases: 98.98989898989899
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 4169.0
Weighted Recall: 0.44646464646464645
Weighted FalsePositiveRate: 0.055353535353535356
Kappa statistic: 0.3911111111111111
Training time: 439.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.68135904499512
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.4783410011746152
Weighted AreaUnderROC: 0.854341189674523
Root mean squared error: 0.2722242627366325
Relative absolute error: 92.96565656565588
Root relative squared error: 94.69335750686434
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.42385608947650966
Weighted FMeasure: 0.4678272135204974
Iteration time: 439.0
Weighted AreaUnderPRC: 0.41632673671364573
Mean absolute error: 0.15366224225728342
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 4608.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919193
Kappa statistic: 0.4288888888888889
Training time: 438.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.57116620752953
Incorrectly Classified Instances: 49.494949494949495
Correctly Classified Instances: 50.505050505050505
Weighted Precision: 0.49945480833163275
Weighted AreaUnderROC: 0.859645342312009
Root mean squared error: 0.2718162707190028
Relative absolute error: 92.83636363636296
Root relative squared error: 94.5514372621478
Weighted TruePositiveRate: 0.5050505050505051
Weighted MatthewsCorrelation: 0.449172236895956
Weighted FMeasure: 0.4913452554104366
Iteration time: 447.0
Weighted AreaUnderPRC: 0.4453606633580875
Mean absolute error: 0.1534485349361381
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 5055.0
Weighted Recall: 0.5050505050505051
Weighted FalsePositiveRate: 0.04949494949494949
Kappa statistic: 0.45555555555555555
Training time: 446.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.2405876951328
Incorrectly Classified Instances: 48.888888888888886
Correctly Classified Instances: 51.111111111111114
Weighted Precision: 0.5190576679380723
Weighted AreaUnderROC: 0.8641257014590348
Root mean squared error: 0.2714743180629191
Relative absolute error: 92.72323232323166
Root relative squared error: 94.43248884517926
Weighted TruePositiveRate: 0.5111111111111111
Weighted MatthewsCorrelation: 0.4594673392174306
Weighted FMeasure: 0.4966736515554359
Iteration time: 450.0
Weighted AreaUnderPRC: 0.44248865628944084
Mean absolute error: 0.15326154103013595
Coverage of cases: 98.38383838383838
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 5505.0
Weighted Recall: 0.5111111111111111
Weighted FalsePositiveRate: 0.048888888888888885
Kappa statistic: 0.46222222222222215
Training time: 450.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.36914600550931
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5484326601097612
Weighted AreaUnderROC: 0.8796610549943883
Root mean squared error: 0.2707469634557742
Relative absolute error: 92.47676767676705
Root relative squared error: 94.17947815040783
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.48736399754707455
Weighted FMeasure: 0.5245433886327255
Iteration time: 461.0
Weighted AreaUnderPRC: 0.4626698008897978
Mean absolute error: 0.15285416144920272
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 5966.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 460.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.35078053259836
Incorrectly Classified Instances: 45.05050505050505
Correctly Classified Instances: 54.94949494949495
Weighted Precision: 0.5595373947704161
Weighted AreaUnderROC: 0.8875016835016836
Root mean squared error: 0.2703207962366031
Relative absolute error: 92.33535353535288
Root relative squared error: 94.03123565197292
Weighted TruePositiveRate: 0.5494949494949495
Weighted MatthewsCorrelation: 0.5053052047079717
Weighted FMeasure: 0.5434358297210845
Iteration time: 473.0
Weighted AreaUnderPRC: 0.4749168958258157
Mean absolute error: 0.15262041906669996
Coverage of cases: 98.98989898989899
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 6439.0
Weighted Recall: 0.5494949494949495
Weighted FalsePositiveRate: 0.04505050505050505
Kappa statistic: 0.5044444444444444
Training time: 472.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.40587695133112
Incorrectly Classified Instances: 43.23232323232323
Correctly Classified Instances: 56.76767676767677
Weighted Precision: 0.570262210422801
Weighted AreaUnderROC: 0.8897957351290684
Root mean squared error: 0.2701857817961644
Relative absolute error: 92.286868686868
Root relative squared error: 93.98427080560501
Weighted TruePositiveRate: 0.5676767676767677
Weighted MatthewsCorrelation: 0.5218912061851159
Weighted FMeasure: 0.5584774692410482
Iteration time: 480.0
Weighted AreaUnderPRC: 0.48041729130603106
Mean absolute error: 0.15254027882127041
Coverage of cases: 98.98989898989899
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6919.0
Weighted Recall: 0.5676767676767677
Weighted FalsePositiveRate: 0.043232323232323226
Kappa statistic: 0.5244444444444444
Training time: 479.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.42424242424205
Incorrectly Classified Instances: 40.80808080808081
Correctly Classified Instances: 59.19191919191919
Weighted Precision: 0.5900921187633037
Weighted AreaUnderROC: 0.8991604938271603
Root mean squared error: 0.2698478376595431
Relative absolute error: 92.16969696969625
Root relative squared error: 93.86671675430654
Weighted TruePositiveRate: 0.591919191919192
Weighted MatthewsCorrelation: 0.5465160442716523
Weighted FMeasure: 0.5804326952874806
Iteration time: 486.0
Weighted AreaUnderPRC: 0.5084532268624573
Mean absolute error: 0.15234660656148238
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7405.0
Weighted Recall: 0.591919191919192
Weighted FalsePositiveRate: 0.04080808080808081
Kappa statistic: 0.5511111111111111
Training time: 486.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.40587695133115
Incorrectly Classified Instances: 38.58585858585859
Correctly Classified Instances: 61.41414141414141
Weighted Precision: 0.6123942347929295
Weighted AreaUnderROC: 0.9113647586980921
Root mean squared error: 0.2691057093598524
Relative absolute error: 91.92727272727207
Root relative squared error: 93.60856702256656
Weighted TruePositiveRate: 0.6141414141414141
Weighted MatthewsCorrelation: 0.5709636004575341
Weighted FMeasure: 0.602570577629881
Iteration time: 490.0
Weighted AreaUnderPRC: 0.5285998298442042
Mean absolute error: 0.15194590533433497
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 7895.0
Weighted Recall: 0.6141414141414141
Weighted FalsePositiveRate: 0.038585858585858585
Kappa statistic: 0.5755555555555555
Training time: 489.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.36914600550932
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.604518983590884
Weighted AreaUnderROC: 0.9141840628507295
Root mean squared error: 0.2688553940556613
Relative absolute error: 91.8424242424236
Root relative squared error: 93.52149470817795
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5641681462607072
Weighted FMeasure: 0.59813607851635
Iteration time: 503.0
Weighted AreaUnderPRC: 0.5225038652471807
Mean absolute error: 0.15180565990483338
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 8398.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.03919191919191918
Kappa statistic: 0.5688888888888889
Training time: 502.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.18549127640001
Incorrectly Classified Instances: 37.77777777777778
Correctly Classified Instances: 62.22222222222222
Weighted Precision: 0.6156224474454033
Weighted AreaUnderROC: 0.9181279461279461
Root mean squared error: 0.26864055558429356
Relative absolute error: 91.76565656565593
Root relative squared error: 93.44676302933736
Weighted TruePositiveRate: 0.6222222222222222
Weighted MatthewsCorrelation: 0.5777899324140042
Weighted FMeasure: 0.6096433246674272
Iteration time: 509.0
Weighted AreaUnderPRC: 0.5373107545971899
Mean absolute error: 0.15167877118290335
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 8907.0
Weighted Recall: 0.6222222222222222
Weighted FalsePositiveRate: 0.03777777777777778
Kappa statistic: 0.5844444444444444
Training time: 508.0
		
Time end:Sun Oct 08 10.07.56 EEST 2017