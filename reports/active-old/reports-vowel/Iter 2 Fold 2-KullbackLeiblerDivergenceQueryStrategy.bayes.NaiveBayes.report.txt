Sun Oct 08 10.01.40 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.40 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 19.559228650137793
Incorrectly Classified Instances: 54.14141414141414
Correctly Classified Instances: 45.85858585858586
Weighted Precision: 0.4620053958009882
Weighted AreaUnderROC: 0.8832143658810325
Root mean squared error: 0.2765821688286196
Relative absolute error: 61.97512336997083
Root relative squared error: 96.2092575056426
Weighted TruePositiveRate: 0.4585858585858586
Weighted MatthewsCorrelation: 0.40301024369334676
Weighted FMeasure: 0.4511635354977387
Iteration time: 105.0
Weighted AreaUnderPRC: 0.48166697877031145
Mean absolute error: 0.10243822044623344
Coverage of cases: 69.29292929292929
Instances selection time: 104.0
Test time: 70.0
Accumulative iteration time: 105.0
Weighted Recall: 0.4585858585858586
Weighted FalsePositiveRate: 0.05414141414141414
Kappa statistic: 0.4044444444444445
Training time: 1.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 23.250688705234154
Incorrectly Classified Instances: 41.01010101010101
Correctly Classified Instances: 58.98989898989899
Weighted Precision: 0.611158409410657
Weighted AreaUnderROC: 0.939551066217733
Root mean squared error: 0.2301567492357022
Relative absolute error: 52.569213388456106
Root relative squared error: 80.06015010895379
Weighted TruePositiveRate: 0.5898989898989899
Weighted MatthewsCorrelation: 0.5450999557906252
Weighted FMeasure: 0.5669880509762427
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6391174418862825
Mean absolute error: 0.08689126179910156
Coverage of cases: 86.26262626262626
Instances selection time: 21.0
Test time: 67.0
Accumulative iteration time: 132.0
Weighted Recall: 0.5898989898989899
Weighted FalsePositiveRate: 0.04101010101010101
Kappa statistic: 0.5488888888888889
Training time: 6.0
		
Time end:Sun Oct 08 10.01.40 EEST 2017