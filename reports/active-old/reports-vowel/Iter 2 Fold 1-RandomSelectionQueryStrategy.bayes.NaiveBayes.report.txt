Sun Oct 08 10.07.34 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.07.34 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 19.779614325068945
Incorrectly Classified Instances: 50.505050505050505
Correctly Classified Instances: 49.494949494949495
Weighted Precision: 0.5410918013718464
Weighted AreaUnderROC: 0.9090056116722783
Root mean squared error: 0.2670569576929453
Relative absolute error: 58.85730610621287
Root relative squared error: 92.89590764354232
Weighted TruePositiveRate: 0.494949494949495
Weighted MatthewsCorrelation: 0.45988260329129255
Weighted FMeasure: 0.4981542476531545
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5468615760953602
Mean absolute error: 0.0972848034813442
Coverage of cases: 76.36363636363636
Instances selection time: 5.0
Test time: 33.0
Accumulative iteration time: 6.0
Weighted Recall: 0.494949494949495
Weighted FalsePositiveRate: 0.050505050505050504
Kappa statistic: 0.44444444444444453
Training time: 1.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 20.80808080808086
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.5483194638939752
Weighted AreaUnderROC: 0.9057149270482603
Root mean squared error: 0.2648912212578453
Relative absolute error: 58.8253933669141
Root relative squared error: 92.14255504942409
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.47360388712111323
Weighted FMeasure: 0.5131295598633463
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5559368280023718
Mean absolute error: 0.09723205515192476
Coverage of cases: 76.76767676767676
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 7.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.04909090909090909
Kappa statistic: 0.4599999999999999
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 21.4508723599633
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5323735133207352
Weighted AreaUnderROC: 0.9066666666666667
Root mean squared error: 0.2621126234966406
Relative absolute error: 59.1151867344212
Root relative squared error: 91.17601831047043
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.4608767525657611
Weighted FMeasure: 0.5031826467524966
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5522177730740093
Mean absolute error: 0.0977110524535894
Coverage of cases: 76.56565656565657
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 9.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.050101010101010104
Kappa statistic: 0.44888888888888884
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 22.42424242424246
Incorrectly Classified Instances: 47.676767676767675
Correctly Classified Instances: 52.323232323232325
Weighted Precision: 0.5417199667990529
Weighted AreaUnderROC: 0.9199371492704826
Root mean squared error: 0.25175509669229157
Relative absolute error: 57.25495981239692
Root relative squared error: 87.57314699139174
Weighted TruePositiveRate: 0.5232323232323233
Weighted MatthewsCorrelation: 0.4828816847842233
Weighted FMeasure: 0.5274466570240809
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5919103053334898
Mean absolute error: 0.09463629721057402
Coverage of cases: 82.42424242424242
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5232323232323233
Weighted FalsePositiveRate: 0.04767676767676768
Kappa statistic: 0.47555555555555556
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 22.258953168044123
Incorrectly Classified Instances: 46.464646464646464
Correctly Classified Instances: 53.535353535353536
Weighted Precision: 0.5465766512254261
Weighted AreaUnderROC: 0.9232861952861953
Root mean squared error: 0.25008152000022094
Relative absolute error: 56.784863734012895
Root relative squared error: 86.99099243094126
Weighted TruePositiveRate: 0.5353535353535354
Weighted MatthewsCorrelation: 0.49261967978380367
Weighted FMeasure: 0.535903689858717
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5915123759002379
Mean absolute error: 0.09385927889919547
Coverage of cases: 85.05050505050505
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5353535353535354
Weighted FalsePositiveRate: 0.04646464646464647
Kappa statistic: 0.4888888888888889
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 22.99357208448119
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5604106903676453
Weighted AreaUnderROC: 0.92747923681257
Root mean squared error: 0.24646435836826583
Relative absolute error: 56.49653753570185
Root relative squared error: 85.73276079452678
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5024576871600631
Weighted FMeasure: 0.5441499543088064
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6103190356958501
Mean absolute error: 0.09338270667058217
Coverage of cases: 86.86868686868686
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.045858585858585856
Kappa statistic: 0.4955555555555556
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 22.093663911845777
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5581545662706074
Weighted AreaUnderROC: 0.9253692480359146
Root mean squared error: 0.24961440854529807
Relative absolute error: 56.59552687635511
Root relative squared error: 86.82850745788302
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.4984907238584711
Weighted FMeasure: 0.5397748079332818
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5977549884762324
Mean absolute error: 0.09354632541546358
Coverage of cases: 86.46464646464646
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262626
Kappa statistic: 0.4911111111111111
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 23.103764921946794
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.5251613489711761
Weighted AreaUnderROC: 0.925557800224467
Root mean squared error: 0.24905160496719278
Relative absolute error: 57.05926208964879
Root relative squared error: 86.63273592785146
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.46953402165806857
Weighted FMeasure: 0.5145039072910196
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5955647919818504
Mean absolute error: 0.09431282990024654
Coverage of cases: 87.27272727272727
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 16.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.048484848484848485
Kappa statistic: 0.4666666666666666
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 22.71808999081731
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.5218611279252696
Weighted AreaUnderROC: 0.9243052749719416
Root mean squared error: 0.25238634535584165
Relative absolute error: 57.46140380589561
Root relative squared error: 87.79272718153486
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.46745046921370603
Weighted FMeasure: 0.511665168799462
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5863296762801852
Mean absolute error: 0.09497752695189417
Coverage of cases: 85.65656565656566
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 18.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.0484848484848485
Kappa statistic: 0.4666666666666666
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 23.17722681359052
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5379483990563673
Weighted AreaUnderROC: 0.9283771043771044
Root mean squared error: 0.24696482194453456
Relative absolute error: 56.642120889194175
Root relative squared error: 85.90684732109267
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.48595275154332646
Weighted FMeasure: 0.5298668203380761
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6018886368518162
Mean absolute error: 0.09362334031271825
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 19.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 22.66299357208452
Incorrectly Classified Instances: 49.898989898989896
Correctly Classified Instances: 50.101010101010104
Weighted Precision: 0.5177079298413113
Weighted AreaUnderROC: 0.9227699214365881
Root mean squared error: 0.2523517005302172
Relative absolute error: 57.75465207781165
Root relative squared error: 87.78067596014247
Weighted TruePositiveRate: 0.501010101010101
Weighted MatthewsCorrelation: 0.45695131123059435
Weighted FMeasure: 0.5025853799916378
Iteration time: 1.0
Weighted AreaUnderPRC: 0.593586040785326
Mean absolute error: 0.0954622348393587
Coverage of cases: 84.04040404040404
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 20.0
Weighted Recall: 0.501010101010101
Weighted FalsePositiveRate: 0.04989898989898989
Kappa statistic: 0.4511111111111111
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 22.97520661157028
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5457146736824117
Weighted AreaUnderROC: 0.9246374859708193
Root mean squared error: 0.24999100142617517
Relative absolute error: 57.2857774933692
Root relative squared error: 86.9595054958424
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4855990855043633
Weighted FMeasure: 0.5289975533483715
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5933202564599771
Mean absolute error: 0.09468723552623069
Coverage of cases: 85.05050505050505
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 21.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.04747474747474749
Kappa statistic: 0.4777777777777778
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 23.397612488521613
Incorrectly Classified Instances: 47.07070707070707
Correctly Classified Instances: 52.92929292929293
Weighted Precision: 0.5464036965248441
Weighted AreaUnderROC: 0.9247407407407406
Root mean squared error: 0.24651609888666015
Relative absolute error: 56.37672476009009
Root relative squared error: 85.75075876192562
Weighted TruePositiveRate: 0.5292929292929293
Weighted MatthewsCorrelation: 0.48840504075641167
Weighted FMeasure: 0.5315246084537077
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5985490944477259
Mean absolute error: 0.09318466902494289
Coverage of cases: 87.07070707070707
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 22.0
Weighted Recall: 0.5292929292929293
Weighted FalsePositiveRate: 0.04707070707070706
Kappa statistic: 0.48222222222222216
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 23.4527089072544
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.5472787144153572
Weighted AreaUnderROC: 0.9245028058361391
Root mean squared error: 0.24520770003480877
Relative absolute error: 56.10785620855569
Root relative squared error: 85.29563151134766
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.4875598055832025
Weighted FMeasure: 0.5307176411857915
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6003524383001059
Mean absolute error: 0.0927402581959604
Coverage of cases: 88.28282828282828
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 24.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.047272727272727286
Kappa statistic: 0.4799999999999999
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 24.517906336088167
Incorrectly Classified Instances: 45.45454545454545
Correctly Classified Instances: 54.54545454545455
Weighted Precision: 0.5590897842357307
Weighted AreaUnderROC: 0.9272413019079685
Root mean squared error: 0.2408932212278527
Relative absolute error: 56.079061625586235
Root relative squared error: 83.79483771723189
Weighted TruePositiveRate: 0.5454545454545454
Weighted MatthewsCorrelation: 0.5046113394119119
Weighted FMeasure: 0.5464504798617367
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6162536764906926
Mean absolute error: 0.09269266384394478
Coverage of cases: 88.88888888888889
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 25.0
Weighted Recall: 0.5454545454545454
Weighted FalsePositiveRate: 0.045454545454545456
Kappa statistic: 0.49999999999999994
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 24.811753902662996
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5572665471220761
Weighted AreaUnderROC: 0.9270482603815936
Root mean squared error: 0.23834628812723377
Relative absolute error: 55.773915430090845
Root relative squared error: 82.90888565616852
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5011692057688824
Weighted FMeasure: 0.5432123472214266
Iteration time: 2.0
Weighted AreaUnderPRC: 0.615894868351974
Mean absolute error: 0.0921882899670929
Coverage of cases: 88.68686868686869
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 27.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 25.656565656565665
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5655745998187859
Weighted AreaUnderROC: 0.9329876543209876
Root mean squared error: 0.23497668564160307
Relative absolute error: 55.593660331745475
Root relative squared error: 81.73676760313316
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5115143588044877
Weighted FMeasure: 0.5527404297763606
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6247318342146799
Mean absolute error: 0.09189034765577823
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 29.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 25.215794306703405
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.554961596397037
Weighted AreaUnderROC: 0.9302132435465769
Root mean squared error: 0.23767369684284728
Relative absolute error: 56.011987153881
Root relative squared error: 82.67492441293419
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.4989434131011371
Weighted FMeasure: 0.5414078134333546
Iteration time: 2.0
Weighted AreaUnderPRC: 0.613999158313941
Mean absolute error: 0.09258179694856424
Coverage of cases: 89.8989898989899
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 31.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.046060606060606066
Kappa statistic: 0.49333333333333335
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 26.134067952249755
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5679096067331362
Weighted AreaUnderROC: 0.9365207631874298
Root mean squared error: 0.2303660983788853
Relative absolute error: 54.79449578091615
Root relative squared error: 80.13297232200675
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5125635015378398
Weighted FMeasure: 0.5535791373589654
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6317297191898529
Mean absolute error: 0.09056941451391157
Coverage of cases: 91.71717171717172
Instances selection time: 0.0
Test time: 32.0
Accumulative iteration time: 32.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 25.78512396694214
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5774060664735722
Weighted AreaUnderROC: 0.935699214365881
Root mean squared error: 0.23209165872285317
Relative absolute error: 55.14965607236281
Root relative squared error: 80.73320942397697
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5195786815458728
Weighted FMeasure: 0.5600005097661105
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6150571477412412
Mean absolute error: 0.09115645631795564
Coverage of cases: 91.11111111111111
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 34.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 2.0
		
Time end:Sun Oct 08 10.07.36 EEST 2017