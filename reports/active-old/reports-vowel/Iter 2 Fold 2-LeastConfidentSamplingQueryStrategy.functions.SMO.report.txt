Sun Oct 08 10.02.48 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.02.48 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 75.15151515151516
Correctly Classified Instances: 24.848484848484848
Weighted Precision: 0.23522560188570388
Weighted AreaUnderROC: 0.7086734006734007
Root mean squared error: 0.28109240350898845
Relative absolute error: 95.92323232323182
Root relative squared error: 97.77814508654578
Weighted TruePositiveRate: 0.24848484848484848
Weighted MatthewsCorrelation: 0.16237264467795706
Weighted FMeasure: 0.2189740743389824
Iteration time: 398.0
Weighted AreaUnderPRC: 0.21252594320416235
Mean absolute error: 0.15855079722848336
Coverage of cases: 95.75757575757575
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 398.0
Weighted Recall: 0.24848484848484848
Weighted FalsePositiveRate: 0.07515151515151515
Kappa statistic: 0.17333333333333334
Training time: 393.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 81.94674012855789
Incorrectly Classified Instances: 68.88888888888889
Correctly Classified Instances: 31.11111111111111
Weighted Precision: 0.3289996239479329
Weighted AreaUnderROC: 0.7308462401795736
Root mean squared error: 0.2798603051630185
Relative absolute error: 95.50303030302973
Root relative squared error: 97.34955900834076
Weighted TruePositiveRate: 0.3111111111111111
Weighted MatthewsCorrelation: 0.2286221858799555
Weighted FMeasure: 0.26501871803644006
Iteration time: 389.0
Weighted AreaUnderPRC: 0.24101848625934938
Mean absolute error: 0.1578562484347609
Coverage of cases: 96.56565656565657
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 787.0
Weighted Recall: 0.3111111111111111
Weighted FalsePositiveRate: 0.06888888888888889
Kappa statistic: 0.24222222222222223
Training time: 384.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 81.92837465564698
Incorrectly Classified Instances: 68.08080808080808
Correctly Classified Instances: 31.91919191919192
Weighted Precision: 0.35757280984782325
Weighted AreaUnderROC: 0.749813692480359
Root mean squared error: 0.2797954332119556
Relative absolute error: 95.4707070707064
Root relative squared error: 97.3269932649628
Weighted TruePositiveRate: 0.3191919191919192
Weighted MatthewsCorrelation: 0.24365181403964847
Weighted FMeasure: 0.2794479892283255
Iteration time: 391.0
Weighted AreaUnderPRC: 0.2560782165950558
Mean absolute error: 0.1578028216044744
Coverage of cases: 95.55555555555556
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 1178.0
Weighted Recall: 0.3191919191919192
Weighted FalsePositiveRate: 0.06808080808080808
Kappa statistic: 0.2511111111111111
Training time: 386.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 81.91000918273605
Incorrectly Classified Instances: 64.44444444444444
Correctly Classified Instances: 35.55555555555556
Weighted Precision: 0.32699241727737655
Weighted AreaUnderROC: 0.7790145903479236
Root mean squared error: 0.27820974692119543
Relative absolute error: 94.93737373737304
Root relative squared error: 96.77541142829912
Weighted TruePositiveRate: 0.35555555555555557
Weighted MatthewsCorrelation: 0.26983091720145225
Weighted FMeasure: 0.31137855242109447
Iteration time: 394.0
Weighted AreaUnderPRC: 0.2775660933748165
Mean absolute error: 0.15692127890474983
Coverage of cases: 96.76767676767676
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 1572.0
Weighted Recall: 0.35555555555555557
Weighted FalsePositiveRate: 0.06444444444444446
Kappa statistic: 0.29111111111111115
Training time: 390.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.02020202020161
Incorrectly Classified Instances: 62.62626262626262
Correctly Classified Instances: 37.37373737373738
Weighted Precision: 0.36667922797503155
Weighted AreaUnderROC: 0.8041818181818182
Root mean squared error: 0.2766050939697286
Relative absolute error: 94.40404040403973
Root relative squared error: 96.21723202841697
Weighted TruePositiveRate: 0.37373737373737376
Weighted MatthewsCorrelation: 0.2985850904100023
Weighted FMeasure: 0.3350221185964106
Iteration time: 417.0
Weighted AreaUnderPRC: 0.32102701363368896
Mean absolute error: 0.15603973620502534
Coverage of cases: 97.57575757575758
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 1989.0
Weighted Recall: 0.37373737373737376
Weighted FalsePositiveRate: 0.06262626262626264
Kappa statistic: 0.31111111111111117
Training time: 413.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.29568411386558
Incorrectly Classified Instances: 57.97979797979798
Correctly Classified Instances: 42.02020202020202
Weighted Precision: 0.3912713774872824
Weighted AreaUnderROC: 0.825364758698092
Root mean squared error: 0.27554798110152123
Relative absolute error: 94.06868686868617
Root relative squared error: 95.84951474360203
Weighted TruePositiveRate: 0.4202020202020202
Weighted MatthewsCorrelation: 0.3461785907243396
Weighted FMeasure: 0.3883172506699608
Iteration time: 406.0
Weighted AreaUnderPRC: 0.34366520601213946
Mean absolute error: 0.1554854328408046
Coverage of cases: 97.57575757575758
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2395.0
Weighted Recall: 0.4202020202020202
Weighted FalsePositiveRate: 0.057979797979797985
Kappa statistic: 0.3622222222222222
Training time: 402.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.05693296602347
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.4456545232840863
Weighted AreaUnderROC: 0.8433692480359148
Root mean squared error: 0.27411495474753605
Relative absolute error: 93.57575757575682
Root relative squared error: 95.35103574878163
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.410360088809402
Weighted FMeasure: 0.4481416166462581
Iteration time: 418.0
Weighted AreaUnderPRC: 0.37382393354812254
Mean absolute error: 0.1546706736789379
Coverage of cases: 96.96969696969697
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2813.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919192
Kappa statistic: 0.4288888888888889
Training time: 414.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 52.323232323232325
Correctly Classified Instances: 47.676767676767675
Weighted Precision: 0.4747454611670105
Weighted AreaUnderROC: 0.8417396184062851
Root mean squared error: 0.27404340580614045
Relative absolute error: 93.55959595959519
Root relative squared error: 95.3261474106934
Weighted TruePositiveRate: 0.4767676767676768
Weighted MatthewsCorrelation: 0.4149456537687114
Weighted FMeasure: 0.45190147967694205
Iteration time: 416.0
Weighted AreaUnderPRC: 0.37644117184144904
Mean absolute error: 0.15464396026379468
Coverage of cases: 96.96969696969697
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 3229.0
Weighted Recall: 0.4767676767676768
Weighted FalsePositiveRate: 0.05232323232323232
Kappa statistic: 0.4244444444444445
Training time: 413.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.03856749311255
Incorrectly Classified Instances: 51.313131313131315
Correctly Classified Instances: 48.686868686868685
Weighted Precision: 0.4831307439927166
Weighted AreaUnderROC: 0.8453288439955107
Root mean squared error: 0.27375835368414914
Relative absolute error: 93.46262626262562
Root relative squared error: 95.22699187538416
Weighted TruePositiveRate: 0.4868686868686869
Weighted MatthewsCorrelation: 0.4213711090328777
Weighted FMeasure: 0.45100109781015296
Iteration time: 425.0
Weighted AreaUnderPRC: 0.3849328553148979
Mean absolute error: 0.1544836797729359
Coverage of cases: 96.76767676767676
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3654.0
Weighted Recall: 0.4868686868686869
Weighted FalsePositiveRate: 0.05131313131313132
Kappa statistic: 0.4355555555555556
Training time: 422.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.538650408397068
Weighted AreaUnderROC: 0.8559326599326601
Root mean squared error: 0.27289829626682544
Relative absolute error: 93.17171717171651
Root relative squared error: 94.92782043608503
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.46578844358540916
Weighted FMeasure: 0.49167948857740007
Iteration time: 433.0
Weighted AreaUnderPRC: 0.41175559870141176
Mean absolute error: 0.15400283830035885
Coverage of cases: 96.36363636363636
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4087.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282829
Kappa statistic: 0.46888888888888886
Training time: 430.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.22222222222186
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.5319775191631415
Weighted AreaUnderROC: 0.8655039281705947
Root mean squared error: 0.27222225552106233
Relative absolute error: 92.94141414141356
Root relative squared error: 94.6926592958391
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.474416548559955
Weighted FMeasure: 0.5076214079978728
Iteration time: 436.0
Weighted AreaUnderPRC: 0.4260005708462404
Mean absolute error: 0.15362217213456886
Coverage of cases: 97.17171717171718
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4523.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.04727272727272727
Kappa statistic: 0.4799999999999999
Training time: 433.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.22222222222186
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.5105397876662143
Weighted AreaUnderROC: 0.8686060606060607
Root mean squared error: 0.27159147973677944
Relative absolute error: 92.74343434343386
Root relative squared error: 94.47324359700566
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.45775526652529824
Weighted FMeasure: 0.49232621310594193
Iteration time: 460.0
Weighted AreaUnderPRC: 0.4273139134373882
Mean absolute error: 0.15329493279906523
Coverage of cases: 97.97979797979798
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 4983.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282829
Kappa statistic: 0.46888888888888886
Training time: 458.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.2038567493109
Incorrectly Classified Instances: 49.494949494949495
Correctly Classified Instances: 50.505050505050505
Weighted Precision: 0.5035863097338713
Weighted AreaUnderROC: 0.8777351290684624
Root mean squared error: 0.27115678829793954
Relative absolute error: 92.59393939393897
Root relative squared error: 94.32203594413359
Weighted TruePositiveRate: 0.5050505050505051
Weighted MatthewsCorrelation: 0.4489119177069268
Weighted FMeasure: 0.48726475726287233
Iteration time: 459.0
Weighted AreaUnderPRC: 0.43102433004484636
Mean absolute error: 0.15304783370899103
Coverage of cases: 98.38383838383838
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5442.0
Weighted Recall: 0.5050505050505051
Weighted FalsePositiveRate: 0.049494949494949494
Kappa statistic: 0.45555555555555555
Training time: 457.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.14876033057813
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5137735835939694
Weighted AreaUnderROC: 0.8888799102132435
Root mean squared error: 0.2704496822083397
Relative absolute error: 92.35959595959544
Root relative squared error: 94.07606870717741
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.4503324674653305
Weighted FMeasure: 0.4901499115104474
Iteration time: 695.0
Weighted AreaUnderPRC: 0.44324845477653313
Mean absolute error: 0.15266048918941494
Coverage of cases: 98.98989898989899
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6137.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.0501010101010101
Kappa statistic: 0.44888888888888884
Training time: 693.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.35078053259835
Incorrectly Classified Instances: 50.707070707070706
Correctly Classified Instances: 49.292929292929294
Weighted Precision: 0.4838232998285005
Weighted AreaUnderROC: 0.8875914702581369
Root mean squared error: 0.27061911645406495
Relative absolute error: 92.41616161616115
Root relative squared error: 94.1350065014911
Weighted TruePositiveRate: 0.49292929292929294
Weighted MatthewsCorrelation: 0.434860886125312
Weighted FMeasure: 0.4798149620968472
Iteration time: 528.0
Weighted AreaUnderPRC: 0.4310736389816899
Mean absolute error: 0.1527539861424161
Coverage of cases: 98.78787878787878
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6665.0
Weighted Recall: 0.49292929292929294
Weighted FalsePositiveRate: 0.05070707070707071
Kappa statistic: 0.4422222222222223
Training time: 526.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.16712580348906
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5286451185515479
Weighted AreaUnderROC: 0.8924826038159371
Root mean squared error: 0.27029721288517017
Relative absolute error: 92.3111111111106
Root relative squared error: 94.02303217038022
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.47074921537929665
Weighted FMeasure: 0.5080884319470745
Iteration time: 520.0
Weighted AreaUnderPRC: 0.45993776581790485
Mean absolute error: 0.15258034894398545
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7185.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.047878787878787875
Kappa statistic: 0.47333333333333333
Training time: 519.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.07529843893441
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.5270415730870714
Weighted AreaUnderROC: 0.8945432098765432
Root mean squared error: 0.2701253291780713
Relative absolute error: 92.24646464646415
Root relative squared error: 93.9632423295986
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.4666679496196363
Weighted FMeasure: 0.5026143757865236
Iteration time: 542.0
Weighted AreaUnderPRC: 0.4601573778395984
Mean absolute error: 0.15247349528341278
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7727.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282828
Kappa statistic: 0.46888888888888886
Training time: 541.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5303329720921254
Weighted AreaUnderROC: 0.8942267115600449
Root mean squared error: 0.2701030774107303
Relative absolute error: 92.23838383838334
Root relative squared error: 93.95550204025481
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.4703067971354426
Weighted FMeasure: 0.5044177233224425
Iteration time: 556.0
Weighted AreaUnderPRC: 0.468205410133093
Mean absolute error: 0.1524601385758412
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8283.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.047878787878787875
Kappa statistic: 0.47333333333333333
Training time: 555.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 81.9467401285579
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5365145327771655
Weighted AreaUnderROC: 0.9029898989898989
Root mean squared error: 0.2695108213818943
Relative absolute error: 92.04444444444394
Root relative squared error: 93.74948545925513
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.4820512207866139
Weighted FMeasure: 0.5194056504225052
Iteration time: 562.0
Weighted AreaUnderPRC: 0.471725875748821
Mean absolute error: 0.1521395775941232
Coverage of cases: 99.5959595959596
Instances selection time: 2.0
Test time: 23.0
Accumulative iteration time: 8845.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.046868686868686865
Kappa statistic: 0.4844444444444444
Training time: 560.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 81.91000918273605
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5557369178844327
Weighted AreaUnderROC: 0.9135622895622895
Root mean squared error: 0.26884907108566497
Relative absolute error: 91.82222222222174
Root relative squared error: 93.51929525963378
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.5003432890098913
Weighted FMeasure: 0.539673629432772
Iteration time: 582.0
Weighted AreaUnderPRC: 0.4966253286273862
Mean absolute error: 0.15177226813590466
Coverage of cases: 99.5959595959596
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 9427.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656565
Kappa statistic: 0.4977777777777777
Training time: 581.0
		
Time end:Sun Oct 08 10.02.59 EEST 2017