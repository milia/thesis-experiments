Sun Oct 08 10.07.13 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.07.13 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 44.334251606978945
Incorrectly Classified Instances: 68.08080808080808
Correctly Classified Instances: 31.91919191919192
Weighted Precision: 0.34438915803719977
Weighted AreaUnderROC: 0.7059124579124579
Root mean squared error: 0.2813682922112564
Relative absolute error: 83.76017546332336
Root relative squared error: 97.874113121332
Weighted TruePositiveRate: 0.3191919191919192
Weighted MatthewsCorrelation: 0.2600955011023753
Weighted FMeasure: 0.3202079261613572
Iteration time: 13.0
Weighted AreaUnderPRC: 0.32964157594099214
Mean absolute error: 0.1384465710137585
Coverage of cases: 70.5050505050505
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 13.0
Weighted Recall: 0.3191919191919192
Weighted FalsePositiveRate: 0.06808080808080808
Kappa statistic: 0.2511111111111111
Training time: 11.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 39.85307621671268
Incorrectly Classified Instances: 64.44444444444444
Correctly Classified Instances: 35.55555555555556
Weighted Precision: 0.38544219928097995
Weighted AreaUnderROC: 0.7315869809203143
Root mean squared error: 0.2734083417904124
Relative absolute error: 79.87879651212933
Root relative squared error: 95.1052400482249
Weighted TruePositiveRate: 0.35555555555555557
Weighted MatthewsCorrelation: 0.29897447426703727
Weighted FMeasure: 0.3499038867202003
Iteration time: 15.0
Weighted AreaUnderPRC: 0.37710397194483736
Mean absolute error: 0.13203106861509065
Coverage of cases: 71.11111111111111
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 28.0
Weighted Recall: 0.35555555555555557
Weighted FalsePositiveRate: 0.06444444444444446
Kappa statistic: 0.29111111111111115
Training time: 13.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 41.24885215794319
Incorrectly Classified Instances: 57.77777777777778
Correctly Classified Instances: 42.22222222222222
Weighted Precision: 0.4441743683841786
Weighted AreaUnderROC: 0.7846150392817058
Root mean squared error: 0.2608357145625003
Relative absolute error: 76.06641834393996
Root relative squared error: 90.73184484485529
Weighted TruePositiveRate: 0.4222222222222222
Weighted MatthewsCorrelation: 0.37248295017379895
Weighted FMeasure: 0.4248221365421328
Iteration time: 19.0
Weighted AreaUnderPRC: 0.4528000725189723
Mean absolute error: 0.12572961709742222
Coverage of cases: 80.20202020202021
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 47.0
Weighted Recall: 0.4222222222222222
Weighted FalsePositiveRate: 0.05777777777777779
Kappa statistic: 0.36444444444444446
Training time: 17.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 42.00183654729123
Incorrectly Classified Instances: 51.313131313131315
Correctly Classified Instances: 48.686868686868685
Weighted Precision: 0.49160186051513083
Weighted AreaUnderROC: 0.8221279461279462
Root mean squared error: 0.2507418273960762
Relative absolute error: 73.4229278651772
Root relative squared error: 87.22068071688454
Weighted TruePositiveRate: 0.4868686868686869
Weighted MatthewsCorrelation: 0.43532875389901315
Weighted FMeasure: 0.4824448793512892
Iteration time: 22.0
Weighted AreaUnderPRC: 0.5022733847689452
Mean absolute error: 0.12136021134740112
Coverage of cases: 83.63636363636364
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.4868686868686869
Weighted FalsePositiveRate: 0.05131313131313131
Kappa statistic: 0.4355555555555556
Training time: 20.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 43.30578512396707
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5092283618557232
Weighted AreaUnderROC: 0.8173670033670034
Root mean squared error: 0.2451492406711551
Relative absolute error: 71.54486491403142
Root relative squared error: 85.2752963899796
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.453626098831635
Weighted FMeasure: 0.4982398600624137
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5417806830711713
Mean absolute error: 0.11825597506451549
Coverage of cases: 85.45454545454545
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 96.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.04969696969696969
Kappa statistic: 0.4533333333333333
Training time: 25.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 41.340679522497865
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5129508070974823
Weighted AreaUnderROC: 0.834915824915825
Root mean squared error: 0.24267471727780668
Relative absolute error: 69.22800565091023
Root relative squared error: 84.41453208488113
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.45612194598955585
Weighted FMeasure: 0.501726961796503
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5529590517903741
Mean absolute error: 0.11442645562133996
Coverage of cases: 84.24242424242425
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 124.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.04969696969696969
Kappa statistic: 0.4533333333333333
Training time: 26.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 41.26721763085411
Incorrectly Classified Instances: 44.04040404040404
Correctly Classified Instances: 55.95959595959596
Weighted Precision: 0.5578875005786204
Weighted AreaUnderROC: 0.816868686868687
Root mean squared error: 0.24537083262029305
Relative absolute error: 70.35458086231333
Root relative squared error: 85.35237726972713
Weighted TruePositiveRate: 0.5595959595959596
Weighted MatthewsCorrelation: 0.5122325342467967
Weighted FMeasure: 0.5516922688003867
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5334267007663156
Mean absolute error: 0.11628856340878309
Coverage of cases: 79.5959595959596
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 153.0
Weighted Recall: 0.5595959595959596
Weighted FalsePositiveRate: 0.044040404040404046
Kappa statistic: 0.5155555555555555
Training time: 28.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 36.712580348944144
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.619475347881819
Weighted AreaUnderROC: 0.875584736251403
Root mean squared error: 0.22588519333363602
Relative absolute error: 63.09371202459345
Root relative squared error: 78.57428707059472
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5718310888643997
Weighted FMeasure: 0.6063660204620862
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6483722181658902
Mean absolute error: 0.10428712731337828
Coverage of cases: 85.85858585858585
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 189.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.039191919191919194
Kappa statistic: 0.5688888888888889
Training time: 34.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 37.53902662993587
Incorrectly Classified Instances: 34.343434343434346
Correctly Classified Instances: 65.65656565656566
Weighted Precision: 0.6607191883768831
Weighted AreaUnderROC: 0.8980471380471381
Root mean squared error: 0.21851022561080716
Relative absolute error: 60.991316399663354
Root relative squared error: 76.00890054641462
Weighted TruePositiveRate: 0.6565656565656566
Weighted MatthewsCorrelation: 0.6226076697219066
Weighted FMeasure: 0.6540379100758085
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6789634456488199
Mean absolute error: 0.10081209322258469
Coverage of cases: 88.48484848484848
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 228.0
Weighted Recall: 0.6565656565656566
Weighted FalsePositiveRate: 0.03434343434343434
Kappa statistic: 0.6222222222222222
Training time: 37.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 36.822773186409655
Incorrectly Classified Instances: 27.87878787878788
Correctly Classified Instances: 72.12121212121212
Weighted Precision: 0.7212910305003266
Weighted AreaUnderROC: 0.9130303030303031
Root mean squared error: 0.20726776901283075
Relative absolute error: 57.70465553997798
Root relative squared error: 72.09820591844337
Weighted TruePositiveRate: 0.7212121212121212
Weighted MatthewsCorrelation: 0.6907816732083885
Weighted FMeasure: 0.7141673112959703
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7319575298366292
Mean absolute error: 0.09537959593384851
Coverage of cases: 91.11111111111111
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 268.0
Weighted Recall: 0.7212121212121212
Weighted FalsePositiveRate: 0.02787878787878788
Kappa statistic: 0.6933333333333334
Training time: 39.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 35.26170798898082
Incorrectly Classified Instances: 23.636363636363637
Correctly Classified Instances: 76.36363636363636
Weighted Precision: 0.7785414522392816
Weighted AreaUnderROC: 0.9289225589225589
Root mean squared error: 0.20075098888111062
Relative absolute error: 55.10949784096409
Root relative squared error: 69.83134041349892
Weighted TruePositiveRate: 0.7636363636363637
Weighted MatthewsCorrelation: 0.7443009138463285
Weighted FMeasure: 0.7631388252416699
Iteration time: 43.0
Weighted AreaUnderPRC: 0.7686117847497066
Mean absolute error: 0.09109007907597429
Coverage of cases: 92.12121212121212
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 311.0
Weighted Recall: 0.7636363636363637
Weighted FalsePositiveRate: 0.02363636363636364
Kappa statistic: 0.74
Training time: 42.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 35.996326905417895
Incorrectly Classified Instances: 27.07070707070707
Correctly Classified Instances: 72.92929292929293
Weighted Precision: 0.7461718162404268
Weighted AreaUnderROC: 0.943905723905724
Root mean squared error: 0.20053910703737882
Relative absolute error: 55.38051373733477
Root relative squared error: 69.7576371991858
Weighted TruePositiveRate: 0.7292929292929293
Weighted MatthewsCorrelation: 0.7067236566022382
Weighted FMeasure: 0.7276767505307457
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7688408490278621
Mean absolute error: 0.09153803923526467
Coverage of cases: 96.36363636363636
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 359.0
Weighted Recall: 0.7292929292929293
Weighted FalsePositiveRate: 0.027070707070707075
Kappa statistic: 0.7022222222222223
Training time: 47.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.949494949495026
Incorrectly Classified Instances: 21.616161616161616
Correctly Classified Instances: 78.38383838383838
Weighted Precision: 0.7966235470266344
Weighted AreaUnderROC: 0.9602962962962963
Root mean squared error: 0.19054568732487565
Relative absolute error: 52.64513843780504
Root relative squared error: 66.28142072957695
Weighted TruePositiveRate: 0.7838383838383839
Weighted MatthewsCorrelation: 0.7658501795010628
Weighted FMeasure: 0.7831165504519226
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8278555879434594
Mean absolute error: 0.08701675774843864
Coverage of cases: 96.76767676767676
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 407.0
Weighted Recall: 0.7838383838383839
Weighted FalsePositiveRate: 0.021616161616161613
Kappa statistic: 0.7622222222222222
Training time: 47.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 34.91276400367319
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8076972823337688
Weighted AreaUnderROC: 0.9696004489337822
Root mean squared error: 0.18442143377819678
Relative absolute error: 51.0546479067372
Root relative squared error: 64.15109581023131
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7819140014308176
Weighted FMeasure: 0.7987826710842392
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8592822391497492
Mean absolute error: 0.08438784777973146
Coverage of cases: 98.18181818181819
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 462.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.019999999999999997
Kappa statistic: 0.78
Training time: 54.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 34.17814508723609
Incorrectly Classified Instances: 18.78787878787879
Correctly Classified Instances: 81.21212121212122
Weighted Precision: 0.8233587392529755
Weighted AreaUnderROC: 0.9684309764309764
Root mean squared error: 0.1777543157027688
Relative absolute error: 48.910492436858796
Root relative squared error: 61.83193516999195
Weighted TruePositiveRate: 0.8121212121212121
Weighted MatthewsCorrelation: 0.7964822109085088
Weighted FMeasure: 0.8114812540662619
Iteration time: 59.0
Weighted AreaUnderPRC: 0.8765719703967024
Mean absolute error: 0.08084378915183324
Coverage of cases: 96.96969696969697
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 521.0
Weighted Recall: 0.8121212121212121
Weighted FalsePositiveRate: 0.018787878787878787
Kappa statistic: 0.7933333333333333
Training time: 58.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 34.36179981634541
Incorrectly Classified Instances: 15.151515151515152
Correctly Classified Instances: 84.84848484848484
Weighted Precision: 0.8531706174235126
Weighted AreaUnderROC: 0.9736071829405163
Root mean squared error: 0.1740526883412853
Relative absolute error: 47.73850429143959
Root relative squared error: 60.54432208372834
Weighted TruePositiveRate: 0.8484848484848485
Weighted MatthewsCorrelation: 0.8343049019324462
Weighted FMeasure: 0.8473375702820056
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8773013508143175
Mean absolute error: 0.07890661866353701
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 582.0
Weighted Recall: 0.8484848484848485
Weighted FalsePositiveRate: 0.015151515151515152
Kappa statistic: 0.8333333333333334
Training time: 60.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 32.50688705234168
Incorrectly Classified Instances: 16.96969696969697
Correctly Classified Instances: 83.03030303030303
Weighted Precision: 0.8374766365765091
Weighted AreaUnderROC: 0.9783950617283951
Root mean squared error: 0.1723652013545988
Relative absolute error: 46.514696824730045
Root relative squared error: 59.95732881974776
Weighted TruePositiveRate: 0.8303030303030303
Weighted MatthewsCorrelation: 0.8158441454238591
Weighted FMeasure: 0.8311596763395873
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8870511685138104
Mean absolute error: 0.07688379640451296
Coverage of cases: 98.58585858585859
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 649.0
Weighted Recall: 0.8303030303030303
Weighted FalsePositiveRate: 0.016969696969696975
Kappa statistic: 0.8133333333333334
Training time: 67.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 33.13131313131325
Incorrectly Classified Instances: 17.171717171717173
Correctly Classified Instances: 82.82828282828282
Weighted Precision: 0.837160048489468
Weighted AreaUnderROC: 0.9790011223344558
Root mean squared error: 0.17153032284376107
Relative absolute error: 46.262769120655214
Root relative squared error: 59.666915876732304
Weighted TruePositiveRate: 0.8282828282828283
Weighted MatthewsCorrelation: 0.8136535269933373
Weighted FMeasure: 0.8278811554780404
Iteration time: 63.0
Weighted AreaUnderPRC: 0.8939196778732348
Mean absolute error: 0.0764673869762901
Coverage of cases: 98.18181818181819
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 712.0
Weighted Recall: 0.8282828282828283
Weighted FalsePositiveRate: 0.01717171717171717
Kappa statistic: 0.8111111111111111
Training time: 62.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.606978879706194
Incorrectly Classified Instances: 17.97979797979798
Correctly Classified Instances: 82.02020202020202
Weighted Precision: 0.8300266741650871
Weighted AreaUnderROC: 0.9772817059483725
Root mean squared error: 0.17148748117792412
Relative absolute error: 45.05420617387633
Root relative squared error: 59.65201338002424
Weighted TruePositiveRate: 0.8202020202020202
Weighted MatthewsCorrelation: 0.804965945776779
Weighted FMeasure: 0.8196707132214689
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8836970934718913
Mean absolute error: 0.07446976227087045
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 780.0
Weighted Recall: 0.8202020202020202
Weighted FalsePositiveRate: 0.017979797979797978
Kappa statistic: 0.8022222222222222
Training time: 67.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 32.15794306703405
Incorrectly Classified Instances: 15.353535353535353
Correctly Classified Instances: 84.64646464646465
Weighted Precision: 0.8536421297006463
Weighted AreaUnderROC: 0.9725813692480357
Root mean squared error: 0.17071544621135495
Relative absolute error: 45.421177211231395
Root relative squared error: 59.38346059798268
Weighted TruePositiveRate: 0.8464646464646465
Weighted MatthewsCorrelation: 0.8331976388469874
Weighted FMeasure: 0.8462185249951977
Iteration time: 176.0
Weighted AreaUnderPRC: 0.8819792143030702
Mean absolute error: 0.075076325968978
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 956.0
Weighted Recall: 0.8464646464646465
Weighted FalsePositiveRate: 0.015353535353535355
Kappa statistic: 0.8311111111111111
Training time: 175.0
		
Time end:Sun Oct 08 10.07.16 EEST 2017