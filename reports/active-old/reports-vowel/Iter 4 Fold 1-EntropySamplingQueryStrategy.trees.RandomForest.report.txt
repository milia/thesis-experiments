Sun Oct 08 10.01.31 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.31 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 42.27731864095517
Incorrectly Classified Instances: 64.44444444444444
Correctly Classified Instances: 35.55555555555556
Weighted Precision: 0.41178168236991763
Weighted AreaUnderROC: 0.7814478114478114
Root mean squared error: 0.27161406950467437
Relative absolute error: 80.66593587844615
Root relative squared error: 94.48110146002556
Weighted TruePositiveRate: 0.35555555555555557
Weighted MatthewsCorrelation: 0.3115616733746487
Weighted FMeasure: 0.36220689158032854
Iteration time: 14.0
Weighted AreaUnderPRC: 0.3891594566400855
Mean absolute error: 0.13333212541892012
Coverage of cases: 79.1919191919192
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.35555555555555557
Weighted FalsePositiveRate: 0.06444444444444444
Kappa statistic: 0.29111111111111115
Training time: 12.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 39.48576675849417
Incorrectly Classified Instances: 65.05050505050505
Correctly Classified Instances: 34.94949494949495
Weighted Precision: 0.4144830371747549
Weighted AreaUnderROC: 0.7470033670033671
Root mean squared error: 0.2774440424693093
Relative absolute error: 81.19349439678463
Root relative squared error: 96.50906071922512
Weighted TruePositiveRate: 0.34949494949494947
Weighted MatthewsCorrelation: 0.30640120085456235
Weighted FMeasure: 0.3550030643617858
Iteration time: 16.0
Weighted AreaUnderPRC: 0.38063693041855606
Mean absolute error: 0.13420412296989281
Coverage of cases: 71.51515151515152
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 30.0
Weighted Recall: 0.34949494949494947
Weighted FalsePositiveRate: 0.06505050505050505
Kappa statistic: 0.28444444444444444
Training time: 14.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 39.522497704316045
Incorrectly Classified Instances: 59.7979797979798
Correctly Classified Instances: 40.2020202020202
Weighted Precision: 0.4513753492271406
Weighted AreaUnderROC: 0.746233445566779
Root mean squared error: 0.2721263986332264
Relative absolute error: 79.51370968822116
Root relative squared error: 94.65931542539164
Weighted TruePositiveRate: 0.402020202020202
Weighted MatthewsCorrelation: 0.35980431015836095
Weighted FMeasure: 0.40775983321867315
Iteration time: 18.0
Weighted AreaUnderPRC: 0.3813546338334268
Mean absolute error: 0.13142761931937466
Coverage of cases: 71.31313131313131
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.402020202020202
Weighted FalsePositiveRate: 0.0597979797979798
Kappa statistic: 0.34222222222222226
Training time: 16.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 39.26538108356298
Incorrectly Classified Instances: 53.73737373737374
Correctly Classified Instances: 46.26262626262626
Weighted Precision: 0.4957264536218669
Weighted AreaUnderROC: 0.8023456790123458
Root mean squared error: 0.2559964264322985
Relative absolute error: 73.80739774936234
Root relative squared error: 89.04849584287722
Weighted TruePositiveRate: 0.4626262626262626
Weighted MatthewsCorrelation: 0.41945404615721943
Weighted FMeasure: 0.4626211044932547
Iteration time: 21.0
Weighted AreaUnderPRC: 0.47753110684117017
Mean absolute error: 0.12199569875927738
Coverage of cases: 78.78787878787878
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.4626262626262626
Weighted FalsePositiveRate: 0.053737373737373736
Kappa statistic: 0.40888888888888886
Training time: 19.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 39.15518824609748
Incorrectly Classified Instances: 46.666666666666664
Correctly Classified Instances: 53.333333333333336
Weighted Precision: 0.5484611577108457
Weighted AreaUnderROC: 0.8416969696969698
Root mean squared error: 0.24236469827456056
Relative absolute error: 69.59660611818238
Root relative squared error: 84.30669180637992
Weighted TruePositiveRate: 0.5333333333333333
Weighted MatthewsCorrelation: 0.490855962949627
Weighted FMeasure: 0.5311656959835391
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5405572151432703
Mean absolute error: 0.11503571259203772
Coverage of cases: 83.43434343434343
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 96.0
Weighted Recall: 0.5333333333333333
Weighted FalsePositiveRate: 0.04666666666666666
Kappa statistic: 0.48666666666666664
Training time: 25.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 36.82277318640964
Incorrectly Classified Instances: 41.41414141414141
Correctly Classified Instances: 58.58585858585859
Weighted Precision: 0.6054276957654686
Weighted AreaUnderROC: 0.8543591470258134
Root mean squared error: 0.23223171614344382
Relative absolute error: 64.94326330598828
Root relative squared error: 80.78192847372718
Weighted TruePositiveRate: 0.5858585858585859
Weighted MatthewsCorrelation: 0.5500603163855952
Weighted FMeasure: 0.5845795842266053
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6174503678907647
Mean absolute error: 0.10734423686940281
Coverage of cases: 83.03030303030303
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 124.0
Weighted Recall: 0.5858585858585859
Weighted FalsePositiveRate: 0.041414141414141424
Kappa statistic: 0.5444444444444444
Training time: 26.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 37.88797061524346
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6134115339174034
Weighted AreaUnderROC: 0.8674231200897868
Root mean squared error: 0.22881689241614825
Relative absolute error: 63.946289167949814
Root relative squared error: 79.59408018724082
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5660871625373927
Weighted FMeasure: 0.6003910062273277
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6180948140551553
Mean absolute error: 0.10569634573214913
Coverage of cases: 85.25252525252525
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 154.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.039595959595959594
Kappa statistic: 0.5644444444444444
Training time: 28.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 38.8246097337007
Incorrectly Classified Instances: 33.73737373737374
Correctly Classified Instances: 66.26262626262626
Weighted Precision: 0.6643145566983646
Weighted AreaUnderROC: 0.9088529741863076
Root mean squared error: 0.21466511010908884
Relative absolute error: 60.61293385400667
Root relative squared error: 74.67137503271103
Weighted TruePositiveRate: 0.6626262626262627
Weighted MatthewsCorrelation: 0.6286617730868189
Weighted FMeasure: 0.6605930177546723
Iteration time: 38.0
Weighted AreaUnderPRC: 0.70296868990282
Mean absolute error: 0.10018666752728439
Coverage of cases: 91.51515151515152
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 192.0
Weighted Recall: 0.6626262626262627
Weighted FalsePositiveRate: 0.033737373737373746
Kappa statistic: 0.6288888888888889
Training time: 37.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 36.36363636363651
Incorrectly Classified Instances: 34.74747474747475
Correctly Classified Instances: 65.25252525252525
Weighted Precision: 0.6521285764675021
Weighted AreaUnderROC: 0.8855982042648709
Root mean squared error: 0.2180534167729397
Relative absolute error: 61.046059083590215
Root relative squared error: 75.8499993442895
Weighted TruePositiveRate: 0.6525252525252525
Weighted MatthewsCorrelation: 0.6157596788406483
Weighted FMeasure: 0.6472506447597263
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6961424211216419
Mean absolute error: 0.10090257699767041
Coverage of cases: 87.07070707070707
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 226.0
Weighted Recall: 0.6525252525252525
Weighted FalsePositiveRate: 0.03474747474747475
Kappa statistic: 0.6177777777777778
Training time: 33.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 35.72084481175404
Incorrectly Classified Instances: 30.1010101010101
Correctly Classified Instances: 69.8989898989899
Weighted Precision: 0.7087681794653619
Weighted AreaUnderROC: 0.9100089786756453
Root mean squared error: 0.21080015518473547
Relative absolute error: 57.75353408115909
Root relative squared error: 73.32694836507858
Weighted TruePositiveRate: 0.6989898989898989
Weighted MatthewsCorrelation: 0.6715991613370904
Weighted FMeasure: 0.6983205309218369
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7161895704509779
Mean absolute error: 0.09546038691100737
Coverage of cases: 89.6969696969697
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 266.0
Weighted Recall: 0.6989898989898989
Weighted FalsePositiveRate: 0.0301010101010101
Kappa statistic: 0.6688888888888889
Training time: 39.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 34.49035812672183
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7464835545885579
Weighted AreaUnderROC: 0.9318226711560046
Root mean squared error: 0.19736090027998657
Relative absolute error: 53.376988278020335
Root relative squared error: 68.65209625407306
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.7115361880643254
Weighted FMeasure: 0.735520642757887
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7815676238647076
Mean absolute error: 0.08822642690581929
Coverage of cases: 92.72727272727273
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 312.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.026666666666666665
Kappa statistic: 0.7066666666666666
Training time: 44.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 34.63728191000924
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7452282384777238
Weighted AreaUnderROC: 0.9485454545454545
Root mean squared error: 0.18794646291488284
Relative absolute error: 50.63520987647973
Root relative squared error: 65.37727910817365
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.7135063952269902
Weighted FMeasure: 0.7372741510308721
Iteration time: 53.0
Weighted AreaUnderPRC: 0.8116860284103097
Mean absolute error: 0.08369456177930587
Coverage of cases: 95.55555555555556
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 365.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.026262626262626262
Kappa statistic: 0.711111111111111
Training time: 51.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.54545454545467
Incorrectly Classified Instances: 20.606060606060606
Correctly Classified Instances: 79.39393939393939
Weighted Precision: 0.7964503968185788
Weighted AreaUnderROC: 0.9568193041526375
Root mean squared error: 0.18297870843648717
Relative absolute error: 50.00001909603656
Root relative squared error: 63.649243017268134
Weighted TruePositiveRate: 0.793939393939394
Weighted MatthewsCorrelation: 0.7740588095116993
Weighted FMeasure: 0.7938087861308377
Iteration time: 63.0
Weighted AreaUnderPRC: 0.8392568770934303
Mean absolute error: 0.08264465966287088
Coverage of cases: 96.16161616161617
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 428.0
Weighted Recall: 0.793939393939394
Weighted FalsePositiveRate: 0.020606060606060607
Kappa statistic: 0.7733333333333333
Training time: 62.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.42516069788803
Incorrectly Classified Instances: 17.373737373737374
Correctly Classified Instances: 82.62626262626263
Weighted Precision: 0.830804901216565
Weighted AreaUnderROC: 0.9733490460157126
Root mean squared error: 0.17352669603193183
Relative absolute error: 47.561652166106576
Root relative squared error: 60.36135537350657
Weighted TruePositiveRate: 0.8262626262626263
Weighted MatthewsCorrelation: 0.8101117510821076
Weighted FMeasure: 0.8257888635480011
Iteration time: 53.0
Weighted AreaUnderPRC: 0.886037208215578
Mean absolute error: 0.0786143011010031
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 481.0
Weighted Recall: 0.8262626262626263
Weighted FalsePositiveRate: 0.017373737373737375
Kappa statistic: 0.8088888888888889
Training time: 52.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.93939393939404
Incorrectly Classified Instances: 21.01010101010101
Correctly Classified Instances: 78.98989898989899
Weighted Precision: 0.7998050778343315
Weighted AreaUnderROC: 0.9639147025813692
Root mean squared error: 0.17988108404552308
Relative absolute error: 48.83299419749201
Root relative squared error: 62.57173269204291
Weighted TruePositiveRate: 0.7898989898989899
Weighted MatthewsCorrelation: 0.7725064400707394
Weighted FMeasure: 0.791426075237196
Iteration time: 51.0
Weighted AreaUnderPRC: 0.8534260943021968
Mean absolute error: 0.08071569288841707
Coverage of cases: 96.96969696969697
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 532.0
Weighted Recall: 0.7898989898989899
Weighted FalsePositiveRate: 0.021010101010101014
Kappa statistic: 0.7688888888888888
Training time: 50.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.11294765840224
Incorrectly Classified Instances: 15.555555555555555
Correctly Classified Instances: 84.44444444444444
Weighted Precision: 0.857318670254663
Weighted AreaUnderROC: 0.98003367003367
Root mean squared error: 0.16891525204799257
Relative absolute error: 46.09532870262815
Root relative squared error: 58.75726208143839
Weighted TruePositiveRate: 0.8444444444444444
Weighted MatthewsCorrelation: 0.8332385443208554
Weighted FMeasure: 0.8456967040168648
Iteration time: 59.0
Weighted AreaUnderPRC: 0.9018932426818113
Mean absolute error: 0.07619062595475776
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 591.0
Weighted Recall: 0.8444444444444444
Weighted FalsePositiveRate: 0.015555555555555553
Kappa statistic: 0.8288888888888889
Training time: 58.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 33.79247015610664
Incorrectly Classified Instances: 16.161616161616163
Correctly Classified Instances: 83.83838383838383
Weighted Precision: 0.8481583608072087
Weighted AreaUnderROC: 0.9710886644219978
Root mean squared error: 0.17450705722508408
Relative absolute error: 47.46865400086123
Root relative squared error: 60.70237454650673
Weighted TruePositiveRate: 0.8383838383838383
Weighted MatthewsCorrelation: 0.8244269192109274
Weighted FMeasure: 0.8364620460317305
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8792134952754704
Mean absolute error: 0.07846058512539096
Coverage of cases: 96.16161616161617
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 651.0
Weighted Recall: 0.8383838383838383
Weighted FalsePositiveRate: 0.016161616161616158
Kappa statistic: 0.8222222222222222
Training time: 59.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 34.123048668503294
Incorrectly Classified Instances: 16.565656565656564
Correctly Classified Instances: 83.43434343434343
Weighted Precision: 0.845854371824018
Weighted AreaUnderROC: 0.969102132435466
Root mean squared error: 0.17404112551866888
Relative absolute error: 47.19028175173746
Root relative squared error: 60.540299949607025
Weighted TruePositiveRate: 0.8343434343434344
Weighted MatthewsCorrelation: 0.82077685198354
Weighted FMeasure: 0.8331244536103756
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8814859878500427
Mean absolute error: 0.07800046570535167
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 716.0
Weighted Recall: 0.8343434343434344
Weighted FalsePositiveRate: 0.016565656565656565
Kappa statistic: 0.8177777777777778
Training time: 64.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.515151515151548
Incorrectly Classified Instances: 11.313131313131313
Correctly Classified Instances: 88.68686868686869
Weighted Precision: 0.8904034609491895
Weighted AreaUnderROC: 0.9750460157126823
Root mean squared error: 0.16013595120779847
Relative absolute error: 42.526118677113715
Root relative squared error: 55.703377520365635
Weighted TruePositiveRate: 0.8868686868686869
Weighted MatthewsCorrelation: 0.8764252111788373
Weighted FMeasure: 0.8863259411870037
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9164592267251103
Mean absolute error: 0.07029110525142808
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 782.0
Weighted Recall: 0.8868686868686869
Weighted FalsePositiveRate: 0.011313131313131317
Kappa statistic: 0.8755555555555556
Training time: 65.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 32.96602387511484
Incorrectly Classified Instances: 15.555555555555555
Correctly Classified Instances: 84.44444444444444
Weighted Precision: 0.8492908975455432
Weighted AreaUnderROC: 0.9714814814814816
Root mean squared error: 0.1695433546776731
Relative absolute error: 45.649757187378775
Root relative squared error: 58.97574792199288
Weighted TruePositiveRate: 0.8444444444444444
Weighted MatthewsCorrelation: 0.8305381872810337
Weighted FMeasure: 0.8448815602032195
Iteration time: 70.0
Weighted AreaUnderPRC: 0.8891302908158388
Mean absolute error: 0.07545414411137036
Coverage of cases: 97.77777777777777
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 852.0
Weighted Recall: 0.8444444444444444
Weighted FalsePositiveRate: 0.015555555555555555
Kappa statistic: 0.8288888888888889
Training time: 69.0
		
Time end:Sun Oct 08 10.01.33 EEST 2017