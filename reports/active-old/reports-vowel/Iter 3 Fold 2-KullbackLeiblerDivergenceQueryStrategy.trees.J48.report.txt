Sun Oct 08 10.01.57 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.57 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.84297520661166
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4667365870107907
Weighted AreaUnderROC: 0.7424354657687992
Root mean squared error: 0.2862471917569525
Relative absolute error: 64.70841455490537
Root relative squared error: 99.57124097568365
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.40040749251603014
Weighted FMeasure: 0.4498429492731039
Iteration time: 8.0
Weighted AreaUnderPRC: 0.31221448832515514
Mean absolute error: 0.10695605711554677
Coverage of cases: 61.21212121212121
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.054949494949494956
Kappa statistic: 0.39555555555555555
Training time: 7.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 12.396694214876103
Incorrectly Classified Instances: 32.72727272727273
Correctly Classified Instances: 67.27272727272727
Weighted Precision: 0.723020895265676
Weighted AreaUnderROC: 0.8661301907968574
Root mean squared error: 0.22766545211764438
Relative absolute error: 37.656912223578765
Root relative squared error: 79.19355105461342
Weighted TruePositiveRate: 0.6727272727272727
Weighted MatthewsCorrelation: 0.6527562091269636
Weighted FMeasure: 0.6681358071904016
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5755123989930028
Mean absolute error: 0.06224283012161819
Coverage of cases: 76.96969696969697
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.6727272727272727
Weighted FalsePositiveRate: 0.03272727272727273
Kappa statistic: 0.64
Training time: 33.0
		
Time end:Sun Oct 08 10.01.57 EEST 2017