Sun Oct 08 10.01.45 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.45 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.38751147842021
Incorrectly Classified Instances: 74.74747474747475
Correctly Classified Instances: 25.252525252525253
Weighted Precision: 0.27880355401317025
Weighted AreaUnderROC: 0.7017441077441079
Root mean squared error: 0.2816756113739316
Relative absolute error: 96.12929292929222
Root relative squared error: 97.98101425882564
Weighted TruePositiveRate: 0.25252525252525254
Weighted MatthewsCorrelation: 0.18398292769354094
Weighted FMeasure: 0.24449800867430038
Iteration time: 401.0
Weighted AreaUnderPRC: 0.2020098274470678
Mean absolute error: 0.1588913932715584
Coverage of cases: 95.35353535353535
Instances selection time: 10.0
Test time: 9.0
Accumulative iteration time: 401.0
Weighted Recall: 0.25252525252525254
Weighted FalsePositiveRate: 0.07474747474747474
Kappa statistic: 0.1777777777777778
Training time: 391.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.5528007346186
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5223592920276027
Weighted AreaUnderROC: 0.8737620650953984
Root mean squared error: 0.271939982834267
Relative absolute error: 92.87272727272673
Root relative squared error: 94.59447058857096
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.4814359739864179
Weighted FMeasure: 0.5202421925197348
Iteration time: 481.0
Weighted AreaUnderPRC: 0.4750831723469045
Mean absolute error: 0.15350864012021045
Coverage of cases: 95.95959595959596
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 882.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.046060606060606066
Kappa statistic: 0.49333333333333335
Training time: 479.0
		
Time end:Sun Oct 08 10.01.46 EEST 2017