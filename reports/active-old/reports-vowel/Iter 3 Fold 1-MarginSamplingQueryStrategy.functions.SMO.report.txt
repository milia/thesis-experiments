Sun Oct 08 10.05.40 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.05.40 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 77.37373737373737
Correctly Classified Instances: 22.626262626262626
Weighted Precision: 0.25253623067093184
Weighted AreaUnderROC: 0.6871380471380472
Root mean squared error: 0.28245199349024225
Relative absolute error: 96.3797979797975
Root relative squared error: 98.25107919926361
Weighted TruePositiveRate: 0.22626262626262628
Weighted MatthewsCorrelation: 0.1571987809663048
Weighted FMeasure: 0.2246459864620874
Iteration time: 395.0
Weighted AreaUnderPRC: 0.21123042307214476
Mean absolute error: 0.1593054512062779
Coverage of cases: 91.51515151515152
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 395.0
Weighted Recall: 0.22626262626262628
Weighted FalsePositiveRate: 0.07737373737373737
Kappa statistic: 0.1488888888888889
Training time: 390.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.02020202020165
Incorrectly Classified Instances: 74.14141414141415
Correctly Classified Instances: 25.858585858585858
Weighted Precision: 0.27480268550335296
Weighted AreaUnderROC: 0.7139349046015712
Root mean squared error: 0.2808071559266269
Relative absolute error: 95.8141414141408
Root relative squared error: 97.6789215602407
Weighted TruePositiveRate: 0.2585858585858586
Weighted MatthewsCorrelation: 0.18500594800526612
Weighted FMeasure: 0.23945752236406143
Iteration time: 402.0
Weighted AreaUnderPRC: 0.22445570591949926
Mean absolute error: 0.1583704816762668
Coverage of cases: 95.35353535353535
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 797.0
Weighted Recall: 0.2585858585858586
Weighted FalsePositiveRate: 0.07414141414141413
Kappa statistic: 0.18444444444444444
Training time: 398.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 71.31313131313131
Correctly Classified Instances: 28.68686868686869
Weighted Precision: 0.2826233345535848
Weighted AreaUnderROC: 0.7476251402918069
Root mean squared error: 0.279045819762589
Relative absolute error: 95.22424242424187
Root relative squared error: 97.0662398198465
Weighted TruePositiveRate: 0.2868686868686869
Weighted MatthewsCorrelation: 0.20758567228296132
Weighted FMeasure: 0.2621921686989136
Iteration time: 399.0
Weighted AreaUnderPRC: 0.25289666622867796
Mean absolute error: 0.15739544202354128
Coverage of cases: 97.97979797979798
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 1196.0
Weighted Recall: 0.2868686868686869
Weighted FalsePositiveRate: 0.07131313131313133
Kappa statistic: 0.21555555555555558
Training time: 394.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 69.6969696969697
Correctly Classified Instances: 30.303030303030305
Weighted Precision: 0.3252157780701729
Weighted AreaUnderROC: 0.7804377104377106
Root mean squared error: 0.2769674556203395
Relative absolute error: 94.53333333333279
Root relative squared error: 96.3432797252061
Weighted TruePositiveRate: 0.30303030303030304
Weighted MatthewsCorrelation: 0.23895571491045017
Weighted FMeasure: 0.2956981562045851
Iteration time: 401.0
Weighted AreaUnderPRC: 0.2623473702095633
Mean absolute error: 0.1562534435261709
Coverage of cases: 97.77777777777777
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 1597.0
Weighted Recall: 0.30303030303030304
Weighted FalsePositiveRate: 0.06969696969696972
Kappa statistic: 0.23333333333333334
Training time: 397.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 66.26262626262626
Correctly Classified Instances: 33.73737373737374
Weighted Precision: 0.3490377924169203
Weighted AreaUnderROC: 0.8037620650953984
Root mean squared error: 0.2757948637696869
Relative absolute error: 94.14141414141352
Root relative squared error: 95.93539301368656
Weighted TruePositiveRate: 0.3373737373737374
Weighted MatthewsCorrelation: 0.26959671487477566
Weighted FMeasure: 0.3179893608784315
Iteration time: 406.0
Weighted AreaUnderPRC: 0.29923488108848806
Mean absolute error: 0.15560564320894896
Coverage of cases: 97.77777777777777
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2003.0
Weighted Recall: 0.3373737373737374
Weighted FalsePositiveRate: 0.06626262626262626
Kappa statistic: 0.27111111111111114
Training time: 402.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.14876033057813
Incorrectly Classified Instances: 62.42424242424242
Correctly Classified Instances: 37.57575757575758
Weighted Precision: 0.39807050050039094
Weighted AreaUnderROC: 0.8136228956228955
Root mean squared error: 0.2753218266590313
Relative absolute error: 93.9797979797974
Root relative squared error: 95.77084679806632
Weighted TruePositiveRate: 0.37575757575757573
Weighted MatthewsCorrelation: 0.3179182113486191
Weighted FMeasure: 0.3580848705034013
Iteration time: 413.0
Weighted AreaUnderPRC: 0.329171310056804
Mean absolute error: 0.15533850905751737
Coverage of cases: 97.77777777777777
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2416.0
Weighted Recall: 0.37575757575757573
Weighted FalsePositiveRate: 0.06242424242424243
Kappa statistic: 0.3133333333333333
Training time: 409.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.14876033057813
Incorrectly Classified Instances: 58.98989898989899
Correctly Classified Instances: 41.01010101010101
Weighted Precision: 0.45150791452311306
Weighted AreaUnderROC: 0.8217934904601569
Root mean squared error: 0.274608642855097
Relative absolute error: 93.73737373737312
Root relative squared error: 95.52276542488102
Weighted TruePositiveRate: 0.4101010101010101
Weighted MatthewsCorrelation: 0.3584511162854877
Weighted FMeasure: 0.39251457267085266
Iteration time: 424.0
Weighted AreaUnderPRC: 0.3434528875043525
Mean absolute error: 0.15493780783036978
Coverage of cases: 97.77777777777777
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 2840.0
Weighted Recall: 0.4101010101010101
Weighted FalsePositiveRate: 0.05898989898989898
Kappa statistic: 0.35111111111111104
Training time: 421.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.07529843893442
Incorrectly Classified Instances: 56.16161616161616
Correctly Classified Instances: 43.83838383838384
Weighted Precision: 0.5059233892677166
Weighted AreaUnderROC: 0.8389652076318743
Root mean squared error: 0.273864784203288
Relative absolute error: 93.48282828282771
Root relative squared error: 95.26401378921778
Weighted TruePositiveRate: 0.4383838383838384
Weighted MatthewsCorrelation: 0.3962196365527843
Weighted FMeasure: 0.41797434057028376
Iteration time: 427.0
Weighted AreaUnderPRC: 0.37990351928956223
Mean absolute error: 0.15451707154186498
Coverage of cases: 97.57575757575758
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 3267.0
Weighted Recall: 0.4383838383838384
Weighted FalsePositiveRate: 0.05616161616161617
Kappa statistic: 0.38222222222222224
Training time: 423.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 55.35353535353536
Correctly Classified Instances: 44.64646464646464
Weighted Precision: 0.5138467678414973
Weighted AreaUnderROC: 0.850347923681257
Root mean squared error: 0.2731504633309956
Relative absolute error: 93.28484848484791
Root relative squared error: 95.01553688618714
Weighted TruePositiveRate: 0.44646464646464645
Weighted MatthewsCorrelation: 0.4063048972175379
Weighted FMeasure: 0.4308036009117493
Iteration time: 429.0
Weighted AreaUnderPRC: 0.39722130141908923
Mean absolute error: 0.15418983220636118
Coverage of cases: 99.1919191919192
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 3696.0
Weighted Recall: 0.44646464646464645
Weighted FalsePositiveRate: 0.055353535353535356
Kappa statistic: 0.3911111111111111
Training time: 425.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.3140495867765
Incorrectly Classified Instances: 52.72727272727273
Correctly Classified Instances: 47.27272727272727
Weighted Precision: 0.5362438315702495
Weighted AreaUnderROC: 0.8503120089786756
Root mean squared error: 0.2729378934605436
Relative absolute error: 93.20808080808024
Root relative squared error: 94.94159434140606
Weighted TruePositiveRate: 0.4727272727272727
Weighted MatthewsCorrelation: 0.43711918893417184
Weighted FMeasure: 0.4628773593173545
Iteration time: 535.0
Weighted AreaUnderPRC: 0.41425229803760577
Mean absolute error: 0.15406294348443114
Coverage of cases: 99.1919191919192
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4231.0
Weighted Recall: 0.4727272727272727
Weighted FalsePositiveRate: 0.05272727272727273
Kappa statistic: 0.42000000000000004
Training time: 532.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 53.13131313131313
Correctly Classified Instances: 46.86868686868687
Weighted Precision: 0.5378752282253725
Weighted AreaUnderROC: 0.8512323232323233
Root mean squared error: 0.2728529081465347
Relative absolute error: 93.16767676767617
Root relative squared error: 94.91203215381344
Weighted TruePositiveRate: 0.4686868686868687
Weighted MatthewsCorrelation: 0.4317168946972696
Weighted FMeasure: 0.4527795364456318
Iteration time: 488.0
Weighted AreaUnderPRC: 0.4170628968146488
Mean absolute error: 0.15399615994657317
Coverage of cases: 99.1919191919192
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4719.0
Weighted Recall: 0.4686868686868687
Weighted FalsePositiveRate: 0.053131313131313126
Kappa statistic: 0.41555555555555557
Training time: 485.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.47933884297485
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5908229604127369
Weighted AreaUnderROC: 0.8600538720538721
Root mean squared error: 0.2725601513725618
Relative absolute error: 93.07878787878734
Root relative squared error: 94.81019655113172
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.4679447570674801
Weighted FMeasure: 0.47352349386475995
Iteration time: 495.0
Weighted AreaUnderPRC: 0.4453835556099937
Mean absolute error: 0.15384923616328586
Coverage of cases: 99.1919191919192
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 5214.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.04969696969696969
Kappa statistic: 0.4533333333333333
Training time: 492.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.79155188246064
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.6008029449533708
Weighted AreaUnderROC: 0.8642693602693602
Root mean squared error: 0.2718814833984803
Relative absolute error: 92.86464646464589
Root relative squared error: 94.57412152808966
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.48547985196081045
Weighted FMeasure: 0.49631887128228325
Iteration time: 507.0
Weighted AreaUnderPRC: 0.453712635147202
Mean absolute error: 0.15349528341263882
Coverage of cases: 98.78787878787878
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5721.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.04848484848484849
Kappa statistic: 0.4666666666666666
Training time: 505.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.64462809917322
Incorrectly Classified Instances: 47.07070707070707
Correctly Classified Instances: 52.92929292929293
Weighted Precision: 0.6098017457319064
Weighted AreaUnderROC: 0.8762424242424243
Root mean squared error: 0.2712053704876691
Relative absolute error: 92.63030303030246
Root relative squared error: 94.33893528519259
Weighted TruePositiveRate: 0.5292929292929293
Weighted MatthewsCorrelation: 0.49742830439595054
Weighted FMeasure: 0.5074984538512917
Iteration time: 513.0
Weighted AreaUnderPRC: 0.4693504198056281
Mean absolute error: 0.1531079388930629
Coverage of cases: 99.5959595959596
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6234.0
Weighted Recall: 0.5292929292929293
Weighted FalsePositiveRate: 0.04707070707070706
Kappa statistic: 0.48222222222222216
Training time: 511.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.58953168044046
Incorrectly Classified Instances: 46.86868686868687
Correctly Classified Instances: 53.13131313131313
Weighted Precision: 0.5832959939304948
Weighted AreaUnderROC: 0.882962962962963
Root mean squared error: 0.2708839394645829
Relative absolute error: 92.5252525252519
Root relative squared error: 94.22712532939765
Weighted TruePositiveRate: 0.5313131313131313
Weighted MatthewsCorrelation: 0.48992297118461126
Weighted FMeasure: 0.5094538444937948
Iteration time: 525.0
Weighted AreaUnderPRC: 0.4623549645792243
Mean absolute error: 0.15293430169463224
Coverage of cases: 99.79797979797979
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6759.0
Weighted Recall: 0.5313131313131313
Weighted FalsePositiveRate: 0.04686868686868687
Kappa statistic: 0.4844444444444444
Training time: 523.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.62626262626229
Incorrectly Classified Instances: 48.282828282828284
Correctly Classified Instances: 51.717171717171716
Weighted Precision: 0.5561996934357495
Weighted AreaUnderROC: 0.8885342312008978
Root mean squared error: 0.2706285388206107
Relative absolute error: 92.44444444444389
Root relative squared error: 94.13828407680681
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.4680099376453522
Weighted FMeasure: 0.4880993255351787
Iteration time: 528.0
Weighted AreaUnderPRC: 0.45814725362850445
Mean absolute error: 0.1528007346189165
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 7287.0
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282829
Kappa statistic: 0.46888888888888886
Training time: 526.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.51606978879673
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5784564255009143
Weighted AreaUnderROC: 0.8948843995510662
Root mean squared error: 0.26997829869420537
Relative absolute error: 92.21818181818114
Root relative squared error: 93.91209769600437
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.503873947791948
Weighted FMeasure: 0.5378398940608589
Iteration time: 535.0
Weighted AreaUnderPRC: 0.476122270137129
Mean absolute error: 0.15242674680691196
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7822.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262627
Kappa statistic: 0.4911111111111111
Training time: 534.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.35078053259834
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5807530249723926
Weighted AreaUnderROC: 0.905324354657688
Root mean squared error: 0.26969435274507914
Relative absolute error: 92.11717171717105
Root relative squared error: 93.81332694352648
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5031380200491852
Weighted FMeasure: 0.5319630392558825
Iteration time: 549.0
Weighted AreaUnderPRC: 0.48480932214588485
Mean absolute error: 0.15225978796226716
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8371.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.045858585858585856
Kappa statistic: 0.4955555555555556
Training time: 548.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.13039485766718
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.600751736212105
Weighted AreaUnderROC: 0.9201234567901235
Root mean squared error: 0.2684945209940202
Relative absolute error: 91.71313131313059
Root relative squared error: 93.39596481786971
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.5497761273365872
Weighted FMeasure: 0.5852334110570389
Iteration time: 555.0
Weighted AreaUnderPRC: 0.530247948252133
Mean absolute error: 0.1515919525836879
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8926.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.041212121212121214
Kappa statistic: 0.5466666666666666
Training time: 554.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.03856749311254
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6164941436321462
Weighted AreaUnderROC: 0.9246621773288439
Root mean squared error: 0.26805775179657526
Relative absolute error: 91.5636363636357
Root relative squared error: 93.24403441553942
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5726354115053689
Weighted FMeasure: 0.6087035635713968
Iteration time: 562.0
Weighted AreaUnderPRC: 0.5478311537043943
Mean absolute error: 0.15134485349361368
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 9488.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.03898989898989899
Kappa statistic: 0.5711111111111111
Training time: 561.0
		
Time end:Sun Oct 08 10.05.51 EEST 2017