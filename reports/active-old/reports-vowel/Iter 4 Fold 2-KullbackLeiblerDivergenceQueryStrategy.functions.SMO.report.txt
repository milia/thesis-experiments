Sun Oct 08 10.01.51 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.51 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.11202938475627
Incorrectly Classified Instances: 74.94949494949495
Correctly Classified Instances: 25.050505050505052
Weighted Precision: 0.23374352288825206
Weighted AreaUnderROC: 0.7245993265993265
Root mean squared error: 0.28060709372962306
Relative absolute error: 95.77373737373685
Root relative squared error: 97.60932981645546
Weighted TruePositiveRate: 0.2505050505050505
Weighted MatthewsCorrelation: 0.16617770409362673
Weighted FMeasure: 0.23158116074224896
Iteration time: 396.0
Weighted AreaUnderPRC: 0.22584544388554864
Mean absolute error: 0.15830369813840903
Coverage of cases: 96.56565656565657
Instances selection time: 11.0
Test time: 8.0
Accumulative iteration time: 396.0
Weighted Recall: 0.2505050505050505
Weighted FalsePositiveRate: 0.07494949494949495
Kappa statistic: 0.17555555555555558
Training time: 385.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.18549127639997
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5317016867887806
Weighted AreaUnderROC: 0.8693400673400674
Root mean squared error: 0.27057334600633587
Relative absolute error: 92.39595959595907
Root relative squared error: 94.11908522641266
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.4896868210946082
Weighted FMeasure: 0.5333678783360536
Iteration time: 484.0
Weighted AreaUnderPRC: 0.4600972658249166
Mean absolute error: 0.15272059437348703
Coverage of cases: 99.5959595959596
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 880.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 482.0
		
Time end:Sun Oct 08 10.01.52 EEST 2017