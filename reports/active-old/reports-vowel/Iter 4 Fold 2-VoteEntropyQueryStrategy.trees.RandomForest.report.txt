Sun Oct 08 10.10.22 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 10.10.22 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 46.4279155188247
Incorrectly Classified Instances: 57.57575757575758
Correctly Classified Instances: 42.42424242424242
Weighted Precision: 0.4425982611296478
Weighted AreaUnderROC: 0.8109809203142537
Root mean squared error: 0.2577370298165138
Relative absolute error: 77.35997691125895
Root relative squared error: 89.65396567456015
Weighted TruePositiveRate: 0.42424242424242425
Weighted MatthewsCorrelation: 0.3730519153843316
Weighted FMeasure: 0.42583701965603
Iteration time: 14.0
Weighted AreaUnderPRC: 0.46747555679896946
Mean absolute error: 0.1278677304318338
Coverage of cases: 84.64646464646465
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 14.0
Weighted Recall: 0.42424242424242425
Weighted FalsePositiveRate: 0.057575757575757586
Kappa statistic: 0.3666666666666667
Training time: 13.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.97612488521589
Incorrectly Classified Instances: 30.303030303030305
Correctly Classified Instances: 69.6969696969697
Weighted Precision: 0.7496469283412278
Weighted AreaUnderROC: 0.9328260381593715
Root mean squared error: 0.20408785441492774
Relative absolute error: 54.54652464445939
Root relative squared error: 70.99207090008225
Weighted TruePositiveRate: 0.696969696969697
Weighted MatthewsCorrelation: 0.6756942661701021
Weighted FMeasure: 0.6815397704917423
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7671712646723909
Mean absolute error: 0.09015954486687561
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 72.0
Weighted Recall: 0.696969696969697
Weighted FalsePositiveRate: 0.0303030303030303
Kappa statistic: 0.6666666666666667
Training time: 57.0
		
Time end:Sun Oct 08 10.10.22 EEST 2017