Sun Oct 08 10.07.23 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.07.23 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 42.27731864095517
Incorrectly Classified Instances: 64.44444444444444
Correctly Classified Instances: 35.55555555555556
Weighted Precision: 0.41178168236991763
Weighted AreaUnderROC: 0.7814478114478114
Root mean squared error: 0.27161406950467437
Relative absolute error: 80.66593587844615
Root relative squared error: 94.48110146002556
Weighted TruePositiveRate: 0.35555555555555557
Weighted MatthewsCorrelation: 0.3115616733746487
Weighted FMeasure: 0.36220689158032854
Iteration time: 15.0
Weighted AreaUnderPRC: 0.3891594566400855
Mean absolute error: 0.13333212541892012
Coverage of cases: 79.1919191919192
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.35555555555555557
Weighted FalsePositiveRate: 0.06444444444444444
Kappa statistic: 0.29111111111111115
Training time: 13.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 39.94490358126733
Incorrectly Classified Instances: 67.07070707070707
Correctly Classified Instances: 32.92929292929293
Weighted Precision: 0.4039184420956826
Weighted AreaUnderROC: 0.741813692480359
Root mean squared error: 0.2808979115260024
Relative absolute error: 81.78590479057402
Root relative squared error: 97.71049094472913
Weighted TruePositiveRate: 0.3292929292929293
Weighted MatthewsCorrelation: 0.2881556401240454
Weighted FMeasure: 0.3384895113582993
Iteration time: 16.0
Weighted AreaUnderPRC: 0.3464690202994511
Mean absolute error: 0.135183313703429
Coverage of cases: 75.75757575757575
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 31.0
Weighted Recall: 0.3292929292929293
Weighted FalsePositiveRate: 0.06707070707070706
Kappa statistic: 0.26222222222222225
Training time: 14.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 39.88980716253463
Incorrectly Classified Instances: 59.5959595959596
Correctly Classified Instances: 40.4040404040404
Weighted Precision: 0.4990625968396932
Weighted AreaUnderROC: 0.7853670033670035
Root mean squared error: 0.2640103778717451
Relative absolute error: 76.17601042238711
Root relative squared error: 91.8361531996072
Weighted TruePositiveRate: 0.40404040404040403
Weighted MatthewsCorrelation: 0.3758512769842696
Weighted FMeasure: 0.41314053858688804
Iteration time: 18.0
Weighted AreaUnderPRC: 0.45369826967719806
Mean absolute error: 0.12591076102873983
Coverage of cases: 77.77777777777777
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 49.0
Weighted Recall: 0.40404040404040403
Weighted FalsePositiveRate: 0.0595959595959596
Kappa statistic: 0.3444444444444445
Training time: 17.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 42.16712580348958
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.4787854108690513
Weighted AreaUnderROC: 0.7939012345679014
Root mean squared error: 0.25967652206698033
Relative absolute error: 75.57944361009777
Root relative squared error: 90.32861910628935
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.3932293296035388
Weighted FMeasure: 0.4387909300700229
Iteration time: 20.0
Weighted AreaUnderPRC: 0.4688051436994837
Mean absolute error: 0.12492470018198058
Coverage of cases: 80.60606060606061
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 69.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.05656565656565657
Kappa statistic: 0.37777777777777777
Training time: 19.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 40.51423324150607
Incorrectly Classified Instances: 50.90909090909091
Correctly Classified Instances: 49.09090909090909
Weighted Precision: 0.5753554408989602
Weighted AreaUnderROC: 0.8330662177328844
Root mean squared error: 0.2491528492233345
Relative absolute error: 71.43999604664101
Root relative squared error: 86.66795379728735
Weighted TruePositiveRate: 0.4909090909090909
Weighted MatthewsCorrelation: 0.4663140808797506
Weighted FMeasure: 0.4935780803784718
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5381816108650208
Mean absolute error: 0.11808263809362228
Coverage of cases: 84.04040404040404
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 94.0
Weighted Recall: 0.4909090909090909
Weighted FalsePositiveRate: 0.05090909090909091
Kappa statistic: 0.44000000000000006
Training time: 24.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 38.80624426078988
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5664793767485155
Weighted AreaUnderROC: 0.8539640852974185
Root mean squared error: 0.2374690141531328
Relative absolute error: 67.05232853424071
Root relative squared error: 82.60372542825239
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.5039755538798115
Weighted FMeasure: 0.5451230362317554
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5825015093071331
Mean absolute error: 0.11083029509791924
Coverage of cases: 85.45454545454545
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 122.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.04606060606060606
Kappa statistic: 0.49333333333333335
Training time: 26.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 40.587695133149836
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.6131713788779629
Weighted AreaUnderROC: 0.8653916947250281
Root mean squared error: 0.23283963952950704
Relative absolute error: 66.79584543822759
Root relative squared error: 80.99339495343976
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.5562218594022593
Weighted FMeasure: 0.5923289571341048
Iteration time: 31.0
Weighted AreaUnderPRC: 0.6203147250752721
Mean absolute error: 0.11040635609624466
Coverage of cases: 86.86868686868686
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 153.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.041212121212121214
Kappa statistic: 0.5466666666666666
Training time: 29.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 39.853076216712694
Incorrectly Classified Instances: 33.13131313131313
Correctly Classified Instances: 66.86868686868686
Weighted Precision: 0.6909579828576473
Weighted AreaUnderROC: 0.890641975308642
Root mean squared error: 0.22140064617792005
Relative absolute error: 63.614136572248434
Root relative squared error: 77.01433490908057
Weighted TruePositiveRate: 0.6686868686868687
Weighted MatthewsCorrelation: 0.643147880352768
Weighted FMeasure: 0.6706628324647956
Iteration time: 35.0
Weighted AreaUnderPRC: 0.6778601445888963
Mean absolute error: 0.10514733317727082
Coverage of cases: 88.68686868686869
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 188.0
Weighted Recall: 0.6686868686868687
Weighted FalsePositiveRate: 0.033131313131313136
Kappa statistic: 0.6355555555555555
Training time: 33.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 36.40036730945829
Incorrectly Classified Instances: 35.95959595959596
Correctly Classified Instances: 64.04040404040404
Weighted Precision: 0.6540976986308676
Weighted AreaUnderROC: 0.9004736251402917
Root mean squared error: 0.21504589020929193
Relative absolute error: 59.72251591990876
Root relative squared error: 74.803829597185
Weighted TruePositiveRate: 0.6404040404040404
Weighted MatthewsCorrelation: 0.6080398103719711
Weighted FMeasure: 0.6385709954333887
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7042853201316159
Mean absolute error: 0.09871490234695725
Coverage of cases: 89.6969696969697
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 223.0
Weighted Recall: 0.6404040404040404
Weighted FalsePositiveRate: 0.035959595959595955
Kappa statistic: 0.6044444444444445
Training time: 34.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 35.35353535353545
Incorrectly Classified Instances: 32.121212121212125
Correctly Classified Instances: 67.87878787878788
Weighted Precision: 0.691023780300411
Weighted AreaUnderROC: 0.9028911335578002
Root mean squared error: 0.21032006734793865
Relative absolute error: 57.791485555238715
Root relative squared error: 73.15994955054425
Weighted TruePositiveRate: 0.6787878787878788
Weighted MatthewsCorrelation: 0.6507564224448226
Weighted FMeasure: 0.6795341423756668
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7233952974886981
Mean absolute error: 0.09552311662022989
Coverage of cases: 87.67676767676768
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 262.0
Weighted Recall: 0.6787878787878788
Weighted FalsePositiveRate: 0.03212121212121212
Kappa statistic: 0.6466666666666667
Training time: 37.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 34.637281910009236
Incorrectly Classified Instances: 24.646464646464647
Correctly Classified Instances: 75.35353535353535
Weighted Precision: 0.7670042328141663
Weighted AreaUnderROC: 0.9375555555555556
Root mean squared error: 0.1962726454075105
Relative absolute error: 53.857034301459855
Root relative squared error: 68.27354620617501
Weighted TruePositiveRate: 0.7535353535353535
Weighted MatthewsCorrelation: 0.7330339511875722
Weighted FMeasure: 0.7533480425527523
Iteration time: 114.0
Weighted AreaUnderPRC: 0.7981578260428613
Mean absolute error: 0.08901989140737224
Coverage of cases: 94.54545454545455
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 376.0
Weighted Recall: 0.7535353535353535
Weighted FalsePositiveRate: 0.024646464646464653
Kappa statistic: 0.7288888888888888
Training time: 111.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 33.95775941230498
Incorrectly Classified Instances: 19.393939393939394
Correctly Classified Instances: 80.60606060606061
Weighted Precision: 0.812310512800058
Weighted AreaUnderROC: 0.9557822671156004
Root mean squared error: 0.18587017839150938
Relative absolute error: 50.59010745339677
Root relative squared error: 64.65504241008776
Weighted TruePositiveRate: 0.806060606060606
Weighted MatthewsCorrelation: 0.7887971372774603
Weighted FMeasure: 0.8065564373361485
Iteration time: 70.0
Weighted AreaUnderPRC: 0.8409995491314752
Mean absolute error: 0.08362001231966462
Coverage of cases: 95.75757575757575
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 446.0
Weighted Recall: 0.806060606060606
Weighted FalsePositiveRate: 0.019393939393939394
Kappa statistic: 0.7866666666666666
Training time: 69.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 35.29843893480267
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.8073826797846292
Weighted AreaUnderROC: 0.9606599326599328
Root mean squared error: 0.18401220980168612
Relative absolute error: 50.71118209392623
Root relative squared error: 64.00874702794958
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.787576563626262
Weighted FMeasure: 0.805149622062455
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8373238863870217
Mean absolute error: 0.0838201356924406
Coverage of cases: 96.96969696969697
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 494.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.01919191919191919
Kappa statistic: 0.7888888888888889
Training time: 47.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 35.00459136822779
Incorrectly Classified Instances: 18.383838383838384
Correctly Classified Instances: 81.61616161616162
Weighted Precision: 0.8194710430742761
Weighted AreaUnderROC: 0.9728193041526374
Root mean squared error: 0.17892226259998736
Relative absolute error: 49.62415454415707
Root relative squared error: 62.23820613193904
Weighted TruePositiveRate: 0.8161616161616162
Weighted MatthewsCorrelation: 0.7983203336892846
Weighted FMeasure: 0.814840413382336
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8666106212902793
Mean absolute error: 0.08202339594075601
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 549.0
Weighted Recall: 0.8161616161616162
Weighted FalsePositiveRate: 0.018383838383838384
Kappa statistic: 0.7977777777777778
Training time: 54.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 34.54545454545461
Incorrectly Classified Instances: 16.565656565656564
Correctly Classified Instances: 83.43434343434343
Weighted Precision: 0.8413146537011973
Weighted AreaUnderROC: 0.9675420875420875
Root mean squared error: 0.17942765585347525
Relative absolute error: 49.41009884781982
Root relative squared error: 62.41400744940054
Weighted TruePositiveRate: 0.8343434343434344
Weighted MatthewsCorrelation: 0.819944990878542
Weighted FMeasure: 0.8344156814133645
Iteration time: 58.0
Weighted AreaUnderPRC: 0.871354273763525
Mean absolute error: 0.08166958487242997
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 607.0
Weighted Recall: 0.8343434343434344
Weighted FalsePositiveRate: 0.016565656565656565
Kappa statistic: 0.8177777777777778
Training time: 57.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.296602387511584
Incorrectly Classified Instances: 18.78787878787879
Correctly Classified Instances: 81.21212121212122
Weighted Precision: 0.8259586379774666
Weighted AreaUnderROC: 0.9733692480359147
Root mean squared error: 0.1806296324073597
Relative absolute error: 48.56293983919025
Root relative squared error: 62.83211564588399
Weighted TruePositiveRate: 0.8121212121212121
Weighted MatthewsCorrelation: 0.798017913195618
Weighted FMeasure: 0.8133926224050876
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8604109531546461
Mean absolute error: 0.08026932204824887
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 663.0
Weighted Recall: 0.8121212121212121
Weighted FalsePositiveRate: 0.01878787878787879
Kappa statistic: 0.7933333333333333
Training time: 55.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 33.149678604224135
Incorrectly Classified Instances: 15.555555555555555
Correctly Classified Instances: 84.44444444444444
Weighted Precision: 0.8551388003972875
Weighted AreaUnderROC: 0.9805185185185186
Root mean squared error: 0.16842026892227802
Relative absolute error: 45.64238365550355
Root relative squared error: 58.58508193257156
Weighted TruePositiveRate: 0.8444444444444444
Weighted MatthewsCorrelation: 0.83246610387306
Weighted FMeasure: 0.8452980847340628
Iteration time: 64.0
Weighted AreaUnderPRC: 0.9053042628427583
Mean absolute error: 0.07544195645537825
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 727.0
Weighted Recall: 0.8444444444444444
Weighted FalsePositiveRate: 0.015555555555555555
Kappa statistic: 0.8288888888888889
Training time: 63.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 32.04775022956852
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8253124751737049
Weighted AreaUnderROC: 0.9792076318742985
Root mean squared error: 0.17069657855589762
Relative absolute error: 45.49678020711789
Root relative squared error: 59.37689747379392
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.8027035171047485
Weighted FMeasure: 0.8195832063292976
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8819949471281178
Mean absolute error: 0.07520128959854244
Coverage of cases: 99.39393939393939
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 794.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.01818181818181818
Kappa statistic: 0.8
Training time: 66.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 33.05785123966952
Incorrectly Classified Instances: 13.535353535353535
Correctly Classified Instances: 86.46464646464646
Weighted Precision: 0.8744027199215878
Weighted AreaUnderROC: 0.9774276094276095
Root mean squared error: 0.16729930025040196
Relative absolute error: 45.585359946908454
Root relative squared error: 58.19515237180112
Weighted TruePositiveRate: 0.8646464646464647
Weighted MatthewsCorrelation: 0.8546554270343765
Weighted FMeasure: 0.8661194683852222
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9091307521950591
Mean absolute error: 0.07534770239158471
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 861.0
Weighted Recall: 0.8646464646464647
Weighted FalsePositiveRate: 0.013535353535353538
Kappa statistic: 0.8511111111111112
Training time: 66.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.27640036730953
Incorrectly Classified Instances: 15.555555555555555
Correctly Classified Instances: 84.44444444444444
Weighted Precision: 0.8490091540912746
Weighted AreaUnderROC: 0.9729539842873175
Root mean squared error: 0.1699767302446031
Relative absolute error: 44.66832308403237
Root relative squared error: 59.1264978481071
Weighted TruePositiveRate: 0.8444444444444444
Weighted MatthewsCorrelation: 0.830482832700637
Weighted FMeasure: 0.8449753898517854
Iteration time: 71.0
Weighted AreaUnderPRC: 0.8866529163572223
Mean absolute error: 0.07383193898187217
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 932.0
Weighted Recall: 0.8444444444444444
Weighted FalsePositiveRate: 0.015555555555555555
Kappa statistic: 0.8288888888888889
Training time: 70.0
		
Time end:Sun Oct 08 10.07.25 EEST 2017