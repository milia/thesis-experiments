Sun Oct 08 10.08.26 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.08.26 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 81.9467401285579
Incorrectly Classified Instances: 74.94949494949495
Correctly Classified Instances: 25.050505050505052
Weighted Precision: 0.2766065675431277
Weighted AreaUnderROC: 0.7203164983164982
Root mean squared error: 0.28168143088862624
Relative absolute error: 96.12525252525197
Root relative squared error: 97.98303858016995
Weighted TruePositiveRate: 0.2505050505050505
Weighted MatthewsCorrelation: 0.17827058687641711
Weighted FMeasure: 0.23170162990202975
Iteration time: 401.0
Weighted AreaUnderPRC: 0.23108673285652154
Mean absolute error: 0.15888471491777287
Coverage of cases: 90.9090909090909
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 401.0
Weighted Recall: 0.2505050505050505
Weighted FalsePositiveRate: 0.07494949494949495
Kappa statistic: 0.17555555555555558
Training time: 393.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 72.92929292929293
Correctly Classified Instances: 27.07070707070707
Weighted Precision: 0.30738089299425564
Weighted AreaUnderROC: 0.7390998877665544
Root mean squared error: 0.2806845401216465
Relative absolute error: 95.77777777777716
Root relative squared error: 97.63626958594466
Weighted TruePositiveRate: 0.27070707070707073
Weighted MatthewsCorrelation: 0.20674795909933052
Weighted FMeasure: 0.2575205523965989
Iteration time: 386.0
Weighted AreaUnderPRC: 0.23806402541464958
Mean absolute error: 0.15831037649219468
Coverage of cases: 92.72727272727273
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 787.0
Weighted Recall: 0.27070707070707073
Weighted FalsePositiveRate: 0.07292929292929293
Kappa statistic: 0.1977777777777778
Training time: 385.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.05693296602347
Incorrectly Classified Instances: 72.12121212121212
Correctly Classified Instances: 27.87878787878788
Weighted Precision: 0.3466787873425351
Weighted AreaUnderROC: 0.7841548821548822
Root mean squared error: 0.27730701329924906
Relative absolute error: 94.63030303030257
Root relative squared error: 96.46139504806509
Weighted TruePositiveRate: 0.2787878787878788
Weighted MatthewsCorrelation: 0.22564127407811388
Weighted FMeasure: 0.2736513410677848
Iteration time: 389.0
Weighted AreaUnderPRC: 0.27101623219838517
Mean absolute error: 0.15641372401703005
Coverage of cases: 97.17171717171718
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 1176.0
Weighted Recall: 0.2787878787878788
Weighted FalsePositiveRate: 0.07212121212121211
Kappa statistic: 0.2066666666666667
Training time: 387.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 68.48484848484848
Correctly Classified Instances: 31.515151515151516
Weighted Precision: 0.35429970498936014
Weighted AreaUnderROC: 0.8077553310886644
Root mean squared error: 0.2760069930453083
Relative absolute error: 94.17777777777718
Root relative squared error: 96.00918229731673
Weighted TruePositiveRate: 0.3151515151515151
Weighted MatthewsCorrelation: 0.2560054982310561
Weighted FMeasure: 0.3054614331260521
Iteration time: 394.0
Weighted AreaUnderPRC: 0.3026392126108831
Mean absolute error: 0.15566574839302114
Coverage of cases: 96.96969696969697
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 1570.0
Weighted Recall: 0.3151515151515151
Weighted FalsePositiveRate: 0.06848484848484848
Kappa statistic: 0.24666666666666665
Training time: 392.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 81.87327823691417
Incorrectly Classified Instances: 67.47474747474747
Correctly Classified Instances: 32.525252525252526
Weighted Precision: 0.3360505526392302
Weighted AreaUnderROC: 0.8184489337822671
Root mean squared error: 0.2752583113113047
Relative absolute error: 93.92323232323176
Root relative squared error: 95.74875294989499
Weighted TruePositiveRate: 0.32525252525252524
Weighted MatthewsCorrelation: 0.258029536058049
Weighted FMeasure: 0.3132629888926288
Iteration time: 400.0
Weighted AreaUnderPRC: 0.3083016322385806
Mean absolute error: 0.1552450121045163
Coverage of cases: 96.36363636363636
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1970.0
Weighted Recall: 0.32525252525252524
Weighted FalsePositiveRate: 0.06747474747474747
Kappa statistic: 0.2577777777777778
Training time: 399.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 81.9467401285579
Incorrectly Classified Instances: 63.63636363636363
Correctly Classified Instances: 36.36363636363637
Weighted Precision: 0.4197290863630918
Weighted AreaUnderROC: 0.828446689113356
Root mean squared error: 0.274645340788243
Relative absolute error: 93.72121212121164
Root relative squared error: 95.53553082083884
Weighted TruePositiveRate: 0.36363636363636365
Weighted MatthewsCorrelation: 0.31695167149106906
Weighted FMeasure: 0.363804725980015
Iteration time: 410.0
Weighted AreaUnderPRC: 0.33689116644589645
Mean absolute error: 0.15491109441522685
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2380.0
Weighted Recall: 0.36363636363636365
Weighted FalsePositiveRate: 0.06363636363636364
Kappa statistic: 0.3
Training time: 409.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 81.92837465564699
Incorrectly Classified Instances: 62.82828282828283
Correctly Classified Instances: 37.17171717171717
Weighted Precision: 0.4283729098010398
Weighted AreaUnderROC: 0.8225319865319864
Root mean squared error: 0.27469220089933194
Relative absolute error: 93.73333333333281
Root relative squared error: 95.55183113590829
Weighted TruePositiveRate: 0.3717171717171717
Weighted MatthewsCorrelation: 0.32742789616832446
Weighted FMeasure: 0.37562738589078565
Iteration time: 411.0
Weighted AreaUnderPRC: 0.3392555476988225
Mean absolute error: 0.15493112947658416
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2791.0
Weighted Recall: 0.3717171717171717
Weighted FalsePositiveRate: 0.06282828282828283
Kappa statistic: 0.30888888888888894
Training time: 410.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 81.98347107437976
Incorrectly Classified Instances: 60.4040404040404
Correctly Classified Instances: 39.5959595959596
Weighted Precision: 0.42643715117703784
Weighted AreaUnderROC: 0.8343636363636363
Root mean squared error: 0.27402568181958065
Relative absolute error: 93.51919191919136
Root relative squared error: 95.3199821123512
Weighted TruePositiveRate: 0.39595959595959596
Weighted MatthewsCorrelation: 0.3453602255187852
Weighted FMeasure: 0.3972769015021266
Iteration time: 420.0
Weighted AreaUnderPRC: 0.35668663594189837
Mean absolute error: 0.15457717672593713
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 3211.0
Weighted Recall: 0.39595959595959596
Weighted FalsePositiveRate: 0.06040404040404039
Kappa statistic: 0.33555555555555555
Training time: 419.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.05693296602347
Incorrectly Classified Instances: 61.81818181818182
Correctly Classified Instances: 38.18181818181818
Weighted Precision: 0.4372504660567167
Weighted AreaUnderROC: 0.8426980920314253
Root mean squared error: 0.27351029869656246
Relative absolute error: 93.35353535353488
Root relative squared error: 95.1407058133529
Weighted TruePositiveRate: 0.38181818181818183
Weighted MatthewsCorrelation: 0.34083362856904126
Weighted FMeasure: 0.3923660876788741
Iteration time: 441.0
Weighted AreaUnderPRC: 0.36790880263057324
Mean absolute error: 0.1543033642207198
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 3652.0
Weighted Recall: 0.38181818181818183
Weighted FalsePositiveRate: 0.061818181818181814
Kappa statistic: 0.32
Training time: 440.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.03856749311255
Incorrectly Classified Instances: 58.58585858585859
Correctly Classified Instances: 41.41414141414141
Weighted Precision: 0.47307316873903893
Weighted AreaUnderROC: 0.8533759820426485
Root mean squared error: 0.2727736833371857
Relative absolute error: 93.11515151515093
Root relative squared error: 94.88447376089256
Weighted TruePositiveRate: 0.41414141414141414
Weighted MatthewsCorrelation: 0.3777428616582679
Weighted FMeasure: 0.42492211720946904
Iteration time: 449.0
Weighted AreaUnderPRC: 0.3876338917794052
Mean absolute error: 0.1539093413473579
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 4101.0
Weighted Recall: 0.41414141414141414
Weighted FalsePositiveRate: 0.05858585858585859
Kappa statistic: 0.3555555555555555
Training time: 448.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.1854912764
Incorrectly Classified Instances: 58.78787878787879
Correctly Classified Instances: 41.21212121212121
Weighted Precision: 0.4557155823446911
Weighted AreaUnderROC: 0.8638945005611671
Root mean squared error: 0.2721919224617653
Relative absolute error: 92.92929292929237
Root relative squared error: 94.68210792470336
Weighted TruePositiveRate: 0.4121212121212121
Weighted MatthewsCorrelation: 0.3709273091811146
Weighted FMeasure: 0.4232118590924545
Iteration time: 441.0
Weighted AreaUnderPRC: 0.38843364005148673
Mean absolute error: 0.15360213707321152
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 4542.0
Weighted Recall: 0.4121212121212121
Weighted FalsePositiveRate: 0.05878787878787879
Kappa statistic: 0.3533333333333333
Training time: 440.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.16712580348909
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.4803422416835737
Weighted AreaUnderROC: 0.8748911335578
Root mean squared error: 0.2714094549377082
Relative absolute error: 92.66666666666612
Root relative squared error: 94.40992617187871
Weighted TruePositiveRate: 0.43232323232323233
Weighted MatthewsCorrelation: 0.3962430168004196
Weighted FMeasure: 0.4467923122986474
Iteration time: 443.0
Weighted AreaUnderPRC: 0.41043466817813906
Mean absolute error: 0.15316804407713505
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 4985.0
Weighted Recall: 0.43232323232323233
Weighted FalsePositiveRate: 0.05676767676767677
Kappa statistic: 0.37555555555555553
Training time: 443.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 55.75757575757576
Correctly Classified Instances: 44.24242424242424
Weighted Precision: 0.45724680588521577
Weighted AreaUnderROC: 0.8817306397306397
Root mean squared error: 0.27081512384515344
Relative absolute error: 92.46464646464592
Root relative squared error: 94.20318777883847
Weighted TruePositiveRate: 0.44242424242424244
Weighted MatthewsCorrelation: 0.39304178428506836
Weighted FMeasure: 0.44656536819242204
Iteration time: 454.0
Weighted AreaUnderPRC: 0.4089813244253596
Mean absolute error: 0.1528341263878455
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5439.0
Weighted Recall: 0.44242424242424244
Weighted FalsePositiveRate: 0.05575757575757575
Kappa statistic: 0.3866666666666667
Training time: 453.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.16712580348909
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.4476252022201294
Weighted AreaUnderROC: 0.8845836139169471
Root mean squared error: 0.27063437154979114
Relative absolute error: 92.4121212121207
Root relative squared error: 94.14031299481715
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.38354852008664087
Weighted FMeasure: 0.438275108784444
Iteration time: 458.0
Weighted AreaUnderPRC: 0.40775816854926455
Mean absolute error: 0.15274730778863022
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5897.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.05656565656565657
Kappa statistic: 0.37777777777777777
Training time: 457.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.09366391184535
Incorrectly Classified Instances: 53.13131313131313
Correctly Classified Instances: 46.86868686868687
Weighted Precision: 0.4923502400375096
Weighted AreaUnderROC: 0.8860493827160494
Root mean squared error: 0.2704427230367813
Relative absolute error: 92.34343434343376
Root relative squared error: 94.07364795557466
Weighted TruePositiveRate: 0.4686868686868687
Weighted MatthewsCorrelation: 0.4252694794971798
Weighted FMeasure: 0.47463870292093674
Iteration time: 468.0
Weighted AreaUnderPRC: 0.42763961970222536
Mean absolute error: 0.15263377577427165
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6365.0
Weighted Recall: 0.4686868686868687
Weighted FalsePositiveRate: 0.05313131313131313
Kappa statistic: 0.41555555555555557
Training time: 467.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.05693296602351
Incorrectly Classified Instances: 49.898989898989896
Correctly Classified Instances: 50.101010101010104
Weighted Precision: 0.5201131013353067
Weighted AreaUnderROC: 0.8916588103254769
Root mean squared error: 0.2701334202753813
Relative absolute error: 92.24242424242362
Root relative squared error: 93.96605682218826
Weighted TruePositiveRate: 0.501010101010101
Weighted MatthewsCorrelation: 0.45811486222060366
Weighted FMeasure: 0.5034525705823175
Iteration time: 481.0
Weighted AreaUnderPRC: 0.44945948759514814
Mean absolute error: 0.1524668169296268
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 6846.0
Weighted Recall: 0.501010101010101
Weighted FalsePositiveRate: 0.049898989898989894
Kappa statistic: 0.4511111111111111
Training time: 481.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.00183654729072
Incorrectly Classified Instances: 48.484848484848484
Correctly Classified Instances: 51.515151515151516
Weighted Precision: 0.536614973155762
Weighted AreaUnderROC: 0.8966846240179573
Root mean squared error: 0.2697143872567033
Relative absolute error: 92.0969696969691
Root relative squared error: 93.82029595926605
Weighted TruePositiveRate: 0.5151515151515151
Weighted MatthewsCorrelation: 0.4739340401686541
Weighted FMeasure: 0.5166739610702369
Iteration time: 488.0
Weighted AreaUnderPRC: 0.46363197230686354
Mean absolute error: 0.15222639619333833
Coverage of cases: 99.5959595959596
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7334.0
Weighted Recall: 0.5151515151515151
Weighted FalsePositiveRate: 0.04848484848484848
Kappa statistic: 0.4666666666666666
Training time: 488.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.07529843893445
Incorrectly Classified Instances: 48.08080808080808
Correctly Classified Instances: 51.91919191919192
Weighted Precision: 0.5402297251901396
Weighted AreaUnderROC: 0.8975824915824916
Root mean squared error: 0.26969502808993384
Relative absolute error: 92.09696969696915
Root relative squared error: 93.81356186260064
Weighted TruePositiveRate: 0.5191919191919192
Weighted MatthewsCorrelation: 0.4790532454736044
Weighted FMeasure: 0.5228022165753272
Iteration time: 498.0
Weighted AreaUnderPRC: 0.4595872260547215
Mean absolute error: 0.15222639619333841
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7832.0
Weighted Recall: 0.5191919191919192
Weighted FalsePositiveRate: 0.048080808080808085
Kappa statistic: 0.4711111111111111
Training time: 498.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.1854912764
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5547926799152348
Weighted AreaUnderROC: 0.900803591470258
Root mean squared error: 0.26933866123642175
Relative absolute error: 91.9838383838378
Root relative squared error: 93.68959945923521
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.4983270890068519
Weighted FMeasure: 0.5399316574701025
Iteration time: 492.0
Weighted AreaUnderPRC: 0.4761586352802842
Mean absolute error: 0.1520394022873362
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 8324.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.04606060606060605
Kappa statistic: 0.49333333333333335
Training time: 492.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 45.25252525252525
Correctly Classified Instances: 54.74747474747475
Weighted Precision: 0.5566966364591526
Weighted AreaUnderROC: 0.9030662177328845
Root mean squared error: 0.26923788298462314
Relative absolute error: 91.9515151515146
Root relative squared error: 93.65454368966292
Weighted TruePositiveRate: 0.5474747474747474
Weighted MatthewsCorrelation: 0.5044047460420695
Weighted FMeasure: 0.5456590420434402
Iteration time: 499.0
Weighted AreaUnderPRC: 0.48184931652273877
Mean absolute error: 0.1519859754570499
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8823.0
Weighted Recall: 0.5474747474747474
Weighted FalsePositiveRate: 0.04525252525252526
Kappa statistic: 0.5022222222222221
Training time: 498.0
		
Time end:Sun Oct 08 10.08.36 EEST 2017