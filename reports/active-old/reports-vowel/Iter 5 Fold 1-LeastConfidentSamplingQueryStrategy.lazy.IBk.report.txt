Sun Oct 08 10.04.09 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.04.09 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.42787521013862667
Weighted AreaUnderROC: 0.6622222222222222
Root mean squared error: 0.3183116618183918
Relative absolute error: 70.80000000000037
Root relative squared error: 110.72488428533063
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.3362593802728566
Weighted FMeasure: 0.3815403419227148
Iteration time: 6.0
Weighted AreaUnderPRC: 0.214773068348759
Mean absolute error: 0.1170247933884311
Coverage of cases: 67.87878787878788
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 6.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.061414141414141414
Kappa statistic: 0.3244444444444445
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 57.17171717171717
Correctly Classified Instances: 42.82828282828283
Weighted Precision: 0.4564520925724513
Weighted AreaUnderROC: 0.6855555555555556
Root mean squared error: 0.3094263140984163
Relative absolute error: 66.02905982905949
Root relative squared error: 107.63411125958291
Weighted TruePositiveRate: 0.42828282828282827
Weighted MatthewsCorrelation: 0.380007572090432
Weighted FMeasure: 0.42737235538183094
Iteration time: 6.0
Weighted AreaUnderPRC: 0.24939750830242316
Mean absolute error: 0.10913894186621473
Coverage of cases: 65.25252525252525
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 12.0
Weighted Recall: 0.42828282828282827
Weighted FalsePositiveRate: 0.05717171717171718
Kappa statistic: 0.37111111111111106
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.46465768189883716
Weighted AreaUnderROC: 0.6944444444444444
Root mean squared error: 0.30667105837874215
Relative absolute error: 63.96296296296252
Root relative squared error: 106.67569406244147
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.3929677943746135
Weighted FMeasure: 0.43769379939838327
Iteration time: 6.0
Weighted AreaUnderPRC: 0.2618602960109197
Mean absolute error: 0.10572390572390566
Coverage of cases: 62.02020202020202
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 18.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.05555555555555556
Kappa statistic: 0.38888888888888884
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 52.525252525252526
Correctly Classified Instances: 47.474747474747474
Weighted Precision: 0.5014777826460367
Weighted AreaUnderROC: 0.7111111111111111
Root mean squared error: 0.299444549832702
Relative absolute error: 60.50980392156794
Root relative squared error: 104.16194914346403
Weighted TruePositiveRate: 0.47474747474747475
Weighted MatthewsCorrelation: 0.4262541642677305
Weighted FMeasure: 0.4616855675205303
Iteration time: 6.0
Weighted AreaUnderPRC: 0.286676239087502
Mean absolute error: 0.10001620482903857
Coverage of cases: 61.21212121212121
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 24.0
Weighted Recall: 0.47474747474747475
Weighted FalsePositiveRate: 0.05252525252525253
Kappa statistic: 0.4222222222222223
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 50.505050505050505
Correctly Classified Instances: 49.494949494949495
Weighted Precision: 0.5371478796240394
Weighted AreaUnderROC: 0.7222222222222222
Root mean squared error: 0.2945981190691689
Relative absolute error: 58.128654970760024
Root relative squared error: 102.47611557260595
Weighted TruePositiveRate: 0.494949494949495
Weighted MatthewsCorrelation: 0.4516836093660857
Weighted FMeasure: 0.47845189208894556
Iteration time: 6.0
Weighted AreaUnderPRC: 0.30574856547585805
Mean absolute error: 0.09608042143927338
Coverage of cases: 54.343434343434346
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 30.0
Weighted Recall: 0.494949494949495
Weighted FalsePositiveRate: 0.050505050505050504
Kappa statistic: 0.44444444444444453
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5793468191390256
Weighted AreaUnderROC: 0.7388888888888889
Root mean squared error: 0.2863967100536755
Relative absolute error: 54.72486772486763
Root relative squared error: 99.62325099633014
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4867104466614872
Weighted FMeasure: 0.5027511468613192
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3342929994632935
Mean absolute error: 0.0904543268179636
Coverage of cases: 52.525252525252526
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 37.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 45.05050505050505
Correctly Classified Instances: 54.94949494949495
Weighted Precision: 0.6070588480409658
Weighted AreaUnderROC: 0.7522222222222223
Root mean squared error: 0.2796097456773956
Relative absolute error: 51.968115942028625
Root relative squared error: 97.26240175530862
Weighted TruePositiveRate: 0.5494949494949495
Weighted MatthewsCorrelation: 0.5146557486084677
Weighted FMeasure: 0.5294407722196655
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3590666246651354
Mean absolute error: 0.08589771230087431
Coverage of cases: 54.94949494949495
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 44.0
Weighted Recall: 0.5494949494949495
Weighted FalsePositiveRate: 0.04505050505050505
Kappa statistic: 0.5044444444444444
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.6687431082339803
Weighted AreaUnderROC: 0.7733333333333333
Root mean squared error: 0.2679443221634198
Relative absolute error: 47.73866666666647
Root relative squared error: 93.20457785609726
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.5628105358548147
Weighted FMeasure: 0.5655360001210519
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4082651525610487
Mean absolute error: 0.07890688705234178
Coverage of cases: 58.78787878787879
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 51.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.041212121212121214
Kappa statistic: 0.5466666666666666
Training time: 1.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6967639912109377
Weighted AreaUnderROC: 0.7866666666666667
Root mean squared error: 0.2603601620499961
Relative absolute error: 45.002469135802144
Root relative squared error: 90.56642364533711
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5890275514483774
Weighted FMeasure: 0.5883663854284451
Iteration time: 6.0
Weighted AreaUnderPRC: 0.43921880789348205
Mean absolute error: 0.07438424650545856
Coverage of cases: 61.21212121212121
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 57.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.038787878787878795
Kappa statistic: 0.5733333333333333
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.7042998276317941
Weighted AreaUnderROC: 0.7955555555555556
Root mean squared error: 0.2552259384837765
Relative absolute error: 43.131034482758324
Root relative squared error: 88.78048119190281
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.6031749819375574
Weighted FMeasure: 0.6024173830338323
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4569764266112051
Mean absolute error: 0.0712909660872043
Coverage of cases: 62.82828282828283
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 64.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.037171717171717175
Kappa statistic: 0.5911111111111111
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 34.14141414141414
Correctly Classified Instances: 65.85858585858585
Weighted Precision: 0.7343792142294325
Weighted AreaUnderROC: 0.8122222222222223
Root mean squared error: 0.24490137776557402
Relative absolute error: 39.77132616487427
Root relative squared error: 85.18907714377822
Weighted TruePositiveRate: 0.6585858585858586
Weighted MatthewsCorrelation: 0.635145759864336
Weighted FMeasure: 0.6309854023999202
Iteration time: 6.0
Weighted AreaUnderPRC: 0.48759565946920036
Mean absolute error: 0.06573772919813971
Coverage of cases: 65.85858585858585
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6585858585858586
Weighted FalsePositiveRate: 0.034141414141414146
Kappa statistic: 0.6244444444444445
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 30.1010101010101
Correctly Classified Instances: 69.8989898989899
Weighted Precision: 0.7677845946991237
Weighted AreaUnderROC: 0.8344444444444444
Root mean squared error: 0.23020997588426262
Relative absolute error: 35.340740740740436
Root relative squared error: 80.0786650275373
Weighted TruePositiveRate: 0.6989898989898989
Weighted MatthewsCorrelation: 0.6797578389176796
Weighted FMeasure: 0.6769641818286911
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5376906683895345
Mean absolute error: 0.05841444750535647
Coverage of cases: 69.8989898989899
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 76.0
Weighted Recall: 0.6989898989898989
Weighted FalsePositiveRate: 0.030101010101010104
Kappa statistic: 0.6688888888888889
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 28.282828282828284
Correctly Classified Instances: 71.71717171717172
Weighted Precision: 0.7758038421980362
Weighted AreaUnderROC: 0.8444444444444444
Root mean squared error: 0.22335799781403917
Relative absolute error: 33.27619047619015
Root relative squared error: 77.69520073779987
Weighted TruePositiveRate: 0.7171717171717171
Weighted MatthewsCorrelation: 0.696292036073236
Weighted FMeasure: 0.6942002921345164
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5604789096538523
Mean absolute error: 0.05500196772924028
Coverage of cases: 71.71717171717172
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 82.0
Weighted Recall: 0.7171717171717171
Weighted FalsePositiveRate: 0.028282828282828285
Kappa statistic: 0.6888888888888889
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.060606060606062
Correctly Classified Instances: 73.93939393939394
Weighted Precision: 0.7854833017911583
Weighted AreaUnderROC: 0.8566666666666666
Root mean squared error: 0.2145863106549518
Relative absolute error: 30.787387387387245
Root relative squared error: 74.6439645998314
Weighted TruePositiveRate: 0.7393939393939394
Weighted MatthewsCorrelation: 0.7170282327482376
Weighted FMeasure: 0.7166714001397443
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5880721886586766
Mean absolute error: 0.050888243615516436
Coverage of cases: 73.93939393939394
Instances selection time: 5.0
Test time: 20.0
Accumulative iteration time: 87.0
Weighted Recall: 0.7393939393939394
Weighted FalsePositiveRate: 0.02606060606060606
Kappa statistic: 0.7133333333333333
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 23.232323232323232
Correctly Classified Instances: 76.76767676767676
Weighted Precision: 0.7968835136533032
Weighted AreaUnderROC: 0.8722222222222222
Root mean squared error: 0.20276817482860632
Relative absolute error: 27.655270655270495
Root relative squared error: 70.53301963989776
Weighted TruePositiveRate: 0.7676767676767676
Weighted MatthewsCorrelation: 0.7415052578821304
Weighted FMeasure: 0.7415527220259239
Iteration time: 4.0
Weighted AreaUnderPRC: 0.620829011599042
Mean absolute error: 0.045711191165736645
Coverage of cases: 76.76767676767676
Instances selection time: 4.0
Test time: 21.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7676767676767676
Weighted FalsePositiveRate: 0.023232323232323233
Kappa statistic: 0.7444444444444444
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 18.585858585858585
Correctly Classified Instances: 81.41414141414141
Weighted Precision: 0.8375079279806115
Weighted AreaUnderROC: 0.8977777777777779
Root mean squared error: 0.18150848825269267
Relative absolute error: 22.578861788617765
Root relative squared error: 63.13782612856652
Weighted TruePositiveRate: 0.8141414141414142
Weighted MatthewsCorrelation: 0.7963330450649383
Weighted FMeasure: 0.7994820446594894
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6898984856240589
Mean absolute error: 0.037320432708459354
Coverage of cases: 81.41414141414141
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8141414141414142
Weighted FalsePositiveRate: 0.018585858585858588
Kappa statistic: 0.7955555555555556
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 16.363636363636363
Correctly Classified Instances: 83.63636363636364
Weighted Precision: 0.866809663180861
Weighted AreaUnderROC: 0.9100000000000001
Root mean squared error: 0.17042608379651228
Relative absolute error: 20.0976744186045
Root relative squared error: 59.28280572496625
Weighted TruePositiveRate: 0.8363636363636363
Weighted MatthewsCorrelation: 0.8220841364397521
Weighted FMeasure: 0.8209354446576802
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7262347487138878
Mean absolute error: 0.03321929655967708
Coverage of cases: 83.63636363636364
Instances selection time: 4.0
Test time: 22.0
Accumulative iteration time: 100.0
Weighted Recall: 0.8363636363636363
Weighted FalsePositiveRate: 0.016363636363636365
Kappa statistic: 0.82
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.535353535353535
Correctly Classified Instances: 86.46464646464646
Weighted Precision: 0.8836250528496887
Weighted AreaUnderROC: 0.9255555555555556
Root mean squared error: 0.15510494881999845
Relative absolute error: 16.969382716049175
Root relative squared error: 53.95334060985433
Weighted TruePositiveRate: 0.8646464646464647
Weighted MatthewsCorrelation: 0.8517935402429583
Weighted FMeasure: 0.8537794056521439
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7681602123174319
Mean absolute error: 0.028048566472808734
Coverage of cases: 86.46464646464646
Instances selection time: 3.0
Test time: 23.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8646464646464647
Weighted FalsePositiveRate: 0.013535353535353538
Kappa statistic: 0.8511111111111112
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.525252525252526
Correctly Classified Instances: 87.47474747474747
Weighted Precision: 0.8905463153859245
Weighted AreaUnderROC: 0.9311111111111112
Root mean squared error: 0.14928317484913856
Relative absolute error: 15.795744680850916
Root relative squared error: 51.92823337508833
Weighted TruePositiveRate: 0.8747474747474747
Weighted MatthewsCorrelation: 0.8625405097481019
Weighted FMeasure: 0.8650717277510686
Iteration time: 2.0
Weighted AreaUnderPRC: 0.783986528559672
Mean absolute error: 0.026108668893968626
Coverage of cases: 87.47474747474747
Instances selection time: 2.0
Test time: 39.0
Accumulative iteration time: 105.0
Weighted Recall: 0.8747474747474747
Weighted FalsePositiveRate: 0.012525252525252526
Kappa statistic: 0.8622222222222222
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 8.080808080808081
Correctly Classified Instances: 91.91919191919192
Weighted Precision: 0.9257801244577639
Weighted AreaUnderROC: 0.9555555555555556
Root mean squared error: 0.12001749347603763
Relative absolute error: 10.934240362811776
Root relative squared error: 41.748150229354444
Weighted TruePositiveRate: 0.9191919191919192
Weighted MatthewsCorrelation: 0.9133533373892997
Weighted FMeasure: 0.9198028482439166
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8578925376306438
Mean absolute error: 0.01807312456663115
Coverage of cases: 91.91919191919192
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 106.0
Weighted Recall: 0.9191919191919192
Weighted FalsePositiveRate: 0.008080808080808083
Kappa statistic: 0.9111111111111111
Training time: 0.0
		
Time end:Sun Oct 08 10.04.10 EEST 2017