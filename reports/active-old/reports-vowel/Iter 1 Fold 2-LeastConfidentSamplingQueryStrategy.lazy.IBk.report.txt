Sun Oct 08 10.04.00 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.04.00 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 65.65656565656566
Correctly Classified Instances: 34.343434343434346
Weighted Precision: 0.3677240655275545
Weighted AreaUnderROC: 0.6388888888888887
Root mean squared error: 0.32903565583370553
Relative absolute error: 75.00000000000044
Root relative squared error: 114.45523142259525
Weighted TruePositiveRate: 0.3434343434343434
Weighted MatthewsCorrelation: 0.2850644905168365
Weighted FMeasure: 0.34144602009397096
Iteration time: 6.0
Weighted AreaUnderPRC: 0.18312997325038227
Mean absolute error: 0.12396694214876186
Coverage of cases: 66.46464646464646
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 6.0
Weighted Recall: 0.3434343434343434
Weighted FalsePositiveRate: 0.06565656565656566
Kappa statistic: 0.27777777777777773
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 63.03030303030303
Correctly Classified Instances: 36.96969696969697
Weighted Precision: 0.3926397523039637
Weighted AreaUnderROC: 0.6533333333333333
Root mean squared error: 0.32480039600275845
Relative absolute error: 71.92820512820478
Root relative squared error: 112.98199399226989
Weighted TruePositiveRate: 0.3696969696969697
Weighted MatthewsCorrelation: 0.3130926043705879
Weighted FMeasure: 0.36601668281170563
Iteration time: 6.0
Weighted AreaUnderPRC: 0.1986406816807994
Mean absolute error: 0.11888959525323181
Coverage of cases: 63.83838383838384
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 12.0
Weighted Recall: 0.3696969696969697
Weighted FalsePositiveRate: 0.06303030303030303
Kappa statistic: 0.3066666666666667
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 60.60606060606061
Correctly Classified Instances: 39.39393939393939
Weighted Precision: 0.42262603276636473
Weighted AreaUnderROC: 0.6666666666666666
Root mean squared error: 0.3202443970301446
Relative absolute error: 69.11111111111065
Root relative squared error: 111.39718727747676
Weighted TruePositiveRate: 0.3939393939393939
Weighted MatthewsCorrelation: 0.339657312323262
Weighted FMeasure: 0.38526677094236467
Iteration time: 7.0
Weighted AreaUnderPRC: 0.21622156358292136
Mean absolute error: 0.11423324150596874
Coverage of cases: 58.58585858585859
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 19.0
Weighted Recall: 0.3939393939393939
Weighted FalsePositiveRate: 0.06060606060606061
Kappa statistic: 0.3333333333333333
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 57.17171717171717
Correctly Classified Instances: 42.82828282828283
Weighted Precision: 0.45591352914407857
Weighted AreaUnderROC: 0.6855555555555555
Root mean squared error: 0.3123595893580859
Relative absolute error: 65.29019607843064
Root relative squared error: 108.65445265030846
Weighted TruePositiveRate: 0.42828282828282827
Weighted MatthewsCorrelation: 0.37377614061112785
Weighted FMeasure: 0.4108064356219803
Iteration time: 6.0
Weighted AreaUnderPRC: 0.2424733974902319
Mean absolute error: 0.10791767946848109
Coverage of cases: 56.16161616161616
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 25.0
Weighted Recall: 0.42828282828282827
Weighted FalsePositiveRate: 0.05717171717171717
Kappa statistic: 0.37111111111111106
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 54.54545454545455
Correctly Classified Instances: 45.45454545454545
Weighted Precision: 0.4732117948063333
Weighted AreaUnderROC: 0.7
Root mean squared error: 0.30611915202254114
Relative absolute error: 62.315789473684035
Root relative squared error: 106.48371313696234
Weighted TruePositiveRate: 0.45454545454545453
Weighted MatthewsCorrelation: 0.3990736172688374
Weighted FMeasure: 0.4350512382906014
Iteration time: 6.0
Weighted AreaUnderPRC: 0.26177113171366634
Mean absolute error: 0.10300130491518088
Coverage of cases: 51.717171717171716
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 31.0
Weighted Recall: 0.45454545454545453
Weighted FalsePositiveRate: 0.05454545454545454
Kappa statistic: 0.4
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 51.111111111111114
Correctly Classified Instances: 48.888888888888886
Weighted Precision: 0.5121083897510772
Weighted AreaUnderROC: 0.7188888888888888
Root mean squared error: 0.2971335144866403
Relative absolute error: 58.51534391534389
Root relative squared error: 103.35805424432588
Weighted TruePositiveRate: 0.4888888888888889
Weighted MatthewsCorrelation: 0.4371603584633952
Weighted FMeasure: 0.46613483862837357
Iteration time: 7.0
Weighted AreaUnderPRC: 0.29582025509371823
Mean absolute error: 0.0967195767195773
Coverage of cases: 48.888888888888886
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 38.0
Weighted Recall: 0.4888888888888889
Weighted FalsePositiveRate: 0.051111111111111114
Kappa statistic: 0.4377777777777778
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.559686034252345
Weighted AreaUnderROC: 0.7388888888888888
Root mean squared error: 0.28701658434197824
Relative absolute error: 54.50724637681122
Root relative squared error: 99.83887460387152
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.48024463847363624
Weighted FMeasure: 0.5018731859190626
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3309668562841938
Mean absolute error: 0.09009462211043233
Coverage of cases: 52.525252525252526
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 45.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5836087005040884
Weighted AreaUnderROC: 0.7533333333333334
Root mean squared error: 0.2794902877322205
Relative absolute error: 51.5626666666665
Root relative squared error: 97.22084824425936
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5064417943649796
Weighted FMeasure: 0.522958506709559
Iteration time: 7.0
Weighted AreaUnderPRC: 0.35503235835965113
Mean absolute error: 0.08522754820936668
Coverage of cases: 55.15151515151515
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 52.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.6437667704731745
Weighted AreaUnderROC: 0.778888888888889
Root mean squared error: 0.26505438870987913
Relative absolute error: 46.49465020576099
Root relative squared error: 92.19931293615174
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5583537035730897
Weighted FMeasure: 0.5632580413064464
Iteration time: 8.0
Weighted AreaUnderPRC: 0.40426897724366784
Mean absolute error: 0.07685066149712609
Coverage of cases: 59.7979797979798
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 60.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.040202020202020204
Kappa statistic: 0.5577777777777777
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 38.58585858585859
Correctly Classified Instances: 61.41414141414141
Weighted Precision: 0.6800766306031893
Weighted AreaUnderROC: 0.7877777777777777
Root mean squared error: 0.2600267657153309
Relative absolute error: 44.62758620689621
Root relative squared error: 90.45045154941678
Weighted TruePositiveRate: 0.6141414141414141
Weighted MatthewsCorrelation: 0.5797304722452123
Weighted FMeasure: 0.576121543298199
Iteration time: 7.0
Weighted AreaUnderPRC: 0.42582197614482586
Mean absolute error: 0.07376460530065537
Coverage of cases: 61.41414141414141
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6141414141414141
Weighted FalsePositiveRate: 0.03858585858585859
Kappa statistic: 0.5755555555555555
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 35.15151515151515
Correctly Classified Instances: 64.84848484848484
Weighted Precision: 0.7033795875661535
Weighted AreaUnderROC: 0.8066666666666665
Root mean squared error: 0.248491577956811
Relative absolute error: 40.84301075268785
Root relative squared error: 86.43793022840913
Weighted TruePositiveRate: 0.6484848484848484
Weighted MatthewsCorrelation: 0.6146436813678291
Weighted FMeasure: 0.6125481617541352
Iteration time: 6.0
Weighted AreaUnderPRC: 0.46600362377098004
Mean absolute error: 0.06750910868212912
Coverage of cases: 64.84848484848484
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6484848484848484
Weighted FalsePositiveRate: 0.035151515151515156
Kappa statistic: 0.6133333333333333
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.7160326290704594
Weighted AreaUnderROC: 0.8133333333333334
Root mean squared error: 0.24442357369788761
Relative absolute error: 39.42222222222189
Root relative squared error: 85.02287273956817
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.6282944710751025
Weighted FMeasure: 0.6250150499333181
Iteration time: 7.0
Weighted AreaUnderPRC: 0.48493169401813585
Mean absolute error: 0.06516069788797048
Coverage of cases: 66.06060606060606
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 80.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 30.303030303030305
Correctly Classified Instances: 69.6969696969697
Weighted Precision: 0.7589762383106038
Weighted AreaUnderROC: 0.8333333333333334
Root mean squared error: 0.2311848819759668
Relative absolute error: 35.428571428571075
Root relative squared error: 80.4177866405387
Weighted TruePositiveRate: 0.696969696969697
Weighted MatthewsCorrelation: 0.6681790053530985
Weighted FMeasure: 0.6582504021984821
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5328734823920426
Mean absolute error: 0.05855962219598562
Coverage of cases: 69.6969696969697
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 86.0
Weighted Recall: 0.696969696969697
Weighted FalsePositiveRate: 0.030303030303030304
Kappa statistic: 0.6666666666666667
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 28.282828282828284
Correctly Classified Instances: 71.71717171717172
Weighted Precision: 0.7699471198974676
Weighted AreaUnderROC: 0.8444444444444444
Root mean squared error: 0.22353426726295786
Relative absolute error: 33.15915915915898
Root relative squared error: 77.7565161612533
Weighted TruePositiveRate: 0.7171717171717171
Weighted MatthewsCorrelation: 0.689987068039056
Weighted FMeasure: 0.6824600942304571
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5600772913912753
Mean absolute error: 0.05480852753580033
Coverage of cases: 71.71717171717172
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7171717171717171
Weighted FalsePositiveRate: 0.028282828282828285
Kappa statistic: 0.6888888888888889
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.262626262626263
Correctly Classified Instances: 73.73737373737374
Weighted Precision: 0.7739934174064501
Weighted AreaUnderROC: 0.8555555555555555
Root mean squared error: 0.21556709962608495
Relative absolute error: 30.894586894586705
Root relative squared error: 74.98513257563434
Weighted TruePositiveRate: 0.7373737373737373
Weighted MatthewsCorrelation: 0.7093541586168489
Weighted FMeasure: 0.7068627984106474
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5859926993450739
Mean absolute error: 0.05106543288361472
Coverage of cases: 73.73737373737374
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7373737373737373
Weighted FalsePositiveRate: 0.026262626262626265
Kappa statistic: 0.711111111111111
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 23.434343434343436
Correctly Classified Instances: 76.56565656565657
Weighted Precision: 0.8004362691598786
Weighted AreaUnderROC: 0.8711111111111111
Root mean squared error: 0.20377499073487795
Relative absolute error: 27.769105691056723
Root relative squared error: 70.8832410992113
Weighted TruePositiveRate: 0.7656565656565657
Weighted MatthewsCorrelation: 0.7399577428183519
Weighted FMeasure: 0.7358905447378046
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6316626304879805
Mean absolute error: 0.045899348249680824
Coverage of cases: 76.56565656565657
Instances selection time: 6.0
Test time: 22.0
Accumulative iteration time: 102.0
Weighted Recall: 0.7656565656565657
Weighted FalsePositiveRate: 0.02343434343434343
Kappa statistic: 0.7422222222222222
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 17.373737373737374
Correctly Classified Instances: 82.62626262626263
Weighted Precision: 0.8372913060149155
Weighted AreaUnderROC: 0.9044444444444443
Root mean squared error: 0.17559788121437653
Relative absolute error: 21.18036175710577
Root relative squared error: 61.0818182630834
Weighted TruePositiveRate: 0.8262626262626263
Weighted MatthewsCorrelation: 0.8030625750445146
Weighted FMeasure: 0.8040058398854858
Iteration time: 5.0
Weighted AreaUnderPRC: 0.722795085256799
Mean absolute error: 0.03500886240843952
Coverage of cases: 82.62626262626263
Instances selection time: 5.0
Test time: 23.0
Accumulative iteration time: 107.0
Weighted Recall: 0.8262626262626263
Weighted FalsePositiveRate: 0.017373737373737375
Kappa statistic: 0.8088888888888889
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.737373737373737
Correctly Classified Instances: 86.26262626262626
Weighted Precision: 0.8753707770057788
Weighted AreaUnderROC: 0.9244444444444444
Root mean squared error: 0.15625580287153298
Relative absolute error: 17.18617283950596
Root relative squared error: 54.353665816155335
Weighted TruePositiveRate: 0.8626262626262626
Weighted MatthewsCorrelation: 0.8463037314622349
Weighted FMeasure: 0.8478197471712475
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7700157547099196
Mean absolute error: 0.02840689725538193
Coverage of cases: 86.26262626262626
Instances selection time: 3.0
Test time: 24.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8626262626262626
Weighted FalsePositiveRate: 0.013737373737373739
Kappa statistic: 0.8488888888888889
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.313131313131313
Correctly Classified Instances: 88.68686868686869
Weighted Precision: 0.9005859654431353
Weighted AreaUnderROC: 0.9377777777777777
Root mean squared error: 0.1418914696864298
Relative absolute error: 14.493617021276433
Root relative squared error: 49.35702472336397
Weighted TruePositiveRate: 0.8868686868686869
Weighted MatthewsCorrelation: 0.8727698968952892
Weighted FMeasure: 0.8716543034848622
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8064588494176577
Mean absolute error: 0.023956391770705
Coverage of cases: 88.68686868686869
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 112.0
Weighted Recall: 0.8868686868686869
Weighted FalsePositiveRate: 0.011313131313131313
Kappa statistic: 0.8755555555555556
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.696969696969697
Correctly Classified Instances: 90.3030303030303
Weighted Precision: 0.9116425226216969
Weighted AreaUnderROC: 0.9466666666666668
Root mean squared error: 0.1314408940389846
Relative absolute error: 12.672108843537377
Root relative squared error: 45.72178631372426
Weighted TruePositiveRate: 0.9030303030303031
Weighted MatthewsCorrelation: 0.893160329814336
Weighted FMeasure: 0.8963015554118566
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8309825722919024
Mean absolute error: 0.02094563445212803
Coverage of cases: 90.3030303030303
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 114.0
Weighted Recall: 0.9030303030303031
Weighted FalsePositiveRate: 0.009696969696969697
Kappa statistic: 0.8933333333333334
Training time: 1.0
		
Time end:Sun Oct 08 10.04.01 EEST 2017