Sun Oct 08 10.04.43 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.04.43 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 19.412304866850388
Incorrectly Classified Instances: 52.525252525252526
Correctly Classified Instances: 47.474747474747474
Weighted Precision: 0.5285042232492343
Weighted AreaUnderROC: 0.8805072951739618
Root mean squared error: 0.27469106329529835
Relative absolute error: 61.853868475056004
Root relative squared error: 95.55143541972792
Weighted TruePositiveRate: 0.47474747474747475
Weighted MatthewsCorrelation: 0.4184507497900952
Weighted FMeasure: 0.4550268402779059
Iteration time: 40.0
Weighted AreaUnderPRC: 0.4812585847903567
Mean absolute error: 0.10223779913232463
Coverage of cases: 69.0909090909091
Instances selection time: 40.0
Test time: 40.0
Accumulative iteration time: 40.0
Weighted Recall: 0.47474747474747475
Weighted FalsePositiveRate: 0.05252525252525253
Kappa statistic: 0.4222222222222223
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 20.495867768595115
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5308995922764665
Weighted AreaUnderROC: 0.8998742985409651
Root mean squared error: 0.25809209933846233
Relative absolute error: 57.24516141035499
Root relative squared error: 89.77747680043724
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.4757122138458227
Weighted FMeasure: 0.5190336662009508
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5531600332137263
Mean absolute error: 0.0946201015047196
Coverage of cases: 77.77777777777777
Instances selection time: 24.0
Test time: 36.0
Accumulative iteration time: 64.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.04787878787878788
Kappa statistic: 0.47333333333333333
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 21.175390266299402
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5796341821076548
Weighted AreaUnderROC: 0.9146172839506173
Root mean squared error: 0.24722064717987866
Relative absolute error: 55.04098197682475
Root relative squared error: 85.99583626802263
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5216274931100706
Weighted FMeasure: 0.5547774914283057
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5826301014294175
Mean absolute error: 0.0909768297137605
Coverage of cases: 79.79797979797979
Instances selection time: 24.0
Test time: 36.0
Accumulative iteration time: 88.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 22.258953168044087
Incorrectly Classified Instances: 40.4040404040404
Correctly Classified Instances: 59.5959595959596
Weighted Precision: 0.6084824201210316
Weighted AreaUnderROC: 0.9138630751964085
Root mean squared error: 0.24092618813866282
Relative absolute error: 53.8566301351138
Root relative squared error: 83.80630527504564
Weighted TruePositiveRate: 0.5959595959595959
Weighted MatthewsCorrelation: 0.5551442326585693
Weighted FMeasure: 0.5844988447179583
Iteration time: 19.0
Weighted AreaUnderPRC: 0.599772102543797
Mean absolute error: 0.08901922336382503
Coverage of cases: 81.81818181818181
Instances selection time: 19.0
Test time: 35.0
Accumulative iteration time: 107.0
Weighted Recall: 0.5959595959595959
Weighted FalsePositiveRate: 0.04040404040404041
Kappa statistic: 0.5555555555555555
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 22.51606978879706
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.596144015731446
Weighted AreaUnderROC: 0.9153445566778902
Root mean squared error: 0.23888991486741792
Relative absolute error: 53.760942565270426
Root relative squared error: 83.09798651272351
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5430664093208996
Weighted FMeasure: 0.575910983624746
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6003375584086583
Mean absolute error: 0.08886106209135665
Coverage of cases: 81.81818181818181
Instances selection time: 17.0
Test time: 37.0
Accumulative iteration time: 125.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.04161616161616163
Kappa statistic: 0.5422222222222222
Training time: 1.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 23.72819100091826
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.616248399502669
Weighted AreaUnderROC: 0.9159461279461281
Root mean squared error: 0.23802109595792498
Relative absolute error: 53.40320941041599
Root relative squared error: 82.79576738361916
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5659555677721062
Weighted FMeasure: 0.5998238979965351
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6095515739262487
Mean absolute error: 0.08826976762052285
Coverage of cases: 81.81818181818181
Instances selection time: 16.0
Test time: 39.0
Accumulative iteration time: 142.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.039797979797979804
Kappa statistic: 0.5622222222222222
Training time: 1.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 23.654729109274566
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6498361405077422
Weighted AreaUnderROC: 0.9274118967452302
Root mean squared error: 0.23071924478162317
Relative absolute error: 52.13771159583198
Root relative squared error: 80.2558144898229
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.6026795088102039
Weighted FMeasure: 0.6343644946440673
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6260975264841082
Mean absolute error: 0.0861780356955906
Coverage of cases: 83.83838383838383
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 158.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.03656565656565656
Kappa statistic: 0.5977777777777777
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 24.022038567493098
Incorrectly Classified Instances: 42.42424242424242
Correctly Classified Instances: 57.57575757575758
Weighted Precision: 0.6027717005722699
Weighted AreaUnderROC: 0.9225768799102132
Root mean squared error: 0.24017416287213816
Relative absolute error: 55.24005505922032
Root relative squared error: 83.54471287802221
Weighted TruePositiveRate: 0.5757575757575758
Weighted MatthewsCorrelation: 0.5437116510630263
Weighted FMeasure: 0.5810936224247021
Iteration time: 14.0
Weighted AreaUnderPRC: 0.57583256715423
Mean absolute error: 0.09130587613094325
Coverage of cases: 84.44444444444444
Instances selection time: 14.0
Test time: 37.0
Accumulative iteration time: 172.0
Weighted Recall: 0.5757575757575758
Weighted FalsePositiveRate: 0.04242424242424243
Kappa statistic: 0.5333333333333333
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 25.160697887970585
Incorrectly Classified Instances: 40.4040404040404
Correctly Classified Instances: 59.5959595959596
Weighted Precision: 0.6317316698297366
Weighted AreaUnderROC: 0.9334186307519642
Root mean squared error: 0.23412194702677708
Relative absolute error: 54.53248074971272
Root relative squared error: 81.43944631216895
Weighted TruePositiveRate: 0.5959595959595959
Weighted MatthewsCorrelation: 0.5666587078644183
Weighted FMeasure: 0.5960861095141897
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6130557452323099
Mean absolute error: 0.09013633181770755
Coverage of cases: 87.27272727272727
Instances selection time: 13.0
Test time: 36.0
Accumulative iteration time: 186.0
Weighted Recall: 0.5959595959595959
Weighted FalsePositiveRate: 0.04040404040404041
Kappa statistic: 0.5555555555555555
Training time: 1.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 24.995408631772243
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.6276032235929597
Weighted AreaUnderROC: 0.9265319865319865
Root mean squared error: 0.23393570975999517
Relative absolute error: 54.242311067740935
Root relative squared error: 81.37466357786306
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5697975828732874
Weighted FMeasure: 0.6014910313859542
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6073365861981276
Mean absolute error: 0.08965671250866328
Coverage of cases: 85.65656565656566
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 199.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.0397979797979798
Kappa statistic: 0.5622222222222222
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 25.528007346189135
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6272798480635458
Weighted AreaUnderROC: 0.9282918069584736
Root mean squared error: 0.23220515081593243
Relative absolute error: 54.51143203043676
Root relative squared error: 80.77268771013752
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5728780390681195
Weighted FMeasure: 0.6054259704288153
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6065926589729603
Mean absolute error: 0.09010154054617704
Coverage of cases: 86.26262626262626
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 211.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 25.693296602387477
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6322881244765161
Weighted AreaUnderROC: 0.9302626262626263
Root mean squared error: 0.23122082265671948
Relative absolute error: 54.84127553101975
Root relative squared error: 80.4302886258406
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.579760886981654
Weighted FMeasure: 0.6123630967738244
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5994320180462702
Mean absolute error: 0.09064673641490925
Coverage of cases: 86.46464646464646
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 222.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.03878787878787879
Kappa statistic: 0.5733333333333333
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 25.913682277318614
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6373081554556125
Weighted AreaUnderROC: 0.9272188552188553
Root mean squared error: 0.2317273227650492
Relative absolute error: 54.58773914302243
Root relative squared error: 80.60647496335943
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5823143316182634
Weighted FMeasure: 0.6150579944616956
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6057791715643106
Mean absolute error: 0.09022766800499633
Coverage of cases: 87.67676767676768
Instances selection time: 8.0
Test time: 36.0
Accumulative iteration time: 231.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.038787878787878795
Kappa statistic: 0.5733333333333333
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 25.491276400367283
Incorrectly Classified Instances: 40.2020202020202
Correctly Classified Instances: 59.7979797979798
Weighted Precision: 0.6190136764681453
Weighted AreaUnderROC: 0.9265005611672279
Root mean squared error: 0.23487012120261985
Relative absolute error: 54.88690920749672
Root relative squared error: 81.69969910520902
Weighted TruePositiveRate: 0.597979797979798
Weighted MatthewsCorrelation: 0.5647638117471954
Weighted FMeasure: 0.5992546425181032
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5969417812606468
Mean absolute error: 0.09072216397933401
Coverage of cases: 87.87878787878788
Instances selection time: 15.0
Test time: 34.0
Accumulative iteration time: 247.0
Weighted Recall: 0.597979797979798
Weighted FalsePositiveRate: 0.040202020202020204
Kappa statistic: 0.5577777777777777
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 25.123966942148744
Incorrectly Classified Instances: 39.19191919191919
Correctly Classified Instances: 60.80808080808081
Weighted Precision: 0.624275658865664
Weighted AreaUnderROC: 0.9299708193041526
Root mean squared error: 0.23309659181779968
Relative absolute error: 54.23467503851537
Root relative squared error: 81.08277594634943
Weighted TruePositiveRate: 0.6080808080808081
Weighted MatthewsCorrelation: 0.5743633077097899
Weighted FMeasure: 0.6091961402454004
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5974773078990097
Mean absolute error: 0.08964409097275326
Coverage of cases: 88.08080808080808
Instances selection time: 7.0
Test time: 34.0
Accumulative iteration time: 255.0
Weighted Recall: 0.6080808080808081
Weighted FalsePositiveRate: 0.03919191919191919
Kappa statistic: 0.5688888888888889
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 25.6382001836547
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6235499610760759
Weighted AreaUnderROC: 0.9315735129068463
Root mean squared error: 0.2325050943572412
Relative absolute error: 54.515266737880964
Root relative squared error: 80.87702323373668
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5716211665371849
Weighted FMeasure: 0.6048607972794375
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6081773584964917
Mean absolute error: 0.09010787890558895
Coverage of cases: 88.68686868686869
Instances selection time: 5.0
Test time: 35.0
Accumulative iteration time: 261.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 24.848484848484844
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.6017199362119149
Weighted AreaUnderROC: 0.9234882154882156
Root mean squared error: 0.23808337420184172
Relative absolute error: 55.42616363432253
Root relative squared error: 82.81743090455893
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.543983041563586
Weighted FMeasure: 0.5805107209310127
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5837650098051399
Mean absolute error: 0.09161349361045104
Coverage of cases: 86.26262626262626
Instances selection time: 4.0
Test time: 35.0
Accumulative iteration time: 267.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.04222222222222222
Kappa statistic: 0.5355555555555555
Training time: 2.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 24.444444444444436
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.611735935928762
Weighted AreaUnderROC: 0.9218496071829406
Root mean squared error: 0.24047156179862542
Relative absolute error: 55.76993792134046
Root relative squared error: 83.64816325597495
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.5518247462855523
Weighted FMeasure: 0.5865214880245132
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5878373158132377
Mean absolute error: 0.09218171557246416
Coverage of cases: 86.66666666666667
Instances selection time: 4.0
Test time: 34.0
Accumulative iteration time: 272.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.04161616161616162
Kappa statistic: 0.5422222222222222
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 24.44444444444443
Incorrectly Classified Instances: 42.02020202020202
Correctly Classified Instances: 57.97979797979798
Weighted Precision: 0.6084638422957928
Weighted AreaUnderROC: 0.9193355780022445
Root mean squared error: 0.24196727029819046
Relative absolute error: 55.77061716547529
Root relative squared error: 84.1684462691478
Weighted TruePositiveRate: 0.5797979797979798
Weighted MatthewsCorrelation: 0.5477915712140734
Weighted FMeasure: 0.5829191628570835
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5840350172815235
Mean absolute error: 0.0921828382900424
Coverage of cases: 87.47474747474747
Instances selection time: 3.0
Test time: 34.0
Accumulative iteration time: 276.0
Weighted Recall: 0.5797979797979798
Weighted FalsePositiveRate: 0.04202020202020202
Kappa statistic: 0.5377777777777777
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 24.848484848484844
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.6044754279510038
Weighted AreaUnderROC: 0.9237934904601571
Root mean squared error: 0.24272029065363776
Relative absolute error: 56.99591476595159
Root relative squared error: 84.43038480839293
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.541621168562239
Weighted FMeasure: 0.5786480326376389
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5875697433043494
Mean absolute error: 0.09420812358008587
Coverage of cases: 87.27272727272727
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 279.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.042828282828282827
Kappa statistic: 0.5288888888888889
Training time: 2.0
		
Time end:Sun Oct 08 10.04.45 EEST 2017