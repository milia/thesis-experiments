Sun Oct 08 10.09.24 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.09.24 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.61616161616162
Correctly Classified Instances: 38.38383838383838
Weighted Precision: 0.44774092118797215
Weighted AreaUnderROC: 0.6611111111111112
Root mean squared error: 0.31883050757727527
Relative absolute error: 71.00000000000038
Root relative squared error: 110.90536506409354
Weighted TruePositiveRate: 0.3838383838383838
Weighted MatthewsCorrelation: 0.33102060412065554
Weighted FMeasure: 0.3700309907161204
Iteration time: 6.0
Weighted AreaUnderPRC: 0.2184627941056509
Mean absolute error: 0.11735537190082783
Coverage of cases: 67.87878787878788
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 6.0
Weighted Recall: 0.3838383838383838
Weighted FalsePositiveRate: 0.06161616161616161
Kappa statistic: 0.32222222222222224
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.4728414978887021
Weighted AreaUnderROC: 0.6877777777777776
Root mean squared error: 0.3083377774491064
Relative absolute error: 65.62222222222185
Root relative squared error: 107.25546319546031
Weighted TruePositiveRate: 0.43232323232323233
Weighted MatthewsCorrelation: 0.3793948241358308
Weighted FMeasure: 0.41709483841296924
Iteration time: 2.0
Weighted AreaUnderPRC: 0.25211396007723347
Mean absolute error: 0.10846648301193763
Coverage of cases: 65.25252525252525
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 8.0
Weighted Recall: 0.43232323232323233
Weighted FalsePositiveRate: 0.05676767676767677
Kappa statistic: 0.37555555555555553
Training time: 1.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 51.91919191919192
Correctly Classified Instances: 48.08080808080808
Weighted Precision: 0.5270073841291152
Weighted AreaUnderROC: 0.7144444444444445
Root mean squared error: 0.2965137708168922
Relative absolute error: 60.25629629629593
Root relative squared error: 103.14247607251961
Weighted TruePositiveRate: 0.4808080808080808
Weighted MatthewsCorrelation: 0.4352596302977167
Weighted FMeasure: 0.46824606563692617
Iteration time: 3.0
Weighted AreaUnderPRC: 0.29985280786494983
Mean absolute error: 0.09959718396082035
Coverage of cases: 64.84848484848484
Instances selection time: 3.0
Test time: 22.0
Accumulative iteration time: 11.0
Weighted Recall: 0.4808080808080808
Weighted FalsePositiveRate: 0.05191919191919192
Kappa statistic: 0.4288888888888889
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5954598628868805
Weighted AreaUnderROC: 0.7477777777777778
Root mean squared error: 0.27987522224875067
Relative absolute error: 53.65098039215628
Root relative squared error: 97.35474792470697
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.5086851714530661
Weighted FMeasure: 0.536865870719482
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3585243863493315
Mean absolute error: 0.08867930643331674
Coverage of cases: 63.43434343434343
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 40.4040404040404
Correctly Classified Instances: 59.5959595959596
Weighted Precision: 0.6281201562668115
Weighted AreaUnderROC: 0.7777777777777778
Root mean squared error: 0.26360167501236303
Relative absolute error: 47.660818713450084
Root relative squared error: 91.69398568820141
Weighted TruePositiveRate: 0.5959595959595959
Weighted MatthewsCorrelation: 0.5624286218431732
Weighted FMeasure: 0.5900996142611677
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4119664377458437
Mean absolute error: 0.07877821274950478
Coverage of cases: 62.02020202020202
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 13.0
Weighted Recall: 0.5959595959595959
Weighted FalsePositiveRate: 0.04040404040404041
Kappa statistic: 0.5555555555555555
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6629743498759867
Weighted AreaUnderROC: 0.8011111111111111
Root mean squared error: 0.2500623661144781
Relative absolute error: 42.93227513227513
Root relative squared error: 86.98432974139236
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6077783030707683
Weighted FMeasure: 0.6345706358871237
Iteration time: 1.0
Weighted AreaUnderPRC: 0.45954379483411717
Mean absolute error: 0.07096243823516596
Coverage of cases: 63.83838383838384
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.03616161616161616
Kappa statistic: 0.6022222222222222
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 32.525252525252526
Correctly Classified Instances: 67.47474747474747
Weighted Precision: 0.6810227598509376
Weighted AreaUnderROC: 0.8211111111111111
Root mean squared error: 0.23769218721221852
Relative absolute error: 38.84927536231874
Root relative squared error: 82.68135629795319
Weighted TruePositiveRate: 0.6747474747474748
Weighted MatthewsCorrelation: 0.6440009869950457
Weighted FMeasure: 0.6742670237482999
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5005548196226172
Mean absolute error: 0.06421367828482479
Coverage of cases: 67.47474747474747
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6747474747474748
Weighted FalsePositiveRate: 0.032525252525252527
Kappa statistic: 0.6422222222222222
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 30.303030303030305
Correctly Classified Instances: 69.6969696969697
Weighted Precision: 0.7049309655022357
Weighted AreaUnderROC: 0.8333333333333334
Root mean squared error: 0.22985250990263367
Relative absolute error: 36.26666666666648
Root relative squared error: 79.95432029186
Weighted TruePositiveRate: 0.696969696969697
Weighted MatthewsCorrelation: 0.6686199698293109
Weighted FMeasure: 0.6957303086649236
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5279950273773291
Mean absolute error: 0.0599449035812673
Coverage of cases: 69.6969696969697
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 16.0
Weighted Recall: 0.696969696969697
Weighted FalsePositiveRate: 0.030303030303030304
Kappa statistic: 0.6666666666666667
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7384331354332048
Weighted AreaUnderROC: 0.8533333333333332
Root mean squared error: 0.2159785036852538
Relative absolute error: 32.21234567901217
Root relative squared error: 75.12823970085168
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.7074017572934165
Weighted FMeasure: 0.731069718162314
Iteration time: 0.0
Weighted AreaUnderPRC: 0.5704975202797937
Mean absolute error: 0.053243546576879956
Coverage of cases: 73.33333333333333
Instances selection time: 0.0
Test time: 16.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.02666666666666666
Kappa statistic: 0.7066666666666666
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 22.828282828282827
Correctly Classified Instances: 77.17171717171718
Weighted Precision: 0.7759261793420191
Weighted AreaUnderROC: 0.8744444444444444
Root mean squared error: 0.20012623643570604
Relative absolute error: 27.951724137930757
Root relative squared error: 69.6140199363627
Weighted TruePositiveRate: 0.7717171717171717
Weighted MatthewsCorrelation: 0.7498451206891759
Weighted FMeasure: 0.770808285179303
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6246594037447111
Mean absolute error: 0.04620119692219989
Coverage of cases: 77.17171717171718
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7717171717171717
Weighted FalsePositiveRate: 0.02282828282828283
Kappa statistic: 0.7488888888888888
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 20.606060606060606
Correctly Classified Instances: 79.39393939393939
Weighted Precision: 0.7981525911632068
Weighted AreaUnderROC: 0.8866666666666666
Root mean squared error: 0.19036858917903912
Relative absolute error: 25.410752688172064
Root relative squared error: 66.21981704345099
Weighted TruePositiveRate: 0.793939393939394
Weighted MatthewsCorrelation: 0.77378520624237
Weighted FMeasure: 0.791673334955768
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6579662670407929
Mean absolute error: 0.042001244112681364
Coverage of cases: 79.39393939393939
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 17.0
Weighted Recall: 0.793939393939394
Weighted FalsePositiveRate: 0.020606060606060603
Kappa statistic: 0.7733333333333333
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.393939393939394
Correctly Classified Instances: 80.60606060606061
Weighted Precision: 0.8113929613911546
Weighted AreaUnderROC: 0.8933333333333333
Root mean squared error: 0.1848733217293293
Relative absolute error: 23.95555555555531
Root relative squared error: 64.30828527923553
Weighted TruePositiveRate: 0.806060606060606
Weighted MatthewsCorrelation: 0.7877918877720258
Weighted FMeasure: 0.8046784638625024
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6764904091928289
Mean absolute error: 0.03959595959595945
Coverage of cases: 80.60606060606061
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 17.0
Weighted Recall: 0.806060606060606
Weighted FalsePositiveRate: 0.019393939393939397
Kappa statistic: 0.7866666666666666
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 17.77777777777778
Correctly Classified Instances: 82.22222222222223
Weighted Precision: 0.826251991003696
Weighted AreaUnderROC: 0.902222222222222
Root mean squared error: 0.17716938776329294
Relative absolute error: 22.083809523809155
Root relative squared error: 61.62846766885253
Weighted TruePositiveRate: 0.8222222222222222
Weighted MatthewsCorrelation: 0.8045107252540161
Weighted FMeasure: 0.8191289796538359
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6991244195413083
Mean absolute error: 0.036502164502164126
Coverage of cases: 82.22222222222223
Instances selection time: 1.0
Test time: 19.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8222222222222222
Weighted FalsePositiveRate: 0.017777777777777778
Kappa statistic: 0.8044444444444444
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.757575757575758
Correctly Classified Instances: 84.24242424242425
Weighted Precision: 0.8476845477267656
Weighted AreaUnderROC: 0.9133333333333332
Root mean squared error: 0.16694736568235458
Relative absolute error: 19.790990990990757
Root relative squared error: 58.07273174133962
Weighted TruePositiveRate: 0.8424242424242424
Weighted MatthewsCorrelation: 0.8272907010793358
Weighted FMeasure: 0.8398183235172831
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7283382850587404
Mean absolute error: 0.03271238180329072
Coverage of cases: 84.24242424242425
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8424242424242424
Weighted FalsePositiveRate: 0.01575757575757576
Kappa statistic: 0.8266666666666667
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 14.545454545454545
Correctly Classified Instances: 85.45454545454545
Weighted Precision: 0.860176380224647
Weighted AreaUnderROC: 0.9199999999999999
Root mean squared error: 0.16051824340369225
Relative absolute error: 18.36923076923042
Root relative squared error: 55.83635806814612
Weighted TruePositiveRate: 0.8545454545454545
Weighted MatthewsCorrelation: 0.8406718362889664
Weighted FMeasure: 0.8517964959259494
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7478505225927052
Mean absolute error: 0.030362364907819073
Coverage of cases: 85.45454545454545
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8545454545454545
Weighted FalsePositiveRate: 0.014545454545454544
Kappa statistic: 0.84
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8709935663636978
Weighted AreaUnderROC: 0.9266666666666666
Root mean squared error: 0.15379046179175454
Relative absolute error: 16.95609756097539
Root relative squared error: 53.49609558381468
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.8533259561988574
Weighted FMeasure: 0.8631878655772515
Iteration time: 0.0
Weighted AreaUnderPRC: 0.766982390440377
Mean absolute error: 0.02802660753880248
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.01333333333333333
Kappa statistic: 0.8533333333333334
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.707070707070708
Correctly Classified Instances: 89.29292929292929
Weighted Precision: 0.8973978261020438
Weighted AreaUnderROC: 0.941111111111111
Root mean squared error: 0.13792563679441905
Relative absolute error: 14.034625322997467
Root relative squared error: 47.97750759994366
Weighted TruePositiveRate: 0.8929292929292929
Weighted MatthewsCorrelation: 0.8824487709701205
Weighted FMeasure: 0.8900139812040484
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8113227071451659
Mean absolute error: 0.023197727806607532
Coverage of cases: 89.29292929292929
Instances selection time: 0.0
Test time: 23.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8929292929292929
Weighted FalsePositiveRate: 0.010707070707070707
Kappa statistic: 0.8822222222222222
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.696969696969697
Correctly Classified Instances: 90.3030303030303
Weighted Precision: 0.9048747442578146
Weighted AreaUnderROC: 0.9466666666666664
Root mean squared error: 0.13133643926787233
Relative absolute error: 12.850370370370332
Root relative squared error: 45.685451664913764
Weighted TruePositiveRate: 0.9030303030303031
Weighted MatthewsCorrelation: 0.8930368137581783
Weighted FMeasure: 0.9008102438771678
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8274245142983458
Mean absolute error: 0.02124028160391804
Coverage of cases: 90.3030303030303
Instances selection time: 5.0
Test time: 32.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9030303030303031
Weighted FalsePositiveRate: 0.009696969696969697
Kappa statistic: 0.8933333333333334
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 8.080808080808081
Correctly Classified Instances: 91.91919191919192
Weighted Precision: 0.9213101892395799
Weighted AreaUnderROC: 0.9555555555555554
Root mean squared error: 0.11997409515282277
Relative absolute error: 11.021276595744652
Root relative squared error: 41.733054099075424
Weighted TruePositiveRate: 0.9191919191919192
Weighted MatthewsCorrelation: 0.9112574871175214
Weighted FMeasure: 0.9178916118914157
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8541137674738086
Mean absolute error: 0.018216986108668964
Coverage of cases: 91.91919191919192
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9191919191919192
Weighted FalsePositiveRate: 0.008080808080808083
Kappa statistic: 0.9111111111111111
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 8.282828282828282
Correctly Classified Instances: 91.71717171717172
Weighted Precision: 0.9191405924484132
Weighted AreaUnderROC: 0.9544444444444442
Root mean squared error: 0.1215041664962887
Relative absolute error: 11.151473922902522
Root relative squared error: 42.26529024614509
Weighted TruePositiveRate: 0.9171717171717172
Weighted MatthewsCorrelation: 0.9090289536444189
Weighted FMeasure: 0.9159735448555847
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8509389367385175
Mean absolute error: 0.018432188302318336
Coverage of cases: 91.71717171717172
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9171717171717172
Weighted FalsePositiveRate: 0.008282828282828286
Kappa statistic: 0.9088888888888889
Training time: 0.0
		
Time end:Sun Oct 08 10.09.25 EEST 2017