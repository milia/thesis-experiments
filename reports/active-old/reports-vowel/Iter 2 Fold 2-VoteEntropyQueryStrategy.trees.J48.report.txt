Sun Oct 08 10.10.20 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 10.10.20 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 26.519742883379404
Incorrectly Classified Instances: 60.80808080808081
Correctly Classified Instances: 39.19191919191919
Weighted Precision: 0.3874449648707465
Weighted AreaUnderROC: 0.7574814814814815
Root mean squared error: 0.28337586897829925
Relative absolute error: 70.74583934583896
Root relative squared error: 98.5724497890962
Weighted TruePositiveRate: 0.39191919191919194
Weighted MatthewsCorrelation: 0.3245276687840686
Weighted FMeasure: 0.3470636634634695
Iteration time: 7.0
Weighted AreaUnderPRC: 0.2946270619529981
Mean absolute error: 0.11693527164601555
Coverage of cases: 71.51515151515152
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7.0
Weighted Recall: 0.39191919191919194
Weighted FalsePositiveRate: 0.06080808080808081
Kappa statistic: 0.3311111111111112
Training time: 6.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 12.966023875114828
Incorrectly Classified Instances: 36.76767676767677
Correctly Classified Instances: 63.23232323232323
Weighted Precision: 0.6760680621240692
Weighted AreaUnderROC: 0.8672615039281705
Root mean squared error: 0.2385559456118906
Relative absolute error: 41.53736807854432
Root relative squared error: 82.98181512597044
Weighted TruePositiveRate: 0.6323232323232323
Weighted MatthewsCorrelation: 0.5789626899480771
Weighted FMeasure: 0.5838485137790548
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5452808270890732
Mean absolute error: 0.06865680674139601
Coverage of cases: 77.17171717171718
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6323232323232323
Weighted FalsePositiveRate: 0.036767676767676775
Kappa statistic: 0.5955555555555555
Training time: 31.0
		
Time end:Sun Oct 08 10.10.20 EEST 2017