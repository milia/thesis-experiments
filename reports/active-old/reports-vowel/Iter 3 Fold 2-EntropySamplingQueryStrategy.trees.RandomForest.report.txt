Sun Oct 08 10.01.29 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.29 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 45.28925619834722
Incorrectly Classified Instances: 57.37373737373738
Correctly Classified Instances: 42.62626262626262
Weighted Precision: 0.43417574041482376
Weighted AreaUnderROC: 0.8135780022446688
Root mean squared error: 0.25853108898205956
Relative absolute error: 77.30628745857265
Root relative squared error: 89.93017958616645
Weighted TruePositiveRate: 0.4262626262626263
Weighted MatthewsCorrelation: 0.3721689405711873
Weighted FMeasure: 0.42826720344587127
Iteration time: 34.0
Weighted AreaUnderPRC: 0.4357724722508809
Mean absolute error: 0.12777898753483166
Coverage of cases: 86.26262626262626
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 34.0
Weighted Recall: 0.4262626262626263
Weighted FalsePositiveRate: 0.05737373737373737
Kappa statistic: 0.36888888888888893
Training time: 28.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 44.187327823691625
Incorrectly Classified Instances: 58.18181818181818
Correctly Classified Instances: 41.81818181818182
Weighted Precision: 0.47069328350719947
Weighted AreaUnderROC: 0.8192817059483726
Root mean squared error: 0.25661490893404715
Relative absolute error: 76.62132329603962
Root relative squared error: 89.26363531671055
Weighted TruePositiveRate: 0.41818181818181815
Weighted MatthewsCorrelation: 0.3794606977446039
Weighted FMeasure: 0.42547868542267137
Iteration time: 17.0
Weighted AreaUnderPRC: 0.471811033802577
Mean absolute error: 0.12664681536535555
Coverage of cases: 85.25252525252525
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 51.0
Weighted Recall: 0.41818181818181815
Weighted FalsePositiveRate: 0.05818181818181817
Kappa statistic: 0.36
Training time: 15.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 40.00000000000017
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.4519398219047771
Weighted AreaUnderROC: 0.7575106621773289
Root mean squared error: 0.26268423401181196
Relative absolute error: 76.65906127349723
Root relative squared error: 91.37485333833924
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.3846486466941078
Weighted FMeasure: 0.4376898694141638
Iteration time: 17.0
Weighted AreaUnderPRC: 0.4399462752065155
Mean absolute error: 0.12670919218759955
Coverage of cases: 73.73737373737374
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 68.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.05656565656565657
Kappa statistic: 0.37777777777777777
Training time: 15.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 41.138659320477686
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.5144430327933552
Weighted AreaUnderROC: 0.8033153759820426
Root mean squared error: 0.25143786539100177
Relative absolute error: 73.4193263667799
Root relative squared error: 87.4627979111025
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.45146150009972724
Weighted FMeasure: 0.4944877618423692
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5063347758391694
Mean absolute error: 0.12135425845748822
Coverage of cases: 77.17171717171718
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 88.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 18.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 39.779614325069
Incorrectly Classified Instances: 42.42424242424242
Correctly Classified Instances: 57.57575757575758
Weighted Precision: 0.5786909317119959
Weighted AreaUnderROC: 0.9036116722783389
Root mean squared error: 0.22717601426223802
Relative absolute error: 65.23855353304425
Root relative squared error: 79.02329983103226
Weighted TruePositiveRate: 0.5757575757575758
Weighted MatthewsCorrelation: 0.5335284574353861
Weighted FMeasure: 0.5736362484401418
Iteration time: 27.0
Weighted AreaUnderPRC: 0.634580917039658
Mean absolute error: 0.10783231988933004
Coverage of cases: 94.34343434343434
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 115.0
Weighted Recall: 0.5757575757575758
Weighted FalsePositiveRate: 0.04242424242424243
Kappa statistic: 0.5333333333333333
Training time: 25.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 39.74288337924718
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6180882268591628
Weighted AreaUnderROC: 0.8859842873176206
Root mean squared error: 0.2269342944306084
Relative absolute error: 64.92562026497185
Root relative squared error: 78.93921745643824
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5741270045644228
Weighted FMeasure: 0.6091942727434531
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6431686042259471
Mean absolute error: 0.10731507481813599
Coverage of cases: 88.08080808080808
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 142.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.038787878787878795
Kappa statistic: 0.5733333333333333
Training time: 25.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 39.247015610652085
Incorrectly Classified Instances: 32.525252525252526
Correctly Classified Instances: 67.47474747474747
Weighted Precision: 0.6825321729617715
Weighted AreaUnderROC: 0.914514029180696
Root mean squared error: 0.21582620586707466
Relative absolute error: 61.26029094923035
Root relative squared error: 75.07526282215846
Weighted TruePositiveRate: 0.6747474747474748
Weighted MatthewsCorrelation: 0.6447052034463077
Weighted FMeasure: 0.6750318347789536
Iteration time: 33.0
Weighted AreaUnderPRC: 0.707626754307861
Mean absolute error: 0.10125667925492686
Coverage of cases: 91.31313131313131
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 175.0
Weighted Recall: 0.6747474747474748
Weighted FalsePositiveRate: 0.032525252525252527
Kappa statistic: 0.6422222222222222
Training time: 31.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 38.64095500459153
Incorrectly Classified Instances: 28.282828282828284
Correctly Classified Instances: 71.71717171717172
Weighted Precision: 0.7264467596835059
Weighted AreaUnderROC: 0.9405970819304151
Root mean squared error: 0.20531947073778475
Relative absolute error: 58.34541151001646
Root relative squared error: 71.42048930628589
Weighted TruePositiveRate: 0.7171717171717171
Weighted MatthewsCorrelation: 0.6913172364539966
Weighted FMeasure: 0.71615036832514
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7685647623924585
Mean absolute error: 0.09643869671077163
Coverage of cases: 93.53535353535354
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 211.0
Weighted Recall: 0.7171717171717171
Weighted FalsePositiveRate: 0.028282828282828285
Kappa statistic: 0.6888888888888889
Training time: 34.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 36.6207529843895
Incorrectly Classified Instances: 29.8989898989899
Correctly Classified Instances: 70.1010101010101
Weighted Precision: 0.7117670055912092
Weighted AreaUnderROC: 0.9126599326599325
Root mean squared error: 0.21059792641567002
Relative absolute error: 58.90850152620392
Root relative squared error: 73.25660297802608
Weighted TruePositiveRate: 0.701010101010101
Weighted MatthewsCorrelation: 0.6748546951200179
Weighted FMeasure: 0.7021360013896535
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7361692423318299
Mean absolute error: 0.09736942401025503
Coverage of cases: 90.9090909090909
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 247.0
Weighted Recall: 0.701010101010101
Weighted FalsePositiveRate: 0.0298989898989899
Kappa statistic: 0.6711111111111111
Training time: 34.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 35.97796143250704
Incorrectly Classified Instances: 29.09090909090909
Correctly Classified Instances: 70.9090909090909
Weighted Precision: 0.7132965813743847
Weighted AreaUnderROC: 0.9287182940516273
Root mean squared error: 0.20525858337437927
Relative absolute error: 56.247882647584504
Root relative squared error: 71.39930960388668
Weighted TruePositiveRate: 0.7090909090909091
Weighted MatthewsCorrelation: 0.6809172979896951
Weighted FMeasure: 0.7080576445968103
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7460994662263429
Mean absolute error: 0.09297170685551218
Coverage of cases: 92.72727272727273
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 285.0
Weighted Recall: 0.7090909090909091
Weighted FalsePositiveRate: 0.02909090909090909
Kappa statistic: 0.68
Training time: 36.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 36.45546372819114
Incorrectly Classified Instances: 25.858585858585858
Correctly Classified Instances: 74.14141414141415
Weighted Precision: 0.7450134470841133
Weighted AreaUnderROC: 0.9439304152637487
Root mean squared error: 0.1946584350260553
Relative absolute error: 53.54551519879488
Root relative squared error: 67.71204224908537
Weighted TruePositiveRate: 0.7414141414141414
Weighted MatthewsCorrelation: 0.7154274198188915
Weighted FMeasure: 0.7381927446467323
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7986672057192428
Mean absolute error: 0.08850498379966153
Coverage of cases: 94.54545454545455
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 332.0
Weighted Recall: 0.7414141414141414
Weighted FalsePositiveRate: 0.025858585858585855
Kappa statistic: 0.7155555555555555
Training time: 46.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 33.921028466483136
Incorrectly Classified Instances: 21.414141414141415
Correctly Classified Instances: 78.58585858585859
Weighted Precision: 0.7919826398452924
Weighted AreaUnderROC: 0.9506487093153758
Root mean squared error: 0.1865964493157082
Relative absolute error: 50.62188935512888
Root relative squared error: 64.90767614515838
Weighted TruePositiveRate: 0.7858585858585858
Weighted MatthewsCorrelation: 0.765865478992704
Weighted FMeasure: 0.7847018645995544
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8340874415928209
Mean absolute error: 0.08367254438864331
Coverage of cases: 94.94949494949495
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 386.0
Weighted Recall: 0.7858585858585858
Weighted FalsePositiveRate: 0.021414141414141417
Kappa statistic: 0.7644444444444444
Training time: 52.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.618916437098385
Incorrectly Classified Instances: 19.393939393939394
Correctly Classified Instances: 80.60606060606061
Weighted Precision: 0.8130140988850287
Weighted AreaUnderROC: 0.9639663299663301
Root mean squared error: 0.1807481106673997
Relative absolute error: 49.287054011450394
Root relative squared error: 62.8733283729274
Weighted TruePositiveRate: 0.806060606060606
Weighted MatthewsCorrelation: 0.7889095164901477
Weighted FMeasure: 0.8063079335922418
Iteration time: 57.0
Weighted AreaUnderPRC: 0.8588604080767183
Mean absolute error: 0.08146620497760448
Coverage of cases: 96.96969696969697
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 443.0
Weighted Recall: 0.806060606060606
Weighted FalsePositiveRate: 0.019393939393939397
Kappa statistic: 0.7866666666666666
Training time: 55.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.49862258953178
Incorrectly Classified Instances: 12.929292929292929
Correctly Classified Instances: 87.07070707070707
Weighted Precision: 0.8737816758404993
Weighted AreaUnderROC: 0.9797441077441078
Root mean squared error: 0.16793186684522696
Relative absolute error: 45.67868607779412
Root relative squared error: 58.415191005053344
Weighted TruePositiveRate: 0.8707070707070707
Weighted MatthewsCorrelation: 0.8586553096673393
Weighted FMeasure: 0.8705471405258705
Iteration time: 58.0
Weighted AreaUnderPRC: 0.9046083676297696
Mean absolute error: 0.07550196045916431
Coverage of cases: 98.18181818181819
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 501.0
Weighted Recall: 0.8707070707070707
Weighted FalsePositiveRate: 0.012929292929292931
Kappa statistic: 0.8577777777777778
Training time: 57.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.90266299357219
Incorrectly Classified Instances: 17.77777777777778
Correctly Classified Instances: 82.22222222222223
Weighted Precision: 0.8268964440918509
Weighted AreaUnderROC: 0.9708462401795734
Root mean squared error: 0.17404126343470605
Relative absolute error: 47.13838992992119
Root relative squared error: 60.540347923775386
Weighted TruePositiveRate: 0.8222222222222222
Weighted MatthewsCorrelation: 0.8054537760653738
Weighted FMeasure: 0.8211817346275286
Iteration time: 55.0
Weighted AreaUnderPRC: 0.88124592334399
Mean absolute error: 0.0779146940990438
Coverage of cases: 96.76767676767676
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 556.0
Weighted Recall: 0.8222222222222222
Weighted FalsePositiveRate: 0.017777777777777778
Kappa statistic: 0.8044444444444444
Training time: 54.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.094582185491355
Incorrectly Classified Instances: 16.565656565656564
Correctly Classified Instances: 83.43434343434343
Weighted Precision: 0.8424733983506572
Weighted AreaUnderROC: 0.9718877665544333
Root mean squared error: 0.17226560102939373
Relative absolute error: 46.45999416374983
Root relative squared error: 59.92268279258019
Weighted TruePositiveRate: 0.8343434343434344
Weighted MatthewsCorrelation: 0.8204470110517361
Weighted FMeasure: 0.8347672930914136
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8811859483847359
Mean absolute error: 0.07679337878305807
Coverage of cases: 97.77777777777777
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 620.0
Weighted Recall: 0.8343434343434344
Weighted FalsePositiveRate: 0.016565656565656568
Kappa statistic: 0.8177777777777778
Training time: 63.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 30.74380165289263
Incorrectly Classified Instances: 14.747474747474747
Correctly Classified Instances: 85.25252525252525
Weighted Precision: 0.8542090026739886
Weighted AreaUnderROC: 0.9805476992143659
Root mean squared error: 0.16224506530721114
Relative absolute error: 42.33627663315254
Root relative squared error: 56.43703400429071
Weighted TruePositiveRate: 0.8525252525252526
Weighted MatthewsCorrelation: 0.8377200407370861
Weighted FMeasure: 0.851003384184517
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9081714459197778
Mean absolute error: 0.06997731674901292
Coverage of cases: 97.17171717171718
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 687.0
Weighted Recall: 0.8525252525252526
Weighted FalsePositiveRate: 0.014747474747474749
Kappa statistic: 0.8377777777777778
Training time: 66.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 30.74380165289264
Incorrectly Classified Instances: 14.545454545454545
Correctly Classified Instances: 85.45454545454545
Weighted Precision: 0.8584197159396956
Weighted AreaUnderROC: 0.9719034792368128
Root mean squared error: 0.166049035941275
Relative absolute error: 43.09655528585986
Root relative squared error: 57.76024725345475
Weighted TruePositiveRate: 0.8545454545454545
Weighted MatthewsCorrelation: 0.8407884812963965
Weighted FMeasure: 0.8534535589582039
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8916518290624393
Mean absolute error: 0.07123397567910766
Coverage of cases: 96.96969696969697
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 752.0
Weighted Recall: 0.8545454545454545
Weighted FalsePositiveRate: 0.014545454545454545
Kappa statistic: 0.84
Training time: 65.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 30.798898071625434
Incorrectly Classified Instances: 14.141414141414142
Correctly Classified Instances: 85.85858585858585
Weighted Precision: 0.86322684966703
Weighted AreaUnderROC: 0.9805589225589225
Root mean squared error: 0.162107574755351
Relative absolute error: 42.28529985706272
Root relative squared error: 56.389207841221236
Weighted TruePositiveRate: 0.8585858585858586
Weighted MatthewsCorrelation: 0.845072576823885
Weighted FMeasure: 0.8566202331720583
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9105054145485079
Mean absolute error: 0.06989305761498014
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 822.0
Weighted Recall: 0.8585858585858586
Weighted FalsePositiveRate: 0.014141414141414142
Kappa statistic: 0.8444444444444444
Training time: 69.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.05601469237841
Incorrectly Classified Instances: 14.545454545454545
Correctly Classified Instances: 85.45454545454545
Weighted Precision: 0.8583103748154898
Weighted AreaUnderROC: 0.9830370370370369
Root mean squared error: 0.1600905590960691
Relative absolute error: 41.620593217785455
Root relative squared error: 55.68758784966998
Weighted TruePositiveRate: 0.8545454545454545
Weighted MatthewsCorrelation: 0.8409272762555237
Weighted FMeasure: 0.8539519386404487
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9135540385047711
Mean absolute error: 0.06879436895501773
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 897.0
Weighted Recall: 0.8545454545454545
Weighted FalsePositiveRate: 0.014545454545454545
Kappa statistic: 0.84
Training time: 74.0
		
Time end:Sun Oct 08 10.01.31 EEST 2017