Sun Oct 08 10.01.42 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.42 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 73.73737373737374
Correctly Classified Instances: 26.262626262626263
Weighted Precision: 0.2589286903216295
Weighted AreaUnderROC: 0.7470325476992143
Root mean squared error: 0.28023838947091917
Relative absolute error: 95.63232323232288
Root relative squared error: 97.48107583999955
Weighted TruePositiveRate: 0.26262626262626265
Weighted MatthewsCorrelation: 0.1834599397321075
Weighted FMeasure: 0.233956061336133
Iteration time: 1937.0
Weighted AreaUnderPRC: 0.23501659765264168
Mean absolute error: 0.1580699557559066
Coverage of cases: 93.93939393939394
Instances selection time: 25.0
Test time: 23.0
Accumulative iteration time: 1937.0
Weighted Recall: 0.26262626262626265
Weighted FalsePositiveRate: 0.07373737373737374
Kappa statistic: 0.1888888888888889
Training time: 1912.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.18549127639996
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5094746253527073
Weighted AreaUnderROC: 0.8807901234567901
Root mean squared error: 0.2736250354384065
Relative absolute error: 93.38181818181774
Root relative squared error: 95.18061705126162
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.47928690951421987
Weighted FMeasure: 0.5085019308551827
Iteration time: 608.0
Weighted AreaUnderPRC: 0.46200559363368565
Mean absolute error: 0.1543501126972204
Coverage of cases: 90.9090909090909
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 2545.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656566
Kappa statistic: 0.4977777777777777
Training time: 606.0
		
Time end:Sun Oct 08 10.01.45 EEST 2017