Sun Oct 08 10.02.18 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.02.18 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 73.73737373737374
Correctly Classified Instances: 26.262626262626263
Weighted Precision: 0.2589286903216295
Weighted AreaUnderROC: 0.7470325476992143
Root mean squared error: 0.28023838947091917
Relative absolute error: 95.63232323232288
Root relative squared error: 97.48107583999955
Weighted TruePositiveRate: 0.26262626262626265
Weighted MatthewsCorrelation: 0.1834599397321075
Weighted FMeasure: 0.233956061336133
Iteration time: 699.0
Weighted AreaUnderPRC: 0.23501659765264168
Mean absolute error: 0.1580699557559066
Coverage of cases: 93.93939393939394
Instances selection time: 14.0
Test time: 18.0
Accumulative iteration time: 699.0
Weighted Recall: 0.26262626262626265
Weighted FalsePositiveRate: 0.07373737373737374
Kappa statistic: 0.1888888888888889
Training time: 685.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.2589531680437
Incorrectly Classified Instances: 63.23232323232323
Correctly Classified Instances: 36.76767676767677
Weighted Precision: 0.35985557953104835
Weighted AreaUnderROC: 0.7854769921436588
Root mean squared error: 0.2782123656082295
Relative absolute error: 94.95353535353496
Root relative squared error: 96.77632234000487
Weighted TruePositiveRate: 0.36767676767676766
Weighted MatthewsCorrelation: 0.28611322041705456
Weighted FMeasure: 0.3196030328173938
Iteration time: 440.0
Weighted AreaUnderPRC: 0.29822413005542303
Mean absolute error: 0.1569479923198935
Coverage of cases: 93.53535353535354
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 1139.0
Weighted Recall: 0.36767676767676766
Weighted FalsePositiveRate: 0.06323232323232324
Kappa statistic: 0.30444444444444446
Training time: 435.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.09366391184534
Incorrectly Classified Instances: 63.23232323232323
Correctly Classified Instances: 36.76767676767677
Weighted Precision: 0.36936267583109544
Weighted AreaUnderROC: 0.8029090909090908
Root mean squared error: 0.277810108235482
Relative absolute error: 94.8202020202015
Root relative squared error: 96.63639689462235
Weighted TruePositiveRate: 0.36767676767676766
Weighted MatthewsCorrelation: 0.28358528643015224
Weighted FMeasure: 0.3103363959880008
Iteration time: 394.0
Weighted AreaUnderPRC: 0.31657995436328024
Mean absolute error: 0.1567276066449622
Coverage of cases: 94.34343434343434
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1533.0
Weighted Recall: 0.36767676767676766
Weighted FalsePositiveRate: 0.06323232323232324
Kappa statistic: 0.30444444444444446
Training time: 390.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.31404958677649
Incorrectly Classified Instances: 62.42424242424242
Correctly Classified Instances: 37.57575757575758
Weighted Precision: 0.34837428408555415
Weighted AreaUnderROC: 0.8097351290684623
Root mean squared error: 0.2759568361136492
Relative absolute error: 94.21414141414093
Root relative squared error: 95.99173518142304
Weighted TruePositiveRate: 0.37575757575757573
Weighted MatthewsCorrelation: 0.298800406754317
Weighted FMeasure: 0.3464180828189871
Iteration time: 392.0
Weighted AreaUnderPRC: 0.3144252656356952
Mean absolute error: 0.15572585357709343
Coverage of cases: 96.36363636363636
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1925.0
Weighted Recall: 0.37575757575757573
Weighted FalsePositiveRate: 0.062424242424242424
Kappa statistic: 0.3133333333333333
Training time: 388.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.35078053259834
Incorrectly Classified Instances: 63.63636363636363
Correctly Classified Instances: 36.36363636363637
Weighted Precision: 0.37219517278741504
Weighted AreaUnderROC: 0.8124107744107744
Root mean squared error: 0.2756127513844826
Relative absolute error: 94.09696969696911
Root relative squared error: 95.87204512167568
Weighted TruePositiveRate: 0.36363636363636365
Weighted MatthewsCorrelation: 0.2952081293266784
Weighted FMeasure: 0.33797995961381294
Iteration time: 405.0
Weighted AreaUnderPRC: 0.3192743796975536
Mean absolute error: 0.1555321813173053
Coverage of cases: 95.95959595959596
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2330.0
Weighted Recall: 0.36363636363636365
Weighted FalsePositiveRate: 0.06363636363636364
Kappa statistic: 0.3
Training time: 401.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.2589531680437
Incorrectly Classified Instances: 59.5959595959596
Correctly Classified Instances: 40.4040404040404
Weighted Precision: 0.4217090017063413
Weighted AreaUnderROC: 0.8261301907968573
Root mean squared error: 0.27443591985554655
Relative absolute error: 93.684848484848
Root relative squared error: 95.46268363576469
Weighted TruePositiveRate: 0.40404040404040403
Weighted MatthewsCorrelation: 0.33645425603649504
Weighted FMeasure: 0.3722729526978913
Iteration time: 409.0
Weighted AreaUnderPRC: 0.34639279542580803
Mean absolute error: 0.15485098923115473
Coverage of cases: 97.17171717171718
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 2739.0
Weighted Recall: 0.40404040404040403
Weighted FalsePositiveRate: 0.05959595959595959
Kappa statistic: 0.3444444444444445
Training time: 405.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.22222222222183
Incorrectly Classified Instances: 57.77777777777778
Correctly Classified Instances: 42.22222222222222
Weighted Precision: 0.4338612746635989
Weighted AreaUnderROC: 0.8322514029180695
Root mean squared error: 0.273981366836923
Relative absolute error: 93.54747474747428
Root relative squared error: 95.3045671215876
Weighted TruePositiveRate: 0.4222222222222222
Weighted MatthewsCorrelation: 0.36402337443131755
Weighted FMeasure: 0.41019437738463876
Iteration time: 414.0
Weighted AreaUnderPRC: 0.357166954527473
Mean absolute error: 0.1546239252024378
Coverage of cases: 96.96969696969697
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 3153.0
Weighted Recall: 0.4222222222222222
Weighted FalsePositiveRate: 0.05777777777777778
Kappa statistic: 0.36444444444444446
Training time: 410.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.36914600550928
Incorrectly Classified Instances: 55.95959595959596
Correctly Classified Instances: 44.04040404040404
Weighted Precision: 0.4637386121127785
Weighted AreaUnderROC: 0.8416947250280584
Root mean squared error: 0.2732049132948344
Relative absolute error: 93.28484848484801
Root relative squared error: 95.03447733565402
Weighted TruePositiveRate: 0.4404040404040404
Weighted MatthewsCorrelation: 0.38941091434179065
Weighted FMeasure: 0.43374963863573934
Iteration time: 415.0
Weighted AreaUnderPRC: 0.3707288603057952
Mean absolute error: 0.15418983220636134
Coverage of cases: 97.57575757575758
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3568.0
Weighted Recall: 0.4404040404040404
Weighted FalsePositiveRate: 0.05595959595959596
Kappa statistic: 0.3844444444444445
Training time: 412.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.33241505968745
Incorrectly Classified Instances: 55.55555555555556
Correctly Classified Instances: 44.44444444444444
Weighted Precision: 0.455208234309125
Weighted AreaUnderROC: 0.8534096520763188
Root mean squared error: 0.2726487907780267
Relative absolute error: 93.09898989898943
Root relative squared error: 94.84102981642008
Weighted TruePositiveRate: 0.4444444444444444
Weighted MatthewsCorrelation: 0.3900825287993558
Weighted FMeasure: 0.4382067335543698
Iteration time: 425.0
Weighted AreaUnderPRC: 0.374707331410045
Mean absolute error: 0.15388262793221494
Coverage of cases: 98.78787878787878
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3993.0
Weighted Recall: 0.4444444444444444
Weighted FalsePositiveRate: 0.05555555555555555
Kappa statistic: 0.38888888888888884
Training time: 422.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.47306836259201507
Weighted AreaUnderROC: 0.8702154882154882
Root mean squared error: 0.27201074586609697
Relative absolute error: 92.87676767676727
Root relative squared error: 94.61908554759033
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.4069101396239323
Weighted FMeasure: 0.4498528834906005
Iteration time: 502.0
Weighted AreaUnderPRC: 0.40063913135833085
Mean absolute error: 0.1535153184739965
Coverage of cases: 98.58585858585859
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4495.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939395
Kappa statistic: 0.40666666666666673
Training time: 499.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.14876033057816
Incorrectly Classified Instances: 52.72727272727273
Correctly Classified Instances: 47.27272727272727
Weighted Precision: 0.4728854425532064
Weighted AreaUnderROC: 0.8771515151515151
Root mean squared error: 0.2716900442057346
Relative absolute error: 92.76767676767635
Root relative squared error: 94.50752930101463
Weighted TruePositiveRate: 0.4727272727272727
Weighted MatthewsCorrelation: 0.4133184268462559
Weighted FMeasure: 0.45294332949596894
Iteration time: 439.0
Weighted AreaUnderPRC: 0.41449562839339066
Mean absolute error: 0.15333500292178007
Coverage of cases: 98.58585858585859
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4934.0
Weighted Recall: 0.4727272727272727
Weighted FalsePositiveRate: 0.05272727272727273
Kappa statistic: 0.42000000000000004
Training time: 436.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 52.121212121212125
Correctly Classified Instances: 47.878787878787875
Weighted Precision: 0.49123316825918173
Weighted AreaUnderROC: 0.8824691358024692
Root mean squared error: 0.27123514236738216
Relative absolute error: 92.61818181818136
Root relative squared error: 94.34929144970558
Weighted TruePositiveRate: 0.47878787878787876
Weighted MatthewsCorrelation: 0.42606665243420344
Weighted FMeasure: 0.4658286443808854
Iteration time: 440.0
Weighted AreaUnderPRC: 0.4163824073223651
Mean absolute error: 0.1530879038317057
Coverage of cases: 98.58585858585859
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5374.0
Weighted Recall: 0.47878787878787876
Weighted FalsePositiveRate: 0.052121212121212124
Kappa statistic: 0.42666666666666664
Training time: 438.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.07529843893444
Incorrectly Classified Instances: 52.92929292929293
Correctly Classified Instances: 47.07070707070707
Weighted Precision: 0.4978256803599959
Weighted AreaUnderROC: 0.8828866442199775
Root mean squared error: 0.2713226482976316
Relative absolute error: 92.64242424242373
Root relative squared error: 94.37973043502517
Weighted TruePositiveRate: 0.4707070707070707
Weighted MatthewsCorrelation: 0.4223794575625231
Weighted FMeasure: 0.457210417210902
Iteration time: 449.0
Weighted AreaUnderPRC: 0.4230723143660284
Mean absolute error: 0.15312797395442038
Coverage of cases: 98.78787878787878
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 5823.0
Weighted Recall: 0.4707070707070707
Weighted FalsePositiveRate: 0.05292929292929293
Kappa statistic: 0.4177777777777778
Training time: 446.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.20385674931093
Incorrectly Classified Instances: 51.717171717171716
Correctly Classified Instances: 48.282828282828284
Weighted Precision: 0.5092931006615815
Weighted AreaUnderROC: 0.8861548821548821
Root mean squared error: 0.2709637168008627
Relative absolute error: 92.52121212121159
Root relative squared error: 94.25487591911123
Weighted TruePositiveRate: 0.48282828282828283
Weighted MatthewsCorrelation: 0.4349092357731987
Weighted FMeasure: 0.46832582664513156
Iteration time: 458.0
Weighted AreaUnderPRC: 0.4356323847601971
Mean absolute error: 0.15292762334084659
Coverage of cases: 99.1919191919192
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6281.0
Weighted Recall: 0.48282828282828283
Weighted FalsePositiveRate: 0.051717171717171724
Kappa statistic: 0.4311111111111111
Training time: 456.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.13039485766723
Incorrectly Classified Instances: 50.101010101010104
Correctly Classified Instances: 49.898989898989896
Weighted Precision: 0.5281614159904795
Weighted AreaUnderROC: 0.8899483726150391
Root mean squared error: 0.27076422938752914
Relative absolute error: 92.45252525252481
Root relative squared error: 94.1854841141376
Weighted TruePositiveRate: 0.498989898989899
Weighted MatthewsCorrelation: 0.45107607404243355
Weighted FMeasure: 0.478544482523363
Iteration time: 460.0
Weighted AreaUnderPRC: 0.44960050147869923
Mean absolute error: 0.15281409132648827
Coverage of cases: 99.1919191919192
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 6741.0
Weighted Recall: 0.498989898989899
Weighted FalsePositiveRate: 0.050101010101010104
Kappa statistic: 0.44888888888888884
Training time: 458.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.14876033057816
Incorrectly Classified Instances: 50.505050505050505
Correctly Classified Instances: 49.494949494949495
Weighted Precision: 0.5268667964653385
Weighted AreaUnderROC: 0.8924579124579124
Root mean squared error: 0.27065635532229876
Relative absolute error: 92.41616161616116
Root relative squared error: 94.14796006201274
Weighted TruePositiveRate: 0.494949494949495
Weighted MatthewsCorrelation: 0.446166428244076
Weighted FMeasure: 0.471157990223521
Iteration time: 465.0
Weighted AreaUnderPRC: 0.4537439928627672
Mean absolute error: 0.15275398614241611
Coverage of cases: 99.1919191919192
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 7206.0
Weighted Recall: 0.494949494949495
Weighted FalsePositiveRate: 0.05050505050505051
Kappa statistic: 0.44444444444444453
Training time: 463.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.13039485766723
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.5120926958789183
Weighted AreaUnderROC: 0.8998630751964085
Root mean squared error: 0.2703931056883509
Relative absolute error: 92.32727272727223
Root relative squared error: 94.05638853399985
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.44991048467946065
Weighted FMeasure: 0.47890680001237806
Iteration time: 474.0
Weighted AreaUnderPRC: 0.45900677445647725
Mean absolute error: 0.15260706235912863
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 7680.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.04909090909090909
Kappa statistic: 0.4599999999999999
Training time: 473.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.07529843893442
Incorrectly Classified Instances: 47.878787878787875
Correctly Classified Instances: 52.121212121212125
Weighted Precision: 0.5268258041440703
Weighted AreaUnderROC: 0.9071245791245791
Root mean squared error: 0.2700091052871644
Relative absolute error: 92.20202020201968
Root relative squared error: 93.92281378608139
Weighted TruePositiveRate: 0.5212121212121212
Weighted MatthewsCorrelation: 0.4665655590833848
Weighted FMeasure: 0.49765867364386507
Iteration time: 486.0
Weighted AreaUnderPRC: 0.47054555999293396
Mean absolute error: 0.15240003339176905
Coverage of cases: 98.98989898989899
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 8166.0
Weighted Recall: 0.5212121212121212
Weighted FalsePositiveRate: 0.047878787878787875
Kappa statistic: 0.47333333333333333
Training time: 485.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.07529843893441
Incorrectly Classified Instances: 43.43434343434343
Correctly Classified Instances: 56.56565656565657
Weighted Precision: 0.5769011991158702
Weighted AreaUnderROC: 0.9103479236812569
Root mean squared error: 0.2695067665178115
Relative absolute error: 92.03636363636306
Root relative squared error: 93.74807497258368
Weighted TruePositiveRate: 0.5656565656565656
Weighted MatthewsCorrelation: 0.5234852966449027
Weighted FMeasure: 0.5587148933967591
Iteration time: 491.0
Weighted AreaUnderPRC: 0.5002258481736963
Mean absolute error: 0.1521262208865515
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 8657.0
Weighted Recall: 0.5656565656565656
Weighted FalsePositiveRate: 0.04343434343434343
Kappa statistic: 0.5222222222222221
Training time: 490.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.07529843893441
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.5731293399117872
Weighted AreaUnderROC: 0.9139191919191919
Root mean squared error: 0.2692581769405109
Relative absolute error: 91.94747474747415
Root relative squared error: 93.66160295323036
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5197864762265592
Weighted FMeasure: 0.5551563304906875
Iteration time: 507.0
Weighted AreaUnderPRC: 0.4947020401792816
Mean absolute error: 0.15197929710326402
Coverage of cases: 98.98989898989899
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 9164.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 507.0
		
Time end:Sun Oct 08 10.02.28 EEST 2017