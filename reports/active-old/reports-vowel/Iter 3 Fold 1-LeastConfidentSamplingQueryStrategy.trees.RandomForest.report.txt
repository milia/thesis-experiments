Sun Oct 08 10.04.32 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.04.32 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 39.026629935721054
Incorrectly Classified Instances: 65.05050505050505
Correctly Classified Instances: 34.94949494949495
Weighted Precision: 0.3689432112800045
Weighted AreaUnderROC: 0.7338047138047139
Root mean squared error: 0.275083176546608
Relative absolute error: 80.57674302549248
Root relative squared error: 95.6878322269639
Weighted TruePositiveRate: 0.34949494949494947
Weighted MatthewsCorrelation: 0.2912235890980325
Weighted FMeasure: 0.35007356324260114
Iteration time: 13.0
Weighted AreaUnderPRC: 0.3631498642445741
Mean absolute error: 0.1331846992156909
Coverage of cases: 69.6969696969697
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 13.0
Weighted Recall: 0.34949494949494947
Weighted FalsePositiveRate: 0.06505050505050505
Kappa statistic: 0.28444444444444444
Training time: 11.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 39.302112029384865
Incorrectly Classified Instances: 61.81818181818182
Correctly Classified Instances: 38.18181818181818
Weighted Precision: 0.40005470747814886
Weighted AreaUnderROC: 0.752736251402918
Root mean squared error: 0.2729592697964342
Relative absolute error: 78.84855972993869
Root relative squared error: 94.9490301114448
Weighted TruePositiveRate: 0.38181818181818183
Weighted MatthewsCorrelation: 0.3253583339222414
Weighted FMeasure: 0.3797322082715319
Iteration time: 15.0
Weighted AreaUnderPRC: 0.37720683268126937
Mean absolute error: 0.13032819790072595
Coverage of cases: 71.71717171717172
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 28.0
Weighted Recall: 0.38181818181818183
Weighted FalsePositiveRate: 0.06181818181818182
Kappa statistic: 0.32
Training time: 13.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 40.587695133149786
Incorrectly Classified Instances: 54.74747474747475
Correctly Classified Instances: 45.25252525252525
Weighted Precision: 0.46650338958432125
Weighted AreaUnderROC: 0.8049831649831651
Root mean squared error: 0.25596887795210155
Relative absolute error: 74.29075552082074
Root relative squared error: 89.03891308909239
Weighted TruePositiveRate: 0.45252525252525255
Weighted MatthewsCorrelation: 0.39956831470361753
Weighted FMeasure: 0.4456711908655372
Iteration time: 19.0
Weighted AreaUnderPRC: 0.4570546634775681
Mean absolute error: 0.12279463722449788
Coverage of cases: 79.5959595959596
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 47.0
Weighted Recall: 0.45252525252525255
Weighted FalsePositiveRate: 0.054747474747474746
Kappa statistic: 0.3977777777777778
Training time: 17.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 42.64462809917362
Incorrectly Classified Instances: 50.90909090909091
Correctly Classified Instances: 49.09090909090909
Weighted Precision: 0.5012533261833482
Weighted AreaUnderROC: 0.8075959595959595
Root mean squared error: 0.25194224910759083
Relative absolute error: 73.89501293973157
Root relative squared error: 87.63824806060596
Weighted TruePositiveRate: 0.4909090909090909
Weighted MatthewsCorrelation: 0.4411650100452342
Weighted FMeasure: 0.4846909997218523
Iteration time: 21.0
Weighted AreaUnderPRC: 0.4982886470963356
Mean absolute error: 0.12214051725575543
Coverage of cases: 79.79797979797979
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 68.0
Weighted Recall: 0.4909090909090909
Weighted FalsePositiveRate: 0.050909090909090904
Kappa statistic: 0.44000000000000006
Training time: 19.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 37.887970615243475
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.6283652924797547
Weighted AreaUnderROC: 0.8674545454545455
Root mean squared error: 0.2355354331544681
Relative absolute error: 66.9175132172242
Root relative squared error: 81.93112822867002
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.569072016544569
Weighted FMeasure: 0.6004754417007776
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6032968234868241
Mean absolute error: 0.1106074598631812
Coverage of cases: 87.67676767676768
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 93.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.04
Kappa statistic: 0.5599999999999999
Training time: 23.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 37.92470156106531
Incorrectly Classified Instances: 43.23232323232323
Correctly Classified Instances: 56.76767676767677
Weighted Precision: 0.5842022317315573
Weighted AreaUnderROC: 0.8547272727272728
Root mean squared error: 0.23669668664296065
Relative absolute error: 67.12520848058585
Root relative squared error: 82.3350708847598
Weighted TruePositiveRate: 0.5676767676767677
Weighted MatthewsCorrelation: 0.5306316421846651
Weighted FMeasure: 0.5703932788548971
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5809441578688158
Mean absolute error: 0.11095075781915088
Coverage of cases: 85.05050505050505
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 122.0
Weighted Recall: 0.5676767676767677
Weighted FalsePositiveRate: 0.04323232323232324
Kappa statistic: 0.5244444444444444
Training time: 27.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 36.12488521579442
Incorrectly Classified Instances: 33.93939393939394
Correctly Classified Instances: 66.06060606060606
Weighted Precision: 0.6905771314722885
Weighted AreaUnderROC: 0.9102671156004489
Root mean squared error: 0.21618461030837485
Relative absolute error: 60.023137998036056
Root relative squared error: 75.1999340015416
Weighted TruePositiveRate: 0.6606060606060606
Weighted MatthewsCorrelation: 0.636316285836531
Weighted FMeasure: 0.6619896949853522
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6994474432045023
Mean absolute error: 0.09921179834386189
Coverage of cases: 90.1010101010101
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 155.0
Weighted Recall: 0.6606060606060606
Weighted FalsePositiveRate: 0.03393939393939394
Kappa statistic: 0.6266666666666667
Training time: 31.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 37.759412304866984
Incorrectly Classified Instances: 34.54545454545455
Correctly Classified Instances: 65.45454545454545
Weighted Precision: 0.6697795259394221
Weighted AreaUnderROC: 0.9089876543209875
Root mean squared error: 0.2211219498118188
Relative absolute error: 61.83715294066714
Root relative squared error: 76.91739022690645
Weighted TruePositiveRate: 0.6545454545454545
Weighted MatthewsCorrelation: 0.6241339714420974
Weighted FMeasure: 0.6529066068248964
Iteration time: 35.0
Weighted AreaUnderPRC: 0.6653801239004771
Mean absolute error: 0.1022101701498637
Coverage of cases: 91.31313131313131
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 190.0
Weighted Recall: 0.6545454545454545
Weighted FalsePositiveRate: 0.034545454545454546
Kappa statistic: 0.62
Training time: 34.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 37.77777777777792
Incorrectly Classified Instances: 32.525252525252526
Correctly Classified Instances: 67.47474747474747
Weighted Precision: 0.6922792248608801
Weighted AreaUnderROC: 0.9238855218855219
Root mean squared error: 0.21045809096772408
Relative absolute error: 59.11339194673393
Root relative squared error: 73.2079611415808
Weighted TruePositiveRate: 0.6747474747474748
Weighted MatthewsCorrelation: 0.6466820227361169
Weighted FMeasure: 0.6721939495652607
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7269449785803338
Mean absolute error: 0.09770808586237076
Coverage of cases: 92.52525252525253
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 229.0
Weighted Recall: 0.6747474747474748
Weighted FalsePositiveRate: 0.03252525252525252
Kappa statistic: 0.6422222222222222
Training time: 38.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 36.41873278236924
Incorrectly Classified Instances: 30.303030303030305
Correctly Classified Instances: 69.6969696969697
Weighted Precision: 0.7310826646295996
Weighted AreaUnderROC: 0.9459887766554432
Root mean squared error: 0.2037997046932545
Relative absolute error: 56.931192268712856
Root relative squared error: 70.89183786304284
Weighted TruePositiveRate: 0.696969696969697
Weighted MatthewsCorrelation: 0.6773456061324826
Weighted FMeasure: 0.6979739540547935
Iteration time: 42.0
Weighted AreaUnderPRC: 0.7798116122289237
Mean absolute error: 0.09410114424580697
Coverage of cases: 95.55555555555556
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 271.0
Weighted Recall: 0.696969696969697
Weighted FalsePositiveRate: 0.030303030303030304
Kappa statistic: 0.6666666666666667
Training time: 40.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 37.70431588613422
Incorrectly Classified Instances: 27.474747474747474
Correctly Classified Instances: 72.52525252525253
Weighted Precision: 0.7548210656535372
Weighted AreaUnderROC: 0.9474163860830527
Root mean squared error: 0.204524689745147
Relative absolute error: 57.45166784308504
Root relative squared error: 71.14402430673397
Weighted TruePositiveRate: 0.7252525252525253
Weighted MatthewsCorrelation: 0.7079633259827985
Weighted FMeasure: 0.7285444633884102
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7673916284641515
Mean absolute error: 0.09496143445138085
Coverage of cases: 94.94949494949495
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 317.0
Weighted Recall: 0.7252525252525253
Weighted FalsePositiveRate: 0.027474747474747475
Kappa statistic: 0.6977777777777778
Training time: 44.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 34.83930211202952
Incorrectly Classified Instances: 28.88888888888889
Correctly Classified Instances: 71.11111111111111
Weighted Precision: 0.7161925023311445
Weighted AreaUnderROC: 0.9254343434343434
Root mean squared error: 0.20283800219095172
Relative absolute error: 55.56509477289623
Root relative squared error: 70.55730912577921
Weighted TruePositiveRate: 0.7111111111111111
Weighted MatthewsCorrelation: 0.684232303447659
Weighted FMeasure: 0.7123353573245196
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7536854626336293
Mean absolute error: 0.09184313185602742
Coverage of cases: 91.91919191919192
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 361.0
Weighted Recall: 0.7111111111111111
Weighted FalsePositiveRate: 0.028888888888888888
Kappa statistic: 0.6822222222222223
Training time: 43.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 36.12488521579443
Incorrectly Classified Instances: 23.636363636363637
Correctly Classified Instances: 76.36363636363636
Weighted Precision: 0.7734800004669179
Weighted AreaUnderROC: 0.9515712682379349
Root mean squared error: 0.19617315421474996
Relative absolute error: 54.3577208030729
Root relative squared error: 68.2389381407875
Weighted TruePositiveRate: 0.7636363636363637
Weighted MatthewsCorrelation: 0.7433695619794076
Weighted FMeasure: 0.7645422672109394
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7823126082170088
Mean absolute error: 0.08984747240177396
Coverage of cases: 96.56565656565657
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 410.0
Weighted Recall: 0.7636363636363637
Weighted FalsePositiveRate: 0.02363636363636364
Kappa statistic: 0.74
Training time: 48.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 32.13957759412313
Incorrectly Classified Instances: 16.96969696969697
Correctly Classified Instances: 83.03030303030303
Weighted Precision: 0.8360790060614407
Weighted AreaUnderROC: 0.9730594837261505
Root mean squared error: 0.1761101540765525
Relative absolute error: 46.99246140006602
Root relative squared error: 61.260012656160065
Weighted TruePositiveRate: 0.8303030303030303
Weighted MatthewsCorrelation: 0.8151866497337311
Weighted FMeasure: 0.8305142096332944
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8668144042203514
Mean absolute error: 0.07767348991746499
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 466.0
Weighted Recall: 0.8303030303030303
Weighted FalsePositiveRate: 0.016969696969696968
Kappa statistic: 0.8133333333333334
Training time: 55.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.278236914600654
Incorrectly Classified Instances: 16.363636363636363
Correctly Classified Instances: 83.63636363636364
Weighted Precision: 0.8472988020961667
Weighted AreaUnderROC: 0.9771200897867564
Root mean squared error: 0.1752997632932893
Relative absolute error: 47.50014158058795
Root relative squared error: 60.97811778246891
Weighted TruePositiveRate: 0.8363636363636363
Weighted MatthewsCorrelation: 0.823724829892457
Weighted FMeasure: 0.8373281675919033
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8805974785662416
Mean absolute error: 0.07851263071171613
Coverage of cases: 97.17171717171718
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 521.0
Weighted Recall: 0.8363636363636363
Weighted FalsePositiveRate: 0.016363636363636365
Kappa statistic: 0.82
Training time: 54.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 32.506887052341675
Incorrectly Classified Instances: 17.575757575757574
Correctly Classified Instances: 82.42424242424242
Weighted Precision: 0.8304528385589796
Weighted AreaUnderROC: 0.9849539842873178
Root mean squared error: 0.16931611699086282
Relative absolute error: 45.33514938093005
Root relative squared error: 58.89670316933255
Weighted TruePositiveRate: 0.8242424242424242
Weighted MatthewsCorrelation: 0.8084428526972222
Weighted FMeasure: 0.8239479136143537
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8970239072597538
Mean absolute error: 0.07493413120814932
Coverage of cases: 99.5959595959596
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 585.0
Weighted Recall: 0.8242424242424242
Weighted FalsePositiveRate: 0.017575757575757578
Kappa statistic: 0.8066666666666666
Training time: 63.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 32.341597796143326
Incorrectly Classified Instances: 15.757575757575758
Correctly Classified Instances: 84.24242424242425
Weighted Precision: 0.8507413545071394
Weighted AreaUnderROC: 0.9775465768799103
Root mean squared error: 0.17235331644796958
Relative absolute error: 46.05695822013608
Root relative squared error: 59.953194648527806
Weighted TruePositiveRate: 0.8424242424242424
Weighted MatthewsCorrelation: 0.8293343772799217
Weighted FMeasure: 0.8428679077197192
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8780571222672385
Mean absolute error: 0.0761272036696469
Coverage of cases: 98.58585858585859
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 649.0
Weighted Recall: 0.8424242424242424
Weighted FalsePositiveRate: 0.01575757575757576
Kappa statistic: 0.8266666666666667
Training time: 64.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 31.44168962350789
Incorrectly Classified Instances: 12.525252525252526
Correctly Classified Instances: 87.47474747474747
Weighted Precision: 0.878319188293825
Weighted AreaUnderROC: 0.9840920314253647
Root mean squared error: 0.16315845183199287
Relative absolute error: 43.088951488977514
Root relative squared error: 56.754756002556476
Weighted TruePositiveRate: 0.8747474747474747
Weighted MatthewsCorrelation: 0.8636700052845506
Weighted FMeasure: 0.8756861212365938
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9071915559911234
Mean absolute error: 0.071221407419798
Coverage of cases: 98.18181818181819
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 716.0
Weighted Recall: 0.8747474747474747
Weighted FalsePositiveRate: 0.012525252525252526
Kappa statistic: 0.8622222222222222
Training time: 66.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.62534435261714
Incorrectly Classified Instances: 13.93939393939394
Correctly Classified Instances: 86.06060606060606
Weighted Precision: 0.8751027632078039
Weighted AreaUnderROC: 0.9842356902356902
Root mean squared error: 0.16258461236804694
Relative absolute error: 43.01120365362241
Root relative squared error: 56.555145633646966
Weighted TruePositiveRate: 0.8606060606060606
Weighted MatthewsCorrelation: 0.8506583041316856
Weighted FMeasure: 0.8594572089113857
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9106725082754138
Mean absolute error: 0.07109289860102923
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 782.0
Weighted Recall: 0.8606060606060606
Weighted FalsePositiveRate: 0.013939393939393942
Kappa statistic: 0.8466666666666667
Training time: 66.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.955922865013843
Incorrectly Classified Instances: 15.151515151515152
Correctly Classified Instances: 84.84848484848484
Weighted Precision: 0.8526730901231562
Weighted AreaUnderROC: 0.9820089786756451
Root mean squared error: 0.16567127287989877
Relative absolute error: 43.83289597257402
Root relative squared error: 57.62884216757378
Weighted TruePositiveRate: 0.8484848484848485
Weighted MatthewsCorrelation: 0.8347978379280692
Weighted FMeasure: 0.8489868529079563
Iteration time: 74.0
Weighted AreaUnderPRC: 0.8959425802288965
Mean absolute error: 0.0724510677232633
Coverage of cases: 99.5959595959596
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 856.0
Weighted Recall: 0.8484848484848485
Weighted FalsePositiveRate: 0.015151515151515154
Kappa statistic: 0.8333333333333334
Training time: 74.0
		
Time end:Sun Oct 08 10.04.34 EEST 2017