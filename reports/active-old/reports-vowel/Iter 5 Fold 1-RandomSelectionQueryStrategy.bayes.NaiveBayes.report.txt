Sun Oct 08 10.07.43 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.07.43 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 18.38383838383845
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.5144881049347932
Weighted AreaUnderROC: 0.9005162738496074
Root mean squared error: 0.2657770350674912
Relative absolute error: 57.47617826840297
Root relative squared error: 92.45068586376838
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.45212876552053066
Weighted FMeasure: 0.4969051214108415
Iteration time: 1.0
Weighted AreaUnderPRC: 0.547025914721249
Mean absolute error: 0.0950019475510799
Coverage of cases: 72.32323232323232
Instances selection time: 1.0
Test time: 35.0
Accumulative iteration time: 1.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 18.18181818181824
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.5106618490200667
Weighted AreaUnderROC: 0.9009921436588104
Root mean squared error: 0.2660736857731988
Relative absolute error: 56.99489455421122
Root relative squared error: 92.55387597271694
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.4497771458396681
Weighted FMeasure: 0.4940492983090405
Iteration time: 1.0
Weighted AreaUnderPRC: 0.550218307049507
Mean absolute error: 0.09420643727968857
Coverage of cases: 73.53535353535354
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 2.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 19.632690541781503
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5179289176979059
Weighted AreaUnderROC: 0.9072188552188554
Root mean squared error: 0.2610113464460292
Relative absolute error: 56.58962616618292
Root relative squared error: 90.79293849084192
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.45714482402223505
Weighted FMeasure: 0.5008112883113489
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5505396072226822
Mean absolute error: 0.09353657217550955
Coverage of cases: 76.16161616161617
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 4.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.049696969696969705
Kappa statistic: 0.4533333333333333
Training time: 1.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 22.001836547291116
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5626389300372733
Weighted AreaUnderROC: 0.914716049382716
Root mean squared error: 0.2438898456679489
Relative absolute error: 54.04142770061071
Root relative squared error: 84.83721515474328
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5100715902781382
Weighted FMeasure: 0.5510520738295901
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5891672773835722
Mean absolute error: 0.08932467388530753
Coverage of cases: 81.21212121212122
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.044848484848484846
Kappa statistic: 0.5066666666666666
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 21.193755739210314
Incorrectly Classified Instances: 41.81818181818182
Correctly Classified Instances: 58.18181818181818
Weighted Precision: 0.5969091851605218
Weighted AreaUnderROC: 0.9165566778900113
Root mean squared error: 0.2416343526262988
Relative absolute error: 53.487248499230695
Root relative squared error: 84.05264067633298
Weighted TruePositiveRate: 0.5818181818181818
Weighted MatthewsCorrelation: 0.5444154783400689
Weighted FMeasure: 0.5807201757145196
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5902891893377314
Mean absolute error: 0.08840867520534056
Coverage of cases: 81.01010101010101
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5818181818181818
Weighted FalsePositiveRate: 0.04181818181818182
Kappa statistic: 0.5399999999999999
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 22.295684113865963
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5638816406722584
Weighted AreaUnderROC: 0.9190123456790124
Root mean squared error: 0.24068700597416087
Relative absolute error: 54.01918947347792
Root relative squared error: 83.72310562933896
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5126763069569861
Weighted FMeasure: 0.5523211213978406
Iteration time: 1.0
Weighted AreaUnderPRC: 0.587709451866002
Mean absolute error: 0.08928791648508805
Coverage of cases: 84.24242424242425
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.044444444444444446
Kappa statistic: 0.5111111111111111
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 22.38751147842059
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.5534440142500094
Weighted AreaUnderROC: 0.9220875420875421
Root mean squared error: 0.2413499666627671
Relative absolute error: 54.47462374424686
Root relative squared error: 83.9537168646054
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5073783689441377
Weighted FMeasure: 0.5492189376669705
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5848786858513768
Mean absolute error: 0.0900407004037144
Coverage of cases: 86.06060606060606
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 9.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646465
Kappa statistic: 0.5088888888888888
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 23.030303030303067
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5813171453834202
Weighted AreaUnderROC: 0.9257687991021325
Root mean squared error: 0.23617142634916202
Relative absolute error: 52.79389194026985
Root relative squared error: 82.15235880654599
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5316739084868561
Weighted FMeasure: 0.5707183261519556
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6062682053370504
Mean absolute error: 0.08726263130623173
Coverage of cases: 87.47474747474747
Instances selection time: 1.0
Test time: 34.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.04282828282828283
Kappa statistic: 0.5288888888888889
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 24.040404040404052
Incorrectly Classified Instances: 40.80808080808081
Correctly Classified Instances: 59.19191919191919
Weighted Precision: 0.6021743994154408
Weighted AreaUnderROC: 0.9274298540965208
Root mean squared error: 0.23416179847424518
Relative absolute error: 53.28616699410966
Root relative squared error: 81.45330865979484
Weighted TruePositiveRate: 0.591919191919192
Weighted MatthewsCorrelation: 0.5542547226171763
Weighted FMeasure: 0.5915014047735485
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6145033432319896
Mean absolute error: 0.08807630908117355
Coverage of cases: 89.0909090909091
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 11.0
Weighted Recall: 0.591919191919192
Weighted FalsePositiveRate: 0.040808080808080814
Kappa statistic: 0.5511111111111111
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 24.90358126721764
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6153073000194765
Weighted AreaUnderROC: 0.9336430976430975
Root mean squared error: 0.22905012866297697
Relative absolute error: 52.92497788148549
Root relative squared error: 79.6752115422585
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5678995155117272
Weighted FMeasure: 0.6035778095773663
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6345353302686706
Mean absolute error: 0.08747930228344765
Coverage of cases: 90.70707070707071
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 13.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.03959595959595961
Kappa statistic: 0.5644444444444444
Training time: 1.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 25.4178145087236
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6273126853156301
Weighted AreaUnderROC: 0.9372929292929293
Root mean squared error: 0.22453341438403987
Relative absolute error: 52.772265432363845
Root relative squared error: 78.10407002947741
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5788276494777993
Weighted FMeasure: 0.6139286087562702
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6547717434050538
Mean absolute error: 0.08722688501217221
Coverage of cases: 90.70707070707071
Instances selection time: 0.0
Test time: 34.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.03878787878787879
Kappa statistic: 0.5733333333333333
Training time: 1.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 25.730027548209353
Incorrectly Classified Instances: 36.36363636363637
Correctly Classified Instances: 63.63636363636363
Weighted Precision: 0.6464022059455978
Weighted AreaUnderROC: 0.9397485970819304
Root mean squared error: 0.2220008963367144
Relative absolute error: 52.43118915801887
Root relative squared error: 77.22313225252405
Weighted TruePositiveRate: 0.6363636363636364
Weighted MatthewsCorrelation: 0.6030520414135342
Weighted FMeasure: 0.6359694124625902
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6612586158788739
Mean absolute error: 0.08666312257523834
Coverage of cases: 91.51515151515152
Instances selection time: 0.0
Test time: 33.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6363636363636364
Weighted FalsePositiveRate: 0.03636363636363636
Kappa statistic: 0.6
Training time: 1.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 25.43617998163451
Incorrectly Classified Instances: 37.97979797979798
Correctly Classified Instances: 62.02020202020202
Weighted Precision: 0.6279696582748908
Weighted AreaUnderROC: 0.9377014590347924
Root mean squared error: 0.22401602174597676
Relative absolute error: 52.864484283468535
Root relative squared error: 77.92409471958032
Weighted TruePositiveRate: 0.6202020202020202
Weighted MatthewsCorrelation: 0.5844662526446652
Weighted FMeasure: 0.6195732441483942
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6500504911593765
Mean absolute error: 0.0873793128652378
Coverage of cases: 90.3030303030303
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6202020202020202
Weighted FalsePositiveRate: 0.03797979797979798
Kappa statistic: 0.5822222222222222
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 25.564738292011004
Incorrectly Classified Instances: 36.76767676767677
Correctly Classified Instances: 63.23232323232323
Weighted Precision: 0.6373980562567112
Weighted AreaUnderROC: 0.9378855218855219
Root mean squared error: 0.22368090946628127
Relative absolute error: 52.74767696263142
Root relative squared error: 77.80752573125012
Weighted TruePositiveRate: 0.6323232323232323
Weighted MatthewsCorrelation: 0.5967500805116985
Weighted FMeasure: 0.6311845782576893
Iteration time: 2.0
Weighted AreaUnderPRC: 0.649499576394564
Mean absolute error: 0.08718624291344092
Coverage of cases: 90.9090909090909
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6323232323232323
Weighted FalsePositiveRate: 0.036767676767676775
Kappa statistic: 0.5955555555555555
Training time: 1.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 26.207529843893468
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6129740465486747
Weighted AreaUnderROC: 0.9414545454545457
Root mean squared error: 0.22360726643789444
Relative absolute error: 53.13204686960186
Root relative squared error: 77.78190896386569
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5692314445180772
Weighted FMeasure: 0.6071658565862335
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6481317329251453
Mean absolute error: 0.08782156507372264
Coverage of cases: 93.53535353535354
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 1.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 26.280991735537174
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.633364778526096
Weighted AreaUnderROC: 0.9427654320987653
Root mean squared error: 0.2223689222403269
Relative absolute error: 52.771741243193425
Root relative squared error: 77.35115026279333
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5929602361323375
Weighted FMeasure: 0.6290262900950355
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6561148527645468
Mean absolute error: 0.08722601858379134
Coverage of cases: 93.93939393939394
Instances selection time: 0.0
Test time: 34.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.03717171717171717
Kappa statistic: 0.5911111111111111
Training time: 1.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 26.023875114784186
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6156925901038998
Weighted AreaUnderROC: 0.9433984287317622
Root mean squared error: 0.22180393039609533
Relative absolute error: 52.67966125002089
Root relative squared error: 77.15461754320233
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.572808178609441
Weighted FMeasure: 0.6099438988026382
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6572531484565438
Mean absolute error: 0.08707382024796897
Coverage of cases: 93.53535353535354
Instances selection time: 0.0
Test time: 34.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.03898989898989899
Kappa statistic: 0.5711111111111111
Training time: 1.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 26.299357208448107
Incorrectly Classified Instances: 38.58585858585859
Correctly Classified Instances: 61.41414141414141
Weighted Precision: 0.6208509617930935
Weighted AreaUnderROC: 0.9441795735129068
Root mean squared error: 0.2208273783947973
Relative absolute error: 52.398321339601864
Root relative squared error: 76.81492339965561
Weighted TruePositiveRate: 0.6141414141414141
Weighted MatthewsCorrelation: 0.5779113941336198
Weighted FMeasure: 0.6148605015028398
Iteration time: 2.0
Weighted AreaUnderPRC: 0.659056688721261
Mean absolute error: 0.08660879560264827
Coverage of cases: 93.33333333333333
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6141414141414141
Weighted FalsePositiveRate: 0.03858585858585859
Kappa statistic: 0.5755555555555555
Training time: 1.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 26.59320477502294
Incorrectly Classified Instances: 35.15151515151515
Correctly Classified Instances: 64.84848484848484
Weighted Precision: 0.6530487598661382
Weighted AreaUnderROC: 0.9471335578002245
Root mean squared error: 0.21575228955482295
Relative absolute error: 51.76695502625843
Root relative squared error: 75.04955099283333
Weighted TruePositiveRate: 0.6484848484848484
Weighted MatthewsCorrelation: 0.6148386754625764
Weighted FMeasure: 0.6486987942292327
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6837597096599204
Mean absolute error: 0.08556521491943597
Coverage of cases: 94.34343434343434
Instances selection time: 1.0
Test time: 33.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6484848484848484
Weighted FalsePositiveRate: 0.03515151515151515
Kappa statistic: 0.6133333333333333
Training time: 1.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 26.77685950413221
Incorrectly Classified Instances: 36.76767676767677
Correctly Classified Instances: 63.23232323232323
Weighted Precision: 0.6406100611015524
Weighted AreaUnderROC: 0.9492749719416387
Root mean squared error: 0.21469035607034717
Relative absolute error: 51.72761083612874
Root relative squared error: 74.68015685403365
Weighted TruePositiveRate: 0.6323232323232323
Weighted MatthewsCorrelation: 0.5989865234975975
Weighted FMeasure: 0.6346470957776837
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6900753843289236
Mean absolute error: 0.08550018320021334
Coverage of cases: 95.55555555555556
Instances selection time: 1.0
Test time: 32.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6323232323232323
Weighted FalsePositiveRate: 0.036767676767676775
Kappa statistic: 0.5955555555555555
Training time: 1.0
		
Time end:Sun Oct 08 10.07.45 EEST 2017