Sun Oct 08 10.09.05 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.09.05 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 75.95959595959596
Correctly Classified Instances: 24.04040404040404
Weighted Precision: 0.24301510944696933
Weighted AreaUnderROC: 0.7143299663299664
Root mean squared error: 0.2814114512424403
Relative absolute error: 96.04444444444384
Root relative squared error: 97.88912601374822
Weighted TruePositiveRate: 0.2404040404040404
Weighted MatthewsCorrelation: 0.1602740393244074
Weighted FMeasure: 0.2165380866520493
Iteration time: 384.0
Weighted AreaUnderPRC: 0.2049530238783278
Mean absolute error: 0.15875114784205696
Coverage of cases: 97.17171717171718
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 384.0
Weighted Recall: 0.2404040404040404
Weighted FalsePositiveRate: 0.07595959595959596
Kappa statistic: 0.16444444444444445
Training time: 383.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.62626262626233
Incorrectly Classified Instances: 72.72727272727273
Correctly Classified Instances: 27.272727272727273
Weighted Precision: 0.31786248278267965
Weighted AreaUnderROC: 0.7386689113355781
Root mean squared error: 0.2796068068850442
Relative absolute error: 95.47474747474702
Root relative squared error: 97.26137949479453
Weighted TruePositiveRate: 0.2727272727272727
Weighted MatthewsCorrelation: 0.21353993244525757
Weighted FMeasure: 0.2703198593215409
Iteration time: 381.0
Weighted AreaUnderPRC: 0.23286143977422907
Mean absolute error: 0.15780949995826055
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 765.0
Weighted Recall: 0.2727272727272727
Weighted FalsePositiveRate: 0.07272727272727272
Kappa statistic: 0.19999999999999998
Training time: 380.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.29568411386558
Incorrectly Classified Instances: 71.91919191919192
Correctly Classified Instances: 28.08080808080808
Weighted Precision: 0.31323878832256813
Weighted AreaUnderROC: 0.768909090909091
Root mean squared error: 0.27754708145974605
Relative absolute error: 94.74747474747429
Root relative squared error: 96.5449028879594
Weighted TruePositiveRate: 0.2808080808080808
Weighted MatthewsCorrelation: 0.21127075975170773
Weighted FMeasure: 0.26767998687843386
Iteration time: 386.0
Weighted AreaUnderPRC: 0.26197188337569516
Mean absolute error: 0.156607396276818
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1151.0
Weighted Recall: 0.2808080808080808
Weighted FalsePositiveRate: 0.07191919191919192
Kappa statistic: 0.20888888888888887
Training time: 385.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.20385674931089
Incorrectly Classified Instances: 64.04040404040404
Correctly Classified Instances: 35.95959595959596
Weighted Precision: 0.37945718191966454
Weighted AreaUnderROC: 0.7837014590347925
Root mean squared error: 0.2765756806222993
Relative absolute error: 94.41616161616108
Root relative squared error: 96.20700057955347
Weighted TruePositiveRate: 0.3595959595959596
Weighted MatthewsCorrelation: 0.2962290791497389
Weighted FMeasure: 0.346416403332284
Iteration time: 393.0
Weighted AreaUnderPRC: 0.3058731834269597
Mean absolute error: 0.15605977126638296
Coverage of cases: 98.38383838383838
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1544.0
Weighted Recall: 0.3595959595959596
Weighted FalsePositiveRate: 0.06404040404040404
Kappa statistic: 0.2955555555555555
Training time: 392.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.22222222222183
Incorrectly Classified Instances: 64.04040404040404
Correctly Classified Instances: 35.95959595959596
Weighted Precision: 0.4095414016000466
Weighted AreaUnderROC: 0.7977777777777777
Root mean squared error: 0.2759816957322894
Relative absolute error: 94.2181818181813
Root relative squared error: 96.00038261325633
Weighted TruePositiveRate: 0.3595959595959596
Weighted MatthewsCorrelation: 0.3026677006224187
Weighted FMeasure: 0.34753853800376766
Iteration time: 403.0
Weighted AreaUnderPRC: 0.3218467904768082
Mean absolute error: 0.15573253193087916
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1947.0
Weighted Recall: 0.3595959595959596
Weighted FalsePositiveRate: 0.06404040404040405
Kappa statistic: 0.2955555555555555
Training time: 402.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.31404958677652
Incorrectly Classified Instances: 62.62626262626262
Correctly Classified Instances: 37.37373737373738
Weighted Precision: 0.41186180738514394
Weighted AreaUnderROC: 0.8088462401795737
Root mean squared error: 0.274923510828153
Relative absolute error: 93.87474747474698
Root relative squared error: 95.63229242016173
Weighted TruePositiveRate: 0.37373737373737376
Weighted MatthewsCorrelation: 0.3225039395424965
Weighted FMeasure: 0.37165110010740493
Iteration time: 406.0
Weighted AreaUnderPRC: 0.34776512221419037
Mean absolute error: 0.15516487185908692
Coverage of cases: 97.37373737373737
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 2353.0
Weighted Recall: 0.37373737373737376
Weighted FalsePositiveRate: 0.06262626262626263
Kappa statistic: 0.31111111111111117
Training time: 405.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.33241505968743
Incorrectly Classified Instances: 60.80808080808081
Correctly Classified Instances: 39.19191919191919
Weighted Precision: 0.42910963029392046
Weighted AreaUnderROC: 0.8121077441077441
Root mean squared error: 0.27453810715228727
Relative absolute error: 93.7414141414137
Root relative squared error: 95.49822954238468
Weighted TruePositiveRate: 0.39191919191919194
Weighted MatthewsCorrelation: 0.34381966504980843
Weighted FMeasure: 0.3948029058679316
Iteration time: 415.0
Weighted AreaUnderPRC: 0.3540739209014012
Mean absolute error: 0.15494448618415585
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2768.0
Weighted Recall: 0.39191919191919194
Weighted FalsePositiveRate: 0.060808080808080804
Kappa statistic: 0.3311111111111112
Training time: 414.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.58953168044046
Incorrectly Classified Instances: 56.56565656565657
Correctly Classified Instances: 43.43434343434343
Weighted Precision: 0.46053160105747865
Weighted AreaUnderROC: 0.8181010101010102
Root mean squared error: 0.2741266931927664
Relative absolute error: 93.60404040403989
Root relative squared error: 95.35511897332451
Weighted TruePositiveRate: 0.43434343434343436
Weighted MatthewsCorrelation: 0.3842420357332539
Weighted FMeasure: 0.4288633657023058
Iteration time: 420.0
Weighted AreaUnderPRC: 0.38174135882330257
Mean absolute error: 0.15471742215543882
Coverage of cases: 97.97979797979798
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 3188.0
Weighted Recall: 0.43434343434343436
Weighted FalsePositiveRate: 0.05656565656565657
Kappa statistic: 0.37777777777777777
Training time: 419.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.44260789715301
Incorrectly Classified Instances: 57.37373737373738
Correctly Classified Instances: 42.62626262626262
Weighted Precision: 0.4654943865386811
Weighted AreaUnderROC: 0.8220336700336701
Root mean squared error: 0.2737674462404937
Relative absolute error: 93.48686868686822
Root relative squared error: 95.2301547260424
Weighted TruePositiveRate: 0.4262626262626263
Weighted MatthewsCorrelation: 0.38466945310869705
Weighted FMeasure: 0.4356289868621112
Iteration time: 427.0
Weighted AreaUnderPRC: 0.3705282625228689
Mean absolute error: 0.15452374989565093
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 3615.0
Weighted Recall: 0.4262626262626263
Weighted FalsePositiveRate: 0.05737373737373737
Kappa statistic: 0.36888888888888893
Training time: 426.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.44260789715298
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4694727789432669
Weighted AreaUnderROC: 0.8373153759820428
Root mean squared error: 0.2730130678306596
Relative absolute error: 93.23232323232273
Root relative squared error: 94.96774378684192
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.4018290618201877
Weighted FMeasure: 0.4509095487978178
Iteration time: 433.0
Weighted AreaUnderPRC: 0.3768653176833658
Mean absolute error: 0.15410301360714598
Coverage of cases: 98.58585858585859
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 4048.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.05494949494949495
Kappa statistic: 0.39555555555555555
Training time: 432.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.47933884297485
Incorrectly Classified Instances: 56.36363636363637
Correctly Classified Instances: 43.63636363636363
Weighted Precision: 0.4575196664234891
Weighted AreaUnderROC: 0.8490325476992142
Root mean squared error: 0.2726327576438526
Relative absolute error: 93.10303030302985
Root relative squared error: 94.83545268199775
Weighted TruePositiveRate: 0.43636363636363634
Weighted MatthewsCorrelation: 0.3866977566765281
Weighted FMeasure: 0.4362947032008764
Iteration time: 446.0
Weighted AreaUnderPRC: 0.387150071742702
Mean absolute error: 0.15388930628600075
Coverage of cases: 98.98989898989899
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 4494.0
Weighted Recall: 0.43636363636363634
Weighted FalsePositiveRate: 0.056363636363636366
Kappa statistic: 0.38
Training time: 446.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.31404958677648
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.478761046468791
Weighted AreaUnderROC: 0.8599977553310886
Root mean squared error: 0.27194020609044883
Relative absolute error: 92.86868686868635
Root relative squared error: 94.59454824835495
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.4121733414467825
Weighted FMeasure: 0.4597023678689332
Iteration time: 446.0
Weighted AreaUnderPRC: 0.4086924097017724
Mean absolute error: 0.1535019617664247
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 4940.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939395
Kappa statistic: 0.40666666666666673
Training time: 446.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 82.2589531680437
Incorrectly Classified Instances: 53.73737373737374
Correctly Classified Instances: 46.26262626262626
Weighted Precision: 0.4815526411463365
Weighted AreaUnderROC: 0.8618518518518519
Root mean squared error: 0.27184016896204843
Relative absolute error: 92.8323232323227
Root relative squared error: 94.55975027895887
Weighted TruePositiveRate: 0.4626262626262626
Weighted MatthewsCorrelation: 0.4162750590493228
Weighted FMeasure: 0.4663679941026804
Iteration time: 451.0
Weighted AreaUnderPRC: 0.4141839187578723
Mean absolute error: 0.15344185658235254
Coverage of cases: 99.1919191919192
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 5391.0
Weighted Recall: 0.4626262626262626
Weighted FalsePositiveRate: 0.05373737373737374
Kappa statistic: 0.40888888888888886
Training time: 451.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 82.31404958677649
Incorrectly Classified Instances: 50.707070707070706
Correctly Classified Instances: 49.292929292929294
Weighted Precision: 0.5148970564877088
Weighted AreaUnderROC: 0.8646509539842873
Root mean squared error: 0.27159818593658197
Relative absolute error: 92.74343434343378
Root relative squared error: 94.4755763522459
Weighted TruePositiveRate: 0.49292929292929294
Weighted MatthewsCorrelation: 0.4516572259636992
Weighted FMeasure: 0.49966285230687174
Iteration time: 459.0
Weighted AreaUnderPRC: 0.43186292178908553
Mean absolute error: 0.1532949327990651
Coverage of cases: 97.57575757575758
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 5850.0
Weighted Recall: 0.49292929292929294
Weighted FalsePositiveRate: 0.050707070707070694
Kappa statistic: 0.4422222222222223
Training time: 458.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.31404958677648
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5234452742328841
Weighted AreaUnderROC: 0.8762042648709316
Root mean squared error: 0.27095273760646543
Relative absolute error: 92.52525252525196
Root relative squared error: 94.25105680038263
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.46456478412047486
Weighted FMeasure: 0.5113325125229827
Iteration time: 466.0
Weighted AreaUnderPRC: 0.44319401454426577
Mean absolute error: 0.15293430169463232
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 6316.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.04929292929292929
Kappa statistic: 0.4577777777777778
Training time: 465.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5405827840966728
Weighted AreaUnderROC: 0.8894545454545455
Root mean squared error: 0.27024509761136567
Relative absolute error: 92.29090909090849
Root relative squared error: 94.00490384410861
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4832844361401505
Weighted FMeasure: 0.526538004765588
Iteration time: 475.0
Weighted AreaUnderPRC: 0.4630486188456525
Mean absolute error: 0.15254695717505634
Coverage of cases: 99.79797979797979
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 6791.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 474.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.24058769513277
Incorrectly Classified Instances: 45.25252525252525
Correctly Classified Instances: 54.74747474747475
Weighted Precision: 0.5726953368139621
Weighted AreaUnderROC: 0.8951806958473625
Root mean squared error: 0.26988293337919694
Relative absolute error: 92.17373737373677
Root relative squared error: 93.8789248194317
Weighted TruePositiveRate: 0.5474747474747474
Weighted MatthewsCorrelation: 0.5120617765053898
Weighted FMeasure: 0.5522866061901338
Iteration time: 486.0
Weighted AreaUnderPRC: 0.48107495637018893
Mean absolute error: 0.15235328491526837
Coverage of cases: 99.79797979797979
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 7277.0
Weighted Recall: 0.5474747474747474
Weighted FalsePositiveRate: 0.045252525252525266
Kappa statistic: 0.5022222222222221
Training time: 486.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.18549127639999
Incorrectly Classified Instances: 43.43434343434343
Correctly Classified Instances: 56.56565656565657
Weighted Precision: 0.5824038450663557
Weighted AreaUnderROC: 0.8946980920314254
Root mean squared error: 0.2698210628554247
Relative absolute error: 92.15353535353474
Root relative squared error: 93.85740312416543
Weighted TruePositiveRate: 0.5656565656565656
Weighted MatthewsCorrelation: 0.528657142971147
Weighted FMeasure: 0.5684160563526205
Iteration time: 491.0
Weighted AreaUnderPRC: 0.4887163614038571
Mean absolute error: 0.1523198931463394
Coverage of cases: 99.5959595959596
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 7768.0
Weighted Recall: 0.5656565656565656
Weighted FalsePositiveRate: 0.04343434343434343
Kappa statistic: 0.5222222222222221
Training time: 490.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.16712580348906
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5910514193407356
Weighted AreaUnderROC: 0.8985476992143658
Root mean squared error: 0.2695642047370708
Relative absolute error: 92.0606060606055
Root relative squared error: 93.76805488832007
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5359541191677473
Weighted FMeasure: 0.5746768952134975
Iteration time: 497.0
Weighted AreaUnderPRC: 0.49684560453634685
Mean absolute error: 0.15216629100926626
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8265.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.042828282828282827
Kappa statistic: 0.5288888888888889
Training time: 496.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.11202938475624
Incorrectly Classified Instances: 41.61616161616162
Correctly Classified Instances: 58.38383838383838
Weighted Precision: 0.5947009764826975
Weighted AreaUnderROC: 0.9030370370370371
Root mean squared error: 0.26937089329857283
Relative absolute error: 91.99999999999937
Root relative squared error: 93.70081139954418
Weighted TruePositiveRate: 0.5838383838383838
Weighted MatthewsCorrelation: 0.546201199702582
Weighted FMeasure: 0.5853259104768739
Iteration time: 511.0
Weighted AreaUnderPRC: 0.5077290799111414
Mean absolute error: 0.15206611570247927
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8776.0
Weighted Recall: 0.5838383838383838
Weighted FalsePositiveRate: 0.041616161616161614
Kappa statistic: 0.5422222222222222
Training time: 510.0
		
Time end:Sun Oct 08 10.09.14 EEST 2017