Sun Oct 08 10.09.27 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.09.27 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.41414141414141
Correctly Classified Instances: 38.58585858585859
Weighted Precision: 0.3957840572844725
Weighted AreaUnderROC: 0.6622222222222223
Root mean squared error: 0.3183116618183919
Relative absolute error: 70.80000000000037
Root relative squared error: 110.72488428533063
Weighted TruePositiveRate: 0.38585858585858585
Weighted MatthewsCorrelation: 0.3261796785093598
Weighted FMeasure: 0.3816415666895352
Iteration time: 1.0
Weighted AreaUnderPRC: 0.21135889601958582
Mean absolute error: 0.11702479338843111
Coverage of cases: 66.66666666666667
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1.0
Weighted Recall: 0.38585858585858585
Weighted FalsePositiveRate: 0.06141414141414142
Kappa statistic: 0.3244444444444445
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 54.74747474747475
Correctly Classified Instances: 45.25252525252525
Weighted Precision: 0.47191400403465317
Weighted AreaUnderROC: 0.6988888888888889
Root mean squared error: 0.3028364091896311
Relative absolute error: 63.58803418803382
Root relative squared error: 105.34180926125752
Weighted TruePositiveRate: 0.45252525252525255
Weighted MatthewsCorrelation: 0.4037187835123658
Weighted FMeasure: 0.45173537780008827
Iteration time: 1.0
Weighted AreaUnderPRC: 0.26306232529567253
Mean absolute error: 0.10510418874055244
Coverage of cases: 67.67676767676768
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.45252525252525255
Weighted FalsePositiveRate: 0.05474747474747475
Kappa statistic: 0.3977777777777778
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5494631046856516
Weighted AreaUnderROC: 0.7288888888888889
Root mean squared error: 0.2889559857095538
Relative absolute error: 57.579259259259
Root relative squared error: 100.51349642193777
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.4706124366965016
Weighted FMeasure: 0.5067849035506674
Iteration time: 0.0
Weighted AreaUnderPRC: 0.3165990612059833
Mean absolute error: 0.09517232935414771
Coverage of cases: 66.46464646464646
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 2.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.0492929292929293
Kappa statistic: 0.4577777777777778
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 47.27272727272727
Correctly Classified Instances: 52.72727272727273
Weighted Precision: 0.5785383616900652
Weighted AreaUnderROC: 0.74
Root mean squared error: 0.28413893900376175
Relative absolute error: 55.10588235294045
Root relative squared error: 98.83788411150924
Weighted TruePositiveRate: 0.5272727272727272
Weighted MatthewsCorrelation: 0.49706879455194175
Weighted FMeasure: 0.5305461156509704
Iteration time: 1.0
Weighted AreaUnderPRC: 0.34105006303374164
Mean absolute error: 0.09108410306271207
Coverage of cases: 62.62626262626262
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5272727272727272
Weighted FalsePositiveRate: 0.04727272727272727
Kappa statistic: 0.4799999999999999
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5930036371193278
Weighted AreaUnderROC: 0.7533333333333334
Root mean squared error: 0.27766673350047133
Relative absolute error: 52.26666666666645
Root relative squared error: 96.58652391525109
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5202596637775612
Weighted FMeasure: 0.55357105844445
Iteration time: 1.0
Weighted AreaUnderPRC: 0.3641292357059483
Mean absolute error: 0.08639118457300296
Coverage of cases: 60.4040404040404
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6487810615903808
Weighted AreaUnderROC: 0.7822222222222222
Root mean squared error: 0.2616263889775004
Relative absolute error: 46.5121693121693
Root relative squared error: 91.00688136914765
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5810248833901815
Weighted FMeasure: 0.611154222660011
Iteration time: 0.0
Weighted AreaUnderPRC: 0.4278510325005428
Mean absolute error: 0.076879618697801
Coverage of cases: 60.4040404040404
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 4.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6749285229086568
Weighted AreaUnderROC: 0.8011111111111111
Root mean squared error: 0.25058519624284553
Relative absolute error: 42.657971014492645
Root relative squared error: 87.1661964852423
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6161556790949886
Weighted FMeasure: 0.6452643146423154
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4678798337968081
Mean absolute error: 0.07050904299916184
Coverage of cases: 63.83838383838384
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.03616161616161616
Kappa statistic: 0.6022222222222222
Training time: 0.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.7132047393843256
Weighted AreaUnderROC: 0.8288888888888889
Root mean squared error: 0.23288788649641687
Relative absolute error: 37.11644444444428
Root relative squared error: 81.01017768705906
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.6671678922125369
Weighted FMeasure: 0.6939574997793446
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5236844644815261
Mean absolute error: 0.06134949494949506
Coverage of cases: 68.88888888888889
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.03111111111111111
Kappa statistic: 0.6577777777777778
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 28.484848484848484
Correctly Classified Instances: 71.51515151515152
Weighted Precision: 0.734914825360604
Weighted AreaUnderROC: 0.8433333333333333
Root mean squared error: 0.22319905699488934
Relative absolute error: 34.13086419753067
Root relative squared error: 77.63991308761436
Weighted TruePositiveRate: 0.7151515151515152
Weighted MatthewsCorrelation: 0.6938700518642693
Weighted FMeasure: 0.7181891406047854
Iteration time: 1.0
Weighted AreaUnderPRC: 0.556498294573268
Mean absolute error: 0.056414651566166756
Coverage of cases: 71.51515151515152
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7151515151515152
Weighted FalsePositiveRate: 0.02848484848484849
Kappa statistic: 0.6866666666666668
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 25.656565656565657
Correctly Classified Instances: 74.34343434343434
Weighted Precision: 0.7650638324734106
Weighted AreaUnderROC: 0.8588888888888887
Root mean squared error: 0.21212682276605754
Relative absolute error: 30.944827586206614
Root relative squared error: 73.78843040311587
Weighted TruePositiveRate: 0.7434343434343434
Weighted MatthewsCorrelation: 0.7259516234494879
Weighted FMeasure: 0.7475202599904621
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5973389206784592
Mean absolute error: 0.05114847534910216
Coverage of cases: 74.34343434343434
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7434343434343434
Weighted FalsePositiveRate: 0.025656565656565655
Kappa statistic: 0.7177777777777777
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 22.424242424242426
Correctly Classified Instances: 77.57575757575758
Weighted Precision: 0.7982963344658545
Weighted AreaUnderROC: 0.8766666666666666
Root mean squared error: 0.19856657024081067
Relative absolute error: 27.339784946236648
Root relative squared error: 69.07148920416458
Weighted TruePositiveRate: 0.7757575757575758
Weighted MatthewsCorrelation: 0.7621969421015474
Weighted FMeasure: 0.780854838643767
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6476875803546979
Mean absolute error: 0.045189727183862514
Coverage of cases: 77.57575757575758
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7757575757575758
Weighted FalsePositiveRate: 0.022424242424242423
Kappa statistic: 0.7533333333333333
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 18.585858585858585
Correctly Classified Instances: 81.41414141414141
Weighted Precision: 0.8264746714869968
Weighted AreaUnderROC: 0.8977777777777778
Root mean squared error: 0.18099138611765783
Relative absolute error: 23.096296296296078
Root relative squared error: 62.95795187030546
Weighted TruePositiveRate: 0.8141414141414142
Weighted MatthewsCorrelation: 0.8003231817559429
Weighted FMeasure: 0.8167649719010707
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6995608340404259
Mean absolute error: 0.03817569635751442
Coverage of cases: 81.41414141414141
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8141414141414142
Weighted FalsePositiveRate: 0.018585858585858588
Kappa statistic: 0.7955555555555556
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.95959595959596
Correctly Classified Instances: 84.04040404040404
Weighted Precision: 0.847387590126958
Weighted AreaUnderROC: 0.9122222222222222
Root mean squared error: 0.16789015140963273
Relative absolute error: 20.146666666666388
Root relative squared error: 58.400680268146324
Weighted TruePositiveRate: 0.8404040404040404
Weighted MatthewsCorrelation: 0.8270796365517108
Weighted FMeasure: 0.8417291704105856
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7341876519912739
Mean absolute error: 0.03330027548209341
Coverage of cases: 84.04040404040404
Instances selection time: 1.0
Test time: 19.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8404040404040404
Weighted FalsePositiveRate: 0.015959595959595962
Kappa statistic: 0.8244444444444444
Training time: 0.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 14.94949494949495
Correctly Classified Instances: 85.05050505050505
Weighted Precision: 0.8592542424237992
Weighted AreaUnderROC: 0.9177777777777778
Root mean squared error: 0.1626218469919823
Relative absolute error: 18.92852852852832
Root relative squared error: 56.568097717787076
Weighted TruePositiveRate: 0.8505050505050505
Weighted MatthewsCorrelation: 0.8389153522606586
Weighted FMeasure: 0.8523227579214582
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7524795659163855
Mean absolute error: 0.0312868240140966
Coverage of cases: 85.05050505050505
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8505050505050505
Weighted FalsePositiveRate: 0.01494949494949495
Kappa statistic: 0.8355555555555556
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.929292929292929
Correctly Classified Instances: 87.07070707070707
Weighted Precision: 0.8800026738200069
Weighted AreaUnderROC: 0.9288888888888889
Root mean squared error: 0.1513621844535111
Relative absolute error: 16.641595441595232
Root relative squared error: 52.65141799407844
Weighted TruePositiveRate: 0.8707070707070707
Weighted MatthewsCorrelation: 0.860996659021719
Weighted FMeasure: 0.8716496027160802
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7828594794355942
Mean absolute error: 0.027506769324950976
Coverage of cases: 87.07070707070707
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8707070707070707
Weighted FalsePositiveRate: 0.01292929292929293
Kappa statistic: 0.8577777777777778
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.707070707070708
Correctly Classified Instances: 89.29292929292929
Weighted Precision: 0.8993444286001396
Weighted AreaUnderROC: 0.941111111111111
Root mean squared error: 0.13785714908300115
Relative absolute error: 14.144715447154363
Root relative squared error: 47.953684112364236
Weighted TruePositiveRate: 0.8929292929292929
Weighted MatthewsCorrelation: 0.884374732360261
Weighted FMeasure: 0.8934050218234796
Iteration time: 0.0
Weighted AreaUnderPRC: 0.820208338589795
Mean absolute error: 0.023379694953974302
Coverage of cases: 89.29292929292929
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8929292929292929
Weighted FalsePositiveRate: 0.010707070707070708
Kappa statistic: 0.8822222222222222
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 7.878787878787879
Correctly Classified Instances: 92.12121212121212
Weighted Precision: 0.9262752863465434
Weighted AreaUnderROC: 0.9566666666666666
Root mean squared error: 0.11837523773412709
Relative absolute error: 11.003100775193735
Root relative squared error: 41.176890678412484
Weighted TruePositiveRate: 0.9212121212121213
Weighted MatthewsCorrelation: 0.9151571912976814
Weighted FMeasure: 0.9219423098097639
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8628667749347119
Mean absolute error: 0.018186943430072405
Coverage of cases: 92.12121212121212
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9212121212121213
Weighted FalsePositiveRate: 0.00787878787878788
Kappa statistic: 0.9133333333333333
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 7.474747474747475
Correctly Classified Instances: 92.52525252525253
Weighted Precision: 0.9309254681338456
Weighted AreaUnderROC: 0.9588888888888888
Root mean squared error: 0.11535866534967423
Relative absolute error: 10.465679012345596
Root relative squared error: 40.12757433763252
Weighted TruePositiveRate: 0.9252525252525252
Weighted MatthewsCorrelation: 0.9198322583189015
Weighted FMeasure: 0.9261123019452444
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8703872932978142
Mean absolute error: 0.017298642995612665
Coverage of cases: 92.52525252525253
Instances selection time: 0.0
Test time: 23.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9252525252525252
Weighted FalsePositiveRate: 0.007474747474747475
Kappa statistic: 0.9177777777777777
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 6.4646464646464645
Correctly Classified Instances: 93.53535353535354
Weighted Precision: 0.9403053339860434
Weighted AreaUnderROC: 0.9644444444444444
Root mean squared error: 0.10735027096715018
Relative absolute error: 9.285106382978622
Root relative squared error: 37.34185000616841
Weighted TruePositiveRate: 0.9353535353535354
Weighted MatthewsCorrelation: 0.9307352451220549
Weighted FMeasure: 0.9362371648940204
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8886208475491549
Mean absolute error: 0.015347283277650713
Coverage of cases: 93.53535353535354
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9353535353535354
Weighted FalsePositiveRate: 0.006464646464646465
Kappa statistic: 0.9288888888888889
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 6.4646464646464645
Correctly Classified Instances: 93.53535353535354
Weighted Precision: 0.9398973501246228
Weighted AreaUnderROC: 0.9644444444444444
Root mean squared error: 0.10738570135159449
Relative absolute error: 9.196371882086169
Root relative squared error: 37.35417448462255
Weighted TruePositiveRate: 0.9353535353535354
Weighted MatthewsCorrelation: 0.9305329804735355
Weighted FMeasure: 0.9360249201891487
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8879377833165712
Mean absolute error: 0.01520061468113426
Coverage of cases: 93.53535353535354
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9353535353535354
Weighted FalsePositiveRate: 0.006464646464646465
Kappa statistic: 0.9288888888888889
Training time: 0.0
		
Time end:Sun Oct 08 10.09.29 EEST 2017