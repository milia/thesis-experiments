Sun Oct 08 10.01.55 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.55 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.4336986998055263
Weighted AreaUnderROC: 0.6788888888888889
Root mean squared error: 0.3104249287084334
Relative absolute error: 67.80000000000034
Root relative squared error: 107.98147989354413
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.3615270044374023
Weighted FMeasure: 0.40987155708975215
Iteration time: 15.0
Weighted AreaUnderPRC: 0.24101757229190232
Mean absolute error: 0.11206611570248061
Coverage of cases: 71.71717171717172
Instances selection time: 11.0
Test time: 9.0
Accumulative iteration time: 15.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.05838383838383838
Kappa statistic: 0.35777777777777775
Training time: 4.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 15.95959595959596
Correctly Classified Instances: 84.04040404040404
Weighted Precision: 0.870115340654825
Weighted AreaUnderROC: 0.9122222222222222
Root mean squared error: 0.16822133848875875
Relative absolute error: 19.767479674796625
Root relative squared error: 58.515883873328555
Weighted TruePositiveRate: 0.8404040404040404
Weighted MatthewsCorrelation: 0.8227705941367028
Weighted FMeasure: 0.8175045116164346
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7372087344861867
Mean absolute error: 0.03267352012363099
Coverage of cases: 84.04040404040404
Instances selection time: 8.0
Test time: 22.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8404040404040404
Weighted FalsePositiveRate: 0.01595959595959596
Kappa statistic: 0.8244444444444444
Training time: 1.0
		
Time end:Sun Oct 08 10.01.55 EEST 2017