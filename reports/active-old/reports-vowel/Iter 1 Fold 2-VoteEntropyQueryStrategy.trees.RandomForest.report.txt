Sun Oct 08 10.10.21 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 10.10.21 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 44.334251606978945
Incorrectly Classified Instances: 68.08080808080808
Correctly Classified Instances: 31.91919191919192
Weighted Precision: 0.34438915803719977
Weighted AreaUnderROC: 0.7059124579124579
Root mean squared error: 0.2813682922112564
Relative absolute error: 83.76017546332336
Root relative squared error: 97.874113121332
Weighted TruePositiveRate: 0.3191919191919192
Weighted MatthewsCorrelation: 0.2600955011023753
Weighted FMeasure: 0.3202079261613572
Iteration time: 21.0
Weighted AreaUnderPRC: 0.32964157594099214
Mean absolute error: 0.1384465710137585
Coverage of cases: 70.5050505050505
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.3191919191919192
Weighted FalsePositiveRate: 0.06808080808080808
Kappa statistic: 0.2511111111111111
Training time: 20.0
		
Iteration: 2
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.22314049586786
Incorrectly Classified Instances: 31.31313131313131
Correctly Classified Instances: 68.68686868686869
Weighted Precision: 0.7170524998251921
Weighted AreaUnderROC: 0.9065364758698092
Root mean squared error: 0.20974932971453722
Relative absolute error: 55.360341501573544
Root relative squared error: 72.96141815507276
Weighted TruePositiveRate: 0.6868686868686869
Weighted MatthewsCorrelation: 0.6492814885180275
Weighted FMeasure: 0.6509186760828553
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7299889522413656
Mean absolute error: 0.09150469669681636
Coverage of cases: 84.84848484848484
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6868686868686869
Weighted FalsePositiveRate: 0.031313131313131314
Kappa statistic: 0.6555555555555556
Training time: 56.0
		
Time end:Sun Oct 08 10.10.21 EEST 2017