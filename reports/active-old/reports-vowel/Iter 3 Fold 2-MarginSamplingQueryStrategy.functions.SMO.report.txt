Sun Oct 08 10.05.51 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.05.51 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 82.07529843893441
Incorrectly Classified Instances: 75.75757575757575
Correctly Classified Instances: 24.242424242424242
Weighted Precision: 0.25690761661648537
Weighted AreaUnderROC: 0.7291829405162737
Root mean squared error: 0.279646322586675
Relative absolute error: 95.45050505050443
Root relative squared error: 97.27512505304875
Weighted TruePositiveRate: 0.24242424242424243
Weighted MatthewsCorrelation: 0.1691527664359617
Weighted FMeasure: 0.23390063533524372
Iteration time: 442.0
Weighted AreaUnderPRC: 0.218388432023962
Mean absolute error: 0.1577694298355455
Coverage of cases: 98.18181818181819
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 442.0
Weighted Recall: 0.24242424242424243
Weighted FalsePositiveRate: 0.07575757575757576
Kappa statistic: 0.16666666666666669
Training time: 437.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 82.05693296602352
Incorrectly Classified Instances: 73.33333333333333
Correctly Classified Instances: 26.666666666666668
Weighted Precision: 0.3283894668827203
Weighted AreaUnderROC: 0.7416588103254771
Root mean squared error: 0.2795549069031626
Relative absolute error: 95.39393939393882
Root relative squared error: 97.24332605793512
Weighted TruePositiveRate: 0.26666666666666666
Weighted MatthewsCorrelation: 0.20988636042989536
Weighted FMeasure: 0.25624729197615664
Iteration time: 444.0
Weighted AreaUnderPRC: 0.22370682825713126
Mean absolute error: 0.15767593288254453
Coverage of cases: 98.98989898989899
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 886.0
Weighted Recall: 0.26666666666666666
Weighted FalsePositiveRate: 0.07333333333333333
Kappa statistic: 0.19333333333333333
Training time: 440.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 70.70707070707071
Correctly Classified Instances: 29.292929292929294
Weighted Precision: 0.28380165534745355
Weighted AreaUnderROC: 0.7559753086419752
Root mean squared error: 0.2789970795785772
Relative absolute error: 95.19999999999939
Root relative squared error: 97.04928552039065
Weighted TruePositiveRate: 0.29292929292929293
Weighted MatthewsCorrelation: 0.2145119705832638
Weighted FMeasure: 0.26512106685231074
Iteration time: 446.0
Weighted AreaUnderPRC: 0.2382112354602706
Mean absolute error: 0.15735537190082646
Coverage of cases: 97.97979797979798
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 1332.0
Weighted Recall: 0.29292929292929293
Weighted FalsePositiveRate: 0.07070707070707072
Kappa statistic: 0.22222222222222224
Training time: 441.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 64.24242424242425
Correctly Classified Instances: 35.75757575757576
Weighted Precision: 0.3468506289111814
Weighted AreaUnderROC: 0.7770796857463526
Root mean squared error: 0.2777532823174111
Relative absolute error: 94.7878787878782
Root relative squared error: 96.61662996818659
Weighted TruePositiveRate: 0.3575757575757576
Weighted MatthewsCorrelation: 0.2860956323648087
Weighted FMeasure: 0.3349422430380367
Iteration time: 464.0
Weighted AreaUnderPRC: 0.27168774922623906
Mean absolute error: 0.1566741798146757
Coverage of cases: 98.18181818181819
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 1796.0
Weighted Recall: 0.3575757575757576
Weighted FalsePositiveRate: 0.06424242424242424
Kappa statistic: 0.2933333333333334
Training time: 460.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 82.11202938475626
Incorrectly Classified Instances: 63.23232323232323
Correctly Classified Instances: 36.76767676767677
Weighted Precision: 0.3967923377612373
Weighted AreaUnderROC: 0.7765544332210998
Root mean squared error: 0.27680739062455534
Relative absolute error: 94.48080808080745
Root relative squared error: 96.28760102956836
Weighted TruePositiveRate: 0.36767676767676766
Weighted MatthewsCorrelation: 0.31372586712255385
Weighted FMeasure: 0.3650717060843964
Iteration time: 459.0
Weighted AreaUnderPRC: 0.28990427047014056
Mean absolute error: 0.15616662492695546
Coverage of cases: 98.58585858585859
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 2255.0
Weighted Recall: 0.36767676767676766
Weighted FalsePositiveRate: 0.06323232323232324
Kappa statistic: 0.30444444444444446
Training time: 454.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 82.14876033057814
Incorrectly Classified Instances: 59.7979797979798
Correctly Classified Instances: 40.2020202020202
Weighted Precision: 0.42053054687066616
Weighted AreaUnderROC: 0.79931088664422
Root mean squared error: 0.2756250868555414
Relative absolute error: 94.07676767676703
Root relative squared error: 95.87633602197694
Weighted TruePositiveRate: 0.402020202020202
Weighted MatthewsCorrelation: 0.3458695652854781
Weighted FMeasure: 0.39450432220621257
Iteration time: 479.0
Weighted AreaUnderPRC: 0.31997282863345533
Mean absolute error: 0.15549878954837626
Coverage of cases: 99.1919191919192
Instances selection time: 10.0
Test time: 24.0
Accumulative iteration time: 2734.0
Weighted Recall: 0.402020202020202
Weighted FalsePositiveRate: 0.0597979797979798
Kappa statistic: 0.34222222222222226
Training time: 469.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 82.14876033057811
Incorrectly Classified Instances: 58.38383838383838
Correctly Classified Instances: 41.61616161616162
Weighted Precision: 0.4087219188266854
Weighted AreaUnderROC: 0.8124399551066217
Root mean squared error: 0.27516433464434414
Relative absolute error: 93.92323232323167
Root relative squared error: 95.71606311529929
Weighted TruePositiveRate: 0.4161616161616162
Weighted MatthewsCorrelation: 0.3478507845064541
Weighted FMeasure: 0.3929845304787082
Iteration time: 526.0
Weighted AreaUnderPRC: 0.335191713254691
Mean absolute error: 0.15524501210451616
Coverage of cases: 99.1919191919192
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 3260.0
Weighted Recall: 0.4161616161616162
Weighted FalsePositiveRate: 0.05838383838383838
Kappa statistic: 0.35777777777777775
Training time: 522.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 82.0752984389344
Incorrectly Classified Instances: 56.36363636363637
Correctly Classified Instances: 43.63636363636363
Weighted Precision: 0.4403077398492395
Weighted AreaUnderROC: 0.8239708193041527
Root mean squared error: 0.2745639797180835
Relative absolute error: 93.72121212121164
Root relative squared error: 95.50722932843583
Weighted TruePositiveRate: 0.43636363636363634
Weighted MatthewsCorrelation: 0.374973544907374
Weighted FMeasure: 0.41755560689810856
Iteration time: 433.0
Weighted AreaUnderPRC: 0.3586052243044677
Mean absolute error: 0.15491109441522685
Coverage of cases: 98.98989898989899
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 3693.0
Weighted Recall: 0.43636363636363634
Weighted FalsePositiveRate: 0.05636363636363636
Kappa statistic: 0.38
Training time: 430.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 82.07529843893441
Incorrectly Classified Instances: 52.72727272727273
Correctly Classified Instances: 47.27272727272727
Weighted Precision: 0.4911373207285594
Weighted AreaUnderROC: 0.8532592592592593
Root mean squared error: 0.27316402127551315
Relative absolute error: 93.2484848484844
Root relative squared error: 95.02025302454435
Weighted TruePositiveRate: 0.4727272727272727
Weighted MatthewsCorrelation: 0.41965545135502336
Weighted FMeasure: 0.45526101210960523
Iteration time: 434.0
Weighted AreaUnderPRC: 0.3922972028113654
Mean absolute error: 0.15412972702228925
Coverage of cases: 99.1919191919192
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 4127.0
Weighted Recall: 0.4727272727272727
Weighted FalsePositiveRate: 0.05272727272727272
Kappa statistic: 0.42000000000000004
Training time: 431.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 82.03856749311255
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.582952836413967
Weighted AreaUnderROC: 0.8574141414141413
Root mean squared error: 0.2730252983884349
Relative absolute error: 93.19999999999946
Root relative squared error: 94.97199819300057
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.46587913742987697
Weighted FMeasure: 0.48197488690917023
Iteration time: 434.0
Weighted AreaUnderPRC: 0.41758473460003687
Mean absolute error: 0.15404958677685962
Coverage of cases: 98.98989898989899
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 4561.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.04929292929292929
Kappa statistic: 0.4577777777777778
Training time: 431.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 82.1303948576672
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5945551069296919
Weighted AreaUnderROC: 0.8621077441077442
Root mean squared error: 0.27291809558182006
Relative absolute error: 93.17575757575716
Root relative squared error: 94.93470763854938
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.4631570351409044
Weighted FMeasure: 0.4704813759604388
Iteration time: 449.0
Weighted AreaUnderPRC: 0.4261194139660367
Mean absolute error: 0.15400951665414506
Coverage of cases: 98.38383838383838
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 5010.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.049696969696969684
Kappa statistic: 0.4533333333333333
Training time: 446.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 82.05693296602347
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5448450189024534
Weighted AreaUnderROC: 0.8736677890011223
Root mean squared error: 0.2725683929346219
Relative absolute error: 93.04646464646413
Root relative squared error: 94.81306338296635
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.45408746485536844
Weighted FMeasure: 0.4706926379811253
Iteration time: 446.0
Weighted AreaUnderPRC: 0.4336960724935483
Mean absolute error: 0.15379580933299955
Coverage of cases: 98.38383838383838
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 5456.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.0492929292929293
Kappa statistic: 0.4577777777777778
Training time: 443.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 81.96510560146882
Incorrectly Classified Instances: 49.292929292929294
Correctly Classified Instances: 50.707070707070706
Weighted Precision: 0.5364002078218952
Weighted AreaUnderROC: 0.8784803591470259
Root mean squared error: 0.27236294779685
Relative absolute error: 92.98181818181764
Root relative squared error: 94.74159918031407
Weighted TruePositiveRate: 0.5070707070707071
Weighted MatthewsCorrelation: 0.4541597324380982
Weighted FMeasure: 0.4755178361567216
Iteration time: 452.0
Weighted AreaUnderPRC: 0.44036314098334306
Mean absolute error: 0.15368895567242685
Coverage of cases: 98.18181818181819
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 5908.0
Weighted Recall: 0.5070707070707071
Weighted FalsePositiveRate: 0.04929292929292929
Kappa statistic: 0.4577777777777778
Training time: 450.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 81.96510560146882
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5818341684309126
Weighted AreaUnderROC: 0.8851537598204265
Root mean squared error: 0.27214797813766106
Relative absolute error: 92.90909090909032
Root relative squared error: 94.66682186771868
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.4900825226929739
Weighted FMeasure: 0.5035615092896947
Iteration time: 615.0
Weighted AreaUnderPRC: 0.45893311704600764
Mean absolute error: 0.15356874530428252
Coverage of cases: 97.77777777777777
Instances selection time: 4.0
Test time: 24.0
Accumulative iteration time: 6523.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262626
Kappa statistic: 0.4911111111111111
Training time: 611.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 82.05693296602345
Incorrectly Classified Instances: 46.666666666666664
Correctly Classified Instances: 53.333333333333336
Weighted Precision: 0.572464698612689
Weighted AreaUnderROC: 0.8838024691358026
Root mean squared error: 0.2717032281235483
Relative absolute error: 92.77575757575703
Root relative squared error: 94.51211533397996
Weighted TruePositiveRate: 0.5333333333333333
Weighted MatthewsCorrelation: 0.4837303739536614
Weighted FMeasure: 0.5013272084135489
Iteration time: 524.0
Weighted AreaUnderPRC: 0.45651174484512375
Mean absolute error: 0.15334835962935145
Coverage of cases: 98.78787878787878
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 7047.0
Weighted Recall: 0.5333333333333333
Weighted FalsePositiveRate: 0.04666666666666667
Kappa statistic: 0.48666666666666664
Training time: 522.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 82.11202938475626
Incorrectly Classified Instances: 46.464646464646464
Correctly Classified Instances: 53.535353535353536
Weighted Precision: 0.5380583099600754
Weighted AreaUnderROC: 0.8870415263748597
Root mean squared error: 0.27136560763144507
Relative absolute error: 92.66262626262568
Root relative squared error: 94.39467386260273
Weighted TruePositiveRate: 0.5353535353535354
Weighted MatthewsCorrelation: 0.48002241137975266
Weighted FMeasure: 0.509927832469135
Iteration time: 516.0
Weighted AreaUnderPRC: 0.4510012935608132
Mean absolute error: 0.1531613657233492
Coverage of cases: 99.1919191919192
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 7563.0
Weighted Recall: 0.5353535353535354
Weighted FalsePositiveRate: 0.04646464646464647
Kappa statistic: 0.4888888888888889
Training time: 514.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 82.02020202020162
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5433429175321658
Weighted AreaUnderROC: 0.8980314253647588
Root mean squared error: 0.27064513933495277
Relative absolute error: 92.42828282828228
Root relative squared error: 94.14405857472843
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.4901780698751526
Weighted FMeasure: 0.5240560182000055
Iteration time: 547.0
Weighted AreaUnderPRC: 0.4897525074663921
Mean absolute error: 0.15277402120377334
Coverage of cases: 98.78787878787878
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8110.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656565
Kappa statistic: 0.4977777777777777
Training time: 546.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 82.09366391184534
Incorrectly Classified Instances: 41.21212121212121
Correctly Classified Instances: 58.78787878787879
Weighted Precision: 0.5867967023388829
Weighted AreaUnderROC: 0.9056543209876543
Root mean squared error: 0.26999696296218484
Relative absolute error: 92.2141414141408
Root relative squared error: 93.91859007174861
Weighted TruePositiveRate: 0.5878787878787879
Weighted MatthewsCorrelation: 0.5425409606205772
Weighted FMeasure: 0.5775821086964904
Iteration time: 541.0
Weighted AreaUnderPRC: 0.5182640779275351
Mean absolute error: 0.15242006845312628
Coverage of cases: 98.98989898989899
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 8651.0
Weighted Recall: 0.5878787878787879
Weighted FalsePositiveRate: 0.04121212121212121
Kappa statistic: 0.5466666666666666
Training time: 539.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 82.0569329660235
Incorrectly Classified Instances: 40.0
Correctly Classified Instances: 60.0
Weighted Precision: 0.5942567921056667
Weighted AreaUnderROC: 0.9147093153759821
Root mean squared error: 0.2693415915832786
Relative absolute error: 91.99191919191856
Root relative squared error: 93.69061878197942
Weighted TruePositiveRate: 0.6
Weighted MatthewsCorrelation: 0.5533752433884894
Weighted FMeasure: 0.5865418636940316
Iteration time: 548.0
Weighted AreaUnderPRC: 0.5384483648642838
Mean absolute error: 0.15205275899490767
Coverage of cases: 99.1919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 9199.0
Weighted Recall: 0.6
Weighted FalsePositiveRate: 0.04
Kappa statistic: 0.5599999999999999
Training time: 547.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 82.03856749311255
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6019759999705712
Weighted AreaUnderROC: 0.9192480359147027
Root mean squared error: 0.2686534371781837
Relative absolute error: 91.75757575757514
Root relative squared error: 93.45124389876183
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5617079648336085
Weighted FMeasure: 0.5981023016875109
Iteration time: 574.0
Weighted AreaUnderPRC: 0.543769193426706
Mean absolute error: 0.15166541447533177
Coverage of cases: 99.39393939393939
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 9773.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 574.0
		
Time end:Sun Oct 08 10.06.02 EEST 2017