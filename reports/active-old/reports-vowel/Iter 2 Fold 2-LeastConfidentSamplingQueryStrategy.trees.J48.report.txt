Sun Oct 08 10.04.15 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.04.15 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 26.519742883379404
Incorrectly Classified Instances: 60.80808080808081
Correctly Classified Instances: 39.19191919191919
Weighted Precision: 0.3874449648707465
Weighted AreaUnderROC: 0.7574814814814815
Root mean squared error: 0.28337586897829925
Relative absolute error: 70.74583934583896
Root relative squared error: 98.5724497890962
Weighted TruePositiveRate: 0.39191919191919194
Weighted MatthewsCorrelation: 0.3245276687840686
Weighted FMeasure: 0.3470636634634695
Iteration time: 8.0
Weighted AreaUnderPRC: 0.2946270619529981
Mean absolute error: 0.11693527164601555
Coverage of cases: 71.51515151515152
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 8.0
Weighted Recall: 0.39191919191919194
Weighted FalsePositiveRate: 0.06080808080808081
Kappa statistic: 0.3311111111111112
Training time: 7.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 16.088154269972524
Incorrectly Classified Instances: 57.77777777777778
Correctly Classified Instances: 42.22222222222222
Weighted Precision: 0.45231770320825754
Weighted AreaUnderROC: 0.7360022446689114
Root mean squared error: 0.296241492152983
Relative absolute error: 65.76523969857249
Root relative squared error: 103.04776379153536
Weighted TruePositiveRate: 0.4222222222222222
Weighted MatthewsCorrelation: 0.3725208500674874
Weighted FMeasure: 0.41715529073130336
Iteration time: 8.0
Weighted AreaUnderPRC: 0.3048231960873045
Mean absolute error: 0.10870287553483125
Coverage of cases: 55.35353535353536
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.4222222222222222
Weighted FalsePositiveRate: 0.05777777777777778
Kappa statistic: 0.36444444444444446
Training time: 7.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 15.77594123048676
Incorrectly Classified Instances: 56.36363636363637
Correctly Classified Instances: 43.63636363636363
Weighted Precision: 0.47522685794057984
Weighted AreaUnderROC: 0.7552323232323231
Root mean squared error: 0.29230303757774045
Relative absolute error: 63.88968932302204
Root relative squared error: 101.67777023045883
Weighted TruePositiveRate: 0.43636363636363634
Weighted MatthewsCorrelation: 0.3874145404106492
Weighted FMeasure: 0.4242310483327807
Iteration time: 9.0
Weighted AreaUnderPRC: 0.31550544866351277
Mean absolute error: 0.1056027922694586
Coverage of cases: 58.98989898989899
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.43636363636363634
Weighted FalsePositiveRate: 0.05636363636363636
Kappa statistic: 0.38
Training time: 8.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 15.44536271809008
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.47691005304510925
Weighted AreaUnderROC: 0.7476363636363638
Root mean squared error: 0.2945065482165725
Relative absolute error: 64.2297619047613
Root relative squared error: 102.44426260184225
Weighted TruePositiveRate: 0.43232323232323233
Weighted MatthewsCorrelation: 0.38697916328278525
Weighted FMeasure: 0.4272523072337718
Iteration time: 11.0
Weighted AreaUnderPRC: 0.32749212697221797
Mean absolute error: 0.10616489571034995
Coverage of cases: 56.76767676767677
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.43232323232323233
Weighted FalsePositiveRate: 0.05676767676767677
Kappa statistic: 0.37555555555555553
Training time: 10.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 14.912764003673162
Incorrectly Classified Instances: 44.84848484848485
Correctly Classified Instances: 55.15151515151515
Weighted Precision: 0.5624093976758554
Weighted AreaUnderROC: 0.7978518518518518
Root mean squared error: 0.26949780989916367
Relative absolute error: 52.349949241902856
Root relative squared error: 93.744959407927
Weighted TruePositiveRate: 0.5515151515151515
Weighted MatthewsCorrelation: 0.5014514382022844
Weighted FMeasure: 0.5274505089982581
Iteration time: 14.0
Weighted AreaUnderPRC: 0.41321046682946
Mean absolute error: 0.08652884172215403
Coverage of cases: 68.48484848484848
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 50.0
Weighted Recall: 0.5515151515151515
Weighted FalsePositiveRate: 0.04484848484848485
Kappa statistic: 0.5066666666666666
Training time: 13.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 15.812672176308611
Incorrectly Classified Instances: 45.85858585858586
Correctly Classified Instances: 54.14141414141414
Weighted Precision: 0.5702572693078101
Weighted AreaUnderROC: 0.7988888888888889
Root mean squared error: 0.2743209832393934
Relative absolute error: 53.30213008575042
Root relative squared error: 95.4227028714691
Weighted TruePositiveRate: 0.5414141414141415
Weighted MatthewsCorrelation: 0.49887060402452726
Weighted FMeasure: 0.526327437446602
Iteration time: 15.0
Weighted AreaUnderPRC: 0.40540382154427523
Mean absolute error: 0.08810269435661283
Coverage of cases: 67.47474747474747
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 65.0
Weighted Recall: 0.5414141414141415
Weighted FalsePositiveRate: 0.04585858585858586
Kappa statistic: 0.4955555555555556
Training time: 14.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 15.59228650137746
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5763951672285982
Weighted AreaUnderROC: 0.8306531986531988
Root mean squared error: 0.2644778655455328
Relative absolute error: 51.5945663531867
Root relative squared error: 91.99876904060173
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.5008628401747853
Weighted FMeasure: 0.5292414508519369
Iteration time: 16.0
Weighted AreaUnderPRC: 0.4432705121573919
Mean absolute error: 0.08528027496394551
Coverage of cases: 73.73737373737374
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 81.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.046060606060606066
Kappa statistic: 0.49333333333333335
Training time: 15.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 15.92286501377416
Incorrectly Classified Instances: 45.656565656565654
Correctly Classified Instances: 54.343434343434346
Weighted Precision: 0.5538605719366724
Weighted AreaUnderROC: 0.8316341189674523
Root mean squared error: 0.26558261672430944
Relative absolute error: 52.51540970736335
Root relative squared error: 92.38305733759762
Weighted TruePositiveRate: 0.5434343434343434
Weighted MatthewsCorrelation: 0.4933413178560093
Weighted FMeasure: 0.5226941496178386
Iteration time: 18.0
Weighted AreaUnderPRC: 0.43629160916903453
Mean absolute error: 0.08680233009481601
Coverage of cases: 72.92929292929293
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 99.0
Weighted Recall: 0.5434343434343434
Weighted FalsePositiveRate: 0.04565656565656566
Kappa statistic: 0.4977777777777777
Training time: 17.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 14.178145087236025
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5876031924461574
Weighted AreaUnderROC: 0.8402985409652076
Root mean squared error: 0.26108477513065936
Relative absolute error: 48.733249480758744
Root relative squared error: 90.81848069863429
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5264616603934131
Weighted FMeasure: 0.5522007176100934
Iteration time: 20.0
Weighted AreaUnderPRC: 0.46002236475057334
Mean absolute error: 0.0805508255880315
Coverage of cases: 73.33333333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 119.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.04282828282828283
Kappa statistic: 0.5288888888888889
Training time: 19.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 17.796143250688832
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5485789470206169
Weighted AreaUnderROC: 0.8264826038159372
Root mean squared error: 0.2693684555083723
Relative absolute error: 54.82946777679217
Root relative squared error: 93.69996341290013
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.49274113121903446
Weighted FMeasure: 0.531479195221121
Iteration time: 22.0
Weighted AreaUnderPRC: 0.43434970218621
Mean absolute error: 0.09062721946577276
Coverage of cases: 72.52525252525253
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 141.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262627
Kappa statistic: 0.4911111111111111
Training time: 21.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 15.647382920110264
Incorrectly Classified Instances: 39.7979797979798
Correctly Classified Instances: 60.2020202020202
Weighted Precision: 0.6236787732318017
Weighted AreaUnderROC: 0.8407609427609428
Root mean squared error: 0.2550109868034587
Relative absolute error: 47.327090002377034
Root relative squared error: 88.7057101332675
Weighted TruePositiveRate: 0.602020202020202
Weighted MatthewsCorrelation: 0.5658633621734498
Weighted FMeasure: 0.5939789416689317
Iteration time: 24.0
Weighted AreaUnderPRC: 0.4763707277951095
Mean absolute error: 0.0782265950452518
Coverage of cases: 73.93939393939394
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 165.0
Weighted Recall: 0.602020202020202
Weighted FalsePositiveRate: 0.0397979797979798
Kappa statistic: 0.5622222222222222
Training time: 23.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 16.473829201102
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6320313332251568
Weighted AreaUnderROC: 0.8423299663299664
Root mean squared error: 0.2518165056383075
Relative absolute error: 47.23962613157988
Root relative squared error: 87.59450812658501
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5739646049630452
Weighted FMeasure: 0.5998685719451584
Iteration time: 26.0
Weighted AreaUnderPRC: 0.46571646501026076
Mean absolute error: 0.0780820266637689
Coverage of cases: 75.15151515151516
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 191.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.03898989898989899
Kappa statistic: 0.5711111111111111
Training time: 25.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 13.902662993572118
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6427763144321014
Weighted AreaUnderROC: 0.838567901234568
Root mean squared error: 0.25079813752264124
Relative absolute error: 43.87362679891389
Root relative squared error: 87.2402682249651
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5921044837159966
Weighted FMeasure: 0.6187770494978141
Iteration time: 28.0
Weighted AreaUnderPRC: 0.4776899208399229
Mean absolute error: 0.07251839140316392
Coverage of cases: 73.13131313131314
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 219.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.037171717171717175
Kappa statistic: 0.5911111111111111
Training time: 27.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 14.949494949494996
Incorrectly Classified Instances: 38.18181818181818
Correctly Classified Instances: 61.81818181818182
Weighted Precision: 0.6409397975763806
Weighted AreaUnderROC: 0.8522514029180698
Root mean squared error: 0.2471060832029279
Relative absolute error: 44.84379014526754
Root relative squared error: 85.95598512647577
Weighted TruePositiveRate: 0.6181818181818182
Weighted MatthewsCorrelation: 0.5844163447477841
Weighted FMeasure: 0.6106443702521281
Iteration time: 29.0
Weighted AreaUnderPRC: 0.49718013550604856
Mean absolute error: 0.07412196718226087
Coverage of cases: 76.56565656565657
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 248.0
Weighted Recall: 0.6181818181818182
Weighted FalsePositiveRate: 0.03818181818181818
Kappa statistic: 0.58
Training time: 28.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 14.876033057851284
Incorrectly Classified Instances: 36.96969696969697
Correctly Classified Instances: 63.03030303030303
Weighted Precision: 0.6699844920946187
Weighted AreaUnderROC: 0.8639034792368127
Root mean squared error: 0.24285529288233657
Relative absolute error: 43.301472600534254
Root relative squared error: 84.47734540690053
Weighted TruePositiveRate: 0.6303030303030303
Weighted MatthewsCorrelation: 0.6027665698033822
Weighted FMeasure: 0.6243932682887502
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5201889960941523
Mean absolute error: 0.0715726819843546
Coverage of cases: 78.18181818181819
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 280.0
Weighted Recall: 0.6303030303030303
Weighted FalsePositiveRate: 0.03696969696969697
Kappa statistic: 0.5933333333333333
Training time: 31.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 14.784205693296656
Incorrectly Classified Instances: 35.95959595959596
Correctly Classified Instances: 64.04040404040404
Weighted Precision: 0.6801046725630873
Weighted AreaUnderROC: 0.8702199775533109
Root mean squared error: 0.23935787073822656
Relative absolute error: 42.240159268835505
Root relative squared error: 83.26076521630583
Weighted TruePositiveRate: 0.6404040404040404
Weighted MatthewsCorrelation: 0.6135419187120397
Weighted FMeasure: 0.6337779416358884
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5303232038715774
Mean absolute error: 0.06981844507245583
Coverage of cases: 79.1919191919192
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 312.0
Weighted Recall: 0.6404040404040404
Weighted FalsePositiveRate: 0.03595959595959596
Kappa statistic: 0.6044444444444445
Training time: 32.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 13.002754820936653
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6491315680039164
Weighted AreaUnderROC: 0.8432480359147027
Root mean squared error: 0.24742134955921444
Relative absolute error: 42.38632285842786
Root relative squared error: 86.06565069957941
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.6019078269097572
Weighted FMeasure: 0.633042187125307
Iteration time: 36.0
Weighted AreaUnderPRC: 0.49442564839768977
Mean absolute error: 0.07006003778252584
Coverage of cases: 73.33333333333333
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 348.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.036565656565656565
Kappa statistic: 0.5977777777777777
Training time: 35.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 11.313131313131379
Incorrectly Classified Instances: 30.707070707070706
Correctly Classified Instances: 69.29292929292929
Weighted Precision: 0.7053420929680236
Weighted AreaUnderROC: 0.8658608305274972
Root mean squared error: 0.23036683120899656
Relative absolute error: 35.527758439308016
Root relative squared error: 80.1332272373586
Weighted TruePositiveRate: 0.692929292929293
Weighted MatthewsCorrelation: 0.6646348741127623
Weighted FMeasure: 0.6890144432742353
Iteration time: 37.0
Weighted AreaUnderPRC: 0.5490591957208998
Mean absolute error: 0.05872356766827809
Coverage of cases: 75.35353535353535
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 385.0
Weighted Recall: 0.692929292929293
Weighted FalsePositiveRate: 0.03070707070707071
Kappa statistic: 0.6622222222222223
Training time: 37.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 12.892561983471117
Incorrectly Classified Instances: 32.121212121212125
Correctly Classified Instances: 67.87878787878788
Weighted Precision: 0.685908064814786
Weighted AreaUnderROC: 0.8710751964085297
Root mean squared error: 0.22456797311848878
Relative absolute error: 36.95668457057331
Root relative squared error: 78.11609130400767
Weighted TruePositiveRate: 0.6787878787878788
Weighted MatthewsCorrelation: 0.6474956404720916
Weighted FMeasure: 0.6750203680258817
Iteration time: 44.0
Weighted AreaUnderPRC: 0.5746867721478844
Mean absolute error: 0.06108542904227033
Coverage of cases: 77.17171717171718
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 429.0
Weighted Recall: 0.6787878787878788
Weighted FalsePositiveRate: 0.03212121212121213
Kappa statistic: 0.6466666666666667
Training time: 43.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 11.386593204775094
Incorrectly Classified Instances: 30.707070707070706
Correctly Classified Instances: 69.29292929292929
Weighted Precision: 0.6983824867963405
Weighted AreaUnderROC: 0.8685364758698092
Root mean squared error: 0.22499546926393757
Relative absolute error: 34.78367775988541
Root relative squared error: 78.26479607016937
Weighted TruePositiveRate: 0.692929292929293
Weighted MatthewsCorrelation: 0.663699617033832
Weighted FMeasure: 0.6922991445133747
Iteration time: 41.0
Weighted AreaUnderPRC: 0.5771333028969208
Mean absolute error: 0.057493682247744836
Coverage of cases: 76.36363636363636
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 470.0
Weighted Recall: 0.692929292929293
Weighted FalsePositiveRate: 0.030707070707070707
Kappa statistic: 0.6622222222222223
Training time: 40.0
		
Time end:Sun Oct 08 10.04.16 EEST 2017