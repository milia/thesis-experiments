Sun Oct 08 10.01.09 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 10.01.09 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 14.471992653810906
Incorrectly Classified Instances: 54.343434343434346
Correctly Classified Instances: 45.656565656565654
Weighted Precision: 0.5001116547069181
Weighted AreaUnderROC: 0.7637575757575759
Root mean squared error: 0.2890812473283462
Relative absolute error: 60.734728234727754
Root relative squared error: 100.557068744004
Weighted TruePositiveRate: 0.45656565656565656
Weighted MatthewsCorrelation: 0.40518508302811695
Weighted FMeasure: 0.4350037292204479
Iteration time: 22.0
Weighted AreaUnderPRC: 0.3290465730335456
Mean absolute error: 0.10038798055326965
Coverage of cases: 61.41414141414141
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 22.0
Weighted Recall: 0.45656565656565656
Weighted FalsePositiveRate: 0.05434343434343433
Kappa statistic: 0.40222222222222226
Training time: 19.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 16.988062442608033
Incorrectly Classified Instances: 53.93939393939394
Correctly Classified Instances: 46.06060606060606
Weighted Precision: 0.5134358242130445
Weighted AreaUnderROC: 0.7721503928170593
Root mean squared error: 0.2883743015753802
Relative absolute error: 61.305331705331206
Root relative squared error: 100.3111572802329
Weighted TruePositiveRate: 0.46060606060606063
Weighted MatthewsCorrelation: 0.42403163632409824
Weighted FMeasure: 0.4645570850769122
Iteration time: 9.0
Weighted AreaUnderPRC: 0.33348168058240607
Mean absolute error: 0.10133112678567206
Coverage of cases: 64.44444444444444
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.46060606060606063
Weighted FalsePositiveRate: 0.05393939393939394
Kappa statistic: 0.40666666666666673
Training time: 8.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 13.039485766758563
Incorrectly Classified Instances: 54.14141414141414
Correctly Classified Instances: 45.85858585858586
Weighted Precision: 0.4899081581309864
Weighted AreaUnderROC: 0.7434478114478115
Root mean squared error: 0.2970519044198966
Relative absolute error: 61.15449735449685
Root relative squared error: 103.32966613832595
Weighted TruePositiveRate: 0.4585858585858586
Weighted MatthewsCorrelation: 0.41167469856313416
Weighted FMeasure: 0.4537574051545378
Iteration time: 11.0
Weighted AreaUnderPRC: 0.3134123067954155
Mean absolute error: 0.10108181380908635
Coverage of cases: 56.36363636363637
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 42.0
Weighted Recall: 0.4585858585858586
Weighted FalsePositiveRate: 0.05414141414141414
Kappa statistic: 0.4044444444444445
Training time: 9.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 12.12121212121219
Incorrectly Classified Instances: 47.474747474747474
Correctly Classified Instances: 52.525252525252526
Weighted Precision: 0.5413758651905648
Weighted AreaUnderROC: 0.7552143658810325
Root mean squared error: 0.28554932780871956
Relative absolute error: 54.12698412698372
Root relative squared error: 99.3284886226169
Weighted TruePositiveRate: 0.5252525252525253
Weighted MatthewsCorrelation: 0.4834200663993686
Weighted FMeasure: 0.5265545085422619
Iteration time: 13.0
Weighted AreaUnderPRC: 0.35772322139521756
Mean absolute error: 0.08946608946608936
Coverage of cases: 57.37373737373738
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 55.0
Weighted Recall: 0.5252525252525253
Weighted FalsePositiveRate: 0.047474747474747475
Kappa statistic: 0.4777777777777778
Training time: 12.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 11.533516988062505
Incorrectly Classified Instances: 46.26262626262626
Correctly Classified Instances: 53.73737373737374
Weighted Precision: 0.5474437149799729
Weighted AreaUnderROC: 0.7740179573512908
Root mean squared error: 0.2793667236895363
Relative absolute error: 51.711111111110824
Root relative squared error: 97.17786642496351
Weighted TruePositiveRate: 0.5373737373737374
Weighted MatthewsCorrelation: 0.49034508401957955
Weighted FMeasure: 0.5265638433003308
Iteration time: 14.0
Weighted AreaUnderPRC: 0.39054237281158855
Mean absolute error: 0.08547291092745646
Coverage of cases: 60.60606060606061
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 69.0
Weighted Recall: 0.5373737373737374
Weighted FalsePositiveRate: 0.04626262626262626
Kappa statistic: 0.4911111111111111
Training time: 12.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 11.753902662993628
Incorrectly Classified Instances: 43.83838383838384
Correctly Classified Instances: 56.16161616161616
Weighted Precision: 0.5875308399899287
Weighted AreaUnderROC: 0.769164983164983
Root mean squared error: 0.2754805592993385
Relative absolute error: 50.4238095238093
Root relative squared error: 95.82606203312845
Weighted TruePositiveRate: 0.5616161616161616
Weighted MatthewsCorrelation: 0.5277777920876722
Weighted FMeasure: 0.5666430835840447
Iteration time: 15.0
Weighted AreaUnderPRC: 0.40016463962615
Mean absolute error: 0.08334513970877623
Coverage of cases: 59.5959595959596
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 84.0
Weighted Recall: 0.5616161616161616
Weighted FalsePositiveRate: 0.043838383838383836
Kappa statistic: 0.5177777777777777
Training time: 14.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 11.992653810835709
Incorrectly Classified Instances: 43.63636363636363
Correctly Classified Instances: 56.36363636363637
Weighted Precision: 0.6008460881710707
Weighted AreaUnderROC: 0.7792502805836138
Root mean squared error: 0.2734752039527781
Relative absolute error: 49.62749287749262
Root relative squared error: 95.12849808768445
Weighted TruePositiveRate: 0.5636363636363636
Weighted MatthewsCorrelation: 0.5295920111379337
Weighted FMeasure: 0.5590888693603261
Iteration time: 17.0
Weighted AreaUnderPRC: 0.40674671320190375
Mean absolute error: 0.08202891384709578
Coverage of cases: 61.81818181818182
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 101.0
Weighted Recall: 0.5636363636363636
Weighted FalsePositiveRate: 0.04363636363636364
Kappa statistic: 0.5199999999999999
Training time: 16.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 11.00091827364561
Incorrectly Classified Instances: 42.82828282828283
Correctly Classified Instances: 57.17171717171717
Weighted Precision: 0.5966847372564731
Weighted AreaUnderROC: 0.7827272727272726
Root mean squared error: 0.2747752725849346
Relative absolute error: 47.747302080635194
Root relative squared error: 95.5807276668214
Weighted TruePositiveRate: 0.5717171717171717
Weighted MatthewsCorrelation: 0.5320744052633393
Weighted FMeasure: 0.5624667084101942
Iteration time: 19.0
Weighted AreaUnderPRC: 0.40506951941312097
Mean absolute error: 0.07892116046386033
Coverage of cases: 61.81818181818182
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 120.0
Weighted Recall: 0.5717171717171717
Weighted FalsePositiveRate: 0.04282828282828283
Kappa statistic: 0.5288888888888889
Training time: 18.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 10.725436179981715
Incorrectly Classified Instances: 40.80808080808081
Correctly Classified Instances: 59.19191919191919
Weighted Precision: 0.6053921802844551
Weighted AreaUnderROC: 0.7824197530864198
Root mean squared error: 0.2695712024156971
Relative absolute error: 46.722711587928785
Root relative squared error: 93.77048903462729
Weighted TruePositiveRate: 0.591919191919192
Weighted MatthewsCorrelation: 0.5531941047733296
Weighted FMeasure: 0.5861907658376537
Iteration time: 20.0
Weighted AreaUnderPRC: 0.40984792836829964
Mean absolute error: 0.07722762245938691
Coverage of cases: 61.21212121212121
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 140.0
Weighted Recall: 0.591919191919192
Weighted FalsePositiveRate: 0.04080808080808081
Kappa statistic: 0.5511111111111111
Training time: 19.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 11.166207529843973
Incorrectly Classified Instances: 39.39393939393939
Correctly Classified Instances: 60.60606060606061
Weighted Precision: 0.6226868661793321
Weighted AreaUnderROC: 0.8038563411896745
Root mean squared error: 0.262845016411378
Relative absolute error: 44.38847296494333
Root relative squared error: 91.43078158327178
Weighted TruePositiveRate: 0.6060606060606061
Weighted MatthewsCorrelation: 0.5706283078052792
Weighted FMeasure: 0.6024852136193661
Iteration time: 23.0
Weighted AreaUnderPRC: 0.4348152475185121
Mean absolute error: 0.0733693768015597
Coverage of cases: 65.65656565656566
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 163.0
Weighted Recall: 0.6060606060606061
Weighted FalsePositiveRate: 0.03939393939393939
Kappa statistic: 0.5666666666666667
Training time: 22.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 10.982552800734704
Incorrectly Classified Instances: 38.98989898989899
Correctly Classified Instances: 61.01010101010101
Weighted Precision: 0.6118042257592787
Weighted AreaUnderROC: 0.8071492704826039
Root mean squared error: 0.25680233724355106
Relative absolute error: 43.04402852049893
Root relative squared error: 89.32883235587356
Weighted TruePositiveRate: 0.6101010101010101
Weighted MatthewsCorrelation: 0.5689366072198673
Weighted FMeasure: 0.602597789702984
Iteration time: 24.0
Weighted AreaUnderPRC: 0.4537673842258332
Mean absolute error: 0.07114715457933753
Coverage of cases: 65.85858585858585
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 187.0
Weighted Recall: 0.6101010101010101
Weighted FalsePositiveRate: 0.03898989898989899
Kappa statistic: 0.5711111111111111
Training time: 23.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 10.872359963269137
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6323808631109882
Weighted AreaUnderROC: 0.8162244668911336
Root mean squared error: 0.2542783537576985
Relative absolute error: 42.13030303030288
Root relative squared error: 88.45086333075957
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5898678314440667
Weighted FMeasure: 0.6212798376626566
Iteration time: 25.0
Weighted AreaUnderPRC: 0.46318603072585035
Mean absolute error: 0.06963686451289777
Coverage of cases: 66.66666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 212.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.037171717171717175
Kappa statistic: 0.5911111111111111
Training time: 24.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 13.076216712580399
Incorrectly Classified Instances: 37.17171717171717
Correctly Classified Instances: 62.82828282828283
Weighted Precision: 0.6399020818447628
Weighted AreaUnderROC: 0.8461459034792367
Root mean squared error: 0.24187323026886184
Relative absolute error: 43.570231663254745
Root relative squared error: 84.13573439391797
Weighted TruePositiveRate: 0.6282828282828283
Weighted MatthewsCorrelation: 0.5933620455701091
Weighted FMeasure: 0.6247523857270111
Iteration time: 26.0
Weighted AreaUnderPRC: 0.5080194779043689
Mean absolute error: 0.07201691184009094
Coverage of cases: 74.74747474747475
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 238.0
Weighted Recall: 0.6282828282828283
Weighted FalsePositiveRate: 0.037171717171717175
Kappa statistic: 0.5911111111111111
Training time: 25.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 13.976124885215823
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6461220644446088
Weighted AreaUnderROC: 0.841443322109989
Root mean squared error: 0.24235781647865517
Relative absolute error: 43.710099203122326
Root relative squared error: 84.30429796994004
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6022333328214493
Weighted FMeasure: 0.6313245753493131
Iteration time: 36.0
Weighted AreaUnderPRC: 0.4905217286942332
Mean absolute error: 0.072248097856401
Coverage of cases: 74.74747474747475
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 274.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.036161616161616165
Kappa statistic: 0.6022222222222222
Training time: 35.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 13.810835629017522
Incorrectly Classified Instances: 35.35353535353536
Correctly Classified Instances: 64.64646464646465
Weighted Precision: 0.6478712483429752
Weighted AreaUnderROC: 0.8623793490460158
Root mean squared error: 0.2337418466183003
Relative absolute error: 41.41316605205484
Root relative squared error: 81.30722817883077
Weighted TruePositiveRate: 0.6464646464646465
Weighted MatthewsCorrelation: 0.609220631832043
Weighted FMeasure: 0.6401807787227618
Iteration time: 33.0
Weighted AreaUnderPRC: 0.5401744315276436
Mean absolute error: 0.06845151413562828
Coverage of cases: 77.17171717171718
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 307.0
Weighted Recall: 0.6464646464646465
Weighted FalsePositiveRate: 0.03535353535353536
Kappa statistic: 0.6111111111111112
Training time: 32.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 11.404958677686027
Incorrectly Classified Instances: 31.91919191919192
Correctly Classified Instances: 68.08080808080808
Weighted Precision: 0.7057238979664623
Weighted AreaUnderROC: 0.8492749719416386
Root mean squared error: 0.23239718186427544
Relative absolute error: 36.657421123917146
Root relative squared error: 80.83948581450323
Weighted TruePositiveRate: 0.6808080808080809
Weighted MatthewsCorrelation: 0.6571564026711386
Weighted FMeasure: 0.6826497782711545
Iteration time: 35.0
Weighted AreaUnderPRC: 0.5400550426291411
Mean absolute error: 0.06059077871721882
Coverage of cases: 73.93939393939394
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 342.0
Weighted Recall: 0.6808080808080809
Weighted FalsePositiveRate: 0.031919191919191924
Kappa statistic: 0.648888888888889
Training time: 34.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 14.031221303948605
Incorrectly Classified Instances: 34.54545454545455
Correctly Classified Instances: 65.45454545454545
Weighted Precision: 0.6623076475522309
Weighted AreaUnderROC: 0.8504197530864197
Root mean squared error: 0.2342956657274663
Relative absolute error: 42.61988834211041
Root relative squared error: 81.49987445646667
Weighted TruePositiveRate: 0.6545454545454545
Weighted MatthewsCorrelation: 0.6215613055807904
Weighted FMeasure: 0.6522989657924484
Iteration time: 35.0
Weighted AreaUnderPRC: 0.5352800491627362
Mean absolute error: 0.07044609643324079
Coverage of cases: 74.94949494949495
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 377.0
Weighted Recall: 0.6545454545454545
Weighted FalsePositiveRate: 0.034545454545454546
Kappa statistic: 0.62
Training time: 34.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 13.627180899908224
Incorrectly Classified Instances: 35.55555555555556
Correctly Classified Instances: 64.44444444444444
Weighted Precision: 0.6585329596661866
Weighted AreaUnderROC: 0.854742985409652
Root mean squared error: 0.23564843212326106
Relative absolute error: 42.92000859223056
Root relative squared error: 81.97043498127998
Weighted TruePositiveRate: 0.6444444444444445
Weighted MatthewsCorrelation: 0.6123533230711237
Weighted FMeasure: 0.6423028235355426
Iteration time: 38.0
Weighted AreaUnderPRC: 0.5403721543941232
Mean absolute error: 0.07094216296236501
Coverage of cases: 74.34343434343434
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 415.0
Weighted Recall: 0.6444444444444445
Weighted FalsePositiveRate: 0.035555555555555556
Kappa statistic: 0.6088888888888889
Training time: 37.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 13.296602387511534
Incorrectly Classified Instances: 30.90909090909091
Correctly Classified Instances: 69.0909090909091
Weighted Precision: 0.7014583400280985
Weighted AreaUnderROC: 0.8723995510662177
Root mean squared error: 0.22205267457465705
Relative absolute error: 36.46616769331808
Root relative squared error: 77.24114334068823
Weighted TruePositiveRate: 0.6909090909090909
Weighted MatthewsCorrelation: 0.6631847988460665
Weighted FMeasure: 0.6906742680277046
Iteration time: 40.0
Weighted AreaUnderPRC: 0.5866232216784549
Mean absolute error: 0.06027465734432779
Coverage of cases: 78.58585858585859
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 455.0
Weighted Recall: 0.6909090909090909
Weighted FalsePositiveRate: 0.030909090909090914
Kappa statistic: 0.66
Training time: 39.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 12.947658402203896
Incorrectly Classified Instances: 30.303030303030305
Correctly Classified Instances: 69.6969696969697
Weighted Precision: 0.7168937363935728
Weighted AreaUnderROC: 0.8829090909090909
Root mean squared error: 0.2204059602265192
Relative absolute error: 35.514161976805504
Root relative squared error: 76.66833286115077
Weighted TruePositiveRate: 0.696969696969697
Weighted MatthewsCorrelation: 0.6731134736801896
Weighted FMeasure: 0.697761313195164
Iteration time: 43.0
Weighted AreaUnderPRC: 0.598308286295601
Mean absolute error: 0.0587010941765384
Coverage of cases: 81.01010101010101
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 498.0
Weighted Recall: 0.696969696969697
Weighted FalsePositiveRate: 0.030303030303030304
Kappa statistic: 0.6666666666666667
Training time: 43.0
		
Time end:Sun Oct 08 10.01.11 EEST 2017