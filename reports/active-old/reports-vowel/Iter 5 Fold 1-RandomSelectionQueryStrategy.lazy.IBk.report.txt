Sun Oct 08 10.09.33 EEST 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 10.09.33 EEST 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 54.545454545454064
Incorrectly Classified Instances: 61.81818181818182
Correctly Classified Instances: 38.18181818181818
Weighted Precision: 0.41061185785733306
Weighted AreaUnderROC: 0.6600000000000001
Root mean squared error: 0.31934851036818623
Relative absolute error: 71.2000000000004
Root relative squared error: 111.08555261598984
Weighted TruePositiveRate: 0.38181818181818183
Weighted MatthewsCorrelation: 0.32869141288998877
Weighted FMeasure: 0.3803945646722095
Iteration time: 1.0
Weighted AreaUnderPRC: 0.22047176851666941
Mean absolute error: 0.11768595041322456
Coverage of cases: 71.91919191919192
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1.0
Weighted Recall: 0.38181818181818183
Weighted FalsePositiveRate: 0.061818181818181814
Kappa statistic: 0.32
Training time: 0.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 45.45454545454594
Incorrectly Classified Instances: 55.35353535353536
Correctly Classified Instances: 44.64646464646464
Weighted Precision: 0.4876805768331035
Weighted AreaUnderROC: 0.6955555555555554
Root mean squared error: 0.3044972561654462
Relative absolute error: 64.1982905982902
Root relative squared error: 105.91953578300112
Weighted TruePositiveRate: 0.44646464646464645
Weighted MatthewsCorrelation: 0.40471405791049464
Weighted FMeasure: 0.4493102707157842
Iteration time: 1.0
Weighted AreaUnderPRC: 0.2753698828760113
Mean absolute error: 0.10611287702196796
Coverage of cases: 67.87878787878788
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 2.0
Weighted Recall: 0.44646464646464645
Weighted FalsePositiveRate: 0.05535353535353535
Kappa statistic: 0.3911111111111111
Training time: 0.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.36363636363658
Incorrectly Classified Instances: 49.696969696969695
Correctly Classified Instances: 50.303030303030305
Weighted Precision: 0.5417781873544982
Weighted AreaUnderROC: 0.7266666666666666
Root mean squared error: 0.2901315366173503
Relative absolute error: 57.991111111110946
Root relative squared error: 100.9224124430905
Weighted TruePositiveRate: 0.503030303030303
Weighted MatthewsCorrelation: 0.4665363186370011
Weighted FMeasure: 0.5058019740372011
Iteration time: 0.0
Weighted AreaUnderPRC: 0.3255558202237296
Mean absolute error: 0.09585307621671292
Coverage of cases: 67.07070707070707
Instances selection time: 0.0
Test time: 11.0
Accumulative iteration time: 2.0
Weighted Recall: 0.503030303030303
Weighted FalsePositiveRate: 0.0496969696969697
Kappa statistic: 0.4533333333333333
Training time: 0.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 27.272727272727032
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.6221387733603645
Weighted AreaUnderROC: 0.7677777777777777
Root mean squared error: 0.2686007767440089
Relative absolute error: 49.909803921568
Root relative squared error: 93.43292593815961
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.552397750120469
Weighted FMeasure: 0.5851463278588005
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4110967932158997
Mean absolute error: 0.08249554367201374
Coverage of cases: 67.87878787878788
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.04222222222222222
Kappa statistic: 0.5355555555555555
Training time: 0.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 18.18181818181829
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6518504414358812
Weighted AreaUnderROC: 0.7822222222222222
Root mean squared error: 0.26096295977425976
Relative absolute error: 46.823391812865275
Root relative squared error: 90.77610716081142
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5826654957260994
Weighted FMeasure: 0.6126998856716238
Iteration time: 0.0
Weighted AreaUnderPRC: 0.4441317326747972
Mean absolute error: 0.07739403605432327
Coverage of cases: 64.04040404040404
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 3.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 0.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 34.74747474747475
Correctly Classified Instances: 65.25252525252525
Weighted Precision: 0.6949369129191276
Weighted AreaUnderROC: 0.808888888888889
Root mean squared error: 0.24514220541131126
Relative absolute error: 41.458201058201034
Root relative squared error: 85.27284917102547
Weighted TruePositiveRate: 0.6525252525252525
Weighted MatthewsCorrelation: 0.6355163476157573
Weighted FMeasure: 0.6644436409863581
Iteration time: 1.0
Weighted AreaUnderPRC: 0.49479039191045926
Mean absolute error: 0.06852595216231619
Coverage of cases: 65.25252525252525
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6525252525252525
Weighted FalsePositiveRate: 0.03474747474747475
Kappa statistic: 0.6177777777777778
Training time: 0.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 31.11111111111111
Correctly Classified Instances: 68.88888888888889
Weighted Precision: 0.7153229712350992
Weighted AreaUnderROC: 0.828888888888889
Root mean squared error: 0.23248522205254132
Relative absolute error: 37.36811594202893
Root relative squared error: 80.87011064176376
Weighted TruePositiveRate: 0.6888888888888889
Weighted MatthewsCorrelation: 0.6684252146657139
Weighted FMeasure: 0.6954854394310533
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5312263422700336
Mean absolute error: 0.061765480895915985
Coverage of cases: 68.88888888888889
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6888888888888889
Weighted FalsePositiveRate: 0.031111111111111114
Kappa statistic: 0.6577777777777778
Training time: 1.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 26.86868686868687
Correctly Classified Instances: 73.13131313131314
Weighted Precision: 0.7525006137613237
Weighted AreaUnderROC: 0.852222222222222
Root mean squared error: 0.21647785797529576
Relative absolute error: 32.65511111111099
Root relative squared error: 75.30194036160167
Weighted TruePositiveRate: 0.7313131313131314
Weighted MatthewsCorrelation: 0.7131386814445748
Weighted FMeasure: 0.7369932135894577
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5873302228956693
Mean absolute error: 0.053975390266299494
Coverage of cases: 73.13131313131314
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7313131313131314
Weighted FalsePositiveRate: 0.02686868686868687
Kappa statistic: 0.7044444444444445
Training time: 0.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 24.242424242424242
Correctly Classified Instances: 75.75757575757575
Weighted Precision: 0.7702840407461575
Weighted AreaUnderROC: 0.8666666666666666
Root mean squared error: 0.2059576997065305
Relative absolute error: 29.654320987654167
Root relative squared error: 71.64249759937896
Weighted TruePositiveRate: 0.7575757575757576
Weighted MatthewsCorrelation: 0.7383412509339605
Weighted FMeasure: 0.7604644852146235
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6164638514064948
Mean absolute error: 0.049015406591164226
Coverage of cases: 75.75757575757575
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7575757575757576
Weighted FalsePositiveRate: 0.024242424242424242
Kappa statistic: 0.7333333333333333
Training time: 0.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 22.02020202020202
Correctly Classified Instances: 77.97979797979798
Weighted Precision: 0.7875833436594615
Weighted AreaUnderROC: 0.8788888888888888
Root mean squared error: 0.19656297308483114
Relative absolute error: 27.096551724137665
Root relative squared error: 68.37453684626817
Weighted TruePositiveRate: 0.7797979797979798
Weighted MatthewsCorrelation: 0.7601608717101722
Weighted FMeasure: 0.7797167449222325
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6444555936819595
Mean absolute error: 0.04478768880022783
Coverage of cases: 77.97979797979798
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7797979797979798
Weighted FalsePositiveRate: 0.022020202020202016
Kappa statistic: 0.7577777777777778
Training time: 0.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.595959595959595
Correctly Classified Instances: 80.4040404040404
Weighted Precision: 0.8132193791728266
Weighted AreaUnderROC: 0.892222222222222
Root mean squared error: 0.18565780420447817
Relative absolute error: 24.33906810035858
Root relative squared error: 64.58116793389078
Weighted TruePositiveRate: 0.804040404040404
Weighted MatthewsCorrelation: 0.7874466361906571
Weighted FMeasure: 0.8045275770088646
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6780002229396913
Mean absolute error: 0.04022986462869212
Coverage of cases: 80.4040404040404
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 9.0
Weighted Recall: 0.804040404040404
Weighted FalsePositiveRate: 0.019595959595959594
Kappa statistic: 0.7844444444444444
Training time: 0.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 19.19191919191919
Correctly Classified Instances: 80.8080808080808
Weighted Precision: 0.8145424098911198
Weighted AreaUnderROC: 0.8944444444444445
Root mean squared error: 0.18391051975791067
Relative absolute error: 23.740740740740506
Root relative squared error: 63.97337409104338
Weighted TruePositiveRate: 0.8080808080808081
Weighted MatthewsCorrelation: 0.790554197534557
Weighted FMeasure: 0.8072550737712267
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6814085291117151
Mean absolute error: 0.039240893786348194
Coverage of cases: 80.8080808080808
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8080808080808081
Weighted FalsePositiveRate: 0.019191919191919194
Kappa statistic: 0.7888888888888889
Training time: 0.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 18.585858585858585
Correctly Classified Instances: 81.41414141414141
Weighted Precision: 0.8171922268115968
Weighted AreaUnderROC: 0.8977777777777778
Root mean squared error: 0.18114097754820183
Relative absolute error: 22.944761904761506
Root relative squared error: 63.009987330591265
Weighted TruePositiveRate: 0.8141414141414142
Weighted MatthewsCorrelation: 0.795850442042245
Weighted FMeasure: 0.81245524838558
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6894222056483731
Mean absolute error: 0.03792522628886224
Coverage of cases: 81.41414141414141
Instances selection time: 0.0
Test time: 20.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8141414141414142
Weighted FalsePositiveRate: 0.018585858585858588
Kappa statistic: 0.7955555555555556
Training time: 1.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 17.373737373737374
Correctly Classified Instances: 82.62626262626263
Weighted Precision: 0.8313227785312819
Weighted AreaUnderROC: 0.9044444444444443
Root mean squared error: 0.17527845984794055
Relative absolute error: 21.515915915915734
Root relative squared error: 60.970707367444696
Weighted TruePositiveRate: 0.8262626262626263
Weighted MatthewsCorrelation: 0.8101575763696683
Weighted FMeasure: 0.8255309106053037
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7092790608237758
Mean absolute error: 0.035563497381679125
Coverage of cases: 82.62626262626263
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8262626262626263
Weighted FalsePositiveRate: 0.017373737373737378
Kappa statistic: 0.8088888888888889
Training time: 0.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 14.94949494949495
Correctly Classified Instances: 85.05050505050505
Weighted Precision: 0.8543703641539238
Weighted AreaUnderROC: 0.9177777777777777
Root mean squared error: 0.16272678132724194
Relative absolute error: 18.80113960113923
Root relative squared error: 56.60459918324646
Weighted TruePositiveRate: 0.8505050505050505
Weighted MatthewsCorrelation: 0.8364387427241446
Weighted FMeasure: 0.8497272820634975
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7459864361434366
Mean absolute error: 0.031076263803536116
Coverage of cases: 85.05050505050505
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8505050505050505
Weighted FalsePositiveRate: 0.014949494949494949
Kappa statistic: 0.8355555555555556
Training time: 0.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 12.727272727272727
Correctly Classified Instances: 87.27272727272727
Weighted Precision: 0.8790975092612959
Weighted AreaUnderROC: 0.93
Root mean squared error: 0.1502635740805747
Relative absolute error: 16.30731707317051
Root relative squared error: 52.269265779726176
Weighted TruePositiveRate: 0.8727272727272727
Weighted MatthewsCorrelation: 0.8619623228243969
Weighted FMeasure: 0.8727994712198839
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7810251955559305
Mean absolute error: 0.02695424309614977
Coverage of cases: 87.27272727272727
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8727272727272727
Weighted FalsePositiveRate: 0.01272727272727273
Kappa statistic: 0.86
Training time: 0.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 11.919191919191919
Correctly Classified Instances: 88.08080808080808
Weighted Precision: 0.8847519264190404
Weighted AreaUnderROC: 0.9344444444444445
Root mean squared error: 0.14550244088557748
Relative absolute error: 15.333850129198972
Root relative squared error: 50.61310301436735
Weighted TruePositiveRate: 0.8808080808080808
Weighted MatthewsCorrelation: 0.8702457869607023
Weighted FMeasure: 0.8812105970059164
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7941491108348053
Mean absolute error: 0.025345206825122427
Coverage of cases: 88.08080808080808
Instances selection time: 0.0
Test time: 23.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8808080808080808
Weighted FalsePositiveRate: 0.01191919191919192
Kappa statistic: 0.8688888888888889
Training time: 0.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 10.909090909090908
Correctly Classified Instances: 89.0909090909091
Weighted Precision: 0.8940798192406306
Weighted AreaUnderROC: 0.9400000000000001
Root mean squared error: 0.1392811723636858
Relative absolute error: 14.151111111111073
Root relative squared error: 48.44903138325181
Weighted TruePositiveRate: 0.8909090909090909
Weighted MatthewsCorrelation: 0.8811290056762562
Weighted FMeasure: 0.8913317656827815
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8099949593337206
Mean absolute error: 0.023390266299357292
Coverage of cases: 89.0909090909091
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8909090909090909
Weighted FalsePositiveRate: 0.01090909090909091
Kappa statistic: 0.88
Training time: 0.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 9.696969696969697
Correctly Classified Instances: 90.3030303030303
Weighted Precision: 0.9055735807211045
Weighted AreaUnderROC: 0.9466666666666665
Root mean squared error: 0.13139058694882488
Relative absolute error: 12.757446808510712
Root relative squared error: 45.70428696511465
Weighted TruePositiveRate: 0.9030303030303031
Weighted MatthewsCorrelation: 0.8941797591799953
Weighted FMeasure: 0.9032180673310255
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8288583121008544
Mean absolute error: 0.021086688939687265
Coverage of cases: 90.3030303030303
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9030303030303031
Weighted FalsePositiveRate: 0.009696969696969697
Kappa statistic: 0.8933333333333334
Training time: 0.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 9.090909090909145
Incorrectly Classified Instances: 6.8686868686868685
Correctly Classified Instances: 93.13131313131314
Weighted Precision: 0.9352387522211784
Weighted AreaUnderROC: 0.9622222222222222
Root mean squared error: 0.11067888834889887
Relative absolute error: 9.630839002267614
Root relative squared error: 38.49971136857913
Weighted TruePositiveRate: 0.9313131313131313
Weighted MatthewsCorrelation: 0.9256348931385228
Weighted FMeasure: 0.9313189336505374
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8769642510842244
Mean absolute error: 0.015918742152508554
Coverage of cases: 93.13131313131314
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9313131313131313
Weighted FalsePositiveRate: 0.0068686868686868695
Kappa statistic: 0.9244444444444444
Training time: 0.0
		
Time end:Sun Oct 08 10.09.34 EEST 2017