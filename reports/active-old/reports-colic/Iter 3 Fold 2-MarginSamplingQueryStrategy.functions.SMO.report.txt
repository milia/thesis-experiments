Sat Oct 07 11.32.59 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.59 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 45.108695652173914
Correctly Classified Instances: 54.891304347826086
Weighted Precision: 0.558476266453929
Weighted AreaUnderROC: 0.5266227180527383
Root mean squared error: 0.6716300741641481
Relative absolute error: 90.21739130434783
Root relative squared error: 134.32601483282963
Weighted TruePositiveRate: 0.5489130434782609
Weighted MatthewsCorrelation: 0.052302441063480025
Weighted FMeasure: 0.5529367251106381
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5470692929363
Mean absolute error: 0.45108695652173914
Coverage of cases: 54.891304347826086
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5489130434782609
Weighted FalsePositiveRate: 0.4956676073727842
Kappa statistic: 0.05213505461767635
Training time: 10.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 44.02173913043478
Correctly Classified Instances: 55.97826086956522
Weighted Precision: 0.6250631564951469
Weighted AreaUnderROC: 0.5900101419878296
Root mean squared error: 0.6634888026970371
Relative absolute error: 88.04347826086956
Root relative squared error: 132.69776053940743
Weighted TruePositiveRate: 0.5597826086956522
Weighted MatthewsCorrelation: 0.17683206264319434
Weighted FMeasure: 0.5635197752973722
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5832542070608094
Mean absolute error: 0.44021739130434784
Coverage of cases: 55.97826086956522
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.5597826086956522
Weighted FalsePositiveRate: 0.379762324719993
Kappa statistic: 0.1600541027953112
Training time: 10.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.23913043478261
Correctly Classified Instances: 65.76086956521739
Weighted Precision: 0.7256025161348201
Weighted AreaUnderROC: 0.6919371196754563
Root mean squared error: 0.5851421232041205
Relative absolute error: 68.47826086956522
Root relative squared error: 117.0284246408241
Weighted TruePositiveRate: 0.657608695652174
Weighted MatthewsCorrelation: 0.37560828954829495
Weighted FMeasure: 0.6611669294886235
Iteration time: 11.0
Weighted AreaUnderPRC: 0.656334226124034
Mean absolute error: 0.3423913043478261
Coverage of cases: 65.76086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.657608695652174
Weighted FalsePositiveRate: 0.27373445630126114
Kappa statistic: 0.34315503173164097
Training time: 10.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.78260869565217
Correctly Classified Instances: 65.21739130434783
Weighted Precision: 0.7474204074433372
Weighted AreaUnderROC: 0.6997971602434078
Root mean squared error: 0.5897678246195885
Relative absolute error: 69.56521739130434
Root relative squared error: 117.95356492391771
Weighted TruePositiveRate: 0.6521739130434783
Weighted MatthewsCorrelation: 0.3995943204868154
Weighted FMeasure: 0.6521739130434783
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6643289608392734
Mean absolute error: 0.34782608695652173
Coverage of cases: 65.21739130434783
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.6521739130434783
Weighted FalsePositiveRate: 0.25257959255666285
Kappa statistic: 0.3486725663716814
Training time: 11.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8189284145805885
Weighted AreaUnderROC: 0.8100912778904664
Root mean squared error: 0.44842720314644063
Relative absolute error: 40.21739130434783
Root relative squared error: 89.68544062928812
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.600448739193718
Weighted FMeasure: 0.8021345916189193
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7625215375145877
Mean absolute error: 0.20108695652173914
Coverage of cases: 79.8913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.1787304876973278
Kappa statistic: 0.5896817743490839
Training time: 15.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8262598058768781
Weighted AreaUnderROC: 0.8182048681541582
Root mean squared error: 0.4234947769299599
Relative absolute error: 35.869565217391305
Root relative squared error: 84.69895538599198
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.625138700330166
Weighted FMeasure: 0.8222519509476033
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7733523669956008
Mean absolute error: 0.1793478260869565
Coverage of cases: 82.06521739130434
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.18424243760472708
Kappa statistic: 0.6231380337636544
Training time: 32.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.811837163806231
Weighted AreaUnderROC: 0.8004563894523327
Root mean squared error: 0.4361391879943235
Relative absolute error: 38.04347826086957
Root relative squared error: 87.2278375988647
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.595835664090806
Weighted FMeasure: 0.810589348911985
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7567461854116485
Mean absolute error: 0.19021739130434784
Coverage of cases: 80.97826086956522
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.20886982979098684
Kappa statistic: 0.5954773869346734
Training time: 23.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8142404144614903
Weighted AreaUnderROC: 0.7986815415821501
Root mean squared error: 0.42986347681054754
Relative absolute error: 36.95652173913043
Root relative squared error: 85.9726953621095
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6011860012495134
Weighted FMeasure: 0.8146280858805194
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7574113099208418
Mean absolute error: 0.18478260869565216
Coverage of cases: 81.52173913043478
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 149.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.2178543081400476
Kappa statistic: 0.6010204081632652
Training time: 30.0
		
Time end:Sat Oct 07 11.33.00 EEST 2017