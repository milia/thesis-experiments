Sat Oct 07 11.32.47 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.47 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 70.3804347826087
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8091550048071788
Weighted AreaUnderROC: 0.7982378296146045
Root mean squared error: 0.39742173179243373
Relative absolute error: 53.86816764415985
Root relative squared error: 79.48434635848675
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5890421128088332
Weighted FMeasure: 0.8058841699678642
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7536408951208382
Mean absolute error: 0.26934083822079924
Coverage of cases: 91.84782608695652
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.20597054413969484
Kappa statistic: 0.5876494023904383
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 70.3804347826087
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8048371227230422
Weighted AreaUnderROC: 0.7982378296146045
Root mean squared error: 0.4116840944363649
Relative absolute error: 47.66226605782358
Root relative squared error: 82.33681888727298
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5793117995888312
Weighted FMeasure: 0.8007067328806459
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7536408951208382
Mean absolute error: 0.2383113302891179
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.2091564511861716
Kappa statistic: 0.5774577954319762
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 70.3804347826087
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8048371227230422
Weighted AreaUnderROC: 0.7982378296146045
Root mean squared error: 0.4181697501866032
Relative absolute error: 46.61361670863208
Root relative squared error: 83.63395003732063
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5793117995888312
Weighted FMeasure: 0.8007067328806459
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7536408951208382
Mean absolute error: 0.23306808354316041
Coverage of cases: 91.84782608695652
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.2091564511861716
Kappa statistic: 0.5774577954319762
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 68.75
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8410331434046848
Weighted AreaUnderROC: 0.8351926977687627
Root mean squared error: 0.37513448256712784
Relative absolute error: 41.725218341091335
Root relative squared error: 75.02689651342557
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6576938263647343
Weighted FMeasure: 0.841046043796152
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8053706787183004
Mean absolute error: 0.20862609170545668
Coverage of cases: 90.76086956521739
Instances selection time: 8.0
Test time: 0.0
Accumulative iteration time: 20.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.19583958020989506
Kappa statistic: 0.65653964984552
Training time: 2.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8423665291266509
Weighted AreaUnderROC: 0.8280299188640974
Root mean squared error: 0.37680319120383915
Relative absolute error: 62.61918496392711
Root relative squared error: 75.36063824076783
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6558560943060164
Weighted FMeasure: 0.8390237967436035
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8026774810782271
Mean absolute error: 0.31309592481963555
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 23.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.21409515830320133
Kappa statistic: 0.6500524658971668
Training time: 2.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.8280299188640974
Root mean squared error: 0.36175824694390696
Relative absolute error: 54.359500955717564
Root relative squared error: 72.3516493887814
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8026774810782271
Mean absolute error: 0.27179750477858783
Coverage of cases: 99.45652173913044
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 4.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.8324670385395537
Root mean squared error: 0.35946696696219677
Relative absolute error: 50.24288222388652
Root relative squared error: 71.89339339243935
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8069321521495989
Mean absolute error: 0.2512144111194326
Coverage of cases: 99.45652173913044
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 4.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.8324670385395537
Root mean squared error: 0.3599138749731351
Relative absolute error: 47.7890159797731
Root relative squared error: 71.98277499462702
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8069321521495989
Mean absolute error: 0.2389450798988655
Coverage of cases: 99.45652173913044
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 7.0
		
Time end:Sat Oct 07 11.32.47 EEST 2017