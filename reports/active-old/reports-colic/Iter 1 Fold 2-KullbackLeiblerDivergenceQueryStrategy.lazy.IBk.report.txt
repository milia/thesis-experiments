Sat Oct 07 11.32.29 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.29 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.697585983127839
Weighted AreaUnderROC: 0.657707910750507
Root mean squared error: 0.5279437066476833
Relative absolute error: 60.869565217391354
Root relative squared error: 105.58874132953666
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.34223902005751605
Weighted FMeasure: 0.6947030213706706
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6348033462178709
Mean absolute error: 0.30434782608695676
Coverage of cases: 70.65217391304348
Instances selection time: 16.0
Test time: 8.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.3911059176294206
Kappa statistic: 0.33369098712446355
Training time: 1.0
		
Time end:Sat Oct 07 11.32.29 EEST 2017