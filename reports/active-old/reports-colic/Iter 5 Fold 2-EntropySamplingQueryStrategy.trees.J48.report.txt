Sat Oct 07 11.32.19 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.19 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 64.67391304347827
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8270693979933111
Weighted AreaUnderROC: 0.8526876267748478
Root mean squared error: 0.4014142754788866
Relative absolute error: 40.9118357487923
Root relative squared error: 80.28285509577732
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6231236462239117
Weighted FMeasure: 0.8176690524516611
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8261604179997964
Mean absolute error: 0.20455917874396148
Coverage of cases: 90.21739130434783
Instances selection time: 9.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.17525795925566626
Kappa statistic: 0.6174168297455968
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.3568454190979556
Relative absolute error: 41.28251164596279
Root relative squared error: 71.36908381959111
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.20641255822981397
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.3538171061978742
Relative absolute error: 35.62312973068122
Root relative squared error: 70.76342123957484
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.1781156486534061
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.35428518369709305
Relative absolute error: 33.052158816425205
Root relative squared error: 70.85703673941862
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.16526079408212602
Coverage of cases: 91.84782608695652
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 2.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.3550657393687963
Relative absolute error: 31.577961019490157
Root relative squared error: 71.01314787375927
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.1578898050974508
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.35752737554508135
Relative absolute error: 40.427189897698206
Root relative squared error: 71.50547510901627
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.20213594948849103
Coverage of cases: 91.84782608695652
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 3.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 94.83695652173913
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.3488739333989516
Relative absolute error: 46.41687569676694
Root relative squared error: 69.77478667979032
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.23208437848383467
Coverage of cases: 96.73913043478261
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 3.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 94.83695652173913
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.35061400018418953
Relative absolute error: 44.47824028326745
Root relative squared error: 70.1228000368379
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8033827964410104
Mean absolute error: 0.22239120141633725
Coverage of cases: 96.73913043478261
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 6.0
		
Time end:Sat Oct 07 11.32.20 EEST 2017