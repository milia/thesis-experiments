Sat Oct 07 11.32.11 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.11 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.804753217796696
Weighted AreaUnderROC: 0.7657200811359026
Root mean squared error: 0.43133190451337783
Relative absolute error: 42.334096109839905
Root relative squared error: 86.26638090267556
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.569703890934128
Weighted FMeasure: 0.7976157082748948
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7334637637095105
Mean absolute error: 0.2116704805491995
Coverage of cases: 80.43478260869566
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.27290766381515125
Kappa statistic: 0.5586353944562901
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7877043050956094
Weighted AreaUnderROC: 0.7758620689655172
Root mean squared error: 0.4584673572178137
Relative absolute error: 45.42728635682154
Root relative squared error: 91.69347144356273
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5431167955389921
Weighted FMeasure: 0.784315744408738
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7312963590846389
Mean absolute error: 0.2271364317841077
Coverage of cases: 78.26086956521739
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 17.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.23088455772113942
Kappa statistic: 0.5418326693227092
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7872767346599298
Weighted AreaUnderROC: 0.7781440162271805
Root mean squared error: 0.4717750821894056
Relative absolute error: 47.03883073448293
Root relative squared error: 94.35501643788112
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5367796294987263
Weighted FMeasure: 0.7750618268847227
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7323182313864179
Mean absolute error: 0.23519415367241464
Coverage of cases: 77.17391304347827
Instances selection time: 10.0
Test time: 24.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.21900079372078668
Kappa statistic: 0.5301556420233462
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7873531330754332
Weighted AreaUnderROC: 0.7725659229208925
Root mean squared error: 0.48956941672017573
Relative absolute error: 49.955634427684096
Root relative squared error: 97.91388334403514
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.5262872322893756
Weighted FMeasure: 0.7596665240549355
Iteration time: 11.0
Weighted AreaUnderPRC: 0.72556723897888
Mean absolute error: 0.2497781721384205
Coverage of cases: 75.54347826086956
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.21030293676691067
Kappa statistic: 0.5094786729857821
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7974967061923585
Weighted AreaUnderROC: 0.7867647058823529
Root mean squared error: 0.4681038261553214
Relative absolute error: 45.5047899778924
Root relative squared error: 93.62076523106428
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5552800687065372
Weighted FMeasure: 0.7807437366588023
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7394894261193048
Mean absolute error: 0.227523949889462
Coverage of cases: 77.71739130434783
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.20364450127877237
Kappa statistic: 0.5453230472516876
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7877043050956094
Weighted AreaUnderROC: 0.7823275862068965
Root mean squared error: 0.46291677197051895
Relative absolute error: 44.27395949755997
Root relative squared error: 92.5833543941038
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5431167955389921
Weighted FMeasure: 0.784315744408738
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7374868267699238
Mean absolute error: 0.22136979748779986
Coverage of cases: 78.26086956521739
Instances selection time: 6.0
Test time: 20.0
Accumulative iteration time: 50.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.23088455772113942
Kappa statistic: 0.5418326693227092
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8010529510266678
Weighted AreaUnderROC: 0.7963362068965517
Root mean squared error: 0.44562368834075805
Relative absolute error: 40.9502083997911
Root relative squared error: 89.12473766815161
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5727061783961418
Weighted FMeasure: 0.799765883135527
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7520879966545084
Mean absolute error: 0.2047510419989555
Coverage of cases: 79.8913043478261
Instances selection time: 4.0
Test time: 21.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.22132683658170915
Kappa statistic: 0.5723618090452262
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8188805046512557
Weighted AreaUnderROC: 0.8071754563894523
Root mean squared error: 0.4211454317358022
Relative absolute error: 36.57191590835696
Root relative squared error: 84.22908634716045
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6102367032074357
Weighted FMeasure: 0.819121360181828
Iteration time: 1.0
Weighted AreaUnderPRC: 0.766963287329542
Mean absolute error: 0.18285957954178478
Coverage of cases: 82.06521739130434
Instances selection time: 1.0
Test time: 41.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.22075359379133963
Kappa statistic: 0.6091658084449021
Training time: 0.0
		
Time end:Sat Oct 07 11.32.12 EEST 2017