Sat Oct 07 11.32.30 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.30 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 64.67391304347827
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8270693979933111
Weighted AreaUnderROC: 0.8526876267748478
Root mean squared error: 0.4014142754788866
Relative absolute error: 40.9118357487923
Root relative squared error: 80.28285509577732
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6231236462239117
Weighted FMeasure: 0.8176690524516611
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8261604179997964
Mean absolute error: 0.20455917874396148
Coverage of cases: 90.21739130434783
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.17525795925566626
Kappa statistic: 0.6174168297455968
Training time: 4.0
		
Time end:Sat Oct 07 11.32.30 EEST 2017