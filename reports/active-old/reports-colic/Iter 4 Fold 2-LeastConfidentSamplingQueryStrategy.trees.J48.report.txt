Sat Oct 07 11.32.48 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.48 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 79.6195652173913
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8080628803245437
Root mean squared error: 0.38941518907116945
Relative absolute error: 46.89613526570046
Root relative squared error: 77.88303781423389
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7872292242651734
Mean absolute error: 0.23448067632850228
Coverage of cases: 91.30434782608695
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 77.17391304347827
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.39038598400362384
Relative absolute error: 41.1052611714976
Root relative squared error: 78.07719680072476
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.20552630585748802
Coverage of cases: 89.67391304347827
Instances selection time: 7.0
Test time: 0.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 77.17391304347827
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.3919727905575411
Relative absolute error: 37.81702898550723
Root relative squared error: 78.39455811150822
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.18908514492753614
Coverage of cases: 89.67391304347827
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.54347826086956
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.3933225076521449
Relative absolute error: 36.3057945853463
Root relative squared error: 78.66450153042898
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.1815289729267315
Coverage of cases: 84.23913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.54347826086956
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.3942794899655793
Relative absolute error: 35.432804431117724
Root relative squared error: 78.85589799311586
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.1771640221555886
Coverage of cases: 84.23913043478261
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 2.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 77.17391304347827
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.3912955516080037
Relative absolute error: 41.6976845085861
Root relative squared error: 78.25911032160074
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.2084884225429305
Coverage of cases: 89.67391304347827
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 2.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 94.29347826086956
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.3842695150936605
Relative absolute error: 47.51690279707514
Root relative squared error: 76.8539030187321
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.2375845139853757
Coverage of cases: 95.65217391304348
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 3.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 77.17391304347827
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7979842799188641
Root mean squared error: 0.38754665433639174
Relative absolute error: 46.29631911961483
Root relative squared error: 77.50933086727835
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7746537028862157
Mean absolute error: 0.23148159559807416
Coverage of cases: 89.67391304347827
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 3.0
		
Time end:Sat Oct 07 11.32.48 EEST 2017