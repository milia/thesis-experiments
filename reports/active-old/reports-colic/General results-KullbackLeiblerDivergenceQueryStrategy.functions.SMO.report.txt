Sat Oct 07 11.32.27 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.27 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.56521739130435
Correctly Classified Instances: 70.43478260869566
Weighted Precision: 0.7007711762697155
Weighted AreaUnderROC: 0.6794117647058823
Root mean squared error: 0.539639680642219
Relative absolute error: 59.1304347826087
Root relative squared error: 107.92793612844382
Weighted TruePositiveRate: 0.7043478260869567
Weighted MatthewsCorrelation: 0.35702910433114876
Weighted FMeasure: 0.6983984752791861
Iteration time: 27.8
Weighted AreaUnderPRC: 0.6556374531511303
Mean absolute error: 0.29565217391304344
Coverage of cases: 70.43478260869566
Instances selection time: 3.5
Test time: 7.4
Accumulative iteration time: 27.8
Weighted Recall: 0.7043478260869567
Weighted FalsePositiveRate: 0.3455242966751918
Kappa statistic: 0.3560628706800998
Training time: 24.3
		
Time end:Sat Oct 07 11.32.28 EEST 2017