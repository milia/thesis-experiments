Sat Oct 07 11.32.31 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.31 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 62.5
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7524225883909679
Weighted AreaUnderROC: 0.7578393707557984
Root mean squared error: 0.45817646145524676
Relative absolute error: 49.72611463171887
Root relative squared error: 91.63529229104935
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46786533373554035
Weighted FMeasure: 0.7533473093388565
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7486951583053395
Mean absolute error: 0.24863057315859435
Coverage of cases: 85.32608695652173
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.29549563453567335
Kappa statistic: 0.46704428424304845
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 61.41304347826087
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779336522215083
Weighted AreaUnderROC: 0.7650724270217831
Root mean squared error: 0.4402573619347415
Relative absolute error: 46.04407154611146
Root relative squared error: 88.0514723869483
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5226235109494171
Weighted FMeasure: 0.7784736124657072
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7509521151621196
Mean absolute error: 0.2302203577305573
Coverage of cases: 84.78260869565217
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2795660993032896
Kappa statistic: 0.5188284518828452
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 61.95652173913044
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8166362622884362
Weighted AreaUnderROC: 0.7986815415821501
Root mean squared error: 0.41201870197587376
Relative absolute error: 42.199536000920475
Root relative squared error: 82.40374039517475
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.594709977749939
Weighted FMeasure: 0.8088592800374006
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7960807908364945
Mean absolute error: 0.21099768000460237
Coverage of cases: 87.5
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.260450657024429
Kappa statistic: 0.5831556503198293
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8115397188623734
Weighted AreaUnderROC: 0.8012170385395537
Root mean squared error: 0.40286475138274835
Relative absolute error: 41.63045191473894
Root relative squared error: 80.57295027654968
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5823917839948547
Weighted FMeasure: 0.8026890169373145
Iteration time: 2.0
Weighted AreaUnderPRC: 0.801241847593601
Mean absolute error: 0.2081522595736947
Coverage of cases: 87.5
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2697217567686745
Kappa statistic: 0.5695187165775402
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 61.141304347826086
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8235262068214013
Weighted AreaUnderROC: 0.8052738336713996
Root mean squared error: 0.400638986012081
Relative absolute error: 39.78091206076323
Root relative squared error: 80.12779720241619
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6075467342894683
Weighted FMeasure: 0.8139639302551821
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8068567182778565
Mean absolute error: 0.19890456030381615
Coverage of cases: 88.04347826086956
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.25726474997795223
Kappa statistic: 0.5941176470588234
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 60.59782608695652
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8151740369131674
Weighted AreaUnderROC: 0.8040060851926978
Root mean squared error: 0.4007783279574187
Relative absolute error: 39.633597603977286
Root relative squared error: 80.15566559148374
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.5945154033392165
Weighted FMeasure: 0.8098731521884
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8049546672713364
Mean absolute error: 0.19816798801988642
Coverage of cases: 88.04347826086956
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.25436546432666024
Kappa statistic: 0.5858050847457626
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 60.869565217391305
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8080235679922161
Weighted AreaUnderROC: 0.8009634888438135
Root mean squared error: 0.41063258240771106
Relative absolute error: 40.958660570402415
Root relative squared error: 82.12651648154221
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5828067472404553
Weighted FMeasure: 0.8057183753802112
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8085492532971056
Mean absolute error: 0.20479330285201208
Coverage of cases: 88.58695652173913
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2514661786753682
Kappa statistic: 0.5776495278069256
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 62.77173913043478
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.791508152173913
Weighted AreaUnderROC: 0.7922160243407709
Root mean squared error: 0.4262315401404233
Relative absolute error: 44.75664154230958
Root relative squared error: 85.24630802808466
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5519506024506531
Weighted FMeasure: 0.7920993278399322
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7954754609768014
Mean absolute error: 0.2237832077115479
Coverage of cases: 87.5
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.24885351441926098
Kappa statistic: 0.5513347022587268
Training time: 1.0
		
Time end:Sat Oct 07 11.32.31 EEST 2017