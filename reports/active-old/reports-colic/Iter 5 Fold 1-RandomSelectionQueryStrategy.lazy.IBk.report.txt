Sat Oct 07 11.33.28 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.28 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.7322833606864488
Weighted AreaUnderROC: 0.7108265720081136
Root mean squared error: 0.5132394344546206
Relative absolute error: 57.794235883480795
Root relative squared error: 102.64788689092413
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.42324157329550277
Weighted FMeasure: 0.7258348404273199
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6761675004491923
Mean absolute error: 0.28897117941740397
Coverage of cases: 72.28260869565217
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 0.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.29027030602345893
Kappa statistic: 0.4210266535044422
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.7322833606864488
Weighted AreaUnderROC: 0.7108265720081136
Root mean squared error: 0.5176913355615123
Relative absolute error: 56.980720166232615
Root relative squared error: 103.53826711230245
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.42324157329550277
Weighted FMeasure: 0.7258348404273199
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6761675004491923
Mean absolute error: 0.28490360083116306
Coverage of cases: 72.28260869565217
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.29027030602345893
Kappa statistic: 0.4210266535044422
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.742974822820414
Weighted AreaUnderROC: 0.713932555780933
Root mean squared error: 0.5097483609956815
Relative absolute error: 54.4661859879252
Root relative squared error: 101.94967219913632
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.44606585509103985
Weighted FMeasure: 0.7365864153125231
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6815147617399792
Mean absolute error: 0.272330929939626
Coverage of cases: 73.3695652173913
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.27781329923273657
Kappa statistic: 0.44373149062191514
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.7255634615697261
Weighted AreaUnderROC: 0.6989097363083164
Root mean squared error: 0.5212768078565279
Relative absolute error: 56.34427684117129
Root relative squared error: 104.25536157130557
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.4107997785334925
Weighted FMeasure: 0.7240016227003209
Iteration time: 7.0
Weighted AreaUnderPRC: 0.666398183475501
Mean absolute error: 0.28172138420585646
Coverage of cases: 72.28260869565217
Instances selection time: 7.0
Test time: 24.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.3085258841167651
Kappa statistic: 0.41055276381909545
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7642339544513456
Weighted AreaUnderROC: 0.740365111561866
Root mean squared error: 0.4850148022574036
Relative absolute error: 48.71039056742811
Root relative squared error: 97.00296045148072
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.49351047957583793
Weighted FMeasure: 0.7621880788928843
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7032507426411018
Mean absolute error: 0.24355195283714054
Coverage of cases: 76.08695652173913
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.2618837640003528
Kappa statistic: 0.4929859719438878
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7794845254675417
Weighted AreaUnderROC: 0.7559584178498986
Root mean squared error: 0.4687500095288962
Relative absolute error: 45.36862003780714
Root relative squared error: 93.75000190577924
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5264472070068135
Weighted FMeasure: 0.7781189515826109
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7184233904635076
Mean absolute error: 0.2268431001890357
Coverage of cases: 77.71739130434783
Instances selection time: 0.0
Test time: 16.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.24624085016315372
Kappa statistic: 0.5261306532663317
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7717391304347826
Weighted AreaUnderROC: 0.7543737322515212
Root mean squared error: 0.4748103984792534
Relative absolute error: 46.32986759865248
Root relative squared error: 94.96207969585068
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5101419878296146
Weighted FMeasure: 0.7717391304347826
Iteration time: 0.0
Weighted AreaUnderPRC: 0.713618942164889
Mean absolute error: 0.2316493379932624
Coverage of cases: 77.17391304347827
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.26159714260516803
Kappa statistic: 0.5101419878296146
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7595297112614725
Weighted AreaUnderROC: 0.7401749492900609
Root mean squared error: 0.4863176501135672
Relative absolute error: 48.40320270259078
Root relative squared error: 97.26353002271344
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.4838067310564844
Weighted FMeasure: 0.7601069346689073
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7010209922277567
Mean absolute error: 0.2420160135129539
Coverage of cases: 76.08695652173913
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.28013934209365904
Kappa statistic: 0.4836734693877552
Training time: 0.0
		
Time end:Sat Oct 07 11.33.29 EEST 2017