Sat Oct 07 11.32.54 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.54 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 60.32608695652174
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7739947499261165
Weighted AreaUnderROC: 0.7583878097715848
Root mean squared error: 0.45979511187090744
Relative absolute error: 49.694604894732684
Root relative squared error: 91.95902237418149
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5131876776411164
Weighted FMeasure: 0.7743913875503622
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7712663821796183
Mean absolute error: 0.24847302447366343
Coverage of cases: 82.6086956521739
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.2766668136519975
Kappa statistic: 0.5113989637305699
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 65.76086956521739
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7851786821503283
Weighted AreaUnderROC: 0.7678890334244642
Root mean squared error: 0.4352866907134335
Relative absolute error: 48.497968637903675
Root relative squared error: 87.0573381426867
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5371052391117257
Weighted FMeasure: 0.7853966857186372
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7643167063423195
Mean absolute error: 0.24248984318951838
Coverage of cases: 88.58695652173913
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.26420980686127526
Kappa statistic: 0.5352331606217615
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 67.1195652173913
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8019167835437121
Weighted AreaUnderROC: 0.7734809066055207
Root mean squared error: 0.42301974308231555
Relative absolute error: 47.062426862632535
Root relative squared error: 84.60394861646311
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5722695350728849
Weighted FMeasure: 0.8014945652173914
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7835315132835007
Mean absolute error: 0.23531213431316267
Coverage of cases: 89.1304347826087
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24856689302407622
Kappa statistic: 0.5696465696465697
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 64.67391304347827
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7900415601023019
Root mean squared error: 0.421812720582615
Relative absolute error: 45.404497482786375
Root relative squared error: 84.362544116523
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7978482740251667
Mean absolute error: 0.22702248741393188
Coverage of cases: 88.58695652173913
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 62.5
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7768900476232473
Root mean squared error: 0.42897233986207023
Relative absolute error: 45.1353801075117
Root relative squared error: 85.79446797241404
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7866615143915743
Mean absolute error: 0.2256769005375585
Coverage of cases: 86.41304347826087
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7808669194814356
Root mean squared error: 0.4351912620573136
Relative absolute error: 45.99263865522192
Root relative squared error: 87.03825241146272
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 1.0
Weighted AreaUnderPRC: 0.790491298080264
Mean absolute error: 0.2299631932761096
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 61.41304347826087
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7448029056724709
Weighted AreaUnderROC: 0.7739466663726959
Root mean squared error: 0.48653306009675895
Relative absolute error: 55.19183241698221
Root relative squared error: 97.30661201935179
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4512661609993095
Weighted FMeasure: 0.7411788932904856
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7833209503839239
Mean absolute error: 0.27595916208491106
Coverage of cases: 83.15217391304348
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.2807125848840286
Kappa statistic: 0.4501992031872509
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 60.59782608695652
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7282240592330831
Weighted AreaUnderROC: 0.7690038252932357
Root mean squared error: 0.4895894683611594
Relative absolute error: 55.05928755316711
Root relative squared error: 97.91789367223188
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.4140784069429701
Weighted FMeasure: 0.7207046236230275
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7752436413238774
Mean absolute error: 0.27529643776583557
Coverage of cases: 82.6086956521739
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.29345621306993563
Kappa statistic: 0.41141732283464566
Training time: 1.0
		
Time end:Sat Oct 07 11.32.55 EEST 2017