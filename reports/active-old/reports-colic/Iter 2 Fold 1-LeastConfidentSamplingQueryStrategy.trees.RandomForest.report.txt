Sat Oct 07 11.32.50 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.50 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 95.92391304347827
Incorrectly Classified Instances: 30.434782608695652
Correctly Classified Instances: 69.56521739130434
Weighted Precision: 0.7105978260869565
Weighted AreaUnderROC: 0.7759888438133874
Root mean squared error: 0.43628395607901205
Relative absolute error: 70.67427066439795
Root relative squared error: 87.25679121580241
Weighted TruePositiveRate: 0.6956521739130435
Weighted MatthewsCorrelation: 0.3052531952078211
Weighted FMeasure: 0.6471329552614997
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7904572034848967
Mean absolute error: 0.35337135332198977
Coverage of cases: 99.45652173913044
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6956521739130435
Weighted FalsePositiveRate: 0.4826704294911368
Kappa statistic: 0.24590163934426218
Training time: 6.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 94.56521739130434
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7860743541272843
Weighted AreaUnderROC: 0.8522439148073022
Root mean squared error: 0.39075648290736004
Relative absolute error: 63.26646929343321
Root relative squared error: 78.15129658147201
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5200509674923437
Weighted FMeasure: 0.7710469257704343
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8639492454980663
Mean absolute error: 0.31633234646716607
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.3160772554899021
Kappa statistic: 0.5000000000000001
Training time: 3.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7809871288132157
Weighted AreaUnderROC: 0.8349391480730223
Root mean squared error: 0.39898506683357
Relative absolute error: 69.25255198121022
Root relative squared error: 79.797013366714
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5196917173025061
Weighted FMeasure: 0.7751285647498831
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8444515907442919
Mean absolute error: 0.3462627599060511
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2978216773965958
Kappa statistic: 0.5095948827292112
Training time: 12.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.802087051712239
Weighted AreaUnderROC: 0.8263184584178499
Root mean squared error: 0.4013165408231443
Relative absolute error: 71.83438472908044
Root relative squared error: 80.26330816462885
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5710926268842421
Weighted FMeasure: 0.8006262512191364
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8303645729889361
Mean absolute error: 0.35917192364540224
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 60.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.25465208572184495
Kappa statistic: 0.5669456066945607
Training time: 19.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8619472315124489
Weighted AreaUnderROC: 0.8667596348884381
Root mean squared error: 0.36334738909895437
Relative absolute error: 61.994198836007456
Root relative squared error: 72.66947781979087
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6934221250520698
Weighted FMeasure: 0.8546088810852472
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8759759570787177
Mean absolute error: 0.30997099418003726
Coverage of cases: 99.45652173913044
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 83.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.20453743716377104
Kappa statistic: 0.6832627118644069
Training time: 20.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8456148763853367
Weighted AreaUnderROC: 0.8809584178498986
Root mean squared error: 0.3654809540531397
Relative absolute error: 60.633947201233084
Root relative squared error: 73.09619081062793
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6477278872343277
Weighted FMeasure: 0.8293710287713286
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8861178388676791
Mean absolute error: 0.3031697360061654
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.24770702883852191
Kappa statistic: 0.6274298056155508
Training time: 19.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.854834523036989
Weighted AreaUnderROC: 0.8886916835699799
Root mean squared error: 0.36056097673681886
Relative absolute error: 60.9115108846795
Root relative squared error: 72.11219534736377
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6712726759970572
Weighted FMeasure: 0.8416978629329401
Iteration time: 21.0
Weighted AreaUnderPRC: 0.902454835942478
Mean absolute error: 0.3045575544233975
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 124.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.22916482935003085
Kappa statistic: 0.6545064377682404
Training time: 20.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8194712217036944
Weighted AreaUnderROC: 0.908975659229209
Root mean squared error: 0.3538165501571706
Relative absolute error: 59.29254379175447
Root relative squared error: 70.76331003143412
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6071565295956425
Weighted FMeasure: 0.8168201825013419
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9190133000280092
Mean absolute error: 0.29646271895877235
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 148.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.23900917188464593
Kappa statistic: 0.6017838405036726
Training time: 23.0
		
Time end:Sat Oct 07 11.32.50 EEST 2017