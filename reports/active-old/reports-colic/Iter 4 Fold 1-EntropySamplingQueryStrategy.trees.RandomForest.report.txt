Sat Oct 07 11.32.24 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.24 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7686869151357689
Weighted AreaUnderROC: 0.8316430020283975
Root mean squared error: 0.3996611865386712
Relative absolute error: 70.77553795640392
Root relative squared error: 79.93223730773424
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.502310798467565
Weighted FMeasure: 0.7693488097580168
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8503474747077666
Mean absolute error: 0.3538776897820196
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.27985272069847433
Kappa statistic: 0.5010330578512396
Training time: 9.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 99.18478260869566
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.7568427195798075
Weighted AreaUnderROC: 0.783341784989858
Root mean squared error: 0.4268656154348886
Relative absolute error: 75.93938722264586
Root relative squared error: 85.37312308697771
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.43219786638966295
Weighted FMeasure: 0.7195504830475402
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7796781388290038
Mean absolute error: 0.3796969361132293
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.39314533909515825
Kappa statistic: 0.39064261555806074
Training time: 5.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7686869151357689
Weighted AreaUnderROC: 0.8002028397565922
Root mean squared error: 0.41057245888615007
Relative absolute error: 70.32034805992798
Root relative squared error: 82.11449177723001
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.502310798467565
Weighted FMeasure: 0.7693488097580168
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7964862344776263
Mean absolute error: 0.3516017402996399
Coverage of cases: 98.91304347826087
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.27985272069847433
Kappa statistic: 0.5010330578512396
Training time: 8.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8166362622884362
Weighted AreaUnderROC: 0.8258113590263692
Root mean squared error: 0.388097279747655
Relative absolute error: 66.32623248716168
Root relative squared error: 77.619455949531
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.594709977749939
Weighted FMeasure: 0.8088592800374006
Iteration time: 42.0
Weighted AreaUnderPRC: 0.8396140403432016
Mean absolute error: 0.33163116243580837
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.260450657024429
Kappa statistic: 0.5831556503198293
Training time: 34.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 95.1086956521739
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8474991827394573
Weighted AreaUnderROC: 0.8360801217038538
Root mean squared error: 0.3735708129477984
Relative absolute error: 58.11477190055881
Root relative squared error: 74.71416258955969
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6578566348786951
Weighted FMeasure: 0.8365137568909178
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8412067872421376
Mean absolute error: 0.290573859502794
Coverage of cases: 97.82608695652173
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 151.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.2323507363965076
Kappa statistic: 0.6433155080213903
Training time: 69.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8669305645684622
Weighted AreaUnderROC: 0.8557302231237323
Root mean squared error: 0.3629622495171922
Relative absolute error: 59.82244395595326
Root relative squared error: 72.59244990343844
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6965829572231759
Weighted FMeasure: 0.8530051584377303
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8596427558443267
Mean absolute error: 0.2991122197797663
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 186.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.2167078225593086
Kappa statistic: 0.6791845493562233
Training time: 34.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 97.28260869565217
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8354562803209712
Weighted AreaUnderROC: 0.8689148073022313
Root mean squared error: 0.36071050393053716
Relative absolute error: 58.65172433803736
Root relative squared error: 72.14210078610743
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.645236345248027
Weighted FMeasure: 0.8352491498271549
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8765758326010757
Mean absolute error: 0.2932586216901868
Coverage of cases: 99.45652173913044
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 211.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.20511067995414056
Kappa statistic: 0.6435950413223142
Training time: 19.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8641684402553969
Weighted AreaUnderROC: 0.8493914807302232
Root mean squared error: 0.37605023140284644
Relative absolute error: 63.92545204754862
Root relative squared error: 75.21004628056929
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6947343250131828
Weighted FMeasure: 0.8538335670874241
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8589574133987342
Mean absolute error: 0.31962726023774307
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 259.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.21062262986153985
Kappa statistic: 0.6812366737739873
Training time: 42.0
		
Time end:Sat Oct 07 11.32.25 EEST 2017