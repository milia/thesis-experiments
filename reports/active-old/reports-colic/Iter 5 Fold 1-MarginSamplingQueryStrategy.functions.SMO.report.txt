Sat Oct 07 11.33.01 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.01 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7157611487014582
Weighted AreaUnderROC: 0.6937119675456389
Root mean squared error: 0.531609533071195
Relative absolute error: 56.52173913043478
Root relative squared error: 106.321906614239
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.3899033149020613
Weighted FMeasure: 0.716490013699618
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6597108342726874
Mean absolute error: 0.2826086956521739
Coverage of cases: 71.73913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.3299673692565482
Kappa statistic: 0.38979591836734684
Training time: 9.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.17391304347826
Correctly Classified Instances: 72.82608695652173
Weighted Precision: 0.7216276477146042
Weighted AreaUnderROC: 0.6871196754563894
Root mean squared error: 0.5212860351426869
Relative absolute error: 54.347826086956516
Root relative squared error: 104.25720702853738
Weighted TruePositiveRate: 0.7282608695652174
Weighted MatthewsCorrelation: 0.3967019599135097
Weighted FMeasure: 0.7204016943947059
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6578849789152247
Mean absolute error: 0.2717391304347826
Coverage of cases: 72.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7282608695652174
Weighted FalsePositiveRate: 0.3540215186524385
Kappa statistic: 0.39088983050847464
Training time: 9.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.741321566270755
Weighted AreaUnderROC: 0.6939655172413793
Root mean squared error: 0.5054055625027248
Relative absolute error: 51.08695652173913
Root relative squared error: 101.08111250054496
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.42936709916118826
Weighted FMeasure: 0.7318439594432032
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6673617959263479
Mean absolute error: 0.2554347826086957
Coverage of cases: 74.45652173913044
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.3566341829085457
Kappa statistic: 0.4144095341278439
Training time: 10.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7370976849237718
Weighted AreaUnderROC: 0.6835699797160243
Root mean squared error: 0.5107539184552492
Relative absolute error: 52.17391304347826
Root relative squared error: 102.15078369104984
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4154557274214364
Weighted FMeasure: 0.7234083850931677
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6597795399307687
Mean absolute error: 0.2608695652173913
Coverage of cases: 73.91304347826087
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.37199047535056
Kappa statistic: 0.3960612691466082
Training time: 11.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7601650735266855
Weighted AreaUnderROC: 0.6965010141987829
Root mean squared error: 0.49453535504684026
Relative absolute error: 48.91304347826087
Root relative squared error: 98.90707100936805
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.4558461548308078
Weighted FMeasure: 0.7378922029899294
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6740043896119556
Mean absolute error: 0.24456521739130435
Coverage of cases: 75.54347826086956
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.36243275421112975
Kappa statistic: 0.42817679558011057
Training time: 12.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7124631849065407
Weighted AreaUnderROC: 0.6572008113590264
Root mean squared error: 0.531609533071195
Relative absolute error: 56.52173913043478
Root relative squared error: 106.321906614239
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.36157584720658653
Weighted FMeasure: 0.6982320678983976
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6377378955522677
Mean absolute error: 0.2826086956521739
Coverage of cases: 71.73913043478261
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 74.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.40298968162977333
Kappa statistic: 0.3414096916299558
Training time: 14.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7662511732175533
Weighted AreaUnderROC: 0.717292089249493
Root mean squared error: 0.4834207816942845
Relative absolute error: 46.73913043478261
Root relative squared error: 96.6841563388569
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.48100340389691293
Weighted FMeasure: 0.7546657501288881
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6891836424188493
Mean absolute error: 0.23369565217391305
Coverage of cases: 76.6304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.33172016932710113
Kappa statistic: 0.4642470205850487
Training time: 17.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7800941409637062
Weighted AreaUnderROC: 0.7454361054766735
Root mean squared error: 0.4662524041201569
Relative absolute error: 43.47826086956522
Root relative squared error: 93.25048082403138
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5203353620545764
Weighted FMeasure: 0.7763213555157648
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7114741031565228
Mean absolute error: 0.21739130434782608
Coverage of cases: 78.26086956521739
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 115.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.291736484698827
Kappa statistic: 0.5127118644067797
Training time: 23.0
		
Time end:Sat Oct 07 11.33.01 EEST 2017