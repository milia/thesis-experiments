Sat Oct 07 11.32.48 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.48 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 93.75
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8436161411894475
Weighted AreaUnderROC: 0.8091404665314401
Root mean squared error: 0.36409559910552636
Relative absolute error: 51.438245015778946
Root relative squared error: 72.81911982110528
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6560620296549327
Weighted FMeasure: 0.8382438447277107
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7915818629350113
Mean absolute error: 0.25719122507889475
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 1.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.22018035100097014
Kappa statistic: 0.6478352692713832
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 100.0
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8502539328626285
Weighted AreaUnderROC: 0.8296972837110855
Root mean squared error: 0.3617508576574185
Relative absolute error: 48.08783794915732
Root relative squared error: 72.3501715314837
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6686954446238564
Weighted FMeasure: 0.8434249488610351
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8088949137923048
Mean absolute error: 0.2404391897457866
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21699444395449338
Kappa statistic: 0.6588983050847458
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 100.0
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8579497016197785
Weighted AreaUnderROC: 0.8301850912778904
Root mean squared error: 0.3615622396399736
Relative absolute error: 48.8874434811457
Root relative squared error: 72.31244792799471
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6733694387083201
Weighted FMeasure: 0.8407462935199067
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8102775731918281
Mean absolute error: 0.24443721740572852
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.23525002204779966
Kappa statistic: 0.652267818574514
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 100.0
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8579497016197785
Weighted AreaUnderROC: 0.8301850912778904
Root mean squared error: 0.36260495058785164
Relative absolute error: 50.0714893355347
Root relative squared error: 72.52099011757032
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6733694387083201
Weighted FMeasure: 0.8407462935199067
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8102775731918281
Mean absolute error: 0.2503574466776735
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.23525002204779966
Kappa statistic: 0.652267818574514
Training time: 2.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 99.45652173913044
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8502119688455092
Weighted AreaUnderROC: 0.8348757606490872
Root mean squared error: 0.35988082645291514
Relative absolute error: 47.489149823591646
Root relative squared error: 71.97616529058303
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6595130258367619
Weighted FMeasure: 0.8355592864513702
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8141788247693416
Mean absolute error: 0.23744574911795824
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.2384359290942764
Kappa statistic: 0.6410118406889127
Training time: 4.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.854834523036989
Weighted AreaUnderROC: 0.8048301217038538
Root mean squared error: 0.3708280925364706
Relative absolute error: 44.35872881681676
Root relative squared error: 74.16561850729411
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6712726759970572
Weighted FMeasure: 0.8416978629329401
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7923860995035102
Mean absolute error: 0.22179364408408378
Coverage of cases: 98.3695652173913
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.22916482935003085
Kappa statistic: 0.6545064377682404
Training time: 2.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 67.3913043478261
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.854834523036989
Weighted AreaUnderROC: 0.8048301217038538
Root mean squared error: 0.3729581140766311
Relative absolute error: 43.000159297713715
Root relative squared error: 74.59162281532622
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6712726759970572
Weighted FMeasure: 0.8416978629329401
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7923860995035102
Mean absolute error: 0.21500079648856857
Coverage of cases: 89.1304347826087
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.22916482935003085
Kappa statistic: 0.6545064377682404
Training time: 2.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 67.3913043478261
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.854834523036989
Weighted AreaUnderROC: 0.8052104462474645
Root mean squared error: 0.37429651212540793
Relative absolute error: 42.199664049269224
Root relative squared error: 74.85930242508158
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6712726759970572
Weighted FMeasure: 0.8416978629329401
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7927125288287288
Mean absolute error: 0.21099832024634613
Coverage of cases: 89.1304347826087
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.22916482935003085
Kappa statistic: 0.6545064377682404
Training time: 3.0
		
Time end:Sat Oct 07 11.32.49 EEST 2017