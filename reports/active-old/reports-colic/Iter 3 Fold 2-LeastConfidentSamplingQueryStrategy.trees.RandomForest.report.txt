Sat Oct 07 11.32.51 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.51 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.754514629477714
Weighted AreaUnderROC: 0.8005831643002028
Root mean squared error: 0.4291179518376665
Relative absolute error: 72.93751373686348
Root relative squared error: 85.8235903675333
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.3864606134965403
Weighted FMeasure: 0.680418207318628
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7999510494770908
Mean absolute error: 0.3646875686843174
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.4484853161654466
Kappa statistic: 0.3156359393232206
Training time: 3.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 99.18478260869566
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8076138540899042
Weighted AreaUnderROC: 0.8278397565922921
Root mean squared error: 0.3977034127331956
Relative absolute error: 68.63678557692515
Root relative squared error: 79.54068254663912
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5837013427582051
Weighted FMeasure: 0.8065943994056488
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8373309354943391
Mean absolute error: 0.34318392788462576
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.24538098597759944
Kappa statistic: 0.5802919708029198
Training time: 5.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 98.3695652173913
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8217441401724603
Weighted AreaUnderROC: 0.8525608519269777
Root mean squared error: 0.38604230410336
Relative absolute error: 67.65259763269998
Root relative squared error: 77.208460820672
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6069916460761884
Weighted FMeasure: 0.8149813573154535
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8582302534711365
Mean absolute error: 0.3382629881634999
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.25117955728018343
Kappa statistic: 0.5967056323060573
Training time: 7.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 96.73913043478261
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8309188754151726
Weighted AreaUnderROC: 0.8695486815415822
Root mean squared error: 0.3702191941464306
Relative absolute error: 59.7456770296798
Root relative squared error: 74.04383882928613
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6315063119508294
Weighted FMeasure: 0.8279219896224727
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8691330302233804
Mean absolute error: 0.298728385148399
Coverage of cases: 99.45652173913044
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.22655216509392365
Kappa statistic: 0.6259181532004198
Training time: 9.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8189211495946942
Weighted AreaUnderROC: 0.8734787018255578
Root mean squared error: 0.38277003212984206
Relative absolute error: 68.8226366249754
Root relative squared error: 76.55400642596841
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6078255671669001
Weighted FMeasure: 0.8176461480110404
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8820492248839663
Mean absolute error: 0.34411318312487704
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.23292397918687718
Kappa statistic: 0.6042752867570386
Training time: 14.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.6413043478261
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8251825551014939
Weighted AreaUnderROC: 0.9006085192697768
Root mean squared error: 0.3511062680568233
Relative absolute error: 58.605294158893415
Root relative squared error: 70.22125361136466
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.624661855288119
Weighted FMeasure: 0.8255323161228417
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9022112445224069
Mean absolute error: 0.2930264707944671
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20539730134932535
Kappa statistic: 0.6244897959183674
Training time: 16.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 94.29347826086956
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8356204710144928
Weighted AreaUnderROC: 0.8850152129817445
Root mean squared error: 0.3493162345451968
Relative absolute error: 53.31368641931192
Root relative squared error: 69.86324690903936
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6465119719580276
Weighted FMeasure: 0.8358678903999465
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8885547993224421
Mean absolute error: 0.2665684320965596
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1990254872563718
Kappa statistic: 0.6457905544147845
Training time: 14.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.28260869565217
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8688409629135723
Weighted AreaUnderROC: 0.8879310344827587
Root mean squared error: 0.3457707060217005
Relative absolute error: 55.77584551673582
Root relative squared error: 69.1541412043401
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7166991186382579
Weighted FMeasure: 0.8681993198617239
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8878072392871061
Mean absolute error: 0.2788792275836791
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.1677396595819737
Kappa statistic: 0.7148760330578512
Training time: 18.0
		
Time end:Sat Oct 07 11.32.51 EEST 2017