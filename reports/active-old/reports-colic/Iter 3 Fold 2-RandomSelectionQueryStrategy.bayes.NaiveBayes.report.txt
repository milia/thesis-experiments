Sat Oct 07 11.33.17 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.17 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 62.22826086956522
Incorrectly Classified Instances: 27.17391304347826
Correctly Classified Instances: 72.82608695652173
Weighted Precision: 0.7416178929765885
Weighted AreaUnderROC: 0.7658055163594674
Root mean squared error: 0.48249652854950614
Relative absolute error: 55.31702681802009
Root relative squared error: 96.49930570990122
Weighted TruePositiveRate: 0.7282608695652174
Weighted MatthewsCorrelation: 0.4414203959779533
Weighted FMeasure: 0.731866253605384
Iteration time: 17.0
Weighted AreaUnderPRC: 0.759108779303908
Mean absolute error: 0.27658513409010044
Coverage of cases: 83.15217391304348
Instances selection time: 17.0
Test time: 11.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7282608695652174
Weighted FalsePositiveRate: 0.2749140135814446
Kappa statistic: 0.4373776908023483
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 59.51086956521739
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7624387054593005
Weighted AreaUnderROC: 0.7727064776435313
Root mean squared error: 0.45402576775531034
Relative absolute error: 47.72711607066168
Root relative squared error: 90.80515355106206
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.49005109010965303
Weighted FMeasure: 0.7615626969124133
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7763895627059394
Mean absolute error: 0.2386355803533084
Coverage of cases: 85.32608695652173
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.26796895669812154
Kappa statistic: 0.4899193548387098
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 61.141304347826086
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7887208569628229
Weighted AreaUnderROC: 0.7918164079724844
Root mean squared error: 0.41872425119409457
Relative absolute error: 43.32613358576589
Root relative squared error: 83.74485023881891
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5465485780863846
Weighted FMeasure: 0.7883582618587378
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7895250286563558
Mean absolute error: 0.21663066792882943
Coverage of cases: 87.5
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.23986903607020021
Kappa statistic: 0.5465116279069766
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 61.141304347826086
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8043478260869565
Weighted AreaUnderROC: 0.8015243297468914
Root mean squared error: 0.4227099663108654
Relative absolute error: 43.10585796953001
Root relative squared error: 84.54199326217308
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5801217038539555
Weighted FMeasure: 0.8043478260869567
Iteration time: 1.0
Weighted AreaUnderPRC: 0.792915782402779
Mean absolute error: 0.21552928984765005
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.22422612223300115
Kappa statistic: 0.5801217038539555
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 63.58695652173913
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8180426155969635
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.4118491863393458
Relative absolute error: 41.873262451281846
Root relative squared error: 82.36983726786916
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6088635388262879
Weighted FMeasure: 0.8162362427808653
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8117723394123143
Mean absolute error: 0.20936631225640923
Coverage of cases: 89.1304347826087
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.1995987300467413
Kappa statistic: 0.6082164328657313
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8180426155969635
Weighted AreaUnderROC: 0.8216277890466531
Root mean squared error: 0.4027683075035246
Relative absolute error: 40.64510809429308
Root relative squared error: 80.55366150070492
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6088635388262879
Weighted FMeasure: 0.8162362427808653
Iteration time: 1.0
Weighted AreaUnderPRC: 0.812738053482985
Mean absolute error: 0.2032255404714654
Coverage of cases: 89.67391304347827
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.1995987300467413
Kappa statistic: 0.6082164328657313
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 65.21739130434783
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8155484642999601
Weighted AreaUnderROC: 0.831262677484787
Root mean squared error: 0.39932740961514923
Relative absolute error: 40.69259800932217
Root relative squared error: 79.86548192302985
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.6022252499594984
Weighted FMeasure: 0.8114793419141243
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8247118700380647
Mean absolute error: 0.20346299004661084
Coverage of cases: 90.21739130434783
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.19669944439544934
Kappa statistic: 0.6002979145978155
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8155484642999601
Weighted AreaUnderROC: 0.8322768762677485
Root mean squared error: 0.4161795722157073
Relative absolute error: 43.365597824278076
Root relative squared error: 83.23591444314145
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.6022252499594984
Weighted FMeasure: 0.8114793419141243
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8269264250090088
Mean absolute error: 0.21682798912139037
Coverage of cases: 88.58695652173913
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.19669944439544934
Kappa statistic: 0.6002979145978155
Training time: 2.0
		
Time end:Sat Oct 07 11.33.17 EEST 2017