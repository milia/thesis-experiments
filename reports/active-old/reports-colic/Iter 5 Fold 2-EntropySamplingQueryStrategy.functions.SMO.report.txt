Sat Oct 07 11.32.07 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.07 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7804381699836884
Weighted AreaUnderROC: 0.7690162271805275
Root mean squared error: 0.4834207816942845
Relative absolute error: 46.73913043478261
Root relative squared error: 96.6841563388569
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.5231555084305589
Weighted FMeasure: 0.7695629522739779
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7231474485815734
Mean absolute error: 0.23369565217391305
Coverage of cases: 76.6304347826087
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.22827189346503218
Kappa statistic: 0.517560975609756
Training time: 19.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.751626885477693
Weighted AreaUnderROC: 0.7269269776876268
Root mean squared error: 0.49453535504684026
Relative absolute error: 48.91304347826087
Root relative squared error: 98.90707100936805
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46535255469989784
Weighted FMeasure: 0.7523807912138121
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6902224144998584
Mean absolute error: 0.24456521739130435
Coverage of cases: 75.54347826086956
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.3015808272334421
Kappa statistic: 0.46373056994818657
Training time: 19.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7572210398297355
Weighted AreaUnderROC: 0.7190669371196755
Root mean squared error: 0.48900964692182575
Relative absolute error: 47.82608695652174
Root relative squared error: 97.80192938436515
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.46967954367088416
Weighted FMeasure: 0.7526414212248715
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6875024786177906
Mean absolute error: 0.2391304347826087
Coverage of cases: 76.08695652173913
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.3227356909780404
Kappa statistic: 0.4605543710021323
Training time: 27.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.75
Weighted AreaUnderROC: 0.6952332657200813
Root mean squared error: 0.5
Relative absolute error: 50.0
Root relative squared error: 100.0
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.44185208579351665
Weighted FMeasure: 0.7349330357142858
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6704867674858223
Mean absolute error: 0.25
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 111.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.35953346855983775
Kappa statistic: 0.4212253829321663
Training time: 23.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7721544779160527
Weighted AreaUnderROC: 0.6977687626774847
Root mean squared error: 0.48900964692182575
Relative absolute error: 47.82608695652174
Root relative squared error: 97.80192938436515
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.47163162290781474
Weighted FMeasure: 0.7407210264952502
Iteration time: 54.0
Weighted AreaUnderPRC: 0.6780062094278086
Mean absolute error: 0.2391304347826087
Coverage of cases: 76.08695652173913
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 165.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.3653320398624217
Kappa statistic: 0.4352678571428573
Training time: 53.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7721544779160527
Weighted AreaUnderROC: 0.6977687626774847
Root mean squared error: 0.48900964692182575
Relative absolute error: 47.82608695652174
Root relative squared error: 97.80192938436515
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.47163162290781474
Weighted FMeasure: 0.7407210264952502
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6780062094278086
Mean absolute error: 0.2391304347826087
Coverage of cases: 76.08695652173913
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.3653320398624217
Kappa statistic: 0.4352678571428573
Training time: 23.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8243280527901042
Weighted AreaUnderROC: 0.8042596348884381
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6214154207846165
Weighted FMeasure: 0.824265759815632
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7657729029858878
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 245.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.21756768674486288
Kappa statistic: 0.6198347107438017
Training time: 46.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8597254004576659
Weighted AreaUnderROC: 0.8514198782961462
Root mean squared error: 0.3759047057780561
Relative absolute error: 28.26086956521739
Root relative squared error: 75.18094115561122
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6987765544156165
Weighted FMeasure: 0.8591052299936988
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8115436274997512
Mean absolute error: 0.14130434782608695
Coverage of cases: 85.8695652173913
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 312.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.15585589558162097
Kappa statistic: 0.6985887096774195
Training time: 61.0
		
Time end:Sat Oct 07 11.32.08 EEST 2017