Sat Oct 07 11.33.39 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.39 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 79.91847826086956
Incorrectly Classified Instances: 21.793478260869566
Correctly Classified Instances: 78.20652173913042
Weighted Precision: 0.7376895747924176
Weighted AreaUnderROC: 0.742317444219067
Root mean squared error: 0.4158312039526245
Relative absolute error: 57.07010617910014
Root relative squared error: 83.16624079052488
Weighted TruePositiveRate: 0.7820652173913044
Weighted MatthewsCorrelation: 0.49363469488506934
Weighted FMeasure: 0.7533737802525181
Iteration time: 2.6
Weighted AreaUnderPRC: 0.72529845759859
Mean absolute error: 0.2853505308955006
Coverage of cases: 93.36956521739131
Instances selection time: 0.5
Test time: 1.4
Accumulative iteration time: 2.6
Weighted Recall: 0.7820652173913044
Weighted FalsePositiveRate: 0.29083803686392096
Kappa statistic: 0.491596168507267
Training time: 2.1
		
Time end:Sat Oct 07 11.33.40 EEST 2017