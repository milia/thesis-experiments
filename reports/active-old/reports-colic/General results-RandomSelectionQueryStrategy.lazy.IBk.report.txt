Sat Oct 07 11.33.25 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.25 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.467391304347824
Correctly Classified Instances: 73.53260869565216
Weighted Precision: 0.7341304176086227
Weighted AreaUnderROC: 0.7147122210953346
Root mean squared error: 0.4986543421116315
Relative absolute error: 55.4146824169708
Root relative squared error: 99.73086842232631
Weighted TruePositiveRate: 0.7353260869565219
Weighted MatthewsCorrelation: 0.4284143684197265
Weighted FMeasure: 0.7322711073809686
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6829518308338952
Mean absolute error: 0.27707341208485403
Coverage of cases: 73.53260869565216
Instances selection time: 0.9
Test time: 6.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7353260869565219
Weighted FalsePositiveRate: 0.30363237498897616
Kappa statistic: 0.4273962406520912
Training time: 0.1
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.26086956521739
Correctly Classified Instances: 76.73913043478261
Weighted Precision: 0.7668849415072959
Weighted AreaUnderROC: 0.7428879310344827
Root mean squared error: 0.472974759989348
Relative absolute error: 48.357531760435535
Root relative squared error: 94.59495199786959
Weighted TruePositiveRate: 0.767391304347826
Weighted MatthewsCorrelation: 0.4969298479356953
Weighted FMeasure: 0.7652696321934237
Iteration time: 1.0
Weighted AreaUnderPRC: 0.706812323202116
Mean absolute error: 0.24178765880217767
Coverage of cases: 76.73913043478261
Instances selection time: 0.6
Test time: 9.2
Accumulative iteration time: 2.0
Weighted Recall: 0.767391304347826
Weighted FalsePositiveRate: 0.27996736925654825
Kappa statistic: 0.4941700686737384
Training time: 0.4
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.880434782608695
Correctly Classified Instances: 77.1195652173913
Weighted Precision: 0.7718765566437015
Weighted AreaUnderROC: 0.7488526876267747
Root mean squared error: 0.47196131467526053
Relative absolute error: 47.14267616441531
Root relative squared error: 94.39226293505212
Weighted TruePositiveRate: 0.771195652173913
Weighted MatthewsCorrelation: 0.5080003819619962
Weighted FMeasure: 0.7699052318062576
Iteration time: 0.5
Weighted AreaUnderPRC: 0.711259257261276
Mean absolute error: 0.23571338082207655
Coverage of cases: 77.1195652173913
Instances selection time: 0.5
Test time: 11.9
Accumulative iteration time: 2.5
Weighted Recall: 0.771195652173913
Weighted FalsePositiveRate: 0.2692179645471382
Kappa statistic: 0.5055897554800233
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.17391304347826
Correctly Classified Instances: 77.82608695652172
Weighted Precision: 0.7785636638457447
Weighted AreaUnderROC: 0.7598060344827586
Root mean squared error: 0.4654032182277346
Relative absolute error: 45.47590080406882
Root relative squared error: 93.08064364554693
Weighted TruePositiveRate: 0.7782608695652173
Weighted MatthewsCorrelation: 0.5238878514773498
Weighted FMeasure: 0.7778284672910392
Iteration time: 1.3
Weighted AreaUnderPRC: 0.7204454409483427
Mean absolute error: 0.22737950402034407
Coverage of cases: 77.82608695652172
Instances selection time: 1.1
Test time: 14.3
Accumulative iteration time: 3.8
Weighted Recall: 0.7782608695652173
Weighted FalsePositiveRate: 0.257165534879619
Kappa statistic: 0.5229914947079725
Training time: 0.2
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.391304347826086
Correctly Classified Instances: 77.60869565217392
Weighted Precision: 0.7760645065809393
Weighted AreaUnderROC: 0.7563070486815416
Root mean squared error: 0.46880047315601353
Relative absolute error: 45.71301702473402
Root relative squared error: 93.7600946312027
Weighted TruePositiveRate: 0.7760869565217392
Weighted MatthewsCorrelation: 0.518638757903533
Weighted FMeasure: 0.7755639009515389
Iteration time: 0.7
Weighted AreaUnderPRC: 0.7174617544673623
Mean absolute error: 0.22856508512367008
Coverage of cases: 77.60869565217392
Instances selection time: 0.6
Test time: 14.3
Accumulative iteration time: 4.5
Weighted Recall: 0.7760869565217392
Weighted FalsePositiveRate: 0.2602654555075404
Kappa statistic: 0.5178556131065117
Training time: 0.1
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.847826086956523
Correctly Classified Instances: 78.15217391304348
Weighted Precision: 0.7802403845570927
Weighted AreaUnderROC: 0.7594066937119675
Root mean squared error: 0.4634760912357069
Relative absolute error: 44.50618391216955
Root relative squared error: 92.69521824714141
Weighted TruePositiveRate: 0.7815217391304348
Weighted MatthewsCorrelation: 0.5271978628832497
Weighted FMeasure: 0.7801169706191584
Iteration time: 0.5
Weighted AreaUnderPRC: 0.721044119137493
Mean absolute error: 0.22253091956084775
Coverage of cases: 78.15217391304348
Instances selection time: 0.5
Test time: 16.5
Accumulative iteration time: 5.0
Weighted Recall: 0.7815217391304348
Weighted FalsePositiveRate: 0.2613391833495017
Kappa statistic: 0.5260155384155601
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.17391304347826
Correctly Classified Instances: 77.82608695652173
Weighted Precision: 0.7766410041164418
Weighted AreaUnderROC: 0.7569345841784989
Root mean squared error: 0.46749866098518805
Relative absolute error: 45.04544833122419
Root relative squared error: 93.49973219703762
Weighted TruePositiveRate: 0.7782608695652173
Weighted MatthewsCorrelation: 0.5195603553874184
Weighted FMeasure: 0.7767184220584518
Iteration time: 0.5
Weighted AreaUnderPRC: 0.7178977579808956
Mean absolute error: 0.225227241656121
Coverage of cases: 77.82608695652173
Instances selection time: 0.5
Test time: 18.4
Accumulative iteration time: 5.5
Weighted Recall: 0.7782608695652173
Weighted FalsePositiveRate: 0.2656848046564953
Kappa statistic: 0.5184658587408429
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.880434782608695
Correctly Classified Instances: 77.11956521739131
Weighted Precision: 0.7707439678022223
Weighted AreaUnderROC: 0.7518128803245436
Root mean squared error: 0.47528731015130943
Relative absolute error: 46.364225999740526
Root relative squared error: 95.05746203026187
Weighted TruePositiveRate: 0.7711956521739131
Weighted MatthewsCorrelation: 0.5070641041113728
Weighted FMeasure: 0.7703379974644828
Iteration time: 0.8
Weighted AreaUnderPRC: 0.7126339248682061
Mean absolute error: 0.23182112999870266
Coverage of cases: 77.11956521739131
Instances selection time: 0.8
Test time: 20.0
Accumulative iteration time: 6.3
Weighted Recall: 0.7711956521739131
Weighted FalsePositiveRate: 0.2680009260075844
Kappa statistic: 0.5061267607762943
Training time: 0.0
		
Time end:Sat Oct 07 11.33.29 EEST 2017