Sat Oct 07 11.33.11 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.33.11 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 36.41304347826087
Correctly Classified Instances: 63.58695652173913
Weighted Precision: 0.6033354136328964
Weighted AreaUnderROC: 0.6476293103448276
Root mean squared error: 0.4809514949039383
Relative absolute error: 81.19685171200072
Root relative squared error: 96.19029898078766
Weighted TruePositiveRate: 0.6358695652173914
Weighted MatthewsCorrelation: 0.11019917290103635
Weighted FMeasure: 0.5650751136887071
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6610354348726415
Mean absolute error: 0.4059842585600036
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6358695652173914
Weighted FalsePositiveRate: 0.5663969485845313
Kappa statistic: 0.08164481525625747
Training time: 3.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 99.45652173913044
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.7272624953103507
Weighted AreaUnderROC: 0.7677484787018255
Root mean squared error: 0.4303099885365378
Relative absolute error: 77.41643096924548
Root relative squared error: 86.06199770730755
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.3748209490623196
Weighted FMeasure: 0.6956824390515861
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7798633784435672
Mean absolute error: 0.3870821548462274
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.41805935267660294
Kappa statistic: 0.3387824126268319
Training time: 11.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 99.18478260869566
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7634989077676824
Weighted AreaUnderROC: 0.7918356997971603
Root mean squared error: 0.41994252760433304
Relative absolute error: 75.2931520768303
Root relative squared error: 83.98850552086661
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.49159389531418957
Weighted FMeasure: 0.7643096511460186
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7947086987848117
Mean absolute error: 0.37646576038415147
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.28303862774495103
Kappa statistic: 0.49073120494335726
Training time: 10.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 99.45652173913044
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7971903054815087
Weighted AreaUnderROC: 0.8261916835699799
Root mean squared error: 0.40337324623334
Relative absolute error: 71.02205883687915
Root relative squared error: 80.674649246668
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5576794596007292
Weighted FMeasure: 0.7936214570663896
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8402673073789005
Mean absolute error: 0.35511029418439577
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.27000837816385925
Kappa statistic: 0.5506863780359028
Training time: 10.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 98.3695652173913
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7634989077676824
Weighted AreaUnderROC: 0.8534482758620691
Root mean squared error: 0.3895198378732472
Relative absolute error: 66.51712087887748
Root relative squared error: 77.90396757464944
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.49159389531418957
Weighted FMeasure: 0.7643096511460186
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8544447351894141
Mean absolute error: 0.3325856043943874
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.28303862774495103
Kappa statistic: 0.49073120494335726
Training time: 12.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 13.58695652173913
Correctly Classified Instances: 86.41304347826087
Weighted Precision: 0.8632893960259689
Weighted AreaUnderROC: 0.9203853955375254
Root mean squared error: 0.35117832328606713
Relative absolute error: 61.2044363246494
Root relative squared error: 70.23566465721342
Weighted TruePositiveRate: 0.8641304347826086
Weighted MatthewsCorrelation: 0.706172250244066
Weighted FMeasure: 0.8634653238090602
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9146800839761041
Mean absolute error: 0.306022181623247
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8641304347826086
Weighted FalsePositiveRate: 0.1648403739306817
Kappa statistic: 0.7057318321392015
Training time: 16.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 99.45652173913044
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.90276369168357
Root mean squared error: 0.36231426224011404
Relative absolute error: 64.31162463685229
Root relative squared error: 72.4628524480228
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9036030354543543
Mean absolute error: 0.32155812318426147
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 22.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8468910705937354
Weighted AreaUnderROC: 0.9136663286004056
Root mean squared error: 0.3506929107726688
Relative absolute error: 59.78885208817375
Root relative squared error: 70.13858215453375
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.668343982566873
Weighted FMeasure: 0.845606884057971
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9161308730752193
Mean absolute error: 0.29894426044086875
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 125.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.19873886586118705
Kappa statistic: 0.6652806652806653
Training time: 23.0
		
Time end:Sat Oct 07 11.33.11 EEST 2017