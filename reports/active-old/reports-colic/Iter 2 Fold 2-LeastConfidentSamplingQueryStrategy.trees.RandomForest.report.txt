Sat Oct 07 11.32.50 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.50 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 96.46739130434783
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8251825551014939
Weighted AreaUnderROC: 0.8286637931034483
Root mean squared error: 0.39731898016538025
Relative absolute error: 67.85917963198337
Root relative squared error: 79.46379603307605
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.624661855288119
Weighted FMeasure: 0.8255323161228417
Iteration time: 4.0
Weighted AreaUnderPRC: 0.822987809157835
Mean absolute error: 0.33929589815991684
Coverage of cases: 98.3695652173913
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20539730134932535
Kappa statistic: 0.6244897959183674
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 99.45652173913044
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.807804185274541
Weighted AreaUnderROC: 0.8247971602434078
Root mean squared error: 0.3895223474538969
Relative absolute error: 66.22661396196165
Root relative squared error: 77.90446949077938
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5865081416287865
Weighted FMeasure: 0.8081590183746662
Iteration time: 5.0
Weighted AreaUnderPRC: 0.827700467614427
Mean absolute error: 0.3311330698098082
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.23321060058206192
Kappa statistic: 0.5854788877445933
Training time: 4.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7887208569628229
Weighted AreaUnderROC: 0.7870182555780935
Root mean squared error: 0.42843378446481584
Relative absolute error: 77.48501400663753
Root relative squared error: 85.68675689296316
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5465485780863846
Weighted FMeasure: 0.7883582618587378
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7756102030383814
Mean absolute error: 0.38742507003318766
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.23986903607020021
Kappa statistic: 0.5465116279069766
Training time: 6.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 98.6413043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8020715977283702
Weighted AreaUnderROC: 0.8147819472616632
Root mean squared error: 0.4109608074538385
Relative absolute error: 72.3758680273834
Root relative squared error: 82.1921614907677
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.573773571857796
Weighted FMeasure: 0.802298979792586
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8086787361140589
Mean absolute error: 0.361879340136917
Coverage of cases: 99.45652173913044
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24248170032630748
Kappa statistic: 0.5723140495867769
Training time: 11.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 97.01086956521739
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.797389055956831
Weighted AreaUnderROC: 0.8142748478701826
Root mean squared error: 0.400673584518443
Relative absolute error: 67.58195879703868
Root relative squared error: 80.1347169036886
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5648353821749129
Weighted FMeasure: 0.797928679237409
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8153140684966563
Mean absolute error: 0.3379097939851934
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.23958241467501543
Kappa statistic: 0.5644831115660184
Training time: 10.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.6413043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8032982738214867
Weighted AreaUnderROC: 0.8246703853955374
Root mean squared error: 0.39255480072147614
Relative absolute error: 65.85561565513774
Root relative squared error: 78.51096014429523
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5777101472109074
Weighted FMeasure: 0.8037238556381969
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8147327178117153
Mean absolute error: 0.3292780782756887
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.23031131493076992
Kappa statistic: 0.5775510204081633
Training time: 14.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 93.20652173913044
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8048371227230422
Weighted AreaUnderROC: 0.8441937119675458
Root mean squared error: 0.3841723795339655
Relative absolute error: 56.45186368097082
Root relative squared error: 76.8344759067931
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5793117995888312
Weighted FMeasure: 0.8007067328806459
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8375880314802778
Mean absolute error: 0.2822593184048541
Coverage of cases: 98.3695652173913
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.2091564511861716
Kappa statistic: 0.5774577954319762
Training time: 14.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 96.19565217391305
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8142404144614903
Weighted AreaUnderROC: 0.870055780933063
Root mean squared error: 0.366351142140077
Relative absolute error: 58.105668849190906
Root relative squared error: 73.2702284280154
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6011860012495134
Weighted FMeasure: 0.8146280858805194
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8785711437074586
Mean absolute error: 0.29052834424595453
Coverage of cases: 98.91304347826087
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 87.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.2178543081400476
Kappa statistic: 0.6010204081632652
Training time: 18.0
		
Time end:Sat Oct 07 11.32.51 EEST 2017