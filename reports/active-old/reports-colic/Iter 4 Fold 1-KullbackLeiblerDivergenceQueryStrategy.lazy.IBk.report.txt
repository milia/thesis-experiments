Sat Oct 07 11.32.29 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.29 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7191463901076985
Weighted AreaUnderROC: 0.7015720081135902
Root mean squared error: 0.5230447552266734
Relative absolute error: 59.8398169336385
Root relative squared error: 104.60895104533468
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.3960041966234916
Weighted FMeasure: 0.7145258606128173
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6644905135186695
Mean absolute error: 0.2991990846681925
Coverage of cases: 71.19565217391305
Instances selection time: 15.0
Test time: 12.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.30881250551194994
Kappa statistic: 0.39473684210526333
Training time: 9.0
		
Time end:Sat Oct 07 11.32.29 EEST 2017