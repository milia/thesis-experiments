Sat Oct 07 11.32.29 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.29 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.989130434782613
Correctly Classified Instances: 72.01086956521739
Weighted Precision: 0.7204460610338199
Weighted AreaUnderROC: 0.6911574543610548
Root mean squared error: 0.5142424121128422
Relative absolute error: 58.29241140453961
Root relative squared error: 102.84848242256842
Weighted TruePositiveRate: 0.7201086956521741
Weighted MatthewsCorrelation: 0.39321259231226763
Weighted FMeasure: 0.7149220443612683
Iteration time: 19.2
Weighted AreaUnderPRC: 0.6613868421329903
Mean absolute error: 0.2914620570226981
Coverage of cases: 72.01086956521739
Instances selection time: 14.8
Test time: 9.0
Accumulative iteration time: 19.2
Weighted Recall: 0.7201086956521741
Weighted FalsePositiveRate: 0.33871924331951675
Kappa statistic: 0.3866178307635845
Training time: 4.4
		
Time end:Sat Oct 07 11.32.29 EEST 2017