Sat Oct 07 11.32.16 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.16 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 100.0
Incorrectly Classified Instances: 36.95652173913044
Correctly Classified Instances: 63.04347826086956
Weighted Precision: 0.39744801512287337
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.49133664788679193
Relative absolute error: 88.40579710144947
Root relative squared error: 98.26732957735838
Weighted TruePositiveRate: 0.6304347826086957
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.487536231884058
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5340264650283555
Mean absolute error: 0.44202898550724734
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6304347826086957
Weighted FalsePositiveRate: 0.6304347826086957
Kappa statistic: 0.0
Training time: 5.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 100.0
Incorrectly Classified Instances: 36.95652173913044
Correctly Classified Instances: 63.04347826086956
Weighted Precision: 0.39744801512287337
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5191005633339207
Relative absolute error: 83.22981366459616
Root relative squared error: 103.82011266678415
Weighted TruePositiveRate: 0.6304347826086957
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.487536231884058
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5340264650283555
Mean absolute error: 0.4161490683229808
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6304347826086957
Weighted FalsePositiveRate: 0.6304347826086957
Kappa statistic: 0.0
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 100.0
Incorrectly Classified Instances: 36.95652173913044
Correctly Classified Instances: 63.04347826086956
Weighted Precision: 0.39744801512287337
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.538167475982363
Relative absolute error: 80.77803203661338
Root relative squared error: 107.63349519647261
Weighted TruePositiveRate: 0.6304347826086957
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.487536231884058
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5340264650283555
Mean absolute error: 0.4038901601830669
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6304347826086957
Weighted FalsePositiveRate: 0.6304347826086957
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 100.0
Incorrectly Classified Instances: 36.95652173913044
Correctly Classified Instances: 63.04347826086956
Weighted Precision: 0.39744801512287337
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.550838595427518
Relative absolute error: 79.34782608695652
Root relative squared error: 110.1677190855036
Weighted TruePositiveRate: 0.6304347826086957
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.487536231884058
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5340264650283555
Mean absolute error: 0.3967391304347826
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6304347826086957
Weighted FalsePositiveRate: 0.6304347826086957
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 100.0
Incorrectly Classified Instances: 36.95652173913044
Correctly Classified Instances: 63.04347826086956
Weighted Precision: 0.39744801512287337
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.5597130563733965
Relative absolute error: 78.4107946026987
Root relative squared error: 111.94261127467931
Weighted TruePositiveRate: 0.6304347826086957
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.487536231884058
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5340264650283555
Mean absolute error: 0.3920539730134935
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6304347826086957
Weighted FalsePositiveRate: 0.6304347826086957
Kappa statistic: 0.0
Training time: 4.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8784355179704015
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.3739735316580398
Relative absolute error: 49.28431890027017
Root relative squared error: 74.79470633160797
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7298489571955852
Weighted FMeasure: 0.8717106354737016
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8136628069210275
Mean absolute error: 0.24642159450135087
Coverage of cases: 97.82608695652173
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.18280933062880325
Kappa statistic: 0.7206969376979937
Training time: 3.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8784355179704015
Weighted AreaUnderROC: 0.8220081135902637
Root mean squared error: 0.34594361614686675
Relative absolute error: 40.87598759148854
Root relative squared error: 69.18872322937335
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7298489571955852
Weighted FMeasure: 0.8717106354737016
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8136628069210275
Mean absolute error: 0.2043799379574427
Coverage of cases: 97.82608695652173
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 54.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.18280933062880325
Kappa statistic: 0.7206969376979937
Training time: 6.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.55434782608695
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.7835319472616632
Root mean squared error: 0.370446776161995
Relative absolute error: 43.637309624063015
Root relative squared error: 74.089355232399
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7896068618513611
Mean absolute error: 0.21818654812031507
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 7.0
		
Time end:Sat Oct 07 11.32.16 EEST 2017