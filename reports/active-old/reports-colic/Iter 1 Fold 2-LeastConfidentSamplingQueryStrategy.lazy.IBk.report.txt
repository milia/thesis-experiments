Sat Oct 07 11.32.41 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.41 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.697585983127839
Weighted AreaUnderROC: 0.657707910750507
Root mean squared error: 0.5279437066476833
Relative absolute error: 60.869565217391354
Root relative squared error: 105.58874132953666
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.34223902005751605
Weighted FMeasure: 0.6947030213706706
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6348033462178709
Mean absolute error: 0.30434782608695676
Coverage of cases: 70.65217391304348
Instances selection time: 16.0
Test time: 11.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.3911059176294206
Kappa statistic: 0.33369098712446355
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.7094179061784898
Weighted AreaUnderROC: 0.6303245436105477
Root mean squared error: 0.5325932650720471
Relative absolute error: 60.119940029985074
Root relative squared error: 106.51865301440941
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.3319272258337758
Weighted FMeasure: 0.6734068786502271
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6204237296040196
Mean absolute error: 0.3005997001499254
Coverage of cases: 70.65217391304348
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.4458726519093395
Kappa statistic: 0.29271070615034167
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.6760361857287915
Weighted AreaUnderROC: 0.6030679513184585
Root mean squared error: 0.5590998984607582
Relative absolute error: 65.0295356817096
Root relative squared error: 111.81997969215165
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.2552532016923575
Weighted FMeasure: 0.6342327390382311
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5996599147931431
Mean absolute error: 0.325147678408548
Coverage of cases: 67.93478260869566
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.49222815063056713
Kappa statistic: 0.21378910776361526
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.6839049419389683
Weighted AreaUnderROC: 0.5940669371196755
Root mean squared error: 0.5605457450657138
Relative absolute error: 64.8514896770004
Root relative squared error: 112.10914901314275
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.2553399561180733
Weighted FMeasure: 0.6261114493731038
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5946361058601134
Mean absolute error: 0.324257448385002
Coverage of cases: 67.93478260869566
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.5043985360261045
Kappa statistic: 0.20270270270270277
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.6968477040940809
Weighted AreaUnderROC: 0.5950811359026369
Root mean squared error: 0.5567281661951647
Relative absolute error: 63.665293602655524
Root relative squared error: 111.34563323903295
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.273096581085656
Weighted FMeasure: 0.630306189830744
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5964876216446351
Mean absolute error: 0.31832646801327763
Coverage of cases: 68.47826086956522
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 43.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.5012126289796278
Kappa statistic: 0.2134433962264153
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.808610400682012
Weighted AreaUnderROC: 0.7614097363083164
Root mean squared error: 0.43916845531945903
Relative absolute error: 40.00869289252545
Root relative squared error: 87.8336910638918
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5708032328123508
Weighted FMeasure: 0.7952452345255943
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7323465837728069
Mean absolute error: 0.20004346446262725
Coverage of cases: 80.43478260869566
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.2850780492106888
Kappa statistic: 0.5529157667386609
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.811837163806231
Weighted AreaUnderROC: 0.8014705882352942
Root mean squared error: 0.43341604876209355
Relative absolute error: 38.8243202030364
Root relative squared error: 86.6832097524187
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.595835664090806
Weighted FMeasure: 0.810589348911985
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7576913192885503
Mean absolute error: 0.194121601015182
Coverage of cases: 80.97826086956522
Instances selection time: 4.0
Test time: 18.0
Accumulative iteration time: 52.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.20886982979098684
Kappa statistic: 0.5954773869346734
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7974967061923585
Weighted AreaUnderROC: 0.7875253549695739
Root mean squared error: 0.46941863679176743
Relative absolute error: 45.18504411858055
Root relative squared error: 93.88372735835348
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5552800687065372
Weighted FMeasure: 0.7807437366588023
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7402798929995175
Mean absolute error: 0.22592522059290274
Coverage of cases: 77.71739130434783
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.20364450127877237
Kappa statistic: 0.5453230472516876
Training time: 0.0
		
Time end:Sat Oct 07 11.32.42 EEST 2017