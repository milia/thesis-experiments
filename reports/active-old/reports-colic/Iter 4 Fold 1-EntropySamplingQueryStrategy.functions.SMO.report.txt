Sat Oct 07 11.32.05 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.05 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7270218720197614
Weighted AreaUnderROC: 0.7106997971602435
Root mean squared error: 0.5366968215490656
Relative absolute error: 57.608695652173914
Root relative squared error: 107.33936430981312
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.4097476225313802
Weighted FMeasure: 0.7159729411749032
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6713238682825464
Mean absolute error: 0.28804347826086957
Coverage of cases: 71.19565217391305
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.29055692741864364
Kappa statistic: 0.4053658536585367
Training time: 20.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.891304347826086
Correctly Classified Instances: 70.1086956521739
Weighted Precision: 0.6976822710408936
Weighted AreaUnderROC: 0.6290567951318459
Root mean squared error: 0.5467294060851866
Relative absolute error: 59.78260869565217
Root relative squared error: 109.34588121703732
Weighted TruePositiveRate: 0.7010869565217391
Weighted MatthewsCorrelation: 0.3174440317349761
Weighted FMeasure: 0.6718143950556321
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6176417588133691
Mean absolute error: 0.29891304347826086
Coverage of cases: 70.1086956521739
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7010869565217391
Weighted FalsePositiveRate: 0.44297336625804745
Kappa statistic: 0.28692220969560306
Training time: 18.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.78260869565217
Correctly Classified Instances: 65.21739130434783
Weighted Precision: 0.6347777289806276
Weighted AreaUnderROC: 0.5567951318458418
Root mean squared error: 0.5897678246195885
Relative absolute error: 69.56521739130434
Root relative squared error: 117.95356492391771
Weighted TruePositiveRate: 0.6521739130434783
Weighted MatthewsCorrelation: 0.16898793967731204
Weighted FMeasure: 0.5920620025718556
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5671448787390816
Mean absolute error: 0.34782608695652173
Coverage of cases: 65.21739130434783
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 76.0
Weighted Recall: 0.6521739130434783
Weighted FalsePositiveRate: 0.5385836493517947
Kappa statistic: 0.13207547169811334
Training time: 24.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.23913043478261
Correctly Classified Instances: 65.76086956521739
Weighted Precision: 0.6439373480961383
Weighted AreaUnderROC: 0.5641480730223123
Root mean squared error: 0.5851421232041205
Relative absolute error: 68.47826086956522
Root relative squared error: 117.0284246408241
Weighted TruePositiveRate: 0.657608695652174
Weighted MatthewsCorrelation: 0.18724930115325372
Weighted FMeasure: 0.6007630730594158
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5720868214960843
Mean absolute error: 0.3423913043478261
Coverage of cases: 65.76086956521739
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 112.0
Weighted Recall: 0.657608695652174
Weighted FalsePositiveRate: 0.5293125496075491
Kappa statistic: 0.14864864864864874
Training time: 29.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.778015010351967
Weighted AreaUnderROC: 0.6059837728194725
Root mean squared error: 0.5417363388859614
Relative absolute error: 58.69565217391305
Root relative squared error: 108.34726777719229
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.3631099836602524
Weighted FMeasure: 0.6425509579214417
Iteration time: 32.0
Weighted AreaUnderPRC: 0.614131138041228
Mean absolute error: 0.29347826086956524
Coverage of cases: 70.65217391304348
Instances selection time: 9.0
Test time: 5.0
Accumulative iteration time: 144.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.49455419349148955
Kappa statistic: 0.25180722891566265
Training time: 23.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.6894219410890523
Weighted AreaUnderROC: 0.5844320486815415
Root mean squared error: 0.566261577288309
Relative absolute error: 64.13043478260869
Root relative squared error: 113.2523154576618
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.2563405573797564
Weighted FMeasure: 0.6216895780694889
Iteration time: 52.0
Weighted AreaUnderPRC: 0.5886792378588742
Mean absolute error: 0.32065217391304346
Coverage of cases: 67.93478260869566
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 196.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.5104837287238734
Kappa statistic: 0.19704142011834322
Training time: 47.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8501811594202898
Weighted AreaUnderROC: 0.77079107505071
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6337771122341721
Weighted FMeasure: 0.8129072216028738
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7537925330812855
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 244.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.2845048064203192
Kappa statistic: 0.5920177383592018
Training time: 41.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8335295480590397
Weighted AreaUnderROC: 0.7963995943204868
Root mean squared error: 0.4104610345325914
Relative absolute error: 33.69565217391305
Root relative squared error: 82.09220690651829
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6318550616768159
Weighted FMeasure: 0.8261946083872442
Iteration time: 60.0
Weighted AreaUnderPRC: 0.7651586596174574
Mean absolute error: 0.16847826086956522
Coverage of cases: 83.15217391304348
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 304.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.23872255048946114
Kappa statistic: 0.6211477151965994
Training time: 59.0
		
Time end:Sat Oct 07 11.32.06 EEST 2017