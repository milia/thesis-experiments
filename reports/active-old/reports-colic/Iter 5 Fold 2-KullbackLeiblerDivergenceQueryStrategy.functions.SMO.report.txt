Sat Oct 07 11.32.28 EEST 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.32.28 EEST 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7804381699836884
Weighted AreaUnderROC: 0.7690162271805275
Root mean squared error: 0.4834207816942845
Relative absolute error: 46.73913043478261
Root relative squared error: 96.6841563388569
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.5231555084305589
Weighted FMeasure: 0.7695629522739779
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7231474485815734
Mean absolute error: 0.23369565217391305
Coverage of cases: 76.6304347826087
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.22827189346503218
Kappa statistic: 0.517560975609756
Training time: 21.0
		
Time end:Sat Oct 07 11.32.28 EEST 2017