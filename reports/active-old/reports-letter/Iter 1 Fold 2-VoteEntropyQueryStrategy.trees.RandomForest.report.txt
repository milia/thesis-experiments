Sun Oct 08 02.43.08 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 02.43.08 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 13.705769230769121
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8038569184905324
Weighted AreaUnderROC: 0.9633542013022008
Root mean squared error: 0.11020400662826912
Relative absolute error: 38.40824000000887
Root relative squared error: 57.306083446703944
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7908275249130667
Weighted FMeasure: 0.7978240441522214
Iteration time: 345.0
Weighted AreaUnderPRC: 0.8393964258319009
Mean absolute error: 0.028408461538464134
Coverage of cases: 94.57
Instances selection time: 71.0
Test time: 195.0
Accumulative iteration time: 345.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.0081346505798458
Kappa statistic: 0.7878368708484408
Training time: 274.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 13.491538461538404
Incorrectly Classified Instances: 19.75
Correctly Classified Instances: 80.25
Weighted Precision: 0.8212061089993391
Weighted AreaUnderROC: 0.9668044347566873
Root mean squared error: 0.10976793703081236
Relative absolute error: 37.9038400000085
Root relative squared error: 57.07932725602642
Weighted TruePositiveRate: 0.8025
Weighted MatthewsCorrelation: 0.8017388945528312
Weighted FMeasure: 0.8068923893453508
Iteration time: 379.0
Weighted AreaUnderPRC: 0.8496384593286838
Mean absolute error: 0.028035384615386988
Coverage of cases: 95.03
Instances selection time: 66.0
Test time: 191.0
Accumulative iteration time: 724.0
Weighted Recall: 0.8025
Weighted FalsePositiveRate: 0.00785121848905665
Kappa statistic: 0.794601599642742
Training time: 313.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 13.093846153846174
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8317252269958001
Weighted AreaUnderROC: 0.9665144559352968
Root mean squared error: 0.10868054811447175
Relative absolute error: 37.04272000000794
Root relative squared error: 56.513885019529255
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.8033526507553901
Weighted FMeasure: 0.8065639516662342
Iteration time: 419.0
Weighted AreaUnderPRC: 0.8561204866639489
Mean absolute error: 0.02739846153846359
Coverage of cases: 94.82
Instances selection time: 56.0
Test time: 200.0
Accumulative iteration time: 1143.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.007934845863185875
Kappa statistic: 0.7907702975559381
Training time: 363.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 12.833076923076964
Incorrectly Classified Instances: 20.61
Correctly Classified Instances: 79.39
Weighted Precision: 0.830876831909274
Weighted AreaUnderROC: 0.9649302260817398
Root mean squared error: 0.10900599842351825
Relative absolute error: 36.7920800000079
Root relative squared error: 56.68311918023345
Weighted TruePositiveRate: 0.7939
Weighted MatthewsCorrelation: 0.7992550707502155
Weighted FMeasure: 0.8014397103763952
Iteration time: 432.0
Weighted AreaUnderPRC: 0.8536794454761486
Mean absolute error: 0.027213076923078967
Coverage of cases: 94.49
Instances selection time: 53.0
Test time: 196.0
Accumulative iteration time: 1575.0
Weighted Recall: 0.7939
Weighted FalsePositiveRate: 0.00812395357398347
Kappa statistic: 0.7856768582710701
Training time: 379.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 12.713076923077146
Incorrectly Classified Instances: 19.27
Correctly Classified Instances: 80.73
Weighted Precision: 0.8411512749790507
Weighted AreaUnderROC: 0.9676723109664613
Root mean squared error: 0.10785175010170389
Relative absolute error: 36.112960000007604
Root relative squared error: 56.08291005288994
Weighted TruePositiveRate: 0.8073
Weighted MatthewsCorrelation: 0.8116454659832963
Weighted FMeasure: 0.813372399025256
Iteration time: 472.0
Weighted AreaUnderPRC: 0.8620355653070837
Mean absolute error: 0.026710769230771127
Coverage of cases: 94.89
Instances selection time: 51.0
Test time: 197.0
Accumulative iteration time: 2047.0
Weighted Recall: 0.8073
Weighted FalsePositiveRate: 0.007604540536659879
Kappa statistic: 0.7996098767227415
Training time: 421.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 12.345769230769447
Incorrectly Classified Instances: 19.54
Correctly Classified Instances: 80.46
Weighted Precision: 0.8398413300963055
Weighted AreaUnderROC: 0.9661708840899883
Root mean squared error: 0.10735025348399192
Relative absolute error: 35.46296000000724
Root relative squared error: 55.8221318116797
Weighted TruePositiveRate: 0.8046
Weighted MatthewsCorrelation: 0.809050075298132
Weighted FMeasure: 0.8103251849693469
Iteration time: 505.0
Weighted AreaUnderPRC: 0.8589605721430739
Mean absolute error: 0.026230000000001696
Coverage of cases: 94.56
Instances selection time: 47.0
Test time: 199.0
Accumulative iteration time: 2552.0
Weighted Recall: 0.8046
Weighted FalsePositiveRate: 0.0077199910694929355
Kappa statistic: 0.7968018736323123
Training time: 458.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 12.110769230769435
Incorrectly Classified Instances: 19.03
Correctly Classified Instances: 80.97
Weighted Precision: 0.8428247570016422
Weighted AreaUnderROC: 0.9688636581371176
Root mean squared error: 0.10632244136804742
Relative absolute error: 34.67152000000683
Root relative squared error: 55.287669511388515
Weighted TruePositiveRate: 0.8097
Weighted MatthewsCorrelation: 0.8131223980704007
Weighted FMeasure: 0.8140169125829102
Iteration time: 546.0
Weighted AreaUnderPRC: 0.8683090641632003
Mean absolute error: 0.025644615384616855
Coverage of cases: 94.95
Instances selection time: 46.0
Test time: 200.0
Accumulative iteration time: 3098.0
Weighted Recall: 0.8097
Weighted FalsePositiveRate: 0.007512423456461146
Kappa statistic: 0.8021075679203119
Training time: 500.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 12.065769230769591
Incorrectly Classified Instances: 17.9
Correctly Classified Instances: 82.1
Weighted Precision: 0.8504416374312561
Weighted AreaUnderROC: 0.9705951267493942
Root mean squared error: 0.10351551351590671
Relative absolute error: 33.670000000006844
Root relative squared error: 53.82806702827525
Weighted TruePositiveRate: 0.821
Weighted MatthewsCorrelation: 0.8235060841492705
Weighted FMeasure: 0.8245667101763445
Iteration time: 580.0
Weighted AreaUnderPRC: 0.8788967349562061
Mean absolute error: 0.02490384615384774
Coverage of cases: 95.19
Instances selection time: 44.0
Test time: 205.0
Accumulative iteration time: 3678.0
Weighted Recall: 0.821
Weighted FalsePositiveRate: 0.0071003797979771075
Kappa statistic: 0.8138476313535831
Training time: 536.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 12.211923076923336
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8411120335393424
Weighted AreaUnderROC: 0.9683990505275999
Root mean squared error: 0.10499157475355407
Relative absolute error: 34.25968000000675
Root relative squared error: 54.595618871851926
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.8136774908216655
Weighted FMeasure: 0.8147555381316475
Iteration time: 631.0
Weighted AreaUnderPRC: 0.8716631073873478
Mean absolute error: 0.02534000000000146
Coverage of cases: 94.84
Instances selection time: 46.0
Test time: 213.0
Accumulative iteration time: 4309.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.007466091068402748
Kappa statistic: 0.8044884521988254
Training time: 585.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 11.924615384615848
Incorrectly Classified Instances: 18.49
Correctly Classified Instances: 81.51
Weighted Precision: 0.842998180360086
Weighted AreaUnderROC: 0.9721404043565098
Root mean squared error: 0.10338873618161265
Relative absolute error: 33.371520000006804
Root relative squared error: 53.762142814442335
Weighted TruePositiveRate: 0.8151
Weighted MatthewsCorrelation: 0.8162706937506838
Weighted FMeasure: 0.8172150696082858
Iteration time: 658.0
Weighted AreaUnderPRC: 0.878928574471647
Mean absolute error: 0.024683076923078508
Coverage of cases: 95.51
Instances selection time: 46.0
Test time: 213.0
Accumulative iteration time: 4967.0
Weighted Recall: 0.8151
Weighted FalsePositiveRate: 0.007340798365962962
Kappa statistic: 0.8077100971282403
Training time: 612.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 11.887307692308026
Incorrectly Classified Instances: 17.13
Correctly Classified Instances: 82.87
Weighted Precision: 0.8496985048805152
Weighted AreaUnderROC: 0.9730557468480129
Root mean squared error: 0.10181922139829093
Relative absolute error: 32.7901600000065
Root relative squared error: 52.945995127114976
Weighted TruePositiveRate: 0.8287
Weighted MatthewsCorrelation: 0.8280335732071399
Weighted FMeasure: 0.8297054551745058
Iteration time: 698.0
Weighted AreaUnderPRC: 0.8849823006488529
Mean absolute error: 0.02425307692307835
Coverage of cases: 95.63
Instances selection time: 42.0
Test time: 221.0
Accumulative iteration time: 5665.0
Weighted Recall: 0.8287
Weighted FalsePositiveRate: 0.006806923226870909
Kappa statistic: 0.8218492892410636
Training time: 656.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 11.76461538461582
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8575701014465374
Weighted AreaUnderROC: 0.9742879754650934
Root mean squared error: 0.10055499834572089
Relative absolute error: 32.1318400000063
Root relative squared error: 52.28859913977851
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.8360780288989994
Weighted FMeasure: 0.8371714781476778
Iteration time: 720.0
Weighted AreaUnderPRC: 0.890947028871628
Mean absolute error: 0.02376615384615519
Coverage of cases: 95.78
Instances selection time: 37.0
Test time: 218.0
Accumulative iteration time: 6385.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.0065033948137899825
Kappa statistic: 0.8302695725833192
Training time: 683.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 11.643846153846624
Incorrectly Classified Instances: 16.71
Correctly Classified Instances: 83.29
Weighted Precision: 0.8535170864400947
Weighted AreaUnderROC: 0.9724008160487925
Root mean squared error: 0.1001998003990042
Relative absolute error: 31.72312000000604
Root relative squared error: 52.10389620748582
Weighted TruePositiveRate: 0.8329
Weighted MatthewsCorrelation: 0.8317889989530232
Weighted FMeasure: 0.8328330855041622
Iteration time: 755.0
Weighted AreaUnderPRC: 0.888527297054681
Mean absolute error: 0.023463846153847348
Coverage of cases: 95.41
Instances selection time: 34.0
Test time: 212.0
Accumulative iteration time: 7140.0
Weighted Recall: 0.8329
Weighted FalsePositiveRate: 0.0066642564495118135
Kappa statistic: 0.8262122096187766
Training time: 721.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 11.497307692308294
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.859278054216322
Weighted AreaUnderROC: 0.9753066436951869
Root mean squared error: 0.09821875114723928
Relative absolute error: 30.85888000000598
Root relative squared error: 51.07375059656799
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.8402388313122001
Weighted FMeasure: 0.8414145409687429
Iteration time: 806.0
Weighted AreaUnderPRC: 0.8963646421711855
Mean absolute error: 0.02282461538461662
Coverage of cases: 95.92
Instances selection time: 33.0
Test time: 215.0
Accumulative iteration time: 7946.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.006283080997173625
Kappa statistic: 0.8360897436110778
Training time: 773.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 11.154615384616147
Incorrectly Classified Instances: 15.17
Correctly Classified Instances: 84.83
Weighted Precision: 0.8632262344143875
Weighted AreaUnderROC: 0.9750323449493541
Root mean squared error: 0.09714659818265267
Relative absolute error: 29.919760000005827
Root relative squared error: 50.51623105498291
Weighted TruePositiveRate: 0.8483
Weighted MatthewsCorrelation: 0.845837530870153
Weighted FMeasure: 0.8473242745439333
Iteration time: 817.0
Weighted AreaUnderPRC: 0.8972672649387164
Mean absolute error: 0.02213000000000122
Coverage of cases: 95.84
Instances selection time: 30.0
Test time: 214.0
Accumulative iteration time: 8763.0
Weighted Recall: 0.8483
Weighted FalsePositiveRate: 0.0060410969104487166
Kappa statistic: 0.8422291266768551
Training time: 787.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 10.903461538462293
Incorrectly Classified Instances: 15.14
Correctly Classified Instances: 84.86
Weighted Precision: 0.8642476069433922
Weighted AreaUnderROC: 0.9747131390025057
Root mean squared error: 0.09681306332706066
Relative absolute error: 29.231280000005576
Root relative squared error: 50.342792930075056
Weighted TruePositiveRate: 0.8486
Weighted MatthewsCorrelation: 0.8458595887715974
Weighted FMeasure: 0.8466221829029451
Iteration time: 847.0
Weighted AreaUnderPRC: 0.897801205441675
Mean absolute error: 0.02162076923077034
Coverage of cases: 95.73
Instances selection time: 29.0
Test time: 214.0
Accumulative iteration time: 9610.0
Weighted Recall: 0.8486
Weighted FalsePositiveRate: 0.006038823538937943
Kappa statistic: 0.84253962411317
Training time: 818.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 10.87576923077006
Incorrectly Classified Instances: 14.61
Correctly Classified Instances: 85.39
Weighted Precision: 0.8668065966503115
Weighted AreaUnderROC: 0.9791924502852422
Root mean squared error: 0.09461622888929216
Relative absolute error: 28.57400000000556
Root relative squared error: 49.20043902243536
Weighted TruePositiveRate: 0.8539
Weighted MatthewsCorrelation: 0.8504036382090558
Weighted FMeasure: 0.8514316432639536
Iteration time: 895.0
Weighted AreaUnderPRC: 0.9097037448534813
Mean absolute error: 0.02113461538461655
Coverage of cases: 96.56
Instances selection time: 27.0
Test time: 214.0
Accumulative iteration time: 10505.0
Weighted Recall: 0.8539
Weighted FalsePositiveRate: 0.005831786514489106
Kappa statistic: 0.8480513032050025
Training time: 868.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 10.721923076923945
Incorrectly Classified Instances: 13.79
Correctly Classified Instances: 86.21
Weighted Precision: 0.8731669342674506
Weighted AreaUnderROC: 0.9792117267983284
Root mean squared error: 0.09333768304882686
Relative absolute error: 28.021760000005496
Root relative squared error: 48.53559518539336
Weighted TruePositiveRate: 0.8621
Weighted MatthewsCorrelation: 0.8586734418844301
Weighted FMeasure: 0.8601237012601634
Iteration time: 935.0
Weighted AreaUnderPRC: 0.9117184261403203
Mean absolute error: 0.020726153846155018
Coverage of cases: 96.51
Instances selection time: 24.0
Test time: 213.0
Accumulative iteration time: 11440.0
Weighted Recall: 0.8621
Weighted FalsePositiveRate: 0.005501581100839359
Kappa statistic: 0.8565786151580532
Training time: 911.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 10.866153846154623
Incorrectly Classified Instances: 12.96
Correctly Classified Instances: 87.04
Weighted Precision: 0.8781319641114192
Weighted AreaUnderROC: 0.9815771771106749
Root mean squared error: 0.09215872595273299
Relative absolute error: 27.788800000005356
Root relative squared error: 47.92253749542449
Weighted TruePositiveRate: 0.8704
Weighted MatthewsCorrelation: 0.8664559325528478
Weighted FMeasure: 0.868544552946092
Iteration time: 1000.0
Weighted AreaUnderPRC: 0.9164446841211089
Mean absolute error: 0.020553846153847245
Coverage of cases: 96.98
Instances selection time: 22.0
Test time: 210.0
Accumulative iteration time: 12440.0
Weighted Recall: 0.8704
Weighted FalsePositiveRate: 0.005180268234361778
Kappa statistic: 0.865212540167651
Training time: 978.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 10.497307692308674
Incorrectly Classified Instances: 12.36
Correctly Classified Instances: 87.64
Weighted Precision: 0.8846614406924757
Weighted AreaUnderROC: 0.9826854667610271
Root mean squared error: 0.08984302549875187
Relative absolute error: 26.527280000005078
Root relative squared error: 46.71837325935423
Weighted TruePositiveRate: 0.8764
Weighted MatthewsCorrelation: 0.8732032526291661
Weighted FMeasure: 0.875350571953386
Iteration time: 1050.0
Weighted AreaUnderPRC: 0.9227747594484521
Mean absolute error: 0.019620769230770246
Coverage of cases: 97.14
Instances selection time: 32.0
Test time: 195.0
Accumulative iteration time: 13490.0
Weighted Recall: 0.8764
Weighted FalsePositiveRate: 0.004928322127914864
Kappa statistic: 0.8714553848368897
Training time: 1018.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 10.422692307693284
Incorrectly Classified Instances: 12.18
Correctly Classified Instances: 87.82
Weighted Precision: 0.8872451866727832
Weighted AreaUnderROC: 0.9816688342953889
Root mean squared error: 0.08927356400844166
Relative absolute error: 26.345280000005058
Root relative squared error: 46.42225328439291
Weighted TruePositiveRate: 0.8782
Weighted MatthewsCorrelation: 0.8748945281347842
Weighted FMeasure: 0.8763116922540719
Iteration time: 1097.0
Weighted AreaUnderPRC: 0.9253021871588185
Mean absolute error: 0.019486153846154868
Coverage of cases: 96.89
Instances selection time: 17.0
Test time: 210.0
Accumulative iteration time: 14587.0
Weighted Recall: 0.8782
Weighted FalsePositiveRate: 0.004849409843943105
Kappa statistic: 0.8733288165731168
Training time: 1080.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 10.171153846154853
Incorrectly Classified Instances: 11.08
Correctly Classified Instances: 88.92
Weighted Precision: 0.8986303088338523
Weighted AreaUnderROC: 0.9872305188285502
Root mean squared error: 0.08613674280780832
Relative absolute error: 25.076480000004747
Root relative squared error: 44.79110626006346
Weighted TruePositiveRate: 0.8892
Weighted MatthewsCorrelation: 0.8868475888154244
Weighted FMeasure: 0.8882202850753105
Iteration time: 1148.0
Weighted AreaUnderPRC: 0.9390484661857242
Mean absolute error: 0.01854769230769323
Coverage of cases: 97.89
Instances selection time: 30.0
Test time: 203.0
Accumulative iteration time: 15735.0
Weighted Recall: 0.8892
Weighted FalsePositiveRate: 0.004421766695582424
Kappa statistic: 0.8847672567948994
Training time: 1118.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 9.994230769231828
Incorrectly Classified Instances: 10.55
Correctly Classified Instances: 89.45
Weighted Precision: 0.9032170308761315
Weighted AreaUnderROC: 0.9866791012633117
Root mean squared error: 0.08431762294124279
Relative absolute error: 24.334960000004752
Root relative squared error: 43.84516392944931
Weighted TruePositiveRate: 0.8945
Weighted MatthewsCorrelation: 0.8924906805223954
Weighted FMeasure: 0.8942145789556245
Iteration time: 1168.0
Weighted AreaUnderPRC: 0.9417663320606622
Mean absolute error: 0.017999230769231774
Coverage of cases: 97.76
Instances selection time: 11.0
Test time: 205.0
Accumulative iteration time: 16903.0
Weighted Recall: 0.8945
Weighted FalsePositiveRate: 0.004227448259445009
Kappa statistic: 0.8902768252257718
Training time: 1157.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 9.656538461539474
Incorrectly Classified Instances: 9.89
Correctly Classified Instances: 90.11
Weighted Precision: 0.908148592023837
Weighted AreaUnderROC: 0.986862369039669
Root mean squared error: 0.08231786421689267
Relative absolute error: 23.18888000000434
Root relative squared error: 42.80528939278718
Weighted TruePositiveRate: 0.9011
Weighted MatthewsCorrelation: 0.8987168163029108
Weighted FMeasure: 0.9004219876146452
Iteration time: 1208.0
Weighted AreaUnderPRC: 0.9426462309319337
Mean absolute error: 0.017151538461539276
Coverage of cases: 97.78
Instances selection time: 10.0
Test time: 204.0
Accumulative iteration time: 18111.0
Weighted Recall: 0.9011
Weighted FalsePositiveRate: 0.003964106759700449
Kappa statistic: 0.8971415886696915
Training time: 1198.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 9.79615384615483
Incorrectly Classified Instances: 9.75
Correctly Classified Instances: 90.25
Weighted Precision: 0.9086856508342155
Weighted AreaUnderROC: 0.9896566537819781
Root mean squared error: 0.08194791967134706
Relative absolute error: 23.20032000000451
Root relative squared error: 42.61291822910344
Weighted TruePositiveRate: 0.9025
Weighted MatthewsCorrelation: 0.9000816765077103
Weighted FMeasure: 0.9021209974131467
Iteration time: 1271.0
Weighted AreaUnderPRC: 0.9467588271384019
Mean absolute error: 0.017160000000000942
Coverage of cases: 98.34
Instances selection time: 7.0
Test time: 211.0
Accumulative iteration time: 19382.0
Weighted Recall: 0.9025
Weighted FalsePositiveRate: 0.003897361215810883
Kappa statistic: 0.8985990570523513
Training time: 1264.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 9.77653846153946
Incorrectly Classified Instances: 9.07
Correctly Classified Instances: 90.93
Weighted Precision: 0.9134508491567888
Weighted AreaUnderROC: 0.9905600882324676
Root mean squared error: 0.08046164877180277
Relative absolute error: 22.803040000004483
Root relative squared error: 41.840057361340364
Weighted TruePositiveRate: 0.9093
Weighted MatthewsCorrelation: 0.9068376577850078
Weighted FMeasure: 0.9093932256276841
Iteration time: 1477.0
Weighted AreaUnderPRC: 0.9496809411546934
Mean absolute error: 0.016866153846154808
Coverage of cases: 98.51
Instances selection time: 5.0
Test time: 209.0
Accumulative iteration time: 20859.0
Weighted Recall: 0.9093
Weighted FalsePositiveRate: 0.003620501510765304
Kappa statistic: 0.9056699131920876
Training time: 1472.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 9.453846153847133
Incorrectly Classified Instances: 8.18
Correctly Classified Instances: 91.82
Weighted Precision: 0.9206165977245079
Weighted AreaUnderROC: 0.992631654441966
Root mean squared error: 0.07756039830103627
Relative absolute error: 21.47808000000426
Root relative squared error: 40.331407116541676
Weighted TruePositiveRate: 0.9182
Weighted MatthewsCorrelation: 0.9157430218945602
Weighted FMeasure: 0.9185475988361477
Iteration time: 1346.0
Weighted AreaUnderPRC: 0.9567815373398744
Mean absolute error: 0.015886153846154782
Coverage of cases: 98.88
Instances selection time: 2.0
Test time: 211.0
Accumulative iteration time: 22205.0
Weighted Recall: 0.9182
Weighted FalsePositiveRate: 0.0032710971337853957
Kappa statistic: 0.9149251296419341
Training time: 1344.0
		
Time end:Sun Oct 08 02.43.44 EEST 2017