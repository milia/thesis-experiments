Sun Oct 08 01.32.59 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 01.32.59 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 3.867730769231282
Incorrectly Classified Instances: 15.221
Correctly Classified Instances: 84.77900000000001
Weighted Precision: 0.850322883730606
Weighted AreaUnderROC: 0.9233917091031426
Root mean squared error: 0.10702758746476729
Relative absolute error: 16.88090077367248
Root relative squared error: 55.65434548168288
Weighted TruePositiveRate: 0.84779
Weighted MatthewsCorrelation: 0.8425599387018785
Weighted FMeasure: 0.8481326829184781
Iteration time: 3299.6
Weighted AreaUnderPRC: 0.7374701325731055
Mean absolute error: 0.012485873353306306
Coverage of cases: 84.97099999999999
Instances selection time: 3298.7
Test time: 4244.4
Accumulative iteration time: 3299.6
Weighted Recall: 0.84779
Weighted FalsePositiveRate: 0.006076620063383493
Kappa statistic: 0.8416956133125311
Training time: 0.9
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 3.868038461538975
Incorrectly Classified Instances: 15.339999999999998
Correctly Classified Instances: 84.66
Weighted Precision: 0.8536367463552047
Weighted AreaUnderROC: 0.9227015012559004
Root mean squared error: 0.10755393651609321
Relative absolute error: 16.873410163550254
Root relative squared error: 55.92804698837239
Weighted TruePositiveRate: 0.8465999999999999
Weighted MatthewsCorrelation: 0.8428636057676785
Weighted FMeasure: 0.8476103967117959
Iteration time: 3606.9
Weighted AreaUnderPRC: 0.7380816346691063
Mean absolute error: 0.012480332961204068
Coverage of cases: 84.85
Instances selection time: 3605.9
Test time: 4674.2
Accumulative iteration time: 6906.5
Weighted Recall: 0.8465999999999999
Weighted FalsePositiveRate: 0.0061115411075147175
Kappa statistic: 0.8404609462271816
Training time: 1.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 3.8689615384620515
Incorrectly Classified Instances: 15.244
Correctly Classified Instances: 84.756
Weighted Precision: 0.856192936349706
Weighted AreaUnderROC: 0.9233438503820643
Root mean squared error: 0.10724455956825966
Relative absolute error: 16.665522108862408
Root relative squared error: 55.767170975498914
Weighted TruePositiveRate: 0.8475600000000002
Weighted MatthewsCorrelation: 0.8443597246095157
Weighted FMeasure: 0.8487008057223813
Iteration time: 3814.5
Weighted AreaUnderPRC: 0.7409868983545239
Mean absolute error: 0.01232656960714503
Coverage of cases: 84.953
Instances selection time: 3813.2
Test time: 5211.2
Accumulative iteration time: 10721.0
Weighted Recall: 0.8475600000000002
Weighted FalsePositiveRate: 0.006072222323279585
Kappa statistic: 0.8414593285931076
Training time: 1.3
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 3.868846153846667
Incorrectly Classified Instances: 15.158000000000001
Correctly Classified Instances: 84.84199999999998
Weighted Precision: 0.859491685990878
Weighted AreaUnderROC: 0.9238339115306398
Root mean squared error: 0.10698070932495736
Relative absolute error: 16.487249126710005
Root relative squared error: 55.629968848981704
Weighted TruePositiveRate: 0.84842
Weighted MatthewsCorrelation: 0.8460667684037573
Weighted FMeasure: 0.849877612785171
Iteration time: 4025.1
Weighted AreaUnderPRC: 0.7438834590160651
Mean absolute error: 0.012194710892535283
Coverage of cases: 85.04
Instances selection time: 4023.8
Test time: 5639.7
Accumulative iteration time: 14746.1
Weighted Recall: 0.84842
Weighted FalsePositiveRate: 0.006030250396028087
Kappa statistic: 0.8423552969476003
Training time: 1.3
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 3.869615384615898
Incorrectly Classified Instances: 15.070000000000002
Correctly Classified Instances: 84.93
Weighted Precision: 0.8616133665257191
Weighted AreaUnderROC: 0.9243024411181938
Root mean squared error: 0.1066816229747557
Relative absolute error: 16.323779484487623
Root relative squared error: 55.47444394687684
Weighted TruePositiveRate: 0.8492999999999998
Weighted MatthewsCorrelation: 0.8472268560923094
Weighted FMeasure: 0.8505583974994781
Iteration time: 4225.5
Weighted AreaUnderPRC: 0.7460480068471318
Mean absolute error: 0.012073801393850107
Coverage of cases: 85.134
Instances selection time: 4224.2
Test time: 6189.4
Accumulative iteration time: 18971.6
Weighted Recall: 0.8492999999999998
Weighted FalsePositiveRate: 0.006003601521732701
Kappa statistic: 0.8432687128409722
Training time: 1.3
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 3.868846153846667
Incorrectly Classified Instances: 14.831
Correctly Classified Instances: 85.16900000000001
Weighted Precision: 0.8638701274511453
Weighted AreaUnderROC: 0.9257942841165103
Root mean squared error: 0.10585713674803936
Relative absolute error: 16.014015607623655
Root relative squared error: 55.045711108984314
Weighted TruePositiveRate: 0.85169
Weighted MatthewsCorrelation: 0.8495081147042207
Weighted FMeasure: 0.8525875757487507
Iteration time: 4392.5
Weighted AreaUnderPRC: 0.7500801660924498
Mean absolute error: 0.0118446861003117
Coverage of cases: 85.374
Instances selection time: 4391.4
Test time: 6685.0
Accumulative iteration time: 23364.1
Weighted Recall: 0.85169
Weighted FalsePositiveRate: 0.005908971487426082
Kappa statistic: 0.845754421042938
Training time: 1.1
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 3.868346153846667
Incorrectly Classified Instances: 14.540000000000001
Correctly Classified Instances: 85.46000000000001
Weighted Precision: 0.8666583678552933
Weighted AreaUnderROC: 0.9273731475420032
Root mean squared error: 0.10484194768058575
Relative absolute error: 15.665682189872305
Root relative squared error: 54.51781279390839
Weighted TruePositiveRate: 0.8545999999999999
Weighted MatthewsCorrelation: 0.8523308911635719
Weighted FMeasure: 0.8551310646876263
Iteration time: 4552.7
Weighted AreaUnderPRC: 0.7545263395476479
Mean absolute error: 0.011587043039844763
Coverage of cases: 85.66399999999999
Instances selection time: 4551.2
Test time: 7189.0
Accumulative iteration time: 27916.8
Weighted Recall: 0.8545999999999999
Weighted FalsePositiveRate: 0.005792687596486044
Kappa statistic: 0.8487811331999946
Training time: 1.5
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 3.8685000000005116
Incorrectly Classified Instances: 14.162
Correctly Classified Instances: 85.838
Weighted Precision: 0.8701823809652944
Weighted AreaUnderROC: 0.9293307361135223
Root mean squared error: 0.10349173920087376
Relative absolute error: 15.239289061078747
Root relative squared error: 53.8157043844581
Weighted TruePositiveRate: 0.8583799999999998
Weighted MatthewsCorrelation: 0.8561185488456303
Weighted FMeasure: 0.85875908764786
Iteration time: 4636.0
Weighted AreaUnderPRC: 0.7605210961904194
Mean absolute error: 0.011271663506713476
Coverage of cases: 86.04
Instances selection time: 4634.6
Test time: 7716.3
Accumulative iteration time: 32552.8
Weighted Recall: 0.8583799999999998
Weighted FalsePositiveRate: 0.005644127064025076
Kappa statistic: 0.8527118321437601
Training time: 1.4
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 3.86846153846205
Incorrectly Classified Instances: 13.963
Correctly Classified Instances: 86.037
Weighted Precision: 0.8719160627141255
Weighted AreaUnderROC: 0.9305612002460124
Root mean squared error: 0.10277186125398748
Relative absolute error: 14.997441159861939
Root relative squared error: 53.441367852077214
Weighted TruePositiveRate: 0.86037
Weighted MatthewsCorrelation: 0.8579523240302456
Weighted FMeasure: 0.8603881121854489
Iteration time: 4751.3
Weighted AreaUnderPRC: 0.7639061017257227
Mean absolute error: 0.01109278192297326
Coverage of cases: 86.239
Instances selection time: 4749.6
Test time: 8306.7
Accumulative iteration time: 37304.1
Weighted Recall: 0.86037
Weighted FalsePositiveRate: 0.005563739574273302
Kappa statistic: 0.8547814192986187
Training time: 1.7
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 3.867769230769743
Incorrectly Classified Instances: 13.676000000000002
Correctly Classified Instances: 86.32399999999998
Weighted Precision: 0.8745538403494584
Weighted AreaUnderROC: 0.9320928723337509
Root mean squared error: 0.10172610316517243
Relative absolute error: 14.667622696371316
Root relative squared error: 52.89757364589336
Weighted TruePositiveRate: 0.86324
Weighted MatthewsCorrelation: 0.8607182470057705
Weighted FMeasure: 0.8629253122066561
Iteration time: 4814.5
Weighted AreaUnderPRC: 0.7684367831405233
Mean absolute error: 0.010848833355302715
Coverage of cases: 86.52199999999999
Instances selection time: 4812.9
Test time: 8797.7
Accumulative iteration time: 42118.6
Weighted Recall: 0.86324
Weighted FalsePositiveRate: 0.005444139560886114
Kappa statistic: 0.857767723422931
Training time: 1.6
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 3.867846153846665
Incorrectly Classified Instances: 13.315999999999999
Correctly Classified Instances: 86.684
Weighted Precision: 0.8778663285431728
Weighted AreaUnderROC: 0.9340360611356445
Root mean squared error: 0.10035755986355957
Relative absolute error: 14.260600133095853
Root relative squared error: 52.18593112905463
Weighted TruePositiveRate: 0.86684
Weighted MatthewsCorrelation: 0.8642707804680386
Weighted FMeasure: 0.866285520085888
Iteration time: 4778.5
Weighted AreaUnderPRC: 0.7743668382542289
Mean absolute error: 0.010547781163530962
Coverage of cases: 86.889
Instances selection time: 4776.6
Test time: 9363.9
Accumulative iteration time: 46897.1
Weighted Recall: 0.86684
Weighted FalsePositiveRate: 0.005302902179266669
Kappa statistic: 0.8615112171263124
Training time: 1.9
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 3.8678461538466657
Incorrectly Classified Instances: 12.885
Correctly Classified Instances: 87.11499999999998
Weighted Precision: 0.8819469832467167
Weighted AreaUnderROC: 0.9364406174615365
Root mean squared error: 0.09870836976807515
Relative absolute error: 13.786414409087905
Root relative squared error: 51.328352279402665
Weighted TruePositiveRate: 0.8711500000000001
Weighted MatthewsCorrelation: 0.8686199629349651
Weighted FMeasure: 0.8704531991117429
Iteration time: 4788.8
Weighted AreaUnderPRC: 0.7816406495397548
Mean absolute error: 0.010197052077726318
Coverage of cases: 87.326
Instances selection time: 4786.7
Test time: 9784.8
Accumulative iteration time: 51685.9
Weighted Recall: 0.8711500000000001
Weighted FalsePositiveRate: 0.005134704854894298
Kappa statistic: 0.8659928719510136
Training time: 2.1
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 3.868500000000511
Incorrectly Classified Instances: 12.704
Correctly Classified Instances: 87.29599999999999
Weighted Precision: 0.8837870319594746
Weighted AreaUnderROC: 0.9376189647636043
Root mean squared error: 0.09798224428935919
Relative absolute error: 13.571413846462965
Root relative squared error: 50.95076703047033
Weighted TruePositiveRate: 0.87296
Weighted MatthewsCorrelation: 0.8704341284046976
Weighted FMeasure: 0.8721006496327186
Iteration time: 4701.0
Weighted AreaUnderPRC: 0.7850312515702128
Mean absolute error: 0.01003802799294458
Coverage of cases: 87.52000000000001
Instances selection time: 4698.9
Test time: 10270.4
Accumulative iteration time: 56386.9
Weighted Recall: 0.87296
Weighted FalsePositiveRate: 0.005062812410386757
Kappa statistic: 0.8678754029624219
Training time: 2.1
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 3.8685384615389737
Incorrectly Classified Instances: 12.244
Correctly Classified Instances: 87.75599999999999
Weighted Precision: 0.8875787879706325
Weighted AreaUnderROC: 0.9398916508941848
Root mean squared error: 0.09622034661875169
Relative absolute error: 13.08460333443529
Root relative squared error: 50.03458024175437
Weighted TruePositiveRate: 0.8775599999999999
Weighted MatthewsCorrelation: 0.8748858311583602
Weighted FMeasure: 0.8764637216171686
Iteration time: 4677.3
Weighted AreaUnderPRC: 0.7919982495037925
Mean absolute error: 0.009677961046178598
Coverage of cases: 87.96799999999999
Instances selection time: 4675.1
Test time: 10829.7
Accumulative iteration time: 61064.2
Weighted Recall: 0.8775599999999999
Weighted FalsePositiveRate: 0.0048838793297178145
Kappa statistic: 0.8726581584559898
Training time: 2.2
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 3.868346153846667
Incorrectly Classified Instances: 11.783
Correctly Classified Instances: 88.217
Weighted Precision: 0.8911965478482413
Weighted AreaUnderROC: 0.9422261392039617
Root mean squared error: 0.09440100449961052
Relative absolute error: 12.5951626109047
Root relative squared error: 49.08852233980089
Weighted TruePositiveRate: 0.88217
Weighted MatthewsCorrelation: 0.8792934248947963
Weighted FMeasure: 0.8808248697138037
Iteration time: 4478.3
Weighted AreaUnderPRC: 0.7991654945441359
Mean absolute error: 0.009315948676703361
Coverage of cases: 88.422
Instances selection time: 4476.1
Test time: 11240.3
Accumulative iteration time: 65542.5
Weighted Recall: 0.88217
Weighted FalsePositiveRate: 0.004701084587409155
Kappa statistic: 0.8774523116309055
Training time: 2.2
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 3.8683461538466672
Incorrectly Classified Instances: 11.248
Correctly Classified Instances: 88.752
Weighted Precision: 0.8954308653901089
Weighted AreaUnderROC: 0.9449678473850932
Root mean squared error: 0.09222681543247399
Relative absolute error: 12.02917062580314
Root relative squared error: 47.95794402488983
Weighted TruePositiveRate: 0.88752
Weighted MatthewsCorrelation: 0.8844930986937536
Weighted FMeasure: 0.8860500639595956
Iteration time: 4343.4
Weighted AreaUnderPRC: 0.8074194585626124
Mean absolute error: 0.008897315551628298
Coverage of cases: 88.958
Instances selection time: 4341.0
Test time: 11711.4
Accumulative iteration time: 69885.9
Weighted Recall: 0.88752
Weighted FalsePositiveRate: 0.004489306026226251
Kappa statistic: 0.8830161070936008
Training time: 2.4
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 3.8681923076928215
Incorrectly Classified Instances: 10.941999999999998
Correctly Classified Instances: 89.05799999999999
Weighted Precision: 0.898082788189762
Weighted AreaUnderROC: 0.9466124416870605
Root mean squared error: 0.09098822342594859
Relative absolute error: 11.703566288179887
Root relative squared error: 47.31387618149656
Weighted TruePositiveRate: 0.8905799999999999
Weighted MatthewsCorrelation: 0.8874878480118719
Weighted FMeasure: 0.8889474056658309
Iteration time: 4046.7
Weighted AreaUnderPRC: 0.8124680191393077
Mean absolute error: 0.00865648394096025
Coverage of cases: 89.25699999999999
Instances selection time: 4044.6
Test time: 12123.8
Accumulative iteration time: 73932.6
Weighted Recall: 0.8905799999999999
Weighted FalsePositiveRate: 0.00436647342612724
Kappa statistic: 0.8861988581402371
Training time: 2.1
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 3.8675769230774373
Incorrectly Classified Instances: 10.405
Correctly Classified Instances: 89.595
Weighted Precision: 0.9025931199389845
Weighted AreaUnderROC: 0.9494177539629274
Root mean squared error: 0.08873852944804034
Relative absolute error: 11.136565354658426
Root relative squared error: 46.144035312984194
Weighted TruePositiveRate: 0.89595
Weighted MatthewsCorrelation: 0.8928514060707691
Weighted FMeasure: 0.8943442302613429
Iteration time: 3826.9
Weighted AreaUnderPRC: 0.8213339977606623
Mean absolute error: 0.008237104552261
Coverage of cases: 89.786
Instances selection time: 3824.5
Test time: 12567.9
Accumulative iteration time: 77759.5
Weighted Recall: 0.89595
Weighted FalsePositiveRate: 0.004151525612980054
Kappa statistic: 0.8917837143061174
Training time: 2.4
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 3.867423076923591
Incorrectly Classified Instances: 9.876999999999999
Correctly Classified Instances: 90.12299999999999
Weighted Precision: 0.9068601973532768
Weighted AreaUnderROC: 0.9522805799845114
Root mean squared error: 0.0864239817740591
Relative absolute error: 10.572478843354949
Root relative squared error: 44.94047052251386
Weighted TruePositiveRate: 0.90123
Weighted MatthewsCorrelation: 0.8981165213402151
Weighted FMeasure: 0.899730295583551
Iteration time: 3566.0
Weighted AreaUnderPRC: 0.8302409690634198
Mean absolute error: 0.00781988080129695
Coverage of cases: 90.319
Instances selection time: 3563.3
Test time: 13064.0
Accumulative iteration time: 81325.5
Weighted Recall: 0.90123
Weighted FalsePositiveRate: 0.003939701646864774
Kappa statistic: 0.8972753937669822
Training time: 2.7
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 3.8676538461543606
Incorrectly Classified Instances: 9.334
Correctly Classified Instances: 90.666
Weighted Precision: 0.9112663502654638
Weighted AreaUnderROC: 0.9550475079607329
Root mean squared error: 0.08398316205420475
Relative absolute error: 9.998325158024228
Root relative squared error: 43.67124426818952
Weighted TruePositiveRate: 0.9066600000000001
Weighted MatthewsCorrelation: 0.9035366013542511
Weighted FMeasure: 0.9052681131193255
Iteration time: 3306.8
Weighted AreaUnderPRC: 0.8393526163419718
Mean absolute error: 0.00739521091569736
Coverage of cases: 90.86699999999999
Instances selection time: 3304.2
Test time: 13565.3
Accumulative iteration time: 84632.3
Weighted Recall: 0.9066600000000001
Weighted FalsePositiveRate: 0.003717617172203501
Kappa statistic: 0.9029236649695541
Training time: 2.6
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 3.8669615384620526
Incorrectly Classified Instances: 8.841
Correctly Classified Instances: 91.159
Weighted Precision: 0.9157289858205357
Weighted AreaUnderROC: 0.9575651484303795
Root mean squared error: 0.08172795856749614
Relative absolute error: 9.476260545738764
Root relative squared error: 42.498538455100956
Weighted TruePositiveRate: 0.91159
Weighted MatthewsCorrelation: 0.9086132933506296
Weighted FMeasure: 0.9103709308424524
Iteration time: 3036.1
Weighted AreaUnderPRC: 0.8474397111460868
Mean absolute error: 0.007009068450989232
Coverage of cases: 91.356
Instances selection time: 3033.4
Test time: 14008.1
Accumulative iteration time: 87668.4
Weighted Recall: 0.91159
Weighted FalsePositiveRate: 0.003520319607678067
Kappa statistic: 0.9080512529019442
Training time: 2.7
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 3.8670000000005147
Incorrectly Classified Instances: 8.329
Correctly Classified Instances: 91.67099999999998
Weighted Precision: 0.9206713875149285
Weighted AreaUnderROC: 0.9602450761123429
Root mean squared error: 0.07929535168359078
Relative absolute error: 8.935307434030305
Root relative squared error: 41.23358287547009
Weighted TruePositiveRate: 0.9167099999999999
Weighted MatthewsCorrelation: 0.9140064125768765
Weighted FMeasure: 0.9157453590435255
Iteration time: 2610.3
Weighted AreaUnderPRC: 0.8565699374100781
Mean absolute error: 0.006608955202684214
Coverage of cases: 91.87299999999999
Instances selection time: 2607.6
Test time: 14380.5
Accumulative iteration time: 90278.7
Weighted Recall: 0.9167099999999999
Weighted FalsePositiveRate: 0.0033178856162147516
Kappa statistic: 0.9133760919161473
Training time: 2.7
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 3.866000000000514
Incorrectly Classified Instances: 7.906999999999999
Correctly Classified Instances: 92.09299999999999
Weighted Precision: 0.9242715306812579
Weighted AreaUnderROC: 0.96237202761209
Root mean squared error: 0.0772253442500386
Relative absolute error: 8.481088674290437
Root relative squared error: 40.15717901002288
Weighted TruePositiveRate: 0.9209299999999999
Weighted MatthewsCorrelation: 0.9182665973789772
Weighted FMeasure: 0.920051631270281
Iteration time: 2239.0
Weighted AreaUnderPRC: 0.8636819905395827
Mean absolute error: 0.006272994581574891
Coverage of cases: 92.292
Instances selection time: 2236.2
Test time: 14935.6
Accumulative iteration time: 92517.7
Weighted Recall: 0.9209299999999999
Weighted FalsePositiveRate: 0.003151144316330453
Kappa statistic: 0.9177648069462159
Training time: 2.8
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 3.865538461538976
Incorrectly Classified Instances: 7.496
Correctly Classified Instances: 92.50399999999999
Weighted Precision: 0.9278012267014487
Weighted AreaUnderROC: 0.9645231288218558
Root mean squared error: 0.07517667303906912
Relative absolute error: 8.043354337795773
Root relative squared error: 39.091869980318684
Weighted TruePositiveRate: 0.9250399999999999
Weighted MatthewsCorrelation: 0.9224215869104707
Weighted FMeasure: 0.9242405368023489
Iteration time: 1812.6
Weighted AreaUnderPRC: 0.8706060539931719
Mean absolute error: 0.00594922658120906
Coverage of cases: 92.704
Instances selection time: 1809.6
Test time: 15310.9
Accumulative iteration time: 94330.3
Weighted Recall: 0.9250399999999999
Weighted FalsePositiveRate: 0.0029858245813706294
Kappa statistic: 0.9220393103150574
Training time: 3.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 3.8650384615389752
Incorrectly Classified Instances: 7.081000000000001
Correctly Classified Instances: 92.91900000000001
Weighted Precision: 0.9312584873739349
Weighted AreaUnderROC: 0.9667080562793136
Root mean squared error: 0.07307188596984898
Relative absolute error: 7.608945070187107
Root relative squared error: 37.99738070432413
Weighted TruePositiveRate: 0.92919
Weighted MatthewsCorrelation: 0.9265672612383768
Weighted FMeasure: 0.9284259387761551
Iteration time: 1355.0
Weighted AreaUnderPRC: 0.8775530095450733
Mean absolute error: 0.005627917951321041
Coverage of cases: 93.112
Instances selection time: 1351.9
Test time: 15756.8
Accumulative iteration time: 95685.3
Weighted Recall: 0.92919
Weighted FalsePositiveRate: 0.002821076524230214
Kappa statistic: 0.9263550852539268
Training time: 3.1
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 3.863846153846667
Incorrectly Classified Instances: 6.413000000000001
Correctly Classified Instances: 93.58699999999999
Weighted Precision: 0.9368755463732384
Weighted AreaUnderROC: 0.9699845867710077
Root mean squared error: 0.06957181495718584
Relative absolute error: 6.916875317739054
Root relative squared error: 36.17734377773915
Weighted TruePositiveRate: 0.9358699999999999
Weighted MatthewsCorrelation: 0.9334662696305875
Weighted FMeasure: 0.935613928760913
Iteration time: 883.9
Weighted AreaUnderPRC: 0.8884126993583534
Mean absolute error: 0.005116032039747106
Coverage of cases: 93.76500000000001
Instances selection time: 880.8
Test time: 16200.2
Accumulative iteration time: 96569.2
Weighted Recall: 0.9358699999999999
Weighted FalsePositiveRate: 0.002555568612618063
Kappa statistic: 0.9333026527269401
Training time: 3.1
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 3.86265384615436
Incorrectly Classified Instances: 5.859
Correctly Classified Instances: 94.14099999999999
Weighted Precision: 0.9418557121096246
Weighted AreaUnderROC: 0.9727280657232263
Root mean squared error: 0.06645413849936195
Relative absolute error: 6.327967621050652
Root relative squared error: 34.55615201967062
Weighted TruePositiveRate: 0.9414100000000001
Weighted MatthewsCorrelation: 0.9392056189914207
Weighted FMeasure: 0.9414309259005373
Iteration time: 370.8
Weighted AreaUnderPRC: 0.8979746636356797
Mean absolute error: 0.004680449423853381
Coverage of cases: 94.314
Instances selection time: 367.8
Test time: 16701.1
Accumulative iteration time: 96940.0
Weighted Recall: 0.9414100000000001
Weighted FalsePositiveRate: 0.002333224895370897
Kappa statistic: 0.9390644415918764
Training time: 3.0
		
Time end:Sun Oct 08 02.38.07 EEST 2017