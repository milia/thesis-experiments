Sun Oct 08 01.24.12 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 01.24.12 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 78.81807692307487
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7608101975165453
Weighted AreaUnderROC: 0.9618993094598156
Root mean squared error: 0.1865052470887207
Relative absolute error: 96.34064000000075
Root relative squared error: 96.98272848614153
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.7332225897143922
Weighted FMeasure: 0.7371957493240441
Iteration time: 2983.0
Weighted AreaUnderPRC: 0.6523430440687603
Mean absolute error: 0.07125786982247583
Coverage of cases: 99.68
Instances selection time: 337.0
Test time: 550.0
Accumulative iteration time: 2983.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.010488021137549363
Kappa statistic: 0.727083268371732
Training time: 2646.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 78.84884615384412
Incorrectly Classified Instances: 28.31
Correctly Classified Instances: 71.69
Weighted Precision: 0.7790692675665882
Weighted AreaUnderROC: 0.9610708330973814
Root mean squared error: 0.18653332556828717
Relative absolute error: 96.35529600000206
Root relative squared error: 96.9973292955161
Weighted TruePositiveRate: 0.7169
Weighted MatthewsCorrelation: 0.7253181004279569
Weighted FMeasure: 0.7238583189267681
Iteration time: 2985.0
Weighted AreaUnderPRC: 0.6512955684353587
Mean absolute error: 0.07126871005916319
Coverage of cases: 99.68
Instances selection time: 331.0
Test time: 543.0
Accumulative iteration time: 5968.0
Weighted Recall: 0.7169
Weighted FalsePositiveRate: 0.01118381097016935
Kappa statistic: 0.7055944929052848
Training time: 2654.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 78.73923076922817
Incorrectly Classified Instances: 29.83
Correctly Classified Instances: 70.17
Weighted Precision: 0.7745809251441047
Weighted AreaUnderROC: 0.9594656183776409
Root mean squared error: 0.18656882768759753
Relative absolute error: 96.37244800000231
Root relative squared error: 97.0157903975575
Weighted TruePositiveRate: 0.7017
Weighted MatthewsCorrelation: 0.7111382584468569
Weighted FMeasure: 0.7062406163660673
Iteration time: 3063.0
Weighted AreaUnderPRC: 0.6376116890063811
Mean absolute error: 0.07128139644969592
Coverage of cases: 99.67
Instances selection time: 317.0
Test time: 563.0
Accumulative iteration time: 9031.0
Weighted Recall: 0.7017
Weighted FalsePositiveRate: 0.011845886311326027
Kappa statistic: 0.6897708838898633
Training time: 2746.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 78.75192307692036
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.779971211958991
Weighted AreaUnderROC: 0.9596682833990042
Root mean squared error: 0.18658003655526756
Relative absolute error: 96.37881600000324
Root relative squared error: 97.0216190087459
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.7122787781937564
Weighted FMeasure: 0.7044260190632389
Iteration time: 3145.0
Weighted AreaUnderPRC: 0.6426198009142541
Mean absolute error: 0.0712861065088682
Coverage of cases: 99.67
Instances selection time: 294.0
Test time: 543.0
Accumulative iteration time: 12176.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.011814157198137745
Kappa statistic: 0.6917279725673674
Training time: 2851.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 78.73807692307416
Incorrectly Classified Instances: 31.07
Correctly Classified Instances: 68.93
Weighted Precision: 0.7871609176548815
Weighted AreaUnderROC: 0.958341868331373
Root mean squared error: 0.18661424625378994
Relative absolute error: 96.39609600000423
Root relative squared error: 97.03940805197755
Weighted TruePositiveRate: 0.6893
Weighted MatthewsCorrelation: 0.7019217085540523
Weighted FMeasure: 0.6896685359341809
Iteration time: 3151.0
Weighted AreaUnderPRC: 0.6367728146442815
Mean absolute error: 0.07129888757395769
Coverage of cases: 99.68
Instances selection time: 292.0
Test time: 535.0
Accumulative iteration time: 15327.0
Weighted Recall: 0.6893
Weighted FalsePositiveRate: 0.012510417162129493
Kappa statistic: 0.6768082510739079
Training time: 2859.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 78.68576923076611
Incorrectly Classified Instances: 31.73
Correctly Classified Instances: 68.27
Weighted Precision: 0.7856262986057106
Weighted AreaUnderROC: 0.9586143204184003
Root mean squared error: 0.18662530410667388
Relative absolute error: 96.40185600000446
Root relative squared error: 97.04515813547718
Weighted TruePositiveRate: 0.6827
Weighted MatthewsCorrelation: 0.6947686435027441
Weighted FMeasure: 0.6802165333044216
Iteration time: 3237.0
Weighted AreaUnderPRC: 0.6306247881584309
Mean absolute error: 0.07130314792898744
Coverage of cases: 99.7
Instances selection time: 270.0
Test time: 576.0
Accumulative iteration time: 18564.0
Weighted Recall: 0.6827
Weighted FalsePositiveRate: 0.01276288117871811
Kappa statistic: 0.6699460005443967
Training time: 2967.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 78.77115384615098
Incorrectly Classified Instances: 32.21
Correctly Classified Instances: 67.79
Weighted Precision: 0.7926529180359844
Weighted AreaUnderROC: 0.9592139963356527
Root mean squared error: 0.18663363023436022
Relative absolute error: 96.40620800000639
Root relative squared error: 97.0494877218741
Weighted TruePositiveRate: 0.6779
Weighted MatthewsCorrelation: 0.6903495909553844
Weighted FMeasure: 0.6716364539198757
Iteration time: 3353.0
Weighted AreaUnderPRC: 0.6332096769399489
Mean absolute error: 0.07130636686390011
Coverage of cases: 99.63
Instances selection time: 256.0
Test time: 538.0
Accumulative iteration time: 21917.0
Weighted Recall: 0.6779
Weighted FalsePositiveRate: 0.012898237042374368
Kappa statistic: 0.6649734459991957
Training time: 3097.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 78.76230769230479
Incorrectly Classified Instances: 34.36
Correctly Classified Instances: 65.64
Weighted Precision: 0.778665373069316
Weighted AreaUnderROC: 0.9611918671326354
Root mean squared error: 0.186652470643844
Relative absolute error: 96.41433600000592
Root relative squared error: 97.05928473480564
Weighted TruePositiveRate: 0.6564
Weighted MatthewsCorrelation: 0.6712089756574994
Weighted FMeasure: 0.6517526363610053
Iteration time: 3460.0
Weighted AreaUnderPRC: 0.6358549801402315
Mean absolute error: 0.07131237869821928
Coverage of cases: 99.63
Instances selection time: 253.0
Test time: 546.0
Accumulative iteration time: 25377.0
Weighted Recall: 0.6564
Weighted FalsePositiveRate: 0.013598860865004014
Kappa statistic: 0.6426774453562976
Training time: 3207.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 78.7869230769204
Incorrectly Classified Instances: 35.1
Correctly Classified Instances: 64.9
Weighted Precision: 0.7794804623826194
Weighted AreaUnderROC: 0.9606471578787176
Root mean squared error: 0.18667749245760606
Relative absolute error: 96.42720000000777
Root relative squared error: 97.07229607796194
Weighted TruePositiveRate: 0.649
Weighted MatthewsCorrelation: 0.6583173993030649
Weighted FMeasure: 0.635880113008122
Iteration time: 3621.0
Weighted AreaUnderPRC: 0.619705898908845
Mean absolute error: 0.07132189349112007
Coverage of cases: 99.62
Instances selection time: 233.0
Test time: 553.0
Accumulative iteration time: 28998.0
Weighted Recall: 0.649
Weighted FalsePositiveRate: 0.013936479954200413
Kappa statistic: 0.6349651776539201
Training time: 3388.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 78.83192307692062
Incorrectly Classified Instances: 35.64
Correctly Classified Instances: 64.36
Weighted Precision: 0.7754268140353004
Weighted AreaUnderROC: 0.9605155427228137
Root mean squared error: 0.1866802507791094
Relative absolute error: 96.42876800000784
Root relative squared error: 97.07373040514366
Weighted TruePositiveRate: 0.6436
Weighted MatthewsCorrelation: 0.6512065477996143
Weighted FMeasure: 0.6271026100885675
Iteration time: 3610.0
Weighted AreaUnderPRC: 0.6160241076073245
Mean absolute error: 0.07132305325443372
Coverage of cases: 99.53
Instances selection time: 220.0
Test time: 548.0
Accumulative iteration time: 32608.0
Weighted Recall: 0.6436
Weighted FalsePositiveRate: 0.014146907669061114
Kappa statistic: 0.6293538019610948
Training time: 3390.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 78.81807692307429
Incorrectly Classified Instances: 35.95
Correctly Classified Instances: 64.05
Weighted Precision: 0.764004130226962
Weighted AreaUnderROC: 0.9602869370830176
Root mean squared error: 0.18668501927684017
Relative absolute error: 96.43168000000834
Root relative squared error: 97.07621002396365
Weighted TruePositiveRate: 0.6405
Weighted MatthewsCorrelation: 0.6403427955763867
Weighted FMeasure: 0.6162224531001987
Iteration time: 3694.0
Weighted AreaUnderPRC: 0.6098952408049476
Mean absolute error: 0.07132520710058794
Coverage of cases: 99.51
Instances selection time: 206.0
Test time: 548.0
Accumulative iteration time: 36302.0
Weighted Recall: 0.6405
Weighted FalsePositiveRate: 0.014247656611602173
Kappa statistic: 0.6261415979494298
Training time: 3488.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 78.88999999999776
Incorrectly Classified Instances: 34.86
Correctly Classified Instances: 65.14
Weighted Precision: 0.7627891849005493
Weighted AreaUnderROC: 0.9606604629968317
Root mean squared error: 0.18668342998618395
Relative absolute error: 96.43152000000877
Root relative squared error: 97.07538359282243
Weighted TruePositiveRate: 0.6514
Weighted MatthewsCorrelation: 0.6466187755240046
Weighted FMeasure: 0.623780040014902
Iteration time: 3806.0
Weighted AreaUnderPRC: 0.6154579300130597
Mean absolute error: 0.071325088757393
Coverage of cases: 99.45
Instances selection time: 194.0
Test time: 541.0
Accumulative iteration time: 40108.0
Weighted Recall: 0.6514
Weighted FalsePositiveRate: 0.013850669915539857
Kappa statistic: 0.637460497520052
Training time: 3612.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 78.9488461538439
Incorrectly Classified Instances: 34.73
Correctly Classified Instances: 65.27
Weighted Precision: 0.7618547856031886
Weighted AreaUnderROC: 0.9613979644759253
Root mean squared error: 0.18664803199354704
Relative absolute error: 96.4157440000089
Root relative squared error: 97.05697663665124
Weighted TruePositiveRate: 0.6527
Weighted MatthewsCorrelation: 0.6463282898356906
Weighted FMeasure: 0.6236230265470284
Iteration time: 4034.0
Weighted AreaUnderPRC: 0.6164170153673495
Mean absolute error: 0.07131342011833984
Coverage of cases: 99.43
Instances selection time: 180.0
Test time: 537.0
Accumulative iteration time: 44142.0
Weighted Recall: 0.6527
Weighted FalsePositiveRate: 0.013797079207770688
Kappa statistic: 0.6388130517051336
Training time: 3854.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 78.9373076923055
Incorrectly Classified Instances: 36.18
Correctly Classified Instances: 63.82
Weighted Precision: 0.7543567917569467
Weighted AreaUnderROC: 0.9620224871973753
Root mean squared error: 0.186636690442662
Relative absolute error: 96.41065600000876
Root relative squared error: 97.05107903019102
Weighted TruePositiveRate: 0.6382
Weighted MatthewsCorrelation: 0.6265343718505232
Weighted FMeasure: 0.5997646253619302
Iteration time: 4037.0
Weighted AreaUnderPRC: 0.6046276504437986
Mean absolute error: 0.07130965680473027
Coverage of cases: 99.43
Instances selection time: 177.0
Test time: 541.0
Accumulative iteration time: 48179.0
Weighted Recall: 0.6382
Weighted FalsePositiveRate: 0.014403958909341564
Kappa statistic: 0.6237264067070966
Training time: 3860.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 78.90769230768986
Incorrectly Classified Instances: 35.67
Correctly Classified Instances: 64.33
Weighted Precision: 0.7566701699176691
Weighted AreaUnderROC: 0.9626116203515527
Root mean squared error: 0.18661805039145318
Relative absolute error: 96.40169600001218
Root relative squared error: 97.04138620356242
Weighted TruePositiveRate: 0.6433
Weighted MatthewsCorrelation: 0.6356051619311702
Weighted FMeasure: 0.610285329048246
Iteration time: 4218.0
Weighted AreaUnderPRC: 0.6145677103338949
Mean absolute error: 0.07130302958579789
Coverage of cases: 99.38
Instances selection time: 166.0
Test time: 548.0
Accumulative iteration time: 52397.0
Weighted Recall: 0.6433
Weighted FalsePositiveRate: 0.014209737283545638
Kappa statistic: 0.6290157253650529
Training time: 4052.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 78.92307692307456
Incorrectly Classified Instances: 34.19
Correctly Classified Instances: 65.81
Weighted Precision: 0.7569546244869986
Weighted AreaUnderROC: 0.9640358342604782
Root mean squared error: 0.18659446567338522
Relative absolute error: 96.38995200001298
Root relative squared error: 97.0291221501671
Weighted TruePositiveRate: 0.6581
Weighted MatthewsCorrelation: 0.6453657776422453
Weighted FMeasure: 0.6226027676581274
Iteration time: 4284.0
Weighted AreaUnderPRC: 0.6277899882294472
Mean absolute error: 0.07129434319526594
Coverage of cases: 99.34
Instances selection time: 143.0
Test time: 549.0
Accumulative iteration time: 56681.0
Weighted Recall: 0.6581
Weighted FalsePositiveRate: 0.01363375897146048
Kappa statistic: 0.6443953192023169
Training time: 4141.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 79.1469230769217
Incorrectly Classified Instances: 33.97
Correctly Classified Instances: 66.03
Weighted Precision: 0.7509255178853794
Weighted AreaUnderROC: 0.9647651697165187
Root mean squared error: 0.18656811510779325
Relative absolute error: 96.37977600001311
Root relative squared error: 97.01541985605925
Weighted TruePositiveRate: 0.6603
Weighted MatthewsCorrelation: 0.6455639398826633
Weighted FMeasure: 0.6235093761208427
Iteration time: 4485.0
Weighted AreaUnderPRC: 0.6291317788988474
Mean absolute error: 0.0712868165680471
Coverage of cases: 99.54
Instances selection time: 132.0
Test time: 543.0
Accumulative iteration time: 61166.0
Weighted Recall: 0.6603
Weighted FalsePositiveRate: 0.013551022312551573
Kappa statistic: 0.6466798583253838
Training time: 4353.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 79.03884615384416
Incorrectly Classified Instances: 32.77
Correctly Classified Instances: 67.23
Weighted Precision: 0.7525084198744343
Weighted AreaUnderROC: 0.9662097704169482
Root mean squared error: 0.18653511798382116
Relative absolute error: 96.36396800001286
Root relative squared error: 96.99826135159377
Weighted TruePositiveRate: 0.6723
Weighted MatthewsCorrelation: 0.6621317986971357
Weighted FMeasure: 0.6428228716861649
Iteration time: 4513.0
Weighted AreaUnderPRC: 0.6336158393570319
Mean absolute error: 0.07127512426035461
Coverage of cases: 99.51
Instances selection time: 118.0
Test time: 540.0
Accumulative iteration time: 65679.0
Weighted Recall: 0.6723
Weighted FalsePositiveRate: 0.0130773343673105
Kappa statistic: 0.6591624720179358
Training time: 4395.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 79.1449999999987
Incorrectly Classified Instances: 31.77
Correctly Classified Instances: 68.23
Weighted Precision: 0.7532446468803554
Weighted AreaUnderROC: 0.9663794592602662
Root mean squared error: 0.1865299386328431
Relative absolute error: 96.36272000001345
Root relative squared error: 96.99556808908517
Weighted TruePositiveRate: 0.6823
Weighted MatthewsCorrelation: 0.6693388844767607
Weighted FMeasure: 0.65214690877194
Iteration time: 4615.0
Weighted AreaUnderPRC: 0.6401565511941005
Mean absolute error: 0.07127420118343197
Coverage of cases: 99.58
Instances selection time: 107.0
Test time: 567.0
Accumulative iteration time: 70294.0
Weighted Recall: 0.6823
Weighted FalsePositiveRate: 0.012686120052021412
Kappa statistic: 0.6695629262958079
Training time: 4508.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 79.11692307692152
Incorrectly Classified Instances: 30.66
Correctly Classified Instances: 69.34
Weighted Precision: 0.7546439731245963
Weighted AreaUnderROC: 0.9670666153437231
Root mean squared error: 0.18649973284291338
Relative absolute error: 96.34803200001322
Root relative squared error: 96.97986107832173
Weighted TruePositiveRate: 0.6934
Weighted MatthewsCorrelation: 0.6801418953415777
Weighted FMeasure: 0.6667733618089038
Iteration time: 4687.0
Weighted AreaUnderPRC: 0.6539874351989128
Mean absolute error: 0.07126333727810635
Coverage of cases: 99.61
Instances selection time: 94.0
Test time: 565.0
Accumulative iteration time: 74981.0
Weighted Recall: 0.6934
Weighted FalsePositiveRate: 0.012230548269755115
Kappa statistic: 0.6811101643108481
Training time: 4593.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 79.13230769230643
Incorrectly Classified Instances: 28.85
Correctly Classified Instances: 71.15
Weighted Precision: 0.7637455158263491
Weighted AreaUnderROC: 0.9679288894703197
Root mean squared error: 0.18646898729433806
Relative absolute error: 96.33321600001172
Root relative squared error: 96.96387339306256
Weighted TruePositiveRate: 0.7115
Weighted MatthewsCorrelation: 0.696773776639298
Weighted FMeasure: 0.6852633901522031
Iteration time: 4866.0
Weighted AreaUnderPRC: 0.6647014350483265
Mean absolute error: 0.0712523786982236
Coverage of cases: 99.61
Instances selection time: 82.0
Test time: 545.0
Accumulative iteration time: 79847.0
Weighted Recall: 0.7115
Weighted FalsePositiveRate: 0.011494109148344584
Kappa statistic: 0.6999411791105186
Training time: 4784.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 79.27961538461493
Incorrectly Classified Instances: 25.99
Correctly Classified Instances: 74.01
Weighted Precision: 0.7815602801228125
Weighted AreaUnderROC: 0.9686071533305961
Root mean squared error: 0.18643952786682905
Relative absolute error: 96.31968000000789
Root relative squared error: 96.94855449075787
Weighted TruePositiveRate: 0.7401
Weighted MatthewsCorrelation: 0.7289782040881346
Weighted FMeasure: 0.7220151640569771
Iteration time: 5008.0
Weighted AreaUnderPRC: 0.6847663656350522
Mean absolute error: 0.07124236686390123
Coverage of cases: 99.7
Instances selection time: 70.0
Test time: 545.0
Accumulative iteration time: 84855.0
Weighted Recall: 0.7401
Weighted FalsePositiveRate: 0.010326405513542646
Kappa statistic: 0.7296958415717687
Training time: 4938.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 79.3092307692305
Incorrectly Classified Instances: 24.38
Correctly Classified Instances: 75.62
Weighted Precision: 0.7837962279278418
Weighted AreaUnderROC: 0.9693294401754394
Root mean squared error: 0.18639531348804034
Relative absolute error: 96.2976960000057
Root relative squared error: 96.92556301378774
Weighted TruePositiveRate: 0.7562
Weighted MatthewsCorrelation: 0.7437373829958226
Weighted FMeasure: 0.7410101761997814
Iteration time: 5065.0
Weighted AreaUnderPRC: 0.6940218795151638
Mean absolute error: 0.07122610650887003
Coverage of cases: 99.68
Instances selection time: 58.0
Test time: 544.0
Accumulative iteration time: 89920.0
Weighted Recall: 0.7562
Weighted FalsePositiveRate: 0.009691298589964353
Kappa statistic: 0.7464405895740946
Training time: 5007.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 79.31423076923043
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.7902059218743409
Weighted AreaUnderROC: 0.9710494415581304
Root mean squared error: 0.18634890069482124
Relative absolute error: 96.27523200000444
Root relative squared error: 96.90142836131382
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.75144667686588
Weighted FMeasure: 0.748912392839935
Iteration time: 4771.0
Weighted AreaUnderPRC: 0.7017409763719131
Mean absolute error: 0.07120949112425372
Coverage of cases: 99.92
Instances selection time: 45.0
Test time: 538.0
Accumulative iteration time: 94691.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.009404756567795132
Kappa statistic: 0.7541361343232093
Training time: 4726.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 79.29769230769169
Incorrectly Classified Instances: 23.13
Correctly Classified Instances: 76.87
Weighted Precision: 0.7918156888387505
Weighted AreaUnderROC: 0.9721539232934722
Root mean squared error: 0.18631744075659276
Relative absolute error: 96.25984000000383
Root relative squared error: 96.885069193435
Weighted TruePositiveRate: 0.7687
Weighted MatthewsCorrelation: 0.755761163970295
Weighted FMeasure: 0.7539439636705695
Iteration time: 4882.0
Weighted AreaUnderPRC: 0.7060725988504537
Mean absolute error: 0.07119810650886865
Coverage of cases: 99.97
Instances selection time: 34.0
Test time: 539.0
Accumulative iteration time: 99573.0
Weighted Recall: 0.7687
Weighted FalsePositiveRate: 0.009204772033812878
Kappa statistic: 0.7594377270286949
Training time: 4848.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 79.34269230769185
Incorrectly Classified Instances: 21.98
Correctly Classified Instances: 78.02
Weighted Precision: 0.7948427320164629
Weighted AreaUnderROC: 0.9731551820481281
Root mean squared error: 0.18627809829503572
Relative absolute error: 96.24096000000307
Root relative squared error: 96.86461111342534
Weighted TruePositiveRate: 0.7802
Weighted MatthewsCorrelation: 0.7670217216748015
Weighted FMeasure: 0.7680308296707633
Iteration time: 5019.0
Weighted AreaUnderPRC: 0.7177529245365225
Mean absolute error: 0.07118414201182667
Coverage of cases: 99.96
Instances selection time: 21.0
Test time: 537.0
Accumulative iteration time: 104592.0
Weighted Recall: 0.7802
Weighted FalsePositiveRate: 0.008761939293089794
Kappa statistic: 0.7713967446730033
Training time: 4998.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 79.35307692307639
Incorrectly Classified Instances: 20.25
Correctly Classified Instances: 79.75
Weighted Precision: 0.8100815687811903
Weighted AreaUnderROC: 0.975316749455768
Root mean squared error: 0.18622054846827507
Relative absolute error: 96.2128000000015
Root relative squared error: 96.83468520350979
Weighted TruePositiveRate: 0.7975
Weighted MatthewsCorrelation: 0.7916825938678932
Weighted FMeasure: 0.7952634750755654
Iteration time: 5173.0
Weighted AreaUnderPRC: 0.7314785131676226
Mean absolute error: 0.07116331360945864
Coverage of cases: 99.98
Instances selection time: 9.0
Test time: 546.0
Accumulative iteration time: 109765.0
Weighted Recall: 0.7975
Weighted FalsePositiveRate: 0.008073520278542275
Kappa statistic: 0.7893913478595962
Training time: 5164.0
		
Time end:Sun Oct 08 01.26.25 EEST 2017