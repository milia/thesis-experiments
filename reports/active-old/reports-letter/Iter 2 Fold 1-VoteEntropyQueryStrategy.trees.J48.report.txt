Sun Oct 08 02.38.58 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 02.38.58 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 5.578076923077598
Incorrectly Classified Instances: 29.53
Correctly Classified Instances: 70.47
Weighted Precision: 0.7098097361141125
Weighted AreaUnderROC: 0.8718071113652213
Root mean squared error: 0.13998509765773529
Relative absolute error: 33.31686463916285
Root relative squared error: 72.79225078202742
Weighted TruePositiveRate: 0.7047
Weighted MatthewsCorrelation: 0.6945788726261243
Weighted FMeasure: 0.7053059864712412
Iteration time: 169.0
Weighted AreaUnderPRC: 0.6085059706999912
Mean absolute error: 0.024642651360324112
Coverage of cases: 75.35
Instances selection time: 45.0
Test time: 177.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7047
Weighted FalsePositiveRate: 0.01179429446215369
Kappa statistic: 0.6928787463137779
Training time: 124.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 5.651923076923778
Incorrectly Classified Instances: 28.37
Correctly Classified Instances: 71.63
Weighted Precision: 0.7264790246594692
Weighted AreaUnderROC: 0.8753297835032512
Root mean squared error: 0.13793672153531306
Relative absolute error: 31.93038014678364
Root relative squared error: 71.72709519836779
Weighted TruePositiveRate: 0.7163
Weighted MatthewsCorrelation: 0.7084954843065103
Weighted FMeasure: 0.7178970390204277
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6070538881928066
Mean absolute error: 0.023617145078978687
Coverage of cases: 76.15
Instances selection time: 43.0
Test time: 179.0
Accumulative iteration time: 366.0
Weighted Recall: 0.7163
Weighted FalsePositiveRate: 0.011286400625286424
Kappa statistic: 0.7049597963783727
Training time: 154.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 5.7065384615391626
Incorrectly Classified Instances: 28.32
Correctly Classified Instances: 71.68
Weighted Precision: 0.7245712819230914
Weighted AreaUnderROC: 0.8784155726024053
Root mean squared error: 0.13742025150571943
Relative absolute error: 32.01724439178761
Root relative squared error: 71.45853078297908
Weighted TruePositiveRate: 0.7168
Weighted MatthewsCorrelation: 0.7075967164249665
Weighted FMeasure: 0.7167635504952242
Iteration time: 215.0
Weighted AreaUnderPRC: 0.6125903853307437
Mean absolute error: 0.02368139378090469
Coverage of cases: 76.49
Instances selection time: 41.0
Test time: 197.0
Accumulative iteration time: 581.0
Weighted Recall: 0.7168
Weighted FalsePositiveRate: 0.011337240077731313
Kappa statistic: 0.7054535953268133
Training time: 174.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 5.55807692307761
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7271724885686119
Weighted AreaUnderROC: 0.8811785768929307
Root mean squared error: 0.13648740632014325
Relative absolute error: 31.4753342185179
Root relative squared error: 70.97345128647945
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.710379365925803
Weighted FMeasure: 0.7193973396159143
Iteration time: 256.0
Weighted AreaUnderPRC: 0.6196658214153252
Mean absolute error: 0.023280572646829516
Coverage of cases: 76.75
Instances selection time: 42.0
Test time: 181.0
Accumulative iteration time: 837.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.011208933067936322
Kappa statistic: 0.7083720528195623
Training time: 214.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 5.522307692308286
Incorrectly Classified Instances: 27.49
Correctly Classified Instances: 72.51
Weighted Precision: 0.7354903226652492
Weighted AreaUnderROC: 0.8816093778715324
Root mean squared error: 0.13566557723663847
Relative absolute error: 31.305693085138053
Root relative squared error: 70.54610016305692
Weighted TruePositiveRate: 0.7251
Weighted MatthewsCorrelation: 0.7169254319971157
Weighted FMeasure: 0.7250873762656291
Iteration time: 272.0
Weighted AreaUnderPRC: 0.6336994012306618
Mean absolute error: 0.023155098435749765
Coverage of cases: 76.66
Instances selection time: 40.0
Test time: 180.0
Accumulative iteration time: 1109.0
Weighted Recall: 0.7251
Weighted FalsePositiveRate: 0.01104217202259245
Kappa statistic: 0.7140763628499719
Training time: 232.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 5.441923076923717
Incorrectly Classified Instances: 26.55
Correctly Classified Instances: 73.45
Weighted Precision: 0.747201759008438
Weighted AreaUnderROC: 0.8888549519359336
Root mean squared error: 0.1335248187975483
Relative absolute error: 29.990029604815557
Root relative squared error: 69.43290577472996
Weighted TruePositiveRate: 0.7345
Weighted MatthewsCorrelation: 0.7271111985700495
Weighted FMeasure: 0.7340070061881043
Iteration time: 290.0
Weighted AreaUnderPRC: 0.6412186732563799
Mean absolute error: 0.022181974559771726
Coverage of cases: 78.19
Instances selection time: 39.0
Test time: 188.0
Accumulative iteration time: 1399.0
Weighted Recall: 0.7345
Weighted FalsePositiveRate: 0.010663442942012255
Kappa statistic: 0.7238546491940472
Training time: 251.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 5.408461538462175
Incorrectly Classified Instances: 27.03
Correctly Classified Instances: 72.97
Weighted Precision: 0.7428471981239831
Weighted AreaUnderROC: 0.8865194285630943
Root mean squared error: 0.13420740524954444
Relative absolute error: 30.139828015538896
Root relative squared error: 69.78785072976798
Weighted TruePositiveRate: 0.7297
Weighted MatthewsCorrelation: 0.7221885613641389
Weighted FMeasure: 0.7290334246227463
Iteration time: 302.0
Weighted AreaUnderPRC: 0.6398561204430461
Mean absolute error: 0.022292772200839268
Coverage of cases: 77.85
Instances selection time: 35.0
Test time: 178.0
Accumulative iteration time: 1701.0
Weighted Recall: 0.7297
Weighted FalsePositiveRate: 0.010798540282999924
Kappa statistic: 0.7188811143864634
Training time: 267.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 5.303076923077538
Incorrectly Classified Instances: 27.26
Correctly Classified Instances: 72.74
Weighted Precision: 0.7387962186796276
Weighted AreaUnderROC: 0.8876980209651356
Root mean squared error: 0.13489613917049983
Relative absolute error: 30.234145581337895
Root relative squared error: 70.1459923686648
Weighted TruePositiveRate: 0.7274
Weighted MatthewsCorrelation: 0.7190152598219809
Weighted FMeasure: 0.7260718627820218
Iteration time: 340.0
Weighted AreaUnderPRC: 0.6425509414251982
Mean absolute error: 0.022362533714004196
Coverage of cases: 77.98
Instances selection time: 35.0
Test time: 179.0
Accumulative iteration time: 2041.0
Weighted Recall: 0.7274
Weighted FalsePositiveRate: 0.010896514078007532
Kappa statistic: 0.7164901762546046
Training time: 305.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 5.476153846154488
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7428894692874041
Weighted AreaUnderROC: 0.8865927700093807
Root mean squared error: 0.1341199551441203
Relative absolute error: 30.232041774990623
Root relative squared error: 69.74237667494742
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.7220864964099145
Weighted FMeasure: 0.7285658290028417
Iteration time: 362.0
Weighted AreaUnderPRC: 0.6501870391602202
Mean absolute error: 0.022360977644220712
Coverage of cases: 77.79
Instances selection time: 34.0
Test time: 177.0
Accumulative iteration time: 2403.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.01078606651312076
Kappa statistic: 0.7191968017638933
Training time: 328.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 5.370384615385242
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7456644156074355
Weighted AreaUnderROC: 0.8891138692101697
Root mean squared error: 0.13604536462079686
Relative absolute error: 30.439405732526374
Root relative squared error: 70.7435896028193
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.7194950909865271
Weighted FMeasure: 0.7237406320698354
Iteration time: 386.0
Weighted AreaUnderPRC: 0.6352419696239937
Mean absolute error: 0.022514353352457196
Coverage of cases: 77.51
Instances selection time: 32.0
Test time: 175.0
Accumulative iteration time: 2789.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.010921867394813394
Kappa statistic: 0.7154563132256905
Training time: 354.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 5.516538461539111
Incorrectly Classified Instances: 26.61
Correctly Classified Instances: 73.39
Weighted Precision: 0.7502794616383243
Weighted AreaUnderROC: 0.8922554876492373
Root mean squared error: 0.13455601137972345
Relative absolute error: 29.828158891090993
Root relative squared error: 69.96912591746107
Weighted TruePositiveRate: 0.7339
Weighted MatthewsCorrelation: 0.7263373655477918
Weighted FMeasure: 0.7308115437666772
Iteration time: 411.0
Weighted AreaUnderPRC: 0.6419419175054261
Mean absolute error: 0.02206224770050801
Coverage of cases: 78.33
Instances selection time: 30.0
Test time: 173.0
Accumulative iteration time: 3200.0
Weighted Recall: 0.7339
Weighted FalsePositiveRate: 0.010604037334200114
Kappa statistic: 0.7232571594418048
Training time: 381.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 4.986538461539087
Incorrectly Classified Instances: 25.81
Correctly Classified Instances: 74.19
Weighted Precision: 0.7604586200576818
Weighted AreaUnderROC: 0.8944035886055983
Root mean squared error: 0.13327179909656148
Relative absolute error: 28.587496741993842
Root relative squared error: 69.3013355302168
Weighted TruePositiveRate: 0.7419
Weighted MatthewsCorrelation: 0.736046872307575
Weighted FMeasure: 0.7405640092360347
Iteration time: 456.0
Weighted AreaUnderPRC: 0.6432250601558365
Mean absolute error: 0.021144598181945157
Coverage of cases: 78.19
Instances selection time: 28.0
Test time: 194.0
Accumulative iteration time: 3656.0
Weighted Recall: 0.7419
Weighted FalsePositiveRate: 0.010271564645561091
Kappa statistic: 0.7315862778835163
Training time: 428.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 5.394230769231443
Incorrectly Classified Instances: 24.63
Correctly Classified Instances: 75.37
Weighted Precision: 0.773376967845418
Weighted AreaUnderROC: 0.898630979107305
Root mean squared error: 0.12977167023704517
Relative absolute error: 28.16328881427097
Root relative squared error: 67.4812685232682
Weighted TruePositiveRate: 0.7537
Weighted MatthewsCorrelation: 0.7485249537107356
Weighted FMeasure: 0.7521909068423882
Iteration time: 481.0
Weighted AreaUnderPRC: 0.6604944444650668
Mean absolute error: 0.020830834921795147
Coverage of cases: 79.66
Instances selection time: 26.0
Test time: 176.0
Accumulative iteration time: 4137.0
Weighted Recall: 0.7537
Weighted FalsePositiveRate: 0.00980031410453317
Kappa statistic: 0.7438563828633868
Training time: 455.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 5.308461538462212
Incorrectly Classified Instances: 23.75
Correctly Classified Instances: 76.25
Weighted Precision: 0.7793335110844815
Weighted AreaUnderROC: 0.901148920225082
Root mean squared error: 0.12686973958113915
Relative absolute error: 26.923552860936315
Root relative squared error: 65.97226458219697
Weighted TruePositiveRate: 0.7625
Weighted MatthewsCorrelation: 0.7570932256569404
Weighted FMeasure: 0.7614072164156361
Iteration time: 522.0
Weighted AreaUnderPRC: 0.6734254296409374
Mean absolute error: 0.019913870459269643
Coverage of cases: 80.03
Instances selection time: 25.0
Test time: 180.0
Accumulative iteration time: 4659.0
Weighted Recall: 0.7625
Weighted FalsePositiveRate: 0.009448715144621162
Kappa statistic: 0.75300912621399
Training time: 497.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 5.298461538462205
Incorrectly Classified Instances: 23.69
Correctly Classified Instances: 76.31
Weighted Precision: 0.7777976264840953
Weighted AreaUnderROC: 0.9036038708866139
Root mean squared error: 0.1260023285926607
Relative absolute error: 26.687491592409096
Root relative squared error: 65.52121086818813
Weighted TruePositiveRate: 0.7631
Weighted MatthewsCorrelation: 0.7562093065885036
Weighted FMeasure: 0.760069494880971
Iteration time: 563.0
Weighted AreaUnderPRC: 0.6791805112873891
Mean absolute error: 0.019739268929293913
Coverage of cases: 80.78
Instances selection time: 25.0
Test time: 172.0
Accumulative iteration time: 5222.0
Weighted Recall: 0.7631
Weighted FalsePositiveRate: 0.009440490693231031
Kappa statistic: 0.7536320475598929
Training time: 538.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 5.0876923076929454
Incorrectly Classified Instances: 23.12
Correctly Classified Instances: 76.88
Weighted Precision: 0.7822508124966343
Weighted AreaUnderROC: 0.9061138866201583
Root mean squared error: 0.12544286118144612
Relative absolute error: 26.02941706108697
Root relative squared error: 65.23028781435653
Weighted TruePositiveRate: 0.7688
Weighted MatthewsCorrelation: 0.7626014801010021
Weighted FMeasure: 0.7674698272875635
Iteration time: 598.0
Weighted AreaUnderPRC: 0.6838114145965188
Mean absolute error: 0.01925252741204389
Coverage of cases: 80.61
Instances selection time: 22.0
Test time: 176.0
Accumulative iteration time: 5820.0
Weighted Recall: 0.7688
Weighted FalsePositiveRate: 0.009218820033009834
Kappa statistic: 0.7595500390824788
Training time: 576.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 5.026923076923695
Incorrectly Classified Instances: 22.31
Correctly Classified Instances: 77.69
Weighted Precision: 0.78887778867954
Weighted AreaUnderROC: 0.9107314773342797
Root mean squared error: 0.12296563467193092
Relative absolute error: 24.91218774273001
Root relative squared error: 63.942130029408546
Weighted TruePositiveRate: 0.7769
Weighted MatthewsCorrelation: 0.7702017613196069
Weighted FMeasure: 0.7746621459556705
Iteration time: 620.0
Weighted AreaUnderPRC: 0.6982132258245473
Mean absolute error: 0.01842617436592199
Coverage of cases: 81.57
Instances selection time: 20.0
Test time: 178.0
Accumulative iteration time: 6440.0
Weighted Recall: 0.7769
Weighted FalsePositiveRate: 0.008905152329732068
Kappa statistic: 0.7679702588417087
Training time: 600.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 5.147307692308288
Incorrectly Classified Instances: 21.75
Correctly Classified Instances: 78.25
Weighted Precision: 0.7942934084061306
Weighted AreaUnderROC: 0.9136745274132562
Root mean squared error: 0.12066660420484038
Relative absolute error: 24.146640188783
Root relative squared error: 62.746634186521376
Weighted TruePositiveRate: 0.7825
Weighted MatthewsCorrelation: 0.7765797331381433
Weighted FMeasure: 0.7815802118470078
Iteration time: 655.0
Weighted AreaUnderPRC: 0.7099893563650599
Mean absolute error: 0.01785994096803227
Coverage of cases: 82.69
Instances selection time: 18.0
Test time: 181.0
Accumulative iteration time: 7095.0
Weighted Recall: 0.7825
Weighted FalsePositiveRate: 0.008700289765636708
Kappa statistic: 0.7737884389605076
Training time: 637.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 5.32500000000063
Incorrectly Classified Instances: 21.39
Correctly Classified Instances: 78.61
Weighted Precision: 0.7975502892417066
Weighted AreaUnderROC: 0.9136259514183439
Root mean squared error: 0.12033970476936824
Relative absolute error: 24.246768175792315
Root relative squared error: 62.57664648007585
Weighted TruePositiveRate: 0.7861
Weighted MatthewsCorrelation: 0.7802139095098315
Weighted FMeasure: 0.7851399239457165
Iteration time: 676.0
Weighted AreaUnderPRC: 0.7063830575583703
Mean absolute error: 0.0179340001300214
Coverage of cases: 82.86
Instances selection time: 16.0
Test time: 180.0
Accumulative iteration time: 7771.0
Weighted Recall: 0.7861
Weighted FalsePositiveRate: 0.008553252737822684
Kappa statistic: 0.7775327922800489
Training time: 660.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 5.111153846154443
Incorrectly Classified Instances: 22.12
Correctly Classified Instances: 77.88
Weighted Precision: 0.7914011508450955
Weighted AreaUnderROC: 0.90957285005006
Root mean squared error: 0.1224269789781066
Relative absolute error: 24.80783778742833
Root relative squared error: 63.66202906861987
Weighted TruePositiveRate: 0.7788
Weighted MatthewsCorrelation: 0.7731074438389186
Weighted FMeasure: 0.7782412908327696
Iteration time: 708.0
Weighted AreaUnderPRC: 0.6933370870392126
Mean absolute error: 0.01834899244632017
Coverage of cases: 81.65
Instances selection time: 15.0
Test time: 185.0
Accumulative iteration time: 8479.0
Weighted Recall: 0.7788
Weighted FalsePositiveRate: 0.0088413567176082
Kappa statistic: 0.7699435155168524
Training time: 693.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 5.070769230769881
Incorrectly Classified Instances: 21.09
Correctly Classified Instances: 78.91
Weighted Precision: 0.8027247012005182
Weighted AreaUnderROC: 0.9145213445101488
Root mean squared error: 0.11998250451360351
Relative absolute error: 24.03568964570173
Root relative squared error: 62.390902347078175
Weighted TruePositiveRate: 0.7891
Weighted MatthewsCorrelation: 0.7839150062091051
Weighted FMeasure: 0.7882323525236598
Iteration time: 746.0
Weighted AreaUnderPRC: 0.69875423701872
Mean absolute error: 0.017777876956877495
Coverage of cases: 82.67
Instances selection time: 17.0
Test time: 179.0
Accumulative iteration time: 9225.0
Weighted Recall: 0.7891
Weighted FalsePositiveRate: 0.00842056062389455
Kappa statistic: 0.780657514305909
Training time: 729.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 4.933076923077531
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8152231756031518
Weighted AreaUnderROC: 0.9203297954251306
Root mean squared error: 0.11554692623928912
Relative absolute error: 22.492001600064324
Root relative squared error: 60.08440164443454
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7995821485395928
Weighted FMeasure: 0.8046093497812068
Iteration time: 816.0
Weighted AreaUnderPRC: 0.7265485383488389
Mean absolute error: 0.01663609585803342
Coverage of cases: 83.62
Instances selection time: 13.0
Test time: 176.0
Accumulative iteration time: 10041.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.0078115575591276324
Kappa statistic: 0.7965685609191356
Training time: 803.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 4.903846153846771
Incorrectly Classified Instances: 18.41
Correctly Classified Instances: 81.59
Weighted Precision: 0.8256768474394958
Weighted AreaUnderROC: 0.9252279420990777
Root mean squared error: 0.11197867147743794
Relative absolute error: 21.029662029604474
Root relative squared error: 58.2289091682718
Weighted TruePositiveRate: 0.8159
Weighted MatthewsCorrelation: 0.811289180439111
Weighted FMeasure: 0.8161627798958081
Iteration time: 821.0
Weighted AreaUnderPRC: 0.7460877891023768
Mean absolute error: 0.015554483749705281
Coverage of cases: 84.68
Instances selection time: 9.0
Test time: 179.0
Accumulative iteration time: 10862.0
Weighted Recall: 0.8159
Weighted FalsePositiveRate: 0.007370616386705342
Kappa statistic: 0.8085247964028871
Training time: 812.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 5.097307692308317
Incorrectly Classified Instances: 19.23
Correctly Classified Instances: 80.77
Weighted Precision: 0.8157422112181428
Weighted AreaUnderROC: 0.9263853184053454
Root mean squared error: 0.1135706093450106
Relative absolute error: 22.13482595671978
Root relative squared error: 59.05671685940963
Weighted TruePositiveRate: 0.8077
Weighted MatthewsCorrelation: 0.8015025905771878
Weighted FMeasure: 0.8062738690811135
Iteration time: 871.0
Weighted AreaUnderPRC: 0.7459306595890639
Mean absolute error: 0.016371912689879208
Coverage of cases: 84.74
Instances selection time: 8.0
Test time: 171.0
Accumulative iteration time: 11733.0
Weighted Recall: 0.8077
Weighted FalsePositiveRate: 0.00769069432056735
Kappa statistic: 0.7999964242730455
Training time: 863.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 4.956923076923676
Incorrectly Classified Instances: 18.17
Correctly Classified Instances: 81.83
Weighted Precision: 0.824384055038069
Weighted AreaUnderROC: 0.9289472174647819
Root mean squared error: 0.11059151598644411
Relative absolute error: 20.939580268727237
Root relative squared error: 57.50758831295495
Weighted TruePositiveRate: 0.8183
Weighted MatthewsCorrelation: 0.8128912753080773
Weighted FMeasure: 0.8187587773015366
Iteration time: 922.0
Weighted AreaUnderPRC: 0.7547529517079348
Mean absolute error: 0.015487855228346387
Coverage of cases: 85.67
Instances selection time: 5.0
Test time: 182.0
Accumulative iteration time: 12655.0
Weighted Recall: 0.8183
Weighted FalsePositiveRate: 0.007256409920366699
Kappa statistic: 0.8110222652277442
Training time: 917.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 4.963076923077529
Incorrectly Classified Instances: 16.91
Correctly Classified Instances: 83.09
Weighted Precision: 0.8350524195036058
Weighted AreaUnderROC: 0.9326582947726569
Root mean squared error: 0.1065080228819676
Relative absolute error: 19.56124165213596
Root relative squared error: 55.384171898627024
Weighted TruePositiveRate: 0.8309
Weighted MatthewsCorrelation: 0.8256621742093024
Weighted FMeasure: 0.8317765639157835
Iteration time: 951.0
Weighted AreaUnderPRC: 0.7690493020032773
Mean absolute error: 0.014468374003057121
Coverage of cases: 86.3
Instances selection time: 3.0
Test time: 186.0
Accumulative iteration time: 13606.0
Weighted Recall: 0.8309
Weighted FalsePositiveRate: 0.006773144241500401
Kappa statistic: 0.8241264244576554
Training time: 948.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 4.886923076923676
Incorrectly Classified Instances: 14.57
Correctly Classified Instances: 85.43
Weighted Precision: 0.8554012166299831
Weighted AreaUnderROC: 0.9424784157204349
Root mean squared error: 0.09957644307466988
Relative absolute error: 17.333328937726026
Root relative squared error: 51.77975039883196
Weighted TruePositiveRate: 0.8543
Weighted MatthewsCorrelation: 0.8488787848587128
Weighted FMeasure: 0.8545186334213662
Iteration time: 1002.0
Weighted AreaUnderPRC: 0.7976132999811215
Mean absolute error: 0.012820509569322194
Coverage of cases: 88.47
Instances selection time: 2.0
Test time: 185.0
Accumulative iteration time: 14608.0
Weighted Recall: 0.8543
Weighted FalsePositiveRate: 0.0058252652572914594
Kappa statistic: 0.848465204209934
Training time: 1000.0
		
Time end:Sun Oct 08 02.39.25 EEST 2017