Sat Oct 07 16.11.29 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 16.11.29 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 11.858461538461638
Incorrectly Classified Instances: 38.56
Correctly Classified Instances: 61.44
Weighted Precision: 0.6305109624059418
Weighted AreaUnderROC: 0.9517896450422475
Root mean squared error: 0.14377520277606068
Relative absolute error: 45.36802614815301
Root relative squared error: 74.76310544355677
Weighted TruePositiveRate: 0.6144
Weighted MatthewsCorrelation: 0.6026877157217193
Weighted FMeasure: 0.6127475470039417
Iteration time: 3038.0
Weighted AreaUnderPRC: 0.6670629123842147
Mean absolute error: 0.033556232358096655
Coverage of cases: 85.9
Instances selection time: 3029.0
Test time: 2135.0
Accumulative iteration time: 3038.0
Weighted Recall: 0.6144
Weighted FalsePositiveRate: 0.015372670343938006
Kappa statistic: 0.5989801824762888
Training time: 9.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 11.639230769230986
Incorrectly Classified Instances: 38.69
Correctly Classified Instances: 61.31
Weighted Precision: 0.6328134139974189
Weighted AreaUnderROC: 0.9513986611321514
Root mean squared error: 0.14436325753653875
Relative absolute error: 45.32571391155571
Root relative squared error: 75.06889391900539
Weighted TruePositiveRate: 0.6131
Weighted MatthewsCorrelation: 0.6017907995818395
Weighted FMeasure: 0.6102226198612298
Iteration time: 2707.0
Weighted AreaUnderPRC: 0.6675790381553761
Mean absolute error: 0.03352493632511049
Coverage of cases: 85.63
Instances selection time: 2698.0
Test time: 2119.0
Accumulative iteration time: 5745.0
Weighted Recall: 0.6131
Weighted FalsePositiveRate: 0.015408488829585235
Kappa statistic: 0.597635452582586
Training time: 9.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 11.551923076923297
Incorrectly Classified Instances: 39.06
Correctly Classified Instances: 60.94
Weighted Precision: 0.6314169586256089
Weighted AreaUnderROC: 0.950999774641604
Root mean squared error: 0.14586809121037053
Relative absolute error: 45.70180509158233
Root relative squared error: 75.85140742939797
Weighted TruePositiveRate: 0.6094
Weighted MatthewsCorrelation: 0.5985836913863767
Weighted FMeasure: 0.6065504958529988
Iteration time: 2567.0
Weighted AreaUnderPRC: 0.6638256254305903
Mean absolute error: 0.033803110274834285
Coverage of cases: 84.7
Instances selection time: 2557.0
Test time: 2130.0
Accumulative iteration time: 8312.0
Weighted Recall: 0.6094
Weighted FalsePositiveRate: 0.015552820562710477
Kappa statistic: 0.5937889097134182
Training time: 10.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 11.547307692307921
Incorrectly Classified Instances: 39.18
Correctly Classified Instances: 60.82
Weighted Precision: 0.6301756758424605
Weighted AreaUnderROC: 0.9504335158319225
Root mean squared error: 0.14623025953876492
Relative absolute error: 45.81202973039886
Root relative squared error: 76.03973496016307
Weighted TruePositiveRate: 0.6082
Weighted MatthewsCorrelation: 0.5971608457202885
Weighted FMeasure: 0.6050152289738099
Iteration time: 2469.0
Weighted AreaUnderPRC: 0.6630828267746599
Mean absolute error: 0.03388463737455064
Coverage of cases: 84.53
Instances selection time: 2458.0
Test time: 2119.0
Accumulative iteration time: 10781.0
Weighted Recall: 0.6082
Weighted FalsePositiveRate: 0.01559527044554226
Kappa statistic: 0.5925447418476112
Training time: 11.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 11.635384615384812
Incorrectly Classified Instances: 39.11
Correctly Classified Instances: 60.89
Weighted Precision: 0.633565216767929
Weighted AreaUnderROC: 0.9515112064523151
Root mean squared error: 0.14573447769827697
Relative absolute error: 45.58847517613614
Root relative squared error: 75.7819284031093
Weighted TruePositiveRate: 0.6089
Weighted MatthewsCorrelation: 0.5980286174937067
Weighted FMeasure: 0.6044070359630886
Iteration time: 2352.0
Weighted AreaUnderPRC: 0.6693922759807919
Mean absolute error: 0.03371928637287706
Coverage of cases: 84.97
Instances selection time: 2339.0
Test time: 2125.0
Accumulative iteration time: 13133.0
Weighted Recall: 0.6089
Weighted FalsePositiveRate: 0.015590477853726636
Kappa statistic: 0.5932617818651184
Training time: 13.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 11.539615384615722
Incorrectly Classified Instances: 38.84
Correctly Classified Instances: 61.16
Weighted Precision: 0.6429495558669283
Weighted AreaUnderROC: 0.9519468069485624
Root mean squared error: 0.14512114743057344
Relative absolute error: 45.210695031205034
Root relative squared error: 75.46299666390345
Weighted TruePositiveRate: 0.6116
Weighted MatthewsCorrelation: 0.6019608508869148
Weighted FMeasure: 0.6061189866316654
Iteration time: 2278.0
Weighted AreaUnderPRC: 0.6739090266191498
Mean absolute error: 0.033439863188756455
Coverage of cases: 85.18
Instances selection time: 2265.0
Test time: 2121.0
Accumulative iteration time: 15411.0
Weighted Recall: 0.6116
Weighted FalsePositiveRate: 0.01550776030121373
Kappa statistic: 0.5960572273338964
Training time: 13.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 11.51961538461574
Incorrectly Classified Instances: 38.58
Correctly Classified Instances: 61.42
Weighted Precision: 0.6450909044448905
Weighted AreaUnderROC: 0.9514195543846243
Root mean squared error: 0.1447969438021051
Relative absolute error: 45.02855262920865
Root relative squared error: 75.2944107770999
Weighted TruePositiveRate: 0.6142
Weighted MatthewsCorrelation: 0.6040740552908881
Weighted FMeasure: 0.6078337128728459
Iteration time: 2149.0
Weighted AreaUnderPRC: 0.6736408826331235
Mean absolute error: 0.03330514247722068
Coverage of cases: 85.17
Instances selection time: 2134.0
Test time: 2098.0
Accumulative iteration time: 17560.0
Weighted Recall: 0.6142
Weighted FalsePositiveRate: 0.015393062059687426
Kappa statistic: 0.5987666682263252
Training time: 15.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 11.346923076923522
Incorrectly Classified Instances: 38.76
Correctly Classified Instances: 61.24
Weighted Precision: 0.6397069541057031
Weighted AreaUnderROC: 0.9533412986592721
Root mean squared error: 0.14457761660133353
Relative absolute error: 44.84068781108023
Root relative squared error: 75.1803606326987
Weighted TruePositiveRate: 0.6124
Weighted MatthewsCorrelation: 0.6006462606391829
Weighted FMeasure: 0.6046919380681757
Iteration time: 2045.0
Weighted AreaUnderPRC: 0.6785894877748685
Mean absolute error: 0.03316618920937424
Coverage of cases: 85.39
Instances selection time: 2029.0
Test time: 2066.0
Accumulative iteration time: 19605.0
Weighted Recall: 0.6124
Weighted FalsePositiveRate: 0.01548998015770368
Kappa statistic: 0.5968813048723354
Training time: 16.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 11.208461538462046
Incorrectly Classified Instances: 40.14
Correctly Classified Instances: 59.86
Weighted Precision: 0.6312880725260649
Weighted AreaUnderROC: 0.9536381915085546
Root mean squared error: 0.1469595908863835
Relative absolute error: 45.57593342552377
Root relative squared error: 76.41898726092475
Weighted TruePositiveRate: 0.5986
Weighted MatthewsCorrelation: 0.5858276840337948
Weighted FMeasure: 0.5868906064792047
Iteration time: 1945.0
Weighted AreaUnderPRC: 0.6738573217900151
Mean absolute error: 0.03371000993011643
Coverage of cases: 85.22
Instances selection time: 1928.0
Test time: 2089.0
Accumulative iteration time: 21550.0
Weighted Recall: 0.5986
Weighted FalsePositiveRate: 0.0160034196372212
Kappa statistic: 0.5825500079402858
Training time: 17.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 11.245384615385136
Incorrectly Classified Instances: 39.74
Correctly Classified Instances: 60.26
Weighted Precision: 0.6451493956113739
Weighted AreaUnderROC: 0.9536373909398628
Root mean squared error: 0.14659873850802116
Relative absolute error: 45.5511578613921
Root relative squared error: 76.23134402417632
Weighted TruePositiveRate: 0.6026
Weighted MatthewsCorrelation: 0.5938045917785331
Weighted FMeasure: 0.5935566252808534
Iteration time: 1854.0
Weighted AreaUnderPRC: 0.6792207796518107
Mean absolute error: 0.033691684808717266
Coverage of cases: 85.06
Instances selection time: 1836.0
Test time: 2100.0
Accumulative iteration time: 23404.0
Weighted Recall: 0.6026
Weighted FalsePositiveRate: 0.015797179529320252
Kappa statistic: 0.5867358901036099
Training time: 18.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 11.516153846154216
Incorrectly Classified Instances: 38.98
Correctly Classified Instances: 61.02
Weighted Precision: 0.6571948988822971
Weighted AreaUnderROC: 0.9540688875369108
Root mean squared error: 0.1454357854692969
Relative absolute error: 45.4553880359867
Root relative squared error: 75.62660844403966
Weighted TruePositiveRate: 0.6102
Weighted MatthewsCorrelation: 0.6032541633148684
Weighted FMeasure: 0.6023612108371588
Iteration time: 1764.0
Weighted AreaUnderPRC: 0.6807837464662315
Mean absolute error: 0.033620849139038726
Coverage of cases: 85.32
Instances selection time: 1742.0
Test time: 2107.0
Accumulative iteration time: 25168.0
Weighted Recall: 0.6102
Weighted FalsePositiveRate: 0.015505220734471368
Kappa statistic: 0.594633724219568
Training time: 22.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 11.42384615384658
Incorrectly Classified Instances: 38.34
Correctly Classified Instances: 61.66
Weighted Precision: 0.6680734907656783
Weighted AreaUnderROC: 0.954271481248374
Root mean squared error: 0.14505706080869976
Relative absolute error: 45.227266249269924
Root relative squared error: 75.42967162052913
Weighted TruePositiveRate: 0.6166
Weighted MatthewsCorrelation: 0.6114922752356291
Weighted FMeasure: 0.6098187275843087
Iteration time: 1707.0
Weighted AreaUnderPRC: 0.6807610609155624
Mean absolute error: 0.033452120006851785
Coverage of cases: 85.21
Instances selection time: 1655.0
Test time: 2076.0
Accumulative iteration time: 26875.0
Weighted Recall: 0.6166
Weighted FalsePositiveRate: 0.01529429634762846
Kappa statistic: 0.6012654797835515
Training time: 52.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 11.46769230769271
Incorrectly Classified Instances: 38.12
Correctly Classified Instances: 61.88
Weighted Precision: 0.6696731144865282
Weighted AreaUnderROC: 0.9541440829127162
Root mean squared error: 0.14482943754847905
Relative absolute error: 45.12247281200031
Root relative squared error: 75.31130752521436
Weighted TruePositiveRate: 0.6188
Weighted MatthewsCorrelation: 0.61341772661591
Weighted FMeasure: 0.6115869410728066
Iteration time: 1542.0
Weighted AreaUnderPRC: 0.6809133987992184
Mean absolute error: 0.03337461006804291
Coverage of cases: 85.19
Instances selection time: 1520.0
Test time: 2057.0
Accumulative iteration time: 28417.0
Weighted Recall: 0.6188
Weighted FalsePositiveRate: 0.015215501928382412
Kappa statistic: 0.6035487709840299
Training time: 22.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 11.48192307692349
Incorrectly Classified Instances: 37.63
Correctly Classified Instances: 62.37
Weighted Precision: 0.6720063876087891
Weighted AreaUnderROC: 0.9560189040931606
Root mean squared error: 0.1438496871583671
Relative absolute error: 44.7677652729433
Root relative squared error: 74.8018373223561
Weighted TruePositiveRate: 0.6237
Weighted MatthewsCorrelation: 0.6174480620165528
Weighted FMeasure: 0.6157273657315502
Iteration time: 1437.0
Weighted AreaUnderPRC: 0.6862638493139601
Mean absolute error: 0.03311225242081143
Coverage of cases: 85.57
Instances selection time: 1414.0
Test time: 2090.0
Accumulative iteration time: 29854.0
Weighted Recall: 0.6237
Weighted FalsePositiveRate: 0.01505122332896839
Kappa statistic: 0.608626178561212
Training time: 23.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 11.384615384615831
Incorrectly Classified Instances: 38.31
Correctly Classified Instances: 61.69
Weighted Precision: 0.6700027223528033
Weighted AreaUnderROC: 0.9560153319936433
Root mean squared error: 0.14522320833227537
Relative absolute error: 45.08136928810043
Root relative squared error: 75.51606833278846
Weighted TruePositiveRate: 0.6169
Weighted MatthewsCorrelation: 0.6096827603119686
Weighted FMeasure: 0.6051897989918816
Iteration time: 1416.0
Weighted AreaUnderPRC: 0.6851725538972964
Mean absolute error: 0.03334420805332407
Coverage of cases: 85.26
Instances selection time: 1392.0
Test time: 2307.0
Accumulative iteration time: 31270.0
Weighted Recall: 0.6169
Weighted FalsePositiveRate: 0.01536162002237467
Kappa statistic: 0.6015382889461272
Training time: 24.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 11.560000000000413
Incorrectly Classified Instances: 38.26
Correctly Classified Instances: 61.74
Weighted Precision: 0.6720718220886434
Weighted AreaUnderROC: 0.956067218149311
Root mean squared error: 0.14499029168640787
Relative absolute error: 45.186556519591576
Root relative squared error: 75.39495167693735
Weighted TruePositiveRate: 0.6174
Weighted MatthewsCorrelation: 0.6104822905019559
Weighted FMeasure: 0.6060617351951694
Iteration time: 1281.0
Weighted AreaUnderPRC: 0.6856385926031966
Mean absolute error: 0.03342200926004828
Coverage of cases: 85.42
Instances selection time: 1254.0
Test time: 2435.0
Accumulative iteration time: 32551.0
Weighted Recall: 0.6174
Weighted FalsePositiveRate: 0.015320718777558855
Kappa statistic: 0.6020702166967925
Training time: 27.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 11.758076923077258
Incorrectly Classified Instances: 37.97
Correctly Classified Instances: 62.03
Weighted Precision: 0.6717001247807408
Weighted AreaUnderROC: 0.9568666895139105
Root mean squared error: 0.1441867887391796
Relative absolute error: 45.10500482989706
Root relative squared error: 74.97713014437862
Weighted TruePositiveRate: 0.6203
Weighted MatthewsCorrelation: 0.6124092501222632
Weighted FMeasure: 0.6086772506018007
Iteration time: 1194.0
Weighted AreaUnderPRC: 0.6890903174433096
Mean absolute error: 0.03336168996293696
Coverage of cases: 85.9
Instances selection time: 1164.0
Test time: 2173.0
Accumulative iteration time: 33745.0
Weighted Recall: 0.6203
Weighted FalsePositiveRate: 0.015186184620227625
Kappa statistic: 0.6050969472433962
Training time: 30.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 11.854230769231078
Incorrectly Classified Instances: 37.66
Correctly Classified Instances: 62.34
Weighted Precision: 0.6620819575558673
Weighted AreaUnderROC: 0.9569992192437363
Root mean squared error: 0.1429949796628719
Relative absolute error: 44.964633336476396
Root relative squared error: 74.35738942469858
Weighted TruePositiveRate: 0.6234
Weighted MatthewsCorrelation: 0.613608723978868
Weighted FMeasure: 0.6138965930849521
Iteration time: 1021.0
Weighted AreaUnderPRC: 0.6881935325710048
Mean absolute error: 0.03325786489383885
Coverage of cases: 86.18
Instances selection time: 993.0
Test time: 2081.0
Accumulative iteration time: 34766.0
Weighted Recall: 0.6234
Weighted FalsePositiveRate: 0.015059271956704189
Kappa statistic: 0.6083213797338055
Training time: 28.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 11.660769230769608
Incorrectly Classified Instances: 37.72
Correctly Classified Instances: 62.28
Weighted Precision: 0.6638988188299482
Weighted AreaUnderROC: 0.9578525962485637
Root mean squared error: 0.14306859515514378
Relative absolute error: 44.83625516393613
Root relative squared error: 74.39566948067996
Weighted TruePositiveRate: 0.6228
Weighted MatthewsCorrelation: 0.6134285858229156
Weighted FMeasure: 0.6130492340694488
Iteration time: 919.0
Weighted AreaUnderPRC: 0.689460937574096
Mean absolute error: 0.03316291062420849
Coverage of cases: 86.14
Instances selection time: 889.0
Test time: 2075.0
Accumulative iteration time: 35685.0
Weighted Recall: 0.6228
Weighted FalsePositiveRate: 0.01506816675779079
Kappa statistic: 0.607705263514785
Training time: 30.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 11.642692307692682
Incorrectly Classified Instances: 37.58
Correctly Classified Instances: 62.42
Weighted Precision: 0.663495360242701
Weighted AreaUnderROC: 0.9579820943003926
Root mean squared error: 0.14244837195271975
Relative absolute error: 44.655920981218074
Root relative squared error: 74.07315341541944
Weighted TruePositiveRate: 0.6242
Weighted MatthewsCorrelation: 0.6148310260341316
Weighted FMeasure: 0.6154868293490522
Iteration time: 821.0
Weighted AreaUnderPRC: 0.6882847924073273
Mean absolute error: 0.03302952735296734
Coverage of cases: 86.04
Instances selection time: 790.0
Test time: 2087.0
Accumulative iteration time: 36506.0
Weighted Recall: 0.6242
Weighted FalsePositiveRate: 0.01498720243475845
Kappa statistic: 0.6091735475469967
Training time: 31.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 11.560384615385065
Incorrectly Classified Instances: 37.0
Correctly Classified Instances: 63.0
Weighted Precision: 0.663454863094149
Weighted AreaUnderROC: 0.9582285561122315
Root mean squared error: 0.14158514711462758
Relative absolute error: 44.10302688756726
Root relative squared error: 73.62427649961148
Weighted TruePositiveRate: 0.63
Weighted MatthewsCorrelation: 0.6188938704197188
Weighted FMeasure: 0.6205314859593413
Iteration time: 722.0
Weighted AreaUnderPRC: 0.6898325963557357
Mean absolute error: 0.032620582017426854
Coverage of cases: 85.95
Instances selection time: 690.0
Test time: 2115.0
Accumulative iteration time: 37228.0
Weighted Recall: 0.63
Weighted FalsePositiveRate: 0.014742056396041548
Kappa statistic: 0.6152101039670059
Training time: 32.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 11.519230769231257
Incorrectly Classified Instances: 37.08
Correctly Classified Instances: 62.92
Weighted Precision: 0.6627354918505527
Weighted AreaUnderROC: 0.9584314840107325
Root mean squared error: 0.14172600349302295
Relative absolute error: 44.018596308048174
Root relative squared error: 73.69752181637708
Weighted TruePositiveRate: 0.6292
Weighted MatthewsCorrelation: 0.6177864932293955
Weighted FMeasure: 0.6190438917753508
Iteration time: 621.0
Weighted AreaUnderPRC: 0.6893514348809974
Mean absolute error: 0.03255813336393641
Coverage of cases: 85.92
Instances selection time: 588.0
Test time: 2088.0
Accumulative iteration time: 37849.0
Weighted Recall: 0.6292
Weighted FalsePositiveRate: 0.014774141007096767
Kappa statistic: 0.6143773317600733
Training time: 33.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 11.460000000000539
Incorrectly Classified Instances: 38.01
Correctly Classified Instances: 61.99
Weighted Precision: 0.653867555882933
Weighted AreaUnderROC: 0.957659712105345
Root mean squared error: 0.1434250414317127
Relative absolute error: 44.65599492270897
Root relative squared error: 74.58102154449581
Weighted TruePositiveRate: 0.6199
Weighted MatthewsCorrelation: 0.608216162871991
Weighted FMeasure: 0.609701825099917
Iteration time: 511.0
Weighted AreaUnderPRC: 0.6843483676270217
Mean absolute error: 0.03302958204341919
Coverage of cases: 85.42
Instances selection time: 476.0
Test time: 2086.0
Accumulative iteration time: 38360.0
Weighted Recall: 0.6199
Weighted FalsePositiveRate: 0.015141733720910022
Kappa statistic: 0.6047063510013752
Training time: 35.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 11.533846153846708
Incorrectly Classified Instances: 38.15
Correctly Classified Instances: 61.85
Weighted Precision: 0.6512277914359275
Weighted AreaUnderROC: 0.9576045753952956
Root mean squared error: 0.14317092684090313
Relative absolute error: 44.78121587911752
Root relative squared error: 74.44888195727482
Weighted TruePositiveRate: 0.6185
Weighted MatthewsCorrelation: 0.6079238538457993
Weighted FMeasure: 0.6108037879703619
Iteration time: 409.0
Weighted AreaUnderPRC: 0.6851271225755344
Mean absolute error: 0.03312220109401721
Coverage of cases: 86.01
Instances selection time: 373.0
Test time: 2107.0
Accumulative iteration time: 38769.0
Weighted Recall: 0.6185
Weighted FalsePositiveRate: 0.015192508093270954
Kappa statistic: 0.6032517961175978
Training time: 36.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 11.619230769231274
Incorrectly Classified Instances: 37.13
Correctly Classified Instances: 62.87
Weighted Precision: 0.657300314506132
Weighted AreaUnderROC: 0.9581672025210067
Root mean squared error: 0.14113391162008462
Relative absolute error: 44.20583501782357
Root relative squared error: 73.38963404244912
Weighted TruePositiveRate: 0.6287
Weighted MatthewsCorrelation: 0.6191091338860821
Weighted FMeasure: 0.624197118748737
Iteration time: 308.0
Weighted AreaUnderPRC: 0.6896274394233642
Mean absolute error: 0.03269662353388861
Coverage of cases: 86.66
Instances selection time: 271.0
Test time: 2103.0
Accumulative iteration time: 39077.0
Weighted Recall: 0.6287
Weighted FalsePositiveRate: 0.014791060702836884
Kappa statistic: 0.6138485816984965
Training time: 37.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 11.807692307692669
Incorrectly Classified Instances: 36.79
Correctly Classified Instances: 63.21
Weighted Precision: 0.6494435376569445
Weighted AreaUnderROC: 0.9582316860198496
Root mean squared error: 0.1403883440120578
Relative absolute error: 44.00970899310548
Root relative squared error: 73.00193888627514
Weighted TruePositiveRate: 0.6321
Weighted MatthewsCorrelation: 0.620262102765607
Weighted FMeasure: 0.6281095740783823
Iteration time: 209.0
Weighted AreaUnderPRC: 0.6892791845327675
Mean absolute error: 0.03255155990613856
Coverage of cases: 87.46
Instances selection time: 170.0
Test time: 2101.0
Accumulative iteration time: 39286.0
Weighted Recall: 0.6321
Weighted FalsePositiveRate: 0.01465667344748814
Kappa statistic: 0.6173810627109422
Training time: 39.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 12.029230769231054
Incorrectly Classified Instances: 36.71
Correctly Classified Instances: 63.29
Weighted Precision: 0.6463299631398735
Weighted AreaUnderROC: 0.9585201269278985
Root mean squared error: 0.1400170690555104
Relative absolute error: 44.16131946615422
Root relative squared error: 72.80887590887049
Weighted TruePositiveRate: 0.6329
Weighted MatthewsCorrelation: 0.6202871109483085
Weighted FMeasure: 0.6293043862489417
Iteration time: 109.0
Weighted AreaUnderPRC: 0.6940208289596834
Mean absolute error: 0.032663697829991165
Coverage of cases: 87.87
Instances selection time: 70.0
Test time: 2093.0
Accumulative iteration time: 39395.0
Weighted Recall: 0.6329
Weighted FalsePositiveRate: 0.014618231928141436
Kappa statistic: 0.6182159755658224
Training time: 39.0
		
Time end:Sat Oct 07 16.13.14 EEST 2017