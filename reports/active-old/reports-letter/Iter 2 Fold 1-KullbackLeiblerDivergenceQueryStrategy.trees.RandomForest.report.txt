Sat Oct 07 18.14.08 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 18.14.08 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 13.872307692307336
Incorrectly Classified Instances: 20.35
Correctly Classified Instances: 79.65
Weighted Precision: 0.809582639164437
Weighted AreaUnderROC: 0.9659051183957356
Root mean squared error: 0.11072314746526986
Relative absolute error: 38.84816000000884
Root relative squared error: 57.576036681944345
Weighted TruePositiveRate: 0.7965
Weighted MatthewsCorrelation: 0.7930158798281999
Weighted FMeasure: 0.7988608995358204
Iteration time: 404.0
Weighted AreaUnderPRC: 0.8397677631564328
Mean absolute error: 0.02873384615384868
Coverage of cases: 95.09
Instances selection time: 129.0
Test time: 229.0
Accumulative iteration time: 404.0
Weighted Recall: 0.7965
Weighted FalsePositiveRate: 0.008127950225523133
Kappa statistic: 0.7883528569935824
Training time: 275.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 13.520769230768966
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.8105262844706626
Weighted AreaUnderROC: 0.9651910769901096
Root mean squared error: 0.1102992433198141
Relative absolute error: 38.08792000000843
Root relative squared error: 57.35560652630733
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.7907975989621047
Weighted FMeasure: 0.7959106586117246
Iteration time: 454.0
Weighted AreaUnderPRC: 0.8415847915497505
Mean absolute error: 0.028171538461540763
Coverage of cases: 94.85
Instances selection time: 124.0
Test time: 226.0
Accumulative iteration time: 858.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.008245718141299678
Kappa statistic: 0.7845133398098584
Training time: 330.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 13.311153846153474
Incorrectly Classified Instances: 20.83
Correctly Classified Instances: 79.17
Weighted Precision: 0.8072998953725742
Weighted AreaUnderROC: 0.9638685509021568
Root mean squared error: 0.10989225492412145
Relative absolute error: 37.54608000000822
Root relative squared error: 57.14397256054715
Weighted TruePositiveRate: 0.7917
Weighted MatthewsCorrelation: 0.7885448235154002
Weighted FMeasure: 0.7936648847814691
Iteration time: 479.0
Weighted AreaUnderPRC: 0.8425664869526763
Mean absolute error: 0.027770769230771434
Coverage of cases: 94.53
Instances selection time: 123.0
Test time: 221.0
Accumulative iteration time: 1337.0
Weighted Recall: 0.7917
Weighted FalsePositiveRate: 0.008329561371277775
Kappa statistic: 0.783354812200812
Training time: 356.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 13.201538461538288
Incorrectly Classified Instances: 19.69
Correctly Classified Instances: 80.31
Weighted Precision: 0.8150889853418868
Weighted AreaUnderROC: 0.9668407334957421
Root mean squared error: 0.10830974102083527
Relative absolute error: 36.994880000008145
Root relative squared error: 56.32106533083827
Weighted TruePositiveRate: 0.8031
Weighted MatthewsCorrelation: 0.798997719118228
Weighted FMeasure: 0.804159374658661
Iteration time: 492.0
Weighted AreaUnderPRC: 0.8518479772765575
Mean absolute error: 0.027363076923079127
Coverage of cases: 95.0
Instances selection time: 118.0
Test time: 217.0
Accumulative iteration time: 1829.0
Weighted Recall: 0.8031
Weighted FalsePositiveRate: 0.007864483765135441
Kappa statistic: 0.7952141792911821
Training time: 374.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 12.944230769230623
Incorrectly Classified Instances: 20.49
Correctly Classified Instances: 79.51
Weighted Precision: 0.8096685010210918
Weighted AreaUnderROC: 0.9644155373442661
Root mean squared error: 0.1090451564896243
Relative absolute error: 36.51856000000767
Root relative squared error: 56.70348137460859
Weighted TruePositiveRate: 0.7951
Weighted MatthewsCorrelation: 0.7911229660717236
Weighted FMeasure: 0.7955966476629087
Iteration time: 533.0
Weighted AreaUnderPRC: 0.8468360071650571
Mean absolute error: 0.027010769230771132
Coverage of cases: 94.51
Instances selection time: 123.0
Test time: 207.0
Accumulative iteration time: 2362.0
Weighted Recall: 0.7951
Weighted FalsePositiveRate: 0.008215276769248438
Kappa statistic: 0.7868845374434975
Training time: 410.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 12.984999999999859
Incorrectly Classified Instances: 19.42
Correctly Classified Instances: 80.58
Weighted Precision: 0.823600879761862
Weighted AreaUnderROC: 0.9661415854150104
Root mean squared error: 0.10833601574588525
Relative absolute error: 36.61008000000783
Root relative squared error: 56.33472818786426
Weighted TruePositiveRate: 0.8058
Weighted MatthewsCorrelation: 0.8030780144983112
Weighted FMeasure: 0.8061942753887448
Iteration time: 570.0
Weighted AreaUnderPRC: 0.8544069431054775
Mean absolute error: 0.027078461538463546
Coverage of cases: 94.75
Instances selection time: 119.0
Test time: 211.0
Accumulative iteration time: 2932.0
Weighted Recall: 0.8058
Weighted FalsePositiveRate: 0.00777187210814936
Kappa statistic: 0.7980193414681684
Training time: 451.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 12.662692307692188
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8271675614505526
Weighted AreaUnderROC: 0.9672260327540897
Root mean squared error: 0.1072846680565325
Relative absolute error: 35.683440000007494
Root relative squared error: 55.788027389400796
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.8045155968787027
Weighted FMeasure: 0.8070605489844009
Iteration time: 604.0
Weighted AreaUnderPRC: 0.8601270396084372
Mean absolute error: 0.02639307692307878
Coverage of cases: 94.85
Instances selection time: 113.0
Test time: 213.0
Accumulative iteration time: 3536.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.007739950069539033
Kappa statistic: 0.7982343215067446
Training time: 491.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 12.473461538461512
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8237386914076579
Weighted AreaUnderROC: 0.9638117476689704
Root mean squared error: 0.10749812162938714
Relative absolute error: 35.38288000000712
Root relative squared error: 55.899023247285214
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.8004841568526258
Weighted FMeasure: 0.8024768542030454
Iteration time: 630.0
Weighted AreaUnderPRC: 0.85534777341845
Mean absolute error: 0.026170769230770843
Coverage of cases: 94.17
Instances selection time: 112.0
Test time: 214.0
Accumulative iteration time: 4166.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.007858888363805362
Kappa statistic: 0.7949091009991244
Training time: 518.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 12.404999999999959
Incorrectly Classified Instances: 19.05
Correctly Classified Instances: 80.95
Weighted Precision: 0.8374495144084343
Weighted AreaUnderROC: 0.9685283827806465
Root mean squared error: 0.10630869419543641
Relative absolute error: 34.98352000000705
Root relative squared error: 55.280520981630794
Weighted TruePositiveRate: 0.8095
Weighted MatthewsCorrelation: 0.8104008246776795
Weighted FMeasure: 0.8114401757497366
Iteration time: 654.0
Weighted AreaUnderPRC: 0.8659987929170435
Mean absolute error: 0.02587538461538622
Coverage of cases: 95.0
Instances selection time: 101.0
Test time: 210.0
Accumulative iteration time: 4820.0
Weighted Recall: 0.8095
Weighted FalsePositiveRate: 0.007570092818347395
Kappa statistic: 0.8018813636901975
Training time: 553.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 12.094615384615508
Incorrectly Classified Instances: 19.05
Correctly Classified Instances: 80.95
Weighted Precision: 0.8339691668287414
Weighted AreaUnderROC: 0.9652411998108106
Root mean squared error: 0.10535069639432675
Relative absolute error: 33.93104000000652
Root relative squared error: 54.78236212505373
Weighted TruePositiveRate: 0.8095
Weighted MatthewsCorrelation: 0.8087265482264152
Weighted FMeasure: 0.8098320255638615
Iteration time: 688.0
Weighted AreaUnderPRC: 0.8651382702320809
Mean absolute error: 0.025096923076924398
Coverage of cases: 94.31
Instances selection time: 94.0
Test time: 211.0
Accumulative iteration time: 5508.0
Weighted Recall: 0.8095
Weighted FalsePositiveRate: 0.007549513411978795
Kappa statistic: 0.8018868174708373
Training time: 594.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 11.974230769231003
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8452221418505677
Weighted AreaUnderROC: 0.9676226022472015
Root mean squared error: 0.10477081214213885
Relative absolute error: 33.68456000000647
Root relative squared error: 54.480822313916
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.8149021104369416
Weighted FMeasure: 0.8139071467280418
Iteration time: 714.0
Weighted AreaUnderPRC: 0.8729734160391308
Mean absolute error: 0.024914615384616697
Coverage of cases: 94.66
Instances selection time: 72.0
Test time: 210.0
Accumulative iteration time: 6222.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.007385209193757934
Kappa statistic: 0.8061517815327249
Training time: 642.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 11.878846153846423
Incorrectly Classified Instances: 18.58
Correctly Classified Instances: 81.42
Weighted Precision: 0.8413572605598966
Weighted AreaUnderROC: 0.9690760372475619
Root mean squared error: 0.10408243774121624
Relative absolute error: 33.37776000000643
Root relative squared error: 54.12286762543622
Weighted TruePositiveRate: 0.8142
Weighted MatthewsCorrelation: 0.8145019453397112
Weighted FMeasure: 0.8149078913602453
Iteration time: 765.0
Weighted AreaUnderPRC: 0.8739971550432875
Mean absolute error: 0.024687692307693618
Coverage of cases: 94.92
Instances selection time: 69.0
Test time: 216.0
Accumulative iteration time: 6987.0
Weighted Recall: 0.8142
Weighted FalsePositiveRate: 0.007361724157680847
Kappa statistic: 0.8067770425252531
Training time: 696.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 11.593461538461964
Incorrectly Classified Instances: 18.03
Correctly Classified Instances: 81.97
Weighted Precision: 0.8521512226143227
Weighted AreaUnderROC: 0.9721076183938275
Root mean squared error: 0.10212963105025802
Relative absolute error: 32.28160000000615
Root relative squared error: 53.10740814613788
Weighted TruePositiveRate: 0.8197
Weighted MatthewsCorrelation: 0.8218019237806841
Weighted FMeasure: 0.8207636364713456
Iteration time: 800.0
Weighted AreaUnderPRC: 0.8839952130050828
Mean absolute error: 0.023876923076924295
Coverage of cases: 95.38
Instances selection time: 65.0
Test time: 214.0
Accumulative iteration time: 7787.0
Weighted Recall: 0.8197
Weighted FalsePositiveRate: 0.007139805264904862
Kappa statistic: 0.8124989178130135
Training time: 735.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 11.608846153846486
Incorrectly Classified Instances: 18.02
Correctly Classified Instances: 81.98
Weighted Precision: 0.850486250347782
Weighted AreaUnderROC: 0.9715540892377714
Root mean squared error: 0.10210627947238378
Relative absolute error: 32.16512000000602
Root relative squared error: 53.09526532564327
Weighted TruePositiveRate: 0.8198
Weighted MatthewsCorrelation: 0.820873983478958
Weighted FMeasure: 0.819692626493973
Iteration time: 853.0
Weighted AreaUnderPRC: 0.8827797874328118
Mean absolute error: 0.023790769230770364
Coverage of cases: 95.27
Instances selection time: 63.0
Test time: 221.0
Accumulative iteration time: 8640.0
Weighted Recall: 0.8198
Weighted FalsePositiveRate: 0.00712462462734739
Kappa statistic: 0.812607536184476
Training time: 790.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 11.558076923077342
Incorrectly Classified Instances: 16.12
Correctly Classified Instances: 83.88
Weighted Precision: 0.8591529145441437
Weighted AreaUnderROC: 0.9756038205757919
Root mean squared error: 0.09896347422544072
Relative absolute error: 31.12096000000582
Root relative squared error: 51.46100659723276
Weighted TruePositiveRate: 0.8388
Weighted MatthewsCorrelation: 0.8378611795874622
Weighted FMeasure: 0.8387220408224061
Iteration time: 886.0
Weighted AreaUnderPRC: 0.8956073856157516
Mean absolute error: 0.02301846153846263
Coverage of cases: 96.02
Instances selection time: 57.0
Test time: 220.0
Accumulative iteration time: 9526.0
Weighted Recall: 0.8388
Weighted FalsePositiveRate: 0.006383289183703558
Kappa statistic: 0.8323585935376858
Training time: 829.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 11.47423076923119
Incorrectly Classified Instances: 16.84
Correctly Classified Instances: 83.16
Weighted Precision: 0.8525042727232806
Weighted AreaUnderROC: 0.97309017969148
Root mean squared error: 0.10016179219335475
Relative absolute error: 31.282160000005664
Root relative squared error: 52.084131940548104
Weighted TruePositiveRate: 0.8316
Weighted MatthewsCorrelation: 0.8298135700929168
Weighted FMeasure: 0.830055804729908
Iteration time: 938.0
Weighted AreaUnderPRC: 0.8899667708285971
Mean absolute error: 0.023137692307693268
Coverage of cases: 95.56
Instances selection time: 55.0
Test time: 233.0
Accumulative iteration time: 10464.0
Weighted Recall: 0.8316
Weighted FalsePositiveRate: 0.006685703218511732
Kappa statistic: 0.8248657154753438
Training time: 883.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 10.982692307692911
Incorrectly Classified Instances: 16.54
Correctly Classified Instances: 83.46
Weighted Precision: 0.8528951957480243
Weighted AreaUnderROC: 0.9722866221779289
Root mean squared error: 0.09886937769680781
Relative absolute error: 30.083040000005386
Root relative squared error: 51.41207640234365
Weighted TruePositiveRate: 0.8346
Weighted MatthewsCorrelation: 0.8316458399644976
Weighted FMeasure: 0.8318438344501404
Iteration time: 965.0
Weighted AreaUnderPRC: 0.8899680286351406
Mean absolute error: 0.022250769230770108
Coverage of cases: 95.31
Instances selection time: 50.0
Test time: 223.0
Accumulative iteration time: 11429.0
Weighted Recall: 0.8346
Weighted FalsePositiveRate: 0.006601412710709403
Kappa statistic: 0.8279784626955228
Training time: 915.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 11.121923076923599
Incorrectly Classified Instances: 14.79
Correctly Classified Instances: 85.21
Weighted Precision: 0.868847768559607
Weighted AreaUnderROC: 0.9771608966563217
Root mean squared error: 0.09535641642724606
Relative absolute error: 29.19696000000533
Root relative squared error: 49.58533654217141
Weighted TruePositiveRate: 0.8521
Weighted MatthewsCorrelation: 0.8496450670902707
Weighted FMeasure: 0.8498379649675105
Iteration time: 1025.0
Weighted AreaUnderPRC: 0.9049461320544467
Mean absolute error: 0.021595384615385543
Coverage of cases: 96.17
Instances selection time: 50.0
Test time: 242.0
Accumulative iteration time: 12454.0
Weighted Recall: 0.8521
Weighted FalsePositiveRate: 0.005878629131445304
Kappa statistic: 0.846180889900649
Training time: 975.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 10.888076923077593
Incorrectly Classified Instances: 14.21
Correctly Classified Instances: 85.79
Weighted Precision: 0.870937456941697
Weighted AreaUnderROC: 0.9748072277558139
Root mean squared error: 0.09439850063998427
Relative absolute error: 28.43984000000522
Root relative squared error: 49.08722033279525
Weighted TruePositiveRate: 0.8579
Weighted MatthewsCorrelation: 0.855046752444597
Weighted FMeasure: 0.8563741994006641
Iteration time: 1058.0
Weighted AreaUnderPRC: 0.9015693701718752
Mean absolute error: 0.02103538461538554
Coverage of cases: 95.74
Instances selection time: 43.0
Test time: 247.0
Accumulative iteration time: 13512.0
Weighted Recall: 0.8579
Weighted FalsePositiveRate: 0.005655529126320319
Kappa statistic: 0.8522114364073871
Training time: 1015.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 10.708461538462261
Incorrectly Classified Instances: 14.38
Correctly Classified Instances: 85.62
Weighted Precision: 0.8717502435021359
Weighted AreaUnderROC: 0.97724762288163
Root mean squared error: 0.09387143417380149
Relative absolute error: 27.87616000000503
Root relative squared error: 48.81314577038018
Weighted TruePositiveRate: 0.8562
Weighted MatthewsCorrelation: 0.8535568516539859
Weighted FMeasure: 0.8538681729858194
Iteration time: 1093.0
Weighted AreaUnderPRC: 0.9070974638685795
Mean absolute error: 0.02061846153846238
Coverage of cases: 96.17
Instances selection time: 39.0
Test time: 249.0
Accumulative iteration time: 14605.0
Weighted Recall: 0.8562
Weighted FalsePositiveRate: 0.0057021094750032515
Kappa statistic: 0.8504472236416273
Training time: 1054.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 10.67269230769306
Incorrectly Classified Instances: 13.95
Correctly Classified Instances: 86.05
Weighted Precision: 0.8735250120775361
Weighted AreaUnderROC: 0.980752270920061
Root mean squared error: 0.09260877180583248
Relative absolute error: 27.641120000005145
Root relative squared error: 48.156561339036244
Weighted TruePositiveRate: 0.8605
Weighted MatthewsCorrelation: 0.857589604733498
Weighted FMeasure: 0.8586860256085463
Iteration time: 1142.0
Weighted AreaUnderPRC: 0.9139604686596374
Mean absolute error: 0.020444615384616338
Coverage of cases: 96.84
Instances selection time: 35.0
Test time: 252.0
Accumulative iteration time: 15747.0
Weighted Recall: 0.8605
Weighted FalsePositiveRate: 0.0055483486659603275
Kappa statistic: 0.85491566778184
Training time: 1107.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 10.401538461539385
Incorrectly Classified Instances: 12.53
Correctly Classified Instances: 87.47
Weighted Precision: 0.8860297306593213
Weighted AreaUnderROC: 0.9833778159676816
Root mean squared error: 0.08915198776851281
Relative absolute error: 26.332800000005026
Root relative squared error: 46.35903363962989
Weighted TruePositiveRate: 0.8747
Weighted MatthewsCorrelation: 0.8723301208382501
Weighted FMeasure: 0.8738139646004762
Iteration time: 1194.0
Weighted AreaUnderPRC: 0.9249849351431433
Mean absolute error: 0.01947692307692408
Coverage of cases: 97.27
Instances selection time: 29.0
Test time: 249.0
Accumulative iteration time: 16941.0
Weighted Recall: 0.8747
Weighted FalsePositiveRate: 0.004991660984168093
Kappa statistic: 0.8696829675732223
Training time: 1165.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 10.31923076923172
Incorrectly Classified Instances: 11.81
Correctly Classified Instances: 88.19
Weighted Precision: 0.891699880408683
Weighted AreaUnderROC: 0.9841051487419848
Root mean squared error: 0.0874431683569838
Relative absolute error: 25.638080000004816
Root relative squared error: 45.470447545634755
Weighted TruePositiveRate: 0.8819
Weighted MatthewsCorrelation: 0.8790222582395582
Weighted FMeasure: 0.8802136676879687
Iteration time: 1228.0
Weighted AreaUnderPRC: 0.9301047557864977
Mean absolute error: 0.01896307692307784
Coverage of cases: 97.35
Instances selection time: 25.0
Test time: 249.0
Accumulative iteration time: 18169.0
Weighted Recall: 0.8819
Weighted FalsePositiveRate: 0.004719905133747845
Kappa statistic: 0.8771683708783826
Training time: 1203.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 10.04307692307792
Incorrectly Classified Instances: 11.53
Correctly Classified Instances: 88.47
Weighted Precision: 0.8929465324930103
Weighted AreaUnderROC: 0.9849097287797461
Root mean squared error: 0.08640023148117164
Relative absolute error: 24.88616000000481
Root relative squared error: 44.92812037021239
Weighted TruePositiveRate: 0.8847
Weighted MatthewsCorrelation: 0.8817443870146049
Weighted FMeasure: 0.8834956760991539
Iteration time: 1272.0
Weighted AreaUnderPRC: 0.9323128756290598
Mean absolute error: 0.018406923076924066
Coverage of cases: 97.51
Instances selection time: 19.0
Test time: 245.0
Accumulative iteration time: 19441.0
Weighted Recall: 0.8847
Weighted FalsePositiveRate: 0.004597005481002253
Kappa statistic: 0.8800824699791197
Training time: 1253.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 10.045384615385611
Incorrectly Classified Instances: 10.21
Correctly Classified Instances: 89.79
Weighted Precision: 0.9025323247462842
Weighted AreaUnderROC: 0.9895547915730034
Root mean squared error: 0.0838239914252572
Relative absolute error: 24.264240000004953
Root relative squared error: 43.58847554113679
Weighted TruePositiveRate: 0.8979
Weighted MatthewsCorrelation: 0.8948237882691114
Weighted FMeasure: 0.8973524337632774
Iteration time: 1339.0
Weighted AreaUnderPRC: 0.9419482500995727
Mean absolute error: 0.017946923076924234
Coverage of cases: 98.4
Instances selection time: 29.0
Test time: 223.0
Accumulative iteration time: 20780.0
Weighted Recall: 0.8979
Weighted FalsePositiveRate: 0.004077528258788112
Kappa statistic: 0.8938089858711348
Training time: 1310.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 9.8576923076933
Incorrectly Classified Instances: 9.65
Correctly Classified Instances: 90.35
Weighted Precision: 0.9067474483187893
Weighted AreaUnderROC: 0.9895578260811759
Root mean squared error: 0.08119160623535525
Relative absolute error: 23.148320000004627
Root relative squared error: 42.219635242387675
Weighted TruePositiveRate: 0.9035
Weighted MatthewsCorrelation: 0.900752094294285
Weighted FMeasure: 0.903984894132863
Iteration time: 1374.0
Weighted AreaUnderPRC: 0.9465800314269569
Mean absolute error: 0.017121538461539493
Coverage of cases: 98.35
Instances selection time: 8.0
Test time: 219.0
Accumulative iteration time: 22154.0
Weighted Recall: 0.9035
Weighted FalsePositiveRate: 0.003860879031619895
Kappa statistic: 0.899634459340401
Training time: 1366.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 9.6953846153856
Incorrectly Classified Instances: 9.1
Correctly Classified Instances: 90.9
Weighted Precision: 0.9108306748302719
Weighted AreaUnderROC: 0.991122750888924
Root mean squared error: 0.08014937976630339
Relative absolute error: 22.631440000004666
Root relative squared error: 41.677677478480675
Weighted TruePositiveRate: 0.909
Weighted MatthewsCorrelation: 0.9059604951110678
Weighted FMeasure: 0.9092024117240902
Iteration time: 1404.0
Weighted AreaUnderPRC: 0.9499562502821013
Mean absolute error: 0.016739230769231884
Coverage of cases: 98.65
Instances selection time: 3.0
Test time: 231.0
Accumulative iteration time: 23558.0
Weighted Recall: 0.909
Weighted FalsePositiveRate: 0.0036401921492563217
Kappa statistic: 0.9053547436296099
Training time: 1401.0
		
Time end:Sat Oct 07 18.14.46 EEST 2017