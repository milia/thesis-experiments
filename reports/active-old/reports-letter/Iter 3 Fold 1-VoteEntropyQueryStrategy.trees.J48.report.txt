Sun Oct 08 02.39.52 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 02.39.52 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 5.430769230769906
Incorrectly Classified Instances: 28.03
Correctly Classified Instances: 71.97
Weighted Precision: 0.7273648870154474
Weighted AreaUnderROC: 0.8805878465798296
Root mean squared error: 0.13657483074616739
Relative absolute error: 31.145178363819795
Root relative squared error: 71.018911988012
Weighted TruePositiveRate: 0.7197
Weighted MatthewsCorrelation: 0.7113709861169274
Weighted FMeasure: 0.7214221325276956
Iteration time: 169.0
Weighted AreaUnderPRC: 0.6212760189019202
Mean absolute error: 0.0230363745294493
Coverage of cases: 77.1
Instances selection time: 44.0
Test time: 186.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7197
Weighted FalsePositiveRate: 0.011195380643640658
Kappa statistic: 0.7084809475710836
Training time: 125.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 5.82230769230843
Incorrectly Classified Instances: 28.65
Correctly Classified Instances: 71.35
Weighted Precision: 0.7273932794226643
Weighted AreaUnderROC: 0.8794881926578402
Root mean squared error: 0.13821963586029087
Relative absolute error: 32.16216564526727
Root relative squared error: 71.87421064735626
Weighted TruePositiveRate: 0.7135
Weighted MatthewsCorrelation: 0.7069387565151198
Weighted FMeasure: 0.7158491329230705
Iteration time: 204.0
Weighted AreaUnderPRC: 0.6169471075214648
Mean absolute error: 0.023788584057147028
Coverage of cases: 76.52
Instances selection time: 43.0
Test time: 176.0
Accumulative iteration time: 373.0
Weighted Recall: 0.7135
Weighted FalsePositiveRate: 0.011423605250336824
Kappa statistic: 0.7020381030553793
Training time: 161.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 5.875769230769977
Incorrectly Classified Instances: 29.93
Correctly Classified Instances: 70.07
Weighted Precision: 0.7139701366366834
Weighted AreaUnderROC: 0.8745397658275859
Root mean squared error: 0.14133531490910717
Relative absolute error: 33.79613719340404
Root relative squared error: 73.49436375274085
Weighted TruePositiveRate: 0.7007
Weighted MatthewsCorrelation: 0.6929964877252255
Weighted FMeasure: 0.7020725386492691
Iteration time: 225.0
Weighted AreaUnderPRC: 0.598196885605825
Mean absolute error: 0.02499714289452613
Coverage of cases: 75.57
Instances selection time: 43.0
Test time: 184.0
Accumulative iteration time: 598.0
Weighted Recall: 0.7007
Weighted FalsePositiveRate: 0.011969712295281206
Kappa statistic: 0.6887205375106781
Training time: 182.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 5.81807692307763
Incorrectly Classified Instances: 29.75
Correctly Classified Instances: 70.25
Weighted Precision: 0.7136611027080836
Weighted AreaUnderROC: 0.8781318032216504
Root mean squared error: 0.1399545409260257
Relative absolute error: 33.25185211187287
Root relative squared error: 72.77636128153844
Weighted TruePositiveRate: 0.7025
Weighted MatthewsCorrelation: 0.693456948875571
Weighted FMeasure: 0.7020070162290083
Iteration time: 245.0
Weighted AreaUnderPRC: 0.6067670662062481
Mean absolute error: 0.02459456517150017
Coverage of cases: 76.06
Instances selection time: 42.0
Test time: 192.0
Accumulative iteration time: 843.0
Weighted Recall: 0.7025
Weighted FalsePositiveRate: 0.011867068405818654
Kappa statistic: 0.6906034714290505
Training time: 203.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 6.144615384616184
Incorrectly Classified Instances: 28.85
Correctly Classified Instances: 71.15
Weighted Precision: 0.7251598033221294
Weighted AreaUnderROC: 0.883108440600652
Root mean squared error: 0.13776513088842804
Relative absolute error: 32.52639406951701
Root relative squared error: 71.63786806198759
Weighted TruePositiveRate: 0.7115
Weighted MatthewsCorrelation: 0.7034993887668431
Weighted FMeasure: 0.711025627421244
Iteration time: 256.0
Weighted AreaUnderPRC: 0.6339034701797798
Mean absolute error: 0.024057983779225196
Coverage of cases: 77.18
Instances selection time: 38.0
Test time: 180.0
Accumulative iteration time: 1099.0
Weighted Recall: 0.7115
Weighted FalsePositiveRate: 0.011535091356993687
Kappa statistic: 0.6999505693263742
Training time: 218.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 5.673461538462189
Incorrectly Classified Instances: 28.82
Correctly Classified Instances: 71.18
Weighted Precision: 0.7243396317725035
Weighted AreaUnderROC: 0.8804071561687575
Root mean squared error: 0.13807178523698985
Relative absolute error: 32.32127583372372
Root relative squared error: 71.79732832323974
Weighted TruePositiveRate: 0.7118
Weighted MatthewsCorrelation: 0.703431321756422
Weighted FMeasure: 0.711143987215148
Iteration time: 276.0
Weighted AreaUnderPRC: 0.6318149021776279
Mean absolute error: 0.023906269107780476
Coverage of cases: 76.7
Instances selection time: 36.0
Test time: 175.0
Accumulative iteration time: 1375.0
Weighted Recall: 0.7118
Weighted FalsePositiveRate: 0.011511022572909872
Kappa statistic: 0.7002652070504404
Training time: 240.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 5.64307692307753
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7375277873779622
Weighted AreaUnderROC: 0.8880014315894352
Root mean squared error: 0.13529817860843152
Relative absolute error: 30.673788813347297
Root relative squared error: 70.35505287638931
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.7156181516090435
Weighted FMeasure: 0.7217200272332916
Iteration time: 313.0
Weighted AreaUnderPRC: 0.6357399237811718
Mean absolute error: 0.022687713619336553
Coverage of cases: 77.9
Instances selection time: 36.0
Test time: 178.0
Accumulative iteration time: 1688.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.011046769842534979
Kappa statistic: 0.7129492225063285
Training time: 277.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 5.755000000000667
Incorrectly Classified Instances: 26.52
Correctly Classified Instances: 73.48
Weighted Precision: 0.7460833465317884
Weighted AreaUnderROC: 0.8884212181060842
Root mean squared error: 0.13423453195402954
Relative absolute error: 30.567948323329464
Root relative squared error: 69.80195661610024
Weighted TruePositiveRate: 0.7348
Weighted MatthewsCorrelation: 0.7266559167114346
Weighted FMeasure: 0.7333867720731039
Iteration time: 327.0
Weighted AreaUnderPRC: 0.6445478133425497
Mean absolute error: 0.022609429233228697
Coverage of cases: 77.97
Instances selection time: 34.0
Test time: 175.0
Accumulative iteration time: 2015.0
Weighted Recall: 0.7348
Weighted FalsePositiveRate: 0.010600952173444401
Kappa statistic: 0.7241798857599265
Training time: 293.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 5.717692307692948
Incorrectly Classified Instances: 27.3
Correctly Classified Instances: 72.7
Weighted Precision: 0.7373767948632196
Weighted AreaUnderROC: 0.8879827529977609
Root mean squared error: 0.1360957667452969
Relative absolute error: 31.04014523612007
Root relative squared error: 70.76979870755932
Weighted TruePositiveRate: 0.727
Weighted MatthewsCorrelation: 0.7174473433609789
Weighted FMeasure: 0.7236790668014671
Iteration time: 349.0
Weighted AreaUnderPRC: 0.6382692580124796
Mean absolute error: 0.02295868730481933
Coverage of cases: 77.81
Instances selection time: 33.0
Test time: 176.0
Accumulative iteration time: 2364.0
Weighted Recall: 0.727
Weighted FalsePositiveRate: 0.010896970762280437
Kappa statistic: 0.716071970742475
Training time: 316.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 5.443846153846748
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7631559611020499
Weighted AreaUnderROC: 0.8984059802321268
Root mean squared error: 0.13062773988308257
Relative absolute error: 28.613192551222173
Root relative squared error: 67.92642473920768
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.7393049296482048
Weighted FMeasure: 0.7432677276274127
Iteration time: 376.0
Weighted AreaUnderPRC: 0.6693424955811876
Mean absolute error: 0.02116360395800161
Coverage of cases: 79.64
Instances selection time: 30.0
Test time: 174.0
Accumulative iteration time: 2740.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.010127747174633625
Kappa statistic: 0.7358395435307312
Training time: 346.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 5.317307692308349
Incorrectly Classified Instances: 25.91
Correctly Classified Instances: 74.09
Weighted Precision: 0.7589701270705421
Weighted AreaUnderROC: 0.8908024882740949
Root mean squared error: 0.1323630239849667
Relative absolute error: 29.250401300875478
Root relative squared error: 68.82877247218748
Weighted TruePositiveRate: 0.7409
Weighted MatthewsCorrelation: 0.7344770024272387
Weighted FMeasure: 0.7386815676350184
Iteration time: 417.0
Weighted AreaUnderPRC: 0.656656595109837
Mean absolute error: 0.021634912204786538
Coverage of cases: 78.55
Instances selection time: 30.0
Test time: 173.0
Accumulative iteration time: 3157.0
Weighted Recall: 0.7409
Weighted FalsePositiveRate: 0.010322463986162898
Kappa statistic: 0.7305402535837731
Training time: 387.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 5.262692307692946
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7543029788637196
Weighted AreaUnderROC: 0.8927222831447691
Root mean squared error: 0.13272512103195805
Relative absolute error: 28.826182196487718
Root relative squared error: 69.017062936623
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.7324266179830602
Weighted FMeasure: 0.7364701202686642
Iteration time: 638.0
Weighted AreaUnderPRC: 0.6521515914958823
Mean absolute error: 0.021321140677872556
Coverage of cases: 78.46
Instances selection time: 28.0
Test time: 178.0
Accumulative iteration time: 3795.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.010279557333762322
Kappa statistic: 0.7312693013042394
Training time: 610.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 5.29538461538527
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7590453132508025
Weighted AreaUnderROC: 0.8953283504588577
Root mean squared error: 0.13144943419853578
Relative absolute error: 28.534524363432126
Root relative squared error: 68.35370578324337
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.736618478307362
Weighted FMeasure: 0.7404072338498243
Iteration time: 469.0
Weighted AreaUnderPRC: 0.6573858957634794
Mean absolute error: 0.021105417428571114
Coverage of cases: 78.59
Instances selection time: 27.0
Test time: 178.0
Accumulative iteration time: 4264.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.01016169588710528
Kappa statistic: 0.7350036699599671
Training time: 442.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 5.180000000000635
Incorrectly Classified Instances: 24.04
Correctly Classified Instances: 75.96
Weighted Precision: 0.7718854164906045
Weighted AreaUnderROC: 0.9044984553130218
Root mean squared error: 0.12713357568900813
Relative absolute error: 27.042536126103695
Root relative squared error: 66.10945935828885
Weighted TruePositiveRate: 0.7596
Weighted MatthewsCorrelation: 0.7509836992126234
Weighted FMeasure: 0.7544524711467051
Iteration time: 502.0
Weighted AreaUnderPRC: 0.6919724993292256
Mean absolute error: 0.020001875832914143
Coverage of cases: 80.33
Instances selection time: 25.0
Test time: 185.0
Accumulative iteration time: 4766.0
Weighted Recall: 0.7596
Weighted FalsePositiveRate: 0.009604033601752309
Kappa statistic: 0.7499724127214952
Training time: 477.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 5.13153846153909
Incorrectly Classified Instances: 23.77
Correctly Classified Instances: 76.23
Weighted Precision: 0.7823888798633932
Weighted AreaUnderROC: 0.9063713723840543
Root mean squared error: 0.12649934488816383
Relative absolute error: 26.787103940544586
Root relative squared error: 65.77965934184978
Weighted TruePositiveRate: 0.7623
Weighted MatthewsCorrelation: 0.7558484148917543
Weighted FMeasure: 0.757104968683277
Iteration time: 519.0
Weighted AreaUnderPRC: 0.6867731282371493
Mean absolute error: 0.019812946701583466
Coverage of cases: 80.94
Instances selection time: 22.0
Test time: 180.0
Accumulative iteration time: 5285.0
Weighted Recall: 0.7623
Weighted FalsePositiveRate: 0.009531535727302141
Kappa statistic: 0.7527671898908261
Training time: 497.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 5.073076923077508
Incorrectly Classified Instances: 23.33
Correctly Classified Instances: 76.67
Weighted Precision: 0.7836623789096655
Weighted AreaUnderROC: 0.9024613117395751
Root mean squared error: 0.1254823807149732
Relative absolute error: 26.094983653270724
Root relative squared error: 65.25083797179062
Weighted TruePositiveRate: 0.7667
Weighted MatthewsCorrelation: 0.759795392771471
Weighted FMeasure: 0.7619901801421497
Iteration time: 588.0
Weighted AreaUnderPRC: 0.6817815098977664
Mean absolute error: 0.019301023412179794
Coverage of cases: 80.13
Instances selection time: 22.0
Test time: 177.0
Accumulative iteration time: 5873.0
Weighted Recall: 0.7667
Weighted FalsePositiveRate: 0.009332971817453206
Kappa statistic: 0.7573533783086942
Training time: 566.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 5.219230769231444
Incorrectly Classified Instances: 22.47
Correctly Classified Instances: 77.53
Weighted Precision: 0.7872023798509507
Weighted AreaUnderROC: 0.9080707795429614
Root mean squared error: 0.12370210468715925
Relative absolute error: 25.706828987988327
Root relative squared error: 64.3250944373273
Weighted TruePositiveRate: 0.7753
Weighted MatthewsCorrelation: 0.7676588306909679
Weighted FMeasure: 0.7712046824186123
Iteration time: 617.0
Weighted AreaUnderPRC: 0.6916407078129603
Mean absolute error: 0.019013926766260902
Coverage of cases: 81.24
Instances selection time: 20.0
Test time: 175.0
Accumulative iteration time: 6490.0
Weighted Recall: 0.7753
Weighted FalsePositiveRate: 0.008981930101425664
Kappa statistic: 0.7663038066114598
Training time: 597.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 4.9073076923082715
Incorrectly Classified Instances: 21.24
Correctly Classified Instances: 78.76
Weighted Precision: 0.7971639233503777
Weighted AreaUnderROC: 0.9179529876587594
Root mean squared error: 0.11952473250095134
Relative absolute error: 23.96224060745871
Root relative squared error: 62.15286090049904
Weighted TruePositiveRate: 0.7876
Weighted MatthewsCorrelation: 0.7798307272826724
Weighted FMeasure: 0.7835424473046877
Iteration time: 852.0
Weighted AreaUnderPRC: 0.7204589405632242
Mean absolute error: 0.017723550745159296
Coverage of cases: 82.51
Instances selection time: 17.0
Test time: 175.0
Accumulative iteration time: 7342.0
Weighted Recall: 0.7876
Weighted FalsePositiveRate: 0.008500540623113726
Kappa statistic: 0.7790929214216464
Training time: 835.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 5.149615384616001
Incorrectly Classified Instances: 20.74
Correctly Classified Instances: 79.26
Weighted Precision: 0.8034233675364792
Weighted AreaUnderROC: 0.9201305816532562
Root mean squared error: 0.11865575668227382
Relative absolute error: 23.422213009909676
Root relative squared error: 61.7009934747867
Weighted TruePositiveRate: 0.7926
Weighted MatthewsCorrelation: 0.7852824627572601
Weighted FMeasure: 0.7884405109898472
Iteration time: 668.0
Weighted AreaUnderPRC: 0.7148116666934184
Mean absolute error: 0.017324122048747344
Coverage of cases: 83.23
Instances selection time: 16.0
Test time: 177.0
Accumulative iteration time: 8010.0
Weighted Recall: 0.7926
Weighted FalsePositiveRate: 0.008300389289716808
Kappa statistic: 0.784296132330849
Training time: 652.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 5.106153846154456
Incorrectly Classified Instances: 20.46
Correctly Classified Instances: 79.54
Weighted Precision: 0.8047656911027214
Weighted AreaUnderROC: 0.9198660295951828
Root mean squared error: 0.1170833124564937
Relative absolute error: 22.999744609255576
Root relative squared error: 60.88332247738098
Weighted TruePositiveRate: 0.7954
Weighted MatthewsCorrelation: 0.7882738900463927
Weighted FMeasure: 0.7922426666981636
Iteration time: 736.0
Weighted AreaUnderPRC: 0.7216655692951621
Mean absolute error: 0.01701164542104465
Coverage of cases: 83.69
Instances selection time: 19.0
Test time: 178.0
Accumulative iteration time: 8746.0
Weighted Recall: 0.7954
Weighted FalsePositiveRate: 0.008179834740961385
Kappa statistic: 0.7872092587893185
Training time: 717.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 5.301153846154446
Incorrectly Classified Instances: 19.23
Correctly Classified Instances: 80.77
Weighted Precision: 0.8170194215800686
Weighted AreaUnderROC: 0.9243568671653662
Root mean squared error: 0.11378660738932853
Relative absolute error: 22.111931138161662
Root relative squared error: 59.16903584245497
Weighted TruePositiveRate: 0.8077
Weighted MatthewsCorrelation: 0.8018574598917969
Weighted FMeasure: 0.8062979030872509
Iteration time: 795.0
Weighted AreaUnderPRC: 0.739770403201764
Mean absolute error: 0.016354978652484153
Coverage of cases: 84.69
Instances selection time: 12.0
Test time: 177.0
Accumulative iteration time: 9541.0
Weighted Recall: 0.8077
Weighted FalsePositiveRate: 0.007678063876497275
Kappa statistic: 0.8000048922360214
Training time: 783.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 5.214615384615986
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8226815365232171
Weighted AreaUnderROC: 0.9266044393510492
Root mean squared error: 0.11265679710772437
Relative absolute error: 21.819566891896642
Root relative squared error: 58.58153449602076
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.8062897404381225
Weighted FMeasure: 0.8098232033958916
Iteration time: 1040.0
Weighted AreaUnderPRC: 0.7455397408254612
Mean absolute error: 0.016138732908205323
Coverage of cases: 85.1
Instances selection time: 11.0
Test time: 179.0
Accumulative iteration time: 10581.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.007525394089337968
Kappa statistic: 0.8044716826946499
Training time: 1029.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 5.218076923077548
Incorrectly Classified Instances: 18.89
Correctly Classified Instances: 81.11
Weighted Precision: 0.8188879472733603
Weighted AreaUnderROC: 0.9259472973588024
Root mean squared error: 0.11352376120894124
Relative absolute error: 21.953405670119835
Root relative squared error: 59.03235582865357
Weighted TruePositiveRate: 0.8111
Weighted MatthewsCorrelation: 0.8044093698847146
Weighted FMeasure: 0.8085290267123288
Iteration time: 919.0
Weighted AreaUnderPRC: 0.7406952105845173
Mean absolute error: 0.016237726087364478
Coverage of cases: 85.09
Instances selection time: 8.0
Test time: 182.0
Accumulative iteration time: 11500.0
Weighted Recall: 0.8111
Weighted FalsePositiveRate: 0.0075567377686580044
Kappa statistic: 0.8035367156129268
Training time: 911.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 5.038076923077536
Incorrectly Classified Instances: 17.39
Correctly Classified Instances: 82.61
Weighted Precision: 0.8312979323120768
Weighted AreaUnderROC: 0.9318851674527711
Root mean squared error: 0.10865280181812403
Relative absolute error: 20.441089165282268
Root relative squared error: 56.49945694542844
Weighted TruePositiveRate: 0.8261
Weighted MatthewsCorrelation: 0.8195626416375504
Weighted FMeasure: 0.8240415612496905
Iteration time: 925.0
Weighted AreaUnderPRC: 0.7602043520588047
Mean absolute error: 0.015119148790887141
Coverage of cases: 86.18
Instances selection time: 7.0
Test time: 193.0
Accumulative iteration time: 12425.0
Weighted Recall: 0.8261
Weighted FalsePositiveRate: 0.006956482326631445
Kappa statistic: 0.819135305761802
Training time: 918.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 4.864230769231359
Incorrectly Classified Instances: 16.95
Correctly Classified Instances: 83.05
Weighted Precision: 0.8351666101825626
Weighted AreaUnderROC: 0.9342922209592118
Root mean squared error: 0.10683858779082187
Relative absolute error: 19.497604943914066
Root relative squared error: 55.55606565123124
Weighted TruePositiveRate: 0.8305
Weighted MatthewsCorrelation: 0.8246673951564455
Weighted FMeasure: 0.829833577001547
Iteration time: 969.0
Weighted AreaUnderPRC: 0.7669045057382866
Mean absolute error: 0.014421305431887091
Coverage of cases: 86.68
Instances selection time: 6.0
Test time: 189.0
Accumulative iteration time: 13394.0
Weighted Recall: 0.8305
Weighted FalsePositiveRate: 0.006774146795092967
Kappa statistic: 0.8237132217028598
Training time: 963.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 4.8938461538467
Incorrectly Classified Instances: 15.67
Correctly Classified Instances: 84.33
Weighted Precision: 0.8456435104696868
Weighted AreaUnderROC: 0.9389465025877322
Root mean squared error: 0.1031152448734665
Relative absolute error: 18.31173134237637
Root relative squared error: 53.619927334206324
Weighted TruePositiveRate: 0.8433
Weighted MatthewsCorrelation: 0.8375531136389467
Weighted FMeasure: 0.843046708443322
Iteration time: 1005.0
Weighted AreaUnderPRC: 0.784217934177598
Mean absolute error: 0.01354417998696288
Coverage of cases: 87.47
Instances selection time: 4.0
Test time: 189.0
Accumulative iteration time: 14399.0
Weighted Recall: 0.8433
Weighted FalsePositiveRate: 0.006263928282406902
Kappa statistic: 0.8370262454619168
Training time: 1001.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 5.058846153846741
Incorrectly Classified Instances: 14.73
Correctly Classified Instances: 85.27
Weighted Precision: 0.8543608231216933
Weighted AreaUnderROC: 0.9433639833424555
Root mean squared error: 0.09964038267854569
Relative absolute error: 17.55826711920627
Root relative squared error: 51.812998992847376
Weighted TruePositiveRate: 0.8527
Weighted MatthewsCorrelation: 0.8473857824102803
Weighted FMeasure: 0.8529741529608693
Iteration time: 1049.0
Weighted AreaUnderPRC: 0.8002138812095834
Mean absolute error: 0.012986883963908152
Coverage of cases: 88.49
Instances selection time: 1.0
Test time: 188.0
Accumulative iteration time: 15448.0
Weighted Recall: 0.8527
Weighted FalsePositiveRate: 0.0058908456854754425
Kappa statistic: 0.8468024392962206
Training time: 1048.0
		
Time end:Sun Oct 08 02.40.20 EEST 2017