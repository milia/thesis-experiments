Sun Oct 08 01.15.27 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 01.15.27 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 78.8907692307661
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7402836297157078
Weighted AreaUnderROC: 0.9596483209667225
Root mean squared error: 0.186532281578835
Relative absolute error: 96.35216000000146
Root relative squared error: 96.99678642100098
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.7138406398467131
Weighted FMeasure: 0.7192029445716991
Iteration time: 2713.0
Weighted AreaUnderPRC: 0.637801237079517
Mean absolute error: 0.07126639053253551
Coverage of cases: 99.69
Instances selection time: 325.0
Test time: 551.0
Accumulative iteration time: 2713.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.011171359097759431
Kappa statistic: 0.708377372700821
Training time: 2388.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 78.9188461538432
Incorrectly Classified Instances: 29.85
Correctly Classified Instances: 70.15
Weighted Precision: 0.7573154077348585
Weighted AreaUnderROC: 0.9593144708611404
Root mean squared error: 0.1865706962693163
Relative absolute error: 96.372032000001
Root relative squared error: 97.01676206005125
Weighted TruePositiveRate: 0.7015
Weighted MatthewsCorrelation: 0.708429777466306
Weighted FMeasure: 0.7094684013293726
Iteration time: 2765.0
Weighted AreaUnderPRC: 0.6391168414435365
Mean absolute error: 0.07128108875738724
Coverage of cases: 99.68
Instances selection time: 322.0
Test time: 541.0
Accumulative iteration time: 5478.0
Weighted Recall: 0.7015
Weighted FalsePositiveRate: 0.011855584301568111
Kappa statistic: 0.689572968151341
Training time: 2443.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 78.89730769230478
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.7615412251281809
Weighted AreaUnderROC: 0.9590259454421654
Root mean squared error: 0.18658773182047095
Relative absolute error: 96.38060800000137
Root relative squared error: 97.02562054665167
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.7034314749208397
Weighted FMeasure: 0.7012385353992451
Iteration time: 2820.0
Weighted AreaUnderPRC: 0.6322441596660796
Mean absolute error: 0.07128743195265379
Coverage of cases: 99.65
Instances selection time: 304.0
Test time: 551.0
Accumulative iteration time: 8298.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.01220212593792101
Kappa statistic: 0.6825777845635038
Training time: 2516.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 79.11884615384447
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.7561888830935094
Weighted AreaUnderROC: 0.9576840991037209
Root mean squared error: 0.18660393629181438
Relative absolute error: 96.39059200000139
Root relative squared error: 97.03404687175025
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.6973627986477386
Weighted FMeasure: 0.6946326101929677
Iteration time: 2864.0
Weighted AreaUnderPRC: 0.62596831652738
Mean absolute error: 0.07129481656803842
Coverage of cases: 99.6
Instances selection time: 302.0
Test time: 540.0
Accumulative iteration time: 11162.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.012411864633900488
Kappa statistic: 0.6775799106490726
Training time: 2562.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 79.09653846153665
Incorrectly Classified Instances: 32.5
Correctly Classified Instances: 67.5
Weighted Precision: 0.7504250024573753
Weighted AreaUnderROC: 0.9583053814778068
Root mean squared error: 0.18660220464001312
Relative absolute error: 96.39139200000183
Root relative squared error: 97.0331464128136
Weighted TruePositiveRate: 0.675
Weighted MatthewsCorrelation: 0.6821796906350629
Weighted FMeasure: 0.6757455770546013
Iteration time: 2955.0
Weighted AreaUnderPRC: 0.6181210340303769
Mean absolute error: 0.07129540828401507
Coverage of cases: 99.59
Instances selection time: 288.0
Test time: 548.0
Accumulative iteration time: 14117.0
Weighted Recall: 0.675
Weighted FalsePositiveRate: 0.013056348243813628
Kappa statistic: 0.6619644465021969
Training time: 2667.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 79.05615384615194
Incorrectly Classified Instances: 32.98
Correctly Classified Instances: 67.02
Weighted Precision: 0.7616374668702446
Weighted AreaUnderROC: 0.9576205634684402
Root mean squared error: 0.18663015302763075
Relative absolute error: 96.40512000000263
Root relative squared error: 97.04767957437477
Weighted TruePositiveRate: 0.6702
Weighted MatthewsCorrelation: 0.6789251701105584
Weighted FMeasure: 0.6676619322647652
Iteration time: 3063.0
Weighted AreaUnderPRC: 0.616034519115502
Mean absolute error: 0.0713055621301695
Coverage of cases: 99.58
Instances selection time: 266.0
Test time: 567.0
Accumulative iteration time: 17180.0
Weighted Recall: 0.6702
Weighted FalsePositiveRate: 0.013156642525676122
Kappa statistic: 0.6570078639007204
Training time: 2797.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 79.17884615384463
Incorrectly Classified Instances: 33.97
Correctly Classified Instances: 66.03
Weighted Precision: 0.7625131229436305
Weighted AreaUnderROC: 0.956281440128084
Root mean squared error: 0.18664324928433298
Relative absolute error: 96.41315200000317
Root relative squared error: 97.05448962785992
Weighted TruePositiveRate: 0.6603
Weighted MatthewsCorrelation: 0.6712545884719123
Weighted FMeasure: 0.657967487748129
Iteration time: 3153.0
Weighted AreaUnderPRC: 0.6103724667415574
Mean absolute error: 0.07131150295857228
Coverage of cases: 99.57
Instances selection time: 257.0
Test time: 537.0
Accumulative iteration time: 20333.0
Weighted Recall: 0.6603
Weighted FalsePositiveRate: 0.013486751819382971
Kappa statistic: 0.6467363246062439
Training time: 2896.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 79.215384615383
Incorrectly Classified Instances: 36.16
Correctly Classified Instances: 63.84
Weighted Precision: 0.7923111760448878
Weighted AreaUnderROC: 0.9557915068038138
Root mean squared error: 0.18667547923364122
Relative absolute error: 96.43004800000426
Root relative squared error: 97.07124920150021
Weighted TruePositiveRate: 0.6384
Weighted MatthewsCorrelation: 0.6624316488145738
Weighted FMeasure: 0.6381515904971711
Iteration time: 3186.0
Weighted AreaUnderPRC: 0.6134295598564284
Mean absolute error: 0.0713239999999932
Coverage of cases: 99.57
Instances selection time: 242.0
Test time: 550.0
Accumulative iteration time: 23519.0
Weighted Recall: 0.6384
Weighted FalsePositiveRate: 0.014281351879457975
Kappa statistic: 0.6239957939936593
Training time: 2944.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 79.19038461538317
Incorrectly Classified Instances: 37.09
Correctly Classified Instances: 62.91
Weighted Precision: 0.7881579667008706
Weighted AreaUnderROC: 0.9554200170473336
Root mean squared error: 0.18669502283702283
Relative absolute error: 96.43932800000513
Root relative squared error: 97.08141187525865
Weighted TruePositiveRate: 0.6291
Weighted MatthewsCorrelation: 0.6471227913782825
Weighted FMeasure: 0.6204269310859345
Iteration time: 3301.0
Weighted AreaUnderPRC: 0.6114400142396025
Mean absolute error: 0.07133086390531929
Coverage of cases: 99.55
Instances selection time: 235.0
Test time: 548.0
Accumulative iteration time: 26820.0
Weighted Recall: 0.6291
Weighted FalsePositiveRate: 0.014633735654411955
Kappa statistic: 0.6143282473603165
Training time: 3066.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 79.2253846153831
Incorrectly Classified Instances: 37.6
Correctly Classified Instances: 62.4
Weighted Precision: 0.7759579557609205
Weighted AreaUnderROC: 0.9550112225546247
Root mean squared error: 0.18670052994964267
Relative absolute error: 96.4428800000058
Root relative squared error: 97.08427557382096
Weighted TruePositiveRate: 0.624
Weighted MatthewsCorrelation: 0.6349282869578788
Weighted FMeasure: 0.6076221896396118
Iteration time: 3301.0
Weighted AreaUnderPRC: 0.6052073902951913
Mean absolute error: 0.07133349112425469
Coverage of cases: 99.53
Instances selection time: 217.0
Test time: 534.0
Accumulative iteration time: 30121.0
Weighted Recall: 0.624
Weighted FalsePositiveRate: 0.014820612283890358
Kappa statistic: 0.6090338428728018
Training time: 3084.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 79.22538461538319
Incorrectly Classified Instances: 36.49
Correctly Classified Instances: 63.51
Weighted Precision: 0.7823375554643066
Weighted AreaUnderROC: 0.9553702333901931
Root mean squared error: 0.18670701399790188
Relative absolute error: 96.44684800000708
Root relative squared error: 97.08764727891574
Weighted TruePositiveRate: 0.6351
Weighted MatthewsCorrelation: 0.6368598362487042
Weighted FMeasure: 0.6077690671031297
Iteration time: 3399.0
Weighted AreaUnderPRC: 0.6058418647554648
Mean absolute error: 0.07133642603549824
Coverage of cases: 99.49
Instances selection time: 206.0
Test time: 540.0
Accumulative iteration time: 33520.0
Weighted Recall: 0.6351
Weighted FalsePositiveRate: 0.014411279463884227
Kappa statistic: 0.6205633256829028
Training time: 3193.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 79.21807692307543
Incorrectly Classified Instances: 34.96
Correctly Classified Instances: 65.04
Weighted Precision: 0.7781705698395673
Weighted AreaUnderROC: 0.9570491118957765
Root mean squared error: 0.18668969832120516
Relative absolute error: 96.4375040000067
Root relative squared error: 97.07864312703346
Weighted TruePositiveRate: 0.6504
Weighted MatthewsCorrelation: 0.6578398381358957
Weighted FMeasure: 0.6327881311321034
Iteration time: 3518.0
Weighted AreaUnderPRC: 0.6230655924637933
Mean absolute error: 0.0713295147928944
Coverage of cases: 99.49
Instances selection time: 192.0
Test time: 547.0
Accumulative iteration time: 37038.0
Weighted Recall: 0.6504
Weighted FalsePositiveRate: 0.013784850131546407
Kappa statistic: 0.6364916469606875
Training time: 3326.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 79.14038461538279
Incorrectly Classified Instances: 35.2
Correctly Classified Instances: 64.8
Weighted Precision: 0.7734891704551237
Weighted AreaUnderROC: 0.9575113105181497
Root mean squared error: 0.18669606591643947
Relative absolute error: 96.44086400000671
Root relative squared error: 97.08195427655531
Weighted TruePositiveRate: 0.648
Weighted MatthewsCorrelation: 0.6535793330805191
Weighted FMeasure: 0.6281521664044171
Iteration time: 3603.0
Weighted AreaUnderPRC: 0.6228679894849143
Mean absolute error: 0.07133199999999501
Coverage of cases: 99.42
Instances selection time: 184.0
Test time: 534.0
Accumulative iteration time: 40641.0
Weighted Recall: 0.648
Weighted FalsePositiveRate: 0.013862358492612886
Kappa statistic: 0.6340076665828143
Training time: 3419.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 79.12115384615194
Incorrectly Classified Instances: 34.24
Correctly Classified Instances: 65.76
Weighted Precision: 0.7670687973943865
Weighted AreaUnderROC: 0.9577043741783516
Root mean squared error: 0.18669485198052743
Relative absolute error: 96.44016000000695
Root relative squared error: 97.08132302988105
Weighted TruePositiveRate: 0.6576
Weighted MatthewsCorrelation: 0.6575253578686878
Weighted FMeasure: 0.6349369301711124
Iteration time: 3780.0
Weighted AreaUnderPRC: 0.6216094275492077
Mean absolute error: 0.07133147928993601
Coverage of cases: 99.44
Instances selection time: 167.0
Test time: 534.0
Accumulative iteration time: 44421.0
Weighted Recall: 0.6576
Weighted FalsePositiveRate: 0.01348852769165459
Kappa statistic: 0.6439883613723171
Training time: 3613.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 79.04269230769023
Incorrectly Classified Instances: 32.95
Correctly Classified Instances: 67.05
Weighted Precision: 0.7704412950469635
Weighted AreaUnderROC: 0.9582011087084107
Root mean squared error: 0.1866788217860125
Relative absolute error: 96.43225600000697
Root relative squared error: 97.07298732873328
Weighted TruePositiveRate: 0.6705
Weighted MatthewsCorrelation: 0.6677378983779756
Weighted FMeasure: 0.6467288351863982
Iteration time: 3852.0
Weighted AreaUnderPRC: 0.6292507562034826
Mean absolute error: 0.07132563313608987
Coverage of cases: 99.41
Instances selection time: 160.0
Test time: 533.0
Accumulative iteration time: 48273.0
Weighted Recall: 0.6705
Weighted FalsePositiveRate: 0.012981579887411516
Kappa statistic: 0.6573911872591113
Training time: 3692.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 79.01961538461322
Incorrectly Classified Instances: 31.24
Correctly Classified Instances: 68.76
Weighted Precision: 0.7712395373481876
Weighted AreaUnderROC: 0.9591674190058888
Root mean squared error: 0.18665807147994418
Relative absolute error: 96.42163200000736
Root relative squared error: 97.06219716957774
Weighted TruePositiveRate: 0.6876
Weighted MatthewsCorrelation: 0.6801114060728184
Weighted FMeasure: 0.6623327559939876
Iteration time: 3995.0
Weighted AreaUnderPRC: 0.6357316906822067
Mean absolute error: 0.07131777514792449
Coverage of cases: 99.35
Instances selection time: 145.0
Test time: 534.0
Accumulative iteration time: 52268.0
Weighted Recall: 0.6876
Weighted FalsePositiveRate: 0.012344874128498017
Kappa statistic: 0.6751442509267331
Training time: 3850.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 79.00538461538237
Incorrectly Classified Instances: 31.14
Correctly Classified Instances: 68.86
Weighted Precision: 0.7553644112556692
Weighted AreaUnderROC: 0.95976800991601
Root mean squared error: 0.18665468875890867
Relative absolute error: 96.42009600000816
Root relative squared error: 97.06043815463929
Weighted TruePositiveRate: 0.6886
Weighted MatthewsCorrelation: 0.6734163842565158
Weighted FMeasure: 0.6573123000016928
Iteration time: 4153.0
Weighted AreaUnderPRC: 0.6322336456734431
Mean absolute error: 0.07131663905325052
Coverage of cases: 99.33
Instances selection time: 130.0
Test time: 533.0
Accumulative iteration time: 56421.0
Weighted Recall: 0.6886
Weighted FalsePositiveRate: 0.012352192651779859
Kappa statistic: 0.6761658132117547
Training time: 4023.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 79.069230769229
Incorrectly Classified Instances: 30.23
Correctly Classified Instances: 69.77
Weighted Precision: 0.7545536642384427
Weighted AreaUnderROC: 0.9603936317090909
Root mean squared error: 0.18663289819622284
Relative absolute error: 96.41059200000886
Root relative squared error: 97.04910706204264
Weighted TruePositiveRate: 0.6977
Weighted MatthewsCorrelation: 0.6853000548157586
Weighted FMeasure: 0.6736494425427186
Iteration time: 4189.0
Weighted AreaUnderPRC: 0.6431070248678596
Mean absolute error: 0.07130960946745223
Coverage of cases: 99.29
Instances selection time: 121.0
Test time: 534.0
Accumulative iteration time: 60610.0
Weighted Recall: 0.6977
Weighted FalsePositiveRate: 0.011999569462312731
Kappa statistic: 0.6856176120528821
Training time: 4068.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 79.06346153845969
Incorrectly Classified Instances: 30.02
Correctly Classified Instances: 69.98
Weighted Precision: 0.7583571226268203
Weighted AreaUnderROC: 0.9612428175657826
Root mean squared error: 0.18661457289442335
Relative absolute error: 96.40220800000938
Root relative squared error: 97.03957790510692
Weighted TruePositiveRate: 0.6998
Weighted MatthewsCorrelation: 0.6861054265966181
Weighted FMeasure: 0.6729076635869338
Iteration time: 4279.0
Weighted AreaUnderPRC: 0.6466523861930167
Mean absolute error: 0.07130340828402065
Coverage of cases: 99.27
Instances selection time: 110.0
Test time: 535.0
Accumulative iteration time: 64889.0
Weighted Recall: 0.6998
Weighted FalsePositiveRate: 0.01191215028204924
Kappa statistic: 0.6878004933251392
Training time: 4169.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 79.11115384615228
Incorrectly Classified Instances: 28.5
Correctly Classified Instances: 71.5
Weighted Precision: 0.764567683945015
Weighted AreaUnderROC: 0.9622892462431993
Root mean squared error: 0.1865884378853109
Relative absolute error: 96.38956800000886
Root relative squared error: 97.02598770036845
Weighted TruePositiveRate: 0.715
Weighted MatthewsCorrelation: 0.7014931126418681
Weighted FMeasure: 0.6909771557350051
Iteration time: 4359.0
Weighted AreaUnderPRC: 0.6580480590576264
Mean absolute error: 0.07129405917159425
Coverage of cases: 99.35
Instances selection time: 94.0
Test time: 535.0
Accumulative iteration time: 69248.0
Weighted Recall: 0.715
Weighted FalsePositiveRate: 0.011289537866292075
Kappa statistic: 0.7036133063398892
Training time: 4265.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 79.1507692307679
Incorrectly Classified Instances: 26.05
Correctly Classified Instances: 73.95
Weighted Precision: 0.7862943491418891
Weighted AreaUnderROC: 0.9646848104819219
Root mean squared error: 0.18653426609080578
Relative absolute error: 96.3627840000064
Root relative squared error: 96.99781836722579
Weighted TruePositiveRate: 0.7395
Weighted MatthewsCorrelation: 0.7266741971799368
Weighted FMeasure: 0.7194838948183595
Iteration time: 4526.0
Weighted AreaUnderPRC: 0.6776860809476329
Mean absolute error: 0.07127424852070485
Coverage of cases: 99.31
Instances selection time: 81.0
Test time: 533.0
Accumulative iteration time: 73774.0
Weighted Recall: 0.7395
Weighted FalsePositiveRate: 0.010343447276173727
Kappa statistic: 0.7290833637009564
Training time: 4445.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 79.10192307692161
Incorrectly Classified Instances: 24.97
Correctly Classified Instances: 75.03
Weighted Precision: 0.7924444577074182
Weighted AreaUnderROC: 0.9673497636744154
Root mean squared error: 0.1864870705272296
Relative absolute error: 96.33916800000515
Root relative squared error: 96.97327667416616
Weighted TruePositiveRate: 0.7503
Weighted MatthewsCorrelation: 0.7426911056713236
Weighted FMeasure: 0.7379951748153141
Iteration time: 4657.0
Weighted AreaUnderPRC: 0.6881685498138291
Mean absolute error: 0.07125678106508261
Coverage of cases: 99.31
Instances selection time: 71.0
Test time: 534.0
Accumulative iteration time: 78431.0
Weighted Recall: 0.7503
Weighted FalsePositiveRate: 0.0099316481180349
Kappa statistic: 0.7403088045517783
Training time: 4586.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 79.24346153846021
Incorrectly Classified Instances: 23.86
Correctly Classified Instances: 76.14
Weighted Precision: 0.8008220113393758
Weighted AreaUnderROC: 0.9683208441293384
Root mean squared error: 0.18646374478746194
Relative absolute error: 96.32937600000382
Root relative squared error: 96.96114728948697
Weighted TruePositiveRate: 0.7614
Weighted MatthewsCorrelation: 0.7549956110275075
Weighted FMeasure: 0.7516073786950442
Iteration time: 4827.0
Weighted AreaUnderPRC: 0.7022456973446882
Mean absolute error: 0.07124953846153134
Coverage of cases: 99.4
Instances selection time: 57.0
Test time: 540.0
Accumulative iteration time: 83258.0
Weighted Recall: 0.7614
Weighted FalsePositiveRate: 0.00950668254158691
Kappa statistic: 0.7518473642882771
Training time: 4770.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 79.29307692307574
Incorrectly Classified Instances: 23.44
Correctly Classified Instances: 76.56
Weighted Precision: 0.8017574379837593
Weighted AreaUnderROC: 0.9708599756481142
Root mean squared error: 0.1863997061929577
Relative absolute error: 96.29932800000341
Root relative squared error: 96.92784722034477
Weighted TruePositiveRate: 0.7656
Weighted MatthewsCorrelation: 0.7589769634642862
Weighted FMeasure: 0.7559065962685583
Iteration time: 4959.0
Weighted AreaUnderPRC: 0.7055884274314814
Mean absolute error: 0.07122731360946004
Coverage of cases: 99.85
Instances selection time: 45.0
Test time: 536.0
Accumulative iteration time: 88217.0
Weighted Recall: 0.7656
Weighted FalsePositiveRate: 0.009343519329939995
Kappa statistic: 0.756214552826351
Training time: 4914.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 79.27038461538308
Incorrectly Classified Instances: 21.54
Correctly Classified Instances: 78.46
Weighted Precision: 0.8092454750230453
Weighted AreaUnderROC: 0.974043823143928
Root mean squared error: 0.18630900805265913
Relative absolute error: 96.25414400000209
Root relative squared error: 96.8806841873895
Weighted TruePositiveRate: 0.7846
Weighted MatthewsCorrelation: 0.781909585953815
Weighted FMeasure: 0.7835928484997652
Iteration time: 5163.0
Weighted AreaUnderPRC: 0.7279367126249419
Mean absolute error: 0.07119389349111586
Coverage of cases: 99.86
Instances selection time: 33.0
Test time: 550.0
Accumulative iteration time: 93380.0
Weighted Recall: 0.7846
Weighted FalsePositiveRate: 0.008590769973963653
Kappa statistic: 0.7759704232699791
Training time: 5130.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 79.28192307692149
Incorrectly Classified Instances: 19.58
Correctly Classified Instances: 80.42
Weighted Precision: 0.8195565337066402
Weighted AreaUnderROC: 0.9750625336208748
Root mean squared error: 0.18627138428282544
Relative absolute error: 96.23603200000032
Root relative squared error: 96.861119827076
Weighted TruePositiveRate: 0.8042
Weighted MatthewsCorrelation: 0.801488693771457
Weighted FMeasure: 0.8062620173508963
Iteration time: 5291.0
Weighted AreaUnderPRC: 0.7429820542922758
Mean absolute error: 0.07118049704141043
Coverage of cases: 99.86
Instances selection time: 25.0
Test time: 544.0
Accumulative iteration time: 98671.0
Weighted Recall: 0.8042
Weighted FalsePositiveRate: 0.007815056236967873
Kappa statistic: 0.7963550613780956
Training time: 5266.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 79.4080769230757
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8212224535293988
Weighted AreaUnderROC: 0.9765067056227337
Root mean squared error: 0.18621484178460335
Relative absolute error: 96.20982399999978
Root relative squared error: 96.8317177280005
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.8062774429372622
Weighted FMeasure: 0.81201596168718
Iteration time: 5441.0
Weighted AreaUnderPRC: 0.746728519448669
Mean absolute error: 0.07116111242602541
Coverage of cases: 99.99
Instances selection time: 9.0
Test time: 544.0
Accumulative iteration time: 104112.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.007596154780548652
Kappa statistic: 0.8019741451245166
Training time: 5432.0
		
Time end:Sun Oct 08 01.17.34 EEST 2017