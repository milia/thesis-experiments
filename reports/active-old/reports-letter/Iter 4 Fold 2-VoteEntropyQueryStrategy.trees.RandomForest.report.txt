Sun Oct 08 02.46.44 EEST 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 02.46.44 EEST 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 13.817692307692077
Incorrectly Classified Instances: 19.89
Correctly Classified Instances: 80.11
Weighted Precision: 0.8108653293668578
Weighted AreaUnderROC: 0.9659559409718941
Root mean squared error: 0.10977774749991154
Relative absolute error: 38.39992000000862
Root relative squared error: 57.08442869995798
Weighted TruePositiveRate: 0.8011
Weighted MatthewsCorrelation: 0.79675912603931
Weighted FMeasure: 0.8031664972513852
Iteration time: 343.0
Weighted AreaUnderPRC: 0.843862746101281
Mean absolute error: 0.02840230769231011
Coverage of cases: 95.06
Instances selection time: 65.0
Test time: 203.0
Accumulative iteration time: 343.0
Weighted Recall: 0.8011
Weighted FalsePositiveRate: 0.007959696554495521
Kappa statistic: 0.7931308479212922
Training time: 278.0
		
Iteration: 2
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 13.417307692307583
Incorrectly Classified Instances: 20.46
Correctly Classified Instances: 79.54
Weighted Precision: 0.8136827486424604
Weighted AreaUnderROC: 0.9634395569178164
Root mean squared error: 0.10997307362749063
Relative absolute error: 37.6012000000083
Root relative squared error: 57.185998286299125
Weighted TruePositiveRate: 0.7954
Weighted MatthewsCorrelation: 0.7937890549426018
Weighted FMeasure: 0.7988039507156973
Iteration time: 366.0
Weighted AreaUnderPRC: 0.8428588137635956
Mean absolute error: 0.027811538461540723
Coverage of cases: 94.46
Instances selection time: 63.0
Test time: 205.0
Accumulative iteration time: 709.0
Weighted Recall: 0.7954
Weighted FalsePositiveRate: 0.0081463987783192
Kappa statistic: 0.7872152870860979
Training time: 303.0
		
Iteration: 3
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 13.19692307692309
Incorrectly Classified Instances: 22.06
Correctly Classified Instances: 77.94
Weighted Precision: 0.805603930301456
Weighted AreaUnderROC: 0.9607161033070483
Root mean squared error: 0.11196943264192398
Relative absolute error: 38.16696000000848
Root relative squared error: 58.224104973804536
Weighted TruePositiveRate: 0.7794
Weighted MatthewsCorrelation: 0.7797231834227868
Weighted FMeasure: 0.7837171315282558
Iteration time: 397.0
Weighted AreaUnderPRC: 0.8344042242642421
Mean absolute error: 0.028230000000002337
Coverage of cases: 93.95
Instances selection time: 62.0
Test time: 220.0
Accumulative iteration time: 1106.0
Weighted Recall: 0.7794
Weighted FalsePositiveRate: 0.008817919253424717
Kappa statistic: 0.770566066244857
Training time: 335.0
		
Iteration: 4
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 13.16500000000007
Incorrectly Classified Instances: 21.58
Correctly Classified Instances: 78.42
Weighted Precision: 0.8168216673396468
Weighted AreaUnderROC: 0.9601148433477803
Root mean squared error: 0.11118280165842186
Relative absolute error: 37.73848000000815
Root relative squared error: 57.81505686238339
Weighted TruePositiveRate: 0.7842
Weighted MatthewsCorrelation: 0.7866358965155533
Weighted FMeasure: 0.7888951277431518
Iteration time: 436.0
Weighted AreaUnderPRC: 0.8424396468434139
Mean absolute error: 0.027913076923079063
Coverage of cases: 93.67
Instances selection time: 60.0
Test time: 217.0
Accumulative iteration time: 1542.0
Weighted Recall: 0.7842
Weighted FalsePositiveRate: 0.008623537450629007
Kappa statistic: 0.775559323482551
Training time: 376.0
		
Iteration: 5
Labeled set size: 3200
Unlabelled set size: 6800
	
Mean region size: 12.777692307692448
Incorrectly Classified Instances: 22.5
Correctly Classified Instances: 77.5
Weighted Precision: 0.8190103305085835
Weighted AreaUnderROC: 0.9596383730586043
Root mean squared error: 0.11201819907085374
Relative absolute error: 37.478480000007906
Root relative squared error: 58.24946351684801
Weighted TruePositiveRate: 0.775
Weighted MatthewsCorrelation: 0.781009964908128
Weighted FMeasure: 0.7815068965890861
Iteration time: 467.0
Weighted AreaUnderPRC: 0.8375415499918041
Mean absolute error: 0.027720769230771214
Coverage of cases: 93.58
Instances selection time: 59.0
Test time: 215.0
Accumulative iteration time: 2009.0
Weighted Recall: 0.775
Weighted FalsePositiveRate: 0.009045053916896403
Kappa statistic: 0.7659743150258022
Training time: 408.0
		
Iteration: 6
Labeled set size: 3500
Unlabelled set size: 6500
	
Mean region size: 12.648846153846298
Incorrectly Classified Instances: 22.31
Correctly Classified Instances: 77.69
Weighted Precision: 0.8202875225985848
Weighted AreaUnderROC: 0.9588300209347587
Root mean squared error: 0.11130346597685976
Relative absolute error: 37.092640000007854
Root relative squared error: 57.877802307971116
Weighted TruePositiveRate: 0.7769
Weighted MatthewsCorrelation: 0.781929701176658
Weighted FMeasure: 0.7815135902529242
Iteration time: 493.0
Weighted AreaUnderPRC: 0.8435426203578229
Mean absolute error: 0.027435384615386603
Coverage of cases: 93.28
Instances selection time: 56.0
Test time: 210.0
Accumulative iteration time: 2502.0
Weighted Recall: 0.7769
Weighted FalsePositiveRate: 0.008980019903791136
Kappa statistic: 0.7679454874306076
Training time: 437.0
		
Iteration: 7
Labeled set size: 3800
Unlabelled set size: 6200
	
Mean region size: 12.371153846154108
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8336268586847778
Weighted AreaUnderROC: 0.9651709652057329
Root mean squared error: 0.1083562500415982
Relative absolute error: 35.75520000000731
Root relative squared error: 56.345250021634996
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.8008351813310712
Weighted FMeasure: 0.8011715370290472
Iteration time: 525.0
Weighted AreaUnderPRC: 0.8581130650504565
Mean absolute error: 0.026446153846155562
Coverage of cases: 94.4
Instances selection time: 53.0
Test time: 207.0
Accumulative iteration time: 3027.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.008149385713493256
Kappa statistic: 0.7890697417080981
Training time: 472.0
		
Iteration: 8
Labeled set size: 4100
Unlabelled set size: 5900
	
Mean region size: 12.437692307692608
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8315764089664183
Weighted AreaUnderROC: 0.9658836237789831
Root mean squared error: 0.10794906206169827
Relative absolute error: 35.75624000000713
Root relative squared error: 56.13351227208702
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.799853756876564
Weighted FMeasure: 0.8002389518733165
Iteration time: 590.0
Weighted AreaUnderPRC: 0.8609649164879147
Mean absolute error: 0.02644692307692467
Coverage of cases: 94.5
Instances selection time: 62.0
Test time: 200.0
Accumulative iteration time: 3617.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.008089757316725963
Kappa statistic: 0.7890865651137198
Training time: 528.0
		
Iteration: 9
Labeled set size: 4400
Unlabelled set size: 5600
	
Mean region size: 12.24153846153869
Incorrectly Classified Instances: 19.38
Correctly Classified Instances: 80.62
Weighted Precision: 0.8378896191826499
Weighted AreaUnderROC: 0.968174956093231
Root mean squared error: 0.10564017301124838
Relative absolute error: 34.551920000006724
Root relative squared error: 54.93288996585299
Weighted TruePositiveRate: 0.8062
Weighted MatthewsCorrelation: 0.8078631550656015
Weighted FMeasure: 0.8079433662184389
Iteration time: 632.0
Weighted AreaUnderPRC: 0.8726069919696574
Mean absolute error: 0.025556153846155262
Coverage of cases: 94.75
Instances selection time: 61.0
Test time: 210.0
Accumulative iteration time: 4249.0
Weighted Recall: 0.8062
Weighted FalsePositiveRate: 0.007758076360105901
Kappa statistic: 0.7984398977676346
Training time: 571.0
		
Iteration: 10
Labeled set size: 4700
Unlabelled set size: 5300
	
Mean region size: 12.160000000000366
Incorrectly Classified Instances: 19.31
Correctly Classified Instances: 80.69
Weighted Precision: 0.8387015503796013
Weighted AreaUnderROC: 0.9698870504853154
Root mean squared error: 0.10561213508363207
Relative absolute error: 34.5498400000068
Root relative squared error: 54.918310243492506
Weighted TruePositiveRate: 0.8069
Weighted MatthewsCorrelation: 0.8084328392301949
Weighted FMeasure: 0.8082468494913082
Iteration time: 661.0
Weighted AreaUnderPRC: 0.8755339173751719
Mean absolute error: 0.025554615384616855
Coverage of cases: 95.08
Instances selection time: 56.0
Test time: 208.0
Accumulative iteration time: 4910.0
Weighted Recall: 0.8069
Weighted FalsePositiveRate: 0.007713048083248259
Kappa statistic: 0.7991716470052831
Training time: 605.0
		
Iteration: 11
Labeled set size: 5000
Unlabelled set size: 5000
	
Mean region size: 11.86307692307738
Incorrectly Classified Instances: 18.74
Correctly Classified Instances: 81.26
Weighted Precision: 0.844118964469799
Weighted AreaUnderROC: 0.9694241707620009
Root mean squared error: 0.103672561461557
Relative absolute error: 33.27168000000656
Root relative squared error: 53.9097319600134
Weighted TruePositiveRate: 0.8126
Weighted MatthewsCorrelation: 0.8140841280115525
Weighted FMeasure: 0.8135230849601236
Iteration time: 696.0
Weighted AreaUnderPRC: 0.8802885123571473
Mean absolute error: 0.024609230769232195
Coverage of cases: 94.89
Instances selection time: 44.0
Test time: 214.0
Accumulative iteration time: 5606.0
Weighted Recall: 0.8126
Weighted FalsePositiveRate: 0.007474679679163887
Kappa statistic: 0.8051038719922231
Training time: 652.0
		
Iteration: 12
Labeled set size: 5300
Unlabelled set size: 4700
	
Mean region size: 11.701923076923713
Incorrectly Classified Instances: 19.06
Correctly Classified Instances: 80.94
Weighted Precision: 0.840912762438999
Weighted AreaUnderROC: 0.9689626530455311
Root mean squared error: 0.1042364176726685
Relative absolute error: 33.30288000000633
Root relative squared error: 54.202937189791406
Weighted TruePositiveRate: 0.8094
Weighted MatthewsCorrelation: 0.81031957886384
Weighted FMeasure: 0.8094108982463369
Iteration time: 744.0
Weighted AreaUnderPRC: 0.8769667439991221
Mean absolute error: 0.024632307692308942
Coverage of cases: 94.81
Instances selection time: 54.0
Test time: 210.0
Accumulative iteration time: 6350.0
Weighted Recall: 0.8094
Weighted FalsePositiveRate: 0.007608088533544103
Kappa statistic: 0.8017725403757927
Training time: 690.0
		
Iteration: 13
Labeled set size: 5600
Unlabelled set size: 4400
	
Mean region size: 11.948846153846631
Incorrectly Classified Instances: 18.11
Correctly Classified Instances: 81.89
Weighted Precision: 0.8446297792388267
Weighted AreaUnderROC: 0.9712578471837411
Root mean squared error: 0.10358422808368362
Relative absolute error: 33.516080000006596
Root relative squared error: 53.86379860351924
Weighted TruePositiveRate: 0.8189
Weighted MatthewsCorrelation: 0.8182810824752371
Weighted FMeasure: 0.8181548638862641
Iteration time: 773.0
Weighted AreaUnderPRC: 0.8828272374952798
Mean absolute error: 0.024790000000001422
Coverage of cases: 95.23
Instances selection time: 40.0
Test time: 208.0
Accumulative iteration time: 7123.0
Weighted Recall: 0.8189
Weighted FalsePositiveRate: 0.007236946153803203
Kappa statistic: 0.8116524366147787
Training time: 733.0
		
Iteration: 14
Labeled set size: 5900
Unlabelled set size: 4100
	
Mean region size: 11.572307692308321
Incorrectly Classified Instances: 17.55
Correctly Classified Instances: 82.45
Weighted Precision: 0.8517891492447647
Weighted AreaUnderROC: 0.9702187163437626
Root mean squared error: 0.10194040491309825
Relative absolute error: 32.12040000000601
Root relative squared error: 53.009010554814786
Weighted TruePositiveRate: 0.8245
Weighted MatthewsCorrelation: 0.824026153555933
Weighted FMeasure: 0.8227130484518425
Iteration time: 798.0
Weighted AreaUnderPRC: 0.8864810963772297
Mean absolute error: 0.02375769230769344
Coverage of cases: 94.94
Instances selection time: 38.0
Test time: 204.0
Accumulative iteration time: 7921.0
Weighted Recall: 0.8245
Weighted FalsePositiveRate: 0.00702168954924298
Kappa statistic: 0.8174746674859056
Training time: 760.0
		
Iteration: 15
Labeled set size: 6200
Unlabelled set size: 3800
	
Mean region size: 11.308461538462309
Incorrectly Classified Instances: 16.97
Correctly Classified Instances: 83.03
Weighted Precision: 0.8546957624495805
Weighted AreaUnderROC: 0.9714863943644007
Root mean squared error: 0.1002769242570881
Relative absolute error: 31.102240000005846
Root relative squared error: 52.14400061368946
Weighted TruePositiveRate: 0.8303
Weighted MatthewsCorrelation: 0.8293896306774368
Weighted FMeasure: 0.8287519438538686
Iteration time: 820.0
Weighted AreaUnderPRC: 0.8902063397744999
Mean absolute error: 0.0230046153846165
Coverage of cases: 95.15
Instances selection time: 34.0
Test time: 201.0
Accumulative iteration time: 8741.0
Weighted Recall: 0.8303
Weighted FalsePositiveRate: 0.0067867079344517845
Kappa statistic: 0.823505349540375
Training time: 786.0
		
Iteration: 16
Labeled set size: 6500
Unlabelled set size: 3500
	
Mean region size: 11.215769230770036
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8605125765847766
Weighted AreaUnderROC: 0.975146391944896
Root mean squared error: 0.09807414777370545
Relative absolute error: 30.318080000005722
Root relative squared error: 50.99855684233039
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.8386965986090352
Weighted FMeasure: 0.8385122208729414
Iteration time: 867.0
Weighted AreaUnderPRC: 0.9010713124802091
Mean absolute error: 0.02242461538461649
Coverage of cases: 95.77
Instances selection time: 33.0
Test time: 210.0
Accumulative iteration time: 9608.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.00637186379825772
Kappa statistic: 0.8344225080384648
Training time: 834.0
		
Iteration: 17
Labeled set size: 6800
Unlabelled set size: 3200
	
Mean region size: 11.35153846153921
Incorrectly Classified Instances: 14.94
Correctly Classified Instances: 85.06
Weighted Precision: 0.8664950649216163
Weighted AreaUnderROC: 0.975544444575352
Root mean squared error: 0.09715016132850196
Relative absolute error: 30.330560000005832
Root relative squared error: 50.51808389082454
Weighted TruePositiveRate: 0.8506
Weighted MatthewsCorrelation: 0.8478240004879831
Weighted FMeasure: 0.8482209679399508
Iteration time: 926.0
Weighted AreaUnderPRC: 0.9044953095806184
Mean absolute error: 0.02243384615384734
Coverage of cases: 95.85
Instances selection time: 31.0
Test time: 206.0
Accumulative iteration time: 10534.0
Weighted Recall: 0.8506
Weighted FalsePositiveRate: 0.005974807882839326
Kappa statistic: 0.8446155739376511
Training time: 895.0
		
Iteration: 18
Labeled set size: 7100
Unlabelled set size: 2900
	
Mean region size: 10.923461538462467
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8741169478844382
Weighted AreaUnderROC: 0.9778041496125713
Root mean squared error: 0.09413002463859065
Relative absolute error: 28.618720000005467
Root relative squared error: 48.947612812070545
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.8560788676949156
Weighted FMeasure: 0.8561937077380609
Iteration time: 949.0
Weighted AreaUnderPRC: 0.9130852846316356
Mean absolute error: 0.0211676923076934
Coverage of cases: 96.18
Instances selection time: 29.0
Test time: 200.0
Accumulative iteration time: 11483.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.0056547837278058165
Kappa statistic: 0.8531437019142479
Training time: 920.0
		
Iteration: 19
Labeled set size: 7400
Unlabelled set size: 2600
	
Mean region size: 10.775769230770162
Incorrectly Classified Instances: 13.64
Correctly Classified Instances: 86.36
Weighted Precision: 0.8773131130843618
Weighted AreaUnderROC: 0.9784108944184916
Root mean squared error: 0.09297435543864167
Relative absolute error: 27.98536000000519
Root relative squared error: 48.34666482809705
Weighted TruePositiveRate: 0.8636
Weighted MatthewsCorrelation: 0.8605251576212614
Weighted FMeasure: 0.8607433403741673
Iteration time: 999.0
Weighted AreaUnderPRC: 0.9152698899715278
Mean absolute error: 0.020699230769231726
Coverage of cases: 96.29
Instances selection time: 26.0
Test time: 215.0
Accumulative iteration time: 12482.0
Weighted Recall: 0.8636
Weighted FalsePositiveRate: 0.005449885936364128
Kappa statistic: 0.8581376615907198
Training time: 973.0
		
Iteration: 20
Labeled set size: 7700
Unlabelled set size: 2300
	
Mean region size: 10.729230769231679
Incorrectly Classified Instances: 12.8
Correctly Classified Instances: 87.2
Weighted Precision: 0.8829777676860205
Weighted AreaUnderROC: 0.9811139628389961
Root mean squared error: 0.09056574322642023
Relative absolute error: 27.234480000005238
Root relative squared error: 47.09418647774181
Weighted TruePositiveRate: 0.872
Weighted MatthewsCorrelation: 0.8688176382670663
Weighted FMeasure: 0.8697737047326631
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.9237292077928125
Mean absolute error: 0.02014384615384722
Coverage of cases: 96.78
Instances selection time: 23.0
Test time: 214.0
Accumulative iteration time: 13515.0
Weighted Recall: 0.872
Weighted FalsePositiveRate: 0.005113489309058372
Kappa statistic: 0.8668733831811389
Training time: 1010.0
		
Iteration: 21
Labeled set size: 8000
Unlabelled set size: 2000
	
Mean region size: 10.582307692308637
Incorrectly Classified Instances: 13.13
Correctly Classified Instances: 86.87
Weighted Precision: 0.8775905561465521
Weighted AreaUnderROC: 0.9826047434747461
Root mean squared error: 0.0905292133045381
Relative absolute error: 26.984880000005024
Root relative squared error: 47.0751909183631
Weighted TruePositiveRate: 0.8687
Weighted MatthewsCorrelation: 0.8649472382614415
Weighted FMeasure: 0.8667427146714998
Iteration time: 1087.0
Weighted AreaUnderPRC: 0.9221181338504202
Mean absolute error: 0.019959230769231704
Coverage of cases: 97.12
Instances selection time: 19.0
Test time: 219.0
Accumulative iteration time: 14602.0
Weighted Recall: 0.8687
Weighted FalsePositiveRate: 0.005234737739802424
Kappa statistic: 0.8634436696348966
Training time: 1068.0
		
Iteration: 22
Labeled set size: 8300
Unlabelled set size: 1700
	
Mean region size: 10.453846153847145
Incorrectly Classified Instances: 12.08
Correctly Classified Instances: 87.92
Weighted Precision: 0.8872353024952295
Weighted AreaUnderROC: 0.9833221294416752
Root mean squared error: 0.08844685496865436
Relative absolute error: 26.163280000004857
Root relative squared error: 45.99236458370348
Weighted TruePositiveRate: 0.8792
Weighted MatthewsCorrelation: 0.8759665189637273
Weighted FMeasure: 0.8779475539020277
Iteration time: 1127.0
Weighted AreaUnderPRC: 0.9276372018020562
Mean absolute error: 0.019351538461539357
Coverage of cases: 97.22
Instances selection time: 17.0
Test time: 226.0
Accumulative iteration time: 15729.0
Weighted Recall: 0.8792
Weighted FalsePositiveRate: 0.00482007202024223
Kappa statistic: 0.8743634018010151
Training time: 1110.0
		
Iteration: 23
Labeled set size: 8600
Unlabelled set size: 1400
	
Mean region size: 10.202692307693303
Incorrectly Classified Instances: 11.63
Correctly Classified Instances: 88.37
Weighted Precision: 0.8913669068121546
Weighted AreaUnderROC: 0.9863264615824935
Root mean squared error: 0.08674099376880574
Relative absolute error: 25.224160000004833
Root relative squared error: 45.105316759782134
Weighted TruePositiveRate: 0.8837
Weighted MatthewsCorrelation: 0.8805289100005735
Weighted FMeasure: 0.8824377127306651
Iteration time: 1143.0
Weighted AreaUnderPRC: 0.9334174899296959
Mean absolute error: 0.018656923076924053
Coverage of cases: 97.79
Instances selection time: 14.0
Test time: 229.0
Accumulative iteration time: 16872.0
Weighted Recall: 0.8837
Weighted FalsePositiveRate: 0.0046376439662185
Kappa statistic: 0.8790440926403686
Training time: 1129.0
		
Iteration: 24
Labeled set size: 8900
Unlabelled set size: 1100
	
Mean region size: 10.151923076924065
Incorrectly Classified Instances: 10.34
Correctly Classified Instances: 89.66
Weighted Precision: 0.9028357461039279
Weighted AreaUnderROC: 0.9862121063819109
Root mean squared error: 0.08478615634460807
Relative absolute error: 24.629280000004716
Root relative squared error: 44.088801299199275
Weighted TruePositiveRate: 0.8966
Weighted MatthewsCorrelation: 0.8936491259248873
Weighted FMeasure: 0.8955295544399166
Iteration time: 1195.0
Weighted AreaUnderPRC: 0.93898715181247
Mean absolute error: 0.018216923076924026
Coverage of cases: 97.71
Instances selection time: 11.0
Test time: 235.0
Accumulative iteration time: 18067.0
Weighted Recall: 0.8966
Weighted FalsePositiveRate: 0.004122803276099767
Kappa statistic: 0.8924611165814266
Training time: 1184.0
		
Iteration: 25
Labeled set size: 9200
Unlabelled set size: 800
	
Mean region size: 9.89730769230868
Incorrectly Classified Instances: 11.2
Correctly Classified Instances: 88.8
Weighted Precision: 0.8935519018441687
Weighted AreaUnderROC: 0.9868205423759073
Root mean squared error: 0.08510852800079181
Relative absolute error: 24.25696000000474
Root relative squared error: 44.25643456041483
Weighted TruePositiveRate: 0.888
Weighted MatthewsCorrelation: 0.8845347843976664
Weighted FMeasure: 0.8869539499027829
Iteration time: 1234.0
Weighted AreaUnderPRC: 0.9358874022756672
Mean absolute error: 0.017941538461539466
Coverage of cases: 97.87
Instances selection time: 8.0
Test time: 231.0
Accumulative iteration time: 19301.0
Weighted Recall: 0.888
Weighted FalsePositiveRate: 0.004461564561284203
Kappa statistic: 0.8835157234428642
Training time: 1226.0
		
Iteration: 26
Labeled set size: 9500
Unlabelled set size: 500
	
Mean region size: 9.931153846154878
Incorrectly Classified Instances: 9.96
Correctly Classified Instances: 90.04
Weighted Precision: 0.9046439565640516
Weighted AreaUnderROC: 0.988853504254287
Root mean squared error: 0.08310003239840172
Relative absolute error: 23.77648000000471
Root relative squared error: 43.21201684717192
Weighted TruePositiveRate: 0.9004
Weighted MatthewsCorrelation: 0.8973465587049116
Weighted FMeasure: 0.8999298486827098
Iteration time: 1277.0
Weighted AreaUnderPRC: 0.9425743853488538
Mean absolute error: 0.01758615384615488
Coverage of cases: 98.24
Instances selection time: 5.0
Test time: 224.0
Accumulative iteration time: 20578.0
Weighted Recall: 0.9004
Weighted FalsePositiveRate: 0.003972424573566104
Kappa statistic: 0.896412453328142
Training time: 1272.0
		
Iteration: 27
Labeled set size: 9800
Unlabelled set size: 200
	
Mean region size: 9.65884615384715
Incorrectly Classified Instances: 8.89
Correctly Classified Instances: 91.11
Weighted Precision: 0.913258580612101
Weighted AreaUnderROC: 0.9914491240361163
Root mean squared error: 0.07979010927716244
Relative absolute error: 22.40160000000444
Root relative squared error: 41.49085682412736
Weighted TruePositiveRate: 0.9111
Weighted MatthewsCorrelation: 0.9083368473150356
Weighted FMeasure: 0.9115352278170389
Iteration time: 1319.0
Weighted AreaUnderPRC: 0.950315737678597
Mean absolute error: 0.016569230769231745
Coverage of cases: 98.71
Instances selection time: 17.0
Test time: 206.0
Accumulative iteration time: 21897.0
Weighted Recall: 0.9111
Weighted FalsePositiveRate: 0.003546460851979922
Kappa statistic: 0.9075410401298095
Training time: 1302.0
		
Time end:Sun Oct 08 02.47.19 EEST 2017