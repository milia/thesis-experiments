Sun Oct 08 09.58.13 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.13 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9356481908046279
Weighted AreaUnderROC: 0.9299928926794598
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8639864565185936
Weighted FMeasure: 0.9356308004699737
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9067901597585912
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07579403115484178
Kappa statistic: 0.8638229519900055
Training time: 8.0
		
Time end:Sun Oct 08 09.58.14 EEST 2017