Sun Oct 08 09.58.14 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.14 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9446163708948918
Weighted AreaUnderROC: 0.9395363408521303
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831133236786068
Weighted FMeasure: 0.944573672639523
Iteration time: 9.0
Weighted AreaUnderPRC: 0.919087716866961
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06562777912523243
Kappa statistic: 0.8829452485840151
Training time: 8.0
		
Time end:Sun Oct 08 09.58.14 EEST 2017