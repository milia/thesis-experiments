Sun Oct 08 09.57.56 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.56 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9399997679693255
Weighted AreaUnderROC: 0.9357769423558896
Root mean squared error: 0.24476076912238712
Relative absolute error: 11.981566820276496
Root relative squared error: 48.95215382447742
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8735107641897845
Weighted FMeasure: 0.9400249880811048
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9133018587822286
Mean absolute error: 0.059907834101382486
Coverage of cases: 94.00921658986175
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.06853828118683805
Kappa statistic: 0.8734693877551021
Training time: 7.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9262302801977866
Weighted AreaUnderROC: 0.9179197994987469
Root mean squared error: 0.27153769332085226
Relative absolute error: 14.746543778801843
Root relative squared error: 54.30753866417045
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8438847310888113
Weighted FMeasure: 0.925914269895387
Iteration time: 9.0
Weighted AreaUnderPRC: 0.892757207852873
Mean absolute error: 0.07373271889400922
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.09042768210849705
Kappa statistic: 0.843236409608091
Training time: 8.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9446163708948918
Weighted AreaUnderROC: 0.9395363408521303
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831133236786068
Weighted FMeasure: 0.944573672639523
Iteration time: 10.0
Weighted AreaUnderPRC: 0.919087716866961
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06562777912523243
Kappa statistic: 0.8829452485840151
Training time: 9.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9446163708948918
Weighted AreaUnderROC: 0.9395363408521303
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831133236786068
Weighted FMeasure: 0.944573672639523
Iteration time: 39.0
Weighted AreaUnderPRC: 0.919087716866961
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06562777912523243
Kappa statistic: 0.8829452485840151
Training time: 38.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9492374891960718
Weighted AreaUnderROC: 0.9454887218045114
Root mean squared error: 0.22514716129596513
Relative absolute error: 10.138248847926267
Root relative squared error: 45.02943225919302
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8929779343478601
Weighted FMeasure: 0.9492519129917041
Iteration time: 22.0
Weighted AreaUnderPRC: 0.926068932303634
Mean absolute error: 0.05069124423963134
Coverage of cases: 94.93087557603687
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.05833131215134611
Kappa statistic: 0.8929356357927787
Training time: 15.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9447004608294931
Weighted AreaUnderROC: 0.9417293233082706
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834586466165414
Weighted FMeasure: 0.9447004608294931
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9202730328124459
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 112.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06124181421295173
Kappa statistic: 0.8834586466165414
Training time: 23.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9538770871579683
Weighted AreaUnderROC: 0.949248120300752
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9026261626194743
Weighted FMeasure: 0.9538113938662691
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9319630514574806
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 136.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.05542081008974049
Kappa statistic: 0.9024543738200125
Training time: 18.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.923426739760783
Weighted AreaUnderROC: 0.9229323308270677
Root mean squared error: 0.27989464772461226
Relative absolute error: 15.668202764976957
Root relative squared error: 55.97892954492245
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.837657259920533
Weighted FMeasure: 0.9220406888978905
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8926512289899999
Mean absolute error: 0.07834101382488479
Coverage of cases: 92.16589861751152
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 160.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.07579432452097987
Kappa statistic: 0.8366904245429192
Training time: 18.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9316676649268418
Weighted AreaUnderROC: 0.9304511278195489
Root mean squared error: 0.26291524102480945
Relative absolute error: 13.82488479262673
Root relative squared error: 52.58304820496189
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8556339470516587
Weighted FMeasure: 0.9310876848689861
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9034408628437838
Mean absolute error: 0.06912442396313365
Coverage of cases: 93.08755760368663
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 191.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.06997332039776862
Kappa statistic: 0.8552754435107376
Training time: 27.0
		
Time end:Sun Oct 08 09.57.57 EEST 2017