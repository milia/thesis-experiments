Sun Oct 08 09.57.37 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.37 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9356481908046279
Weighted AreaUnderROC: 0.9299928926794598
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8639864565185936
Weighted FMeasure: 0.9356308004699737
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9067901597585912
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07579403115484178
Kappa statistic: 0.8638229519900055
Training time: 10.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9402732233725933
Weighted AreaUnderROC: 0.9359452736318408
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8738725167000166
Weighted FMeasure: 0.9402992725370855
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9136789862552267
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.06847642521338262
Kappa statistic: 0.8738313596295967
Training time: 7.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9402732233725933
Weighted AreaUnderROC: 0.9359452736318408
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8738725167000166
Weighted FMeasure: 0.9402992725370855
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9136789862552267
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.06847642521338262
Kappa statistic: 0.8738313596295967
Training time: 6.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9284825870646767
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9024589042577282
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 7.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9284825870646767
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9024589042577282
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 9.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9284825870646767
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9024589042577282
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 6.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9222281283348684
Weighted AreaUnderROC: 0.9187988628287137
Root mean squared error: 0.2792519496371454
Relative absolute error: 15.59633027522936
Root relative squared error: 55.85038992742908
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.835780527149313
Weighted FMeasure: 0.9221030305137415
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8901322159773943
Mean absolute error: 0.0779816513761468
Coverage of cases: 92.20183486238533
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 60.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.08442062296642605
Kappa statistic: 0.8357415122772804
Training time: 6.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9319859644377685
Weighted AreaUnderROC: 0.9307036247334755
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8560621400011506
Weighted FMeasure: 0.9314067581972066
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9038985336711546
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06978541108350775
Kappa statistic: 0.8557055864442679
Training time: 8.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9483604637174641
Weighted AreaUnderROC: 0.9507818052594172
Root mean squared error: 0.2346185661017325
Relative absolute error: 11.009174311926607
Root relative squared error: 46.9237132203465
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8884013914874982
Weighted FMeasure: 0.9453583114133572
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9267712557162647
Mean absolute error: 0.05504587155963303
Coverage of cases: 94.4954128440367
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.04339051792153257
Kappa statistic: 0.8858240223463687
Training time: 11.0
		
Time end:Sun Oct 08 09.57.37 EEST 2017