Sun Oct 08 09.57.05 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.05 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 70.96774193548387
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9611976369495165
Root mean squared error: 0.20528661780429514
Relative absolute error: 19.31197084985534
Root relative squared error: 41.057323560859025
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9424316479990247
Mean absolute error: 0.0965598542492767
Coverage of cases: 99.07834101382488
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 7.0
		
Time end:Sun Oct 08 09.57.05 EEST 2017