Sun Oct 08 09.57.45 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.45 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 71.42857142857143
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9678540624460927
Weighted AreaUnderROC: 0.9778464017185821
Root mean squared error: 0.1762648527992323
Relative absolute error: 18.072375236666296
Root relative squared error: 35.25297055984646
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9322100732143521
Weighted FMeasure: 0.9677765092718976
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9662838345062744
Mean absolute error: 0.09036187618333148
Coverage of cases: 99.53917050691244
Instances selection time: 2.0
Test time: 0.0
Accumulative iteration time: 2.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.03353140916808149
Kappa statistic: 0.9321663019693655
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 100.0
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9396737115313311
Weighted AreaUnderROC: 0.9858575008950948
Root mean squared error: 0.22319969152893226
Relative absolute error: 25.0036503378579
Root relative squared error: 44.63993830578645
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.8665901221878949
Weighted FMeasure: 0.9343949158291235
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9805046418115921
Mean absolute error: 0.12501825168928948
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.09776457272212791
Kappa statistic: 0.8603603603603602
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 98.38709677419355
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9499957217421067
Weighted AreaUnderROC: 0.9840225563909775
Root mean squared error: 0.19755042093502986
Relative absolute error: 21.956809008349115
Root relative squared error: 39.51008418700597
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.894243339131137
Weighted FMeasure: 0.9494643022372562
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9778269826026348
Mean absolute error: 0.10978404504174558
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0495593823267847
Kappa statistic: 0.8938686585745409
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 84.33179723502305
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9591597501497389
Weighted AreaUnderROC: 0.9867525957751522
Root mean squared error: 0.17930295713432914
Relative absolute error: 18.38342958227677
Root relative squared error: 35.86059142686583
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9135480351708758
Weighted FMeasure: 0.9586526109213916
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9819208163466772
Mean absolute error: 0.09191714791138383
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03935241329129274
Kappa statistic: 0.9131652661064424
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9634584218573727
Weighted AreaUnderROC: 0.9865735767991407
Root mean squared error: 0.1753868935952457
Relative absolute error: 15.947573613528752
Root relative squared error: 35.07737871904914
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.922817617517183
Weighted FMeasure: 0.9632108561899833
Iteration time: 1.0
Weighted AreaUnderPRC: 0.981751472647059
Mean absolute error: 0.07973786806764376
Coverage of cases: 99.07834101382488
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.036441911229687124
Kappa statistic: 0.9226450405489708
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9591597501497389
Weighted AreaUnderROC: 0.9869316147511636
Root mean squared error: 0.17486956718003321
Relative absolute error: 15.028722198396723
Root relative squared error: 34.97391343600664
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9135480351708758
Weighted FMeasure: 0.9586526109213916
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9820995275149814
Mean absolute error: 0.07514361099198362
Coverage of cases: 99.07834101382488
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03935241329129274
Kappa statistic: 0.9131652661064424
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9634584218573727
Weighted AreaUnderROC: 0.9867525957751522
Root mean squared error: 0.1737031354078274
Relative absolute error: 14.136778290363445
Root relative squared error: 34.74062708156548
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.922817617517183
Weighted FMeasure: 0.9632108561899833
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9819301838153632
Mean absolute error: 0.07068389145181722
Coverage of cases: 99.07834101382488
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.036441911229687124
Kappa statistic: 0.9226450405489708
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9584752104228179
Weighted AreaUnderROC: 0.9867525957751522
Root mean squared error: 0.17333809101631573
Relative absolute error: 13.596196423958853
Root relative squared error: 34.667618203263146
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9124451045059361
Weighted FMeasure: 0.9584788379023034
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9819301838153632
Mean absolute error: 0.06798098211979427
Coverage of cases: 99.07834101382488
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.048124343115854146
Kappa statistic: 0.9124018838304552
Training time: 2.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9584752104228179
Weighted AreaUnderROC: 0.9867525957751522
Root mean squared error: 0.1733520130892025
Relative absolute error: 13.224545240252834
Root relative squared error: 34.6704026178405
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9124451045059361
Weighted FMeasure: 0.9584788379023034
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9819301838153632
Mean absolute error: 0.06612272620126416
Coverage of cases: 99.07834101382488
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.048124343115854146
Kappa statistic: 0.9124018838304552
Training time: 1.0
		
Time end:Sun Oct 08 09.57.45 EEST 2017