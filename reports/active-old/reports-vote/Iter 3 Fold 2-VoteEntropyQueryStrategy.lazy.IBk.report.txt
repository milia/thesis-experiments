Sun Oct 08 09.58.14 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.14 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 52.07373271889401
Incorrectly Classified Instances: 8.755760368663594
Correctly Classified Instances: 91.2442396313364
Weighted Precision: 0.9170135275754423
Weighted AreaUnderROC: 0.9458020050125313
Root mean squared error: 0.2845228511499447
Relative absolute error: 20.532294740958108
Root relative squared error: 56.904570229988934
Weighted TruePositiveRate: 0.9124423963133641
Weighted MatthewsCorrelation: 0.8220730463695131
Weighted FMeasure: 0.9131353272928798
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9323469342886104
Mean absolute error: 0.10266147370479053
Coverage of cases: 92.62672811059907
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9124423963133641
Weighted FalsePositiveRate: 0.07722936373191042
Kappa statistic: 0.8190476190476191
Training time: 0.0
		
Time end:Sun Oct 08 09.58.14 EEST 2017