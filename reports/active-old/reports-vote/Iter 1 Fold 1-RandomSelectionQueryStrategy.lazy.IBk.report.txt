Sun Oct 08 09.58.00 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.00 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 54.12844036697248
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9033460568316257
Weighted AreaUnderROC: 0.9299484719260839
Root mean squared error: 0.2807969450362124
Relative absolute error: 21.778841059948107
Root relative squared error: 56.159389007242474
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.7956180344188749
Weighted FMeasure: 0.9033271884011586
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9135155191150587
Mean absolute error: 0.10889420529974053
Coverage of cases: 93.57798165137615
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.11369104673226267
Kappa statistic: 0.7952777032465791
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9034921870887914
Weighted AreaUnderROC: 0.9333244491826581
Root mean squared error: 0.2815829809708748
Relative absolute error: 22.04074111859871
Root relative squared error: 56.31659619417496
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.7953119766793095
Weighted FMeasure: 0.9030744119391613
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9263024023040803
Mean absolute error: 0.11020370559299356
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.11813312206986039
Kappa statistic: 0.7943580990027851
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9174311926605505
Weighted AreaUnderROC: 0.9371446339729922
Root mean squared error: 0.27335230936086946
Relative absolute error: 19.574619122619787
Root relative squared error: 54.670461872173895
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8256929637526652
Weighted FMeasure: 0.9174311926605505
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9241452488790266
Mean absolute error: 0.09787309561309894
Coverage of cases: 94.03669724770643
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.0917382289078852
Kappa statistic: 0.8256929637526653
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 52.75229357798165
Incorrectly Classified Instances: 8.715596330275229
Correctly Classified Instances: 91.28440366972477
Weighted Precision: 0.9130732930545467
Weighted AreaUnderROC: 0.9322139303482587
Root mean squared error: 0.2778097494707123
Relative absolute error: 18.708161884059997
Root relative squared error: 55.56194989414246
Weighted TruePositiveRate: 0.9128440366972477
Weighted MatthewsCorrelation: 0.8164550970561277
Weighted FMeasure: 0.912938681162417
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9143515031621857
Mean absolute error: 0.09354080942029998
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9128440366972477
Weighted FalsePositiveRate: 0.09461375951174664
Kappa statistic: 0.8164169843099015
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 52.522935779816514
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9174311926605505
Weighted AreaUnderROC: 0.9360785358919688
Root mean squared error: 0.27464121771041244
Relative absolute error: 17.985128968262476
Root relative squared error: 54.92824354208249
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8256929637526652
Weighted FMeasure: 0.9174311926605505
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9205056813195123
Mean absolute error: 0.08992564484131238
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.0917382289078852
Kappa statistic: 0.8256929637526653
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 52.293577981651374
Incorrectly Classified Instances: 8.715596330275229
Correctly Classified Instances: 91.28440366972477
Weighted Precision: 0.9130732930545467
Weighted AreaUnderROC: 0.9397210376687989
Root mean squared error: 0.27718165252391275
Relative absolute error: 17.943351587582647
Root relative squared error: 55.436330504782546
Weighted TruePositiveRate: 0.9128440366972477
Weighted MatthewsCorrelation: 0.8164550970561277
Weighted FMeasure: 0.912938681162417
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9268970054585347
Mean absolute error: 0.08971675793791324
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9128440366972477
Weighted FalsePositiveRate: 0.09461375951174664
Kappa statistic: 0.8164169843099015
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9034908236411497
Weighted AreaUnderROC: 0.9340351812366737
Root mean squared error: 0.29119181783545617
Relative absolute error: 19.081352716584217
Root relative squared error: 58.238363567091234
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.7962266195569134
Weighted FMeasure: 0.903560363329138
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9175263841412843
Mean absolute error: 0.09540676358292108
Coverage of cases: 92.66055045871559
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.10924897139466495
Kappa statistic: 0.7961891194016562
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 51.60550458715596
Incorrectly Classified Instances: 9.174311926605505
Correctly Classified Instances: 90.8256880733945
Weighted Precision: 0.908256880733945
Weighted AreaUnderROC: 0.9338130774697939
Root mean squared error: 0.2870220280449796
Relative absolute error: 18.335180031247695
Root relative squared error: 57.40440560899592
Weighted TruePositiveRate: 0.908256880733945
Weighted MatthewsCorrelation: 0.8063255152807391
Weighted FMeasure: 0.908256880733945
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9162993405263494
Mean absolute error: 0.09167590015623849
Coverage of cases: 92.66055045871559
Instances selection time: 4.0
Test time: 27.0
Accumulative iteration time: 18.0
Weighted Recall: 0.908256880733945
Weighted FalsePositiveRate: 0.10193136545320579
Kappa statistic: 0.8063255152807391
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9174311926605505
Weighted AreaUnderROC: 0.9398098791755508
Root mean squared error: 0.2720720617239622
Relative absolute error: 16.82675206542941
Root relative squared error: 54.41441234479244
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8256929637526652
Weighted FMeasure: 0.9174311926605505
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9244275355969969
Mean absolute error: 0.08413376032714705
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.0917382289078852
Kappa statistic: 0.8256929637526653
Training time: 0.0
		
Time end:Sun Oct 08 09.58.01 EEST 2017