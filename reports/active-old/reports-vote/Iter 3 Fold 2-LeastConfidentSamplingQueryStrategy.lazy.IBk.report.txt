Sun Oct 08 09.57.20 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.20 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 52.07373271889401
Incorrectly Classified Instances: 8.755760368663594
Correctly Classified Instances: 91.2442396313364
Weighted Precision: 0.9170135275754423
Weighted AreaUnderROC: 0.9458020050125313
Root mean squared error: 0.2845228511499447
Relative absolute error: 20.532294740958108
Root relative squared error: 56.904570229988934
Weighted TruePositiveRate: 0.9124423963133641
Weighted MatthewsCorrelation: 0.8220730463695131
Weighted FMeasure: 0.9131353272928798
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9323469342886104
Mean absolute error: 0.10266147370479053
Coverage of cases: 92.62672811059907
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9124423963133641
Weighted FalsePositiveRate: 0.07722936373191042
Kappa statistic: 0.8190476190476191
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 55.29953917050691
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.8960136145342515
Weighted AreaUnderROC: 0.9296007876834944
Root mean squared error: 0.29533815103230787
Relative absolute error: 22.985061722832537
Root relative squared error: 59.06763020646157
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.7799519820148965
Weighted FMeasure: 0.8945256379206754
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9135623862322655
Mean absolute error: 0.11492530861416268
Coverage of cases: 92.62672811059907
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.10641523162745573
Kappa statistic: 0.7790517508521847
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9275046034235832
Weighted AreaUnderROC: 0.9518583324671125
Root mean squared error: 0.2508902571964649
Relative absolute error: 16.549327525666044
Root relative squared error: 50.17805143929298
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8465906990559555
Weighted FMeasure: 0.9265617931200358
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9355009442359955
Mean absolute error: 0.08274663762833022
Coverage of cases: 94.47004608294931
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.07288382245937425
Kappa statistic: 0.8459627329192546
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9275046034235832
Weighted AreaUnderROC: 0.9678213390619406
Root mean squared error: 0.25679984805955164
Relative absolute error: 17.178344070781677
Root relative squared error: 51.35996961191033
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8465906990559555
Weighted FMeasure: 0.9265617931200358
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9561842940040727
Mean absolute error: 0.08589172035390838
Coverage of cases: 94.93087557603687
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.07288382245937425
Kappa statistic: 0.8459627329192546
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.923426739760783
Weighted AreaUnderROC: 0.9532904842752041
Root mean squared error: 0.2766071270670423
Relative absolute error: 19.096735023193705
Root relative squared error: 55.32142541340846
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.837657259920533
Weighted FMeasure: 0.9220406888978905
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9421373257015101
Mean absolute error: 0.09548367511596853
Coverage of cases: 93.54838709677419
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.07579432452097987
Kappa statistic: 0.8366904245429192
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9285581237829142
Weighted AreaUnderROC: 0.9512620837808807
Root mean squared error: 0.27376661153219994
Relative absolute error: 18.253514707006623
Root relative squared error: 54.75332230643999
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8480333063688249
Weighted FMeasure: 0.9266877727389408
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9401698617773855
Mean absolute error: 0.09126757353503312
Coverage of cases: 93.54838709677419
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06849785754709353
Kappa statistic: 0.8466295609152752
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9285581237829142
Weighted AreaUnderROC: 0.947234156820623
Root mean squared error: 0.2749439907380753
Relative absolute error: 18.161973021922346
Root relative squared error: 54.98879814761506
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8480333063688249
Weighted FMeasure: 0.9266877727389408
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9356317728838877
Mean absolute error: 0.09080986510961174
Coverage of cases: 93.54838709677419
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06849785754709353
Kappa statistic: 0.8466295609152752
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 52.764976958525345
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9285581237829142
Weighted AreaUnderROC: 0.943295739348371
Root mean squared error: 0.2758995017778832
Relative absolute error: 17.72981700308134
Root relative squared error: 55.17990035557664
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8480333063688249
Weighted FMeasure: 0.9266877727389408
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9324944161763942
Mean absolute error: 0.0886490850154067
Coverage of cases: 93.08755760368663
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06849785754709353
Kappa statistic: 0.8466295609152752
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9246327149552955
Weighted AreaUnderROC: 0.9464285714285714
Root mean squared error: 0.27997482435897236
Relative absolute error: 18.293982963577715
Root relative squared error: 55.99496487179447
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8392791910875476
Weighted FMeasure: 0.9221671441026281
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9416119702471234
Mean absolute error: 0.09146991481788858
Coverage of cases: 93.08755760368663
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.07140835960869917
Kappa statistic: 0.8373958654736193
Training time: 0.0
		
Time end:Sun Oct 08 09.57.21 EEST 2017