Sun Oct 08 09.57.25 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.25 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 70.87155963302752
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9593495149616872
Weighted AreaUnderROC: 0.9650408670931058
Root mean squared error: 0.1969112586335136
Relative absolute error: 14.162352756223795
Root relative squared error: 39.38225172670272
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9138039551373668
Weighted FMeasure: 0.9588440549183241
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9483321163935471
Mean absolute error: 0.07081176378111897
Coverage of cases: 99.08256880733946
Instances selection time: 6.0
Test time: 0.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03920600144754602
Kappa statistic: 0.9134233518665609
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 67.66055045871559
Incorrectly Classified Instances: 12.844036697247706
Correctly Classified Instances: 87.1559633027523
Weighted Precision: 0.8713828115967654
Weighted AreaUnderROC: 0.8479033404406539
Root mean squared error: 0.3427660058408665
Relative absolute error: 34.124183665468024
Root relative squared error: 68.55320116817329
Weighted TruePositiveRate: 0.8715596330275229
Weighted MatthewsCorrelation: 0.7262130843280572
Weighted FMeasure: 0.8702100784470149
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8205637313174767
Mean absolute error: 0.1706209183273401
Coverage of cases: 91.74311926605505
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8715596330275229
Weighted FalsePositiveRate: 0.16047221298487901
Kappa statistic: 0.7239507959479016
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 54.12844036697248
Incorrectly Classified Instances: 13.302752293577981
Correctly Classified Instances: 86.69724770642202
Weighted Precision: 0.8863393025843428
Weighted AreaUnderROC: 0.9732587064676617
Root mean squared error: 0.32560884218737424
Relative absolute error: 28.439856422347727
Root relative squared error: 65.12176843747484
Weighted TruePositiveRate: 0.8669724770642202
Weighted MatthewsCorrelation: 0.73006494748098
Weighted FMeasure: 0.8603234995384722
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9654844764198711
Mean absolute error: 0.14219928211173863
Coverage of cases: 91.74311926605505
Instances selection time: 4.0
Test time: 0.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8669724770642202
Weighted FalsePositiveRate: 0.20776849696471766
Kappa statistic: 0.7012569700406389
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.972171416834569
Root mean squared error: 0.18766872550679434
Relative absolute error: 14.339575817390173
Root relative squared error: 37.53374510135887
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9649681838567061
Mean absolute error: 0.07169787908695087
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 15.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9588474694561553
Weighted AreaUnderROC: 0.9688740928385595
Root mean squared error: 0.18794391163856097
Relative absolute error: 13.452443501253756
Root relative squared error: 37.588782327712195
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9130822475220538
Weighted FMeasure: 0.9587604279190397
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9608479737114426
Mean absolute error: 0.06726221750626878
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.04364807678514374
Kappa statistic: 0.9130396241467955
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9588474694561553
Weighted AreaUnderROC: 0.9763820152187945
Root mean squared error: 0.18792857019118187
Relative absolute error: 12.95073321411401
Root relative squared error: 37.585714038236375
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9130822475220538
Weighted FMeasure: 0.9587604279190397
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9673815547835356
Mean absolute error: 0.06475366607057005
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.04364807678514374
Kappa statistic: 0.9130396241467955
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.9764227682035432
Root mean squared error: 0.18786579935453424
Relative absolute error: 12.616393065198626
Root relative squared error: 37.57315987090685
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9674708135193573
Mean absolute error: 0.06308196532599313
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 20.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.9763750872113873
Root mean squared error: 0.18801088130320312
Relative absolute error: 12.353892583631607
Root relative squared error: 37.602176260640626
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9673660848033719
Mean absolute error: 0.06176946291815803
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9586644232383151
Weighted AreaUnderROC: 0.9687787308542478
Root mean squared error: 0.18824990462299296
Relative absolute error: 12.142315849069865
Root relative squared error: 37.649980924598594
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9126954652715682
Weighted FMeasure: 0.9586687271410591
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9606551174326038
Mean absolute error: 0.060711579245349326
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.048090152122741474
Kappa statistic: 0.912652479743567
Training time: 0.0
		
Time end:Sun Oct 08 09.57.25 EEST 2017