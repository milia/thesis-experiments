Sun Oct 08 09.57.57 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.57 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.9264294552011899
Weighted AreaUnderROC: 0.9203091684434968
Root mean squared error: 0.27091418459143857
Relative absolute error: 14.678899082568808
Root relative squared error: 54.18283691828771
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.84452891028708
Weighted FMeasure: 0.9264352005371128
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8943293184070584
Mean absolute error: 0.07339449541284404
Coverage of cases: 92.66055045871559
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.08598716770016235
Kappa statistic: 0.8443690879885776
Training time: 19.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9403210885163606
Weighted AreaUnderROC: 0.9337242359630419
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8736408556079505
Weighted FMeasure: 0.940154926153098
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9125310025813416
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.07291850055098036
Kappa statistic: 0.8732671496288347
Training time: 15.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.926605504587156
Weighted AreaUnderROC: 0.9225302061122957
Root mean squared error: 0.27091418459143857
Relative absolute error: 14.678899082568808
Root relative squared error: 54.18283691828771
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.8450604122245915
Weighted FMeasure: 0.926605504587156
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8955941133128591
Mean absolute error: 0.07339449541284404
Coverage of cases: 92.66055045871559
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 64.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.08154509236256464
Kappa statistic: 0.8450604122245913
Training time: 13.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.926605504587156
Weighted AreaUnderROC: 0.9225302061122957
Root mean squared error: 0.27091418459143857
Relative absolute error: 14.678899082568808
Root relative squared error: 54.18283691828771
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.8450604122245915
Weighted FMeasure: 0.926605504587156
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8955941133128591
Mean absolute error: 0.07339449541284404
Coverage of cases: 92.66055045871559
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 84.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.08154509236256464
Kappa statistic: 0.8450604122245913
Training time: 14.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9411071479457414
Weighted AreaUnderROC: 0.9403873489694384
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8753094117132226
Weighted FMeasure: 0.9405525237709126
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9163784797055607
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 104.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05959227453818717
Kappa statistic: 0.8749448415850323
Training time: 15.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9284825870646767
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9024589042577282
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 136.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 26.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9319859644377685
Weighted AreaUnderROC: 0.9307036247334755
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8560621400011506
Weighted FMeasure: 0.9314067581972066
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9038985336711546
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 165.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06978541108350775
Kappa statistic: 0.8557055864442679
Training time: 24.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9284825870646767
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9024589042577282
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 193.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 23.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.936216226700545
Weighted AreaUnderROC: 0.9344349680170575
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8651882672018902
Weighted FMeasure: 0.9359161530377482
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9093865647587341
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 229.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.06690988047964633
Kappa statistic: 0.8650274190695207
Training time: 30.0
		
Time end:Sun Oct 08 09.57.58 EEST 2017