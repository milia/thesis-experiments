Sun Oct 08 09.58.15 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.15 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 57.6036866359447
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9767722878625134
Root mean squared error: 0.20771792791432264
Relative absolute error: 13.609100964512688
Root relative squared error: 41.54358558286453
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9646467861106518
Mean absolute error: 0.06804550482256344
Coverage of cases: 97.23502304147465
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 5.0
		
Time end:Sun Oct 08 09.58.15 EEST 2017