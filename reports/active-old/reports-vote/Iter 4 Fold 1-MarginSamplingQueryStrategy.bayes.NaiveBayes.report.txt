Sun Oct 08 09.57.32 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.32 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9046709022561126
Weighted AreaUnderROC: 0.9704157782515991
Root mean squared error: 0.30507279188287084
Relative absolute error: 20.853819523510577
Root relative squared error: 61.01455837657417
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7951630856759044
Weighted FMeasure: 0.8999615639401037
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9722622326412015
Mean absolute error: 0.10426909761755289
Coverage of cases: 92.66055045871559
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.08991412531053773
Kappa statistic: 0.7915869980879542
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 55.27522935779817
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9237863274704359
Weighted AreaUnderROC: 0.9838308457711443
Root mean squared error: 0.245789021781101
Relative absolute error: 15.48901665798635
Root relative squared error: 49.157804356220204
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8381463301648796
Weighted FMeasure: 0.9224039806500435
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9845573543819428
Mean absolute error: 0.07744508328993174
Coverage of cases: 96.3302752293578
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.0755364722912306
Kappa statistic: 0.8371847816536333
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9402732233725933
Weighted AreaUnderROC: 0.9856965174129353
Root mean squared error: 0.2220027487748254
Relative absolute error: 13.768783704095855
Root relative squared error: 44.40054975496508
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8738725167000166
Weighted FMeasure: 0.9402992725370855
Iteration time: 4.0
Weighted AreaUnderPRC: 0.98635674088819
Mean absolute error: 0.06884391852047927
Coverage of cases: 97.24770642201835
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.06847642521338262
Kappa statistic: 0.8738313596295967
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 56.19266055045872
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9818763326226013
Root mean squared error: 0.2393092822640984
Relative absolute error: 15.653058202027115
Root relative squared error: 47.86185645281968
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9827219227585124
Mean absolute error: 0.07826529101013557
Coverage of cases: 96.78899082568807
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9218335726739931
Weighted AreaUnderROC: 0.9799218194740584
Root mean squared error: 0.2525029223317347
Relative absolute error: 16.73448465995878
Root relative squared error: 50.50058446634694
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8346294450134126
Weighted FMeasure: 0.9217410572771282
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9808657644432359
Mean absolute error: 0.08367242329979391
Coverage of cases: 96.78899082568807
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.0933047736416215
Kappa statistic: 0.8342724264377068
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 8.715596330275229
Correctly Classified Instances: 91.28440366972477
Weighted Precision: 0.9127918197319974
Weighted AreaUnderROC: 0.9769012082444918
Root mean squared error: 0.2676847599604456
Relative absolute error: 17.942967571140443
Root relative squared error: 53.53695199208912
Weighted TruePositiveRate: 0.9128440366972477
Weighted MatthewsCorrelation: 0.814920437538401
Weighted FMeasure: 0.9123054203259076
Iteration time: 1.0
Weighted AreaUnderPRC: 0.977885069540145
Mean absolute error: 0.08971483785570222
Coverage of cases: 95.87155963302752
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9128440366972477
Weighted FalsePositiveRate: 0.10793998552453982
Kappa statistic: 0.8139430419549006
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 55.5045871559633
Incorrectly Classified Instances: 9.174311926605505
Correctly Classified Instances: 90.8256880733945
Weighted Precision: 0.9080175508575987
Weighted AreaUnderROC: 0.9754797441364605
Root mean squared error: 0.275101967874827
Relative absolute error: 18.490452957545063
Root relative squared error: 55.0203935749654
Weighted TruePositiveRate: 0.908256880733945
Weighted MatthewsCorrelation: 0.8052046328654509
Weighted FMeasure: 0.9078126439656193
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9764144977579658
Mean absolute error: 0.09245226478772532
Coverage of cases: 95.87155963302752
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.908256880733945
Weighted FalsePositiveRate: 0.11081551612840124
Kappa statistic: 0.8045894585873072
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 11.009174311926605
Correctly Classified Instances: 88.9908256880734
Weighted Precision: 0.8899082568807339
Weighted AreaUnderROC: 0.9736140724946695
Root mean squared error: 0.2899177125554101
Relative absolute error: 19.853170977109556
Root relative squared error: 57.98354251108202
Weighted TruePositiveRate: 0.8899082568807339
Weighted MatthewsCorrelation: 0.767590618336887
Weighted FMeasure: 0.8899082568807339
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9744758114768017
Mean absolute error: 0.09926585488554779
Coverage of cases: 95.41284403669725
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8899082568807339
Weighted FalsePositiveRate: 0.12231763854384695
Kappa statistic: 0.767590618336887
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 54.12844036697248
Incorrectly Classified Instances: 11.46788990825688
Correctly Classified Instances: 88.53211009174312
Weighted Precision: 0.8856087872135817
Weighted AreaUnderROC: 0.9723702914001423
Root mean squared error: 0.2988507797120919
Relative absolute error: 20.637331168852686
Root relative squared error: 59.77015594241838
Weighted TruePositiveRate: 0.8853211009174312
Weighted MatthewsCorrelation: 0.7584788067765722
Weighted FMeasure: 0.8854456331084435
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9732529049507668
Mean absolute error: 0.10318665584426343
Coverage of cases: 94.95412844036697
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8853211009174312
Weighted FalsePositiveRate: 0.12519316914770837
Kappa statistic: 0.7584434004077653
Training time: 0.0
		
Time end:Sun Oct 08 09.57.32 EEST 2017