Sun Oct 08 09.57.13 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.13 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9619758351101635
Root mean squared error: 0.20318563844357893
Relative absolute error: 8.256880733944955
Root relative squared error: 40.637127688715786
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9433713947023102
Mean absolute error: 0.04128440366972477
Coverage of cases: 95.87155963302752
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 16.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9460576121126579
Weighted AreaUnderROC: 0.9463397299218195
Root mean squared error: 0.2346185661017325
Relative absolute error: 11.009174311926607
Root relative squared error: 46.9237132203465
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8854714684128192
Weighted FMeasure: 0.9451771825358343
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9234360791132107
Mean absolute error: 0.05504587155963303
Coverage of cases: 94.4954128440367
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.05227466859672802
Kappa statistic: 0.8848185980979218
Training time: 12.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9544904991886003
Weighted AreaUnderROC: 0.9538024164889838
Root mean squared error: 0.2141764684390597
Relative absolute error: 9.174311926605505
Root relative squared error: 42.83529368781194
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9037590324881913
Weighted FMeasure: 0.9542258235983915
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9347209842685323
Mean absolute error: 0.045871559633027525
Coverage of cases: 95.41284403669725
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04652360738900517
Kappa statistic: 0.9035910136210863
Training time: 16.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.944954128440367
Weighted AreaUnderROC: 0.9418976545842217
Root mean squared error: 0.2346185661017325
Relative absolute error: 11.009174311926607
Root relative squared error: 46.9237132203465
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8837953091684436
Weighted FMeasure: 0.944954128440367
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9206294869036208
Mean absolute error: 0.05504587155963303
Coverage of cases: 94.4954128440367
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 74.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.06115881927192347
Kappa statistic: 0.8837953091684435
Training time: 12.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9363343078939409
Weighted AreaUnderROC: 0.9255508173418621
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.864096304298112
Weighted FMeasure: 0.9352936945517136
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9047309198390756
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.08467818183003722
Kappa statistic: 0.8625967945254819
Training time: 17.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9363343078939409
Weighted AreaUnderROC: 0.9255508173418621
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.864096304298112
Weighted FMeasure: 0.9352936945517136
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9047309198390756
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 118.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.08467818183003722
Kappa statistic: 0.8625967945254819
Training time: 13.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9499903503048218
Weighted AreaUnderROC: 0.9411869225302061
Root mean squared error: 0.224630175168721
Relative absolute error: 10.091743119266056
Root relative squared error: 44.9260350337442
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8933542809747683
Weighted FMeasure: 0.9492294538728939
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9243163358252126
Mean absolute error: 0.05045871559633028
Coverage of cases: 94.95412844036697
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 140.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.0671674393432575
Kappa statistic: 0.8922828137633635
Training time: 19.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.944866926408066
Weighted AreaUnderROC: 0.9396766169154228
Root mean squared error: 0.2346185661017325
Relative absolute error: 11.009174311926607
Root relative squared error: 46.9237132203465
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8834440027501075
Weighted FMeasure: 0.9448264004028346
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9194303411388184
Mean absolute error: 0.05504587155963303
Coverage of cases: 94.4954128440367
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 153.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.0656008946095212
Kappa statistic: 0.8832768159914332
Training time: 12.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9541284403669725
Weighted AreaUnderROC: 0.9515813788201848
Root mean squared error: 0.2141764684390597
Relative absolute error: 9.174311926605505
Root relative squared error: 42.83529368781194
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9031627576403696
Weighted FMeasure: 0.9541284403669725
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9334136982192576
Mean absolute error: 0.045871559633027525
Coverage of cases: 95.41284403669725
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.050965682726602894
Kappa statistic: 0.9031627576403697
Training time: 23.0
		
Time end:Sun Oct 08 09.57.14 EEST 2017