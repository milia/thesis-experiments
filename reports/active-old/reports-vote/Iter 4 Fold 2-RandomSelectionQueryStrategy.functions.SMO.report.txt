Sun Oct 08 09.57.58 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.58 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9591597501497389
Weighted AreaUnderROC: 0.9595864661654135
Root mean squared error: 0.2036532699906392
Relative absolute error: 8.294930875576037
Root relative squared error: 40.73065399812784
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9135480351708758
Weighted FMeasure: 0.9586526109213916
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9415748089866285
Mean absolute error: 0.041474654377880185
Coverage of cases: 95.85253456221199
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03935241329129274
Kappa statistic: 0.9131652661064424
Training time: 16.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.7649769585253456
Correctly Classified Instances: 97.23502304147465
Weighted Precision: 0.9723502304147466
Weighted AreaUnderROC: 0.9708646616541353
Root mean squared error: 0.16628219864210797
Relative absolute error: 5.529953917050691
Root relative squared error: 33.2564397284216
Weighted TruePositiveRate: 0.9723502304147466
Weighted MatthewsCorrelation: 0.9417293233082706
Weighted FMeasure: 0.9723502304147466
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9593309310141713
Mean absolute error: 0.027649769585253458
Coverage of cases: 97.23502304147465
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9723502304147466
Weighted FalsePositiveRate: 0.030620907106475866
Kappa statistic: 0.9417293233082707
Training time: 18.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.7649769585253456
Correctly Classified Instances: 97.23502304147465
Weighted Precision: 0.9723502304147466
Weighted AreaUnderROC: 0.9708646616541353
Root mean squared error: 0.16628219864210797
Relative absolute error: 5.529953917050691
Root relative squared error: 33.2564397284216
Weighted TruePositiveRate: 0.9723502304147466
Weighted MatthewsCorrelation: 0.9417293233082706
Weighted FMeasure: 0.9723502304147466
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9593309310141713
Mean absolute error: 0.027649769585253458
Coverage of cases: 97.23502304147465
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9723502304147466
Weighted FalsePositiveRate: 0.030620907106475866
Kappa statistic: 0.9417293233082707
Training time: 17.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.304147465437788
Correctly Classified Instances: 97.6958525345622
Weighted Precision: 0.977051348398597
Weighted AreaUnderROC: 0.9768170426065163
Root mean squared error: 0.15179418517972906
Relative absolute error: 4.6082949308755765
Root relative squared error: 30.35883703594581
Weighted TruePositiveRate: 0.9769585253456221
Weighted MatthewsCorrelation: 0.9515920398606602
Weighted FMeasure: 0.9769832209084983
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9665856617928602
Mean absolute error: 0.02304147465437788
Coverage of cases: 97.6958525345622
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.9769585253456221
Weighted FalsePositiveRate: 0.02332444013258954
Kappa statistic: 0.9515473585495468
Training time: 20.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.7649769585253456
Correctly Classified Instances: 97.23502304147465
Weighted Precision: 0.9726381977173684
Weighted AreaUnderROC: 0.9730576441102758
Root mean squared error: 0.16628219864210797
Relative absolute error: 5.529953917050691
Root relative squared error: 33.2564397284216
Weighted TruePositiveRate: 0.9723502304147466
Weighted MatthewsCorrelation: 0.9421599746118634
Weighted FMeasure: 0.9724081421424874
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9605639968404878
Mean absolute error: 0.027649769585253458
Coverage of cases: 97.23502304147465
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 123.0
Weighted Recall: 0.9723502304147466
Weighted FalsePositiveRate: 0.026234942194195168
Kappa statistic: 0.941983780411728
Training time: 24.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.7649769585253456
Correctly Classified Instances: 97.23502304147465
Weighted Precision: 0.9723502304147466
Weighted AreaUnderROC: 0.9708646616541353
Root mean squared error: 0.16628219864210797
Relative absolute error: 5.529953917050691
Root relative squared error: 33.2564397284216
Weighted TruePositiveRate: 0.9723502304147466
Weighted MatthewsCorrelation: 0.9417293233082706
Weighted FMeasure: 0.9723502304147466
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9593309310141713
Mean absolute error: 0.027649769585253458
Coverage of cases: 97.23502304147465
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 155.0
Weighted Recall: 0.9723502304147466
Weighted FalsePositiveRate: 0.030620907106475866
Kappa statistic: 0.9417293233082707
Training time: 26.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 50.0
Incorrectly Classified Instances: 1.8433179723502304
Correctly Classified Instances: 98.15668202764977
Weighted Precision: 0.9815668202764977
Weighted AreaUnderROC: 0.9805764411027569
Root mean squared error: 0.13576884666042613
Relative absolute error: 3.686635944700461
Root relative squared error: 27.153769332085226
Weighted TruePositiveRate: 0.9815668202764977
Weighted MatthewsCorrelation: 0.9611528822055138
Weighted FMeasure: 0.9815668202764977
Iteration time: 35.0
Weighted AreaUnderPRC: 0.9727082683667694
Mean absolute error: 0.018433179723502304
Coverage of cases: 98.15668202764977
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 190.0
Weighted Recall: 0.9815668202764977
Weighted FalsePositiveRate: 0.02041393807098391
Kappa statistic: 0.9611528822055138
Training time: 29.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 50.0
Incorrectly Classified Instances: 2.304147465437788
Correctly Classified Instances: 97.6958525345622
Weighted Precision: 0.977051348398597
Weighted AreaUnderROC: 0.9768170426065163
Root mean squared error: 0.15179418517972906
Relative absolute error: 4.6082949308755765
Root relative squared error: 30.35883703594581
Weighted TruePositiveRate: 0.9769585253456221
Weighted MatthewsCorrelation: 0.9515920398606602
Weighted FMeasure: 0.9769832209084983
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9665856617928602
Mean absolute error: 0.02304147465437788
Coverage of cases: 97.6958525345622
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 218.0
Weighted Recall: 0.9769585253456221
Weighted FalsePositiveRate: 0.02332444013258954
Kappa statistic: 0.9515473585495468
Training time: 22.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 50.0
Incorrectly Classified Instances: 1.8433179723502304
Correctly Classified Instances: 98.15668202764977
Weighted Precision: 0.9818179735773639
Weighted AreaUnderROC: 0.9827694235588973
Root mean squared error: 0.13576884666042613
Relative absolute error: 3.686635944700461
Root relative squared error: 27.153769332085226
Weighted TruePositiveRate: 0.9815668202764977
Weighted MatthewsCorrelation: 0.9615023317065441
Weighted FMeasure: 0.9816054280949916
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9739082543879717
Mean absolute error: 0.018433179723502304
Coverage of cases: 98.15668202764977
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 252.0
Weighted Recall: 0.9815668202764977
Weighted FalsePositiveRate: 0.016027973158703208
Kappa statistic: 0.9613225202744853
Training time: 27.0
		
Time end:Sun Oct 08 09.57.59 EEST 2017