Sun Oct 08 09.58.09 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.09 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 70.41284403669725
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9499521449755371
Weighted AreaUnderROC: 0.9899609097370291
Root mean squared error: 0.20803599414946639
Relative absolute error: 20.509165136905487
Root relative squared error: 41.60719882989328
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8903242366845491
Weighted FMeasure: 0.9454335803309657
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9897911264146398
Mean absolute error: 0.10254582568452743
Coverage of cases: 99.54128440366972
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.038948442583934845
Kappa statistic: 0.8863201807752477
Training time: 2.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 69.03669724770643
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9574271405144961
Weighted AreaUnderROC: 0.9884677203758403
Root mean squared error: 0.21236831665121386
Relative absolute error: 21.338498887628628
Root relative squared error: 42.47366333024277
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.907486088308968
Weighted FMeasure: 0.954465259511131
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9882814612213024
Mean absolute error: 0.10669249443814315
Coverage of cases: 99.08256880733946
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.03319738137621199
Kappa statistic: 0.9048533519553074
Training time: 2.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 64.6788990825688
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.9652284021406727
Weighted AreaUnderROC: 0.9946250888415067
Root mean squared error: 0.19101969509842848
Relative absolute error: 16.59948998247888
Root relative squared error: 38.2039390196857
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9250709270858005
Weighted FMeasure: 0.9635152659681085
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9947879271855027
Mean absolute error: 0.0829974499123944
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.02744632016848914
Kappa statistic: 0.9235490092933544
Training time: 3.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 67.20183486238533
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.9935145700071073
Root mean squared error: 0.17588017232803407
Relative absolute error: 16.766341156578676
Root relative squared error: 35.17603446560681
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9933130589535555
Mean absolute error: 0.08383170578289338
Coverage of cases: 99.54128440366972
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 3.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 63.76146788990825
Incorrectly Classified Instances: 3.2110091743119265
Correctly Classified Instances: 96.78899082568807
Weighted Precision: 0.9684706984696602
Weighted AreaUnderROC: 0.9937639782737687
Root mean squared error: 0.18017955996626062
Relative absolute error: 14.686008170705431
Root relative squared error: 36.03591199325212
Weighted TruePositiveRate: 0.9678899082568807
Weighted MatthewsCorrelation: 0.9330512268494389
Weighted FMeasure: 0.9679898204920298
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9937238175545479
Mean absolute error: 0.07343004085352715
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9678899082568807
Weighted FalsePositiveRate: 0.02901286490222544
Kappa statistic: 0.932662607007325
Training time: 4.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 62.61467889908257
Incorrectly Classified Instances: 3.2110091743119265
Correctly Classified Instances: 96.78899082568807
Weighted Precision: 0.9692583438612977
Weighted AreaUnderROC: 0.9918538858786017
Root mean squared error: 0.17715698268168414
Relative absolute error: 14.186795611724545
Root relative squared error: 35.43139653633683
Weighted TruePositiveRate: 0.9678899082568807
Weighted MatthewsCorrelation: 0.9340299888424322
Weighted FMeasure: 0.9680486979147237
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9915862090136103
Mean absolute error: 0.07093397805862273
Coverage of cases: 99.54128440366972
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9678899082568807
Weighted FalsePositiveRate: 0.024570789564627712
Kappa statistic: 0.9329584395044371
Training time: 5.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 65.36697247706422
Incorrectly Classified Instances: 2.7522935779816513
Correctly Classified Instances: 97.24770642201835
Weighted Precision: 0.9733784564059794
Weighted AreaUnderROC: 0.9916933191186922
Root mean squared error: 0.17935624905074135
Relative absolute error: 16.112168941216606
Root relative squared error: 35.87124981014827
Weighted TruePositiveRate: 0.9724770642201835
Weighted MatthewsCorrelation: 0.9431046631124593
Weighted FMeasure: 0.9725885912679172
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9917363278455357
Mean absolute error: 0.08056084470608303
Coverage of cases: 99.54128440366972
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9724770642201835
Weighted FalsePositiveRate: 0.021695258960766284
Kappa statistic: 0.942409299048961
Training time: 6.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 66.74311926605505
Incorrectly Classified Instances: 3.2110091743119265
Correctly Classified Instances: 96.78899082568807
Weighted Precision: 0.9692583438612977
Weighted AreaUnderROC: 0.9874289267945986
Root mean squared error: 0.19369181281915226
Relative absolute error: 16.948608082382087
Root relative squared error: 38.73836256383045
Weighted TruePositiveRate: 0.9678899082568807
Weighted MatthewsCorrelation: 0.9340299888424322
Weighted FMeasure: 0.9680486979147237
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9841840324113392
Mean absolute error: 0.08474304041191043
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9678899082568807
Weighted FalsePositiveRate: 0.024570789564627712
Kappa statistic: 0.9329584395044371
Training time: 6.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 61.92660550458716
Incorrectly Classified Instances: 3.2110091743119265
Correctly Classified Instances: 96.78899082568807
Weighted Precision: 0.9692583438612977
Weighted AreaUnderROC: 0.9955579246624022
Root mean squared error: 0.1591983298044642
Relative absolute error: 12.131161037845668
Root relative squared error: 31.839665960892837
Weighted TruePositiveRate: 0.9678899082568807
Weighted MatthewsCorrelation: 0.9340299888424322
Weighted FMeasure: 0.9680486979147237
Iteration time: 7.0
Weighted AreaUnderPRC: 0.995509884348683
Mean absolute error: 0.06065580518922834
Coverage of cases: 99.54128440366972
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.9678899082568807
Weighted FalsePositiveRate: 0.024570789564627712
Kappa statistic: 0.9329584395044371
Training time: 6.0
		
Time end:Sun Oct 08 09.58.10 EEST 2017