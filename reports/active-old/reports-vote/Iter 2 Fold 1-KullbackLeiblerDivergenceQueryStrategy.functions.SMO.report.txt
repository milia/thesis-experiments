Sun Oct 08 09.57.03 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.03 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9619758351101635
Root mean squared error: 0.20318563844357893
Relative absolute error: 8.256880733944955
Root relative squared error: 40.637127688715786
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9433713947023102
Mean absolute error: 0.04128440366972477
Coverage of cases: 95.87155963302752
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 33.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 23.0
		
Time end:Sun Oct 08 09.57.03 EEST 2017