Sun Oct 08 09.57.08 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.08 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 11.009174311926605
Correctly Classified Instances: 88.9908256880734
Weighted Precision: 0.8956146537122277
Weighted AreaUnderROC: 0.9709488272921108
Root mean squared error: 0.31129815974330366
Relative absolute error: 22.163886865549053
Root relative squared error: 62.25963194866073
Weighted TruePositiveRate: 0.8899082568807339
Weighted MatthewsCorrelation: 0.7761308554741754
Weighted FMeasure: 0.8908671606619313
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9728941579457105
Mean absolute error: 0.11081943432774526
Coverage of cases: 93.57798165137615
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8899082568807339
Weighted FalsePositiveRate: 0.10010726185585833
Kappa statistic: 0.7726403615504954
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 9.174311926605505
Correctly Classified Instances: 90.8256880733945
Weighted Precision: 0.9120937565293369
Weighted AreaUnderROC: 0.9817874911158492
Root mean squared error: 0.2634159466407571
Relative absolute error: 17.495385367239034
Root relative squared error: 52.68318932815143
Weighted TruePositiveRate: 0.908256880733945
Weighted MatthewsCorrelation: 0.8120626042016194
Weighted FMeasure: 0.9089305190222622
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9828047379569165
Mean absolute error: 0.08747692683619518
Coverage of cases: 96.78899082568807
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 3.0
Weighted Recall: 0.908256880733945
Weighted FalsePositiveRate: 0.08416306410281488
Kappa statistic: 0.8097067039106145
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 56.65137614678899
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9313829636151901
Weighted AreaUnderROC: 0.9854299928926794
Root mean squared error: 0.2218383527497781
Relative absolute error: 14.541522760126997
Root relative squared error: 44.36767054995562
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8551059572424982
Weighted FMeasure: 0.931267379865066
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9860742944299195
Mean absolute error: 0.07270761380063498
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.07422748642110549
Kappa statistic: 0.855066040244659
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 57.11009174311926
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9544904991886003
Weighted AreaUnderROC: 0.98818407960199
Root mean squared error: 0.20748780371247108
Relative absolute error: 13.301335987662394
Root relative squared error: 41.497560742494215
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9037590324881913
Weighted FMeasure: 0.9542258235983915
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9886132835142157
Mean absolute error: 0.06650667993831197
Coverage of cases: 98.1651376146789
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04652360738900517
Kappa statistic: 0.9035910136210863
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 56.65137614678899
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9402732233725933
Weighted AreaUnderROC: 0.9857853589196873
Root mean squared error: 0.22407200150609674
Relative absolute error: 14.671299769322838
Root relative squared error: 44.81440030121935
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8738725167000166
Weighted FMeasure: 0.9402992725370855
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9863931733313115
Mean absolute error: 0.07335649884661419
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.06847642521338262
Kappa statistic: 0.8738313596295967
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 56.19266055045872
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9310776234397324
Weighted AreaUnderROC: 0.9838308457711443
Root mean squared error: 0.23731894191936592
Relative absolute error: 15.60298286318827
Root relative squared error: 47.46378838387319
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8544610424142408
Weighted FMeasure: 0.9311145452350986
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9846395450857561
Mean absolute error: 0.07801491431594135
Coverage of cases: 97.24770642201835
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.0786695617587032
Kappa statistic: 0.8544207995726114
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9222281283348684
Weighted AreaUnderROC: 0.9830312722103767
Root mean squared error: 0.24685761515152843
Relative absolute error: 16.348630891999434
Root relative squared error: 49.37152303030569
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.835780527149313
Weighted FMeasure: 0.9221030305137415
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9839135200012823
Mean absolute error: 0.08174315445999718
Coverage of cases: 96.78899082568807
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.08442062296642605
Kappa statistic: 0.8357415122772804
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 9.174311926605505
Correctly Classified Instances: 90.8256880733945
Weighted Precision: 0.9088048179684621
Weighted AreaUnderROC: 0.9798329779673062
Root mean squared error: 0.2681755838846207
Relative absolute error: 18.11314252144659
Root relative squared error: 53.63511677692414
Weighted TruePositiveRate: 0.908256880733945
Weighted MatthewsCorrelation: 0.8073321192724388
Weighted FMeasure: 0.9084516471967831
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9809159542068359
Mean absolute error: 0.09056571260723295
Coverage of cases: 96.78899082568807
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.908256880733945
Weighted FalsePositiveRate: 0.09748929011560806
Kappa statistic: 0.8071820272421723
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.8965031176359185
Weighted AreaUnderROC: 0.9779673063255154
Root mean squared error: 0.28242782582130255
Relative absolute error: 19.214285286990858
Root relative squared error: 56.48556516426051
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7806161349583483
Weighted FMeasure: 0.8950171502912354
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9792538620073411
Mean absolute error: 0.09607142643495428
Coverage of cases: 95.41284403669725
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.10611588192719233
Kappa statistic: 0.7797205869431508
Training time: 0.0
		
Time end:Sun Oct 08 09.57.09 EEST 2017