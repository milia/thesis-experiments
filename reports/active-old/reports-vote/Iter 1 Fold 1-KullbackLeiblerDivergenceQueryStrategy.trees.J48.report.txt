Sun Oct 08 09.57.04 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.04 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 69.4954128440367
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9543710942693792
Weighted AreaUnderROC: 0.962864250177683
Root mean squared error: 0.20631033738379065
Relative absolute error: 15.557043879166645
Root relative squared error: 41.262067476758126
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9029846072218838
Weighted FMeasure: 0.9539063219828097
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9534726663373257
Mean absolute error: 0.07778521939583323
Coverage of cases: 97.70642201834862
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.05984983340179834
Kappa statistic: 0.9022947292936537
Training time: 6.0
		
Time end:Sun Oct 08 09.57.04 EEST 2017