Sun Oct 08 09.56.44 EEST 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.56.44 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 52.941910117109884
Incorrectly Classified Instances: 8.32114319536634
Correctly Classified Instances: 91.67885680463367
Weighted Precision: 0.9213660659109616
Weighted AreaUnderROC: 0.9456394517744252
Root mean squared error: 0.274315789821634
Relative absolute error: 20.51928235122831
Root relative squared error: 54.86315796432681
Weighted TruePositiveRate: 0.9167885680463363
Weighted MatthewsCorrelation: 0.8307787834025309
Weighted FMeasure: 0.9173060762576402
Iteration time: 16.1
Weighted AreaUnderPRC: 0.9292297139794383
Mean absolute error: 0.10259641175614154
Coverage of cases: 93.47144125480911
Instances selection time: 14.6
Test time: 11.7
Accumulative iteration time: 16.1
Weighted Recall: 0.9167885680463363
Weighted FalsePositiveRate: 0.07622073054212833
Kappa statistic: 0.8274533558366638
Training time: 1.5
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.79243647740244
Incorrectly Classified Instances: 7.9080877689933615
Correctly Classified Instances: 92.09191223100665
Weighted Precision: 0.9221723949966301
Weighted AreaUnderROC: 0.9505563281872057
Root mean squared error: 0.2636316246585671
Relative absolute error: 19.103546196656524
Root relative squared error: 52.72632493171342
Weighted TruePositiveRate: 0.9209191223100663
Weighted MatthewsCorrelation: 0.8349583045103642
Weighted FMeasure: 0.9210860478692657
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9381926644121894
Mean absolute error: 0.09551773098328264
Coverage of cases: 94.34469200524245
Instances selection time: 10.0
Test time: 13.2
Accumulative iteration time: 26.1
Weighted Recall: 0.9209191223100663
Weighted FalsePositiveRate: 0.08287344842385831
Kappa statistic: 0.8340771366604296
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.584851815837325
Incorrectly Classified Instances: 7.1272143068532525
Correctly Classified Instances: 92.87278569314674
Weighted Precision: 0.9300627959472904
Weighted AreaUnderROC: 0.9540025760733624
Root mean squared error: 0.25559523089887454
Relative absolute error: 17.553719138971008
Root relative squared error: 51.119046179774905
Weighted TruePositiveRate: 0.9287278569314674
Weighted MatthewsCorrelation: 0.8510783653987909
Weighted FMeasure: 0.9286722843206316
Iteration time: 9.1
Weighted AreaUnderPRC: 0.9431579076339538
Mean absolute error: 0.08776859569485503
Coverage of cases: 94.66558153299792
Instances selection time: 9.1
Test time: 11.6
Accumulative iteration time: 35.2
Weighted Recall: 0.9287278569314674
Weighted FalsePositiveRate: 0.07838932403205476
Kappa statistic: 0.8496689593277148
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.99970405445398
Incorrectly Classified Instances: 7.218534646767853
Correctly Classified Instances: 92.78146535323215
Weighted Precision: 0.9288790620864853
Weighted AreaUnderROC: 0.9579562913146406
Root mean squared error: 0.25583754752632176
Relative absolute error: 17.375596970711893
Root relative squared error: 51.16750950526435
Weighted TruePositiveRate: 0.9278146535323216
Weighted MatthewsCorrelation: 0.8488979171275561
Weighted FMeasure: 0.9277868496020462
Iteration time: 7.1
Weighted AreaUnderPRC: 0.9484129633714046
Mean absolute error: 0.08687798485355948
Coverage of cases: 94.80425316027564
Instances selection time: 7.1
Test time: 12.4
Accumulative iteration time: 42.3
Weighted Recall: 0.9278146535323216
Weighted FalsePositiveRate: 0.07984585092879773
Kappa statistic: 0.8478080773949221
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.815477952056824
Incorrectly Classified Instances: 7.494609563268929
Correctly Classified Instances: 92.50539043673106
Weighted Precision: 0.9262564125917929
Weighted AreaUnderROC: 0.95419568183303
Root mean squared error: 0.26423983149553354
Relative absolute error: 17.85693336362111
Root relative squared error: 52.8479662991067
Weighted TruePositiveRate: 0.9250539043673106
Weighted MatthewsCorrelation: 0.8433948349558735
Weighted FMeasure: 0.9251063329447199
Iteration time: 7.7
Weighted AreaUnderPRC: 0.945355316910104
Mean absolute error: 0.08928466681810557
Coverage of cases: 94.25189193759776
Instances selection time: 7.6
Test time: 16.3
Accumulative iteration time: 50.0
Weighted Recall: 0.9250539043673106
Weighted FalsePositiveRate: 0.08114656138298423
Kappa statistic: 0.8423319708938581
Training time: 0.1
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 54.20602037796474
Incorrectly Classified Instances: 7.402866444002877
Correctly Classified Instances: 92.59713355599712
Weighted Precision: 0.9272352634217256
Weighted AreaUnderROC: 0.9558149104103542
Root mean squared error: 0.2611011070374984
Relative absolute error: 17.584166841243878
Root relative squared error: 52.220221407499686
Weighted TruePositiveRate: 0.9259713355599712
Weighted MatthewsCorrelation: 0.8456520597073427
Weighted FMeasure: 0.926152326267275
Iteration time: 5.9
Weighted AreaUnderPRC: 0.9481070730109854
Mean absolute error: 0.08792083420621939
Coverage of cases: 94.57362702405615
Instances selection time: 5.8
Test time: 18.6
Accumulative iteration time: 55.9
Weighted Recall: 0.9259713355599712
Weighted FalsePositiveRate: 0.07660164267103249
Kappa statistic: 0.844780763866068
Training time: 0.1
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 54.068617088741384
Incorrectly Classified Instances: 7.44916078298736
Correctly Classified Instances: 92.55083921701265
Weighted Precision: 0.9268464009061278
Weighted AreaUnderROC: 0.9569564235749288
Root mean squared error: 0.25782314927071026
Relative absolute error: 17.07381206748873
Root relative squared error: 51.56462985414205
Weighted TruePositiveRate: 0.9255083921701266
Weighted MatthewsCorrelation: 0.8448793599663489
Weighted FMeasure: 0.9257477605666807
Iteration time: 4.3
Weighted AreaUnderPRC: 0.9491416386827902
Mean absolute error: 0.08536906033744364
Coverage of cases: 94.75774743161544
Instances selection time: 4.3
Test time: 22.3
Accumulative iteration time: 60.2
Weighted Recall: 0.9255083921701266
Weighted FalsePositiveRate: 0.07556356742168817
Kappa statistic: 0.8440533061865028
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 53.90806663002579
Incorrectly Classified Instances: 7.540692512577687
Correctly Classified Instances: 92.45930748742232
Weighted Precision: 0.9259124499938114
Weighted AreaUnderROC: 0.9558126058750288
Root mean squared error: 0.25817921058053195
Relative absolute error: 16.875754508319137
Root relative squared error: 51.63584211610638
Weighted TruePositiveRate: 0.9245930748742233
Weighted MatthewsCorrelation: 0.8429504719796533
Weighted FMeasure: 0.9248463719777307
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9474505458150061
Mean absolute error: 0.0843787725415957
Coverage of cases: 94.75795882129117
Instances selection time: 3.0
Test time: 19.0
Accumulative iteration time: 63.2
Weighted Recall: 0.9245930748742233
Weighted FalsePositiveRate: 0.07613517639668602
Kappa statistic: 0.8421682447288272
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 53.93110810468018
Incorrectly Classified Instances: 7.586564072210713
Correctly Classified Instances: 92.4134359277893
Weighted Precision: 0.9255799798179997
Weighted AreaUnderROC: 0.9528917910447762
Root mean squared error: 0.2587945572094189
Relative absolute error: 16.917958385999164
Root relative squared error: 51.7589114418838
Weighted TruePositiveRate: 0.9241343592778929
Weighted MatthewsCorrelation: 0.8421772908075587
Weighted FMeasure: 0.9244135385837652
Iteration time: 1.9
Weighted AreaUnderPRC: 0.9433916858946281
Mean absolute error: 0.0845897919299958
Coverage of cases: 94.75795882129117
Instances selection time: 1.8
Test time: 17.6
Accumulative iteration time: 65.1
Weighted Recall: 0.9241343592778929
Weighted FalsePositiveRate: 0.07598413296584408
Kappa statistic: 0.8413242600008551
Training time: 0.1
		
Time end:Sun Oct 08 09.56.49 EEST 2017