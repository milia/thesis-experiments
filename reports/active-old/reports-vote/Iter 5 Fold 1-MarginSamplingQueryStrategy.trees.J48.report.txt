Sun Oct 08 09.57.44 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 09.57.44 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 67.20183486238533
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9343341148327433
Weighted AreaUnderROC: 0.9202203269367448
Root mean squared error: 0.25939011738767015
Relative absolute error: 18.88225114382572
Root relative squared error: 51.87802347753403
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.856225786863442
Weighted FMeasure: 0.9301386294391045
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9024073152849904
Mean absolute error: 0.09441125571912859
Coverage of cases: 94.4954128440367
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.10087993844669184
Kappa statistic: 0.8511064566068663
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9340627587394906
Weighted AreaUnderROC: 0.9355923527839178
Root mean squared error: 0.2538425645028311
Relative absolute error: 19.134780407840623
Root relative squared error: 50.76851290056622
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8588833378427607
Weighted FMeasure: 0.9316461035537277
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9212224378596019
Mean absolute error: 0.09567390203920312
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.060901260408312306
Kappa statistic: 0.8569678943224563
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9383626746012067
Weighted AreaUnderROC: 0.9799218194740584
Root mean squared error: 0.23722743591425102
Relative absolute error: 19.38635833297047
Root relative squared error: 47.44548718285021
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8656038695451582
Weighted FMeasure: 0.9349025714975575
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9702787727813443
Mean absolute error: 0.09693179166485234
Coverage of cases: 98.62385321100918
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.09356233250523269
Kappa statistic: 0.861348355442486
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 55.27522935779817
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9340627587394906
Weighted AreaUnderROC: 0.9594438521677328
Root mean squared error: 0.2404170589479621
Relative absolute error: 18.550792411824446
Root relative squared error: 48.08341178959242
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8588833378427607
Weighted FMeasure: 0.9316461035537277
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9408625922811934
Mean absolute error: 0.09275396205912223
Coverage of cases: 96.3302752293578
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.060901260408312306
Kappa statistic: 0.8569678943224563
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9355288546907369
Weighted AreaUnderROC: 0.9545131485429993
Root mean squared error: 0.24680826767439357
Relative absolute error: 18.175543950131477
Root relative squared error: 49.36165353487871
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8607386575043884
Weighted FMeasure: 0.931746505374246
Iteration time: 2.0
Weighted AreaUnderPRC: 0.93184701157746
Mean absolute error: 0.09087771975065739
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.056459185070714575
Kappa statistic: 0.8575908021949307
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9355288546907369
Weighted AreaUnderROC: 0.94034333574591
Root mean squared error: 0.24578168411826543
Relative absolute error: 17.53624555759751
Root relative squared error: 49.156336823653085
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8607386575043884
Weighted FMeasure: 0.931746505374246
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9207124895014762
Mean absolute error: 0.08768122778798755
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.056459185070714575
Kappa statistic: 0.8575908021949307
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9355288546907369
Weighted AreaUnderROC: 0.94909381663113
Root mean squared error: 0.24422070642113608
Relative absolute error: 17.05465491200194
Root relative squared error: 48.84414128422721
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8607386575043884
Weighted FMeasure: 0.931746505374246
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9251303366857464
Mean absolute error: 0.0852732745600097
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.056459185070714575
Kappa statistic: 0.8575908021949307
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.9328358208955224
Root mean squared error: 0.2431558771856851
Relative absolute error: 16.676585244653293
Root relative squared error: 48.63117543713702
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9184523482358695
Mean absolute error: 0.08338292622326646
Coverage of cases: 94.03669724770643
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.9461604167889256
Root mean squared error: 0.24240129505820104
Relative absolute error: 16.37177376403545
Root relative squared error: 48.48025901164021
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9273336788840382
Mean absolute error: 0.08185886882017725
Coverage of cases: 94.03669724770643
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 1.0
		
Time end:Sun Oct 08 09.57.45 EEST 2017