Sun Oct 08 09.58.13 EEST 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.58.13 EEST 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9408958964316524
Weighted AreaUnderROC: 0.9433191186922532
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8712920064828202
Weighted FMeasure: 0.9363391770527931
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9161053300410956
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.049141579129255424
Kappa statistic: 0.8673735442377891
Training time: 8.0
		
Time end:Sun Oct 08 09.58.13 EEST 2017