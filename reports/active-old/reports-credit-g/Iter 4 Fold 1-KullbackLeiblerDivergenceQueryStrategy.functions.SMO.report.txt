Sat Oct 07 11.38.55 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.55 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6782071131075689
Weighted AreaUnderROC: 0.608095238095238
Root mean squared error: 0.5531726674375732
Relative absolute error: 61.199999999999996
Root relative squared error: 110.63453348751464
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.23131506575928193
Weighted FMeasure: 0.6837137260261491
Iteration time: 41.0
Weighted AreaUnderPRC: 0.6366363985259164
Mean absolute error: 0.306
Coverage of cases: 69.4
Instances selection time: 14.0
Test time: 3.0
Accumulative iteration time: 41.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.47780952380952385
Kappa statistic: 0.22883064516129029
Training time: 27.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7050952531788769
Weighted AreaUnderROC: 0.5776190476190476
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.23927187126978333
Weighted FMeasure: 0.6737419933625682
Iteration time: 110.0
Weighted AreaUnderPRC: 0.624622715959998
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 151.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5707619047619048
Kappa statistic: 0.19221698113207528
Training time: 103.0
		
Time end:Sat Oct 07 11.38.55 EEST 2017