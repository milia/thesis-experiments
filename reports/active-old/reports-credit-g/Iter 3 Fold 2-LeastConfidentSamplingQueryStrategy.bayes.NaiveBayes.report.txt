Sat Oct 07 11.39.09 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.09 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 80.9
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6730581361938511
Weighted AreaUnderROC: 0.7014476190476191
Root mean squared error: 0.47415196555824607
Relative absolute error: 65.44159613170052
Root relative squared error: 94.83039311164922
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.21569529831356454
Weighted FMeasure: 0.6788642473118279
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7287381200482763
Mean absolute error: 0.3272079806585026
Coverage of cases: 93.6
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5036190476190476
Kappa statistic: 0.2099792099792099
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 82.6
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6933887733887732
Weighted AreaUnderROC: 0.7161333333333332
Root mean squared error: 0.46205773849045967
Relative absolute error: 64.87317707925065
Root relative squared error: 92.41154769809194
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.26864674234219216
Weighted FMeasure: 0.6975396825396826
Iteration time: 5.0
Weighted AreaUnderPRC: 0.73958399593222
Mean absolute error: 0.3243658853962533
Coverage of cases: 93.6
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 11.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4468571428571429
Kappa statistic: 0.2673267326732673
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 82.4
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6943922924901186
Weighted AreaUnderROC: 0.7028761904761904
Root mean squared error: 0.4611732784337503
Relative absolute error: 65.57698044174778
Root relative squared error: 92.23465568675006
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.27128766059783654
Weighted FMeasure: 0.6982634978960469
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7292209282043868
Mean absolute error: 0.3278849022087389
Coverage of cases: 92.8
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 15.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.443047619047619
Kappa statistic: 0.27021696252465466
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 82.7
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7024286581663631
Weighted AreaUnderROC: 0.7041333333333334
Root mean squared error: 0.45404022345176454
Relative absolute error: 64.16399861894905
Root relative squared error: 90.8080446903529
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2885910627058925
Weighted FMeasure: 0.7068438914027149
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7306618549706443
Mean absolute error: 0.3208199930947453
Coverage of cases: 93.8
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 20.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.44552380952380954
Kappa statistic: 0.28571428571428553
Training time: 1.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 85.1
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7135248655913979
Weighted AreaUnderROC: 0.7248380952380953
Root mean squared error: 0.4478318836307737
Relative absolute error: 65.006157707986
Root relative squared error: 89.56637672615474
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3160101124854019
Weighted FMeasure: 0.7172841228402319
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7454900287472132
Mean absolute error: 0.32503078853993
Coverage of cases: 95.0
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 24.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.42304761904761906
Kappa statistic: 0.31411530815109334
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 87.2
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.721210407239819
Weighted AreaUnderROC: 0.7346476190476191
Root mean squared error: 0.4396015495357679
Relative absolute error: 64.57688888412882
Root relative squared error: 87.92030990715358
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.33542568293273695
Weighted FMeasure: 0.724009324009324
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7533242502132582
Mean absolute error: 0.3228844444206441
Coverage of cases: 96.4
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 28.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4022857142857143
Kappa statistic: 0.3346379647749511
Training time: 1.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 88.5
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7129524886877827
Weighted AreaUnderROC: 0.7525904761904761
Root mean squared error: 0.43915467825661003
Relative absolute error: 64.53070377533331
Root relative squared error: 87.83093565132201
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3158101459191266
Weighted FMeasure: 0.7158919511860689
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7670393691913419
Mean absolute error: 0.32265351887666655
Coverage of cases: 96.8
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 32.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.41333333333333333
Kappa statistic: 0.31506849315068497
Training time: 1.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 91.1
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7202720491443616
Weighted AreaUnderROC: 0.7583809523809524
Root mean squared error: 0.43008830340600407
Relative absolute error: 64.70694602867175
Root relative squared error: 86.01766068120081
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3321641640100498
Weighted FMeasure: 0.7237707485123708
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7774679576141262
Mean absolute error: 0.32353473014335876
Coverage of cases: 98.6
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 36.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.41285714285714287
Kappa statistic: 0.3303571428571428
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 92.3
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7294793024266121
Weighted AreaUnderROC: 0.7554666666666666
Root mean squared error: 0.4321103286492174
Relative absolute error: 65.54734042707851
Root relative squared error: 86.42206572984348
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.35433147968069073
Weighted FMeasure: 0.7326041011883845
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7733418593660054
Mean absolute error: 0.3277367021353926
Coverage of cases: 99.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 39.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.398
Kappa statistic: 0.3527667984189724
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 92.1
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7161376115305421
Weighted AreaUnderROC: 0.7572190476190476
Root mean squared error: 0.4293102199087219
Relative absolute error: 65.20434043095274
Root relative squared error: 85.86204398174438
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.32137545725816535
Weighted FMeasure: 0.7199654139269269
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7762847219620962
Mean absolute error: 0.32602170215476367
Coverage of cases: 99.2
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 43.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4251428571428571
Kappa statistic: 0.3186372745490981
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 91.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.711856554564173
Weighted AreaUnderROC: 0.7590666666666667
Root mean squared error: 0.4275959194448647
Relative absolute error: 64.89308284595043
Root relative squared error: 85.51918388897293
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.31126931080350606
Weighted FMeasure: 0.7158472582493817
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7778629222039999
Mean absolute error: 0.32446541422975217
Coverage of cases: 99.2
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 46.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43066666666666664
Kappa statistic: 0.3086172344689378
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 91.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.711050394656952
Weighted AreaUnderROC: 0.7551428571428571
Root mean squared error: 0.4287192719744174
Relative absolute error: 65.51122361171424
Root relative squared error: 85.74385439488348
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.30891437698095536
Weighted FMeasure: 0.7151018099547511
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7753641545561841
Mean absolute error: 0.3275561180585712
Coverage of cases: 99.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 49.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43447619047619046
Kappa statistic: 0.305835010060362
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 91.2
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.713599016626771
Weighted AreaUnderROC: 0.7557904761904762
Root mean squared error: 0.43069027612377514
Relative absolute error: 65.27733243103715
Root relative squared error: 86.13805522475502
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.31515740136270226
Weighted FMeasure: 0.7175385824629291
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7732036523456345
Mean absolute error: 0.32638666215518575
Coverage of cases: 98.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 52.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.42980952380952375
Kappa statistic: 0.31224899598393563
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 89.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.719672131147541
Weighted AreaUnderROC: 0.750609523809524
Root mean squared error: 0.43617494305400145
Relative absolute error: 64.7358903473009
Root relative squared error: 87.23498861080029
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3292376912560182
Weighted FMeasure: 0.7233597285067874
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7688670030308461
Mean absolute error: 0.3236794517365045
Coverage of cases: 97.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 55.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.42342857142857143
Kappa statistic: 0.3259557344064385
Training time: 1.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 87.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.713599016626771
Weighted AreaUnderROC: 0.7536952380952381
Root mean squared error: 0.437180568108961
Relative absolute error: 63.93085308477479
Root relative squared error: 87.4361136217922
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.31515740136270226
Weighted FMeasure: 0.7175385824629291
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7715446484410543
Mean absolute error: 0.31965426542387393
Coverage of cases: 96.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 57.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.42980952380952375
Kappa statistic: 0.31224899598393563
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 87.5
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7067395264116576
Weighted AreaUnderROC: 0.7550857142857141
Root mean squared error: 0.43721840379125876
Relative absolute error: 63.65395366065324
Root relative squared error: 87.44368075825176
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.29875271984342394
Weighted FMeasure: 0.7109728506787331
Iteration time: 2.0
Weighted AreaUnderPRC: 0.773947449729996
Mean absolute error: 0.3182697683032662
Coverage of cases: 96.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 59.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.44000000000000006
Kappa statistic: 0.29577464788732377
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 85.8
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.705921052631579
Weighted AreaUnderROC: 0.7540761904761905
Root mean squared error: 0.44018619094805833
Relative absolute error: 63.26942531240844
Root relative squared error: 88.03723818961167
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.29635068075106674
Weighted FMeasure: 0.7101978691019787
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7739186300826495
Mean absolute error: 0.31634712656204217
Coverage of cases: 95.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 64.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.44380952380952376
Kappa statistic: 0.2929292929292929
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 86.1
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6990133836650652
Weighted AreaUnderROC: 0.7510476190476191
Root mean squared error: 0.44271797849478184
Relative absolute error: 63.76364230538799
Root relative squared error: 88.54359569895637
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.28095087143952824
Weighted FMeasure: 0.703492791216746
Iteration time: 2.0
Weighted AreaUnderPRC: 0.770928545769655
Mean absolute error: 0.31881821152693995
Coverage of cases: 95.6
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 66.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.44723809523809516
Kappa statistic: 0.2785571142284568
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 85.1
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7101871101871102
Weighted AreaUnderROC: 0.7558476190476191
Root mean squared error: 0.4428008364903722
Relative absolute error: 63.13673055432278
Root relative squared error: 88.56016729807445
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.30844625972622064
Weighted FMeasure: 0.7138888888888888
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7750224098597266
Mean absolute error: 0.3156836527716139
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 68.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4247619047619048
Kappa statistic: 0.3069306930693069
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 84.6
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7078623277057337
Weighted AreaUnderROC: 0.7571238095238095
Root mean squared error: 0.4431896871869951
Relative absolute error: 62.837626137198136
Root relative squared error: 88.63793743739902
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.30349261469606426
Weighted FMeasure: 0.7111731843575418
Iteration time: 2.0
Weighted AreaUnderPRC: 0.776125960568537
Mean absolute error: 0.31418813068599066
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 70.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.42266666666666663
Kappa statistic: 0.3025540275049115
Training time: 1.0
		
Time end:Sat Oct 07 11.39.10 EEST 2017