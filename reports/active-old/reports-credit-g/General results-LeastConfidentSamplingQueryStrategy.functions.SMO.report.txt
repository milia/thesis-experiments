Sat Oct 07 11.39.15 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.15 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.579999999999995
Correctly Classified Instances: 68.41999999999999
Weighted Precision: 0.684445103911539
Weighted AreaUnderROC: 0.6165238095238095
Root mean squared error: 0.5614123710347445
Relative absolute error: 63.15999999999999
Root relative squared error: 112.28247420694893
Weighted TruePositiveRate: 0.6842
Weighted MatthewsCorrelation: 0.24260077472350763
Weighted FMeasure: 0.6792511197408607
Iteration time: 25.5
Weighted AreaUnderPRC: 0.6414932916955902
Mean absolute error: 0.3158
Coverage of cases: 68.41999999999999
Instances selection time: 5.1
Test time: 3.9
Accumulative iteration time: 25.5
Weighted Recall: 0.6842
Weighted FalsePositiveRate: 0.4511523809523809
Kappa statistic: 0.23686993466325248
Training time: 20.4
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.880000000000003
Correctly Classified Instances: 70.12
Weighted Precision: 0.6872214667131714
Weighted AreaUnderROC: 0.6101904761904762
Root mean squared error: 0.5461781631998319
Relative absolute error: 59.760000000000005
Root relative squared error: 109.23563263996638
Weighted TruePositiveRate: 0.7011999999999999
Weighted MatthewsCorrelation: 0.2453665549617722
Weighted FMeasure: 0.6864488441493637
Iteration time: 20.5
Weighted AreaUnderPRC: 0.6393896474478507
Mean absolute error: 0.2988
Coverage of cases: 70.12
Instances selection time: 2.5
Test time: 5.1
Accumulative iteration time: 46.0
Weighted Recall: 0.7011999999999999
Weighted FalsePositiveRate: 0.4808190476190476
Kappa statistic: 0.23624498591759607
Training time: 18.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.82
Correctly Classified Instances: 71.17999999999999
Weighted Precision: 0.6893130635487303
Weighted AreaUnderROC: 0.6010000000000001
Root mean squared error: 0.5364603895242047
Relative absolute error: 57.64
Root relative squared error: 107.29207790484095
Weighted TruePositiveRate: 0.7118
Weighted MatthewsCorrelation: 0.24295414746361638
Weighted FMeasure: 0.6870896779702605
Iteration time: 21.8
Weighted AreaUnderPRC: 0.6354020235972743
Mean absolute error: 0.28820000000000007
Coverage of cases: 71.17999999999999
Instances selection time: 2.6
Test time: 5.2
Accumulative iteration time: 67.8
Weighted Recall: 0.7118
Weighted FalsePositiveRate: 0.5098
Kappa statistic: 0.22743590175433986
Training time: 19.2
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.240000000000002
Correctly Classified Instances: 71.75999999999999
Weighted Precision: 0.6926157396823116
Weighted AreaUnderROC: 0.5933333333333334
Root mean squared error: 0.5310179624268577
Relative absolute error: 56.480000000000004
Root relative squared error: 106.20359248537156
Weighted TruePositiveRate: 0.7175999999999999
Weighted MatthewsCorrelation: 0.23969045391878768
Weighted FMeasure: 0.6828428044471051
Iteration time: 25.6
Weighted AreaUnderPRC: 0.632164778770395
Mean absolute error: 0.2824
Coverage of cases: 71.75999999999999
Instances selection time: 3.0
Test time: 5.4
Accumulative iteration time: 93.4
Weighted Recall: 0.7175999999999999
Weighted FalsePositiveRate: 0.5309333333333333
Kappa statistic: 0.2154058993740545
Training time: 22.6
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.02
Correctly Classified Instances: 71.98
Weighted Precision: 0.6991320619480248
Weighted AreaUnderROC: 0.5855714285714286
Root mean squared error: 0.5289129286396849
Relative absolute error: 56.04
Root relative squared error: 105.78258572793698
Weighted TruePositiveRate: 0.7197999999999999
Weighted MatthewsCorrelation: 0.23790946611486113
Weighted FMeasure: 0.6769582535844408
Iteration time: 30.3
Weighted AreaUnderPRC: 0.6287182640645719
Mean absolute error: 0.28020000000000006
Coverage of cases: 71.98
Instances selection time: 4.4
Test time: 3.4
Accumulative iteration time: 123.7
Weighted Recall: 0.7197999999999999
Weighted FalsePositiveRate: 0.5486571428571428
Kappa statistic: 0.20226132798924196
Training time: 25.9
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.019999999999992
Correctly Classified Instances: 71.97999999999999
Weighted Precision: 0.6949382586104134
Weighted AreaUnderROC: 0.5802380952380953
Root mean squared error: 0.5291193498009648
Relative absolute error: 56.039999999999985
Root relative squared error: 105.82386996019295
Weighted TruePositiveRate: 0.7198
Weighted MatthewsCorrelation: 0.22789562439732863
Weighted FMeasure: 0.6729042263146943
Iteration time: 33.4
Weighted AreaUnderPRC: 0.6253616558675943
Mean absolute error: 0.2802
Coverage of cases: 71.97999999999999
Instances selection time: 4.1
Test time: 2.9
Accumulative iteration time: 157.1
Weighted Recall: 0.7198
Weighted FalsePositiveRate: 0.5593238095238096
Kappa statistic: 0.1916441707347401
Training time: 29.3
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.379999999999995
Correctly Classified Instances: 71.62
Weighted Precision: 0.69116391035388
Weighted AreaUnderROC: 0.5618571428571428
Root mean squared error: 0.5326076611661791
Relative absolute error: 56.760000000000005
Root relative squared error: 106.52153223323583
Weighted TruePositiveRate: 0.7162
Weighted MatthewsCorrelation: 0.20036541057362972
Weighted FMeasure: 0.6560677007493165
Iteration time: 35.5
Weighted AreaUnderPRC: 0.6148502424694544
Mean absolute error: 0.2838
Coverage of cases: 71.62
Instances selection time: 3.8
Test time: 3.0
Accumulative iteration time: 192.6
Weighted Recall: 0.7162
Weighted FalsePositiveRate: 0.5924857142857142
Kappa statistic: 0.15355490004793448
Training time: 31.7
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.24
Correctly Classified Instances: 71.75999999999999
Weighted Precision: 0.6978615948934852
Weighted AreaUnderROC: 0.5611428571428572
Root mean squared error: 0.5312926736073046
Relative absolute error: 56.480000000000004
Root relative squared error: 106.25853472146093
Weighted TruePositiveRate: 0.7176
Weighted MatthewsCorrelation: 0.20542770169442393
Weighted FMeasure: 0.655375976468487
Iteration time: 37.1
Weighted AreaUnderPRC: 0.6149057933530951
Mean absolute error: 0.28240000000000004
Coverage of cases: 71.75999999999999
Instances selection time: 3.8
Test time: 3.2
Accumulative iteration time: 229.7
Weighted Recall: 0.7176
Weighted FalsePositiveRate: 0.5953142857142858
Kappa statistic: 0.1528421034898309
Training time: 33.3
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.899999999999995
Correctly Classified Instances: 71.1
Weighted Precision: 0.6930801067124179
Weighted AreaUnderROC: 0.5480476190476191
Root mean squared error: 0.537471858426355
Relative absolute error: 57.79999999999999
Root relative squared error: 107.49437168527099
Weighted TruePositiveRate: 0.711
Weighted MatthewsCorrelation: 0.17601834632260607
Weighted FMeasure: 0.6410549217448833
Iteration time: 30.9
Weighted AreaUnderPRC: 0.6069727759615463
Mean absolute error: 0.28900000000000003
Coverage of cases: 71.1
Instances selection time: 3.0
Test time: 3.1
Accumulative iteration time: 260.6
Weighted Recall: 0.711
Weighted FalsePositiveRate: 0.614904761904762
Kappa statistic: 0.12127964267281863
Training time: 27.9
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.059999999999995
Correctly Classified Instances: 70.93999999999998
Weighted Precision: 0.6873556712406969
Weighted AreaUnderROC: 0.5417619047619048
Root mean squared error: 0.5389594540006578
Relative absolute error: 58.11999999999999
Root relative squared error: 107.79189080013157
Weighted TruePositiveRate: 0.7093999999999999
Weighted MatthewsCorrelation: 0.15910242560363644
Weighted FMeasure: 0.6331698401387366
Iteration time: 33.0
Weighted AreaUnderPRC: 0.603431983159071
Mean absolute error: 0.2906
Coverage of cases: 70.93999999999998
Instances selection time: 1.4
Test time: 4.9
Accumulative iteration time: 293.6
Weighted Recall: 0.7093999999999999
Weighted FalsePositiveRate: 0.6258761904761905
Kappa statistic: 0.1058010209061657
Training time: 31.6
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.119999999999997
Correctly Classified Instances: 70.87999999999998
Weighted Precision: 0.6760408953142413
Weighted AreaUnderROC: 0.5379047619047619
Root mean squared error: 0.539558812723689
Relative absolute error: 58.239999999999995
Root relative squared error: 107.9117625447378
Weighted TruePositiveRate: 0.7087999999999999
Weighted MatthewsCorrelation: 0.1463846068329296
Weighted FMeasure: 0.6283372857701168
Iteration time: 37.7
Weighted AreaUnderPRC: 0.6013776687675066
Mean absolute error: 0.2912
Coverage of cases: 70.87999999999998
Instances selection time: 1.4
Test time: 5.0
Accumulative iteration time: 331.3
Weighted Recall: 0.7087999999999999
Weighted FalsePositiveRate: 0.6329904761904762
Kappa statistic: 0.0965087637134515
Training time: 36.3
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.240000000000002
Correctly Classified Instances: 70.75999999999999
Weighted Precision: 0.6516783519109546
Weighted AreaUnderROC: 0.5309523809523811
Root mean squared error: 0.5406580056496286
Relative absolute error: 58.480000000000004
Root relative squared error: 108.13160112992573
Weighted TruePositiveRate: 0.7076000000000001
Weighted MatthewsCorrelation: 0.12671062255311377
Weighted FMeasure: 0.6195759033218166
Iteration time: 39.6
Weighted AreaUnderPRC: 0.5975833513087763
Mean absolute error: 0.2924
Coverage of cases: 70.75999999999999
Instances selection time: 3.2
Test time: 3.0
Accumulative iteration time: 370.9
Weighted Recall: 0.7076000000000001
Weighted FalsePositiveRate: 0.6456952380952382
Kappa statistic: 0.07965111611139548
Training time: 36.4
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.359999999999996
Correctly Classified Instances: 70.63999999999999
Weighted Precision: 0.6409817925984239
Weighted AreaUnderROC: 0.5278095238095238
Root mean squared error: 0.541778293232852
Relative absolute error: 58.71999999999999
Root relative squared error: 108.35565864657042
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.11236957507516052
Weighted FMeasure: 0.6158228208819589
Iteration time: 47.9
Weighted AreaUnderPRC: 0.5955308524706588
Mean absolute error: 0.29359999999999997
Coverage of cases: 70.63999999999999
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 418.8
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.6507809523809523
Kappa statistic: 0.07157348125557883
Training time: 44.9
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.52
Correctly Classified Instances: 70.48
Weighted Precision: 0.6645679908601931
Weighted AreaUnderROC: 0.5203809523809524
Root mean squared error: 0.543254767657746
Relative absolute error: 59.04
Root relative squared error: 108.6509535315492
Weighted TruePositiveRate: 0.7048
Weighted MatthewsCorrelation: 0.09827538642641377
Weighted FMeasure: 0.605475331737304
Iteration time: 53.6
Weighted AreaUnderPRC: 0.5916832717156727
Mean absolute error: 0.29519999999999996
Coverage of cases: 70.48
Instances selection time: 3.0
Test time: 3.1
Accumulative iteration time: 472.4
Weighted Recall: 0.7048
Weighted FalsePositiveRate: 0.6640380952380952
Kappa statistic: 0.0527938595228679
Training time: 50.6
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.619999999999997
Correctly Classified Instances: 70.38
Weighted Precision: 0.6243757157065292
Weighted AreaUnderROC: 0.5179523809523809
Root mean squared error: 0.5442107994929316
Relative absolute error: 59.239999999999995
Root relative squared error: 108.84215989858633
Weighted TruePositiveRate: 0.7038
Weighted MatthewsCorrelation: 0.08577560964954137
Weighted FMeasure: 0.6026892268145552
Iteration time: 54.1
Weighted AreaUnderPRC: 0.5900160863679688
Mean absolute error: 0.2961999999999999
Coverage of cases: 70.38
Instances selection time: 2.9
Test time: 3.0
Accumulative iteration time: 526.5
Weighted Recall: 0.7038
Weighted FalsePositiveRate: 0.6678952380952381
Kappa statistic: 0.04683846290979695
Training time: 51.2
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.040000000000003
Correctly Classified Instances: 70.96000000000001
Weighted Precision: 0.7084355278673695
Weighted AreaUnderROC: 0.5327619047619048
Root mean squared error: 0.538825619841579
Relative absolute error: 58.08
Root relative squared error: 107.76512396831579
Weighted TruePositiveRate: 0.7096
Weighted MatthewsCorrelation: 0.14083963259551252
Weighted FMeasure: 0.6211488668434043
Iteration time: 75.5
Weighted AreaUnderPRC: 0.5986516508879794
Mean absolute error: 0.29040000000000005
Coverage of cases: 70.96000000000001
Instances selection time: 2.9
Test time: 3.0
Accumulative iteration time: 602.0
Weighted Recall: 0.7096
Weighted FalsePositiveRate: 0.6440761904761905
Kappa statistic: 0.08382762996674767
Training time: 72.6
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.459999999999997
Correctly Classified Instances: 71.53999999999999
Weighted Precision: 0.7008136854793127
Weighted AreaUnderROC: 0.5540476190476189
Root mean squared error: 0.5333998351265501
Relative absolute error: 56.919999999999995
Root relative squared error: 106.67996702531005
Weighted TruePositiveRate: 0.7154
Weighted MatthewsCorrelation: 0.19015807482587047
Weighted FMeasure: 0.6465195323918203
Iteration time: 77.7
Weighted AreaUnderPRC: 0.61077707791661
Mean absolute error: 0.2846
Coverage of cases: 71.53999999999999
Instances selection time: 2.7
Test time: 3.0
Accumulative iteration time: 679.7
Weighted Recall: 0.7154
Weighted FalsePositiveRate: 0.607304761904762
Kappa statistic: 0.1350474311816216
Training time: 75.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32000000000001
Weighted Precision: 0.7011171590706995
Weighted AreaUnderROC: 0.5859047619047619
Root mean squared error: 0.5259616411784088
Relative absolute error: 55.36
Root relative squared error: 105.19232823568177
Weighted TruePositiveRate: 0.7232000000000001
Weighted MatthewsCorrelation: 0.24326013777593064
Weighted FMeasure: 0.6797742129428899
Iteration time: 110.9
Weighted AreaUnderPRC: 0.6288532772851051
Mean absolute error: 0.2768
Coverage of cases: 72.32000000000001
Instances selection time: 2.7
Test time: 3.0
Accumulative iteration time: 790.6
Weighted Recall: 0.7232000000000001
Weighted FalsePositiveRate: 0.5513904761904762
Kappa statistic: 0.2064186663919881
Training time: 108.2
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.919999999999998
Correctly Classified Instances: 73.08
Weighted Precision: 0.7110702013345577
Weighted AreaUnderROC: 0.6145714285714285
Root mean squared error: 0.5184222912460441
Relative absolute error: 53.839999999999996
Root relative squared error: 103.68445824920882
Weighted TruePositiveRate: 0.7307999999999999
Weighted MatthewsCorrelation: 0.2862792013194566
Weighted FMeasure: 0.7023406500129057
Iteration time: 125.9
Weighted AreaUnderPRC: 0.6460440824474599
Mean absolute error: 0.2692
Coverage of cases: 73.08
Instances selection time: 2.4
Test time: 3.1
Accumulative iteration time: 916.5
Weighted Recall: 0.7307999999999999
Weighted FalsePositiveRate: 0.5016571428571429
Kappa statistic: 0.2630688599103305
Training time: 123.5
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.939999999999998
Correctly Classified Instances: 74.06
Weighted Precision: 0.7253524404272991
Weighted AreaUnderROC: 0.6470952380952382
Root mean squared error: 0.5087290030067768
Relative absolute error: 51.879999999999995
Root relative squared error: 101.74580060135538
Weighted TruePositiveRate: 0.7406
Weighted MatthewsCorrelation: 0.33393421036022203
Weighted FMeasure: 0.7244354181614876
Iteration time: 164.1
Weighted AreaUnderPRC: 0.6661160589151335
Mean absolute error: 0.25939999999999996
Coverage of cases: 74.06
Instances selection time: 0.9
Test time: 3.0
Accumulative iteration time: 1080.6
Weighted Recall: 0.7406
Weighted FalsePositiveRate: 0.4464095238095238
Kappa statistic: 0.32237390978501324
Training time: 163.2
		
Time end:Sat Oct 07 11.39.35 EEST 2017