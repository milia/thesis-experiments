Sat Oct 07 11.38.26 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.26 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 87.61
Incorrectly Classified Instances: 30.159999999999997
Correctly Classified Instances: 69.84
Weighted Precision: 0.6507224725869631
Weighted AreaUnderROC: 0.5932704761904761
Root mean squared error: 0.4925649965760951
Relative absolute error: 73.33626172384196
Root relative squared error: 98.512999315219
Weighted TruePositiveRate: 0.6984
Weighted MatthewsCorrelation: 0.17413085545892865
Weighted FMeasure: 0.6558728966424818
Iteration time: 11.4
Weighted AreaUnderPRC: 0.6362605256878605
Mean absolute error: 0.3666813086192097
Coverage of cases: 91.62
Instances selection time: 3.0
Test time: 2.6
Accumulative iteration time: 11.4
Weighted Recall: 0.6984
Weighted FalsePositiveRate: 0.5498285714285714
Kappa statistic: 0.16109673712309397
Training time: 8.4
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 89.11000000000001
Incorrectly Classified Instances: 30.380000000000003
Correctly Classified Instances: 69.62
Weighted Precision: 0.6440626021565409
Weighted AreaUnderROC: 0.5613257142857143
Root mean squared error: 0.4976574786772775
Relative absolute error: 73.29329488889384
Root relative squared error: 99.53149573545552
Weighted TruePositiveRate: 0.6962
Weighted MatthewsCorrelation: 0.15337511772639903
Weighted FMeasure: 0.6459752896860369
Iteration time: 7.2
Weighted AreaUnderPRC: 0.6170221656757203
Mean absolute error: 0.3664664744444691
Coverage of cases: 91.64000000000001
Instances selection time: 1.8
Test time: 2.4
Accumulative iteration time: 18.6
Weighted Recall: 0.6962
Weighted FalsePositiveRate: 0.5702
Kappa statistic: 0.1378218757525328
Training time: 5.4
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 87.64
Incorrectly Classified Instances: 30.28
Correctly Classified Instances: 69.72
Weighted Precision: 0.649761033373247
Weighted AreaUnderROC: 0.5893428571428572
Root mean squared error: 0.49553659825723695
Relative absolute error: 71.05450663308014
Root relative squared error: 99.1073196514474
Weighted TruePositiveRate: 0.6972
Weighted MatthewsCorrelation: 0.1674116472750524
Weighted FMeasure: 0.6515896129344136
Iteration time: 5.5
Weighted AreaUnderPRC: 0.6355944488991934
Mean absolute error: 0.35527253316540075
Coverage of cases: 92.42000000000002
Instances selection time: 2.3
Test time: 3.1
Accumulative iteration time: 24.1
Weighted Recall: 0.6972
Weighted FalsePositiveRate: 0.5594857142857144
Kappa statistic: 0.15160490779631788
Training time: 3.2
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 91.15
Incorrectly Classified Instances: 30.68
Correctly Classified Instances: 69.32
Weighted Precision: 0.6053878377606384
Weighted AreaUnderROC: 0.5765790476190477
Root mean squared error: 0.4946818069208626
Relative absolute error: 72.24101599605396
Root relative squared error: 98.93636138417251
Weighted TruePositiveRate: 0.6932
Weighted MatthewsCorrelation: 0.10820659048785194
Weighted FMeasure: 0.6264813523009671
Iteration time: 5.5
Weighted AreaUnderPRC: 0.6228790519736751
Mean absolute error: 0.3612050799802698
Coverage of cases: 94.28
Instances selection time: 1.8
Test time: 4.3
Accumulative iteration time: 29.6
Weighted Recall: 0.6932
Weighted FalsePositiveRate: 0.6095809523809526
Kappa statistic: 0.0949976210696443
Training time: 3.7
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 88.73
Incorrectly Classified Instances: 30.54
Correctly Classified Instances: 69.46
Weighted Precision: 0.623293283252498
Weighted AreaUnderROC: 0.5873447619047618
Root mean squared error: 0.4964827640682268
Relative absolute error: 70.22966031493567
Root relative squared error: 99.29655281364533
Weighted TruePositiveRate: 0.6946
Weighted MatthewsCorrelation: 0.12331334758265484
Weighted FMeasure: 0.6331210989478
Iteration time: 5.8
Weighted AreaUnderPRC: 0.6301754068251495
Mean absolute error: 0.35114830157467847
Coverage of cases: 93.3
Instances selection time: 1.6
Test time: 2.2
Accumulative iteration time: 35.4
Weighted Recall: 0.6946
Weighted FalsePositiveRate: 0.6047904761904761
Kappa statistic: 0.10527603952263262
Training time: 4.2
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 90.10999999999999
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.79999999999998
Weighted Precision: 0.6149500783939563
Weighted AreaUnderROC: 0.5683047619047619
Root mean squared error: 0.5072292718178715
Relative absolute error: 70.3520106978143
Root relative squared error: 101.44585436357428
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.1077090355721158
Weighted FMeasure: 0.6283463319329201
Iteration time: 7.1
Weighted AreaUnderPRC: 0.6163726236776104
Mean absolute error: 0.3517600534890715
Coverage of cases: 93.29999999999998
Instances selection time: 2.1
Test time: 2.4
Accumulative iteration time: 42.5
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.6083809523809525
Kappa statistic: 0.09301812098575295
Training time: 5.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 90.64
Incorrectly Classified Instances: 30.820000000000004
Correctly Classified Instances: 69.17999999999999
Weighted Precision: 0.6181845098606862
Weighted AreaUnderROC: 0.5508476190476191
Root mean squared error: 0.5056489838755137
Relative absolute error: 69.64216381544479
Root relative squared error: 101.12979677510276
Weighted TruePositiveRate: 0.6918
Weighted MatthewsCorrelation: 0.10779890131145757
Weighted FMeasure: 0.6271650332443522
Iteration time: 8.6
Weighted AreaUnderPRC: 0.6097540163822859
Mean absolute error: 0.348210819077224
Coverage of cases: 93.56
Instances selection time: 2.7
Test time: 1.9
Accumulative iteration time: 51.1
Weighted Recall: 0.6918
Weighted FalsePositiveRate: 0.6174190476190475
Kappa statistic: 0.08895050199675608
Training time: 5.9
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 89.4
Incorrectly Classified Instances: 30.82
Correctly Classified Instances: 69.17999999999999
Weighted Precision: 0.6184443859804925
Weighted AreaUnderROC: 0.5711742857142857
Root mean squared error: 0.5092099267712082
Relative absolute error: 68.54418528539607
Root relative squared error: 101.84198535424167
Weighted TruePositiveRate: 0.6918
Weighted MatthewsCorrelation: 0.11112833561418886
Weighted FMeasure: 0.6296326625192571
Iteration time: 7.2
Weighted AreaUnderPRC: 0.6186807203097412
Mean absolute error: 0.3427209264269804
Coverage of cases: 93.22
Instances selection time: 2.4
Test time: 2.5
Accumulative iteration time: 58.3
Weighted Recall: 0.6918
Weighted FalsePositiveRate: 0.6128476190476191
Kappa statistic: 0.09376159070405565
Training time: 4.8
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 88.84
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6318560187648177
Weighted AreaUnderROC: 0.5702933333333333
Root mean squared error: 0.5045324356141822
Relative absolute error: 67.26537633979618
Root relative squared error: 100.90648712283642
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.1407747912883484
Weighted FMeasure: 0.6400596192851822
Iteration time: 7.9
Weighted AreaUnderPRC: 0.6193541785792904
Mean absolute error: 0.33632688169898084
Coverage of cases: 93.08000000000001
Instances selection time: 1.2
Test time: 3.1
Accumulative iteration time: 66.2
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5956190476190476
Kappa statistic: 0.12142264770882635
Training time: 6.7
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 90.83000000000001
Incorrectly Classified Instances: 29.660000000000004
Correctly Classified Instances: 70.33999999999999
Weighted Precision: 0.6347847129465535
Weighted AreaUnderROC: 0.5821257142857142
Root mean squared error: 0.5003479636480404
Relative absolute error: 66.18348860898837
Root relative squared error: 100.06959272960805
Weighted TruePositiveRate: 0.7034
Weighted MatthewsCorrelation: 0.1475233510143073
Weighted FMeasure: 0.6431890553437943
Iteration time: 8.2
Weighted AreaUnderPRC: 0.6280919609091836
Mean absolute error: 0.3309174430449418
Coverage of cases: 94.02000000000001
Instances selection time: 2.8
Test time: 2.0
Accumulative iteration time: 74.4
Weighted Recall: 0.7034
Weighted FalsePositiveRate: 0.5934
Kappa statistic: 0.128070879342104
Training time: 5.4
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 88.43
Incorrectly Classified Instances: 29.620000000000005
Correctly Classified Instances: 70.38
Weighted Precision: 0.6363145573647195
Weighted AreaUnderROC: 0.5761361904761905
Root mean squared error: 0.4987791500117778
Relative absolute error: 66.6225065819878
Root relative squared error: 99.75583000235555
Weighted TruePositiveRate: 0.7038
Weighted MatthewsCorrelation: 0.15139333348875772
Weighted FMeasure: 0.6446181200011221
Iteration time: 8.8
Weighted AreaUnderPRC: 0.6247843508280663
Mean absolute error: 0.333112532909939
Coverage of cases: 92.82000000000001
Instances selection time: 2.3
Test time: 2.1
Accumulative iteration time: 83.2
Weighted Recall: 0.7038
Weighted FalsePositiveRate: 0.5898000000000001
Kappa statistic: 0.13213257050818145
Training time: 6.5
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 87.68
Incorrectly Classified Instances: 29.419999999999998
Correctly Classified Instances: 70.58
Weighted Precision: 0.6446755570148925
Weighted AreaUnderROC: 0.5775219047619047
Root mean squared error: 0.498337383325196
Relative absolute error: 65.43490622451625
Root relative squared error: 99.66747666503922
Weighted TruePositiveRate: 0.7058000000000001
Weighted MatthewsCorrelation: 0.1769547348431261
Weighted FMeasure: 0.6559011055156878
Iteration time: 10.3
Weighted AreaUnderPRC: 0.6304234368756526
Mean absolute error: 0.3271745311225813
Coverage of cases: 91.72
Instances selection time: 1.7
Test time: 2.8
Accumulative iteration time: 93.5
Weighted Recall: 0.7058000000000001
Weighted FalsePositiveRate: 0.5611333333333333
Kappa statistic: 0.16198888579224247
Training time: 8.6
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 87.11
Incorrectly Classified Instances: 29.840000000000003
Correctly Classified Instances: 70.16
Weighted Precision: 0.6403749225956348
Weighted AreaUnderROC: 0.5818695238095237
Root mean squared error: 0.504358562065812
Relative absolute error: 65.43343789755788
Root relative squared error: 100.87171241316238
Weighted TruePositiveRate: 0.7015999999999999
Weighted MatthewsCorrelation: 0.1667833151699123
Weighted FMeasure: 0.6516529620924562
Iteration time: 11.1
Weighted AreaUnderPRC: 0.6316777566138774
Mean absolute error: 0.32716718948778933
Coverage of cases: 91.26
Instances selection time: 2.0
Test time: 3.3
Accumulative iteration time: 104.6
Weighted Recall: 0.7015999999999999
Weighted FalsePositiveRate: 0.5667428571428571
Kappa statistic: 0.15187178392986772
Training time: 9.1
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 86.32000000000002
Incorrectly Classified Instances: 29.7
Correctly Classified Instances: 70.3
Weighted Precision: 0.6430545007969984
Weighted AreaUnderROC: 0.5942133333333334
Root mean squared error: 0.49593611446184144
Relative absolute error: 65.09301838515727
Root relative squared error: 99.18722289236831
Weighted TruePositiveRate: 0.7030000000000001
Weighted MatthewsCorrelation: 0.16891925523523071
Weighted FMeasure: 0.6510447808434425
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6407946804703413
Mean absolute error: 0.3254650919257863
Coverage of cases: 91.56
Instances selection time: 2.9
Test time: 2.0
Accumulative iteration time: 116.6
Weighted Recall: 0.7030000000000001
Weighted FalsePositiveRate: 0.5688095238095239
Kappa statistic: 0.15123192993899115
Training time: 9.1
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 83.56000000000002
Incorrectly Classified Instances: 29.74
Correctly Classified Instances: 70.26
Weighted Precision: 0.6431199311037001
Weighted AreaUnderROC: 0.5908276190476192
Root mean squared error: 0.5004418295138497
Relative absolute error: 65.41638099058544
Root relative squared error: 100.08836590276994
Weighted TruePositiveRate: 0.7026000000000001
Weighted MatthewsCorrelation: 0.17385551068779867
Weighted FMeasure: 0.65430908796853
Iteration time: 12.5
Weighted AreaUnderPRC: 0.6376826310208583
Mean absolute error: 0.32708190495292727
Coverage of cases: 90.28
Instances selection time: 1.9
Test time: 3.0
Accumulative iteration time: 129.1
Weighted Recall: 0.7026000000000001
Weighted FalsePositiveRate: 0.5598380952380952
Kappa statistic: 0.15943293883908377
Training time: 10.6
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 84.08
Incorrectly Classified Instances: 29.660000000000004
Correctly Classified Instances: 70.34
Weighted Precision: 0.6624555103222303
Weighted AreaUnderROC: 0.5912714285714287
Root mean squared error: 0.4980377115078819
Relative absolute error: 65.28942339988384
Root relative squared error: 99.60754230157639
Weighted TruePositiveRate: 0.7034
Weighted MatthewsCorrelation: 0.1940690651226525
Weighted FMeasure: 0.6629595293383979
Iteration time: 14.3
Weighted AreaUnderPRC: 0.6380325471925861
Mean absolute error: 0.3264471169994192
Coverage of cases: 89.97999999999999
Instances selection time: 2.9
Test time: 2.8
Accumulative iteration time: 143.4
Weighted Recall: 0.7034
Weighted FalsePositiveRate: 0.5446380952380953
Kappa statistic: 0.17717151712016327
Training time: 11.4
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 84.46000000000001
Incorrectly Classified Instances: 29.640000000000004
Correctly Classified Instances: 70.36
Weighted Precision: 0.6621107410833624
Weighted AreaUnderROC: 0.5933038095238097
Root mean squared error: 0.4940352747215243
Relative absolute error: 66.00755975227142
Root relative squared error: 98.80705494430488
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.1954518253938614
Weighted FMeasure: 0.6641937199269702
Iteration time: 12.3
Weighted AreaUnderPRC: 0.6393508717471214
Mean absolute error: 0.330037798761357
Coverage of cases: 90.11999999999999
Instances selection time: 1.1
Test time: 2.4
Accumulative iteration time: 155.7
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.542647619047619
Kappa statistic: 0.17959798548637831
Training time: 11.2
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 86.72999999999999
Incorrectly Classified Instances: 29.159999999999997
Correctly Classified Instances: 70.84
Weighted Precision: 0.6860722837682051
Weighted AreaUnderROC: 0.6140095238095238
Root mean squared error: 0.48711231657746445
Relative absolute error: 65.41183153014507
Root relative squared error: 97.4224633154929
Weighted TruePositiveRate: 0.7083999999999999
Weighted MatthewsCorrelation: 0.2262633152336136
Weighted FMeasure: 0.677146664181044
Iteration time: 13.4
Weighted AreaUnderPRC: 0.6538963299361275
Mean absolute error: 0.32705915765072535
Coverage of cases: 91.21999999999998
Instances selection time: 2.5
Test time: 1.8
Accumulative iteration time: 169.1
Weighted Recall: 0.7083999999999999
Weighted FalsePositiveRate: 0.5261142857142856
Kappa statistic: 0.2059515049162881
Training time: 10.9
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 88.28
Incorrectly Classified Instances: 29.02
Correctly Classified Instances: 70.97999999999999
Weighted Precision: 0.6848517683187716
Weighted AreaUnderROC: 0.6269933333333333
Root mean squared error: 0.48310371286472653
Relative absolute error: 66.07925657883257
Root relative squared error: 96.6207425729453
Weighted TruePositiveRate: 0.7098
Weighted MatthewsCorrelation: 0.23563350650311304
Weighted FMeasure: 0.6851514734702366
Iteration time: 14.1
Weighted AreaUnderPRC: 0.6629820586732872
Mean absolute error: 0.33039628289416284
Coverage of cases: 91.53999999999999
Instances selection time: 0.5
Test time: 2.8
Accumulative iteration time: 183.2
Weighted Recall: 0.7098
Weighted FalsePositiveRate: 0.5098952380952382
Kappa statistic: 0.22307725166684594
Training time: 13.6
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 89.51
Incorrectly Classified Instances: 29.179999999999996
Correctly Classified Instances: 70.82000000000001
Weighted Precision: 0.6895676343605686
Weighted AreaUnderROC: 0.6217828571428571
Root mean squared error: 0.4848117465391101
Relative absolute error: 67.9537693160186
Root relative squared error: 96.96234930782202
Weighted TruePositiveRate: 0.7081999999999999
Weighted MatthewsCorrelation: 0.2489916776191095
Weighted FMeasure: 0.6897422890164013
Iteration time: 15.7
Weighted AreaUnderPRC: 0.6596133088934983
Mean absolute error: 0.3397688465800929
Coverage of cases: 91.08000000000001
Instances selection time: 2.5
Test time: 2.5
Accumulative iteration time: 198.9
Weighted Recall: 0.7081999999999999
Weighted FalsePositiveRate: 0.4907714285714285
Kappa statistic: 0.23832570916387735
Training time: 13.2
		
Time end:Sat Oct 07 11.38.38 EEST 2017