Sat Oct 07 11.41.56 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.41.56 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.7190909090909091
Weighted AreaUnderROC: 0.6714285714285714
Root mean squared error: 0.5761944116355173
Relative absolute error: 66.4
Root relative squared error: 115.23888232710347
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.31652097680399843
Weighted FMeasure: 0.6809609609609608
Iteration time: 19.0
Weighted AreaUnderPRC: 0.673038961038961
Mean absolute error: 0.332
Coverage of cases: 66.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 19.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.3251428571428571
Kappa statistic: 0.3025210084033614
Training time: 18.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.7049568533012243
Weighted AreaUnderROC: 0.6552380952380953
Root mean squared error: 0.5727128425310541
Relative absolute error: 65.60000000000001
Root relative squared error: 114.54256850621083
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.2909188660443691
Weighted FMeasure: 0.6825470700232706
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6629730951903138
Mean absolute error: 0.328
Coverage of cases: 67.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 38.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.3615238095238095
Kappa statistic: 0.28446771378708563
Training time: 18.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.7073537777318095
Weighted AreaUnderROC: 0.6576190476190477
Root mean squared error: 0.5603570290448759
Relative absolute error: 62.8
Root relative squared error: 112.07140580897519
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.29989081911684473
Weighted FMeasure: 0.6937909423666545
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6650862457120201
Mean absolute error: 0.314
Coverage of cases: 68.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 67.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.3707619047619047
Kappa statistic: 0.2965949820788532
Training time: 28.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7239837398373984
Weighted AreaUnderROC: 0.6752380952380952
Root mean squared error: 0.532916503778969
Relative absolute error: 56.8
Root relative squared error: 106.58330075579381
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.34209490699408945
Weighted FMeasure: 0.7194332510074094
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6781760743321719
Mean absolute error: 0.284
Coverage of cases: 71.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 96.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.36552380952380953
Kappa statistic: 0.3413729128014842
Training time: 28.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7315508021390373
Weighted AreaUnderROC: 0.6857142857142858
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3593134953348084
Weighted FMeasure: 0.7246323529411764
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6851122994652407
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 129.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.34857142857142853
Kappa statistic: 0.3577981651376147
Training time: 32.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.739239618406285
Weighted AreaUnderROC: 0.6961904761904761
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3764945810047567
Weighted FMeasure: 0.7296826928328268
Iteration time: 108.0
Weighted AreaUnderPRC: 0.692167115600449
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 237.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.33161904761904765
Kappa statistic: 0.3738656987295825
Training time: 103.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7405274725274724
Weighted AreaUnderROC: 0.6976190476190476
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.37973247902349233
Weighted FMeasure: 0.7314643874643876
Iteration time: 49.0
Weighted AreaUnderPRC: 0.6932813186813187
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 286.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.3307619047619048
Kappa statistic: 0.37727272727272715
Training time: 48.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7315037740184901
Weighted AreaUnderROC: 0.6861904761904762
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.35873205230644417
Weighted FMeasure: 0.7232499965701897
Iteration time: 53.0
Weighted AreaUnderPRC: 0.6852246239239186
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 339.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.3456190476190476
Kappa statistic: 0.3567518248175182
Training time: 53.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7381024096385542
Weighted AreaUnderROC: 0.6933333333333334
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.3751394207580609
Weighted FMeasure: 0.7321086703922979
Iteration time: 42.0
Weighted AreaUnderPRC: 0.6907532128514056
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 381.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.3413333333333333
Kappa statistic: 0.3738489871086556
Training time: 41.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7357215447154472
Weighted AreaUnderROC: 0.6895238095238095
Root mean squared error: 0.521536192416212
Relative absolute error: 54.400000000000006
Root relative squared error: 104.30723848324239
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.36998307875991204
Weighted FMeasure: 0.7312881840634343
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6883463704994193
Mean absolute error: 0.272
Coverage of cases: 72.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 444.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.34895238095238096
Kappa statistic: 0.3692022263450835
Training time: 62.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7331798073947314
Weighted AreaUnderROC: 0.6861904761904762
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.3640477551601308
Weighted FMeasure: 0.7290976649878389
Iteration time: 81.0
Weighted AreaUnderPRC: 0.6860314612877975
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 525.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.3536190476190476
Kappa statistic: 0.36338289962825276
Training time: 81.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7346359522830112
Weighted AreaUnderROC: 0.6814285714285714
Root mean squared error: 0.5118593556827891
Relative absolute error: 52.400000000000006
Root relative squared error: 102.37187113655781
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.36797066827851516
Weighted FMeasure: 0.7361673368702059
Iteration time: 77.0
Weighted AreaUnderPRC: 0.6846345066698007
Mean absolute error: 0.262
Coverage of cases: 73.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 602.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.3751428571428571
Kappa statistic: 0.36776061776061775
Training time: 76.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7355706653715397
Weighted AreaUnderROC: 0.6833333333333333
Root mean squared error: 0.5118593556827891
Relative absolute error: 52.400000000000006
Root relative squared error: 102.37187113655781
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.370299311442842
Weighted FMeasure: 0.7367087390311335
Iteration time: 84.0
Weighted AreaUnderPRC: 0.6857955318115591
Mean absolute error: 0.262
Coverage of cases: 73.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 686.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.3713333333333333
Kappa statistic: 0.3701923076923077
Training time: 83.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.716165076258054
Weighted AreaUnderROC: 0.6561904761904762
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3231999273386658
Weighted FMeasure: 0.7193091509953576
Iteration time: 93.0
Weighted AreaUnderPRC: 0.667536220536661
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 779.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.4116190476190476
Kappa statistic: 0.3222003929273084
Training time: 92.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7113691661136917
Weighted AreaUnderROC: 0.6519047619047619
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.31215294713163616
Weighted FMeasure: 0.7141840679075995
Iteration time: 132.0
Weighted AreaUnderPRC: 0.6642622871046229
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 911.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.41419047619047616
Kappa statistic: 0.3115234374999999
Training time: 131.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7244678248103744
Weighted AreaUnderROC: 0.6657142857142858
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3429072399812674
Weighted FMeasure: 0.7274451176331732
Iteration time: 168.0
Weighted AreaUnderPRC: 0.6742929287986298
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1079.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4005714285714286
Kappa statistic: 0.3418467583497053
Training time: 167.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7051202413701305
Weighted AreaUnderROC: 0.6361904761904762
Root mean squared error: 0.5291502622129182
Relative absolute error: 56.00000000000001
Root relative squared error: 105.83005244258364
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2939569283526039
Weighted FMeasure: 0.7094038006687873
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6553462419025645
Mean absolute error: 0.28
Coverage of cases: 72.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1187.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.44761904761904764
Kappa statistic: 0.29006085192697756
Training time: 107.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7120558459604314
Weighted AreaUnderROC: 0.6442857142857142
Root mean squared error: 0.523450093132096
Relative absolute error: 54.800000000000004
Root relative squared error: 104.6900186264192
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.310525316139584
Weighted FMeasure: 0.7160217454319844
Iteration time: 199.0
Weighted AreaUnderPRC: 0.6608494541123535
Mean absolute error: 0.274
Coverage of cases: 72.6
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 1386.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.4374285714285715
Kappa statistic: 0.30668016194331976
Training time: 198.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7102631578947369
Weighted AreaUnderROC: 0.6428571428571429
Root mean squared error: 0.5253570214625479
Relative absolute error: 55.2
Root relative squared error: 105.07140429250958
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3065696697424829
Weighted FMeasure: 0.7143378995433791
Iteration time: 245.0
Weighted AreaUnderPRC: 0.6597368421052632
Mean absolute error: 0.276
Coverage of cases: 72.4
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 1631.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43828571428571433
Kappa statistic: 0.303030303030303
Training time: 245.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7213526043086993
Weighted AreaUnderROC: 0.6514285714285715
Root mean squared error: 0.5138093031466052
Relative absolute error: 52.800000000000004
Root relative squared error: 102.76186062932105
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.33080448925818157
Weighted FMeasure: 0.7244565217391304
Iteration time: 202.0
Weighted AreaUnderPRC: 0.666580638123807
Mean absolute error: 0.264
Coverage of cases: 73.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1833.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4331428571428572
Kappa statistic: 0.3251533742331288
Training time: 201.0
		
Time end:Sat Oct 07 11.41.58 EEST 2017