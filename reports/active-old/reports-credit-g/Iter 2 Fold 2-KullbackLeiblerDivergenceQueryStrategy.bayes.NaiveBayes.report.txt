Sat Oct 07 11.38.52 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.52 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 78.5
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6813471502590673
Weighted AreaUnderROC: 0.7023047619047618
Root mean squared error: 0.4735400282219765
Relative absolute error: 63.66931924232622
Root relative squared error: 94.7080056443953
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.23718057720397917
Weighted FMeasure: 0.6868824110671937
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7237600727205112
Mean absolute error: 0.3183465962116311
Coverage of cases: 92.2
Instances selection time: 20.0
Test time: 13.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4828571428571429
Kappa statistic: 0.2331288343558281
Training time: 1.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 73.9
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7180237220885567
Weighted AreaUnderROC: 0.7603619047619047
Root mean squared error: 0.4533088901856914
Relative absolute error: 56.596133174983244
Root relative squared error: 90.66177803713829
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.2707896266755677
Weighted FMeasure: 0.6866352269285684
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7804031490780314
Mean absolute error: 0.2829806658749162
Coverage of cases: 92.8
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 25.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.5520952380952382
Kappa statistic: 0.22313084112149537
Training time: 2.0
		
Time end:Sat Oct 07 11.38.52 EEST 2017