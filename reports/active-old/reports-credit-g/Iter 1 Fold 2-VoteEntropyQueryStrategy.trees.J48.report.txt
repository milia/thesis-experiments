Sat Oct 07 11.43.04 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.04 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 94.3
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6558333333333333
Weighted AreaUnderROC: 0.6062190476190477
Root mean squared error: 0.4916913900870568
Relative absolute error: 81.81020703933739
Root relative squared error: 98.33827801741137
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.16666666666666666
Weighted FMeasure: 0.6591304347826088
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6374207404248268
Mean absolute error: 0.4090510351966869
Coverage of cases: 93.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5586666666666668
Kappa statistic: 0.1538461538461537
Training time: 1.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 6.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 6.0
		
Time end:Sat Oct 07 11.43.05 EEST 2017