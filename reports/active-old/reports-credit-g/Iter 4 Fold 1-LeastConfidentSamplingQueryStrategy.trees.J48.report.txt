Sat Oct 07 11.40.06 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.40.06 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 67.4
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6713709270654695
Weighted AreaUnderROC: 0.615447619047619
Root mean squared error: 0.5046668130668718
Relative absolute error: 66.23622693096387
Root relative squared error: 100.93336261337436
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.19360590669443306
Weighted FMeasure: 0.6668759756546754
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6542303880214313
Mean absolute error: 0.3311811346548193
Coverage of cases: 83.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5602857142857144
Kappa statistic: 0.1722972972972971
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 100.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6213288396726048
Weighted AreaUnderROC: 0.5373238095238095
Root mean squared error: 0.4841892441475425
Relative absolute error: 74.07709406343153
Root relative squared error: 96.8378488295085
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.06696144368104526
Weighted FMeasure: 0.6106152901733012
Iteration time: 2.0
Weighted AreaUnderPRC: 0.59961732289827
Mean absolute error: 0.37038547031715763
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.6547619047619048
Kappa statistic: 0.04556650246305396
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 75.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6778127975311073
Weighted AreaUnderROC: 0.7174857142857143
Root mean squared error: 0.4665542892225382
Relative absolute error: 64.96293969144456
Root relative squared error: 93.31085784450764
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.20880889894162227
Weighted FMeasure: 0.6728721704935554
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7205307546866714
Mean absolute error: 0.3248146984572228
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 7.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5509523809523811
Kappa statistic: 0.18721973094170394
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.464354390525167
Relative absolute error: 78.0000000000003
Root relative squared error: 92.8708781050334
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3900000000000015
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 79.6
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.69843865075858
Weighted AreaUnderROC: 0.727552380952381
Root mean squared error: 0.4547751719267694
Relative absolute error: 61.1411687667172
Root relative squared error: 90.95503438535388
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2436549706883545
Weighted FMeasure: 0.6821281486447658
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7346770479066397
Mean absolute error: 0.305705843833586
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5487619047619047
Kappa statistic: 0.21052631578947367
Training time: 2.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 87.1
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6426704750648412
Weighted AreaUnderROC: 0.5668761904761904
Root mean squared error: 0.5275683053359417
Relative absolute error: 66.57878787878798
Root relative squared error: 105.51366106718834
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.13378773764523103
Weighted FMeasure: 0.6457995225344015
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6149320820371016
Mean absolute error: 0.33289393939393985
Coverage of cases: 89.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5840952380952381
Kappa statistic: 0.11995515695067281
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 90.9
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6008109794135995
Weighted AreaUnderROC: 0.5629809523809524
Root mean squared error: 0.5270173062463633
Relative absolute error: 69.47375859434693
Root relative squared error: 105.40346124927267
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.03776106288314798
Weighted FMeasure: 0.6065284653465346
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6112126376119672
Mean absolute error: 0.3473687929717347
Coverage of cases: 93.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.6531428571428571
Kappa statistic: 0.028776978417266345
Training time: 3.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 89.8
Incorrectly Classified Instances: 34.6
Correctly Classified Instances: 65.4
Weighted Precision: 0.5893368700265251
Weighted AreaUnderROC: 0.5437619047619048
Root mean squared error: 0.5392332809437153
Relative absolute error: 70.41084639498442
Root relative squared error: 107.84665618874305
Weighted TruePositiveRate: 0.654
Weighted MatthewsCorrelation: 0.019466160726095222
Weighted FMeasure: 0.6043371352392238
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6015309457154644
Mean absolute error: 0.3520542319749221
Coverage of cases: 91.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 25.0
Weighted Recall: 0.654
Weighted FalsePositiveRate: 0.6397142857142857
Kappa statistic: 0.01704545454545456
Training time: 4.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 97.6
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6921474131821403
Weighted AreaUnderROC: 0.603552380952381
Root mean squared error: 0.47731027403645837
Relative absolute error: 66.62620999915092
Root relative squared error: 95.46205480729168
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.249801649234031
Weighted FMeasure: 0.6909803921568628
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6425793525108537
Mean absolute error: 0.3331310499957546
Coverage of cases: 98.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 31.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5132380952380952
Kappa statistic: 0.23369565217391294
Training time: 6.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 97.4
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6567977593651535
Weighted AreaUnderROC: 0.5917238095238095
Root mean squared error: 0.4957846053473084
Relative absolute error: 68.12074074074066
Root relative squared error: 99.15692106946167
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.16972967422538476
Weighted FMeasure: 0.6604974856321837
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6345236109496694
Mean absolute error: 0.3406037037037033
Coverage of cases: 98.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5548571428571429
Kappa statistic: 0.15754923413566735
Training time: 6.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 68.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6664794561040497
Weighted AreaUnderROC: 0.6095047619047619
Root mean squared error: 0.5040009659730258
Relative absolute error: 65.06365591397841
Root relative squared error: 100.80019319460516
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.19928130279549125
Weighted FMeasure: 0.6725490196078431
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6474387753170905
Mean absolute error: 0.32531827956989207
Coverage of cases: 84.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5167619047619048
Kappa statistic: 0.19287211740041926
Training time: 6.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5091603002552971
Relative absolute error: 65.64698924731177
Root relative squared error: 101.83206005105943
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.32823494623655886
Coverage of cases: 82.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 6.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5103642556313887
Relative absolute error: 65.3288888888887
Root relative squared error: 102.07285112627773
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.3266444444444435
Coverage of cases: 82.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 6.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5112948358199133
Relative absolute error: 65.08837398373988
Root relative squared error: 102.25896716398266
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.32544186991869944
Coverage of cases: 82.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 65.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 7.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5120352243140851
Relative absolute error: 64.90014492753616
Root relative squared error: 102.40704486281702
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.3245007246376808
Coverage of cases: 82.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 72.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5126381134605978
Relative absolute error: 64.7488235294116
Root relative squared error: 102.52762269211956
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.32374411764705796
Coverage of cases: 82.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 80.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 8.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 66.7
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5831809523809524
Root mean squared error: 0.5131384428165794
Relative absolute error: 64.62452380952392
Root relative squared error: 102.62768856331587
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6317344040576078
Mean absolute error: 0.32312261904761963
Coverage of cases: 82.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 7.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 96.7
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6645716097329
Weighted AreaUnderROC: 0.5793428571428572
Root mean squared error: 0.495400495440133
Relative absolute error: 67.36277284119386
Root relative squared error: 99.0800990880266
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.1917996384335235
Weighted FMeasure: 0.6696315866725377
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6318005317027902
Mean absolute error: 0.3368138642059693
Coverage of cases: 97.6
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 98.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5311428571428571
Kappa statistic: 0.18269230769230746
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 96.7
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6645716097329
Weighted AreaUnderROC: 0.5793428571428572
Root mean squared error: 0.49005496745559124
Relative absolute error: 69.19536130536149
Root relative squared error: 98.01099349111824
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.1917996384335235
Weighted FMeasure: 0.6696315866725377
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6318005317027902
Mean absolute error: 0.34597680652680746
Coverage of cases: 97.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5311428571428571
Kappa statistic: 0.18269230769230746
Training time: 8.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 92.5
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6749005424954791
Weighted AreaUnderROC: 0.6321333333333333
Root mean squared error: 0.48404665194600865
Relative absolute error: 68.86568398348321
Root relative squared error: 96.80933038920173
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2196600187522237
Weighted FMeasure: 0.6804737465455981
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6657124500400762
Mean absolute error: 0.34432841991741603
Coverage of cases: 93.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 117.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5027619047619047
Kappa statistic: 0.21354166666666657
Training time: 9.0
		
Time end:Sat Oct 07 11.40.07 EEST 2017