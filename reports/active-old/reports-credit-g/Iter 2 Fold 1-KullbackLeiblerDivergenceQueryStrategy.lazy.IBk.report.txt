Sat Oct 07 11.38.57 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.57 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 39.6
Correctly Classified Instances: 60.4
Weighted Precision: 0.6232884399551067
Weighted AreaUnderROC: 0.5533333333333332
Root mean squared error: 0.6231624274583404
Relative absolute error: 79.60784313725505
Root relative squared error: 124.63248549166808
Weighted TruePositiveRate: 0.604
Weighted MatthewsCorrelation: 0.10234804143818627
Weighted FMeasure: 0.6121534288470991
Iteration time: 42.0
Weighted AreaUnderPRC: 0.6045997755331088
Mean absolute error: 0.39803921568627526
Coverage of cases: 60.4
Instances selection time: 41.0
Test time: 38.0
Accumulative iteration time: 42.0
Weighted Recall: 0.604
Weighted FalsePositiveRate: 0.49733333333333335
Kappa statistic: 0.10163339382940098
Training time: 1.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6354650484303886
Weighted AreaUnderROC: 0.5495238095238095
Root mean squared error: 0.5677977172084587
Relative absolute error: 64.97512437810919
Root relative squared error: 113.55954344169174
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.12258254249611124
Weighted FMeasure: 0.6428609913793103
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6039555607422104
Mean absolute error: 0.3248756218905459
Coverage of cases: 67.6
Instances selection time: 35.0
Test time: 83.0
Accumulative iteration time: 78.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.5769523809523811
Kappa statistic: 0.11378555798687111
Training time: 1.0
		
Time end:Sat Oct 07 11.38.57 EEST 2017