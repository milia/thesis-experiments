Sat Oct 07 11.38.54 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.54 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6852869002245079
Weighted AreaUnderROC: 0.63
Root mean squared error: 0.5779273310719956
Relative absolute error: 66.8
Root relative squared error: 115.58546621439912
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.2485272234939796
Weighted FMeasure: 0.6735027790486459
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6475708139717017
Mean absolute error: 0.334
Coverage of cases: 66.6
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 20.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.406
Kappa statistic: 0.24638989169675105
Training time: 14.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6724570493591491
Weighted AreaUnderROC: 0.599047619047619
Root mean squared error: 0.5549774770204643
Relative absolute error: 61.6
Root relative squared error: 110.99549540409286
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.21637526341415644
Weighted FMeasure: 0.6785326086956521
Iteration time: 163.0
Weighted AreaUnderPRC: 0.6314318334696847
Mean absolute error: 0.308
Coverage of cases: 69.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 183.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.49390476190476185
Kappa statistic: 0.21267893660531684
Training time: 159.0
		
Time end:Sat Oct 07 11.38.54 EEST 2017