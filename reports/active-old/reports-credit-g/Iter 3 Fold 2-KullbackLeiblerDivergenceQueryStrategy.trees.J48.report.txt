Sat Oct 07 11.39.01 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.01 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.632695374800638
Weighted AreaUnderROC: 0.5266666666666666
Root mean squared error: 0.47361116731680036
Relative absolute error: 80.14782608695708
Root relative squared error: 94.72223346336007
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.0922285951821433
Weighted FMeasure: 0.6214966984592811
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5929862838915472
Mean absolute error: 0.40073913043478543
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6386666666666667
Kappa statistic: 0.06779661016949144
Training time: 5.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4905354217587146
Relative absolute error: 70.0
Root relative squared error: 98.10708435174293
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 12.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.35
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 7.0
		
Time end:Sat Oct 07 11.39.01 EEST 2017