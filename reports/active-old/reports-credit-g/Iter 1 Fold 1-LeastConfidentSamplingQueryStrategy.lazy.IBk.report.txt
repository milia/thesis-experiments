Sat Oct 07 11.39.35 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.35 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6233333333333333
Weighted AreaUnderROC: 0.5380952380952381
Root mean squared error: 0.5706017113158316
Relative absolute error: 67.05882352941175
Root relative squared error: 114.12034226316632
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.09523809523809523
Weighted FMeasure: 0.6325691699604744
Iteration time: 22.0
Weighted AreaUnderPRC: 0.5979047619047619
Mean absolute error: 0.33529411764705874
Coverage of cases: 66.8
Instances selection time: 22.0
Test time: 26.0
Accumulative iteration time: 22.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.5918095238095238
Kappa statistic: 0.08791208791208799
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6243401759530791
Weighted AreaUnderROC: 0.5342857142857143
Root mean squared error: 0.5645845471055881
Relative absolute error: 65.37704918032796
Root relative squared error: 112.91690942111762
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.09283377618409412
Weighted FMeasure: 0.6303571428571428
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5962098031001255
Mean absolute error: 0.3268852459016398
Coverage of cases: 67.6
Instances selection time: 23.0
Test time: 31.0
Accumulative iteration time: 46.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.6074285714285714
Kappa statistic: 0.08163265306122462
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6269230769230769
Weighted AreaUnderROC: 0.5304761904761904
Root mean squared error: 0.558210434432484
Relative absolute error: 63.718309859155156
Root relative squared error: 111.6420868864968
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.09150180217433553
Weighted FMeasure: 0.6267500434253951
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5945582417582418
Mean absolute error: 0.31859154929577577
Coverage of cases: 68.4
Instances selection time: 25.0
Test time: 35.0
Accumulative iteration time: 71.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.6230476190476191
Kappa statistic: 0.07494145199063251
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6521739130434783
Weighted AreaUnderROC: 0.5380952380952381
Root mean squared error: 0.5443660535373832
Relative absolute error: 60.493827160493694
Root relative squared error: 108.87321070747664
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.12869789041755736
Weighted FMeasure: 0.6335282651072124
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5994782608695652
Mean absolute error: 0.3024691358024685
Coverage of cases: 70.0
Instances selection time: 27.0
Test time: 39.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.6238095238095238
Kappa statistic: 0.09638554216867447
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6421052631578948
Weighted AreaUnderROC: 0.5314285714285716
Root mean squared error: 0.5483516483516485
Relative absolute error: 61.230769230769134
Root relative squared error: 109.6703296703297
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.1086979871789546
Weighted FMeasure: 0.6264123257520177
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5956812030075188
Mean absolute error: 0.30615384615384567
Coverage of cases: 69.6
Instances selection time: 29.0
Test time: 44.0
Accumulative iteration time: 127.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.6331428571428571
Kappa statistic: 0.0799031476997578
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6555760368663595
Weighted AreaUnderROC: 0.5357142857142858
Root mean squared error: 0.5432071541794442
Relative absolute error: 60.00000000000008
Root relative squared error: 108.64143083588884
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.12828950144769635
Weighted FMeasure: 0.630402918255679
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5984562211981568
Mean absolute error: 0.3000000000000004
Coverage of cases: 70.2
Instances selection time: 29.0
Test time: 49.0
Accumulative iteration time: 156.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.6305714285714286
Kappa statistic: 0.09146341463414609
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6605402676091896
Weighted AreaUnderROC: 0.5371428571428571
Root mean squared error: 0.5416212919505332
Relative absolute error: 59.5675675675675
Root relative squared error: 108.32425839010665
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.13522361390780144
Weighted FMeasure: 0.6317348678601876
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5994399394092402
Mean absolute error: 0.2978378378378375
Coverage of cases: 70.4
Instances selection time: 30.0
Test time: 51.0
Accumulative iteration time: 186.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6297142857142857
Kappa statistic: 0.09535452322738387
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6604433760683761
Weighted AreaUnderROC: 0.5352380952380953
Root mean squared error: 0.541821736685276
Relative absolute error: 59.53719008264482
Root relative squared error: 108.3643473370552
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.13195448519086483
Weighted FMeasure: 0.6293935892957897
Iteration time: 29.0
Weighted AreaUnderPRC: 0.598456517094017
Mean absolute error: 0.2976859504132241
Coverage of cases: 70.4
Instances selection time: 29.0
Test time: 56.0
Accumulative iteration time: 215.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6335238095238096
Kappa statistic: 0.09090909090909079
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6660086663456909
Weighted AreaUnderROC: 0.5366666666666666
Root mean squared error: 0.5401576557880694
Relative absolute error: 59.11450381679377
Root relative squared error: 108.03153115761388
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1393521936064996
Weighted FMeasure: 0.6307125655191953
Iteration time: 29.0
Weighted AreaUnderPRC: 0.5994779971112181
Mean absolute error: 0.29557251908396887
Coverage of cases: 70.6
Instances selection time: 29.0
Test time: 58.0
Accumulative iteration time: 244.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6326666666666667
Kappa statistic: 0.09482758620689637
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6660086663456909
Weighted AreaUnderROC: 0.5366666666666666
Root mean squared error: 0.5403031264366339
Relative absolute error: 59.092198581560076
Root relative squared error: 108.06062528732679
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.1393521936064996
Weighted FMeasure: 0.6307125655191953
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5994779971112181
Mean absolute error: 0.29546099290780037
Coverage of cases: 70.6
Instances selection time: 28.0
Test time: 61.0
Accumulative iteration time: 272.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6326666666666667
Kappa statistic: 0.09482758620689637
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6604255319148936
Weighted AreaUnderROC: 0.5333333333333332
Root mean squared error: 0.5422644184627297
Relative absolute error: 59.470198675496775
Root relative squared error: 108.45288369254592
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12864083083132663
Weighted FMeasure: 0.6269918699186992
Iteration time: 27.0
Weighted AreaUnderPRC: 0.5974751773049645
Mean absolute error: 0.2973509933774839
Coverage of cases: 70.4
Instances selection time: 27.0
Test time: 65.0
Accumulative iteration time: 299.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6373333333333334
Kappa statistic: 0.0864197530864195
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6604255319148936
Weighted AreaUnderROC: 0.5333333333333332
Root mean squared error: 0.5423754560937137
Relative absolute error: 59.45341614906842
Root relative squared error: 108.47509121874275
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12864083083132663
Weighted FMeasure: 0.6269918699186992
Iteration time: 26.0
Weighted AreaUnderPRC: 0.5974751773049645
Mean absolute error: 0.2972670807453421
Coverage of cases: 70.4
Instances selection time: 26.0
Test time: 69.0
Accumulative iteration time: 325.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6373333333333334
Kappa statistic: 0.0864197530864195
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.660502421307506
Weighted AreaUnderROC: 0.5314285714285716
Root mean squared error: 0.542473552799727
Relative absolute error: 59.43859649122818
Root relative squared error: 108.49471055994539
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.12528072938528728
Weighted FMeasure: 0.6245277344924683
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5964960048426151
Mean absolute error: 0.2971929824561409
Coverage of cases: 70.4
Instances selection time: 24.0
Test time: 73.0
Accumulative iteration time: 349.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6411428571428571
Kappa statistic: 0.08188585607940439
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.666878083157153
Weighted AreaUnderROC: 0.5328571428571428
Root mean squared error: 0.5407248113712082
Relative absolute error: 59.02762430939229
Root relative squared error: 108.14496227424164
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.13323765821353606
Weighted FMeasure: 0.6258170809564017
Iteration time: 23.0
Weighted AreaUnderPRC: 0.5975279774489077
Mean absolute error: 0.29513812154696145
Coverage of cases: 70.6
Instances selection time: 23.0
Test time: 76.0
Accumulative iteration time: 372.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.6402857142857143
Kappa statistic: 0.08582089552238793
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6719148936170213
Weighted AreaUnderROC: 0.5380952380952381
Root mean squared error: 0.5389601627864565
Relative absolute error: 58.61780104712043
Root relative squared error: 107.79203255729131
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.147018092378659
Weighted FMeasure: 0.6320325203252032
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6005390070921987
Mean absolute error: 0.29308900523560216
Coverage of cases: 70.8
Instances selection time: 20.0
Test time: 80.0
Accumulative iteration time: 392.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.6318095238095238
Kappa statistic: 0.09876543209876519
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6651271649243251
Weighted AreaUnderROC: 0.5600000000000002
Root mean squared error: 0.5427094537132403
Relative absolute error: 59.40298507462668
Root relative squared error: 108.54189074264806
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.17172588621660795
Weighted FMeasure: 0.6557303807303807
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6113928537993446
Mean absolute error: 0.2970149253731334
Coverage of cases: 70.4
Instances selection time: 18.0
Test time: 83.0
Accumulative iteration time: 410.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.584
Kappa statistic: 0.14549653579676672
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.681410988453242
Weighted AreaUnderROC: 0.5847619047619048
Root mean squared error: 0.5353883510290877
Relative absolute error: 57.80094786729853
Root relative squared error: 107.07767020581753
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.21877097156977449
Weighted FMeasure: 0.6772459499263623
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6256507549803324
Mean absolute error: 0.2890047393364927
Coverage of cases: 71.2
Instances selection time: 15.0
Test time: 86.0
Accumulative iteration time: 425.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5424761904761903
Kappa statistic: 0.19821826280623592
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6883333333333334
Weighted AreaUnderROC: 0.5952380952380953
Root mean squared error: 0.531714256699039
Relative absolute error: 56.99547511312236
Root relative squared error: 106.34285133980781
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.23809523809523808
Weighted FMeasure: 0.685691699604743
Iteration time: 11.0
Weighted AreaUnderPRC: 0.631904761904762
Mean absolute error: 0.2849773755656118
Coverage of cases: 71.6
Instances selection time: 11.0
Test time: 90.0
Accumulative iteration time: 436.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.5255238095238095
Kappa statistic: 0.21978021978021967
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6954311734158757
Weighted AreaUnderROC: 0.6138095238095238
Root mean squared error: 0.5298904658972846
Relative absolute error: 56.588744588744554
Root relative squared error: 105.97809317945692
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2637848587217724
Weighted FMeasure: 0.6976692420600996
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6424123148550818
Mean absolute error: 0.2829437229437228
Coverage of cases: 71.8
Instances selection time: 8.0
Test time: 93.0
Accumulative iteration time: 444.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.49038095238095236
Kappa statistic: 0.2531779661016949
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6858419953541848
Weighted AreaUnderROC: 0.6109523809523809
Root mean squared error: 0.5410955436160543
Relative absolute error: 58.97095435684647
Root relative squared error: 108.21910872321085
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.24628879051460906
Weighted FMeasure: 0.690863332968596
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6393382153499613
Mean absolute error: 0.29485477178423236
Coverage of cases: 70.6
Instances selection time: 5.0
Test time: 96.0
Accumulative iteration time: 449.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.48409523809523813
Kappa statistic: 0.24070247933884284
Training time: 0.0
		
Time end:Sat Oct 07 11.39.37 EEST 2017