Sat Oct 07 11.39.37 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.37 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.6
Correctly Classified Instances: 61.4
Weighted Precision: 0.6234858276747192
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.6152458772126342
Relative absolute error: 77.64705882352965
Root relative squared error: 123.04917544252685
Weighted TruePositiveRate: 0.614
Weighted MatthewsCorrelation: 0.1033485954546663
Weighted FMeasure: 0.6183638638149848
Iteration time: 21.0
Weighted AreaUnderPRC: 0.604442995758315
Mean absolute error: 0.38823529411764823
Coverage of cases: 61.4
Instances selection time: 21.0
Test time: 26.0
Accumulative iteration time: 21.0
Weighted Recall: 0.614
Weighted FalsePositiveRate: 0.5082857142857142
Kappa statistic: 0.10315985130111523
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.2
Correctly Classified Instances: 62.8
Weighted Precision: 0.6223070265166418
Weighted AreaUnderROC: 0.5495238095238095
Root mean squared error: 0.6049535730818689
Relative absolute error: 74.81967213114774
Root relative squared error: 120.99071461637378
Weighted TruePositiveRate: 0.628
Weighted MatthewsCorrelation: 0.10065561493447908
Weighted FMeasure: 0.6250058045042952
Iteration time: 23.0
Weighted AreaUnderPRC: 0.602927626091746
Mean absolute error: 0.3740983606557387
Coverage of cases: 62.8
Instances selection time: 23.0
Test time: 31.0
Accumulative iteration time: 44.0
Weighted Recall: 0.628
Weighted FalsePositiveRate: 0.528952380952381
Kappa statistic: 0.10058027079303669
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6366539465756176
Weighted AreaUnderROC: 0.5614285714285715
Root mean squared error: 0.5807173941997986
Relative absolute error: 68.84507042253533
Root relative squared error: 116.14347883995973
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.13298043371744908
Weighted FMeasure: 0.6445636678571977
Iteration time: 24.0
Weighted AreaUnderPRC: 0.609513597107853
Mean absolute error: 0.34422535211267663
Coverage of cases: 65.8
Instances selection time: 24.0
Test time: 34.0
Accumulative iteration time: 68.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5351428571428573
Kappa statistic: 0.13109756097560973
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6506052676628109
Weighted AreaUnderROC: 0.5719047619047619
Root mean squared error: 0.5639705624459432
Relative absolute error: 64.83950617283935
Root relative squared error: 112.79411248918865
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.16294965083003313
Weighted FMeasure: 0.6582207618352197
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6157760436281823
Mean absolute error: 0.3241975308641967
Coverage of cases: 67.8
Instances selection time: 25.0
Test time: 38.0
Accumulative iteration time: 93.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5341904761904762
Kappa statistic: 0.15794979079497912
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.654919281145294
Weighted AreaUnderROC: 0.5742857142857142
Root mean squared error: 0.5590685665623695
Relative absolute error: 63.60439560439548
Root relative squared error: 111.8137133124739
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.17150977871471235
Weighted FMeasure: 0.6617964996568291
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6173772768809015
Mean absolute error: 0.3180219780219774
Coverage of cases: 68.4
Instances selection time: 26.0
Test time: 42.0
Accumulative iteration time: 119.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.5354285714285714
Kappa statistic: 0.16490486257928122
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6504166666666666
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5558195736840729
Relative absolute error: 62.77227722772288
Root relative squared error: 111.16391473681458
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.15476190476190477
Weighted FMeasure: 0.654703557312253
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6110297619047619
Mean absolute error: 0.3138613861386144
Coverage of cases: 68.8
Instances selection time: 27.0
Test time: 46.0
Accumulative iteration time: 146.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5641904761904761
Kappa statistic: 0.14285714285714268
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6438570907765221
Weighted AreaUnderROC: 0.5552380952380952
Root mean squared error: 0.5596190165764424
Relative absolute error: 63.53153153153141
Root relative squared error: 111.92380331528848
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.13952268169829904
Weighted FMeasure: 0.6488410144532315
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6072879815287396
Mean absolute error: 0.317657657657657
Coverage of cases: 68.4
Instances selection time: 27.0
Test time: 50.0
Accumulative iteration time: 173.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.5735238095238097
Kappa statistic: 0.12803532008830049
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6472275091993401
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.5562720224311933
Relative absolute error: 62.71074380165311
Root relative squared error: 111.25440448623867
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.1450279474451314
Weighted FMeasure: 0.6503497790868924
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6080169521634309
Mean absolute error: 0.31355371900826556
Coverage of cases: 68.8
Instances selection time: 27.0
Test time: 54.0
Accumulative iteration time: 200.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5756190476190477
Kappa statistic: 0.13140311804008883
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6428152492668621
Weighted AreaUnderROC: 0.5485714285714286
Root mean squared error: 0.5564466627865694
Relative absolute error: 62.68702290076325
Root relative squared error: 111.28933255731388
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.13151451626080002
Weighted FMeasure: 0.6440476190476192
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6040321742773357
Mean absolute error: 0.3134351145038163
Coverage of cases: 68.8
Instances selection time: 27.0
Test time: 58.0
Accumulative iteration time: 227.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.5908571428571429
Kappa statistic: 0.11564625850340121
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6442737277977553
Weighted AreaUnderROC: 0.5480952380952381
Root mean squared error: 0.5548098712711375
Relative absolute error: 62.269503546099095
Root relative squared error: 110.96197425422748
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.13283139486494489
Weighted FMeasure: 0.643824829536303
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6039052776869711
Mean absolute error: 0.31134751773049546
Coverage of cases: 69.0
Instances selection time: 26.0
Test time: 61.0
Accumulative iteration time: 253.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5938095238095238
Kappa statistic: 0.115296803652968
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6487220877051385
Weighted AreaUnderROC: 0.549047619047619
Root mean squared error: 0.5513478705546617
Relative absolute error: 61.45695364238421
Root relative squared error: 110.26957411093234
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.139342076052131
Weighted FMeasure: 0.6449845450311217
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6046774049732887
Mean absolute error: 0.307284768211921
Coverage of cases: 69.4
Instances selection time: 26.0
Test time: 64.0
Accumulative iteration time: 279.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5959047619047619
Kappa statistic: 0.11866359447004582
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6602127390148157
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.5442046609618624
Relative absolute error: 59.85093167701877
Root relative squared error: 108.84093219237248
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.15736988775386396
Weighted FMeasure: 0.6489372091154637
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6074007091300493
Mean absolute error: 0.29925465838509385
Coverage of cases: 70.2
Instances selection time: 25.0
Test time: 68.0
Accumulative iteration time: 304.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5962857142857143
Kappa statistic: 0.12967289719626166
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.652181317961483
Weighted AreaUnderROC: 0.5461904761904762
Root mean squared error: 0.5479438545762327
Relative absolute error: 60.631578947368524
Root relative squared error: 109.58877091524654
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.13987942176459978
Weighted FMeasure: 0.6423365027179496
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6035089130529718
Mean absolute error: 0.3031578947368426
Coverage of cases: 69.8
Instances selection time: 24.0
Test time: 72.0
Accumulative iteration time: 328.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.6056190476190477
Kappa statistic: 0.1138497652582158
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6626659292035398
Weighted AreaUnderROC: 0.5504761904761905
Root mean squared error: 0.5425608462731825
Relative absolute error: 59.42541436464096
Root relative squared error: 108.51216925463649
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15703858804309678
Weighted FMeasure: 0.6465805184009672
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6063788348082595
Mean absolute error: 0.2971270718232048
Coverage of cases: 70.4
Instances selection time: 22.0
Test time: 75.0
Accumulative iteration time: 350.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.603047619047619
Kappa statistic: 0.12529550827423164
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6626659292035398
Weighted AreaUnderROC: 0.5504761904761905
Root mean squared error: 0.5426390279572847
Relative absolute error: 59.41361256544499
Root relative squared error: 108.52780559145694
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15703858804309678
Weighted FMeasure: 0.6465805184009672
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6063788348082595
Mean absolute error: 0.297068062827225
Coverage of cases: 70.4
Instances selection time: 20.0
Test time: 79.0
Accumulative iteration time: 370.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.603047619047619
Kappa statistic: 0.12529550827423164
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6590909090909092
Weighted AreaUnderROC: 0.5571428571428572
Root mean squared error: 0.5463640275971795
Relative absolute error: 60.19900497512415
Root relative squared error: 109.2728055194359
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.16116459280507606
Weighted FMeasure: 0.6528028933092224
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6094545454545455
Mean absolute error: 0.30099502487562074
Coverage of cases: 70.0
Instances selection time: 20.0
Test time: 83.0
Accumulative iteration time: 390.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5857142857142856
Kappa statistic: 0.13793103448275842
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6618823529411765
Weighted AreaUnderROC: 0.569047619047619
Root mean squared error: 0.5482466011812068
Relative absolute error: 60.58767772511844
Root relative squared error: 109.64932023624137
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.1772283760921208
Weighted FMeasure: 0.662279569892473
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6155960784313724
Mean absolute error: 0.3029383886255922
Coverage of cases: 69.8
Instances selection time: 14.0
Test time: 91.0
Accumulative iteration time: 405.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5599047619047618
Kappa statistic: 0.16111111111111096
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6686450468846556
Weighted AreaUnderROC: 0.5842857142857142
Root mean squared error: 0.5483052095441104
Relative absolute error: 60.57918552036213
Root relative squared error: 109.66104190882209
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.20020792698249995
Weighted FMeasure: 0.6727710079324728
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6238174749455923
Mean absolute error: 0.30289592760181067
Coverage of cases: 69.8
Instances selection time: 14.0
Test time: 92.0
Accumulative iteration time: 419.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5294285714285714
Kappa statistic: 0.18991416309012868
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6712396851442297
Weighted AreaUnderROC: 0.5947619047619047
Root mean squared error: 0.5519782692687688
Relative absolute error: 61.36796536796537
Root relative squared error: 110.39565385375376
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.21176552780446894
Weighted FMeasure: 0.6772554215479364
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6292173741409242
Mean absolute error: 0.30683982683982686
Coverage of cases: 69.4
Instances selection time: 8.0
Test time: 94.0
Accumulative iteration time: 427.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5044761904761905
Kappa statistic: 0.20643153526970953
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6668421052631579
Weighted AreaUnderROC: 0.5952380952380953
Root mean squared error: 0.5609751340425877
Relative absolute error: 63.35269709543571
Root relative squared error: 112.19502680851754
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.20437977982832192
Weighted FMeasure: 0.672937595129376
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6287719298245614
Mean absolute error: 0.31676348547717853
Coverage of cases: 68.4
Instances selection time: 4.0
Test time: 98.0
Accumulative iteration time: 431.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.49352380952380953
Kappa statistic: 0.20202020202020218
Training time: 0.0
		
Time end:Sat Oct 07 11.39.40 EEST 2017