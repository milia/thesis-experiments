Sat Oct 07 11.38.52 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.52 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 77.6
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7024133663366336
Weighted AreaUnderROC: 0.7372761904761905
Root mean squared error: 0.4491036941950417
Relative absolute error: 59.46362501521727
Root relative squared error: 89.82073883900834
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.27923161896479953
Weighted FMeasure: 0.7035906061978392
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7688611043800561
Mean absolute error: 0.29731812507608635
Coverage of cases: 93.4
Instances selection time: 18.0
Test time: 12.0
Accumulative iteration time: 26.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.484
Kappa statistic: 0.267515923566879
Training time: 8.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 74.9
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7482051282051282
Weighted AreaUnderROC: 0.7776761904761905
Root mean squared error: 0.43674226352768897
Relative absolute error: 54.77130658062437
Root relative squared error: 87.3484527055378
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.3126311574289796
Weighted FMeasure: 0.694180602006689
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8018792803474935
Mean absolute error: 0.27385653290312184
Coverage of cases: 93.6
Instances selection time: 3.0
Test time: 17.0
Accumulative iteration time: 34.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.5507619047619048
Kappa statistic: 0.24404761904761896
Training time: 5.0
		
Time end:Sat Oct 07 11.38.52 EEST 2017