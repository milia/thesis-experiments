Sat Oct 07 11.38.06 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.38.06 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 39.6
Correctly Classified Instances: 60.4
Weighted Precision: 0.6232884399551067
Weighted AreaUnderROC: 0.5533333333333332
Root mean squared error: 0.6231624274583404
Relative absolute error: 79.60784313725505
Root relative squared error: 124.63248549166808
Weighted TruePositiveRate: 0.604
Weighted MatthewsCorrelation: 0.10234804143818627
Weighted FMeasure: 0.6121534288470991
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6045997755331088
Mean absolute error: 0.39803921568627526
Coverage of cases: 60.4
Instances selection time: 21.0
Test time: 27.0
Accumulative iteration time: 21.0
Weighted Recall: 0.604
Weighted FalsePositiveRate: 0.49733333333333335
Kappa statistic: 0.10163339382940098
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.2
Correctly Classified Instances: 62.8
Weighted Precision: 0.6294162129461586
Weighted AreaUnderROC: 0.559047619047619
Root mean squared error: 0.6049535730818688
Relative absolute error: 74.81967213114777
Root relative squared error: 120.99071461637377
Weighted TruePositiveRate: 0.628
Weighted MatthewsCorrelation: 0.1176523542284921
Weighted FMeasure: 0.6286988367900721
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6077068360556563
Mean absolute error: 0.3740983606557388
Coverage of cases: 62.8
Instances selection time: 23.0
Test time: 31.0
Accumulative iteration time: 44.0
Weighted Recall: 0.628
Weighted FalsePositiveRate: 0.5099047619047619
Kappa statistic: 0.11764705882352937
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.648310502283105
Weighted AreaUnderROC: 0.5785714285714286
Root mean squared error: 0.5807173941997984
Relative absolute error: 68.84507042253524
Root relative squared error: 116.14347883995968
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.1622037835747299
Weighted FMeasure: 0.6525874125874126
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6185251141552512
Mean absolute error: 0.3442253521126762
Coverage of cases: 65.8
Instances selection time: 25.0
Test time: 35.0
Accumulative iteration time: 69.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5008571428571429
Kappa statistic: 0.16176470588235306
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.8
Correctly Classified Instances: 66.2
Weighted Precision: 0.6448664035711975
Weighted AreaUnderROC: 0.5719047619047619
Root mean squared error: 0.5778107514068914
Relative absolute error: 67.99999999999994
Root relative squared error: 115.56215028137828
Weighted TruePositiveRate: 0.662
Weighted MatthewsCorrelation: 0.15301854535616732
Weighted FMeasure: 0.6515621929652191
Iteration time: 47.0
Weighted AreaUnderPRC: 0.6151170817968126
Mean absolute error: 0.3399999999999997
Coverage of cases: 66.2
Instances selection time: 47.0
Test time: 39.0
Accumulative iteration time: 116.0
Weighted Recall: 0.662
Weighted FalsePositiveRate: 0.5181904761904762
Kappa statistic: 0.15160642570281127
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6454375041842404
Weighted AreaUnderROC: 0.570952380952381
Root mean squared error: 0.574769397167508
Relative absolute error: 67.16483516483511
Root relative squared error: 114.9538794335016
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.15359755522403032
Weighted FMeasure: 0.6528779680242807
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6147543638838678
Mean absolute error: 0.33582417582417556
Coverage of cases: 66.6
Instances selection time: 28.0
Test time: 43.0
Accumulative iteration time: 144.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.5240952380952381
Kappa statistic: 0.15142276422764225
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.6
Correctly Classified Instances: 66.4
Weighted Precision: 0.6387878787878788
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5767995966780564
Relative absolute error: 67.52475247524761
Root relative squared error: 115.35991933561127
Weighted TruePositiveRate: 0.664
Weighted MatthewsCorrelation: 0.13696356726888634
Weighted FMeasure: 0.6472349272349273
Iteration time: 31.0
Weighted AreaUnderPRC: 0.609939393939394
Mean absolute error: 0.33762376237623803
Coverage of cases: 66.4
Instances selection time: 31.0
Test time: 47.0
Accumulative iteration time: 175.0
Weighted Recall: 0.664
Weighted FalsePositiveRate: 0.5401904761904762
Kappa statistic: 0.13402061855670114
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.64249547920434
Weighted AreaUnderROC: 0.5642857142857142
Root mean squared error: 0.5718805098863388
Relative absolute error: 66.30630630630617
Root relative squared error: 114.37610197726775
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.14465415869048878
Weighted FMeasure: 0.6508487958941966
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6113942133815552
Mean absolute error: 0.33153153153153087
Coverage of cases: 67.0
Instances selection time: 28.0
Test time: 51.0
Accumulative iteration time: 203.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.5414285714285714
Kappa statistic: 0.1406250000000001
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6400034740316137
Weighted AreaUnderROC: 0.5604761904761905
Root mean squared error: 0.5720924773226022
Relative absolute error: 66.28099173553741
Root relative squared error: 114.41849546452043
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.13805334655717552
Weighted FMeasure: 0.6485832371448784
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6094023325640834
Mean absolute error: 0.33140495867768704
Coverage of cases: 67.0
Instances selection time: 28.0
Test time: 54.0
Accumulative iteration time: 232.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.549047619047619
Kappa statistic: 0.13340336134453798
Training time: 1.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6453142451010254
Weighted AreaUnderROC: 0.5647619047619048
Root mean squared error: 0.5670461052444276
Relative absolute error: 65.06870229007625
Root relative squared error: 113.40922104888551
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.14952134554615948
Weighted FMeasure: 0.6532343857240906
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6118948928825262
Mean absolute error: 0.32534351145038126
Coverage of cases: 67.6
Instances selection time: 28.0
Test time: 58.0
Accumulative iteration time: 260.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.5464761904761904
Kappa statistic: 0.1437632135306554
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6459649122807017
Weighted AreaUnderROC: 0.5642857142857142
Root mean squared error: 0.5654457417621235
Relative absolute error: 64.65248226950337
Root relative squared error: 113.0891483524247
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.15018785229652765
Weighted FMeasure: 0.653585619678335
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6117368421052631
Mean absolute error: 0.3232624113475168
Coverage of cases: 67.8
Instances selection time: 28.0
Test time: 62.0
Accumulative iteration time: 288.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5494285714285715
Kappa statistic: 0.14361702127659587
Training time: 0.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6360607831677382
Weighted AreaUnderROC: 0.5519047619047619
Root mean squared error: 0.5690799803490137
Relative absolute error: 65.43046357615913
Root relative squared error: 113.81599606980275
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.1254822214196188
Weighted FMeasure: 0.6441296016722797
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6051066154574045
Mean absolute error: 0.32715231788079563
Coverage of cases: 67.4
Instances selection time: 34.0
Test time: 65.0
Accumulative iteration time: 322.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.5701904761904761
Kappa statistic: 0.11796536796536818
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6387535754528908
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.5656939496668089
Relative absolute error: 64.62111801242249
Root relative squared error: 113.13878993336178
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.13019858302295956
Weighted FMeasure: 0.6457677479282193
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6057598509144492
Mean absolute error: 0.3231055900621124
Coverage of cases: 67.8
Instances selection time: 26.0
Test time: 69.0
Accumulative iteration time: 348.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5722857142857143
Kappa statistic: 0.12117903930131005
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6416999909798851
Weighted AreaUnderROC: 0.5538095238095239
Root mean squared error: 0.562271198369966
Relative absolute error: 63.81286549707615
Root relative squared error: 112.4542396739932
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.1352116780188925
Weighted FMeasure: 0.6473450801148624
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6064392615532638
Mean absolute error: 0.31906432748538077
Coverage of cases: 68.2
Instances selection time: 24.0
Test time: 73.0
Accumulative iteration time: 372.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.5743809523809524
Kappa statistic: 0.12444933920704852
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6383521691578564
Weighted AreaUnderROC: 0.5504761904761905
Root mean squared error: 0.564127357272796
Relative absolute error: 64.19889502762449
Root relative squared error: 112.82547145455919
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.12749486431051463
Weighted FMeasure: 0.6443959640032725
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6046135374893669
Mean absolute error: 0.32099447513812246
Coverage of cases: 68.0
Instances selection time: 22.0
Test time: 77.0
Accumulative iteration time: 394.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.579047619047619
Kappa statistic: 0.1169977924944815
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6438131596676397
Weighted AreaUnderROC: 0.5528571428571429
Root mean squared error: 0.5588943315374837
Relative absolute error: 62.99476439790587
Root relative squared error: 111.77886630749674
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.13719489885511202
Weighted FMeasure: 0.64734779622672
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6061527124570917
Mean absolute error: 0.31497382198952933
Coverage of cases: 68.6
Instances selection time: 20.0
Test time: 80.0
Accumulative iteration time: 414.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5802857142857143
Kappa statistic: 0.12388392857142885
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6375055246176964
Weighted AreaUnderROC: 0.550952380952381
Root mean squared error: 0.5660425760628727
Relative absolute error: 64.57711442786042
Root relative squared error: 113.20851521257454
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.12674318291097378
Weighted FMeasure: 0.6443551247192338
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6047734052270249
Mean absolute error: 0.32288557213930213
Coverage of cases: 67.8
Instances selection time: 18.0
Test time: 84.0
Accumulative iteration time: 432.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5760952380952382
Kappa statistic: 0.11732456140350893
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6417092924126172
Weighted AreaUnderROC: 0.559047619047619
Root mean squared error: 0.5678644823405504
Relative absolute error: 64.96682464454972
Root relative squared error: 113.57289646811009
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.13966514692629675
Weighted FMeasure: 0.6495693320831243
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6088963341858482
Mean absolute error: 0.32483412322274857
Coverage of cases: 67.6
Instances selection time: 16.0
Test time: 87.0
Accumulative iteration time: 448.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.5579047619047618
Kappa statistic: 0.1327623126338332
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.0
Correctly Classified Instances: 65.0
Weighted Precision: 0.6292485498136262
Weighted AreaUnderROC: 0.5538095238095239
Root mean squared error: 0.5902723170224704
Relative absolute error: 70.13574660633479
Root relative squared error: 118.05446340449409
Weighted TruePositiveRate: 0.65
Weighted MatthewsCorrelation: 0.11580647103555444
Weighted FMeasure: 0.6372540543839218
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6054163391340788
Mean absolute error: 0.35067873303167396
Coverage of cases: 65.0
Instances selection time: 12.0
Test time: 90.0
Accumulative iteration time: 460.0
Weighted Recall: 0.65
Weighted FalsePositiveRate: 0.5423809523809523
Kappa statistic: 0.11437246963562758
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.633176110387058
Weighted AreaUnderROC: 0.5604761904761905
Root mean squared error: 0.5936937068364075
Relative absolute error: 70.92640692640694
Root relative squared error: 118.7387413672815
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.12605069445223452
Weighted FMeasure: 0.6387093580942294
Iteration time: 9.0
Weighted AreaUnderPRC: 0.608736643290097
Mean absolute error: 0.3546320346320347
Coverage of cases: 64.6
Instances selection time: 9.0
Test time: 93.0
Accumulative iteration time: 469.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.525047619047619
Kappa statistic: 0.12549407114624522
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.6453259144534312
Weighted AreaUnderROC: 0.5776190476190476
Root mean squared error: 0.5937469365525613
Relative absolute error: 70.92116182572614
Root relative squared error: 118.74938731051226
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.15553612134033978
Weighted FMeasure: 0.645660523189519
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6176802118587353
Mean absolute error: 0.3546058091286307
Coverage of cases: 64.6
Instances selection time: 5.0
Test time: 101.0
Accumulative iteration time: 474.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.49076190476190484
Kappa statistic: 0.15553435114503825
Training time: 0.0
		
Time end:Sat Oct 07 11.38.09 EEST 2017