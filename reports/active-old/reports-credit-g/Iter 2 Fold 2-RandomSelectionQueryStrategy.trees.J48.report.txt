Sat Oct 07 11.42.40 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.42.40 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 85.3
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6423038551073137
Weighted AreaUnderROC: 0.6517619047619048
Root mean squared error: 0.5199958487009875
Relative absolute error: 73.00620842572049
Root relative squared error: 103.99916974019749
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.1423776015694922
Weighted FMeasure: 0.6504970670623847
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6668677862840771
Mean absolute error: 0.36503104212860243
Coverage of cases: 86.4
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.5511428571428572
Kappa statistic: 0.13665254237288157
Training time: 1.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.45833333333333387
Relative absolute error: 83.33333333333385
Root relative squared error: 91.66666666666677
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 2.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.4166666666666693
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 83.9
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6229848302678366
Weighted AreaUnderROC: 0.6490761904761905
Root mean squared error: 0.5189601070436375
Relative absolute error: 73.37992869875232
Root relative squared error: 103.79202140872749
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.09822950805957215
Weighted FMeasure: 0.6333435488813974
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6708370207128675
Mean absolute error: 0.3668996434937616
Coverage of cases: 85.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.5732380952380953
Kappa statistic: 0.09427966101694935
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 81.5
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6829131652661063
Weighted AreaUnderROC: 0.5861428571428571
Root mean squared error: 0.5149047294151554
Relative absolute error: 68.75316986183378
Root relative squared error: 102.98094588303108
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.24326780972100304
Weighted FMeasure: 0.6878212827288013
Iteration time: 4.0
Weighted AreaUnderPRC: 0.63532133245045
Mean absolute error: 0.3437658493091689
Coverage of cases: 83.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.4655238095238095
Kappa statistic: 0.24151696606786424
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 88.9
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6851851851851852
Weighted AreaUnderROC: 0.6601428571428571
Root mean squared error: 0.49336671853687075
Relative absolute error: 69.61513412655023
Root relative squared error: 98.67334370737414
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.24794443415576684
Weighted FMeasure: 0.6903280542986424
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6789486198129785
Mean absolute error: 0.3480756706327512
Coverage of cases: 90.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.46761904761904766
Kappa statistic: 0.2454728370221326
Training time: 4.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 90.6
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6418666666666667
Weighted AreaUnderROC: 0.6693238095238095
Root mean squared error: 0.5059997990850731
Relative absolute error: 75.61606660024559
Root relative squared error: 101.19995981701462
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.1461462628969012
Weighted FMeasure: 0.6483510971786834
Iteration time: 4.0
Weighted AreaUnderPRC: 0.684078770607838
Mean absolute error: 0.37808033300122795
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.519904761904762
Kappa statistic: 0.14500000000000013
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 85.5
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6728320922411721
Weighted AreaUnderROC: 0.6464095238095238
Root mean squared error: 0.5045799748074341
Relative absolute error: 70.65709031543238
Root relative squared error: 100.91599496148682
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.2206505240853399
Weighted FMeasure: 0.6760299625468165
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6693551500418655
Mean absolute error: 0.3532854515771619
Coverage of cases: 87.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4647619047619048
Kappa statistic: 0.2202729044834309
Training time: 4.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 90.5
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6746513334964521
Weighted AreaUnderROC: 0.6754666666666667
Root mean squared error: 0.4846790084172919
Relative absolute error: 70.23557886858924
Root relative squared error: 96.93580168345838
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.22466336412565796
Weighted FMeasure: 0.678629317806279
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6904688858873892
Mean absolute error: 0.3511778943429462
Coverage of cases: 92.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.46685714285714286
Kappa statistic: 0.22396856581532432
Training time: 5.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 93.8
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6788027077726124
Weighted AreaUnderROC: 0.6966952380952383
Root mean squared error: 0.4672634384826125
Relative absolute error: 70.0485091476181
Root relative squared error: 93.4526876965225
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.2345170204469587
Weighted FMeasure: 0.6826973011251869
Iteration time: 6.0
Weighted AreaUnderPRC: 0.710080801861576
Mean absolute error: 0.3502425457380905
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.4613333333333334
Kappa statistic: 0.23379174852652246
Training time: 5.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 84.7
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6660694536606946
Weighted AreaUnderROC: 0.5842285714285714
Root mean squared error: 0.5227912222340718
Relative absolute error: 73.86325513196493
Root relative squared error: 104.55824444681437
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.2045139998448651
Weighted FMeasure: 0.6695886742477923
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6360254503916066
Mean absolute error: 0.36931627565982467
Coverage of cases: 87.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.47495238095238096
Kappa statistic: 0.20410156250000003
Training time: 5.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 87.8
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7011431849378214
Weighted AreaUnderROC: 0.6616857142857143
Root mean squared error: 0.4873552428609531
Relative absolute error: 68.82511262683677
Root relative squared error: 97.47104857219063
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.28742496623157465
Weighted FMeasure: 0.7047276884939702
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6776195226450279
Mean absolute error: 0.34412556313418385
Coverage of cases: 89.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.43285714285714283
Kappa statistic: 0.28641732283464555
Training time: 6.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 87.7
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.695408204877253
Weighted AreaUnderROC: 0.6655428571428571
Root mean squared error: 0.49102579891951703
Relative absolute error: 69.07160456770207
Root relative squared error: 98.20515978390341
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.2739316457321619
Weighted FMeasure: 0.6989692344008184
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6784048610562862
Mean absolute error: 0.34535802283851036
Coverage of cases: 89.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 65.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4392380952380952
Kappa statistic: 0.2730844793713162
Training time: 6.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 89.5
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6739359368143923
Weighted AreaUnderROC: 0.6739047619047619
Root mean squared error: 0.5029786722796197
Relative absolute error: 72.20156975156968
Root relative squared error: 100.59573445592393
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.22244026598871205
Weighted FMeasure: 0.6787556112329053
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6862174795431458
Mean absolute error: 0.36100784875784836
Coverage of cases: 89.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 72.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.47361904761904766
Kappa statistic: 0.22123015873015886
Training time: 7.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 88.8
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6852087114337568
Weighted AreaUnderROC: 0.6835333333333333
Root mean squared error: 0.5013982407113221
Relative absolute error: 70.56502886002893
Root relative squared error: 100.27964814226442
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.25048565738969286
Weighted FMeasure: 0.6845936355528568
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6929870750933371
Mean absolute error: 0.35282514430014467
Coverage of cases: 90.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.43257142857142855
Kappa statistic: 0.25047438330170785
Training time: 7.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 90.3
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6540439709988503
Weighted AreaUnderROC: 0.6618
Root mean squared error: 0.499068512362316
Relative absolute error: 73.27144643781271
Root relative squared error: 99.81370247246319
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.17229645001665783
Weighted FMeasure: 0.6614217456322721
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6767689862886268
Mean absolute error: 0.3663572321890636
Coverage of cases: 90.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 87.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5227619047619046
Kappa statistic: 0.16838842975206622
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 94.3
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6834636480336095
Weighted AreaUnderROC: 0.6680571428571429
Root mean squared error: 0.4580230084559925
Relative absolute error: 70.50014680021422
Root relative squared error: 91.6046016911985
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.22866358888697674
Weighted FMeasure: 0.6825431034482761
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6868921414626219
Mean absolute error: 0.3525007340010711
Coverage of cases: 96.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 95.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5272380952380953
Kappa statistic: 0.212253829321663
Training time: 7.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 87.8
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6697194210056939
Weighted AreaUnderROC: 0.631057142857143
Root mean squared error: 0.4994516176485984
Relative absolute error: 71.06620367687312
Root relative squared error: 99.89032352971968
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.20974504302867028
Weighted FMeasure: 0.6759754631612075
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6551324326445401
Mean absolute error: 0.3553310183843656
Coverage of cases: 88.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 103.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.49857142857142855
Kappa statistic: 0.20594262295081947
Training time: 8.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 91.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6982230241395336
Weighted AreaUnderROC: 0.6653238095238095
Root mean squared error: 0.4665754829003779
Relative absolute error: 68.51781100064687
Root relative squared error: 93.31509658007559
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.26584614031711834
Weighted FMeasure: 0.6976649310805534
Iteration time: 8.0
Weighted AreaUnderPRC: 0.68639128646017
Mean absolute error: 0.34258905500323433
Coverage of cases: 93.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 111.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.500095238095238
Kappa statistic: 0.2510775862068964
Training time: 8.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 94.4
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6994697200778995
Weighted AreaUnderROC: 0.6777714285714286
Root mean squared error: 0.47599219618436367
Relative absolute error: 69.83010216951004
Root relative squared error: 95.19843923687273
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2779997935851596
Weighted FMeasure: 0.703481156112735
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6883912463270396
Mean absolute error: 0.3491505108475502
Coverage of cases: 93.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 120.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4675238095238095
Kappa statistic: 0.27169421487603296
Training time: 9.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 93.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6842105263157895
Weighted AreaUnderROC: 0.6837142857142858
Root mean squared error: 0.47479360644599755
Relative absolute error: 69.9933456830481
Root relative squared error: 94.95872128919952
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.24525573579398632
Weighted FMeasure: 0.6894977168949772
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6936655032288349
Mean absolute error: 0.3499667284152405
Coverage of cases: 93.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 130.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4714285714285714
Kappa statistic: 0.24242424242424235
Training time: 9.0
		
Time end:Sat Oct 07 11.42.41 EEST 2017