Sat Oct 07 11.42.37 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.42.37 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46904157598234286
Relative absolute error: 76.00000000000045
Root relative squared error: 93.80831519646857
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3800000000000023
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4657729537494041
Relative absolute error: 77.33333333333331
Root relative squared error: 93.15459074988081
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3866666666666666
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46025724838820053
Relative absolute error: 80.5714285714282
Root relative squared error: 92.05144967764011
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.40285714285714097
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.45932185066682707
Relative absolute error: 81.49999999999935
Root relative squared error: 91.86437013336541
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.4074999999999967
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 93.2
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6946792787466746
Weighted AreaUnderROC: 0.6331619047619048
Root mean squared error: 0.4742521422343127
Relative absolute error: 68.89379797979784
Root relative squared error: 94.85042844686255
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.26426433631576013
Weighted FMeasure: 0.6980646804176215
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6654844439848421
Mean absolute error: 0.34446898989898916
Coverage of cases: 93.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4836190476190476
Kappa statistic: 0.2557651991614256
Training time: 3.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 96.2
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6931490746056479
Weighted AreaUnderROC: 0.6792
Root mean squared error: 0.4642943118754992
Relative absolute error: 68.34242078908746
Root relative squared error: 92.85886237509983
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.25443643472410893
Weighted FMeasure: 0.6933147862040148
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6923721889768358
Mean absolute error: 0.3417121039454373
Coverage of cases: 96.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5056190476190476
Kappa statistic: 0.2403017241379309
Training time: 4.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 93.7
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.69875
Weighted AreaUnderROC: 0.6334
Root mean squared error: 0.46918869278958725
Relative absolute error: 67.17596874670042
Root relative squared error: 93.83773855791745
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.27277236279499045
Weighted FMeasure: 0.7013333333333334
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6750007038324012
Mean absolute error: 0.3358798437335021
Coverage of cases: 95.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4819047619047619
Kappa statistic: 0.26315789473684204
Training time: 4.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 95.4
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6673963982057975
Weighted AreaUnderROC: 0.5933238095238095
Root mean squared error: 0.4863808584196773
Relative absolute error: 72.53145030723277
Root relative squared error: 97.27617168393546
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.20514035899048347
Weighted FMeasure: 0.6736637184419885
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6401150560666821
Mean absolute error: 0.36265725153616385
Coverage of cases: 95.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.49647619047619046
Kappa statistic: 0.2022357723577236
Training time: 5.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 94.2
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.674965034965035
Weighted AreaUnderROC: 0.6015047619047619
Root mean squared error: 0.4825389022224325
Relative absolute error: 70.45704390847246
Root relative squared error: 96.5077804444865
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.22124883943435492
Weighted FMeasure: 0.6808316008316009
Iteration time: 5.0
Weighted AreaUnderPRC: 0.64822390821301
Mean absolute error: 0.3522852195423623
Coverage of cases: 95.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.496
Kappa statistic: 0.21649484536082464
Training time: 5.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 93.6
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6965013377238115
Weighted AreaUnderROC: 0.615447619047619
Root mean squared error: 0.48560879165304005
Relative absolute error: 68.98198664081022
Root relative squared error: 97.121758330608
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2723555036342434
Weighted FMeasure: 0.7010612337551786
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6539517793838067
Mean absolute error: 0.3449099332040511
Coverage of cases: 94.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.46542857142857147
Kappa statistic: 0.2674180327868851
Training time: 6.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 90.9
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6983431516936671
Weighted AreaUnderROC: 0.6574380952380953
Root mean squared error: 0.48121526279895965
Relative absolute error: 68.38242536224843
Root relative squared error: 96.24305255979193
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2763563228299732
Weighted FMeasure: 0.7027162332692031
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6786157714933988
Mean absolute error: 0.34191212681124217
Coverage of cases: 91.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4645714285714286
Kappa statistic: 0.2710472279260778
Training time: 6.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 92.2
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7207566273932253
Weighted AreaUnderROC: 0.6817142857142857
Root mean squared error: 0.46321913951353527
Relative absolute error: 66.03651035239257
Root relative squared error: 92.64382790270706
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3286965354871651
Weighted FMeasure: 0.7236517097995409
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6998106627369513
Mean absolute error: 0.3301825517619628
Coverage of cases: 94.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4369523809523809
Kappa statistic: 0.3223819301848048
Training time: 6.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 87.8
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7072510104361466
Weighted AreaUnderROC: 0.6521619047619047
Root mean squared error: 0.47529809148755997
Relative absolute error: 68.05771637450579
Root relative squared error: 95.05961829751199
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.30236758828738425
Weighted FMeasure: 0.7101299412112534
Iteration time: 7.0
Weighted AreaUnderPRC: 0.679982612012833
Mean absolute error: 0.3402885818725289
Coverage of cases: 91.6
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 62.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.4197142857142857
Kappa statistic: 0.30175781249999983
Training time: 7.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 86.4
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7107936507936508
Weighted AreaUnderROC: 0.6453523809523808
Root mean squared error: 0.4795933867380656
Relative absolute error: 68.20699404585284
Root relative squared error: 95.91867734761313
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.31104631654389564
Weighted FMeasure: 0.7131034482758621
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6725266212455869
Mean absolute error: 0.34103497022926416
Coverage of cases: 89.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.41123809523809524
Kappa statistic: 0.3106796116504854
Training time: 7.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 95.2
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7020979020979021
Weighted AreaUnderROC: 0.689247619047619
Root mean squared error: 0.4540884606253563
Relative absolute error: 68.95767782353145
Root relative squared error: 90.81769212507126
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.28446279355845633
Weighted FMeasure: 0.706029106029106
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7035643855963258
Mean absolute error: 0.34478838911765725
Coverage of cases: 96.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 78.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.46285714285714286
Kappa statistic: 0.2783505154639175
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 93.2
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.70158539514773
Weighted AreaUnderROC: 0.6414857142857142
Root mean squared error: 0.4718786457850544
Relative absolute error: 69.09472222222212
Root relative squared error: 94.37572915701088
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.2889936067666398
Weighted FMeasure: 0.70437734082397
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6726402258811088
Mean absolute error: 0.3454736111111106
Coverage of cases: 94.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 86.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4260952380952381
Kappa statistic: 0.2884990253411305
Training time: 8.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 93.7
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7277915019762846
Weighted AreaUnderROC: 0.6580571428571429
Root mean squared error: 0.4633094563887807
Relative absolute error: 66.46000381546159
Root relative squared error: 92.66189127775614
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.35049573668479617
Weighted FMeasure: 0.7308836602856634
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6827372578780776
Mean absolute error: 0.33230001907730794
Coverage of cases: 94.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 95.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.3988571428571429
Kappa statistic: 0.34911242603550285
Training time: 8.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.3
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7146052631578946
Weighted AreaUnderROC: 0.6352952380952381
Root mean squared error: 0.4691158122683167
Relative absolute error: 67.5317487284713
Root relative squared error: 93.82316245366334
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.316788658733899
Weighted FMeasure: 0.7184779299847793
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6705248502505249
Mean absolute error: 0.3376587436423565
Coverage of cases: 93.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 104.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.43276190476190474
Kappa statistic: 0.3131313131313131
Training time: 9.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 97.5
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7002084346557355
Weighted AreaUnderROC: 0.6820857142857143
Root mean squared error: 0.44729226182651655
Relative absolute error: 69.28554391549459
Root relative squared error: 89.45845236530332
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.28039180988652934
Weighted FMeasure: 0.7043721711539359
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7130084971213179
Mean absolute error: 0.3464277195774729
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 114.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.46371428571428575
Kappa statistic: 0.27469135802469136
Training time: 9.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 95.6
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7035
Weighted AreaUnderROC: 0.6446
Root mean squared error: 0.459648911191491
Relative absolute error: 68.87082584953157
Root relative squared error: 91.9297822382982
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.28368325730679006
Weighted FMeasure: 0.7056
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6798521083767362
Mean absolute error: 0.34435412924765785
Coverage of cases: 96.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 125.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.4763809523809524
Kappa statistic: 0.27368421052631575
Training time: 10.0
		
Time end:Sat Oct 07 11.42.38 EEST 2017