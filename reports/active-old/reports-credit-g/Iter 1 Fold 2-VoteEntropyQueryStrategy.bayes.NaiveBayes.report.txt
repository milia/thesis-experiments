Sat Oct 07 11.42.58 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.42.58 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 85.7
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.7234375
Weighted AreaUnderROC: 0.7414285714285714
Root mean squared error: 0.46040666009800707
Relative absolute error: 67.07491765410924
Root relative squared error: 92.08133201960142
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.33561084188120116
Weighted FMeasure: 0.7049645390070923
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7564598005565945
Mean absolute error: 0.3353745882705462
Coverage of cases: 94.2
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.33980952380952373
Kappa statistic: 0.32980599647266307
Training time: 0.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 76.2
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.702494950695022
Weighted AreaUnderROC: 0.7773904761904762
Root mean squared error: 0.45291904513373277
Relative absolute error: 57.177334699789974
Root relative squared error: 90.58380902674655
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2458125729643654
Weighted FMeasure: 0.6805161101668586
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7870751735185021
Mean absolute error: 0.28588667349894986
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 8.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5555238095238096
Kappa statistic: 0.2071759259259259
Training time: 2.0
		
Time end:Sat Oct 07 11.42.58 EEST 2017