Sat Oct 07 11.39.07 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.07 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 78.5
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6813471502590673
Weighted AreaUnderROC: 0.7023047619047618
Root mean squared error: 0.4735400282219765
Relative absolute error: 63.66931924232622
Root relative squared error: 94.7080056443953
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.23718057720397917
Weighted FMeasure: 0.6868824110671937
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7237600727205112
Mean absolute error: 0.3183465962116311
Coverage of cases: 92.2
Instances selection time: 16.0
Test time: 11.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4828571428571429
Kappa statistic: 0.2331288343558281
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 77.9
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7102631578947369
Weighted AreaUnderROC: 0.706704761904762
Root mean squared error: 0.46202103208592665
Relative absolute error: 61.797142592750845
Root relative squared error: 92.40420641718534
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3065696697424829
Weighted FMeasure: 0.7143378995433791
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7306797550553206
Mean absolute error: 0.3089857129637542
Coverage of cases: 92.6
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 25.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.43828571428571433
Kappa statistic: 0.303030303030303
Training time: 1.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 80.2
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6990133836650652
Weighted AreaUnderROC: 0.731657142857143
Root mean squared error: 0.45705876553441654
Relative absolute error: 62.347997391739604
Root relative squared error: 91.41175310688331
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.28095087143952824
Weighted FMeasure: 0.703492791216746
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7503508439460355
Mean absolute error: 0.31173998695869803
Coverage of cases: 93.8
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 30.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.44723809523809516
Kappa statistic: 0.2785571142284568
Training time: 1.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 83.3
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6840316205533598
Weighted AreaUnderROC: 0.7264761904761905
Root mean squared error: 0.45288565885981374
Relative absolute error: 63.01615615879794
Root relative squared error: 90.57713177196275
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.24371313147221071
Weighted FMeasure: 0.6894159928122193
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7439462182159855
Mean absolute error: 0.3150807807939897
Coverage of cases: 95.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 34.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.47819047619047617
Kappa statistic: 0.23979591836734687
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 84.1
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6972368421052632
Weighted AreaUnderROC: 0.720704761904762
Root mean squared error: 0.4524135612264576
Relative absolute error: 63.750075755857026
Root relative squared error: 90.48271224529152
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.27591270276823454
Weighted FMeasure: 0.701917808219178
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7371977393979499
Mean absolute error: 0.31875037877928514
Coverage of cases: 95.4
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 39.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4548571428571429
Kappa statistic: 0.27272727272727265
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 85.3
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.705921052631579
Weighted AreaUnderROC: 0.7376571428571428
Root mean squared error: 0.4442285333693843
Relative absolute error: 62.94904222404436
Root relative squared error: 88.84570667387686
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.29635068075106674
Weighted FMeasure: 0.7101978691019787
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7529926988288838
Mean absolute error: 0.3147452111202218
Coverage of cases: 96.4
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 43.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.44380952380952376
Kappa statistic: 0.2929292929292929
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 87.6
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7020979020979021
Weighted AreaUnderROC: 0.7441904761904762
Root mean squared error: 0.43983987077015785
Relative absolute error: 63.669366648262795
Root relative squared error: 87.96797415403157
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.28446279355845633
Weighted FMeasure: 0.706029106029106
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7635194532513663
Mean absolute error: 0.31834683324131396
Coverage of cases: 97.2
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 47.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.46285714285714286
Kappa statistic: 0.2783505154639175
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 90.9
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7125038643551878
Weighted AreaUnderROC: 0.7481714285714286
Root mean squared error: 0.43144084883585426
Relative absolute error: 64.61679641818783
Root relative squared error: 86.28816976717086
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.30753888208789715
Weighted FMeasure: 0.7152253719540616
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7746668937034482
Mean absolute error: 0.3230839820909392
Coverage of cases: 98.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 51.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.4547619047619048
Kappa statistic: 0.299792531120332
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 90.9
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7092128117835059
Weighted AreaUnderROC: 0.7452000000000001
Root mean squared error: 0.42991498858756644
Relative absolute error: 65.32173878176538
Root relative squared error: 85.98299771751329
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.3013949417132357
Weighted FMeasure: 0.7127587762275832
Iteration time: 3.0
Weighted AreaUnderPRC: 0.769073610734446
Mean absolute error: 0.3266086939088269
Coverage of cases: 98.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 54.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.45266666666666666
Kappa statistic: 0.2952674897119342
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 90.4
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.727746907228282
Weighted AreaUnderROC: 0.7666666666666667
Root mean squared error: 0.42279185175501266
Relative absolute error: 63.17138074815384
Root relative squared error: 84.55837035100254
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.34540104100741226
Weighted FMeasure: 0.7303279661148113
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7850170905545063
Mean absolute error: 0.3158569037407692
Coverage of cases: 98.6
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 58.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.4267619047619048
Kappa statistic: 0.33913934426229503
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 90.1
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7085548698937093
Weighted AreaUnderROC: 0.7600380952380954
Root mean squared error: 0.423949237204503
Relative absolute error: 63.45440041503172
Root relative squared error: 84.7898474409006
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.29914046229885993
Weighted FMeasure: 0.7118930382088277
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7801285431579249
Mean absolute error: 0.3172720020751586
Coverage of cases: 98.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 61.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.45647619047619054
Kappa statistic: 0.2923553719008264
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 89.5
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7103831330764
Weighted AreaUnderROC: 0.7661523809523809
Root mean squared error: 0.42197300074004523
Relative absolute error: 62.23598140402428
Root relative squared error: 84.39460014800905
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2989945113654187
Weighted FMeasure: 0.7115171738886306
Iteration time: 3.0
Weighted AreaUnderPRC: 0.782940017999465
Mean absolute error: 0.3111799070201214
Coverage of cases: 97.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 64.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.47
Kappa statistic: 0.2879746835443037
Training time: 1.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 90.1
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7225
Weighted AreaUnderROC: 0.757104761904762
Root mean squared error: 0.4222121517624492
Relative absolute error: 63.17759881634672
Root relative squared error: 84.44243035248984
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.32732683535398854
Weighted FMeasure: 0.7226666666666667
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7786532363933725
Mean absolute error: 0.3158879940817336
Coverage of cases: 98.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 67.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.4542857142857143
Kappa statistic: 0.3157894736842105
Training time: 1.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 91.7
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7163576300085251
Weighted AreaUnderROC: 0.7612761904761904
Root mean squared error: 0.4203414360566547
Relative absolute error: 64.06879274020636
Root relative squared error: 84.06828721133094
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3086149214339138
Weighted FMeasure: 0.7144639002158791
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7802038687529362
Mean absolute error: 0.3203439637010318
Coverage of cases: 98.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 69.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.47504761904761905
Kappa statistic: 0.2933618843683084
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 91.1
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7160671052309799
Weighted AreaUnderROC: 0.7545904761904761
Root mean squared error: 0.42293040846258
Relative absolute error: 64.4228556982977
Root relative squared error: 84.586081692516
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3140287972949645
Weighted FMeasure: 0.717660629342138
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7758536304947545
Mean absolute error: 0.3221142784914885
Coverage of cases: 98.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 71.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4568571428571429
Kappa statistic: 0.30439330543933046
Training time: 1.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 89.1
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7279428904428905
Weighted AreaUnderROC: 0.7651619047619048
Root mean squared error: 0.420713985461734
Relative absolute error: 62.007284950422836
Root relative squared error: 84.1427970923468
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3419426003362873
Weighted FMeasure: 0.7287116590318972
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7845935673779075
Mean absolute error: 0.3100364247521142
Coverage of cases: 98.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 73.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.44114285714285717
Kappa statistic: 0.3319415448851774
Training time: 1.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 88.1
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7186383061383061
Weighted AreaUnderROC: 0.7606285714285715
Root mean squared error: 0.423568081329583
Relative absolute error: 62.03284767313367
Root relative squared error: 84.7136162659166
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3204367764157661
Weighted FMeasure: 0.720233898376644
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7803583611240353
Mean absolute error: 0.31016423836566837
Coverage of cases: 97.8
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 75.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.45219047619047625
Kappa statistic: 0.3110647181628392
Training time: 1.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 87.4
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7232905982905983
Weighted AreaUnderROC: 0.7653523809523809
Root mean squared error: 0.4236722927937324
Relative absolute error: 61.80401852524978
Root relative squared error: 84.73445855874648
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.3311896883760267
Weighted FMeasure: 0.7244727787042706
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7850772834731449
Mean absolute error: 0.3090200926262489
Coverage of cases: 98.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 78.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.4466666666666667
Kappa statistic: 0.3215031315240083
Training time: 1.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.3
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7411378655873408
Weighted AreaUnderROC: 0.7725523809523811
Root mean squared error: 0.42164689504429387
Relative absolute error: 61.069286392532405
Root relative squared error: 84.32937900885878
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.3767062713101988
Weighted FMeasure: 0.7428708514117969
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7920013808268029
Mean absolute error: 0.30534643196266203
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 80.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.41019047619047616
Kappa statistic: 0.3698770491803278
Training time: 1.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 87.0
Incorrectly Classified Instances: 25.2
Correctly Classified Instances: 74.8
Weighted Precision: 0.7346877556585765
Weighted AreaUnderROC: 0.7662095238095238
Root mean squared error: 0.4254594695592054
Relative absolute error: 61.92577247111268
Root relative squared error: 85.09189391184108
Weighted TruePositiveRate: 0.748
Weighted MatthewsCorrelation: 0.36201245994291564
Weighted FMeasure: 0.7369812252964427
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7870937439178116
Mean absolute error: 0.3096288623555634
Coverage of cases: 97.4
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 81.0
Weighted Recall: 0.748
Weighted FalsePositiveRate: 0.41657142857142854
Kappa statistic: 0.3558282208588957
Training time: 1.0
		
Time end:Sat Oct 07 11.39.08 EEST 2017