Sat Oct 07 11.41.04 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.41.04 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.4
Correctly Classified Instances: 65.6
Weighted Precision: 0.6455917135633309
Weighted AreaUnderROC: 0.5752380952380952
Root mean squared error: 0.580819265209849
Relative absolute error: 69.41176470588252
Root relative squared error: 116.16385304196979
Weighted TruePositiveRate: 0.656
Weighted MatthewsCorrelation: 0.15568776987655242
Weighted FMeasure: 0.6501534345739239
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6166901231547183
Mean absolute error: 0.3470588235294126
Coverage of cases: 65.6
Instances selection time: 20.0
Test time: 27.0
Accumulative iteration time: 20.0
Weighted Recall: 0.656
Weighted FalsePositiveRate: 0.5055238095238095
Kappa statistic: 0.15520628683693524
Training time: 0.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6478321678321678
Weighted AreaUnderROC: 0.5714285714285714
Root mean squared error: 0.5680582162595045
Relative absolute error: 66.16393442622966
Root relative squared error: 113.6116432519009
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.1580348853102535
Weighted FMeasure: 0.6556340956340955
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6152447552447553
Mean absolute error: 0.33081967213114827
Coverage of cases: 67.2
Instances selection time: 23.0
Test time: 30.0
Accumulative iteration time: 43.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.5291428571428572
Kappa statistic: 0.1546391752577321
Training time: 0.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6482959268495428
Weighted AreaUnderROC: 0.5680952380952381
Root mean squared error: 0.5634841453555279
Relative absolute error: 64.90140845070442
Root relative squared error: 112.69682907110558
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.15661617261998123
Weighted FMeasure: 0.6559575184894036
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6137510113604876
Mean absolute error: 0.3245070422535221
Coverage of cases: 67.8
Instances selection time: 24.0
Test time: 35.0
Accumulative iteration time: 67.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5418095238095239
Kappa statistic: 0.15084388185654016
Training time: 0.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6493398052642224
Weighted AreaUnderROC: 0.5623809523809524
Root mean squared error: 0.5569215032100057
Relative absolute error: 63.259259259259096
Root relative squared error: 111.38430064200114
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.15365778717124057
Weighted FMeasure: 0.6545685492219282
Iteration time: 28.0
Weighted AreaUnderPRC: 0.611158250267256
Mean absolute error: 0.31629629629629546
Coverage of cases: 68.6
Instances selection time: 28.0
Test time: 38.0
Accumulative iteration time: 95.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5612380952380953
Kappa statistic: 0.14301310043668125
Training time: 0.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.657158208222038
Weighted AreaUnderROC: 0.5661904761904762
Root mean squared error: 0.550152299120882
Relative absolute error: 61.62637362637352
Root relative squared error: 110.0304598241764
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.1680697703346299
Weighted FMeasure: 0.6592462572162922
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6137319640170703
Mean absolute error: 0.3081318681318676
Coverage of cases: 69.4
Instances selection time: 27.0
Test time: 42.0
Accumulative iteration time: 122.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5616190476190476
Kappa statistic: 0.1537610619469026
Training time: 0.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6610599595778127
Weighted AreaUnderROC: 0.5671428571428571
Root mean squared error: 0.5468403865766814
Relative absolute error: 60.79207920792092
Root relative squared error: 109.3680773153363
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.17427460124838554
Weighted FMeasure: 0.6608249505110492
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6145780436944597
Mean absolute error: 0.3039603960396046
Coverage of cases: 69.8
Instances selection time: 27.0
Test time: 48.0
Accumulative iteration time: 149.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5637142857142856
Kappa statistic: 0.15736607142857142
Training time: 0.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6560049019607843
Weighted AreaUnderROC: 0.5600000000000002
Root mean squared error: 0.548891202172245
Relative absolute error: 61.15315315315301
Root relative squared error: 109.77824043444899
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.160422236979937
Weighted FMeasure: 0.6547643069992257
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6106044117647058
Mean absolute error: 0.3057657657657651
Coverage of cases: 69.6
Instances selection time: 27.0
Test time: 50.0
Accumulative iteration time: 176.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.576
Kappa statistic: 0.14221218961625282
Training time: 0.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6633649340743163
Weighted AreaUnderROC: 0.5623809523809524
Root mean squared error: 0.5436490277918546
Relative absolute error: 59.93388429752087
Root relative squared error: 108.72980555837093
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.1722862646268097
Weighted FMeasure: 0.6576122554897365
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6124333369656024
Mean absolute error: 0.29966942148760434
Coverage of cases: 70.2
Instances selection time: 27.0
Test time: 54.0
Accumulative iteration time: 203.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.5772380952380952
Kappa statistic: 0.14954337899543377
Training time: 0.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6656818181818182
Weighted AreaUnderROC: 0.5619047619047619
Root mean squared error: 0.5419917212752335
Relative absolute error: 59.51145038167925
Root relative squared error: 108.39834425504671
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.17459497553883238
Weighted FMeasure: 0.6574321880650994
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6124015151515151
Mean absolute error: 0.29755725190839627
Coverage of cases: 70.4
Instances selection time: 27.0
Test time: 57.0
Accumulative iteration time: 230.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5801904761904763
Kappa statistic: 0.14942528735632166
Training time: 0.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6718832891246684
Weighted AreaUnderROC: 0.5647619047619048
Root mean squared error: 0.5384623040049916
Relative absolute error: 58.69503546099273
Root relative squared error: 107.69246080099832
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.18535492480522767
Weighted FMeasure: 0.6603826728826728
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6144148541114058
Mean absolute error: 0.29347517730496364
Coverage of cases: 70.8
Instances selection time: 26.0
Test time: 62.0
Accumulative iteration time: 257.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5784761904761905
Kappa statistic: 0.15704387990762125
Training time: 1.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6783258594917788
Weighted AreaUnderROC: 0.5657142857142857
Root mean squared error: 0.5348866044903098
Relative absolute error: 57.88079470198683
Root relative squared error: 106.97732089806196
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.19404619436181228
Weighted FMeasure: 0.6616021282885014
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6155073243647236
Mean absolute error: 0.28940397350993413
Coverage of cases: 71.2
Instances selection time: 25.0
Test time: 66.0
Accumulative iteration time: 282.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5805714285714285
Kappa statistic: 0.1608391608391606
Training time: 0.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6860619469026549
Weighted AreaUnderROC: 0.5647619047619048
Root mean squared error: 0.5312679827867992
Relative absolute error: 57.06832298136661
Root relative squared error: 106.25359655735984
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.20148347145152037
Weighted FMeasure: 0.6609083352225497
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6157250737463128
Mean absolute error: 0.2853416149068331
Coverage of cases: 71.6
Instances selection time: 25.0
Test time: 68.0
Accumulative iteration time: 307.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.5864761904761905
Kappa statistic: 0.16075650118203308
Training time: 0.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6785685805422648
Weighted AreaUnderROC: 0.5561904761904762
Root mean squared error: 0.5350928322383781
Relative absolute error: 57.84795321637433
Root relative squared error: 107.01856644767562
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.1817873255506067
Weighted FMeasure: 0.6522575528894119
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6105397926634769
Mean absolute error: 0.28923976608187163
Coverage of cases: 71.2
Instances selection time: 23.0
Test time: 74.0
Accumulative iteration time: 330.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5996190476190477
Kappa statistic: 0.1408114558472554
Training time: 0.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6829525215001782
Weighted AreaUnderROC: 0.5576190476190476
Root mean squared error: 0.533317466925148
Relative absolute error: 57.43646408839776
Root relative squared error: 106.66349338502961
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.18835770711670358
Weighted FMeasure: 0.6536805542179505
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6116505114243549
Mean absolute error: 0.2871823204419888
Coverage of cases: 71.4
Instances selection time: 22.0
Test time: 77.0
Accumulative iteration time: 352.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5987619047619048
Kappa statistic: 0.14473684210526314
Training time: 0.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6788521522145976
Weighted AreaUnderROC: 0.5542857142857143
Root mean squared error: 0.5352560130247775
Relative absolute error: 57.82198952879579
Root relative squared error: 107.0512026049555
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.17936504869495293
Weighted FMeasure: 0.6502475247524754
Iteration time: 19.0
Weighted AreaUnderPRC: 0.609556082345602
Mean absolute error: 0.28910994764397896
Coverage of cases: 71.2
Instances selection time: 19.0
Test time: 82.0
Accumulative iteration time: 371.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.6034285714285714
Kappa statistic: 0.13669064748201432
Training time: 0.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6680691721132898
Weighted AreaUnderROC: 0.5695238095238095
Root mean squared error: 0.5427094537132404
Relative absolute error: 59.402985074626656
Root relative squared error: 108.54189074264808
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.18588608411960952
Weighted FMeasure: 0.6638494568150355
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6164562636165577
Mean absolute error: 0.2970149253731333
Coverage of cases: 70.4
Instances selection time: 17.0
Test time: 85.0
Accumulative iteration time: 388.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.5649523809523811
Kappa statistic: 0.16478555304740405
Training time: 0.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6787774725274724
Weighted AreaUnderROC: 0.5895238095238096
Root mean squared error: 0.5390934325992794
Relative absolute error: 58.597156398104225
Root relative squared error: 107.81868651985589
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.21946329408320595
Weighted FMeasure: 0.6794001472852648
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6277144688644688
Mean absolute error: 0.2929857819905211
Coverage of cases: 70.8
Instances selection time: 15.0
Test time: 87.0
Accumulative iteration time: 403.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.528952380952381
Kappa statistic: 0.20479302832243992
Training time: 0.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6920763022743948
Weighted AreaUnderROC: 0.6085714285714285
Root mean squared error: 0.5317142566990389
Relative absolute error: 56.99547511312236
Root relative squared error: 106.3428513398078
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2546820025542104
Weighted FMeasure: 0.6939283545840923
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6392212137092548
Mean absolute error: 0.2849773755656118
Coverage of cases: 71.6
Instances selection time: 11.0
Test time: 90.0
Accumulative iteration time: 414.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4988571428571429
Kappa statistic: 0.24307036247334743
Training time: 0.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6938604565537555
Weighted AreaUnderROC: 0.6209523809523809
Root mean squared error: 0.5354978354978361
Relative absolute error: 57.78354978354982
Root relative squared error: 107.09956709956722
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.26588828029853484
Weighted FMeasure: 0.6985291379631353
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6456462812960235
Mean absolute error: 0.28891774891774913
Coverage of cases: 71.2
Instances selection time: 8.0
Test time: 93.0
Accumulative iteration time: 422.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4700952380952381
Kappa statistic: 0.26078028747433246
Training time: 0.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6956709956709957
Weighted AreaUnderROC: 0.6295238095238094
Root mean squared error: 0.539251969005529
Relative absolute error: 58.572614107883844
Root relative squared error: 107.8503938011058
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.27342497621534223
Weighted FMeasure: 0.7001441268316119
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6500998556998557
Mean absolute error: 0.29286307053941923
Coverage of cases: 70.8
Instances selection time: 5.0
Test time: 96.0
Accumulative iteration time: 427.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4489523809523809
Kappa statistic: 0.2714570858283433
Training time: 0.0
		
Time end:Sat Oct 07 11.41.06 EEST 2017