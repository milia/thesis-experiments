Sat Oct 07 11.43.04 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.04 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46904157598234286
Relative absolute error: 76.00000000000045
Root relative squared error: 93.80831519646857
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3800000000000023
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 91.8
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6781975254411011
Weighted AreaUnderROC: 0.5571142857142857
Root mean squared error: 0.5046807745443043
Relative absolute error: 62.974294272705535
Root relative squared error: 100.93615490886086
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.15497405346635953
Weighted FMeasure: 0.6333535203696269
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6143689075602192
Mean absolute error: 0.31487147136352767
Coverage of cases: 95.2
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.630952380952381
Kappa statistic: 0.10272277227722773
Training time: 7.0
		
Time end:Sat Oct 07 11.43.04 EEST 2017