Sat Oct 07 11.40.18 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.40.18 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 95.1
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6545918973506
Weighted AreaUnderROC: 0.6297761904761905
Root mean squared error: 0.4551181442834412
Relative absolute error: 74.71672287399157
Root relative squared error: 91.02362885668825
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.14968475113472532
Weighted FMeasure: 0.6478681214247858
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6810832184309761
Mean absolute error: 0.37358361436995785
Coverage of cases: 98.4
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5941904761904763
Kappa statistic: 0.12615740740740733
Training time: 4.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 93.8
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7043988269794722
Weighted AreaUnderROC: 0.7359961904761906
Root mean squared error: 0.42455341267297186
Relative absolute error: 69.31568902776921
Root relative squared error: 84.91068253459437
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2604503165164862
Weighted FMeasure: 0.6896825396825397
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7608285190475549
Mean absolute error: 0.34657844513884606
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5356190476190476
Kappa statistic: 0.22902494331065754
Training time: 5.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 93.0
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.6989063069631791
Weighted AreaUnderROC: 0.6656066666666666
Root mean squared error: 0.44593807423155346
Relative absolute error: 72.2558550221804
Root relative squared error: 89.1876148463107
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.259800855576143
Weighted FMeasure: 0.6932915189528225
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7087164463259312
Mean absolute error: 0.36127927511090197
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5182857142857142
Kappa statistic: 0.23841059602649012
Training time: 5.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 94.4
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.6829926579573266
Weighted AreaUnderROC: 0.711652380952381
Root mean squared error: 0.43424254404970875
Relative absolute error: 70.0816363947097
Root relative squared error: 86.84850880994175
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.21651884799064872
Weighted FMeasure: 0.6744625127272513
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7310910407971166
Mean absolute error: 0.3504081819735485
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 26.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.5530476190476189
Kappa statistic: 0.19117647058823525
Training time: 7.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 93.5
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7075640692265465
Weighted AreaUnderROC: 0.7137685714285715
Root mean squared error: 0.4322418956692374
Relative absolute error: 69.55880821759963
Root relative squared error: 86.44837913384748
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.28268340998678604
Weighted FMeasure: 0.7029729563373267
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7430079050705092
Mean absolute error: 0.34779404108799816
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5004761904761905
Kappa statistic: 0.26310043668122257
Training time: 7.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 94.3
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7014117647058823
Weighted AreaUnderROC: 0.7200428571428572
Root mean squared error: 0.43215572078256875
Relative absolute error: 70.73529943776362
Root relative squared error: 86.43114415651375
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.26278690248142056
Weighted FMeasure: 0.6935913978494626
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7447606105515823
Mean absolute error: 0.35367649718881805
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 43.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5212380952380953
Kappa statistic: 0.2388888888888888
Training time: 8.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 94.4
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7043988269794722
Weighted AreaUnderROC: 0.7044257142857142
Root mean squared error: 0.43378884429784853
Relative absolute error: 70.51500577755672
Root relative squared error: 86.75776885956971
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2604503165164862
Weighted FMeasure: 0.6896825396825397
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7338571385405028
Mean absolute error: 0.3525750288877836
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 53.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5356190476190476
Kappa statistic: 0.22902494331065754
Training time: 9.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 94.7
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.692635885447107
Weighted AreaUnderROC: 0.717077142857143
Root mean squared error: 0.43160202299137745
Relative absolute error: 71.38586861151403
Root relative squared error: 86.32040459827549
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2521156558797846
Weighted FMeasure: 0.6921611891766345
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7410958537268665
Mean absolute error: 0.3569293430575701
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 65.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5094285714285715
Kappa statistic: 0.237012987012987
Training time: 11.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 94.4
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6925623052959502
Weighted AreaUnderROC: 0.7128419047619048
Root mean squared error: 0.43406257585478586
Relative absolute error: 71.03890985571716
Root relative squared error: 86.81251517095717
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.24115926404437824
Weighted FMeasure: 0.6848468005280345
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7396714101777189
Mean absolute error: 0.35519454927858585
Coverage of cases: 99.2
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 77.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.5352380952380953
Kappa statistic: 0.21700223713646533
Training time: 10.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 97.3
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6822916666666666
Weighted AreaUnderROC: 0.6874371428571429
Root mean squared error: 0.4442636349617265
Relative absolute error: 76.80428555509307
Root relative squared error: 88.8527269923453
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.23987430570964147
Weighted FMeasure: 0.6877752965520066
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7118030889382094
Mean absolute error: 0.3840214277754654
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 89.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.47904761904761906
Kappa statistic: 0.2362525458248472
Training time: 11.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 94.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7005516328331863
Weighted AreaUnderROC: 0.7465428571428572
Root mean squared error: 0.4255568668426242
Relative absolute error: 69.34753961465213
Root relative squared error: 85.11137336852485
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2704658339356833
Weighted FMeasure: 0.69927876662476
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7516767028093533
Mean absolute error: 0.34673769807326066
Coverage of cases: 98.8
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 104.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.4992380952380952
Kappa statistic: 0.25485961123110135
Training time: 13.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 94.7
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6834636480336095
Weighted AreaUnderROC: 0.67688
Root mean squared error: 0.44417260026855837
Relative absolute error: 73.342130627575
Root relative squared error: 88.83452005371167
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.22866358888697674
Weighted FMeasure: 0.6825431034482761
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7162979422290937
Mean absolute error: 0.36671065313787504
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 118.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.5272380952380953
Kappa statistic: 0.212253829321663
Training time: 13.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 94.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6682692307692308
Weighted AreaUnderROC: 0.6660380952380951
Root mean squared error: 0.45024543024811
Relative absolute error: 72.93129075104375
Root relative squared error: 90.04908604962199
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.19611613513818404
Weighted FMeasure: 0.670616589676642
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7013781633027483
Mean absolute error: 0.3646564537552187
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 133.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.54
Kappa statistic: 0.18300653594771224
Training time: 14.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 94.9
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7072221052948073
Weighted AreaUnderROC: 0.714147619047619
Root mean squared error: 0.43006867590085723
Relative absolute error: 70.73570484920384
Root relative squared error: 86.01373518017145
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2787993006938226
Weighted FMeasure: 0.7005760114182793
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7430214057302258
Mean absolute error: 0.3536785242460192
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 148.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.508095238095238
Kappa statistic: 0.2566079295154184
Training time: 14.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 92.7
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7012753783047809
Weighted AreaUnderROC: 0.7199161904761904
Root mean squared error: 0.4308319449651704
Relative absolute error: 68.77670123248207
Root relative squared error: 86.16638899303408
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.2549541464505272
Weighted FMeasure: 0.6881214282771569
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7512095209084089
Mean absolute error: 0.34388350616241037
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 164.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.5364761904761903
Kappa statistic: 0.2251131221719457
Training time: 15.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 94.6
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.701907556859868
Weighted AreaUnderROC: 0.6935171428571428
Root mean squared error: 0.4392151900044307
Relative absolute error: 72.89450303714953
Root relative squared error: 87.84303800088614
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.27702252909405345
Weighted FMeasure: 0.7025500910746811
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7175217832734293
Mean absolute error: 0.3644725151857477
Coverage of cases: 98.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 182.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.4878095238095238
Kappa statistic: 0.2643923240938165
Training time: 17.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 93.8
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.71051567239636
Weighted AreaUnderROC: 0.709432380952381
Root mean squared error: 0.4341225625620428
Relative absolute error: 70.37025996761308
Root relative squared error: 86.82451251240856
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2914348590123061
Weighted FMeasure: 0.7068861478392049
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7380075527504771
Mean absolute error: 0.3518512998380654
Coverage of cases: 99.4
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 199.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.492
Kappa statistic: 0.27331887201735355
Training time: 17.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 95.4
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.70825
Weighted AreaUnderROC: 0.7159733333333334
Root mean squared error: 0.43271107976427825
Relative absolute error: 71.67514410365023
Root relative squared error: 86.54221595285566
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2945941518185898
Weighted FMeasure: 0.7098666666666665
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7397044335690892
Mean absolute error: 0.3583757205182511
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 218.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.47085714285714286
Kappa statistic: 0.2842105263157894
Training time: 18.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.7
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7287433837556088
Weighted AreaUnderROC: 0.7105019047619047
Root mean squared error: 0.4334917145259593
Relative absolute error: 69.9817891412966
Root relative squared error: 86.69834290519186
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.3359421147672456
Weighted FMeasure: 0.7247809139564507
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7371025377336682
Mean absolute error: 0.349908945706483
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 239.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.4631428571428572
Kappa statistic: 0.3186695278969957
Training time: 20.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 91.2
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.679408734247444
Weighted AreaUnderROC: 0.6947809523809524
Root mean squared error: 0.4452563668004542
Relative absolute error: 71.18120046846417
Root relative squared error: 89.05127336009085
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2254486978078258
Weighted FMeasure: 0.682587210724595
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7182348727991541
Mean absolute error: 0.35590600234232084
Coverage of cases: 98.2
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 259.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5145714285714287
Kappa statistic: 0.21474358974358954
Training time: 20.0
		
Time end:Sat Oct 07 11.40.19 EEST 2017