Sat Oct 07 11.39.01 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.01 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 77.4
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6990146990810561
Weighted AreaUnderROC: 0.6258190476190476
Root mean squared error: 0.5130627605961953
Relative absolute error: 70.21001317523076
Root relative squared error: 102.61255211923906
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2827968705988804
Weighted FMeasure: 0.7020216878185613
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6522804622034738
Mean absolute error: 0.3510500658761538
Coverage of cases: 83.6
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.43076190476190473
Kappa statistic: 0.28222656249999983
Training time: 5.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 97.4
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6607040229885057
Weighted AreaUnderROC: 0.5046857142857143
Root mean squared error: 0.49876754699008696
Relative absolute error: 67.0646526220624
Root relative squared error: 99.7535093980174
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.13845032015582256
Weighted FMeasure: 0.6340175953079179
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5946054108928422
Mean absolute error: 0.335323263110312
Coverage of cases: 97.6
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6259047619047619
Kappa statistic: 0.09975669099756673
Training time: 11.0
		
Time end:Sat Oct 07 11.39.01 EEST 2017