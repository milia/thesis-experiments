Sat Oct 07 11.39.01 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.39.01 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 80.4
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6342548076923076
Weighted AreaUnderROC: 0.5494
Root mean squared error: 0.5111984042676158
Relative absolute error: 77.46191803088365
Root relative squared error: 102.23968085352315
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.10579895876407543
Weighted FMeasure: 0.6314747264200103
Iteration time: 9.0
Weighted AreaUnderPRC: 0.609239707263218
Mean absolute error: 0.38730959015441824
Coverage of cases: 84.4
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.6175238095238095
Kappa statistic: 0.08665105386416849
Training time: 4.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 99.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.666901822457378
Weighted AreaUnderROC: 0.6224571428571428
Root mean squared error: 0.4962754800898615
Relative absolute error: 65.69816227524794
Root relative squared error: 99.2550960179723
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.10052910052910054
Weighted FMeasure: 0.6053448477068503
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6473594979482583
Mean absolute error: 0.3284908113762397
Coverage of cases: 99.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6678095238095237
Kappa statistic: 0.04884318766066835
Training time: 11.0
		
Time end:Sat Oct 07 11.39.01 EEST 2017