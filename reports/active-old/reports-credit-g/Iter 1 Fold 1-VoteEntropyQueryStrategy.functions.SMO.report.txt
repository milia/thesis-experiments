Sat Oct 07 11.42.59 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.42.59 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7073720498541501
Weighted AreaUnderROC: 0.6128571428571429
Root mean squared error: 0.5196152422706632
Relative absolute error: 54.0
Root relative squared error: 103.92304845413265
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2807302275691662
Weighted FMeasure: 0.7017884586155066
Iteration time: 39.0
Weighted AreaUnderPRC: 0.6439499867409174
Mean absolute error: 0.27
Coverage of cases: 73.0
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 39.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5042857142857143
Kappa statistic: 0.2598684210526316
Training time: 35.0
		
Iteration: 2
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.73281999495077
Weighted AreaUnderROC: 0.5704761904761905
Root mean squared error: 0.5176871642217914
Relative absolute error: 53.6
Root relative squared error: 103.53743284435828
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.25657813920967454
Weighted FMeasure: 0.666570758738278
Iteration time: 135.0
Weighted AreaUnderPRC: 0.6234247917192629
Mean absolute error: 0.268
Coverage of cases: 73.2
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 174.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.591047619047619
Kappa statistic: 0.18092909535452328
Training time: 132.0
		
Time end:Sat Oct 07 11.43.00 EEST 2017