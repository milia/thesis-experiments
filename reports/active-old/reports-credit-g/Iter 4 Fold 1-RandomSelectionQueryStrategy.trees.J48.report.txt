Sat Oct 07 11.42.43 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.42.43 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 100.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.48999999999999994
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.46357307945997034
Relative absolute error: 78.40000000000022
Root relative squared error: 92.71461589199407
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.5764705882352942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.58
Mean absolute error: 0.3920000000000011
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.7
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 77.1
Incorrectly Classified Instances: 33.6
Correctly Classified Instances: 66.4
Weighted Precision: 0.6295482673267326
Weighted AreaUnderROC: 0.5840571428571428
Root mean squared error: 0.4955531205606254
Relative absolute error: 69.93740939911135
Root relative squared error: 99.11062411212508
Weighted TruePositiveRate: 0.664
Weighted MatthewsCorrelation: 0.11302232196194265
Weighted FMeasure: 0.639153781458239
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6308381630916658
Mean absolute error: 0.3496870469955567
Coverage of cases: 91.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.664
Weighted FalsePositiveRate: 0.5668571428571428
Kappa statistic: 0.1082802547770702
Training time: 2.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 77.5
Incorrectly Classified Instances: 35.6
Correctly Classified Instances: 64.4
Weighted Precision: 0.61911644395964
Weighted AreaUnderROC: 0.624752380952381
Root mean squared error: 0.49551288345464645
Relative absolute error: 70.87891293891296
Root relative squared error: 99.10257669092928
Weighted TruePositiveRate: 0.644
Weighted MatthewsCorrelation: 0.09154338067522005
Weighted FMeasure: 0.6284337944664032
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6578653505430068
Mean absolute error: 0.3543945646945648
Coverage of cases: 92.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.644
Weighted FalsePositiveRate: 0.5601904761904762
Kappa statistic: 0.08997955010224952
Training time: 2.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 91.3
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6541775230154594
Weighted AreaUnderROC: 0.5932761904761905
Root mean squared error: 0.49677559371139723
Relative absolute error: 72.8509614656672
Root relative squared error: 99.35511874227944
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.17066437330296497
Weighted FMeasure: 0.6613620285214282
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6356740264756549
Mean absolute error: 0.364254807328336
Coverage of cases: 91.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.5324761904761904
Kappa statistic: 0.1649159663865548
Training time: 3.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 94.7
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.6567931237369217
Weighted AreaUnderROC: 0.6394
Root mean squared error: 0.4857272953934448
Relative absolute error: 70.64375478927207
Root relative squared error: 97.14545907868896
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.17548867170193225
Weighted FMeasure: 0.6633622056981252
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6637395900196277
Mean absolute error: 0.35321877394636036
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.5345714285714286
Kappa statistic: 0.16843220338983075
Training time: 2.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 89.2
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6454171056412222
Weighted AreaUnderROC: 0.5704095238095239
Root mean squared error: 0.5151771307127788
Relative absolute error: 72.21337950890931
Root relative squared error: 103.03542614255576
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.15162738792339686
Weighted FMeasure: 0.6535114247311827
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6202167293614342
Mean absolute error: 0.3610668975445466
Coverage of cases: 91.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.5367619047619048
Kappa statistic: 0.14760914760914778
Training time: 3.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 79.0
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6474011570739293
Weighted AreaUnderROC: 0.6392380952380953
Root mean squared error: 0.49730225041271625
Relative absolute error: 72.7956309900145
Root relative squared error: 99.46045008254325
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.15756965919069257
Weighted FMeasure: 0.6550706543328983
Iteration time: 5.0
Weighted AreaUnderPRC: 0.668664363710731
Mean absolute error: 0.3639781549500725
Coverage of cases: 91.4
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.5261904761904761
Kappa statistic: 0.1547131147540984
Training time: 4.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 92.6
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6769020998091082
Weighted AreaUnderROC: 0.6580380952380952
Root mean squared error: 0.48235079503799366
Relative absolute error: 69.56913962422904
Root relative squared error: 96.47015900759874
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.22677792030906782
Weighted FMeasure: 0.6827075098814229
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6856696756368775
Mean absolute error: 0.3478456981211452
Coverage of cases: 95.2
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.4883809523809524
Kappa statistic: 0.22290388548057247
Training time: 5.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 93.9
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6718246869409661
Weighted AreaUnderROC: 0.6452857142857142
Root mean squared error: 0.4979202513911029
Relative absolute error: 72.95680247952554
Root relative squared error: 99.58405027822059
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.2185429580316203
Weighted FMeasure: 0.6698197434593434
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6694568480229282
Mean absolute error: 0.36478401239762764
Coverage of cases: 93.8
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.447047619047619
Kappa statistic: 0.2184557438794727
Training time: 5.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 90.0
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.7006788389513108
Weighted AreaUnderROC: 0.6500952380952381
Root mean squared error: 0.4936972433523456
Relative absolute error: 66.99637972090794
Root relative squared error: 98.73944867046912
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.28721053877843555
Weighted FMeasure: 0.7022373822050068
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6754988524939061
Mean absolute error: 0.3349818986045397
Coverage of cases: 92.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.42019047619047617
Kappa statistic: 0.2870905587668593
Training time: 5.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 89.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7046945701357467
Weighted AreaUnderROC: 0.6470761904761905
Root mean squared error: 0.47993895001166276
Relative absolute error: 66.59382324087959
Root relative squared error: 95.98779000233255
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.29619460890551624
Weighted FMeasure: 0.7077745783628135
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6718396353909684
Mean absolute error: 0.33296911620439795
Coverage of cases: 92.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.42438095238095236
Kappa statistic: 0.29549902152641877
Training time: 5.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 88.2
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6980470162748643
Weighted AreaUnderROC: 0.618247619047619
Root mean squared error: 0.4950445666217594
Relative absolute error: 68.76261064425792
Root relative squared error: 99.00891332435188
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.27323563308203436
Weighted FMeasure: 0.7016344255823135
Iteration time: 6.0
Weighted AreaUnderPRC: 0.652786170149873
Mean absolute error: 0.3438130532212896
Coverage of cases: 89.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 52.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.47514285714285714
Kappa statistic: 0.26562499999999994
Training time: 6.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 89.5
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6943922924901186
Weighted AreaUnderROC: 0.6290095238095238
Root mean squared error: 0.5058388623669863
Relative absolute error: 68.64793748103041
Root relative squared error: 101.16777247339725
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.27128766059783654
Weighted FMeasure: 0.6982634978960469
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6605973977518764
Mean absolute error: 0.34323968740515204
Coverage of cases: 91.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 59.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.443047619047619
Kappa statistic: 0.27021696252465466
Training time: 7.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 86.4
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6844803370786517
Weighted AreaUnderROC: 0.613247619047619
Root mean squared error: 0.5087790117319303
Relative absolute error: 69.78500918330258
Root relative squared error: 101.75580234638606
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.2486587886068334
Weighted FMeasure: 0.6861421055674395
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6475566110128959
Mean absolute error: 0.34892504591651285
Coverage of cases: 88.6
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.4422857142857143
Kappa statistic: 0.2485549132947976
Training time: 7.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 87.1
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6933701657458563
Weighted AreaUnderROC: 0.6221619047619048
Root mean squared error: 0.4975821732640061
Relative absolute error: 67.2136686341329
Root relative squared error: 99.51643465280122
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.26946701171483983
Weighted FMeasure: 0.6962780898876404
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6614886596088366
Mean absolute error: 0.3360683431706645
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.43714285714285717
Kappa statistic: 0.26900584795321625
Training time: 7.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 89.8
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6847922716766431
Weighted AreaUnderROC: 0.6284571428571428
Root mean squared error: 0.5010906220600472
Relative absolute error: 69.42690082840038
Root relative squared error: 100.21812441200943
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.24926809539438854
Weighted FMeasure: 0.6871737455105928
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6636627088811307
Mean absolute error: 0.34713450414200187
Coverage of cases: 92.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 84.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.4452380952380952
Kappa statistic: 0.24903100775193776
Training time: 8.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 92.8
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7060162601626017
Weighted AreaUnderROC: 0.6567809523809524
Root mean squared error: 0.4713618706077639
Relative absolute error: 67.28898333635189
Root relative squared error: 94.27237412155279
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2839992402462488
Weighted FMeasure: 0.7047368421052632
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6838507063179556
Mean absolute error: 0.33644491668175946
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 94.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.48990476190476184
Kappa statistic: 0.2688172043010752
Training time: 9.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.694
Weighted AreaUnderROC: 0.6847142857142858
Root mean squared error: 0.46805993269304763
Relative absolute error: 68.144390293225
Root relative squared error: 93.61198653860953
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.26186146828319085
Weighted FMeasure: 0.6970666666666667
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7018966095341717
Mean absolute error: 0.340721951466125
Coverage of cases: 95.4
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 103.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.48742857142857143
Kappa statistic: 0.2526315789473684
Training time: 9.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 92.9
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6857716609961373
Weighted AreaUnderROC: 0.6727333333333333
Root mean squared error: 0.4778248709180545
Relative absolute error: 69.20694522666274
Root relative squared error: 95.5649741836109
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.24171081196681235
Weighted FMeasure: 0.6890924829696059
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6905702401513998
Mean absolute error: 0.3460347261333137
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 112.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.5014285714285714
Kappa statistic: 0.2319915254237288
Training time: 8.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 93.5
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6737556561085972
Weighted AreaUnderROC: 0.6508380952380952
Root mean squared error: 0.5021739695256802
Relative absolute error: 72.12044401974697
Root relative squared error: 100.43479390513603
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.2231003264365801
Weighted FMeasure: 0.6757476430236881
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6733496368484871
Mean absolute error: 0.3606022200987349
Coverage of cases: 94.2
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 122.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.4580000000000001
Kappa statistic: 0.22297297297297308
Training time: 9.0
		
Time end:Sat Oct 07 11.42.44 EEST 2017