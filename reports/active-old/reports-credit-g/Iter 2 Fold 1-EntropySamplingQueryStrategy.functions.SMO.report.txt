Sat Oct 07 11.37.42 EEST 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.37.42 EEST 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6852869002245079
Weighted AreaUnderROC: 0.63
Root mean squared error: 0.5779273310719956
Relative absolute error: 66.8
Root relative squared error: 115.58546621439912
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.2485272234939796
Weighted FMeasure: 0.6735027790486459
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6475708139717017
Mean absolute error: 0.334
Coverage of cases: 66.6
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 36.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.406
Kappa statistic: 0.24638989169675105
Training time: 26.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.2
Correctly Classified Instances: 65.8
Weighted Precision: 0.6612716222533895
Weighted AreaUnderROC: 0.5976190476190476
Root mean squared error: 0.5848076606885378
Relative absolute error: 68.4
Root relative squared error: 116.96153213770756
Weighted TruePositiveRate: 0.658
Weighted MatthewsCorrelation: 0.19345003697545785
Weighted FMeasure: 0.6595730628611863
Iteration time: 31.0
Weighted AreaUnderPRC: 0.6288588125292194
Mean absolute error: 0.342
Coverage of cases: 65.8
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 67.0
Weighted Recall: 0.658
Weighted FalsePositiveRate: 0.46276190476190476
Kappa statistic: 0.1933962264150945
Training time: 28.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6744079449961804
Weighted AreaUnderROC: 0.6057142857142858
Root mean squared error: 0.5585696017507576
Relative absolute error: 62.4
Root relative squared error: 111.71392035015153
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.22316303205811022
Weighted FMeasure: 0.6796060533269276
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6348583651642474
Mean absolute error: 0.312
Coverage of cases: 68.8
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 89.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.47657142857142853
Kappa statistic: 0.22155688622754485
Training time: 20.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6728313372593132
Weighted AreaUnderROC: 0.6014285714285714
Root mean squared error: 0.5567764362830022
Relative absolute error: 62.0
Root relative squared error: 111.35528725660045
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.21829007372188577
Weighted FMeasure: 0.6787107338829023
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6326066168199563
Mean absolute error: 0.31
Coverage of cases: 69.0
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 129.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.4871428571428571
Kappa statistic: 0.21558704453441283
Training time: 38.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6733270418668497
Weighted AreaUnderROC: 0.6038095238095237
Root mean squared error: 0.5585696017507576
Relative absolute error: 62.4
Root relative squared error: 111.71392035015153
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.22031399271157245
Weighted FMeasure: 0.6787838571514748
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6337930336307481
Mean absolute error: 0.312
Coverage of cases: 68.8
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 188.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.4803809523809524
Kappa statistic: 0.21843687374749485
Training time: 55.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6902372511589856
Weighted AreaUnderROC: 0.618095238095238
Root mean squared error: 0.5403702434442518
Relative absolute error: 58.4
Root relative squared error: 108.07404868885035
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.2579858909938019
Weighted FMeasure: 0.6952322134387351
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6435769111898919
Mean absolute error: 0.292
Coverage of cases: 70.8
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 224.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.47180952380952385
Kappa statistic: 0.25357873210633936
Training time: 33.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6636268890046899
Weighted AreaUnderROC: 0.5842857142857142
Root mean squared error: 0.5567764362830022
Relative absolute error: 62.0
Root relative squared error: 111.35528725660045
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.19240505780015799
Weighted FMeasure: 0.6698812227724615
Iteration time: 47.0
Weighted AreaUnderPRC: 0.6231741383160873
Mean absolute error: 0.31
Coverage of cases: 69.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 271.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.5214285714285715
Kappa statistic: 0.18592436974789908
Training time: 44.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6635519801980198
Weighted AreaUnderROC: 0.5819047619047619
Root mean squared error: 0.5549774770204643
Relative absolute error: 61.6
Root relative squared error: 110.99549540409286
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.19058666056327586
Weighted FMeasure: 0.6692242996700524
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6220278877887789
Mean absolute error: 0.308
Coverage of cases: 69.2
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 309.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5281904761904762
Kappa statistic: 0.18259023354564746
Training time: 35.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.648365352207617
Weighted AreaUnderROC: 0.562857142857143
Root mean squared error: 0.5621387729022078
Relative absolute error: 63.2
Root relative squared error: 112.42775458044156
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.15265635472073177
Weighted FMeasure: 0.6543881444671222
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6112938321536906
Mean absolute error: 0.316
Coverage of cases: 68.4
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 345.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.5582857142857144
Kappa statistic: 0.14316702819956634
Training time: 32.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6472527472527472
Weighted AreaUnderROC: 0.560952380952381
Root mean squared error: 0.5621387729022078
Relative absolute error: 63.2
Root relative squared error: 112.42775458044156
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.14942181724814024
Weighted FMeasure: 0.6530494744593962
Iteration time: 57.0
Weighted AreaUnderPRC: 0.6102886446886446
Mean absolute error: 0.316
Coverage of cases: 68.4
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 402.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.5620952380952382
Kappa statistic: 0.13943355119825715
Training time: 55.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6548669340138533
Weighted AreaUnderROC: 0.5647619047619048
Root mean squared error: 0.5549774770204643
Relative absolute error: 61.6
Root relative squared error: 110.99549540409286
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.16357831647386784
Weighted FMeasure: 0.6577311153531497
Iteration time: 46.0
Weighted AreaUnderPRC: 0.6128191517802892
Mean absolute error: 0.308
Coverage of cases: 69.2
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 448.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5624761904761904
Kappa statistic: 0.15011037527593818
Training time: 45.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6509634551495017
Weighted AreaUnderROC: 0.5571428571428572
Root mean squared error: 0.5549774770204643
Relative absolute error: 61.6
Root relative squared error: 110.99549540409286
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.15093444108439266
Weighted FMeasure: 0.6517948717948718
Iteration time: 44.0
Weighted AreaUnderPRC: 0.6087840531561463
Mean absolute error: 0.308
Coverage of cases: 69.2
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 492.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5777142857142856
Kappa statistic: 0.1348314606741571
Training time: 40.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6525
Weighted AreaUnderROC: 0.5523809523809524
Root mean squared error: 0.5513619500836089
Relative absolute error: 60.8
Root relative squared error: 110.27239001672177
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.1477342100713197
Weighted FMeasure: 0.6481735985533454
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6065833333333333
Mean absolute error: 0.304
Coverage of cases: 69.6
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 532.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5912380952380952
Kappa statistic: 0.1264367816091952
Training time: 37.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6525
Weighted AreaUnderROC: 0.5523809523809524
Root mean squared error: 0.5513619500836089
Relative absolute error: 60.8
Root relative squared error: 110.27239001672177
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.1477342100713197
Weighted FMeasure: 0.6481735985533454
Iteration time: 46.0
Weighted AreaUnderPRC: 0.6065833333333333
Mean absolute error: 0.304
Coverage of cases: 69.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 578.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5912380952380952
Kappa statistic: 0.1264367816091952
Training time: 43.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6525
Weighted AreaUnderROC: 0.5523809523809524
Root mean squared error: 0.5513619500836089
Relative absolute error: 60.8
Root relative squared error: 110.27239001672177
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.1477342100713197
Weighted FMeasure: 0.6481735985533454
Iteration time: 56.0
Weighted AreaUnderPRC: 0.6065833333333333
Mean absolute error: 0.304
Coverage of cases: 69.6
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 634.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5912380952380952
Kappa statistic: 0.1264367816091952
Training time: 53.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6887598491355166
Weighted AreaUnderROC: 0.5795238095238096
Root mean squared error: 0.5310367218940701
Relative absolute error: 56.39999999999999
Root relative squared error: 106.20734437881403
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.22269457946993887
Weighted FMeasure: 0.6744310093164902
Iteration time: 70.0
Weighted AreaUnderPRC: 0.6238145039023115
Mean absolute error: 0.282
Coverage of cases: 71.8
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 704.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.5589523809523811
Kappa statistic: 0.19151376146788981
Training time: 67.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6953839587642404
Weighted AreaUnderROC: 0.5938095238095238
Root mean squared error: 0.5272570530585627
Relative absolute error: 55.60000000000001
Root relative squared error: 105.45141061171255
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.24631947958981784
Weighted FMeasure: 0.6864084944731325
Iteration time: 134.0
Weighted AreaUnderPRC: 0.6321413900653338
Mean absolute error: 0.278
Coverage of cases: 72.2
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 838.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5343809523809524
Kappa statistic: 0.22085201793721967
Training time: 132.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6699029126213591
Weighted AreaUnderROC: 0.5838095238095238
Root mean squared error: 0.5477225575051661
Relative absolute error: 60.0
Root relative squared error: 109.54451150103321
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.20170333378254346
Weighted FMeasure: 0.673129094157348
Iteration time: 266.0
Weighted AreaUnderPRC: 0.6237436893203884
Mean absolute error: 0.3
Coverage of cases: 70.0
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 1104.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5323809523809524
Kappa statistic: 0.19006479481641447
Training time: 263.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6859061734930192
Weighted AreaUnderROC: 0.6157142857142858
Root mean squared error: 0.5458937625582472
Relative absolute error: 59.599999999999994
Root relative squared error: 109.17875251164944
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.24903515452778516
Weighted FMeasure: 0.6911477377325963
Iteration time: 144.0
Weighted AreaUnderPRC: 0.6416238867200423
Mean absolute error: 0.298
Coverage of cases: 70.2
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 1248.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.47057142857142853
Kappa statistic: 0.24595141700404846
Training time: 142.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7062709284627092
Weighted AreaUnderROC: 0.6452380952380953
Root mean squared error: 0.5347896782848375
Relative absolute error: 57.199999999999996
Root relative squared error: 106.95793565696749
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.29983123630480374
Weighted FMeasure: 0.7094736842105263
Iteration time: 155.0
Weighted AreaUnderPRC: 0.659878741755454
Mean absolute error: 0.286
Coverage of cases: 71.4
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 1403.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.42352380952380947
Kappa statistic: 0.29901960784313725
Training time: 154.0
		
Time end:Sat Oct 07 11.37.44 EEST 2017