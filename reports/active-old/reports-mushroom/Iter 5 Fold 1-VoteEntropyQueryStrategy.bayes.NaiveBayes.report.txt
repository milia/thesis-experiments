Sun Oct 08 04.36.30 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.36.30 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 52.96651895617922
Incorrectly Classified Instances: 6.154603643525357
Correctly Classified Instances: 93.84539635647464
Weighted Precision: 0.9430010342890247
Weighted AreaUnderROC: 0.9942174446649603
Root mean squared error: 0.22818424655933697
Relative absolute error: 12.519924588017828
Root relative squared error: 45.636849311867394
Weighted TruePositiveRate: 0.9384539635647464
Weighted MatthewsCorrelation: 0.8810584908917131
Weighted FMeasure: 0.9381729429006245
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9943995956185475
Mean absolute error: 0.06259962294008914
Coverage of cases: 96.7503692762186
Instances selection time: 24.0
Test time: 39.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9384539635647464
Weighted FalsePositiveRate: 0.06542646984686094
Kappa statistic: 0.876288343304172
Training time: 1.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 53.052683407188574
Incorrectly Classified Instances: 6.006893156080748
Correctly Classified Instances: 93.99310684391925
Weighted Precision: 0.9438892497678181
Weighted AreaUnderROC: 0.9940450991739068
Root mean squared error: 0.22165180958983505
Relative absolute error: 11.864522060198524
Root relative squared error: 44.33036191796701
Weighted TruePositiveRate: 0.9399310684391925
Weighted MatthewsCorrelation: 0.8834505524597509
Weighted FMeasure: 0.9396852911020952
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9942344950730233
Mean absolute error: 0.05932261030099262
Coverage of cases: 97.19350073855244
Instances selection time: 22.0
Test time: 39.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9399310684391925
Weighted FalsePositiveRate: 0.06369746310745414
Kappa statistic: 0.8792874813291228
Training time: 1.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 53.24963072378139
Incorrectly Classified Instances: 5.809945839487937
Correctly Classified Instances: 94.19005416051206
Weighted Precision: 0.945394234696132
Weighted AreaUnderROC: 0.9949602294573884
Root mean squared error: 0.21498848194063397
Relative absolute error: 11.376165276535467
Root relative squared error: 42.99769638812679
Weighted TruePositiveRate: 0.9419005416051206
Weighted MatthewsCorrelation: 0.8869510175873061
Weighted FMeasure: 0.941685313054908
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9951161124145439
Mean absolute error: 0.05688082638267733
Coverage of cases: 97.83357951747908
Instances selection time: 20.0
Test time: 39.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9419005416051206
Weighted FalsePositiveRate: 0.06151025431732405
Kappa statistic: 0.8832701768058058
Training time: 2.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 53.21270310192023
Incorrectly Classified Instances: 5.489906450024619
Correctly Classified Instances: 94.51009354997538
Weighted Precision: 0.9481338020346896
Weighted AreaUnderROC: 0.9952743351833367
Root mean squared error: 0.20677895542076546
Relative absolute error: 10.746567165380506
Root relative squared error: 41.35579108415309
Weighted TruePositiveRate: 0.9451009354997538
Weighted MatthewsCorrelation: 0.8929238097650618
Weighted FMeasure: 0.9449194702171639
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9954175327968784
Mean absolute error: 0.05373283582690253
Coverage of cases: 97.88281634662728
Instances selection time: 18.0
Test time: 39.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9451009354997538
Weighted FalsePositiveRate: 0.05807122022421464
Kappa statistic: 0.889725695366786
Training time: 1.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 53.20039389463319
Incorrectly Classified Instances: 5.391432791728213
Correctly Classified Instances: 94.6085672082718
Weighted Precision: 0.9489827873712491
Weighted AreaUnderROC: 0.9951063590145914
Root mean squared error: 0.2053986746398946
Relative absolute error: 10.640335944037275
Root relative squared error: 41.079734927978926
Weighted TruePositiveRate: 0.9460856720827179
Weighted MatthewsCorrelation: 0.8947672616223135
Weighted FMeasure: 0.9459138529562727
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9952588377318026
Mean absolute error: 0.05320167972018638
Coverage of cases: 98.03052683407189
Instances selection time: 15.0
Test time: 40.0
Accumulative iteration time: 106.0
Weighted Recall: 0.9460856720827179
Weighted FalsePositiveRate: 0.05701305588787328
Kappa statistic: 0.89171140863618
Training time: 2.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 53.17577548005909
Incorrectly Classified Instances: 5.2437223042836045
Correctly Classified Instances: 94.75627769571639
Weighted Precision: 0.9502616533182033
Weighted AreaUnderROC: 0.995046159462787
Root mean squared error: 0.20263888595997737
Relative absolute error: 10.399692896425773
Root relative squared error: 40.527777191995476
Weighted TruePositiveRate: 0.947562776957164
Weighted MatthewsCorrelation: 0.8975375051951899
Weighted FMeasure: 0.9474047933775779
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9952029126374221
Mean absolute error: 0.05199846448212886
Coverage of cases: 98.12900049236829
Instances selection time: 13.0
Test time: 39.0
Accumulative iteration time: 121.0
Weighted Recall: 0.947562776957164
Weighted FalsePositiveRate: 0.05542580938336125
Kappa statistic: 0.8946894492267178
Training time: 2.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 53.07730182176268
Incorrectly Classified Instances: 5.219103889709503
Correctly Classified Instances: 94.7808961102905
Weighted Precision: 0.9506869928649204
Weighted AreaUnderROC: 0.9957146657759722
Root mean squared error: 0.2029026223802992
Relative absolute error: 10.416694200155904
Root relative squared error: 40.58052447605984
Weighted TruePositiveRate: 0.947808961102905
Weighted MatthewsCorrelation: 0.8982053481499744
Weighted FMeasure: 0.9476441643776156
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9958421358152268
Mean absolute error: 0.05208347100077952
Coverage of cases: 98.22747415066469
Instances selection time: 11.0
Test time: 39.0
Accumulative iteration time: 134.0
Weighted Recall: 0.947808961102905
Weighted FalsePositiveRate: 0.05526758847544699
Kappa statistic: 0.8951745508353716
Training time: 2.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 53.04037419990153
Incorrectly Classified Instances: 4.99753815854259
Correctly Classified Instances: 95.0024618414574
Weighted Precision: 0.9530338196420782
Weighted AreaUnderROC: 0.9965620715636736
Root mean squared error: 0.193980160105214
Relative absolute error: 9.776835302212875
Root relative squared error: 38.7960320210428
Weighted TruePositiveRate: 0.9500246184145741
Weighted MatthewsCorrelation: 0.9027771915159218
Weighted FMeasure: 0.9498624026202375
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9966551828953756
Mean absolute error: 0.04888417651106438
Coverage of cases: 98.2767109798129
Instances selection time: 9.0
Test time: 39.0
Accumulative iteration time: 145.0
Weighted Recall: 0.9500246184145741
Weighted FalsePositiveRate: 0.05309935907102108
Kappa statistic: 0.8996193358402549
Training time: 2.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 53.08961102904973
Incorrectly Classified Instances: 4.99753815854259
Correctly Classified Instances: 95.0024618414574
Weighted Precision: 0.952960548297871
Weighted AreaUnderROC: 0.996756020926141
Root mean squared error: 0.19272464941215647
Relative absolute error: 9.685600097518808
Root relative squared error: 38.5449298824313
Weighted TruePositiveRate: 0.9500246184145741
Weighted MatthewsCorrelation: 0.90270586920329
Weighted FMeasure: 0.9498653522836682
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9968411753303709
Mean absolute error: 0.04842800048759404
Coverage of cases: 98.35056622353521
Instances selection time: 7.0
Test time: 39.0
Accumulative iteration time: 155.0
Weighted Recall: 0.9500246184145741
Weighted FalsePositiveRate: 0.0530639190122974
Kappa statistic: 0.8996229038956371
Training time: 3.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 53.003446578040375
Incorrectly Classified Instances: 4.874446085672083
Correctly Classified Instances: 95.12555391432792
Weighted Precision: 0.9543891452860321
Weighted AreaUnderROC: 0.9970135682022083
Root mean squared error: 0.19343811895694962
Relative absolute error: 9.723079442246632
Root relative squared error: 38.687623791389925
Weighted TruePositiveRate: 0.9512555391432792
Weighted MatthewsCorrelation: 0.9053674510876427
Weighted FMeasure: 0.9510929577325947
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9970898916150686
Mean absolute error: 0.04861539721123316
Coverage of cases: 98.2520925652388
Instances selection time: 4.0
Test time: 39.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9512555391432792
Weighted FalsePositiveRate: 0.05191841388548916
Kappa statistic: 0.9020865452696761
Training time: 3.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 52.99113737075332
Incorrectly Classified Instances: 4.899064500246184
Correctly Classified Instances: 95.10093549975382
Weighted Precision: 0.954102860431267
Weighted AreaUnderROC: 0.9971837290321077
Root mean squared error: 0.19262498096501898
Relative absolute error: 9.652151556782668
Root relative squared error: 38.524996193003794
Weighted TruePositiveRate: 0.9510093549975381
Weighted MatthewsCorrelation: 0.9048345561380896
Weighted FMeasure: 0.9508474196108325
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9972538051746024
Mean absolute error: 0.04826075778391334
Coverage of cases: 98.2767109798129
Instances selection time: 2.0
Test time: 39.0
Accumulative iteration time: 168.0
Weighted Recall: 0.9510093549975381
Weighted FalsePositiveRate: 0.05214751491085081
Kappa statistic: 0.9015937819640826
Training time: 4.0
		
Time end:Sun Oct 08 04.36.31 EEST 2017