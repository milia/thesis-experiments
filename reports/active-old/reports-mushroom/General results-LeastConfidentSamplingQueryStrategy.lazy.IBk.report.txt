Sun Oct 08 03.28.59 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.28.59 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.04185130477597
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.999262912057666
Weighted AreaUnderROC: 0.9994873207121413
Root mean squared error: 0.021314649717084834
Relative absolute error: 0.32937841257372086
Root relative squared error: 4.262929943416967
Weighted TruePositiveRate: 0.999261447562777
Weighted MatthewsCorrelation: 0.9985223904985755
Weighted FMeasure: 0.9992614162308362
Iteration time: 911.1
Weighted AreaUnderPRC: 0.9993367955681778
Mean absolute error: 0.0016468920628686043
Coverage of cases: 99.94337764647959
Instances selection time: 911.1
Test time: 1145.8
Accumulative iteration time: 911.1
Weighted Recall: 0.999261447562777
Weighted FalsePositiveRate: 7.829912346389098E-4
Kappa statistic: 0.9985208638281605
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.011078286558345
Incorrectly Classified Instances: 0.014771048744460858
Correctly Classified Instances: 99.98522895125555
Weighted Precision: 0.9998524998264908
Weighted AreaUnderROC: 0.9999426647817087
Root mean squared error: 0.0063378732709697015
Relative absolute error: 0.17853677780182672
Root relative squared error: 1.2675746541939403
Weighted TruePositiveRate: 0.9998522895125553
Weighted MatthewsCorrelation: 0.9997043996027136
Weighted FMeasure: 0.9998522855055146
Iteration time: 968.3
Weighted AreaUnderPRC: 0.9999248779831292
Mean absolute error: 8.926838890091339E-4
Coverage of cases: 99.99261447562778
Instances selection time: 968.3
Test time: 1177.0
Accumulative iteration time: 1879.4
Weighted Recall: 0.9998522895125553
Weighted FalsePositiveRate: 1.5872465045120349E-4
Kappa statistic: 0.9997041811539653
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.011078286558345
Incorrectly Classified Instances: 0.014771048744460858
Correctly Classified Instances: 99.98522895125555
Weighted Precision: 0.9998524998264908
Weighted AreaUnderROC: 0.9999426647817087
Root mean squared error: 0.0063240765257429
Relative absolute error: 0.17475683408840445
Root relative squared error: 1.26481530514858
Weighted TruePositiveRate: 0.9998522895125553
Weighted MatthewsCorrelation: 0.9997043996027136
Weighted FMeasure: 0.9998522855055146
Iteration time: 958.6
Weighted AreaUnderPRC: 0.9999248779831292
Mean absolute error: 8.737841704420223E-4
Coverage of cases: 99.99261447562778
Instances selection time: 958.6
Test time: 1177.3
Accumulative iteration time: 2838.0
Weighted Recall: 0.9998522895125553
Weighted FalsePositiveRate: 1.5872465045120349E-4
Kappa statistic: 0.9997041811539653
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.011078286558345
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.004256326192626936
Relative absolute error: 0.15641840214149602
Root relative squared error: 0.851265238525387
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 937.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.820920107074801E-4
Coverage of cases: 100.0
Instances selection time: 937.3
Test time: 1185.6
Accumulative iteration time: 3775.3
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.009847365829636
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.004038014256474873
Relative absolute error: 0.1521286294557249
Root relative squared error: 0.8076028512949748
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 969.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.606431472786245E-4
Coverage of cases: 100.0
Instances selection time: 969.8
Test time: 1206.3
Accumulative iteration time: 4745.1
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.009847365829636
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.004023509233544851
Relative absolute error: 0.14874625267035907
Root relative squared error: 0.8047018467089699
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 959.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.437312633517953E-4
Coverage of cases: 100.0
Instances selection time: 959.2
Test time: 1223.8
Accumulative iteration time: 5704.3
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.009847365829636
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.004010239299359477
Relative absolute error: 0.14540521206649565
Root relative squared error: 0.8020478598718954
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1014.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.270260603324782E-4
Coverage of cases: 100.0
Instances selection time: 1014.4
Test time: 1313.3
Accumulative iteration time: 6718.7
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.007385524372225
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00341066363228727
Relative absolute error: 0.1394684950326655
Root relative squared error: 0.6821327264574539
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 989.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.973424751633276E-4
Coverage of cases: 100.0
Instances selection time: 989.0
Test time: 1264.7
Accumulative iteration time: 7707.7
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.007385524372225
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0033965039112873662
Relative absolute error: 0.13641083673810755
Root relative squared error: 0.6793007822574735
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 997.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.820541836905377E-4
Coverage of cases: 100.0
Instances selection time: 997.7
Test time: 1300.3
Accumulative iteration time: 8705.4
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.007385524372225
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0032491145753757006
Relative absolute error: 0.13271635548550997
Root relative squared error: 0.6498229150751402
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1013.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.6358177742755E-4
Coverage of cases: 100.0
Instances selection time: 1013.3
Test time: 1354.7
Accumulative iteration time: 9718.8
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.1
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003777638450323569
Relative absolute error: 0.13283235498409102
Root relative squared error: 0.7555276900647139
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1018.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.641617749204552E-4
Coverage of cases: 100.0
Instances selection time: 1018.6
Test time: 1299.5
Accumulative iteration time: 10737.4
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003765959923474965
Relative absolute error: 0.13009986181428534
Root relative squared error: 0.7531919846949932
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1004.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.504993090714268E-4
Coverage of cases: 100.0
Instances selection time: 1004.8
Test time: 1344.7
Accumulative iteration time: 11742.2
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003754650121042425
Relative absolute error: 0.12739788105290678
Root relative squared error: 0.750930024208485
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1006.1
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.369894052645338E-4
Coverage of cases: 100.0
Instances selection time: 1006.1
Test time: 1333.4
Accumulative iteration time: 12748.3
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0037436944951054012
Relative absolute error: 0.12475592985306913
Root relative squared error: 0.7487388990210803
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1059.1
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.237796492653458E-4
Coverage of cases: 100.0
Instances selection time: 1059.0
Test time: 1368.1
Accumulative iteration time: 13807.4
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.1
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0037329749601512736
Relative absolute error: 0.12220384929896926
Root relative squared error: 0.7465949920302549
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1021.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.110192464948464E-4
Coverage of cases: 100.0
Instances selection time: 1021.0
Test time: 1363.9
Accumulative iteration time: 14828.4
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0037227944841969987
Relative absolute error: 0.11976954876932448
Root relative squared error: 0.7445588968393999
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1025.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.988477438466224E-4
Coverage of cases: 100.0
Instances selection time: 1025.9
Test time: 1379.3
Accumulative iteration time: 15854.3
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.003712615889972472
Relative absolute error: 0.11726726969015866
Root relative squared error: 0.7425231779944943
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1032.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.863363484507933E-4
Coverage of cases: 100.0
Instances selection time: 1032.4
Test time: 1395.4
Accumulative iteration time: 16886.7
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0037026533507388245
Relative absolute error: 0.1148446194941027
Root relative squared error: 0.7405306701477649
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1037.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.742230974705134E-4
Coverage of cases: 100.0
Instances selection time: 1036.9
Test time: 1410.4
Accumulative iteration time: 17923.7
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.1
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0036930060848612764
Relative absolute error: 0.11251343902098454
Root relative squared error: 0.7386012169722552
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1042.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.625671951049227E-4
Coverage of cases: 100.0
Instances selection time: 1042.1
Test time: 1423.8
Accumulative iteration time: 18965.9
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.1
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0036840999020116916
Relative absolute error: 0.11034187772914483
Root relative squared error: 0.7368199804023381
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1048.1
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.517093886457242E-4
Coverage of cases: 100.0
Instances selection time: 1048.1
Test time: 1440.3
Accumulative iteration time: 20014.0
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0036752938053955047
Relative absolute error: 0.10827045240562028
Root relative squared error: 0.7350587610791011
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1066.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.413522620281014E-4
Coverage of cases: 100.0
Instances selection time: 1066.6
Test time: 1459.4
Accumulative iteration time: 21080.6
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0036667178308771413
Relative absolute error: 0.10613860386765243
Root relative squared error: 0.7333435661754284
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1055.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.306930193382622E-4
Coverage of cases: 100.0
Instances selection time: 1055.2
Test time: 1477.5
Accumulative iteration time: 22135.9
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.1
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.008616445100934
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132456
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0036585571952318444
Relative absolute error: 0.1041111736190328
Root relative squared error: 0.7317114390463689
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 1059.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.205558680951639E-4
Coverage of cases: 100.0
Instances selection time: 1059.9
Test time: 1490.9
Accumulative iteration time: 23195.8
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017614197090327663
Relative absolute error: 0.09165500807699323
Root relative squared error: 0.35228394180655337
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1064.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.582750403849661E-4
Coverage of cases: 100.0
Instances selection time: 1064.2
Test time: 1506.7
Accumulative iteration time: 24260.2
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.2
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017528921515587014
Relative absolute error: 0.08979368949827862
Root relative squared error: 0.35057843031174024
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1068.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.489684474913931E-4
Coverage of cases: 100.0
Instances selection time: 1068.7
Test time: 1521.4
Accumulative iteration time: 25329.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017446552675276946
Relative absolute error: 0.08794074878550535
Root relative squared error: 0.3489310535055389
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1071.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.397037439275267E-4
Coverage of cases: 100.0
Instances selection time: 1071.0
Test time: 1536.0
Accumulative iteration time: 26400.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.001736699009634414
Relative absolute error: 0.08618094791499649
Root relative squared error: 0.34733980192688274
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1074.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.309047395749826E-4
Coverage of cases: 100.0
Instances selection time: 1074.3
Test time: 1552.5
Accumulative iteration time: 27474.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.2
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.00615460364352
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.002000654362844476
Relative absolute error: 0.08546453181053568
Root relative squared error: 0.40013087256889524
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1098.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2732265905267837E-4
Coverage of cases: 100.0
Instances selection time: 1098.3
Test time: 1568.4
Accumulative iteration time: 28572.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.00615460364352
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0019942600730916363
Relative absolute error: 0.08398582612715713
Root relative squared error: 0.3988520146183273
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1080.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.199291306357856E-4
Coverage of cases: 100.0
Instances selection time: 1080.6
Test time: 1588.6
Accumulative iteration time: 29653.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017156421032849041
Relative absolute error: 0.08165240971028606
Root relative squared error: 0.34312842065698074
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1118.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.082620485514303E-4
Coverage of cases: 100.0
Instances selection time: 1118.4
Test time: 1648.7
Accumulative iteration time: 30771.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0017091726398596422
Relative absolute error: 0.08028155641791428
Root relative squared error: 0.3418345279719285
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1092.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.014077820895714E-4
Coverage of cases: 100.0
Instances selection time: 1092.8
Test time: 1648.8
Accumulative iteration time: 31864.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.00615460364353
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0019259680507055874
Relative absolute error: 0.07976459494820715
Root relative squared error: 0.3851936101411175
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1120.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.988229747410358E-4
Coverage of cases: 100.0
Instances selection time: 1120.7
Test time: 1642.3
Accumulative iteration time: 32985.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0016964539237017827
Relative absolute error: 0.07764524509384055
Root relative squared error: 0.3392907847403566
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1093.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.882262254692027E-4
Coverage of cases: 100.0
Instances selection time: 1093.7
Test time: 1663.0
Accumulative iteration time: 34079.3
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0016908884284430506
Relative absolute error: 0.07654669264163325
Root relative squared error: 0.33817768568861
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1100.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.827334632081663E-4
Coverage of cases: 100.0
Instances selection time: 1100.4
Test time: 1677.9
Accumulative iteration time: 35179.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0016850769813603508
Relative absolute error: 0.07534094211808703
Root relative squared error: 0.3370153962720701
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1117.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.767047105904352E-4
Coverage of cases: 100.0
Instances selection time: 1117.7
Test time: 1696.8
Accumulative iteration time: 36297.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.00492368291482
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0016791299827598263
Relative absolute error: 0.07415776285017035
Root relative squared error: 0.33582599655196527
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1098.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7078881425085174E-4
Coverage of cases: 100.0
Instances selection time: 1098.4
Test time: 1712.5
Accumulative iteration time: 37395.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.00369276218611
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0015338487169556541
Relative absolute error: 0.07248219793958113
Root relative squared error: 0.30676974339113083
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1097.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6241098969790563E-4
Coverage of cases: 100.0
Instances selection time: 1097.6
Test time: 1724.2
Accumulative iteration time: 38493.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.00246184145741
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0012860158680179231
Relative absolute error: 0.06902144260140508
Root relative squared error: 0.25720317360358463
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1122.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.451072130070254E-4
Coverage of cases: 100.0
Instances selection time: 1122.2
Test time: 1726.9
Accumulative iteration time: 39615.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0012309207287
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.647830521853197E-4
Relative absolute error: 0.06569914742351242
Root relative squared error: 0.19295661043706397
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1089.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.28495737117562E-4
Coverage of cases: 100.0
Instances selection time: 1089.0
Test time: 1769.4
Accumulative iteration time: 40704.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0012309207287
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.588297690223339E-4
Relative absolute error: 0.06459818988315744
Root relative squared error: 0.19176595380446676
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1093.1
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.229909494157871E-4
Coverage of cases: 100.0
Instances selection time: 1093.1
Test time: 1781.9
Accumulative iteration time: 41797.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0012309207287
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.527521686264393E-4
Relative absolute error: 0.06344922496704304
Root relative squared error: 0.19055043372528785
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1094.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.172461248352153E-4
Coverage of cases: 100.0
Instances selection time: 1094.5
Test time: 1776.1
Accumulative iteration time: 42892.3
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0012309207287
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.468466127797962E-4
Relative absolute error: 0.06234467567766398
Root relative squared error: 0.18936932255595915
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1092.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1172337838832E-4
Coverage of cases: 100.0
Instances selection time: 1092.5
Test time: 1794.5
Accumulative iteration time: 43984.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4688689387067423E-4
Relative absolute error: 0.059271346629199376
Root relative squared error: 0.06937737877413484
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1137.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.963567331459969E-4
Coverage of cases: 100.0
Instances selection time: 1137.2
Test time: 1833.1
Accumulative iteration time: 45122.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4070552856734095E-4
Relative absolute error: 0.05822954721852143
Root relative squared error: 0.06814110571346818
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1096.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.911477360926071E-4
Coverage of cases: 100.0
Instances selection time: 1096.5
Test time: 1823.9
Accumulative iteration time: 46218.6
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.348778636062058E-4
Relative absolute error: 0.057243519945917185
Root relative squared error: 0.06697557272124115
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1091.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8621759972958594E-4
Coverage of cases: 100.0
Instances selection time: 1091.2
Test time: 1931.2
Accumulative iteration time: 47309.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.2842018723307963E-4
Relative absolute error: 0.05614975747151694
Root relative squared error: 0.06568403744661593
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1086.1
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.807487873575847E-4
Coverage of cases: 100.0
Instances selection time: 1086.0
Test time: 1850.1
Accumulative iteration time: 48395.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.1
		
Time end:Sun Oct 08 03.49.24 EEST 2017