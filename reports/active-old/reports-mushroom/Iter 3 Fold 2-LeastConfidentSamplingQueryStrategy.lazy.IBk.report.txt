Sun Oct 08 03.39.16 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.39.16 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.58796404660274E-4
Relative absolute error: 0.14937509623861459
Root relative squared error: 0.1717592809320548
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 899.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.468754811930729E-4
Coverage of cases: 100.0
Instances selection time: 899.0
Test time: 1156.0
Accumulative iteration time: 899.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.394969807632243E-4
Relative absolute error: 0.14623321969623546
Root relative squared error: 0.16789939615264488
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 917.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.311660984811773E-4
Coverage of cases: 100.0
Instances selection time: 917.0
Test time: 1167.0
Accumulative iteration time: 1816.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.178622422289014E-4
Relative absolute error: 0.14236289990207474
Root relative squared error: 0.16357244844578028
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 923.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.118144995103737E-4
Coverage of cases: 100.0
Instances selection time: 923.0
Test time: 1267.0
Accumulative iteration time: 2739.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.954670365202878E-4
Relative absolute error: 0.1383131504530327
Root relative squared error: 0.15909340730405755
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 931.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.915657522651635E-4
Coverage of cases: 100.0
Instances selection time: 931.0
Test time: 1198.0
Accumulative iteration time: 3670.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.762092999816625E-4
Relative absolute error: 0.13502904039070582
Root relative squared error: 0.1552418599963325
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 947.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.75145201953529E-4
Coverage of cases: 100.0
Instances selection time: 947.0
Test time: 1219.0
Accumulative iteration time: 4617.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.559427086150675E-4
Relative absolute error: 0.13147861000972808
Root relative squared error: 0.1511885417230135
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 955.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.573930500486404E-4
Coverage of cases: 100.0
Instances selection time: 955.0
Test time: 1231.0
Accumulative iteration time: 5572.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.36660883613726E-4
Relative absolute error: 0.12809856552026058
Root relative squared error: 0.1473321767227452
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1171.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.404928276013029E-4
Coverage of cases: 100.0
Instances selection time: 1171.0
Test time: 1274.0
Accumulative iteration time: 6743.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.202223355002159E-4
Relative absolute error: 0.12532356309647885
Root relative squared error: 0.14404446710004318
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 995.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.266178154823943E-4
Coverage of cases: 100.0
Instances selection time: 995.0
Test time: 1281.0
Accumulative iteration time: 7738.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.027375098605002E-4
Relative absolute error: 0.12225801793328064
Root relative squared error: 0.14054750197210003
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1100.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.112900896664032E-4
Coverage of cases: 100.0
Instances selection time: 1100.0
Test time: 1310.0
Accumulative iteration time: 8838.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.855141998883008E-4
Relative absolute error: 0.11927046814982946
Root relative squared error: 0.13710283997766015
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1025.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.963523407491473E-4
Coverage of cases: 100.0
Instances selection time: 1025.0
Test time: 1347.0
Accumulative iteration time: 9863.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.701727586337428E-4
Relative absolute error: 0.11660757487411866
Root relative squared error: 0.13403455172674855
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1217.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.830378743705933E-4
Coverage of cases: 100.0
Instances selection time: 1217.0
Test time: 1300.0
Accumulative iteration time: 11080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.533120119936674E-4
Relative absolute error: 0.11366781029619076
Root relative squared error: 0.13066240239873347
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1000.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.683390514809538E-4
Coverage of cases: 100.0
Instances selection time: 1000.0
Test time: 1318.0
Accumulative iteration time: 12080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.377680421592022E-4
Relative absolute error: 0.11085865144223728
Root relative squared error: 0.12755360843184044
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1008.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.542932572111864E-4
Coverage of cases: 100.0
Instances selection time: 1008.0
Test time: 1336.0
Accumulative iteration time: 13088.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.213563696534957E-4
Relative absolute error: 0.10783825467579101
Root relative squared error: 0.12427127393069914
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1458.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.391912733789551E-4
Coverage of cases: 100.0
Instances selection time: 1458.0
Test time: 1355.0
Accumulative iteration time: 14546.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.067714520755506E-4
Relative absolute error: 0.10522518089662658
Root relative squared error: 0.12135429041511012
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1028.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.261259044831329E-4
Coverage of cases: 100.0
Instances selection time: 1028.0
Test time: 1366.0
Accumulative iteration time: 15574.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.95410985874344E-4
Relative absolute error: 0.10322395315777834
Root relative squared error: 0.11908219717486879
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1025.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.161197657888917E-4
Coverage of cases: 100.0
Instances selection time: 1025.0
Test time: 1380.0
Accumulative iteration time: 16599.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.803163669522086E-4
Relative absolute error: 0.10046796999471908
Root relative squared error: 0.11606327339044173
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1032.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.023398499735954E-4
Coverage of cases: 100.0
Instances selection time: 1032.0
Test time: 1394.0
Accumulative iteration time: 17631.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.66873135900027E-4
Relative absolute error: 0.09802456414355828
Root relative squared error: 0.11337462718000539
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1038.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.901228207177914E-4
Coverage of cases: 100.0
Instances selection time: 1038.0
Test time: 1411.0
Accumulative iteration time: 18669.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.52230235623631E-4
Relative absolute error: 0.09534334005443455
Root relative squared error: 0.1104460471247262
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1049.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.7671670027217274E-4
Coverage of cases: 100.0
Instances selection time: 1049.0
Test time: 1428.0
Accumulative iteration time: 19718.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.398325038228469E-4
Relative absolute error: 0.09308270138135794
Root relative squared error: 0.10796650076456939
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1047.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.654135069067897E-4
Coverage of cases: 100.0
Instances selection time: 1047.0
Test time: 1442.0
Accumulative iteration time: 20765.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.286549475163788E-4
Relative absolute error: 0.09109128217427195
Root relative squared error: 0.10573098950327575
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1055.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.5545641087135975E-4
Coverage of cases: 100.0
Instances selection time: 1055.0
Test time: 1472.0
Accumulative iteration time: 21820.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.169183944932618E-4
Relative absolute error: 0.08893427857657728
Root relative squared error: 0.10338367889865237
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1060.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.446713928828864E-4
Coverage of cases: 100.0
Instances selection time: 1060.0
Test time: 1481.0
Accumulative iteration time: 22880.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.055238792035723E-4
Relative absolute error: 0.08688429762213179
Root relative squared error: 0.10110477584071445
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1058.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.344214881106589E-4
Coverage of cases: 100.0
Instances selection time: 1058.0
Test time: 1489.0
Accumulative iteration time: 23938.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.946423385071114E-4
Relative absolute error: 0.08492929808211655
Root relative squared error: 0.09892846770142229
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1062.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.2464649041058273E-4
Coverage of cases: 100.0
Instances selection time: 1062.0
Test time: 1503.0
Accumulative iteration time: 25000.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.843841570761204E-4
Relative absolute error: 0.08304563315191424
Root relative squared error: 0.09687683141522409
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1068.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.152281657595712E-4
Coverage of cases: 100.0
Instances selection time: 1068.0
Test time: 1520.0
Accumulative iteration time: 26068.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.7491739390045055E-4
Relative absolute error: 0.08131890095205224
Root relative squared error: 0.09498347878009011
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1071.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0659450476026123E-4
Coverage of cases: 100.0
Instances selection time: 1071.0
Test time: 1535.0
Accumulative iteration time: 27139.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.653350112751538E-4
Relative absolute error: 0.07954611842459175
Root relative squared error: 0.09306700225503076
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1074.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9773059212295874E-4
Coverage of cases: 100.0
Instances selection time: 1074.0
Test time: 1551.0
Accumulative iteration time: 28213.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.5643585612763297E-4
Relative absolute error: 0.07796511587701523
Root relative squared error: 0.0912871712255266
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1083.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.898255793850761E-4
Coverage of cases: 100.0
Instances selection time: 1083.0
Test time: 1565.0
Accumulative iteration time: 29296.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.484155261272572E-4
Relative absolute error: 0.07659230183069976
Root relative squared error: 0.08968310522545143
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1084.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.829615091534988E-4
Coverage of cases: 100.0
Instances selection time: 1084.0
Test time: 1587.0
Accumulative iteration time: 30380.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.410648538964204E-4
Relative absolute error: 0.07535733870376471
Root relative squared error: 0.08821297077928408
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1079.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.767866935188236E-4
Coverage of cases: 100.0
Instances selection time: 1079.0
Test time: 1601.0
Accumulative iteration time: 31459.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.321894708418412E-4
Relative absolute error: 0.0738329965556684
Root relative squared error: 0.08643789416836824
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1081.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.69164982778342E-4
Coverage of cases: 100.0
Instances selection time: 1081.0
Test time: 1614.0
Accumulative iteration time: 32540.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.2544381703722006E-4
Relative absolute error: 0.07265058859265337
Root relative squared error: 0.085088763407444
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1082.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.632529429632669E-4
Coverage of cases: 100.0
Instances selection time: 1082.0
Test time: 1628.0
Accumulative iteration time: 33622.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.1758128485988154E-4
Relative absolute error: 0.07128672749815777
Root relative squared error: 0.08351625697197632
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1088.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5643363749078886E-4
Coverage of cases: 100.0
Instances selection time: 1088.0
Test time: 1675.0
Accumulative iteration time: 34710.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.10830040865565E-4
Relative absolute error: 0.07015282555166986
Root relative squared error: 0.082166008173113
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1088.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5076412775834936E-4
Coverage of cases: 100.0
Instances selection time: 1088.0
Test time: 1690.0
Accumulative iteration time: 35798.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.041437351567195E-4
Relative absolute error: 0.06897805031495288
Root relative squared error: 0.0808287470313439
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1086.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.448902515747644E-4
Coverage of cases: 100.0
Instances selection time: 1086.0
Test time: 1709.0
Accumulative iteration time: 36884.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.965752619596101E-4
Relative absolute error: 0.06769642015299243
Root relative squared error: 0.07931505239192202
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1086.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.3848210076496216E-4
Coverage of cases: 100.0
Instances selection time: 1086.0
Test time: 1741.0
Accumulative iteration time: 37970.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.892950250227883E-4
Relative absolute error: 0.06648146158813886
Root relative squared error: 0.07785900500455767
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1090.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.324073079406943E-4
Coverage of cases: 100.0
Instances selection time: 1090.0
Test time: 1701.0
Accumulative iteration time: 39060.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8024363915391436E-4
Relative absolute error: 0.0649205724623489
Root relative squared error: 0.07604872783078287
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1083.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2460286231174453E-4
Coverage of cases: 100.0
Instances selection time: 1083.0
Test time: 1713.0
Accumulative iteration time: 40143.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.736528466184152E-4
Relative absolute error: 0.06378222702474182
Root relative squared error: 0.07473056932368305
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1081.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.189111351237091E-4
Coverage of cases: 100.0
Instances selection time: 1081.0
Test time: 1729.0
Accumulative iteration time: 41224.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.678278584212572E-4
Relative absolute error: 0.06279073591555477
Root relative squared error: 0.07356557168425144
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1083.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1395367957777384E-4
Coverage of cases: 100.0
Instances selection time: 1083.0
Test time: 1745.0
Accumulative iteration time: 42307.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.620183131360308E-4
Relative absolute error: 0.061815766423657235
Root relative squared error: 0.07240366262720616
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1081.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.090788321182862E-4
Coverage of cases: 100.0
Instances selection time: 1081.0
Test time: 1758.0
Accumulative iteration time: 43388.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.561179733378879E-4
Relative absolute error: 0.060851152486572635
Root relative squared error: 0.07122359466757758
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1079.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.042557624328632E-4
Coverage of cases: 100.0
Instances selection time: 1079.0
Test time: 1773.0
Accumulative iteration time: 44467.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5020530809454025E-4
Relative absolute error: 0.05988221907860712
Root relative squared error: 0.07004106161890805
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1076.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.994110953930356E-4
Coverage of cases: 100.0
Instances selection time: 1076.0
Test time: 1806.0
Accumulative iteration time: 45543.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.441944558107294E-4
Relative absolute error: 0.05883862174771483
Root relative squared error: 0.06883889116214588
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1078.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9419310873857413E-4
Coverage of cases: 100.0
Instances selection time: 1078.0
Test time: 1805.0
Accumulative iteration time: 46621.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.399122439128879E-4
Relative absolute error: 0.05809837373381839
Root relative squared error: 0.06798244878257757
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1074.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9049186866909193E-4
Coverage of cases: 100.0
Instances selection time: 1074.0
Test time: 1816.0
Accumulative iteration time: 47695.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3211835597896143E-4
Relative absolute error: 0.05677835160762052
Root relative squared error: 0.06642367119579229
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1071.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.838917580381026E-4
Coverage of cases: 100.0
Instances selection time: 1071.0
Test time: 1830.0
Accumulative iteration time: 48766.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Sun Oct 08 03.41.19 EEST 2017