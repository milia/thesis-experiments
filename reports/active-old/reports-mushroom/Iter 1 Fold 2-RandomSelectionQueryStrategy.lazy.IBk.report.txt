Sun Oct 08 04.22.53 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 04.22.53 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995725346341615
Root mean squared error: 0.02380704444918569
Relative absolute error: 0.2998751165858793
Root relative squared error: 4.7614088898371385
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9994649749105265
Mean absolute error: 0.0014993755829293964
Coverage of cases: 99.9507631708518
Instances selection time: 18.0
Test time: 1185.0
Accumulative iteration time: 18.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995737483348027
Root mean squared error: 0.02380658311053849
Relative absolute error: 0.29585846131138605
Root relative squared error: 4.761316622107699
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9994642608331864
Mean absolute error: 0.0014792923065569301
Coverage of cases: 99.9507631708518
Instances selection time: 18.0
Test time: 1205.0
Accumulative iteration time: 36.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.999577146696598
Root mean squared error: 0.023806129638815333
Relative absolute error: 0.29159385433070983
Root relative squared error: 4.761225927763067
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9994682230067581
Mean absolute error: 0.0014579692716535492
Coverage of cases: 99.9507631708518
Instances selection time: 18.0
Test time: 1226.0
Accumulative iteration time: 54.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995783603972392
Root mean squared error: 0.023805834536421475
Relative absolute error: 0.2883401493627368
Root relative squared error: 4.761166907284295
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9994694573990897
Mean absolute error: 0.0014417007468136838
Coverage of cases: 99.9507631708518
Instances selection time: 18.0
Test time: 1237.0
Accumulative iteration time: 72.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995772680666623
Root mean squared error: 0.023805469162506178
Relative absolute error: 0.28472371031238286
Root relative squared error: 4.761093832501236
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9994666859464132
Mean absolute error: 0.0014236185515619142
Coverage of cases: 99.9507631708518
Instances selection time: 17.0
Test time: 1272.0
Accumulative iteration time: 89.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995778749169829
Root mean squared error: 0.023805123343758058
Relative absolute error: 0.2811147121907545
Root relative squared error: 4.761024668751611
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9994663758111219
Mean absolute error: 0.0014055735609537726
Coverage of cases: 99.9507631708518
Instances selection time: 17.0
Test time: 1285.0
Accumulative iteration time: 106.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9995775108067905
Root mean squared error: 0.02380484087370387
Relative absolute error: 0.2779079836416734
Root relative squared error: 4.760968174740774
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9994643482004097
Mean absolute error: 0.0013895399182083672
Coverage of cases: 99.9507631708518
Instances selection time: 17.0
Test time: 1295.0
Accumulative iteration time: 123.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993836828143873
Root mean squared error: 0.024518040798186626
Relative absolute error: 0.2833849077519749
Root relative squared error: 4.903608159637325
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9992719988727728
Mean absolute error: 0.0014169245387598746
Coverage of cases: 99.9507631708518
Instances selection time: 16.0
Test time: 1306.0
Accumulative iteration time: 139.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993833187041951
Root mean squared error: 0.024517728153244246
Relative absolute error: 0.2806787862609269
Root relative squared error: 4.903545630648849
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9992690623128042
Mean absolute error: 0.0014033939313046345
Coverage of cases: 99.9507631708518
Instances selection time: 17.0
Test time: 1327.0
Accumulative iteration time: 156.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993851392551569
Root mean squared error: 0.024517377355487983
Relative absolute error: 0.27761439305514213
Root relative squared error: 4.903475471097597
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9992725694856833
Mean absolute error: 0.0013880719652757108
Coverage of cases: 99.9507631708518
Instances selection time: 16.0
Test time: 1339.0
Accumulative iteration time: 172.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.999385381995285
Root mean squared error: 0.02439396449575301
Relative absolute error: 0.27189723982411335
Root relative squared error: 4.878792899150602
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9992707748716503
Mean absolute error: 0.0013594861991205669
Coverage of cases: 99.9507631708518
Instances selection time: 16.0
Test time: 1370.0
Accumulative iteration time: 188.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993882948768239
Root mean squared error: 0.024393670363224578
Relative absolute error: 0.2690803564572045
Root relative squared error: 4.8787340726449155
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 16.0
Weighted AreaUnderPRC: 0.999271605879589
Mean absolute error: 0.0013454017822860226
Coverage of cases: 99.9507631708518
Instances selection time: 16.0
Test time: 1373.0
Accumulative iteration time: 204.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993901154277858
Root mean squared error: 0.024393387749442646
Relative absolute error: 0.26629392240958344
Root relative squared error: 4.878677549888529
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 16.0
Weighted AreaUnderPRC: 0.999273309604122
Mean absolute error: 0.0013314696120479172
Coverage of cases: 99.9507631708518
Instances selection time: 16.0
Test time: 1409.0
Accumulative iteration time: 220.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993896299475293
Root mean squared error: 0.024393170912025468
Relative absolute error: 0.2640789816243738
Root relative squared error: 4.878634182405094
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.999271085610179
Mean absolute error: 0.001320394908121869
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1408.0
Accumulative iteration time: 235.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993907222781064
Root mean squared error: 0.024392945801284587
Relative absolute error: 0.26177217990093016
Root relative squared error: 4.878589160256917
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9992687050921866
Mean absolute error: 0.0013088608995046507
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1420.0
Accumulative iteration time: 250.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993869598061185
Root mean squared error: 0.024392823713739315
Relative absolute error: 0.260340915940844
Root relative squared error: 4.878564742747863
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9992598192901665
Mean absolute error: 0.00130170457970422
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1434.0
Accumulative iteration time: 265.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993880521366956
Root mean squared error: 0.024392582114903544
Relative absolute error: 0.25778033007918233
Root relative squared error: 4.8785164229807085
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9992593896388867
Mean absolute error: 0.0012889016503959117
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1451.0
Accumulative iteration time: 280.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993881735067598
Root mean squared error: 0.02439236477671302
Relative absolute error: 0.2554353954562136
Root relative squared error: 4.878472955342604
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9992574608063397
Mean absolute error: 0.0012771769772810678
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1472.0
Accumulative iteration time: 295.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993891444672729
Root mean squared error: 0.024392188831168563
Relative absolute error: 0.25341270223367157
Root relative squared error: 4.878437766233713
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9992569359881776
Mean absolute error: 0.0012670635111683578
Coverage of cases: 99.9507631708518
Instances selection time: 15.0
Test time: 1496.0
Accumulative iteration time: 310.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993908436481705
Root mean squared error: 0.024392047902639526
Relative absolute error: 0.25159968829019325
Root relative squared error: 4.878409580527905
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9992590353096631
Mean absolute error: 0.0012579984414509662
Coverage of cases: 99.9507631708518
Instances selection time: 14.0
Test time: 1506.0
Accumulative iteration time: 324.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.999391207758363
Root mean squared error: 0.02439191534840116
Relative absolute error: 0.24985276311336768
Root relative squared error: 4.878383069680232
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9992580769789915
Mean absolute error: 0.0012492638155668385
Coverage of cases: 99.9507631708518
Instances selection time: 14.0
Test time: 1513.0
Accumulative iteration time: 338.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.999508134125142
Weighted AreaUnderROC: 0.9993955770806714
Root mean squared error: 0.024391731909756163
Relative absolute error: 0.24762352521840786
Root relative squared error: 4.878346381951233
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.9990145101369982
Weighted FMeasure: 0.9995076403134782
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9992631121934104
Mean absolute error: 0.0012381176260920393
Coverage of cases: 99.9507631708518
Instances selection time: 14.0
Test time: 1530.0
Accumulative iteration time: 352.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 4.5820205072329465E-4
Kappa statistic: 0.9990140245420991
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012757038221603973
Relative absolute error: 0.15854403110044726
Root relative squared error: 2.5514076443207947
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.927201555022363E-4
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 1549.0
Accumulative iteration time: 366.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012756639810789313
Relative absolute error: 0.15685281633543693
Root relative squared error: 2.5513279621578624
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.842640816771847E-4
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 1575.0
Accumulative iteration time: 380.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01275622816857787
Relative absolute error: 0.1549877309795805
Root relative squared error: 2.551245633715574
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.749386548979025E-4
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 1596.0
Accumulative iteration time: 393.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012755823557948307
Relative absolute error: 0.15309834054336566
Root relative squared error: 2.5511647115896614
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.654917027168283E-4
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 1595.0
Accumulative iteration time: 406.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012755483606291957
Relative absolute error: 0.1515287514312589
Root relative squared error: 2.5510967212583915
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.576437571562946E-4
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 1607.0
Accumulative iteration time: 419.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01275509942425993
Relative absolute error: 0.14969780049611153
Root relative squared error: 2.551019884851986
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.484890024805577E-4
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 1624.0
Accumulative iteration time: 432.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013755011146268637
Relative absolute error: 0.1598733574940801
Root relative squared error: 2.7510022292537273
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.993667874704005E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1641.0
Accumulative iteration time: 445.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 1.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013754726493703375
Relative absolute error: 0.15833932133074488
Root relative squared error: 2.750945298740675
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.916966066537244E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1662.0
Accumulative iteration time: 457.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013754483966189127
Relative absolute error: 0.15701608959157853
Root relative squared error: 2.750896793237825
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.850804479578926E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1672.0
Accumulative iteration time: 469.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0492368291482
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013754190076409252
Relative absolute error: 0.1553570693360229
Root relative squared error: 2.7508380152818503
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.767853466801144E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1681.0
Accumulative iteration time: 481.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0246184145741
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.006355023666223395
Relative absolute error: 0.10149021850188822
Root relative squared error: 1.271004733244679
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.074510925094411E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1702.0
Accumulative iteration time: 493.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0246184145741
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0063545237615059555
Relative absolute error: 0.10020138017801727
Root relative squared error: 1.2709047523011912
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.010069008900864E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1716.0
Accumulative iteration time: 505.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0246184145741
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.00635401554739559
Relative absolute error: 0.09886881920071267
Root relative squared error: 1.270803109479118
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.943440960035634E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1733.0
Accumulative iteration time: 517.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.042570514064289E-4
Relative absolute error: 0.06958193125388842
Root relative squared error: 0.08085141028128578
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.479096562694421E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1749.0
Accumulative iteration time: 528.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.979477661336924E-4
Relative absolute error: 0.06854355280562346
Root relative squared error: 0.07958955322673848
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 12.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4271776402811734E-4
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 1761.0
Accumulative iteration time: 540.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9059840063869917E-4
Relative absolute error: 0.06727791379219983
Root relative squared error: 0.07811968012773983
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.363895689609991E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1780.0
Accumulative iteration time: 551.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.812795404221977E-4
Relative absolute error: 0.0656528353282469
Root relative squared error: 0.07625590808443954
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.282641766412345E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1809.0
Accumulative iteration time: 562.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.74682163927107E-4
Relative absolute error: 0.06452859599695347
Root relative squared error: 0.0749364327854214
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2264297998476737E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1806.0
Accumulative iteration time: 573.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.6762377421826174E-4
Relative absolute error: 0.06329729125864719
Root relative squared error: 0.07352475484365235
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.16486456293236E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1822.0
Accumulative iteration time: 584.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.619715978871781E-4
Relative absolute error: 0.06231046711542881
Root relative squared error: 0.07239431957743561
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 11.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.11552335577144E-4
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 1839.0
Accumulative iteration time: 595.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.553447815418452E-4
Relative absolute error: 0.06111783371339857
Root relative squared error: 0.07106895630836904
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.055891685669929E-4
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 1859.0
Accumulative iteration time: 605.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.497545249981004E-4
Relative absolute error: 0.060128858748893364
Root relative squared error: 0.06995090499962009
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.006442937444668E-4
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 1871.0
Accumulative iteration time: 615.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4298000546066073E-4
Relative absolute error: 0.058958174233957494
Root relative squared error: 0.06859600109213215
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.947908711697875E-4
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 1885.0
Accumulative iteration time: 625.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3610670236586363E-4
Relative absolute error: 0.05772541295074332
Root relative squared error: 0.06722134047317273
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.886270647537166E-4
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 1896.0
Accumulative iteration time: 635.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Sun Oct 08 04.24.09 EEST 2017