Sun Oct 08 04.43.41 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.43.41 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 3.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 14.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 3.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.393894633185624
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9999787602387785
Root mean squared error: 0.036982288984880186
Relative absolute error: 0.4923682914820285
Root relative squared error: 7.396457796976037
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 6.0
Weighted AreaUnderPRC: 0.999957865773997
Mean absolute error: 0.0024618414574101425
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 4.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.393894633185624
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9999787602387785
Root mean squared error: 0.03840664388786422
Relative absolute error: 0.42522716082538825
Root relative squared error: 7.681328777572843
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 6.0
Weighted AreaUnderPRC: 0.999957865773997
Mean absolute error: 0.002126135804126941
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 4.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.0
Incorrectly Classified Instances: 1.4032496307237814
Correctly Classified Instances: 98.59675036927622
Weighted Precision: 0.9863376343218748
Weighted AreaUnderROC: 0.9882392407865557
Root mean squared error: 0.11753212782443984
Relative absolute error: 3.539143279172831
Root relative squared error: 23.506425564887966
Weighted TruePositiveRate: 0.9859675036927622
Weighted MatthewsCorrelation: 0.9722550307462424
Weighted FMeasure: 0.9859576383537003
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9824396265701151
Mean absolute error: 0.017695716395864154
Coverage of cases: 98.59675036927622
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9859675036927622
Weighted FalsePositiveRate: 0.015078841792864332
Kappa statistic: 0.9718702913955657
Training time: 5.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.0
Incorrectly Classified Instances: 1.4032496307237814
Correctly Classified Instances: 98.59675036927622
Weighted Precision: 0.9863376343218748
Weighted AreaUnderROC: 0.9882392407865557
Root mean squared error: 0.11765825457803689
Relative absolute error: 3.4240268209169993
Root relative squared error: 23.531650915607376
Weighted TruePositiveRate: 0.9859675036927622
Weighted MatthewsCorrelation: 0.9722550307462424
Weighted FMeasure: 0.9859576383537003
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9824396265701151
Mean absolute error: 0.017120134104584996
Coverage of cases: 98.59675036927622
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 40.0
Weighted Recall: 0.9859675036927622
Weighted FalsePositiveRate: 0.015078841792864332
Kappa statistic: 0.9718702913955657
Training time: 5.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 8.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 48.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 6.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 8.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 56.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 7.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 9.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 65.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 8.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 75.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 85.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Time end:Sun Oct 08 04.43.42 EEST 2017