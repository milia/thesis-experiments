Sun Oct 08 03.22.54 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 03.22.54 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0246184145741
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.999006100544903
Root mean squared error: 0.03711736131250239
Relative absolute error: 0.44162413497988207
Root relative squared error: 7.423472262500478
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 1810.0
Weighted AreaUnderPRC: 0.9986953664302942
Mean absolute error: 0.0022081206748994103
Coverage of cases: 99.8769079271295
Instances selection time: 1809.0
Test time: 1131.0
Accumulative iteration time: 1810.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 1.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.999006100544903
Root mean squared error: 0.036592623501137
Relative absolute error: 0.3816108088308543
Root relative squared error: 7.3185247002274
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 2073.0
Weighted AreaUnderPRC: 0.9986953664302942
Mean absolute error: 0.0019080540441542716
Coverage of cases: 99.8769079271295
Instances selection time: 2072.0
Test time: 1425.0
Accumulative iteration time: 3883.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 1.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.3781523238935557E-4
Relative absolute error: 0.07460017532601639
Root relative squared error: 0.08756304647787111
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2197.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.730008766300819E-4
Coverage of cases: 100.0
Instances selection time: 2196.0
Test time: 1664.0
Accumulative iteration time: 6080.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031570874919876406
Relative absolute error: 0.06578413329652978
Root relative squared error: 0.6314174983975281
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2254.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.289206664826489E-4
Coverage of cases: 100.0
Instances selection time: 2253.0
Test time: 1899.0
Accumulative iteration time: 8334.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031508785608803753
Relative absolute error: 0.054191841707182054
Root relative squared error: 0.630175712176075
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2231.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.7095920853591025E-4
Coverage of cases: 100.0
Instances selection time: 2230.0
Test time: 2097.0
Accumulative iteration time: 10565.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0031473430519707367
Relative absolute error: 0.046343034578152385
Root relative squared error: 0.6294686103941474
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2132.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.3171517289076194E-4
Coverage of cases: 100.0
Instances selection time: 2131.0
Test time: 2322.0
Accumulative iteration time: 12697.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.922850890753923E-4
Relative absolute error: 0.030357630553325587
Root relative squared error: 0.03845701781507846
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2304.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.5178815276662794E-4
Coverage of cases: 100.0
Instances selection time: 2298.0
Test time: 2567.0
Accumulative iteration time: 15001.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 6.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4943625183571585E-4
Relative absolute error: 0.023910960720743737
Root relative squared error: 0.02988725036714317
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1703.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1955480360371869E-4
Coverage of cases: 100.0
Instances selection time: 1702.0
Test time: 2827.0
Accumulative iteration time: 16704.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1523032542222997E-4
Relative absolute error: 0.018731738849219554
Root relative squared error: 0.023046065084445993
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1364.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.365869424609776E-5
Coverage of cases: 100.0
Instances selection time: 1362.0
Test time: 3125.0
Accumulative iteration time: 18068.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 2.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.849857901368183E-5
Relative absolute error: 0.014795059346055562
Root relative squared error: 0.017699715802736365
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 958.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.397529673027781E-5
Coverage of cases: 100.0
Instances selection time: 956.0
Test time: 3320.0
Accumulative iteration time: 19026.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 2.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.218773117588335E-5
Relative absolute error: 0.012218146704587471
Root relative squared error: 0.01443754623517667
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 471.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.109073352293735E-5
Coverage of cases: 100.0
Instances selection time: 470.0
Test time: 3621.0
Accumulative iteration time: 19497.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Time end:Sun Oct 08 03.23.41 EEST 2017