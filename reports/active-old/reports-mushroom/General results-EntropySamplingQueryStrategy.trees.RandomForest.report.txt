Sun Oct 08 03.14.22 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.14.22 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 53.931560807484004
Incorrectly Classified Instances: 0.22402757262432296
Correctly Classified Instances: 99.77597242737569
Weighted Precision: 0.9977686145304585
Weighted AreaUnderROC: 0.9997199871250636
Root mean squared error: 0.0559449901441547
Relative absolute error: 2.5997857653040493
Root relative squared error: 11.188998028830941
Weighted TruePositiveRate: 0.9977597242737566
Weighted MatthewsCorrelation: 0.9955224612171161
Weighted FMeasure: 0.9977596625317162
Iteration time: 36.6
Weighted AreaUnderPRC: 0.9996129255579117
Mean absolute error: 0.012998928826520244
Coverage of cases: 99.97784342688331
Instances selection time: 13.4
Test time: 19.9
Accumulative iteration time: 36.6
Weighted Recall: 0.9977597242737566
Weighted FalsePositiveRate: 0.0022726516420265575
Kappa statistic: 0.9955134664396181
Training time: 23.2
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 54.03618906942394
Incorrectly Classified Instances: 0.05662235352043328
Correctly Classified Instances: 99.94337764647959
Weighted Precision: 0.9994347458357312
Weighted AreaUnderROC: 0.9998831813132825
Root mean squared error: 0.04789510257398323
Relative absolute error: 2.4516395527807058
Root relative squared error: 9.579020514796644
Weighted TruePositiveRate: 0.9994337764647956
Weighted MatthewsCorrelation: 0.998867022405651
Weighted FMeasure: 0.9994337578770219
Iteration time: 27.9
Weighted AreaUnderPRC: 0.9998360765482438
Mean absolute error: 0.01225819776390353
Coverage of cases: 99.99015263417036
Instances selection time: 11.9
Test time: 19.5
Accumulative iteration time: 64.5
Weighted Recall: 0.9994337764647956
Weighted FalsePositiveRate: 6.0844449339628E-4
Kappa statistic: 0.998866015524578
Training time: 16.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 53.38749384539635
Incorrectly Classified Instances: 0.032003938946331856
Correctly Classified Instances: 99.96799606105368
Weighted Precision: 0.9996804862401998
Weighted AreaUnderROC: 0.9999449101278948
Root mean squared error: 0.03934249246672454
Relative absolute error: 1.880854792907136
Root relative squared error: 7.8684984933449105
Weighted TruePositiveRate: 0.9996799606105367
Weighted MatthewsCorrelation: 0.9993595998911335
Weighted FMeasure: 0.999679950550987
Iteration time: 29.5
Weighted AreaUnderPRC: 0.9999337658052811
Mean absolute error: 0.009404273964535682
Coverage of cases: 99.98522895125555
Instances selection time: 10.4
Test time: 18.3
Accumulative iteration time: 94.0
Weighted Recall: 0.9996799606105367
Weighted FalsePositiveRate: 3.439034093109409E-4
Kappa statistic: 0.9993590539245728
Training time: 19.1
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 53.26193993106843
Incorrectly Classified Instances: 0.019694731659281144
Correctly Classified Instances: 99.98030526834073
Weighted Precision: 0.9998034263974803
Weighted AreaUnderROC: 0.9999812604620996
Root mean squared error: 0.034562677591602455
Relative absolute error: 1.7017011563403108
Root relative squared error: 6.912535518320491
Weighted TruePositiveRate: 0.9998030526834072
Weighted MatthewsCorrelation: 0.9996059560580491
Weighted FMeasure: 0.9998030455118393
Iteration time: 26.3
Weighted AreaUnderPRC: 0.9999818655068949
Mean absolute error: 0.008508505781701557
Coverage of cases: 99.99261447562778
Instances selection time: 11.6
Test time: 18.1
Accumulative iteration time: 120.3
Weighted Recall: 0.9998030526834072
Weighted FalsePositiveRate: 2.1163286726827131E-4
Kappa statistic: 0.9996055678822335
Training time: 14.7
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 52.89266371245692
Incorrectly Classified Instances: 0.012309207287050714
Correctly Classified Instances: 99.98769079271295
Weighted Precision: 0.999877106479375
Weighted AreaUnderROC: 0.9999987862993589
Root mean squared error: 0.03170235959213773
Relative absolute error: 1.4890858511070495
Root relative squared error: 6.340471918427545
Weighted TruePositiveRate: 0.9998769079271295
Weighted MatthewsCorrelation: 0.9997536887825035
Weighted FMeasure: 0.9998769041217281
Iteration time: 22.5
Weighted AreaUnderPRC: 0.9999987901170716
Mean absolute error: 0.007445429255535249
Coverage of cases: 100.0
Instances selection time: 8.7
Test time: 15.9
Accumulative iteration time: 142.8
Weighted Recall: 0.9998769079271295
Weighted FalsePositiveRate: 1.3227054204266957E-4
Kappa statistic: 0.9997534825474481
Training time: 13.8
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 52.73633677991137
Incorrectly Classified Instances: 0.009847365829640572
Correctly Classified Instances: 99.99015263417036
Weighted Precision: 0.9999017131987401
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.029170820726053585
Relative absolute error: 1.3714125350554878
Root relative squared error: 5.834164145210719
Weighted TruePositiveRate: 0.9999015263417036
Weighted MatthewsCorrelation: 0.9998029780290245
Weighted FMeasure: 0.9999015227559196
Iteration time: 23.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00685706267527744
Coverage of cases: 100.0
Instances selection time: 8.8
Test time: 16.4
Accumulative iteration time: 166.1
Weighted Recall: 0.9999015263417036
Weighted FalsePositiveRate: 1.0581643363413566E-4
Kappa statistic: 0.9998027839411169
Training time: 14.5
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 53.23239783357951
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03017367799893399
Relative absolute error: 1.5354550052894762
Root relative squared error: 6.034735599786798
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007677275026447381
Coverage of cases: 100.0
Instances selection time: 9.9
Test time: 15.5
Accumulative iteration time: 193.4
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.4
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 52.642786804529784
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.026319910414780396
Relative absolute error: 1.2519404421478728
Root relative squared error: 5.263982082956079
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006259702210739365
Coverage of cases: 100.0
Instances selection time: 8.9
Test time: 15.8
Accumulative iteration time: 216.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.4
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 52.62678483505662
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025938870466427698
Relative absolute error: 1.2450341512111094
Root relative squared error: 5.187774093285539
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006225170756055547
Coverage of cases: 100.0
Instances selection time: 9.2
Test time: 14.5
Accumulative iteration time: 238.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.8
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 52.53938946331856
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025230579525207592
Relative absolute error: 1.1629823753970367
Root relative squared error: 5.0461159050415185
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005814911876985184
Coverage of cases: 100.0
Instances selection time: 8.4
Test time: 15.5
Accumulative iteration time: 260.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.4
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 52.125800098473654
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.022403616324495272
Relative absolute error: 0.9796560688601339
Root relative squared error: 4.480723264899053
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00489828034430067
Coverage of cases: 100.0
Instances selection time: 9.7
Test time: 18.4
Accumulative iteration time: 287.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.7
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 52.013786312161486
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02308650989500253
Relative absolute error: 0.9506577208980259
Root relative squared error: 4.617301979000507
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004753288604490129
Coverage of cases: 100.0
Instances selection time: 8.5
Test time: 14.6
Accumulative iteration time: 309.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.3
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 51.82668636139832
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020514683595340696
Relative absolute error: 0.8647304275595948
Root relative squared error: 4.102936719068139
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004323652137797973
Coverage of cases: 100.0
Instances selection time: 9.1
Test time: 15.1
Accumulative iteration time: 335.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.2
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 51.832840965041854
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02118374446468073
Relative absolute error: 0.8255387548931609
Root relative squared error: 4.236748892936146
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004127693774465804
Coverage of cases: 100.0
Instances selection time: 7.3
Test time: 15.4
Accumulative iteration time: 356.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.4
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 51.64451009354998
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019808009754467556
Relative absolute error: 0.7682352773663526
Root relative squared error: 3.961601950893511
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0038411763868317615
Coverage of cases: 100.0
Instances selection time: 7.5
Test time: 15.0
Accumulative iteration time: 379.2
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 51.50664697193501
Incorrectly Classified Instances: 0.002461841457410143
Correctly Classified Instances: 99.9975381585426
Weighted Precision: 0.9999753932806348
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01849253626139844
Relative absolute error: 0.6718846840648374
Root relative squared error: 3.6985072522796885
Weighted TruePositiveRate: 0.9999753815854259
Weighted MatthewsCorrelation: 0.9999507107534791
Weighted FMeasure: 0.9999753813658085
Iteration time: 26.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003359423420324188
Coverage of cases: 100.0
Instances selection time: 9.5
Test time: 18.5
Accumulative iteration time: 405.8
Weighted Recall: 0.9999753815854259
Weighted FalsePositiveRate: 2.6454108408533914E-5
Kappa statistic: 0.9999506986063313
Training time: 17.1
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 51.356474643032996
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018404325688968527
Relative absolute error: 0.6186505288275435
Root relative squared error: 3.680865137793704
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003093252644137717
Coverage of cases: 100.0
Instances selection time: 6.9
Test time: 14.4
Accumulative iteration time: 428.1
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.4
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 51.35401280157557
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017553766020479273
Relative absolute error: 0.6540698453790678
Root relative squared error: 3.510753204095854
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003270349226895339
Coverage of cases: 100.0
Instances selection time: 6.7
Test time: 14.2
Accumulative iteration time: 449.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.9
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 51.64697193500738
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01878774182278229
Relative absolute error: 0.7043797405428037
Root relative squared error: 3.7575483645564582
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00352189870271402
Coverage of cases: 100.0
Instances selection time: 6.2
Test time: 13.5
Accumulative iteration time: 469.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.6
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 51.68143771541113
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02004952457731829
Relative absolute error: 0.7883118177777406
Root relative squared error: 4.009904915463659
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0039415590888887036
Coverage of cases: 100.0
Instances selection time: 7.8
Test time: 16.7
Accumulative iteration time: 493.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.4
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 51.50049236829149
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018508988650830564
Relative absolute error: 0.6830317355022868
Root relative squared error: 3.7017977301661125
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0034151586775114347
Coverage of cases: 100.0
Instances selection time: 6.6
Test time: 14.6
Accumulative iteration time: 515.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.4
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 51.1139832594781
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.015685439093766594
Relative absolute error: 0.49141135685548526
Root relative squared error: 3.137087818753319
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002457056784277426
Coverage of cases: 100.0
Instances selection time: 6.1
Test time: 13.4
Accumulative iteration time: 536.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.2
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 51.22107336287544
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01673850084483037
Relative absolute error: 0.551525444493682
Root relative squared error: 3.3477001689660733
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 24.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0027576272224684104
Coverage of cases: 100.0
Instances selection time: 7.7
Test time: 15.1
Accumulative iteration time: 560.5
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 16.8
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.87764647956672
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014607819945165426
Relative absolute error: 0.4074325459361937
Root relative squared error: 2.9215639890330847
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 21.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0020371627296809688
Coverage of cases: 100.0
Instances selection time: 6.9
Test time: 13.3
Accumulative iteration time: 581.7
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 14.3
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 51.17306745445594
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017205526512156066
Relative absolute error: 0.5477767498877825
Root relative squared error: 3.4411053024312133
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 20.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002738883749438912
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.6
Accumulative iteration time: 602.2
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 14.5
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.84564254062038
Incorrectly Classified Instances: 0.007385524372230429
Correctly Classified Instances: 99.99261447562778
Weighted Precision: 0.9999262499132454
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014367215953008713
Relative absolute error: 0.3864501099667895
Root relative squared error: 2.8734431906017432
Weighted TruePositiveRate: 0.9999261447562777
Weighted MatthewsCorrelation: 0.9998521998013569
Weighted FMeasure: 0.9999261427527573
Iteration time: 21.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019322505498339478
Coverage of cases: 100.0
Instances selection time: 5.9
Test time: 13.3
Accumulative iteration time: 623.4
Weighted Recall: 0.9999261447562777
Weighted FalsePositiveRate: 7.936232522560174E-5
Kappa statistic: 0.9998520905769827
Training time: 15.3
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.84071885770557
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014657536670263694
Relative absolute error: 0.4099230365127419
Root relative squared error: 2.9315073340527387
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0020496151825637092
Coverage of cases: 100.0
Instances selection time: 5.9
Test time: 13.2
Accumulative iteration time: 644.6
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.3
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 51.02166420482521
Incorrectly Classified Instances: 0.002461841457410143
Correctly Classified Instances: 99.9975381585426
Weighted Precision: 0.9999753932806346
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01589067924151038
Relative absolute error: 0.46616042873655167
Root relative squared error: 3.1781358483020763
Weighted TruePositiveRate: 0.9999753815854259
Weighted MatthewsCorrelation: 0.999950710753479
Weighted FMeasure: 0.9999753813658085
Iteration time: 21.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0023308021436827583
Coverage of cases: 100.0
Instances selection time: 5.8
Test time: 13.2
Accumulative iteration time: 665.8
Weighted Recall: 0.9999753815854259
Weighted FalsePositiveRate: 2.6454108408533914E-5
Kappa statistic: 0.9999506986063313
Training time: 15.4
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.99704579025111
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.015665476138113598
Relative absolute error: 0.46862329613476933
Root relative squared error: 3.1330952276227197
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0023431164806738468
Coverage of cases: 100.0
Instances selection time: 6.1
Test time: 13.0
Accumulative iteration time: 687.4
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.5
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.876415558838
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.015170050187167144
Relative absolute error: 0.4192048891154919
Root relative squared error: 3.034010037433429
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0020960244455774596
Coverage of cases: 100.0
Instances selection time: 5.9
Test time: 13.3
Accumulative iteration time: 709.2
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.9
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.60438207779419
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012234844801819644
Relative absolute error: 0.2828195682797318
Root relative squared error: 2.4469689603639284
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.5
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001414097841398659
Coverage of cases: 100.0
Instances selection time: 6.5
Test time: 14.3
Accumulative iteration time: 732.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.6806991629739
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013117905363950138
Relative absolute error: 0.3458321285811728
Root relative squared error: 2.623581072790028
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 24.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0017291606429058644
Coverage of cases: 100.0
Instances selection time: 6.4
Test time: 14.7
Accumulative iteration time: 756.9
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 17.8
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.78286558345643
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013574225199624862
Relative absolute error: 0.3604030459461677
Root relative squared error: 2.7148450399249717
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 29.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0018020152297308384
Coverage of cases: 100.0
Instances selection time: 7.8
Test time: 15.8
Accumulative iteration time: 786.3
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 21.6
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.83948793697686
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014633281588248956
Relative absolute error: 0.3962752181668455
Root relative squared error: 2.9266563176497913
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019813760908342277
Coverage of cases: 100.0
Instances selection time: 6.6
Test time: 14.6
Accumulative iteration time: 810.6
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.7
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.59822747415067
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012885422832563099
Relative absolute error: 0.2909405404653424
Root relative squared error: 2.57708456651262
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.2
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014547027023267122
Coverage of cases: 100.0
Instances selection time: 6.1
Test time: 14.4
Accumulative iteration time: 834.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.1
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.585918266863615
Incorrectly Classified Instances: 0.002461841457410143
Correctly Classified Instances: 99.9975381585426
Weighted Precision: 0.9999753932806346
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011759827319418362
Relative absolute error: 0.25860524267532936
Root relative squared error: 2.351965463883672
Weighted TruePositiveRate: 0.9999753815854259
Weighted MatthewsCorrelation: 0.999950710753479
Weighted FMeasure: 0.9999753813658085
Iteration time: 23.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012930262133766467
Coverage of cases: 100.0
Instances selection time: 5.9
Test time: 14.0
Accumulative iteration time: 858.1
Weighted Recall: 0.9999753815854259
Weighted FalsePositiveRate: 2.6454108408533914E-5
Kappa statistic: 0.9999506986063313
Training time: 17.4
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.550221565731164
Incorrectly Classified Instances: 0.014771048744460858
Correctly Classified Instances: 99.98522895125555
Weighted Precision: 0.9998527095423775
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01204069019302879
Relative absolute error: 0.2467724328769607
Root relative squared error: 2.408138038605758
Weighted TruePositiveRate: 0.9998522895125553
Weighted MatthewsCorrelation: 0.9997046017253315
Weighted FMeasure: 0.9998522813366684
Iteration time: 24.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012338621643848036
Coverage of cases: 100.0
Instances selection time: 6.3
Test time: 17.6
Accumulative iteration time: 882.8
Weighted Recall: 0.9998522895125553
Weighted FalsePositiveRate: 1.5872465045120349E-4
Kappa statistic: 0.9997041654265374
Training time: 18.4
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.70162481536189
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01366475854685115
Relative absolute error: 0.3157634688237942
Root relative squared error: 2.7329517093702296
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0015788173441189713
Coverage of cases: 100.0
Instances selection time: 6.2
Test time: 13.2
Accumulative iteration time: 908.7
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 19.7
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.69670113244707
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013795036404182745
Relative absolute error: 0.3427293849530275
Root relative squared error: 2.759007280836549
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 26.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001713646924765137
Coverage of cases: 100.0
Instances selection time: 6.2
Test time: 15.2
Accumulative iteration time: 935.5
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 20.6
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.866568193008376
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.999950809929474
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014714292710367994
Relative absolute error: 0.3886299744164599
Root relative squared error: 2.9428585420735986
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.9999014440317275
Weighted FMeasure: 0.9999507622863903
Iteration time: 22.8
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019431498720822997
Coverage of cases: 100.0
Instances selection time: 5.3
Test time: 13.4
Accumulative iteration time: 958.3
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013954653566
Training time: 17.5
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.635155096011815
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01238287809834793
Relative absolute error: 0.29617619695331143
Root relative squared error: 2.4765756196695863
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014808809847665573
Coverage of cases: 100.0
Instances selection time: 5.2
Test time: 13.3
Accumulative iteration time: 981.3
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.8
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.64992614475628
Incorrectly Classified Instances: 0.009847365829640572
Correctly Classified Instances: 99.99015263417036
Weighted Precision: 0.9999017131987402
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012736198265979224
Relative absolute error: 0.2966187177433865
Root relative squared error: 2.5472396531958443
Weighted TruePositiveRate: 0.9999015263417036
Weighted MatthewsCorrelation: 0.9998029780290245
Weighted FMeasure: 0.9999015227559196
Iteration time: 24.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014830935887169324
Coverage of cases: 100.0
Instances selection time: 5.2
Test time: 13.4
Accumulative iteration time: 1005.6
Weighted Recall: 0.9999015263417036
Weighted FalsePositiveRate: 1.0581643363413566E-4
Kappa statistic: 0.9998027839411169
Training time: 19.1
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.59330379123584
Incorrectly Classified Instances: 0.004923682914820286
Correctly Classified Instances: 99.99507631708518
Weighted Precision: 0.9999507865612696
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013200097395222007
Relative absolute error: 0.28668454526591386
Root relative squared error: 2.6400194790444016
Weighted TruePositiveRate: 0.9999507631708517
Weighted MatthewsCorrelation: 0.999901421506958
Weighted FMeasure: 0.999950762731617
Iteration time: 27.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014334227263295691
Coverage of cases: 100.0
Instances selection time: 5.8
Test time: 14.9
Accumulative iteration time: 1033.5
Weighted Recall: 0.9999507631708517
Weighted FalsePositiveRate: 5.290821681706783E-5
Kappa statistic: 0.9999013972126628
Training time: 22.1
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.931806991629735
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014169642121727253
Relative absolute error: 0.3819704969998762
Root relative squared error: 2.833928424345451
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019098524849993814
Coverage of cases: 100.0
Instances selection time: 5.7
Test time: 14.6
Accumulative iteration time: 1058.9
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 19.7
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.75578532742492
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013592631882437483
Relative absolute error: 0.3320067828561845
Root relative squared error: 2.718526376487497
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0016600339142809229
Coverage of cases: 100.0
Instances selection time: 5.3
Test time: 13.1
Accumulative iteration time: 1083.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 19.6
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.62161496799607
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012207669072612582
Relative absolute error: 0.28654311612210553
Root relative squared error: 2.4415338145225167
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 29.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014327155806105275
Coverage of cases: 100.0
Instances selection time: 6.6
Test time: 16.2
Accumulative iteration time: 1113.5
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 23.1
		
Time end:Sun Oct 08 03.15.25 EEST 2017