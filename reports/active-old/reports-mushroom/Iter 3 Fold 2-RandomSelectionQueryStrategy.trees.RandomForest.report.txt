Sun Oct 08 04.35.49 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 04.35.49 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 55.15755785327425
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9999998786299359
Root mean squared error: 0.045243335861236966
Relative absolute error: 2.7394581239483555
Root relative squared error: 9.048667172247393
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9999997573796393
Mean absolute error: 0.013697290619741778
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 14.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 8.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 54.67749876907927
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04710598053390052
Relative absolute error: 2.623755597164781
Root relative squared error: 9.421196106780103
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013118777985823903
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 13.0
Accumulative iteration time: 53.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 54.579025110782865
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0456934537037733
Relative absolute error: 2.5958536740965568
Root relative squared error: 9.13869074075466
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.012979268370482784
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 80.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 54.2959133431807
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999949024573068
Root mean squared error: 0.049940994533900294
Relative absolute error: 2.7345388512153055
Root relative squared error: 9.98819890678006
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9999949153618145
Mean absolute error: 0.013672694256076527
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 106.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 9.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 51.7725258493353
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03394619353320347
Relative absolute error: 1.1088123415568094
Root relative squared error: 6.789238706640694
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005544061707784047
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 133.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 10.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 55.55145248645987
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992625774237212
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05596866274702417
Relative absolute error: 3.4127915760055756
Root relative squared error: 11.193732549404833
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985221550307092
Weighted FMeasure: 0.9992614667891501
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.017063957880027878
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 160.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 6.873030760849421E-4
Kappa statistic: 0.9985210630190265
Training time: 10.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 54.67749876907927
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.05615231923509203
Relative absolute error: 3.083570749546781
Root relative squared error: 11.230463847018406
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999995148875116
Mean absolute error: 0.015417853747733905
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 15.0
Accumulative iteration time: 187.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 10.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 53.729689807976364
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.9985228951255539
Weighted AreaUnderROC: 0.9999939314967939
Root mean squared error: 0.05027563825272532
Relative absolute error: 2.4242296602105085
Root relative squared error: 10.055127650545064
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997041968797213
Weighted FMeasure: 0.9985228951255539
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999929772565499
Mean absolute error: 0.012121148301052542
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 214.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0014809263283409595
Kappa statistic: 0.997041968797213
Training time: 10.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 53.138847858197934
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03871465432894728
Relative absolute error: 1.945660333589314
Root relative squared error: 7.742930865789456
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00972830166794657
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 14.0
Accumulative iteration time: 240.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 54.443623830625306
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999932032764092
Root mean squared error: 0.04797502181397271
Relative absolute error: 2.5818589341015863
Root relative squared error: 9.595004362794542
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999932215714936
Mean absolute error: 0.01290929467050793
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 13.0
Accumulative iteration time: 267.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 10.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 53.163466272772034
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03261207346940857
Relative absolute error: 1.5984563226845767
Root relative squared error: 6.522414693881714
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007992281613422884
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 14.0
Accumulative iteration time: 294.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 52.6957163958641
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03702076000275088
Relative absolute error: 1.5284308147367893
Root relative squared error: 7.404152000550177
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007642154073683946
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 14.0
Accumulative iteration time: 320.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 10.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 52.868045297882816
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03377967783176175
Relative absolute error: 1.4702381972614422
Root relative squared error: 6.75593556635235
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007351190986307211
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 13.0
Accumulative iteration time: 346.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 54.086656819300835
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04304241890048193
Relative absolute error: 2.1725823995612665
Root relative squared error: 8.608483780096385
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010862911997806332
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 13.0
Accumulative iteration time: 373.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 11.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 52.96651895617922
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987702342283096
Weighted AreaUnderROC: 0.9999985435592307
Root mean squared error: 0.04115881008112048
Relative absolute error: 1.8234499530338417
Root relative squared error: 8.231762016224096
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975361959651212
Weighted FMeasure: 0.9987691113152499
Iteration time: 28.0
Weighted AreaUnderPRC: 0.999998545084367
Mean absolute error: 0.009117249765169208
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 13.0
Accumulative iteration time: 401.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0011809451855319284
Kappa statistic: 0.997535105031711
Training time: 12.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 54.82520925652388
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.04483637605685322
Relative absolute error: 2.583499872830408
Root relative squared error: 8.967275211370644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999995148875116
Mean absolute error: 0.01291749936415204
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 15.0
Accumulative iteration time: 428.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 12.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 52.92959133431807
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.030665353897321074
Relative absolute error: 1.52631817740983
Root relative squared error: 6.133070779464215
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00763159088704915
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 13.0
Accumulative iteration time: 455.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 52.19103889709503
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02922131966916216
Relative absolute error: 1.0795327660396945
Root relative squared error: 5.844263933832432
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005397663830198472
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 14.0
Accumulative iteration time: 482.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 53.729689807976364
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.043987713587979475
Relative absolute error: 2.109820499839152
Root relative squared error: 8.797542717595896
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.010549102499195759
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 13.0
Accumulative iteration time: 510.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 51.90792712949286
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999991504095511
Root mean squared error: 0.031251365730445804
Relative absolute error: 1.041838256830522
Root relative squared error: 6.250273146089161
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999990300090309
Mean absolute error: 0.00520919128415261
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 13.0
Accumulative iteration time: 537.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 12.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 51.784835056622356
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0257505503053053
Relative absolute error: 0.9355526793325679
Root relative squared error: 5.15011006106106
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0046777633966628394
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 13.0
Accumulative iteration time: 563.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 52.74495322501231
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03425019498340459
Relative absolute error: 1.4514555716920088
Root relative squared error: 6.850038996680919
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007257277858460044
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 14.0
Accumulative iteration time: 590.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 13.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 51.39094042343673
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02161085162391923
Relative absolute error: 0.624741646435727
Root relative squared error: 4.322170324783846
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003123708232178635
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 13.0
Accumulative iteration time: 616.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 12.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 52.99113737075332
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999992717796152
Root mean squared error: 0.03978712669855752
Relative absolute error: 1.754661306549427
Root relative squared error: 7.957425339711505
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9999992722793452
Mean absolute error: 0.008773306532747134
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 644.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 15.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 53.028064992614475
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03240658306853118
Relative absolute error: 1.5725912318266244
Root relative squared error: 6.481316613706237
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007862956159133122
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 671.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 52.74495322501231
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03185046973557612
Relative absolute error: 1.4335762996026447
Root relative squared error: 6.370093947115224
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007167881498013224
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 13.0
Accumulative iteration time: 698.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 14.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 51.08321024126046
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0272186581401494
Relative absolute error: 0.7314531166357656
Root relative squared error: 5.44373162802988
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003657265583178828
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 724.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 52.09256523879862
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03371614598387618
Relative absolute error: 1.2439249250245172
Root relative squared error: 6.743229196775236
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006219624625122586
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 752.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 51.994091580502214
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03086210386150466
Relative absolute error: 1.1747056869093508
Root relative squared error: 6.172420772300932
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005873528434546754
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 13.0
Accumulative iteration time: 780.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 15.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 52.375677006400785
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02936587521593399
Relative absolute error: 1.226637932341105
Root relative squared error: 5.873175043186798
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006133189661705524
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 16.0
Accumulative iteration time: 807.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 51.292466765140325
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03238515874592559
Relative absolute error: 0.8840766679867492
Root relative squared error: 6.477031749185118
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004420383339933746
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 14.0
Accumulative iteration time: 838.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 15.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 52.732644017725256
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.035141402097299046
Relative absolute error: 1.5105475789674687
Root relative squared error: 7.02828041945981
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0075527378948373435
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 866.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 52.5480059084195
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02977904583308645
Relative absolute error: 1.2548340153364994
Root relative squared error: 5.95580916661729
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 32.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006274170076682497
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 14.0
Accumulative iteration time: 898.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 16.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 51.809453471196456
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02568900423269276
Relative absolute error: 0.9813468702850323
Root relative squared error: 5.137800846538552
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004906734351425162
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 13.0
Accumulative iteration time: 926.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 16.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 52.14180206794683
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02542875809752262
Relative absolute error: 1.0215564887248259
Root relative squared error: 5.085751619504524
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0051077824436241296
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 952.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 51.76021664204825
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02517127541893443
Relative absolute error: 0.7898619545880222
Root relative squared error: 5.034255083786886
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003949309772940111
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 978.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 15.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 52.38798621368784
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9999975725987176
Root mean squared error: 0.03510820248660405
Relative absolute error: 1.3704600714156299
Root relative squared error: 7.021640497320809
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 27.0
Weighted AreaUnderPRC: 0.999997576245433
Mean absolute error: 0.00685230035707815
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 1005.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 16.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 51.37863121614968
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9999990290394871
Root mean squared error: 0.031486167946432025
Relative absolute error: 0.9539512256996535
Root relative squared error: 6.297233589286405
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999990297578557
Mean absolute error: 0.004769756128498268
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 1032.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 16.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 51.784835056622356
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02454246978408371
Relative absolute error: 0.9270294014039899
Root relative squared error: 4.908493956816741
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00463514700701995
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 1059.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 51.82176267848351
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.026505259875346875
Relative absolute error: 1.0857501892180619
Root relative squared error: 5.301051975069375
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005428750946090309
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 1085.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 51.92023633677991
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.024672698521696318
Relative absolute error: 0.9384817102401898
Root relative squared error: 4.934539704339263
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004692408551200949
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 1111.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 51.40324963072378
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019186641869231837
Relative absolute error: 0.6780380499074865
Root relative squared error: 3.8373283738463675
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 29.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0033901902495374323
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 1140.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.960118168389954
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016051071775848448
Relative absolute error: 0.4386498793825443
Root relative squared error: 3.21021435516969
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0021932493969127214
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 1168.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 52.67109798129
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.031165872135577165
Relative absolute error: 1.3822895804630089
Root relative squared error: 6.233174427115433
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006911447902315044
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 1195.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 51.698670605612996
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.023503192003995962
Relative absolute error: 0.8942019885068655
Root relative squared error: 4.700638400799193
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 28.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004471009942534327
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 1223.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 51.477104874446084
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020519750397085333
Relative absolute error: 0.684793921186863
Root relative squared error: 4.103950079417067
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 27.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003423969605934315
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 1250.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Time end:Sun Oct 08 04.35.55 EEST 2017