Sun Oct 08 03.57.31 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.57.31 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.527106562507675E-4
Relative absolute error: 0.1479261998986473
Root relative squared error: 0.1705421312501535
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 942.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.396309994932366E-4
Coverage of cases: 100.0
Instances selection time: 941.0
Test time: 1204.0
Accumulative iteration time: 942.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.307658990214428E-4
Relative absolute error: 0.14411479700098578
Root relative squared error: 0.16615317980428856
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 955.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.205739850049289E-4
Coverage of cases: 100.0
Instances selection time: 955.0
Test time: 1197.0
Accumulative iteration time: 1897.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.076733409684192E-4
Relative absolute error: 0.1400273236424199
Root relative squared error: 0.16153466819368384
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 965.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.001366182120995E-4
Coverage of cases: 100.0
Instances selection time: 965.0
Test time: 1213.0
Accumulative iteration time: 2862.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.859062071489199E-4
Relative absolute error: 0.13619845837674383
Root relative squared error: 0.15718124142978399
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 977.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.809922918837191E-4
Coverage of cases: 100.0
Instances selection time: 977.0
Test time: 1236.0
Accumulative iteration time: 3839.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.663977480864235E-4
Relative absolute error: 0.13281373514211675
Root relative squared error: 0.15327954961728468
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1182.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.640686757105837E-4
Coverage of cases: 100.0
Instances selection time: 1182.0
Test time: 1252.0
Accumulative iteration time: 5021.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.469552219111956E-4
Relative absolute error: 0.1293554185324125
Root relative squared error: 0.1493910443822391
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 997.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.467770926620626E-4
Coverage of cases: 100.0
Instances selection time: 997.0
Test time: 1278.0
Accumulative iteration time: 6018.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.299194951169095E-4
Relative absolute error: 0.12642335622704803
Root relative squared error: 0.1459838990233819
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1007.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.321167811352402E-4
Coverage of cases: 100.0
Instances selection time: 1007.0
Test time: 1279.0
Accumulative iteration time: 7025.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.121631656091449E-4
Relative absolute error: 0.12326612064646064
Root relative squared error: 0.142432633121829
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1014.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.163306032323032E-4
Coverage of cases: 100.0
Instances selection time: 1014.0
Test time: 1292.0
Accumulative iteration time: 8039.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.934909876219766E-4
Relative absolute error: 0.11990743762190834
Root relative squared error: 0.1386981975243953
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1022.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.995371881095416E-4
Coverage of cases: 100.0
Instances selection time: 1022.0
Test time: 1307.0
Accumulative iteration time: 9061.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.753870148066073E-4
Relative absolute error: 0.11669341305983669
Root relative squared error: 0.13507740296132145
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1029.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.834670652991834E-4
Coverage of cases: 100.0
Instances selection time: 1029.0
Test time: 1323.0
Accumulative iteration time: 10090.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.603116261852408E-4
Relative absolute error: 0.11416864104793104
Root relative squared error: 0.13206232523704817
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1033.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.708432052396552E-4
Coverage of cases: 100.0
Instances selection time: 1033.0
Test time: 1336.0
Accumulative iteration time: 11123.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.439581761287787E-4
Relative absolute error: 0.11128831240134972
Root relative squared error: 0.12879163522575574
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1039.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.564415620067486E-4
Coverage of cases: 100.0
Instances selection time: 1039.0
Test time: 1354.0
Accumulative iteration time: 12162.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.289583234903593E-4
Relative absolute error: 0.10867912726744539
Root relative squared error: 0.12579166469807185
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1053.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.43395636337227E-4
Coverage of cases: 100.0
Instances selection time: 1053.0
Test time: 1375.0
Accumulative iteration time: 13215.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.154158804469715E-4
Relative absolute error: 0.10634010637280199
Root relative squared error: 0.1230831760893943
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1058.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.317005318640099E-4
Coverage of cases: 100.0
Instances selection time: 1058.0
Test time: 1389.0
Accumulative iteration time: 14273.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.01687361656595E-4
Relative absolute error: 0.10393926614625845
Root relative squared error: 0.120337472331319
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1065.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.196963307312922E-4
Coverage of cases: 100.0
Instances selection time: 1065.0
Test time: 1401.0
Accumulative iteration time: 15338.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.86579020326848E-4
Relative absolute error: 0.10127083193825848
Root relative squared error: 0.1173158040653696
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1067.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.063541596912924E-4
Coverage of cases: 100.0
Instances selection time: 1067.0
Test time: 1410.0
Accumulative iteration time: 16405.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.747472698589926E-4
Relative absolute error: 0.09918977383895583
Root relative squared error: 0.11494945397179852
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1072.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.959488691947792E-4
Coverage of cases: 100.0
Instances selection time: 1072.0
Test time: 1429.0
Accumulative iteration time: 17477.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.611870503232545E-4
Relative absolute error: 0.09678822160442686
Root relative squared error: 0.11223741006465089
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1075.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.839411080221343E-4
Coverage of cases: 100.0
Instances selection time: 1075.0
Test time: 1440.0
Accumulative iteration time: 18552.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.473460250279869E-4
Relative absolute error: 0.09423591912968479
Root relative squared error: 0.10946920500559738
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1083.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.71179595648424E-4
Coverage of cases: 100.0
Instances selection time: 1082.0
Test time: 1457.0
Accumulative iteration time: 19635.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.350194127644788E-4
Relative absolute error: 0.09202635606469738
Root relative squared error: 0.10700388255289577
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1087.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.601317803234869E-4
Coverage of cases: 100.0
Instances selection time: 1087.0
Test time: 1471.0
Accumulative iteration time: 20722.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.233257368711474E-4
Relative absolute error: 0.08998344235320363
Root relative squared error: 0.10466514737422948
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1094.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.499172117660181E-4
Coverage of cases: 100.0
Instances selection time: 1094.0
Test time: 1487.0
Accumulative iteration time: 21816.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.110762555304432E-4
Relative absolute error: 0.08779764710410456
Root relative squared error: 0.10221525110608864
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1103.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.389882355205228E-4
Coverage of cases: 100.0
Instances selection time: 1103.0
Test time: 1515.0
Accumulative iteration time: 22919.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.022579695929425E-4
Relative absolute error: 0.08629849191997573
Root relative squared error: 0.1004515939185885
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1106.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.314924595998786E-4
Coverage of cases: 100.0
Instances selection time: 1106.0
Test time: 1524.0
Accumulative iteration time: 24025.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.902405313259549E-4
Relative absolute error: 0.08410287737575596
Root relative squared error: 0.09804810626519098
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1108.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.205143868787798E-4
Coverage of cases: 100.0
Instances selection time: 1108.0
Test time: 1532.0
Accumulative iteration time: 25133.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.804299232879108E-4
Relative absolute error: 0.0823720822501842
Root relative squared error: 0.09608598465758215
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1114.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.11860411250921E-4
Coverage of cases: 100.0
Instances selection time: 1114.0
Test time: 1551.0
Accumulative iteration time: 26247.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.706924121339471E-4
Relative absolute error: 0.08060656938199483
Root relative squared error: 0.09413848242678942
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1119.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.030328469099741E-4
Coverage of cases: 100.0
Instances selection time: 1119.0
Test time: 1833.0
Accumulative iteration time: 27366.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.607114236543095E-4
Relative absolute error: 0.07878480470416166
Root relative squared error: 0.09214228473086189
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1122.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.939240235208083E-4
Coverage of cases: 100.0
Instances selection time: 1122.0
Test time: 1807.0
Accumulative iteration time: 28488.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.518066594229591E-4
Relative absolute error: 0.07710747611706244
Root relative squared error: 0.09036133188459182
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1129.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8553738058531216E-4
Coverage of cases: 100.0
Instances selection time: 1129.0
Test time: 1595.0
Accumulative iteration time: 29617.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.4290954852153604E-4
Relative absolute error: 0.07550329588646867
Root relative squared error: 0.08858190970430721
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1129.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.775164794323434E-4
Coverage of cases: 100.0
Instances selection time: 1129.0
Test time: 1622.0
Accumulative iteration time: 30746.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.337912627233215E-4
Relative absolute error: 0.07383905504760778
Root relative squared error: 0.0867582525446643
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1133.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.691952752380389E-4
Coverage of cases: 100.0
Instances selection time: 1133.0
Test time: 1628.0
Accumulative iteration time: 31879.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.261006811666487E-4
Relative absolute error: 0.07246167228819105
Root relative squared error: 0.08522013623332975
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1131.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.623083614409552E-4
Coverage of cases: 100.0
Instances selection time: 1131.0
Test time: 1639.0
Accumulative iteration time: 33010.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0026493934249653024
Relative absolute error: 0.07919879675683389
Root relative squared error: 0.5298786849930605
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1132.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.9599398378416945E-4
Coverage of cases: 100.0
Instances selection time: 1132.0
Test time: 1653.0
Accumulative iteration time: 34142.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.0943504866456764E-4
Relative absolute error: 0.0695592401316069
Root relative squared error: 0.08188700973291353
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1133.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.477962006580345E-4
Coverage of cases: 100.0
Instances selection time: 1133.0
Test time: 1670.0
Accumulative iteration time: 35275.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.0317062412149613E-4
Relative absolute error: 0.06855758860808348
Root relative squared error: 0.08063412482429923
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1134.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.427879430404174E-4
Coverage of cases: 100.0
Instances selection time: 1133.0
Test time: 1684.0
Accumulative iteration time: 36409.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.974416707808908E-4
Relative absolute error: 0.06760678887098322
Root relative squared error: 0.07948833415617816
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1138.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.380339443549161E-4
Coverage of cases: 100.0
Instances selection time: 1138.0
Test time: 1701.0
Accumulative iteration time: 37547.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9149073225453314E-4
Relative absolute error: 0.06659036705572764
Root relative squared error: 0.07829814645090663
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1139.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.329518352786382E-4
Coverage of cases: 100.0
Instances selection time: 1139.0
Test time: 1729.0
Accumulative iteration time: 38686.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8460172498893974E-4
Relative absolute error: 0.06543411866451457
Root relative squared error: 0.07692034499778795
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1140.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.271705933225728E-4
Coverage of cases: 100.0
Instances selection time: 1140.0
Test time: 1737.0
Accumulative iteration time: 39826.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.7834664266308003E-4
Relative absolute error: 0.06438394827888753
Root relative squared error: 0.075669328532616
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1135.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2191974139443764E-4
Coverage of cases: 100.0
Instances selection time: 1135.0
Test time: 1747.0
Accumulative iteration time: 40961.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.718951330452774E-4
Relative absolute error: 0.0633075768695183
Root relative squared error: 0.07437902660905547
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1135.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1653788434759145E-4
Coverage of cases: 100.0
Instances selection time: 1135.0
Test time: 1766.0
Accumulative iteration time: 42096.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.653778771474135E-4
Relative absolute error: 0.062209286544258606
Root relative squared error: 0.0730755754294827
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1133.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1104643272129303E-4
Coverage of cases: 100.0
Instances selection time: 1133.0
Test time: 1779.0
Accumulative iteration time: 43229.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5927422264874577E-4
Relative absolute error: 0.06119149962892569
Root relative squared error: 0.07185484452974915
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1133.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0595749814462845E-4
Coverage of cases: 100.0
Instances selection time: 1132.0
Test time: 1796.0
Accumulative iteration time: 44362.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.543500424033838E-4
Relative absolute error: 0.06042227611026677
Root relative squared error: 0.07087000848067676
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1130.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0211138055133386E-4
Coverage of cases: 100.0
Instances selection time: 1130.0
Test time: 1814.0
Accumulative iteration time: 45492.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4823601654915373E-4
Relative absolute error: 0.059426737037163786
Root relative squared error: 0.06964720330983075
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1143.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9713368518581893E-4
Coverage of cases: 100.0
Instances selection time: 1143.0
Test time: 1834.0
Accumulative iteration time: 46635.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4225624437926903E-4
Relative absolute error: 0.058470838258266405
Root relative squared error: 0.06845124887585381
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1124.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9235419129133204E-4
Coverage of cases: 100.0
Instances selection time: 1124.0
Test time: 1843.0
Accumulative iteration time: 47759.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3663802492889646E-4
Relative absolute error: 0.05752876152464749
Root relative squared error: 0.06732760498577929
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1123.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8764380762323746E-4
Coverage of cases: 100.0
Instances selection time: 1123.0
Test time: 1858.0
Accumulative iteration time: 48882.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3111817068260936E-4
Relative absolute error: 0.05661798768340068
Root relative squared error: 0.06622363413652187
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1119.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.830899384170034E-4
Coverage of cases: 100.0
Instances selection time: 1119.0
Test time: 1874.0
Accumulative iteration time: 50001.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Sun Oct 08 03.59.37 EEST 2017