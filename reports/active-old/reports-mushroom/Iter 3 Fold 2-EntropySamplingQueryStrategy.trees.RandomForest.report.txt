Sun Oct 08 03.14.53 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.14.53 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.77597242737568
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 0.9999953879375634
Root mean squared error: 0.06130529187420028
Relative absolute error: 3.2645349405748614
Root relative squared error: 12.261058374840056
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9999941857233846
Mean absolute error: 0.016322674702874306
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 11.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 55.00984736582964
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999998786299359
Root mean squared error: 0.055335653183134687
Relative absolute error: 3.1037861704918672
Root relative squared error: 11.067130636626937
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9999997573796393
Mean absolute error: 0.015518930852459336
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 10.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 54.43131462333826
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.039392570106145754
Relative absolute error: 2.254797549466098
Root relative squared error: 7.878514021229151
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 17.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01127398774733049
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 53.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 53.188084687346134
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.033269163758370426
Relative absolute error: 1.712533549332387
Root relative squared error: 6.653832751674085
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008562667746661935
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 69.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 53.4711964549483
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03376355737830274
Relative absolute error: 1.7280789900674343
Root relative squared error: 6.752711475660549
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008640394950337171
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 87.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 52.5726243229936
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02850080714700849
Relative absolute error: 1.2772560458817734
Root relative squared error: 5.700161429401699
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006386280229408867
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 103.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 53.64352535696701
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03585755900669169
Relative absolute error: 1.8521156999411321
Root relative squared error: 7.1715118013383385
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00926057849970566
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 122.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 53.4711964549483
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.035461860840101576
Relative absolute error: 1.7744403549553631
Root relative squared error: 7.092372168020315
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008872201774776815
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 140.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 51.46479566715904
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01979979839133507
Relative absolute error: 0.7263485178634441
Root relative squared error: 3.9599596782670137
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 16.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0036317425893172208
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 156.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 52.5233874938454
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.031142400071328107
Relative absolute error: 1.2568466845018202
Root relative squared error: 6.228480014265622
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006284233422509102
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 174.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 51.37863121614968
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017094889165683053
Relative absolute error: 0.602591510224067
Root relative squared error: 3.4189778331366103
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003012957551120335
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 193.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 51.317085179714425
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017688346558222094
Relative absolute error: 0.5402688378285723
Root relative squared error: 3.537669311644419
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002701344189142861
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 212.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 53.17577548005909
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03025381271010192
Relative absolute error: 1.5790505048334094
Root relative squared error: 6.050762542020384
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007895252524167047
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 231.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 51.24322993599212
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018646427972495244
Relative absolute error: 0.5807697330304219
Root relative squared error: 3.729285594499049
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0029038486651521093
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 250.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 51.563269325455444
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.023094631262756193
Relative absolute error: 0.8588935289878987
Root relative squared error: 4.618926252551239
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004294467644939494
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 269.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 51.76021664204825
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019906852200553323
Relative absolute error: 0.6142242797456258
Root relative squared error: 3.9813704401106644
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003071121398728129
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 23.0
Accumulative iteration time: 287.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 11.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.57853274249138
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012378600767110592
Relative absolute error: 0.26590340551507163
Root relative squared error: 2.4757201534221185
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 18.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0013295170275753581
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 305.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 52.17872968980797
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025904384431386648
Relative absolute error: 1.1393703206061943
Root relative squared error: 5.180876886277329
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005696851603030971
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 325.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 52.36336779911374
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.020333131014666085
Relative absolute error: 0.9052968417260955
Root relative squared error: 4.066626202933217
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.004526484208630477
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 344.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.935499753815854
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016285466218336368
Relative absolute error: 0.4876337877351272
Root relative squared error: 3.2570932436672737
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002438168938675636
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 364.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 52.72033481043821
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.025624217890123216
Relative absolute error: 1.2908325070069044
Root relative squared error: 5.1248435780246435
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.006454162535034522
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 387.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.48005908419498
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014060300222314167
Relative absolute error: 0.2699344787324277
Root relative squared error: 2.812060044462833
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0013496723936621386
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 408.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 51.30477597242738
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018834341945634287
Relative absolute error: 0.6130959384925426
Root relative squared error: 3.7668683891268575
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0030654796924627133
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 427.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 13.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.258493353028065
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01814594438997052
Relative absolute error: 0.2548658272317639
Root relative squared error: 3.6291888779941037
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0012743291361588193
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 16.0
Accumulative iteration time: 458.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 16.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 51.36632200886263
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02115488189069249
Relative absolute error: 0.6558266626200567
Root relative squared error: 4.2309763781384975
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0032791333131002836
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 480.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 51.61250615460364
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.019242752760925446
Relative absolute error: 0.6863805598884934
Root relative squared error: 3.848550552185089
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003431902799442467
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 505.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 19.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.54160512063023
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01606127937798224
Relative absolute error: 0.3426787252426984
Root relative squared error: 3.212255875596448
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001713393626213492
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 528.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 51.23092072870507
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.023060004441762795
Relative absolute error: 0.6597716106773082
Root relative squared error: 4.612000888352559
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.003298858053386541
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 549.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 15.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.67700640078779
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012293106980712513
Relative absolute error: 0.277664790010404
Root relative squared error: 2.4586213961425027
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00138832395005202
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 569.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.56622353520433
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01438791003892749
Relative absolute error: 0.3338952844993483
Root relative squared error: 2.877582007785498
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0016694764224967416
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 591.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 51.41555883801083
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017885904570800158
Relative absolute error: 0.6069538115802878
Root relative squared error: 3.5771809141600315
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0030347690579014394
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 613.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.67700640078779
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014951645579652421
Relative absolute error: 0.3914907386169412
Root relative squared error: 2.9903291159304843
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001957453693084706
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 634.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.65238798621369
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012961293765985728
Relative absolute error: 0.33496600634904267
Root relative squared error: 2.5922587531971457
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0016748300317452132
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 657.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.615460364352536
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01520203573213928
Relative absolute error: 0.37078707737624955
Root relative squared error: 3.040407146427856
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0018539353868812479
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 679.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.48005908419498
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011723419787498105
Relative absolute error: 0.23559868720178043
Root relative squared error: 2.344683957499621
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011779934360089022
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 699.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.83702609551945
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.015294049980372747
Relative absolute error: 0.3894718899221761
Root relative squared error: 3.0588099960745496
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 21.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019473594496108805
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 720.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.19694731659281
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.007562898305202858
Relative absolute error: 0.09105862509118898
Root relative squared error: 1.5125796610405715
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.552931254559449E-4
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 742.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.393894633185624
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017694723656049915
Relative absolute error: 0.26346703585514847
Root relative squared error: 3.538944731209983
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0013173351792757422
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 762.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 15.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.81240768094535
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013984743887341597
Relative absolute error: 0.3911375328396598
Root relative squared error: 2.7969487774683195
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001955687664198299
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 785.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.8493353028065
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02001381201076373
Relative absolute error: 0.412540279762704
Root relative squared error: 4.0027624021527455
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00206270139881352
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 808.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 17.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.418513047759724
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.008758049158762864
Relative absolute error: 0.1598594089253574
Root relative squared error: 1.7516098317525728
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 22.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.992970446267871E-4
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 830.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 16.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.664697193500736
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.011452925249890052
Relative absolute error: 0.2857822272065429
Root relative squared error: 2.2905850499780103
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0014289111360327143
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 854.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.369276218611525
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016881033192044915
Relative absolute error: 0.23057459800443553
Root relative squared error: 3.376206638408983
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0011528729900221776
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 874.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 16.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.68931560807484
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01442942842451057
Relative absolute error: 0.3346819270039957
Root relative squared error: 2.885885684902114
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0016734096350199783
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 897.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 51.02166420482521
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02057444674422854
Relative absolute error: 0.5041193919881858
Root relative squared error: 4.114889348845708
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0025205969599409288
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 922.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 20.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.33234859675037
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013090135698887426
Relative absolute error: 0.19427787049648212
Root relative squared error: 2.6180271397774852
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.713893524824106E-4
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 945.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 18.0
		
Time end:Sun Oct 08 03.14.59 EEST 2017