Sun Oct 08 04.21.19 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sun Oct 08 04.21.19 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 35.0
Accumulative iteration time: 181.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 163.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 33.0
Accumulative iteration time: 387.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 188.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 245.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 33.0
Accumulative iteration time: 632.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 227.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 197.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 18.0
Test time: 33.0
Accumulative iteration time: 829.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 179.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 33.0
Accumulative iteration time: 1002.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 156.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 205.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 33.0
Accumulative iteration time: 1207.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 188.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 224.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 17.0
Test time: 33.0
Accumulative iteration time: 1431.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 207.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 179.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 33.0
Accumulative iteration time: 1610.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 163.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 192.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 1802.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 176.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 1974.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 156.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 34.0
Accumulative iteration time: 2149.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 159.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 16.0
Test time: 35.0
Accumulative iteration time: 2339.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 174.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 2520.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 166.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 185.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 2705.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 170.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 229.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 2934.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 214.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 211.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 3145.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 196.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 212.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 3357.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 197.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 246.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 3603.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 231.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9997446373850868
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 230.0
Weighted AreaUnderPRC: 0.9996264164492228
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 3833.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 216.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 233.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 33.0
Accumulative iteration time: 4066.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 219.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 210.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 4276.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 196.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 247.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 33.0
Accumulative iteration time: 4523.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 233.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 401.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 4924.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 387.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 223.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 33.0
Accumulative iteration time: 5147.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 209.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 247.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 33.0
Accumulative iteration time: 5394.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 233.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 208.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 5602.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 195.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 238.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 5840.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 225.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 253.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 6093.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 240.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 260.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 6353.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 248.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 260.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 6613.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 248.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 254.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 6867.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 242.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 287.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 7154.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 275.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 276.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 7430.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 264.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 277.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 7707.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 265.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 261.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 7968.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 249.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 288.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 8256.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 276.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 264.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 32.0
Accumulative iteration time: 8520.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 253.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 283.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 32.0
Accumulative iteration time: 8803.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 272.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 286.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 30.0
Accumulative iteration time: 9089.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 273.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 322.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 30.0
Accumulative iteration time: 9411.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 309.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 310.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 30.0
Accumulative iteration time: 9721.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 297.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 444.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 29.0
Accumulative iteration time: 10165.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 431.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 349.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 30.0
Accumulative iteration time: 10514.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 339.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 319.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 30.0
Accumulative iteration time: 10833.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 309.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 285.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 29.0
Accumulative iteration time: 11118.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 275.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 316.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 30.0
Accumulative iteration time: 11434.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 307.0
		
Time end:Sun Oct 08 04.21.37 EEST 2017