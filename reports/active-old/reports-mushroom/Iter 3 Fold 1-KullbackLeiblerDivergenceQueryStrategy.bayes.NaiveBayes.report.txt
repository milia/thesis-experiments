Sun Oct 08 03.15.32 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 03.15.32 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 53.32348596750369
Incorrectly Classified Instances: 5.711472181191532
Correctly Classified Instances: 94.28852781880848
Weighted Precision: 0.9480615754024382
Weighted AreaUnderROC: 0.9954568757597766
Root mean squared error: 0.21812523608290257
Relative absolute error: 11.706440690779777
Root relative squared error: 43.62504721658051
Weighted TruePositiveRate: 0.9428852781880847
Weighted MatthewsCorrelation: 0.890563501654853
Weighted FMeasure: 0.942600144690341
Iteration time: 53.0
Weighted AreaUnderPRC: 0.9955998680366233
Mean absolute error: 0.05853220345389889
Coverage of cases: 97.61201378631216
Instances selection time: 48.0
Test time: 42.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9428852781880847
Weighted FalsePositiveRate: 0.06119633121418022
Kappa statistic: 0.8851710741219654
Training time: 5.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 53.01575578532743
Incorrectly Classified Instances: 5.957656326932546
Correctly Classified Instances: 94.04234367306745
Weighted Precision: 0.9456564781458564
Weighted AreaUnderROC: 0.9952515176112818
Root mean squared error: 0.2226121324932394
Relative absolute error: 11.928591418774555
Root relative squared error: 44.52242649864788
Weighted TruePositiveRate: 0.9404234367306745
Weighted MatthewsCorrelation: 0.8856778882731512
Weighted FMeasure: 0.9401216752894717
Iteration time: 45.0
Weighted AreaUnderPRC: 0.9953985255148046
Mean absolute error: 0.059642957093872774
Coverage of cases: 97.21811915312654
Instances selection time: 43.0
Test time: 45.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9404234367306745
Weighted FalsePositiveRate: 0.06369998182013885
Kappa statistic: 0.8802172895829677
Training time: 2.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 52.75726243229936
Incorrectly Classified Instances: 6.056129985228951
Correctly Classified Instances: 93.94387001477105
Weighted Precision: 0.944182796530841
Weighted AreaUnderROC: 0.9954277469443874
Root mean squared error: 0.22268422168127242
Relative absolute error: 11.78025807656696
Root relative squared error: 44.53684433625448
Weighted TruePositiveRate: 0.9394387001477105
Weighted MatthewsCorrelation: 0.8832260822660241
Weighted FMeasure: 0.9391536926340399
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9955673913162516
Mean absolute error: 0.05890129038283481
Coverage of cases: 97.16888232397834
Instances selection time: 39.0
Test time: 41.0
Accumulative iteration time: 139.0
Weighted Recall: 0.9394387001477105
Weighted FalsePositiveRate: 0.06451006574541435
Kappa statistic: 0.878259068569521
Training time: 2.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 52.90497291974397
Incorrectly Classified Instances: 5.834564254062038
Correctly Classified Instances: 94.16543574593796
Weighted Precision: 0.9461131526087289
Weighted AreaUnderROC: 0.9958331229585555
Root mean squared error: 0.21657891976152338
Relative absolute error: 11.353275658170249
Root relative squared error: 43.31578395230468
Weighted TruePositiveRate: 0.9416543574593796
Weighted MatthewsCorrelation: 0.8873949112233199
Weighted FMeasure: 0.9413940024509729
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9959562805147834
Mean absolute error: 0.056766378290851245
Coverage of cases: 97.56277695716396
Instances selection time: 34.0
Test time: 42.0
Accumulative iteration time: 175.0
Weighted Recall: 0.9416543574593796
Weighted FalsePositiveRate: 0.06216463604736999
Kappa statistic: 0.8827276069485331
Training time: 2.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 53.052683407188574
Incorrectly Classified Instances: 5.5145248645987195
Correctly Classified Instances: 94.48547513540127
Weighted Precision: 0.9488769894012883
Weighted AreaUnderROC: 0.9960190618967908
Root mean squared error: 0.20729672367135785
Relative absolute error: 10.659513638701183
Root relative squared error: 41.45934473427157
Weighted TruePositiveRate: 0.9448547513540128
Weighted MatthewsCorrelation: 0.8933918675169611
Weighted FMeasure: 0.9446291197002842
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9961379173894916
Mean absolute error: 0.053297568193505916
Coverage of cases: 98.07976366322009
Instances selection time: 30.0
Test time: 41.0
Accumulative iteration time: 208.0
Weighted Recall: 0.9448547513540128
Weighted FalsePositiveRate: 0.05876104201298428
Kappa statistic: 0.8891819500726373
Training time: 3.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 53.04037419990153
Incorrectly Classified Instances: 5.145248645987198
Correctly Classified Instances: 94.8547513540128
Weighted Precision: 0.9518429857856832
Weighted AreaUnderROC: 0.9960316843834595
Root mean squared error: 0.19980513674326114
Relative absolute error: 10.110073609334481
Root relative squared error: 39.96102734865223
Weighted TruePositiveRate: 0.948547513540128
Weighted MatthewsCorrelation: 0.900092895972869
Weighted FMeasure: 0.9483680992244815
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9961516432275445
Mean absolute error: 0.05055036804667241
Coverage of cases: 98.1536189069424
Instances selection time: 26.0
Test time: 41.0
Accumulative iteration time: 236.0
Weighted Recall: 0.948547513540128
Weighted FalsePositiveRate: 0.05472204563425681
Kappa statistic: 0.8966377229856204
Training time: 2.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 52.95420974889217
Incorrectly Classified Instances: 5.1698670605613
Correctly Classified Instances: 94.8301329394387
Weighted Precision: 0.9512566229423942
Weighted AreaUnderROC: 0.9964508965849376
Root mean squared error: 0.1988196459886458
Relative absolute error: 10.021843931675653
Root relative squared error: 39.76392919772916
Weighted TruePositiveRate: 0.948301329394387
Weighted MatthewsCorrelation: 0.899268069475201
Weighted FMeasure: 0.948135048860626
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9965514365740085
Mean absolute error: 0.050109219658378265
Coverage of cases: 98.22747415066469
Instances selection time: 45.0
Test time: 47.0
Accumulative iteration time: 284.0
Weighted Recall: 0.948301329394387
Weighted FalsePositiveRate: 0.054809386424723695
Kappa statistic: 0.8961597792065382
Training time: 3.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 52.96651895617922
Incorrectly Classified Instances: 5.022156573116692
Correctly Classified Instances: 94.97784342688331
Weighted Precision: 0.9528221172407335
Weighted AreaUnderROC: 0.9967385436369074
Root mean squared error: 0.19684191106791193
Relative absolute error: 9.874587864528818
Root relative squared error: 39.368382213582386
Weighted TruePositiveRate: 0.9497784342688331
Weighted MatthewsCorrelation: 0.9023169454592884
Weighted FMeasure: 0.9496139278977872
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9968259219187041
Mean absolute error: 0.04937293932264409
Coverage of cases: 98.2520925652388
Instances selection time: 17.0
Test time: 40.0
Accumulative iteration time: 304.0
Weighted Recall: 0.9497784342688331
Weighted FalsePositiveRate: 0.05336390015510642
Kappa statistic: 0.8991230568983124
Training time: 3.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 53.003446578040375
Incorrectly Classified Instances: 4.99753815854259
Correctly Classified Instances: 95.0024618414574
Weighted Precision: 0.9531080173651707
Weighted AreaUnderROC: 0.9969074907661655
Root mean squared error: 0.19646280767735988
Relative absolute error: 9.83468706434339
Root relative squared error: 39.292561535471975
Weighted TruePositiveRate: 0.9500246184145741
Weighted MatthewsCorrelation: 0.902849431742585
Weighted FMeasure: 0.949859428045221
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9969879995799925
Mean absolute error: 0.049173435321716946
Coverage of cases: 98.2520925652388
Instances selection time: 13.0
Test time: 41.0
Accumulative iteration time: 320.0
Weighted Recall: 0.9500246184145741
Weighted FalsePositiveRate: 0.05313479912974478
Kappa statistic: 0.8996157675311999
Training time: 3.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 53.01575578532743
Incorrectly Classified Instances: 4.899064500246184
Correctly Classified Instances: 95.10093549975382
Weighted Precision: 0.9536015184766612
Weighted AreaUnderROC: 0.9969706031995089
Root mean squared error: 0.19364835947554138
Relative absolute error: 9.654276269012383
Root relative squared error: 38.729671895108275
Weighted TruePositiveRate: 0.9510093549975381
Weighted MatthewsCorrelation: 0.9043465027790387
Weighted FMeasure: 0.9508673188565004
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9970467991603489
Mean absolute error: 0.04827138134506192
Coverage of cases: 98.301329394387
Instances selection time: 15.0
Test time: 82.0
Accumulative iteration time: 342.0
Weighted Recall: 0.9510093549975381
Weighted FalsePositiveRate: 0.051899434499784965
Kappa statistic: 0.9016182627256752
Training time: 7.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 53.04037419990153
Incorrectly Classified Instances: 4.923682914820286
Correctly Classified Instances: 95.07631708517971
Weighted Precision: 0.9533874350746453
Weighted AreaUnderROC: 0.9970443961984954
Root mean squared error: 0.1926103152366087
Relative absolute error: 9.612193453553655
Root relative squared error: 38.522063047321744
Weighted TruePositiveRate: 0.9507631708517972
Weighted MatthewsCorrelation: 0.9038839690104676
Weighted FMeasure: 0.9506190322715365
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9971165701528171
Mean absolute error: 0.048060967267768276
Coverage of cases: 98.3259478089611
Instances selection time: 4.0
Test time: 41.0
Accumulative iteration time: 350.0
Weighted Recall: 0.9507631708517972
Weighted FalsePositiveRate: 0.0521639755838703
Kappa statistic: 0.9011221251294396
Training time: 4.0
		
Time end:Sun Oct 08 03.15.34 EEST 2017