Sun Oct 08 04.43.55 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.43.55 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.77597242737568
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982828499228874
Weighted AreaUnderROC: 0.9999953879375634
Root mean squared error: 0.06130529187420028
Relative absolute error: 3.2645349405748614
Root relative squared error: 12.261058374840056
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965553244703538
Weighted FMeasure: 0.9982768127217427
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9999941857233846
Mean absolute error: 0.016322674702874306
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.0016037071775315311
Kappa statistic: 0.9965493916109222
Training time: 10.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 53.163466272772034
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.9999970871184611
Root mean squared error: 0.043646008198604015
Relative absolute error: 1.8214597249878735
Root relative squared error: 8.729201639720802
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9999956382967835
Mean absolute error: 0.009107298624939367
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 12.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 53.24963072378139
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03441827963223595
Relative absolute error: 1.6514269749282082
Root relative squared error: 6.883655926447189
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008257134874641041
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 55.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 53.08961102904973
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03869757631752634
Relative absolute error: 1.699031781596717
Root relative squared error: 7.739515263505268
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 23.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.008495158907983585
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 78.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 17.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 52.6957163958641
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539415225314
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.035597871354425555
Relative absolute error: 1.4831421361646955
Root relative squared error: 7.119574270885111
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071249983162
Weighted FMeasure: 0.9997538180204761
Iteration time: 24.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007415710680823477
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 102.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.2910102536164733E-4
Kappa statistic: 0.9995070035354472
Training time: 19.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 52.129492860659774
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977944804187858
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0429130916630128
Relative absolute error: 1.4371559121449113
Root relative squared error: 8.582618332602559
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955734577461355
Weighted FMeasure: 0.9977845084484809
Iteration time: 26.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.007185779560724557
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 128.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.002061909228254826
Kappa statistic: 0.9955636607043825
Training time: 22.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 51.41555883801083
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018687834489406606
Relative absolute error: 0.6090364916908739
Root relative squared error: 3.737566897881321
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 32.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0030451824584543695
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 160.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 28.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.775480059084195
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.01892603240459885
Relative absolute error: 0.4556403953370977
Root relative squared error: 3.78520648091977
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 35.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0022782019766854887
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 14.0
Accumulative iteration time: 195.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 32.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.147710487444606
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.008568288822976578
Relative absolute error: 0.09744362179985998
Root relative squared error: 1.7136577645953155
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.8721810899929986E-4
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 226.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 29.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.62776957163959
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.015066793931073314
Relative absolute error: 0.32786639412771695
Root relative squared error: 3.013358786214663
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 40.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0016393319706385849
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 266.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 38.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.910881339241755
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.017652038398722696
Relative absolute error: 0.44862367513376045
Root relative squared error: 3.530407679744539
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 44.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002243118375668802
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 310.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 43.0
		
Time end:Sun Oct 08 04.43.56 EEST 2017