Sun Oct 08 03.41.19 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sun Oct 08 03.41.19 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.098473658296406
Incorrectly Classified Instances: 0.19694731659281142
Correctly Classified Instances: 99.80305268340719
Weighted Precision: 0.998030940633535
Weighted AreaUnderROC: 0.9984560514143012
Root mean squared error: 0.04332309762206794
Relative absolute error: 0.6053338913768155
Root relative squared error: 8.664619524413588
Weighted TruePositiveRate: 0.9980305268340719
Weighted MatthewsCorrelation: 0.9960563028398949
Weighted FMeasure: 0.9980304914556168
Iteration time: 899.0
Weighted AreaUnderPRC: 0.9979221915156049
Mean absolute error: 0.0030266694568840775
Coverage of cases: 99.85228951255539
Instances selection time: 899.0
Test time: 1138.0
Accumulative iteration time: 899.0
Weighted Recall: 0.9980305268340719
Weighted FalsePositiveRate: 0.0020100084965116376
Kappa statistic: 0.9960558186142641
Training time: 0.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.141161170096795E-4
Relative absolute error: 0.14077442217394884
Root relative squared error: 0.16282322340193592
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 900.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.038721108697442E-4
Coverage of cases: 100.0
Instances selection time: 900.0
Test time: 1149.0
Accumulative iteration time: 1799.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.940509478359092E-4
Relative absolute error: 0.13728516050698042
Root relative squared error: 0.15881018956718185
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 918.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.864258025349021E-4
Coverage of cases: 100.0
Instances selection time: 918.0
Test time: 1156.0
Accumulative iteration time: 2717.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.742420778318841E-4
Relative absolute error: 0.13381353695778056
Root relative squared error: 0.15484841556637682
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 926.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.690676847889029E-4
Coverage of cases: 100.0
Instances selection time: 926.0
Test time: 1171.0
Accumulative iteration time: 3643.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.537432188944439E-4
Relative absolute error: 0.1302813163027815
Root relative squared error: 0.15074864377888877
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 939.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.514065815139075E-4
Coverage of cases: 100.0
Instances selection time: 939.0
Test time: 1188.0
Accumulative iteration time: 4582.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.348448210876297E-4
Relative absolute error: 0.12687434329365058
Root relative squared error: 0.14696896421752592
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 957.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.343717164682529E-4
Coverage of cases: 100.0
Instances selection time: 957.0
Test time: 1218.0
Accumulative iteration time: 5539.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.191922398914971E-4
Relative absolute error: 0.12420605903642054
Root relative squared error: 0.14383844797829942
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 956.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.210302951821027E-4
Coverage of cases: 100.0
Instances selection time: 956.0
Test time: 1545.0
Accumulative iteration time: 6495.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.018530320122393E-4
Relative absolute error: 0.12118769095611943
Root relative squared error: 0.14037060640244786
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 959.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.059384547805972E-4
Coverage of cases: 100.0
Instances selection time: 959.0
Test time: 1238.0
Accumulative iteration time: 7454.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.838256908575923E-4
Relative absolute error: 0.1180316657396777
Root relative squared error: 0.13676513817151845
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 967.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.901583286983885E-4
Coverage of cases: 100.0
Instances selection time: 967.0
Test time: 1259.0
Accumulative iteration time: 8421.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.685353706158486E-4
Relative absolute error: 0.1155485912106254
Root relative squared error: 0.13370707412316973
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 975.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.77742956053127E-4
Coverage of cases: 100.0
Instances selection time: 975.0
Test time: 1273.0
Accumulative iteration time: 9396.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.559892351919315E-4
Relative absolute error: 0.11355386471177606
Root relative squared error: 0.1311978470383863
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 980.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.677693235588803E-4
Coverage of cases: 100.0
Instances selection time: 980.0
Test time: 1288.0
Accumulative iteration time: 10376.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.417735205287399E-4
Relative absolute error: 0.1111379546071944
Root relative squared error: 0.12835470410574798
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 987.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.55689773035972E-4
Coverage of cases: 100.0
Instances selection time: 987.0
Test time: 1304.0
Accumulative iteration time: 11363.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.287658394132226E-4
Relative absolute error: 0.10887355749236877
Root relative squared error: 0.1257531678826445
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 993.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.443677874618438E-4
Coverage of cases: 100.0
Instances selection time: 993.0
Test time: 1320.0
Accumulative iteration time: 12356.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.141283267265947E-4
Relative absolute error: 0.10626402805655691
Root relative squared error: 0.12282566534531894
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1004.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.313201402827846E-4
Coverage of cases: 100.0
Instances selection time: 1004.0
Test time: 1347.0
Accumulative iteration time: 13360.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.991443904645345E-4
Relative absolute error: 0.10366241451573097
Root relative squared error: 0.1198288780929069
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1011.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.183120725786548E-4
Coverage of cases: 100.0
Instances selection time: 1011.0
Test time: 1355.0
Accumulative iteration time: 14371.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.858772700530945E-4
Relative absolute error: 0.10123679662757185
Root relative squared error: 0.1171754540106189
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1012.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.061839831378592E-4
Coverage of cases: 100.0
Instances selection time: 1012.0
Test time: 1365.0
Accumulative iteration time: 15383.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.716731073577262E-4
Relative absolute error: 0.09874706355370962
Root relative squared error: 0.11433462147154524
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1018.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.93735317768548E-4
Coverage of cases: 100.0
Instances selection time: 1018.0
Test time: 1383.0
Accumulative iteration time: 16401.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.599609609668472E-4
Relative absolute error: 0.09665972939876456
Root relative squared error: 0.11199219219336944
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1024.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.832986469938228E-4
Coverage of cases: 100.0
Instances selection time: 1024.0
Test time: 1400.0
Accumulative iteration time: 17425.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.473027019985504E-4
Relative absolute error: 0.09437211367784865
Root relative squared error: 0.10946054039971008
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1030.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.718605683892432E-4
Coverage of cases: 100.0
Instances selection time: 1030.0
Test time: 1412.0
Accumulative iteration time: 18455.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.348544373018488E-4
Relative absolute error: 0.09214600514740688
Root relative squared error: 0.10697088746036976
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1033.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.607300257370344E-4
Coverage of cases: 100.0
Instances selection time: 1033.0
Test time: 1428.0
Accumulative iteration time: 19488.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.236885323428509E-4
Relative absolute error: 0.0902027322477617
Root relative squared error: 0.10473770646857018
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1038.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.510136612388085E-4
Coverage of cases: 100.0
Instances selection time: 1038.0
Test time: 1445.0
Accumulative iteration time: 20526.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.120105168454958E-4
Relative absolute error: 0.08812298390172048
Root relative squared error: 0.10240210336909916
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1040.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.406149195086024E-4
Coverage of cases: 100.0
Instances selection time: 1040.0
Test time: 1464.0
Accumulative iteration time: 21566.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.011605009137142E-4
Relative absolute error: 0.08616486705199251
Root relative squared error: 0.10023210018274284
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1038.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.308243352599626E-4
Coverage of cases: 100.0
Instances selection time: 1038.0
Test time: 1504.0
Accumulative iteration time: 22604.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.906695825198955E-4
Relative absolute error: 0.08424669267648176
Root relative squared error: 0.0981339165039791
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1051.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.212334633824088E-4
Coverage of cases: 100.0
Instances selection time: 1051.0
Test time: 1506.0
Accumulative iteration time: 23655.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.7971228723272743E-4
Relative absolute error: 0.08227943380134144
Root relative squared error: 0.09594245744654549
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1048.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.113971690067072E-4
Coverage of cases: 100.0
Instances selection time: 1048.0
Test time: 1520.0
Accumulative iteration time: 24703.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.6860349476493193E-4
Relative absolute error: 0.08023198677572176
Root relative squared error: 0.09372069895298639
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1052.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.011599338786088E-4
Coverage of cases: 100.0
Instances selection time: 1052.0
Test time: 1536.0
Accumulative iteration time: 25755.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.601594869891937E-4
Relative absolute error: 0.07867886842113184
Root relative squared error: 0.09203189739783874
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1055.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.933943421056592E-4
Coverage of cases: 100.0
Instances selection time: 1054.0
Test time: 1551.0
Accumulative iteration time: 26810.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.512113300228268E-4
Relative absolute error: 0.07702970945994958
Root relative squared error: 0.09024226600456536
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1271.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8514854729974787E-4
Coverage of cases: 100.0
Instances selection time: 1271.0
Test time: 1564.0
Accumulative iteration time: 28081.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.436980654359848E-4
Relative absolute error: 0.07571900805815307
Root relative squared error: 0.08873961308719695
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1060.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.785950402907653E-4
Coverage of cases: 100.0
Instances selection time: 1060.0
Test time: 1584.0
Accumulative iteration time: 29141.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.352784762861882E-4
Relative absolute error: 0.07424764508925226
Root relative squared error: 0.08705569525723764
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1059.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.712382254462613E-4
Coverage of cases: 100.0
Instances selection time: 1059.0
Test time: 1603.0
Accumulative iteration time: 30200.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.278412690434016E-4
Relative absolute error: 0.07300804203560704
Root relative squared error: 0.08556825380868031
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1082.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.650402101780352E-4
Coverage of cases: 100.0
Instances selection time: 1082.0
Test time: 1619.0
Accumulative iteration time: 31282.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.209482147956816E-4
Relative absolute error: 0.07186810748979232
Root relative squared error: 0.08418964295913632
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1064.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5934053744896157E-4
Coverage of cases: 100.0
Instances selection time: 1064.0
Test time: 1629.0
Accumulative iteration time: 32346.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.1430182502143485E-4
Relative absolute error: 0.07079452658376585
Root relative squared error: 0.08286036500428696
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1066.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5397263291882925E-4
Coverage of cases: 100.0
Instances selection time: 1066.0
Test time: 1651.0
Accumulative iteration time: 33412.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.066763369593177E-4
Relative absolute error: 0.06947217310370493
Root relative squared error: 0.08133526739186353
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1065.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4736086551852464E-4
Coverage of cases: 100.0
Instances selection time: 1065.0
Test time: 1660.0
Accumulative iteration time: 34477.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.0073174785629326E-4
Relative absolute error: 0.0684679293483459
Root relative squared error: 0.08014634957125866
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1266.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.423396467417295E-4
Coverage of cases: 100.0
Instances selection time: 1266.0
Test time: 1671.0
Accumulative iteration time: 35743.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.9187816034058235E-4
Relative absolute error: 0.06699111655606227
Root relative squared error: 0.07837563206811647
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1066.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.349555827803113E-4
Coverage of cases: 100.0
Instances selection time: 1066.0
Test time: 1687.0
Accumulative iteration time: 36809.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8658104684468367E-4
Relative absolute error: 0.06609703707413826
Root relative squared error: 0.07731620936893674
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1066.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.304851853706913E-4
Coverage of cases: 100.0
Instances selection time: 1066.0
Test time: 1707.0
Accumulative iteration time: 37875.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.796632076027585E-4
Relative absolute error: 0.06492184686919689
Root relative squared error: 0.07593264152055171
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1077.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2460923434598447E-4
Coverage of cases: 100.0
Instances selection time: 1077.0
Test time: 1723.0
Accumulative iteration time: 38952.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.724880158967419E-4
Relative absolute error: 0.06366382554166225
Root relative squared error: 0.07449760317934838
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1068.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.183191277083112E-4
Coverage of cases: 100.0
Instances selection time: 1068.0
Test time: 1745.0
Accumulative iteration time: 40020.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.6609662088497527E-4
Relative absolute error: 0.06258504494233506
Root relative squared error: 0.07321932417699506
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1067.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.129252247116753E-4
Coverage of cases: 100.0
Instances selection time: 1067.0
Test time: 1759.0
Accumulative iteration time: 41087.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5937540293093666E-4
Relative absolute error: 0.061435171869197044
Root relative squared error: 0.07187508058618733
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1063.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0717585934598524E-4
Coverage of cases: 100.0
Instances selection time: 1063.0
Test time: 1773.0
Accumulative iteration time: 42150.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.537938047564416E-4
Relative absolute error: 0.060482726322412715
Root relative squared error: 0.07075876095128832
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1061.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0241363161206356E-4
Coverage of cases: 100.0
Instances selection time: 1061.0
Test time: 1787.0
Accumulative iteration time: 43211.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4769185795032923E-4
Relative absolute error: 0.05945607824596526
Root relative squared error: 0.06953837159006585
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1060.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.972803912298263E-4
Coverage of cases: 100.0
Instances selection time: 1060.0
Test time: 1792.0
Accumulative iteration time: 44271.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.414032511062883E-4
Relative absolute error: 0.058403700697606524
Root relative squared error: 0.06828065022125766
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1057.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9201850348803263E-4
Coverage of cases: 100.0
Instances selection time: 1057.0
Test time: 1804.0
Accumulative iteration time: 45328.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3527430950740553E-4
Relative absolute error: 0.05737320000927114
Root relative squared error: 0.0670548619014811
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1056.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.868660000463557E-4
Coverage of cases: 100.0
Instances selection time: 1056.0
Test time: 2149.0
Accumulative iteration time: 46384.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.2805791077621154E-4
Relative absolute error: 0.05613976258914083
Root relative squared error: 0.0656115821552423
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1097.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8069881294570415E-4
Coverage of cases: 100.0
Instances selection time: 1097.0
Test time: 1896.0
Accumulative iteration time: 47481.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Time end:Sun Oct 08 03.43.20 EEST 2017