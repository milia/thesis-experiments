Sun Oct 08 02.52.38 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sun Oct 08 02.52.38 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 220.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 25.0
Test time: 29.0
Accumulative iteration time: 220.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 195.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 207.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 23.0
Test time: 28.0
Accumulative iteration time: 427.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 184.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 204.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 22.0
Test time: 29.0
Accumulative iteration time: 631.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 182.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 220.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 22.0
Test time: 29.0
Accumulative iteration time: 851.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 198.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 389.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 44.0
Test time: 54.0
Accumulative iteration time: 1240.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 345.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 350.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 41.0
Test time: 58.0
Accumulative iteration time: 1590.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 309.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 391.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 44.0
Test time: 55.0
Accumulative iteration time: 1981.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 347.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 455.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 42.0
Test time: 53.0
Accumulative iteration time: 2436.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 413.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 270.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 2706.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 249.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 245.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 22.0
Test time: 28.0
Accumulative iteration time: 2951.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 223.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 208.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 3159.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 187.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 224.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 3383.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 203.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 222.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 3605.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 201.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 260.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 3865.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 239.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 259.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 22.0
Test time: 28.0
Accumulative iteration time: 4124.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 237.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 227.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 29.0
Accumulative iteration time: 4351.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 206.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 291.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 4642.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 271.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 209.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 4851.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 188.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 200.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 20.0
Test time: 38.0
Accumulative iteration time: 5051.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 180.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 233.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 5284.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 213.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 245.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 5529.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 225.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 241.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 20.0
Test time: 29.0
Accumulative iteration time: 5770.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 221.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 288.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 21.0
Test time: 28.0
Accumulative iteration time: 6058.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 267.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 244.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 6302.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 225.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.998467824310521
Root mean squared error: 0.03843312210120439
Relative absolute error: 0.29542097488921715
Root relative squared error: 7.686624420240879
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 318.0
Weighted AreaUnderPRC: 0.997761997281029
Mean absolute error: 0.0014771048744460858
Coverage of cases: 99.85228951255539
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 6620.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 299.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 294.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 6914.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 275.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 246.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 7160.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 227.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 242.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 7402.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 222.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 242.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 28.0
Accumulative iteration time: 7644.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 222.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 238.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 7882.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 219.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 245.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 8127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 226.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 474.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 55.0
Accumulative iteration time: 8601.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 434.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 506.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 39.0
Test time: 52.0
Accumulative iteration time: 9107.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 467.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 455.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 34.0
Test time: 56.0
Accumulative iteration time: 9562.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 421.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 475.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 39.0
Test time: 53.0
Accumulative iteration time: 10037.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 436.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 248.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 10285.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 229.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 255.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 19.0
Test time: 28.0
Accumulative iteration time: 10540.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 236.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 235.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 10775.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 217.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 225.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 11000.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 207.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 294.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 29.0
Accumulative iteration time: 11294.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 276.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 253.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 29.0
Accumulative iteration time: 11547.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 236.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 325.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 17.0
Test time: 29.0
Accumulative iteration time: 11872.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 308.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 267.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 12139.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 252.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 296.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 12435.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 281.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 272.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 32.0
Accumulative iteration time: 12707.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 257.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 276.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 32.0
Accumulative iteration time: 12983.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 262.0
		
Time end:Sun Oct 08 02.52.58 EEST 2017