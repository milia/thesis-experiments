Sun Oct 08 03.24.43 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 03.24.43 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 55.071393402264896
Incorrectly Classified Instances: 0.32003938946331856
Correctly Classified Instances: 99.67996061053668
Weighted Precision: 0.9968023378806495
Weighted AreaUnderROC: 0.9999649240514688
Root mean squared error: 0.06210533750439456
Relative absolute error: 3.213130927140041
Root relative squared error: 12.42106750087891
Weighted TruePositiveRate: 0.9967996061053668
Weighted MatthewsCorrelation: 0.9935933837510547
Weighted FMeasure: 0.9967994594590669
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9999620600814119
Mean absolute error: 0.016065654635700203
Coverage of cases: 100.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9967996061053668
Weighted FalsePositiveRate: 0.0032972738582146416
Kappa statistic: 0.9935903644993149
Training time: 10.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 54.20974889217134
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.9999163760258198
Root mean squared error: 0.05878844716359653
Relative absolute error: 2.772795206743396
Root relative squared error: 11.757689432719307
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9998905590798133
Mean absolute error: 0.01386397603371698
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 12.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 53.75430822255047
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.9998842129588273
Root mean squared error: 0.05233003779011428
Relative absolute error: 2.2529552722793866
Root relative squared error: 10.466007558022856
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9998498296701132
Mean absolute error: 0.011264776361396934
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 14.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 53.003446578040375
Incorrectly Classified Instances: 0.14771048744460857
Correctly Classified Instances: 99.85228951255539
Weighted Precision: 0.998527095423775
Weighted AreaUnderROC: 0.999909700672293
Root mean squared error: 0.05031934075979879
Relative absolute error: 2.0136324549452724
Root relative squared error: 10.063868151959758
Weighted TruePositiveRate: 0.9985228951255539
Weighted MatthewsCorrelation: 0.997046017253315
Weighted FMeasure: 0.9985228133666838
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9999106710985057
Mean absolute error: 0.010068162274726362
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 92.0
Weighted Recall: 0.9985228951255539
Weighted FalsePositiveRate: 0.0015872465045120349
Kappa statistic: 0.9970416542653733
Training time: 18.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 54.246676514032494
Incorrectly Classified Instances: 0.22156573116691286
Correctly Classified Instances: 99.77843426883308
Weighted Precision: 0.9977937799414331
Weighted AreaUnderROC: 0.9999458689514015
Root mean squared error: 0.05793554485380223
Relative absolute error: 2.749958958799809
Root relative squared error: 11.587108970760445
Weighted TruePositiveRate: 0.9977843426883308
Weighted MatthewsCorrelation: 0.9955720487411955
Weighted FMeasure: 0.9977841550896477
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9999462602993042
Mean absolute error: 0.013749794793999043
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 12.0
Accumulative iteration time: 120.0
Weighted Recall: 0.9977843426883308
Weighted FalsePositiveRate: 0.0023808697567680526
Kappa statistic: 0.9955622454615528
Training time: 20.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 53.37272279665189
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9999024184684457
Root mean squared error: 0.05565314232534786
Relative absolute error: 2.3532315464651132
Root relative squared error: 11.130628465069572
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9999036248497759
Mean absolute error: 0.011766157732325566
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 147.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 21.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 51.6494337764648
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.034713144548491635
Relative absolute error: 1.1880958890065008
Root relative squared error: 6.942628909698327
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9999990299971542
Mean absolute error: 0.005940479445032503
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 177.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 25.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 51.317085179714425
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02303778297475996
Relative absolute error: 0.7332237034995698
Root relative squared error: 4.607556594951992
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 37.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0036661185174978487
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 214.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 32.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.775480059084195
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016390381111422322
Relative absolute error: 0.3856153750419673
Root relative squared error: 3.278076222284464
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 35.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0019280768752098364
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 249.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 32.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 51.08321024126046
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.024335740413677528
Relative absolute error: 0.6502544803220545
Root relative squared error: 4.867148082735506
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 37.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0032512724016102723
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 286.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 35.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.76317085179714
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0185924776165739
Relative absolute error: 0.43538750073405763
Root relative squared error: 3.71849552331478
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 42.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002176937503670288
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 328.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 40.0
		
Time end:Sun Oct 08 03.24.44 EEST 2017