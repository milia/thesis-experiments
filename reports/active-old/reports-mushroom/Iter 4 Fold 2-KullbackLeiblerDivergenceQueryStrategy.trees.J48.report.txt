Sun Oct 08 03.24.32 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 03.24.32 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 12.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 7.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 27.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 9.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 38.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 8.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 47.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 3.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 59.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 8.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.09847365829640571
Correctly Classified Instances: 99.9015263417036
Weighted Precision: 0.999017131987402
Weighted AreaUnderROC: 0.9989785495403473
Root mean squared error: 0.03138051279001121
Relative absolute error: 0.19694731659281145
Root relative squared error: 6.276102558002242
Weighted TruePositiveRate: 0.999015263417036
Weighted MatthewsCorrelation: 0.9980297802902457
Weighted FMeasure: 0.9990152275591968
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9985070665589048
Mean absolute error: 9.847365829640572E-4
Coverage of cases: 99.9015263417036
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 75.0
Weighted Recall: 0.999015263417036
Weighted FalsePositiveRate: 0.0010581643363413566
Kappa statistic: 0.9980278394111678
Training time: 11.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 7.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 82.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 5.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 7.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 89.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 6.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 7.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 96.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 6.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 106.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 116.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.0
		
Time end:Sun Oct 08 03.24.33 EEST 2017