Sun Oct 08 03.24.27 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 03.24.27 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 9.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 4.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 3.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 37.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 3.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 4.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 5.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.02816487218443376
Relative absolute error: 0.31183325127195155
Root relative squared error: 5.6329744368867525
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0015591662563597577
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 5.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.028824841575903294
Relative absolute error: 0.29542097488921715
Root relative squared error: 5.764968315180659
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0014771048744460858
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 8.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.030815051830019195
Relative absolute error: 0.2743194766828445
Root relative squared error: 6.163010366003839
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.0013715973834142227
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 81.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 12.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.17232890201871
Incorrectly Classified Instances: 0.12309207287050714
Correctly Classified Instances: 99.8769079271295
Weighted Precision: 0.9987719975281197
Weighted AreaUnderROC: 0.9999945383471145
Root mean squared error: 0.03192494891723085
Relative absolute error: 0.2658788774002954
Root relative squared error: 6.384989783446169
Weighted TruePositiveRate: 0.9987690792712949
Weighted MatthewsCorrelation: 0.9975377868143344
Weighted FMeasure: 0.9987690228688719
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9999891150652402
Mean absolute error: 0.001329394387001477
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 91.0
Weighted Recall: 0.9987690792712949
Weighted FalsePositiveRate: 0.0013227054204266956
Kappa statistic: 0.9975347555766595
Training time: 9.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 101.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 10.0
		
Time end:Sun Oct 08 03.24.27 EEST 2017