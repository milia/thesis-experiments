Sun Oct 08 04.43.41 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.43.41 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.043082225504676
Incorrectly Classified Instances: 0.6179222058099458
Correctly Classified Instances: 99.38207779419005
Weighted Precision: 0.9939445593065912
Weighted AreaUnderROC: 0.995987360036042
Root mean squared error: 0.06730129075781485
Relative absolute error: 1.8613822080974007
Root relative squared error: 13.460258151562968
Weighted TruePositiveRate: 0.9938207779419004
Weighted MatthewsCorrelation: 0.9877450288351856
Weighted FMeasure: 0.9938176532475828
Iteration time: 5.6
Weighted AreaUnderPRC: 0.9939429232798188
Mean absolute error: 0.009306911040487004
Coverage of cases: 99.43870014771048
Instances selection time: 3.2
Test time: 7.9
Accumulative iteration time: 5.6
Weighted Recall: 0.9938207779419004
Weighted FalsePositiveRate: 0.0065442930519880455
Kappa statistic: 0.9876166572292581
Training time: 2.4
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.11078286558346
Incorrectly Classified Instances: 0.2781880846873461
Correctly Classified Instances: 99.72181191531266
Weighted Precision: 0.9972398704558018
Weighted AreaUnderROC: 0.9988387190894722
Root mean squared error: 0.045823621372719406
Relative absolute error: 0.5743259902937454
Root relative squared error: 9.164724274543882
Weighted TruePositiveRate: 0.9972181191531266
Weighted MatthewsCorrelation: 0.9944510378749418
Weighted FMeasure: 0.997218177621844
Iteration time: 5.7
Weighted AreaUnderPRC: 0.9982299024350316
Mean absolute error: 0.0028716299514687264
Coverage of cases: 99.83751846381092
Instances selection time: 2.5
Test time: 8.2
Accumulative iteration time: 11.3
Weighted Recall: 0.9972181191531266
Weighted FalsePositiveRate: 0.0027589538684603355
Kappa statistic: 0.9944294766438505
Training time: 3.2
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.1944854751354
Incorrectly Classified Instances: 0.2314130969965534
Correctly Classified Instances: 99.76858690300347
Weighted Precision: 0.9977006977593268
Weighted AreaUnderROC: 0.999501618105695
Root mean squared error: 0.039454494685451556
Relative absolute error: 0.4911373707533235
Root relative squared error: 7.89089893709031
Weighted TruePositiveRate: 0.9976858690300345
Weighted MatthewsCorrelation: 0.9953810493234817
Weighted FMeasure: 0.997686061615578
Iteration time: 5.9
Weighted AreaUnderPRC: 0.9992460053237817
Mean absolute error: 0.0024556868537666176
Coverage of cases: 99.9507631708518
Instances selection time: 2.6
Test time: 7.5
Accumulative iteration time: 17.2
Weighted Recall: 0.9976858690300345
Weighted FalsePositiveRate: 0.002238605779336345
Kappa statistic: 0.9953666523416251
Training time: 3.3
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.203101920236335
Incorrectly Classified Instances: 0.26834071885770555
Correctly Classified Instances: 99.7316592811423
Weighted Precision: 0.9973347743779997
Weighted AreaUnderROC: 0.9996027436431216
Root mean squared error: 0.04028609407087033
Relative absolute error: 0.49986252054198876
Root relative squared error: 8.057218814174066
Weighted TruePositiveRate: 0.997316592811423
Weighted MatthewsCorrelation: 0.9946450598104877
Weighted FMeasure: 0.9973168602008953
Iteration time: 5.6
Weighted AreaUnderPRC: 0.9993937831493482
Mean absolute error: 0.0024993126027099436
Coverage of cases: 99.96061053668144
Instances selection time: 1.8
Test time: 7.1
Accumulative iteration time: 22.8
Weighted Recall: 0.997316592811423
Weighted FalsePositiveRate: 0.002546817258655124
Kappa statistic: 0.994627466843054
Training time: 3.8
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.15386509108813
Incorrectly Classified Instances: 0.38158542589857214
Correctly Classified Instances: 99.61841457410142
Weighted Precision: 0.996239645821599
Weighted AreaUnderROC: 0.9984303452347201
Root mean squared error: 0.048297544493744064
Relative absolute error: 0.8057864997303709
Root relative squared error: 9.659508898748811
Weighted TruePositiveRate: 0.9961841457410143
Weighted MatthewsCorrelation: 0.9924132028348813
Weighted FMeasure: 0.9961834403958602
Iteration time: 5.7
Weighted AreaUnderPRC: 0.9976439876241784
Mean absolute error: 0.004028932498651854
Coverage of cases: 99.82028557360906
Instances selection time: 1.6
Test time: 6.9
Accumulative iteration time: 28.5
Weighted Recall: 0.9961841457410143
Weighted FalsePositiveRate: 0.0037637062454476842
Kappa statistic: 0.9923568584836413
Training time: 4.1
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.14894140817331
Incorrectly Classified Instances: 0.39635647464303303
Correctly Classified Instances: 99.60364352535697
Weighted Precision: 0.9960927739024967
Weighted AreaUnderROC: 0.9985045873029437
Root mean squared error: 0.04997951712948376
Relative absolute error: 0.8199271258907611
Root relative squared error: 9.995903425896751
Weighted TruePositiveRate: 0.9960364352535697
Weighted MatthewsCorrelation: 0.9921182079076439
Weighted FMeasure: 0.9960357131520675
Iteration time: 6.2
Weighted AreaUnderPRC: 0.9977511484407602
Mean absolute error: 0.004099635629453806
Coverage of cases: 99.8055145248646
Instances selection time: 1.6
Test time: 7.0
Accumulative iteration time: 34.7
Weighted Recall: 0.9960364352535697
Weighted FalsePositiveRate: 0.003922430895898888
Kappa statistic: 0.992060992452814
Training time: 4.6
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.03077301821763
Incorrectly Classified Instances: 0.05416051206302315
Correctly Classified Instances: 99.94583948793698
Weighted Precision: 0.9994620387950718
Weighted AreaUnderROC: 0.9999994174236922
Root mean squared error: 0.008132145806307129
Relative absolute error: 0.09026752010503858
Root relative squared error: 1.626429161261426
Weighted TruePositiveRate: 0.9994583948793696
Weighted MatthewsCorrelation: 0.9989191646526013
Weighted FMeasure: 0.9994584521141681
Iteration time: 6.7
Weighted AreaUnderPRC: 0.9999988405387757
Mean absolute error: 4.5133760052519295E-4
Coverage of cases: 100.0
Instances selection time: 1.4
Test time: 6.8
Accumulative iteration time: 41.4
Weighted Recall: 0.9994583948793696
Weighted FalsePositiveRate: 5.040222557956242E-4
Kappa statistic: 0.9989156435853908
Training time: 5.3
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.01846381093058
Incorrectly Classified Instances: 0.06646971935007387
Correctly Classified Instances: 99.93353028064993
Weighted Precision: 0.9993385673522084
Weighted AreaUnderROC: 0.9997148288973385
Root mean squared error: 0.010939395466130283
Relative absolute error: 0.10647464303298868
Root relative squared error: 2.187879093226057
Weighted TruePositiveRate: 0.9993353028064993
Weighted MatthewsCorrelation: 0.9986722686913477
Weighted FMeasure: 0.9993353557330898
Iteration time: 7.7
Weighted AreaUnderPRC: 0.9995639771973994
Mean absolute error: 5.323732151649434E-4
Coverage of cases: 99.97045790251107
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 49.1
Weighted Recall: 0.9993353028064993
Weighted FalsePositiveRate: 6.185727684764478E-4
Kappa statistic: 0.9986691139287137
Training time: 6.7
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 8.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 0.8
Test time: 6.9
Accumulative iteration time: 57.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 7.9
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 0.7
Test time: 7.0
Accumulative iteration time: 67.8
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.3
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.0
Relative absolute error: 0.0
Root relative squared error: 0.0
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 10.3
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0
Coverage of cases: 100.0
Instances selection time: 0.5
Test time: 6.9
Accumulative iteration time: 78.1
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 9.8
		
Time end:Sun Oct 08 04.43.49 EEST 2017