Sun Oct 08 04.43.49 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.43.49 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 53.931560807484004
Incorrectly Classified Instances: 0.22402757262432296
Correctly Classified Instances: 99.77597242737569
Weighted Precision: 0.9977686145304585
Weighted AreaUnderROC: 0.9997199871250636
Root mean squared error: 0.0559449901441547
Relative absolute error: 2.5997857653040493
Root relative squared error: 11.188998028830941
Weighted TruePositiveRate: 0.9977597242737566
Weighted MatthewsCorrelation: 0.9955224612171161
Weighted FMeasure: 0.9977596625317162
Iteration time: 18.7
Weighted AreaUnderPRC: 0.9996129255579117
Mean absolute error: 0.012998928826520244
Coverage of cases: 99.97784342688331
Instances selection time: 7.0
Test time: 13.5
Accumulative iteration time: 18.7
Weighted Recall: 0.9977597242737566
Weighted FalsePositiveRate: 0.0022726516420265575
Kappa statistic: 0.9955134664396181
Training time: 11.7
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 52.99729197439685
Incorrectly Classified Instances: 0.11324470704086656
Correctly Classified Instances: 99.88675529295915
Weighted Precision: 0.998870743971767
Weighted AreaUnderROC: 0.9999604333590962
Root mean squared error: 0.04432404609225974
Relative absolute error: 1.8465118803929428
Root relative squared error: 8.864809218451947
Weighted TruePositiveRate: 0.9988675529295913
Weighted MatthewsCorrelation: 0.9977352305419082
Weighted FMeasure: 0.9988674777535655
Iteration time: 18.1
Weighted AreaUnderPRC: 0.999961009977571
Mean absolute error: 0.009232559401964715
Coverage of cases: 99.99507631708518
Instances selection time: 6.4
Test time: 13.0
Accumulative iteration time: 36.8
Weighted Recall: 0.9988675529295913
Weighted FalsePositiveRate: 0.001192080945685976
Kappa statistic: 0.9977318947254524
Training time: 11.7
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 53.15977351058591
Incorrectly Classified Instances: 0.041851304775972424
Correctly Classified Instances: 99.95814869522403
Weighted Precision: 0.9995815982326235
Weighted AreaUnderROC: 0.9999950723753968
Root mean squared error: 0.03946100922307022
Relative absolute error: 1.8033836004660118
Root relative squared error: 7.8922018446140445
Weighted TruePositiveRate: 0.9995814869522401
Weighted MatthewsCorrelation: 0.9991620047673647
Weighted FMeasure: 0.9995814880083873
Iteration time: 20.2
Weighted AreaUnderPRC: 0.9999950632054884
Mean absolute error: 0.009016918002330057
Coverage of cases: 100.0
Instances selection time: 5.6
Test time: 13.6
Accumulative iteration time: 57.0
Weighted Recall: 0.9995814869522401
Weighted FalsePositiveRate: 4.2491180183849224E-4
Kappa statistic: 0.9991618955268876
Training time: 14.6
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 53.31733136386016
Incorrectly Classified Instances: 0.05908419497784343
Correctly Classified Instances: 99.94091580502217
Weighted Precision: 0.9994111482723709
Weighted AreaUnderROC: 0.9999995145197434
Root mean squared error: 0.04318893139192108
Relative absolute error: 1.9585123264557285
Root relative squared error: 8.637786278384215
Weighted TruePositiveRate: 0.9994091580502215
Weighted MatthewsCorrelation: 0.9988188379512015
Weighted FMeasure: 0.9994091846587321
Iteration time: 23.3
Weighted AreaUnderPRC: 0.9999995156109183
Mean absolute error: 0.009792561632278643
Coverage of cases: 100.0
Instances selection time: 5.3
Test time: 13.7
Accumulative iteration time: 80.3
Weighted Recall: 0.9994091580502215
Weighted FalsePositiveRate: 5.711064961021687E-4
Kappa statistic: 0.9988169028123984
Training time: 18.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 53.3727227966519
Incorrectly Classified Instances: 0.046774987690792705
Correctly Classified Instances: 99.95322501230922
Weighted Precision: 0.9995330258818264
Weighted AreaUnderROC: 0.9999995630677692
Root mean squared error: 0.04363868271630143
Relative absolute error: 1.9872239692592533
Root relative squared error: 8.727736543260288
Weighted TruePositiveRate: 0.999532250123092
Weighted MatthewsCorrelation: 0.9990640559563462
Weighted FMeasure: 0.9995322440753721
Iteration time: 25.4
Weighted AreaUnderPRC: 0.9999995637123928
Mean absolute error: 0.009936119846296266
Coverage of cases: 100.0
Instances selection time: 4.9
Test time: 13.5
Accumulative iteration time: 105.7
Weighted Recall: 0.999532250123092
Weighted FalsePositiveRate: 4.6718800103845264E-4
Kappa statistic: 0.9990632682728045
Training time: 20.5
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 52.58124076809454
Incorrectly Classified Instances: 0.05169867060561299
Correctly Classified Instances: 99.9483013293944
Weighted Precision: 0.9994844252739782
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03806413009855531
Relative absolute error: 1.5106610033639023
Root relative squared error: 7.612826019711062
Weighted TruePositiveRate: 0.9994830132939437
Weighted MatthewsCorrelation: 0.9989661487383789
Weighted FMeasure: 0.9994830349520608
Iteration time: 27.4
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00755330501681951
Coverage of cases: 100.0
Instances selection time: 4.2
Test time: 13.6
Accumulative iteration time: 133.1
Weighted Recall: 0.9994830132939437
Weighted FalsePositiveRate: 4.8820016500419775E-4
Kappa statistic: 0.9989647807948618
Training time: 23.2
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 51.91161989167898
Incorrectly Classified Instances: 0.03938946331856229
Correctly Classified Instances: 99.96061053668143
Weighted Precision: 0.9996074577241645
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.029055001884809473
Relative absolute error: 1.0266437698967346
Root relative squared error: 5.811000376961895
Weighted TruePositiveRate: 0.9996061053668143
Weighted MatthewsCorrelation: 0.9992125684877744
Weighted FMeasure: 0.9996061154181257
Iteration time: 29.7
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005133218849483673
Coverage of cases: 100.0
Instances selection time: 3.4
Test time: 13.7
Accumulative iteration time: 162.8
Weighted Recall: 0.9996061053668143
Weighted FalsePositiveRate: 3.9136968168522E-4
Kappa statistic: 0.9992112370934608
Training time: 26.3
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 51.430329886755295
Incorrectly Classified Instances: 0.029542097488921715
Correctly Classified Instances: 99.97045790251107
Weighted Precision: 0.9997063785437396
Weighted AreaUnderROC: 0.9999996116157949
Root mean squared error: 0.024092712618529976
Relative absolute error: 0.7519277701371776
Root relative squared error: 4.818542523705995
Weighted TruePositiveRate: 0.9997045790251107
Weighted MatthewsCorrelation: 0.9994102584769585
Weighted FMeasure: 0.9997046078468651
Iteration time: 32.8
Weighted AreaUnderPRC: 0.9999996124454313
Mean absolute error: 0.003759638850685889
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 13.4
Accumulative iteration time: 195.6
Weighted Recall: 0.9997045790251107
Weighted FalsePositiveRate: 2.749212304339768E-4
Kappa statistic: 0.9994085195320576
Training time: 29.8
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.796405711472175
Incorrectly Classified Instances: 0.009847365829640572
Correctly Classified Instances: 99.99015263417036
Weighted Precision: 0.999901619858948
Weighted AreaUnderROC: 0.9999998058078974
Root mean squared error: 0.016649388693634682
Relative absolute error: 0.40287732004906324
Root relative squared error: 3.3298777387269363
Weighted TruePositiveRate: 0.9999015263417036
Weighted MatthewsCorrelation: 0.9998028880634549
Weighted FMeasure: 0.9999015245727808
Iteration time: 34.9
Weighted AreaUnderPRC: 0.9999998062226467
Mean absolute error: 0.002014386600245316
Coverage of cases: 100.0
Instances selection time: 2.2
Test time: 13.3
Accumulative iteration time: 230.5
Weighted Recall: 0.9999015263417036
Weighted FalsePositiveRate: 1.0581643363413566E-4
Kappa statistic: 0.9998027909307133
Training time: 32.7
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.66223535204333
Incorrectly Classified Instances: 0.002461841457410143
Correctly Classified Instances: 99.9975381585426
Weighted Precision: 0.9999753932806346
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.014036493794705136
Relative absolute error: 0.3198075866379056
Root relative squared error: 2.807298758941027
Weighted TruePositiveRate: 0.9999753815854259
Weighted MatthewsCorrelation: 0.999950710753479
Weighted FMeasure: 0.9999753813658085
Iteration time: 38.9
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001599037933189528
Coverage of cases: 100.0
Instances selection time: 1.7
Test time: 13.2
Accumulative iteration time: 269.4
Weighted Recall: 0.9999753815854259
Weighted FalsePositiveRate: 2.6454108408533914E-5
Kappa statistic: 0.9999506986063313
Training time: 37.2
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.64500246184146
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.012871623779327513
Relative absolute error: 0.29206001308781
Root relative squared error: 2.574324755865502
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 41.6
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00146030006543905
Coverage of cases: 100.0
Instances selection time: 0.8
Test time: 13.5
Accumulative iteration time: 311.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 40.8
		
Time end:Sun Oct 08 04.44.00 EEST 2017