Sun Oct 08 04.43.51 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.43.51 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 54.64057114721812
Incorrectly Classified Instances: 0.29542097488921715
Correctly Classified Instances: 99.70457902511079
Weighted Precision: 0.997062543803559
Weighted AreaUnderROC: 0.9999130990340884
Root mean squared error: 0.0661437109481598
Relative absolute error: 3.411492114060836
Root relative squared error: 13.228742189631959
Weighted TruePositiveRate: 0.9970457902511078
Weighted MatthewsCorrelation: 0.994100083535708
Weighted FMeasure: 0.9970454502679525
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9999147728738907
Mean absolute error: 0.01705746057030418
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9970457902511078
Weighted FalsePositiveRate: 0.0031744930090240697
Kappa statistic: 0.9940826793332744
Training time: 9.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 54.09896602658789
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.044680632793599184
Relative absolute error: 2.375473614313205
Root relative squared error: 8.936126558719836
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 19.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.011877368071566027
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 35.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 12.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 52.85573609059576
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.02841876300540633
Relative absolute error: 1.2502995699964052
Root relative squared error: 5.683752601081267
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 20.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0062514978499820266
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 55.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 14.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 54.81290004923683
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04861585680073863
Relative absolute error: 2.6583501957413476
Root relative squared error: 9.723171360147726
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 25.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.013291750978706737
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 80.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 20.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 54.99753815854259
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.04752532718150507
Relative absolute error: 2.6464803841794278
Root relative squared error: 9.505065436301013
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 30.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01323240192089714
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 110.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 25.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 52.375677006400785
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.03116778420023811
Relative absolute error: 1.303356951276938
Root relative squared error: 6.233556840047623
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 29.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.00651678475638469
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 139.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 24.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 51.76021664204825
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.030891830212193376
Relative absolute error: 1.0124672027980053
Root relative squared error: 6.178366042438675
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.005062336013990027
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 170.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 27.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 51.19399310684392
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.018462903242699204
Relative absolute error: 0.5507521910774874
Root relative squared error: 3.692580648539841
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 31.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.002753760955387437
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 201.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 28.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 51.07090103397341
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.016230918340747063
Relative absolute error: 0.4700347087035741
Root relative squared error: 3.2461836681494125
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 36.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.0023501735435178706
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 14.0
Accumulative iteration time: 237.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 34.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.38158542589857
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.008530543426385954
Relative absolute error: 0.17383405946863756
Root relative squared error: 1.7061086852771907
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 39.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.691702973431878E-4
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 276.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 37.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.529295913343184
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013073897593360573
Relative absolute error: 0.27214895313122783
Root relative squared error: 2.6147795186721146
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 39.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.001360744765656139
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 315.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 38.0
		
Time end:Sun Oct 08 04.43.52 EEST 2017