Sun Oct 08 04.40.39 EEST 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 04.40.39 EEST 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.58796404660274E-4
Relative absolute error: 0.14937509623861459
Root relative squared error: 0.1717592809320548
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 905.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.468754811930729E-4
Coverage of cases: 100.0
Instances selection time: 905.0
Test time: 1146.0
Accumulative iteration time: 905.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 2
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.948514057578021E-4
Relative absolute error: 0.10297551026045412
Root relative squared error: 0.11897028115156043
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1031.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.148775513022706E-4
Coverage of cases: 100.0
Instances selection time: 1031.0
Test time: 1581.0
Accumulative iteration time: 1936.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 0.0
		
Iteration: 3
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.3648018375533907E-4
Relative absolute error: 0.07442582824542707
Root relative squared error: 0.08729603675106781
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1097.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.721291412271353E-4
Coverage of cases: 100.0
Instances selection time: 1096.0
Test time: 1620.0
Accumulative iteration time: 3033.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 4
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.3736591099811766E-4
Relative absolute error: 0.05628247639486349
Root relative squared error: 0.06747318219962353
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1113.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8141238197431745E-4
Coverage of cases: 100.0
Instances selection time: 1112.0
Test time: 1873.0
Accumulative iteration time: 4146.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 5
Labeled set size: 2012
Unlabelled set size: 2050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.719868554693249E-4
Relative absolute error: 0.04418374817337439
Root relative squared error: 0.05439737109386498
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1301.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.2091874086687196E-4
Coverage of cases: 100.0
Instances selection time: 1300.0
Test time: 2405.0
Accumulative iteration time: 5447.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 6
Labeled set size: 2312
Unlabelled set size: 1750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.3015865264003536E-4
Relative absolute error: 0.03641454614583642
Root relative squared error: 0.04603173052800707
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1063.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8207273072918208E-4
Coverage of cases: 100.0
Instances selection time: 1062.0
Test time: 2382.0
Accumulative iteration time: 6510.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 7
Labeled set size: 2612
Unlabelled set size: 1450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.88859187022927E-4
Relative absolute error: 0.029917163937998027
Root relative squared error: 0.0377718374045854
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 978.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4958581968999013E-4
Coverage of cases: 100.0
Instances selection time: 977.0
Test time: 2598.0
Accumulative iteration time: 7488.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 8
Labeled set size: 2912
Unlabelled set size: 1150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.5018337434002328E-4
Relative absolute error: 0.024037260106745888
Root relative squared error: 0.030036674868004654
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 856.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2018630053372943E-4
Coverage of cases: 100.0
Instances selection time: 855.0
Test time: 2859.0
Accumulative iteration time: 8344.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 9
Labeled set size: 3212
Unlabelled set size: 850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1603336554389126E-4
Relative absolute error: 0.01893025251362001
Root relative squared error: 0.023206673108778254
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 681.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.465126256810005E-5
Coverage of cases: 100.0
Instances selection time: 680.0
Test time: 3105.0
Accumulative iteration time: 9025.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 10
Labeled set size: 3512
Unlabelled set size: 550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.809116219386231E-5
Relative absolute error: 0.014881869981218439
Root relative squared error: 0.017618232438772464
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 476.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.440934990609219E-5
Coverage of cases: 100.0
Instances selection time: 475.0
Test time: 3366.0
Accumulative iteration time: 9501.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Iteration: 11
Labeled set size: 3812
Unlabelled set size: 250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.89936600645119E-5
Relative absolute error: 0.012038513677813778
Root relative squared error: 0.01379873201290238
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 237.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.0192568389068887E-5
Coverage of cases: 100.0
Instances selection time: 236.0
Test time: 3613.0
Accumulative iteration time: 9738.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1.0
		
Time end:Sun Oct 08 04.41.16 EEST 2017