Sun Oct 08 07.16.55 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.16.55 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 50.700104493207284
Incorrectly Classified Instances: 15.047021943573668
Correctly Classified Instances: 84.95297805642633
Weighted Precision: 0.8535705795609407
Weighted AreaUnderROC: 0.9029028221659993
Root mean squared error: 0.3002287932600232
Relative absolute error: 28.39147089112262
Root relative squared error: 63.688144686484215
Weighted TruePositiveRate: 0.8495297805642633
Weighted MatthewsCorrelation: 0.7644297478461284
Weighted FMeasure: 0.8509538826663254
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8170416040860624
Mean absolute error: 0.12618431507165895
Coverage of cases: 92.60188087774294
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8495297805642633
Weighted FalsePositiveRate: 0.07867386775116121
Kappa statistic: 0.7583012899437426
Training time: 16.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 54.921630094043394
Incorrectly Classified Instances: 16.990595611285265
Correctly Classified Instances: 83.00940438871473
Weighted Precision: 0.8260634206883651
Weighted AreaUnderROC: 0.8687566332909665
Root mean squared error: 0.3177105562934676
Relative absolute error: 31.971835828000515
Root relative squared error: 67.39658664289763
Weighted TruePositiveRate: 0.8300940438871474
Weighted MatthewsCorrelation: 0.7273590960369961
Weighted FMeasure: 0.8272241722069652
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7696348631038671
Mean absolute error: 0.14209704812444995
Coverage of cases: 90.40752351097179
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.8300940438871474
Weighted FalsePositiveRate: 0.10960542394495079
Kappa statistic: 0.720515227812629
Training time: 19.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 48.40125391849515
Incorrectly Classified Instances: 18.369905956112852
Correctly Classified Instances: 81.63009404388714
Weighted Precision: 0.8122218872351054
Weighted AreaUnderROC: 0.8527353625783807
Root mean squared error: 0.33142019381863447
Relative absolute error: 32.418049688676334
Root relative squared error: 70.3048399413941
Weighted TruePositiveRate: 0.8163009404388715
Weighted MatthewsCorrelation: 0.7040672766139086
Weighted FMeasure: 0.8129875417675383
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7451643409780336
Mean absolute error: 0.14408022083856475
Coverage of cases: 86.89655172413794
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8163009404388715
Weighted FalsePositiveRate: 0.12255287582285165
Kappa statistic: 0.696347804842867
Training time: 23.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 51.38975966562145
Incorrectly Classified Instances: 12.037617554858935
Correctly Classified Instances: 87.96238244514106
Weighted Precision: 0.8803483032447957
Weighted AreaUnderROC: 0.9237402534440261
Root mean squared error: 0.26357813942204833
Relative absolute error: 22.200737093090854
Root relative squared error: 55.91336692735846
Weighted TruePositiveRate: 0.8796238244514106
Weighted MatthewsCorrelation: 0.8036109372914547
Weighted FMeasure: 0.8753624944619307
Iteration time: 41.0
Weighted AreaUnderPRC: 0.8616142166035216
Mean absolute error: 0.09866994263596159
Coverage of cases: 94.48275862068965
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8796238244514106
Weighted FalsePositiveRate: 0.09056562530469996
Kappa statistic: 0.8006179904762277
Training time: 38.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 42.25705329153587
Incorrectly Classified Instances: 8.087774294670846
Correctly Classified Instances: 91.91222570532915
Weighted Precision: 0.9222433898002806
Weighted AreaUnderROC: 0.9531245345236369
Root mean squared error: 0.21904170599453504
Relative absolute error: 15.783887208966139
Root relative squared error: 46.465762701421205
Weighted TruePositiveRate: 0.9191222570532915
Weighted MatthewsCorrelation: 0.873637951625548
Weighted FMeasure: 0.9197990296375068
Iteration time: 53.0
Weighted AreaUnderPRC: 0.9009738248584402
Mean absolute error: 0.07015060981762887
Coverage of cases: 95.54858934169279
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 156.0
Weighted Recall: 0.9191222570532915
Weighted FalsePositiveRate: 0.03803009836900857
Kappa statistic: 0.8700869433447617
Training time: 52.0
		
Time end:Sun Oct 08 07.16.55 EEST 2017