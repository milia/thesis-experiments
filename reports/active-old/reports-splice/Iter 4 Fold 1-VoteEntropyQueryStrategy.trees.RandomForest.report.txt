Sun Oct 08 09.50.59 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.50.59 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 95.31870428422157
Incorrectly Classified Instances: 26.018808777429467
Correctly Classified Instances: 73.98119122257053
Weighted Precision: 0.7579830201529852
Weighted AreaUnderROC: 0.8959391881398667
Root mean squared error: 0.3657743784392549
Relative absolute error: 69.95908017295284
Root relative squared error: 77.59246301360663
Weighted TruePositiveRate: 0.7398119122257053
Weighted MatthewsCorrelation: 0.5706143098023732
Weighted FMeasure: 0.7248304438008585
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8451445106120465
Mean absolute error: 0.31092924521313076
Coverage of cases: 99.62382445141066
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7398119122257053
Weighted FalsePositiveRate: 0.2321055506193314
Kappa statistic: 0.5422463864832586
Training time: 58.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 87.89968652037608
Incorrectly Classified Instances: 34.106583072100314
Correctly Classified Instances: 65.8934169278997
Weighted Precision: 0.7396102172889542
Weighted AreaUnderROC: 0.8951643025583331
Root mean squared error: 0.38516941314571873
Relative absolute error: 69.77201501624978
Root relative squared error: 81.70677118229328
Weighted TruePositiveRate: 0.6589341692789968
Weighted MatthewsCorrelation: 0.5033329242595032
Weighted FMeasure: 0.6414499873345998
Iteration time: 110.0
Weighted AreaUnderPRC: 0.8381675567396822
Mean absolute error: 0.31009784451667277
Coverage of cases: 98.93416927899686
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 174.0
Weighted Recall: 0.6589341692789968
Weighted FalsePositiveRate: 0.17027761317285509
Kappa statistic: 0.4642409483221717
Training time: 105.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 83.00940438871432
Incorrectly Classified Instances: 27.272727272727273
Correctly Classified Instances: 72.72727272727273
Weighted Precision: 0.774144564965442
Weighted AreaUnderROC: 0.9218744486173706
Root mean squared error: 0.36017039809576157
Relative absolute error: 62.667862238040044
Root relative squared error: 76.40367926285055
Weighted TruePositiveRate: 0.7272727272727273
Weighted MatthewsCorrelation: 0.5535780837165338
Weighted FMeasure: 0.6641594320936057
Iteration time: 151.0
Weighted AreaUnderPRC: 0.8740846780103303
Mean absolute error: 0.27852383216907317
Coverage of cases: 98.36990595611286
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 325.0
Weighted Recall: 0.7272727272727273
Weighted FalsePositiveRate: 0.20932835327077487
Kappa statistic: 0.5312132612273689
Training time: 148.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 81.08672936259111
Incorrectly Classified Instances: 30.9717868338558
Correctly Classified Instances: 69.0282131661442
Weighted Precision: 0.7710979612598968
Weighted AreaUnderROC: 0.8609883323063805
Root mean squared error: 0.3899840028852559
Relative absolute error: 67.2143560577533
Root relative squared error: 82.72809989833064
Weighted TruePositiveRate: 0.690282131661442
Weighted MatthewsCorrelation: 0.4667906767417155
Weighted FMeasure: 0.6113252074618155
Iteration time: 173.0
Weighted AreaUnderPRC: 0.7858443324159861
Mean absolute error: 0.2987304713677993
Coverage of cases: 95.04702194357367
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 498.0
Weighted Recall: 0.690282131661442
Weighted FalsePositiveRate: 0.29849864327758135
Kappa statistic: 0.4340002140646203
Training time: 171.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 98.64158829676066
Incorrectly Classified Instances: 20.12539184952978
Correctly Classified Instances: 79.87460815047022
Weighted Precision: 0.8211769869964475
Weighted AreaUnderROC: 0.9410889947450404
Root mean squared error: 0.3401124787408312
Relative absolute error: 64.87233435511286
Root relative squared error: 72.14875202514136
Weighted TruePositiveRate: 0.7987460815047022
Weighted MatthewsCorrelation: 0.6711310694302877
Weighted FMeasure: 0.7900018937883732
Iteration time: 250.0
Weighted AreaUnderPRC: 0.9103515854792985
Mean absolute error: 0.28832148602273033
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 12.0
Accumulative iteration time: 748.0
Weighted Recall: 0.7987460815047022
Weighted FalsePositiveRate: 0.18856988600679483
Kappa statistic: 0.6495920993197093
Training time: 250.0
		
Time end:Sun Oct 08 09.51.00 EEST 2017