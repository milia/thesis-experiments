Sun Oct 08 07.16.54 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.16.54 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 54.62904911180728
Incorrectly Classified Instances: 16.677115987460816
Correctly Classified Instances: 83.32288401253919
Weighted Precision: 0.8346049350279627
Weighted AreaUnderROC: 0.9351634825791444
Root mean squared error: 0.2969822659989205
Relative absolute error: 29.908647954557527
Root relative squared error: 62.99945225399441
Weighted TruePositiveRate: 0.8332288401253919
Weighted MatthewsCorrelation: 0.7307271535120331
Weighted FMeasure: 0.8338257976544473
Iteration time: 34.0
Weighted AreaUnderPRC: 0.8688552918280954
Mean absolute error: 0.1329273242424809
Coverage of cases: 97.86833855799372
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8332288401253919
Weighted FalsePositiveRate: 0.10099028899684795
Kappa statistic: 0.7297741419579036
Training time: 30.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 53.10344827586131
Incorrectly Classified Instances: 16.112852664576803
Correctly Classified Instances: 83.8871473354232
Weighted Precision: 0.8386705467099695
Weighted AreaUnderROC: 0.9315271728045642
Root mean squared error: 0.2905759408807011
Relative absolute error: 28.683040713717073
Root relative squared error: 61.640465473920834
Weighted TruePositiveRate: 0.838871473354232
Weighted MatthewsCorrelation: 0.7382190254390151
Weighted FMeasure: 0.8385457591961167
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8566020672452254
Mean absolute error: 0.12748018094985655
Coverage of cases: 96.8025078369906
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 84.0
Weighted Recall: 0.838871473354232
Weighted FalsePositiveRate: 0.10539320126504947
Kappa statistic: 0.7363130985647311
Training time: 44.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 47.77429467084553
Incorrectly Classified Instances: 17.36677115987461
Correctly Classified Instances: 82.6332288401254
Weighted Precision: 0.8316068980343315
Weighted AreaUnderROC: 0.8864806859734262
Root mean squared error: 0.31939011217217783
Relative absolute error: 31.77868236779625
Root relative squared error: 67.75287424826294
Weighted TruePositiveRate: 0.826332288401254
Weighted MatthewsCorrelation: 0.7267540819218147
Weighted FMeasure: 0.8120687012568962
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7839820931606852
Mean absolute error: 0.14123858830131986
Coverage of cases: 91.84952978056427
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 113.0
Weighted Recall: 0.826332288401254
Weighted FalsePositiveRate: 0.11413248796084655
Kappa statistic: 0.7124328539191572
Training time: 27.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 48.50574712643599
Incorrectly Classified Instances: 18.746081504702193
Correctly Classified Instances: 81.25391849529781
Weighted Precision: 0.8239378770679658
Weighted AreaUnderROC: 0.8797309726610261
Root mean squared error: 0.33516048944507654
Relative absolute error: 33.6075024542597
Root relative squared error: 71.09827646172397
Weighted TruePositiveRate: 0.812539184952978
Weighted MatthewsCorrelation: 0.7026809605720086
Weighted FMeasure: 0.7868879307858502
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7719318851002324
Mean absolute error: 0.14936667757449096
Coverage of cases: 92.2257053291536
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 145.0
Weighted Recall: 0.812539184952978
Weighted FalsePositiveRate: 0.13851160788168712
Kappa statistic: 0.6839434484065788
Training time: 31.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 42.3615464994772
Incorrectly Classified Instances: 7.084639498432602
Correctly Classified Instances: 92.91536050156739
Weighted Precision: 0.9296844431379884
Weighted AreaUnderROC: 0.954820042456298
Root mean squared error: 0.21105090625438247
Relative absolute error: 14.176502265557446
Root relative squared error: 44.77065809641155
Weighted TruePositiveRate: 0.929153605015674
Weighted MatthewsCorrelation: 0.8897259550440867
Weighted FMeasure: 0.9292635875253853
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9019765841639477
Mean absolute error: 0.0630066767358123
Coverage of cases: 95.54858934169279
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 197.0
Weighted Recall: 0.929153605015674
Weighted FalsePositiveRate: 0.03723082253562994
Kappa statistic: 0.8852048046663277
Training time: 51.0
		
Time end:Sun Oct 08 07.16.54 EEST 2017