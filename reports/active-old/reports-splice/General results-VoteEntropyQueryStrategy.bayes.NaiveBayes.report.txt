Sun Oct 08 09.44.28 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.44.28 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 40.83176593521427
Incorrectly Classified Instances: 7.22257053291536
Correctly Classified Instances: 92.77742946708466
Weighted Precision: 0.9280684245117804
Weighted AreaUnderROC: 0.9871436735370971
Root mean squared error: 0.18997029832363316
Relative absolute error: 13.643184481545452
Root relative squared error: 40.29878585060128
Weighted TruePositiveRate: 0.9277742946708465
Weighted MatthewsCorrelation: 0.8826751280340671
Weighted FMeasure: 0.9275924807109204
Iteration time: 40.9
Weighted AreaUnderPRC: 0.9783914287617057
Mean absolute error: 0.06063637547353673
Coverage of cases: 98.11285266457679
Instances selection time: 39.3
Test time: 52.9
Accumulative iteration time: 40.9
Weighted Recall: 0.9277742946708465
Weighted FalsePositiveRate: 0.04755659635675184
Kappa statistic: 0.8821567455772907
Training time: 1.6
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 41.103448275862135
Incorrectly Classified Instances: 7.824451410658307
Correctly Classified Instances: 92.1755485893417
Weighted Precision: 0.9245349826607893
Weighted AreaUnderROC: 0.9882311128546185
Root mean squared error: 0.19975303762554222
Relative absolute error: 14.653142905451684
Root relative squared error: 42.37401824028926
Weighted TruePositiveRate: 0.9217554858934169
Weighted MatthewsCorrelation: 0.8737496506079208
Weighted FMeasure: 0.9210322907149999
Iteration time: 30.7
Weighted AreaUnderPRC: 0.980055522347642
Mean absolute error: 0.06512507957978673
Coverage of cases: 97.79310344827586
Instances selection time: 29.2
Test time: 50.8
Accumulative iteration time: 71.6
Weighted Recall: 0.9217554858934169
Weighted FalsePositiveRate: 0.0588658747448936
Kappa statistic: 0.8711806288836857
Training time: 1.5
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 41.4148380355277
Incorrectly Classified Instances: 8.564263322884013
Correctly Classified Instances: 91.43573667711598
Weighted Precision: 0.9191635709313278
Weighted AreaUnderROC: 0.9888473249615881
Root mean squared error: 0.20853710001867495
Relative absolute error: 15.651247738461512
Root relative squared error: 44.23739926565422
Weighted TruePositiveRate: 0.91435736677116
Weighted MatthewsCorrelation: 0.8611919624954435
Weighted FMeasure: 0.9128542937510898
Iteration time: 21.7
Weighted AreaUnderPRC: 0.9808842118657397
Mean absolute error: 0.06956110105983053
Coverage of cases: 97.58620689655172
Instances selection time: 19.8
Test time: 51.0
Accumulative iteration time: 93.3
Weighted Recall: 0.91435736677116
Weighted FalsePositiveRate: 0.07338223597709645
Kappa statistic: 0.8573842696904561
Training time: 1.9
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 40.63113897596633
Incorrectly Classified Instances: 7.1347962382445145
Correctly Classified Instances: 92.86520376175548
Weighted Precision: 0.9308866764649164
Weighted AreaUnderROC: 0.9899756296312349
Root mean squared error: 0.19163493827519856
Relative absolute error: 13.515872044083062
Root relative squared error: 40.65190930999705
Weighted TruePositiveRate: 0.9286520376175549
Weighted MatthewsCorrelation: 0.8845065935699858
Weighted FMeasure: 0.9273079237193469
Iteration time: 13.6
Weighted AreaUnderPRC: 0.9822595834693907
Mean absolute error: 0.06007054241814831
Coverage of cases: 97.98746081504703
Instances selection time: 11.3
Test time: 50.8
Accumulative iteration time: 106.9
Weighted Recall: 0.9286520376175549
Weighted FalsePositiveRate: 0.05611083391212022
Kappa statistic: 0.8823078824607817
Training time: 2.3
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.52455590386622
Incorrectly Classified Instances: 4.652037617554859
Correctly Classified Instances: 95.34796238244515
Weighted Precision: 0.9535964674235677
Weighted AreaUnderROC: 0.9930388315818425
Root mean squared error: 0.1553558228406527
Relative absolute error: 9.309110323031932
Root relative squared error: 32.955946748232066
Weighted TruePositiveRate: 0.9534796238244512
Weighted MatthewsCorrelation: 0.9251481623269632
Weighted FMeasure: 0.9534653080343618
Iteration time: 5.4
Weighted AreaUnderPRC: 0.9876996004926214
Mean absolute error: 0.041373823657920646
Coverage of cases: 98.71473354231975
Instances selection time: 2.8
Test time: 50.8
Accumulative iteration time: 112.3
Weighted Recall: 0.9534796238244512
Weighted FalsePositiveRate: 0.02819699574638067
Kappa statistic: 0.9243768925926389
Training time: 2.6
		
Time end:Sun Oct 08 09.44.33 EEST 2017