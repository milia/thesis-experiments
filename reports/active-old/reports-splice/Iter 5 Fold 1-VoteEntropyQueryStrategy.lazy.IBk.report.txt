Sun Oct 08 09.50.18 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.50.18 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 39.91640543364687
Incorrectly Classified Instances: 35.42319749216301
Correctly Classified Instances: 64.57680250783699
Weighted Precision: 0.7176869626409693
Weighted AreaUnderROC: 0.8009098607744777
Root mean squared error: 0.4431707806316254
Relative absolute error: 51.49240485147724
Root relative squared error: 94.0107192625064
Weighted TruePositiveRate: 0.64576802507837
Weighted MatthewsCorrelation: 0.4945422060564399
Weighted FMeasure: 0.643214538737511
Iteration time: 642.0
Weighted AreaUnderPRC: 0.6714089453150496
Mean absolute error: 0.22885513267323737
Coverage of cases: 73.60501567398119
Instances selection time: 641.0
Test time: 801.0
Accumulative iteration time: 642.0
Weighted Recall: 0.64576802507837
Weighted FalsePositiveRate: 0.1417647497307832
Kappa statistic: 0.47697998052259355
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 39.31034482758621
Incorrectly Classified Instances: 26.70846394984326
Correctly Classified Instances: 73.29153605015674
Weighted Precision: 0.7387493193184678
Weighted AreaUnderROC: 0.8256022274763614
Root mean squared error: 0.3939984913763022
Relative absolute error: 41.18796849387513
Root relative squared error: 83.57970150883489
Weighted TruePositiveRate: 0.7329153605015674
Weighted MatthewsCorrelation: 0.570906313891302
Weighted FMeasure: 0.7346649935116689
Iteration time: 901.0
Weighted AreaUnderPRC: 0.7070690138473803
Mean absolute error: 0.18305763775056028
Coverage of cases: 79.68652037617555
Instances selection time: 900.0
Test time: 1488.0
Accumulative iteration time: 1543.0
Weighted Recall: 0.7329153605015674
Weighted FalsePositiveRate: 0.15485639151039016
Kappa statistic: 0.5730430274753758
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 38.24451410658306
Incorrectly Classified Instances: 25.078369905956112
Correctly Classified Instances: 74.92163009404389
Weighted Precision: 0.7470270424382209
Weighted AreaUnderROC: 0.8227142782909351
Root mean squared error: 0.39114544079477803
Relative absolute error: 39.623106321925675
Root relative squared error: 82.9744780848557
Weighted TruePositiveRate: 0.7492163009404389
Weighted MatthewsCorrelation: 0.5833443297243632
Weighted FMeasure: 0.7461037393005402
Iteration time: 910.0
Weighted AreaUnderPRC: 0.7061836693207005
Mean absolute error: 0.1761026947641181
Coverage of cases: 79.62382445141066
Instances selection time: 909.0
Test time: 2197.0
Accumulative iteration time: 2453.0
Weighted Recall: 0.7492163009404389
Weighted FalsePositiveRate: 0.1798338925955926
Kappa statistic: 0.5824205600931504
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.996865203761615
Incorrectly Classified Instances: 27.648902821316614
Correctly Classified Instances: 72.35109717868339
Weighted Precision: 0.76754680388466
Weighted AreaUnderROC: 0.8342084305366738
Root mean squared error: 0.39878396920459247
Relative absolute error: 41.50463497876086
Root relative squared error: 84.59485465591544
Weighted TruePositiveRate: 0.7235109717868339
Weighted MatthewsCorrelation: 0.5840510944325237
Weighted FMeasure: 0.7255494660997089
Iteration time: 653.0
Weighted AreaUnderPRC: 0.7155549519112868
Mean absolute error: 0.18446504435005245
Coverage of cases: 78.93416927899686
Instances selection time: 652.0
Test time: 2905.0
Accumulative iteration time: 3106.0
Weighted Recall: 0.7235109717868339
Weighted FalsePositiveRate: 0.14053922747366882
Kappa statistic: 0.5658184201949572
Training time: 1.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 39.54022988505758
Incorrectly Classified Instances: 27.21003134796238
Correctly Classified Instances: 72.78996865203762
Weighted Precision: 0.785596607707762
Weighted AreaUnderROC: 0.8639838721389649
Root mean squared error: 0.38520013252763396
Relative absolute error: 39.41493340439006
Root relative squared error: 81.71328774727311
Weighted TruePositiveRate: 0.7278996865203762
Weighted MatthewsCorrelation: 0.6096858267982783
Weighted FMeasure: 0.7283256184718726
Iteration time: 162.0
Weighted AreaUnderPRC: 0.756508072640851
Mean absolute error: 0.17517748179729312
Coverage of cases: 81.00313479623824
Instances selection time: 161.0
Test time: 3569.0
Accumulative iteration time: 3268.0
Weighted Recall: 0.7278996865203762
Weighted FalsePositiveRate: 0.10475761472386706
Kappa statistic: 0.5916141162533723
Training time: 1.0
		
Time end:Sun Oct 08 09.50.32 EEST 2017