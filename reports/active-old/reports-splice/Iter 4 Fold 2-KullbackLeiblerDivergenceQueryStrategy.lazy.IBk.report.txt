Sun Oct 08 07.15.58 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.15.58 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 38.78787878787887
Incorrectly Classified Instances: 32.97805642633229
Correctly Classified Instances: 67.02194357366771
Weighted Precision: 0.7339363070799985
Weighted AreaUnderROC: 0.8157595998550814
Root mean squared error: 0.43172242703385777
Relative absolute error: 48.08528683671714
Root relative squared error: 91.58215672378556
Weighted TruePositiveRate: 0.6702194357366771
Weighted MatthewsCorrelation: 0.5253842814404105
Weighted FMeasure: 0.6698374188248133
Iteration time: 1281.0
Weighted AreaUnderPRC: 0.6888769914464162
Mean absolute error: 0.2137123859409699
Coverage of cases: 74.98432601880877
Instances selection time: 1280.0
Test time: 797.0
Accumulative iteration time: 1281.0
Weighted Recall: 0.6702194357366771
Weighted FalsePositiveRate: 0.13410302023984255
Kappa statistic: 0.5088782647738388
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 39.01776384535015
Incorrectly Classified Instances: 43.13479623824451
Correctly Classified Instances: 56.86520376175549
Weighted Precision: 0.7183031064460449
Weighted AreaUnderROC: 0.7665131990086519
Root mean squared error: 0.49820201697457356
Relative absolute error: 61.81431118495765
Root relative squared error: 105.68460738105973
Weighted TruePositiveRate: 0.5686520376175549
Weighted MatthewsCorrelation: 0.43470571114213724
Weighted FMeasure: 0.5500985920247656
Iteration time: 1889.0
Weighted AreaUnderPRC: 0.635422328595273
Mean absolute error: 0.27473027193315136
Coverage of cases: 65.8307210031348
Instances selection time: 1888.0
Test time: 1498.0
Accumulative iteration time: 3170.0
Weighted Recall: 0.5686520376175549
Weighted FalsePositiveRate: 0.1510671387736242
Kappa statistic: 0.3894451504354271
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 38.78787878787881
Incorrectly Classified Instances: 32.288401253918494
Correctly Classified Instances: 67.7115987460815
Weighted Precision: 0.7527334495872509
Weighted AreaUnderROC: 0.8213800267267696
Root mean squared error: 0.4297423234582646
Relative absolute error: 47.07125993615683
Root relative squared error: 91.16211332405946
Weighted TruePositiveRate: 0.677115987460815
Weighted MatthewsCorrelation: 0.5441104850515153
Weighted FMeasure: 0.6816792607763102
Iteration time: 1906.0
Weighted AreaUnderPRC: 0.7035105689761947
Mean absolute error: 0.20920559971625732
Coverage of cases: 75.29780564263324
Instances selection time: 1905.0
Test time: 2166.0
Accumulative iteration time: 5076.0
Weighted Recall: 0.677115987460815
Weighted FalsePositiveRate: 0.1309635308134162
Kappa statistic: 0.5157583038969247
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.24451410658302
Incorrectly Classified Instances: 27.021943573667713
Correctly Classified Instances: 72.97805642633229
Weighted Precision: 0.7666651271469337
Weighted AreaUnderROC: 0.8395526354873948
Root mean squared error: 0.3997889095668915
Relative absolute error: 40.89193732280197
Root relative squared error: 84.80803469937635
Weighted TruePositiveRate: 0.7297805642633229
Weighted MatthewsCorrelation: 0.5934306595644478
Weighted FMeasure: 0.7328806021039095
Iteration time: 1343.0
Weighted AreaUnderPRC: 0.7244590472532635
Mean absolute error: 0.18174194365690177
Coverage of cases: 78.55799373040752
Instances selection time: 1342.0
Test time: 2815.0
Accumulative iteration time: 6419.0
Weighted Recall: 0.7297805642633229
Weighted FalsePositiveRate: 0.13045880882704433
Kappa statistic: 0.5791340303635472
Training time: 1.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.829676071055445
Incorrectly Classified Instances: 26.08150470219436
Correctly Classified Instances: 73.91849529780565
Weighted Precision: 0.7853783342659556
Weighted AreaUnderROC: 0.8576074905251796
Root mean squared error: 0.3866459447446385
Relative absolute error: 39.02248218910545
Root relative squared error: 82.01999083416298
Weighted TruePositiveRate: 0.7391849529780564
Weighted MatthewsCorrelation: 0.6184600516798453
Weighted FMeasure: 0.7395023494777112
Iteration time: 339.0
Weighted AreaUnderPRC: 0.7503937210175805
Mean absolute error: 0.17343325417380595
Coverage of cases: 80.25078369905957
Instances selection time: 337.0
Test time: 3486.0
Accumulative iteration time: 6758.0
Weighted Recall: 0.7391849529780564
Weighted FalsePositiveRate: 0.10461340398273576
Kappa statistic: 0.6058605317009857
Training time: 2.0
		
Time end:Sun Oct 08 07.16.15 EEST 2017