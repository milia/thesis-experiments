Sun Oct 08 09.49.05 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.49.05 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 39.0595611285268
Incorrectly Classified Instances: 35.23510971786834
Correctly Classified Instances: 64.76489028213166
Weighted Precision: 0.7057409246718507
Weighted AreaUnderROC: 0.7984915737925038
Root mean squared error: 0.44929937429361966
Relative absolute error: 51.85656819692814
Root relative squared error: 95.31079030376628
Weighted TruePositiveRate: 0.6476489028213166
Weighted MatthewsCorrelation: 0.48615116517237034
Weighted FMeasure: 0.6461645586293336
Iteration time: 628.0
Weighted AreaUnderPRC: 0.6591546084735307
Mean absolute error: 0.23047363643079696
Coverage of cases: 72.78996865203762
Instances selection time: 626.0
Test time: 785.0
Accumulative iteration time: 628.0
Weighted Recall: 0.6476489028213166
Weighted FalsePositiveRate: 0.14935741679048808
Kappa statistic: 0.4752653703477137
Training time: 2.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 38.955067920585215
Incorrectly Classified Instances: 44.70219435736677
Correctly Classified Instances: 55.29780564263323
Weighted Precision: 0.7015546063830529
Weighted AreaUnderROC: 0.7491400228131281
Root mean squared error: 0.510294399000753
Relative absolute error: 64.43494306884887
Root relative squared error: 108.24978898048265
Weighted TruePositiveRate: 0.5529780564263322
Weighted MatthewsCorrelation: 0.4117616444419505
Weighted FMeasure: 0.5344922265534207
Iteration time: 918.0
Weighted AreaUnderPRC: 0.6097789079681225
Mean absolute error: 0.2863775247504459
Coverage of cases: 64.07523510971787
Instances selection time: 917.0
Test time: 1466.0
Accumulative iteration time: 1546.0
Weighted Recall: 0.5529780564263322
Weighted FalsePositiveRate: 0.15984780938531934
Kappa statistic: 0.36725929107624916
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 38.82967607105541
Incorrectly Classified Instances: 33.73040752351097
Correctly Classified Instances: 66.26959247648902
Weighted Precision: 0.7378205735859404
Weighted AreaUnderROC: 0.8078584759868395
Root mean squared error: 0.43807042739329255
Relative absolute error: 48.80078990533658
Root relative squared error: 92.92877095412483
Weighted TruePositiveRate: 0.6626959247648903
Weighted MatthewsCorrelation: 0.5213895199428444
Weighted FMeasure: 0.6648884238979946
Iteration time: 927.0
Weighted AreaUnderPRC: 0.6763083720554246
Mean absolute error: 0.2168923995792786
Coverage of cases: 74.35736677115987
Instances selection time: 927.0
Test time: 2128.0
Accumulative iteration time: 2473.0
Weighted Recall: 0.6626959247648903
Weighted FalsePositiveRate: 0.14077555076268825
Kappa statistic: 0.4937353353986441
Training time: 0.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.704284221525576
Incorrectly Classified Instances: 30.031347962382444
Correctly Classified Instances: 69.96865203761756
Weighted Precision: 0.7370494043152793
Weighted AreaUnderROC: 0.8061455000509092
Root mean squared error: 0.42026236469408773
Relative absolute error: 45.22926021034922
Root relative squared error: 89.15111038580399
Weighted TruePositiveRate: 0.6996865203761755
Weighted MatthewsCorrelation: 0.5423260886051164
Weighted FMeasure: 0.6982328804450992
Iteration time: 954.0
Weighted AreaUnderPRC: 0.6743995782318147
Mean absolute error: 0.2010189342682233
Coverage of cases: 76.42633228840126
Instances selection time: 951.0
Test time: 2811.0
Accumulative iteration time: 3427.0
Weighted Recall: 0.6996865203761755
Weighted FalsePositiveRate: 0.1552436529112705
Kappa statistic: 0.5299680331136584
Training time: 3.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.537095088819314
Incorrectly Classified Instances: 28.46394984326019
Correctly Classified Instances: 71.53605015673982
Weighted Precision: 0.7711926455173325
Weighted AreaUnderROC: 0.8400685551969623
Root mean squared error: 0.40106981313602863
Relative absolute error: 41.365440008572065
Root relative squared error: 85.07975537931124
Weighted TruePositiveRate: 0.7153605015673982
Weighted MatthewsCorrelation: 0.5886023760205896
Weighted FMeasure: 0.7146655111907421
Iteration time: 162.0
Weighted AreaUnderPRC: 0.7260965029535205
Mean absolute error: 0.18384640003810224
Coverage of cases: 78.49529780564264
Instances selection time: 161.0
Test time: 3508.0
Accumulative iteration time: 3589.0
Weighted Recall: 0.7153605015673982
Weighted FalsePositiveRate: 0.11264429767254537
Kappa statistic: 0.57356055413042
Training time: 1.0
		
Time end:Sun Oct 08 09.49.20 EEST 2017