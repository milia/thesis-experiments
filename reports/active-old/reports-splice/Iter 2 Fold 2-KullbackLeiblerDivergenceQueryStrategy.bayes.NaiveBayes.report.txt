Sun Oct 08 07.10.06 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.10.06 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 40.710553814002004
Incorrectly Classified Instances: 6.018808777429467
Correctly Classified Instances: 93.98119122257053
Weighted Precision: 0.9397349994721064
Weighted AreaUnderROC: 0.9866033765769975
Root mean squared error: 0.17847198807953277
Relative absolute error: 12.530589658806932
Root relative squared error: 37.85962590686426
Weighted TruePositiveRate: 0.9398119122257054
Weighted MatthewsCorrelation: 0.9020292197559739
Weighted FMeasure: 0.9397601469866188
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9765505489773779
Mean absolute error: 0.055691509594698735
Coverage of cases: 98.36990595611286
Instances selection time: 74.0
Test time: 52.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9398119122257054
Weighted FalsePositiveRate: 0.03909065912909096
Kappa statistic: 0.9020048306396182
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 40.480668756530726
Incorrectly Classified Instances: 6.77115987460815
Correctly Classified Instances: 93.22884012539186
Weighted Precision: 0.9334939202161928
Weighted AreaUnderROC: 0.9880624390330451
Root mean squared error: 0.18406756660603724
Relative absolute error: 12.882139386993156
Root relative squared error: 39.04662736309019
Weighted TruePositiveRate: 0.9322884012539185
Weighted MatthewsCorrelation: 0.8918801889835242
Weighted FMeasure: 0.932309639911879
Iteration time: 58.0
Weighted AreaUnderPRC: 0.9800948963368591
Mean absolute error: 0.05725395283108199
Coverage of cases: 98.0564263322884
Instances selection time: 57.0
Test time: 52.0
Accumulative iteration time: 133.0
Weighted Recall: 0.9322884012539185
Weighted FalsePositiveRate: 0.03766575812152103
Kappa statistic: 0.890407926053025
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 40.6896551724135
Incorrectly Classified Instances: 6.081504702194358
Correctly Classified Instances: 93.91849529780565
Weighted Precision: 0.9404021687962475
Weighted AreaUnderROC: 0.9892278064645142
Root mean squared error: 0.181285660103111
Relative absolute error: 12.494515141716857
Root relative squared error: 38.45649587723637
Weighted TruePositiveRate: 0.9391849529780564
Weighted MatthewsCorrelation: 0.9029981341478003
Weighted FMeasure: 0.9384254077159621
Iteration time: 46.0
Weighted AreaUnderPRC: 0.9820919115253457
Mean absolute error: 0.055531178407631734
Coverage of cases: 98.18181818181819
Instances selection time: 44.0
Test time: 53.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9391849529780564
Weighted FalsePositiveRate: 0.04310001972729948
Kappa statistic: 0.9003625755097309
Training time: 2.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 40.982236154649556
Incorrectly Classified Instances: 6.4576802507836994
Correctly Classified Instances: 93.5423197492163
Weighted Precision: 0.9373412006006636
Weighted AreaUnderROC: 0.9891184124409906
Root mean squared error: 0.19163354999227664
Relative absolute error: 13.672695565967617
Root relative squared error: 40.651614810716566
Weighted TruePositiveRate: 0.935423197492163
Weighted MatthewsCorrelation: 0.8963662232109613
Weighted FMeasure: 0.9342615797373501
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9816485095279391
Mean absolute error: 0.06076753584874634
Coverage of cases: 98.0564263322884
Instances selection time: 22.0
Test time: 57.0
Accumulative iteration time: 203.0
Weighted Recall: 0.935423197492163
Weighted FalsePositiveRate: 0.05048500075393015
Kappa statistic: 0.8936533131020982
Training time: 2.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.57889237199569
Incorrectly Classified Instances: 4.137931034482759
Correctly Classified Instances: 95.86206896551724
Weighted Precision: 0.9586917253554648
Weighted AreaUnderROC: 0.9917114386842311
Root mean squared error: 0.15272199218385593
Relative absolute error: 9.13720144199279
Root relative squared error: 32.397226892856665
Weighted TruePositiveRate: 0.9586206896551724
Weighted MatthewsCorrelation: 0.9336671257697307
Weighted FMeasure: 0.9586494159710083
Iteration time: 7.0
Weighted AreaUnderPRC: 0.985169138390081
Mean absolute error: 0.04060978418663554
Coverage of cases: 98.80877742946709
Instances selection time: 4.0
Test time: 91.0
Accumulative iteration time: 210.0
Weighted Recall: 0.9586206896551724
Weighted FalsePositiveRate: 0.024076451078238748
Kappa statistic: 0.9328187900617502
Training time: 3.0
		
Time end:Sun Oct 08 07.10.07 EEST 2017