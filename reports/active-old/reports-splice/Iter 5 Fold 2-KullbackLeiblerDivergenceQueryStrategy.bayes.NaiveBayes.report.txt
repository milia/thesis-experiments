Sun Oct 08 07.10.11 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.10.11 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 40.647857889237294
Incorrectly Classified Instances: 8.338557993730408
Correctly Classified Instances: 91.6614420062696
Weighted Precision: 0.9166853686646722
Weighted AreaUnderROC: 0.9839160052520565
Root mean squared error: 0.2072406224819326
Relative absolute error: 15.032043944739456
Root relative squared error: 43.96237484828825
Weighted TruePositiveRate: 0.9166144200626959
Weighted MatthewsCorrelation: 0.8640072521749923
Weighted FMeasure: 0.9161658629252647
Iteration time: 76.0
Weighted AreaUnderPRC: 0.973367458774532
Mean absolute error: 0.06680908419884354
Coverage of cases: 97.17868338557994
Instances selection time: 75.0
Test time: 52.0
Accumulative iteration time: 76.0
Weighted Recall: 0.9166144200626959
Weighted FalsePositiveRate: 0.059307055281523344
Kappa statistic: 0.8633057412426365
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 41.442006269592596
Incorrectly Classified Instances: 9.0282131661442
Correctly Classified Instances: 90.9717868338558
Weighted Precision: 0.914692115558235
Weighted AreaUnderROC: 0.9881929604927662
Root mean squared error: 0.21643785761960402
Relative absolute error: 16.501734120050283
Root relative squared error: 45.91340304849262
Weighted TruePositiveRate: 0.909717868338558
Weighted MatthewsCorrelation: 0.8530293252844494
Weighted FMeasure: 0.9083331364309503
Iteration time: 58.0
Weighted AreaUnderPRC: 0.9792919081079418
Mean absolute error: 0.07334104053355849
Coverage of cases: 97.17868338557994
Instances selection time: 57.0
Test time: 52.0
Accumulative iteration time: 134.0
Weighted Recall: 0.909717868338558
Weighted FalsePositiveRate: 0.08149932477951588
Kappa statistic: 0.8491144143428023
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 41.692789968652164
Incorrectly Classified Instances: 10.9717868338558
Correctly Classified Instances: 89.0282131661442
Weighted Precision: 0.9005916603611802
Weighted AreaUnderROC: 0.987839904255186
Root mean squared error: 0.23517362818793391
Relative absolute error: 18.726647667019467
Root relative squared error: 49.887860174379
Weighted TruePositiveRate: 0.890282131661442
Weighted MatthewsCorrelation: 0.8214040552179646
Weighted FMeasure: 0.8881762047224518
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9788016470116528
Mean absolute error: 0.08322954518675507
Coverage of cases: 96.36363636363636
Instances selection time: 39.0
Test time: 52.0
Accumulative iteration time: 175.0
Weighted Recall: 0.890282131661442
Weighted FalsePositiveRate: 0.10576240794825731
Kappa statistic: 0.8144177387719823
Training time: 2.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 41.21212121212102
Incorrectly Classified Instances: 7.836990595611285
Correctly Classified Instances: 92.16300940438872
Weighted Precision: 0.9238311528613415
Weighted AreaUnderROC: 0.9903730030288792
Root mean squared error: 0.1961799643016818
Relative absolute error: 14.391032020115308
Root relative squared error: 41.616054927195734
Weighted TruePositiveRate: 0.9216300940438872
Weighted MatthewsCorrelation: 0.8726341848451602
Weighted FMeasure: 0.9201966526784633
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9837201397227634
Mean absolute error: 0.06396014231162504
Coverage of cases: 97.99373040752351
Instances selection time: 22.0
Test time: 52.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9216300940438872
Weighted FalsePositiveRate: 0.06343245263183099
Kappa statistic: 0.8704555481411548
Training time: 3.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.62068965517247
Incorrectly Classified Instances: 4.5141065830721
Correctly Classified Instances: 95.4858934169279
Weighted Precision: 0.9547858366792479
Weighted AreaUnderROC: 0.9933343892669686
Root mean squared error: 0.15241438807759136
Relative absolute error: 9.218852681771216
Root relative squared error: 32.331974208018515
Weighted TruePositiveRate: 0.954858934169279
Weighted MatthewsCorrelation: 0.9278252150887073
Weighted FMeasure: 0.9547880918618097
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9884222270182172
Mean absolute error: 0.04097267858565077
Coverage of cases: 98.68338557993731
Instances selection time: 5.0
Test time: 63.0
Accumulative iteration time: 208.0
Weighted Recall: 0.954858934169279
Weighted FalsePositiveRate: 0.028603515094134488
Kappa statistic: 0.9264827797242529
Training time: 3.0
		
Time end:Sun Oct 08 07.10.11 EEST 2017