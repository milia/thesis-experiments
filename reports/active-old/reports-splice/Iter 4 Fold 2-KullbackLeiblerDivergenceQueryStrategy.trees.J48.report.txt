Sun Oct 08 07.16.56 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.16.56 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 43.19749216301007
Incorrectly Classified Instances: 20.37617554858934
Correctly Classified Instances: 79.62382445141066
Weighted Precision: 0.7943323689403403
Weighted AreaUnderROC: 0.8493790454311402
Root mean squared error: 0.34694572004938695
Relative absolute error: 34.92623324818472
Root relative squared error: 73.59830140517047
Weighted TruePositiveRate: 0.7962382445141066
Weighted MatthewsCorrelation: 0.6664585190726372
Weighted FMeasure: 0.7949900451841722
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7532437515195192
Mean absolute error: 0.15522770332526895
Coverage of cases: 87.21003134796238
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7962382445141066
Weighted FalsePositiveRate: 0.12891462250750046
Kappa statistic: 0.6688089068122344
Training time: 18.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 55.50679205851557
Incorrectly Classified Instances: 17.24137931034483
Correctly Classified Instances: 82.75862068965517
Weighted Precision: 0.8288197735216877
Weighted AreaUnderROC: 0.8942222111083139
Root mean squared error: 0.3136714984911204
Relative absolute error: 32.12042236116513
Root relative squared error: 66.5397730944044
Weighted TruePositiveRate: 0.8275862068965517
Weighted MatthewsCorrelation: 0.7277193723944173
Weighted FMeasure: 0.819541268498031
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7928536198553378
Mean absolute error: 0.1427574327162927
Coverage of cases: 95.10971786833856
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8275862068965517
Weighted FalsePositiveRate: 0.10182689124832711
Kappa statistic: 0.7190457065956443
Training time: 22.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 55.00522466039673
Incorrectly Classified Instances: 19.059561128526646
Correctly Classified Instances: 80.94043887147335
Weighted Precision: 0.8233972627008596
Weighted AreaUnderROC: 0.8671582997003909
Root mean squared error: 0.3309104993859116
Relative absolute error: 34.304407797694104
Root relative squared error: 70.1967174244807
Weighted TruePositiveRate: 0.8094043887147335
Weighted MatthewsCorrelation: 0.696032371843031
Weighted FMeasure: 0.7842874703645634
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7755661016893873
Mean absolute error: 0.1524640346564217
Coverage of cases: 93.41692789968653
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8094043887147335
Weighted FalsePositiveRate: 0.14141090880832008
Kappa statistic: 0.6791292390238349
Training time: 26.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 52.288401253918316
Incorrectly Classified Instances: 14.231974921630094
Correctly Classified Instances: 85.7680250783699
Weighted Precision: 0.8649880174027231
Weighted AreaUnderROC: 0.9223077413417756
Root mean squared error: 0.28849651978656776
Relative absolute error: 26.59449687078069
Root relative squared error: 61.19935364693963
Weighted TruePositiveRate: 0.857680250783699
Weighted MatthewsCorrelation: 0.7808498345355191
Weighted FMeasure: 0.8504242922276646
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8393634315152741
Mean absolute error: 0.11819776387013908
Coverage of cases: 96.55172413793103
Instances selection time: 10.0
Test time: 4.0
Accumulative iteration time: 115.0
Weighted Recall: 0.857680250783699
Weighted FalsePositiveRate: 0.08752107620346233
Kappa statistic: 0.766160751768011
Training time: 35.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 48.171368861023225
Incorrectly Classified Instances: 7.460815047021944
Correctly Classified Instances: 92.53918495297806
Weighted Precision: 0.926676120952985
Weighted AreaUnderROC: 0.9584106455529555
Root mean squared error: 0.20947648881168474
Relative absolute error: 16.761434597999763
Root relative squared error: 44.43667372136657
Weighted TruePositiveRate: 0.9253918495297806
Weighted MatthewsCorrelation: 0.8828816305027729
Weighted FMeasure: 0.9257446317139716
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9098736642575207
Mean absolute error: 0.07449526488000063
Coverage of cases: 96.61442006269593
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 215.0
Weighted Recall: 0.9253918495297806
Weighted FalsePositiveRate: 0.03747101103542328
Kappa statistic: 0.8796189768263122
Training time: 99.0
		
Time end:Sun Oct 08 07.16.56 EEST 2017