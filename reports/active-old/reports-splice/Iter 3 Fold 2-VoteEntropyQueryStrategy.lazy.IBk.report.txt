Sun Oct 08 09.49.34 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.49.34 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 39.03866248693846
Incorrectly Classified Instances: 32.539184952978054
Correctly Classified Instances: 67.46081504702194
Weighted Precision: 0.730131178571241
Weighted AreaUnderROC: 0.8125518839301353
Root mean squared error: 0.4294588664943566
Relative absolute error: 47.89436409353361
Root relative squared error: 91.1019830216533
Weighted TruePositiveRate: 0.6746081504702194
Weighted MatthewsCorrelation: 0.526778855873136
Weighted FMeasure: 0.6763331978049438
Iteration time: 648.0
Weighted AreaUnderPRC: 0.6787536820012857
Mean absolute error: 0.21286384041570974
Coverage of cases: 75.23510971786834
Instances selection time: 644.0
Test time: 806.0
Accumulative iteration time: 648.0
Weighted Recall: 0.6746081504702194
Weighted FalsePositiveRate: 0.13556930516331056
Kappa statistic: 0.5117421816109933
Training time: 4.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 37.97283176593522
Incorrectly Classified Instances: 26.018808777429467
Correctly Classified Instances: 73.98119122257053
Weighted Precision: 0.7404560485063262
Weighted AreaUnderROC: 0.8164170493212687
Root mean squared error: 0.39705228442398843
Relative absolute error: 40.44781957627003
Root relative squared error: 84.22750884054265
Weighted TruePositiveRate: 0.7398119122257053
Weighted MatthewsCorrelation: 0.5759476610740143
Weighted FMeasure: 0.7400749012852627
Iteration time: 1196.0
Weighted AreaUnderPRC: 0.6917295746634604
Mean absolute error: 0.17976808700564864
Coverage of cases: 78.74608150470219
Instances selection time: 1195.0
Test time: 1589.0
Accumulative iteration time: 1844.0
Weighted Recall: 0.7398119122257053
Weighted FalsePositiveRate: 0.16347821762069567
Kappa statistic: 0.577572695085248
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 37.17868338557991
Incorrectly Classified Instances: 24.764890282131663
Correctly Classified Instances: 75.23510971786834
Weighted Precision: 0.750563201643793
Weighted AreaUnderROC: 0.8147575398002851
Root mean squared error: 0.39327444730798367
Relative absolute error: 38.94148100699326
Root relative squared error: 83.42610856765911
Weighted TruePositiveRate: 0.7523510971786834
Weighted MatthewsCorrelation: 0.5885513303671649
Weighted FMeasure: 0.7481844901066304
Iteration time: 986.0
Weighted AreaUnderPRC: 0.6938393228449042
Mean absolute error: 0.17307324891997397
Coverage of cases: 78.87147335423198
Instances selection time: 985.0
Test time: 2570.0
Accumulative iteration time: 2830.0
Weighted Recall: 0.7523510971786834
Weighted FalsePositiveRate: 0.18236546202866963
Kappa statistic: 0.5852675773741502
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.24451410658292
Incorrectly Classified Instances: 27.460815047021942
Correctly Classified Instances: 72.53918495297806
Weighted Precision: 0.7708279337739147
Weighted AreaUnderROC: 0.8375769016836263
Root mean squared error: 0.39409051070638323
Relative absolute error: 40.04191199464181
Root relative squared error: 83.59922175652505
Weighted TruePositiveRate: 0.7253918495297805
Weighted MatthewsCorrelation: 0.5905914916100958
Weighted FMeasure: 0.7266491614271562
Iteration time: 641.0
Weighted AreaUnderPRC: 0.7214370950686
Mean absolute error: 0.1779640533095232
Coverage of cases: 79.18495297805643
Instances selection time: 641.0
Test time: 2900.0
Accumulative iteration time: 3471.0
Weighted Recall: 0.7253918495297805
Weighted FalsePositiveRate: 0.13851891393935942
Kappa statistic: 0.5666572795696656
Training time: 0.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 39.122257053291484
Incorrectly Classified Instances: 25.893416927899686
Correctly Classified Instances: 74.1065830721003
Weighted Precision: 0.7892738936413941
Weighted AreaUnderROC: 0.8647548591822436
Root mean squared error: 0.3798385931668593
Relative absolute error: 38.27152715048514
Root relative squared error: 80.57593349539242
Weighted TruePositiveRate: 0.7410658307210032
Weighted MatthewsCorrelation: 0.6236000310282759
Weighted FMeasure: 0.7431993399096972
Iteration time: 160.0
Weighted AreaUnderPRC: 0.759936166187592
Mean absolute error: 0.17009567622438226
Coverage of cases: 81.44200626959248
Instances selection time: 159.0
Test time: 3563.0
Accumulative iteration time: 3631.0
Weighted Recall: 0.7410658307210032
Weighted FalsePositiveRate: 0.10306435122747162
Kappa statistic: 0.6077739631004396
Training time: 1.0
		
Time end:Sun Oct 08 09.49.49 EEST 2017