Sun Oct 08 09.48.36 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.48.36 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 39.602925809822416
Incorrectly Classified Instances: 33.793103448275865
Correctly Classified Instances: 66.20689655172414
Weighted Precision: 0.7357764134378653
Weighted AreaUnderROC: 0.8031574212207715
Root mean squared error: 0.43688104454815957
Relative absolute error: 49.869465983118864
Root relative squared error: 92.67646475155868
Weighted TruePositiveRate: 0.6620689655172414
Weighted MatthewsCorrelation: 0.5224499825564469
Weighted FMeasure: 0.6630552627456416
Iteration time: 652.0
Weighted AreaUnderPRC: 0.675009732309375
Mean absolute error: 0.22164207103608888
Coverage of cases: 74.1065830721003
Instances selection time: 652.0
Test time: 804.0
Accumulative iteration time: 652.0
Weighted Recall: 0.6620689655172414
Weighted FalsePositiveRate: 0.1314479653108004
Kappa statistic: 0.4988197819584332
Training time: 0.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 39.16405433646817
Incorrectly Classified Instances: 27.648902821316614
Correctly Classified Instances: 72.35109717868339
Weighted Precision: 0.7334417324985678
Weighted AreaUnderROC: 0.8209751162201114
Root mean squared error: 0.3967436268134926
Relative absolute error: 41.54318063399961
Root relative squared error: 84.16203267370874
Weighted TruePositiveRate: 0.7235109717868339
Weighted MatthewsCorrelation: 0.5596240596762285
Weighted FMeasure: 0.725695678822519
Iteration time: 916.0
Weighted AreaUnderPRC: 0.7004256432179449
Mean absolute error: 0.1846363583733358
Coverage of cases: 79.43573667711598
Instances selection time: 916.0
Test time: 1495.0
Accumulative iteration time: 1568.0
Weighted Recall: 0.7235109717868339
Weighted FalsePositiveRate: 0.15769178826082228
Kappa statistic: 0.5590851421794412
Training time: 0.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 38.59979101358415
Incorrectly Classified Instances: 26.269592476489027
Correctly Classified Instances: 73.73040752351098
Weighted Precision: 0.7370754185027676
Weighted AreaUnderROC: 0.8174054986584574
Root mean squared error: 0.39250140988166254
Relative absolute error: 40.234404767309165
Root relative squared error: 83.2621225657803
Weighted TruePositiveRate: 0.7373040752351098
Weighted MatthewsCorrelation: 0.5640514600453483
Weighted FMeasure: 0.7347024179380308
Iteration time: 908.0
Weighted AreaUnderPRC: 0.698932108396726
Mean absolute error: 0.17881957674360036
Coverage of cases: 80.0626959247649
Instances selection time: 907.0
Test time: 2429.0
Accumulative iteration time: 2476.0
Weighted Recall: 0.7373040752351098
Weighted FalsePositiveRate: 0.18475517453321047
Kappa statistic: 0.5647647708926652
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.45350052246594
Incorrectly Classified Instances: 29.968652037617556
Correctly Classified Instances: 70.03134796238244
Weighted Precision: 0.7507803348513318
Weighted AreaUnderROC: 0.820139976460985
Root mean squared error: 0.41488591843691
Relative absolute error: 43.94678315793681
Root relative squared error: 88.01059390366338
Weighted TruePositiveRate: 0.7003134796238244
Weighted MatthewsCorrelation: 0.5532861152623135
Weighted FMeasure: 0.7009514815799087
Iteration time: 670.0
Weighted AreaUnderPRC: 0.6952034260221446
Mean absolute error: 0.19531903625750136
Coverage of cases: 77.3667711598746
Instances selection time: 669.0
Test time: 2920.0
Accumulative iteration time: 3146.0
Weighted Recall: 0.7003134796238244
Weighted FalsePositiveRate: 0.1502816533094837
Kappa statistic: 0.5308214394066667
Training time: 1.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.26541274817135
Incorrectly Classified Instances: 26.83385579937304
Correctly Classified Instances: 73.16614420062696
Weighted Precision: 0.7912509615252451
Weighted AreaUnderROC: 0.8582209051184846
Root mean squared error: 0.3873132657635342
Relative absolute error: 38.71549026634446
Root relative squared error: 82.16155099946982
Weighted TruePositiveRate: 0.7316614420062696
Weighted MatthewsCorrelation: 0.6169369451363107
Weighted FMeasure: 0.7319753066966389
Iteration time: 174.0
Weighted AreaUnderPRC: 0.7507276080888818
Mean absolute error: 0.17206884562820152
Coverage of cases: 80.37617554858934
Instances selection time: 173.0
Test time: 3571.0
Accumulative iteration time: 3320.0
Weighted Recall: 0.7316614420062696
Weighted FalsePositiveRate: 0.10175954446979836
Kappa statistic: 0.5977723282728356
Training time: 1.0
		
Time end:Sun Oct 08 09.48.51 EEST 2017