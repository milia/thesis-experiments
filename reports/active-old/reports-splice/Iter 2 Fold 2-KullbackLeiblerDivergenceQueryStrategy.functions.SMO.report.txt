Sun Oct 08 07.11.19 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.11.19 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 66.771159874607
Incorrectly Classified Instances: 8.463949843260188
Correctly Classified Instances: 91.53605015673982
Weighted Precision: 0.9158735086654078
Weighted AreaUnderROC: 0.9467230511229353
Root mean squared error: 0.3084776837396496
Relative absolute error: 54.79623824451408
Root relative squared error: 65.43799860510688
Weighted TruePositiveRate: 0.9153605015673981
Weighted MatthewsCorrelation: 0.8647331261040241
Weighted FMeasure: 0.915480304548787
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.8787770565403819
Mean absolute error: 0.2435388366422903
Coverage of cases: 99.05956112852665
Instances selection time: 307.0
Test time: 186.0
Accumulative iteration time: 1060.0
Weighted Recall: 0.9153605015673981
Weighted FalsePositiveRate: 0.04827299640637417
Kappa statistic: 0.8628939828080229
Training time: 753.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 67.0428422152549
Incorrectly Classified Instances: 9.592476489028213
Correctly Classified Instances: 90.40752351097179
Weighted Precision: 0.9095146638874188
Weighted AreaUnderROC: 0.9482336469693518
Root mean squared error: 0.3117721990467672
Relative absolute error: 55.39184952978054
Root relative squared error: 66.1368708394226
Weighted TruePositiveRate: 0.9040752351097179
Weighted MatthewsCorrelation: 0.8511357646682026
Weighted FMeasure: 0.9050255706108076
Iteration time: 2347.0
Weighted AreaUnderPRC: 0.8759530836077298
Mean absolute error: 0.24618599791014134
Coverage of cases: 99.24764890282131
Instances selection time: 239.0
Test time: 197.0
Accumulative iteration time: 3407.0
Weighted Recall: 0.9040752351097179
Weighted FalsePositiveRate: 0.044706686389651806
Kappa statistic: 0.8463468537104432
Training time: 2108.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 66.9592476489017
Incorrectly Classified Instances: 9.153605015673982
Correctly Classified Instances: 90.84639498432603
Weighted Precision: 0.9107660396980821
Weighted AreaUnderROC: 0.9442604202799226
Root mean squared error: 0.311175790659304
Relative absolute error: 55.266457680250774
Root relative squared error: 66.01035351488308
Weighted TruePositiveRate: 0.9084639498432602
Weighted MatthewsCorrelation: 0.8538211124328787
Weighted FMeasure: 0.9077419879159719
Iteration time: 3469.0
Weighted AreaUnderPRC: 0.8748648085796518
Mean absolute error: 0.24562870080112015
Coverage of cases: 99.05956112852665
Instances selection time: 181.0
Test time: 217.0
Accumulative iteration time: 6876.0
Weighted Recall: 0.9084639498432602
Weighted FalsePositiveRate: 0.05998623686500531
Kappa statistic: 0.8503717734867546
Training time: 3288.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 67.0010449320783
Incorrectly Classified Instances: 9.655172413793103
Correctly Classified Instances: 90.34482758620689
Weighted Precision: 0.9062228017538624
Weighted AreaUnderROC: 0.9381391984784201
Root mean squared error: 0.3150322631597201
Relative absolute error: 55.83072100313479
Root relative squared error: 66.82843487183416
Weighted TruePositiveRate: 0.903448275862069
Weighted MatthewsCorrelation: 0.8461623503334422
Weighted FMeasure: 0.9020213013054886
Iteration time: 5536.0
Weighted AreaUnderPRC: 0.8651991288639772
Mean absolute error: 0.24813653779171582
Coverage of cases: 98.62068965517241
Instances selection time: 102.0
Test time: 221.0
Accumulative iteration time: 12412.0
Weighted Recall: 0.903448275862069
Weighted FalsePositiveRate: 0.06776882705942652
Kappa statistic: 0.841178395140756
Training time: 5434.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 67.0010449320783
Incorrectly Classified Instances: 7.210031347962382
Correctly Classified Instances: 92.78996865203762
Weighted Precision: 0.9288451273454781
Weighted AreaUnderROC: 0.9565026916465619
Root mean squared error: 0.3030860276175785
Relative absolute error: 54.16927899686519
Root relative squared error: 64.29425562338417
Weighted TruePositiveRate: 0.9278996865203761
Weighted MatthewsCorrelation: 0.8861347922477192
Weighted FMeasure: 0.9282037698020086
Iteration time: 8734.0
Weighted AreaUnderPRC: 0.8981961587999546
Mean absolute error: 0.24075235109718407
Coverage of cases: 99.3730407523511
Instances selection time: 21.0
Test time: 231.0
Accumulative iteration time: 21146.0
Weighted Recall: 0.9278996865203761
Weighted FalsePositiveRate: 0.03762843170342049
Kappa statistic: 0.8835017373912899
Training time: 8713.0
		
Time end:Sun Oct 08 07.11.41 EEST 2017