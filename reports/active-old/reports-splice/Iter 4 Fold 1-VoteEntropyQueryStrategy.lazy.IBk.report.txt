Sun Oct 08 09.49.49 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 09.49.49 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 39.331243469174616
Incorrectly Classified Instances: 33.040752351097176
Correctly Classified Instances: 66.95924764890282
Weighted Precision: 0.7238595757144829
Weighted AreaUnderROC: 0.8045356389143297
Root mean squared error: 0.4353303812591816
Relative absolute error: 49.29425923329823
Root relative squared error: 92.34751939346668
Weighted TruePositiveRate: 0.6695924764890282
Weighted MatthewsCorrelation: 0.5169725784652572
Weighted FMeasure: 0.6697306923743972
Iteration time: 630.0
Weighted AreaUnderPRC: 0.670566400778027
Mean absolute error: 0.21908559659244153
Coverage of cases: 74.54545454545455
Instances selection time: 629.0
Test time: 788.0
Accumulative iteration time: 630.0
Weighted Recall: 0.6695924764890282
Weighted FalsePositiveRate: 0.13952014457884868
Kappa statistic: 0.5049891818613322
Training time: 1.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 38.85057471264372
Incorrectly Classified Instances: 43.322884012539184
Correctly Classified Instances: 56.677115987460816
Weighted Precision: 0.7272092452828357
Weighted AreaUnderROC: 0.7580053423668243
Root mean squared error: 0.5048469913824637
Relative absolute error: 63.09818469811599
Root relative squared error: 107.09421932044877
Weighted TruePositiveRate: 0.5667711598746081
Weighted MatthewsCorrelation: 0.438809447755037
Weighted FMeasure: 0.5550879523296217
Iteration time: 924.0
Weighted AreaUnderPRC: 0.6223748499545606
Mean absolute error: 0.28043637643607744
Coverage of cases: 65.141065830721
Instances selection time: 923.0
Test time: 1469.0
Accumulative iteration time: 1554.0
Weighted Recall: 0.5667711598746081
Weighted FalsePositiveRate: 0.1530708620135664
Kappa statistic: 0.3845213524006093
Training time: 1.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 38.49529780564256
Incorrectly Classified Instances: 31.974921630094045
Correctly Classified Instances: 68.02507836990596
Weighted Precision: 0.7584869676443162
Weighted AreaUnderROC: 0.804286972105703
Root mean squared error: 0.43793591649516844
Relative absolute error: 48.40683744064458
Root relative squared error: 92.90023688366271
Weighted TruePositiveRate: 0.6802507836990596
Weighted MatthewsCorrelation: 0.5473077569456013
Weighted FMeasure: 0.6854647252567
Iteration time: 938.0
Weighted AreaUnderPRC: 0.6784384069473993
Mean absolute error: 0.21514149973620303
Coverage of cases: 73.98119122257053
Instances selection time: 937.0
Test time: 2151.0
Accumulative iteration time: 2492.0
Weighted Recall: 0.6802507836990596
Weighted FalsePositiveRate: 0.13672136360317905
Kappa statistic: 0.5150573948605444
Training time: 1.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 38.160919540229756
Incorrectly Classified Instances: 27.899686520376175
Correctly Classified Instances: 72.10031347962382
Weighted Precision: 0.7712083256565171
Weighted AreaUnderROC: 0.8179045102943613
Root mean squared error: 0.4137200772525148
Relative absolute error: 43.42970536649436
Root relative squared error: 87.76328164148165
Weighted TruePositiveRate: 0.7210031347962382
Weighted MatthewsCorrelation: 0.5860555128940002
Weighted FMeasure: 0.722855015320512
Iteration time: 674.0
Weighted AreaUnderPRC: 0.6961739109020701
Mean absolute error: 0.19302091273997932
Coverage of cases: 76.7398119122257
Instances selection time: 673.0
Test time: 2818.0
Accumulative iteration time: 3166.0
Weighted Recall: 0.7210031347962382
Weighted FalsePositiveRate: 0.13623841228727693
Kappa statistic: 0.564403958675221
Training time: 1.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 37.74294670846401
Incorrectly Classified Instances: 26.4576802507837
Correctly Classified Instances: 73.5423197492163
Weighted Precision: 0.7849175727267346
Weighted AreaUnderROC: 0.8557155690713184
Root mean squared error: 0.3929292479919942
Relative absolute error: 39.12928115312457
Root relative squared error: 83.35288073449998
Weighted TruePositiveRate: 0.735423197492163
Weighted MatthewsCorrelation: 0.6147794979718825
Weighted FMeasure: 0.7351519918312153
Iteration time: 167.0
Weighted AreaUnderPRC: 0.744202584827778
Mean absolute error: 0.17390791623611318
Coverage of cases: 79.3730407523511
Instances selection time: 166.0
Test time: 3493.0
Accumulative iteration time: 3333.0
Weighted Recall: 0.735423197492163
Weighted FalsePositiveRate: 0.10481041933722053
Kappa statistic: 0.6013180215697795
Training time: 1.0
		
Time end:Sun Oct 08 09.50.04 EEST 2017