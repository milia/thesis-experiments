Sun Oct 08 07.10.04 EEST 2017
Dataset: splice
Test set size: 1595
Initial Labelled set size: 319
Initial Unlabelled set size: 1276
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 07.10.04 EEST 2017
		
Iteration: 1
Labeled set size: 319
Unlabelled set size: 1276
	
Mean region size: 40.37617554858957
Incorrectly Classified Instances: 7.021943573667712
Correctly Classified Instances: 92.97805642633229
Weighted Precision: 0.9298863505001996
Weighted AreaUnderROC: 0.9874631582705381
Root mean squared error: 0.18906936869846094
Relative absolute error: 13.129477625211887
Root relative squared error: 40.10766981640194
Weighted TruePositiveRate: 0.9297805642633229
Weighted MatthewsCorrelation: 0.8867556507242509
Weighted FMeasure: 0.9297058746539083
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9774520619590195
Mean absolute error: 0.058353233889831935
Coverage of cases: 97.86833855799372
Instances selection time: 75.0
Test time: 53.0
Accumulative iteration time: 77.0
Weighted Recall: 0.9297805642633229
Weighted FalsePositiveRate: 0.044176810478572
Kappa statistic: 0.8857047068751843
Training time: 2.0
		
Iteration: 2
Labeled set size: 619
Unlabelled set size: 976
	
Mean region size: 40.91954022988525
Incorrectly Classified Instances: 8.401253918495298
Correctly Classified Instances: 91.5987460815047
Weighted Precision: 0.9194417442947755
Weighted AreaUnderROC: 0.9872063420517426
Root mean squared error: 0.2121567918235763
Relative absolute error: 15.902516412259594
Root relative squared error: 45.00525185196953
Weighted TruePositiveRate: 0.915987460815047
Weighted MatthewsCorrelation: 0.8630087047963662
Weighted FMeasure: 0.9149215430585268
Iteration time: 59.0
Weighted AreaUnderPRC: 0.976046646310698
Mean absolute error: 0.07067785072115536
Coverage of cases: 97.30407523510972
Instances selection time: 56.0
Test time: 53.0
Accumulative iteration time: 136.0
Weighted Recall: 0.915987460815047
Weighted FalsePositiveRate: 0.07379544491909326
Kappa statistic: 0.8601623250229815
Training time: 3.0
		
Iteration: 3
Labeled set size: 919
Unlabelled set size: 676
	
Mean region size: 41.63009404388731
Incorrectly Classified Instances: 9.968652037617554
Correctly Classified Instances: 90.03134796238244
Weighted Precision: 0.9081485161021378
Weighted AreaUnderROC: 0.9868654151641995
Root mean squared error: 0.22714033358213878
Relative absolute error: 17.634755136497017
Root relative squared error: 48.1837410470709
Weighted TruePositiveRate: 0.9003134796238245
Weighted MatthewsCorrelation: 0.8372903912984794
Weighted FMeasure: 0.8985285412892288
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9751661185286284
Mean absolute error: 0.07837668949554408
Coverage of cases: 96.92789968652038
Instances selection time: 80.0
Test time: 103.0
Accumulative iteration time: 221.0
Weighted Recall: 0.9003134796238245
Weighted FalsePositiveRate: 0.0940077835737281
Kappa statistic: 0.8323727121904145
Training time: 5.0
		
Iteration: 4
Labeled set size: 1219
Unlabelled set size: 376
	
Mean region size: 40.45977011494242
Incorrectly Classified Instances: 7.021943573667712
Correctly Classified Instances: 92.97805642633229
Weighted Precision: 0.930714683961729
Weighted AreaUnderROC: 0.9894319507128146
Root mean squared error: 0.19052146146500404
Relative absolute error: 13.269549905038817
Root relative squared error: 40.4157052090423
Weighted TruePositiveRate: 0.9297805642633229
Weighted MatthewsCorrelation: 0.8855789887285919
Weighted FMeasure: 0.9289313929048394
Iteration time: 50.0
Weighted AreaUnderPRC: 0.9784163706623914
Mean absolute error: 0.05897577735572941
Coverage of cases: 97.80564263322884
Instances selection time: 44.0
Test time: 105.0
Accumulative iteration time: 271.0
Weighted Recall: 0.9297805642633229
Weighted FalsePositiveRate: 0.05513215901972256
Kappa statistic: 0.8843608841498545
Training time: 6.0
		
Iteration: 5
Labeled set size: 1519
Unlabelled set size: 76
	
Mean region size: 38.746081504702346
Incorrectly Classified Instances: 4.952978056426332
Correctly Classified Instances: 95.04702194357367
Weighted Precision: 0.9504150410813024
Weighted AreaUnderROC: 0.9931614983353136
Root mean squared error: 0.1623732252460794
Relative absolute error: 9.871379770125534
Root relative squared error: 34.44456259638965
Weighted TruePositiveRate: 0.9504702194357367
Weighted MatthewsCorrelation: 0.9201561704568835
Weighted FMeasure: 0.9504315094559984
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9872811245266511
Mean absolute error: 0.0438727989783367
Coverage of cases: 98.36990595611286
Instances selection time: 13.0
Test time: 102.0
Accumulative iteration time: 292.0
Weighted Recall: 0.9504702194357367
Weighted FalsePositiveRate: 0.031424105686768886
Kappa statistic: 0.9193809985994604
Training time: 8.0
		
Time end:Sun Oct 08 07.10.05 EEST 2017