Sat Oct 07 12.54.15 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.15 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 92.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8385454545454546
Weighted AreaUnderROC: 0.8806689342403629
Root mean squared error: 0.3673845785705062
Relative absolute error: 57.828571428571415
Root relative squared error: 73.47691571410124
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6462303276414968
Weighted FMeasure: 0.8373348918760957
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8699456440091987
Mean absolute error: 0.2891428571428571
Coverage of cases: 98.85714285714286
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.215
Kappa statistic: 0.6428571428571428
Training time: 12.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 88.28571428571429
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8993875657284255
Weighted AreaUnderROC: 0.9278628117913832
Root mean squared error: 0.3128669274399488
Relative absolute error: 45.59999999999998
Root relative squared error: 62.57338548798976
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7751798973474079
Weighted FMeasure: 0.8949071618037135
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9228604156232887
Mean absolute error: 0.2279999999999999
Coverage of cases: 98.28571428571429
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.15507936507936507
Kappa statistic: 0.7687564234326824
Training time: 9.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 82.57142857142857
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9156797331109257
Weighted AreaUnderROC: 0.9462868480725625
Root mean squared error: 0.27339139917498695
Relative absolute error: 32.91428571428572
Root relative squared error: 54.67827983499739
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8164593389134399
Weighted FMeasure: 0.9147006208565716
Iteration time: 11.0
Weighted AreaUnderPRC: 0.937377924726718
Mean absolute error: 0.1645714285714286
Coverage of cases: 99.42857142857143
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.08988095238095238
Kappa statistic: 0.8159057437407953
Training time: 9.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 82.0
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9081622088006903
Weighted AreaUnderROC: 0.9517431972789115
Root mean squared error: 0.27474663653212245
Relative absolute error: 34.85714285714286
Root relative squared error: 54.949327306424486
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8004494800714647
Weighted FMeasure: 0.9082386525835
Iteration time: 31.0
Weighted AreaUnderPRC: 0.947046015901801
Mean absolute error: 0.1742857142857143
Coverage of cases: 99.42857142857143
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 73.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.11392857142857143
Kappa statistic: 0.8001998001998002
Training time: 26.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 80.57142857142857
Incorrectly Classified Instances: 6.285714285714286
Correctly Classified Instances: 93.71428571428571
Weighted Precision: 0.9369854410505282
Weighted AreaUnderROC: 0.9615929705215418
Root mean squared error: 0.2417791199776002
Relative absolute error: 29.828571428571433
Root relative squared error: 48.35582399552004
Weighted TruePositiveRate: 0.9371428571428572
Weighted MatthewsCorrelation: 0.8631828139903247
Weighted FMeasure: 0.9370311111111111
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9588736979358016
Mean absolute error: 0.14914285714285716
Coverage of cases: 98.85714285714286
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 103.0
Weighted Recall: 0.9371428571428572
Weighted FalsePositiveRate: 0.07702380952380952
Kappa statistic: 0.8631159780985566
Training time: 29.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 76.28571428571429
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9311935576646535
Weighted AreaUnderROC: 0.9540816326530612
Root mean squared error: 0.24773257470794693
Relative absolute error: 26.97142857142859
Root relative squared error: 49.546514941589386
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.850415115531606
Weighted FMeasure: 0.9311789894376248
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9477787814558541
Mean absolute error: 0.13485714285714295
Coverage of cases: 98.28571428571429
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 139.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.08718253968253967
Kappa statistic: 0.85014985014985
Training time: 29.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 72.85714285714286
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9146396396396397
Weighted AreaUnderROC: 0.9725765306122449
Root mean squared error: 0.23772733480667652
Relative absolute error: 25.02857142857143
Root relative squared error: 47.5454669613353
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8146939463542284
Weighted FMeasure: 0.9144309876063696
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9701507869158518
Mean absolute error: 0.12514285714285714
Coverage of cases: 99.42857142857143
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 187.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.09682539682539681
Kappa statistic: 0.81463173504696
Training time: 44.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 72.28571428571429
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.92
Weighted AreaUnderROC: 0.9738520408163265
Root mean squared error: 0.2361597522259636
Relative absolute error: 25.142857142857146
Root relative squared error: 47.23195044519272
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8263888888888888
Weighted FMeasure: 0.92
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9707938618204855
Mean absolute error: 0.12571428571428572
Coverage of cases: 99.42857142857143
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 230.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.0936111111111111
Kappa statistic: 0.826388888888889
Training time: 41.0
		
Time end:Sat Oct 07 12.54.16 EEST 2017