Sat Oct 07 12.54.43 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.43 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.845224093634028
Weighted AreaUnderROC: 0.917375283446712
Root mean squared error: 0.3805329680513955
Relative absolute error: 30.912757657556256
Root relative squared error: 76.1065936102791
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6640633643962476
Weighted FMeasure: 0.84544
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9252510518809134
Mean absolute error: 0.15456378828778128
Coverage of cases: 88.0
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.18400793650793648
Kappa statistic: 0.6640119462419115
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 53.714285714285715
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8909106984969053
Weighted AreaUnderROC: 0.9275793650793651
Root mean squared error: 0.30246059478839543
Relative absolute error: 21.137034368344658
Root relative squared error: 60.49211895767909
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7617400717626528
Weighted FMeasure: 0.8903706376989425
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9309821564306731
Mean absolute error: 0.10568517184172328
Coverage of cases: 93.14285714285714
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1444047619047619
Kappa statistic: 0.7602221100454316
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 54.285714285714285
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9258444022770397
Weighted AreaUnderROC: 0.9431689342403629
Root mean squared error: 0.275337281423888
Relative absolute error: 18.329988145603743
Root relative squared error: 55.067456284777606
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8288945932684056
Weighted FMeasure: 0.9178233719892952
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9474770265895209
Mean absolute error: 0.09164994072801871
Coverage of cases: 94.28571428571429
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.13527777777777777
Kappa statistic: 0.818840579710145
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 53.714285714285715
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9234642746674916
Weighted AreaUnderROC: 0.9384920634920635
Root mean squared error: 0.27731834044198145
Relative absolute error: 17.82961772960076
Root relative squared error: 55.46366808839629
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8269967354053899
Weighted FMeasure: 0.9182611258473328
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9462925987663355
Mean absolute error: 0.0891480886480038
Coverage of cases: 93.14285714285714
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 18.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.12833333333333333
Kappa statistic: 0.8201438848920864
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9165289256198347
Weighted AreaUnderROC: 0.941609977324263
Root mean squared error: 0.28414162167170975
Relative absolute error: 18.34598814466793
Root relative squared error: 56.82832433434195
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8134037908484545
Weighted FMeasure: 0.9126444371079564
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9510419713611444
Mean absolute error: 0.09172994072333966
Coverage of cases: 92.57142857142857
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.13154761904761905
Kappa statistic: 0.8079877112135176
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 55.42857142857143
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9165289256198347
Weighted AreaUnderROC: 0.9411848072562358
Root mean squared error: 0.2866938167461024
Relative absolute error: 19.128737365741824
Root relative squared error: 57.33876334922048
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8134037908484545
Weighted FMeasure: 0.9126444371079564
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9507755684665091
Mean absolute error: 0.09564368682870912
Coverage of cases: 94.28571428571429
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.13154761904761905
Kappa statistic: 0.8079877112135176
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 55.42857142857143
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.884979544126242
Weighted AreaUnderROC: 0.9366496598639457
Root mean squared error: 0.316105950363878
Relative absolute error: 23.511825459766005
Root relative squared error: 63.2211900727756
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7494396893960719
Weighted FMeasure: 0.8848432556801842
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9469524918222152
Mean absolute error: 0.11755912729883002
Coverage of cases: 92.57142857142857
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.14761904761904762
Kappa statistic: 0.7484909456740442
Training time: 1.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 54.285714285714285
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.884979544126242
Weighted AreaUnderROC: 0.9304138321995464
Root mean squared error: 0.3231276893628635
Relative absolute error: 23.841588663297706
Root relative squared error: 64.6255378725727
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7494396893960719
Weighted FMeasure: 0.8848432556801842
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9416269933166271
Mean absolute error: 0.11920794331648854
Coverage of cases: 90.85714285714286
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.14761904761904762
Kappa statistic: 0.7484909456740442
Training time: 1.0
		
Time end:Sat Oct 07 12.54.43 EEST 2017