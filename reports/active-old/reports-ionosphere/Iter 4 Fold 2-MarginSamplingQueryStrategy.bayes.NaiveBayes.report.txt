Sat Oct 07 12.54.45 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.45 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 58.285714285714285
Incorrectly Classified Instances: 17.142857142857142
Correctly Classified Instances: 82.85714285714286
Weighted Precision: 0.8614379084967321
Weighted AreaUnderROC: 0.9668367346938774
Root mean squared error: 0.3761357733050777
Relative absolute error: 34.01046657080021
Root relative squared error: 75.22715466101553
Weighted TruePositiveRate: 0.8285714285714286
Weighted MatthewsCorrelation: 0.6764666413954515
Weighted FMeasure: 0.8319775220765321
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9720207997009572
Mean absolute error: 0.17005233285400106
Coverage of cases: 92.0
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8285714285714286
Weighted FalsePositiveRate: 0.12420634920634921
Kappa statistic: 0.6543778801843319
Training time: 1.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 58.0
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9349302707136998
Weighted AreaUnderROC: 0.985827664399093
Root mean squared error: 0.25209205855093453
Relative absolute error: 18.879010428829364
Root relative squared error: 50.4184117101869
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.8565054899774425
Weighted FMeasure: 0.9320433694745621
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9877293544664492
Mean absolute error: 0.09439505214414683
Coverage of cases: 97.14285714285714
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.0594047619047619
Kappa statistic: 0.8542274052478134
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 5.714285714285714
Correctly Classified Instances: 94.28571428571429
Weighted Precision: 0.9434965034965035
Weighted AreaUnderROC: 0.9832766439909297
Root mean squared error: 0.21492717783179888
Relative absolute error: 13.824367522104437
Root relative squared error: 42.985435566359776
Weighted TruePositiveRate: 0.9428571428571428
Weighted MatthewsCorrelation: 0.8771133873838058
Weighted FMeasure: 0.9430461711711711
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9855309000225332
Mean absolute error: 0.06912183761052218
Coverage of cases: 98.28571428571429
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9428571428571428
Weighted FalsePositiveRate: 0.059920634920634916
Kappa statistic: 0.876847290640394
Training time: 0.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 56.57142857142857
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9333112216694307
Weighted AreaUnderROC: 0.9742063492063492
Root mean squared error: 0.2290468745134411
Relative absolute error: 15.211742576213663
Root relative squared error: 45.80937490268822
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.8542519125398579
Weighted FMeasure: 0.9318601398601398
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9782148148667622
Mean absolute error: 0.07605871288106832
Coverage of cases: 97.14285714285714
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.06634920634920635
Kappa statistic: 0.8532289628180038
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 58.0
Incorrectly Classified Instances: 5.142857142857143
Correctly Classified Instances: 94.85714285714286
Weighted Precision: 0.948811936936937
Weighted AreaUnderROC: 0.9834183673469388
Root mean squared error: 0.21361852883061716
Relative absolute error: 14.129231966657043
Root relative squared error: 42.72370576612343
Weighted TruePositiveRate: 0.9485714285714286
Weighted MatthewsCorrelation: 0.8888469147723923
Weighted FMeasure: 0.9486585925638219
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9856630554673199
Mean absolute error: 0.07064615983328522
Coverage of cases: 97.14285714285714
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9485714285714286
Weighted FalsePositiveRate: 0.056706349206349205
Kappa statistic: 0.8887790410281761
Training time: 1.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 58.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8859159397303726
Weighted AreaUnderROC: 0.971797052154195
Root mean squared error: 0.3123674063916201
Relative absolute error: 25.290171692699374
Root relative squared error: 62.473481278324016
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7405682339074181
Weighted FMeasure: 0.8708459737351114
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9757969434077557
Mean absolute error: 0.12645085846349688
Coverage of cases: 95.42857142857143
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.1017063492063492
Kappa statistic: 0.7289014615747289
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 58.285714285714285
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8680775275013096
Weighted AreaUnderROC: 0.9648526077097506
Root mean squared error: 0.3698247743555914
Relative absolute error: 32.476307858383635
Root relative squared error: 73.96495487111828
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.694252054136814
Weighted FMeasure: 0.843115766854687
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9697629720771638
Mean absolute error: 0.16238153929191818
Coverage of cases: 92.57142857142857
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 32.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.11777777777777777
Kappa statistic: 0.6753246753246752
Training time: 1.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 56.285714285714285
Incorrectly Classified Instances: 18.285714285714285
Correctly Classified Instances: 81.71428571428571
Weighted Precision: 0.8500915032679739
Weighted AreaUnderROC: 0.9553571428571429
Root mean squared error: 0.4012169146052166
Relative absolute error: 36.41122817837205
Root relative squared error: 80.24338292104332
Weighted TruePositiveRate: 0.8171428571428572
Weighted MatthewsCorrelation: 0.652647393458992
Weighted FMeasure: 0.8207760235483007
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9612848993410796
Mean absolute error: 0.18205614089186026
Coverage of cases: 88.57142857142857
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8171428571428572
Weighted FalsePositiveRate: 0.13757936507936508
Kappa statistic: 0.631336405529954
Training time: 1.0
		
Time end:Sat Oct 07 12.54.45 EEST 2017