Sat Oct 07 12.55.16 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.16 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8679736294240111
Weighted AreaUnderROC: 0.8120039682539684
Root mean squared error: 0.3779644730092272
Relative absolute error: 28.57142857142857
Root relative squared error: 75.59289460184544
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6904105408077927
Weighted FMeasure: 0.8500442290681128
Iteration time: 9.0
Weighted AreaUnderPRC: 0.792795439674829
Mean absolute error: 0.14285714285714285
Coverage of cases: 85.71428571428571
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.2331349206349206
Kappa statistic: 0.6680828465215082
Training time: 5.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 68.57142857142857
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8549063150589868
Weighted AreaUnderROC: 0.8421910430839001
Root mean squared error: 0.3651268156400042
Relative absolute error: 37.71428571428569
Root relative squared error: 73.02536312800085
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6629697402987389
Weighted FMeasure: 0.8380477673935617
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8160182323267694
Mean absolute error: 0.18857142857142845
Coverage of cases: 93.71428571428571
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.24650793650793648
Kappa statistic: 0.641529474243229
Training time: 3.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 53.42857142857143
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.9040362811791383
Weighted AreaUnderROC: 0.8683390022675737
Root mean squared error: 0.3127011010148979
Relative absolute error: 24.231884057971016
Root relative squared error: 62.54022020297958
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7784513950941252
Weighted FMeasure: 0.8937394957983191
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8540436971764752
Mean absolute error: 0.12115942028985507
Coverage of cases: 90.85714285714286
Instances selection time: 6.0
Test time: 0.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.16896825396825396
Kappa statistic: 0.7653806047966631
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.57142857142857
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8915126050420167
Weighted AreaUnderROC: 0.873157596371882
Root mean squared error: 0.32623392133340673
Relative absolute error: 21.714285714285715
Root relative squared error: 65.24678426668135
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7615353840256642
Weighted FMeasure: 0.8898803157626687
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8488016325354752
Mean absolute error: 0.10857142857142857
Coverage of cases: 89.71428571428571
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.15134920634920634
Kappa statistic: 0.7585155058464668
Training time: 12.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.57142857142857
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8742857142857143
Weighted AreaUnderROC: 0.8664965986394557
Root mean squared error: 0.35152727998183664
Relative absolute error: 25.142857142857146
Root relative squared error: 70.30545599636733
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.7271825396825397
Weighted FMeasure: 0.8742857142857143
Iteration time: 24.0
Weighted AreaUnderPRC: 0.833141914700882
Mean absolute error: 0.12571428571428572
Coverage of cases: 88.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.1471031746031746
Kappa statistic: 0.7271825396825398
Training time: 18.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8966465343687086
Weighted AreaUnderROC: 0.9034155328798186
Root mean squared error: 0.30775363781327036
Relative absolute error: 21.952380952380953
Root relative squared error: 61.550727562654075
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7754666623413939
Weighted FMeasure: 0.8967684841564374
Iteration time: 23.0
Weighted AreaUnderPRC: 0.892708902984279
Mean absolute error: 0.10976190476190477
Coverage of cases: 94.28571428571429
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.1273015873015873
Kappa statistic: 0.7752247752247752
Training time: 20.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 61.42857142857143
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8926170798898072
Weighted AreaUnderROC: 0.9111394557823129
Root mean squared error: 0.30822432264717026
Relative absolute error: 24.20094191522762
Root relative squared error: 61.64486452943405
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7618572895272595
Weighted FMeasure: 0.8893496203367449
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9017111248154601
Mean absolute error: 0.1210047095761381
Coverage of cases: 96.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 117.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.15829365079365076
Kappa statistic: 0.7567844342037892
Training time: 21.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 83.42857142857143
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9164625850340136
Weighted AreaUnderROC: 0.8414115646258503
Root mean squared error: 0.2953385417235706
Relative absolute error: 24.224489795918377
Root relative squared error: 59.06770834471412
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8049654071886116
Weighted FMeasure: 0.9055462184873949
Iteration time: 36.0
Weighted AreaUnderPRC: 0.851394666323213
Mean absolute error: 0.12112244897959189
Coverage of cases: 96.57142857142857
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 153.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.1555952380952381
Kappa statistic: 0.7914494264859229
Training time: 31.0
		
Time end:Sat Oct 07 12.55.16 EEST 2017