Sat Oct 07 12.55.12 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.12 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.285714285714285
Correctly Classified Instances: 81.71428571428571
Weighted Precision: 0.8493725992317542
Weighted AreaUnderROC: 0.7495039682539684
Root mean squared error: 0.416777438404351
Relative absolute error: 40.0
Root relative squared error: 83.3554876808702
Weighted TruePositiveRate: 0.8171428571428572
Weighted MatthewsCorrelation: 0.6123300838193761
Weighted FMeasure: 0.7993700787401574
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7405594780806047
Mean absolute error: 0.2
Coverage of cases: 81.71428571428571
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8171428571428572
Weighted FalsePositiveRate: 0.3181349206349206
Kappa statistic: 0.5570321151716501
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8650528789659224
Weighted AreaUnderROC: 0.78125
Root mean squared error: 0.3933112659506066
Relative absolute error: 34.38596491228071
Root relative squared error: 78.66225319012132
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6612434872807522
Weighted FMeasure: 0.82752
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7695407755581669
Mean absolute error: 0.17192982456140357
Coverage of cases: 84.0
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 2.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.2775
Kappa statistic: 0.6183206106870228
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.571428571428573
Correctly Classified Instances: 83.42857142857143
Weighted Precision: 0.8610791366906475
Weighted AreaUnderROC: 0.7733134920634921
Root mean squared error: 0.4019685075465498
Relative absolute error: 34.879406307977725
Root relative squared error: 80.39370150930995
Weighted TruePositiveRate: 0.8342857142857143
Weighted MatthewsCorrelation: 0.6491001392411999
Weighted FMeasure: 0.8206012314378849
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7622175174146397
Mean absolute error: 0.17439703153988861
Coverage of cases: 83.42857142857143
Instances selection time: 0.0
Test time: 12.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8342857142857143
Weighted FalsePositiveRate: 0.2876587301587301
Kappa statistic: 0.6031746031746031
Training time: 0.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8650528789659224
Weighted AreaUnderROC: 0.7813208616780045
Root mean squared error: 0.39598845922148607
Relative absolute error: 33.396232204221896
Root relative squared error: 79.19769184429721
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6612434872807522
Weighted FMeasure: 0.82752
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7697162337750044
Mean absolute error: 0.1669811610211095
Coverage of cases: 84.0
Instances selection time: 0.0
Test time: 13.0
Accumulative iteration time: 2.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.2775
Kappa statistic: 0.6183206106870228
Training time: 0.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8813687659264653
Weighted AreaUnderROC: 0.8130668934240363
Root mean squared error: 0.36724824694401353
Relative absolute error: 28.66426676771504
Root relative squared error: 73.44964938880271
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7094205842974307
Weighted FMeasure: 0.8544840525328331
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7995441133871674
Mean absolute error: 0.1433213338385752
Coverage of cases: 86.28571428571429
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.23686507936507933
Kappa statistic: 0.677765843179377
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8813687659264653
Weighted AreaUnderROC: 0.8130668934240363
Root mean squared error: 0.36768712754746485
Relative absolute error: 28.483867999754644
Root relative squared error: 73.53742550949298
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7094205842974307
Weighted FMeasure: 0.8544840525328331
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7995441133871674
Mean absolute error: 0.1424193399987732
Coverage of cases: 86.28571428571429
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.23686507936507933
Kappa statistic: 0.677765843179377
Training time: 0.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.858796992481203
Weighted AreaUnderROC: 0.7963435374149659
Root mean squared error: 0.39033407184955116
Relative absolute error: 31.734327243881427
Root relative squared error: 78.06681436991023
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6656449160884182
Weighted FMeasure: 0.8368979591836735
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7782147190636691
Mean absolute error: 0.15867163621940714
Coverage of cases: 84.57142857142857
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.25345238095238093
Kappa statistic: 0.6388443017656502
Training time: 0.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.863368569415081
Weighted AreaUnderROC: 0.8042800453514738
Root mean squared error: 0.38330723760167823
Relative absolute error: 30.505264509501785
Root relative squared error: 76.66144752033564
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6780442464015918
Weighted FMeasure: 0.8435013918960717
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7857010286158642
Mean absolute error: 0.15252632254750892
Coverage of cases: 85.14285714285714
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.24329365079365078
Kappa statistic: 0.6535181236673774
Training time: 0.0
		
Time end:Sat Oct 07 12.55.12 EEST 2017