Sat Oct 07 12.55.25 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.25 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.568181818181817
Correctly Classified Instances: 74.43181818181819
Weighted Precision: 0.8171389528193326
Weighted AreaUnderROC: 0.6428571428571429
Root mean squared error: 0.49253282655966524
Relative absolute error: 53.762711575211256
Root relative squared error: 98.50656531193304
Weighted TruePositiveRate: 0.7443181818181818
Weighted MatthewsCorrelation: 0.45203978125120226
Weighted FMeasure: 0.6945236497819524
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6529796036457789
Mean absolute error: 0.26881355787605626
Coverage of cases: 74.43181818181819
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7443181818181818
Weighted FalsePositiveRate: 0.4586038961038961
Kappa statistic: 0.3393393393393392
Training time: 5.0
		
Time end:Sat Oct 07 12.55.25 EEST 2017