Sat Oct 07 12.55.25 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.25 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.142857142857142
Correctly Classified Instances: 70.85714285714286
Weighted Precision: 0.7997546012269939
Weighted AreaUnderROC: 0.5952380952380952
Root mean squared error: 0.5257434125746092
Relative absolute error: 60.54054054054026
Root relative squared error: 105.14868251492184
Weighted TruePositiveRate: 0.7085714285714285
Weighted MatthewsCorrelation: 0.36177250531690774
Weighted FMeasure: 0.6365090909090909
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6132403155127082
Mean absolute error: 0.30270270270270133
Coverage of cases: 70.85714285714286
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7085714285714285
Weighted FalsePositiveRate: 0.5180952380952382
Kappa statistic: 0.23146473779385154
Training time: 5.0
		
Time end:Sat Oct 07 12.55.25 EEST 2017