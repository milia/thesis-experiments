Sat Oct 07 12.55.27 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.27 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 84.375
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9288530799801292
Weighted AreaUnderROC: 0.9712038207613428
Root mean squared error: 0.29008227672977055
Relative absolute error: 41.47727272727273
Root relative squared error: 58.01645534595411
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.8396176373487361
Weighted FMeasure: 0.9247098646034815
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9650952753311928
Mean absolute error: 0.20738636363636365
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.11843865328378601
Kappa statistic: 0.8340586016826226
Training time: 9.0
		
Time end:Sat Oct 07 12.55.27 EEST 2017