Sat Oct 07 12.53.59 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.53.59 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.142857142857142
Correctly Classified Instances: 70.85714285714286
Weighted Precision: 0.7997546012269939
Weighted AreaUnderROC: 0.5952380952380952
Root mean squared error: 0.5257434125746092
Relative absolute error: 60.54054054054026
Root relative squared error: 105.14868251492184
Weighted TruePositiveRate: 0.7085714285714285
Weighted MatthewsCorrelation: 0.36177250531690774
Weighted FMeasure: 0.6365090909090909
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6132403155127082
Mean absolute error: 0.30270270270270133
Coverage of cases: 70.85714285714286
Instances selection time: 15.0
Test time: 23.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7085714285714285
Weighted FalsePositiveRate: 0.5180952380952382
Kappa statistic: 0.23146473779385154
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8810409437890355
Weighted AreaUnderROC: 0.8244047619047618
Root mean squared error: 0.35654569078577253
Relative absolute error: 28.87218045112789
Root relative squared error: 71.3091381571545
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7178513413168464
Weighted FMeasure: 0.8620406907426637
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8074132645979973
Mean absolute error: 0.14436090225563944
Coverage of cases: 86.85714285714286
Instances selection time: 7.0
Test time: 22.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.21976190476190474
Kappa statistic: 0.6946362187997875
Training time: 0.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8993307086614173
Weighted AreaUnderROC: 0.8561507936507936
Root mean squared error: 0.32545361262705713
Relative absolute error: 23.747680890537826
Root relative squared error: 65.09072252541142
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7663386313695959
Weighted FMeasure: 0.8874997172905124
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8386224221972254
Mean absolute error: 0.11873840445268913
Coverage of cases: 89.14285714285714
Instances selection time: 14.0
Test time: 22.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.17912698412698413
Kappa statistic: 0.7514390371533229
Training time: 0.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8440425531914894
Weighted AreaUnderROC: 0.7986111111111112
Root mean squared error: 0.39598903033789207
Relative absolute error: 33.4020618556701
Root relative squared error: 79.19780606757841
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6467870731747851
Weighted FMeasure: 0.8336969696969697
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7731840425531915
Mean absolute error: 0.16701030927835048
Coverage of cases: 84.0
Instances selection time: 9.0
Test time: 26.0
Accumulative iteration time: 45.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.24277777777777776
Kappa statistic: 0.6323529411764706
Training time: 0.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8549063150589868
Weighted AreaUnderROC: 0.7996031746031745
Root mean squared error: 0.38951431681448345
Relative absolute error: 32.03907203907204
Root relative squared error: 77.9028633628967
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6629697402987389
Weighted FMeasure: 0.8380477673935617
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7785245960146724
Mean absolute error: 0.16019536019536018
Coverage of cases: 84.57142857142857
Instances selection time: 11.0
Test time: 29.0
Accumulative iteration time: 56.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.24650793650793648
Kappa statistic: 0.641529474243229
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.854255551510739
Weighted AreaUnderROC: 0.7881944444444444
Root mean squared error: 0.39713664263102433
Relative absolute error: 32.99270072992683
Root relative squared error: 79.42732852620486
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6532065918808355
Weighted FMeasure: 0.8302313946216385
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7703068074262832
Mean absolute error: 0.16496350364963416
Coverage of cases: 84.0
Instances selection time: 9.0
Test time: 35.0
Accumulative iteration time: 65.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.26361111111111113
Kappa statistic: 0.6240601503759398
Training time: 0.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8634814814814815
Weighted AreaUnderROC: 0.7926587301587301
Root mean squared error: 0.39033429361374034
Relative absolute error: 31.73794358507748
Root relative squared error: 78.06685872274807
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6690735871491088
Weighted FMeasure: 0.8356715537911246
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7771978835978837
Mean absolute error: 0.1586897179253874
Coverage of cases: 84.57142857142857
Instances selection time: 7.0
Test time: 42.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.2603968253968254
Kappa statistic: 0.6361185983827494
Training time: 0.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8634814814814815
Weighted AreaUnderROC: 0.7926587301587301
Root mean squared error: 0.39060759361454495
Relative absolute error: 31.63841807909608
Root relative squared error: 78.12151872290899
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6690735871491088
Weighted FMeasure: 0.8356715537911246
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7771978835978837
Mean absolute error: 0.1581920903954804
Coverage of cases: 84.57142857142857
Instances selection time: 0.0
Test time: 47.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.2603968253968254
Kappa statistic: 0.6361185983827494
Training time: 0.0
		
Time end:Sat Oct 07 12.53.59 EEST 2017