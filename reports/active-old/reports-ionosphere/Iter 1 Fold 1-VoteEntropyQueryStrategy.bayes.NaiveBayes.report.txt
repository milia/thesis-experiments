Sat Oct 07 12.55.23 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.23 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 55.39772727272727
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8498218332100549
Weighted AreaUnderROC: 0.9598258182328979
Root mean squared error: 0.4033015497936983
Relative absolute error: 36.52185287006816
Root relative squared error: 80.66030995873966
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.6434149018453629
Weighted FMeasure: 0.8107834928229665
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9644363402319359
Mean absolute error: 0.1826092643503408
Coverage of cases: 88.06818181818181
Instances selection time: 12.0
Test time: 13.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.13579697097396212
Kappa statistic: 0.6148796498905907
Training time: 4.0
		
Time end:Sat Oct 07 12.55.23 EEST 2017