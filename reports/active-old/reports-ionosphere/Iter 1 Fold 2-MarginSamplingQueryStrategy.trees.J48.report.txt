Sat Oct 07 12.54.55 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.55 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.9071755725190841
Weighted AreaUnderROC: 0.8492063492063492
Root mean squared error: 0.3295017884191656
Relative absolute error: 21.714285714285715
Root relative squared error: 65.90035768383312
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.772732942334954
Weighted FMeasure: 0.8860336140917656
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8376898582333696
Mean absolute error: 0.10857142857142857
Coverage of cases: 89.14285714285714
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.19301587301587303
Kappa statistic: 0.7477429633563464
Training time: 2.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 54.285714285714285
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8722428149341928
Weighted AreaUnderROC: 0.9022108843537415
Root mean squared error: 0.32950178841916555
Relative absolute error: 25.523809523809526
Root relative squared error: 65.9003576838331
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7175398934358419
Weighted FMeasure: 0.8646948873007146
Iteration time: 3.0
Weighted AreaUnderPRC: 0.875058258033056
Mean absolute error: 0.12761904761904763
Coverage of cases: 92.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.12575396825396826
Kappa statistic: 0.7123681687440078
Training time: 3.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.891104767342278
Weighted AreaUnderROC: 0.8804563492063492
Root mean squared error: 0.3295017884191656
Relative absolute error: 21.714285714285715
Root relative squared error: 65.90035768383312
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7636230891932863
Weighted FMeasure: 0.8912355555555556
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8488651849435178
Mean absolute error: 0.10857142857142857
Coverage of cases: 89.14285714285714
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.13051587301587303
Kappa statistic: 0.763563962170234
Training time: 3.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 83.14285714285714
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8965984804208066
Weighted AreaUnderROC: 0.8814484126984128
Root mean squared error: 0.3126592525508169
Relative absolute error: 26.890756302520984
Root relative squared error: 62.53185051016338
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7746224746580367
Weighted FMeasure: 0.8963589301121656
Iteration time: 6.0
Weighted AreaUnderPRC: 0.853220639559155
Mean absolute error: 0.13445378151260493
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.13424603174603172
Kappa statistic: 0.7736418511066399
Training time: 5.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8965984804208066
Weighted AreaUnderROC: 0.8814484126984128
Root mean squared error: 0.31473339883618856
Relative absolute error: 24.62533692722365
Root relative squared error: 62.94667976723771
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7746224746580367
Weighted FMeasure: 0.8963589301121656
Iteration time: 8.0
Weighted AreaUnderPRC: 0.853220639559155
Mean absolute error: 0.12312668463611824
Coverage of cases: 89.71428571428571
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.13424603174603172
Kappa statistic: 0.7736418511066399
Training time: 7.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 86.28571428571429
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8907826086956522
Weighted AreaUnderROC: 0.8744331065759637
Root mean squared error: 0.324290678935496
Relative absolute error: 30.240000000000116
Root relative squared error: 64.8581357870992
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7624437362098716
Weighted FMeasure: 0.8908219619641131
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8449892127614483
Mean absolute error: 0.15120000000000058
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.13746031746031745
Kappa statistic: 0.761904761904762
Training time: 8.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 52.285714285714285
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8542177998894418
Weighted AreaUnderROC: 0.8419784580498866
Root mean squared error: 0.38485618835389107
Relative absolute error: 31.085714285714282
Root relative squared error: 76.97123767077821
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6828137420186708
Weighted FMeasure: 0.8523636363636364
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8036575295758734
Mean absolute error: 0.15542857142857142
Coverage of cases: 85.71428571428571
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.15996031746031744
Kappa statistic: 0.6819960861056751
Training time: 9.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9211199999999999
Weighted AreaUnderROC: 0.8893849206349205
Root mean squared error: 0.28611306291355554
Relative absolute error: 20.010389610389616
Root relative squared error: 57.22261258271111
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.8169217288768313
Weighted FMeasure: 0.911706060266607
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8741499159663866
Mean absolute error: 0.10005194805194809
Coverage of cases: 91.42857142857143
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 59.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.14543650793650792
Kappa statistic: 0.8051948051948051
Training time: 16.0
		
Time end:Sat Oct 07 12.54.55 EEST 2017