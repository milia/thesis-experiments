Sat Oct 07 12.54.41 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.41 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 92.0
Incorrectly Classified Instances: 9.142857142857142
Correctly Classified Instances: 90.85714285714286
Weighted Precision: 0.9081622088006903
Weighted AreaUnderROC: 0.9516014739229026
Root mean squared error: 0.29442437787267917
Relative absolute error: 44.685714285714276
Root relative squared error: 58.88487557453583
Weighted TruePositiveRate: 0.9085714285714286
Weighted MatthewsCorrelation: 0.8004494800714647
Weighted FMeasure: 0.9082386525835
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9456284029715727
Mean absolute error: 0.2234285714285714
Coverage of cases: 99.42857142857143
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9085714285714286
Weighted FalsePositiveRate: 0.11392857142857143
Kappa statistic: 0.8001998001998002
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 94.0
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.9058768554150632
Weighted AreaUnderROC: 0.9513180272108843
Root mean squared error: 0.3101151859735806
Relative absolute error: 49.4857142857143
Root relative squared error: 62.023037194716125
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7942604808121067
Weighted FMeasure: 0.903602077451288
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9376424958093464
Mean absolute error: 0.2474285714285715
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.09630952380952379
Kappa statistic: 0.7927840078010725
Training time: 10.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 85.71428571428571
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.9016804923735617
Weighted AreaUnderROC: 0.970875850340136
Root mean squared error: 0.27412197493608026
Relative absolute error: 38.971428571428575
Root relative squared error: 54.82439498721605
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7798134631147867
Weighted FMeasure: 0.8929837908227956
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9618530644585238
Mean absolute error: 0.19485714285714287
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.08884920634920634
Kappa statistic: 0.7730530339225993
Training time: 9.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 83.71428571428571
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8965714285714285
Weighted AreaUnderROC: 0.9629393424036282
Root mean squared error: 0.2722393904531187
Relative absolute error: 35.31428571428572
Root relative squared error: 54.44787809062374
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7727556926637408
Weighted FMeasure: 0.8925345622119816
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9583175279639194
Mean absolute error: 0.17657142857142857
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.10273809523809523
Kappa statistic: 0.7699757869249395
Training time: 11.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 81.14285714285714
Incorrectly Classified Instances: 5.714285714285714
Correctly Classified Instances: 94.28571428571429
Weighted Precision: 0.9428571428571428
Weighted AreaUnderROC: 0.9656320861678005
Root mean squared error: 0.2526431701601517
Relative absolute error: 31.657142857142865
Root relative squared error: 50.52863403203034
Weighted TruePositiveRate: 0.9428571428571428
Weighted MatthewsCorrelation: 0.8759920634920635
Weighted FMeasure: 0.9428571428571428
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9616730690285403
Mean absolute error: 0.15828571428571434
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9428571428571428
Weighted FalsePositiveRate: 0.06686507936507936
Kappa statistic: 0.8759920634920635
Training time: 15.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 84.0
Incorrectly Classified Instances: 6.285714285714286
Correctly Classified Instances: 93.71428571428571
Weighted Precision: 0.9383319432860717
Weighted AreaUnderROC: 0.9709467120181406
Root mean squared error: 0.250827202899298
Relative absolute error: 33.94285714285712
Root relative squared error: 50.1654405798596
Weighted TruePositiveRate: 0.9371428571428572
Weighted MatthewsCorrelation: 0.8655844495580512
Weighted FMeasure: 0.9374471219614858
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9668029386737296
Mean absolute error: 0.1697142857142856
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9371428571428572
Weighted FalsePositiveRate: 0.06313492063492063
Kappa statistic: 0.8649975454099166
Training time: 16.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 81.71428571428571
Incorrectly Classified Instances: 4.0
Correctly Classified Instances: 96.0
Weighted Precision: 0.9599257779046532
Weighted AreaUnderROC: 0.9749858276643991
Root mean squared error: 0.22703052028935908
Relative absolute error: 29.257142857142863
Root relative squared error: 45.406104057871815
Weighted TruePositiveRate: 0.96
Weighted MatthewsCorrelation: 0.9129626763888442
Weighted FMeasure: 0.9599288888888889
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9725579078632105
Mean absolute error: 0.14628571428571432
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 99.0
Weighted Recall: 0.96
Weighted FalsePositiveRate: 0.050277777777777775
Kappa statistic: 0.9128919860627177
Training time: 21.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 74.28571428571429
Incorrectly Classified Instances: 5.142857142857143
Correctly Classified Instances: 94.85714285714286
Weighted Precision: 0.9484556094775907
Weighted AreaUnderROC: 0.9770408163265306
Root mean squared error: 0.2255152829790984
Relative absolute error: 25.371428571428574
Root relative squared error: 45.10305659581968
Weighted TruePositiveRate: 0.9485714285714286
Weighted MatthewsCorrelation: 0.8880727451895845
Weighted FMeasure: 0.9484799999999999
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9747334540683197
Mean absolute error: 0.12685714285714286
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 122.0
Weighted Recall: 0.9485714285714286
Weighted FalsePositiveRate: 0.06365079365079365
Kappa statistic: 0.8880039820806372
Training time: 22.0
		
Time end:Sat Oct 07 12.54.41 EEST 2017