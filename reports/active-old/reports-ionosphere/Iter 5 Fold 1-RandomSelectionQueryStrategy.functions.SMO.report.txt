Sat Oct 07 12.55.10 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.10 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.886363636363637
Correctly Classified Instances: 80.11363636363636
Weighted Precision: 0.8481649262899263
Weighted AreaUnderROC: 0.7222222222222222
Root mean squared error: 0.44594129250792236
Relative absolute error: 39.77272727272727
Root relative squared error: 89.18825850158447
Weighted TruePositiveRate: 0.8011363636363636
Weighted MatthewsCorrelation: 0.5825284021739535
Weighted FMeasure: 0.7762271253650563
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7204854324882732
Mean absolute error: 0.19886363636363635
Coverage of cases: 80.11363636363636
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8011363636363636
Weighted FalsePositiveRate: 0.35669191919191917
Kappa statistic: 0.5067264573991032
Training time: 10.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.8905993463058679
Weighted AreaUnderROC: 0.8403567916842253
Root mean squared error: 0.3454246398538787
Relative absolute error: 23.863636363636363
Root relative squared error: 69.08492797077574
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7427169260726693
Weighted FMeasure: 0.8755508705814514
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8235744708601674
Mean absolute error: 0.11931818181818182
Coverage of cases: 88.06818181818181
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.19996823481336753
Kappa statistic: 0.7239318792948911
Training time: 11.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.636363636363637
Correctly Classified Instances: 86.36363636363636
Weighted Precision: 0.8771479438554102
Weighted AreaUnderROC: 0.8165472678747016
Root mean squared error: 0.3692744729379982
Relative absolute error: 27.27272727272727
Root relative squared error: 73.85489458759965
Weighted TruePositiveRate: 0.8636363636363636
Weighted MatthewsCorrelation: 0.7063468509613468
Weighted FMeasure: 0.8563151068903484
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8005182494844646
Mean absolute error: 0.13636363636363635
Coverage of cases: 86.36363636363636
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8636363636363636
Weighted FalsePositiveRate: 0.2305418278869606
Kappa statistic: 0.6809185677594803
Training time: 12.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.636363636363637
Correctly Classified Instances: 86.36363636363636
Weighted Precision: 0.8771479438554102
Weighted AreaUnderROC: 0.8165472678747016
Root mean squared error: 0.3692744729379982
Relative absolute error: 27.27272727272727
Root relative squared error: 73.85489458759965
Weighted TruePositiveRate: 0.8636363636363636
Weighted MatthewsCorrelation: 0.7063468509613468
Weighted FMeasure: 0.8563151068903484
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8005182494844646
Mean absolute error: 0.13636363636363635
Coverage of cases: 86.36363636363636
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 67.0
Weighted Recall: 0.8636363636363636
Weighted FalsePositiveRate: 0.2305418278869606
Kappa statistic: 0.6809185677594803
Training time: 12.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.636363636363637
Correctly Classified Instances: 86.36363636363636
Weighted Precision: 0.8771479438554102
Weighted AreaUnderROC: 0.8165472678747016
Root mean squared error: 0.3692744729379982
Relative absolute error: 27.27272727272727
Root relative squared error: 73.85489458759965
Weighted TruePositiveRate: 0.8636363636363636
Weighted MatthewsCorrelation: 0.7063468509613468
Weighted FMeasure: 0.8563151068903484
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8005182494844646
Mean absolute error: 0.13636363636363635
Coverage of cases: 86.36363636363636
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 83.0
Weighted Recall: 0.8636363636363636
Weighted FalsePositiveRate: 0.2305418278869606
Kappa statistic: 0.6809185677594803
Training time: 13.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8825870031337621
Weighted AreaUnderROC: 0.8359320129231633
Root mean squared error: 0.3535533905932738
Relative absolute error: 25.0
Root relative squared error: 70.71067811865476
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.728026353260121
Weighted FMeasure: 0.8700413223140496
Iteration time: 21.0
Weighted AreaUnderPRC: 0.816575066485102
Mean absolute error: 0.125
Coverage of cases: 87.5
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 104.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.20313597415367324
Kappa statistic: 0.7118618842089597
Training time: 16.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8724837662337662
Weighted AreaUnderROC: 0.8350189633375474
Root mean squared error: 0.3614994027406106
Relative absolute error: 26.136363636363637
Root relative squared error: 72.29988054812212
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7122600860618665
Weighted FMeasure: 0.8653551941079249
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8109365776269186
Mean absolute error: 0.13068181818181818
Coverage of cases: 86.93181818181819
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 131.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.199280255143087
Kappa statistic: 0.7020900794818957
Training time: 21.0
		
Time end:Sat Oct 07 12.55.11 EEST 2017