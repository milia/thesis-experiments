Sat Oct 07 12.54.56 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.56 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.571428571428573
Correctly Classified Instances: 83.42857142857143
Weighted Precision: 0.8329565217391305
Weighted AreaUnderROC: 0.8149801587301587
Root mean squared error: 0.4070801956792859
Relative absolute error: 33.14285714285714
Root relative squared error: 81.41603913585718
Weighted TruePositiveRate: 0.8342857142857143
Weighted MatthewsCorrelation: 0.6370418059121953
Weighted FMeasure: 0.8333598366820673
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7778443064182194
Mean absolute error: 0.1657142857142857
Coverage of cases: 83.42857142857143
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8342857142857143
Weighted FalsePositiveRate: 0.20432539682539683
Kappa statistic: 0.6365914786967418
Training time: 9.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.890962962962963
Weighted AreaUnderROC: 0.8174603174603174
Root mean squared error: 0.3545882521403891
Relative absolute error: 28.651162790697665
Root relative squared error: 70.91765042807782
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7257747386024231
Weighted FMeasure: 0.8600165087850321
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8068486772486773
Mean absolute error: 0.14325581395348833
Coverage of cases: 86.85714285714286
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.23365079365079364
Kappa statistic: 0.6900269541778975
Training time: 3.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 56.857142857142854
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8870588235294118
Weighted AreaUnderROC: 0.9132653061224489
Root mean squared error: 0.32071349029490925
Relative absolute error: 25.71428571428571
Root relative squared error: 64.14269805898185
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7140055472954167
Weighted FMeasure: 0.8533586337760911
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8984692677070828
Mean absolute error: 0.12857142857142856
Coverage of cases: 94.85714285714286
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2438095238095238
Kappa statistic: 0.6753246753246753
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 52.285714285714285
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8867913385826771
Weighted AreaUnderROC: 0.8522534013605443
Root mean squared error: 0.33596686401811987
Relative absolute error: 25.805609915198975
Root relative squared error: 67.19337280362397
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7396555313915459
Weighted FMeasure: 0.8756575822684609
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8411861142357205
Mean absolute error: 0.12902804957599487
Coverage of cases: 89.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.1925
Kappa statistic: 0.7252747252747253
Training time: 6.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 59.142857142857146
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.89648
Weighted AreaUnderROC: 0.8596230158730158
Root mean squared error: 0.32851222532342506
Relative absolute error: 28.025396825396804
Root relative squared error: 65.70244506468501
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7642171012073582
Weighted FMeasure: 0.8881610096710355
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8447821565059145
Mean absolute error: 0.14012698412698402
Coverage of cases: 90.85714285714286
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1721825396825397
Kappa statistic: 0.7532467532467533
Training time: 8.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 52.57142857142857
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8791836734693877
Weighted AreaUnderROC: 0.8506235827664399
Root mean squared error: 0.3444598855402785
Relative absolute error: 26.403726708074537
Root relative squared error: 68.8919771080557
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.725423370905152
Weighted FMeasure: 0.870126050420168
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8397816218550913
Mean absolute error: 0.1320186335403727
Coverage of cases: 89.71428571428571
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.1957142857142857
Kappa statistic: 0.713242961418144
Training time: 9.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 52.57142857142857
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8791836734693877
Weighted AreaUnderROC: 0.8506235827664399
Root mean squared error: 0.3456515532031542
Relative absolute error: 26.19648219648219
Root relative squared error: 69.13031064063084
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.725423370905152
Weighted FMeasure: 0.870126050420168
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8397816218550913
Mean absolute error: 0.13098241098241095
Coverage of cases: 89.71428571428571
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.1957142857142857
Kappa statistic: 0.713242961418144
Training time: 10.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8860606060606061
Weighted AreaUnderROC: 0.876984126984127
Root mean squared error: 0.3275692099413302
Relative absolute error: 22.98412698412698
Root relative squared error: 65.51384198826604
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7488065701242741
Weighted FMeasure: 0.8838106370543541
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8629807642353672
Mean absolute error: 0.1149206349206349
Coverage of cases: 91.42857142857143
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.1615079365079365
Kappa statistic: 0.7448979591836734
Training time: 15.0
		
Time end:Sat Oct 07 12.54.56 EEST 2017