Sat Oct 07 12.55.19 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.19 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 56.0
Incorrectly Classified Instances: 30.285714285714285
Correctly Classified Instances: 69.71428571428571
Weighted Precision: 0.6961119040822153
Weighted AreaUnderROC: 0.6935232426303853
Root mean squared error: 0.5258956603223088
Relative absolute error: 65.41496598639478
Root relative squared error: 105.17913206446177
Weighted TruePositiveRate: 0.6971428571428572
Weighted MatthewsCorrelation: 0.3404942588058721
Weighted FMeasure: 0.6966044444444444
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6653823315147775
Mean absolute error: 0.3270748299319739
Coverage of cases: 74.28571428571429
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6971428571428572
Weighted FalsePositiveRate: 0.3578571428571429
Kappa statistic: 0.34046789447486325
Training time: 3.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 81.14285714285714
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.8250708924103419
Weighted AreaUnderROC: 0.8129960317460316
Root mean squared error: 0.4126912864135441
Relative absolute error: 40.43243243243236
Root relative squared error: 82.53825728270881
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6199588963349947
Weighted FMeasure: 0.8237146164369147
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7719825886651575
Mean absolute error: 0.2021621621621618
Coverage of cases: 90.28571428571429
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.19686507936507935
Kappa statistic: 0.6195385370643103
Training time: 3.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 58.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.856749311294766
Weighted AreaUnderROC: 0.8722363945578231
Root mean squared error: 0.36580851960537114
Relative absolute error: 30.247619047619068
Root relative squared error: 73.16170392107423
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6845375375454673
Weighted FMeasure: 0.8544073951799275
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8425144795598786
Mean absolute error: 0.15123809523809534
Coverage of cases: 90.85714285714286
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.1984126984126984
Kappa statistic: 0.679979518689196
Training time: 4.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 51.714285714285715
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.822283756779903
Weighted AreaUnderROC: 0.8214285714285714
Root mean squared error: 0.3966597552546566
Relative absolute error: 36.551378446115386
Root relative squared error: 79.33195105093131
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6142835019977283
Weighted FMeasure: 0.8225422222222223
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7920980507034258
Mean absolute error: 0.18275689223057692
Coverage of cases: 85.14285714285714
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.21075396825396825
Kappa statistic: 0.6142359382777501
Training time: 7.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 51.714285714285715
Incorrectly Classified Instances: 17.714285714285715
Correctly Classified Instances: 82.28571428571429
Weighted Precision: 0.822283756779903
Weighted AreaUnderROC: 0.8214285714285714
Root mean squared error: 0.39560461654741236
Relative absolute error: 37.13741496598655
Root relative squared error: 79.12092330948248
Weighted TruePositiveRate: 0.8228571428571428
Weighted MatthewsCorrelation: 0.6142835019977283
Weighted FMeasure: 0.8225422222222223
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7920980507034258
Mean absolute error: 0.18568707482993274
Coverage of cases: 85.14285714285714
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8228571428571428
Weighted FalsePositiveRate: 0.21075396825396825
Kappa statistic: 0.6142359382777501
Training time: 8.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.57142857142857
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8765891472868218
Weighted AreaUnderROC: 0.811437074829932
Root mean squared error: 0.35687132535010785
Relative absolute error: 31.012244897959203
Root relative squared error: 71.37426507002156
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7056969481308573
Weighted FMeasure: 0.8555397463656046
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8088184752111257
Mean absolute error: 0.155061224489796
Coverage of cases: 86.85714285714286
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2299206349206349
Kappa statistic: 0.6801705756929638
Training time: 9.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 52.57142857142857
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8647438330170779
Weighted AreaUnderROC: 0.8522534013605443
Root mean squared error: 0.3570158504328869
Relative absolute error: 27.619047619047617
Root relative squared error: 71.40317008657739
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6979061935736514
Weighted FMeasure: 0.8591257805530775
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8273529914882906
Mean absolute error: 0.13809523809523808
Coverage of cases: 88.57142857142857
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.20214285714285715
Kappa statistic: 0.68944099378882
Training time: 12.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 52.57142857142857
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8647438330170779
Weighted AreaUnderROC: 0.8522534013605443
Root mean squared error: 0.35723808254306766
Relative absolute error: 27.42857142857142
Root relative squared error: 71.44761650861354
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6979061935736514
Weighted FMeasure: 0.8591257805530775
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8273529914882906
Mean absolute error: 0.1371428571428571
Coverage of cases: 88.57142857142857
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.20214285714285715
Kappa statistic: 0.68944099378882
Training time: 13.0
		
Time end:Sat Oct 07 12.55.19 EEST 2017