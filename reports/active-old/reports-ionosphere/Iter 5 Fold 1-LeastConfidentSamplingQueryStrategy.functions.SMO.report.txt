Sat Oct 07 12.54.28 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.28 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.340909090909092
Correctly Classified Instances: 84.6590909090909
Weighted Precision: 0.8594386024423337
Weighted AreaUnderROC: 0.7962494732406238
Root mean squared error: 0.39167472590032015
Relative absolute error: 30.681818181818183
Root relative squared error: 78.33494518006403
Weighted TruePositiveRate: 0.8465909090909091
Weighted MatthewsCorrelation: 0.6663770545764683
Weighted FMeasure: 0.8377714390872287
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7785786285881691
Mean absolute error: 0.1534090909090909
Coverage of cases: 84.6590909090909
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8465909090909091
Weighted FalsePositiveRate: 0.25409196260966166
Kappa statistic: 0.6396724294813466
Training time: 9.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.454545454545453
Correctly Classified Instances: 79.54545454545455
Weighted Precision: 0.8448749237339842
Weighted AreaUnderROC: 0.7142857142857143
Root mean squared error: 0.45226701686664544
Relative absolute error: 40.909090909090914
Root relative squared error: 90.4534033733291
Weighted TruePositiveRate: 0.7954545454545454
Weighted MatthewsCorrelation: 0.5701087346456561
Weighted FMeasure: 0.7685981956974324
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7135474443951412
Mean absolute error: 0.20454545454545456
Coverage of cases: 79.54545454545455
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7954545454545454
Weighted FalsePositiveRate: 0.36688311688311687
Kappa statistic: 0.4905933429811866
Training time: 7.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.295454545454547
Correctly Classified Instances: 76.70454545454545
Weighted Precision: 0.8290658205430933
Weighted AreaUnderROC: 0.6746031746031746
Root mean squared error: 0.48265364958171136
Relative absolute error: 46.590909090909086
Root relative squared error: 96.53072991634227
Weighted TruePositiveRate: 0.7670454545454546
Weighted MatthewsCorrelation: 0.506197419654031
Weighted FMeasure: 0.7287483226181177
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6794984135182998
Mean absolute error: 0.23295454545454544
Coverage of cases: 76.70454545454545
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7670454545454546
Weighted FalsePositiveRate: 0.41783910533910534
Kappa statistic: 0.4079422382671481
Training time: 7.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.863636363636363
Correctly Classified Instances: 76.13636363636364
Weighted Precision: 0.8260263929618769
Weighted AreaUnderROC: 0.6666666666666666
Root mean squared error: 0.48850421045919723
Relative absolute error: 47.72727272727273
Root relative squared error: 97.70084209183945
Weighted TruePositiveRate: 0.7613636363636364
Weighted MatthewsCorrelation: 0.4929612080967143
Weighted FMeasure: 0.7204036635006784
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6728110003998933
Mean absolute error: 0.23863636363636365
Coverage of cases: 76.13636363636364
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7613636363636364
Weighted FalsePositiveRate: 0.428030303030303
Kappa statistic: 0.39100346020761245
Training time: 9.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.568181818181817
Correctly Classified Instances: 74.43181818181819
Weighted Precision: 0.8171389528193326
Weighted AreaUnderROC: 0.6428571428571429
Root mean squared error: 0.5056498968474316
Relative absolute error: 51.13636363636363
Root relative squared error: 101.1299793694863
Weighted TruePositiveRate: 0.7443181818181818
Weighted MatthewsCorrelation: 0.45203978125120226
Weighted FMeasure: 0.6945236497819524
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6529796036457789
Mean absolute error: 0.2556818181818182
Coverage of cases: 74.43181818181819
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7443181818181818
Weighted FalsePositiveRate: 0.4586038961038961
Kappa statistic: 0.3393393393393392
Training time: 16.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.75
Correctly Classified Instances: 81.25
Weighted Precision: 0.8548801369863014
Weighted AreaUnderROC: 0.7380952380952382
Root mean squared error: 0.4330127018922193
Relative absolute error: 37.5
Root relative squared error: 86.60254037844386
Weighted TruePositiveRate: 0.8125
Weighted MatthewsCorrelation: 0.6070900940198353
Weighted FMeasure: 0.7911788516627226
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7344966142590287
Mean absolute error: 0.1875
Coverage of cases: 81.25
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8125
Weighted FalsePositiveRate: 0.33630952380952384
Kappa statistic: 0.5386081982840801
Training time: 9.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8727527137042063
Weighted AreaUnderROC: 0.8086107599381936
Root mean squared error: 0.37688918072220456
Relative absolute error: 28.40909090909091
Root relative squared error: 75.37783614444092
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6941822612159796
Weighted FMeasure: 0.8497883695252116
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7929547996807579
Mean absolute error: 0.14204545454545456
Coverage of cases: 85.79545454545455
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 83.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.24073302557815832
Kappa statistic: 0.6663633606308765
Training time: 13.0
		
Time end:Sat Oct 07 12.54.29 EEST 2017