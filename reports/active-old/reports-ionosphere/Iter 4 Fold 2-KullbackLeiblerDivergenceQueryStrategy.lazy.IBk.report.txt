Sat Oct 07 12.54.19 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.19 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8525237191650854
Weighted AreaUnderROC: 0.8179563492063492
Root mean squared error: 0.3758603473771345
Relative absolute error: 33.51351351351351
Root relative squared error: 75.1720694754269
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6717085136347005
Weighted FMeasure: 0.8473862622658341
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7896689979217493
Mean absolute error: 0.16756756756756755
Coverage of cases: 85.14285714285714
Instances selection time: 21.0
Test time: 11.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.21551587301587302
Kappa statistic: 0.6635610766045549
Training time: 2.0
		
Time end:Sat Oct 07 12.54.19 EEST 2017