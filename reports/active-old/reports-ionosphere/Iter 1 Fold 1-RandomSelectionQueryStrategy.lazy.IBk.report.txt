Sat Oct 07 12.55.11 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.11 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.613636363636363
Correctly Classified Instances: 82.38636363636364
Weighted Precision: 0.8295884986225895
Weighted AreaUnderROC: 0.7750386290209299
Root mean squared error: 0.4090793802007121
Relative absolute error: 38.72850122850123
Root relative squared error: 81.81587604014243
Weighted TruePositiveRate: 0.8238636363636364
Weighted MatthewsCorrelation: 0.6090036321589298
Weighted FMeasure: 0.8150551384530023
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7520123106060606
Mean absolute error: 0.19364250614250614
Coverage of cases: 82.38636363636364
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8238636363636364
Weighted FalsePositiveRate: 0.27378637832177655
Kappa statistic: 0.5894039735099338
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8748668323863636
Weighted AreaUnderROC: 0.8315072341621014
Root mean squared error: 0.3555337933860273
Relative absolute error: 28.728070175438642
Root relative squared error: 71.10675867720546
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7136855417399497
Weighted FMeasure: 0.864555190800004
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8097895682679063
Mean absolute error: 0.1436403508771932
Coverage of cases: 86.93181818181819
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.20630371349397897
Kappa statistic: 0.6998813760379595
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8748668323863636
Weighted AreaUnderROC: 0.8315072341621014
Root mean squared error: 0.35701001071168237
Relative absolute error: 28.054899645808728
Root relative squared error: 71.40200214233647
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7136855417399497
Weighted FMeasure: 0.864555190800004
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8097895682679063
Mean absolute error: 0.14027449822904364
Coverage of cases: 86.93181818181819
Instances selection time: 1.0
Test time: 24.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.20630371349397897
Kappa statistic: 0.6998813760379595
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8603573907009022
Weighted AreaUnderROC: 0.8082595870206489
Root mean squared error: 0.3805085167451481
Relative absolute error: 30.986532528897214
Root relative squared error: 76.10170334902962
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6762726937061966
Weighted FMeasure: 0.8454110779930453
Iteration time: 1.0
Weighted AreaUnderPRC: 0.788072107791524
Mean absolute error: 0.15493266264448607
Coverage of cases: 85.22727272727273
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.23687730656757203
Kappa statistic: 0.6569200779727096
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8508440763642722
Weighted AreaUnderROC: 0.7923865711476331
Root mean squared error: 0.39552982027060596
Relative absolute error: 32.97405420250247
Root relative squared error: 79.10596405412119
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.651180644718082
Weighted FMeasure: 0.8323676247054066
Iteration time: 0.0
Weighted AreaUnderPRC: 0.7731680502436777
Mean absolute error: 0.16487027101251236
Coverage of cases: 84.0909090909091
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.25725970194996745
Kappa statistic: 0.6277383290527271
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.909090909090908
Correctly Classified Instances: 84.0909090909091
Weighted Precision: 0.8508440763642722
Weighted AreaUnderROC: 0.7923865711476331
Root mean squared error: 0.39600661807608706
Relative absolute error: 32.80530319294275
Root relative squared error: 79.2013236152174
Weighted TruePositiveRate: 0.8409090909090909
Weighted MatthewsCorrelation: 0.651180644718082
Weighted FMeasure: 0.8323676247054066
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7731680502436777
Mean absolute error: 0.16402651596471376
Coverage of cases: 84.0909090909091
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8409090909090909
Weighted FalsePositiveRate: 0.25725970194996745
Kappa statistic: 0.6277383290527271
Training time: 0.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8651584828215263
Weighted AreaUnderROC: 0.816196094957157
Root mean squared error: 0.374534622732127
Relative absolute error: 29.313885424554236
Root relative squared error: 74.9069245464254
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6887680284297514
Weighted FMeasure: 0.8518462745017281
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7956181444699614
Mean absolute error: 0.14656942712277118
Coverage of cases: 85.79545454545455
Instances selection time: 0.0
Test time: 42.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.22668610887637436
Kappa statistic: 0.6713474753510605
Training time: 1.0
		
Time end:Sat Oct 07 12.55.12 EEST 2017