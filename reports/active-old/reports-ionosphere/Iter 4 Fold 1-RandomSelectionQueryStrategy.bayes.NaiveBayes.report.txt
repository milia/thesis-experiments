Sat Oct 07 12.55.04 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.04 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 52.84090909090909
Incorrectly Classified Instances: 17.045454545454547
Correctly Classified Instances: 82.95454545454545
Weighted Precision: 0.8425607560756077
Weighted AreaUnderROC: 0.8974575080769771
Root mean squared error: 0.4056122420380243
Relative absolute error: 34.447153987038305
Root relative squared error: 81.12244840760486
Weighted TruePositiveRate: 0.8295454545454546
Weighted MatthewsCorrelation: 0.6507830237615468
Weighted FMeasure: 0.8321774223338628
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9091768055818917
Mean absolute error: 0.17223576993519152
Coverage of cases: 85.79545454545455
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8295454545454546
Weighted FalsePositiveRate: 0.1582433053671992
Kappa statistic: 0.6441568944601699
Training time: 0.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 52.84090909090909
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8615907853040206
Weighted AreaUnderROC: 0.9157184997892962
Root mean squared error: 0.3601366466940179
Relative absolute error: 28.282981058441443
Root relative squared error: 72.02732933880358
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6975883953601293
Weighted FMeasure: 0.8590584875385056
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9268988274950799
Mean absolute error: 0.1414149052922072
Coverage of cases: 88.63636363636364
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.1494280670165626
Kappa statistic: 0.6963003865267807
Training time: 0.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 52.84090909090909
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8711260330578512
Weighted AreaUnderROC: 0.9272369714847591
Root mean squared error: 0.3415636783111117
Relative absolute error: 25.785064267536224
Root relative squared error: 68.31273566222234
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7191388251958121
Weighted FMeasure: 0.8699587121607144
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9381801256535268
Mean absolute error: 0.12892532133768111
Coverage of cases: 89.77272727272727
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.14309258833595118
Kappa statistic: 0.7186544342507644
Training time: 1.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 54.26136363636363
Incorrectly Classified Instances: 9.659090909090908
Correctly Classified Instances: 90.3409090909091
Weighted Precision: 0.9028898902821317
Weighted AreaUnderROC: 0.9351032448377581
Root mean squared error: 0.2973961906952155
Relative absolute error: 20.27756803819634
Root relative squared error: 59.4792381390431
Weighted TruePositiveRate: 0.9034090909090909
Weighted MatthewsCorrelation: 0.7881744205189147
Weighted FMeasure: 0.9028638445376117
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9456081440667776
Mean absolute error: 0.1013878401909817
Coverage of cases: 93.18181818181819
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9034090909090909
Weighted FalsePositiveRate: 0.12408615229411689
Kappa statistic: 0.7876206700738217
Training time: 1.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 55.39772727272727
Incorrectly Classified Instances: 11.931818181818182
Correctly Classified Instances: 88.06818181818181
Weighted Precision: 0.881151075487013
Weighted AreaUnderROC: 0.9310296389942407
Root mean squared error: 0.32969834195664766
Relative absolute error: 25.405103106205186
Root relative squared error: 65.93966839132953
Weighted TruePositiveRate: 0.8806818181818182
Weighted MatthewsCorrelation: 0.7413776367499303
Weighted FMeasure: 0.880886423287998
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9426231241430056
Mean absolute error: 0.12702551553102592
Coverage of cases: 92.61363636363636
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8806818181818182
Weighted FalsePositiveRate: 0.13675710965533974
Kappa statistic: 0.7413213885778276
Training time: 0.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 56.25
Incorrectly Classified Instances: 12.5
Correctly Classified Instances: 87.5
Weighted Precision: 0.8775332055319732
Weighted AreaUnderROC: 0.9293440089900268
Root mean squared error: 0.3417277950160148
Relative absolute error: 27.34742447736986
Root relative squared error: 68.34555900320296
Weighted TruePositiveRate: 0.875
Weighted MatthewsCorrelation: 0.7326903369366268
Weighted FMeasure: 0.8757969507969509
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9414161813017494
Mean absolute error: 0.1367371223868493
Coverage of cases: 92.04545454545455
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.875
Weighted FalsePositiveRate: 0.13290139064475348
Kappa statistic: 0.7318188114697326
Training time: 1.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 55.96590909090909
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.863668524871355
Weighted AreaUnderROC: 0.9280797864868661
Root mean squared error: 0.356485821544769
Relative absolute error: 29.33582850284267
Root relative squared error: 71.2971643089538
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.7008861716475863
Weighted FMeasure: 0.8594224508968561
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9404270131777518
Mean absolute error: 0.14667914251421335
Coverage of cases: 91.47727272727273
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.14240460866567062
Kappa statistic: 0.6983822319714833
Training time: 1.0
		
Time end:Sat Oct 07 12.55.04 EEST 2017