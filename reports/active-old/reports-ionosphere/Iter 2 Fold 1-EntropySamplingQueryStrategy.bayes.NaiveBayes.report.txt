Sat Oct 07 12.53.43 EEST 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.53.43 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 53.40909090909091
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8575027653444461
Weighted AreaUnderROC: 0.9240061806433488
Root mean squared error: 0.35352097129381094
Relative absolute error: 27.521174388411314
Root relative squared error: 70.70419425876219
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.68993185628911
Weighted FMeasure: 0.8576992390869042
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9309190530981148
Mean absolute error: 0.13760587194205656
Coverage of cases: 89.20454545454545
Instances selection time: 15.0
Test time: 24.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.17049844206923853
Kappa statistic: 0.6898787707922187
Training time: 2.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 56.81818181818182
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8772399475524476
Weighted AreaUnderROC: 0.9469026548672567
Root mean squared error: 0.3358434951576174
Relative absolute error: 27.39751572577896
Root relative squared error: 67.16869903152349
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7286504173034182
Weighted FMeasure: 0.8709642508029604
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9545958572722367
Mean absolute error: 0.1369875786288948
Coverage of cases: 93.75
Instances selection time: 9.0
Test time: 19.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.12202221328327521
Kappa statistic: 0.7244008714596949
Training time: 1.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 55.68181818181818
Incorrectly Classified Instances: 5.681818181818182
Correctly Classified Instances: 94.31818181818181
Weighted Precision: 0.9457362885048834
Weighted AreaUnderROC: 0.9671302149178256
Root mean squared error: 0.22680158424103303
Relative absolute error: 13.739959289694784
Root relative squared error: 45.36031684820661
Weighted TruePositiveRate: 0.9431818181818182
Weighted MatthewsCorrelation: 0.8773674197521902
Weighted FMeasure: 0.9422270290914359
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9708919187983495
Mean absolute error: 0.06869979644847392
Coverage of cases: 97.1590909090909
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9431818181818182
Weighted FalsePositiveRate: 0.09488851856108492
Kappa statistic: 0.8728139904610495
Training time: 2.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 55.96590909090909
Incorrectly Classified Instances: 7.386363636363637
Correctly Classified Instances: 92.61363636363636
Weighted Precision: 0.9263439110567983
Weighted AreaUnderROC: 0.9678325607529146
Root mean squared error: 0.2467359001551306
Relative absolute error: 16.420871025533
Root relative squared error: 49.34718003102612
Weighted TruePositiveRate: 0.9261363636363636
Weighted MatthewsCorrelation: 0.838091371004704
Weighted FMeasure: 0.9254096454509678
Iteration time: 10.0
Weighted AreaUnderPRC: 0.971610357747346
Mean absolute error: 0.082104355127665
Coverage of cases: 97.1590909090909
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9261363636363636
Weighted FalsePositiveRate: 0.10439173658200206
Kappa statistic: 0.8364312267657993
Training time: 2.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 57.10227272727273
Incorrectly Classified Instances: 10.795454545454545
Correctly Classified Instances: 89.20454545454545
Weighted Precision: 0.8924766639610389
Weighted AreaUnderROC: 0.9515381373788453
Root mean squared error: 0.29271057391699085
Relative absolute error: 22.76678469082852
Root relative squared error: 58.54211478339817
Weighted TruePositiveRate: 0.8920454545454546
Weighted MatthewsCorrelation: 0.7660155642552151
Weighted FMeasure: 0.8922305734510458
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9594116384225945
Mean absolute error: 0.1138339234541426
Coverage of cases: 93.75
Instances selection time: 2.0
Test time: 16.0
Accumulative iteration time: 56.0
Weighted Recall: 0.8920454545454546
Weighted FalsePositiveRate: 0.12339817262383633
Kappa statistic: 0.7659574468085107
Training time: 8.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 58.52272727272727
Incorrectly Classified Instances: 13.068181818181818
Correctly Classified Instances: 86.93181818181819
Weighted Precision: 0.8748391938250428
Weighted AreaUnderROC: 0.9502739148756848
Root mean squared error: 0.3287552110434921
Relative absolute error: 27.5908516472583
Root relative squared error: 65.75104220869842
Weighted TruePositiveRate: 0.8693181818181818
Weighted MatthewsCorrelation: 0.7251021053362348
Weighted FMeasure: 0.8706686548251077
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9585628585085911
Mean absolute error: 0.1379542582362915
Coverage of cases: 94.88636363636364
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 71.0
Weighted Recall: 0.8693181818181818
Weighted FalsePositiveRate: 0.12904567163416722
Kappa statistic: 0.7225116534137646
Training time: 7.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 55.96590909090909
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.8383021128682097
Weighted AreaUnderROC: 0.9439528023598821
Root mean squared error: 0.4011880459745138
Relative absolute error: 36.02792520264575
Root relative squared error: 80.23760919490276
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.636745771376699
Weighted FMeasure: 0.8214986891043229
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9532358922111765
Mean absolute error: 0.18013962601322872
Coverage of cases: 88.63636363636364
Instances selection time: 6.0
Test time: 24.0
Accumulative iteration time: 83.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.15755532569691863
Kappa statistic: 0.6254821119829765
Training time: 6.0
		
Time end:Sat Oct 07 12.53.44 EEST 2017