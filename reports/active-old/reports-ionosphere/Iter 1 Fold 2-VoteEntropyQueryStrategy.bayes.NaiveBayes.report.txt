Sat Oct 07 12.55.23 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 12.55.23 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 53.142857142857146
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.845224093634028
Weighted AreaUnderROC: 0.917375283446712
Root mean squared error: 0.3805329680513955
Relative absolute error: 30.912757657556256
Root relative squared error: 76.1065936102791
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6640633643962476
Weighted FMeasure: 0.84544
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9252510518809134
Mean absolute error: 0.15456378828778128
Coverage of cases: 88.0
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.18400793650793648
Kappa statistic: 0.6640119462419115
Training time: 11.0
		
Time end:Sat Oct 07 12.55.23 EEST 2017