Sat Oct 07 12.54.59 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.59 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 85.42857142857143
Incorrectly Classified Instances: 18.285714285714285
Correctly Classified Instances: 81.71428571428571
Weighted Precision: 0.820923076923077
Weighted AreaUnderROC: 0.8869047619047618
Root mean squared error: 0.36182079226363656
Relative absolute error: 49.714285714285715
Root relative squared error: 72.36415845272731
Weighted TruePositiveRate: 0.8171428571428572
Weighted MatthewsCorrelation: 0.5937960758350571
Weighted FMeasure: 0.8087052341597796
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8763390197211319
Mean absolute error: 0.24857142857142858
Coverage of cases: 98.28571428571429
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8171428571428572
Weighted FalsePositiveRate: 0.276468253968254
Kappa statistic: 0.5767195767195769
Training time: 5.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 80.85714285714286
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.9216969696969698
Weighted AreaUnderROC: 0.9643565759637187
Root mean squared error: 0.2599999999999999
Relative absolute error: 32.800000000000004
Root relative squared error: 51.99999999999998
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8257387519863572
Weighted FMeasure: 0.9186674459380479
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9598816268681506
Mean absolute error: 0.16400000000000003
Coverage of cases: 98.85714285714286
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.12138888888888888
Kappa statistic: 0.8214285714285715
Training time: 5.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 79.71428571428571
Incorrectly Classified Instances: 8.0
Correctly Classified Instances: 92.0
Weighted Precision: 0.92049955396967
Weighted AreaUnderROC: 0.972859977324263
Root mean squared error: 0.27328687177710215
Relative absolute error: 36.22857142857143
Root relative squared error: 54.65737435542043
Weighted TruePositiveRate: 0.92
Weighted MatthewsCorrelation: 0.8250805543510701
Weighted FMeasure: 0.9190434782608697
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9676777092432189
Mean absolute error: 0.18114285714285713
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.92
Weighted FalsePositiveRate: 0.11444444444444445
Kappa statistic: 0.822695035460993
Training time: 14.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 87.14285714285714
Incorrectly Classified Instances: 8.571428571428571
Correctly Classified Instances: 91.42857142857143
Weighted Precision: 0.9151260504201679
Weighted AreaUnderROC: 0.9657738095238096
Root mean squared error: 0.2722393904531188
Relative absolute error: 37.828571428571436
Root relative squared error: 54.44787809062376
Weighted TruePositiveRate: 0.9142857142857143
Weighted MatthewsCorrelation: 0.812576629603792
Weighted FMeasure: 0.9130634071810542
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9610179892500533
Mean absolute error: 0.1891428571428572
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9142857142857143
Weighted FalsePositiveRate: 0.12460317460317459
Kappa statistic: 0.8093543467208947
Training time: 9.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 76.85714285714286
Incorrectly Classified Instances: 7.428571428571429
Correctly Classified Instances: 92.57142857142857
Weighted Precision: 0.9259416445623343
Weighted AreaUnderROC: 0.9811507936507936
Root mean squared error: 0.2423692578336971
Relative absolute error: 28.34285714285714
Root relative squared error: 48.47385156673942
Weighted TruePositiveRate: 0.9257142857142857
Weighted MatthewsCorrelation: 0.8376105968386144
Weighted FMeasure: 0.9249904363203291
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9782642993644406
Mean absolute error: 0.1417142857142857
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.9257142857142857
Weighted FalsePositiveRate: 0.10428571428571429
Kappa statistic: 0.8359414437152953
Training time: 11.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 82.28571428571429
Incorrectly Classified Instances: 6.857142857142857
Correctly Classified Instances: 93.14285714285714
Weighted Precision: 0.9314552893045003
Weighted AreaUnderROC: 0.964640022675737
Root mean squared error: 0.2565707922359273
Relative absolute error: 33.37142857142859
Root relative squared error: 51.31415844718546
Weighted TruePositiveRate: 0.9314285714285714
Weighted MatthewsCorrelation: 0.850170830443931
Weighted FMeasure: 0.9309059534081104
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9612995146605662
Mean absolute error: 0.16685714285714293
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9314285714285714
Weighted FalsePositiveRate: 0.09412698412698413
Kappa statistic: 0.8490945674044265
Training time: 14.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 77.71428571428571
Incorrectly Classified Instances: 7.428571428571429
Correctly Classified Instances: 92.57142857142857
Weighted Precision: 0.9269327731092436
Weighted AreaUnderROC: 0.9811507936507936
Root mean squared error: 0.23543273227945896
Relative absolute error: 28.114285714285714
Root relative squared error: 47.08654645589179
Weighted TruePositiveRate: 0.9257142857142857
Weighted MatthewsCorrelation: 0.8380972523928558
Weighted FMeasure: 0.924654952890247
Iteration time: 18.0
Weighted AreaUnderPRC: 0.978027594425529
Mean absolute error: 0.14057142857142857
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9257142857142857
Weighted FalsePositiveRate: 0.11123015873015873
Kappa statistic: 0.8347737671581088
Training time: 17.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 81.14285714285714
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.9033193277310924
Weighted AreaUnderROC: 0.9633645124716552
Root mean squared error: 0.2848558532711889
Relative absolute error: 36.57142857142857
Root relative squared error: 56.97117065423778
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7870560068147279
Weighted FMeasure: 0.9014718614718614
Iteration time: 42.0
Weighted AreaUnderPRC: 0.9576363215675257
Mean absolute error: 0.18285714285714283
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 121.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.13797619047619047
Kappa statistic: 0.7839349262836806
Training time: 37.0
		
Time end:Sat Oct 07 12.55.00 EEST 2017