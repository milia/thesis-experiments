Sat Oct 07 12.54.16 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.16 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 54.7
Incorrectly Classified Instances: 15.208766233766235
Correctly Classified Instances: 84.79123376623377
Weighted Precision: 0.8616074700295229
Weighted AreaUnderROC: 0.9394057401721753
Root mean squared error: 0.36586742405132205
Relative absolute error: 30.41695047368408
Root relative squared error: 73.1734848102644
Weighted TruePositiveRate: 0.8479123376623378
Weighted MatthewsCorrelation: 0.6905863825491085
Weighted FMeasure: 0.8496170801852854
Iteration time: 40.5
Weighted AreaUnderPRC: 0.946416444799944
Mean absolute error: 0.1520847523684204
Coverage of cases: 89.46233766233766
Instances selection time: 35.9
Test time: 22.5
Accumulative iteration time: 40.5
Weighted Recall: 0.8479123376623378
Weighted FalsePositiveRate: 0.14518196822842838
Kappa statistic: 0.6822016286677068
Training time: 4.6
		
Time end:Sat Oct 07 12.54.17 EEST 2017