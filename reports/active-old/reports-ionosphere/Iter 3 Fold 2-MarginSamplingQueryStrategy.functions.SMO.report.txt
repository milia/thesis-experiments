Sat Oct 07 12.54.48 EEST 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 12.54.48 EEST 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.285714285714285
Correctly Classified Instances: 73.71428571428571
Weighted Precision: 0.8136708860759494
Weighted AreaUnderROC: 0.6349206349206349
Root mean squared error: 0.5126959555693246
Relative absolute error: 52.57142857142857
Root relative squared error: 102.53919111386492
Weighted TruePositiveRate: 0.7371428571428571
Weighted MatthewsCorrelation: 0.4373555618514747
Weighted FMeasure: 0.683962962962963
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6454423146473779
Mean absolute error: 0.26285714285714284
Coverage of cases: 73.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7371428571428571
Weighted FalsePositiveRate: 0.4673015873015873
Kappa statistic: 0.32113341204250284
Training time: 6.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8560869565217392
Weighted AreaUnderROC: 0.8397817460317459
Root mean squared error: 0.3779644730092272
Relative absolute error: 28.57142857142857
Root relative squared error: 75.59289460184544
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6872025780312658
Weighted FMeasure: 0.8563446867948856
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8045772256728779
Mean absolute error: 0.14285714285714285
Coverage of cases: 85.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.17757936507936506
Kappa statistic: 0.6867167919799497
Training time: 8.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8385037989479837
Weighted AreaUnderROC: 0.8194444444444444
Root mean squared error: 0.4
Relative absolute error: 32.0
Root relative squared error: 80.0
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6487085483482128
Weighted FMeasure: 0.8387805579522576
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7833575686732904
Mean absolute error: 0.16
Coverage of cases: 84.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.2011111111111111
Kappa statistic: 0.6478873239436619
Training time: 8.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.8500624442462088
Weighted AreaUnderROC: 0.8283730158730158
Root mean squared error: 0.38544964466377263
Relative absolute error: 29.714285714285715
Root relative squared error: 77.08992893275453
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6726641957886802
Weighted FMeasure: 0.8496521739130435
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7947650142304915
Mean absolute error: 0.14857142857142858
Coverage of cases: 85.14285714285714
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.19468253968253968
Kappa statistic: 0.6707193515704154
Training time: 10.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8667573696145126
Weighted AreaUnderROC: 0.8268849206349205
Root mean squared error: 0.3703280399090206
Relative absolute error: 27.42857142857143
Root relative squared error: 74.06560798180412
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6989093588106654
Weighted FMeasure: 0.8583193277310923
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8023730482669258
Mean absolute error: 0.13714285714285715
Coverage of cases: 86.28571428571429
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.2090873015873016
Kappa statistic: 0.6871741397288843
Training time: 11.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 50.0
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8900842601954836
Weighted AreaUnderROC: 0.8402777777777778
Root mean squared error: 0.34641016151377546
Relative absolute error: 24.0
Root relative squared error: 69.2820323027551
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7421113924493619
Weighted FMeasure: 0.8748745669800906
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8228880350522413
Mean absolute error: 0.12
Coverage of cases: 88.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 60.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.19944444444444445
Kappa statistic: 0.7232472324723247
Training time: 12.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.9028715874620828
Weighted AreaUnderROC: 0.8526785714285714
Root mean squared error: 0.3295017884191656
Relative absolute error: 21.714285714285715
Root relative squared error: 65.90035768383312
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7691562682674874
Weighted FMeasure: 0.8867912748867486
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8380192402137802
Mean absolute error: 0.10857142857142857
Coverage of cases: 89.14285714285714
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 73.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.18607142857142858
Kappa statistic: 0.7496046389035319
Training time: 12.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.9113846153846153
Weighted AreaUnderROC: 0.8571428571428571
Root mean squared error: 0.32071349029490925
Relative absolute error: 20.57142857142857
Root relative squared error: 64.14269805898185
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7844645405527362
Weighted FMeasure: 0.892396694214876
Iteration time: 15.0
Weighted AreaUnderPRC: 0.845556043956044
Mean absolute error: 0.10285714285714286
Coverage of cases: 89.71428571428571
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.18285714285714286
Kappa statistic: 0.7619047619047619
Training time: 14.0
		
Time end:Sat Oct 07 12.54.49 EEST 2017