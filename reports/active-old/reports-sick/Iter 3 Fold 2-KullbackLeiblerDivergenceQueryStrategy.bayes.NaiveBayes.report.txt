Sun Oct 08 05.26.46 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 05.26.46 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 58.11240721102863
Incorrectly Classified Instances: 6.362672322375397
Correctly Classified Instances: 93.6373276776246
Weighted Precision: 0.9297343664343973
Weighted AreaUnderROC: 0.9375608805766609
Root mean squared error: 0.2278284027873463
Relative absolute error: 16.23412033081123
Root relative squared error: 45.565680557469264
Weighted TruePositiveRate: 0.936373276776246
Weighted MatthewsCorrelation: 0.38785669437493403
Weighted FMeasure: 0.9325899519685783
Iteration time: 38.0
Weighted AreaUnderPRC: 0.9573416551375294
Mean absolute error: 0.08117060165405615
Coverage of cases: 97.66702014846236
Instances selection time: 35.0
Test time: 34.0
Accumulative iteration time: 38.0
Weighted Recall: 0.936373276776246
Weighted FalsePositiveRate: 0.5922372939202163
Kappa statistic: 0.38439108248376136
Training time: 3.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 58.98727465535525
Incorrectly Classified Instances: 5.779427359490986
Correctly Classified Instances: 94.22057264050902
Weighted Precision: 0.935302946321624
Weighted AreaUnderROC: 0.9412429378531073
Root mean squared error: 0.21011124696097555
Relative absolute error: 15.25427569953823
Root relative squared error: 42.02224939219511
Weighted TruePositiveRate: 0.9422057264050901
Weighted MatthewsCorrelation: 0.4337323367961405
Weighted FMeasure: 0.9379998113134661
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9605223378747944
Mean absolute error: 0.07627137849769115
Coverage of cases: 98.83351007423117
Instances selection time: 27.0
Test time: 35.0
Accumulative iteration time: 67.0
Weighted Recall: 0.9422057264050901
Weighted FalsePositiveRate: 0.5676879005722438
Kappa statistic: 0.42795367397026995
Training time: 2.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 57.7677624602333
Incorrectly Classified Instances: 6.415694591728526
Correctly Classified Instances: 93.58430540827148
Weighted Precision: 0.920923345095302
Weighted AreaUnderROC: 0.9408386908240796
Root mean squared error: 0.21889384590933586
Relative absolute error: 14.814191793541509
Root relative squared error: 43.77876918186717
Weighted TruePositiveRate: 0.9358430540827147
Weighted MatthewsCorrelation: 0.29628589250625637
Weighted FMeasure: 0.925902969942002
Iteration time: 50.0
Weighted AreaUnderPRC: 0.9596236080059877
Mean absolute error: 0.07407095896770755
Coverage of cases: 98.46235418875928
Instances selection time: 47.0
Test time: 43.0
Accumulative iteration time: 117.0
Weighted Recall: 0.9358430540827147
Weighted FalsePositiveRate: 0.7211635294382575
Kappa statistic: 0.27865546430987304
Training time: 3.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 57.237539766702014
Incorrectly Classified Instances: 6.468716861081655
Correctly Classified Instances: 93.53128313891834
Weighted Precision: 0.9156322587675155
Weighted AreaUnderROC: 0.9384911357880382
Root mean squared error: 0.22035558462668484
Relative absolute error: 14.587352035700846
Root relative squared error: 44.071116925336966
Weighted TruePositiveRate: 0.9353128313891834
Weighted MatthewsCorrelation: 0.23821866214833434
Weighted FMeasure: 0.9214029410624888
Iteration time: 18.0
Weighted AreaUnderPRC: 0.959090531483345
Mean absolute error: 0.07293676017850423
Coverage of cases: 98.46235418875928
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 135.0
Weighted Recall: 0.9353128313891834
Weighted FalsePositiveRate: 0.7856440217262183
Kappa statistic: 0.21080287564482436
Training time: 4.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 57.0254506892895
Incorrectly Classified Instances: 6.680805938494167
Correctly Classified Instances: 93.31919406150583
Weighted Precision: 0.9042171416439007
Weighted AreaUnderROC: 0.9357149814923047
Root mean squared error: 0.220595326709434
Relative absolute error: 14.243672730232618
Root relative squared error: 44.119065341886795
Weighted TruePositiveRate: 0.9331919406150583
Weighted MatthewsCorrelation: 0.13280869102138815
Weighted FMeasure: 0.9135886079871297
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9590490875050824
Mean absolute error: 0.07121836365116309
Coverage of cases: 98.46235418875928
Instances selection time: 7.0
Test time: 29.0
Accumulative iteration time: 146.0
Weighted Recall: 0.9331919406150583
Weighted FalsePositiveRate: 0.8663401969953428
Kappa statistic: 0.103558063737325
Training time: 4.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 59.41145281018028
Incorrectly Classified Instances: 5.9915164369035
Correctly Classified Instances: 94.0084835630965
Weighted Precision: 0.949782056448278
Weighted AreaUnderROC: 0.9348188194038572
Root mean squared error: 0.20348242376630313
Relative absolute error: 14.821746796311256
Root relative squared error: 40.69648475326063
Weighted TruePositiveRate: 0.940084835630965
Weighted MatthewsCorrelation: 0.5581972537728035
Weighted FMeasure: 0.9440623566848164
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9717404125627879
Mean absolute error: 0.07410873398155628
Coverage of cases: 99.04559915164369
Instances selection time: 0.0
Test time: 28.0
Accumulative iteration time: 151.0
Weighted Recall: 0.940084835630965
Weighted FalsePositiveRate: 0.30198820597968895
Kappa statistic: 0.5514683907683111
Training time: 5.0
		
Time end:Sun Oct 08 05.26.47 EEST 2017