Sun Oct 08 05.26.43 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 05.26.43 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 61.34676564156946
Incorrectly Classified Instances: 8.854718981972429
Correctly Classified Instances: 91.14528101802757
Weighted Precision: 0.9452837642982903
Weighted AreaUnderROC: 0.8968753040800808
Root mean squared error: 0.25001404020597906
Relative absolute error: 20.30678589623753
Root relative squared error: 50.00280804119581
Weighted TruePositiveRate: 0.9114528101802757
Weighted MatthewsCorrelation: 0.49404997256281097
Weighted FMeasure: 0.9240471489928498
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9620817093987771
Mean absolute error: 0.10153392948118764
Coverage of cases: 98.56839872746554
Instances selection time: 72.0
Test time: 59.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9114528101802757
Weighted FalsePositiveRate: 0.247474629778951
Kappa statistic: 0.4640021238287459
Training time: 6.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 60.790031813361615
Incorrectly Classified Instances: 7.794273594909862
Correctly Classified Instances: 92.20572640509013
Weighted Precision: 0.9454883764338159
Weighted AreaUnderROC: 0.8923462101111495
Root mean squared error: 0.23903669434429264
Relative absolute error: 18.809543826455844
Root relative squared error: 47.80733886885853
Weighted TruePositiveRate: 0.9220572640509014
Weighted MatthewsCorrelation: 0.50742804283443
Weighted FMeasure: 0.9310671917807144
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9581918235459762
Mean absolute error: 0.09404771913227922
Coverage of cases: 98.67444326617179
Instances selection time: 63.0
Test time: 41.0
Accumulative iteration time: 145.0
Weighted Recall: 0.9220572640509014
Weighted FalsePositiveRate: 0.27900252023636796
Kappa statistic: 0.4878275873535486
Training time: 4.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 59.94167550371156
Incorrectly Classified Instances: 6.786850477200424
Correctly Classified Instances: 93.21314952279958
Weighted Precision: 0.9435576870731558
Weighted AreaUnderROC: 0.887336685109285
Root mean squared error: 0.22347906986231114
Relative absolute error: 16.79350759871947
Root relative squared error: 44.695813972462226
Weighted TruePositiveRate: 0.9321314952279958
Weighted MatthewsCorrelation: 0.5040880278316561
Weighted FMeasure: 0.9369256521050056
Iteration time: 37.0
Weighted AreaUnderPRC: 0.9562866289556472
Mean absolute error: 0.08396753799359734
Coverage of cases: 98.93955461293743
Instances selection time: 34.0
Test time: 46.0
Accumulative iteration time: 182.0
Weighted Recall: 0.9321314952279958
Weighted FalsePositiveRate: 0.3508437492704661
Kappa statistic: 0.4971798012113941
Training time: 3.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 59.01378579003181
Incorrectly Classified Instances: 5.832449628844114
Correctly Classified Instances: 94.16755037115588
Weighted Precision: 0.945983251423991
Weighted AreaUnderROC: 0.888463444486934
Root mean squared error: 0.21337877357213852
Relative absolute error: 15.346906416959575
Root relative squared error: 42.675754714427704
Weighted TruePositiveRate: 0.9416755037115588
Weighted MatthewsCorrelation: 0.5305195222546255
Weighted FMeasure: 0.943626145127461
Iteration time: 38.0
Weighted AreaUnderPRC: 0.9575000033265438
Mean absolute error: 0.07673453208479787
Coverage of cases: 98.99257688229056
Instances selection time: 30.0
Test time: 49.0
Accumulative iteration time: 220.0
Weighted Recall: 0.9416755037115588
Weighted FalsePositiveRate: 0.37438541993988533
Kappa statistic: 0.528941082440987
Training time: 8.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 58.64262990455992
Incorrectly Classified Instances: 5.1961823966065745
Correctly Classified Instances: 94.80381760339343
Weighted Precision: 0.9497428640288158
Weighted AreaUnderROC: 0.8880317385746977
Root mean squared error: 0.20145283987818938
Relative absolute error: 13.952028633223495
Root relative squared error: 40.29056797563788
Weighted TruePositiveRate: 0.9480381760339343
Weighted MatthewsCorrelation: 0.5643108873971989
Weighted FMeasure: 0.9488453595206099
Iteration time: 25.0
Weighted AreaUnderPRC: 0.959113365875076
Mean absolute error: 0.06976014316611748
Coverage of cases: 98.99257688229056
Instances selection time: 17.0
Test time: 52.0
Accumulative iteration time: 245.0
Weighted Recall: 0.9480381760339343
Weighted FalsePositiveRate: 0.3659127133415517
Kappa statistic: 0.5639532689113694
Training time: 8.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 63.60021208907741
Incorrectly Classified Instances: 14.4220572640509
Correctly Classified Instances: 85.5779427359491
Weighted Precision: 0.937218126354831
Weighted AreaUnderROC: 0.8712679625462175
Root mean squared error: 0.3261062071847138
Relative absolute error: 30.516561765639132
Root relative squared error: 65.22124143694276
Weighted TruePositiveRate: 0.855779427359491
Weighted MatthewsCorrelation: 0.38744508104697134
Weighted FMeasure: 0.8857397425180186
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9530284068526846
Mean absolute error: 0.15258280882819567
Coverage of cases: 96.39448568398727
Instances selection time: 1.0
Test time: 53.0
Accumulative iteration time: 259.0
Weighted Recall: 0.855779427359491
Weighted FalsePositiveRate: 0.2430675629527113
Kappa statistic: 0.32906789991054103
Training time: 13.0
		
Time end:Sun Oct 08 05.26.44 EEST 2017