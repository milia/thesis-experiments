Sun Oct 08 06.00.51 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.00.51 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.726405090137858
Correctly Classified Instances: 94.27359490986214
Weighted Precision: 0.9378229911369007
Weighted AreaUnderROC: 0.7116379310344828
Root mean squared error: 0.23868128771032385
Relative absolute error: 11.9164493912872
Root relative squared error: 47.73625754206477
Weighted TruePositiveRate: 0.9427359490986215
Weighted MatthewsCorrelation: 0.45857335776535607
Weighted FMeasure: 0.9399132437099479
Iteration time: 238.0
Weighted AreaUnderPRC: 0.9211779194424489
Mean absolute error: 0.059582246956436
Coverage of cases: 94.27359490986214
Instances selection time: 237.0
Test time: 322.0
Accumulative iteration time: 238.0
Weighted Recall: 0.9427359490986215
Weighted FalsePositiveRate: 0.5273745619955628
Kappa statistic: 0.455746395476845
Training time: 1.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.408271474019088
Correctly Classified Instances: 94.59172852598091
Weighted Precision: 0.936580563303787
Weighted AreaUnderROC: 0.6674751607247223
Root mean squared error: 0.23221879760985778
Relative absolute error: 11.077959424775075
Root relative squared error: 46.44375952197156
Weighted TruePositiveRate: 0.9459172852598091
Weighted MatthewsCorrelation: 0.4316223290111286
Weighted FMeasure: 0.9388649615035505
Iteration time: 325.0
Weighted AreaUnderPRC: 0.9146694822172431
Mean absolute error: 0.05538979712387538
Coverage of cases: 94.59172852598091
Instances selection time: 324.0
Test time: 544.0
Accumulative iteration time: 563.0
Weighted Recall: 0.9459172852598091
Weighted FalsePositiveRate: 0.6157789645896357
Kappa statistic: 0.41339269378544885
Training time: 1.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.355249204665959
Correctly Classified Instances: 94.64475079533403
Weighted Precision: 0.9356574809401118
Weighted AreaUnderROC: 0.6284239236314048
Root mean squared error: 0.23117985538982716
Relative absolute error: 10.891843675067348
Root relative squared error: 46.23597107796543
Weighted TruePositiveRate: 0.9464475079533404
Weighted MatthewsCorrelation: 0.3938004754358682
Weighted FMeasure: 0.9351658281595174
Iteration time: 351.0
Weighted AreaUnderPRC: 0.908365309443123
Mean absolute error: 0.05445921837533674
Coverage of cases: 94.64475079533403
Instances selection time: 350.0
Test time: 768.0
Accumulative iteration time: 914.0
Weighted Recall: 0.9464475079533404
Weighted FalsePositiveRate: 0.6963013945693545
Kappa statistic: 0.3503338949407586
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.514316012725344
Correctly Classified Instances: 94.48568398727465
Weighted Precision: 0.9327375461747235
Weighted AreaUnderROC: 0.6151763101500097
Root mean squared error: 0.23464342982600386
Relative absolute error: 11.167074874913517
Root relative squared error: 46.92868596520077
Weighted TruePositiveRate: 0.9448568398727466
Weighted MatthewsCorrelation: 0.3651816135221545
Weighted FMeasure: 0.9325081218566766
Iteration time: 304.0
Weighted AreaUnderPRC: 0.9055783514156128
Mean absolute error: 0.055835374374567585
Coverage of cases: 94.48568398727465
Instances selection time: 304.0
Test time: 1000.0
Accumulative iteration time: 1218.0
Weighted Recall: 0.9448568398727466
Weighted FalsePositiveRate: 0.7205727954542779
Kappa statistic: 0.3195209614076967
Training time: 0.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.6203605514316015
Correctly Classified Instances: 94.3796394485684
Weighted Precision: 0.9306579059228252
Weighted AreaUnderROC: 0.5973894408727838
Root mean squared error: 0.23692364462915
Relative absolute error: 11.352189285500195
Root relative squared error: 47.38472892583
Weighted TruePositiveRate: 0.943796394485684
Weighted MatthewsCorrelation: 0.3302259754467376
Weighted FMeasure: 0.9290702843215501
Iteration time: 202.0
Weighted AreaUnderPRC: 0.9021475622813518
Mean absolute error: 0.05676094642750098
Coverage of cases: 94.3796394485684
Instances selection time: 201.0
Test time: 1219.0
Accumulative iteration time: 1420.0
Weighted Recall: 0.943796394485684
Weighted FalsePositiveRate: 0.76092088308884
Kappa statistic: 0.2730644480968098
Training time: 1.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.665959703075291
Correctly Classified Instances: 95.33404029692471
Weighted Precision: 0.9490294864699398
Weighted AreaUnderROC: 0.7494398986947204
Root mean squared error: 0.2158939846440348
Relative absolute error: 9.427524487903597
Root relative squared error: 43.17879692880696
Weighted TruePositiveRate: 0.953340402969247
Weighted MatthewsCorrelation: 0.5534991355628818
Weighted FMeasure: 0.9505659647769573
Iteration time: 9.0
Weighted AreaUnderPRC: 0.931085590494806
Mean absolute error: 0.04713762243951798
Coverage of cases: 95.33404029692471
Instances selection time: 8.0
Test time: 1451.0
Accumulative iteration time: 1429.0
Weighted Recall: 0.953340402969247
Weighted FalsePositiveRate: 0.4622338376078599
Kappa statistic: 0.5485534604880912
Training time: 1.0
		
Time end:Sun Oct 08 06.00.58 EEST 2017