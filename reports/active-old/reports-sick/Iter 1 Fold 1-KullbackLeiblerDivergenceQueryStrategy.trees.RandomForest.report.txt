Sun Oct 08 05.28.25 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 05.28.25 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 56.49522799575822
Incorrectly Classified Instances: 3.552492046659597
Correctly Classified Instances: 96.4475079533404
Weighted Precision: 0.9622266967576296
Weighted AreaUnderROC: 0.9189293992466107
Root mean squared error: 0.1576115249013702
Relative absolute error: 9.326362788503264
Root relative squared error: 31.52230498027404
Weighted TruePositiveRate: 0.9644750795334041
Weighted MatthewsCorrelation: 0.6365179624569355
Weighted FMeasure: 0.9594182433420478
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9767365322190152
Mean absolute error: 0.046631813942516316
Coverage of cases: 99.2576882290562
Instances selection time: 15.0
Test time: 17.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9644750795334041
Weighted FalsePositiveRate: 0.49016677913814716
Kappa statistic: 0.6045775154743058
Training time: 53.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 55.116648992576884
Incorrectly Classified Instances: 4.7720042417815485
Correctly Classified Instances: 95.22799575821846
Weighted Precision: 0.9490277447051565
Weighted AreaUnderROC: 0.9164763633249964
Root mean squared error: 0.18182328654046445
Relative absolute error: 9.994030045903395
Root relative squared error: 36.36465730809289
Weighted TruePositiveRate: 0.9522799575821845
Weighted MatthewsCorrelation: 0.45609611144285417
Weighted FMeasure: 0.9396000951686625
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9721498212344684
Mean absolute error: 0.04997015022951698
Coverage of cases: 99.15164369034994
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9522799575821845
Weighted FalsePositiveRate: 0.7023646554929694
Kappa statistic: 0.37489412163306784
Training time: 88.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 54.63944856839873
Incorrectly Classified Instances: 5.03711558854719
Correctly Classified Instances: 94.9628844114528
Weighted Precision: 0.9498741471027664
Weighted AreaUnderROC: 0.9074755112562296
Root mean squared error: 0.1897005449546438
Relative absolute error: 10.248887665291043
Root relative squared error: 37.94010899092876
Weighted TruePositiveRate: 0.9496288441145281
Weighted MatthewsCorrelation: 0.4056956754194713
Weighted FMeasure: 0.933176427719056
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9717317681503391
Mean absolute error: 0.05124443832645521
Coverage of cases: 99.09862142099682
Instances selection time: 11.0
Test time: 15.0
Accumulative iteration time: 307.0
Weighted Recall: 0.9496288441145281
Weighted FalsePositiveRate: 0.7675848012009201
Kappa statistic: 0.29271835845287747
Training time: 130.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 55.22269353128314
Incorrectly Classified Instances: 4.612937433722164
Correctly Classified Instances: 95.38706256627783
Weighted Precision: 0.956030612512799
Weighted AreaUnderROC: 0.9073371324143605
Root mean squared error: 0.17031262659121954
Relative absolute error: 9.339840431201067
Root relative squared error: 34.06252531824391
Weighted TruePositiveRate: 0.9538706256627784
Weighted MatthewsCorrelation: 0.481744229535825
Weighted FMeasure: 0.9403912058879831
Iteration time: 185.0
Weighted AreaUnderPRC: 0.9768314264866004
Mean absolute error: 0.046699202156005336
Coverage of cases: 99.04559915164369
Instances selection time: 11.0
Test time: 18.0
Accumulative iteration time: 492.0
Weighted Recall: 0.9538706256627784
Weighted FalsePositiveRate: 0.7103923647932131
Kappa statistic: 0.3767254936222261
Training time: 174.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 54.2152704135737
Incorrectly Classified Instances: 5.302226935312832
Correctly Classified Instances: 94.69777306468717
Weighted Precision: 0.9498116306657455
Weighted AreaUnderROC: 0.8953918528334129
Root mean squared error: 0.19107324982089588
Relative absolute error: 9.896418683321114
Root relative squared error: 38.21464996417917
Weighted TruePositiveRate: 0.9469777306468717
Weighted MatthewsCorrelation: 0.3513735740807299
Weighted FMeasure: 0.9273124780417745
Iteration time: 197.0
Weighted AreaUnderPRC: 0.9731081281054341
Mean absolute error: 0.04948209341660557
Coverage of cases: 98.72746553552491
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 689.0
Weighted Recall: 0.9469777306468717
Weighted FalsePositiveRate: 0.8165429480381761
Kappa statistic: 0.21979067554709908
Training time: 191.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 56.60127253446448
Incorrectly Classified Instances: 2.1208907741251326
Correctly Classified Instances: 97.87910922587487
Weighted Precision: 0.9784929446709055
Weighted AreaUnderROC: 0.9559305004092684
Root mean squared error: 0.1341903404993435
Relative absolute error: 7.369838540555959
Root relative squared error: 26.8380680998687
Weighted TruePositiveRate: 0.9787910922587487
Weighted MatthewsCorrelation: 0.7985237585610421
Weighted FMeasure: 0.9770523694827415
Iteration time: 372.0
Weighted AreaUnderPRC: 0.9873655008014935
Mean absolute error: 0.036849192702779796
Coverage of cases: 99.57582184517497
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 1061.0
Weighted Recall: 0.9787910922587487
Weighted FalsePositiveRate: 0.3022241809092286
Kappa statistic: 0.7850871445420001
Training time: 364.0
		
Time end:Sun Oct 08 05.28.27 EEST 2017