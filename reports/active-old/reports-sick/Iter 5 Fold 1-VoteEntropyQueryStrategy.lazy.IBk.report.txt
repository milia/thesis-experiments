Sun Oct 08 06.01.12 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.01.12 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.256627783669141
Correctly Classified Instances: 93.74337221633085
Weighted Precision: 0.9278357447268987
Weighted AreaUnderROC: 0.6507254560184617
Root mean squared error: 0.24948558572899404
Relative absolute error: 12.97404318556285
Root relative squared error: 49.89711714579881
Weighted TruePositiveRate: 0.9374337221633086
Weighted MatthewsCorrelation: 0.36173201495127266
Weighted FMeasure: 0.9315743232502157
Iteration time: 249.0
Weighted AreaUnderPRC: 0.9102509880869486
Mean absolute error: 0.06487021592781425
Coverage of cases: 93.74337221633085
Instances selection time: 249.0
Test time: 319.0
Accumulative iteration time: 249.0
Weighted Recall: 0.9374337221633086
Weighted FalsePositiveRate: 0.6382807012711573
Kappa statistic: 0.3538132763456655
Training time: 0.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.885471898197243
Correctly Classified Instances: 94.11452810180276
Weighted Precision: 0.9288617886178862
Weighted AreaUnderROC: 0.6346893182431934
Root mean squared error: 0.24224680795841882
Relative absolute error: 12.029445560467114
Root relative squared error: 48.44936159168377
Weighted TruePositiveRate: 0.9411452810180275
Weighted MatthewsCorrelation: 0.35783247076667907
Weighted FMeasure: 0.9323466419461051
Iteration time: 353.0
Weighted AreaUnderPRC: 0.9082777376642226
Mean absolute error: 0.060147227802335564
Coverage of cases: 94.11452810180276
Instances selection time: 352.0
Test time: 547.0
Accumulative iteration time: 602.0
Weighted Recall: 0.9411452810180275
Weighted FalsePositiveRate: 0.6786946881326521
Kappa statistic: 0.33803636363636347
Training time: 1.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.885471898197243
Correctly Classified Instances: 94.11452810180276
Weighted Precision: 0.9265832205384078
Weighted AreaUnderROC: 0.6096727469128225
Root mean squared error: 0.24235412610361007
Relative absolute error: 11.9504475425704
Root relative squared error: 48.47082522072201
Weighted TruePositiveRate: 0.9411452810180275
Weighted MatthewsCorrelation: 0.3246365356100001
Weighted FMeasure: 0.9295956825975861
Iteration time: 376.0
Weighted AreaUnderPRC: 0.9041068025348583
Mean absolute error: 0.059752237712852
Coverage of cases: 94.11452810180276
Instances selection time: 375.0
Test time: 768.0
Accumulative iteration time: 978.0
Weighted Recall: 0.9411452810180275
Weighted FalsePositiveRate: 0.7274806847447356
Kappa statistic: 0.2936520254539809
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.097560975609756
Correctly Classified Instances: 93.90243902439025
Weighted Precision: 0.9221021689044252
Weighted AreaUnderROC: 0.5940073159354822
Root mean squared error: 0.24674048550308153
Relative absolute error: 12.33155440189195
Root relative squared error: 49.34809710061631
Weighted TruePositiveRate: 0.9390243902439024
Weighted MatthewsCorrelation: 0.2835656763209239
Weighted FMeasure: 0.9260094627694551
Iteration time: 327.0
Weighted AreaUnderPRC: 0.9008988650931969
Mean absolute error: 0.061657772009459745
Coverage of cases: 93.90243902439025
Instances selection time: 326.0
Test time: 998.0
Accumulative iteration time: 1305.0
Weighted Recall: 0.9390243902439024
Weighted FalsePositiveRate: 0.7601424026662627
Kappa statistic: 0.25146848706143743
Training time: 1.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.9915164369035
Correctly Classified Instances: 94.0084835630965
Weighted Precision: 0.9236053899894436
Weighted AreaUnderROC: 0.5961701814253799
Root mean squared error: 0.24462148509153256
Relative absolute error: 12.09354406297765
Root relative squared error: 48.92429701830651
Weighted TruePositiveRate: 0.940084835630965
Weighted MatthewsCorrelation: 0.29191260513543116
Weighted FMeasure: 0.9267604665369277
Iteration time: 202.0
Weighted AreaUnderPRC: 0.9014366124745493
Mean absolute error: 0.06046772031488824
Coverage of cases: 94.0084835630965
Instances selection time: 201.0
Test time: 1222.0
Accumulative iteration time: 1507.0
Weighted Recall: 0.940084835630965
Weighted FalsePositiveRate: 0.7600735425761936
Kappa statistic: 0.25598201381082386
Training time: 1.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.931071049840933
Correctly Classified Instances: 95.06892895015906
Weighted Precision: 0.9471216843802517
Weighted AreaUnderROC: 0.7506125254707485
Root mean squared error: 0.22194256839562748
Relative absolute error: 9.957045568279327
Root relative squared error: 44.388513679125495
Weighted TruePositiveRate: 0.9506892895015907
Weighted MatthewsCorrelation: 0.5357901548088434
Weighted FMeasure: 0.9486006896466429
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9306808705575565
Mean absolute error: 0.049785227841396634
Coverage of cases: 95.06892895015906
Instances selection time: 8.0
Test time: 1446.0
Accumulative iteration time: 1516.0
Weighted Recall: 0.9506892895015907
Weighted FalsePositiveRate: 0.45853796256765506
Kappa statistic: 0.5333496512054834
Training time: 1.0
		
Time end:Sun Oct 08 06.01.19 EEST 2017