Sun Oct 08 05.26.46 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 05.26.46 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 62.67232237539767
Incorrectly Classified Instances: 7.688229056203605
Correctly Classified Instances: 92.31177094379639
Weighted Precision: 0.9540819653803871
Weighted AreaUnderROC: 0.9124812206558715
Root mean squared error: 0.24029837962754744
Relative absolute error: 19.9704946171881
Root relative squared error: 48.05967592550949
Weighted TruePositiveRate: 0.9231177094379639
Weighted MatthewsCorrelation: 0.5631795987745428
Weighted FMeasure: 0.9339920535418381
Iteration time: 39.0
Weighted AreaUnderPRC: 0.9692624553403512
Mean absolute error: 0.09985247308594049
Coverage of cases: 99.04559915164369
Instances selection time: 35.0
Test time: 31.0
Accumulative iteration time: 39.0
Weighted Recall: 0.9231177094379639
Weighted FalsePositiveRate: 0.16761234523694757
Kappa statistic: 0.529481598733676
Training time: 4.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 63.57370095440085
Incorrectly Classified Instances: 8.430540827147402
Correctly Classified Instances: 91.5694591728526
Weighted Precision: 0.9506023104482204
Weighted AreaUnderROC: 0.8942668286575336
Root mean squared error: 0.24594619954366048
Relative absolute error: 21.14339764952496
Root relative squared error: 49.1892399087321
Weighted TruePositiveRate: 0.915694591728526
Weighted MatthewsCorrelation: 0.5308371048388957
Weighted FMeasure: 0.9281925249637475
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9633394780244247
Mean absolute error: 0.1057169882476248
Coverage of cases: 98.93955461293743
Instances selection time: 27.0
Test time: 30.0
Accumulative iteration time: 69.0
Weighted Recall: 0.915694591728526
Weighted FalsePositiveRate: 0.19248736417347237
Kappa statistic: 0.49555056875338543
Training time: 3.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 62.77836691410393
Incorrectly Classified Instances: 7.423117709437964
Correctly Classified Instances: 92.57688229056204
Weighted Precision: 0.9506734979657694
Weighted AreaUnderROC: 0.9021239745085933
Root mean squared error: 0.23083728760897046
Relative absolute error: 19.063692110511614
Root relative squared error: 46.16745752179409
Weighted TruePositiveRate: 0.9257688229056203
Weighted MatthewsCorrelation: 0.5439176893003443
Weighted FMeasure: 0.9349463068734062
Iteration time: 26.0
Weighted AreaUnderPRC: 0.964833788323158
Mean absolute error: 0.09531846055255806
Coverage of cases: 99.15164369034994
Instances selection time: 22.0
Test time: 30.0
Accumulative iteration time: 95.0
Weighted Recall: 0.9257688229056203
Weighted FalsePositiveRate: 0.2243571910592059
Kappa statistic: 0.5197054639779095
Training time: 4.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 62.089077412513255
Incorrectly Classified Instances: 6.6277836691410394
Correctly Classified Instances: 93.37221633085896
Weighted Precision: 0.9496596691111931
Weighted AreaUnderROC: 0.8961713191138245
Root mean squared error: 0.21997547141120763
Relative absolute error: 17.74687804274558
Root relative squared error: 43.995094282241524
Weighted TruePositiveRate: 0.9337221633085896
Weighted MatthewsCorrelation: 0.5466330034132549
Weighted FMeasure: 0.9399261939652291
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9638991761665727
Mean absolute error: 0.08873439021372789
Coverage of cases: 99.09862142099682
Instances selection time: 14.0
Test time: 41.0
Accumulative iteration time: 113.0
Weighted Recall: 0.9337221633085896
Weighted FalsePositiveRate: 0.272626736995772
Kappa statistic: 0.5331997449676654
Training time: 4.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 61.16118769883351
Incorrectly Classified Instances: 5.832449628844114
Correctly Classified Instances: 94.16755037115588
Weighted Precision: 0.951509044357509
Weighted AreaUnderROC: 0.8968602194062104
Root mean squared error: 0.20531202753240352
Relative absolute error: 15.856639510218892
Root relative squared error: 41.0624055064807
Weighted TruePositiveRate: 0.9416755037115588
Weighted MatthewsCorrelation: 0.5691917746655495
Weighted FMeasure: 0.9456627442725979
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9638102768317793
Mean absolute error: 0.07928319755109446
Coverage of cases: 99.20466595970308
Instances selection time: 8.0
Test time: 27.0
Accumulative iteration time: 125.0
Weighted Recall: 0.9416755037115588
Weighted FalsePositiveRate: 0.28837228519094904
Kappa statistic: 0.5619232637202526
Training time: 4.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 62.38069989395546
Incorrectly Classified Instances: 10.286320254506894
Correctly Classified Instances: 89.71367974549311
Weighted Precision: 0.9502108333251634
Weighted AreaUnderROC: 0.9008511705915921
Root mean squared error: 0.27796766756071356
Relative absolute error: 23.86769255532386
Root relative squared error: 55.59353351214271
Weighted TruePositiveRate: 0.897136797454931
Weighted MatthewsCorrelation: 0.5060467448246503
Weighted FMeasure: 0.9156792365795999
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9670661985269878
Mean absolute error: 0.1193384627766193
Coverage of cases: 97.45493107104984
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 131.0
Weighted Recall: 0.897136797454931
Weighted FalsePositiveRate: 0.15303741857294353
Kappa statistic: 0.4530702055971944
Training time: 5.0
		
Time end:Sun Oct 08 05.26.46 EEST 2017