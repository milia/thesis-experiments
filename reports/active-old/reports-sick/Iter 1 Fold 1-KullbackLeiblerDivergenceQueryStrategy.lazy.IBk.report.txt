Sun Oct 08 05.26.58 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sun Oct 08 05.26.58 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.468716861081655
Correctly Classified Instances: 93.53128313891834
Weighted Precision: 0.9213111001104526
Weighted AreaUnderROC: 0.6246728696634178
Root mean squared error: 0.25367813328579053
Relative absolute error: 13.387374723823736
Root relative squared error: 50.73562665715811
Weighted TruePositiveRate: 0.9353128313891834
Weighted MatthewsCorrelation: 0.29791371809919026
Weighted FMeasure: 0.9263321088876698
Iteration time: 460.0
Weighted AreaUnderPRC: 0.9049866632481736
Mean absolute error: 0.06693687361911868
Coverage of cases: 93.53128313891834
Instances selection time: 459.0
Test time: 314.0
Accumulative iteration time: 460.0
Weighted Recall: 0.9353128313891834
Weighted FalsePositiveRate: 0.7115974163694205
Kappa statistic: 0.2836889589001859
Training time: 1.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.2036055143160125
Correctly Classified Instances: 93.79639448568399
Weighted Precision: 0.9191544671405544
Weighted AreaUnderROC: 0.577858247612501
Root mean squared error: 0.24870764218213334
Relative absolute error: 12.664683187680078
Root relative squared error: 49.74152843642667
Weighted TruePositiveRate: 0.9379639448568399
Weighted MatthewsCorrelation: 0.2546968426012482
Weighted FMeasure: 0.9235983935463425
Iteration time: 622.0
Weighted AreaUnderPRC: 0.8981262822900643
Mean absolute error: 0.0633234159384004
Coverage of cases: 93.79639448568399
Instances selection time: 621.0
Test time: 522.0
Accumulative iteration time: 1082.0
Weighted Recall: 0.9379639448568399
Weighted FalsePositiveRate: 0.7846042610623734
Kappa statistic: 0.2206336311941512
Training time: 1.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.779427359490986
Correctly Classified Instances: 94.22057264050902
Weighted Precision: 0.9265165608368372
Weighted AreaUnderROC: 0.576822232587828
Root mean squared error: 0.24016085901725306
Relative absolute error: 11.738079278886612
Root relative squared error: 48.032171803450616
Weighted TruePositiveRate: 0.9422057264050901
Weighted MatthewsCorrelation: 0.28019817432185745
Weighted FMeasure: 0.9253177177217261
Iteration time: 652.0
Weighted AreaUnderPRC: 0.8988304357876117
Mean absolute error: 0.05869039639443306
Coverage of cases: 94.22057264050902
Instances selection time: 651.0
Test time: 722.0
Accumulative iteration time: 1734.0
Weighted Recall: 0.9422057264050901
Weighted FalsePositiveRate: 0.8005908195727921
Kappa statistic: 0.21911584833129508
Training time: 1.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.673382820784729
Correctly Classified Instances: 94.32661717921528
Weighted Precision: 0.930881238072831
Weighted AreaUnderROC: 0.5653745120663836
Root mean squared error: 0.2380036073312162
Relative absolute error: 11.484227719896571
Root relative squared error: 47.60072146624324
Weighted TruePositiveRate: 0.9432661717921527
Weighted MatthewsCorrelation: 0.2764776951680668
Weighted FMeasure: 0.9240452252382482
Iteration time: 558.0
Weighted AreaUnderPRC: 0.8973994433553514
Mean absolute error: 0.057421138599482856
Coverage of cases: 94.32661717921528
Instances selection time: 556.0
Test time: 946.0
Accumulative iteration time: 2292.0
Weighted Recall: 0.9432661717921527
Weighted FalsePositiveRate: 0.8249149577887649
Kappa statistic: 0.19282428702851803
Training time: 2.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.6203605514316015
Correctly Classified Instances: 94.3796394485684
Weighted Precision: 0.9344058807408959
Weighted AreaUnderROC: 0.5636560037316181
Root mean squared error: 0.23692364206490749
Relative absolute error: 11.351915190604881
Root relative squared error: 47.384728412981495
Weighted TruePositiveRate: 0.943796394485684
Weighted MatthewsCorrelation: 0.2804849293320053
Weighted FMeasure: 0.9236895218602535
Iteration time: 336.0
Weighted AreaUnderPRC: 0.8973463019644233
Mean absolute error: 0.0567595759530244
Coverage of cases: 94.3796394485684
Instances selection time: 335.0
Test time: 1139.0
Accumulative iteration time: 2628.0
Weighted Recall: 0.943796394485684
Weighted FalsePositiveRate: 0.8330115271790776
Kappa statistic: 0.18415618547024581
Training time: 1.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.71898197242842
Correctly Classified Instances: 95.28101802757158
Weighted Precision: 0.9471840139913027
Weighted AreaUnderROC: 0.729204821643385
Root mean squared error: 0.2171171782577372
Relative absolute error: 9.53294747606455
Root relative squared error: 43.423435651547436
Weighted TruePositiveRate: 0.9528101802757158
Weighted MatthewsCorrelation: 0.5290818332323365
Weighted FMeasure: 0.9488060664138178
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9278383213406091
Mean absolute error: 0.047664737380322744
Coverage of cases: 95.28101802757158
Instances selection time: 12.0
Test time: 1363.0
Accumulative iteration time: 2642.0
Weighted Recall: 0.9528101802757158
Weighted FalsePositiveRate: 0.5071862389996006
Kappa statistic: 0.5195523370638582
Training time: 2.0
		
Time end:Sun Oct 08 05.27.06 EEST 2017