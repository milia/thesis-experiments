Sun Oct 08 06.01.34 EEST 2017
Dataset: sick
Test set size: 1886
Initial Labelled set size: 377
Initial Unlabelled set size: 1509
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sun Oct 08 06.01.34 EEST 2017
		
Iteration: 1
Labeled set size: 377
Unlabelled set size: 1509
	
Mean region size: 57.37009544008484
Incorrectly Classified Instances: 5.03711558854719
Correctly Classified Instances: 94.9628844114528
Weighted Precision: 0.9481800398080145
Weighted AreaUnderROC: 0.8900408053517681
Root mean squared error: 0.18908824918585176
Relative absolute error: 11.421371128854082
Root relative squared error: 37.81764983717035
Weighted TruePositiveRate: 0.9496288441145281
Weighted MatthewsCorrelation: 0.41415244285924646
Weighted FMeasure: 0.9340055222880311
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9649668578653032
Mean absolute error: 0.05710685564427041
Coverage of cases: 98.99257688229056
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9496288441145281
Weighted FalsePositiveRate: 0.7524829255483874
Kappa statistic: 0.3112184095399923
Training time: 9.0
		
Iteration: 2
Labeled set size: 677
Unlabelled set size: 1209
	
Mean region size: 55.885471898197245
Incorrectly Classified Instances: 5.6203605514316015
Correctly Classified Instances: 94.3796394485684
Weighted Precision: 0.9469720779529107
Weighted AreaUnderROC: 0.8829846186869359
Root mean squared error: 0.19817374088413195
Relative absolute error: 11.184439706607243
Root relative squared error: 39.63474817682639
Weighted TruePositiveRate: 0.943796394485684
Weighted MatthewsCorrelation: 0.2851945497051631
Weighted FMeasure: 0.9209721978345359
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9660641955269738
Mean absolute error: 0.05592219853303622
Coverage of cases: 98.72746553552491
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 34.0
Weighted Recall: 0.943796394485684
Weighted FalsePositiveRate: 0.8575894979339599
Kappa statistic: 0.1504360094511209
Training time: 16.0
		
Iteration: 3
Labeled set size: 977
Unlabelled set size: 909
	
Mean region size: 54.665959703075295
Incorrectly Classified Instances: 5.567338282078473
Correctly Classified Instances: 94.43266171792153
Weighted Precision: 0.9474443266171791
Weighted AreaUnderROC: 0.845939473421619
Root mean squared error: 0.1989047094686849
Relative absolute error: 10.749196163280866
Root relative squared error: 39.78094189373698
Weighted TruePositiveRate: 0.9443266171792153
Weighted MatthewsCorrelation: 0.2991943204329093
Weighted FMeasure: 0.9221139097921468
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9599097616653051
Mean absolute error: 0.05374598081640433
Coverage of cases: 98.35630965005302
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 59.0
Weighted Recall: 0.9443266171792153
Weighted FalsePositiveRate: 0.8494990309723187
Kappa statistic: 0.16432459805038643
Training time: 22.0
		
Iteration: 4
Labeled set size: 1277
Unlabelled set size: 609
	
Mean region size: 54.612937433722166
Incorrectly Classified Instances: 5.249204665959703
Correctly Classified Instances: 94.75079533404029
Weighted Precision: 0.9502884309323238
Weighted AreaUnderROC: 0.8771577238458076
Root mean squared error: 0.1863213584138538
Relative absolute error: 9.759706195544934
Root relative squared error: 37.26427168277076
Weighted TruePositiveRate: 0.9475079533404029
Weighted MatthewsCorrelation: 0.3725438930292022
Weighted FMeasure: 0.9286854700471883
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9689571891021042
Mean absolute error: 0.04879853097772467
Coverage of cases: 98.78048780487805
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9475079533404029
Weighted FalsePositiveRate: 0.8009562292024719
Kappa statistic: 0.24374832924250878
Training time: 29.0
		
Iteration: 5
Labeled set size: 1577
Unlabelled set size: 309
	
Mean region size: 54.71898197242842
Incorrectly Classified Instances: 5.832449628844114
Correctly Classified Instances: 94.16755037115588
Weighted Precision: 0.9450881072177975
Weighted AreaUnderROC: 0.8748141741448416
Root mean squared error: 0.20057742472119827
Relative absolute error: 10.849907399542817
Root relative squared error: 40.115484944239654
Weighted TruePositiveRate: 0.9416755037115588
Weighted MatthewsCorrelation: 0.22067561171385128
Weighted FMeasure: 0.9162605356904291
Iteration time: 38.0
Weighted AreaUnderPRC: 0.9638961899658481
Mean absolute error: 0.05424953699771408
Coverage of cases: 98.72746553552491
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 128.0
Weighted Recall: 0.9416755037115588
Weighted FalsePositiveRate: 0.8899513657805245
Kappa statistic: 0.092872759073021
Training time: 36.0
		
Iteration: 6
Labeled set size: 1877
Unlabelled set size: 9
	
Mean region size: 57.0254506892895
Incorrectly Classified Instances: 2.0678685047720045
Correctly Classified Instances: 97.93213149522799
Weighted Precision: 0.9786190888961793
Weighted AreaUnderROC: 0.9833992238068645
Root mean squared error: 0.12413991262167962
Relative absolute error: 7.219673507404691
Root relative squared error: 24.827982524335923
Weighted TruePositiveRate: 0.9793213149522799
Weighted MatthewsCorrelation: 0.8068713381944297
Weighted FMeasure: 0.9780378103631112
Iteration time: 60.0
Weighted AreaUnderPRC: 0.9923583674485393
Mean absolute error: 0.03609836753702345
Coverage of cases: 99.78791092258749
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9793213149522799
Weighted FalsePositiveRate: 0.26719390408144417
Kappa statistic: 0.7990229027657094
Training time: 60.0
		
Time end:Sun Oct 08 06.01.35 EEST 2017