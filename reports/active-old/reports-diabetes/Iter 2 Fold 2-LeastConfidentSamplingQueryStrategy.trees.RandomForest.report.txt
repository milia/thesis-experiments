Sat Oct 07 11.45.04 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.04 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 87.890625
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6969311482313308
Weighted AreaUnderROC: 0.7474925373134328
Root mean squared error: 0.45803020278871004
Relative absolute error: 69.47916666666666
Root relative squared error: 91.606040557742
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.3329464961715268
Weighted FMeasure: 0.6960793821972425
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7475392518918828
Mean absolute error: 0.3473958333333333
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.3606856343283582
Kappa statistic: 0.3328977846409693
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 89.84375
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7105681883648095
Weighted AreaUnderROC: 0.7794029850746269
Root mean squared error: 0.4348431230532073
Relative absolute error: 65.05208333333329
Root relative squared error: 86.96862461064147
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.36195203501703155
Weighted FMeasure: 0.7125069277223561
Iteration time: 8.0
Weighted AreaUnderPRC: 0.77177575154255
Mean absolute error: 0.32526041666666644
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.3633697139303482
Kappa statistic: 0.3608990411042571
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 88.28125
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7400894054503874
Weighted AreaUnderROC: 0.8052388059701493
Root mean squared error: 0.4175823272122517
Relative absolute error: 59.895833333333336
Root relative squared error: 83.51646544245034
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.416849403754396
Weighted FMeasure: 0.7358680076906334
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7954701606478177
Mean absolute error: 0.2994791666666667
Coverage of cases: 98.69791666666667
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3673958333333333
Kappa statistic: 0.4060088027045991
Training time: 16.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 86.45833333333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7400894054503874
Weighted AreaUnderROC: 0.7849850746268657
Root mean squared error: 0.42738302493196917
Relative absolute error: 61.14583333333331
Root relative squared error: 85.47660498639384
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.416849403754396
Weighted FMeasure: 0.7358680076906334
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7770611661656318
Mean absolute error: 0.30572916666666655
Coverage of cases: 97.65625
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 57.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3673958333333333
Kappa statistic: 0.4060088027045991
Training time: 14.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7195457175925926
Weighted AreaUnderROC: 0.8016567164179106
Root mean squared error: 0.4217399475190053
Relative absolute error: 59.27083333333331
Root relative squared error: 84.34798950380106
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.372219375747142
Weighted FMeasure: 0.7163582242336082
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7876659591178153
Mean absolute error: 0.29635416666666653
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 70.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.39101741293532344
Kappa statistic: 0.36196319018404893
Training time: 12.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 87.36979166666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7262073863636364
Weighted AreaUnderROC: 0.783865671641791
Root mean squared error: 0.43050963016716215
Relative absolute error: 60.572916666666686
Root relative squared error: 86.10192603343243
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3807726202487147
Weighted FMeasure: 0.7180446242946243
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7822377363779219
Mean absolute error: 0.30286458333333344
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 86.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.39861380597014934
Kappa statistic: 0.36481577581733254
Training time: 15.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 88.671875
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.758359593837535
Weighted AreaUnderROC: 0.8009701492537312
Root mean squared error: 0.41523713104682736
Relative absolute error: 61.718749999999964
Root relative squared error: 83.04742620936547
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4663619199304539
Weighted FMeasure: 0.7594469121352828
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7851647849450045
Mean absolute error: 0.30859374999999983
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 110.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3105432213930348
Kappa statistic: 0.4645418326693228
Training time: 23.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7627371399532414
Weighted AreaUnderROC: 0.8126716417910448
Root mean squared error: 0.4094584838539799
Relative absolute error: 59.479166666666686
Root relative squared error: 81.89169677079599
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.4693263382514538
Weighted FMeasure: 0.7598365972172559
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8037924600249505
Mean absolute error: 0.2973958333333334
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 129.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3319903606965174
Kappa statistic: 0.46098037978676415
Training time: 19.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 86.58854166666667
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7087053571428571
Weighted AreaUnderROC: 0.7767462686567163
Root mean squared error: 0.4318986667417873
Relative absolute error: 62.552083333333265
Root relative squared error: 86.37973334835746
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.35295644433718315
Weighted FMeasure: 0.708984065324243
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7764886501002843
Mean absolute error: 0.3127604166666663
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 151.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.3896753731343284
Kappa statistic: 0.34710327455919393
Training time: 21.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 83.85416666666667
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7206622383252818
Weighted AreaUnderROC: 0.7746119402985073
Root mean squared error: 0.43301270189221946
Relative absolute error: 61.24999999999999
Root relative squared error: 86.60254037844389
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.38050480490288896
Weighted FMeasure: 0.7213113680880704
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7689044714417133
Mean absolute error: 0.30624999999999997
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 174.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.3702412935323383
Kappa statistic: 0.3758439609902475
Training time: 22.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 84.50520833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7259718168861138
Weighted AreaUnderROC: 0.7918805970149254
Root mean squared error: 0.4301162633521315
Relative absolute error: 60.72916666666664
Root relative squared error: 86.0232526704263
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3912233949539621
Weighted FMeasure: 0.7259232954545455
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7808903345378351
Mean absolute error: 0.3036458333333332
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 199.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3674496268656717
Kappa statistic: 0.3856192746894214
Training time: 24.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7516515485265485
Weighted AreaUnderROC: 0.7935820895522389
Root mean squared error: 0.4164582812239422
Relative absolute error: 59.374999999999986
Root relative squared error: 83.29165624478844
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44913243260585123
Weighted FMeasure: 0.7517698618644397
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7894810003475685
Mean absolute error: 0.29687499999999994
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 225.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3306483208955224
Kappa statistic: 0.44487968662562954
Training time: 25.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7480839657375693
Weighted AreaUnderROC: 0.7902835820895522
Root mean squared error: 0.4235883418288721
Relative absolute error: 62.291666666666714
Root relative squared error: 84.71766836577443
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44427789981020976
Weighted FMeasure: 0.7494326434277414
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7787719277659936
Mean absolute error: 0.31145833333333356
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 253.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31958924129353233
Kappa statistic: 0.4429854027972881
Training time: 27.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7475812258865812
Weighted AreaUnderROC: 0.8009253731343283
Root mean squared error: 0.42071986919247517
Relative absolute error: 62.13541666666673
Root relative squared error: 84.14397383849503
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4427330800764766
Weighted FMeasure: 0.748873150031339
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7912923067766529
Mean absolute error: 0.31067708333333366
Coverage of cases: 99.21875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 282.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32305192786069653
Kappa statistic: 0.44100520992951264
Training time: 29.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 89.97395833333333
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7485988849765258
Weighted AreaUnderROC: 0.7991044776119404
Root mean squared error: 0.41451527917154846
Relative absolute error: 62.39583333333333
Root relative squared error: 82.9030558343097
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.43702704998854974
Weighted FMeasure: 0.7452173164954065
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7908250145400082
Mean absolute error: 0.31197916666666664
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 313.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.3528202736318408
Kappa statistic: 0.42755645775183976
Training time: 30.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.9375
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7079685135900031
Weighted AreaUnderROC: 0.7827910447761194
Root mean squared error: 0.4330427711747037
Relative absolute error: 63.177083333333314
Root relative squared error: 86.60855423494074
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.35448888235881687
Weighted FMeasure: 0.7097998276179153
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7797011869143659
Mean absolute error: 0.3158854166666666
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 345.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.37722046019900496
Kappa statistic: 0.3517130289325321
Training time: 31.0
		
Time end:Sat Oct 07 11.45.05 EEST 2017