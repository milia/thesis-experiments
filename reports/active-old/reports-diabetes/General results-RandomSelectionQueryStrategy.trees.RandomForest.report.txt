Sat Oct 07 11.46.24 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.24 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.91145833333334
Incorrectly Classified Instances: 27.265625
Correctly Classified Instances: 72.734375
Weighted Precision: 0.7193611636746832
Weighted AreaUnderROC: 0.7626014925373135
Root mean squared error: 0.4334174298491768
Relative absolute error: 67.53124999999997
Root relative squared error: 86.68348596983536
Weighted TruePositiveRate: 0.72734375
Weighted MatthewsCorrelation: 0.37126259793351807
Weighted FMeasure: 0.7146115549971843
Iteration time: 6.9
Weighted AreaUnderPRC: 0.7509354667484852
Mean absolute error: 0.33765624999999994
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.1
Accumulative iteration time: 6.9
Weighted Recall: 0.72734375
Weighted FalsePositiveRate: 0.38645419776119405
Kappa statistic: 0.36071186245311665
Training time: 5.9
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 88.69791666666667
Incorrectly Classified Instances: 25.755208333333336
Correctly Classified Instances: 74.24479166666666
Weighted Precision: 0.7374386299657332
Weighted AreaUnderROC: 0.7800716417910447
Root mean squared error: 0.4256844999539358
Relative absolute error: 64.30729166666666
Root relative squared error: 85.13689999078716
Weighted TruePositiveRate: 0.7424479166666667
Weighted MatthewsCorrelation: 0.4067646844645119
Weighted FMeasure: 0.7291047848855227
Iteration time: 7.5
Weighted AreaUnderPRC: 0.7678723464426822
Mean absolute error: 0.3215364583333333
Coverage of cases: 97.73437500000001
Instances selection time: 0.9
Test time: 1.9
Accumulative iteration time: 14.4
Weighted Recall: 0.7424479166666667
Weighted FalsePositiveRate: 0.37524194651741294
Kappa statistic: 0.39234345797630016
Training time: 6.6
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 89.19270833333334
Incorrectly Classified Instances: 26.848958333333336
Correctly Classified Instances: 73.15104166666666
Weighted Precision: 0.7261954654951077
Weighted AreaUnderROC: 0.7707119402985075
Root mean squared error: 0.4322745802058199
Relative absolute error: 65.04166666666666
Root relative squared error: 86.45491604116398
Weighted TruePositiveRate: 0.7315104166666668
Weighted MatthewsCorrelation: 0.3825843619292745
Weighted FMeasure: 0.7181057945759649
Iteration time: 8.9
Weighted AreaUnderPRC: 0.7594494730274947
Mean absolute error: 0.3252083333333332
Coverage of cases: 97.78645833333334
Instances selection time: 0.8
Test time: 1.9
Accumulative iteration time: 23.3
Weighted Recall: 0.7315104166666668
Weighted FalsePositiveRate: 0.38387459577114424
Kappa statistic: 0.36907478748071654
Training time: 8.1
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 88.77604166666666
Incorrectly Classified Instances: 26.770833333333332
Correctly Classified Instances: 73.22916666666667
Weighted Precision: 0.7243663179185087
Weighted AreaUnderROC: 0.7762731343283582
Root mean squared error: 0.42943103980586406
Relative absolute error: 64.62499999999997
Root relative squared error: 85.8862079611728
Weighted TruePositiveRate: 0.7322916666666666
Weighted MatthewsCorrelation: 0.38370119976718353
Weighted FMeasure: 0.7211361258431754
Iteration time: 10.1
Weighted AreaUnderPRC: 0.764983284367132
Mean absolute error: 0.3231249999999998
Coverage of cases: 98.02083333333334
Instances selection time: 0.5
Test time: 2.0
Accumulative iteration time: 33.4
Weighted Recall: 0.7322916666666666
Weighted FalsePositiveRate: 0.37930062189054725
Kappa statistic: 0.37437422709696067
Training time: 9.6
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 88.12500000000003
Incorrectly Classified Instances: 26.380208333333332
Correctly Classified Instances: 73.61979166666666
Weighted Precision: 0.7305128440838661
Weighted AreaUnderROC: 0.7775567164179104
Root mean squared error: 0.42770422524761054
Relative absolute error: 64.4895833333333
Root relative squared error: 85.5408450495221
Weighted TruePositiveRate: 0.7361979166666668
Weighted MatthewsCorrelation: 0.39218912908837583
Weighted FMeasure: 0.7226725361618267
Iteration time: 12.1
Weighted AreaUnderPRC: 0.764237921781158
Mean absolute error: 0.3224479166666666
Coverage of cases: 97.70833333333334
Instances selection time: 0.5
Test time: 1.9
Accumulative iteration time: 45.5
Weighted Recall: 0.7361979166666668
Weighted FalsePositiveRate: 0.38170836442786066
Kappa statistic: 0.3784178139573392
Training time: 11.6
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 89.16666666666667
Incorrectly Classified Instances: 26.197916666666668
Correctly Classified Instances: 73.80208333333334
Weighted Precision: 0.7310484198188643
Weighted AreaUnderROC: 0.7736701492537313
Root mean squared error: 0.4283697203746759
Relative absolute error: 64.8125
Root relative squared error: 85.67394407493518
Weighted TruePositiveRate: 0.7380208333333333
Weighted MatthewsCorrelation: 0.3948425518051855
Weighted FMeasure: 0.724783302165507
Iteration time: 16.3
Weighted AreaUnderPRC: 0.7615748258812567
Mean absolute error: 0.32406250000000003
Coverage of cases: 97.44791666666666
Instances selection time: 0.9
Test time: 2.2
Accumulative iteration time: 61.8
Weighted Recall: 0.7380208333333333
Weighted FalsePositiveRate: 0.3793462064676617
Kappa statistic: 0.38242931683437964
Training time: 15.4
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 89.12760416666667
Incorrectly Classified Instances: 26.692708333333332
Correctly Classified Instances: 73.30729166666666
Weighted Precision: 0.7265690501323683
Weighted AreaUnderROC: 0.7788402985074627
Root mean squared error: 0.4280066277623692
Relative absolute error: 64.48958333333331
Root relative squared error: 85.60132555247382
Weighted TruePositiveRate: 0.7330729166666666
Weighted MatthewsCorrelation: 0.38751399991269914
Weighted FMeasure: 0.7220814595351299
Iteration time: 16.3
Weighted AreaUnderPRC: 0.7655956749844406
Mean absolute error: 0.3224479166666666
Coverage of cases: 97.83854166666666
Instances selection time: 0.5
Test time: 1.8
Accumulative iteration time: 78.1
Weighted Recall: 0.7330729166666666
Weighted FalsePositiveRate: 0.3764579912935323
Kappa statistic: 0.377397293035811
Training time: 15.8
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 88.59375
Incorrectly Classified Instances: 26.744791666666668
Correctly Classified Instances: 73.25520833333334
Weighted Precision: 0.7236882408668868
Weighted AreaUnderROC: 0.7804074626865672
Root mean squared error: 0.4267993077223327
Relative absolute error: 63.79166666666665
Root relative squared error: 85.35986154446653
Weighted TruePositiveRate: 0.7325520833333333
Weighted MatthewsCorrelation: 0.38088164550469417
Weighted FMeasure: 0.7195811498010685
Iteration time: 17.6
Weighted AreaUnderPRC: 0.7673524950873694
Mean absolute error: 0.3189583333333332
Coverage of cases: 97.890625
Instances selection time: 0.6
Test time: 1.7
Accumulative iteration time: 95.7
Weighted Recall: 0.7325520833333333
Weighted FalsePositiveRate: 0.3843550684079602
Kappa statistic: 0.3703862173997195
Training time: 17.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 89.0234375
Incorrectly Classified Instances: 27.005208333333336
Correctly Classified Instances: 72.99479166666667
Weighted Precision: 0.7212204557637535
Weighted AreaUnderROC: 0.7840283582089552
Root mean squared error: 0.42612742445238433
Relative absolute error: 64.59895833333329
Root relative squared error: 85.22548489047685
Weighted TruePositiveRate: 0.7299479166666667
Weighted MatthewsCorrelation: 0.3766990777976219
Weighted FMeasure: 0.7178653893061944
Iteration time: 18.7
Weighted AreaUnderPRC: 0.770110077800154
Mean absolute error: 0.32299479166666645
Coverage of cases: 98.09895833333333
Instances selection time: 0.5
Test time: 1.9
Accumulative iteration time: 114.4
Weighted Recall: 0.7299479166666667
Weighted FalsePositiveRate: 0.3822882151741293
Kappa statistic: 0.367365810909436
Training time: 18.2
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 89.23177083333333
Incorrectly Classified Instances: 26.015625
Correctly Classified Instances: 73.984375
Weighted Precision: 0.7328684340914948
Weighted AreaUnderROC: 0.7848701492537313
Root mean squared error: 0.4248408777033482
Relative absolute error: 64.046875
Root relative squared error: 84.96817554066965
Weighted TruePositiveRate: 0.73984375
Weighted MatthewsCorrelation: 0.4059954900822884
Weighted FMeasure: 0.7318693287798737
Iteration time: 20.6
Weighted AreaUnderPRC: 0.7708358261084667
Mean absolute error: 0.32023437499999996
Coverage of cases: 97.86458333333334
Instances selection time: 0.6
Test time: 2.0
Accumulative iteration time: 135.0
Weighted Recall: 0.73984375
Weighted FalsePositiveRate: 0.3572467350746269
Kappa statistic: 0.4000645503324608
Training time: 20.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 88.56770833333334
Incorrectly Classified Instances: 25.833333333333336
Correctly Classified Instances: 74.16666666666667
Weighted Precision: 0.7356660342907319
Weighted AreaUnderROC: 0.794355223880597
Root mean squared error: 0.42084122298729404
Relative absolute error: 62.927083333333314
Root relative squared error: 84.16824459745882
Weighted TruePositiveRate: 0.7416666666666667
Weighted MatthewsCorrelation: 0.41180822187653005
Weighted FMeasure: 0.7340562717251679
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7792185356696063
Mean absolute error: 0.3146354166666666
Coverage of cases: 98.02083333333334
Instances selection time: 0.7
Test time: 2.1
Accumulative iteration time: 159.0
Weighted Recall: 0.7416666666666667
Weighted FalsePositiveRate: 0.35315323383084574
Kappa statistic: 0.4055845976565764
Training time: 23.3
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.50260416666667
Incorrectly Classified Instances: 25.963541666666668
Correctly Classified Instances: 74.03645833333333
Weighted Precision: 0.7338790843760405
Weighted AreaUnderROC: 0.791855223880597
Root mean squared error: 0.4214725512381875
Relative absolute error: 63.01041666666665
Root relative squared error: 84.2945102476375
Weighted TruePositiveRate: 0.7403645833333334
Weighted MatthewsCorrelation: 0.40707161739885656
Weighted FMeasure: 0.7319066862111812
Iteration time: 29.8
Weighted AreaUnderPRC: 0.7778637028451341
Mean absolute error: 0.3150520833333333
Coverage of cases: 97.89062499999999
Instances selection time: 0.9
Test time: 2.2
Accumulative iteration time: 188.8
Weighted Recall: 0.7403645833333334
Weighted FalsePositiveRate: 0.3590451803482587
Kappa statistic: 0.4000489942700055
Training time: 28.9
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.0859375
Incorrectly Classified Instances: 26.223958333333332
Correctly Classified Instances: 73.77604166666666
Weighted Precision: 0.7311370703072401
Weighted AreaUnderROC: 0.7891955223880597
Root mean squared error: 0.42291493628269083
Relative absolute error: 63.140624999999986
Root relative squared error: 84.58298725653819
Weighted TruePositiveRate: 0.7377604166666666
Weighted MatthewsCorrelation: 0.3984814673419108
Weighted FMeasure: 0.7274097571902931
Iteration time: 29.3
Weighted AreaUnderPRC: 0.7750136994812433
Mean absolute error: 0.31570312499999986
Coverage of cases: 97.96875
Instances selection time: 1.2
Test time: 2.1
Accumulative iteration time: 218.1
Weighted Recall: 0.7377604166666666
Weighted FalsePositiveRate: 0.36909773009950253
Kappa statistic: 0.3892265549854166
Training time: 28.1
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 87.8515625
Incorrectly Classified Instances: 25.416666666666664
Correctly Classified Instances: 74.58333333333333
Weighted Precision: 0.739474995937007
Weighted AreaUnderROC: 0.7865746268656717
Root mean squared error: 0.4236503854889765
Relative absolute error: 63.11979166666665
Root relative squared error: 84.73007709779532
Weighted TruePositiveRate: 0.7458333333333333
Weighted MatthewsCorrelation: 0.41866305982191276
Weighted FMeasure: 0.7369462887429175
Iteration time: 28.1
Weighted AreaUnderPRC: 0.7699836527418912
Mean absolute error: 0.3155989583333333
Coverage of cases: 97.60416666666667
Instances selection time: 0.2
Test time: 2.3
Accumulative iteration time: 246.2
Weighted Recall: 0.7458333333333333
Weighted FalsePositiveRate: 0.35472885572139307
Kappa statistic: 0.4109551681982171
Training time: 27.9
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 88.26822916666666
Incorrectly Classified Instances: 25.182291666666664
Correctly Classified Instances: 74.81770833333334
Weighted Precision: 0.7422485908832297
Weighted AreaUnderROC: 0.7899477611940299
Root mean squared error: 0.42128931583045875
Relative absolute error: 62.86979166666665
Root relative squared error: 84.25786316609177
Weighted TruePositiveRate: 0.7481770833333333
Weighted MatthewsCorrelation: 0.4251943554640043
Weighted FMeasure: 0.7400298746886314
Iteration time: 30.4
Weighted AreaUnderPRC: 0.7747638758591684
Mean absolute error: 0.31434895833333326
Coverage of cases: 97.76041666666667
Instances selection time: 0.5
Test time: 2.2
Accumulative iteration time: 276.6
Weighted Recall: 0.7481770833333333
Weighted FalsePositiveRate: 0.3507024564676617
Kappa statistic: 0.4178585539767304
Training time: 29.9
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 87.79947916666667
Incorrectly Classified Instances: 25.598958333333336
Correctly Classified Instances: 74.40104166666666
Weighted Precision: 0.7377851566401237
Weighted AreaUnderROC: 0.7961283582089551
Root mean squared error: 0.420556294111995
Relative absolute error: 62.32291666666665
Root relative squared error: 84.11125882239901
Weighted TruePositiveRate: 0.7440104166666667
Weighted MatthewsCorrelation: 0.4162447022414738
Weighted FMeasure: 0.7360571955857215
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7821360287209127
Mean absolute error: 0.31161458333333325
Coverage of cases: 98.15104166666666
Instances selection time: 0.4
Test time: 2.1
Accumulative iteration time: 308.6
Weighted Recall: 0.7440104166666667
Weighted FalsePositiveRate: 0.3518969838308458
Kappa statistic: 0.40976906386111656
Training time: 31.6
		
Time end:Sat Oct 07 11.46.34 EEST 2017