Sat Oct 07 11.43.42 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.42 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.375
Correctly Classified Instances: 65.625
Weighted Precision: 0.6624710648148149
Weighted AreaUnderROC: 0.6303880597014926
Root mean squared error: 0.5863019699779287
Relative absolute error: 68.75
Root relative squared error: 117.26039399558574
Weighted TruePositiveRate: 0.65625
Weighted MatthewsCorrelation: 0.2567458041725298
Weighted FMeasure: 0.6589249008956101
Iteration time: 11.0
Weighted AreaUnderPRC: 0.619847728587963
Mean absolute error: 0.34375
Coverage of cases: 65.625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.65625
Weighted FalsePositiveRate: 0.3954738805970149
Kappa statistic: 0.25633802816901413
Training time: 10.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7105590183956986
Weighted AreaUnderROC: 0.6847462686567164
Root mean squared error: 0.5659615711335886
Relative absolute error: 64.0625
Root relative squared error: 113.19231422671771
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.353310275911793
Weighted FMeasure: 0.6867618837377839
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6579301151575593
Mean absolute error: 0.3203125
Coverage of cases: 67.96875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.31019496268656715
Kappa statistic: 0.3438906484414069
Training time: 9.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.7345888318670578
Weighted AreaUnderROC: 0.7057910447761194
Root mean squared error: 0.5636562191028618
Relative absolute error: 63.541666666666664
Root relative squared error: 112.73124382057236
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3925453199934823
Weighted FMeasure: 0.6895961138130504
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6741405995438253
Mean absolute error: 0.3177083333333333
Coverage of cases: 68.22916666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.27070957711442784
Kappa statistic: 0.37052563689132534
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.59375
Correctly Classified Instances: 66.40625
Weighted Precision: 0.7344350582049803
Weighted AreaUnderROC: 0.6987164179104478
Root mean squared error: 0.5796011559684815
Relative absolute error: 67.1875
Root relative squared error: 115.9202311936963
Weighted TruePositiveRate: 0.6640625
Weighted MatthewsCorrelation: 0.38115207830070147
Weighted FMeasure: 0.6707841474600751
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6689234509112115
Mean absolute error: 0.3359375
Coverage of cases: 66.40625
Instances selection time: 9.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.6640625
Weighted FalsePositiveRate: 0.26662966417910444
Kappa statistic: 0.3496139908618245
Training time: 11.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 41.40625
Correctly Classified Instances: 58.59375
Weighted Precision: 0.7610976036640912
Weighted AreaUnderROC: 0.6681492537313433
Root mean squared error: 0.6434768838116876
Relative absolute error: 82.8125
Root relative squared error: 128.6953767623375
Weighted TruePositiveRate: 0.5859375
Weighted MatthewsCorrelation: 0.3575325365085767
Weighted FMeasure: 0.5750417867877079
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6510707515542643
Mean absolute error: 0.4140625
Coverage of cases: 58.59375
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.5859375
Weighted FalsePositiveRate: 0.2496389925373134
Kappa statistic: 0.26956022395559176
Training time: 13.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 47.135416666666664
Correctly Classified Instances: 52.864583333333336
Weighted Precision: 0.7729402642934197
Weighted AreaUnderROC: 0.6328059701492538
Root mean squared error: 0.68655237722017
Relative absolute error: 94.27083333333334
Root relative squared error: 137.310475444034
Weighted TruePositiveRate: 0.5286458333333334
Weighted MatthewsCorrelation: 0.31934500419694983
Weighted FMeasure: 0.4948432164148868
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6291393450394417
Mean absolute error: 0.4713541666666667
Coverage of cases: 52.864583333333336
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 85.0
Weighted Recall: 0.5286458333333334
Weighted FalsePositiveRate: 0.2630338930348259
Kappa statistic: 0.20384879725085917
Training time: 11.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.9375
Correctly Classified Instances: 64.0625
Weighted Precision: 0.7538648731230357
Weighted AreaUnderROC: 0.6980298507462687
Root mean squared error: 0.5994789404140899
Relative absolute error: 71.875
Root relative squared error: 119.89578808281797
Weighted TruePositiveRate: 0.640625
Weighted MatthewsCorrelation: 0.39104902206960324
Weighted FMeasure: 0.6427316272253583
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6703477499163369
Mean absolute error: 0.359375
Coverage of cases: 64.0625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.640625
Weighted FalsePositiveRate: 0.24456529850746267
Kappa statistic: 0.3336686450055326
Training time: 10.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.7136217184448381
Weighted AreaUnderROC: 0.6803283582089552
Root mean squared error: 0.5885186204927306
Relative absolute error: 69.27083333333334
Root relative squared error: 117.70372409854613
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.3445981819609572
Weighted FMeasure: 0.6612549508014111
Iteration time: 20.0
Weighted AreaUnderPRC: 0.65454138783679
Mean absolute error: 0.3463541666666667
Coverage of cases: 65.36458333333333
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 116.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.2929891169154229
Kappa statistic: 0.32117603275027923
Training time: 16.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.7165302215705441
Weighted AreaUnderROC: 0.6896716417910448
Root mean squared error: 0.568257570707744
Relative absolute error: 64.58333333333334
Root relative squared error: 113.65151414154879
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.3617976447981704
Weighted FMeasure: 0.684579613095238
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6615469526413327
Mean absolute error: 0.3229166666666667
Coverage of cases: 67.70833333333333
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 140.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.29774004975124374
Kappa statistic: 0.34801183043049627
Training time: 18.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7174219327420547
Weighted AreaUnderROC: 0.6935522388059702
Root mean squared error: 0.5496210815947049
Relative absolute error: 60.416666666666664
Root relative squared error: 109.92421631894098
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.3730073022025741
Weighted FMeasure: 0.70348124613261
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6651815049311716
Mean absolute error: 0.3020833333333333
Coverage of cases: 69.79166666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 157.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.31081218905472635
Kappa statistic: 0.3679909194097616
Training time: 16.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7114470872694557
Weighted AreaUnderROC: 0.6866268656716418
Root mean squared error: 0.5496210815947049
Relative absolute error: 60.416666666666664
Root relative squared error: 109.92421631894098
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.36224036169686347
Weighted FMeasure: 0.7024232914923291
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6602363123102226
Mean absolute error: 0.3020833333333333
Coverage of cases: 69.79166666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 183.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.32466293532338314
Kappa statistic: 0.3595583160800551
Training time: 20.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7086950231481483
Weighted AreaUnderROC: 0.6819701492537313
Root mean squared error: 0.5448623679425842
Relative absolute error: 59.375
Root relative squared error: 108.97247358851685
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.35831557285616794
Weighted FMeasure: 0.7054351416825723
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6574779369212962
Mean absolute error: 0.296875
Coverage of cases: 70.3125
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 201.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.3391847014925373
Kappa statistic: 0.3577464788732395
Training time: 18.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7156591366364117
Weighted AreaUnderROC: 0.6867761194029852
Root mean squared error: 0.5327796605226842
Relative absolute error: 56.770833333333336
Root relative squared error: 106.55593210453685
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.37420627505623555
Weighted FMeasure: 0.7158975588149543
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6623061541460775
Mean absolute error: 0.2838541666666667
Coverage of cases: 71.61458333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 212.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.3425935945273632
Kappa statistic: 0.3742001076490641
Training time: 10.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7227205598455598
Weighted AreaUnderROC: 0.6913134328358209
Root mean squared error: 0.5229125165837972
Relative absolute error: 54.6875
Root relative squared error: 104.58250331675944
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.38921805242030655
Weighted FMeasure: 0.7242290792757394
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6669714232927123
Mean absolute error: 0.2734375
Coverage of cases: 72.65625
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 236.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3439356343283582
Kappa statistic: 0.38868336466735404
Training time: 18.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7249203639351328
Weighted AreaUnderROC: 0.6883880597014925
Root mean squared error: 0.5179084539440022
Relative absolute error: 53.645833333333336
Root relative squared error: 103.58169078880044
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.392085437260387
Weighted FMeasure: 0.7264469728625483
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6661511784446947
Mean absolute error: 0.2682291666666667
Coverage of cases: 73.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 254.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3549947139303482
Kappa statistic: 0.38959195012037784
Training time: 17.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7318421229200215
Weighted AreaUnderROC: 0.6909253731343283
Root mean squared error: 0.5103103630798288
Relative absolute error: 52.083333333333336
Root relative squared error: 102.06207261596576
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4048085050164467
Weighted FMeasure: 0.7320301616231447
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6696074931645866
Mean absolute error: 0.2604166666666667
Coverage of cases: 73.95833333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 267.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.35773258706467664
Kappa statistic: 0.39984996249062277
Training time: 13.0
		
Time end:Sat Oct 07 11.43.43 EEST 2017