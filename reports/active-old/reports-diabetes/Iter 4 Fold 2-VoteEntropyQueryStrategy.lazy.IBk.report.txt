Sat Oct 07 11.46.38 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.38 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.458333333333336
Correctly Classified Instances: 63.541666666666664
Weighted Precision: 0.628088109539899
Weighted AreaUnderROC: 0.5884179104477613
Root mean squared error: 0.5961538461538475
Relative absolute error: 73.61111111111106
Root relative squared error: 119.2307692307695
Weighted TruePositiveRate: 0.6354166666666666
Weighted MatthewsCorrelation: 0.18103480782748482
Weighted FMeasure: 0.6311442057291666
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5932475833815646
Mean absolute error: 0.3680555555555553
Coverage of cases: 63.541666666666664
Instances selection time: 14.0
Test time: 11.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6354166666666666
Weighted FalsePositiveRate: 0.4585808457711443
Kappa statistic: 0.18058773320326776
Training time: 0.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.676199398869155
Weighted AreaUnderROC: 0.6443880597014925
Root mean squared error: 0.5690391019658327
Relative absolute error: 65.28880070546731
Root relative squared error: 113.80782039316655
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.287325995001191
Weighted FMeasure: 0.6752984852534641
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6299888347575867
Mean absolute error: 0.32644400352733655
Coverage of cases: 67.44791666666667
Instances selection time: 4.0
Test time: 45.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.3857030472636816
Kappa statistic: 0.2872839579497535
Training time: 1.0
		
Time end:Sat Oct 07 11.46.38 EEST 2017