Sat Oct 07 11.46.31 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.31 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7195457175925926
Weighted AreaUnderROC: 0.7663731343283583
Root mean squared error: 0.43298263052151786
Relative absolute error: 65.67708333333323
Root relative squared error: 86.59652610430358
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.372219375747142
Weighted FMeasure: 0.7163582242336082
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7536338781503931
Mean absolute error: 0.32838541666666615
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.39101741293532344
Kappa statistic: 0.36196319018404893
Training time: 8.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 85.546875
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7196889671361503
Weighted AreaUnderROC: 0.7585373134328358
Root mean squared error: 0.44235402488655345
Relative absolute error: 65.20833333333329
Root relative squared error: 88.47080497731069
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.374779873729053
Weighted FMeasure: 0.7181127756970452
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7399319788019113
Mean absolute error: 0.3260416666666664
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.38409203980099504
Kappa statistic: 0.3666582085765034
Training time: 6.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 86.58854166666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7311587061483927
Weighted AreaUnderROC: 0.7841641791044776
Root mean squared error: 0.4277789051211696
Relative absolute error: 61.614583333333236
Root relative squared error: 85.55578102423392
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3848103570411836
Weighted FMeasure: 0.7171643922508656
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7687726729247468
Mean absolute error: 0.3080729166666662
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.40760603233830844
Kappa statistic: 0.3626503648195622
Training time: 11.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 84.11458333333333
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7077126842751843
Weighted AreaUnderROC: 0.753641791044776
Root mean squared error: 0.45098272324632027
Relative absolute error: 64.27083333333329
Root relative squared error: 90.19654464926406
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3417745709487041
Weighted FMeasure: 0.7014590139590141
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7400155715307236
Mean absolute error: 0.32135416666666644
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 38.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.41737686567164173
Kappa statistic: 0.32745199792423446
Training time: 9.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7284376327930553
Weighted AreaUnderROC: 0.7561492537313432
Root mean squared error: 0.44002248995553267
Relative absolute error: 65.78124999999993
Root relative squared error: 88.00449799110653
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.39060572179030356
Weighted FMeasure: 0.7240977921537809
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7384655386444647
Mean absolute error: 0.3289062499999997
Coverage of cases: 95.83333333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3833672263681591
Kappa statistic: 0.3792176195659132
Training time: 11.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.734765950792327
Weighted AreaUnderROC: 0.7662089552238806
Root mean squared error: 0.43892624665198604
Relative absolute error: 63.958333333333265
Root relative squared error: 87.7852493303972
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.41156856488864196
Weighted FMeasure: 0.7350639329805997
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7549287570501435
Mean absolute error: 0.3197916666666663
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3528740671641791
Kappa statistic: 0.40692667706708274
Training time: 12.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 86.71875
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7303750134691018
Weighted AreaUnderROC: 0.7880597014925373
Root mean squared error: 0.42817441928883776
Relative absolute error: 63.020833333333336
Root relative squared error: 85.63488385776755
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.4040138802890662
Weighted FMeasure: 0.7317586821273534
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7750138575328857
Mean absolute error: 0.3151041666666667
Coverage of cases: 96.875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3487403606965174
Kappa statistic: 0.4014445336131858
Training time: 14.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 86.45833333333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7429356357927787
Weighted AreaUnderROC: 0.7803432835820896
Root mean squared error: 0.42856956844834454
Relative absolute error: 61.5104166666666
Root relative squared error: 85.71391368966891
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4236002761284839
Weighted FMeasure: 0.7389989706639218
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7632062291406053
Mean absolute error: 0.307552083333333
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 93.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3625373134328358
Kappa statistic: 0.413217878517764
Training time: 15.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 87.36979166666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7285875780586039
Weighted AreaUnderROC: 0.7941641791044775
Root mean squared error: 0.42288074560093175
Relative absolute error: 62.65624999999994
Root relative squared error: 84.57614912018634
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.38957191466744073
Weighted FMeasure: 0.7231927774203969
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7800932590317374
Mean absolute error: 0.31328124999999973
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 110.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.38682991293532343
Kappa statistic: 0.3769037979564294
Training time: 16.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 87.109375
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7318421229200215
Weighted AreaUnderROC: 0.7944626865671642
Root mean squared error: 0.421214761137356
Relative absolute error: 62.65624999999997
Root relative squared error: 84.2429522274712
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4048085050164467
Weighted FMeasure: 0.7320301616231447
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7799148792828072
Mean absolute error: 0.31328124999999984
Coverage of cases: 97.39583333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 129.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.35773258706467664
Kappa statistic: 0.39984996249062277
Training time: 19.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 86.328125
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7295374014124015
Weighted AreaUnderROC: 0.7934626865671642
Root mean squared error: 0.42358834182887223
Relative absolute error: 61.874999999999915
Root relative squared error: 84.71766836577444
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.40092366835130283
Weighted FMeasure: 0.7304167317022411
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7746150728678932
Mean absolute error: 0.30937499999999957
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 151.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.35566573383084576
Kappa statistic: 0.39712740160417825
Training time: 21.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 86.328125
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7289109858069885
Weighted AreaUnderROC: 0.7912238805970149
Root mean squared error: 0.4214928825021843
Relative absolute error: 61.145833333333265
Root relative squared error: 84.29857650043687
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3980270790778066
Weighted FMeasure: 0.7289833598410587
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7749969134678053
Mean absolute error: 0.30572916666666633
Coverage of cases: 96.875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 175.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.36259110696517416
Kappa statistic: 0.39274754180497273
Training time: 24.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 84.89583333333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7200663919413919
Weighted AreaUnderROC: 0.7858208955223881
Root mean squared error: 0.4283264428758359
Relative absolute error: 61.51041666666662
Root relative squared error: 85.66528857516718
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.37754557253483895
Weighted FMeasure: 0.719762433275197
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7798964352871421
Mean absolute error: 0.3075520833333331
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 200.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.37716666666666665
Kappa statistic: 0.37128463476070517
Training time: 24.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 87.890625
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7513275014684034
Weighted AreaUnderROC: 0.7790447761194031
Root mean squared error: 0.42756578441217674
Relative absolute error: 63.54166666666657
Root relative squared error: 85.51315688243535
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44466213763717183
Weighted FMeasure: 0.7490427364180317
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7560089970840117
Mean absolute error: 0.31770833333333287
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 229.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.34449906716417916
Kappa statistic: 0.4367547788783042
Training time: 28.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 86.328125
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7370389201780002
Weighted AreaUnderROC: 0.7706716417910447
Root mean squared error: 0.43162725045267186
Relative absolute error: 63.12499999999995
Root relative squared error: 86.32545009053437
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4132490917090201
Weighted FMeasure: 0.7351648453761522
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7520694407446212
Mean absolute error: 0.31562499999999977
Coverage of cases: 95.83333333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 258.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3618662935323383
Kappa statistic: 0.40538490709139163
Training time: 29.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.28645833333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7299304920593453
Weighted AreaUnderROC: 0.7869850746268656
Root mean squared error: 0.4293284484711755
Relative absolute error: 62.18749999999995
Root relative squared error: 85.8656896942351
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.40244510248415977
Weighted FMeasure: 0.7310989228386188
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7757621204750116
Mean absolute error: 0.31093749999999976
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 291.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3522030472636816
Kappa statistic: 0.3992937240567498
Training time: 32.0
		
Time end:Sat Oct 07 11.46.32 EEST 2017