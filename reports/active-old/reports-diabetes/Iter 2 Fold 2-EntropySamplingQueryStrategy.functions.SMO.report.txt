Sat Oct 07 11.43.36 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.36 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7239583333333334
Weighted AreaUnderROC: 0.6962388059701491
Root mean squared error: 0.5253966755382705
Relative absolute error: 55.208333333333336
Root relative squared error: 105.0793351076541
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.39247761194029857
Weighted FMeasure: 0.7239583333333334
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6697882462686567
Mean absolute error: 0.2760416666666667
Coverage of cases: 72.39583333333333
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.3314807213930348
Kappa statistic: 0.3924776119402987
Training time: 15.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.716625619634326
Weighted AreaUnderROC: 0.6670746268656718
Root mean squared error: 0.5229125165837972
Relative absolute error: 54.6875
Root relative squared error: 104.58250331675944
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.36655241363917584
Weighted FMeasure: 0.7140839258506858
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6520662605126115
Mean absolute error: 0.2734375
Coverage of cases: 72.65625
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3924132462686567
Kappa statistic: 0.3570198379792051
Training time: 16.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7057989730702406
Weighted AreaUnderROC: 0.6276417910447761
Root mean squared error: 0.535218024111047
Relative absolute error: 57.291666666666664
Root relative squared error: 107.0436048222094
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3187478697773744
Weighted FMeasure: 0.6834456524274334
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6267063181581968
Mean absolute error: 0.2864583333333333
Coverage of cases: 71.35416666666667
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.4582580845771144
Kappa statistic: 0.2882178484766782
Training time: 16.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7163816915655151
Weighted AreaUnderROC: 0.5663582089552239
Root mean squared error: 0.5566828839953077
Relative absolute error: 61.979166666666664
Root relative squared error: 111.33657679906155
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.24741787053277706
Weighted FMeasure: 0.6144400771195951
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5896858906322946
Mean absolute error: 0.3098958333333333
Coverage of cases: 69.01041666666667
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 80.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.557387748756219
Kappa statistic: 0.1628929435040668
Training time: 14.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7234708681522748
Weighted AreaUnderROC: 0.5646268656716418
Root mean squared error: 0.5566828839953077
Relative absolute error: 61.979166666666664
Root relative squared error: 111.33657679906155
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.2497170893306646
Weighted FMeasure: 0.6116148883105618
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5891588943656182
Mean absolute error: 0.3098958333333333
Coverage of cases: 69.01041666666667
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 100.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.5608504353233831
Kappa statistic: 0.15932003826624455
Training time: 17.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6941277637587119
Weighted AreaUnderROC: 0.5422388059701494
Root mean squared error: 0.570544330734548
Relative absolute error: 65.10416666666666
Root relative squared error: 114.1088661469096
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.1856695989381931
Weighted FMeasure: 0.5825777804346671
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5726511786498388
Mean absolute error: 0.3255208333333333
Coverage of cases: 67.44791666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 125.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.5900015547263682
Kappa statistic: 0.10547894148341405
Training time: 24.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 149.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 169.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 14.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 193.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 28.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 221.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 22.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 36.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 257.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 30.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 289.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 26.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6939964157706093
Weighted AreaUnderROC: 0.5275820895522388
Root mean squared error: 0.5773502691896257
Relative absolute error: 66.66666666666666
Root relative squared error: 115.47005383792515
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.1511184430267245
Weighted FMeasure: 0.5600875067905856
Iteration time: 32.0
Weighted AreaUnderPRC: 0.5633479292674731
Mean absolute error: 0.3333333333333333
Coverage of cases: 66.66666666666667
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 321.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.6115024875621891
Kappa statistic: 0.06993642143505893
Training time: 27.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7250011736144643
Weighted AreaUnderROC: 0.6267164179104477
Root mean squared error: 0.5278691441383303
Relative absolute error: 55.729166666666664
Root relative squared error: 105.57382882766606
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.339761357007559
Weighted FMeasure: 0.6837798226670416
Iteration time: 32.0
Weighted AreaUnderPRC: 0.6294295628558566
Mean absolute error: 0.2786458333333333
Coverage of cases: 72.13541666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 353.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.4679213308457711
Kappa statistic: 0.29241578838603005
Training time: 26.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7432946713374345
Weighted AreaUnderROC: 0.668955223880597
Root mean squared error: 0.5051814855409226
Relative absolute error: 51.041666666666664
Root relative squared error: 101.03629710818451
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.40424240491949326
Weighted FMeasure: 0.7228121266427717
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6595208020346793
Mean absolute error: 0.2552083333333333
Coverage of cases: 74.47916666666667
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 379.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.40688121890547263
Kappa statistic: 0.3756304751791876
Training time: 22.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7399626749955696
Weighted AreaUnderROC: 0.6917313432835822
Root mean squared error: 0.5025974200756175
Relative absolute error: 50.520833333333336
Root relative squared error: 100.5194840151235
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.41783564552971814
Weighted FMeasure: 0.7366861704972391
Iteration time: 40.0
Weighted AreaUnderPRC: 0.6724083929688884
Mean absolute error: 0.2526041666666667
Coverage of cases: 74.73958333333333
Instances selection time: 8.0
Test time: 1.0
Accumulative iteration time: 419.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3639331467661691
Kappa statistic: 0.4081982840800764
Training time: 32.0
		
Time end:Sat Oct 07 11.43.37 EEST 2017