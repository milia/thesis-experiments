Sat Oct 07 11.46.35 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.35 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.760416666666668
Correctly Classified Instances: 72.23958333333333
Weighted Precision: 0.7379487258347857
Weighted AreaUnderROC: 0.6463880597014925
Root mean squared error: 0.5255845776519031
Relative absolute error: 55.52083333333333
Root relative squared error: 105.11691553038062
Weighted TruePositiveRate: 0.7223958333333333
Weighted MatthewsCorrelation: 0.3588373346350616
Weighted FMeasure: 0.6905890690298909
Iteration time: 16.8
Weighted AreaUnderPRC: 0.6432450302619955
Mean absolute error: 0.27760416666666665
Coverage of cases: 72.23958333333333
Instances selection time: 2.8
Test time: 5.4
Accumulative iteration time: 16.8
Weighted Recall: 0.7223958333333333
Weighted FalsePositiveRate: 0.4296197139303482
Kappa statistic: 0.3181794725145709
Training time: 14.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.994791666666664
Correctly Classified Instances: 77.00520833333334
Weighted Precision: 0.7675064154123776
Weighted AreaUnderROC: 0.713113432835821
Root mean squared error: 0.4790327950162025
Relative absolute error: 45.98958333333333
Root relative squared error: 95.8065590032405
Weighted TruePositiveRate: 0.7700520833333333
Weighted MatthewsCorrelation: 0.4726167056210073
Weighted FMeasure: 0.7587264088624605
Iteration time: 22.6
Weighted AreaUnderPRC: 0.6937604221927021
Mean absolute error: 0.2299479166666667
Coverage of cases: 77.00520833333334
Instances selection time: 2.8
Test time: 4.6
Accumulative iteration time: 39.4
Weighted Recall: 0.7700520833333333
Weighted FalsePositiveRate: 0.3438252176616915
Kappa statistic: 0.45750708018580044
Training time: 19.8
		
Time end:Sat Oct 07 11.46.37 EEST 2017