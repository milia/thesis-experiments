Sat Oct 07 11.43.43 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.43 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7853133378016087
Weighted AreaUnderROC: 0.5410447761194029
Root mean squared error: 0.5659615711335886
Relative absolute error: 64.0625
Root relative squared error: 113.19231422671771
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.23456301313414055
Weighted FMeasure: 0.5754507527536393
Iteration time: 21.0
Weighted AreaUnderPRC: 0.576776553947442
Mean absolute error: 0.3203125
Coverage of cases: 67.96875
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.597597947761194
Kappa statistic: 0.10430099370401277
Training time: 14.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7667086332675414
Weighted AreaUnderROC: 0.7042686567164179
Root mean squared error: 0.481425833678806
Relative absolute error: 46.35416666666667
Root relative squared error: 96.2851667357612
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.4651749088190358
Weighted FMeasure: 0.753541099677109
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6876016425857595
Mean absolute error: 0.23177083333333334
Coverage of cases: 76.82291666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3596918532338309
Kappa statistic: 0.44472606745954363
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.767044461914745
Weighted AreaUnderROC: 0.7494029850746268
Root mean squared error: 0.4973890160963884
Relative absolute error: 49.47916666666667
Root relative squared error: 99.47780321927769
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4826854693974207
Weighted FMeasure: 0.7566481797497157
Iteration time: 26.0
Weighted AreaUnderPRC: 0.711880196731962
Mean absolute error: 0.24739583333333334
Coverage of cases: 75.26041666666667
Instances selection time: 11.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.2537981965174129
Kappa statistic: 0.4781115879828325
Training time: 15.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.802083333333336
Correctly Classified Instances: 61.197916666666664
Weighted Precision: 0.7529158622908624
Weighted AreaUnderROC: 0.6812238805970149
Root mean squared error: 0.6229131828219189
Relative absolute error: 77.60416666666666
Root relative squared error: 124.58263656438378
Weighted TruePositiveRate: 0.6119791666666666
Weighted MatthewsCorrelation: 0.36869133971659873
Weighted FMeasure: 0.609017175572519
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6588525776055073
Mean absolute error: 0.3880208333333333
Coverage of cases: 61.197916666666664
Instances selection time: 9.0
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.6119791666666666
Weighted FalsePositiveRate: 0.24953140547263683
Kappa statistic: 0.29796319018404904
Training time: 7.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 61.71875
Correctly Classified Instances: 38.28125
Weighted Precision: 0.7770805256064689
Weighted AreaUnderROC: 0.526
Root mean squared error: 0.7856128181235335
Relative absolute error: 123.4375
Root relative squared error: 157.1225636247067
Weighted TruePositiveRate: 0.3828125
Weighted MatthewsCorrelation: 0.13704623730343585
Weighted FMeasure: 0.24955130256371647
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5617078042523023
Mean absolute error: 0.6171875
Coverage of cases: 38.28125
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 91.0
Weighted Recall: 0.3828125
Weighted FalsePositiveRate: 0.3308125
Kappa statistic: 0.03687084620920291
Training time: 13.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 65.10416666666667
Correctly Classified Instances: 34.895833333333336
Weighted Precision: 0.12177191840277778
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.8068715304598785
Relative absolute error: 130.20833333333331
Root relative squared error: 161.37430609197568
Weighted TruePositiveRate: 0.3489583333333333
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.1805421492921493
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.6510416666666666
Coverage of cases: 34.895833333333336
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 107.0
Weighted Recall: 0.3489583333333333
Weighted FalsePositiveRate: 0.3489583333333333
Kappa statistic: 0.0
Training time: 15.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 63.28125
Correctly Classified Instances: 36.71875
Weighted Precision: 0.775074602122016
Weighted AreaUnderROC: 0.514
Root mean squared error: 0.795495128834866
Relative absolute error: 126.5625
Root relative squared error: 159.0990257669732
Weighted TruePositiveRate: 0.3671875
Weighted MatthewsCorrelation: 0.0997609875748522
Weighted FMeasure: 0.21848063421840136
Iteration time: 16.0
Weighted AreaUnderPRC: 0.554249406809516
Mean absolute error: 0.6328125
Coverage of cases: 36.71875
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 123.0
Weighted Recall: 0.3671875
Weighted FalsePositiveRate: 0.3391875
Kappa statistic: 0.019708366600832028
Training time: 10.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.7727706520079402
Weighted AreaUnderROC: 0.7214925373134329
Root mean squared error: 0.5773502691896257
Relative absolute error: 66.66666666666666
Root relative squared error: 115.47005383792515
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.43383505202629674
Weighted FMeasure: 0.6698990447734166
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6890267642841463
Mean absolute error: 0.3333333333333333
Coverage of cases: 66.66666666666667
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.22368159203980098
Kappa statistic: 0.37649685406941336
Training time: 11.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7728410844033045
Weighted AreaUnderROC: 0.747910447761194
Root mean squared error: 0.5278691441383303
Relative absolute error: 55.729166666666664
Root relative squared error: 105.57382882766606
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.4728172498528131
Weighted FMeasure: 0.7277863666950116
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7090598624939348
Mean absolute error: 0.2786458333333333
Coverage of cases: 72.13541666666667
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 156.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.2255332711442786
Kappa statistic: 0.447058190235237
Training time: 16.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7902210621072427
Weighted AreaUnderROC: 0.7752537313432836
Root mean squared error: 0.481425833678806
Relative absolute error: 46.35416666666667
Root relative squared error: 96.2851667357612
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.5285949769913114
Weighted FMeasure: 0.7728927126583626
Iteration time: 22.0
Weighted AreaUnderPRC: 0.734173745150858
Mean absolute error: 0.23177083333333334
Coverage of cases: 76.82291666666667
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 178.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.21772170398009952
Kappa statistic: 0.5190543202927103
Training time: 20.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.354166666666668
Correctly Classified Instances: 78.64583333333333
Weighted Precision: 0.7850967847769028
Weighted AreaUnderROC: 0.7615522388059701
Root mean squared error: 0.462105687767059
Relative absolute error: 42.70833333333333
Root relative squared error: 92.4211375534118
Weighted TruePositiveRate: 0.7864583333333334
Weighted MatthewsCorrelation: 0.5268928516262492
Weighted FMeasure: 0.785687981000481
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7275400023239281
Mean absolute error: 0.21354166666666666
Coverage of cases: 78.64583333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 192.0
Weighted Recall: 0.7864583333333334
Weighted FalsePositiveRate: 0.263353855721393
Kappa statistic: 0.5267524347721535
Training time: 13.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.7991347036249113
Weighted AreaUnderROC: 0.7718208955223881
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.556550627980107
Weighted FMeasure: 0.7997639973958334
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7395058739584638
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 202.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.2584415422885572
Kappa statistic: 0.5551761980246313
Training time: 9.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.7930836397058822
Weighted AreaUnderROC: 0.7574328358208954
Root mean squared error: 0.45069390943299864
Relative absolute error: 40.625
Root relative squared error: 90.13878188659973
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5399118713245621
Weighted FMeasure: 0.7920725866741426
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7288235932393791
Mean absolute error: 0.203125
Coverage of cases: 79.6875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 212.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.28200932835820897
Kappa statistic: 0.535253227408143
Training time: 10.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8006367370892019
Weighted AreaUnderROC: 0.7527761194029851
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5490719672556438
Weighted FMeasure: 0.7940054899324562
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7289739369375979
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 223.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.2965310945273632
Kappa statistic: 0.537173306267445
Training time: 10.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8013073236287521
Weighted AreaUnderROC: 0.7510447761194029
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5489180773868335
Weighted FMeasure: 0.793374185108938
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7281488740054588
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 247.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.29999378109452735
Kappa statistic: 0.5354641538265632
Training time: 18.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.8026437424617533
Weighted AreaUnderROC: 0.7403880597014926
Root mean squared error: 0.4477955262542641
Relative absolute error: 40.10416666666667
Root relative squared error: 89.55910525085282
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5430743360651912
Weighted FMeasure: 0.7875256270484498
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7218570021260832
Mean absolute error: 0.20052083333333334
Coverage of cases: 79.94791666666667
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 267.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.3187030472636816
Kappa statistic: 0.5213985108449336
Training time: 14.0
		
Time end:Sat Oct 07 11.43.44 EEST 2017