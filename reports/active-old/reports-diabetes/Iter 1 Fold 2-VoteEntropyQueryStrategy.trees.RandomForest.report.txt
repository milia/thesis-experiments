Sat Oct 07 11.46.39 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.39 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 86.45833333333333
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.696781458878233
Weighted AreaUnderROC: 0.7588955223880597
Root mean squared error: 0.4425600241775119
Relative absolute error: 65.5729166666667
Root relative squared error: 88.51200483550238
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.307196697456082
Weighted FMeasure: 0.6818910256410255
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7415105461287036
Mean absolute error: 0.3278645833333335
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.4541243781094528
Kappa statistic: 0.2836775483011327
Training time: 13.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 86.328125
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7291970739505143
Weighted AreaUnderROC: 0.7775820895522388
Root mean squared error: 0.42744395344107206
Relative absolute error: 63.5416666666667
Root relative squared error: 85.48879068821441
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.39945061297043627
Weighted FMeasure: 0.7297116892024299
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7587191315165605
Mean absolute error: 0.3177083333333335
Coverage of cases: 96.61458333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.35912842039801
Kappa statistic: 0.3949453978159126
Training time: 30.0
		
Time end:Sat Oct 07 11.46.39 EEST 2017