Sat Oct 07 11.44.18 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.18 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7977240073145246
Weighted AreaUnderROC: 0.7329253731343285
Root mean squared error: 0.45357377202244253
Relative absolute error: 41.14583333333333
Root relative squared error: 90.7147544044885
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5304339929146479
Weighted FMeasure: 0.7812331109493439
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7153833509529562
Mean absolute error: 0.20572916666666666
Coverage of cases: 79.42708333333333
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.3284200870646767
Kappa statistic: 0.5071163969584714
Training time: 18.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.270833333333332
Correctly Classified Instances: 80.72916666666667
Weighted Precision: 0.813983566151605
Weighted AreaUnderROC: 0.7463880597014926
Root mean squared error: 0.4389855730355308
Relative absolute error: 38.54166666666667
Root relative squared error: 87.79711460710617
Weighted TruePositiveRate: 0.8072916666666666
Weighted MatthewsCorrelation: 0.5633972191356217
Weighted FMeasure: 0.7947087204158372
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7296919100528069
Mean absolute error: 0.19270833333333334
Coverage of cases: 80.72916666666667
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8072916666666666
Weighted FalsePositiveRate: 0.3145155472636816
Kappa statistic: 0.5374397708034899
Training time: 24.0
		
Time end:Sat Oct 07 11.44.18 EEST 2017