Sat Oct 07 11.44.24 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.24 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7247019262111397
Weighted AreaUnderROC: 0.765955223880597
Root mean squared error: 0.43069106290549686
Relative absolute error: 65.46875000000004
Root relative squared error: 86.13821258109937
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3714183707167488
Weighted FMeasure: 0.7115636871469224
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7585398220980468
Mean absolute error: 0.3273437500000002
Coverage of cases: 98.17708333333333
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.4138603855721393
Kappa statistic: 0.3500295799645042
Training time: 5.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.15625
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.734867255185088
Weighted AreaUnderROC: 0.7829253731343283
Root mean squared error: 0.4253674881793389
Relative absolute error: 61.77083333333332
Root relative squared error: 85.07349763586778
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4014504487775707
Weighted FMeasure: 0.7277601289134438
Iteration time: 62.0
Weighted AreaUnderPRC: 0.7699640252511287
Mean absolute error: 0.3088541666666666
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.38403824626865674
Kappa statistic: 0.3869573630910147
Training time: 61.0
		
Time end:Sat Oct 07 11.44.24 EEST 2017