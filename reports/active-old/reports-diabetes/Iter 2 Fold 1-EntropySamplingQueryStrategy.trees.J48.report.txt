Sat Oct 07 11.43.57 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.57 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 70.44270833333333
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7259089528917273
Weighted AreaUnderROC: 0.7040000000000001
Root mean squared error: 0.48113063861894384
Relative absolute error: 63.17616959064299
Root relative squared error: 96.22612772378876
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.38619227929282257
Weighted FMeasure: 0.7020417918481359
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6760020101706741
Mean absolute error: 0.31588084795321497
Coverage of cases: 88.02083333333333
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.29143190298507465
Kappa statistic: 0.3758959826637773
Training time: 6.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 76.43229166666667
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.6925980926194922
Weighted AreaUnderROC: 0.725955223880597
Root mean squared error: 0.46183131025907326
Relative absolute error: 64.4610164141415
Root relative squared error: 92.36626205181466
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.3031928801544803
Weighted FMeasure: 0.6823174406801167
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7020401502385906
Mean absolute error: 0.3223050820707075
Coverage of cases: 92.44791666666667
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.44859483830845776
Kappa statistic: 0.2841966347740018
Training time: 3.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 100.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4799022641281639
Relative absolute error: 87.49999999999994
Root relative squared error: 95.98045282563278
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.4374999999999997
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 100.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.48680506023116343
Relative absolute error: 84.89583333333334
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.4244791666666667
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 100.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.49431764509653114
Relative absolute error: 82.95940170940148
Root relative squared error: 98.86352901930623
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.41479700854700735
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 2.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 98.046875
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7583999515503876
Weighted AreaUnderROC: 0.5756417910447761
Root mean squared error: 0.4755061889909937
Relative absolute error: 68.52964743589766
Root relative squared error: 95.10123779819874
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.3584540061952793
Weighted FMeasure: 0.6712370351019777
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6188293265979837
Mean absolute error: 0.3426482371794883
Coverage of cases: 96.875
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.4942269900497513
Kappa statistic: 0.2743867655447805
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 98.046875
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7583999515503876
Weighted AreaUnderROC: 0.5756417910447761
Root mean squared error: 0.47550154809276973
Relative absolute error: 68.59092194570154
Root relative squared error: 95.10030961855395
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.3584540061952793
Weighted FMeasure: 0.6712370351019777
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6188293265979837
Mean absolute error: 0.3429546097285077
Coverage of cases: 96.875
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.4942269900497513
Kappa statistic: 0.2743867655447805
Training time: 4.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.45304743284043236
Relative absolute error: 64.04513888888907
Root relative squared error: 90.60948656808647
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.3202256944444453
Coverage of cases: 99.47916666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 10.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.45591456153568805
Relative absolute error: 63.27732652865612
Root relative squared error: 91.18291230713761
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.3163866326432806
Coverage of cases: 99.47916666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 8.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.4592676631934605
Relative absolute error: 62.3528044455974
Root relative squared error: 91.8535326386921
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.311764022227987
Coverage of cases: 99.47916666666667
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 6.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.46219338520954995
Relative absolute error: 61.592641843971585
Root relative squared error: 92.43867704190998
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.3079632092198579
Coverage of cases: 99.47916666666667
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 3.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.46475620654005295
Relative absolute error: 60.956587422203036
Root relative squared error: 92.9512413080106
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.3047829371110152
Coverage of cases: 99.47916666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 97.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 7.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.46091468066127333
Relative absolute error: 61.92007895088986
Root relative squared error: 92.18293613225467
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.3096003947544493
Coverage of cases: 99.47916666666667
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 105.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 4.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 99.47916666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7761465509627273
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.44492963919070944
Relative absolute error: 66.94242254572589
Root relative squared error: 88.98592783814189
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4218467471958687
Weighted FMeasure: 0.7087118314712654
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6527324962618627
Mean absolute error: 0.33471211272862944
Coverage of cases: 99.47916666666667
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 116.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.4470376243781095
Kappa statistic: 0.35076343861116926
Training time: 8.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 81.51041666666667
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7124904434250765
Weighted AreaUnderROC: 0.68
Root mean squared error: 0.49403981969440525
Relative absolute error: 64.11497716330649
Root relative squared error: 98.80796393888104
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.36309675721581924
Weighted FMeasure: 0.713654957867921
Iteration time: 22.0
Weighted AreaUnderPRC: 0.6738538499483574
Mean absolute error: 0.32057488581653243
Coverage of cases: 89.32291666666667
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 138.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3778914800995025
Kappa statistic: 0.35900156006240247
Training time: 17.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 82.94270833333333
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.74609375
Weighted AreaUnderROC: 0.752268656716418
Root mean squared error: 0.43739261514390393
Relative absolute error: 61.978989231491944
Root relative squared error: 87.47852302878078
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.4336585079213105
Weighted FMeasure: 0.7294269130924974
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7260656646368487
Mean absolute error: 0.3098949461574597
Coverage of cases: 93.75
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 157.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.2726150497512438
Kappa statistic: 0.4262516914749662
Training time: 15.0
		
Time end:Sat Oct 07 11.43.58 EEST 2017