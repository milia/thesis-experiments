Sat Oct 07 11.46.20 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.20 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 93.88020833333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7477796052631579
Weighted AreaUnderROC: 0.7191194029850746
Root mean squared error: 0.4463412789039801
Relative absolute error: 62.25656702898515
Root relative squared error: 89.26825578079603
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.41817168358472295
Weighted FMeasure: 0.7306420594486993
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7100926100694762
Mean absolute error: 0.3112828351449258
Coverage of cases: 98.17708333333333
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3937014925373134
Kappa statistic: 0.3930453108535301
Training time: 7.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 89.453125
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.726099537037037
Weighted AreaUnderROC: 0.6065970149253731
Root mean squared error: 0.47881166121981716
Relative absolute error: 69.58973340395505
Root relative squared error: 95.76233224396343
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.3470300430024303
Weighted FMeasure: 0.6891048379371866
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6201606393891179
Mean absolute error: 0.3479486670197753
Coverage of cases: 92.1875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.45960012437810943
Kappa statistic: 0.3032046014790469
Training time: 2.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 100.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.4768112981783798
Relative absolute error: 90.10416666666683
Root relative squared error: 95.36225963567595
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.45052083333333415
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 67.96875
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.755432514240018
Weighted AreaUnderROC: 0.7158955223880596
Root mean squared error: 0.4489491508536393
Relative absolute error: 57.283857937927685
Root relative squared error: 89.78983017072785
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4580216459192675
Weighted FMeasure: 0.7461603611489335
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6956506476304624
Mean absolute error: 0.2864192896896384
Coverage of cases: 88.54166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.2697695895522388
Kappa statistic: 0.45432623299075614
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 86.84895833333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7572783119658121
Weighted AreaUnderROC: 0.7565074626865672
Root mean squared error: 0.4338447503816066
Relative absolute error: 64.84592989963365
Root relative squared error: 86.76895007632132
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4623605384422538
Weighted FMeasure: 0.7485989876400714
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7246305586062839
Mean absolute error: 0.32422964949816824
Coverage of cases: 95.83333333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.26837375621890547
Kappa statistic: 0.4589371980676328
Training time: 2.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6525593660339357
Weighted AreaUnderROC: 0.6899701492537313
Root mean squared error: 0.4719982203826904
Relative absolute error: 71.58871852621846
Root relative squared error: 94.39964407653808
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.19808328080559612
Weighted FMeasure: 0.6266698766034345
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6662931986856601
Mean absolute error: 0.35794359263109227
Coverage of cases: 93.75
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.5311358830845772
Kappa statistic: 0.1667245330185404
Training time: 2.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 76.171875
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6572096414479328
Weighted AreaUnderROC: 0.6567761194029851
Root mean squared error: 0.5078416573807202
Relative absolute error: 71.15017019975198
Root relative squared error: 101.56833147614404
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.24417850046547426
Weighted FMeasure: 0.6604581724581725
Iteration time: 4.0
Weighted AreaUnderPRC: 0.648770567643556
Mean absolute error: 0.3557508509987599
Coverage of cases: 85.41666666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.4314427860696517
Kappa statistic: 0.2427902390929258
Training time: 4.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7489283410294717
Weighted AreaUnderROC: 0.7223432835820894
Root mean squared error: 0.4427154301941517
Relative absolute error: 70.2702743545497
Root relative squared error: 88.54308603883034
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.4216022686146408
Weighted FMeasure: 0.7022852593973283
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6961938734430211
Mean absolute error: 0.3513513717727485
Coverage of cases: 97.13541666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.25334235074626865
Kappa statistic: 0.39726321438154016
Training time: 3.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 69.27083333333333
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7111665426019239
Weighted AreaUnderROC: 0.7129850746268657
Root mean squared error: 0.4768119102010207
Relative absolute error: 65.6205616226376
Root relative squared error: 95.36238204020414
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.3632053023163552
Weighted FMeasure: 0.7062182602223975
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6951017915737469
Mean absolute error: 0.328102808113188
Coverage of cases: 86.19791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.332259328358209
Kappa statistic: 0.3620890650501282
Training time: 5.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 95.703125
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7249203639351328
Weighted AreaUnderROC: 0.7322686567164179
Root mean squared error: 0.4426673034431197
Relative absolute error: 68.15442122361742
Root relative squared error: 88.53346068862395
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.392085437260387
Weighted FMeasure: 0.7264469728625483
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7125623691573453
Mean absolute error: 0.34077210611808706
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3549947139303482
Kappa statistic: 0.38959195012037784
Training time: 5.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 96.09375
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7136114065824052
Weighted AreaUnderROC: 0.7334328358208954
Root mean squared error: 0.4455023538106169
Relative absolute error: 68.292824074074
Root relative squared error: 89.10047076212338
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3687927179037959
Weighted FMeasure: 0.7154541015625
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7148005840328553
Mean absolute error: 0.34146412037037005
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.3585111940298507
Kappa statistic: 0.3678819656139495
Training time: 5.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 99.73958333333333
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7576872448124333
Weighted AreaUnderROC: 0.7452238805970149
Root mean squared error: 0.4257255262125843
Relative absolute error: 67.79947916666667
Root relative squared error: 85.14510524251686
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45457214634081117
Weighted FMeasure: 0.7522060690705942
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7250567256712523
Mean absolute error: 0.3389973958333334
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3486327736318408
Kappa statistic: 0.4427505262486445
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 82.94270833333333
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7551869504274028
Weighted AreaUnderROC: 0.7313283582089553
Root mean squared error: 0.4417108741782888
Relative absolute error: 61.89773441427878
Root relative squared error: 88.34217483565776
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.4608261797362611
Weighted FMeasure: 0.756236680722755
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7093361952186975
Mean absolute error: 0.3094886720713939
Coverage of cases: 94.27083333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.30294682835820896
Kappa statistic: 0.4604460022964888
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 84.375
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7539711047254151
Weighted AreaUnderROC: 0.7689402985074626
Root mean squared error: 0.428802018687118
Relative absolute error: 61.307194879984614
Root relative squared error: 85.7604037374236
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45758283975804454
Weighted FMeasure: 0.7552362138021884
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7388792711117969
Mean absolute error: 0.3065359743999231
Coverage of cases: 96.09375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3098722014925373
Kappa statistic: 0.4566368449881321
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 91.14583333333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.741365731924078
Weighted AreaUnderROC: 0.7220298507462687
Root mean squared error: 0.4523095824201168
Relative absolute error: 61.980909386170346
Root relative squared error: 90.46191648402336
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4135943006442093
Weighted FMeasure: 0.7323374782298653
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7135268779416273
Mean absolute error: 0.30990454693085173
Coverage of cases: 93.75
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 72.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3812465796019901
Kappa statistic: 0.39708643573972174
Training time: 6.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 79.94791666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7303750134691018
Weighted AreaUnderROC: 0.7203134328358209
Root mean squared error: 0.4610559263864002
Relative absolute error: 58.82798768527575
Root relative squared error: 92.21118527728004
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.4040138802890662
Weighted FMeasure: 0.7317586821273534
Iteration time: 8.0
Weighted AreaUnderPRC: 0.70896355972696
Mean absolute error: 0.29413993842637876
Coverage of cases: 90.625
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3487403606965174
Kappa statistic: 0.4014445336131858
Training time: 8.0
		
Time end:Sat Oct 07 11.46.21 EEST 2017