Sat Oct 07 11.44.39 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.39 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7590940238657815
Weighted AreaUnderROC: 0.702
Root mean squared error: 0.48680506023116343
Relative absolute error: 47.39583333333333
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45284467192495764
Weighted FMeasure: 0.7497593104153878
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6839992129573741
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.35902083333333334
Kappa statistic: 0.436496162033155
Training time: 6.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7621767114410275
Weighted AreaUnderROC: 0.7398208955223881
Root mean squared error: 0.48947250518628044
Relative absolute error: 47.91666666666667
Root relative squared error: 97.89450103725609
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.47646644696421064
Weighted FMeasure: 0.7612122469955726
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7061711915098092
Mean absolute error: 0.23958333333333334
Coverage of cases: 76.04166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.2807748756218906
Kappa statistic: 0.4763429384560654
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7581185328898744
Weighted AreaUnderROC: 0.7394029850746269
Root mean squared error: 0.5103103630798288
Relative absolute error: 52.083333333333336
Root relative squared error: 102.06207261596576
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4613693034646274
Weighted FMeasure: 0.7443803845970774
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7027646534899299
Mean absolute error: 0.2604166666666667
Coverage of cases: 73.95833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.26077736318407957
Kappa statistic: 0.45516458569807045
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.7633916762852883
Weighted AreaUnderROC: 0.7097611940298507
Root mean squared error: 0.5885186204927306
Relative absolute error: 69.27083333333334
Root relative squared error: 117.70372409854613
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.4124721154792537
Weighted FMeasure: 0.6563609998755671
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6795889196954054
Mean absolute error: 0.3463541666666667
Coverage of cases: 65.36458333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.23412344527363183
Kappa statistic: 0.35498863349330645
Training time: 6.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.239583333333336
Correctly Classified Instances: 62.760416666666664
Weighted Precision: 0.7638475642194863
Weighted AreaUnderROC: 0.694955223880597
Root mean squared error: 0.6102424381615337
Relative absolute error: 74.47916666666666
Root relative squared error: 122.04848763230673
Weighted TruePositiveRate: 0.6276041666666666
Weighted MatthewsCorrelation: 0.3934796990404659
Weighted FMeasure: 0.6260759976983583
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6693839529361889
Mean absolute error: 0.3723958333333333
Coverage of cases: 62.760416666666664
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6276041666666666
Weighted FalsePositiveRate: 0.23769371890547264
Kappa statistic: 0.3223752406337923
Training time: 7.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 49.479166666666664
Correctly Classified Instances: 50.520833333333336
Weighted Precision: 0.7563039060560933
Weighted AreaUnderROC: 0.6130746268656716
Root mean squared error: 0.7034142923389222
Relative absolute error: 98.95833333333334
Root relative squared error: 140.68285846778443
Weighted TruePositiveRate: 0.5052083333333334
Weighted MatthewsCorrelation: 0.2823706573238294
Weighted FMeasure: 0.46367487770789656
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6151195054927298
Mean absolute error: 0.4947916666666667
Coverage of cases: 50.520833333333336
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.5052083333333334
Weighted FalsePositiveRate: 0.27905907960199006
Kappa statistic: 0.17196295623751598
Training time: 7.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 39.322916666666664
Correctly Classified Instances: 60.677083333333336
Weighted Precision: 0.7692044138246862
Weighted AreaUnderROC: 0.6841492537313433
Root mean squared error: 0.6270798726371838
Relative absolute error: 78.64583333333334
Root relative squared error: 125.41597452743676
Weighted TruePositiveRate: 0.6067708333333334
Weighted MatthewsCorrelation: 0.38326335960375973
Weighted FMeasure: 0.5999138170343429
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6626711825513678
Mean absolute error: 0.3932291666666667
Coverage of cases: 60.677083333333336
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.6067708333333334
Weighted FalsePositiveRate: 0.23847232587064673
Kappa statistic: 0.2985240745221389
Training time: 8.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.7843066212986819
Weighted AreaUnderROC: 0.7414925373134328
Root mean squared error: 0.5543389456520863
Relative absolute error: 61.458333333333336
Root relative squared error: 110.86778913041726
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.46781414666756277
Weighted FMeasure: 0.6973277505446623
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7050745818416818
Mean absolute error: 0.3072916666666667
Coverage of cases: 69.27083333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.20972325870646769
Kappa statistic: 0.4166237511587188
Training time: 8.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.775500027138515
Weighted AreaUnderROC: 0.7544477611940299
Root mean squared error: 0.5153882032022076
Relative absolute error: 53.125
Root relative squared error: 103.07764064044152
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.48522601020892625
Weighted FMeasure: 0.740567466506977
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7147310615569366
Mean absolute error: 0.265625
Coverage of cases: 73.4375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 74.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.2254794776119403
Kappa statistic: 0.46538545533959386
Training time: 9.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7597116333918384
Weighted AreaUnderROC: 0.7405970149253731
Root mean squared error: 0.5153882032022076
Relative absolute error: 53.125
Root relative squared error: 103.07764064044152
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.4612224941942659
Weighted FMeasure: 0.7399440836940837
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7032764169416715
Mean absolute error: 0.265625
Coverage of cases: 73.4375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 84.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.2531809701492537
Kappa statistic: 0.451490029128389
Training time: 9.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7568680118798045
Weighted AreaUnderROC: 0.7379402985074627
Root mean squared error: 0.5077524002897476
Relative absolute error: 51.5625
Root relative squared error: 101.55048005794951
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4605010025813094
Weighted FMeasure: 0.7464017873181246
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7019181946355258
Mean absolute error: 0.2578125
Coverage of cases: 74.21875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.26630690298507464
Kappa statistic: 0.45613733905579396
Training time: 9.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7411917391448521
Weighted AreaUnderROC: 0.7195522388059702
Root mean squared error: 0.5179084539440022
Relative absolute error: 53.645833333333336
Root relative squared error: 103.58169078880044
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.4284733697423182
Weighted FMeasure: 0.7350502615233737
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6870947662853618
Mean absolute error: 0.2682291666666667
Coverage of cases: 73.17708333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 115.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.29266635572139305
Kappa statistic: 0.426549904308995
Training time: 20.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7403374369081884
Weighted AreaUnderROC: 0.7087761194029851
Root mean squared error: 0.5051814855409226
Relative absolute error: 51.041666666666664
Root relative squared error: 101.03629710818451
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4274670648026431
Weighted FMeasure: 0.7418009440104166
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6820027063323844
Mean absolute error: 0.2552083333333333
Coverage of cases: 74.47916666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 126.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32723942786069654
Kappa statistic: 0.42641141324228743
Training time: 11.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7412843125370401
Weighted AreaUnderROC: 0.7055820895522388
Root mean squared error: 0.5025974200756175
Relative absolute error: 50.520833333333336
Root relative squared error: 100.5194840151235
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4278707663464245
Weighted FMeasure: 0.7423821006569632
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6806298736927956
Mean absolute error: 0.2526041666666667
Coverage of cases: 74.73958333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 137.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3362316542288557
Kappa statistic: 0.4251497005988024
Training time: 10.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.754201892109501
Weighted AreaUnderROC: 0.7138507462686569
Root mean squared error: 0.48947250518628044
Relative absolute error: 47.91666666666667
Root relative squared error: 97.89450103725609
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4534159052435623
Weighted FMeasure: 0.7534677486932931
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6895014813556765
Mean absolute error: 0.23958333333333334
Coverage of cases: 76.04166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 149.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.33271517412935325
Kappa statistic: 0.4478619654913728
Training time: 11.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7569652457757297
Weighted AreaUnderROC: 0.7141194029850745
Root mean squared error: 0.48680506023116343
Relative absolute error: 47.39583333333333
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4579432603644094
Weighted FMeasure: 0.7551390844281333
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6905609333997376
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 159.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3347820273631841
Kappa statistic: 0.45087686215349804
Training time: 10.0
		
Time end:Sat Oct 07 11.44.40 EEST 2017