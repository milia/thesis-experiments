Sat Oct 07 11.44.20 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.20 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7210379340853793
Weighted AreaUnderROC: 0.6811940298507463
Root mean squared error: 0.513861123687978
Relative absolute error: 55.34188034188018
Root relative squared error: 102.7722247375956
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.38205391928202553
Weighted FMeasure: 0.722049284611855
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6611181241014156
Mean absolute error: 0.2767094017094009
Coverage of cases: 72.91666666666667
Instances selection time: 23.0
Test time: 11.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.36677860696517417
Kappa statistic: 0.3780989161579668
Training time: 3.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.71875
Weighted AreaUnderROC: 0.6905074626865672
Root mean squared error: 0.5289318514195858
Relative absolute error: 56.4814814814817
Root relative squared error: 105.78637028391715
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3810149253731343
Weighted FMeasure: 0.71875
Iteration time: 5.0
Weighted AreaUnderPRC: 0.665169776119403
Mean absolute error: 0.2824074074074085
Coverage of cases: 71.875
Instances selection time: 5.0
Test time: 47.0
Accumulative iteration time: 31.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.3377350746268657
Kappa statistic: 0.3810149253731344
Training time: 0.0
		
Time end:Sat Oct 07 11.44.20 EEST 2017