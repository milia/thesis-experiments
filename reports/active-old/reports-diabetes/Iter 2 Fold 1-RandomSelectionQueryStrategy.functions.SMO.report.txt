Sat Oct 07 11.46.03 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.03 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7439311267436267
Weighted AreaUnderROC: 0.7174328358208956
Root mean squared error: 0.5051814855409226
Relative absolute error: 51.041666666666664
Root relative squared error: 101.03629710818451
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4364055783078247
Weighted FMeasure: 0.7443406199762355
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6876903568798101
Mean absolute error: 0.2552083333333333
Coverage of cases: 74.47916666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3099259950248756
Kappa statistic: 0.43637670740474466
Training time: 10.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7456514550264551
Weighted AreaUnderROC: 0.7026567164179105
Root mean squared error: 0.4973890160963884
Relative absolute error: 49.47916666666667
Root relative squared error: 99.47780321927769
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.43342768640930923
Weighted FMeasure: 0.7443759672601392
Iteration time: 6.0
Weighted AreaUnderPRC: 0.680388015666336
Mean absolute error: 0.24739583333333334
Coverage of cases: 75.26041666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3472907338308458
Kappa statistic: 0.4267395813690363
Training time: 6.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7513083504010923
Weighted AreaUnderROC: 0.7083880597014925
Root mean squared error: 0.4921254921257382
Relative absolute error: 48.4375
Root relative squared error: 98.42509842514764
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.4456854733868593
Weighted FMeasure: 0.7497575258441364
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6854403390150304
Mean absolute error: 0.2421875
Coverage of cases: 75.78125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3410363805970149
Kappa statistic: 0.43880822176126716
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.6912003719927687
Weighted AreaUnderROC: 0.6456119402985074
Root mean squared error: 0.5448623679425842
Relative absolute error: 59.375
Root relative squared error: 108.97247358851685
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.3142857919171499
Weighted FMeasure: 0.6919264527845036
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6342297446460854
Mean absolute error: 0.296875
Coverage of cases: 70.3125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.41190111940298507
Kappa statistic: 0.3083048919226394
Training time: 6.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7340275534377861
Weighted AreaUnderROC: 0.6684179104477611
Root mean squared error: 0.5103103630798288
Relative absolute error: 52.083333333333336
Root relative squared error: 102.06207261596576
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.3917691878528006
Weighted FMeasure: 0.7205028851315084
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6570209671661598
Mean absolute error: 0.2604166666666667
Coverage of cases: 73.95833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.402747512437811
Kappa statistic: 0.3701613961422387
Training time: 6.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7752530231335436
Weighted AreaUnderROC: 0.6812238805970149
Root mean squared error: 0.48680506023116343
Relative absolute error: 47.39583333333333
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4551984688552994
Weighted FMeasure: 0.7375257201646092
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6750446854832289
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.4005730721393035
Kappa statistic: 0.41000877963125554
Training time: 7.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7654344149037545
Weighted AreaUnderROC: 0.6717611940298508
Root mean squared error: 0.4947642536265799
Relative absolute error: 48.95833333333333
Root relative squared error: 98.95285072531598
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.43400219094311837
Weighted FMeasure: 0.7282467723004696
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6664631463514866
Mean absolute error: 0.24479166666666666
Coverage of cases: 75.52083333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.41168594527363184
Kappa statistic: 0.3893625659764516
Training time: 8.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.614583333333332
Correctly Classified Instances: 78.38541666666667
Weighted Precision: 0.7892697255836695
Weighted AreaUnderROC: 0.7162686567164179
Root mean squared error: 0.46491486675877913
Relative absolute error: 43.22916666666667
Root relative squared error: 92.98297335175583
Weighted TruePositiveRate: 0.7838541666666666
Weighted MatthewsCorrelation: 0.5053382339610977
Weighted FMeasure: 0.7675707381863548
Iteration time: 8.0
Weighted AreaUnderPRC: 0.701909623742322
Mean absolute error: 0.21614583333333334
Coverage of cases: 78.38541666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7838541666666666
Weighted FalsePositiveRate: 0.3513168532338309
Kappa statistic: 0.4762374285150857
Training time: 7.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7702870558243587
Weighted AreaUnderROC: 0.7062686567164179
Root mean squared error: 0.47871355387816905
Relative absolute error: 45.83333333333333
Root relative squared error: 95.74271077563381
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.471659169399945
Weighted FMeasure: 0.7558698296836983
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6898904721723635
Mean absolute error: 0.22916666666666666
Coverage of cases: 77.08333333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.3582960199004976
Kappa statistic: 0.44992837609063685
Training time: 7.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.135416666666668
Correctly Classified Instances: 77.86458333333333
Weighted Precision: 0.7748178052454368
Weighted AreaUnderROC: 0.7261194029850747
Root mean squared error: 0.4704829079431756
Relative absolute error: 44.27083333333333
Root relative squared error: 94.09658158863512
Weighted TruePositiveRate: 0.7786458333333334
Weighted MatthewsCorrelation: 0.49277674215905565
Weighted FMeasure: 0.7692610772398486
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7035371143936624
Mean absolute error: 0.22135416666666666
Coverage of cases: 77.86458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7786458333333334
Weighted FalsePositiveRate: 0.3264070273631841
Kappa statistic: 0.48141086749285045
Training time: 7.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.135416666666668
Correctly Classified Instances: 77.86458333333333
Weighted Precision: 0.7748178052454368
Weighted AreaUnderROC: 0.7261194029850747
Root mean squared error: 0.4704829079431756
Relative absolute error: 44.27083333333333
Root relative squared error: 94.09658158863512
Weighted TruePositiveRate: 0.7786458333333334
Weighted MatthewsCorrelation: 0.49277674215905565
Weighted FMeasure: 0.7692610772398486
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7035371143936624
Mean absolute error: 0.22135416666666666
Coverage of cases: 77.86458333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7786458333333334
Weighted FalsePositiveRate: 0.3264070273631841
Kappa statistic: 0.48141086749285045
Training time: 9.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7516515485265485
Weighted AreaUnderROC: 0.7135820895522388
Root mean squared error: 0.4921254921257382
Relative absolute error: 48.4375
Root relative squared error: 98.42509842514764
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44913243260585123
Weighted FMeasure: 0.7517698618644397
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6885010990235599
Mean absolute error: 0.2421875
Coverage of cases: 75.78125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 97.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3306483208955224
Kappa statistic: 0.44487968662562954
Training time: 8.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7714178313169239
Weighted AreaUnderROC: 0.7258507462686566
Root mean squared error: 0.4732423621500228
Relative absolute error: 44.79166666666667
Root relative squared error: 94.64847243000456
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.48747156655292256
Weighted FMeasure: 0.7675936398198887
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7022335862536503
Mean absolute error: 0.22395833333333334
Coverage of cases: 77.60416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.32434017412935323
Kappa statistic: 0.4781949184679559
Training time: 9.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.135416666666668
Correctly Classified Instances: 77.86458333333333
Weighted Precision: 0.7741467784380794
Weighted AreaUnderROC: 0.7295820895522388
Root mean squared error: 0.4704829079431756
Relative absolute error: 44.27083333333333
Root relative squared error: 94.09658158863512
Weighted TruePositiveRate: 0.7786458333333334
Weighted MatthewsCorrelation: 0.4939905388657358
Weighted FMeasure: 0.7706304580164804
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7053825741806153
Mean absolute error: 0.22135416666666666
Coverage of cases: 77.86458333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 117.0
Weighted Recall: 0.7786458333333334
Weighted FalsePositiveRate: 0.3194816542288557
Kappa statistic: 0.48520598069522436
Training time: 10.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7714178313169239
Weighted AreaUnderROC: 0.7258507462686566
Root mean squared error: 0.4732423621500228
Relative absolute error: 44.79166666666667
Root relative squared error: 94.64847243000456
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.48747156655292256
Weighted FMeasure: 0.7675936398198887
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7022335862536503
Mean absolute error: 0.22395833333333334
Coverage of cases: 77.60416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 127.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.32434017412935323
Kappa statistic: 0.4781949184679559
Training time: 9.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.65625
Correctly Classified Instances: 77.34375
Weighted Precision: 0.7681792118432028
Weighted AreaUnderROC: 0.729044776119403
Root mean squared error: 0.4759858191164943
Relative absolute error: 45.3125
Root relative squared error: 95.19716382329887
Weighted TruePositiveRate: 0.7734375
Weighted MatthewsCorrelation: 0.48427627639787607
Weighted FMeasure: 0.7671773956496181
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7029794491105146
Mean absolute error: 0.2265625
Coverage of cases: 77.34375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 150.0
Weighted Recall: 0.7734375
Weighted FalsePositiveRate: 0.315347947761194
Kappa statistic: 0.478814352574103
Training time: 22.0
		
Time end:Sat Oct 07 11.46.04 EEST 2017