Sat Oct 07 11.45.48 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.48 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.6891480576643515
Weighted AreaUnderROC: 0.7520746268656716
Root mean squared error: 0.44350051672273566
Relative absolute error: 66.61458333333326
Root relative squared error: 88.70010334454713
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.2984445177935727
Weighted FMeasure: 0.6813732890499195
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7368074656075398
Mean absolute error: 0.3330729166666663
Coverage of cases: 97.91666666666667
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.4465279850746269
Kappa statistic: 0.281983991602152
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 85.41666666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7356626650675356
Weighted AreaUnderROC: 0.7524179104477612
Root mean squared error: 0.4540328549051639
Relative absolute error: 63.958333333333236
Root relative squared error: 90.80657098103278
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.41817444989249347
Weighted FMeasure: 0.7362747258586843
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7343419893047544
Mean absolute error: 0.3197916666666662
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.32103886815920396
Kappa statistic: 0.4181119846366199
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7417468284755714
Weighted AreaUnderROC: 0.7822686567164179
Root mean squared error: 0.43223016630802924
Relative absolute error: 61.97916666666657
Root relative squared error: 86.44603326160585
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4316200554708091
Weighted FMeasure: 0.7419620029603715
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7608525158452846
Mean absolute error: 0.30989583333333287
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.31132182835820893
Kappa statistic: 0.43161294180970033
Training time: 9.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 83.984375
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.753841229193342
Weighted AreaUnderROC: 0.7927761194029851
Root mean squared error: 0.43514245751630976
Relative absolute error: 59.42708333333334
Root relative squared error: 87.02849150326195
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4577761087417153
Weighted FMeasure: 0.7515906680805938
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7733696989129658
Mean absolute error: 0.2971354166666667
Coverage of cases: 96.875
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.28635820895522385
Kappa statistic: 0.45730773760452237
Training time: 10.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 82.421875
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7529704098620581
Weighted AreaUnderROC: 0.7911194029850747
Root mean squared error: 0.42863032829389636
Relative absolute error: 58.385416666666636
Root relative squared error: 85.72606565877928
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.4545475000034653
Weighted FMeasure: 0.7541600310833111
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7689498217319161
Mean absolute error: 0.29192708333333317
Coverage of cases: 94.27083333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3167975746268657
Kappa statistic: 0.4527735212994177
Training time: 12.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 89.0625
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7471324906367042
Weighted AreaUnderROC: 0.7759850746268658
Root mean squared error: 0.43262161103363006
Relative absolute error: 62.76041666666661
Root relative squared error: 86.52432220672601
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44123984905464003
Weighted FMeasure: 0.7482936564509212
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7608357521222086
Mean absolute error: 0.31380208333333304
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32651461442786067
Kappa statistic: 0.4390108876176416
Training time: 14.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 85.67708333333333
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7321347600088476
Weighted AreaUnderROC: 0.7653880597014925
Root mean squared error: 0.44120455384473733
Relative absolute error: 61.197916666666565
Root relative squared error: 88.24091076894747
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.40622339292359017
Weighted FMeasure: 0.7327396967421683
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7497040910759333
Mean absolute error: 0.3059895833333328
Coverage of cases: 95.83333333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.35426990049751245
Kappa statistic: 0.40201818861342975
Training time: 17.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7134590082073388
Weighted AreaUnderROC: 0.7848805970149254
Root mean squared error: 0.43090263788161437
Relative absolute error: 62.49999999999992
Root relative squared error: 86.18052757632287
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3664779373901526
Weighted FMeasure: 0.7151246014230911
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7721777928399368
Mean absolute error: 0.3124999999999996
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 102.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3709661069651741
Kappa statistic: 0.3636082027135864
Training time: 18.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 89.84375
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7350659381909382
Weighted AreaUnderROC: 0.7773731343283582
Root mean squared error: 0.43050963016716237
Relative absolute error: 63.17708333333327
Root relative squared error: 86.10192603343248
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4129758594149399
Weighted FMeasure: 0.7357550142427908
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7572786095358891
Mean absolute error: 0.31588541666666636
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 126.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.34941138059701493
Kappa statistic: 0.40906547285954115
Training time: 23.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 85.67708333333333
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.715954350490196
Weighted AreaUnderROC: 0.7941492537313432
Root mean squared error: 0.4253674881793392
Relative absolute error: 61.04166666666655
Root relative squared error: 85.07349763586785
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.3716276516909324
Weighted FMeasure: 0.7174319767622964
Iteration time: 23.0
Weighted AreaUnderPRC: 0.781018298495295
Mean absolute error: 0.30520833333333275
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 149.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.36957027363184086
Kappa statistic: 0.36842105263157904
Training time: 22.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7154895211236453
Weighted AreaUnderROC: 0.7955820895522389
Root mean squared error: 0.42689528380310465
Relative absolute error: 61.562499999999964
Root relative squared error: 85.37905676062093
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.36996918246124316
Weighted FMeasure: 0.7167040785466985
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7779616309541156
Mean absolute error: 0.3078124999999998
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 174.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.373032960199005
Kappa statistic: 0.36613927993023554
Training time: 24.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 91.27604166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7514174727985165
Weighted AreaUnderROC: 0.7851492537313433
Root mean squared error: 0.4226343474762399
Relative absolute error: 63.9062499999999
Root relative squared error: 84.52686949524798
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44370490433658655
Weighted FMeasure: 0.7483052073769509
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7722354119692625
Mean absolute error: 0.3195312499999995
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 203.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3479617537313433
Kappa statistic: 0.4346862534034066
Training time: 28.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 86.58854166666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7456226822259845
Weighted AreaUnderROC: 0.8104029850746267
Root mean squared error: 0.4142324729746168
Relative absolute error: 60.364583333333265
Root relative squared error: 82.84649459492336
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4323300373300309
Weighted FMeasure: 0.7436458060184193
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7945782413870441
Mean absolute error: 0.30182291666666633
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 232.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.35075342039801
Kappa statistic: 0.4246419784240741
Training time: 28.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 89.453125
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7289109858069885
Weighted AreaUnderROC: 0.8117014925373134
Root mean squared error: 0.41123543540572116
Relative absolute error: 61.458333333333236
Root relative squared error: 82.24708708114423
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3980270790778066
Weighted FMeasure: 0.7289833598410587
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7928811792197069
Mean absolute error: 0.3072916666666662
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 262.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.36259110696517416
Kappa statistic: 0.39274754180497273
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7513574808529303
Weighted AreaUnderROC: 0.7998059701492536
Root mean squared error: 0.41664583281247386
Relative absolute error: 61.4583333333332
Root relative squared error: 83.32916656249478
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.446772860728541
Weighted FMeasure: 0.7504500244081037
Iteration time: 31.0
Weighted AreaUnderPRC: 0.785490415438744
Mean absolute error: 0.307291666666666
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 293.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.33757369402985077
Kappa statistic: 0.4408467464144799
Training time: 31.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 87.109375
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7214635854341737
Weighted AreaUnderROC: 0.7853432835820896
Root mean squared error: 0.4311443107205138
Relative absolute error: 63.22916666666657
Root relative squared error: 86.22886214410276
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.3836479530933345
Weighted FMeasure: 0.7227634488988568
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7701640357539898
Mean absolute error: 0.31614583333333285
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 326.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.36331592039801003
Kappa statistic: 0.38033763654419056
Training time: 32.0
		
Time end:Sat Oct 07 11.45.49 EEST 2017