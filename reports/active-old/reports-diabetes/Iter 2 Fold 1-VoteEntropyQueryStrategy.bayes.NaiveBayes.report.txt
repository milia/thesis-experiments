Sat Oct 07 11.46.34 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.34 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7399945596518177
Weighted AreaUnderROC: 0.8005074626865673
Root mean squared error: 0.42758294887413734
Relative absolute error: 62.529946221242916
Root relative squared error: 85.51658977482747
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4211698994317592
Weighted FMeasure: 0.7389944086761422
Iteration time: 9.0
Weighted AreaUnderPRC: 0.789734688289943
Mean absolute error: 0.3126497311062146
Coverage of cases: 96.09375
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.35354508706467663
Kappa statistic: 0.4146709409768056
Training time: 1.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 83.85416666666667
Incorrectly Classified Instances: 22.65625
Correctly Classified Instances: 77.34375
Weighted Precision: 0.7682371588621589
Weighted AreaUnderROC: 0.8275522388059703
Root mean squared error: 0.405329084488131
Relative absolute error: 54.71572132268913
Root relative squared error: 81.06581689762619
Weighted TruePositiveRate: 0.7734375
Weighted MatthewsCorrelation: 0.4852890057967625
Weighted FMeasure: 0.7677847094860889
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8313599976925582
Mean absolute error: 0.27357860661344563
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7734375
Weighted FalsePositiveRate: 0.3118852611940299
Kappa statistic: 0.48069390039171794
Training time: 1.0
		
Time end:Sat Oct 07 11.46.34 EEST 2017