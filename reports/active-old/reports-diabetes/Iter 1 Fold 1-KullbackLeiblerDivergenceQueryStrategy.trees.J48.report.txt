Sat Oct 07 11.44.21 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.21 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 75.26041666666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7428736256277997
Weighted AreaUnderROC: 0.6665373134328358
Root mean squared error: 0.48856023850410474
Relative absolute error: 53.81193693693699
Root relative squared error: 97.71204770082095
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4278886529255667
Weighted FMeasure: 0.7420454545454546
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6700470089972189
Mean absolute error: 0.26905968468468494
Coverage of cases: 83.33333333333333
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.34868656716417906
Kappa statistic: 0.4217593173547496
Training time: 6.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 84.63541666666667
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.6925980926194922
Weighted AreaUnderROC: 0.7467462686567164
Root mean squared error: 0.46027400971865584
Relative absolute error: 63.602995268047856
Root relative squared error: 92.05480194373116
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.3031928801544803
Weighted FMeasure: 0.6823174406801167
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7251189708846918
Mean absolute error: 0.3180149763402393
Coverage of cases: 92.96875
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.44859483830845776
Kappa statistic: 0.2841966347740018
Training time: 17.0
		
Time end:Sat Oct 07 11.44.21 EEST 2017