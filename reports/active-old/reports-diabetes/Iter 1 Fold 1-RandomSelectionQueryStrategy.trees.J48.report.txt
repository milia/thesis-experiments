Sat Oct 07 11.46.17 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.17 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 100.0
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7269774811218985
Weighted AreaUnderROC: 0.6537611940298508
Root mean squared error: 0.45637295336780354
Relative absolute error: 71.46529162512513
Root relative squared error: 91.27459067356071
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.36973389899269243
Weighted FMeasure: 0.7080659421158199
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6465491789290048
Mean absolute error: 0.3573264581256257
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.42424844527363187
Kappa statistic: 0.3425094753640536
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 90.234375
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7269774811218985
Weighted AreaUnderROC: 0.6537611940298508
Root mean squared error: 0.4580686139882523
Relative absolute error: 70.93378890253841
Root relative squared error: 91.61372279765047
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.36973389899269243
Weighted FMeasure: 0.7080659421158199
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6465491789290048
Mean absolute error: 0.35466894451269204
Coverage of cases: 94.27083333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.42424844527363187
Kappa statistic: 0.3425094753640536
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 90.234375
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7214271250238231
Weighted AreaUnderROC: 0.6205820895522388
Root mean squared error: 0.47119987371353583
Relative absolute error: 72.22222222222298
Root relative squared error: 94.23997474270716
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.34711124134866717
Weighted FMeasure: 0.6935548708920188
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6307392712130145
Mean absolute error: 0.3611111111111149
Coverage of cases: 93.48958333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.4492120646766169
Kappa statistic: 0.311408850994722
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7177749619741424
Weighted AreaUnderROC: 0.654955223880597
Root mean squared error: 0.45327945234885936
Relative absolute error: 73.83988550554031
Root relative squared error: 90.65589046977188
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3588693160926007
Weighted FMeasure: 0.7070849920964815
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6452933127990576
Mean absolute error: 0.3691994275277015
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.41665205223880597
Kappa statistic: 0.3399253487001506
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.71354166666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7253568772912775
Weighted AreaUnderROC: 0.6570597014925373
Root mean squared error: 0.46521739865278033
Relative absolute error: 70.35590277777798
Root relative squared error: 93.04347973055607
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.37077244016105126
Weighted FMeasure: 0.7104309415048852
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6490759464585333
Mean absolute error: 0.3517795138888899
Coverage of cases: 93.75
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.41732307213930353
Kappa statistic: 0.3475420653249753
Training time: 3.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 84.375
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7012026736924278
Weighted AreaUnderROC: 0.6699701492537313
Root mean squared error: 0.5200883865851301
Relative absolute error: 65.59113185748359
Root relative squared error: 104.01767731702603
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.3421977767261337
Weighted FMeasure: 0.699389634248451
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6622765544464376
Mean absolute error: 0.32795565928741793
Coverage of cases: 87.76041666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.3523644278606965
Kappa statistic: 0.34199952729851085
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 100.0
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7232871096017647
Weighted AreaUnderROC: 0.6637761194029851
Root mean squared error: 0.4516993663227485
Relative absolute error: 72.65992545316095
Root relative squared error: 90.3398732645497
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.37381219108517866
Weighted FMeasure: 0.71477228389598
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6516871693112253
Mean absolute error: 0.3632996272658047
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.40347232587064674
Kappa statistic: 0.3573796061610451
Training time: 3.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 70.44270833333333
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6971296296296297
Weighted AreaUnderROC: 0.6658358208955224
Root mean squared error: 0.5228925435684717
Relative absolute error: 67.3163288908664
Root relative squared error: 104.57850871369433
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.3321020043503688
Weighted FMeasure: 0.69113752764521
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6590966967496809
Mean absolute error: 0.336581644454332
Coverage of cases: 82.03125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.34755970149253734
Kappa statistic: 0.3307772743116068
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 85.546875
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7637784250028004
Weighted AreaUnderROC: 0.7651044776119402
Root mean squared error: 0.42865051392297276
Relative absolute error: 61.21423801511129
Root relative squared error: 85.73010278459455
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.47590981793251436
Weighted FMeasure: 0.7539714515943364
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7365362179404595
Mean absolute error: 0.30607119007555644
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.25865671641791044
Kappa statistic: 0.4717413733807176
Training time: 3.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.717084034792368
Weighted AreaUnderROC: 0.6584179104477612
Root mean squared error: 0.4504764979830918
Relative absolute error: 74.71590909090953
Root relative squared error: 90.09529959661836
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.36076037426605617
Weighted FMeasure: 0.709233881641533
Iteration time: 3.0
Weighted AreaUnderPRC: 0.647175832763398
Mean absolute error: 0.3735795454545477
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.40972667910447763
Kappa statistic: 0.34490154026125946
Training time: 3.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.717084034792368
Weighted AreaUnderROC: 0.6584179104477612
Root mean squared error: 0.4488666810070415
Relative absolute error: 75.88668585587543
Root relative squared error: 89.7733362014083
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.36076037426605617
Weighted FMeasure: 0.709233881641533
Iteration time: 4.0
Weighted AreaUnderPRC: 0.647175832763398
Mean absolute error: 0.3794334292793771
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.40972667910447763
Kappa statistic: 0.34490154026125946
Training time: 4.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.717084034792368
Weighted AreaUnderROC: 0.6584179104477612
Root mean squared error: 0.44881559742434995
Relative absolute error: 75.6520006409009
Root relative squared error: 89.76311948486999
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.36076037426605617
Weighted FMeasure: 0.709233881641533
Iteration time: 5.0
Weighted AreaUnderPRC: 0.647175832763398
Mean absolute error: 0.3782600032045045
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.40972667910447763
Kappa statistic: 0.34490154026125946
Training time: 4.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 100.0
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.707653154805576
Weighted AreaUnderROC: 0.6558805970149254
Root mean squared error: 0.45310219882171304
Relative absolute error: 75.55840964931924
Root relative squared error: 90.6204397643426
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.34560620714904294
Weighted FMeasure: 0.7044956140350876
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6435910528501263
Mean absolute error: 0.37779204824659623
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.40698880597014925
Kappa statistic: 0.3349583066067992
Training time: 4.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 99.08854166666667
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7108809599829714
Weighted AreaUnderROC: 0.6552537313432836
Root mean squared error: 0.45640520754779573
Relative absolute error: 71.60959480694449
Root relative squared error: 91.28104150955915
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3477085574469337
Weighted FMeasure: 0.7036954793870859
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6552220083190804
Mean absolute error: 0.35804797403472244
Coverage of cases: 99.21875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.4159810323383084
Kappa statistic: 0.33242347436147385
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 91.796875
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7210838050037439
Weighted AreaUnderROC: 0.6611343283582088
Root mean squared error: 0.4586719912956232
Relative absolute error: 71.61308123182017
Root relative squared error: 91.73439825912465
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.35542720803472544
Weighted FMeasure: 0.7011394313983829
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6567841716079013
Mean absolute error: 0.3580654061591009
Coverage of cases: 95.57291666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.43396548507462684
Kappa statistic: 0.3271477204458982
Training time: 6.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 98.56770833333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7229823287627752
Weighted AreaUnderROC: 0.6772537313432836
Root mean squared error: 0.45799945167832884
Relative absolute error: 69.41010060731087
Root relative squared error: 91.59989033566576
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3747502900179147
Weighted FMeasure: 0.7157810335842899
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6830884945409776
Mean absolute error: 0.3470505030365543
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.40000963930348266
Kappa statistic: 0.35979281320815815
Training time: 6.0
		
Time end:Sat Oct 07 11.46.18 EEST 2017