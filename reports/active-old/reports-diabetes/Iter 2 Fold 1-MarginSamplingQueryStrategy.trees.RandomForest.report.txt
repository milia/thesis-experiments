Sat Oct 07 11.45.46 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.46 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 91.015625
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7633952151157207
Weighted AreaUnderROC: 0.8051492537313433
Root mean squared error: 0.4137606997609448
Relative absolute error: 64.37500000000003
Root relative squared error: 82.75213995218897
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.47685063132386696
Weighted FMeasure: 0.764190899201389
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7825101416197894
Mean absolute error: 0.32187500000000013
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3077515547263681
Kappa statistic: 0.4744417789260011
Training time: 6.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 93.359375
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.728344298245614
Weighted AreaUnderROC: 0.7747910447761194
Root mean squared error: 0.42545931062793785
Relative absolute error: 68.38541666666669
Root relative squared error: 85.09186212558757
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3928552799866056
Weighted FMeasure: 0.7258278682497025
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7583531552187804
Mean absolute error: 0.34192708333333344
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3764418532338309
Kappa statistic: 0.3837940896091515
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 93.359375
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7280196653973626
Weighted AreaUnderROC: 0.7971791044776119
Root mean squared error: 0.41839226012598896
Relative absolute error: 66.25000000000007
Root relative squared error: 83.67845202519779
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.3472691725955896
Weighted FMeasure: 0.6875361689814814
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7775375689345719
Mean absolute error: 0.3312500000000003
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.46306281094527363
Kappa statistic: 0.3004262340162245
Training time: 9.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 89.0625
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7445472452265931
Weighted AreaUnderROC: 0.8114776119402984
Root mean squared error: 0.41202624309623775
Relative absolute error: 62.13541666666672
Root relative squared error: 82.40524861924754
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.39735032915113067
Weighted FMeasure: 0.7157420223200663
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7904123332222781
Mean absolute error: 0.3106770833333336
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.4221277985074627
Kappa statistic: 0.3606458123107971
Training time: 12.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 91.66666666666667
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7119924987659544
Weighted AreaUnderROC: 0.7810895522388059
Root mean squared error: 0.4290857528901807
Relative absolute error: 65.00000000000009
Root relative squared error: 85.81715057803613
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3167539867199131
Weighted FMeasure: 0.6757450810185185
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7608446869870683
Mean absolute error: 0.3250000000000004
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.4755715174129353
Kappa statistic: 0.2740272239791007
Training time: 23.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 88.28125
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7197907395045245
Weighted AreaUnderROC: 0.8082686567164178
Root mean squared error: 0.41476650057592646
Relative absolute error: 63.85416666666674
Root relative squared error: 82.95330011518529
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.33955396238339625
Weighted FMeasure: 0.6884652041272917
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7921734107875626
Mean absolute error: 0.3192708333333337
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.4575332711442786
Kappa statistic: 0.30079640596283436
Training time: 15.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.6981358063069921
Weighted AreaUnderROC: 0.7708059701492539
Root mean squared error: 0.44058436573865545
Relative absolute error: 65.72916666666674
Root relative squared error: 88.11687314773108
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.2946505779639512
Weighted FMeasure: 0.6693927037186199
Iteration time: 37.0
Weighted AreaUnderPRC: 0.7535877342279859
Mean absolute error: 0.32864583333333375
Coverage of cases: 98.4375
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 116.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.4797590174129353
Kappa statistic: 0.25866192851773373
Training time: 30.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.63020833333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7339248971193415
Weighted AreaUnderROC: 0.7923432835820895
Root mean squared error: 0.42539809786441996
Relative absolute error: 62.76041666666667
Root relative squared error: 85.07961957288398
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.3620774161407471
Weighted FMeasure: 0.6949707843912019
Iteration time: 21.0
Weighted AreaUnderPRC: 0.774755902541977
Mean absolute error: 0.31380208333333337
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 137.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.4533457711442786
Kappa statistic: 0.31635168447000805
Training time: 20.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 89.32291666666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7380881519274376
Weighted AreaUnderROC: 0.7855522388059701
Root mean squared error: 0.4214310936163429
Relative absolute error: 63.12500000000006
Root relative squared error: 84.28621872326858
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.40748823864745304
Weighted FMeasure: 0.730047487745098
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7808625568854545
Mean absolute error: 0.31562500000000027
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 159.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3826424129353234
Kappa statistic: 0.3920124079100426
Training time: 21.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 90.10416666666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.75
Weighted AreaUnderROC: 0.8082089552238806
Root mean squared error: 0.40961745364831964
Relative absolute error: 62.96875000000003
Root relative squared error: 81.92349072966392
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4179552214821333
Weighted FMeasure: 0.7284690220174092
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7936426624875909
Mean absolute error: 0.31484375000000014
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 182.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.4006268656716418
Kappa statistic: 0.3883727103796124
Training time: 22.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.716585662902932
Weighted AreaUnderROC: 0.799865671641791
Root mean squared error: 0.41895206567498083
Relative absolute error: 60.83333333333333
Root relative squared error: 83.79041313499617
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3652819674145688
Weighted FMeasure: 0.7131709720410594
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7882607630263142
Mean absolute error: 0.30416666666666664
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 208.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.39587593283582095
Kappa statistic: 0.3546321787566425
Training time: 25.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7570318898443898
Weighted AreaUnderROC: 0.8134776119402986
Root mean squared error: 0.40549609533672865
Relative absolute error: 59.89583333333335
Root relative squared error: 81.09921906734573
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4457693690820658
Weighted FMeasure: 0.7456873081873082
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8033433100368909
Mean absolute error: 0.29947916666666674
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 237.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.36734203980099506
Kappa statistic: 0.42708873897249594
Training time: 28.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 89.0625
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7257469093406593
Weighted AreaUnderROC: 0.7942238805970149
Root mean squared error: 0.41558192132638944
Relative absolute error: 62.81249999999999
Root relative squared error: 83.11638426527789
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3898401366336668
Weighted FMeasure: 0.7251516172506739
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7821035531158363
Mean absolute error: 0.31406249999999997
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 267.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3709123134328358
Kappa statistic: 0.3833753148614609
Training time: 29.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7374396246025924
Weighted AreaUnderROC: 0.8123432835820895
Root mean squared error: 0.4087264264354174
Relative absolute error: 59.73958333333332
Root relative squared error: 81.74528528708348
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.409141626310874
Weighted FMeasure: 0.731857131254061
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8005950190079828
Mean absolute error: 0.2986979166666666
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 298.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3757170398009951
Kappa statistic: 0.39653624118024366
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 85.9375
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7514174727985165
Weighted AreaUnderROC: 0.8064626865671641
Root mean squared error: 0.4142010381445223
Relative absolute error: 59.479166666666686
Root relative squared error: 82.84020762890447
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44370490433658655
Weighted FMeasure: 0.7483052073769509
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7970907397544872
Mean absolute error: 0.2973958333333334
Coverage of cases: 98.17708333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 329.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3479617537313433
Kappa statistic: 0.4346862534034066
Training time: 31.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 86.84895833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.754016316073355
Weighted AreaUnderROC: 0.8035522388059703
Root mean squared error: 0.41053826455845355
Relative absolute error: 61.1458333333333
Root relative squared error: 82.1076529116907
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4248602112478639
Weighted FMeasure: 0.7307404320485716
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7939847682726425
Mean absolute error: 0.3057291666666665
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 361.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3992310323383084
Kappa statistic: 0.3935767005784958
Training time: 31.0
		
Time end:Sat Oct 07 11.45.47 EEST 2017