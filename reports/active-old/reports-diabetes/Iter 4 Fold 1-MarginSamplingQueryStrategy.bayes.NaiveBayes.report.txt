Sat Oct 07 11.45.16 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.16 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 73.56770833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7312366704871892
Weighted AreaUnderROC: 0.7902790733830846
Root mean squared error: 0.46219064595299925
Relative absolute error: 58.383888632230864
Root relative squared error: 92.43812919059985
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.40796879991281904
Weighted FMeasure: 0.7284985791172515
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7897521897960657
Mean absolute error: 0.2919194431611543
Coverage of cases: 90.36458333333333
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.31277145522388056
Kappa statistic: 0.4074422432543649
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 81.25
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.72016434998657
Weighted AreaUnderROC: 0.7748111784825871
Root mean squared error: 0.46048672198282015
Relative absolute error: 63.343572623292246
Root relative squared error: 92.09734439656403
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.38325479926967776
Weighted FMeasure: 0.716158030835285
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7822458413214006
Mean absolute error: 0.3167178631164612
Coverage of cases: 94.01041666666667
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.3232133084577114
Kappa statistic: 0.38238390455023974
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 87.76041666666667
Incorrectly Classified Instances: 22.65625
Correctly Classified Instances: 77.34375
Weighted Precision: 0.7685575602068854
Weighted AreaUnderROC: 0.8078261038557214
Root mean squared error: 0.42213640669163044
Relative absolute error: 60.23798970387726
Root relative squared error: 84.42728133832608
Weighted TruePositiveRate: 0.7734375
Weighted MatthewsCorrelation: 0.4875129814898205
Weighted FMeasure: 0.7689406469809876
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8089340665559096
Mean absolute error: 0.3011899485193863
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7734375
Weighted FalsePositiveRate: 0.3049598880597015
Kappa statistic: 0.4844126180628433
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 91.27604166666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.735829663003071
Weighted AreaUnderROC: 0.7863880597014924
Root mean squared error: 0.4375282082355306
Relative absolute error: 64.48386555820332
Root relative squared error: 87.50564164710613
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.41594232331774544
Weighted FMeasure: 0.7370703913921582
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7860899389274848
Mean absolute error: 0.3224193277910166
Coverage of cases: 97.91666666666667
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3424860074626866
Kappa statistic: 0.41329711710599415
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 94.921875
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7597918344068706
Weighted AreaUnderROC: 0.8274626865671642
Root mean squared error: 0.40213642161183727
Relative absolute error: 61.59479238299045
Root relative squared error: 80.42728432236746
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.46556775530034117
Weighted FMeasure: 0.7588271454608302
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8301918607221004
Mean absolute error: 0.30797396191495224
Coverage of cases: 99.47916666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.32646082089552236
Kappa statistic: 0.4598649662415604
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 93.61979166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7516515485265485
Weighted AreaUnderROC: 0.8153731343283582
Root mean squared error: 0.41497691624250255
Relative absolute error: 63.19576868691171
Root relative squared error: 82.9953832485005
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44913243260585123
Weighted FMeasure: 0.7517698618644397
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8136016558918641
Mean absolute error: 0.31597884343455857
Coverage of cases: 98.17708333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3306483208955224
Kappa statistic: 0.44487968662562954
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 92.44791666666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7471324906367042
Weighted AreaUnderROC: 0.8186865671641791
Root mean squared error: 0.41220056242867475
Relative absolute error: 62.21056428314036
Root relative squared error: 82.44011248573496
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44123984905464003
Weighted FMeasure: 0.7482936564509212
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8179398961859863
Mean absolute error: 0.3110528214157018
Coverage of cases: 98.69791666666667
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32651461442786067
Kappa statistic: 0.4390108876176416
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7547717673814165
Weighted AreaUnderROC: 0.8085373134328359
Root mean squared error: 0.4187185389928912
Relative absolute error: 63.41563540240863
Root relative squared error: 83.74370779857824
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.45703661217526026
Weighted FMeasure: 0.7553634201819687
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8083334227007462
Mean absolute error: 0.31707817701204316
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.3223271144278607
Kappa statistic: 0.4538033395176251
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 93.22916666666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7464019759113519
Weighted AreaUnderROC: 0.813134328358209
Root mean squared error: 0.4153163295036667
Relative absolute error: 63.16573314771987
Root relative squared error: 83.06326590073334
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4384122675781669
Weighted FMeasure: 0.7470732442541462
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8130640247036194
Mean absolute error: 0.31582866573859936
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.333439987562189
Kappa statistic: 0.4349792453999132
Training time: 1.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7471324906367042
Weighted AreaUnderROC: 0.8175522388059702
Root mean squared error: 0.4130910775698261
Relative absolute error: 62.47895568456521
Root relative squared error: 82.61821551396523
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44123984905464003
Weighted FMeasure: 0.7482936564509212
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8174343113466905
Mean absolute error: 0.31239477842282604
Coverage of cases: 98.17708333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32651461442786067
Kappa statistic: 0.4390108876176416
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 92.70833333333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7451388888888889
Weighted AreaUnderROC: 0.8104179104477612
Root mean squared error: 0.41775086038941533
Relative absolute error: 63.08552937908806
Root relative squared error: 83.55017207788306
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4376054243981519
Weighted FMeasure: 0.7465149054811727
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8126432928054149
Mean absolute error: 0.3154276468954403
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 27.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.32444776119402985
Kappa statistic: 0.43612334801762115
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 89.453125
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7480839657375693
Weighted AreaUnderROC: 0.8075820895522389
Root mean squared error: 0.42188897258038327
Relative absolute error: 61.773436856888395
Root relative squared error: 84.37779451607665
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44427789981020976
Weighted FMeasure: 0.7494326434277414
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8097041623821633
Mean absolute error: 0.308867184284442
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31958924129353233
Kappa statistic: 0.4429854027972881
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7416309062384644
Weighted AreaUnderROC: 0.802179104477612
Root mean squared error: 0.428013717705714
Relative absolute error: 61.69500252356106
Root relative squared error: 85.6027435411428
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.43090552972108154
Weighted FMeasure: 0.7428748990510802
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8050675849964343
Mean absolute error: 0.3084750126178053
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32031405472636815
Kappa statistic: 0.4304395205230657
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 85.9375
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7326222743791643
Weighted AreaUnderROC: 0.7944477611940298
Root mean squared error: 0.4366359284840568
Relative absolute error: 61.948754289623295
Root relative squared error: 87.32718569681136
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.4114358469329831
Weighted FMeasure: 0.7334167568542568
Iteration time: 1.0
Weighted AreaUnderPRC: 0.798177845394529
Mean absolute error: 0.3097437714481165
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3258973880597015
Kappa statistic: 0.4113261993507274
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 82.94270833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7301266125266322
Weighted AreaUnderROC: 0.7920597014925373
Root mean squared error: 0.4433229515479858
Relative absolute error: 61.28439391568191
Root relative squared error: 88.66459030959716
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.40573385086399444
Weighted FMeasure: 0.7281013737783515
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7961577122277509
Mean absolute error: 0.30642196957840956
Coverage of cases: 95.05208333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.31623414179104475
Kappa statistic: 0.40541497080162797
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 79.81770833333333
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7292390046296297
Weighted AreaUnderROC: 0.7904529695273631
Root mean squared error: 0.45122109870804067
Relative absolute error: 60.50965033326783
Root relative squared error: 90.24421974160813
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.4034576922711182
Weighted FMeasure: 0.7261063598101112
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7946451363040365
Mean absolute error: 0.30254825166633914
Coverage of cases: 93.75
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.31416728855721393
Kappa statistic: 0.40281690140845083
Training time: 0.0
		
Time end:Sat Oct 07 11.45.17 EEST 2017