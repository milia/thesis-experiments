Sat Oct 07 11.46.34 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.34 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 79.33593749999999
Incorrectly Classified Instances: 28.203125
Correctly Classified Instances: 71.796875
Weighted Precision: 0.7173780346567268
Weighted AreaUnderROC: 0.7591564054726369
Root mean squared error: 0.4641101538993843
Relative absolute error: 62.40445883679913
Root relative squared error: 92.82203077987685
Weighted TruePositiveRate: 0.71796875
Weighted MatthewsCorrelation: 0.37157087039922226
Weighted FMeasure: 0.7125305826255136
Iteration time: 4.9
Weighted AreaUnderPRC: 0.7491401284832042
Mean absolute error: 0.3120222941839957
Coverage of cases: 91.69270833333333
Instances selection time: 3.7
Test time: 4.6
Accumulative iteration time: 4.9
Weighted Recall: 0.71796875
Weighted FalsePositiveRate: 0.3610075559701492
Kappa statistic: 0.36530916104075645
Training time: 1.2
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 83.0078125
Incorrectly Classified Instances: 24.53125
Correctly Classified Instances: 75.46875
Weighted Precision: 0.7512599835334288
Weighted AreaUnderROC: 0.8117164179104478
Root mean squared error: 0.4240170677504526
Relative absolute error: 57.450329533628576
Root relative squared error: 84.80341355009054
Weighted TruePositiveRate: 0.7546875
Weighted MatthewsCorrelation: 0.4503930752467048
Weighted FMeasure: 0.7515395771753483
Iteration time: 1.8
Weighted AreaUnderPRC: 0.8118753782245047
Mean absolute error: 0.2872516476681429
Coverage of cases: 96.51041666666666
Instances selection time: 0.4
Test time: 3.9
Accumulative iteration time: 6.7
Weighted Recall: 0.7546875
Weighted FalsePositiveRate: 0.3170875
Kappa statistic: 0.4482398996484817
Training time: 1.4
		
Time end:Sat Oct 07 11.46.35 EEST 2017