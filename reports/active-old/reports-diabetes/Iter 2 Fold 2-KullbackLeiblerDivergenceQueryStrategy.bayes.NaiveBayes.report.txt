Sat Oct 07 11.44.16 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Sat Oct 07 11.44.16 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 67.1875
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7070236521655754
Weighted AreaUnderROC: 0.7131044776119403
Root mean squared error: 0.5273836560358217
Relative absolute error: 66.94000018046293
Root relative squared error: 105.47673120716435
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.3473334371516133
Weighted FMeasure: 0.6864840719438693
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6940377979448993
Mean absolute error: 0.33470000090231466
Coverage of cases: 82.03125
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.31712033582089555
Kappa statistic: 0.33963424864381186
Training time: 1.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 82.68229166666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.734375
Weighted AreaUnderROC: 0.7952238805970149
Root mean squared error: 0.4414271059369448
Relative absolute error: 60.19637895619656
Root relative squared error: 88.28542118738896
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.41540298507462686
Weighted FMeasure: 0.734375
Iteration time: 11.0
Weighted AreaUnderPRC: 0.796494184489875
Mean absolute error: 0.3009818947809828
Coverage of cases: 95.57291666666667
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 22.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.31897201492537314
Kappa statistic: 0.4154029850746269
Training time: 6.0
		
Time end:Sat Oct 07 11.44.16 EEST 2017