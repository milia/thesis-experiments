Sat Oct 07 11.43.52 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.52 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 39.0625
Correctly Classified Instances: 60.9375
Weighted Precision: 0.6406386582167832
Weighted AreaUnderROC: 0.6065074626865673
Root mean squared error: 0.6170683429798981
Relative absolute error: 78.68589743589759
Root relative squared error: 123.41366859597962
Weighted TruePositiveRate: 0.609375
Weighted MatthewsCorrelation: 0.20377196642112894
Weighted FMeasure: 0.6179259402732779
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6034547321168415
Mean absolute error: 0.39342948717948795
Coverage of cases: 60.9375
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 8.0
Weighted Recall: 0.609375
Weighted FalsePositiveRate: 0.3963600746268657
Kappa statistic: 0.19857524487978626
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.677083333333336
Correctly Classified Instances: 64.32291666666667
Weighted Precision: 0.6940480593483356
Weighted AreaUnderROC: 0.6619402985074627
Root mean squared error: 0.5912646777884965
Relative absolute error: 71.93877551020402
Root relative squared error: 118.25293555769929
Weighted TruePositiveRate: 0.6432291666666666
Weighted MatthewsCorrelation: 0.30885413370879117
Weighted FMeasure: 0.6514647872637064
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6408799529551308
Mean absolute error: 0.3596938775510201
Coverage of cases: 64.32291666666667
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6432291666666666
Weighted FalsePositiveRate: 0.3193485696517413
Kappa statistic: 0.29202777628249976
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.699859069304742
Weighted AreaUnderROC: 0.6600597014925373
Root mean squared error: 0.6072198005990808
Relative absolute error: 75.42372881355934
Root relative squared error: 121.44396011981615
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.30800098895417305
Weighted FMeasure: 0.6318837412587412
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6399059001199753
Mean absolute error: 0.3771186440677967
Coverage of cases: 62.5
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 20.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.30488059701492537
Kappa statistic: 0.2794746169081622
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.020833333333336
Correctly Classified Instances: 61.979166666666664
Weighted Precision: 0.7078307748538012
Weighted AreaUnderROC: 0.6629850746268656
Root mean squared error: 0.6121687492920113
Relative absolute error: 76.38888888888887
Root relative squared error: 122.43374985840225
Weighted TruePositiveRate: 0.6197916666666666
Weighted MatthewsCorrelation: 0.31635194735522626
Weighted FMeasure: 0.6251414668553648
Iteration time: 7.0
Weighted AreaUnderPRC: 0.642419647752193
Mean absolute error: 0.38194444444444436
Coverage of cases: 61.979166666666664
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6197916666666666
Weighted FalsePositiveRate: 0.29382151741293533
Kappa statistic: 0.28034504004929134
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 39.84375
Correctly Classified Instances: 60.15625
Weighted Precision: 0.7205790755615054
Weighted AreaUnderROC: 0.6611044776119402
Root mean squared error: 0.6272432104254579
Relative absolute error: 79.94462025316452
Root relative squared error: 125.44864208509159
Weighted TruePositiveRate: 0.6015625
Weighted MatthewsCorrelation: 0.3216573298262076
Weighted FMeasure: 0.6018732409887763
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6423341550519206
Mean absolute error: 0.3997231012658226
Coverage of cases: 60.15625
Instances selection time: 6.0
Test time: 24.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6015625
Weighted FalsePositiveRate: 0.2793535447761194
Kappa statistic: 0.2687079910380881
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 40.364583333333336
Correctly Classified Instances: 59.635416666666664
Weighted Precision: 0.7253557721538227
Weighted AreaUnderROC: 0.6605671641791044
Root mean squared error: 0.6317768977755452
Relative absolute error: 80.94569288389512
Root relative squared error: 126.35537955510905
Weighted TruePositiveRate: 0.5963541666666666
Weighted MatthewsCorrelation: 0.3240740010930434
Weighted FMeasure: 0.5946977597429758
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6424445434514648
Mean absolute error: 0.4047284644194756
Coverage of cases: 59.635416666666664
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 40.0
Weighted Recall: 0.5963541666666666
Weighted FalsePositiveRate: 0.2752198383084577
Kappa statistic: 0.2655116244632015
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 37.239583333333336
Correctly Classified Instances: 62.760416666666664
Weighted Precision: 0.7208539350083467
Weighted AreaUnderROC: 0.674179104477612
Root mean squared error: 0.6071735887376231
Relative absolute error: 74.73695286195282
Root relative squared error: 121.43471774752462
Weighted TruePositiveRate: 0.6276041666666666
Weighted MatthewsCorrelation: 0.3391538466634738
Weighted FMeasure: 0.6322700324053616
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6509016154796831
Mean absolute error: 0.37368476430976405
Coverage of cases: 62.760416666666664
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 52.0
Weighted Recall: 0.6276041666666666
Weighted FalsePositiveRate: 0.2792459577114428
Kappa statistic: 0.2982671369421867
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.15625
Correctly Classified Instances: 64.84375
Weighted Precision: 0.7206967283610844
Weighted AreaUnderROC: 0.6832537313432835
Root mean squared error: 0.5902187712731057
Relative absolute error: 70.58486238532117
Root relative squared error: 118.04375425462115
Weighted TruePositiveRate: 0.6484375
Weighted MatthewsCorrelation: 0.35191894195311363
Weighted FMeasure: 0.6552532893162188
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6570290293181075
Mean absolute error: 0.3529243119266059
Coverage of cases: 64.84375
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 59.0
Weighted Recall: 0.6484375
Weighted FalsePositiveRate: 0.28193003731343286
Kappa statistic: 0.3214304413843656
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.7227105034722222
Weighted AreaUnderROC: 0.694865671641791
Root mean squared error: 0.5658804956463549
Relative absolute error: 64.88095238095237
Root relative squared error: 113.17609912927098
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.37152368800154406
Weighted FMeasure: 0.6846222089590909
Iteration time: 7.0
Weighted AreaUnderPRC: 0.66552734375
Mean absolute error: 0.3244047619047618
Coverage of cases: 67.70833333333333
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.2873519900497512
Kappa statistic: 0.35416666666666674
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.7129610609353071
Weighted AreaUnderROC: 0.6859402985074627
Root mean squared error: 0.5683418330162776
Relative absolute error: 65.37467700258391
Root relative squared error: 113.66836660325552
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.35474274364454916
Weighted FMeasure: 0.6820131611406284
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6587030766780836
Mean absolute error: 0.32687338501291957
Coverage of cases: 67.44791666666667
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 72.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.3025985696517413
Kappa statistic: 0.3417082670470129
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.7158803104575163
Weighted AreaUnderROC: 0.6902089552238806
Root mean squared error: 0.5616365383051205
Relative absolute error: 63.80395683453221
Root relative squared error: 112.3273076610241
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3633557381541086
Weighted FMeasure: 0.6894679400843645
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6620477813521242
Mean absolute error: 0.319019784172661
Coverage of cases: 68.22916666666667
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.3018737562189055
Kappa statistic: 0.35235567352355673
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6970736326681449
Weighted AreaUnderROC: 0.6706268656716418
Root mean squared error: 0.566357397294239
Relative absolute error: 64.82102908277385
Root relative squared error: 113.2714794588478
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.3288263015715474
Weighted FMeasure: 0.683031676900376
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6477203075919253
Mean absolute error: 0.32410514541386926
Coverage of cases: 67.70833333333333
Instances selection time: 4.0
Test time: 19.0
Accumulative iteration time: 82.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.33582960199004974
Kappa statistic: 0.32440408626560735
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6990978422619047
Weighted AreaUnderROC: 0.6728955223880598
Root mean squared error: 0.5618897192191878
Relative absolute error: 63.77096436058692
Root relative squared error: 112.37794384383757
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3343121378946778
Weighted FMeasure: 0.687626474640489
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6495775979662698
Mean absolute error: 0.3188548218029346
Coverage of cases: 68.22916666666667
Instances selection time: 4.0
Test time: 20.0
Accumulative iteration time: 86.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.33650062189054725
Kappa statistic: 0.3308957952468006
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7003505184795689
Weighted AreaUnderROC: 0.6737014925373135
Root mean squared error: 0.5550413348323107
Relative absolute error: 62.204142011833994
Root relative squared error: 111.00826696646213
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.33899205146574435
Weighted FMeasure: 0.6938930205949657
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6506596745270316
Mean absolute error: 0.31102071005917
Coverage of cases: 69.01041666666667
Instances selection time: 3.0
Test time: 21.0
Accumulative iteration time: 89.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.34270118159203977
Kappa statistic: 0.3374702777938872
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7067343877747319
Weighted AreaUnderROC: 0.6799701492537314
Root mean squared error: 0.5457232826254595
Relative absolute error: 60.119878957169156
Root relative squared error: 109.1446565250919
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.35389133118321064
Weighted FMeasure: 0.7030561694189738
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6558533324422332
Mean absolute error: 0.3005993947858458
Coverage of cases: 70.05208333333333
Instances selection time: 2.0
Test time: 22.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.34058053482587064
Kappa statistic: 0.35321342785166104
Training time: 0.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7012026736924278
Weighted AreaUnderROC: 0.6727761194029851
Root mean squared error: 0.5481715128521253
Relative absolute error: 60.62610229276913
Root relative squared error: 109.63430257042506
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.3421977767261337
Weighted FMeasure: 0.699389634248451
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6507353187817134
Mean absolute error: 0.3031305114638457
Coverage of cases: 69.79166666666667
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 92.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.3523644278606965
Kappa statistic: 0.34199952729851085
Training time: 1.0
		
Time end:Sat Oct 07 11.43.53 EEST 2017