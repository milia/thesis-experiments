Sat Oct 07 11.46.21 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.21 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 63.541666666666664
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7236281971087016
Weighted AreaUnderROC: 0.7259850746268657
Root mean squared error: 0.4932143489755427
Relative absolute error: 57.596184546615646
Root relative squared error: 98.64286979510854
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3873326610522306
Weighted FMeasure: 0.7243594454242602
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7039128251028505
Mean absolute error: 0.28798092273307824
Coverage of cases: 81.77083333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.36538277363184085
Kappa statistic: 0.3829641185647427
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 88.28125
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7670852128151747
Weighted AreaUnderROC: 0.7924477611940298
Root mean squared error: 0.4207371939741498
Relative absolute error: 55.12521742568024
Root relative squared error: 84.14743879482997
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.4873199883274227
Weighted FMeasure: 0.7676084217962664
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7729640351238064
Mean absolute error: 0.2756260871284012
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.2835127487562189
Kappa statistic: 0.4872471943827641
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 84.63541666666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7437462775461584
Weighted AreaUnderROC: 0.7819701492537313
Root mean squared error: 0.44044241765270103
Relative absolute error: 57.45731516290735
Root relative squared error: 88.0884835305402
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42107976712987666
Weighted FMeasure: 0.7364562583678934
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7518041877453405
Mean absolute error: 0.28728657581453676
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.37292537313432844
Kappa statistic: 0.4066443471542622
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 79.6875
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7431171764587119
Weighted AreaUnderROC: 0.6837611940298508
Root mean squared error: 0.48051441650720744
Relative absolute error: 55.66333178961915
Root relative squared error: 96.1028833014415
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.43452724787163627
Weighted FMeasure: 0.7438710016835017
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6844855799868709
Mean absolute error: 0.27831665894809576
Coverage of cases: 87.23958333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.31338868159203975
Kappa statistic: 0.4344114464350125
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 80.20833333333333
Incorrectly Classified Instances: 34.375
Correctly Classified Instances: 65.625
Weighted Precision: 0.6692723037831733
Weighted AreaUnderROC: 0.6294179104477612
Root mean squared error: 0.522578740628925
Relative absolute error: 73.54477191406174
Root relative squared error: 104.515748125785
Weighted TruePositiveRate: 0.65625
Weighted MatthewsCorrelation: 0.2704473920886305
Weighted FMeasure: 0.6610243055555555
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6264379474746015
Mean absolute error: 0.3677238595703087
Coverage of cases: 88.54166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.65625
Weighted FalsePositiveRate: 0.378160447761194
Kappa statistic: 0.26878245816503177
Training time: 2.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 100.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7313368055555555
Weighted AreaUnderROC: 0.6805373134328359
Root mean squared error: 0.4426094224868457
Relative absolute error: 71.0967295957288
Root relative squared error: 88.52188449736914
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.39745458766220243
Weighted FMeasure: 0.7272675233015463
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6635470920138888
Mean absolute error: 0.355483647978644
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.37850870646766177
Kappa statistic: 0.3865030674846626
Training time: 2.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 95.3125
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7080853174603176
Weighted AreaUnderROC: 0.6146268656716418
Root mean squared error: 0.4677169662955678
Relative absolute error: 73.3765968508619
Root relative squared error: 93.54339325911356
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3394926604869688
Weighted FMeasure: 0.6992806505421184
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6314882841007572
Mean absolute error: 0.3668829842543095
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.42430223880597023
Kappa statistic: 0.32235294117647056
Training time: 2.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 65.36458333333333
Incorrectly Classified Instances: 33.59375
Correctly Classified Instances: 66.40625
Weighted Precision: 0.691715834999863
Weighted AreaUnderROC: 0.6384776119402985
Root mean squared error: 0.5178454979161305
Relative absolute error: 66.84270773086554
Root relative squared error: 103.5690995832261
Weighted TruePositiveRate: 0.6640625
Weighted MatthewsCorrelation: 0.3143902361308116
Weighted FMeasure: 0.6711906120386922
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6378453032688216
Mean absolute error: 0.33421353865432774
Coverage of cases: 80.46875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6640625
Weighted FalsePositiveRate: 0.3358833955223881
Kappa statistic: 0.3074212851630222
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 75.13020833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7478126983360468
Weighted AreaUnderROC: 0.7418955223880598
Root mean squared error: 0.4552781925425278
Relative absolute error: 61.10620678820386
Root relative squared error: 91.05563850850557
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.43779745846120965
Weighted FMeasure: 0.7318905308500572
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7180366494275967
Mean absolute error: 0.3055310339410193
Coverage of cases: 91.14583333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.2712192164179104
Kappa statistic: 0.43073360817755685
Training time: 4.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 82.29166666666667
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.695445956160242
Weighted AreaUnderROC: 0.7031044776119403
Root mean squared error: 0.47989264662424
Relative absolute error: 69.45269257493106
Root relative squared error: 95.97852932484801
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3172039998175525
Weighted FMeasure: 0.6914828431372548
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6869446690406912
Mean absolute error: 0.3472634628746553
Coverage of cases: 90.625
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.42642288557213925
Kappa statistic: 0.30515703761147744
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 75.13020833333333
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.6960155857214682
Weighted AreaUnderROC: 0.7289253731343285
Root mean squared error: 0.4705805935489815
Relative absolute error: 66.04977649640628
Root relative squared error: 94.1161187097963
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3093701132217181
Weighted FMeasure: 0.6844995701551966
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7056017232257901
Mean absolute error: 0.3302488824820314
Coverage of cases: 89.0625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.44719900497512444
Kappa statistic: 0.28917096390321306
Training time: 4.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 85.9375
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7206622383252818
Weighted AreaUnderROC: 0.7120597014925373
Root mean squared error: 0.47241694311971694
Relative absolute error: 62.84301164647862
Root relative squared error: 94.48338862394338
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.38050480490288896
Weighted FMeasure: 0.7213113680880704
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6948312454045814
Mean absolute error: 0.3142150582323931
Coverage of cases: 90.88541666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.3702412935323383
Kappa statistic: 0.3758439609902475
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 78.25520833333333
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.7248641304347826
Weighted AreaUnderROC: 0.7075522388059702
Root mean squared error: 0.47677577282833977
Relative absolute error: 68.95070358051059
Root relative squared error: 95.35515456566796
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.38050480490288896
Weighted FMeasure: 0.694706498951782
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6917922214998554
Mean absolute error: 0.344753517902553
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.28869402985074627
Kappa statistic: 0.367032967032967
Training time: 5.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 66.27604166666667
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7076584952051227
Weighted AreaUnderROC: 0.6987014925373134
Root mean squared error: 0.4974849098700757
Relative absolute error: 61.69425146239666
Root relative squared error: 99.49698197401514
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3563101489468847
Weighted FMeasure: 0.7090566834432882
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6844177230015446
Mean absolute error: 0.30847125731198327
Coverage of cases: 82.29166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.3592360074626866
Kappa statistic: 0.3560161962893576
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 89.58333333333333
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7257025952008346
Weighted AreaUnderROC: 0.6982537313432836
Root mean squared error: 0.4704043122371285
Relative absolute error: 62.91586557821552
Root relative squared error: 94.0808624474257
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.38836631322121945
Weighted FMeasure: 0.7067197329131701
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6905001271246123
Mean absolute error: 0.31457932789107756
Coverage of cases: 92.1875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.2955656094527363
Kappa statistic: 0.38057566066318815
Training time: 7.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 98.4375
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7187217063084227
Weighted AreaUnderROC: 0.7629701492537313
Root mean squared error: 0.4303728995127475
Relative absolute error: 62.39418967475626
Root relative squared error: 86.0745799025495
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.3807988774091369
Weighted FMeasure: 0.7173116235616236
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7428671931017604
Mean absolute error: 0.3119709483737813
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.3322055348258706
Kappa statistic: 0.38064516129032266
Training time: 6.0
		
Time end:Sat Oct 07 11.46.21 EEST 2017