Sat Oct 07 11.45.40 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.40 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.625
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6944899980621981
Weighted AreaUnderROC: 0.6432388059701494
Root mean squared error: 0.521769658614954
Relative absolute error: 67.105736525379
Root relative squared error: 104.3539317229908
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.32666952153424084
Weighted FMeasure: 0.6966816209914767
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6345233675280922
Mean absolute error: 0.335528682626895
Coverage of cases: 89.32291666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.3821327736318408
Kappa statistic: 0.3257191718072437
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 95.44270833333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.720734126984127
Weighted AreaUnderROC: 0.6514776119402985
Root mean squared error: 0.46632495842399924
Relative absolute error: 71.69192226549902
Root relative squared error: 93.26499168479985
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.3659251790650296
Weighted FMeasure: 0.7104184042257436
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6408814574734502
Mean absolute error: 0.3584596113274951
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.41179353233830845
Kappa statistic: 0.34745098039215677
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 33.854166666666664
Correctly Classified Instances: 66.14583333333333
Weighted Precision: 0.7472563244047619
Weighted AreaUnderROC: 0.7240597014925374
Root mean squared error: 0.48298902945541133
Relative absolute error: 72.83338979223146
Root relative squared error: 96.59780589108226
Weighted TruePositiveRate: 0.6614583333333334
Weighted MatthewsCorrelation: 0.39711110302406466
Weighted FMeasure: 0.6668554412499432
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6960558711533261
Mean absolute error: 0.36416694896115726
Coverage of cases: 94.79166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6614583333333334
Weighted FalsePositiveRate: 0.25071206467661694
Kappa statistic: 0.3553719008264463
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 93.48958333333333
Incorrectly Classified Instances: 39.0625
Correctly Classified Instances: 60.9375
Weighted Precision: 0.7050836894586895
Weighted AreaUnderROC: 0.6164477611940299
Root mean squared error: 0.5128036582315258
Relative absolute error: 84.02883638211395
Root relative squared error: 102.56073164630517
Weighted TruePositiveRate: 0.609375
Weighted MatthewsCorrelation: 0.3062057468983906
Weighted FMeasure: 0.6136209239130435
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6186397370830713
Mean absolute error: 0.4201441819105698
Coverage of cases: 94.53125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.609375
Weighted FalsePositiveRate: 0.29594216417910446
Kappa statistic: 0.26717557251908397
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 84.24479166666667
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7532871526270082
Weighted AreaUnderROC: 0.7816119402985074
Root mean squared error: 0.44047990406155413
Relative absolute error: 71.90943590704654
Root relative squared error: 88.09598081231083
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.4328553005195535
Weighted FMeasure: 0.7100015566625156
Iteration time: 3.0
Weighted AreaUnderPRC: 0.751092349806718
Mean absolute error: 0.35954717953523274
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.24915485074626864
Kappa statistic: 0.40996333836532245
Training time: 2.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 83.72395833333333
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7459713494304044
Weighted AreaUnderROC: 0.6867611940298507
Root mean squared error: 0.5209376774213318
Relative absolute error: 72.35903814262036
Root relative squared error: 104.18753548426636
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.42351445173450647
Weighted FMeasure: 0.7125769669029252
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6802129882329345
Mean absolute error: 0.3617951907131018
Coverage of cases: 88.80208333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.261609763681592
Kappa statistic: 0.40679170995789343
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 83.59375
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7181188883607227
Weighted AreaUnderROC: 0.6861343283582091
Root mean squared error: 0.530717013225617
Relative absolute error: 73.71079119140059
Root relative squared error: 106.1434026451234
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.36567718043693215
Weighted FMeasure: 0.6871009505623783
Iteration time: 4.0
Weighted AreaUnderPRC: 0.686019952223876
Mean absolute error: 0.36855395595700297
Coverage of cases: 88.80208333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.2963442164179104
Kappa statistic: 0.35224093477426077
Training time: 4.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 85.28645833333333
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7291057462960415
Weighted AreaUnderROC: 0.7123880597014925
Root mean squared error: 0.5059981662530483
Relative absolute error: 70.66268957631206
Root relative squared error: 101.19963325060965
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.3965178891496477
Weighted FMeasure: 0.7116502980943255
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6993627754933609
Mean absolute error: 0.3533134478815603
Coverage of cases: 92.1875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.29277394278606966
Kappa statistic: 0.3893611032929917
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.7396321951929021
Weighted AreaUnderROC: 0.7168805970149253
Root mean squared error: 0.4995602701263812
Relative absolute error: 65.11747503345856
Root relative squared error: 99.91205402527625
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.41508522751080834
Weighted FMeasure: 0.714827935222672
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7039663948387407
Mean absolute error: 0.3255873751672928
Coverage of cases: 92.96875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.2740646766169154
Kappa statistic: 0.403528236991013
Training time: 4.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7461935277431854
Weighted AreaUnderROC: 0.7220895522388061
Root mean squared error: 0.4906694573635679
Relative absolute error: 64.2884233255005
Root relative squared error: 98.13389147271357
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.43509297160778376
Weighted FMeasure: 0.7317005066640044
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7078974796062253
Mean absolute error: 0.3214421166275025
Coverage of cases: 94.27083333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.27468190298507467
Kappa statistic: 0.4288628250892402
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7480858262108262
Weighted AreaUnderROC: 0.7285373134328358
Root mean squared error: 0.4874515746341925
Relative absolute error: 65.0949473433732
Root relative squared error: 97.4903149268385
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.42672382893015043
Weighted FMeasure: 0.7125979087672917
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7112687624134152
Mean absolute error: 0.325474736716866
Coverage of cases: 95.05208333333333
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.25814707711442786
Kappa statistic: 0.4086672117743254
Training time: 9.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7427418922189043
Weighted AreaUnderROC: 0.7380149253731343
Root mean squared error: 0.4895428481709902
Relative absolute error: 64.20617590428598
Root relative squared error: 97.90856963419805
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.4242009062001509
Weighted FMeasure: 0.7221688117226158
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7182362027589364
Mean absolute error: 0.3210308795214299
Coverage of cases: 94.53125
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.2733398631840796
Kappa statistic: 0.41479783009898785
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7535998002315075
Weighted AreaUnderROC: 0.7662985074626866
Root mean squared error: 0.4520042414105071
Relative absolute error: 62.37523676663306
Root relative squared error: 90.40084828210142
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.45370454817721845
Weighted FMeasure: 0.7437202620774338
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7422517531664313
Mean absolute error: 0.3118761838331653
Coverage of cases: 94.27083333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.27116542288557216
Kappa statistic: 0.44973059727158093
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 92.44791666666667
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7129517910767911
Weighted AreaUnderROC: 0.7316567164179104
Root mean squared error: 0.4646793974763949
Relative absolute error: 66.78013389430569
Root relative squared error: 92.93587949527898
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3647670951603916
Weighted FMeasure: 0.714401884080592
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7226140676413766
Mean absolute error: 0.33390066947152847
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3744287935323383
Kappa statistic: 0.3613131878380898
Training time: 5.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 84.375
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.729955038035281
Weighted AreaUnderROC: 0.7421492537313433
Root mean squared error: 0.45145843265804747
Relative absolute error: 61.63579689973647
Root relative squared error: 90.2916865316095
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3861022090077879
Weighted FMeasure: 0.7192921854596568
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7158461361389561
Mean absolute error: 0.30817898449868236
Coverage of cases: 91.40625
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.40068065920398005
Kappa statistic: 0.3674734164002869
Training time: 7.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.768301982638388
Weighted AreaUnderROC: 0.7972985074626866
Root mean squared error: 0.41593871430844115
Relative absolute error: 59.34541384128361
Root relative squared error: 83.18774286168824
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4891319394177632
Weighted FMeasure: 0.765027055801101
Iteration time: 6.0
Weighted AreaUnderPRC: 0.770192198951794
Mean absolute error: 0.29672706920641806
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 77.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.26552829601990047
Kappa statistic: 0.4881949733434883
Training time: 6.0
		
Time end:Sat Oct 07 11.45.41 EEST 2017