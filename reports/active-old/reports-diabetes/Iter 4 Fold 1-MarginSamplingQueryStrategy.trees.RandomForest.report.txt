Sat Oct 07 11.45.50 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.45.50 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7247019262111397
Weighted AreaUnderROC: 0.765955223880597
Root mean squared error: 0.43069106290549686
Relative absolute error: 65.46875000000004
Root relative squared error: 86.13821258109937
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3714183707167488
Weighted FMeasure: 0.7115636871469224
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7585398220980468
Mean absolute error: 0.3273437500000002
Coverage of cases: 98.17708333333333
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.4138603855721393
Kappa statistic: 0.3500295799645042
Training time: 13.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7214271250238231
Weighted AreaUnderROC: 0.7495522388059701
Root mean squared error: 0.4392820847701396
Relative absolute error: 66.87500000000003
Root relative squared error: 87.85641695402792
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.34711124134866717
Weighted FMeasure: 0.6935548708920188
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7403097060666114
Mean absolute error: 0.33437500000000014
Coverage of cases: 97.39583333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.4492120646766169
Kappa statistic: 0.311408850994722
Training time: 14.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 89.58333333333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7224212334506452
Weighted AreaUnderROC: 0.753179104477612
Root mean squared error: 0.4343038011192935
Relative absolute error: 65.88541666666666
Root relative squared error: 86.8607602238587
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.36369024284089485
Weighted FMeasure: 0.7070353151441112
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7504188712117758
Mean absolute error: 0.3294270833333333
Coverage of cases: 97.91666666666667
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.42218159203980093
Kappa statistic: 0.3399444664815548
Training time: 12.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 87.36979166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7581156619630628
Weighted AreaUnderROC: 0.7950447761194029
Root mean squared error: 0.4145152791715483
Relative absolute error: 61.145833333333385
Root relative squared error: 82.90305583430965
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.4383520001676222
Weighted FMeasure: 0.7385444423296535
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7828273737935288
Mean absolute error: 0.30572916666666694
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3860513059701493
Kappa statistic: 0.41088749587594847
Training time: 11.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 85.41666666666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7298840140123035
Weighted AreaUnderROC: 0.7551641791044776
Root mean squared error: 0.4373511651598367
Relative absolute error: 62.86458333333332
Root relative squared error: 87.47023303196734
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3768167717942131
Weighted FMeasure: 0.7114983358934972
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7464828287776104
Mean absolute error: 0.3143229166666666
Coverage of cases: 95.83333333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.4193899253731343
Kappa statistic: 0.3501460047783382
Training time: 12.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 91.14583333333333
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7017609126984127
Weighted AreaUnderROC: 0.7250746268656716
Root mean squared error: 0.44610583572361706
Relative absolute error: 69.79166666666666
Root relative squared error: 89.22116714472341
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3262764011979384
Weighted FMeasure: 0.6937117737003059
Iteration time: 14.0
Weighted AreaUnderPRC: 0.723481046397836
Mean absolute error: 0.3489583333333333
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 92.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.4305565920398009
Kappa statistic: 0.30980392156862735
Training time: 13.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 83.72395833333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.729955038035281
Weighted AreaUnderROC: 0.7661641791044776
Root mean squared error: 0.4347532825254648
Relative absolute error: 61.97916666666671
Root relative squared error: 86.95065650509297
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3861022090077879
Weighted FMeasure: 0.7192921854596568
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7596604731371724
Mean absolute error: 0.30989583333333354
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 109.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.40068065920398005
Kappa statistic: 0.3674734164002869
Training time: 16.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7345770747884566
Weighted AreaUnderROC: 0.7905671641791044
Root mean squared error: 0.4233731116481852
Relative absolute error: 62.65625000000002
Root relative squared error: 84.67462232963705
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.40232516404785823
Weighted FMeasure: 0.728674108560587
Iteration time: 19.0
Weighted AreaUnderPRC: 0.775194170581195
Mean absolute error: 0.3132812500000001
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 128.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.38057555970149254
Kappa statistic: 0.3892423366107577
Training time: 18.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 86.19791666666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7225116478479937
Weighted AreaUnderROC: 0.772731343283582
Root mean squared error: 0.4342138490037675
Relative absolute error: 63.541666666666686
Root relative squared error: 86.8427698007535
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3779438446024362
Weighted FMeasure: 0.7186343820974201
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7567523418072546
Mean absolute error: 0.3177083333333334
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 149.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3896215796019901
Kappa statistic: 0.36692489916127796
Training time: 20.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 86.328125
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7114077155799277
Weighted AreaUnderROC: 0.7641044776119403
Root mean squared error: 0.43334334924322226
Relative absolute error: 65.26041666666669
Root relative squared error: 86.66866984864446
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3455957833070528
Weighted FMeasure: 0.7015056586126048
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7568998980189144
Mean absolute error: 0.32630208333333344
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 185.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.42290640547263686
Kappa statistic: 0.32735249819920104
Training time: 35.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 87.890625
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7399626749955696
Weighted AreaUnderROC: 0.804910447761194
Root mean squared error: 0.41101373659120777
Relative absolute error: 61.61458333333333
Root relative squared error: 82.20274731824155
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.41783564552971814
Weighted FMeasure: 0.7366861704972391
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7929286745557856
Mean absolute error: 0.30807291666666664
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 211.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3639331467661691
Kappa statistic: 0.4081982840800764
Training time: 25.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.734867255185088
Weighted AreaUnderROC: 0.8003134328358209
Root mean squared error: 0.42031734043061636
Relative absolute error: 60.416666666666686
Root relative squared error: 84.06346808612327
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4014504487775707
Weighted FMeasure: 0.7277601289134438
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7825381649285683
Mean absolute error: 0.3020833333333334
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 237.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.38403824626865674
Kappa statistic: 0.3869573630910147
Training time: 26.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7048937267262952
Weighted AreaUnderROC: 0.788044776119403
Root mean squared error: 0.42457086177299863
Relative absolute error: 61.66666666666668
Root relative squared error: 84.91417235459973
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.3414039185815658
Weighted FMeasure: 0.7031918849307117
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7796602930968972
Mean absolute error: 0.3083333333333334
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 265.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.40492195273631837
Kappa statistic: 0.33252535561650826
Training time: 27.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 84.89583333333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7262073863636364
Weighted AreaUnderROC: 0.7891641791044774
Root mean squared error: 0.42229531531066433
Relative absolute error: 61.14583333333338
Root relative squared error: 84.45906306213287
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3807726202487147
Weighted FMeasure: 0.7180446242946243
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7699367725134255
Mean absolute error: 0.3057291666666669
Coverage of cases: 96.875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 296.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.39861380597014934
Kappa statistic: 0.36481577581733254
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 84.50520833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7456514550264551
Weighted AreaUnderROC: 0.7949999999999999
Root mean squared error: 0.42102924482748233
Relative absolute error: 60.05208333333337
Root relative squared error: 84.20584896549647
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.43342768640930923
Weighted FMeasure: 0.7443759672601392
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7802277911079951
Mean absolute error: 0.30026041666666686
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 329.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3472907338308458
Kappa statistic: 0.4267395813690363
Training time: 32.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 87.890625
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.6922717355547815
Weighted AreaUnderROC: 0.7621492537313433
Root mean squared error: 0.4352322177106532
Relative absolute error: 66.45833333333334
Root relative squared error: 87.04644354213063
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.30855310698956645
Weighted FMeasure: 0.6870802726237452
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7499083177405224
Mean absolute error: 0.3322916666666667
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 360.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.4347440920398009
Kappa statistic: 0.2949892766621172
Training time: 31.0
		
Time end:Sat Oct 07 11.45.51 EEST 2017