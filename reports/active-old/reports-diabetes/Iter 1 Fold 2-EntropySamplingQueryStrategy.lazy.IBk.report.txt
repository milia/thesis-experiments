Sat Oct 07 11.43.45 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Sat Oct 07 11.43.45 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.6548491005137004
Weighted AreaUnderROC: 0.6185373134328359
Root mean squared error: 0.5766826422066879
Relative absolute error: 69.04380341880339
Root relative squared error: 115.33652844133758
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.2401812258475775
Weighted FMeasure: 0.6566344642438807
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6125930122332296
Mean absolute error: 0.345219017094017
Coverage of cases: 65.88541666666667
Instances selection time: 19.0
Test time: 11.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.4217795398009951
Kappa statistic: 0.23998307850365622
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.6368131038647343
Weighted AreaUnderROC: 0.5934925373134329
Root mean squared error: 0.584757093138547
Relative absolute error: 70.40816326530609
Root relative squared error: 116.95141862770939
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.19822705405120558
Weighted FMeasure: 0.6409204165750139
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5970346939160628
Mean absolute error: 0.35204081632653045
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.46405659203980093
Kappa statistic: 0.19579894973743434
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.634858630952381
Weighted AreaUnderROC: 0.5900298507462688
Root mean squared error: 0.5857607051977038
Relative absolute error: 70.3036723163841
Root relative squared error: 117.15214103954077
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.193127111052421
Weighted FMeasure: 0.6389246736430422
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5950079055059524
Mean absolute error: 0.35151836158192046
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.4709819651741293
Kappa statistic: 0.18992443324937014
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.639547858617131
Weighted AreaUnderROC: 0.5908358208955223
Root mean squared error: 0.5798742974112169
Relative absolute error: 68.68961352657003
Root relative squared error: 115.97485948224337
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.2006775639722934
Weighted FMeasure: 0.6421466413083693
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5960496333607457
Mean absolute error: 0.34344806763285013
Coverage of cases: 65.88541666666667
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.4771825248756219
Kappa statistic: 0.19482681349638248
Training time: 1.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.375
Correctly Classified Instances: 65.625
Weighted Precision: 0.6300014663985252
Weighted AreaUnderROC: 0.5732537313432836
Root mean squared error: 0.5826137587289525
Relative absolute error: 69.14556962025313
Root relative squared error: 116.5227517457905
Weighted TruePositiveRate: 0.65625
Weighted MatthewsCorrelation: 0.173569789173776
Weighted FMeasure: 0.6281602076829101
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5857560004503938
Mean absolute error: 0.3457278481012656
Coverage of cases: 65.625
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 49.0
Weighted Recall: 0.65625
Weighted FalsePositiveRate: 0.5097425373134329
Kappa statistic: 0.16223720745735812
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.6323415587918015
Weighted AreaUnderROC: 0.5735223880597015
Root mean squared error: 0.5808134775931968
Relative absolute error: 68.58614232209736
Root relative squared error: 116.16269551863935
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.17679180609959264
Weighted FMeasure: 0.6287052273511882
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5861345349160598
Mean absolute error: 0.34293071161048677
Coverage of cases: 65.88541666666667
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 56.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.5118093905472637
Kappa statistic: 0.1637741871135048
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.59375
Correctly Classified Instances: 66.40625
Weighted Precision: 0.6372732649842271
Weighted AreaUnderROC: 0.5723283582089552
Root mean squared error: 0.5766885637156826
Relative absolute error: 67.51893939393942
Root relative squared error: 115.3377127431365
Weighted TruePositiveRate: 0.6640625
Weighted MatthewsCorrelation: 0.18167450008835287
Weighted FMeasure: 0.6279210758377426
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5859896403225967
Mean absolute error: 0.3375946969696971
Coverage of cases: 66.40625
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6640625
Weighted FalsePositiveRate: 0.5194057835820894
Kappa statistic: 0.16363881947727424
Training time: 1.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6555195184665732
Weighted AreaUnderROC: 0.5823283582089552
Root mean squared error: 0.5656634780858659
Relative absolute error: 64.90825688073393
Root relative squared error: 113.13269561717318
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.21329372229300372
Weighted FMeasure: 0.6380955354169641
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5933706061535763
Mean absolute error: 0.32454128440366964
Coverage of cases: 67.70833333333333
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 69.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.5124266169154229
Kappa statistic: 0.18810530623380187
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6597335397653193
Weighted AreaUnderROC: 0.5825970149253731
Root mean squared error: 0.5635942267623043
Relative absolute error: 64.36449579831944
Root relative squared error: 112.71884535246086
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21834792292780236
Weighted FMeasure: 0.638340560937148
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5939883071388798
Mean absolute error: 0.32182247899159716
Coverage of cases: 67.96875
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 75.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5144934701492537
Kappa statistic: 0.1898456260720411
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6566007295173962
Weighted AreaUnderROC: 0.5754029850746268
Root mean squared error: 0.5660640068297366
Relative absolute error: 64.85788113695088
Root relative squared error: 113.21280136594731
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.2067697908772412
Weighted FMeasure: 0.6306477745169969
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5896013082210999
Mean absolute error: 0.3242894056847544
Coverage of cases: 67.70833333333333
Instances selection time: 10.0
Test time: 25.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.5262773631840796
Kappa statistic: 0.17505197505197523
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6644209131823461
Weighted AreaUnderROC: 0.582865671641791
Root mean squared error: 0.5616365383051218
Relative absolute error: 63.80395683453243
Root relative squared error: 112.32730766102436
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.22381857098384392
Weighted FMeasure: 0.6385149183512414
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5946599271377758
Mean absolute error: 0.31901978417266214
Coverage of cases: 68.22916666666667
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 90.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.5165603233830846
Kappa statistic: 0.19160684704583097
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.6763253445862274
Weighted AreaUnderROC: 0.6168358208955224
Root mean squared error: 0.5524858072762739
Relative absolute error: 61.7170022371366
Root relative squared error: 110.49716145525478
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.27178032634807897
Weighted FMeasure: 0.6701934044551798
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6154952140568074
Mean absolute error: 0.308585011185683
Coverage of cases: 69.27083333333333
Instances selection time: 5.0
Test time: 19.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.45903669154228854
Kappa statistic: 0.25679044744784163
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6849970657276995
Weighted AreaUnderROC: 0.6381492537313432
Root mean squared error: 0.5478990116841509
Relative absolute error: 60.66561844863738
Root relative squared error: 109.57980233683018
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.300083262217657
Weighted FMeasure: 0.6855873267390121
Iteration time: 4.0
Weighted AreaUnderPRC: 0.628856566045579
Mean absolute error: 0.3033280922431869
Coverage of cases: 69.79166666666667
Instances selection time: 4.0
Test time: 20.0
Accumulative iteration time: 99.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.42161815920398005
Kappa statistic: 0.29358030956609993
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6921924667313183
Weighted AreaUnderROC: 0.654
Root mean squared error: 0.5456334426944829
Relative absolute error: 60.133136094674555
Root relative squared error: 109.12668853889657
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.320514779088312
Weighted FMeasure: 0.6945767172737192
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6389393835521782
Mean absolute error: 0.3006656804733728
Coverage of cases: 70.05208333333333
Instances selection time: 3.0
Test time: 21.0
Accumulative iteration time: 102.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.3925208333333334
Kappa statistic: 0.31847644916352863
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6927748328013376
Weighted AreaUnderROC: 0.6603880597014925
Root mean squared error: 0.5504481244391471
Relative absolute error: 61.155726256983236
Root relative squared error: 110.08962488782943
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.3237125130829006
Weighted FMeasure: 0.6939245849489891
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6423099636687282
Mean absolute error: 0.30577863128491617
Coverage of cases: 69.53125
Instances selection time: 2.0
Test time: 22.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.37453638059701494
Kappa statistic: 0.3235772357723577
Training time: 0.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.696887025012025
Weighted AreaUnderROC: 0.6658507462686568
Root mean squared error: 0.5481715128521266
Relative absolute error: 60.62610229276891
Root relative squared error: 109.63430257042532
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.3328760836186539
Weighted FMeasure: 0.697382774665748
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6461589715984247
Mean absolute error: 0.30313051146384457
Coverage of cases: 69.79166666666667
Instances selection time: 1.0
Test time: 24.0
Accumulative iteration time: 105.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.3662151741293533
Kappa statistic: 0.3328540618260243
Training time: 0.0
		
Time end:Sat Oct 07 11.43.46 EEST 2017