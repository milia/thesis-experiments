Sat Oct 07 11.46.40 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 300
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.40 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7150716131185408
Weighted AreaUnderROC: 0.7388208955223879
Root mean squared error: 0.44863798509117275
Relative absolute error: 66.6145833333332
Root relative squared error: 89.72759701823455
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.34092008537141383
Weighted FMeasure: 0.6941281643056217
Iteration time: 11.0
Weighted AreaUnderPRC: 0.729789613909796
Mean absolute error: 0.333072916666666
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.44368252487562193
Kappa statistic: 0.31166655498224216
Training time: 10.0
		
Iteration: 2
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 22.65625
Correctly Classified Instances: 77.34375
Weighted Precision: 0.7699195274084866
Weighted AreaUnderROC: 0.8071940298507463
Root mean squared error: 0.4082482904638627
Relative absolute error: 61.45833333333327
Root relative squared error: 81.64965809277254
Weighted TruePositiveRate: 0.7734375
Weighted MatthewsCorrelation: 0.4792388621053749
Weighted FMeasure: 0.7623416625483065
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7932599117126111
Mean absolute error: 0.30729166666666635
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7734375
Weighted FalsePositiveRate: 0.3395867537313433
Kappa statistic: 0.46526666239836095
Training time: 32.0
		
Time end:Sat Oct 07 11.46.40 EEST 2017