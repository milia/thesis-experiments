Sat Oct 07 11.46.17 EEST 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Sat Oct 07 11.46.17 EEST 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 79.0625
Incorrectly Classified Instances: 29.375
Correctly Classified Instances: 70.625
Weighted Precision: 0.6883149083449328
Weighted AreaUnderROC: 0.6724835820895523
Root mean squared error: 0.4829367600151414
Relative absolute error: 66.66278649265935
Root relative squared error: 96.58735200302829
Weighted TruePositiveRate: 0.70625
Weighted MatthewsCorrelation: 0.32691843418272265
Weighted FMeasure: 0.6842213351956101
Iteration time: 3.7
Weighted AreaUnderPRC: 0.6669981846425476
Mean absolute error: 0.3333139324632968
Coverage of cases: 89.66145833333331
Instances selection time: 1.6
Test time: 1.6
Accumulative iteration time: 3.7
Weighted Recall: 0.70625
Weighted FalsePositiveRate: 0.4029544776119403
Kappa statistic: 0.31377580087540646
Training time: 2.1
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 81.39322916666666
Incorrectly Classified Instances: 29.036458333333332
Correctly Classified Instances: 70.96354166666666
Weighted Precision: 0.7234806687179571
Weighted AreaUnderROC: 0.6795880597014926
Root mean squared error: 0.4760957138854585
Relative absolute error: 65.56855552260649
Root relative squared error: 95.21914277709172
Weighted TruePositiveRate: 0.7096354166666666
Weighted MatthewsCorrelation: 0.37032883944789663
Weighted FMeasure: 0.6989726785133004
Iteration time: 2.1
Weighted AreaUnderPRC: 0.6690644680645936
Mean absolute error: 0.3278427776130325
Coverage of cases: 90.625
Instances selection time: 0.7
Test time: 1.1
Accumulative iteration time: 5.8
Weighted Recall: 0.7096354166666666
Weighted FalsePositiveRate: 0.3640891480099502
Kappa statistic: 0.3518839133806957
Training time: 1.4
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 88.73697916666667
Incorrectly Classified Instances: 28.59375
Correctly Classified Instances: 71.40625
Weighted Precision: 0.6949046134189962
Weighted AreaUnderROC: 0.6673552238805971
Root mean squared error: 0.4687137953888256
Relative absolute error: 68.31879906803132
Root relative squared error: 93.74275907776511
Weighted TruePositiveRate: 0.7140625
Weighted MatthewsCorrelation: 0.33143989542307356
Weighted FMeasure: 0.6849512155510171
Iteration time: 2.4
Weighted AreaUnderPRC: 0.6677404593889964
Mean absolute error: 0.34159399534015666
Coverage of cases: 95.02604166666666
Instances selection time: 0.7
Test time: 1.0
Accumulative iteration time: 8.2
Weighted Recall: 0.7140625
Weighted FalsePositiveRate: 0.41573414179104484
Kappa statistic: 0.3111817153411337
Training time: 1.7
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 81.75781249999999
Incorrectly Classified Instances: 28.802083333333336
Correctly Classified Instances: 71.19791666666667
Weighted Precision: 0.7130063755929312
Weighted AreaUnderROC: 0.7027701492537314
Root mean squared error: 0.46827740751972424
Relative absolute error: 65.4792331451625
Root relative squared error: 93.65548150394484
Weighted TruePositiveRate: 0.7119791666666666
Weighted MatthewsCorrelation: 0.3574071910582076
Weighted FMeasure: 0.7042041345970165
Iteration time: 2.7
Weighted AreaUnderPRC: 0.6886040995725106
Mean absolute error: 0.3273961657258125
Coverage of cases: 92.13541666666667
Instances selection time: 0.7
Test time: 1.1
Accumulative iteration time: 10.9
Weighted Recall: 0.7119791666666666
Weighted FalsePositiveRate: 0.37218215174129354
Kappa statistic: 0.34714374520280644
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 88.21614583333333
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7229838407615332
Weighted AreaUnderROC: 0.6949716417910448
Root mean squared error: 0.467147852801986
Relative absolute error: 66.82262375159505
Root relative squared error: 93.42957056039721
Weighted TruePositiveRate: 0.7135416666666667
Weighted MatthewsCorrelation: 0.3690080334238114
Weighted FMeasure: 0.7035058126107423
Iteration time: 2.9
Weighted AreaUnderPRC: 0.6808678989661765
Mean absolute error: 0.3341131187579752
Coverage of cases: 94.32291666666666
Instances selection time: 0.3
Test time: 1.2
Accumulative iteration time: 13.8
Weighted Recall: 0.7135416666666667
Weighted FalsePositiveRate: 0.3706521144278607
Kappa statistic: 0.3512621089505236
Training time: 2.6
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 82.66927083333334
Incorrectly Classified Instances: 29.348958333333332
Correctly Classified Instances: 70.65104166666666
Weighted Precision: 0.708492995513373
Weighted AreaUnderROC: 0.6908298507462687
Root mean squared error: 0.48999350887352894
Relative absolute error: 67.40987922302446
Root relative squared error: 97.99870177470578
Weighted TruePositiveRate: 0.7065104166666667
Weighted MatthewsCorrelation: 0.3453447006560949
Weighted FMeasure: 0.697356254844764
Iteration time: 3.4
Weighted AreaUnderPRC: 0.6785481829348755
Mean absolute error: 0.3370493961151223
Coverage of cases: 89.45312499999999
Instances selection time: 0.5
Test time: 1.4
Accumulative iteration time: 17.2
Weighted Recall: 0.7065104166666667
Weighted FalsePositiveRate: 0.3816925062189055
Kappa statistic: 0.33347873392548333
Training time: 2.9
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 83.02083333333333
Incorrectly Classified Instances: 28.750000000000007
Correctly Classified Instances: 71.25
Weighted Precision: 0.7145262254820833
Weighted AreaUnderROC: 0.6792820895522389
Root mean squared error: 0.4763696840546096
Relative absolute error: 66.21166675548344
Root relative squared error: 95.27393681092192
Weighted TruePositiveRate: 0.7125000000000001
Weighted MatthewsCorrelation: 0.3650739985131751
Weighted FMeasure: 0.7084135623596071
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6725303912829929
Mean absolute error: 0.3310583337774172
Coverage of cases: 91.25000000000001
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 21.2
Weighted Recall: 0.7125000000000001
Weighted FalsePositiveRate: 0.3580522388059702
Kappa statistic: 0.35865222464435276
Training time: 3.5
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 77.76041666666666
Incorrectly Classified Instances: 28.645833333333336
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.722598978686513
Weighted AreaUnderROC: 0.6938432835820896
Root mean squared error: 0.4798136610454679
Relative absolute error: 65.36496020389235
Root relative squared error: 95.96273220909362
Weighted TruePositiveRate: 0.7135416666666667
Weighted MatthewsCorrelation: 0.3843583514076376
Weighted FMeasure: 0.7138741776059297
Iteration time: 4.5
Weighted AreaUnderPRC: 0.6804122091340854
Mean absolute error: 0.3268248010194617
Coverage of cases: 88.41145833333334
Instances selection time: 0.6
Test time: 1.0
Accumulative iteration time: 25.7
Weighted Recall: 0.7135416666666667
Weighted FalsePositiveRate: 0.3277148009950248
Kappa statistic: 0.37944711108298146
Training time: 3.9
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 82.63020833333331
Incorrectly Classified Instances: 27.239583333333332
Correctly Classified Instances: 72.76041666666666
Weighted Precision: 0.7375264745987289
Weighted AreaUnderROC: 0.7273074626865672
Root mean squared error: 0.4551257622161021
Relative absolute error: 64.39876759955274
Root relative squared error: 91.02515244322042
Weighted TruePositiveRate: 0.7276041666666666
Weighted MatthewsCorrelation: 0.412713181689631
Weighted FMeasure: 0.7256363430141171
Iteration time: 4.4
Weighted AreaUnderPRC: 0.7086678454242594
Mean absolute error: 0.3219938379977637
Coverage of cases: 92.578125
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 30.1
Weighted Recall: 0.7276041666666666
Weighted FalsePositiveRate: 0.31913849502487557
Kappa statistic: 0.4039440662738881
Training time: 3.9
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 85.26041666666666
Incorrectly Classified Instances: 28.229166666666664
Correctly Classified Instances: 71.77083333333333
Weighted Precision: 0.720435299507752
Weighted AreaUnderROC: 0.7050447761194031
Root mean squared error: 0.46511441316027097
Relative absolute error: 66.66386881327625
Root relative squared error: 93.02288263205419
Weighted TruePositiveRate: 0.7177083333333334
Weighted MatthewsCorrelation: 0.3696314862023309
Weighted FMeasure: 0.7084282612685828
Iteration time: 4.6
Weighted AreaUnderPRC: 0.6920463651508755
Mean absolute error: 0.3333193440663813
Coverage of cases: 93.359375
Instances selection time: 0.3
Test time: 1.1
Accumulative iteration time: 34.7
Weighted Recall: 0.7177083333333334
Weighted FalsePositiveRate: 0.37153519900497506
Kappa statistic: 0.3562399812812594
Training time: 4.3
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 80.3515625
Incorrectly Classified Instances: 28.541666666666668
Correctly Classified Instances: 71.45833333333334
Weighted Precision: 0.7193282404376913
Weighted AreaUnderROC: 0.70055223880597
Root mean squared error: 0.4677429431332157
Relative absolute error: 65.92055469649902
Root relative squared error: 93.54858862664312
Weighted TruePositiveRate: 0.7145833333333333
Weighted MatthewsCorrelation: 0.3666199300094914
Weighted FMeasure: 0.7048247990677872
Iteration time: 5.4
Weighted AreaUnderPRC: 0.6883103238673419
Mean absolute error: 0.32960277348249506
Coverage of cases: 91.06770833333333
Instances selection time: 0.6
Test time: 1.1
Accumulative iteration time: 40.1
Weighted Recall: 0.7145833333333333
Weighted FalsePositiveRate: 0.36697736318407953
Kappa statistic: 0.35241403150557465
Training time: 4.8
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 86.73177083333334
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7277170101556594
Weighted AreaUnderROC: 0.7000358208955223
Root mean squared error: 0.46062029580532676
Relative absolute error: 66.6614750975446
Root relative squared error: 92.12405916106535
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.386778348050944
Weighted FMeasure: 0.7166006467731643
Iteration time: 5.4
Weighted AreaUnderPRC: 0.6850622330897899
Mean absolute error: 0.333307375487723
Coverage of cases: 93.46354166666667
Instances selection time: 0.3
Test time: 1.2
Accumulative iteration time: 45.5
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.36436548507462685
Kappa statistic: 0.37370309336493646
Training time: 5.1
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 87.2265625
Incorrectly Classified Instances: 27.734375
Correctly Classified Instances: 72.265625
Weighted Precision: 0.7291057870304942
Weighted AreaUnderROC: 0.7070955223880598
Root mean squared error: 0.45989802192896906
Relative absolute error: 67.33916709461789
Root relative squared error: 91.9796043857938
Weighted TruePositiveRate: 0.7226562500000001
Weighted MatthewsCorrelation: 0.39393259694983934
Weighted FMeasure: 0.7183742102269275
Iteration time: 5.8
Weighted AreaUnderPRC: 0.6890678812386886
Mean absolute error: 0.33669583547308946
Coverage of cases: 94.50520833333334
Instances selection time: 0.7
Test time: 1.1
Accumulative iteration time: 51.3
Weighted Recall: 0.7226562500000001
Weighted FalsePositiveRate: 0.33875774253731344
Kappa statistic: 0.38450840617793947
Training time: 5.1
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 85.48177083333333
Incorrectly Classified Instances: 28.151041666666668
Correctly Classified Instances: 71.84895833333334
Weighted Precision: 0.7178780120762029
Weighted AreaUnderROC: 0.6873253731343284
Root mean squared error: 0.471275855762921
Relative absolute error: 67.16879399242035
Root relative squared error: 94.25517115258421
Weighted TruePositiveRate: 0.7184895833333333
Weighted MatthewsCorrelation: 0.3695638928987336
Weighted FMeasure: 0.7105726393007584
Iteration time: 6.3
Weighted AreaUnderPRC: 0.6805729096615493
Mean absolute error: 0.33584396996210175
Coverage of cases: 91.82291666666667
Instances selection time: 0.3
Test time: 1.0
Accumulative iteration time: 57.6
Weighted Recall: 0.7184895833333333
Weighted FalsePositiveRate: 0.36557615049751246
Kappa statistic: 0.3606274491968193
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 86.71875000000001
Incorrectly Classified Instances: 27.942708333333332
Correctly Classified Instances: 72.05729166666666
Weighted Precision: 0.7226579664721509
Weighted AreaUnderROC: 0.6846179104477612
Root mean squared error: 0.4734994109784652
Relative absolute error: 66.08885608078108
Root relative squared error: 94.69988219569305
Weighted TruePositiveRate: 0.7205729166666667
Weighted MatthewsCorrelation: 0.3773504577803902
Weighted FMeasure: 0.7125459522686834
Iteration time: 6.6
Weighted AreaUnderPRC: 0.6773277997552316
Mean absolute error: 0.33044428040390533
Coverage of cases: 91.09375
Instances selection time: 0.4
Test time: 1.1
Accumulative iteration time: 64.2
Weighted Recall: 0.7205729166666667
Weighted FalsePositiveRate: 0.36411321517412937
Kappa statistic: 0.3657694798672986
Training time: 6.2
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 89.73958333333334
Incorrectly Classified Instances: 26.901041666666664
Correctly Classified Instances: 73.09895833333333
Weighted Precision: 0.7289822618644968
Weighted AreaUnderROC: 0.7093626865671643
Root mean squared error: 0.45635204013600045
Relative absolute error: 65.76937284271852
Root relative squared error: 91.27040802720009
Weighted TruePositiveRate: 0.7309895833333333
Weighted MatthewsCorrelation: 0.39644111031690493
Weighted FMeasure: 0.724818067486771
Iteration time: 6.8
Weighted AreaUnderPRC: 0.6968253425627332
Mean absolute error: 0.3288468642135926
Coverage of cases: 93.80208333333334
Instances selection time: 0.4
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7309895833333333
Weighted FalsePositiveRate: 0.3519507773631841
Kappa statistic: 0.38958872696927843
Training time: 6.4
		
Time end:Sat Oct 07 11.46.24 EEST 2017