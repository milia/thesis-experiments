import matplotlib.pyplot as plt

def getList(inputfile):
	f = open(str(inputfile),'r')
	alist = []
	for line in f:
		alist.append(round(float(line[32:]),4))
	return alist

alist = getList(EntropySamplingForest.res)
alist2 = getList(LeastForest.res)
alist3 = getList(MarginForest.res)
alist4 = getList(VoteEntropyForest.res)
alist5 = getList(KullbackForest.res)
alist6 = getList(RandomSelectionForest.res)

plt.ylabel('Correctly Classified Instances (%)')
plt.xlabel('Iteration')
plt.title('Entropy Sampling - Random Forest - anneal.arff')
#plt.plot(alist, 'bo', alist, 'k')
plt.plot(alist, alist2, alist3, alist4, alist5, alist6)
plt.show()

