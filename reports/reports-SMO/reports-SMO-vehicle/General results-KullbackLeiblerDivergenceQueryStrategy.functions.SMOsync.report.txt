Fri Dec 01 13.16.08 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Fri Dec 01 13.16.08 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 56.861702127659576
Incorrectly Classified Instances: 37.16312056737589
Correctly Classified Instances: 62.83687943262411
Weighted Precision: 0.6256359105205275
Weighted AreaUnderROC: 0.8343256478238386
Root mean squared error: 0.3614197282808473
Relative absolute error: 58.1128802378948
Root relative squared error: 83.46631096535545
Weighted TruePositiveRate: 0.6283687943262412
Weighted MatthewsCorrelation: 0.5016577969281383
Weighted FMeasure: 0.6199519899214285
Iteration time: 106.3
Weighted AreaUnderPRC: 0.6323761190466747
Mean absolute error: 0.2179233008921056
Coverage of cases: 92.26950354609929
Instances selection time: 26.2
Test time: 19.5
Accumulative iteration time: 106.3
Weighted Recall: 0.6283687943262412
Weighted FalsePositiveRate: 0.12520173416126765
Kappa statistic: 0.5042547909369308
Training time: 80.1
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 58.58747044917258
Incorrectly Classified Instances: 37.21040189125295
Correctly Classified Instances: 62.78959810874704
Weighted Precision: 0.6258980559435134
Weighted AreaUnderROC: 0.8435439008534329
Root mean squared error: 0.3549952849097285
Relative absolute error: 58.20213329956912
Root relative squared error: 81.98264932147187
Weighted TruePositiveRate: 0.6278959810874705
Weighted MatthewsCorrelation: 0.5018386780066874
Weighted FMeasure: 0.6185249804752505
Iteration time: 77.5
Weighted AreaUnderPRC: 0.645129875777099
Mean absolute error: 0.21825799987338423
Coverage of cases: 94.11347517730496
Instances selection time: 21.0
Test time: 15.9
Accumulative iteration time: 183.8
Weighted Recall: 0.6278959810874705
Weighted FalsePositiveRate: 0.12492591030700315
Kappa statistic: 0.5038766095910685
Training time: 56.5
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 58.10874704491725
Incorrectly Classified Instances: 36.52482269503547
Correctly Classified Instances: 63.47517730496454
Weighted Precision: 0.6357165178269419
Weighted AreaUnderROC: 0.8483315500967532
Root mean squared error: 0.3535452539635469
Relative absolute error: 57.41455564328051
Root relative squared error: 81.64777901862736
Weighted TruePositiveRate: 0.6347517730496455
Weighted MatthewsCorrelation: 0.5124463753717341
Weighted FMeasure: 0.6213191401461866
Iteration time: 85.6
Weighted AreaUnderPRC: 0.659202295138486
Mean absolute error: 0.2153045836623019
Coverage of cases: 93.7825059101655
Instances selection time: 25.0
Test time: 17.9
Accumulative iteration time: 269.4
Weighted Recall: 0.6347517730496455
Weighted FalsePositiveRate: 0.1224488969889391
Kappa statistic: 0.5131070070301669
Training time: 60.6
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 57.825059101654844
Incorrectly Classified Instances: 36.92671394799054
Correctly Classified Instances: 63.073286052009465
Weighted Precision: 0.6387753870487035
Weighted AreaUnderROC: 0.8530775915141626
Root mean squared error: 0.35538340164184634
Relative absolute error: 57.15829433893437
Root relative squared error: 82.07228104137798
Weighted TruePositiveRate: 0.6307328605200946
Weighted MatthewsCorrelation: 0.5105780839963486
Weighted FMeasure: 0.6159111429960733
Iteration time: 85.2
Weighted AreaUnderPRC: 0.6776251269579172
Mean absolute error: 0.21434360377100387
Coverage of cases: 93.42789598108746
Instances selection time: 20.5
Test time: 17.0
Accumulative iteration time: 354.6
Weighted Recall: 0.6307328605200946
Weighted FalsePositiveRate: 0.12385891645528287
Kappa statistic: 0.5076661411077603
Training time: 64.7
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 56.19976359338061
Incorrectly Classified Instances: 37.28132387706855
Correctly Classified Instances: 62.71867612293145
Weighted Precision: 0.6374045793874739
Weighted AreaUnderROC: 0.8543758507185906
Root mean squared error: 0.3573568444128579
Relative absolute error: 56.714296581563325
Root relative squared error: 82.5280281274075
Weighted TruePositiveRate: 0.6271867612293144
Weighted MatthewsCorrelation: 0.5069105601874055
Weighted FMeasure: 0.6113636503292572
Iteration time: 88.2
Weighted AreaUnderPRC: 0.6820548028015261
Mean absolute error: 0.21267861218086245
Coverage of cases: 92.64775413711584
Instances selection time: 19.5
Test time: 18.3
Accumulative iteration time: 442.8
Weighted Recall: 0.6271867612293144
Weighted FalsePositiveRate: 0.1251225074746906
Kappa statistic: 0.5028621453645691
Training time: 68.7
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 57.015366430260045
Incorrectly Classified Instances: 35.20094562647755
Correctly Classified Instances: 64.79905437352247
Weighted Precision: 0.6732520891122854
Weighted AreaUnderROC: 0.8632320059285192
Root mean squared error: 0.35072357975927465
Relative absolute error: 55.304594334776255
Root relative squared error: 80.99614127406657
Weighted TruePositiveRate: 0.6479905437352247
Weighted MatthewsCorrelation: 0.5403141576744566
Weighted FMeasure: 0.6292325293829677
Iteration time: 79.4
Weighted AreaUnderPRC: 0.7026734928716294
Mean absolute error: 0.20739222875541094
Coverage of cases: 93.16784869976358
Instances selection time: 16.0
Test time: 15.1
Accumulative iteration time: 522.2
Weighted Recall: 0.6479905437352247
Weighted FalsePositiveRate: 0.11782002814237318
Kappa statistic: 0.5308781624221084
Training time: 63.4
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 54.60401891252955
Incorrectly Classified Instances: 33.61702127659574
Correctly Classified Instances: 66.38297872340425
Weighted Precision: 0.6948402168485166
Weighted AreaUnderROC: 0.8703572469803218
Root mean squared error: 0.3447365884339173
Relative absolute error: 52.781491090121314
Root relative squared error: 79.61350485273417
Weighted TruePositiveRate: 0.6638297872340426
Weighted MatthewsCorrelation: 0.5632052182887003
Weighted FMeasure: 0.6436067360857097
Iteration time: 85.9
Weighted AreaUnderPRC: 0.7166141440111453
Mean absolute error: 0.1979305915879549
Coverage of cases: 92.36406619385343
Instances selection time: 17.4
Test time: 16.9
Accumulative iteration time: 608.1
Weighted Recall: 0.6638297872340426
Weighted FalsePositiveRate: 0.11239490033492243
Kappa statistic: 0.5521447056348003
Training time: 68.5
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 53.71749408983451
Incorrectly Classified Instances: 33.02600472813239
Correctly Classified Instances: 66.97399527186761
Weighted Precision: 0.7068618272866056
Weighted AreaUnderROC: 0.8723394700315664
Root mean squared error: 0.34569502699436466
Relative absolute error: 52.16868247740753
Root relative squared error: 79.83484675708453
Weighted TruePositiveRate: 0.6697399527186761
Weighted MatthewsCorrelation: 0.5731488782192643
Weighted FMeasure: 0.6486874086098345
Iteration time: 99.4
Weighted AreaUnderPRC: 0.7212035975580546
Mean absolute error: 0.19563255929027817
Coverage of cases: 92.0567375886525
Instances selection time: 17.9
Test time: 18.1
Accumulative iteration time: 707.5
Weighted Recall: 0.6697399527186761
Weighted FalsePositiveRate: 0.11031875952133477
Kappa statistic: 0.560041514514861
Training time: 81.5
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 53.12056737588652
Incorrectly Classified Instances: 33.073286052009465
Correctly Classified Instances: 66.92671394799054
Weighted Precision: 0.7077041717348201
Weighted AreaUnderROC: 0.8726826456199254
Root mean squared error: 0.34622768082547084
Relative absolute error: 51.8987632854925
Root relative squared error: 79.95785789019416
Weighted TruePositiveRate: 0.6692671394799053
Weighted MatthewsCorrelation: 0.5730323558467365
Weighted FMeasure: 0.6457514538411265
Iteration time: 102.6
Weighted AreaUnderPRC: 0.7214363209957338
Mean absolute error: 0.1946203623205969
Coverage of cases: 91.74940898345153
Instances selection time: 15.6
Test time: 19.3
Accumulative iteration time: 810.1
Weighted Recall: 0.6692671394799053
Weighted FalsePositiveRate: 0.110543667009079
Kappa statistic: 0.5594695469515216
Training time: 87.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 53.374704491725765
Incorrectly Classified Instances: 32.127659574468076
Correctly Classified Instances: 67.87234042553192
Weighted Precision: 0.7185806654402171
Weighted AreaUnderROC: 0.8819665511614639
Root mean squared error: 0.33837560282496615
Relative absolute error: 50.810684644984924
Root relative squared error: 78.14449815127846
Weighted TruePositiveRate: 0.6787234042553192
Weighted MatthewsCorrelation: 0.5853844293401368
Weighted FMeasure: 0.6564660335758934
Iteration time: 98.8
Weighted AreaUnderPRC: 0.7363628947209133
Mean absolute error: 0.19054006741869348
Coverage of cases: 93.00236406619385
Instances selection time: 13.0
Test time: 19.3
Accumulative iteration time: 908.9
Weighted Recall: 0.6787234042553192
Weighted FalsePositiveRate: 0.10735120665423159
Kappa statistic: 0.5721377585579742
Training time: 85.8
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 52.369976359338054
Incorrectly Classified Instances: 30.330969267139483
Correctly Classified Instances: 69.66903073286053
Weighted Precision: 0.7288068509303698
Weighted AreaUnderROC: 0.8862645603725714
Root mean squared error: 0.33222453098910043
Relative absolute error: 49.187514213462784
Root relative squared error: 76.7239689591817
Weighted TruePositiveRate: 0.6966903073286053
Weighted MatthewsCorrelation: 0.6049495245221694
Weighted FMeasure: 0.671913292738451
Iteration time: 95.6
Weighted AreaUnderPRC: 0.7412259128208971
Mean absolute error: 0.18445317830048544
Coverage of cases: 92.86052009456264
Instances selection time: 11.9
Test time: 19.4
Accumulative iteration time: 1004.5
Weighted Recall: 0.6966903073286053
Weighted FalsePositiveRate: 0.10130372077234488
Kappa statistic: 0.596055993215739
Training time: 83.7
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 52.133569739952726
Incorrectly Classified Instances: 29.45626477541371
Correctly Classified Instances: 70.5437352245863
Weighted Precision: 0.734708883592783
Weighted AreaUnderROC: 0.8869368472772686
Root mean squared error: 0.3292005787935967
Relative absolute error: 48.617826363304935
Root relative squared error: 76.02561711354545
Weighted TruePositiveRate: 0.7054373522458628
Weighted MatthewsCorrelation: 0.6150743259297395
Weighted FMeasure: 0.6822313635360839
Iteration time: 108.7
Weighted AreaUnderPRC: 0.747116850091362
Mean absolute error: 0.18231684886239355
Coverage of cases: 92.55319148936171
Instances selection time: 12.9
Test time: 21.5
Accumulative iteration time: 1113.2
Weighted Recall: 0.7054373522458628
Weighted FalsePositiveRate: 0.09841808076234863
Kappa statistic: 0.6076744790688671
Training time: 95.8
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 51.44799054373523
Incorrectly Classified Instances: 29.43262411347518
Correctly Classified Instances: 70.56737588652483
Weighted Precision: 0.7321072783381374
Weighted AreaUnderROC: 0.8853019075651687
Root mean squared error: 0.3290365968357224
Relative absolute error: 48.39464398224287
Root relative squared error: 75.9877471025371
Weighted TruePositiveRate: 0.7056737588652483
Weighted MatthewsCorrelation: 0.6133325845869394
Weighted FMeasure: 0.6803632483327655
Iteration time: 117.4
Weighted AreaUnderPRC: 0.741400227403378
Mean absolute error: 0.18147991493341076
Coverage of cases: 92.03309692671395
Instances selection time: 11.3
Test time: 23.4
Accumulative iteration time: 1230.6
Weighted Recall: 0.7056737588652483
Weighted FalsePositiveRate: 0.09832338904831302
Kappa statistic: 0.6079863324614315
Training time: 106.1
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 52.836879432624116
Incorrectly Classified Instances: 29.078014184397166
Correctly Classified Instances: 70.92198581560282
Weighted Precision: 0.7318192512542534
Weighted AreaUnderROC: 0.8903167906543572
Root mean squared error: 0.3251900032722921
Relative absolute error: 48.73950977147588
Root relative squared error: 75.09941437081325
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6169771814976467
Weighted FMeasure: 0.6852992775979693
Iteration time: 106.0
Weighted AreaUnderPRC: 0.7472245842396211
Mean absolute error: 0.1827731616430345
Coverage of cases: 94.11347517730498
Instances selection time: 8.2
Test time: 18.6
Accumulative iteration time: 1336.6
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09727194755049229
Kappa statistic: 0.6126424756199462
Training time: 97.8
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 54.85224586288416
Incorrectly Classified Instances: 28.06146572104019
Correctly Classified Instances: 71.9385342789598
Weighted Precision: 0.7280649554114965
Weighted AreaUnderROC: 0.8947492673827107
Root mean squared error: 0.31675333395069344
Relative absolute error: 48.95958826808358
Root relative squared error: 73.15104904925771
Weighted TruePositiveRate: 0.7193853427895981
Weighted MatthewsCorrelation: 0.6271102186108172
Weighted FMeasure: 0.7058778487865285
Iteration time: 95.4
Weighted AreaUnderPRC: 0.7534891723477951
Mean absolute error: 0.18359845600531338
Coverage of cases: 95.95744680851064
Instances selection time: 6.0
Test time: 19.5
Accumulative iteration time: 1432.0
Weighted Recall: 0.7193853427895981
Weighted FalsePositiveRate: 0.09401458155089043
Kappa statistic: 0.6260830699994642
Training time: 89.4
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 55.80969267139479
Incorrectly Classified Instances: 27.683215130023637
Correctly Classified Instances: 72.31678486997636
Weighted Precision: 0.7229953629241269
Weighted AreaUnderROC: 0.8959006412907952
Root mean squared error: 0.31288052764599594
Relative absolute error: 49.54330524242236
Root relative squared error: 72.25666274424319
Weighted TruePositiveRate: 0.7231678486997637
Weighted MatthewsCorrelation: 0.6291828284451729
Weighted FMeasure: 0.7131798953677643
Iteration time: 107.5
Weighted AreaUnderPRC: 0.7513232662322266
Mean absolute error: 0.18578739465908384
Coverage of cases: 96.8321513002364
Instances selection time: 5.4
Test time: 20.6
Accumulative iteration time: 1539.5
Weighted Recall: 0.7231678486997637
Weighted FalsePositiveRate: 0.09288820080783913
Kappa statistic: 0.6310336323748222
Training time: 102.1
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 56.06973995271867
Incorrectly Classified Instances: 29.408983451536642
Correctly Classified Instances: 70.59101654846334
Weighted Precision: 0.6980797032987173
Weighted AreaUnderROC: 0.8929797796197494
Root mean squared error: 0.3141150680637165
Relative absolute error: 50.42532444952704
Root relative squared error: 72.54176764124172
Weighted TruePositiveRate: 0.7059101654846336
Weighted MatthewsCorrelation: 0.6031149372030893
Weighted FMeasure: 0.6977032315485545
Iteration time: 103.7
Weighted AreaUnderPRC: 0.7406384398809968
Mean absolute error: 0.1890949666857264
Coverage of cases: 97.25768321513002
Instances selection time: 2.3
Test time: 22.1
Accumulative iteration time: 1643.2
Weighted Recall: 0.7059101654846336
Weighted FalsePositiveRate: 0.09893727170575964
Kappa statistic: 0.6079261902829269
Training time: 101.4
		
Time end:Fri Dec 01 13.16.35 EET 2017