Fri Dec 01 13.17.04 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.17.04 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 54.43262411347518
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6389706069645416
Weighted AreaUnderROC: 0.8220655711381739
Root mean squared error: 0.35869783175158526
Relative absolute error: 56.40290191543334
Root relative squared error: 82.83771588780513
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5205385011555648
Weighted FMeasure: 0.6373853017540922
Iteration time: 67.0
Weighted AreaUnderPRC: 0.5889184742129501
Mean absolute error: 0.21151088218287503
Coverage of cases: 91.725768321513
Instances selection time: 14.0
Test time: 18.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.12050516834056503
Kappa statistic: 0.5238971958437068
Training time: 53.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 54.1371158392435
Incorrectly Classified Instances: 36.170212765957444
Correctly Classified Instances: 63.829787234042556
Weighted Precision: 0.63872876754197
Weighted AreaUnderROC: 0.8446861443510849
Root mean squared error: 0.3522536899539158
Relative absolute error: 55.849191545115566
Root relative squared error: 81.34950508717291
Weighted TruePositiveRate: 0.6382978723404256
Weighted MatthewsCorrelation: 0.5157379314428846
Weighted FMeasure: 0.6264192275151127
Iteration time: 68.0
Weighted AreaUnderPRC: 0.6324573958005879
Mean absolute error: 0.20943446829418338
Coverage of cases: 94.32624113475177
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 135.0
Weighted Recall: 0.6382978723404256
Weighted FalsePositiveRate: 0.12302186508815459
Kappa statistic: 0.5171376984600693
Training time: 54.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 49.527186761229316
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6571234583900305
Weighted AreaUnderROC: 0.8587890314995348
Root mean squared error: 0.33577942074912615
Relative absolute error: 49.454217499119345
Root relative squared error: 77.54493558313784
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5448976453157246
Weighted FMeasure: 0.6470315764794761
Iteration time: 71.0
Weighted AreaUnderPRC: 0.6461932202916443
Mean absolute error: 0.18545331562169753
Coverage of cases: 93.85342789598108
Instances selection time: 15.0
Test time: 19.0
Accumulative iteration time: 206.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.114973965378045
Kappa statistic: 0.5486670198398782
Training time: 56.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 47.45862884160756
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6829617104668433
Weighted AreaUnderROC: 0.86924297489612
Root mean squared error: 0.33055029263263974
Relative absolute error: 48.06596563907504
Root relative squared error: 76.33732017286566
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5756749543017203
Weighted FMeasure: 0.6781936839597316
Iteration time: 116.0
Weighted AreaUnderPRC: 0.662143337641068
Mean absolute error: 0.1802473711465314
Coverage of cases: 94.79905437352245
Instances selection time: 36.0
Test time: 49.0
Accumulative iteration time: 322.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.1075055989034456
Kappa statistic: 0.5772523866348449
Training time: 80.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 47.990543735224584
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.6786955747762653
Weighted AreaUnderROC: 0.8670827443610776
Root mean squared error: 0.3279457586612353
Relative absolute error: 48.267624647992726
Root relative squared error: 75.7358288170641
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.571698644245586
Weighted FMeasure: 0.6779969647806643
Iteration time: 125.0
Weighted AreaUnderPRC: 0.6588623790673361
Mean absolute error: 0.18100359242997272
Coverage of cases: 93.61702127659575
Instances selection time: 30.0
Test time: 40.0
Accumulative iteration time: 447.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.10815328651836152
Kappa statistic: 0.5742436216691768
Training time: 95.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 46.808510638297875
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7145454583157538
Weighted AreaUnderROC: 0.8814020808238664
Root mean squared error: 0.3152843528340479
Relative absolute error: 45.56722590725355
Root relative squared error: 72.8118023920058
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6191742662318134
Weighted FMeasure: 0.7128414881093396
Iteration time: 131.0
Weighted AreaUnderPRC: 0.6952508584361673
Mean absolute error: 0.1708770971522008
Coverage of cases: 94.08983451536643
Instances selection time: 31.0
Test time: 38.0
Accumulative iteration time: 578.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09637169771098449
Kappa statistic: 0.6214878005130346
Training time: 100.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 44.26713947990544
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6962905893076873
Weighted AreaUnderROC: 0.8789423718145792
Root mean squared error: 0.3175003863586002
Relative absolute error: 43.856085490179986
Root relative squared error: 73.32357341277921
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.594204896855832
Weighted FMeasure: 0.6924802131117707
Iteration time: 79.0
Weighted AreaUnderPRC: 0.6939449361817732
Mean absolute error: 0.16446032058817495
Coverage of cases: 94.32624113475177
Instances selection time: 19.0
Test time: 27.0
Accumulative iteration time: 657.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10277830305720861
Kappa statistic: 0.5961994540817088
Training time: 60.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 44.26713947990544
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7178508746651574
Weighted AreaUnderROC: 0.8928163879021672
Root mean squared error: 0.3058170294117689
Relative absolute error: 42.141824643523655
Root relative squared error: 70.6254177014626
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.626403545095217
Weighted FMeasure: 0.7117739016938059
Iteration time: 83.0
Weighted AreaUnderPRC: 0.724481913409092
Mean absolute error: 0.1580318424132137
Coverage of cases: 94.32624113475177
Instances selection time: 19.0
Test time: 29.0
Accumulative iteration time: 740.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09424916373429855
Kappa statistic: 0.6308845465393795
Training time: 64.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 48.87706855791962
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6895770940928913
Weighted AreaUnderROC: 0.8887032881041546
Root mean squared error: 0.3101389778045426
Relative absolute error: 45.687097337207604
Root relative squared error: 71.62352892865921
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5929414270050193
Weighted FMeasure: 0.6901066116635082
Iteration time: 80.0
Weighted AreaUnderPRC: 0.7128200033634641
Mean absolute error: 0.1713266150145285
Coverage of cases: 95.98108747044917
Instances selection time: 15.0
Test time: 24.0
Accumulative iteration time: 820.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10218438236398947
Kappa statistic: 0.599416883533298
Training time: 65.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 48.58156028368794
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.7019379265494782
Weighted AreaUnderROC: 0.8976676167953969
Root mean squared error: 0.30165261053884934
Relative absolute error: 43.99681887437453
Root relative squared error: 69.6636863585432
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6092519057123608
Weighted FMeasure: 0.7040236117121976
Iteration time: 94.0
Weighted AreaUnderPRC: 0.7356624014696311
Mean absolute error: 0.1649880707789045
Coverage of cases: 96.21749408983452
Instances selection time: 23.0
Test time: 23.0
Accumulative iteration time: 914.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09789243510165375
Kappa statistic: 0.6152768044849334
Training time: 71.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 47.5177304964539
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7062212970461074
Weighted AreaUnderROC: 0.8916630514323399
Root mean squared error: 0.3046519687798176
Relative absolute error: 44.06986710976887
Root relative squared error: 70.35635847367087
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6153630728221385
Weighted FMeasure: 0.708247403387902
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7157016379283268
Mean absolute error: 0.1652620016616333
Coverage of cases: 95.98108747044917
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 999.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09631141922437637
Kappa statistic: 0.621575278637194
Training time: 71.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 48.64066193853428
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6890975758523556
Weighted AreaUnderROC: 0.8902192075378793
Root mean squared error: 0.3055396201060404
Relative absolute error: 45.16763954154638
Root relative squared error: 70.56135276652738
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5951590115052395
Weighted FMeasure: 0.6924697622093977
Iteration time: 82.0
Weighted AreaUnderPRC: 0.7138965062669913
Mean absolute error: 0.16937864828079893
Coverage of cases: 96.21749408983452
Instances selection time: 12.0
Test time: 22.0
Accumulative iteration time: 1081.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.10094829734070118
Kappa statistic: 0.6027043950146103
Training time: 70.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 48.58156028368794
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.6965953431607708
Weighted AreaUnderROC: 0.8912329566689984
Root mean squared error: 0.30452090519041075
Relative absolute error: 45.18258285682568
Root relative squared error: 70.32609063422086
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6048085763760207
Weighted FMeasure: 0.7000690384288718
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7172393628072726
Mean absolute error: 0.1694346857130963
Coverage of cases: 96.6903073286052
Instances selection time: 12.0
Test time: 22.0
Accumulative iteration time: 1171.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09851649503817697
Kappa statistic: 0.6122100649931429
Training time: 78.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 48.10874704491726
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7051472754896961
Weighted AreaUnderROC: 0.8902761738715105
Root mean squared error: 0.30606587015331677
Relative absolute error: 44.893170204883425
Root relative squared error: 70.6828850091098
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6148917569159431
Weighted FMeasure: 0.7079079670011307
Iteration time: 89.0
Weighted AreaUnderPRC: 0.7085297137253367
Mean absolute error: 0.16834938826831283
Coverage of cases: 95.74468085106383
Instances selection time: 11.0
Test time: 23.0
Accumulative iteration time: 1260.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09631141922437637
Kappa statistic: 0.6215837421163279
Training time: 78.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 48.463356973995275
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7079817932850614
Weighted AreaUnderROC: 0.8909805448678951
Root mean squared error: 0.30481819410714844
Relative absolute error: 44.92674449736594
Root relative squared error: 70.39474656866311
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6182684925071182
Weighted FMeasure: 0.7097058417717628
Iteration time: 89.0
Weighted AreaUnderPRC: 0.7117193542840777
Mean absolute error: 0.16847529186512225
Coverage of cases: 95.74468085106383
Instances selection time: 10.0
Test time: 23.0
Accumulative iteration time: 1349.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.095452756906332
Kappa statistic: 0.624740008498647
Training time: 79.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 48.58156028368794
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6944087389487078
Weighted AreaUnderROC: 0.8904311810768007
Root mean squared error: 0.3056277465951596
Relative absolute error: 45.05641320041659
Root relative squared error: 70.58170470741366
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6017784186288492
Weighted FMeasure: 0.6982576804805248
Iteration time: 93.0
Weighted AreaUnderPRC: 0.7117476256520954
Mean absolute error: 0.16896154950156222
Coverage of cases: 95.74468085106383
Instances selection time: 8.0
Test time: 28.0
Accumulative iteration time: 1442.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09930700297681565
Kappa statistic: 0.6090456456277392
Training time: 85.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 48.463356973995275
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6944436343125296
Weighted AreaUnderROC: 0.8903391213447044
Root mean squared error: 0.3057627328590397
Relative absolute error: 45.05001896733859
Root relative squared error: 70.61287844972888
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6017427306183046
Weighted FMeasure: 0.6983094726314535
Iteration time: 109.0
Weighted AreaUnderPRC: 0.7103072979085854
Mean absolute error: 0.1689375711275197
Coverage of cases: 95.74468085106383
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 1551.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.0994052965995254
Kappa statistic: 0.609016503421441
Training time: 106.0
		
Time end:Fri Dec 01 13.17.06 EET 2017