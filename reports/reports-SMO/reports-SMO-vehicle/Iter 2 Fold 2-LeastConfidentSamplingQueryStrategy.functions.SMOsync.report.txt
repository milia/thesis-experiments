Fri Dec 01 13.16.43 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.16.43 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 57.74231678486998
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6491787363878142
Weighted AreaUnderROC: 0.8388661240533225
Root mean squared error: 0.3653449820177987
Relative absolute error: 57.2870373750882
Root relative squared error: 84.37280948602202
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5215210375571279
Weighted FMeasure: 0.616310535581298
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6640615663660314
Mean absolute error: 0.21482639015658073
Coverage of cases: 91.48936170212765
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.12094014087664468
Kappa statistic: 0.5236308853471358
Training time: 54.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 50.53191489361702
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6952472557888177
Weighted AreaUnderROC: 0.8721664282781507
Root mean squared error: 0.3385882673243798
Relative absolute error: 48.704591579054835
Root relative squared error: 78.19361091367188
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5889742988860714
Weighted FMeasure: 0.6807706180334993
Iteration time: 69.0
Weighted AreaUnderPRC: 0.707563701570597
Mean absolute error: 0.18264221842145562
Coverage of cases: 91.96217494089835
Instances selection time: 14.0
Test time: 18.0
Accumulative iteration time: 135.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10408533708897702
Kappa statistic: 0.5898992460231637
Training time: 55.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 51.004728132387704
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6747503748420676
Weighted AreaUnderROC: 0.8780450898983793
Root mean squared error: 0.3279771650982842
Relative absolute error: 48.839033390835795
Root relative squared error: 75.74308182301789
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5763423162164883
Weighted FMeasure: 0.6765420914193059
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7130816487348054
Mean absolute error: 0.18314637521563423
Coverage of cases: 94.08983451536643
Instances selection time: 15.0
Test time: 19.0
Accumulative iteration time: 206.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10550126929202516
Kappa statistic: 0.5837297031326882
Training time: 56.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 51.35933806146572
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7141775997419801
Weighted AreaUnderROC: 0.8838853270548189
Root mean squared error: 0.3195512441496368
Relative absolute error: 47.34297767411717
Root relative squared error: 73.79719873186906
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6185385718093318
Weighted FMeasure: 0.7027188690818394
Iteration time: 72.0
Weighted AreaUnderPRC: 0.7223393101485335
Mean absolute error: 0.1775361662779394
Coverage of cases: 93.3806146572104
Instances selection time: 15.0
Test time: 20.0
Accumulative iteration time: 278.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09587886526954954
Kappa statistic: 0.621566814779471
Training time: 57.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 49.290780141843975
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7200090030261044
Weighted AreaUnderROC: 0.889206078849014
Root mean squared error: 0.31788623762730384
Relative absolute error: 46.26544111740185
Root relative squared error: 73.41268194632049
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6297284726278356
Weighted FMeasure: 0.7138262945502262
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7178155890195903
Mean absolute error: 0.17349540419025694
Coverage of cases: 94.56264775413712
Instances selection time: 15.0
Test time: 21.0
Accumulative iteration time: 353.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09299732692541511
Kappa statistic: 0.6341212437551265
Training time: 60.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 48.58156028368794
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7104207826803794
Weighted AreaUnderROC: 0.8976088581870605
Root mean squared error: 0.31292304481726707
Relative absolute error: 45.35684453373406
Root relative squared error: 72.26648166435459
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6219253386241622
Weighted FMeasure: 0.7136545495565472
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7395566957154759
Mean absolute error: 0.17008816700150273
Coverage of cases: 95.50827423167848
Instances selection time: 16.0
Test time: 21.0
Accumulative iteration time: 432.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09420879599024615
Kappa statistic: 0.627960018783122
Training time: 63.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 50.53191489361702
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7120620328126814
Weighted AreaUnderROC: 0.8985536990838425
Root mean squared error: 0.30826082662230553
Relative absolute error: 45.883737466830155
Root relative squared error: 71.18978849240187
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6247037012043247
Weighted FMeasure: 0.7163390741654438
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7391258435492427
Mean absolute error: 0.1720640155006131
Coverage of cases: 96.6903073286052
Instances selection time: 13.0
Test time: 19.0
Accumulative iteration time: 508.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09349431832381072
Kappa statistic: 0.6311266471885995
Training time: 63.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 53.3096926713948
Incorrectly Classified Instances: 26.24113475177305
Correctly Classified Instances: 73.75886524822695
Weighted Precision: 0.7292196261057468
Weighted AreaUnderROC: 0.9061096285502711
Root mean squared error: 0.3013186934896822
Relative absolute error: 45.816866958888575
Root relative squared error: 69.58657151925375
Weighted TruePositiveRate: 0.7375886524822695
Weighted MatthewsCorrelation: 0.6449370630473099
Weighted FMeasure: 0.7309446652695271
Iteration time: 123.0
Weighted AreaUnderPRC: 0.7551853485449083
Mean absolute error: 0.17181325109583215
Coverage of cases: 97.16312056737588
Instances selection time: 24.0
Test time: 35.0
Accumulative iteration time: 631.0
Weighted Recall: 0.7375886524822695
Weighted FalsePositiveRate: 0.08878140993528266
Kappa statistic: 0.6499884455112674
Training time: 99.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 51.06382978723404
Incorrectly Classified Instances: 25.29550827423168
Correctly Classified Instances: 74.70449172576832
Weighted Precision: 0.7381255553701443
Weighted AreaUnderROC: 0.9101878866746179
Root mean squared error: 0.29760385429395037
Relative absolute error: 44.02432663121163
Root relative squared error: 68.72866615539297
Weighted TruePositiveRate: 0.7470449172576832
Weighted MatthewsCorrelation: 0.6574405321221571
Weighted FMeasure: 0.7383395615320546
Iteration time: 87.0
Weighted AreaUnderPRC: 0.7680666495925784
Mean absolute error: 0.16509122486704358
Coverage of cases: 97.87234042553192
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 718.0
Weighted Recall: 0.7470449172576832
Weighted FalsePositiveRate: 0.08519606444658484
Kappa statistic: 0.6627070773312268
Training time: 73.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 51.06382978723404
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7152712346283114
Weighted AreaUnderROC: 0.9038200856517952
Root mean squared error: 0.3019455371712053
Relative absolute error: 45.174940367608436
Root relative squared error: 69.73133486656062
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6282745294677227
Weighted FMeasure: 0.7180040210015507
Iteration time: 83.0
Weighted AreaUnderPRC: 0.7506011759323734
Mean absolute error: 0.16940602637853164
Coverage of cases: 97.63593380614657
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 801.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09249934724695495
Kappa statistic: 0.6343311945270407
Training time: 69.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 51.35933806146572
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.687328293808082
Weighted AreaUnderROC: 0.8993409121691701
Root mean squared error: 0.30567239118245243
Relative absolute error: 46.22619312756695
Root relative squared error: 70.59201493321021
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5927418520310316
Weighted FMeasure: 0.6913520974691186
Iteration time: 83.0
Weighted AreaUnderPRC: 0.7369068970901409
Mean absolute error: 0.1733482242283761
Coverage of cases: 97.87234042553192
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 884.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10117918278638537
Kappa statistic: 0.5996646545942321
Training time: 71.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 53.486997635933804
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6888331950735337
Weighted AreaUnderROC: 0.8985835288390565
Root mean squared error: 0.3057150156859105
Relative absolute error: 47.560142212487655
Root relative squared error: 70.60185864062844
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5954138236280873
Weighted FMeasure: 0.6941968901581085
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7353582523441474
Mean absolute error: 0.1783505332968287
Coverage of cases: 97.63593380614657
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 969.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.1006311531220654
Kappa statistic: 0.6027784195502955
Training time: 73.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 54.9645390070922
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.6893215702863538
Weighted AreaUnderROC: 0.8976577181611587
Root mean squared error: 0.30942447647593446
Relative absolute error: 48.665061381237535
Root relative squared error: 71.45852191489593
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.5959014143679022
Weighted FMeasure: 0.69426847275993
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7353289674532077
Mean absolute error: 0.18249398017964075
Coverage of cases: 97.87234042553192
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 1053.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.10016982425182067
Kappa statistic: 0.602905677246312
Training time: 74.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 53.36879432624114
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.6978500072172679
Weighted AreaUnderROC: 0.8985719276538037
Root mean squared error: 0.3063845368620826
Relative absolute error: 47.410616091973075
Root relative squared error: 70.7564779331449
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6056815216841118
Weighted FMeasure: 0.7025201370830777
Iteration time: 86.0
Weighted AreaUnderPRC: 0.7378775837184407
Mean absolute error: 0.17778981034489902
Coverage of cases: 97.63593380614657
Instances selection time: 9.0
Test time: 18.0
Accumulative iteration time: 1139.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09836579882165665
Kappa statistic: 0.6122042842448907
Training time: 77.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 53.36879432624114
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6892850775427385
Weighted AreaUnderROC: 0.8972925692756339
Root mean squared error: 0.30694850078554026
Relative absolute error: 47.413461403245115
Root relative squared error: 70.8867198223535
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5933965859351005
Weighted FMeasure: 0.693685434732475
Iteration time: 102.0
Weighted AreaUnderPRC: 0.7395636270009038
Mean absolute error: 0.17780048026216916
Coverage of cases: 97.87234042553192
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 1241.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.1016864026855293
Kappa statistic: 0.5995243881529413
Training time: 94.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 53.191489361702125
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6867019034772175
Weighted AreaUnderROC: 0.8975548546411204
Root mean squared error: 0.3054849082312792
Relative absolute error: 47.27042768830234
Root relative squared error: 70.54871760027888
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.592478303598757
Weighted FMeasure: 0.6920776791487518
Iteration time: 91.0
Weighted AreaUnderPRC: 0.7370820163107107
Mean absolute error: 0.17726410383113378
Coverage of cases: 97.87234042553192
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 1332.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10119493457198049
Kappa statistic: 0.5996944858420268
Training time: 84.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 54.43262411347518
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.6973680109717121
Weighted AreaUnderROC: 0.8973496914528649
Root mean squared error: 0.3065927943844562
Relative absolute error: 48.21050217349528
Root relative squared error: 70.80457294778616
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6033581668875666
Weighted FMeasure: 0.7015467688930414
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7372355271133166
Mean absolute error: 0.1807893831506073
Coverage of cases: 98.3451536643026
Instances selection time: 3.0
Test time: 20.0
Accumulative iteration time: 1422.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.0991641826530929
Kappa statistic: 0.609039817534026
Training time: 87.0
		
Time end:Fri Dec 01 13.16.46 EET 2017