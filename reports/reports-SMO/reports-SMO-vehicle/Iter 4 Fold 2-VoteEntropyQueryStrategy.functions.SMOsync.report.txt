Fri Dec 01 13.18.01 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Fri Dec 01 13.18.01 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 51.6548463356974
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6520586585397472
Weighted AreaUnderROC: 0.8591388992090192
Root mean squared error: 0.35175941678783296
Relative absolute error: 52.896554349349366
Root relative squared error: 81.23535758897646
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.53548850491252
Weighted FMeasure: 0.6461332926848291
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6898498190077923
Mean absolute error: 0.1983620788100601
Coverage of cases: 92.67139479905437
Instances selection time: 16.0
Test time: 18.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11745067042940381
Kappa statistic: 0.5360180873925502
Training time: 50.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 54.255319148936174
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6463625620848384
Weighted AreaUnderROC: 0.8654129426401659
Root mean squared error: 0.34878010689896183
Relative absolute error: 53.4955335718436
Root relative squared error: 80.54731544244083
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5312045410414841
Weighted FMeasure: 0.6392034573314657
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6969582632988705
Mean absolute error: 0.20060825089441353
Coverage of cases: 94.08983451536643
Instances selection time: 14.0
Test time: 16.0
Accumulative iteration time: 129.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.11765141663178395
Kappa statistic: 0.5330707956681285
Training time: 49.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 54.37352245862884
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.6284734072308151
Weighted AreaUnderROC: 0.8634953339080258
Root mean squared error: 0.3533806791440093
Relative absolute error: 54.7772495596148
Root relative squared error: 81.60977209208262
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.5089946254837725
Weighted FMeasure: 0.6202203631350642
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6913380278566064
Mean absolute error: 0.20541468584855552
Coverage of cases: 93.14420803782505
Instances selection time: 13.0
Test time: 16.0
Accumulative iteration time: 192.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.12290447879183439
Kappa statistic: 0.5109024042729369
Training time: 50.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 53.54609929078014
Incorrectly Classified Instances: 36.643026004728135
Correctly Classified Instances: 63.356973995271865
Weighted Precision: 0.627073263475488
Weighted AreaUnderROC: 0.8627722121263425
Root mean squared error: 0.35683122799088635
Relative absolute error: 54.71941508677006
Root relative squared error: 82.4066422143212
Weighted TruePositiveRate: 0.6335697399527187
Weighted MatthewsCorrelation: 0.508471390688167
Weighted FMeasure: 0.6188118874058818
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6900165328259333
Mean absolute error: 0.20519780657538775
Coverage of cases: 92.67139479905437
Instances selection time: 12.0
Test time: 17.0
Accumulative iteration time: 256.0
Weighted Recall: 0.6335697399527187
Weighted FalsePositiveRate: 0.12304729911555715
Kappa statistic: 0.5108878096815344
Training time: 52.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 52.836879432624116
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.6008431111206971
Weighted AreaUnderROC: 0.8597701802899692
Root mean squared error: 0.3618556211808208
Relative absolute error: 55.10231238079092
Root relative squared error: 83.56697611861047
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.4759495116293942
Weighted FMeasure: 0.5927811212144891
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6883003164768015
Mean absolute error: 0.20663367142796596
Coverage of cases: 92.19858156028369
Instances selection time: 12.0
Test time: 17.0
Accumulative iteration time: 322.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.1309143633658424
Kappa statistic: 0.47923506237688773
Training time: 54.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 56.32387706855792
Incorrectly Classified Instances: 37.11583924349882
Correctly Classified Instances: 62.88416075650118
Weighted Precision: 0.6439727091662126
Weighted AreaUnderROC: 0.8734926156789673
Root mean squared error: 0.35198644905018167
Relative absolute error: 55.262796539726025
Root relative squared error: 81.28778844408916
Weighted TruePositiveRate: 0.6288416075650118
Weighted MatthewsCorrelation: 0.5094886828216959
Weighted FMeasure: 0.6032865022616701
Iteration time: 64.0
Weighted AreaUnderPRC: 0.7098913395577611
Mean absolute error: 0.2072354870239726
Coverage of cases: 93.3806146572104
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 386.0
Weighted Recall: 0.6288416075650118
Weighted FalsePositiveRate: 0.12432828688768008
Kappa statistic: 0.5047096990714846
Training time: 54.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 54.846335697399525
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6908863127489704
Weighted AreaUnderROC: 0.8777496586663036
Root mean squared error: 0.3485459235469086
Relative absolute error: 53.09903869493994
Root relative squared error: 80.49323311390177
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5477989808303398
Weighted FMeasure: 0.6251401888674878
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7218170723888264
Mean absolute error: 0.19912139510602478
Coverage of cases: 91.25295508274232
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 457.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.11715480128120997
Kappa statistic: 0.5330255178535466
Training time: 61.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 55.73286052009456
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6948624201524456
Weighted AreaUnderROC: 0.8815733432643502
Root mean squared error: 0.34935335830675557
Relative absolute error: 53.54974195392473
Root relative squared error: 80.67970218428205
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.542938684561985
Weighted FMeasure: 0.6204084825909854
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7312003332517825
Mean absolute error: 0.20081153232721774
Coverage of cases: 93.61702127659575
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 526.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.11973866412814067
Kappa statistic: 0.5235136143230138
Training time: 61.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 55.141843971631204
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.7245067769738948
Weighted AreaUnderROC: 0.8931125760679283
Root mean squared error: 0.33640146506358826
Relative absolute error: 50.872607605330806
Root relative squared error: 77.68859056409887
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5932437007902325
Weighted FMeasure: 0.6617832029195707
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7579374629494657
Mean absolute error: 0.1907722785199905
Coverage of cases: 92.19858156028369
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 597.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10661853771780498
Kappa statistic: 0.5771893182157243
Training time: 64.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 51.89125295508274
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7991824639838344
Weighted AreaUnderROC: 0.8963203248284721
Root mean squared error: 0.3304037968841888
Relative absolute error: 47.310815326986834
Root relative squared error: 76.30348842894435
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6556616760773286
Weighted FMeasure: 0.699825497799183
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7610930124762442
Mean absolute error: 0.1774155574762006
Coverage of cases: 92.90780141843972
Instances selection time: 7.0
Test time: 16.0
Accumulative iteration time: 671.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09310523681663027
Kappa statistic: 0.631156887441403
Training time: 67.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 50.709219858156025
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7968448468724307
Weighted AreaUnderROC: 0.8986799735966283
Root mean squared error: 0.3307325233705483
Relative absolute error: 46.63237324621102
Root relative squared error: 76.37940455910011
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6526649457949778
Weighted FMeasure: 0.6972511735957723
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7615527484653255
Mean absolute error: 0.17487139967329132
Coverage of cases: 92.43498817966903
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 748.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09382759037586325
Kappa statistic: 0.6280210157618215
Training time: 71.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7838653575985068
Weighted AreaUnderROC: 0.8879739723090534
Root mean squared error: 0.3296845949665207
Relative absolute error: 46.365446702134705
Root relative squared error: 76.13739586063738
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6489738987753852
Weighted FMeasure: 0.6996837730793538
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7533286733072238
Mean absolute error: 0.17387042513300513
Coverage of cases: 91.25295508274232
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 827.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09280248005570343
Kappa statistic: 0.631467250469127
Training time: 73.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 47.75413711583924
Incorrectly Classified Instances: 26.713947990543737
Correctly Classified Instances: 73.28605200945627
Weighted Precision: 0.8054248112507413
Weighted AreaUnderROC: 0.8889746434792054
Root mean squared error: 0.3281541966550836
Relative absolute error: 45.34372383825399
Root relative squared error: 75.78396550980716
Weighted TruePositiveRate: 0.7328605200945626
Weighted MatthewsCorrelation: 0.6640587290276425
Weighted FMeasure: 0.7052594417108604
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7502485465760045
Mean absolute error: 0.17003896439345248
Coverage of cases: 90.0709219858156
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 917.0
Weighted Recall: 0.7328605200945626
Weighted FalsePositiveRate: 0.08926302559590477
Kappa statistic: 0.6442838644380608
Training time: 84.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 52.42316784869976
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.766643523312988
Weighted AreaUnderROC: 0.8800764738415003
Root mean squared error: 0.3331106150962329
Relative absolute error: 48.121046892168245
Root relative squared error: 76.92860131829276
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6249123510478422
Weighted FMeasure: 0.6769814180784941
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7248282422949403
Mean absolute error: 0.1804539258456309
Coverage of cases: 93.85342789598108
Instances selection time: 5.0
Test time: 17.0
Accumulative iteration time: 998.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09798087627143681
Kappa statistic: 0.6096914857202388
Training time: 76.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 54.37352245862884
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7358940265718643
Weighted AreaUnderROC: 0.8912637412778038
Root mean squared error: 0.3200585912095544
Relative absolute error: 48.295815692294845
Root relative squared error: 73.91436551651545
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6166165360928393
Weighted FMeasure: 0.6845681198975877
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7387671453820095
Mean absolute error: 0.18110930884610568
Coverage of cases: 97.16312056737588
Instances selection time: 2.0
Test time: 18.0
Accumulative iteration time: 1077.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09803327886524738
Kappa statistic: 0.6096537250786989
Training time: 77.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.146572104018915
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.7099682165195091
Weighted AreaUnderROC: 0.8901487272330584
Root mean squared error: 0.3169766826582711
Relative absolute error: 49.80871714412444
Root relative squared error: 73.20262922383496
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.6033874952018179
Weighted FMeasure: 0.6850978877478313
Iteration time: 78.0
Weighted AreaUnderPRC: 0.7317215659303741
Mean absolute error: 0.18678268929046662
Coverage of cases: 97.87234042553192
Instances selection time: 2.0
Test time: 17.0
Accumulative iteration time: 1155.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.09982526944565322
Kappa statistic: 0.6032515241519463
Training time: 76.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 56.442080378250594
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.6922481326490703
Weighted AreaUnderROC: 0.8894932110795151
Root mean squared error: 0.31399332268686736
Relative absolute error: 50.02958114845947
Root relative squared error: 72.51365175080316
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5926456193639634
Weighted FMeasure: 0.6894800511187547
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7373935295359577
Mean absolute error: 0.18761092930672302
Coverage of cases: 98.10874704491725
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 1236.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10177583213537693
Kappa statistic: 0.596752811499218
Training time: 77.0
		
Time end:Fri Dec 01 13.18.03 EET 2017