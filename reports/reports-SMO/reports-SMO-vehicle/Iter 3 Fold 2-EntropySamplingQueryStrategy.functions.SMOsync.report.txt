Fri Dec 01 13.15.52 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.15.52 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 62.29314420803782
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6414864940139029
Weighted AreaUnderROC: 0.8560289901748671
Root mean squared error: 0.34327260615901006
Relative absolute error: 57.266594335548
Root relative squared error: 79.27541262853154
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5307559164224604
Weighted FMeasure: 0.642817189222346
Iteration time: 137.0
Weighted AreaUnderPRC: 0.663408958451505
Mean absolute error: 0.214749728758305
Coverage of cases: 95.27186761229315
Instances selection time: 24.0
Test time: 26.0
Accumulative iteration time: 137.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11630128620019076
Kappa statistic: 0.5368647634085848
Training time: 113.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 51.06382978723404
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6764015508152648
Weighted AreaUnderROC: 0.8783003048317825
Root mean squared error: 0.3345157082122787
Relative absolute error: 49.64240746305469
Root relative squared error: 77.25309367380699
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5647042169852627
Weighted FMeasure: 0.6678103726291432
Iteration time: 137.0
Weighted AreaUnderPRC: 0.7042554092849407
Mean absolute error: 0.1861590279864551
Coverage of cases: 94.32624113475177
Instances selection time: 33.0
Test time: 42.0
Accumulative iteration time: 274.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.11028506071573126
Kappa statistic: 0.5647953477969133
Training time: 104.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 49.054373522458626
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6707901384361652
Weighted AreaUnderROC: 0.8839792252429319
Root mean squared error: 0.33166883068146935
Relative absolute error: 48.6595134030541
Root relative squared error: 76.5956354703019
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.551553174814053
Weighted FMeasure: 0.6582213970816143
Iteration time: 149.0
Weighted AreaUnderPRC: 0.7148882063634194
Mean absolute error: 0.18247317526145287
Coverage of cases: 94.79905437352245
Instances selection time: 41.0
Test time: 44.0
Accumulative iteration time: 423.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11456262052035804
Kappa statistic: 0.5489093553078042
Training time: 108.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 53.54609929078014
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.674681823855043
Weighted AreaUnderROC: 0.8909077896570127
Root mean squared error: 0.3190826554580044
Relative absolute error: 50.10945945797718
Root relative squared error: 73.68898280896778
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5660516272166182
Weighted FMeasure: 0.6679876935993133
Iteration time: 142.0
Weighted AreaUnderPRC: 0.7259957310198317
Mean absolute error: 0.18791047296741442
Coverage of cases: 97.63593380614657
Instances selection time: 31.0
Test time: 42.0
Accumulative iteration time: 565.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.10920754780176087
Kappa statistic: 0.5681808020744847
Training time: 111.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 51.89125295508274
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6953626902095696
Weighted AreaUnderROC: 0.8943893922748597
Root mean squared error: 0.3140165131269891
Relative absolute error: 49.024317106057744
Root relative squared error: 72.51900735354192
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.590064438901613
Weighted FMeasure: 0.6898011450443061
Iteration time: 149.0
Weighted AreaUnderPRC: 0.7256302101311191
Mean absolute error: 0.18384118914771655
Coverage of cases: 98.10874704491725
Instances selection time: 35.0
Test time: 46.0
Accumulative iteration time: 714.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10364385298798602
Kappa statistic: 0.5902201290668733
Training time: 114.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 51.6548463356974
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6898425373036331
Weighted AreaUnderROC: 0.8923370871714101
Root mean squared error: 0.3160711737040626
Relative absolute error: 48.95736069155485
Root relative squared error: 72.99351088844861
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5811256434011287
Weighted FMeasure: 0.6844087199843014
Iteration time: 150.0
Weighted AreaUnderPRC: 0.7173594865107915
Mean absolute error: 0.1835901025933307
Coverage of cases: 97.87234042553192
Instances selection time: 31.0
Test time: 40.0
Accumulative iteration time: 864.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10630238177923379
Kappa statistic: 0.5806824280007155
Training time: 119.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 48.817966903073284
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.7129688274749825
Weighted AreaUnderROC: 0.8984131027934352
Root mean squared error: 0.31469139365832943
Relative absolute error: 46.389164039367934
Root relative squared error: 72.674864336118
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.6085867170564927
Weighted FMeasure: 0.703558752099811
Iteration time: 131.0
Weighted AreaUnderPRC: 0.7403442444930888
Mean absolute error: 0.17395936514762975
Coverage of cases: 97.39952718676123
Instances selection time: 19.0
Test time: 27.0
Accumulative iteration time: 995.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.0995104778349682
Kappa statistic: 0.6059779571214594
Training time: 112.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 48.87706855791962
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7168261245564009
Weighted AreaUnderROC: 0.899983604872085
Root mean squared error: 0.3124439342526445
Relative absolute error: 46.43490536231682
Root relative squared error: 72.15583581897201
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.618584201307143
Weighted FMeasure: 0.7151660266292075
Iteration time: 87.0
Weighted AreaUnderPRC: 0.7434261518008884
Mean absolute error: 0.17413089510868807
Coverage of cases: 97.87234042553192
Instances selection time: 19.0
Test time: 25.0
Accumulative iteration time: 1082.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09664983851345411
Kappa statistic: 0.6184103600211733
Training time: 68.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 48.817966903073284
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7281607745535942
Weighted AreaUnderROC: 0.9021108141567754
Root mean squared error: 0.31143174213556873
Relative absolute error: 45.81235558503035
Root relative squared error: 71.9220800624659
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.632004548994486
Weighted FMeasure: 0.7246924673662267
Iteration time: 158.0
Weighted AreaUnderPRC: 0.7525304336910844
Mean absolute error: 0.1717963334438638
Coverage of cases: 97.39952718676123
Instances selection time: 39.0
Test time: 50.0
Accumulative iteration time: 1240.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09353369778779853
Kappa statistic: 0.6310358966712641
Training time: 119.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 48.64066193853428
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7338451351144122
Weighted AreaUnderROC: 0.9048607876623371
Root mean squared error: 0.3085543030412177
Relative absolute error: 45.26462777175167
Root relative squared error: 71.25756396818576
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6387056392211589
Weighted FMeasure: 0.730090526217575
Iteration time: 184.0
Weighted AreaUnderPRC: 0.7539055571465072
Mean absolute error: 0.16974235414406877
Coverage of cases: 97.16312056737588
Instances selection time: 48.0
Test time: 58.0
Accumulative iteration time: 1424.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09199069704662279
Kappa statistic: 0.6373348641636597
Training time: 136.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 48.93617021276596
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7172334101249732
Weighted AreaUnderROC: 0.9069957087862142
Root mean squared error: 0.3052157217134266
Relative absolute error: 45.4945754053158
Root relative squared error: 70.4865516368611
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6186374374774712
Weighted FMeasure: 0.7149890355309685
Iteration time: 171.0
Weighted AreaUnderPRC: 0.7608809601545891
Mean absolute error: 0.17060465776993425
Coverage of cases: 96.92671394799055
Instances selection time: 35.0
Test time: 50.0
Accumulative iteration time: 1595.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09678614727226546
Kappa statistic: 0.6184160497416744
Training time: 136.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 48.40425531914894
Incorrectly Classified Instances: 27.89598108747045
Correctly Classified Instances: 72.10401891252955
Weighted Precision: 0.7218506570671221
Weighted AreaUnderROC: 0.9086233359742417
Root mean squared error: 0.3032579954031867
Relative absolute error: 45.66494764352257
Root relative squared error: 70.03443411197446
Weighted TruePositiveRate: 0.7210401891252955
Weighted MatthewsCorrelation: 0.6270689824862725
Weighted FMeasure: 0.7198869669319429
Iteration time: 187.0
Weighted AreaUnderPRC: 0.7623834391221549
Mean absolute error: 0.17124355366320962
Coverage of cases: 96.45390070921985
Instances selection time: 33.0
Test time: 48.0
Accumulative iteration time: 1782.0
Weighted Recall: 0.7210401891252955
Weighted FalsePositiveRate: 0.09422591210372747
Kappa statistic: 0.6279655647896247
Training time: 154.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 49.40898345153664
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7150420193957523
Weighted AreaUnderROC: 0.9077881875672958
Root mean squared error: 0.30508830554921523
Relative absolute error: 45.86809635667496
Root relative squared error: 70.4571261341785
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6176709510636219
Weighted FMeasure: 0.7135958422153206
Iteration time: 158.0
Weighted AreaUnderPRC: 0.7668748136236494
Mean absolute error: 0.1720053613375311
Coverage of cases: 97.63593380614657
Instances selection time: 22.0
Test time: 46.0
Accumulative iteration time: 1940.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.0966878536495557
Kappa statistic: 0.618481469334208
Training time: 136.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 48.28605200945626
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7207907052869152
Weighted AreaUnderROC: 0.9078323734062481
Root mean squared error: 0.30782637016639774
Relative absolute error: 46.10464337420407
Root relative squared error: 71.08945507169406
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6224001241681638
Weighted FMeasure: 0.7175050709707408
Iteration time: 187.0
Weighted AreaUnderPRC: 0.7558964375771359
Mean absolute error: 0.17289241265326527
Coverage of cases: 96.6903073286052
Instances selection time: 26.0
Test time: 47.0
Accumulative iteration time: 2127.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09612407219964061
Kappa statistic: 0.6214285181566642
Training time: 161.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 49.23167848699764
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7267944103192059
Weighted AreaUnderROC: 0.9094387041191625
Root mean squared error: 0.30492877141791935
Relative absolute error: 46.234152904666246
Root relative squared error: 70.42028330471905
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6314277008177575
Weighted FMeasure: 0.7242738976095414
Iteration time: 182.0
Weighted AreaUnderPRC: 0.762242327624264
Mean absolute error: 0.17337807339249842
Coverage of cases: 96.6903073286052
Instances selection time: 24.0
Test time: 44.0
Accumulative iteration time: 2309.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09362411551771073
Kappa statistic: 0.6309478535155815
Training time: 158.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7217080492057365
Weighted AreaUnderROC: 0.9101423498490471
Root mean squared error: 0.3045827285746752
Relative absolute error: 46.430923765570846
Root relative squared error: 70.34036813323979
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.622729304265232
Weighted FMeasure: 0.7182854898550257
Iteration time: 191.0
Weighted AreaUnderPRC: 0.7669957825042568
Mean absolute error: 0.1741159641208907
Coverage of cases: 96.92671394799055
Instances selection time: 21.0
Test time: 47.0
Accumulative iteration time: 2500.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09615421144294467
Kappa statistic: 0.6214567501659297
Training time: 170.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 51.182033096926716
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7281232402965266
Weighted AreaUnderROC: 0.9081773025009494
Root mean squared error: 0.30702003070668404
Relative absolute error: 47.504524996226
Root relative squared error: 70.90323895004448
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6321185723367676
Weighted FMeasure: 0.7243606350751118
Iteration time: 180.0
Weighted AreaUnderPRC: 0.7639588197347886
Mean absolute error: 0.1781419687358475
Coverage of cases: 97.16312056737588
Instances selection time: 8.0
Test time: 41.0
Accumulative iteration time: 2680.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09343540416508879
Kappa statistic: 0.6310166408207086
Training time: 172.0
		
Time end:Fri Dec 01 13.15.57 EET 2017