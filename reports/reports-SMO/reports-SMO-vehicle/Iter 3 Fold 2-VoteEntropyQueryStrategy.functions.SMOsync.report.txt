Fri Dec 01 13.17.57 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Fri Dec 01 13.17.57 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 62.29314420803782
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6414864940139029
Weighted AreaUnderROC: 0.8560289901748671
Root mean squared error: 0.34327260615901006
Relative absolute error: 57.266594335548
Root relative squared error: 79.27541262853154
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5307559164224604
Weighted FMeasure: 0.642817189222346
Iteration time: 65.0
Weighted AreaUnderPRC: 0.663408958451505
Mean absolute error: 0.214749728758305
Coverage of cases: 95.27186761229315
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 65.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11630128620019076
Kappa statistic: 0.5368647634085848
Training time: 53.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 66.25295508274232
Incorrectly Classified Instances: 36.406619385342786
Correctly Classified Instances: 63.593380614657214
Weighted Precision: 0.6182514234174642
Weighted AreaUnderROC: 0.8536614650740914
Root mean squared error: 0.3465224925498256
Relative absolute error: 59.0187574313376
Root relative squared error: 80.02594174156076
Weighted TruePositiveRate: 0.6359338061465721
Weighted MatthewsCorrelation: 0.5063148383143445
Weighted FMeasure: 0.6219106753445287
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6564753675272782
Mean absolute error: 0.22132034036751597
Coverage of cases: 95.98108747044917
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 124.0
Weighted Recall: 0.6359338061465721
Weighted FalsePositiveRate: 0.12186361668607944
Kappa statistic: 0.5144128631169355
Training time: 49.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 64.59810874704492
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6101033772916132
Weighted AreaUnderROC: 0.8478388269557039
Root mean squared error: 0.3514214054688292
Relative absolute error: 59.11013803849784
Root relative squared error: 81.15729721857006
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.4944559482182123
Weighted FMeasure: 0.6121154948700269
Iteration time: 59.0
Weighted AreaUnderPRC: 0.6476049089664487
Mean absolute error: 0.22166301764436688
Coverage of cases: 94.79905437352245
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 183.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.12505578768393827
Kappa statistic: 0.5016479009768101
Training time: 50.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 65.18912529550828
Incorrectly Classified Instances: 41.371158392434985
Correctly Classified Instances: 58.628841607565015
Weighted Precision: 0.5630701459176642
Weighted AreaUnderROC: 0.8200882061347633
Root mean squared error: 0.37137725177860764
Relative absolute error: 63.075016957462225
Root relative squared error: 85.76590251411302
Weighted TruePositiveRate: 0.5862884160756501
Weighted MatthewsCorrelation: 0.43848688306052314
Weighted FMeasure: 0.5650250425206113
Iteration time: 61.0
Weighted AreaUnderPRC: 0.6239047931896475
Mean absolute error: 0.23653131359048335
Coverage of cases: 93.14420803782505
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 244.0
Weighted Recall: 0.5862884160756501
Weighted FalsePositiveRate: 0.1386057393933284
Kappa statistic: 0.44772299979110086
Training time: 52.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 60.99290780141844
Incorrectly Classified Instances: 40.8983451536643
Correctly Classified Instances: 59.1016548463357
Weighted Precision: 0.5701946670548095
Weighted AreaUnderROC: 0.820252515868856
Root mean squared error: 0.3741112889731207
Relative absolute error: 61.57952702604101
Root relative squared error: 86.39730135820365
Weighted TruePositiveRate: 0.5910165484633569
Weighted MatthewsCorrelation: 0.44568847706597864
Weighted FMeasure: 0.5711304324643078
Iteration time: 63.0
Weighted AreaUnderPRC: 0.6211914537930525
Mean absolute error: 0.2309232263476538
Coverage of cases: 91.725768321513
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 307.0
Weighted Recall: 0.5910165484633569
Weighted FalsePositiveRate: 0.13704835119444375
Kappa statistic: 0.45399807501473577
Training time: 54.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 56.02836879432624
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6444197764625621
Weighted AreaUnderROC: 0.8467137537416854
Root mean squared error: 0.35033482994213627
Relative absolute error: 53.9768041942668
Root relative squared error: 80.90636334943767
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5468020096522249
Weighted FMeasure: 0.6432124517343116
Iteration time: 64.0
Weighted AreaUnderPRC: 0.6620057733084739
Mean absolute error: 0.20241301572850048
Coverage of cases: 91.25295508274232
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 371.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11038106770379229
Kappa statistic: 0.5591650786088613
Training time: 55.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 53.191489361702125
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6452238181134424
Weighted AreaUnderROC: 0.8512980036355066
Root mean squared error: 0.3493300902712592
Relative absolute error: 52.02236445076397
Root relative squared error: 80.6743286616591
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5427916798911993
Weighted FMeasure: 0.6377557417644134
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6583430091103162
Mean absolute error: 0.1950838666903649
Coverage of cases: 91.01654846335697
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 437.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11320188554019489
Kappa statistic: 0.5523909592899778
Training time: 57.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 50.29550827423168
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6398251780287081
Weighted AreaUnderROC: 0.8418928727140015
Root mean squared error: 0.3626908672998081
Relative absolute error: 52.511123607804656
Root relative squared error: 83.7598679472652
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5301944598400018
Weighted FMeasure: 0.6265526011513807
Iteration time: 69.0
Weighted AreaUnderPRC: 0.6299644371583775
Mean absolute error: 0.19691671352926746
Coverage of cases: 87.23404255319149
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 506.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11756986307473388
Kappa statistic: 0.5363640430671957
Training time: 61.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 49.586288416075654
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6461921303857165
Weighted AreaUnderROC: 0.8470403913718774
Root mean squared error: 0.36056605020136867
Relative absolute error: 51.768690478613934
Root relative squared error: 83.26916245776013
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5352186900338239
Weighted FMeasure: 0.6306545095649084
Iteration time: 71.0
Weighted AreaUnderPRC: 0.6360375260287287
Mean absolute error: 0.19413258929480226
Coverage of cases: 88.4160756501182
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 577.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11677935513609519
Kappa statistic: 0.5394733898570502
Training time: 63.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 49.46808510638298
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6867479055731767
Weighted AreaUnderROC: 0.8715882614230294
Root mean squared error: 0.34152675868182825
Relative absolute error: 48.60564235143837
Root relative squared error: 78.87222642416556
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.57937510331868
Weighted FMeasure: 0.663470268163663
Iteration time: 75.0
Weighted AreaUnderPRC: 0.6883870568336257
Mean absolute error: 0.18227115881789388
Coverage of cases: 90.54373522458629
Instances selection time: 8.0
Test time: 20.0
Accumulative iteration time: 652.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10624725052965081
Kappa statistic: 0.580610533378061
Training time: 67.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 47.931442080378254
Incorrectly Classified Instances: 30.49645390070922
Correctly Classified Instances: 69.50354609929079
Weighted Precision: 0.701445643247896
Weighted AreaUnderROC: 0.879508445470222
Root mean squared error: 0.3416003886099785
Relative absolute error: 48.140786802205106
Root relative squared error: 78.88923052770075
Weighted TruePositiveRate: 0.6950354609929078
Weighted MatthewsCorrelation: 0.5907343416284947
Weighted FMeasure: 0.6580943226982856
Iteration time: 76.0
Weighted AreaUnderPRC: 0.6963467261447752
Mean absolute error: 0.18052795050826914
Coverage of cases: 90.0709219858156
Instances selection time: 7.0
Test time: 21.0
Accumulative iteration time: 728.0
Weighted Recall: 0.6950354609929078
Weighted FalsePositiveRate: 0.1026790211544343
Kappa statistic: 0.5934843665025218
Training time: 69.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 48.40425531914894
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7682724589165366
Weighted AreaUnderROC: 0.8930234232661272
Root mean squared error: 0.3335760101143572
Relative absolute error: 46.82489953759065
Root relative squared error: 77.03607969389019
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6373126106279746
Weighted FMeasure: 0.685265223031502
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7513707948497793
Mean absolute error: 0.17559337326596494
Coverage of cases: 89.83451536643027
Instances selection time: 6.0
Test time: 20.0
Accumulative iteration time: 805.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09448042522780435
Kappa statistic: 0.6250139678330118
Training time: 71.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 48.64066193853428
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.7309513075186638
Weighted AreaUnderROC: 0.8839598751268365
Root mean squared error: 0.3437556966254
Relative absolute error: 48.537851424356255
Root relative squared error: 79.38697759285682
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5968149026239151
Weighted FMeasure: 0.6515245036082111
Iteration time: 82.0
Weighted AreaUnderPRC: 0.7261445427017383
Mean absolute error: 0.18201694284133593
Coverage of cases: 88.65248226950355
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 887.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10297942530741037
Kappa statistic: 0.590506970094126
Training time: 76.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 50.88652482269504
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.7399386577875262
Weighted AreaUnderROC: 0.8995632014402056
Root mean squared error: 0.3349862860590081
Relative absolute error: 48.643803029103424
Root relative squared error: 77.36176897240053
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.6026204251286358
Weighted FMeasure: 0.6602972136846
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7537951543767494
Mean absolute error: 0.18241426135913785
Coverage of cases: 93.3806146572104
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 971.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10282872909089007
Kappa statistic: 0.5905100194356947
Training time: 79.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 53.132387706855795
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.7223790420825966
Weighted AreaUnderROC: 0.8992246204432344
Root mean squared error: 0.3304982264797695
Relative absolute error: 49.308103842260266
Root relative squared error: 76.32529600991553
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.6038463920058622
Weighted FMeasure: 0.6767118010655355
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7489739849027996
Mean absolute error: 0.184905389408476
Coverage of cases: 94.56264775413712
Instances selection time: 2.0
Test time: 19.0
Accumulative iteration time: 1047.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10149670305284277
Kappa statistic: 0.5966416603964748
Training time: 74.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 54.72813238770686
Incorrectly Classified Instances: 29.78723404255319
Correctly Classified Instances: 70.2127659574468
Weighted Precision: 0.7124170049476688
Weighted AreaUnderROC: 0.896532923501082
Root mean squared error: 0.3234670985226436
Relative absolute error: 50.546373935219044
Root relative squared error: 74.7015265624142
Weighted TruePositiveRate: 0.7021276595744681
Weighted MatthewsCorrelation: 0.60554945285884
Weighted FMeasure: 0.6901402537221685
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7435679371377298
Mean absolute error: 0.1895489022570714
Coverage of cases: 95.74468085106383
Instances selection time: 2.0
Test time: 18.0
Accumulative iteration time: 1121.0
Weighted Recall: 0.7021276595744681
Weighted FalsePositiveRate: 0.10015680112199793
Kappa statistic: 0.6028021015761822
Training time: 72.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 55.851063829787236
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6997316242598801
Weighted AreaUnderROC: 0.8972259102910745
Root mean squared error: 0.32038802352016055
Relative absolute error: 51.32315625934179
Root relative squared error: 73.99044464979875
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5985171193435546
Weighted FMeasure: 0.6934543892751736
Iteration time: 80.0
Weighted AreaUnderPRC: 0.741310742136512
Mean absolute error: 0.19246183597253172
Coverage of cases: 96.6903073286052
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 1201.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10137713435969109
Kappa statistic: 0.5994109093620671
Training time: 79.0
		
Time end:Fri Dec 01 13.17.59 EET 2017