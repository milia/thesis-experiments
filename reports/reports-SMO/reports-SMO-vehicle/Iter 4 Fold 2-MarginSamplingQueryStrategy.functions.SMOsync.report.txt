Fri Dec 01 13.17.18 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.17.18 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 51.6548463356974
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6520586585397472
Weighted AreaUnderROC: 0.8591388992090192
Root mean squared error: 0.35175941678783296
Relative absolute error: 52.896554349349366
Root relative squared error: 81.23535758897646
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.53548850491252
Weighted FMeasure: 0.6461332926848291
Iteration time: 83.0
Weighted AreaUnderPRC: 0.6898498190077923
Mean absolute error: 0.1983620788100601
Coverage of cases: 92.67139479905437
Instances selection time: 16.0
Test time: 18.0
Accumulative iteration time: 83.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11745067042940381
Kappa statistic: 0.5360180873925502
Training time: 67.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 48.226950354609926
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6590578792447082
Weighted AreaUnderROC: 0.8584521056994079
Root mean squared error: 0.3547827368750105
Relative absolute error: 51.20215654015974
Root relative squared error: 81.9335634554478
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5467498624218515
Weighted FMeasure: 0.6519201082253222
Iteration time: 70.0
Weighted AreaUnderPRC: 0.6836468189321847
Mean absolute error: 0.19200808702559904
Coverage of cases: 92.43498817966903
Instances selection time: 18.0
Test time: 20.0
Accumulative iteration time: 153.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11438693229755884
Kappa statistic: 0.5488252405459835
Training time: 52.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 49.70449172576832
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.6859035188564405
Weighted AreaUnderROC: 0.8682507931704745
Root mean squared error: 0.344221209048392
Relative absolute error: 49.63217637825582
Root relative squared error: 79.49448308194702
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5753543306676808
Weighted FMeasure: 0.674027090970413
Iteration time: 70.0
Weighted AreaUnderPRC: 0.6907009191953082
Mean absolute error: 0.18612066141845932
Coverage of cases: 94.56264775413712
Instances selection time: 16.0
Test time: 19.0
Accumulative iteration time: 223.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.10789778511422009
Kappa statistic: 0.5741324921135647
Training time: 54.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 50.05910165484634
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6837564227134505
Weighted AreaUnderROC: 0.8790008224008989
Root mean squared error: 0.32902997671684425
Relative absolute error: 48.622370809767254
Root relative squared error: 75.98621825157053
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5825681797157866
Weighted FMeasure: 0.6781113664016297
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7049321249124618
Mean absolute error: 0.18233389053662719
Coverage of cases: 95.74468085106383
Instances selection time: 15.0
Test time: 21.0
Accumulative iteration time: 296.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10441860914102956
Kappa statistic: 0.5870587446251985
Training time: 58.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 50.88652482269504
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6862835409568353
Weighted AreaUnderROC: 0.8866822207727916
Root mean squared error: 0.3204153307347908
Relative absolute error: 47.784249665234285
Root relative squared error: 73.99675098088578
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5858842583871268
Weighted FMeasure: 0.6865502554475524
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7213803965816797
Mean absolute error: 0.17919093624462856
Coverage of cases: 95.98108747044917
Instances selection time: 15.0
Test time: 19.0
Accumulative iteration time: 369.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.10403566315093887
Kappa statistic: 0.5900276595268804
Training time: 58.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 48.167848699763596
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.718874795932196
Weighted AreaUnderROC: 0.8975451196206713
Root mean squared error: 0.30892557100019324
Relative absolute error: 45.16120592552551
Root relative squared error: 71.34330463060817
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6300733299778223
Weighted FMeasure: 0.7203638309203931
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7427836121376541
Mean absolute error: 0.16935452222072064
Coverage of cases: 96.45390070921985
Instances selection time: 17.0
Test time: 23.0
Accumulative iteration time: 446.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09293841276669318
Kappa statistic: 0.6340639426052846
Training time: 60.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 50.118203309692674
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6796536059500737
Weighted AreaUnderROC: 0.889700808389401
Root mean squared error: 0.315838458136439
Relative absolute error: 47.215571508358096
Root relative squared error: 72.93976753020377
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5745147237885664
Weighted FMeasure: 0.6785707820033713
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7202479322728574
Mean absolute error: 0.17705839315634286
Coverage of cases: 96.92671394799055
Instances selection time: 15.0
Test time: 21.0
Accumulative iteration time: 523.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10746243653031881
Kappa statistic: 0.5772586924419385
Training time: 62.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 52.24586288416076
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6849014274445792
Weighted AreaUnderROC: 0.8863048577367345
Root mean squared error: 0.3164124448990132
Relative absolute error: 47.802283021079695
Root relative squared error: 73.07232409495717
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5830983440116784
Weighted FMeasure: 0.6849156323673609
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7178964332238227
Mean absolute error: 0.17925856132904885
Coverage of cases: 96.6903073286052
Instances selection time: 13.0
Test time: 20.0
Accumulative iteration time: 600.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10500049498449054
Kappa statistic: 0.5868124165802955
Training time: 64.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 49.822695035460995
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7035556262712872
Weighted AreaUnderROC: 0.8952160207429726
Root mean squared error: 0.3085114822503896
Relative absolute error: 44.751729008498835
Root relative squared error: 71.24767493014116
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6083175477625307
Weighted FMeasure: 0.702566143612243
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7422077922902874
Mean absolute error: 0.16781898378187063
Coverage of cases: 96.21749408983452
Instances selection time: 15.0
Test time: 21.0
Accumulative iteration time: 679.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09829900877013717
Kappa statistic: 0.6121869409660108
Training time: 64.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 50.8274231678487
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.708592424024906
Weighted AreaUnderROC: 0.8957244728767912
Root mean squared error: 0.3069442717214718
Relative absolute error: 45.74645924050305
Root relative squared error: 70.88574316184216
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6148125304053307
Weighted FMeasure: 0.7100070077708917
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7402493142593091
Mean absolute error: 0.17154922215188645
Coverage of cases: 96.45390070921985
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 758.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09696698273208987
Kappa statistic: 0.6183221476510068
Training time: 65.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 51.35933806146572
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6990847227882082
Weighted AreaUnderROC: 0.8947976916583169
Root mean squared error: 0.3055387389643002
Relative absolute error: 46.37982498745873
Root relative squared error: 70.56114927555902
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.602116394401976
Weighted FMeasure: 0.7010110405637063
Iteration time: 78.0
Weighted AreaUnderPRC: 0.7340941895663459
Mean absolute error: 0.17392434370297025
Coverage of cases: 96.6903073286052
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 836.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.0999324272412251
Kappa statistic: 0.605848720452631
Training time: 66.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 51.41843971631206
Incorrectly Classified Instances: 29.55082742316785
Correctly Classified Instances: 70.44917257683215
Weighted Precision: 0.6961839497556276
Weighted AreaUnderROC: 0.8897192853121926
Root mean squared error: 0.30938855607531457
Relative absolute error: 47.040870641402186
Root relative squared error: 71.450226453709
Weighted TruePositiveRate: 0.7044917257683215
Weighted MatthewsCorrelation: 0.600708857689437
Weighted FMeasure: 0.6985058413784788
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7185267306242671
Mean absolute error: 0.17640326490525818
Coverage of cases: 96.92671394799055
Instances selection time: 12.0
Test time: 19.0
Accumulative iteration time: 917.0
Weighted Recall: 0.7044917257683215
Weighted FalsePositiveRate: 0.10006873600003648
Kappa statistic: 0.6058575347367166
Training time: 69.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 49.822695035460995
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.705200528076446
Weighted AreaUnderROC: 0.8961277383506957
Root mean squared error: 0.30270411626088106
Relative absolute error: 45.26167405700514
Root relative squared error: 69.90652120321099
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6131832363272274
Weighted FMeasure: 0.70839588884798
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7347634975352455
Mean absolute error: 0.16973127771376925
Coverage of cases: 96.21749408983452
Instances selection time: 12.0
Test time: 20.0
Accumulative iteration time: 1002.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09683854986607605
Kappa statistic: 0.6184729376160056
Training time: 73.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 50.59101654846336
Incorrectly Classified Instances: 28.13238770685579
Correctly Classified Instances: 71.8676122931442
Weighted Precision: 0.7138000257458967
Weighted AreaUnderROC: 0.8948683515571267
Root mean squared error: 0.3036626857575858
Relative absolute error: 45.85766392944771
Root relative squared error: 70.12789334599476
Weighted TruePositiveRate: 0.7186761229314421
Weighted MatthewsCorrelation: 0.6212698204128545
Weighted FMeasure: 0.7156685830267598
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7310202512037492
Mean absolute error: 0.1719662397354289
Coverage of cases: 97.16312056737588
Instances selection time: 10.0
Test time: 21.0
Accumulative iteration time: 1087.0
Weighted Recall: 0.7186761229314421
Weighted FalsePositiveRate: 0.09520513139498811
Kappa statistic: 0.6247595902971389
Training time: 75.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 50.709219858156025
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.7073323759297586
Weighted AreaUnderROC: 0.8949086791307684
Root mean squared error: 0.30399828614103386
Relative absolute error: 46.02188314755252
Root relative squared error: 70.20539693468432
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6142386950563731
Weighted FMeasure: 0.7099380969221374
Iteration time: 90.0
Weighted AreaUnderPRC: 0.72754292809336
Mean absolute error: 0.17258206180332195
Coverage of cases: 97.39952718676123
Instances selection time: 9.0
Test time: 21.0
Accumulative iteration time: 1177.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.0967101170000622
Kappa statistic: 0.6184729376160056
Training time: 81.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 52.364066193853425
Incorrectly Classified Instances: 28.84160756501182
Correctly Classified Instances: 71.15839243498817
Weighted Precision: 0.7057281339842485
Weighted AreaUnderROC: 0.8949476804944322
Root mean squared error: 0.3049977326993818
Relative absolute error: 46.767383203790736
Root relative squared error: 70.43620923048545
Weighted TruePositiveRate: 0.7115839243498818
Weighted MatthewsCorrelation: 0.6113366063815852
Weighted FMeasure: 0.7080458964617011
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7299761175125759
Mean absolute error: 0.17537768701421527
Coverage of cases: 97.63593380614657
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 1267.0
Weighted Recall: 0.7115839243498818
Weighted FalsePositiveRate: 0.09762905780471473
Kappa statistic: 0.6152911445248728
Training time: 82.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 53.3096926713948
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7174347117890408
Weighted AreaUnderROC: 0.8963763388008364
Root mean squared error: 0.3024209851472551
Relative absolute error: 46.961428620149626
Root relative squared error: 69.84113487334382
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6270410736714165
Weighted FMeasure: 0.7200070915965785
Iteration time: 89.0
Weighted AreaUnderPRC: 0.7388852957332117
Mean absolute error: 0.1761053573255611
Coverage of cases: 98.10874704491725
Instances selection time: 3.0
Test time: 20.0
Accumulative iteration time: 1356.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.0934576675155953
Kappa statistic: 0.6311871227364186
Training time: 86.0
		
Time end:Fri Dec 01 13.17.21 EET 2017