Fri Dec 01 13.17.23 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.17.23 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 59.692671394799056
Incorrectly Classified Instances: 40.189125295508276
Correctly Classified Instances: 59.810874704491724
Weighted Precision: 0.5850480155059249
Weighted AreaUnderROC: 0.8249052846079248
Root mean squared error: 0.36687215852999794
Relative absolute error: 60.277679154564964
Root relative squared error: 84.72549579418934
Weighted TruePositiveRate: 0.5981087470449172
Weighted MatthewsCorrelation: 0.45682256712908337
Weighted FMeasure: 0.5902646569042035
Iteration time: 68.0
Weighted AreaUnderPRC: 0.6262501249271192
Mean absolute error: 0.22604129682961863
Coverage of cases: 92.19858156028369
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 68.0
Weighted Recall: 0.5981087470449172
Weighted FalsePositiveRate: 0.13530460425088786
Kappa statistic: 0.4641180415828303
Training time: 56.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 57.21040189125296
Incorrectly Classified Instances: 37.35224586288416
Correctly Classified Instances: 62.64775413711584
Weighted Precision: 0.6133978668525183
Weighted AreaUnderROC: 0.843661848874509
Root mean squared error: 0.3501680269998026
Relative absolute error: 56.799113069141264
Root relative squared error: 80.86784185997448
Weighted TruePositiveRate: 0.6264775413711584
Weighted MatthewsCorrelation: 0.4944246299245764
Weighted FMeasure: 0.6188579898748708
Iteration time: 66.0
Weighted AreaUnderPRC: 0.6415680682444549
Mean absolute error: 0.2129966740092797
Coverage of cases: 94.32624113475177
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 134.0
Weighted Recall: 0.6264775413711584
Weighted FalsePositiveRate: 0.1260007087749342
Kappa statistic: 0.5019746941087051
Training time: 53.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 55.7919621749409
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6143238746820897
Weighted AreaUnderROC: 0.8591886356017834
Root mean squared error: 0.33737446617194317
Relative absolute error: 54.55407658712152
Root relative squared error: 77.91329554483107
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.4992703890177493
Weighted FMeasure: 0.6203430389053907
Iteration time: 69.0
Weighted AreaUnderPRC: 0.6657401557692374
Mean absolute error: 0.2045777872017057
Coverage of cases: 95.50827423167848
Instances selection time: 13.0
Test time: 17.0
Accumulative iteration time: 203.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12437380186875764
Kappa statistic: 0.5080808683206107
Training time: 56.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 54.60992907801418
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6535963578967546
Weighted AreaUnderROC: 0.8623994823219837
Root mean squared error: 0.334645796221265
Relative absolute error: 52.9873751906604
Root relative squared error: 77.2831362126096
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5460635259161971
Weighted FMeasure: 0.6580691741579964
Iteration time: 70.0
Weighted AreaUnderPRC: 0.6734315743049553
Mean absolute error: 0.19870265696497652
Coverage of cases: 95.27186761229315
Instances selection time: 14.0
Test time: 17.0
Accumulative iteration time: 273.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11337620943510793
Kappa statistic: 0.5522808586762075
Training time: 56.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 55.43735224586288
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6609783461040546
Weighted AreaUnderROC: 0.8696502084442311
Root mean squared error: 0.32843514074602487
Relative absolute error: 52.118909894092845
Root relative squared error: 75.84884676842005
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5559138733475351
Weighted FMeasure: 0.6650894585152817
Iteration time: 71.0
Weighted AreaUnderPRC: 0.6885234289758182
Mean absolute error: 0.19544591210284817
Coverage of cases: 95.74468085106383
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 344.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.11091426788927967
Kappa statistic: 0.5616220810593182
Training time: 58.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 52.4822695035461
Incorrectly Classified Instances: 28.60520094562648
Correctly Classified Instances: 71.39479905437352
Weighted Precision: 0.6982139403618632
Weighted AreaUnderROC: 0.8852529163678494
Root mean squared error: 0.31294308325007597
Relative absolute error: 48.127803469320554
Root relative squared error: 72.27110934218514
Weighted TruePositiveRate: 0.7139479905437353
Weighted MatthewsCorrelation: 0.6104037269584605
Weighted FMeasure: 0.7031881998505884
Iteration time: 76.0
Weighted AreaUnderPRC: 0.714013095520516
Mean absolute error: 0.1804792630099521
Coverage of cases: 95.98108747044917
Instances selection time: 15.0
Test time: 20.0
Accumulative iteration time: 420.0
Weighted Recall: 0.7139479905437353
Weighted FalsePositiveRate: 0.09599055806990366
Kappa statistic: 0.618791196514356
Training time: 61.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 52.600472813238774
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7248324940899238
Weighted AreaUnderROC: 0.8935607500083536
Root mean squared error: 0.31004804158784605
Relative absolute error: 48.150686065619404
Root relative squared error: 71.60252810898369
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6370141693262135
Weighted FMeasure: 0.726020820483107
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7315213805029376
Mean absolute error: 0.18056507274607278
Coverage of cases: 96.21749408983452
Instances selection time: 14.0
Test time: 19.0
Accumulative iteration time: 501.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09050289352833205
Kappa statistic: 0.6407455970438359
Training time: 67.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 51.35933806146572
Incorrectly Classified Instances: 27.423167848699762
Correctly Classified Instances: 72.57683215130024
Weighted Precision: 0.7204184735627749
Weighted AreaUnderROC: 0.8931683139786754
Root mean squared error: 0.309980323622325
Relative absolute error: 47.611297811379366
Root relative squared error: 71.58688931473468
Weighted TruePositiveRate: 0.7257683215130024
Weighted MatthewsCorrelation: 0.6308129079647851
Weighted FMeasure: 0.7222728036695576
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7320253482065707
Mean absolute error: 0.17854236679267263
Coverage of cases: 96.21749408983452
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 578.0
Weighted Recall: 0.7257683215130024
Weighted FalsePositiveRate: 0.09213767632730618
Kappa statistic: 0.6344565047343054
Training time: 63.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 51.241134751773046
Incorrectly Classified Instances: 26.004728132387708
Correctly Classified Instances: 73.99527186761229
Weighted Precision: 0.7352136560436602
Weighted AreaUnderROC: 0.8964956034303695
Root mean squared error: 0.30614114563430894
Relative absolute error: 46.6298212761702
Root relative squared error: 70.70026913679548
Weighted TruePositiveRate: 0.7399527186761229
Weighted MatthewsCorrelation: 0.6501669207441203
Weighted FMeasure: 0.7367284808345451
Iteration time: 78.0
Weighted AreaUnderPRC: 0.7405588115023644
Mean absolute error: 0.17486182978563825
Coverage of cases: 96.21749408983452
Instances selection time: 15.0
Test time: 22.0
Accumulative iteration time: 656.0
Weighted Recall: 0.7399527186761229
Weighted FalsePositiveRate: 0.08730421096556187
Kappa statistic: 0.653327770285876
Training time: 63.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 50.76832151300236
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7254013679453885
Weighted AreaUnderROC: 0.8986134472917096
Root mean squared error: 0.3052173965065559
Relative absolute error: 46.697257469592195
Root relative squared error: 70.48693841376672
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6351927184735332
Weighted FMeasure: 0.7242079782534657
Iteration time: 78.0
Weighted AreaUnderPRC: 0.751330495391817
Mean absolute error: 0.17511471551097074
Coverage of cases: 95.74468085106383
Instances selection time: 15.0
Test time: 21.0
Accumulative iteration time: 734.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09133278093095856
Kappa statistic: 0.6375780423477523
Training time: 63.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 53.959810874704495
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7290466714251396
Weighted AreaUnderROC: 0.9008721094069667
Root mean squared error: 0.30438933960250797
Relative absolute error: 48.15322568516306
Root relative squared error: 70.29570686318417
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6389739626839256
Weighted FMeasure: 0.7296269131671215
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7513889678554042
Mean absolute error: 0.18057459631936146
Coverage of cases: 97.16312056737588
Instances selection time: 11.0
Test time: 18.0
Accumulative iteration time: 811.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09080565028925888
Kappa statistic: 0.6405233143240524
Training time: 66.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 53.427895981087474
Incorrectly Classified Instances: 26.004728132387708
Correctly Classified Instances: 73.99527186761229
Weighted Precision: 0.7419696953459735
Weighted AreaUnderROC: 0.9050778618171502
Root mean squared error: 0.30282401045484764
Relative absolute error: 47.03735579258591
Root relative squared error: 69.93420958127534
Weighted TruePositiveRate: 0.7399527186761229
Weighted MatthewsCorrelation: 0.6528271781968542
Weighted FMeasure: 0.7395850983824265
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7670703736759859
Mean absolute error: 0.17639008422219718
Coverage of cases: 96.45390070921985
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 892.0
Weighted Recall: 0.7399527186761229
Weighted FalsePositiveRate: 0.0883919523802806
Kappa statistic: 0.6528674062413738
Training time: 69.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 53.959810874704495
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.7381272587288638
Weighted AreaUnderROC: 0.9055967894850815
Root mean squared error: 0.30264777945531546
Relative absolute error: 47.946512752736886
Root relative squared error: 69.89351077526754
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.6469969663087353
Weighted FMeasure: 0.7363731843591611
Iteration time: 80.0
Weighted AreaUnderPRC: 0.7720032892747071
Mean absolute error: 0.17979942282276332
Coverage of cases: 97.16312056737588
Instances selection time: 9.0
Test time: 17.0
Accumulative iteration time: 972.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.08965582403892217
Kappa statistic: 0.6466982363249935
Training time: 71.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 56.02836879432624
Incorrectly Classified Instances: 27.18676122931442
Correctly Classified Instances: 72.81323877068557
Weighted Precision: 0.7300890535034406
Weighted AreaUnderROC: 0.9009639514271662
Root mean squared error: 0.30869348907125077
Relative absolute error: 49.833657238824664
Root relative squared error: 71.28970760494857
Weighted TruePositiveRate: 0.7281323877068558
Weighted MatthewsCorrelation: 0.6369927098610478
Weighted FMeasure: 0.7287830104190323
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7617196541365057
Mean absolute error: 0.1868762146455925
Coverage of cases: 97.87234042553192
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1051.0
Weighted Recall: 0.7281323877068558
Weighted FalsePositiveRate: 0.09211776558475043
Kappa statistic: 0.637253732233673
Training time: 71.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 55.141843971631204
Incorrectly Classified Instances: 28.368794326241133
Correctly Classified Instances: 71.63120567375887
Weighted Precision: 0.7206509412824488
Weighted AreaUnderROC: 0.9031854893391139
Root mean squared error: 0.30628573438580037
Relative absolute error: 49.34460059085327
Root relative squared error: 70.73366047863362
Weighted TruePositiveRate: 0.7163120567375887
Weighted MatthewsCorrelation: 0.6223068488747402
Weighted FMeasure: 0.7178321013202517
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7681126544778775
Mean absolute error: 0.18504225221569975
Coverage of cases: 97.39952718676123
Instances selection time: 8.0
Test time: 23.0
Accumulative iteration time: 1136.0
Weighted Recall: 0.7163120567375887
Weighted FalsePositiveRate: 0.09622100149446416
Kappa statistic: 0.6213607441499639
Training time: 77.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 56.32387706855792
Incorrectly Classified Instances: 26.47754137115839
Correctly Classified Instances: 73.5224586288416
Weighted Precision: 0.7393751544552294
Weighted AreaUnderROC: 0.9040985633579272
Root mean squared error: 0.3071812393090553
Relative absolute error: 49.94374628029754
Root relative squared error: 70.94046848203438
Weighted TruePositiveRate: 0.735224586288416
Weighted MatthewsCorrelation: 0.6474964735519421
Weighted FMeasure: 0.7362705494817294
Iteration time: 87.0
Weighted AreaUnderPRC: 0.7684353852113477
Mean absolute error: 0.18728904855111578
Coverage of cases: 98.3451536643026
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 1223.0
Weighted Recall: 0.735224586288416
Weighted FalsePositiveRate: 0.08990481387815226
Kappa statistic: 0.6465796344647519
Training time: 81.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 56.6193853427896
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7360177029336107
Weighted AreaUnderROC: 0.9047100781045492
Root mean squared error: 0.3068637387130802
Relative absolute error: 50.07120207502192
Root relative squared error: 70.86714486021275
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6417910114734809
Weighted FMeasure: 0.7313836887897462
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7696613672592861
Mean absolute error: 0.18776700778133223
Coverage of cases: 98.58156028368795
Instances selection time: 3.0
Test time: 17.0
Accumulative iteration time: 1308.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09150809310593612
Kappa statistic: 0.6402551381998584
Training time: 82.0
		
Time end:Fri Dec 01 13.17.26 EET 2017