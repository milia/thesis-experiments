Mon Nov 27 15.57.41 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.41 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 71.15942028985508
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8209383856655491
Weighted AreaUnderROC: 0.8787445533769062
Root mean squared error: 0.3835159227739741
Relative absolute error: 42.855727285661985
Root relative squared error: 76.70318455479482
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6370662570805954
Weighted FMeasure: 0.8205034324942792
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8766656795831416
Mean absolute error: 0.2142786364283099
Coverage of cases: 94.20289855072464
Instances selection time: 16.0
Test time: 8.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.18170733873259448
Kappa statistic: 0.6368914239152577
Training time: 45.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 74.6376811594203
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8327410207939508
Weighted AreaUnderROC: 0.9024373638344227
Root mean squared error: 0.36775856765482434
Relative absolute error: 43.25057234208044
Root relative squared error: 73.55171353096488
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6435959029348383
Weighted FMeasure: 0.8156404255253713
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8889121224062578
Mean absolute error: 0.2162528617104022
Coverage of cases: 95.94202898550725
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.2095872406933788
Kappa statistic: 0.6265060240963856
Training time: 28.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 77.3913043478261
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8168187511182681
Weighted AreaUnderROC: 0.9062159586056644
Root mean squared error: 0.38302590586519364
Relative absolute error: 47.05428230983392
Root relative squared error: 76.60518117303873
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5979192010116163
Weighted FMeasure: 0.7856871564217891
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8962316879794743
Mean absolute error: 0.2352714115491696
Coverage of cases: 96.52173913043478
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 134.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.24497904234157436
Kappa statistic: 0.5684688969927595
Training time: 29.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 74.4927536231884
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8082940269929481
Weighted AreaUnderROC: 0.896207788671024
Root mean squared error: 0.4002451132445394
Relative absolute error: 48.06600672789769
Root relative squared error: 80.04902264890788
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.576239446504013
Weighted FMeasure: 0.7724088295722725
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8844252487233251
Mean absolute error: 0.24033003363948843
Coverage of cases: 95.3623188405797
Instances selection time: 10.0
Test time: 3.0
Accumulative iteration time: 160.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2595286302926968
Kappa statistic: 0.5429010546398856
Training time: 16.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 76.81159420289855
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8047017965671541
Weighted AreaUnderROC: 0.9059436274509803
Root mean squared error: 0.40142594452102254
Relative absolute error: 49.555559694049926
Root relative squared error: 80.2851889042045
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5621903524762739
Weighted FMeasure: 0.7616176091255624
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8977312845744391
Mean absolute error: 0.24777779847024964
Coverage of cases: 95.65217391304348
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.27176843563512365
Kappa statistic: 0.5229738353541799
Training time: 27.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 76.23188405797102
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8150724398190891
Weighted AreaUnderROC: 0.9125136165577342
Root mean squared error: 0.40124553915368455
Relative absolute error: 48.345047820526176
Root relative squared error: 80.2491078307369
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5891772708593355
Weighted FMeasure: 0.7784779274503453
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9084177891139746
Mean absolute error: 0.24172523910263088
Coverage of cases: 95.94202898550725
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 223.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2535814506962205
Kappa statistic: 0.5550903598494886
Training time: 26.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 75.94202898550725
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8150724398190891
Weighted AreaUnderROC: 0.9132625272331156
Root mean squared error: 0.40342162945303706
Relative absolute error: 48.29329579088164
Root relative squared error: 80.68432589060741
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5891772708593355
Weighted FMeasure: 0.7784779274503453
Iteration time: 39.0
Weighted AreaUnderPRC: 0.9091319568261272
Mean absolute error: 0.24146647895440823
Coverage of cases: 95.65217391304348
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 262.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2535814506962205
Kappa statistic: 0.5550903598494886
Training time: 34.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 74.05797101449275
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8076407673121128
Weighted AreaUnderROC: 0.9121051198257081
Root mean squared error: 0.41076612507120636
Relative absolute error: 48.66259938671683
Root relative squared error: 82.15322501424127
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5645960455380044
Weighted FMeasure: 0.7609034452118938
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9081663278744827
Mean absolute error: 0.24331299693358413
Coverage of cases: 94.20289855072464
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 285.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.27309605001420856
Kappa statistic: 0.5223133454041964
Training time: 20.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 76.81159420289855
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8239017963302163
Weighted AreaUnderROC: 0.9186410675381264
Root mean squared error: 0.39344246587409587
Relative absolute error: 47.11811527222905
Root relative squared error: 78.68849317481917
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.6074610406243708
Weighted FMeasure: 0.7878697637482628
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9121226288662535
Mean absolute error: 0.23559057636114522
Coverage of cases: 96.23188405797102
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 309.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.24399687411196364
Kappa statistic: 0.5736680436394449
Training time: 21.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 79.1304347826087
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8430483322931834
Weighted AreaUnderROC: 0.9209899237472767
Root mean squared error: 0.36693681773827846
Relative absolute error: 43.884636527701474
Root relative squared error: 73.3873635476557
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6629168610262626
Weighted FMeasure: 0.8243793315709663
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9183686789170327
Mean absolute error: 0.21942318263850735
Coverage of cases: 98.55072463768116
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 340.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.20133027848820687
Kappa statistic: 0.6443361115479372
Training time: 27.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 76.52173913043478
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8453335626188223
Weighted AreaUnderROC: 0.9258918845315905
Root mean squared error: 0.33909464998306854
Relative absolute error: 40.31912693482187
Root relative squared error: 67.8189299966137
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754796956079258
Weighted FMeasure: 0.8347406007141357
Iteration time: 40.0
Weighted AreaUnderPRC: 0.9229998317176472
Mean absolute error: 0.20159563467410935
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 380.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.18643524438761014
Kappa statistic: 0.6644784828592268
Training time: 37.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 75.94202898550725
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.849067820717297
Weighted AreaUnderROC: 0.926062091503268
Root mean squared error: 0.33726395756613814
Relative absolute error: 40.39755829143238
Root relative squared error: 67.45279151322762
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6821320356061777
Weighted FMeasure: 0.8375484546883185
Iteration time: 36.0
Weighted AreaUnderPRC: 0.925443575811363
Mean absolute error: 0.20198779145716192
Coverage of cases: 98.26086956521739
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 416.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.18412546177891445
Kappa statistic: 0.6702465981961315
Training time: 33.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 76.66666666666667
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8491223903989837
Weighted AreaUnderROC: 0.9262663398692811
Root mean squared error: 0.3287455020052119
Relative absolute error: 39.14152305937624
Root relative squared error: 65.74910040104238
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6942907752744866
Weighted FMeasure: 0.8491700359255878
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9257493704766112
Mean absolute error: 0.19570761529688122
Coverage of cases: 97.97101449275362
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 464.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.15595428388746804
Kappa statistic: 0.6942427651089069
Training time: 47.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 76.08695652173913
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8548567821352482
Weighted AreaUnderROC: 0.9211601307189543
Root mean squared error: 0.31987865501768764
Relative absolute error: 38.53708287230264
Root relative squared error: 63.975731003537526
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7020945085864515
Weighted FMeasure: 0.8497558142604472
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9203096367982703
Mean absolute error: 0.1926854143615132
Coverage of cases: 97.97101449275362
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 554.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.14267814009661833
Kappa statistic: 0.6982541124230498
Training time: 89.0
		
Time end:Mon Nov 27 15.57.43 EET 2017