Mon Nov 27 15.57.46 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.46 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 78.69565217391305
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.860554327356096
Weighted AreaUnderROC: 0.891038281090637
Root mean squared error: 0.3563898792301955
Relative absolute error: 39.97071235172251
Root relative squared error: 71.2779758460391
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7114898324543226
Weighted FMeasure: 0.8525744886605414
Iteration time: 41.0
Weighted AreaUnderPRC: 0.8853705798589135
Mean absolute error: 0.19985356175861255
Coverage of cases: 96.81159420289855
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13680028143948017
Kappa statistic: 0.7051726738048559
Training time: 33.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 74.4927536231884
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8261019780325427
Weighted AreaUnderROC: 0.8967838444278236
Root mean squared error: 0.3635058221667984
Relative absolute error: 41.80606795502064
Root relative squared error: 72.70116443335968
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6470273930255858
Weighted FMeasure: 0.8253872310523276
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8996398306815012
Mean absolute error: 0.20903033977510319
Coverage of cases: 96.52173913043478
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 73.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.18424973615048731
Kappa statistic: 0.6458996202401724
Training time: 27.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8291588021678422
Weighted AreaUnderROC: 0.8995716325559258
Root mean squared error: 0.35986103331273755
Relative absolute error: 41.076631706683465
Root relative squared error: 71.9722066625475
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6529598837416185
Weighted FMeasure: 0.8282119047911723
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9018374943138366
Mean absolute error: 0.20538315853341732
Coverage of cases: 96.52173913043478
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 94.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.18191268478088418
Kappa statistic: 0.6515807671898802
Training time: 16.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 70.0
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8266033067507055
Weighted AreaUnderROC: 0.9064051132113958
Root mean squared error: 0.37075152708068165
Relative absolute error: 40.724639780669804
Root relative squared error: 74.15030541613633
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6430626008560334
Weighted FMeasure: 0.8210744790048121
Iteration time: 38.0
Weighted AreaUnderPRC: 0.9085575635784386
Mean absolute error: 0.203623198903349
Coverage of cases: 95.65217391304348
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.1953921183149501
Kappa statistic: 0.6370112285906478
Training time: 33.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 68.84057971014492
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8275028398806625
Weighted AreaUnderROC: 0.9067110899571632
Root mean squared error: 0.3714254985254418
Relative absolute error: 40.039632852210055
Root relative squared error: 74.28509970508837
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6437288825030477
Weighted FMeasure: 0.8208090379238021
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9083006172005189
Mean absolute error: 0.20019816426105028
Coverage of cases: 95.3623188405797
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 180.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.19665002271421572
Kappa statistic: 0.6365473291539298
Training time: 43.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 74.20289855072464
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7908993241596617
Weighted AreaUnderROC: 0.8910722785068336
Root mean squared error: 0.4036111507166975
Relative absolute error: 48.73380090064086
Root relative squared error: 80.7222301433395
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.552094146589478
Weighted FMeasure: 0.7656622851365015
Iteration time: 46.0
Weighted AreaUnderPRC: 0.8909767152407513
Mean absolute error: 0.2436690045032043
Coverage of cases: 95.3623188405797
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 226.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26153798398278255
Kappa statistic: 0.5283250368085255
Training time: 37.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7838485655131617
Weighted AreaUnderROC: 0.8839668185217924
Root mean squared error: 0.41928706056544035
Relative absolute error: 50.21653704707916
Root relative squared error: 83.85741211308807
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5315741909773352
Weighted FMeasure: 0.7517723078791458
Iteration time: 43.0
Weighted AreaUnderPRC: 0.8838557070219114
Mean absolute error: 0.2510826852353958
Coverage of cases: 93.91304347826087
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 269.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2771757114575234
Kappa statistic: 0.502199542495161
Training time: 37.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 73.6231884057971
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7540892298948564
Weighted AreaUnderROC: 0.8760794179642348
Root mean squared error: 0.44051733257997006
Relative absolute error: 54.41330043816885
Root relative squared error: 88.10346651599401
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.46243999323501633
Weighted FMeasure: 0.7113522485981886
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8764322038471705
Mean absolute error: 0.2720665021908442
Coverage of cases: 94.4927536231884
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 323.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.3190572762846835
Kappa statistic: 0.42561105207226346
Training time: 50.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 73.76811594202898
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7771669720695208
Weighted AreaUnderROC: 0.8742775549058271
Root mean squared error: 0.443536088370545
Relative absolute error: 55.16795991564641
Root relative squared error: 88.70721767410899
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4959372713713594
Weighted FMeasure: 0.720967340379784
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8749359458382926
Mean absolute error: 0.27583979957823207
Coverage of cases: 93.33333333333333
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 373.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.3109669752055365
Kappa statistic: 0.447882214872506
Training time: 43.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 78.84057971014492
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.7920585308204267
Weighted AreaUnderROC: 0.8867546066498946
Root mean squared error: 0.40966267852799454
Relative absolute error: 51.56556159280149
Root relative squared error: 81.93253570559891
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5604327898735514
Weighted FMeasure: 0.7732294230243368
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8869971366916154
Mean absolute error: 0.25782780796400745
Coverage of cases: 96.52173913043478
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 429.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.25183226364651357
Kappa statistic: 0.542200649509376
Training time: 50.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 84.20289855072464
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8149594156359276
Weighted AreaUnderROC: 0.9043652682396138
Root mean squared error: 0.3733834560437353
Relative absolute error: 47.93327052018407
Root relative squared error: 74.67669120874706
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6154720110349589
Weighted FMeasure: 0.805338439252819
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9071399911621194
Mean absolute error: 0.23966635260092034
Coverage of cases: 98.84057971014492
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 505.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2146248015585598
Kappa statistic: 0.6054956859212031
Training time: 69.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 88.69565217391305
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.7992787643736817
Weighted AreaUnderROC: 0.9095668729176584
Root mean squared error: 0.3839173861512938
Relative absolute error: 49.04701658979126
Root relative squared error: 76.78347723025875
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.588781896933557
Weighted FMeasure: 0.7948221243529034
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9120445114295825
Mean absolute error: 0.2452350829489563
Coverage of cases: 99.1304347826087
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 587.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.22145719823844115
Kappa statistic: 0.5837211707518875
Training time: 77.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 89.1304347826087
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8090774125132556
Weighted AreaUnderROC: 0.8983137281566601
Root mean squared error: 0.3735423872566545
Relative absolute error: 47.15537117706113
Root relative squared error: 74.70847745133091
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6116027583050592
Weighted FMeasure: 0.8075224292615596
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9006892973430483
Mean absolute error: 0.23577685588530564
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 688.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20456156636443454
Kappa statistic: 0.6095009432344366
Training time: 100.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 90.72463768115942
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8141748236870726
Weighted AreaUnderROC: 0.902257428435439
Root mean squared error: 0.3710936365213013
Relative absolute error: 46.885685007350475
Root relative squared error: 74.21872730426026
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6236358951430904
Weighted FMeasure: 0.8140843007867268
Iteration time: 164.0
Weighted AreaUnderPRC: 0.9026391923308421
Mean absolute error: 0.23442842503675237
Coverage of cases: 99.42028985507247
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 852.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.19359794162889996
Kappa statistic: 0.6232467665426749
Training time: 159.0
		
Time end:Mon Nov 27 15.57.47 EET 2017