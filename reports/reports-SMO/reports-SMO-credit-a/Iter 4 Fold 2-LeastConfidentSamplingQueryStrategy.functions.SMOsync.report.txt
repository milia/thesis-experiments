Mon Nov 27 15.58.08 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.08 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 70.28985507246377
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8316538122761915
Weighted AreaUnderROC: 0.8963078806010742
Root mean squared error: 0.3563624446622988
Relative absolute error: 39.235942028139554
Root relative squared error: 71.27248893245977
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6589785634600754
Weighted FMeasure: 0.8315138975879711
Iteration time: 30.0
Weighted AreaUnderPRC: 0.8866622926196764
Mean absolute error: 0.19617971014069777
Coverage of cases: 94.78260869565217
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.1758019202134841
Kappa statistic: 0.6585673821792991
Training time: 22.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 68.26086956521739
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8230880576868428
Weighted AreaUnderROC: 0.8882504929625349
Root mean squared error: 0.3551305759686741
Relative absolute error: 37.80224875243683
Root relative squared error: 71.02611519373482
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6420481014627261
Weighted FMeasure: 0.8231312635668115
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8694686284957804
Mean absolute error: 0.18901124376218414
Coverage of cases: 93.91304347826087
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.1815551699230279
Kappa statistic: 0.6420370464867071
Training time: 23.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8406156921539231
Weighted AreaUnderROC: 0.8856326919154144
Root mean squared error: 0.3562009731159986
Relative absolute error: 40.30986167260864
Root relative squared error: 71.24019462319971
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6765671344274917
Weighted FMeasure: 0.8400151684928734
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8688566094852753
Mean absolute error: 0.2015493083630432
Coverage of cases: 94.20289855072464
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 92.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1687907661046746
Kappa statistic: 0.6756133002820753
Training time: 26.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 67.97101449275362
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8439886696490788
Weighted AreaUnderROC: 0.8996396273883186
Root mean squared error: 0.3526327489515343
Relative absolute error: 36.62745890322594
Root relative squared error: 70.52654979030686
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6826240773296991
Weighted FMeasure: 0.8426891026979308
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8913103841059608
Mean absolute error: 0.1831372945161297
Coverage of cases: 93.91304347826087
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 128.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1677116191343371
Kappa statistic: 0.6809057275966018
Training time: 31.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 68.55072463768116
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8439886696490788
Weighted AreaUnderROC: 0.883932821105596
Root mean squared error: 0.36118019864921996
Relative absolute error: 38.49615226562966
Root relative squared error: 72.236039729844
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6826240773296991
Weighted FMeasure: 0.8426891026979308
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8705924362662114
Mean absolute error: 0.1924807613281483
Coverage of cases: 93.33333333333333
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 164.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1677116191343371
Kappa statistic: 0.6809057275966018
Training time: 30.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 70.43478260869566
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8358120932549021
Weighted AreaUnderROC: 0.8823689399605629
Root mean squared error: 0.361095090484473
Relative absolute error: 39.390549227473734
Root relative squared error: 72.2190180968946
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6651647937435959
Weighted FMeasure: 0.8336747299358634
Iteration time: 58.0
Weighted AreaUnderPRC: 0.8694992223751993
Mean absolute error: 0.19695274613736868
Coverage of cases: 93.91304347826087
Instances selection time: 10.0
Test time: 5.0
Accumulative iteration time: 222.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17849648644094354
Kappa statistic: 0.6625366807955656
Training time: 48.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 69.42028985507247
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8358120932549021
Weighted AreaUnderROC: 0.882198952879581
Root mean squared error: 0.36230918328084083
Relative absolute error: 38.89690320637816
Root relative squared error: 72.46183665616816
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6651647937435959
Weighted FMeasure: 0.8336747299358634
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8691959563539318
Mean absolute error: 0.19448451603189082
Coverage of cases: 93.6231884057971
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 269.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17849648644094354
Kappa statistic: 0.6625366807955656
Training time: 44.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 76.23188405797102
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8369971627942643
Weighted AreaUnderROC: 0.8855646970830215
Root mean squared error: 0.35918022361742785
Relative absolute error: 42.217605200369064
Root relative squared error: 71.83604472348557
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6659255967825302
Weighted FMeasure: 0.8332657149063553
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8770567989911485
Mean absolute error: 0.2110880260018453
Coverage of cases: 95.3623188405797
Instances selection time: 11.0
Test time: 7.0
Accumulative iteration time: 323.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18101229523947482
Kappa statistic: 0.6616774193548386
Training time: 43.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 74.92753623188406
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8369971627942643
Weighted AreaUnderROC: 0.8859046712449854
Root mean squared error: 0.3598678029443357
Relative absolute error: 41.57667261368182
Root relative squared error: 71.97356058886713
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6659255967825302
Weighted FMeasure: 0.8332657149063553
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8775108309022353
Mean absolute error: 0.2078833630684091
Coverage of cases: 95.3623188405797
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 377.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18101229523947482
Kappa statistic: 0.6616774193548386
Training time: 47.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 74.4927536231884
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8369971627942643
Weighted AreaUnderROC: 0.885972666077378
Root mean squared error: 0.360071304919374
Relative absolute error: 41.17940800840319
Root relative squared error: 72.0142609838748
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6659255967825302
Weighted FMeasure: 0.8332657149063553
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8774846365790268
Mean absolute error: 0.20589704004201598
Coverage of cases: 95.3623188405797
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 437.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18101229523947482
Kappa statistic: 0.6616774193548386
Training time: 55.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 74.92753623188406
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8299132580807453
Weighted AreaUnderROC: 0.8864826273203236
Root mean squared error: 0.3558643818027099
Relative absolute error: 41.148847066736344
Root relative squared error: 71.17287636054198
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6532770060394134
Weighted FMeasure: 0.8278387555476481
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8779440317348127
Mean absolute error: 0.20574423533368172
Coverage of cases: 95.07246376811594
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 503.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.18442849357941551
Kappa statistic: 0.6506958625778662
Training time: 59.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 74.6376811594203
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8299132580807453
Weighted AreaUnderROC: 0.8864826273203236
Root mean squared error: 0.3563318187323375
Relative absolute error: 40.96506447139526
Root relative squared error: 71.2663637464675
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6532770060394134
Weighted FMeasure: 0.8278387555476481
Iteration time: 75.0
Weighted AreaUnderPRC: 0.8779171844315524
Mean absolute error: 0.20482532235697629
Coverage of cases: 94.78260869565217
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 578.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.18442849357941551
Kappa statistic: 0.6506958625778662
Training time: 72.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 74.78260869565217
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8378103444131996
Weighted AreaUnderROC: 0.8885224722921058
Root mean squared error: 0.35227611745523785
Relative absolute error: 40.85598551303479
Root relative squared error: 70.45522349104758
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6706753034651035
Weighted FMeasure: 0.8370280823155056
Iteration time: 80.0
Weighted AreaUnderPRC: 0.8810832800883026
Mean absolute error: 0.20427992756517394
Coverage of cases: 95.07246376811594
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 658.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1723857218735434
Kappa statistic: 0.6695063122241609
Training time: 77.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 74.4927536231884
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8375981555584155
Weighted AreaUnderROC: 0.9053851907255048
Root mean squared error: 0.34625703380552464
Relative absolute error: 40.315089651920225
Root relative squared error: 69.25140676110493
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6706701667548525
Weighted FMeasure: 0.8371817168338908
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8993384437559545
Mean absolute error: 0.20157544825960114
Coverage of cases: 96.23188405797102
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 730.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17112781747427777
Kappa statistic: 0.6699241440579512
Training time: 67.0
		
Time end:Mon Nov 27 15.58.10 EET 2017