Mon Nov 27 15.58.06 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.06 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 74.92753623188406
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8517654808959157
Weighted AreaUnderROC: 0.8983523965141612
Root mean squared error: 0.3507443484022557
Relative absolute error: 39.805091230534494
Root relative squared error: 70.14886968045114
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6911540064146006
Weighted FMeasure: 0.8410141965129179
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8798910430770883
Mean absolute error: 0.19902545615267248
Coverage of cases: 95.94202898550725
Instances selection time: 12.0
Test time: 3.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1456246447854504
Kappa statistic: 0.6827188362177076
Training time: 23.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 72.6086956521739
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8766774436553045
Weighted AreaUnderROC: 0.9351851851851852
Root mean squared error: 0.3285971601972829
Relative absolute error: 33.79203697667196
Root relative squared error: 65.71943203945658
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7396180136636497
Weighted FMeasure: 0.864100318980724
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9351260153531543
Mean absolute error: 0.1689601848833598
Coverage of cases: 97.97101449275362
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 60.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12050831202046036
Kappa statistic: 0.7292219828665898
Training time: 20.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 70.72463768115942
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8809486166007905
Weighted AreaUnderROC: 0.9368872549019608
Root mean squared error: 0.3229078111242828
Relative absolute error: 31.9752553533234
Root relative squared error: 64.58156222485655
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7495542347303089
Weighted FMeasure: 0.8699207062378419
Iteration time: 25.0
Weighted AreaUnderPRC: 0.93810694642318
Mean absolute error: 0.159876276766617
Coverage of cases: 97.97101449275362
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 85.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.11588874680306906
Kappa statistic: 0.7404063205417608
Training time: 21.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 72.89855072463769
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8804290629427243
Weighted AreaUnderROC: 0.9368191721132898
Root mean squared error: 0.31946095069961794
Relative absolute error: 33.59693948865271
Root relative squared error: 63.892190139923585
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7463735715089245
Weighted FMeasure: 0.8669693301049234
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9373556649908884
Mean absolute error: 0.16798469744326353
Coverage of cases: 97.97101449275362
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 109.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.11687091503267974
Kappa statistic: 0.7351557024131371
Training time: 20.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 77.68115942028986
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8766774436553045
Weighted AreaUnderROC: 0.9363085511982571
Root mean squared error: 0.31558633879504483
Relative absolute error: 35.91102651758928
Root relative squared error: 63.11726775900897
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7396180136636497
Weighted FMeasure: 0.864100318980724
Iteration time: 30.0
Weighted AreaUnderPRC: 0.935092106158688
Mean absolute error: 0.1795551325879464
Coverage of cases: 98.55072463768116
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 139.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12050831202046036
Kappa statistic: 0.7292219828665898
Training time: 26.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 75.79710144927536
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8788033957687641
Weighted AreaUnderROC: 0.9391680283224402
Root mean squared error: 0.3125639826246433
Relative absolute error: 35.15230651637691
Root relative squared error: 62.51279652492866
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7445749743269653
Weighted FMeasure: 0.8670120433290722
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9373483152334462
Mean absolute error: 0.17576153258188454
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 171.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.11819852941176472
Kappa statistic: 0.7348105073190296
Training time: 31.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 90.28985507246377
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8562927676597536
Weighted AreaUnderROC: 0.9218069172113289
Root mean squared error: 0.3319235610029195
Relative absolute error: 42.82319950249466
Root relative squared error: 66.38471220058389
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7061641311633703
Weighted FMeasure: 0.8526231983165018
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9110872971045578
Mean absolute error: 0.21411599751247332
Coverage of cases: 99.42028985507247
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 203.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.14169597186700766
Kappa statistic: 0.7034733808584862
Training time: 30.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 76.52173913043478
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8751119894598156
Weighted AreaUnderROC: 0.9325980392156863
Root mean squared error: 0.3140766761384091
Relative absolute error: 36.74966753338184
Root relative squared error: 62.81533522768182
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7378741890671672
Weighted FMeasure: 0.864139404292857
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9216721443463989
Mean absolute error: 0.1837483376669092
Coverage of cases: 98.26086956521739
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 234.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12183592639954532
Kappa statistic: 0.7288688236769502
Training time: 28.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 79.1304347826087
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.9300108932461874
Root mean squared error: 0.32215959256423116
Relative absolute error: 40.11013499483407
Root relative squared error: 64.43191851284624
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 56.0
Weighted AreaUnderPRC: 0.919843443022564
Mean absolute error: 0.20055067497417034
Coverage of cases: 98.55072463768116
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 290.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 53.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 98.84057971014492
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.926062091503268
Root mean squared error: 0.3306053644079752
Relative absolute error: 45.18681789938832
Root relative squared error: 66.12107288159504
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9135865804356531
Mean absolute error: 0.2259340894969416
Coverage of cases: 99.71014492753623
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 372.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 79.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.9251089324618736
Root mean squared error: 0.3324566125939138
Relative absolute error: 46.337602822485806
Root relative squared error: 66.49132251878275
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9135331167192952
Mean absolute error: 0.23168801411242904
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 537.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 157.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 99.85507246376811
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.9271854575163399
Root mean squared error: 0.3329079596113337
Relative absolute error: 46.18999641373473
Root relative squared error: 66.58159192226674
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 237.0
Weighted AreaUnderPRC: 0.9175908199006928
Mean absolute error: 0.23094998206867368
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 774.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 232.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 99.85507246376811
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.9286832788671026
Root mean squared error: 0.33349168610778546
Relative absolute error: 46.19988731869281
Root relative squared error: 66.6983372215571
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 310.0
Weighted AreaUnderPRC: 0.9207071922735586
Mean absolute error: 0.23099943659346406
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 1084.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 309.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 99.27536231884058
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8742429454779609
Weighted AreaUnderROC: 0.929976851851852
Root mean squared error: 0.3333399501500679
Relative absolute error: 45.48132735991165
Root relative squared error: 66.66799003001358
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7317089619657334
Weighted FMeasure: 0.8582073534747638
Iteration time: 244.0
Weighted AreaUnderPRC: 0.9228487859925366
Mean absolute error: 0.22740663679955825
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 1328.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.12380026285876669
Kappa statistic: 0.7184330185379502
Training time: 239.0
		
Time end:Mon Nov 27 15.58.08 EET 2017