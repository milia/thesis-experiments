Mon Nov 27 15.58.01 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.01 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.289855072463766
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7480016910019186
Weighted AreaUnderROC: 0.7975765487354363
Root mean squared error: 0.49872037492211413
Relative absolute error: 50.03010132670259
Root relative squared error: 99.74407498442282
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.4894711007177291
Weighted FMeasure: 0.7479075848915252
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7517189183228833
Mean absolute error: 0.25015050663351296
Coverage of cases: 75.3623188405797
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.25803850525717537
Kappa statistic: 0.4894626728580906
Training time: 22.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 54.63768115942029
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8374463355829815
Weighted AreaUnderROC: 0.8887527233115469
Root mean squared error: 0.3918139400792841
Relative absolute error: 34.060504062234784
Root relative squared error: 78.36278801585682
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6702646584796548
Weighted FMeasure: 0.8373065774804906
Iteration time: 17.0
Weighted AreaUnderPRC: 0.873742888395755
Mean absolute error: 0.1703025203111739
Coverage of cases: 87.2463768115942
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17050387183859053
Kappa statistic: 0.6698451758433303
Training time: 15.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 62.89855072463768
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8440780829262833
Weighted AreaUnderROC: 0.9050245098039216
Root mean squared error: 0.3536698017897514
Relative absolute error: 34.63788006026762
Root relative squared error: 70.73396035795028
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6839318381228269
Weighted FMeasure: 0.8436642799143722
Iteration time: 14.0
Weighted AreaUnderPRC: 0.889655604660124
Mean absolute error: 0.17318940030133811
Coverage of cases: 94.20289855072464
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 58.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1579186203466894
Kappa statistic: 0.6837441434100632
Training time: 12.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 65.21739130434783
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.907373366013072
Root mean squared error: 0.34605073667502617
Relative absolute error: 35.135708975246445
Root relative squared error: 69.21014733500523
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 18.0
Weighted AreaUnderPRC: 0.890398279474563
Mean absolute error: 0.17567854487623222
Coverage of cases: 95.07246376811594
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 76.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 15.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 70.28985507246377
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8352126134734831
Weighted AreaUnderROC: 0.8978417755991286
Root mean squared error: 0.35419721015718125
Relative absolute error: 37.79162151847059
Root relative squared error: 70.83944203143625
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6660561670848038
Weighted FMeasure: 0.8349341726965428
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8803088065172557
Mean absolute error: 0.18895810759235296
Coverage of cases: 95.3623188405797
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.16750319693094629
Kappa statistic: 0.6659532181623604
Training time: 16.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 71.8840579710145
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8560780474035654
Weighted AreaUnderROC: 0.9051947167755992
Root mean squared error: 0.34130195284501774
Relative absolute error: 37.330432285535025
Root relative squared error: 68.26039056900355
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7079938154727557
Weighted FMeasure: 0.8553157792288226
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8892257789924276
Mean absolute error: 0.18665216142767513
Coverage of cases: 96.23188405797102
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 124.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.14469664677465188
Kappa statistic: 0.7075577255619978
Training time: 21.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 69.1304347826087
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8587425944510991
Weighted AreaUnderROC: 0.904445806100218
Root mean squared error: 0.34496411093056034
Relative absolute error: 36.36160199056737
Root relative squared error: 68.99282218611206
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7135225380036757
Weighted FMeasure: 0.8581758599358831
Iteration time: 62.0
Weighted AreaUnderPRC: 0.8878224368435913
Mean absolute error: 0.18180800995283686
Coverage of cases: 95.65217391304348
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 186.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14238686416595625
Kappa statistic: 0.7132169576059849
Training time: 56.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 73.18840579710145
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8572219991265071
Weighted AreaUnderROC: 0.903390522875817
Root mean squared error: 0.34333550718016115
Relative absolute error: 37.77020015665972
Root relative squared error: 68.66710143603223
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7095373689749384
Weighted FMeasure: 0.8554280870801314
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8873667851792284
Mean absolute error: 0.18885100078329858
Coverage of cases: 96.52173913043478
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 219.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.14204141801648196
Kappa statistic: 0.7083291062189307
Training time: 30.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 81.44927536231884
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8596487124915219
Weighted AreaUnderROC: 0.9009395424836601
Root mean squared error: 0.3429994459162065
Relative absolute error: 40.24465648267556
Root relative squared error: 68.5998891832413
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7125645488929276
Weighted FMeasure: 0.855522549284364
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8854399887966052
Mean absolute error: 0.2012232824133778
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 255.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13805857487922704
Kappa statistic: 0.7094785772029103
Training time: 33.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 80.8695652173913
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8596487124915219
Weighted AreaUnderROC: 0.896003540305011
Root mean squared error: 0.3457161396323224
Relative absolute error: 40.367195361584194
Root relative squared error: 69.14322792646448
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7125645488929276
Weighted FMeasure: 0.855522549284364
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8787928561378745
Mean absolute error: 0.20183597680792098
Coverage of cases: 97.3913043478261
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 302.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13805857487922704
Kappa statistic: 0.7094785772029103
Training time: 44.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 82.7536231884058
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8524918613418119
Weighted AreaUnderROC: 0.8959014161220045
Root mean squared error: 0.34800570603509545
Relative absolute error: 41.356629721683674
Root relative squared error: 69.6011412070191
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6968959432575401
Weighted FMeasure: 0.8468689238099602
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8804312938336263
Mean absolute error: 0.20678314860841837
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 349.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.144987922705314
Kappa statistic: 0.6926529171499167
Training time: 44.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 80.72463768115942
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8548567821352482
Weighted AreaUnderROC: 0.8957652505446623
Root mean squared error: 0.34827174561157603
Relative absolute error: 40.88640090542897
Root relative squared error: 69.65434912231521
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7020945085864515
Weighted FMeasure: 0.8497558142604472
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8801821138598727
Mean absolute error: 0.20443200452714486
Coverage of cases: 97.3913043478261
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 388.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.14267814009661833
Kappa statistic: 0.6982541124230498
Training time: 36.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 97.10144927536231
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8524918613418119
Weighted AreaUnderROC: 0.8996459694989106
Root mean squared error: 0.3589256911524426
Relative absolute error: 45.58419067505181
Root relative squared error: 71.78513823048853
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6968959432575401
Weighted FMeasure: 0.8468689238099602
Iteration time: 95.0
Weighted AreaUnderPRC: 0.8821459596454606
Mean absolute error: 0.22792095337525903
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 483.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.144987922705314
Kappa statistic: 0.6926529171499167
Training time: 94.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 97.10144927536231
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8524918613418119
Weighted AreaUnderROC: 0.9055691721132898
Root mean squared error: 0.36014842177277145
Relative absolute error: 45.15994907921084
Root relative squared error: 72.02968435455429
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6968959432575401
Weighted FMeasure: 0.8468689238099602
Iteration time: 152.0
Weighted AreaUnderPRC: 0.886439050064778
Mean absolute error: 0.2257997453960542
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 635.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.144987922705314
Kappa statistic: 0.6926529171499167
Training time: 151.0
		
Time end:Mon Nov 27 15.58.02 EET 2017