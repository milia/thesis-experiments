Mon Nov 27 15.58.32 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.32 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 68.1159420289855
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8584699272602844
Weighted AreaUnderROC: 0.899605629972122
Root mean squared error: 0.3477462738256297
Relative absolute error: 36.63131241147155
Root relative squared error: 69.54925476512595
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7121389831717815
Weighted FMeasure: 0.8573285310977533
Iteration time: 11.0
Weighted AreaUnderPRC: 0.88732395520304
Mean absolute error: 0.18315656205735772
Coverage of cases: 94.78260869565217
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.15225264908852434
Kappa statistic: 0.7106348744458328
Training time: 10.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 74.05797101449275
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8444114528101803
Weighted AreaUnderROC: 0.8862446454069489
Root mean squared error: 0.3521962965777701
Relative absolute error: 40.12100095529954
Root relative squared error: 70.43925931555401
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6828474240164532
Weighted FMeasure: 0.8425183512140033
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8683366249106552
Mean absolute error: 0.2006050047764977
Coverage of cases: 94.4927536231884
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.16896952353360276
Kappa statistic: 0.6805007717372663
Training time: 16.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 75.3623188405797
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8434782608695652
Weighted AreaUnderROC: 0.8907323043448698
Root mean squared error: 0.35362455074560795
Relative absolute error: 39.73630605266386
Root relative squared error: 70.72491014912158
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6833140681308221
Weighted FMeasure: 0.8434782608695652
Iteration time: 19.0
Weighted AreaUnderPRC: 0.868013342715029
Mean absolute error: 0.1986815302633193
Coverage of cases: 95.3623188405797
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.16016419273874316
Kappa statistic: 0.6833140681308221
Training time: 18.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 74.78260869565217
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8523088455772114
Weighted AreaUnderROC: 0.8960359012715035
Root mean squared error: 0.3463082004028454
Relative absolute error: 39.597290206584326
Root relative squared error: 69.26164008056908
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7001922013685327
Weighted FMeasure: 0.851650428966119
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8732722099719912
Mean absolute error: 0.19798645103292165
Coverage of cases: 94.78260869565217
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.15692675182773067
Kappa statistic: 0.6992050602615607
Training time: 27.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 81.8840579710145
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8403823110633527
Weighted AreaUnderROC: 0.898211735908071
Root mean squared error: 0.3453069923382656
Relative absolute error: 42.32679706565292
Root relative squared error: 69.06139846765312
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6769449312052831
Weighted FMeasure: 0.8404167939449478
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8776091174559856
Mean absolute error: 0.2116339853282646
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 100.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.16501705290687765
Kappa statistic: 0.6768397567995639
Training time: 24.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 85.94202898550725
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8404909065075304
Weighted AreaUnderROC: 0.9008975317875842
Root mean squared error: 0.3513215133905956
Relative absolute error: 43.49335355737819
Root relative squared error: 70.26430267811912
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6772581785577253
Weighted FMeasure: 0.8405281884618793
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8788710317430353
Mean absolute error: 0.21746676778689097
Coverage of cases: 97.68115942028986
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 124.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.16375914850761197
Kappa statistic: 0.6772465173240801
Training time: 24.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 76.66666666666667
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8444829612220915
Weighted AreaUnderROC: 0.9175902631400014
Root mean squared error: 0.34590347108539926
Relative absolute error: 40.54457859879732
Root relative squared error: 69.18069421707985
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6849256238141056
Weighted FMeasure: 0.8437248478349253
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9052211769134987
Mean absolute error: 0.2027228929939866
Coverage of cases: 96.23188405797102
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 167.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15639047954094618
Kappa statistic: 0.6845046570702794
Training time: 42.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 75.5072463768116
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8424043725457437
Weighted AreaUnderROC: 0.9181682192153396
Root mean squared error: 0.3543016145313474
Relative absolute error: 42.56021699356591
Root relative squared error: 70.86032290626947
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6802025316558425
Weighted FMeasure: 0.8409194731124556
Iteration time: 37.0
Weighted AreaUnderPRC: 0.9096443921780888
Mean absolute error: 0.21280108496782954
Coverage of cases: 96.52173913043478
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 204.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1574696265112837
Kappa statistic: 0.6792650563715961
Training time: 36.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 75.65217391304348
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.848840579710145
Weighted AreaUnderROC: 0.9191541442850344
Root mean squared error: 0.3460767635594765
Relative absolute error: 41.989983472437245
Root relative squared error: 69.21535271189529
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6927374103078296
Weighted FMeasure: 0.8467480545441562
Iteration time: 37.0
Weighted AreaUnderPRC: 0.9113281449808132
Mean absolute error: 0.2099499173621862
Coverage of cases: 96.81159420289855
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 241.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.15027971497354606
Kappa statistic: 0.6913142567738667
Training time: 37.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 75.5072463768116
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8456108692722767
Weighted AreaUnderROC: 0.9202080641871216
Root mean squared error: 0.3484768730949173
Relative absolute error: 41.95780270224173
Root relative squared error: 69.69537461898346
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6864601744851099
Weighted FMeasure: 0.8438355076874224
Iteration time: 240.0
Weighted AreaUnderPRC: 0.9136464628505893
Mean absolute error: 0.20978901351120866
Coverage of cases: 96.23188405797102
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 481.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1538746707424149
Kappa statistic: 0.6852934220750702
Training time: 234.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 75.3623188405797
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8450011585990046
Weighted AreaUnderROC: 0.9215339634187801
Root mean squared error: 0.3420817340014449
Relative absolute error: 40.03481113811284
Root relative squared error: 68.41634680028899
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6856470103536297
Weighted FMeasure: 0.8437855194111465
Iteration time: 146.0
Weighted AreaUnderPRC: 0.9148455582011011
Mean absolute error: 0.2001740556905642
Coverage of cases: 96.23188405797102
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 627.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15513257514168055
Kappa statistic: 0.6848995331844937
Training time: 138.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 75.5072463768116
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8456108692722767
Weighted AreaUnderROC: 0.9233698238933842
Root mean squared error: 0.33932610331714486
Relative absolute error: 40.637058059290545
Root relative squared error: 67.86522066342897
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6864601744851099
Weighted FMeasure: 0.8438355076874224
Iteration time: 148.0
Weighted AreaUnderPRC: 0.919036268645585
Mean absolute error: 0.20318529029645274
Coverage of cases: 96.52173913043478
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 775.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1538746707424149
Kappa statistic: 0.6852934220750702
Training time: 143.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8464850766467361
Weighted AreaUnderROC: 0.9226898755694568
Root mean squared error: 0.33859064179423515
Relative absolute error: 45.886482151637786
Root relative squared error: 67.71812835884703
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6893858315617994
Weighted FMeasure: 0.8464237875710872
Iteration time: 266.0
Weighted AreaUnderPRC: 0.9104289357211082
Mean absolute error: 0.22943241075818893
Coverage of cases: 99.1304347826087
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 1041.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.15656923696987435
Kappa statistic: 0.689373991336108
Training time: 259.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 76.66666666666667
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8629492271408156
Weighted AreaUnderROC: 0.9365608213775753
Root mean squared error: 0.3341073246398873
Relative absolute error: 43.62139401186716
Root relative squared error: 66.82146492797746
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7191843997130298
Weighted FMeasure: 0.8583780944090653
Iteration time: 153.0
Weighted AreaUnderPRC: 0.9370716642641853
Mean absolute error: 0.2181069700593358
Coverage of cases: 97.3913043478261
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 1194.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13464198749880515
Kappa statistic: 0.715676875725314
Training time: 153.0
		
Time end:Mon Nov 27 15.58.34 EET 2017