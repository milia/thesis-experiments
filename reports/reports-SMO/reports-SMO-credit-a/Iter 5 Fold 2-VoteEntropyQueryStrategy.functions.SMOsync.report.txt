Mon Nov 27 15.58.52 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.52 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 83.47826086956522
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8334863624088577
Weighted AreaUnderROC: 0.8758169934640522
Root mean squared error: 0.36439639671820984
Relative absolute error: 47.75567338610598
Root relative squared error: 72.87927934364197
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6619380511372778
Weighted FMeasure: 0.8322371948596781
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8622415934895004
Mean absolute error: 0.2387783669305299
Coverage of cases: 97.68115942028986
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.16715775078147202
Kappa statistic: 0.6612149532710281
Training time: 12.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 97.2463768115942
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8459288537549408
Weighted AreaUnderROC: 0.8953567538126361
Root mean squared error: 0.3658680546268712
Relative absolute error: 48.497982902126395
Root relative squared error: 73.17361092537425
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6794739607514588
Weighted FMeasure: 0.835232894567933
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8765916514308087
Mean absolute error: 0.24248991451063198
Coverage of cases: 99.42028985507247
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.1515718243819267
Kappa statistic: 0.6711813393528969
Training time: 19.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 97.2463768115942
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8431244968140285
Weighted AreaUnderROC: 0.8975694444444444
Root mean squared error: 0.38267206724830294
Relative absolute error: 51.97875036549784
Root relative squared error: 76.53441344966059
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6583361990934509
Weighted FMeasure: 0.8139906915826648
Iteration time: 62.0
Weighted AreaUnderPRC: 0.8851277546343589
Mean absolute error: 0.2598937518274892
Coverage of cases: 99.42028985507247
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 99.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.15977461636828647
Kappa statistic: 0.6353186007333267
Training time: 59.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 99.56521739130434
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8431244968140285
Weighted AreaUnderROC: 0.9002927559912856
Root mean squared error: 0.39309496759967494
Relative absolute error: 52.23126465827905
Root relative squared error: 78.61899351993499
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6583361990934509
Weighted FMeasure: 0.8139906915826648
Iteration time: 43.0
Weighted AreaUnderPRC: 0.8862118154827134
Mean absolute error: 0.26115632329139526
Coverage of cases: 99.71014492753623
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 142.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.15977461636828647
Kappa statistic: 0.6353186007333267
Training time: 41.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 95.07246376811594
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8437260113971262
Weighted AreaUnderROC: 0.9005310457516339
Root mean squared error: 0.40232276243528364
Relative absolute error: 52.06565025792127
Root relative squared error: 80.46455248705672
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6563343588240287
Weighted FMeasure: 0.8108179250715193
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8851673411643356
Mean absolute error: 0.26032825128960635
Coverage of cases: 98.84057971014492
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 178.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.16075678459789713
Kappa statistic: 0.6303348004549727
Training time: 33.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 94.92753623188406
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8445387062566276
Weighted AreaUnderROC: 0.9059095860566448
Root mean squared error: 0.40937786167055085
Relative absolute error: 52.16001854037729
Root relative squared error: 81.87557233411017
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6545315656412999
Weighted FMeasure: 0.807607598233595
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8940112272947409
Mean absolute error: 0.2608000927018864
Coverage of cases: 98.84057971014492
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 216.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.1617389528275078
Kappa statistic: 0.6253701875616979
Training time: 35.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 72.46376811594203
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8359880377271681
Weighted AreaUnderROC: 0.9077818627450981
Root mean squared error: 0.42514977812095056
Relative absolute error: 53.91657927571711
Root relative squared error: 85.02995562419011
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.63205078145499
Weighted FMeasure: 0.7924240775965039
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8994221579747045
Mean absolute error: 0.26958289637858557
Coverage of cases: 86.95652173913044
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 251.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.17328786587098607
Kappa statistic: 0.5982779827798279
Training time: 32.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 98.98550724637681
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8566064659977703
Weighted AreaUnderROC: 0.9066584967320261
Root mean squared error: 0.38345301057631054
Relative absolute error: 48.46821607322192
Root relative squared error: 76.69060211526211
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6887744901688485
Weighted FMeasure: 0.8317060767861683
Iteration time: 40.0
Weighted AreaUnderPRC: 0.895640089148744
Mean absolute error: 0.2423410803661096
Coverage of cases: 99.71014492753623
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 291.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.14458830633702757
Kappa statistic: 0.6686537506209638
Training time: 38.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 98.98550724637681
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8566064659977703
Weighted AreaUnderROC: 0.9110498366013072
Root mean squared error: 0.3744015373452956
Relative absolute error: 46.53581769013616
Root relative squared error: 74.88030746905912
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6887744901688485
Weighted FMeasure: 0.8317060767861683
Iteration time: 62.0
Weighted AreaUnderPRC: 0.9069057961625309
Mean absolute error: 0.23267908845068083
Coverage of cases: 99.71014492753623
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 353.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.14458830633702757
Kappa statistic: 0.6686537506209638
Training time: 59.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 98.98550724637681
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8566064659977703
Weighted AreaUnderROC: 0.9102328431372548
Root mean squared error: 0.3682982020479527
Relative absolute error: 45.5527466559973
Root relative squared error: 73.65964040959054
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6887744901688485
Weighted FMeasure: 0.8317060767861683
Iteration time: 67.0
Weighted AreaUnderPRC: 0.905000103742376
Mean absolute error: 0.2277637332799865
Coverage of cases: 99.71014492753623
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 420.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.14458830633702757
Kappa statistic: 0.6686537506209638
Training time: 64.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 99.42028985507247
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8662419723924116
Weighted AreaUnderROC: 0.9165305010893248
Root mean squared error: 0.362939633102999
Relative absolute error: 43.35517945348142
Root relative squared error: 72.5879266205998
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7124406412110714
Weighted FMeasure: 0.8464723251249414
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9092510641069945
Mean absolute error: 0.2167758972674071
Coverage of cases: 99.71014492753623
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 493.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1330393932935493
Kappa statistic: 0.6962372290057313
Training time: 70.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 99.85507246376811
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8662419723924116
Weighted AreaUnderROC: 0.9133986928104575
Root mean squared error: 0.3576722758223398
Relative absolute error: 44.051990110515725
Root relative squared error: 71.53445516446796
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7124406412110714
Weighted FMeasure: 0.8464723251249414
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9054772579428454
Mean absolute error: 0.22025995055257863
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 581.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1330393932935493
Kappa statistic: 0.6962372290057313
Training time: 86.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 99.71014492753623
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8662419723924116
Weighted AreaUnderROC: 0.9226239106753814
Root mean squared error: 0.3521343098690832
Relative absolute error: 45.08456914979691
Root relative squared error: 70.42686197381663
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7124406412110714
Weighted FMeasure: 0.8464723251249414
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9208112779315653
Mean absolute error: 0.22542284574898455
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 674.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1330393932935493
Kappa statistic: 0.6962372290057313
Training time: 92.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 99.71014492753623
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8662419723924116
Weighted AreaUnderROC: 0.9179602396514163
Root mean squared error: 0.35121809052050995
Relative absolute error: 45.10329126846344
Root relative squared error: 70.24361810410198
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.7124406412110714
Weighted FMeasure: 0.8464723251249414
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9157941784873371
Mean absolute error: 0.22551645634231718
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 815.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1330393932935493
Kappa statistic: 0.6962372290057313
Training time: 140.0
		
Time end:Mon Nov 27 15.58.54 EET 2017