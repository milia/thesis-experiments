Mon Nov 27 15.58.19 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.58.19 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8307631724183985
Weighted AreaUnderROC: 0.898211735908071
Root mean squared error: 0.38874913551921114
Relative absolute error: 38.69113742605745
Root relative squared error: 77.74982710384222
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6449356343328536
Weighted FMeasure: 0.8146517384582562
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8828586469409483
Mean absolute error: 0.19345568713028727
Coverage of cases: 90.72463768115942
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.16843985364358685
Kappa statistic: 0.632526711713211
Training time: 13.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 75.07246376811594
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8513649607944566
Weighted AreaUnderROC: 0.9039912966614536
Root mean squared error: 0.34618611742589533
Relative absolute error: 38.383215981989935
Root relative squared error: 69.23722348517907
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6958605965638481
Weighted FMeasure: 0.846817122524091
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8933617325641235
Mean absolute error: 0.1919160799099497
Coverage of cases: 96.23188405797102
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.1465060017757491
Kappa statistic: 0.6924668247641151
Training time: 13.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 75.65217391304348
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8553721087380685
Weighted AreaUnderROC: 0.9038893044128645
Root mean squared error: 0.3419906374088406
Relative absolute error: 38.838307459381696
Root relative squared error: 68.39812748176813
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7053530691070542
Weighted FMeasure: 0.8525632936134319
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8930579608840444
Mean absolute error: 0.19419153729690847
Coverage of cases: 96.52173913043478
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.14308980343580846
Kappa statistic: 0.7033333895361581
Training time: 13.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 70.8695652173913
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8586751821829199
Weighted AreaUnderROC: 0.907051064119127
Root mean squared error: 0.34705778579517577
Relative absolute error: 38.22081189340205
Root relative squared error: 69.41155715903515
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7116927095094847
Weighted FMeasure: 0.855466288594833
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8993504682367269
Mean absolute error: 0.19110405946701026
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 68.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13949484766693965
Kappa statistic: 0.7093317157011425
Training time: 17.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 70.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.860554327356096
Weighted AreaUnderROC: 0.9141225266879718
Root mean squared error: 0.34064329399229043
Relative absolute error: 36.95670353785446
Root relative squared error: 68.12865879845809
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.7114898324543226
Weighted FMeasure: 0.8525744886605414
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9063062278876635
Mean absolute error: 0.1847835176892723
Coverage of cases: 95.94202898550725
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 91.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13680028143948017
Kappa statistic: 0.7051726738048559
Training time: 20.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8523937688367806
Weighted AreaUnderROC: 0.9181002243829468
Root mean squared error: 0.33581455684386935
Relative absolute error: 37.38534645366768
Root relative squared error: 67.16291136877388
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6970878652732949
Weighted FMeasure: 0.8468194290147367
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9083127219075022
Mean absolute error: 0.1869267322683384
Coverage of cases: 96.52173913043478
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.14524809737648345
Kappa statistic: 0.6928491038282574
Training time: 21.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 71.01449275362319
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8528903627282403
Weighted AreaUnderROC: 0.9204460461004963
Root mean squared error: 0.33296058378164195
Relative absolute error: 36.87560042433112
Root relative squared error: 66.5921167563284
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7000272788376504
Weighted FMeasure: 0.8496849401386263
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9111718952902365
Mean absolute error: 0.1843780021216556
Coverage of cases: 96.81159420289855
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.1454268548054116
Kappa statistic: 0.6977049843291881
Training time: 23.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 71.8840579710145
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8586751821829199
Weighted AreaUnderROC: 0.9229958523152241
Root mean squared error: 0.3293715427790374
Relative absolute error: 36.38659117937482
Root relative squared error: 65.87430855580749
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7116927095094847
Weighted FMeasure: 0.855466288594833
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9147869791557669
Mean absolute error: 0.18193295589687408
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 169.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13949484766693965
Kappa statistic: 0.7093317157011425
Training time: 28.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 73.76811594202898
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8629492271408156
Weighted AreaUnderROC: 0.9227578704018494
Root mean squared error: 0.327146240664691
Relative absolute error: 36.86144457849959
Root relative squared error: 65.4292481329382
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7191843997130298
Weighted FMeasure: 0.8583780944090653
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9177748224207789
Mean absolute error: 0.18430722289249793
Coverage of cases: 97.3913043478261
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 205.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13464198749880515
Kappa statistic: 0.715676875725314
Training time: 33.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 74.78260869565217
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8639916733392949
Weighted AreaUnderROC: 0.9227918678180458
Root mean squared error: 0.32639580170723914
Relative absolute error: 36.95278636811569
Root relative squared error: 65.27916034144783
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7204108845740065
Weighted FMeasure: 0.8583802268249452
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9190072533900261
Mean absolute error: 0.18476393184057846
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 248.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1333840830995395
Kappa statistic: 0.7160303035393323
Training time: 40.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 74.05797101449275
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8639916733392949
Weighted AreaUnderROC: 0.9230298497314204
Root mean squared error: 0.32649285535725986
Relative absolute error: 36.550506167564855
Root relative squared error: 65.29857107145197
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7204108845740065
Weighted FMeasure: 0.8583802268249452
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9188187977012661
Mean absolute error: 0.18275253083782428
Coverage of cases: 97.3913043478261
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 282.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1333840830995395
Kappa statistic: 0.7160303035393323
Training time: 31.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 75.5072463768116
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8616481233067675
Weighted AreaUnderROC: 0.9203100564357107
Root mean squared error: 0.3280425900451309
Relative absolute error: 37.55222447119552
Root relative squared error: 65.60851800902618
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7152294939616468
Weighted FMeasure: 0.8554874631138816
Iteration time: 45.0
Weighted AreaUnderPRC: 0.9166330921549879
Mean absolute error: 0.1877611223559776
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 327.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13572113446914266
Kappa statistic: 0.7104149879129734
Training time: 44.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 74.6376811594203
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8616481233067675
Weighted AreaUnderROC: 0.9203100564357107
Root mean squared error: 0.3280737569377071
Relative absolute error: 37.321059464204204
Root relative squared error: 65.61475138754142
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7152294939616468
Weighted FMeasure: 0.8554874631138816
Iteration time: 49.0
Weighted AreaUnderPRC: 0.9167058063929197
Mean absolute error: 0.18660529732102102
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 376.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13572113446914266
Kappa statistic: 0.7104149879129734
Training time: 48.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 74.78260869565217
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8709394714407502
Weighted AreaUnderROC: 0.9190181546202488
Root mean squared error: 0.32853469286389625
Relative absolute error: 37.276214905284135
Root relative squared error: 65.70693857277925
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7333969936971387
Weighted FMeasure: 0.8641535188084627
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9149607611465783
Mean absolute error: 0.1863810745264207
Coverage of cases: 97.3913043478261
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 419.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12619417156180188
Kappa statistic: 0.7279590638369263
Training time: 43.0
		
Time end:Mon Nov 27 15.58.20 EET 2017