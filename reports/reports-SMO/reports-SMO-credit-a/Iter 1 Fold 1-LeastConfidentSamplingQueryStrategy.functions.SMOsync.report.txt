Mon Nov 27 15.57.58 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.58 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 71.15942028985508
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8209383856655491
Weighted AreaUnderROC: 0.8787445533769062
Root mean squared error: 0.3835159227739741
Relative absolute error: 42.855727285661985
Root relative squared error: 76.70318455479482
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6370662570805954
Weighted FMeasure: 0.8205034324942792
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8766656795831416
Mean absolute error: 0.2142786364283099
Coverage of cases: 94.20289855072464
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.18170733873259448
Kappa statistic: 0.6368914239152577
Training time: 40.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 67.3913043478261
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8611028271942127
Weighted AreaUnderROC: 0.9109477124183006
Root mean squared error: 0.34446686943907856
Relative absolute error: 35.140308911900995
Root relative squared error: 68.89337388781571
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7185603890239081
Weighted FMeasure: 0.8609570716103836
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9079558987843656
Mean absolute error: 0.17570154455950499
Coverage of cases: 95.07246376811594
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 73.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.14140469593634555
Kappa statistic: 0.7185109637939827
Training time: 18.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 73.91304347826087
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8373411528974766
Weighted AreaUnderROC: 0.916428376906318
Root mean squared error: 0.33403661039679294
Relative absolute error: 37.33789211212811
Root relative squared error: 66.80732207935858
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6690913877633229
Weighted FMeasure: 0.8352129751211876
Iteration time: 36.0
Weighted AreaUnderPRC: 0.909668436377804
Mean absolute error: 0.18668946056064054
Coverage of cases: 97.3913043478261
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 109.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.1621927394146064
Kappa statistic: 0.6677143001976985
Training time: 30.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 71.59420289855072
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8471804802705287
Weighted AreaUnderROC: 0.9181304466230938
Root mean squared error: 0.3306757801407354
Relative absolute error: 36.14801956980815
Root relative squared error: 66.13515602814708
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6881106075177796
Weighted FMeasure: 0.8439409518213865
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9124120346696917
Mean absolute error: 0.18074009784904074
Coverage of cases: 97.3913043478261
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 134.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15128054845126457
Kappa statistic: 0.6858241424668623
Training time: 23.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8514434618462359
Weighted AreaUnderROC: 0.9249046840958605
Root mean squared error: 0.3252087241801805
Relative absolute error: 36.280300350715805
Root relative squared error: 65.0417448360361
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6978506320181755
Weighted FMeasure: 0.8496452105633368
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9212487939927938
Mean absolute error: 0.18140150175357903
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 161.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.14798859761295824
Kappa statistic: 0.6966622704676879
Training time: 26.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 70.28985507246377
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8409999823043302
Weighted AreaUnderROC: 0.9256535947712419
Root mean squared error: 0.3236273621608487
Relative absolute error: 35.60696137337083
Root relative squared error: 64.72547243216974
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6777789187246496
Weighted FMeasure: 0.8407259561106994
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9224434918133715
Mean absolute error: 0.17803480686685416
Coverage of cases: 97.10144927536231
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 187.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.16155601733447
Kappa statistic: 0.6776741578759617
Training time: 24.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 70.28985507246377
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8440780829262833
Weighted AreaUnderROC: 0.9269812091503269
Root mean squared error: 0.3270628011460115
Relative absolute error: 35.80854133372823
Root relative squared error: 65.4125602292023
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6839318381228269
Weighted FMeasure: 0.8436642799143722
Iteration time: 35.0
Weighted AreaUnderPRC: 0.9259761645005375
Mean absolute error: 0.17904270666864114
Coverage of cases: 97.68115942028986
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 222.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.1579186203466894
Kappa statistic: 0.6837441434100632
Training time: 31.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 72.17391304347827
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8482326232148043
Weighted AreaUnderROC: 0.9283769063180828
Root mean squared error: 0.3195182426912116
Relative absolute error: 36.33891868887079
Root relative squared error: 63.90364853824232
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6915798701954797
Weighted FMeasure: 0.8467279503105589
Iteration time: 65.0
Weighted AreaUnderPRC: 0.927686114902712
Mean absolute error: 0.18169459344435396
Coverage of cases: 97.68115942028986
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 287.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.15162599460073886
Kappa statistic: 0.6906248413786101
Training time: 57.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 76.95652173913044
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8596487124915219
Weighted AreaUnderROC: 0.926402505446623
Root mean squared error: 0.318383743836899
Relative absolute error: 38.091653512233435
Root relative squared error: 63.6767487673798
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7125645488929276
Weighted FMeasure: 0.855522549284364
Iteration time: 55.0
Weighted AreaUnderPRC: 0.9252211721808467
Mean absolute error: 0.19045826756116718
Coverage of cases: 98.26086956521739
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 342.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13805857487922704
Kappa statistic: 0.7094785772029103
Training time: 51.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 78.55072463768116
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8722323049001814
Weighted AreaUnderROC: 0.9262663398692811
Root mean squared error: 0.31622397805250724
Relative absolute error: 38.08328130930029
Root relative squared error: 63.24479561050145
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7371028342783617
Weighted FMeasure: 0.8670916818457802
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9259285332511855
Mean absolute error: 0.19041640654650147
Coverage of cases: 98.55072463768116
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 412.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.12483660130718953
Kappa statistic: 0.7330709456050056
Training time: 66.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 77.53623188405797
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8654348995486119
Weighted AreaUnderROC: 0.926402505446623
Root mean squared error: 0.31739698232680635
Relative absolute error: 37.71451992625914
Root relative squared error: 63.47939646536127
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7242359522717465
Weighted FMeasure: 0.8613016473129894
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9258798896112852
Mean absolute error: 0.18857259963129572
Coverage of cases: 98.55072463768116
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 480.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.1321113952827508
Kappa statistic: 0.7210994341147939
Training time: 65.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 77.82608695652173
Incorrectly Classified Instances: 13.623188405797102
Correctly Classified Instances: 86.3768115942029
Weighted Precision: 0.8688198757763975
Weighted AreaUnderROC: 0.9267429193899782
Root mean squared error: 0.31741836852131133
Relative absolute error: 37.69957078584819
Root relative squared error: 63.483673704262266
Weighted TruePositiveRate: 0.863768115942029
Weighted MatthewsCorrelation: 0.7306576510683751
Weighted FMeasure: 0.8641979311505871
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9260431703186838
Mean absolute error: 0.18849785392924093
Coverage of cases: 98.55072463768116
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 532.0
Weighted Recall: 0.863768115942029
Weighted FalsePositiveRate: 0.12847399829497017
Kappa statistic: 0.7270891189093663
Training time: 49.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 79.56521739130434
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.870049478892515
Weighted AreaUnderROC: 0.9265386710239651
Root mean squared error: 0.3166849210372799
Relative absolute error: 38.12622296053112
Root relative squared error: 63.336984207455984
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7296115818315805
Weighted FMeasure: 0.8612911725955203
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9258574526356129
Mean absolute error: 0.19063111480265563
Coverage of cases: 98.55072463768116
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 634.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.1268009377664109
Kappa statistic: 0.7225572979493367
Training time: 98.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 90.14492753623189
Incorrectly Classified Instances: 12.753623188405797
Correctly Classified Instances: 87.2463768115942
Weighted Precision: 0.8862877607947697
Weighted AreaUnderROC: 0.924325980392157
Root mean squared error: 0.31841525652768077
Relative absolute error: 41.638717553640014
Root relative squared error: 63.683051305536154
Weighted TruePositiveRate: 0.8724637681159421
Weighted MatthewsCorrelation: 0.758064249896765
Weighted FMeasure: 0.8727532722742746
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9234482261640768
Mean absolute error: 0.20819358776820007
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 723.0
Weighted Recall: 0.8724637681159421
Weighted FalsePositiveRate: 0.11092373543620346
Kappa statistic: 0.7466706718734355
Training time: 88.0
		
Time end:Mon Nov 27 15.58.00 EET 2017