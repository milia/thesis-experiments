Mon Nov 27 15.57.52 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.52 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 70.28985507246377
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8316538122761915
Weighted AreaUnderROC: 0.8963078806010742
Root mean squared error: 0.3563624446622988
Relative absolute error: 39.235942028139554
Root relative squared error: 71.27248893245977
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6589785634600754
Weighted FMeasure: 0.8315138975879711
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8866622926196764
Mean absolute error: 0.19617971014069777
Coverage of cases: 94.78260869565217
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.1758019202134841
Kappa statistic: 0.6585673821792991
Training time: 19.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 69.1304347826087
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.829182826427162
Weighted AreaUnderROC: 0.8958319167743252
Root mean squared error: 0.3643479191030554
Relative absolute error: 38.422943729015905
Root relative squared error: 72.86958382061108
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6487658782830954
Weighted FMeasure: 0.824133249445346
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8843932044108221
Mean absolute error: 0.19211471864507954
Coverage of cases: 94.4927536231884
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.19179716254608126
Kappa statistic: 0.6431895749301892
Training time: 14.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 74.20289855072464
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8197863457651705
Weighted AreaUnderROC: 0.8896103896103896
Root mean squared error: 0.3817182060694576
Relative absolute error: 44.94431945807725
Root relative squared error: 76.34364121389152
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6157225431794425
Weighted FMeasure: 0.8002981164078359
Iteration time: 20.0
Weighted AreaUnderPRC: 0.879046894828924
Mean absolute error: 0.22472159729038627
Coverage of cases: 95.65217391304348
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22450927932375692
Kappa statistic: 0.5966742859136991
Training time: 15.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 73.04347826086956
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8061984322375843
Weighted AreaUnderROC: 0.8802950975725846
Root mean squared error: 0.4008692262000748
Relative absolute error: 47.37777939571547
Root relative squared error: 80.17384524001497
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5828512464383532
Weighted FMeasure: 0.7809450364173004
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8672558841134969
Mean absolute error: 0.23688889697857737
Coverage of cases: 94.20289855072464
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2460790139369698
Kappa statistic: 0.558846713027028
Training time: 16.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7978203403351971
Weighted AreaUnderROC: 0.886686611817502
Root mean squared error: 0.39524431314844755
Relative absolute error: 46.72042895298763
Root relative squared error: 79.04886262968951
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5687239437335891
Weighted FMeasure: 0.7754656255722684
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8748214930620588
Mean absolute error: 0.23360214476493815
Coverage of cases: 95.07246376811594
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 106.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2507531166761761
Kappa statistic: 0.5473470601612932
Training time: 19.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 71.15942028985508
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8094511641422395
Weighted AreaUnderROC: 0.8903243353505134
Root mean squared error: 0.39990088466159074
Relative absolute error: 45.99236210030477
Root relative squared error: 79.98017693231814
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.5964121267062948
Weighted FMeasure: 0.7915758114435036
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8780314895952056
Mean absolute error: 0.22996181050152384
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 132.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.23277833783183205
Kappa statistic: 0.5788867964009207
Training time: 22.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 72.17391304347827
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8084476514672808
Weighted AreaUnderROC: 0.885972666077378
Root mean squared error: 0.4031386568230594
Relative absolute error: 47.38984743640173
Root relative squared error: 80.62773136461188
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5883302134573432
Weighted FMeasure: 0.784198690410092
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8760937002852486
Mean absolute error: 0.23694923718200864
Coverage of cases: 94.20289855072464
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 153.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24248405816810095
Kappa statistic: 0.5651717256590694
Training time: 17.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 70.0
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7950203305512579
Weighted AreaUnderROC: 0.8808730536479229
Root mean squared error: 0.41635962440825414
Relative absolute error: 48.493221892138706
Root relative squared error: 83.27192488165083
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5554233387585721
Weighted FMeasure: 0.7644954668403127
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8685515263519332
Mean absolute error: 0.24246610946069352
Coverage of cases: 93.04347826086956
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 184.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26405379278131386
Kappa statistic: 0.5270982707718261
Training time: 22.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 71.44927536231884
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8018695259416363
Weighted AreaUnderROC: 0.8861426531583599
Root mean squared error: 0.4234780012920126
Relative absolute error: 50.74277041662858
Root relative squared error: 84.69560025840252
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5646992656761204
Weighted FMeasure: 0.7665970218552051
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8724912652045516
Mean absolute error: 0.2537138520831429
Coverage of cases: 94.20289855072464
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 223.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2629746458109763
Kappa statistic: 0.5322486926205695
Training time: 35.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8126963259907782
Weighted AreaUnderROC: 0.9020194465220643
Root mean squared error: 0.3915968202509003
Relative absolute error: 46.71211805949205
Root relative squared error: 78.31936405018006
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5954567930796039
Weighted FMeasure: 0.786946542268881
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8942335472311923
Mean absolute error: 0.23356059029746024
Coverage of cases: 95.07246376811594
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 250.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.2401470067984978
Kappa statistic: 0.570933104450945
Training time: 23.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 77.2463768115942
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8150502832914621
Weighted AreaUnderROC: 0.9014754878629224
Root mean squared error: 0.3712650601594728
Relative absolute error: 46.20788391576423
Root relative squared error: 74.25301203189456
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6118583486849446
Weighted FMeasure: 0.801498199433764
Iteration time: 43.0
Weighted AreaUnderPRC: 0.898887012495539
Mean absolute error: 0.23103941957882115
Coverage of cases: 96.23188405797102
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 293.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22073556612595993
Kappa statistic: 0.5982305807102012
Training time: 39.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 76.52173913043478
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8214794842027736
Weighted AreaUnderROC: 0.8972598082545725
Root mean squared error: 0.37062771273421263
Relative absolute error: 45.17446263901315
Root relative squared error: 74.12554254684252
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6316779592590545
Weighted FMeasure: 0.8149339244131071
Iteration time: 37.0
Weighted AreaUnderPRC: 0.8930230731436265
Mean absolute error: 0.22587231319506576
Coverage of cases: 95.94202898550725
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 330.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.2025820298526877
Kappa statistic: 0.6246308481425834
Training time: 34.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 76.52173913043478
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.841710928429059
Weighted AreaUnderROC: 0.906133133881825
Root mean squared error: 0.35783314972547975
Relative absolute error: 42.902802106721694
Root relative squared error: 71.56662994509595
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6770525814477786
Weighted FMeasure: 0.8395107043240787
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9028968316572447
Mean absolute error: 0.21451401053360847
Coverage of cases: 96.52173913043478
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 381.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.17256447930247154
Kappa statistic: 0.674377499013265
Training time: 47.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 76.66666666666667
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8404909065075304
Weighted AreaUnderROC: 0.9079009995240362
Root mean squared error: 0.34812944317378514
Relative absolute error: 42.222238222097054
Root relative squared error: 69.62588863475703
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6772581785577253
Weighted FMeasure: 0.8405281884618793
Iteration time: 104.0
Weighted AreaUnderPRC: 0.9047579225104104
Mean absolute error: 0.21111119111048526
Coverage of cases: 97.68115942028986
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 485.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.16375914850761197
Kappa statistic: 0.6772465173240801
Training time: 101.0
		
Time end:Mon Nov 27 15.57.53 EET 2017