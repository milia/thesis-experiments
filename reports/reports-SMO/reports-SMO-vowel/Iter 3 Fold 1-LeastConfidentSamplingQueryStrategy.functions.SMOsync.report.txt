Fri Dec 01 13.31.23 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.31.23 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 37.06152433425169
Incorrectly Classified Instances: 72.32323232323232
Correctly Classified Instances: 27.67676767676768
Weighted Precision: 0.3241218270416629
Weighted AreaUnderROC: 0.7472098765432098
Root mean squared error: 0.2966040140957153
Relative absolute error: 81.78092899713086
Root relative squared error: 103.1738672460259
Weighted TruePositiveRate: 0.2767676767676768
Weighted MatthewsCorrelation: 0.22192287199579402
Weighted FMeasure: 0.2820532669156388
Iteration time: 626.0
Weighted AreaUnderPRC: 0.3218830805737147
Mean absolute error: 0.13517508925145683
Coverage of cases: 69.8989898989899
Instances selection time: 135.0
Test time: 197.0
Accumulative iteration time: 626.0
Weighted Recall: 0.2767676767676768
Weighted FalsePositiveRate: 0.07232323232323232
Kappa statistic: 0.20444444444444446
Training time: 491.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 43.21395775941239
Incorrectly Classified Instances: 68.68686868686869
Correctly Classified Instances: 31.31313131313131
Weighted Precision: 0.33236737463034055
Weighted AreaUnderROC: 0.772246913580247
Root mean squared error: 0.28603879443523783
Relative absolute error: 80.4186384425059
Root relative squared error: 99.49874985424495
Weighted TruePositiveRate: 0.31313131313131315
Weighted MatthewsCorrelation: 0.25044229891334885
Weighted FMeasure: 0.31172546486628805
Iteration time: 580.0
Weighted AreaUnderPRC: 0.338132435750717
Mean absolute error: 0.13292336932645687
Coverage of cases: 76.96969696969697
Instances selection time: 106.0
Test time: 153.0
Accumulative iteration time: 1206.0
Weighted Recall: 0.31313131313131315
Weighted FalsePositiveRate: 0.06868686868686869
Kappa statistic: 0.24444444444444446
Training time: 474.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 41.92837465564754
Incorrectly Classified Instances: 63.63636363636363
Correctly Classified Instances: 36.36363636363637
Weighted Precision: 0.3765361646147285
Weighted AreaUnderROC: 0.8015488215488216
Root mean squared error: 0.27702873547873763
Relative absolute error: 77.45924804429508
Root relative squared error: 96.3645959572015
Weighted TruePositiveRate: 0.36363636363636365
Weighted MatthewsCorrelation: 0.30385751424831914
Weighted FMeasure: 0.36174438584373775
Iteration time: 595.0
Weighted AreaUnderPRC: 0.3840876868531357
Mean absolute error: 0.1280318149492489
Coverage of cases: 83.63636363636364
Instances selection time: 113.0
Test time: 168.0
Accumulative iteration time: 1801.0
Weighted Recall: 0.36363636363636365
Weighted FalsePositiveRate: 0.06363636363636363
Kappa statistic: 0.3
Training time: 482.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 39.412304866850505
Incorrectly Classified Instances: 61.01010101010101
Correctly Classified Instances: 38.98989898989899
Weighted Precision: 0.4046305858570258
Weighted AreaUnderROC: 0.8218316498316498
Root mean squared error: 0.2681543959073885
Relative absolute error: 74.02366833969427
Root relative squared error: 93.27765212192668
Weighted TruePositiveRate: 0.3898989898989899
Weighted MatthewsCorrelation: 0.3345395910107107
Weighted FMeasure: 0.39210219861291123
Iteration time: 587.0
Weighted AreaUnderPRC: 0.43425852916404295
Mean absolute error: 0.12235317080941281
Coverage of cases: 83.83838383838383
Instances selection time: 112.0
Test time: 171.0
Accumulative iteration time: 2388.0
Weighted Recall: 0.3898989898989899
Weighted FalsePositiveRate: 0.06101010101010101
Kappa statistic: 0.32888888888888895
Training time: 475.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 37.979797979798114
Incorrectly Classified Instances: 56.96969696969697
Correctly Classified Instances: 43.03030303030303
Weighted Precision: 0.4292433612304891
Weighted AreaUnderROC: 0.834388327721661
Root mean squared error: 0.26463635237159766
Relative absolute error: 71.30882100777012
Root relative squared error: 92.05389876904627
Weighted TruePositiveRate: 0.4303030303030303
Weighted MatthewsCorrelation: 0.3703768987007258
Weighted FMeasure: 0.4228130672106246
Iteration time: 603.0
Weighted AreaUnderPRC: 0.431960779510297
Mean absolute error: 0.11786581984755466
Coverage of cases: 85.85858585858585
Instances selection time: 122.0
Test time: 193.0
Accumulative iteration time: 2991.0
Weighted Recall: 0.4303030303030303
Weighted FalsePositiveRate: 0.056969696969696976
Kappa statistic: 0.3733333333333333
Training time: 481.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 35.05968778696069
Incorrectly Classified Instances: 54.343434343434346
Correctly Classified Instances: 45.656565656565654
Weighted Precision: 0.4740022741949917
Weighted AreaUnderROC: 0.8576475869809204
Root mean squared error: 0.25974592922079626
Relative absolute error: 67.52186466286246
Root relative squared error: 90.35276242240583
Weighted TruePositiveRate: 0.45656565656565656
Weighted MatthewsCorrelation: 0.4071256910449887
Weighted FMeasure: 0.45455528835836984
Iteration time: 635.0
Weighted AreaUnderPRC: 0.483849017937418
Mean absolute error: 0.11160638787250066
Coverage of cases: 85.85858585858585
Instances selection time: 141.0
Test time: 245.0
Accumulative iteration time: 3626.0
Weighted Recall: 0.45656565656565656
Weighted FalsePositiveRate: 0.054343434343434346
Kappa statistic: 0.40222222222222226
Training time: 494.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 34.765840220385826
Incorrectly Classified Instances: 54.54545454545455
Correctly Classified Instances: 45.45454545454545
Weighted Precision: 0.4649133328169714
Weighted AreaUnderROC: 0.8595510662177328
Root mean squared error: 0.25862458423964735
Relative absolute error: 66.86883225826868
Root relative squared error: 89.96270196225062
Weighted TruePositiveRate: 0.45454545454545453
Weighted MatthewsCorrelation: 0.4020553424855152
Weighted FMeasure: 0.45044590233982307
Iteration time: 647.0
Weighted AreaUnderPRC: 0.4882297524660818
Mean absolute error: 0.11052699546821342
Coverage of cases: 84.84848484848484
Instances selection time: 142.0
Test time: 248.0
Accumulative iteration time: 4273.0
Weighted Recall: 0.45454545454545453
Weighted FalsePositiveRate: 0.054545454545454536
Kappa statistic: 0.4
Training time: 505.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 30.927456382001946
Incorrectly Classified Instances: 51.515151515151516
Correctly Classified Instances: 48.484848484848484
Weighted Precision: 0.500546201613595
Weighted AreaUnderROC: 0.8761795735129069
Root mean squared error: 0.25695755081474414
Relative absolute error: 64.79085356115098
Root relative squared error: 89.38282348083483
Weighted TruePositiveRate: 0.48484848484848486
Weighted MatthewsCorrelation: 0.43669234207049495
Weighted FMeasure: 0.4796847024279554
Iteration time: 691.0
Weighted AreaUnderPRC: 0.489847396652062
Mean absolute error: 0.10709231993578744
Coverage of cases: 84.24242424242425
Instances selection time: 181.0
Test time: 313.0
Accumulative iteration time: 4964.0
Weighted Recall: 0.48484848484848486
Weighted FalsePositiveRate: 0.051515151515151514
Kappa statistic: 0.43333333333333335
Training time: 510.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 31.386593204775128
Incorrectly Classified Instances: 49.09090909090909
Correctly Classified Instances: 50.90909090909091
Weighted Precision: 0.51580746736284
Weighted AreaUnderROC: 0.8851851851851851
Root mean squared error: 0.24865722044777624
Relative absolute error: 62.17533120616094
Root relative squared error: 86.49554905877206
Weighted TruePositiveRate: 0.509090909090909
Weighted MatthewsCorrelation: 0.45915859314427815
Weighted FMeasure: 0.5005525339648066
Iteration time: 692.0
Weighted AreaUnderPRC: 0.5234186064782135
Mean absolute error: 0.10276914248952287
Coverage of cases: 85.65656565656566
Instances selection time: 172.0
Test time: 304.0
Accumulative iteration time: 5656.0
Weighted Recall: 0.509090909090909
Weighted FalsePositiveRate: 0.0490909090909091
Kappa statistic: 0.4599999999999999
Training time: 520.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 34.8576675849405
Incorrectly Classified Instances: 43.03030303030303
Correctly Classified Instances: 56.96969696969697
Weighted Precision: 0.5818259934186506
Weighted AreaUnderROC: 0.8979708193041527
Root mean squared error: 0.23814479242338804
Relative absolute error: 60.520701572277304
Root relative squared error: 82.83879526625049
Weighted TruePositiveRate: 0.5696969696969697
Weighted MatthewsCorrelation: 0.5283834700766021
Weighted FMeasure: 0.5633327590940705
Iteration time: 677.0
Weighted AreaUnderPRC: 0.5706209217590099
Mean absolute error: 0.10003421747483916
Coverage of cases: 89.6969696969697
Instances selection time: 152.0
Test time: 272.0
Accumulative iteration time: 6333.0
Weighted Recall: 0.5696969696969697
Weighted FalsePositiveRate: 0.04303030303030302
Kappa statistic: 0.5266666666666666
Training time: 525.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 32.13957759412318
Incorrectly Classified Instances: 44.44444444444444
Correctly Classified Instances: 55.55555555555556
Weighted Precision: 0.5707177381013457
Weighted AreaUnderROC: 0.9017239057239058
Root mean squared error: 0.23929590432443099
Relative absolute error: 60.21677959309756
Root relative squared error: 83.23921016564292
Weighted TruePositiveRate: 0.5555555555555556
Weighted MatthewsCorrelation: 0.5158148426521902
Weighted FMeasure: 0.5550442161708505
Iteration time: 678.0
Weighted AreaUnderPRC: 0.5700355325460627
Mean absolute error: 0.09953186709602967
Coverage of cases: 89.4949494949495
Instances selection time: 139.0
Test time: 273.0
Accumulative iteration time: 7011.0
Weighted Recall: 0.5555555555555556
Weighted FalsePositiveRate: 0.04444444444444444
Kappa statistic: 0.5111111111111111
Training time: 539.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 31.331496786042365
Incorrectly Classified Instances: 44.64646464646464
Correctly Classified Instances: 55.35353535353536
Weighted Precision: 0.55889200709275
Weighted AreaUnderROC: 0.90275645342312
Root mean squared error: 0.24325580934620647
Relative absolute error: 61.10507286540334
Root relative squared error: 84.61666527618532
Weighted TruePositiveRate: 0.5535353535353535
Weighted MatthewsCorrelation: 0.5095680527155909
Weighted FMeasure: 0.5506045608410319
Iteration time: 680.0
Weighted AreaUnderPRC: 0.5413645846278095
Mean absolute error: 0.10100012043868387
Coverage of cases: 89.4949494949495
Instances selection time: 129.0
Test time: 274.0
Accumulative iteration time: 7691.0
Weighted Recall: 0.5535353535353535
Weighted FalsePositiveRate: 0.04464646464646465
Kappa statistic: 0.5088888888888888
Training time: 551.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 33.92102846648318
Incorrectly Classified Instances: 45.45454545454545
Correctly Classified Instances: 54.54545454545455
Weighted Precision: 0.5589935951778043
Weighted AreaUnderROC: 0.9114118967452302
Root mean squared error: 0.2405955589697814
Relative absolute error: 62.16157487731412
Root relative squared error: 83.69129573924472
Weighted TruePositiveRate: 0.5454545454545454
Weighted MatthewsCorrelation: 0.5038664500300494
Weighted FMeasure: 0.5442565832944658
Iteration time: 654.0
Weighted AreaUnderPRC: 0.5499646924291889
Mean absolute error: 0.10274640475589177
Coverage of cases: 91.31313131313131
Instances selection time: 84.0
Test time: 221.0
Accumulative iteration time: 8345.0
Weighted Recall: 0.5454545454545454
Weighted FalsePositiveRate: 0.045454545454545456
Kappa statistic: 0.49999999999999994
Training time: 570.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.73737373737393
Incorrectly Classified Instances: 43.03030303030303
Correctly Classified Instances: 56.96969696969697
Weighted Precision: 0.5779088783638208
Weighted AreaUnderROC: 0.9206778900112235
Root mean squared error: 0.2328482597178453
Relative absolute error: 58.90115707460989
Root relative squared error: 80.99639349063071
Weighted TruePositiveRate: 0.5696969696969697
Weighted MatthewsCorrelation: 0.5286345274221584
Weighted FMeasure: 0.5681268100963875
Iteration time: 655.0
Weighted AreaUnderPRC: 0.5800564603444127
Mean absolute error: 0.09735728442084342
Coverage of cases: 91.71717171717172
Instances selection time: 83.0
Test time: 234.0
Accumulative iteration time: 9000.0
Weighted Recall: 0.5696969696969697
Weighted FalsePositiveRate: 0.04303030303030304
Kappa statistic: 0.5266666666666666
Training time: 572.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 33.48025711662095
Incorrectly Classified Instances: 42.22222222222222
Correctly Classified Instances: 57.77777777777778
Weighted Precision: 0.588898181874523
Weighted AreaUnderROC: 0.9277485970819304
Root mean squared error: 0.22714020675964103
Relative absolute error: 56.727845876981284
Root relative squared error: 79.01084417182409
Weighted TruePositiveRate: 0.5777777777777777
Weighted MatthewsCorrelation: 0.5399850661963377
Weighted FMeasure: 0.5803967358912234
Iteration time: 662.0
Weighted AreaUnderPRC: 0.5961431598493782
Mean absolute error: 0.09376503450740768
Coverage of cases: 92.52525252525253
Instances selection time: 87.0
Test time: 258.0
Accumulative iteration time: 9662.0
Weighted Recall: 0.5777777777777777
Weighted FalsePositiveRate: 0.04222222222222222
Kappa statistic: 0.5355555555555555
Training time: 575.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.99449035812693
Incorrectly Classified Instances: 39.5959595959596
Correctly Classified Instances: 60.4040404040404
Weighted Precision: 0.6138794574999069
Weighted AreaUnderROC: 0.9349225589225587
Root mean squared error: 0.21959208281606932
Relative absolute error: 54.97943028986646
Root relative squared error: 76.38522516230081
Weighted TruePositiveRate: 0.604040404040404
Weighted MatthewsCorrelation: 0.5680898977404287
Weighted FMeasure: 0.6056533516127649
Iteration time: 675.0
Weighted AreaUnderPRC: 0.6288736182086537
Mean absolute error: 0.09087509138820961
Coverage of cases: 95.15151515151516
Instances selection time: 79.0
Test time: 251.0
Accumulative iteration time: 10337.0
Weighted Recall: 0.604040404040404
Weighted FalsePositiveRate: 0.0395959595959596
Kappa statistic: 0.5644444444444444
Training time: 596.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 33.92102846648321
Incorrectly Classified Instances: 38.78787878787879
Correctly Classified Instances: 61.21212121212121
Weighted Precision: 0.6207499190293019
Weighted AreaUnderROC: 0.9383703703703705
Root mean squared error: 0.21574502894728373
Relative absolute error: 54.056620563551085
Root relative squared error: 75.04702538656107
Weighted TruePositiveRate: 0.6121212121212121
Weighted MatthewsCorrelation: 0.5761991653776917
Weighted FMeasure: 0.6125993987392901
Iteration time: 683.0
Weighted AreaUnderPRC: 0.6490450691606978
Mean absolute error: 0.0893497860554569
Coverage of cases: 95.55555555555556
Instances selection time: 69.0
Test time: 258.0
Accumulative iteration time: 11020.0
Weighted Recall: 0.6121212121212121
Weighted FalsePositiveRate: 0.03878787878787879
Kappa statistic: 0.5733333333333333
Training time: 614.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 33.49862258953186
Incorrectly Classified Instances: 35.95959595959596
Correctly Classified Instances: 64.04040404040404
Weighted Precision: 0.6435758235459159
Weighted AreaUnderROC: 0.9377867564534231
Root mean squared error: 0.21237736775194763
Relative absolute error: 52.67122722984981
Root relative squared error: 73.87558261240808
Weighted TruePositiveRate: 0.6404040404040404
Weighted MatthewsCorrelation: 0.6050400643147926
Weighted FMeasure: 0.6393235912495271
Iteration time: 687.0
Weighted AreaUnderPRC: 0.6490732073439086
Mean absolute error: 0.08705987971876057
Coverage of cases: 95.35353535353535
Instances selection time: 60.0
Test time: 265.0
Accumulative iteration time: 11707.0
Weighted Recall: 0.6404040404040404
Weighted FalsePositiveRate: 0.03595959595959596
Kappa statistic: 0.6044444444444445
Training time: 627.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 32.855831037649395
Incorrectly Classified Instances: 34.94949494949495
Correctly Classified Instances: 65.05050505050505
Weighted Precision: 0.6551600421571531
Weighted AreaUnderROC: 0.9417957351290686
Root mean squared error: 0.20678057556997073
Relative absolute error: 50.57712781084707
Root relative squared error: 71.92873541498432
Weighted TruePositiveRate: 0.6505050505050505
Weighted MatthewsCorrelation: 0.6167500372001153
Weighted FMeasure: 0.6498343126030258
Iteration time: 686.0
Weighted AreaUnderPRC: 0.6859761983141405
Mean absolute error: 0.08359855836503702
Coverage of cases: 95.15151515151516
Instances selection time: 47.0
Test time: 294.0
Accumulative iteration time: 12393.0
Weighted Recall: 0.6505050505050505
Weighted FalsePositiveRate: 0.03494949494949495
Kappa statistic: 0.6155555555555555
Training time: 639.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 33.774104683195766
Incorrectly Classified Instances: 36.16161616161616
Correctly Classified Instances: 63.83838383838384
Weighted Precision: 0.6412055288294222
Weighted AreaUnderROC: 0.946280583613917
Root mean squared error: 0.2067191741707302
Relative absolute error: 51.25791027886815
Root relative squared error: 71.90737690494097
Weighted TruePositiveRate: 0.6383838383838384
Weighted MatthewsCorrelation: 0.6024909124542672
Weighted FMeasure: 0.6367027565917481
Iteration time: 676.0
Weighted AreaUnderPRC: 0.6906244356087712
Mean absolute error: 0.08472381864275781
Coverage of cases: 97.37373737373737
Instances selection time: 22.0
Test time: 285.0
Accumulative iteration time: 13069.0
Weighted Recall: 0.6383838383838384
Weighted FalsePositiveRate: 0.036161616161616165
Kappa statistic: 0.6022222222222222
Training time: 654.0
		
Time end:Fri Dec 01 13.31.41 EET 2017