Fri Dec 01 13.22.23 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.22.23 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 36.03673094582194
Incorrectly Classified Instances: 69.13131313131314
Correctly Classified Instances: 30.868686868686872
Weighted Precision: 0.36165728275925096
Weighted AreaUnderROC: 0.7548426487093154
Root mean squared error: 0.2962981225298428
Relative absolute error: 78.76072093058045
Root relative squared error: 103.06746269885473
Weighted TruePositiveRate: 0.3086868686868687
Weighted MatthewsCorrelation: 0.2562383983194222
Weighted FMeasure: 0.31108384195152683
Iteration time: 958.1
Weighted AreaUnderPRC: 0.3269424454502943
Mean absolute error: 0.13018300980261316
Coverage of cases: 69.11111111111111
Instances selection time: 269.8
Test time: 300.2
Accumulative iteration time: 958.1
Weighted Recall: 0.3086868686868687
Weighted FalsePositiveRate: 0.06913131313131313
Kappa statistic: 0.23955555555555558
Training time: 688.3
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 34.92011019283757
Incorrectly Classified Instances: 65.33333333333334
Correctly Classified Instances: 34.666666666666664
Weighted Precision: 0.38569989238096253
Weighted AreaUnderROC: 0.7814038159371492
Root mean squared error: 0.28680474800812794
Relative absolute error: 75.55205425308961
Root relative squared error: 99.76518722019537
Weighted TruePositiveRate: 0.3466666666666666
Weighted MatthewsCorrelation: 0.2931579774834993
Weighted FMeasure: 0.34690423060404896
Iteration time: 878.5
Weighted AreaUnderPRC: 0.36303981927756135
Mean absolute error: 0.1248794285175043
Coverage of cases: 73.43434343434342
Instances selection time: 229.5
Test time: 259.2
Accumulative iteration time: 1836.6
Weighted Recall: 0.3466666666666666
Weighted FalsePositiveRate: 0.06533333333333333
Kappa statistic: 0.2813333333333333
Training time: 649.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 34.36547291092756
Incorrectly Classified Instances: 62.64646464646465
Correctly Classified Instances: 37.35353535353535
Weighted Precision: 0.4057077977073768
Weighted AreaUnderROC: 0.803358024691358
Root mean squared error: 0.2809947502961093
Relative absolute error: 73.3375520832372
Root relative squared error: 97.74417636345733
Weighted TruePositiveRate: 0.37353535353535355
Weighted MatthewsCorrelation: 0.3202338042052912
Weighted FMeasure: 0.3722931535645716
Iteration time: 768.8
Weighted AreaUnderPRC: 0.39148136310992365
Mean absolute error: 0.12121909435245896
Coverage of cases: 75.45454545454547
Instances selection time: 191.7
Test time: 262.4
Accumulative iteration time: 2605.4
Weighted Recall: 0.37353535353535355
Weighted FalsePositiveRate: 0.06264646464646464
Kappa statistic: 0.31088888888888894
Training time: 577.1
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 35.197428833792614
Incorrectly Classified Instances: 60.16161616161615
Correctly Classified Instances: 39.83838383838385
Weighted Precision: 0.4275993136437249
Weighted AreaUnderROC: 0.8258742985409653
Root mean squared error: 0.2738169392513333
Relative absolute error: 71.8650172512946
Root relative squared error: 95.24737089671878
Weighted TruePositiveRate: 0.3983838383838384
Weighted MatthewsCorrelation: 0.3469332809604223
Weighted FMeasure: 0.39762865168257366
Iteration time: 711.3
Weighted AreaUnderPRC: 0.4157119186399047
Mean absolute error: 0.11878515248147943
Coverage of cases: 79.35353535353536
Instances selection time: 151.3
Test time: 226.8
Accumulative iteration time: 3316.7
Weighted Recall: 0.3983838383838384
Weighted FalsePositiveRate: 0.06016161616161617
Kappa statistic: 0.33822222222222226
Training time: 560.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 35.91368227731878
Incorrectly Classified Instances: 56.76767676767677
Correctly Classified Instances: 43.23232323232323
Weighted Precision: 0.46138582176257054
Weighted AreaUnderROC: 0.8402163860830528
Root mean squared error: 0.2676220200041065
Relative absolute error: 70.23655515425176
Root relative squared error: 93.09246487509301
Weighted TruePositiveRate: 0.4323232323232323
Weighted MatthewsCorrelation: 0.3841485707050632
Weighted FMeasure: 0.4315609241183086
Iteration time: 782.8
Weighted AreaUnderPRC: 0.4365950105313976
Mean absolute error: 0.1160934795938053
Coverage of cases: 81.71717171717172
Instances selection time: 160.8
Test time: 212.2
Accumulative iteration time: 4099.5
Weighted Recall: 0.4323232323232323
Weighted FalsePositiveRate: 0.05676767676767676
Kappa statistic: 0.37555555555555553
Training time: 622.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 35.8053259871443
Incorrectly Classified Instances: 54.424242424242415
Correctly Classified Instances: 45.575757575757585
Weighted Precision: 0.47749399869438525
Weighted AreaUnderROC: 0.8543277216610552
Root mean squared error: 0.261697652909678
Relative absolute error: 68.60561513990442
Root relative squared error: 91.03167056662475
Weighted TruePositiveRate: 0.45575757575757575
Weighted MatthewsCorrelation: 0.4071425658478772
Weighted FMeasure: 0.45309056048572466
Iteration time: 710.7
Weighted AreaUnderPRC: 0.46022501500701846
Mean absolute error: 0.11339771097504933
Coverage of cases: 84.34343434343434
Instances selection time: 147.1
Test time: 201.8
Accumulative iteration time: 4810.2
Weighted Recall: 0.45575757575757575
Weighted FalsePositiveRate: 0.05442424242424242
Kappa statistic: 0.4013333333333334
Training time: 563.6
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 35.20661157024807
Incorrectly Classified Instances: 54.464646464646464
Correctly Classified Instances: 45.535353535353536
Weighted Precision: 0.47245490034205806
Weighted AreaUnderROC: 0.8650927048260382
Root mean squared error: 0.2589687757514584
Relative absolute error: 67.65349507767075
Root relative squared error: 90.08242916639877
Weighted TruePositiveRate: 0.4553535353535354
Weighted MatthewsCorrelation: 0.4055055503927479
Weighted FMeasure: 0.4529033331909827
Iteration time: 718.4
Weighted AreaUnderPRC: 0.4738539809912288
Mean absolute error: 0.1118239588060681
Coverage of cases: 85.2121212121212
Instances selection time: 141.4
Test time: 227.0
Accumulative iteration time: 5528.6
Weighted Recall: 0.4553535353535354
Weighted FalsePositiveRate: 0.05446464646464647
Kappa statistic: 0.40088888888888885
Training time: 577.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 35.364554637282026
Incorrectly Classified Instances: 51.65656565656565
Correctly Classified Instances: 48.343434343434346
Weighted Precision: 0.497852379423247
Weighted AreaUnderROC: 0.8775991021324355
Root mean squared error: 0.25411962962450896
Relative absolute error: 66.12633285185323
Root relative squared error: 88.39565105490291
Weighted TruePositiveRate: 0.48343434343434344
Weighted MatthewsCorrelation: 0.43576139339075237
Weighted FMeasure: 0.48183731678281944
Iteration time: 707.8
Weighted AreaUnderPRC: 0.4918585976805268
Mean absolute error: 0.10929972372207215
Coverage of cases: 87.03030303030303
Instances selection time: 136.7
Test time: 225.7
Accumulative iteration time: 6236.4
Weighted Recall: 0.48343434343434344
Weighted FalsePositiveRate: 0.05165656565656566
Kappa statistic: 0.43177777777777776
Training time: 571.1
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 35.12029384756671
Incorrectly Classified Instances: 50.54545454545454
Correctly Classified Instances: 49.45454545454546
Weighted Precision: 0.5102663086791607
Weighted AreaUnderROC: 0.8856026936026936
Root mean squared error: 0.25015527804767546
Relative absolute error: 64.68639186889727
Root relative squared error: 87.01664920777083
Weighted TruePositiveRate: 0.4945454545454545
Weighted MatthewsCorrelation: 0.44859861625109526
Weighted FMeasure: 0.49350818283736403
Iteration time: 809.2
Weighted AreaUnderPRC: 0.5134127235627932
Mean absolute error: 0.10691965598164906
Coverage of cases: 88.64646464646466
Instances selection time: 153.5
Test time: 219.9
Accumulative iteration time: 7045.6
Weighted Recall: 0.4945454545454545
Weighted FalsePositiveRate: 0.05054545454545454
Kappa statistic: 0.44400000000000006
Training time: 655.7
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 33.94123048668515
Incorrectly Classified Instances: 49.15151515151515
Correctly Classified Instances: 50.84848484848485
Weighted Precision: 0.5233681207910967
Weighted AreaUnderROC: 0.892371268237935
Root mean squared error: 0.24724290228719564
Relative absolute error: 63.463330077032424
Root relative squared error: 86.00357771917886
Weighted TruePositiveRate: 0.5084848484848485
Weighted MatthewsCorrelation: 0.46335402798330316
Weighted FMeasure: 0.5067375895451017
Iteration time: 767.9
Weighted AreaUnderPRC: 0.526122646226108
Mean absolute error: 0.10489806624302947
Coverage of cases: 88.88888888888889
Instances selection time: 137.4
Test time: 250.6
Accumulative iteration time: 7813.5
Weighted Recall: 0.5084848484848485
Weighted FalsePositiveRate: 0.04915151515151515
Kappa statistic: 0.45933333333333337
Training time: 630.5
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 33.37190082644638
Incorrectly Classified Instances: 48.58585858585859
Correctly Classified Instances: 51.41414141414142
Weighted Precision: 0.5327230302549922
Weighted AreaUnderROC: 0.8993198653198654
Root mean squared error: 0.24474605630820695
Relative absolute error: 62.416388742260814
Root relative squared error: 85.13504849055279
Weighted TruePositiveRate: 0.5141414141414141
Weighted MatthewsCorrelation: 0.471481194490137
Weighted FMeasure: 0.5144863627063039
Iteration time: 697.9
Weighted AreaUnderPRC: 0.5380967648557605
Mean absolute error: 0.1031675846979524
Coverage of cases: 90.0
Instances selection time: 125.3
Test time: 237.9
Accumulative iteration time: 8511.4
Weighted Recall: 0.5141414141414141
Weighted FalsePositiveRate: 0.048585858585858586
Kappa statistic: 0.4655555555555555
Training time: 572.6
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 32.866850321395866
Incorrectly Classified Instances: 46.58585858585858
Correctly Classified Instances: 53.41414141414142
Weighted Precision: 0.5550529356910003
Weighted AreaUnderROC: 0.9047869809203142
Root mean squared error: 0.24042984322868813
Relative absolute error: 60.98713652125051
Root relative squared error: 83.63365142878499
Weighted TruePositiveRate: 0.5341414141414141
Weighted MatthewsCorrelation: 0.4950329224170118
Weighted FMeasure: 0.536720754411798
Iteration time: 728.3
Weighted AreaUnderPRC: 0.554214580150989
Mean absolute error: 0.10080518433264611
Coverage of cases: 90.82828282828282
Instances selection time: 134.8
Test time: 274.9
Accumulative iteration time: 9239.7
Weighted Recall: 0.5341414141414141
Weighted FalsePositiveRate: 0.04658585858585859
Kappa statistic: 0.48755555555555546
Training time: 593.5
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 32.69054178145096
Incorrectly Classified Instances: 45.61616161616162
Correctly Classified Instances: 54.38383838383838
Weighted Precision: 0.5594308356550821
Weighted AreaUnderROC: 0.9127991021324355
Root mean squared error: 0.23649756741930777
Relative absolute error: 59.60080617382912
Root relative squared error: 82.26580715476636
Weighted TruePositiveRate: 0.5438383838383839
Weighted MatthewsCorrelation: 0.5032367616084454
Weighted FMeasure: 0.5441352931445418
Iteration time: 818.5
Weighted AreaUnderPRC: 0.5733297133989316
Mean absolute error: 0.09851372921294133
Coverage of cases: 91.57575757575759
Instances selection time: 141.1
Test time: 266.3
Accumulative iteration time: 10058.2
Weighted Recall: 0.5438383838383839
Weighted FalsePositiveRate: 0.0456161616161616
Kappa statistic: 0.49822222222222223
Training time: 677.4
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 32.46464646464655
Incorrectly Classified Instances: 43.292929292929294
Correctly Classified Instances: 56.707070707070706
Weighted Precision: 0.5829827599505107
Weighted AreaUnderROC: 0.9200121212121211
Root mean squared error: 0.23192924673467968
Relative absolute error: 57.99716044004991
Root relative squared error: 80.67671432576307
Weighted TruePositiveRate: 0.5670707070707071
Weighted MatthewsCorrelation: 0.529178333367844
Weighted FMeasure: 0.5683050945795578
Iteration time: 835.0
Weighted AreaUnderPRC: 0.5946866053573833
Mean absolute error: 0.09586307510752112
Coverage of cases: 92.74747474747474
Instances selection time: 138.6
Test time: 304.3
Accumulative iteration time: 10893.2
Weighted Recall: 0.5670707070707071
Weighted FalsePositiveRate: 0.0432929292929293
Kappa statistic: 0.5237777777777777
Training time: 696.4
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 31.994490358126807
Incorrectly Classified Instances: 42.303030303030305
Correctly Classified Instances: 57.69696969696969
Weighted Precision: 0.587182758881142
Weighted AreaUnderROC: 0.9245333333333333
Root mean squared error: 0.22806786724653075
Relative absolute error: 56.496280609414406
Root relative squared error: 79.33353137555443
Weighted TruePositiveRate: 0.5769696969696971
Weighted MatthewsCorrelation: 0.5377298176012533
Weighted FMeasure: 0.5766356414443322
Iteration time: 845.9
Weighted AreaUnderPRC: 0.6125555189121304
Mean absolute error: 0.09338228199903267
Coverage of cases: 93.07070707070706
Instances selection time: 124.4
Test time: 286.8
Accumulative iteration time: 11739.1
Weighted Recall: 0.5769696969696971
Weighted FalsePositiveRate: 0.04230303030303031
Kappa statistic: 0.5346666666666666
Training time: 721.5
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 31.8218549127641
Incorrectly Classified Instances: 40.989898989899
Correctly Classified Instances: 59.010101010101
Weighted Precision: 0.6001064460075327
Weighted AreaUnderROC: 0.9283358024691359
Root mean squared error: 0.2256579686963056
Relative absolute error: 55.67598762734324
Root relative squared error: 78.49524685720306
Weighted TruePositiveRate: 0.5901010101010101
Weighted MatthewsCorrelation: 0.5522089984478531
Weighted FMeasure: 0.5900535861164544
Iteration time: 763.1
Weighted AreaUnderPRC: 0.6198138959301668
Mean absolute error: 0.09202642583031999
Coverage of cases: 93.3939393939394
Instances selection time: 126.1
Test time: 347.2
Accumulative iteration time: 12502.2
Weighted Recall: 0.5901010101010101
Weighted FalsePositiveRate: 0.040989898989898986
Kappa statistic: 0.5491111111111111
Training time: 637.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 31.8714416896236
Incorrectly Classified Instances: 40.14141414141414
Correctly Classified Instances: 59.858585858585855
Weighted Precision: 0.6087783238201838
Weighted AreaUnderROC: 0.9343914702581368
Root mean squared error: 0.2218846647479446
Relative absolute error: 54.536939418193356
Root relative squared error: 77.18270103130105
Weighted TruePositiveRate: 0.5985858585858586
Weighted MatthewsCorrelation: 0.5617212426124214
Weighted FMeasure: 0.598833226311719
Iteration time: 768.0
Weighted AreaUnderPRC: 0.6374907899684238
Mean absolute error: 0.09014370151767556
Coverage of cases: 94.54545454545455
Instances selection time: 95.2
Test time: 294.5
Accumulative iteration time: 13270.2
Weighted Recall: 0.5985858585858586
Weighted FalsePositiveRate: 0.04014141414141414
Kappa statistic: 0.5584444444444444
Training time: 672.8
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 31.449035812672275
Incorrectly Classified Instances: 38.727272727272734
Correctly Classified Instances: 61.27272727272727
Weighted Precision: 0.6201159797425387
Weighted AreaUnderROC: 0.9382886644219978
Root mean squared error: 0.21806241485484748
Relative absolute error: 53.27519326009944
Root relative squared error: 75.85312933196364
Weighted TruePositiveRate: 0.6127272727272727
Weighted MatthewsCorrelation: 0.5761299168210784
Weighted FMeasure: 0.6122223987968216
Iteration time: 790.6
Weighted AreaUnderPRC: 0.6544394225300361
Mean absolute error: 0.08805817067785089
Coverage of cases: 95.01010101010101
Instances selection time: 84.5
Test time: 341.0
Accumulative iteration time: 14060.8
Weighted Recall: 0.6127272727272727
Weighted FalsePositiveRate: 0.038727272727272735
Kappa statistic: 0.5740000000000001
Training time: 706.1
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.50964187327832
Incorrectly Classified Instances: 38.06060606060607
Correctly Classified Instances: 61.93939393939392
Weighted Precision: 0.6271830598204113
Weighted AreaUnderROC: 0.9404615039281705
Root mean squared error: 0.2150579531521415
Relative absolute error: 52.48131986031391
Root relative squared error: 74.8080256984008
Weighted TruePositiveRate: 0.6193939393939395
Weighted MatthewsCorrelation: 0.583796834211747
Weighted FMeasure: 0.6194404443609376
Iteration time: 819.7
Weighted AreaUnderPRC: 0.6703199915605953
Mean absolute error: 0.08674598324018883
Coverage of cases: 95.47474747474749
Instances selection time: 72.2
Test time: 350.5
Accumulative iteration time: 14880.5
Weighted Recall: 0.6193939393939395
Weighted FalsePositiveRate: 0.038060606060606066
Kappa statistic: 0.5813333333333333
Training time: 747.5
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.649219467401377
Incorrectly Classified Instances: 37.959595959595966
Correctly Classified Instances: 62.04040404040404
Weighted Precision: 0.6257437648519
Weighted AreaUnderROC: 0.9426199775533108
Root mean squared error: 0.21354676277445842
Relative absolute error: 52.1997555262508
Root relative squared error: 74.28235730552385
Weighted TruePositiveRate: 0.6204040404040404
Weighted MatthewsCorrelation: 0.5837098703266627
Weighted FMeasure: 0.6192821742638899
Iteration time: 781.4
Weighted AreaUnderPRC: 0.6771732590615216
Mean absolute error: 0.08628058764669606
Coverage of cases: 95.7979797979798
Instances selection time: 42.3
Test time: 366.7
Accumulative iteration time: 15661.9
Weighted Recall: 0.6204040404040404
Weighted FalsePositiveRate: 0.03795959595959596
Kappa statistic: 0.5824444444444443
Training time: 739.1
		
Time end:Fri Dec 01 13.26.05 EET 2017