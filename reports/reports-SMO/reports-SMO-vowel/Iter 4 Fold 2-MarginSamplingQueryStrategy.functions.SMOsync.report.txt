Fri Dec 01 13.35.38 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.35.38 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 39.430670339761335
Incorrectly Classified Instances: 67.27272727272727
Correctly Classified Instances: 32.72727272727273
Weighted Precision: 0.3580943728695698
Weighted AreaUnderROC: 0.7691447811447812
Root mean squared error: 0.29159743316913733
Relative absolute error: 78.565445884868
Root relative squared error: 101.43232535405222
Weighted TruePositiveRate: 0.32727272727272727
Weighted MatthewsCorrelation: 0.27092813700483
Weighted FMeasure: 0.32935449722964544
Iteration time: 609.0
Weighted AreaUnderPRC: 0.33138972804858063
Mean absolute error: 0.12986024113201405
Coverage of cases: 76.36363636363636
Instances selection time: 156.0
Test time: 188.0
Accumulative iteration time: 609.0
Weighted Recall: 0.32727272727272727
Weighted FalsePositiveRate: 0.06727272727272726
Kappa statistic: 0.26
Training time: 453.0
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 39.485766758494144
Incorrectly Classified Instances: 65.25252525252525
Correctly Classified Instances: 34.74747474747475
Weighted Precision: 0.3932898795205679
Weighted AreaUnderROC: 0.8034074074074076
Root mean squared error: 0.2832917065531963
Relative absolute error: 76.73805551606078
Root relative squared error: 98.54317384385604
Weighted TruePositiveRate: 0.3474747474747475
Weighted MatthewsCorrelation: 0.29980458347056216
Weighted FMeasure: 0.35543016410773975
Iteration time: 592.0
Weighted AreaUnderPRC: 0.36703766697610907
Mean absolute error: 0.12683976118357235
Coverage of cases: 80.60606060606061
Instances selection time: 135.0
Test time: 170.0
Accumulative iteration time: 1201.0
Weighted Recall: 0.3474747474747475
Weighted FalsePositiveRate: 0.06525252525252524
Kappa statistic: 0.2822222222222222
Training time: 457.0
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 39.70615243342521
Incorrectly Classified Instances: 59.19191919191919
Correctly Classified Instances: 40.80808080808081
Weighted Precision: 0.4197270389603179
Weighted AreaUnderROC: 0.8189584736251402
Root mean squared error: 0.27654854110827154
Relative absolute error: 73.99305804798273
Root relative squared error: 96.19756008487249
Weighted TruePositiveRate: 0.4080808080808081
Weighted MatthewsCorrelation: 0.3514428144601112
Weighted FMeasure: 0.4040543853099988
Iteration time: 585.0
Weighted AreaUnderPRC: 0.405229214112432
Mean absolute error: 0.12230257528592266
Coverage of cases: 81.01010101010101
Instances selection time: 122.0
Test time: 224.0
Accumulative iteration time: 1786.0
Weighted Recall: 0.4080808080808081
Weighted FalsePositiveRate: 0.05919191919191919
Kappa statistic: 0.34888888888888897
Training time: 463.0
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 39.13682277318652
Incorrectly Classified Instances: 58.78787878787879
Correctly Classified Instances: 41.21212121212121
Weighted Precision: 0.41490896099519237
Weighted AreaUnderROC: 0.8311919191919191
Root mean squared error: 0.26887713118110596
Relative absolute error: 72.28691916911646
Root relative squared error: 93.52905597905885
Weighted TruePositiveRate: 0.4121212121212121
Weighted MatthewsCorrelation: 0.35196929334812205
Weighted FMeasure: 0.4050768697553248
Iteration time: 592.0
Weighted AreaUnderPRC: 0.42406220269464273
Mean absolute error: 0.11948251102333375
Coverage of cases: 83.23232323232324
Instances selection time: 119.0
Test time: 168.0
Accumulative iteration time: 2378.0
Weighted Recall: 0.4121212121212121
Weighted FalsePositiveRate: 0.05878787878787879
Kappa statistic: 0.3533333333333333
Training time: 473.0
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 37.17171717171725
Incorrectly Classified Instances: 60.2020202020202
Correctly Classified Instances: 39.7979797979798
Weighted Precision: 0.39925521911541967
Weighted AreaUnderROC: 0.8434702581369248
Root mean squared error: 0.2644159109702266
Relative absolute error: 70.06899972251155
Root relative squared error: 91.97721810796375
Weighted TruePositiveRate: 0.397979797979798
Weighted MatthewsCorrelation: 0.3346249727543087
Weighted FMeasure: 0.38724741336979807
Iteration time: 605.0
Weighted AreaUnderPRC: 0.45689111239793095
Mean absolute error: 0.11581652846696198
Coverage of cases: 84.44444444444444
Instances selection time: 118.0
Test time: 225.0
Accumulative iteration time: 2983.0
Weighted Recall: 0.397979797979798
Weighted FalsePositiveRate: 0.0602020202020202
Kappa statistic: 0.3377777777777778
Training time: 487.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 36.23507805325996
Incorrectly Classified Instances: 56.96969696969697
Correctly Classified Instances: 43.03030303030303
Weighted Precision: 0.4310958034152936
Weighted AreaUnderROC: 0.8521481481481481
Root mean squared error: 0.25772736859059026
Relative absolute error: 68.0429516282907
Root relative squared error: 89.65060501188029
Weighted TruePositiveRate: 0.4303030303030303
Weighted MatthewsCorrelation: 0.3722072341838415
Weighted FMeasure: 0.4263059344085293
Iteration time: 611.0
Weighted AreaUnderPRC: 0.4747236094332733
Mean absolute error: 0.11246768864180352
Coverage of cases: 85.45454545454545
Instances selection time: 119.0
Test time: 192.0
Accumulative iteration time: 3594.0
Weighted Recall: 0.4303030303030303
Weighted FalsePositiveRate: 0.05696969696969697
Kappa statistic: 0.3733333333333333
Training time: 492.0
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 35.5555555555557
Incorrectly Classified Instances: 54.94949494949495
Correctly Classified Instances: 45.05050505050505
Weighted Precision: 0.4528135845512903
Weighted AreaUnderROC: 0.8692839506172839
Root mean squared error: 0.2560060537383273
Relative absolute error: 67.273306817001
Root relative squared error: 89.05184470650329
Weighted TruePositiveRate: 0.4505050505050505
Weighted MatthewsCorrelation: 0.3956077142793748
Weighted FMeasure: 0.4486051847814773
Iteration time: 614.0
Weighted AreaUnderPRC: 0.48135767080557607
Mean absolute error: 0.11119554845785361
Coverage of cases: 86.86868686868686
Instances selection time: 108.0
Test time: 185.0
Accumulative iteration time: 4208.0
Weighted Recall: 0.4505050505050505
Weighted FalsePositiveRate: 0.05494949494949495
Kappa statistic: 0.39555555555555555
Training time: 506.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 35.28007346189183
Incorrectly Classified Instances: 52.72727272727273
Correctly Classified Instances: 47.27272727272727
Weighted Precision: 0.4749804304464003
Weighted AreaUnderROC: 0.8792682379349045
Root mean squared error: 0.25472726782046934
Relative absolute error: 65.88645813772311
Root relative squared error: 88.60701833108348
Weighted TruePositiveRate: 0.4727272727272727
Weighted MatthewsCorrelation: 0.4187419739673588
Weighted FMeasure: 0.46697705945532403
Iteration time: 645.0
Weighted AreaUnderPRC: 0.5000802302890642
Mean absolute error: 0.10890323659127857
Coverage of cases: 86.86868686868686
Instances selection time: 114.0
Test time: 203.0
Accumulative iteration time: 4853.0
Weighted Recall: 0.4727272727272727
Weighted FalsePositiveRate: 0.05272727272727273
Kappa statistic: 0.42000000000000004
Training time: 531.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 35.83103764921964
Incorrectly Classified Instances: 50.90909090909091
Correctly Classified Instances: 49.09090909090909
Weighted Precision: 0.5051950771522273
Weighted AreaUnderROC: 0.8852435465768799
Root mean squared error: 0.2506676924696901
Relative absolute error: 66.05186234889455
Root relative squared error: 87.19489284251416
Weighted TruePositiveRate: 0.4909090909090909
Weighted MatthewsCorrelation: 0.44372954545208376
Weighted FMeasure: 0.4889312498188429
Iteration time: 628.0
Weighted AreaUnderPRC: 0.5111080730135534
Mean absolute error: 0.10917663198164457
Coverage of cases: 88.08080808080808
Instances selection time: 100.0
Test time: 197.0
Accumulative iteration time: 5481.0
Weighted Recall: 0.4909090909090909
Weighted FalsePositiveRate: 0.05090909090909091
Kappa statistic: 0.44000000000000006
Training time: 528.0
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 36.951331496786125
Incorrectly Classified Instances: 51.313131313131315
Correctly Classified Instances: 48.686868686868685
Weighted Precision: 0.5048428390784921
Weighted AreaUnderROC: 0.8934006734006733
Root mean squared error: 0.24694835160243975
Relative absolute error: 65.39824736313149
Root relative squared error: 85.90111810365788
Weighted TruePositiveRate: 0.4868686868686869
Weighted MatthewsCorrelation: 0.44220657493882704
Weighted FMeasure: 0.48929535556478015
Iteration time: 744.0
Weighted AreaUnderPRC: 0.5307605476737223
Mean absolute error: 0.10809627663327587
Coverage of cases: 91.71717171717172
Instances selection time: 91.0
Test time: 186.0
Accumulative iteration time: 6225.0
Weighted Recall: 0.4868686868686869
Weighted FalsePositiveRate: 0.05131313131313132
Kappa statistic: 0.4355555555555556
Training time: 653.0
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 34.784205693296684
Incorrectly Classified Instances: 50.303030303030305
Correctly Classified Instances: 49.696969696969695
Weighted Precision: 0.518230743210329
Weighted AreaUnderROC: 0.896462401795735
Root mean squared error: 0.24458796517444129
Relative absolute error: 63.121787286768495
Root relative squared error: 85.08005640388922
Weighted TruePositiveRate: 0.49696969696969695
Weighted MatthewsCorrelation: 0.4556646118274494
Weighted FMeasure: 0.503088613549651
Iteration time: 646.0
Weighted AreaUnderPRC: 0.5466250934225626
Mean absolute error: 0.10433353270540315
Coverage of cases: 90.9090909090909
Instances selection time: 98.0
Test time: 217.0
Accumulative iteration time: 6871.0
Weighted Recall: 0.49696969696969695
Weighted FalsePositiveRate: 0.05030303030303031
Kappa statistic: 0.4466666666666666
Training time: 548.0
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 35.24334251606994
Incorrectly Classified Instances: 46.06060606060606
Correctly Classified Instances: 53.93939393939394
Weighted Precision: 0.5471944040746526
Weighted AreaUnderROC: 0.8976835016835018
Root mean squared error: 0.24084413470924865
Relative absolute error: 62.13313169782959
Root relative squared error: 83.7777629451002
Weighted TruePositiveRate: 0.5393939393939394
Weighted MatthewsCorrelation: 0.49615436701804744
Weighted FMeasure: 0.5402927635128162
Iteration time: 661.0
Weighted AreaUnderPRC: 0.5587273416634148
Mean absolute error: 0.10269939123608263
Coverage of cases: 89.8989898989899
Instances selection time: 97.0
Test time: 224.0
Accumulative iteration time: 7532.0
Weighted Recall: 0.5393939393939394
Weighted FalsePositiveRate: 0.04606060606060606
Kappa statistic: 0.49333333333333335
Training time: 564.0
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 34.38016528925633
Incorrectly Classified Instances: 44.24242424242424
Correctly Classified Instances: 55.75757575757576
Weighted Precision: 0.5616637517827746
Weighted AreaUnderROC: 0.8996588103254769
Root mean squared error: 0.23839140162595457
Relative absolute error: 61.69366972320222
Root relative squared error: 82.92457841117796
Weighted TruePositiveRate: 0.5575757575757576
Weighted MatthewsCorrelation: 0.5144273658528293
Weighted FMeasure: 0.556909177361189
Iteration time: 666.0
Weighted AreaUnderPRC: 0.569730374689543
Mean absolute error: 0.10197300780694647
Coverage of cases: 89.8989898989899
Instances selection time: 96.0
Test time: 234.0
Accumulative iteration time: 8198.0
Weighted Recall: 0.5575757575757576
Weighted FalsePositiveRate: 0.04424242424242425
Kappa statistic: 0.5133333333333333
Training time: 570.0
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 33.99449035812687
Incorrectly Classified Instances: 44.24242424242424
Correctly Classified Instances: 55.75757575757576
Weighted Precision: 0.5729143220325071
Weighted AreaUnderROC: 0.9068911335578003
Root mean squared error: 0.23440749750570322
Relative absolute error: 59.89047434617112
Root relative squared error: 81.53877520120838
Weighted TruePositiveRate: 0.5575757575757576
Weighted MatthewsCorrelation: 0.5194543938668357
Weighted FMeasure: 0.5613702372446173
Iteration time: 686.0
Weighted AreaUnderPRC: 0.5833638283332007
Mean absolute error: 0.09899251958044877
Coverage of cases: 91.11111111111111
Instances selection time: 96.0
Test time: 246.0
Accumulative iteration time: 8884.0
Weighted Recall: 0.5575757575757576
Weighted FalsePositiveRate: 0.04424242424242424
Kappa statistic: 0.5133333333333333
Training time: 590.0
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 34.56382001836562
Incorrectly Classified Instances: 41.81818181818182
Correctly Classified Instances: 58.18181818181818
Weighted Precision: 0.5862069340431865
Weighted AreaUnderROC: 0.9176386083052749
Root mean squared error: 0.2265685009549005
Relative absolute error: 57.710249036198356
Root relative squared error: 78.81197599742694
Weighted TruePositiveRate: 0.5818181818181818
Weighted MatthewsCorrelation: 0.54085491111441
Weighted FMeasure: 0.5803714185194566
Iteration time: 677.0
Weighted AreaUnderPRC: 0.6134890544085534
Mean absolute error: 0.09538884138214665
Coverage of cases: 92.92929292929293
Instances selection time: 90.0
Test time: 243.0
Accumulative iteration time: 9561.0
Weighted Recall: 0.5818181818181818
Weighted FalsePositiveRate: 0.04181818181818182
Kappa statistic: 0.5399999999999999
Training time: 587.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 33.719008264462936
Incorrectly Classified Instances: 38.18181818181818
Correctly Classified Instances: 61.81818181818182
Weighted Precision: 0.6186621667320724
Weighted AreaUnderROC: 0.9205252525252525
Root mean squared error: 0.2228553690280805
Relative absolute error: 55.96052408646471
Root relative squared error: 77.52036104186845
Weighted TruePositiveRate: 0.6181818181818182
Weighted MatthewsCorrelation: 0.5799016850179958
Weighted FMeasure: 0.6174716849147192
Iteration time: 706.0
Weighted AreaUnderPRC: 0.6292019846578571
Mean absolute error: 0.09249673402721498
Coverage of cases: 92.92929292929293
Instances selection time: 84.0
Test time: 253.0
Accumulative iteration time: 10267.0
Weighted Recall: 0.6181818181818182
Weighted FalsePositiveRate: 0.03818181818181818
Kappa statistic: 0.58
Training time: 622.0
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 33.49862258953176
Incorrectly Classified Instances: 37.97979797979798
Correctly Classified Instances: 62.02020202020202
Weighted Precision: 0.6167424253180889
Weighted AreaUnderROC: 0.9261099887766554
Root mean squared error: 0.22076939960051195
Relative absolute error: 55.493748563107125
Root relative squared error: 76.79475544460308
Weighted TruePositiveRate: 0.6202020202020202
Weighted MatthewsCorrelation: 0.5799832701751102
Weighted FMeasure: 0.6169890548159911
Iteration time: 689.0
Weighted AreaUnderPRC: 0.6344696882740084
Mean absolute error: 0.09172520423654129
Coverage of cases: 94.74747474747475
Instances selection time: 68.0
Test time: 258.0
Accumulative iteration time: 10956.0
Weighted Recall: 0.6202020202020202
Weighted FalsePositiveRate: 0.03797979797979798
Kappa statistic: 0.5822222222222222
Training time: 621.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 35.280073461891746
Incorrectly Classified Instances: 36.56565656565657
Correctly Classified Instances: 63.43434343434343
Weighted Precision: 0.6335046189469139
Weighted AreaUnderROC: 0.937557800224467
Root mean squared error: 0.21525197020586964
Relative absolute error: 55.12635360532112
Root relative squared error: 74.87551463581738
Weighted TruePositiveRate: 0.6343434343434343
Weighted MatthewsCorrelation: 0.5968383770636956
Weighted FMeasure: 0.6324777623624953
Iteration time: 700.0
Weighted AreaUnderPRC: 0.6617083150575017
Mean absolute error: 0.09111793984350657
Coverage of cases: 97.37373737373737
Instances selection time: 49.0
Test time: 232.0
Accumulative iteration time: 11656.0
Weighted Recall: 0.6343434343434343
Weighted FalsePositiveRate: 0.036565656565656565
Kappa statistic: 0.5977777777777777
Training time: 651.0
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 34.60055096418741
Incorrectly Classified Instances: 36.96969696969697
Correctly Classified Instances: 63.03030303030303
Weighted Precision: 0.6300202296369276
Weighted AreaUnderROC: 0.9385903479236811
Root mean squared error: 0.21395387095520244
Relative absolute error: 54.69344440964086
Root relative squared error: 74.42397010710009
Weighted TruePositiveRate: 0.6303030303030303
Weighted MatthewsCorrelation: 0.5922824176434233
Weighted FMeasure: 0.6276743366723727
Iteration time: 705.0
Weighted AreaUnderPRC: 0.6692347578976557
Mean absolute error: 0.09040238745395242
Coverage of cases: 96.76767676767676
Instances selection time: 35.0
Test time: 243.0
Accumulative iteration time: 12361.0
Weighted Recall: 0.6303030303030303
Weighted FalsePositiveRate: 0.03696969696969697
Kappa statistic: 0.5933333333333333
Training time: 670.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 33.82920110192844
Incorrectly Classified Instances: 35.95959595959596
Correctly Classified Instances: 64.04040404040404
Weighted Precision: 0.6384722144115923
Weighted AreaUnderROC: 0.9405791245791245
Root mean squared error: 0.21162600883003152
Relative absolute error: 53.52884257178476
Root relative squared error: 73.6142220037183
Weighted TruePositiveRate: 0.6404040404040404
Weighted MatthewsCorrelation: 0.6025972368994048
Weighted FMeasure: 0.6370457542826737
Iteration time: 749.0
Weighted AreaUnderPRC: 0.6772735263737683
Mean absolute error: 0.08847742573848777
Coverage of cases: 96.96969696969697
Instances selection time: 24.0
Test time: 272.0
Accumulative iteration time: 13110.0
Weighted Recall: 0.6404040404040404
Weighted FalsePositiveRate: 0.03595959595959596
Kappa statistic: 0.6044444444444445
Training time: 725.0
		
Time end:Fri Dec 01 13.35.56 EET 2017