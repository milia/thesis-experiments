Fri Dec 01 13.33.20 EET 2017
Dataset: vowel
Test set size: 495
Initial Labelled set size: 99
Initial Unlabelled set size: 396
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.33.20 EET 2017
		
Iteration: 1
Labeled set size: 99
Unlabelled set size: 396
	
Mean region size: 36.03673094582194
Incorrectly Classified Instances: 69.13131313131314
Correctly Classified Instances: 30.868686868686872
Weighted Precision: 0.36165728275925096
Weighted AreaUnderROC: 0.7548426487093154
Root mean squared error: 0.2962981225298428
Relative absolute error: 78.76072093058045
Root relative squared error: 103.06746269885473
Weighted TruePositiveRate: 0.3086868686868687
Weighted MatthewsCorrelation: 0.2562383983194222
Weighted FMeasure: 0.31108384195152683
Iteration time: 734.1
Weighted AreaUnderPRC: 0.3269424454502943
Mean absolute error: 0.13018300980261316
Coverage of cases: 69.11111111111111
Instances selection time: 186.9
Test time: 236.4
Accumulative iteration time: 734.1
Weighted Recall: 0.3086868686868687
Weighted FalsePositiveRate: 0.06913131313131313
Kappa statistic: 0.23955555555555558
Training time: 547.2
		
Iteration: 2
Labeled set size: 119
Unlabelled set size: 376
	
Mean region size: 35.88062442607908
Incorrectly Classified Instances: 65.73737373737374
Correctly Classified Instances: 34.26262626262626
Weighted Precision: 0.37231990807318666
Weighted AreaUnderROC: 0.7800875420875419
Root mean squared error: 0.2888386490393658
Relative absolute error: 76.41688725173879
Root relative squared error: 100.47268079754383
Weighted TruePositiveRate: 0.34262626262626267
Weighted MatthewsCorrelation: 0.285811189213056
Weighted FMeasure: 0.3416204046497217
Iteration time: 645.0
Weighted AreaUnderPRC: 0.3455031633361465
Mean absolute error: 0.12630890454832938
Coverage of cases: 72.60606060606061
Instances selection time: 169.1
Test time: 217.6
Accumulative iteration time: 1379.1
Weighted Recall: 0.34262626262626267
Weighted FalsePositiveRate: 0.06573737373737373
Kappa statistic: 0.2768888888888889
Training time: 475.9
		
Iteration: 3
Labeled set size: 139
Unlabelled set size: 356
	
Mean region size: 36.955004591368336
Incorrectly Classified Instances: 62.4040404040404
Correctly Classified Instances: 37.5959595959596
Weighted Precision: 0.40186163183747176
Weighted AreaUnderROC: 0.8092650953984288
Root mean squared error: 0.2795387459440518
Relative absolute error: 73.97555973639123
Root relative squared error: 97.23770445953919
Weighted TruePositiveRate: 0.37595959595959594
Weighted MatthewsCorrelation: 0.32144898411955936
Weighted FMeasure: 0.37540437640650215
Iteration time: 630.3
Weighted AreaUnderPRC: 0.38407081871747767
Mean absolute error: 0.12227365245684581
Coverage of cases: 78.08080808080808
Instances selection time: 146.2
Test time: 209.6
Accumulative iteration time: 2009.4
Weighted Recall: 0.37595959595959594
Weighted FalsePositiveRate: 0.06240404040404043
Kappa statistic: 0.31355555555555553
Training time: 484.1
		
Iteration: 4
Labeled set size: 159
Unlabelled set size: 336
	
Mean region size: 36.60973370064291
Incorrectly Classified Instances: 59.515151515151516
Correctly Classified Instances: 40.484848484848484
Weighted Precision: 0.4345925333687627
Weighted AreaUnderROC: 0.8294518518518521
Root mean squared error: 0.2721332295469323
Relative absolute error: 71.65238033954809
Root relative squared error: 94.66169156243083
Weighted TruePositiveRate: 0.4048484848484849
Weighted MatthewsCorrelation: 0.35444306451745133
Weighted FMeasure: 0.4049316003531581
Iteration time: 635.2
Weighted AreaUnderPRC: 0.42124676790445453
Mean absolute error: 0.1184336865116505
Coverage of cases: 79.83838383838383
Instances selection time: 143.7
Test time: 206.2
Accumulative iteration time: 2644.6
Weighted Recall: 0.4048484848484849
Weighted FalsePositiveRate: 0.05951515151515152
Kappa statistic: 0.3453333333333333
Training time: 491.5
		
Iteration: 5
Labeled set size: 179
Unlabelled set size: 316
	
Mean region size: 36.42791551882474
Incorrectly Classified Instances: 56.68686868686869
Correctly Classified Instances: 43.313131313131315
Weighted Precision: 0.45541637201194896
Weighted AreaUnderROC: 0.8454487093153761
Root mean squared error: 0.2663458842281757
Relative absolute error: 70.16997925774162
Root relative squared error: 92.64856035298057
Weighted TruePositiveRate: 0.43313131313131314
Weighted MatthewsCorrelation: 0.38307298003024315
Weighted FMeasure: 0.43209055963210175
Iteration time: 632.0
Weighted AreaUnderPRC: 0.43825034572099
Mean absolute error: 0.11598343678965631
Coverage of cases: 82.92929292929293
Instances selection time: 134.0
Test time: 204.5
Accumulative iteration time: 3276.6
Weighted Recall: 0.43313131313131314
Weighted FalsePositiveRate: 0.05668686868686869
Kappa statistic: 0.3764444444444444
Training time: 498.0
		
Iteration: 6
Labeled set size: 199
Unlabelled set size: 296
	
Mean region size: 35.66023875114797
Incorrectly Classified Instances: 55.030303030303024
Correctly Classified Instances: 44.969696969696976
Weighted Precision: 0.4713762088429707
Weighted AreaUnderROC: 0.8572615039281708
Root mean squared error: 0.26125638961928727
Relative absolute error: 68.3538708632424
Root relative squared error: 90.8781768916247
Weighted TruePositiveRate: 0.4496969696969697
Weighted MatthewsCorrelation: 0.4016245333776517
Weighted FMeasure: 0.4500764512177925
Iteration time: 639.9
Weighted AreaUnderPRC: 0.4607753715887385
Mean absolute error: 0.11298160473263279
Coverage of cases: 84.3838383838384
Instances selection time: 133.3
Test time: 209.4
Accumulative iteration time: 3916.5
Weighted Recall: 0.4496969696969697
Weighted FalsePositiveRate: 0.05503030303030303
Kappa statistic: 0.39466666666666667
Training time: 506.6
		
Iteration: 7
Labeled set size: 219
Unlabelled set size: 276
	
Mean region size: 34.697887970615376
Incorrectly Classified Instances: 52.68686868686869
Correctly Classified Instances: 47.31313131313131
Weighted Precision: 0.49218043993661437
Weighted AreaUnderROC: 0.872558024691358
Root mean squared error: 0.2569473696367478
Relative absolute error: 66.40895217661428
Root relative squared error: 89.37928195254429
Weighted TruePositiveRate: 0.4731313131313131
Weighted MatthewsCorrelation: 0.4262696898590024
Weighted FMeasure: 0.472773983541963
Iteration time: 646.1
Weighted AreaUnderPRC: 0.4866060703721232
Mean absolute error: 0.10976686310184247
Coverage of cases: 86.24242424242422
Instances selection time: 132.1
Test time: 219.3
Accumulative iteration time: 4562.6
Weighted Recall: 0.4731313131313131
Weighted FalsePositiveRate: 0.052686868686868685
Kappa statistic: 0.42044444444444445
Training time: 514.0
		
Iteration: 8
Labeled set size: 239
Unlabelled set size: 256
	
Mean region size: 34.88154269972466
Incorrectly Classified Instances: 50.262626262626256
Correctly Classified Instances: 49.737373737373744
Weighted Precision: 0.5176956425743275
Weighted AreaUnderROC: 0.8827272727272728
Root mean squared error: 0.25119786777536135
Relative absolute error: 64.59384144958369
Root relative squared error: 87.37931461027483
Weighted TruePositiveRate: 0.49737373737373736
Weighted MatthewsCorrelation: 0.4538347800828886
Weighted FMeasure: 0.49831205827397584
Iteration time: 657.3
Weighted AreaUnderPRC: 0.506566091360615
Mean absolute error: 0.1067666800819572
Coverage of cases: 87.53535353535355
Instances selection time: 129.3
Test time: 226.5
Accumulative iteration time: 5219.9
Weighted Recall: 0.49737373737373736
Weighted FalsePositiveRate: 0.050262626262626266
Kappa statistic: 0.4471111111111111
Training time: 528.0
		
Iteration: 9
Labeled set size: 259
Unlabelled set size: 236
	
Mean region size: 34.64462809917368
Incorrectly Classified Instances: 48.28282828282828
Correctly Classified Instances: 51.71717171717173
Weighted Precision: 0.5346342602682262
Weighted AreaUnderROC: 0.8895286195286195
Root mean squared error: 0.2470499523552748
Relative absolute error: 63.242433148113754
Root relative squared error: 85.936459980662
Weighted TruePositiveRate: 0.5171717171717172
Weighted MatthewsCorrelation: 0.47443401849917244
Weighted FMeasure: 0.5174868143362219
Iteration time: 681.7
Weighted AreaUnderPRC: 0.5202535707390772
Mean absolute error: 0.10453294735225482
Coverage of cases: 88.06060606060605
Instances selection time: 139.9
Test time: 236.0
Accumulative iteration time: 5901.6
Weighted Recall: 0.5171717171717172
Weighted FalsePositiveRate: 0.04828282828282829
Kappa statistic: 0.4688888888888889
Training time: 541.8
		
Iteration: 10
Labeled set size: 279
Unlabelled set size: 216
	
Mean region size: 33.64370982552812
Incorrectly Classified Instances: 46.3030303030303
Correctly Classified Instances: 53.6969696969697
Weighted Precision: 0.5541220583661697
Weighted AreaUnderROC: 0.8988686868686869
Root mean squared error: 0.24208525698410818
Relative absolute error: 61.157633456666964
Root relative squared error: 84.20948800186603
Weighted TruePositiveRate: 0.5369696969696969
Weighted MatthewsCorrelation: 0.49636701023618884
Weighted FMeasure: 0.5379333621929858
Iteration time: 681.6
Weighted AreaUnderPRC: 0.5442457606142765
Mean absolute error: 0.10108699744903696
Coverage of cases: 89.05050505050505
Instances selection time: 129.1
Test time: 252.1
Accumulative iteration time: 6583.2
Weighted Recall: 0.5369696969696969
Weighted FalsePositiveRate: 0.046303030303030304
Kappa statistic: 0.49066666666666664
Training time: 552.5
		
Iteration: 11
Labeled set size: 299
Unlabelled set size: 196
	
Mean region size: 33.44168962350791
Incorrectly Classified Instances: 44.66666666666667
Correctly Classified Instances: 55.333333333333336
Weighted Precision: 0.5698679344417366
Weighted AreaUnderROC: 0.905376430976431
Root mean squared error: 0.23810910109696412
Relative absolute error: 59.62518441368248
Root relative squared error: 82.82638001898715
Weighted TruePositiveRate: 0.5533333333333333
Weighted MatthewsCorrelation: 0.514562056486368
Weighted FMeasure: 0.5552868014573312
Iteration time: 690.1
Weighted AreaUnderPRC: 0.5649655081067153
Mean absolute error: 0.09855402382426921
Coverage of cases: 90.32323232323232
Instances selection time: 120.3
Test time: 255.3
Accumulative iteration time: 7273.3
Weighted Recall: 0.5533333333333333
Weighted FalsePositiveRate: 0.04466666666666667
Kappa statistic: 0.5086666666666666
Training time: 569.8
		
Iteration: 12
Labeled set size: 319
Unlabelled set size: 176
	
Mean region size: 32.754820936639234
Incorrectly Classified Instances: 43.37373737373737
Correctly Classified Instances: 56.62626262626263
Weighted Precision: 0.580740878608424
Weighted AreaUnderROC: 0.9112457912457913
Root mean squared error: 0.2349798917128993
Relative absolute error: 58.47965319909804
Root relative squared error: 81.73788283677324
Weighted TruePositiveRate: 0.5662626262626262
Weighted MatthewsCorrelation: 0.5277738656010225
Weighted FMeasure: 0.567295677239464
Iteration time: 684.8
Weighted AreaUnderPRC: 0.5794083898438377
Mean absolute error: 0.09666058380016265
Coverage of cases: 90.94949494949495
Instances selection time: 119.7
Test time: 271.6
Accumulative iteration time: 7958.1
Weighted Recall: 0.5662626262626262
Weighted FalsePositiveRate: 0.043373737373737384
Kappa statistic: 0.522888888888889
Training time: 565.1
		
Iteration: 13
Labeled set size: 339
Unlabelled set size: 156
	
Mean region size: 32.34343434343446
Incorrectly Classified Instances: 42.62626262626263
Correctly Classified Instances: 57.37373737373737
Weighted Precision: 0.586365111353752
Weighted AreaUnderROC: 0.9158711560044892
Root mean squared error: 0.23267275090633105
Relative absolute error: 57.80920528756152
Root relative squared error: 80.93534265531113
Weighted TruePositiveRate: 0.5737373737373737
Weighted MatthewsCorrelation: 0.5352128229557513
Weighted FMeasure: 0.5742351235124747
Iteration time: 686.5
Weighted AreaUnderPRC: 0.5947123121908393
Mean absolute error: 0.0955524054339866
Coverage of cases: 91.13131313131312
Instances selection time: 112.6
Test time: 282.1
Accumulative iteration time: 8644.6
Weighted Recall: 0.5737373737373737
Weighted FalsePositiveRate: 0.04262626262626263
Kappa statistic: 0.5311111111111112
Training time: 573.9
		
Iteration: 14
Labeled set size: 359
Unlabelled set size: 136
	
Mean region size: 32.22038567493125
Incorrectly Classified Instances: 41.17171717171716
Correctly Classified Instances: 58.82828282828283
Weighted Precision: 0.6022551854784938
Weighted AreaUnderROC: 0.9201961840628506
Root mean squared error: 0.22875825925371912
Relative absolute error: 56.48640738619706
Root relative squared error: 79.57368460987442
Weighted TruePositiveRate: 0.5882828282828283
Weighted MatthewsCorrelation: 0.5517980688087558
Weighted FMeasure: 0.5892590770131276
Iteration time: 694.4
Weighted AreaUnderPRC: 0.60822504151961
Mean absolute error: 0.09336596262181393
Coverage of cases: 91.85858585858584
Instances selection time: 111.1
Test time: 284.7
Accumulative iteration time: 9339.0
Weighted Recall: 0.5882828282828283
Weighted FalsePositiveRate: 0.04117171717171717
Kappa statistic: 0.547111111111111
Training time: 583.3
		
Iteration: 15
Labeled set size: 379
Unlabelled set size: 116
	
Mean region size: 31.99265381083573
Incorrectly Classified Instances: 40.42424242424242
Correctly Classified Instances: 59.57575757575758
Weighted Precision: 0.6058881134954428
Weighted AreaUnderROC: 0.9256628507295174
Root mean squared error: 0.22507278163747949
Relative absolute error: 55.304731576965466
Root relative squared error: 78.29168922125704
Weighted TruePositiveRate: 0.5957575757575757
Weighted MatthewsCorrelation: 0.5584790416333856
Weighted FMeasure: 0.5956767894201075
Iteration time: 687.5
Weighted AreaUnderPRC: 0.6228096333508867
Mean absolute error: 0.09141277946605922
Coverage of cases: 92.78787878787878
Instances selection time: 96.5
Test time: 289.3
Accumulative iteration time: 10026.5
Weighted Recall: 0.5957575757575757
Weighted FalsePositiveRate: 0.040424242424242425
Kappa statistic: 0.5553333333333332
Training time: 591.0
		
Iteration: 16
Labeled set size: 399
Unlabelled set size: 96
	
Mean region size: 31.990817263544624
Incorrectly Classified Instances: 39.23232323232323
Correctly Classified Instances: 60.76767676767677
Weighted Precision: 0.617498298815177
Weighted AreaUnderROC: 0.9297171717171718
Root mean squared error: 0.22168674649034736
Relative absolute error: 54.357034942038034
Root relative squared error: 77.11385505800175
Weighted TruePositiveRate: 0.6076767676767677
Weighted MatthewsCorrelation: 0.5716297124415836
Weighted FMeasure: 0.6079726238859375
Iteration time: 697.3
Weighted AreaUnderPRC: 0.635102466182655
Mean absolute error: 0.08984633874717088
Coverage of cases: 93.41414141414143
Instances selection time: 88.9
Test time: 299.0
Accumulative iteration time: 10723.8
Weighted Recall: 0.6076767676767677
Weighted FalsePositiveRate: 0.03923232323232324
Kappa statistic: 0.5684444444444444
Training time: 608.4
		
Iteration: 17
Labeled set size: 419
Unlabelled set size: 76
	
Mean region size: 31.399449035812758
Incorrectly Classified Instances: 39.333333333333336
Correctly Classified Instances: 60.66666666666667
Weighted Precision: 0.6149794143757663
Weighted AreaUnderROC: 0.9326487093153759
Root mean squared error: 0.22012901108399263
Relative absolute error: 53.767251806556466
Root relative squared error: 76.57199595164518
Weighted TruePositiveRate: 0.6066666666666666
Weighted MatthewsCorrelation: 0.569963119147267
Weighted FMeasure: 0.6066959254377456
Iteration time: 694.6
Weighted AreaUnderPRC: 0.641352823837589
Mean absolute error: 0.0888714905893501
Coverage of cases: 93.75757575757576
Instances selection time: 79.6
Test time: 311.0
Accumulative iteration time: 11418.4
Weighted Recall: 0.6066666666666666
Weighted FalsePositiveRate: 0.039333333333333345
Kappa statistic: 0.5673333333333332
Training time: 615.0
		
Iteration: 18
Labeled set size: 439
Unlabelled set size: 56
	
Mean region size: 31.906336088154365
Incorrectly Classified Instances: 38.66666666666667
Correctly Classified Instances: 61.333333333333336
Weighted Precision: 0.6222170593921954
Weighted AreaUnderROC: 0.9374873176206509
Root mean squared error: 0.21791866211852629
Relative absolute error: 53.46811707241778
Root relative squared error: 75.80312486463143
Weighted TruePositiveRate: 0.6133333333333333
Weighted MatthewsCorrelation: 0.5775569840303459
Weighted FMeasure: 0.6136156830851552
Iteration time: 705.1
Weighted AreaUnderPRC: 0.6549358550789808
Mean absolute error: 0.08837705301226137
Coverage of cases: 94.7878787878788
Instances selection time: 65.7
Test time: 301.1
Accumulative iteration time: 12123.5
Weighted Recall: 0.6133333333333333
Weighted FalsePositiveRate: 0.03866666666666667
Kappa statistic: 0.5746666666666667
Training time: 639.4
		
Iteration: 19
Labeled set size: 459
Unlabelled set size: 36
	
Mean region size: 31.777777777777864
Incorrectly Classified Instances: 38.66666666666667
Correctly Classified Instances: 61.333333333333336
Weighted Precision: 0.619306524019452
Weighted AreaUnderROC: 0.9393185185185186
Root mean squared error: 0.21645379365986397
Relative absolute error: 53.068021240380084
Root relative squared error: 75.29356957642098
Weighted TruePositiveRate: 0.6133333333333333
Weighted MatthewsCorrelation: 0.5762024217724994
Weighted FMeasure: 0.6124004522000833
Iteration time: 698.4
Weighted AreaUnderPRC: 0.6633980876491231
Mean absolute error: 0.08771573758740567
Coverage of cases: 95.35353535353535
Instances selection time: 53.4
Test time: 307.5
Accumulative iteration time: 12821.9
Weighted Recall: 0.6133333333333333
Weighted FalsePositiveRate: 0.03866666666666667
Kappa statistic: 0.5746666666666667
Training time: 645.0
		
Iteration: 20
Labeled set size: 479
Unlabelled set size: 16
	
Mean region size: 31.88429752066124
Incorrectly Classified Instances: 38.242424242424235
Correctly Classified Instances: 61.757575757575765
Weighted Precision: 0.6221721965411156
Weighted AreaUnderROC: 0.9418814814814814
Root mean squared error: 0.21454781974937004
Relative absolute error: 52.739136931649355
Root relative squared error: 74.63057551743893
Weighted TruePositiveRate: 0.6175757575757576
Weighted MatthewsCorrelation: 0.5803777230686975
Weighted FMeasure: 0.616468685829485
Iteration time: 707.3
Weighted AreaUnderPRC: 0.673050403768508
Mean absolute error: 0.08717212715975155
Coverage of cases: 95.73737373737373
Instances selection time: 30.8
Test time: 310.1
Accumulative iteration time: 13529.2
Weighted Recall: 0.6175757575757576
Weighted FalsePositiveRate: 0.038242424242424244
Kappa statistic: 0.5793333333333335
Training time: 676.5
		
Time end:Fri Dec 01 13.36.36 EET 2017