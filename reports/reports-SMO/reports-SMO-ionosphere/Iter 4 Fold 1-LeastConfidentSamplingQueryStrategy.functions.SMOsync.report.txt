Mon Nov 27 16.31.16 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.16 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 74.14772727272727
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8083270333270334
Weighted AreaUnderROC: 0.8727349346818373
Root mean squared error: 0.3756635629864613
Relative absolute error: 43.105368012227366
Root relative squared error: 75.13271259729225
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.5828231731728553
Weighted FMeasure: 0.8074649959415584
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8805313242865199
Mean absolute error: 0.21552684006113684
Coverage of cases: 96.02272727272727
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.22007847118466586
Kappa statistic: 0.5826475101129864
Training time: 16.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 72.72727272727273
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8532780748663101
Weighted AreaUnderROC: 0.8492765837898582
Root mean squared error: 0.3522700577569825
Relative absolute error: 39.71576541682129
Root relative squared error: 70.4540115513965
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6725808088285802
Weighted FMeasure: 0.8482218246150134
Iteration time: 16.0
Weighted AreaUnderPRC: 0.85864293126077
Mean absolute error: 0.19857882708410646
Coverage of cases: 93.75
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.21580693151489608
Kappa statistic: 0.6644669306349905
Training time: 13.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 68.18181818181819
Incorrectly Classified Instances: 16.477272727272727
Correctly Classified Instances: 83.52272727272727
Weighted Precision: 0.8425878099173555
Weighted AreaUnderROC: 0.8842534063773002
Root mean squared error: 0.3500068731783543
Relative absolute error: 37.26783038526649
Root relative squared error: 70.00137463567087
Weighted TruePositiveRate: 0.8352272727272727
Weighted MatthewsCorrelation: 0.6363745819188817
Weighted FMeasure: 0.8269870650044214
Iteration time: 22.0
Weighted AreaUnderPRC: 0.8789405379845189
Mean absolute error: 0.18633915192633246
Coverage of cases: 95.45454545454545
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8352272727272727
Weighted FalsePositiveRate: 0.2604274412902731
Kappa statistic: 0.6158940397350993
Training time: 18.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 65.625
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8603573907009022
Weighted AreaUnderROC: 0.9091164489394576
Root mean squared error: 0.3247780939981576
Relative absolute error: 32.20065394253543
Root relative squared error: 64.95561879963152
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6762726937061966
Weighted FMeasure: 0.8454110779930453
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9070740218587688
Mean absolute error: 0.16100326971267717
Coverage of cases: 96.02272727272727
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 84.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.23687730656757203
Kappa statistic: 0.6569200779727096
Training time: 20.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 64.48863636363636
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.868586432506887
Weighted AreaUnderROC: 0.8796179238657115
Root mean squared error: 0.33750561477395635
Relative absolute error: 33.50490357055517
Root relative squared error: 67.50112295479127
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6911164814387856
Weighted FMeasure: 0.85085091810726
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8772254346099285
Mean absolute error: 0.16752451785277586
Coverage of cases: 94.31818181818181
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.2337095672272663
Kappa statistic: 0.6688741721854303
Training time: 17.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 63.92045454545455
Incorrectly Classified Instances: 14.772727272727273
Correctly Classified Instances: 85.22727272727273
Weighted Precision: 0.8639960101098411
Weighted AreaUnderROC: 0.8794774546986937
Root mean squared error: 0.3385119355125574
Relative absolute error: 33.186582812787705
Root relative squared error: 67.70238710251148
Weighted TruePositiveRate: 0.8522727272727273
Weighted MatthewsCorrelation: 0.6787637478397143
Weighted FMeasure: 0.8443413657978774
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8770325716107742
Mean absolute error: 0.16593291406393854
Coverage of cases: 94.31818181818181
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 129.0
Weighted Recall: 0.8522727272727273
Weighted FalsePositiveRate: 0.24390076491846402
Kappa statistic: 0.6543284484061037
Training time: 22.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 65.625
Incorrectly Classified Instances: 14.204545454545455
Correctly Classified Instances: 85.79545454545455
Weighted Precision: 0.8651584828215263
Weighted AreaUnderROC: 0.8900126422250316
Root mean squared error: 0.3309297204490634
Relative absolute error: 33.599115334561226
Root relative squared error: 66.18594408981268
Weighted TruePositiveRate: 0.8579545454545454
Weighted MatthewsCorrelation: 0.6887680284297514
Weighted FMeasure: 0.8518462745017281
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8858244370004471
Mean absolute error: 0.16799557667280612
Coverage of cases: 94.88636363636364
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 157.0
Weighted Recall: 0.8579545454545454
Weighted FalsePositiveRate: 0.22668610887637436
Kappa statistic: 0.6713474753510605
Training time: 22.0
		
Time end:Mon Nov 27 16.31.16 EET 2017