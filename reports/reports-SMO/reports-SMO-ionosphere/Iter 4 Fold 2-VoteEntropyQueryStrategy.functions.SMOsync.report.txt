Mon Nov 27 16.31.31 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.31 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 64.28571428571429
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8618019625334523
Weighted AreaUnderROC: 0.8927154195011338
Root mean squared error: 0.3183790521002999
Relative absolute error: 31.78299879077703
Root relative squared error: 63.675810420059975
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6980669222157452
Weighted FMeasure: 0.8612173913043479
Iteration time: 11.0
Weighted AreaUnderPRC: 0.88354551922511
Mean absolute error: 0.15891499395388514
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.1813095238095238
Kappa statistic: 0.696048632218845
Training time: 10.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 66.57142857142857
Incorrectly Classified Instances: 14.857142857142858
Correctly Classified Instances: 85.14285714285714
Weighted Precision: 0.863368569415081
Weighted AreaUnderROC: 0.8820861678004536
Root mean squared error: 0.33161048100400636
Relative absolute error: 33.04877907122384
Root relative squared error: 66.32209620080127
Weighted TruePositiveRate: 0.8514285714285714
Weighted MatthewsCorrelation: 0.6780442464015918
Weighted FMeasure: 0.8435013918960717
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8715750089224886
Mean absolute error: 0.1652438953561192
Coverage of cases: 95.42857142857143
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8514285714285714
Weighted FalsePositiveRate: 0.24329365079365078
Kappa statistic: 0.6535181236673774
Training time: 9.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 65.42857142857143
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8721804511278195
Weighted AreaUnderROC: 0.8796768707482995
Root mean squared error: 0.3351700291154314
Relative absolute error: 32.66812410203137
Root relative squared error: 67.03400582308629
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6935194938140639
Weighted FMeasure: 0.8489795918367345
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8665849961355935
Mean absolute error: 0.16334062051015685
Coverage of cases: 94.85714285714286
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 35.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.24007936507936506
Kappa statistic: 0.6655965757089352
Training time: 13.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 64.57142857142857
Incorrectly Classified Instances: 13.142857142857142
Correctly Classified Instances: 86.85714285714286
Weighted Precision: 0.8810409437890355
Weighted AreaUnderROC: 0.8774092970521542
Root mean squared error: 0.3334718320655903
Relative absolute error: 32.02490513888063
Root relative squared error: 66.69436641311806
Weighted TruePositiveRate: 0.8685714285714285
Weighted MatthewsCorrelation: 0.7178513413168464
Weighted FMeasure: 0.8620406907426637
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8661165843270656
Mean absolute error: 0.16012452569440314
Coverage of cases: 94.85714285714286
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8685714285714285
Weighted FalsePositiveRate: 0.21976190476190474
Kappa statistic: 0.6946362187997875
Training time: 11.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 64.0
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8721804511278195
Weighted AreaUnderROC: 0.8652210884353742
Root mean squared error: 0.34603771625657576
Relative absolute error: 32.89139683288571
Root relative squared error: 69.20754325131514
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6935194938140639
Weighted FMeasure: 0.8489795918367345
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8517504819247329
Mean absolute error: 0.16445698416442855
Coverage of cases: 92.57142857142857
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.24007936507936506
Kappa statistic: 0.6655965757089352
Training time: 11.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 63.142857142857146
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.907340425531915
Weighted AreaUnderROC: 0.9247448979591837
Root mean squared error: 0.29918448785192325
Relative absolute error: 27.043987062639875
Root relative squared error: 59.83689757038465
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7810867146147322
Weighted FMeasure: 0.893090909090909
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9237494376107576
Mean absolute error: 0.13521993531319937
Coverage of cases: 95.42857142857143
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.1759126984126984
Kappa statistic: 0.7636554621848739
Training time: 12.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 72.85714285714286
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.904573002754821
Weighted AreaUnderROC: 0.957624716553288
Root mean squared error: 0.26180683319142684
Relative absolute error: 29.412028662466533
Root relative squared error: 52.36136663828537
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7876305401878569
Weighted FMeasure: 0.9009970287223505
Iteration time: 16.0
Weighted AreaUnderPRC: 0.952733465132869
Mean absolute error: 0.14706014331233266
Coverage of cases: 99.42857142857143
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.14492063492063492
Kappa statistic: 0.7823860727086532
Training time: 15.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 76.85714285714286
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.8965984804208066
Weighted AreaUnderROC: 0.9590419501133787
Root mean squared error: 0.26050530058282767
Relative absolute error: 30.178801250081516
Root relative squared error: 52.101060116565534
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7746224746580367
Weighted FMeasure: 0.8963589301121656
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9519436693491626
Mean absolute error: 0.15089400625040758
Coverage of cases: 99.42857142857143
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 104.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.13424603174603172
Kappa statistic: 0.7736418511066399
Training time: 16.0
		
Time end:Mon Nov 27 16.31.31 EET 2017