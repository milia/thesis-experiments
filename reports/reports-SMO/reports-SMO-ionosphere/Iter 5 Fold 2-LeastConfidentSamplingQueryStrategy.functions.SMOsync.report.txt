Mon Nov 27 16.31.17 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.17 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 73.71428571428571
Incorrectly Classified Instances: 21.142857142857142
Correctly Classified Instances: 78.85714285714286
Weighted Precision: 0.8140821678321678
Weighted AreaUnderROC: 0.8574263038548752
Root mean squared error: 0.3932358201429405
Relative absolute error: 47.04010083225667
Root relative squared error: 78.64716402858811
Weighted TruePositiveRate: 0.7885714285714286
Weighted MatthewsCorrelation: 0.5383406605150606
Weighted FMeasure: 0.7669267285861713
Iteration time: 36.0
Weighted AreaUnderPRC: 0.84487946148733
Mean absolute error: 0.23520050416128335
Coverage of cases: 96.57142857142857
Instances selection time: 11.0
Test time: 6.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7885714285714286
Weighted FalsePositiveRate: 0.35503968253968254
Kappa statistic: 0.48582545858810455
Training time: 25.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 66.28571428571429
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8693617021276595
Weighted AreaUnderROC: 0.8643707482993198
Root mean squared error: 0.32790377139268795
Relative absolute error: 32.26776697389355
Root relative squared error: 65.58075427853758
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.7005069297507639
Weighted FMeasure: 0.8574545454545454
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8486670089838944
Mean absolute error: 0.16133883486946776
Coverage of cases: 94.85714285714286
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.21603174603174605
Kappa statistic: 0.684873949579832
Training time: 17.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 65.71428571428571
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8440425531914894
Weighted AreaUnderROC: 0.810657596371882
Root mean squared error: 0.34861820429592955
Relative absolute error: 34.342223819508945
Root relative squared error: 69.7236408591859
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.6467870731747851
Weighted FMeasure: 0.8336969696969697
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8043853152380506
Mean absolute error: 0.17171111909754472
Coverage of cases: 91.42857142857143
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 87.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.24277777777777776
Kappa statistic: 0.6323529411764706
Training time: 23.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 62.57142857142857
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8679736294240111
Weighted AreaUnderROC: 0.8151927437641725
Root mean squared error: 0.3506284744169163
Relative absolute error: 33.567935189456826
Root relative squared error: 70.12569488338326
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6904105408077927
Weighted FMeasure: 0.8500442290681128
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8048581883458827
Mean absolute error: 0.16783967594728413
Coverage of cases: 91.42857142857143
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.2331349206349206
Kappa statistic: 0.6680828465215082
Training time: 23.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 62.285714285714285
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8679736294240111
Weighted AreaUnderROC: 0.8157596371882087
Root mean squared error: 0.35022608403974403
Relative absolute error: 33.65990937265176
Root relative squared error: 70.0452168079488
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6904105408077927
Weighted FMeasure: 0.8500442290681128
Iteration time: 24.0
Weighted AreaUnderPRC: 0.804541329840607
Mean absolute error: 0.1682995468632588
Coverage of cases: 91.42857142857143
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.2331349206349206
Kappa statistic: 0.6680828465215082
Training time: 23.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 67.42857142857143
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8867913385826771
Weighted AreaUnderROC: 0.8581349206349205
Root mean squared error: 0.3140048010820802
Relative absolute error: 31.293178179011605
Root relative squared error: 62.80096021641604
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7396555313915459
Weighted FMeasure: 0.8756575822684609
Iteration time: 17.0
Weighted AreaUnderPRC: 0.837151682329291
Mean absolute error: 0.15646589089505802
Coverage of cases: 95.42857142857143
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 151.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.1925
Kappa statistic: 0.7252747252747253
Training time: 16.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 67.14285714285714
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8867913385826771
Weighted AreaUnderROC: 0.8579931972789115
Root mean squared error: 0.31408163172253073
Relative absolute error: 30.962296238179277
Root relative squared error: 62.816326344506145
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7396555313915459
Weighted FMeasure: 0.8756575822684609
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8369247524268955
Mean absolute error: 0.15481148119089638
Coverage of cases: 95.42857142857143
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 171.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.1925
Kappa statistic: 0.7252747252747253
Training time: 20.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 76.57142857142857
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.89648
Weighted AreaUnderROC: 0.8721655328798186
Root mean squared error: 0.30134216695060956
Relative absolute error: 34.139588827348355
Root relative squared error: 60.26843339012191
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7642171012073582
Weighted FMeasure: 0.8881610096710355
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8504012401958646
Mean absolute error: 0.17069794413674177
Coverage of cases: 96.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 195.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1721825396825397
Kappa statistic: 0.7532467532467533
Training time: 24.0
		
Time end:Mon Nov 27 16.31.18 EET 2017