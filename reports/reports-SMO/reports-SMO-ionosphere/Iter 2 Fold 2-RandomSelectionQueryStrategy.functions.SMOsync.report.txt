Mon Nov 27 16.31.24 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.24 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 61.714285714285715
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8679736294240111
Weighted AreaUnderROC: 0.8618197278911565
Root mean squared error: 0.35989614301984674
Relative absolute error: 32.90964964800559
Root relative squared error: 71.97922860396935
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6904105408077927
Weighted FMeasure: 0.8500442290681128
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8512971406940057
Mean absolute error: 0.16454824824002795
Coverage of cases: 93.14285714285714
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.2331349206349206
Kappa statistic: 0.6680828465215082
Training time: 8.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 61.142857142857146
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.89648
Weighted AreaUnderROC: 0.9044784580498866
Root mean squared error: 0.2975650667719582
Relative absolute error: 25.78943014798101
Root relative squared error: 59.51301335439164
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7642171012073582
Weighted FMeasure: 0.8881610096710355
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8886036707782607
Mean absolute error: 0.12894715073990506
Coverage of cases: 95.42857142857143
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1721825396825397
Kappa statistic: 0.7532467532467533
Training time: 9.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 64.57142857142857
Incorrectly Classified Instances: 10.285714285714286
Correctly Classified Instances: 89.71428571428571
Weighted Precision: 0.9014041745730551
Weighted AreaUnderROC: 0.9104308390022676
Root mean squared error: 0.28999708650552425
Relative absolute error: 26.873953708838133
Root relative squared error: 57.99941730110485
Weighted TruePositiveRate: 0.8971428571428571
Weighted MatthewsCorrelation: 0.7764992333905041
Weighted FMeasure: 0.8943443354148082
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8917543201858794
Mean absolute error: 0.13436976854419067
Coverage of cases: 96.57142857142857
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8971428571428571
Weighted FalsePositiveRate: 0.16202380952380951
Kappa statistic: 0.7670807453416149
Training time: 10.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 64.0
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8946808510638299
Weighted AreaUnderROC: 0.915107709750567
Root mean squared error: 0.29358759666786644
Relative absolute error: 26.097520033240336
Root relative squared error: 58.717519333573286
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7542267863267427
Weighted FMeasure: 0.8812121212121211
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8986927020843886
Mean absolute error: 0.13048760016620167
Coverage of cases: 97.14285714285714
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.18928571428571428
Kappa statistic: 0.7373949579831932
Training time: 19.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 67.71428571428571
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8946808510638299
Weighted AreaUnderROC: 0.9007936507936508
Root mean squared error: 0.3000632655675318
Relative absolute error: 30.08615804068991
Root relative squared error: 60.01265311350637
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7542267863267427
Weighted FMeasure: 0.8812121212121211
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8878313293729224
Mean absolute error: 0.15043079020344954
Coverage of cases: 96.57142857142857
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.18928571428571428
Kappa statistic: 0.7373949579831932
Training time: 25.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 65.14285714285714
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8891840607210626
Weighted AreaUnderROC: 0.9016439909297054
Root mean squared error: 0.307199230101242
Relative absolute error: 29.304137967973702
Root relative squared error: 61.4398460202484
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.750301553451553
Weighted FMeasure: 0.8826048171275648
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8914119525265689
Mean absolute error: 0.14652068983986852
Coverage of cases: 96.0
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.17539682539682538
Kappa statistic: 0.7412008281573498
Training time: 31.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 66.85714285714286
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8942589118198875
Weighted AreaUnderROC: 0.8972505668934241
Root mean squared error: 0.2980173914237257
Relative absolute error: 29.023188605033027
Root relative squared error: 59.60347828474514
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7627383930025419
Weighted FMeasure: 0.8887770582793709
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8823338764472988
Mean absolute error: 0.14511594302516514
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 128.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1652380952380952
Kappa statistic: 0.7550283651366685
Training time: 16.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 66.0
Incorrectly Classified Instances: 9.714285714285714
Correctly Classified Instances: 90.28571428571429
Weighted Precision: 0.9063852407754847
Weighted AreaUnderROC: 0.8921485260770974
Root mean squared error: 0.3003014822196545
Relative absolute error: 29.142734541417077
Root relative squared error: 60.0602964439309
Weighted TruePositiveRate: 0.9028571428571428
Weighted MatthewsCorrelation: 0.7887882015067271
Weighted FMeasure: 0.9004847363552266
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8762269526875477
Mean absolute error: 0.14571367270708538
Coverage of cases: 96.0
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9028571428571428
Weighted FalsePositiveRate: 0.15186507936507934
Kappa statistic: 0.780814853017019
Training time: 31.0
		
Time end:Mon Nov 27 16.31.25 EET 2017