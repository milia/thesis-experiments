Mon Nov 27 16.31.26 EET 2017
Dataset: ionosphere
Test set size: 176
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.26 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 50.28409090909091
Incorrectly Classified Instances: 17.613636363636363
Correctly Classified Instances: 82.38636363636364
Weighted Precision: 0.8367396390374332
Weighted AreaUnderROC: 0.7794833607886705
Root mean squared error: 0.4186934827781461
Relative absolute error: 35.23944733271871
Root relative squared error: 83.73869655562922
Weighted TruePositiveRate: 0.8238636363636364
Weighted MatthewsCorrelation: 0.613196150388918
Weighted FMeasure: 0.8123327378357206
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7976344909571037
Mean absolute error: 0.17619723666359355
Coverage of cases: 82.95454545454545
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8238636363636364
Weighted FalsePositiveRate: 0.2878332950235605
Kappa statistic: 0.5831295843520782
Training time: 20.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 60.22727272727273
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8070280989009341
Weighted AreaUnderROC: 0.7765135552746171
Root mean squared error: 0.41623938877585875
Relative absolute error: 41.75227337822655
Root relative squared error: 83.24787775517176
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.567291285511284
Weighted FMeasure: 0.7991547708489858
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8030777153174988
Mean absolute error: 0.20876136689113273
Coverage of cases: 87.5
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.2832895963426937
Kappa statistic: 0.5546956392320285
Training time: 10.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 58.52272727272727
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8070280989009341
Weighted AreaUnderROC: 0.7992695603315073
Root mean squared error: 0.4119853673863141
Relative absolute error: 39.94400923830442
Root relative squared error: 82.39707347726282
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.567291285511284
Weighted FMeasure: 0.7991547708489858
Iteration time: 12.0
Weighted AreaUnderPRC: 0.810875120165932
Mean absolute error: 0.1997200461915221
Coverage of cases: 88.06818181818181
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.2832895963426937
Kappa statistic: 0.5546956392320285
Training time: 11.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 64.77272727272727
Incorrectly Classified Instances: 17.613636363636363
Correctly Classified Instances: 82.38636363636364
Weighted Precision: 0.8234650072150071
Weighted AreaUnderROC: 0.8435173479421268
Root mean squared error: 0.3759287179586645
Relative absolute error: 38.65400422405912
Root relative squared error: 75.1857435917329
Weighted TruePositiveRate: 0.8238636363636364
Weighted MatthewsCorrelation: 0.6071382620393185
Weighted FMeasure: 0.818522218145464
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8398683354145743
Mean absolute error: 0.1932700211202956
Coverage of cases: 93.18181818181819
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 64.0
Weighted Recall: 0.8238636363636364
Weighted FalsePositiveRate: 0.25271600326910054
Kappa statistic: 0.5984692375625552
Training time: 17.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 64.48863636363636
Incorrectly Classified Instances: 19.318181818181817
Correctly Classified Instances: 80.68181818181819
Weighted Precision: 0.8046577540106952
Weighted AreaUnderROC: 0.8384604579294844
Root mean squared error: 0.3807138548267487
Relative absolute error: 39.43580088529152
Root relative squared error: 76.14277096534974
Weighted TruePositiveRate: 0.8068181818181818
Weighted MatthewsCorrelation: 0.5680791779710828
Weighted FMeasure: 0.8015208475734791
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8320832580941296
Mean absolute error: 0.1971790044264576
Coverage of cases: 93.18181818181819
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 77.0
Weighted Recall: 0.8068181818181818
Weighted FalsePositiveRate: 0.26924267964090975
Kappa statistic: 0.5612259862149874
Training time: 12.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 67.61363636363636
Incorrectly Classified Instances: 18.75
Correctly Classified Instances: 81.25
Weighted Precision: 0.8112103174603175
Weighted AreaUnderROC: 0.8350891979210563
Root mean squared error: 0.38145619512381285
Relative absolute error: 40.29550833410832
Root relative squared error: 76.29123902476258
Weighted TruePositiveRate: 0.8125
Weighted MatthewsCorrelation: 0.5808578060336814
Weighted FMeasure: 0.8068139741548488
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8291633589310191
Mean absolute error: 0.2014775416705416
Coverage of cases: 94.88636363636364
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 93.0
Weighted Recall: 0.8125
Weighted FalsePositiveRate: 0.266074940300604
Kappa statistic: 0.57256402708272
Training time: 15.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 66.47727272727273
Incorrectly Classified Instances: 18.181818181818183
Correctly Classified Instances: 81.81818181818181
Weighted Precision: 0.817977298292259
Weighted AreaUnderROC: 0.8384604579294844
Root mean squared error: 0.3853259330092557
Relative absolute error: 40.799636106056525
Root relative squared error: 77.06518660185114
Weighted TruePositiveRate: 0.8181818181818182
Weighted MatthewsCorrelation: 0.5939055960018766
Weighted FMeasure: 0.8121212121212121
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8342479244994908
Mean absolute error: 0.20399818053028262
Coverage of cases: 93.75
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8181818181818182
Weighted FalsePositiveRate: 0.2629072009602983
Kappa statistic: 0.5839858176983308
Training time: 18.0
		
Time end:Mon Nov 27 16.31.26 EET 2017