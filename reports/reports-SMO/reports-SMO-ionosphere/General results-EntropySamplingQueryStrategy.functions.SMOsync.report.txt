Mon Nov 27 16.30.55 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 140
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.30.55 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 140
	
Mean region size: 65.8577922077922
Incorrectly Classified Instances: 16.69707792207792
Correctly Classified Instances: 83.30292207792209
Weighted Precision: 0.8465895196472148
Weighted AreaUnderROC: 0.8524874562858127
Root mean squared error: 0.3703558308876669
Relative absolute error: 38.30202278650155
Root relative squared error: 74.07116617753339
Weighted TruePositiveRate: 0.8330292207792207
Weighted MatthewsCorrelation: 0.6378198044916489
Weighted FMeasure: 0.8235534964976677
Iteration time: 64.1
Weighted AreaUnderPRC: 0.8516866240375018
Mean absolute error: 0.19151011393250772
Coverage of cases: 92.13116883116882
Instances selection time: 12.3
Test time: 18.4
Accumulative iteration time: 64.1
Weighted Recall: 0.8330292207792207
Weighted FalsePositiveRate: 0.25470536911466113
Kappa statistic: 0.6122463922495597
Training time: 51.8
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 120
	
Mean region size: 66.03409090909092
Incorrectly Classified Instances: 15.15649350649351
Correctly Classified Instances: 84.84350649350651
Weighted Precision: 0.8613907971997904
Weighted AreaUnderROC: 0.8439507728540597
Root mean squared error: 0.3555476877865331
Relative absolute error: 35.34751435437036
Root relative squared error: 71.10953755730661
Weighted TruePositiveRate: 0.8484350649350649
Weighted MatthewsCorrelation: 0.6711586892900188
Weighted FMeasure: 0.8396181832693346
Iteration time: 38.4
Weighted AreaUnderPRC: 0.8404532285135538
Mean absolute error: 0.1767375717718518
Coverage of cases: 92.25259740259739
Instances selection time: 7.3
Test time: 9.3
Accumulative iteration time: 102.5
Weighted Recall: 0.8484350649350649
Weighted FalsePositiveRate: 0.24622530935141557
Kappa statistic: 0.6450658794634774
Training time: 31.1
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 100
	
Mean region size: 63.27045454545454
Incorrectly Classified Instances: 13.788636363636362
Correctly Classified Instances: 86.21136363636364
Weighted Precision: 0.8721464822392984
Weighted AreaUnderROC: 0.8730760740874519
Root mean squared error: 0.3412299628697879
Relative absolute error: 32.28518755890176
Root relative squared error: 68.24599257395758
Weighted TruePositiveRate: 0.8621136363636364
Weighted MatthewsCorrelation: 0.7008195791785944
Weighted FMeasure: 0.8556106425034157
Iteration time: 44.2
Weighted AreaUnderPRC: 0.8651872680383541
Mean absolute error: 0.1614259377945088
Coverage of cases: 92.98961038961039
Instances selection time: 7.9
Test time: 10.9
Accumulative iteration time: 146.7
Weighted Recall: 0.8621136363636364
Weighted FalsePositiveRate: 0.22388407462743745
Kappa statistic: 0.6801851824917703
Training time: 36.3
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 80
	
Mean region size: 63.758603896103885
Incorrectly Classified Instances: 13.730519480519481
Correctly Classified Instances: 86.26948051948051
Weighted Precision: 0.8688876407549717
Weighted AreaUnderROC: 0.8755798115706458
Root mean squared error: 0.3355479402645235
Relative absolute error: 31.846912391880817
Root relative squared error: 67.10958805290471
Weighted TruePositiveRate: 0.8626948051948051
Weighted MatthewsCorrelation: 0.6994074809225251
Weighted FMeasure: 0.8572305107588439
Iteration time: 49.8
Weighted AreaUnderPRC: 0.8710536283258941
Mean absolute error: 0.15923456195940408
Coverage of cases: 93.96006493506493
Instances selection time: 7.4
Test time: 8.8
Accumulative iteration time: 196.5
Weighted Recall: 0.8626948051948051
Weighted FalsePositiveRate: 0.21448929880345807
Kappa statistic: 0.6841405062954722
Training time: 42.4
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 60
	
Mean region size: 64.44545454545455
Incorrectly Classified Instances: 13.84512987012987
Correctly Classified Instances: 86.15487012987012
Weighted Precision: 0.8673606389845784
Weighted AreaUnderROC: 0.8842505217426202
Root mean squared error: 0.32985454854165674
Relative absolute error: 31.902722722889337
Root relative squared error: 65.97090970833133
Weighted TruePositiveRate: 0.8615487012987014
Weighted MatthewsCorrelation: 0.6965828191626962
Weighted FMeasure: 0.8561438282152996
Iteration time: 47.5
Weighted AreaUnderPRC: 0.878242561635213
Mean absolute error: 0.15951361361444666
Coverage of cases: 94.70259740259739
Instances selection time: 5.3
Test time: 10.0
Accumulative iteration time: 244.0
Weighted Recall: 0.8615487012987014
Weighted FalsePositiveRate: 0.21515261336500272
Kappa statistic: 0.6817833251452539
Training time: 42.2
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 40
	
Mean region size: 65.84025974025974
Incorrectly Classified Instances: 13.559090909090909
Correctly Classified Instances: 86.44090909090909
Weighted Precision: 0.8685960694679921
Weighted AreaUnderROC: 0.8921068870025886
Root mean squared error: 0.32296002460157264
Relative absolute error: 31.79086350602131
Root relative squared error: 64.59200492031451
Weighted TruePositiveRate: 0.8644090909090909
Weighted MatthewsCorrelation: 0.7022661733651744
Weighted FMeasure: 0.859852861790632
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8887075417684187
Mean absolute error: 0.15895431753010655
Coverage of cases: 95.55714285714285
Instances selection time: 4.7
Test time: 11.5
Accumulative iteration time: 292.0
Weighted Recall: 0.8644090909090909
Weighted FalsePositiveRate: 0.20587032141899395
Kappa statistic: 0.6903589402949328
Training time: 43.3
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 20
	
Mean region size: 66.92467532467532
Incorrectly Classified Instances: 13.217857142857145
Correctly Classified Instances: 86.78214285714286
Weighted Precision: 0.8708041848310499
Weighted AreaUnderROC: 0.8937372072722894
Root mean squared error: 0.3212968890101221
Relative absolute error: 32.22164729665487
Root relative squared error: 64.25937780202442
Weighted TruePositiveRate: 0.8678214285714286
Weighted MatthewsCorrelation: 0.7092175794508095
Weighted FMeasure: 0.8638509869657266
Iteration time: 52.9
Weighted AreaUnderPRC: 0.8902034441361334
Mean absolute error: 0.16110823648327438
Coverage of cases: 95.95616883116882
Instances selection time: 6.0
Test time: 9.5
Accumulative iteration time: 344.9
Weighted Recall: 0.8678214285714286
Weighted FalsePositiveRate: 0.1983541578873437
Kappa statistic: 0.699354022637454
Training time: 46.9
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 71.54285714285713
Incorrectly Classified Instances: 12.571428571428573
Correctly Classified Instances: 87.42857142857142
Weighted Precision: 0.877214829449249
Weighted AreaUnderROC: 0.9035430839002269
Root mean squared error: 0.3064677076776018
Relative absolute error: 32.5779380302762
Root relative squared error: 61.293541535520355
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.7245595457905217
Weighted FMeasure: 0.8709130759017348
Iteration time: 57.4
Weighted AreaUnderPRC: 0.8949460227309384
Mean absolute error: 0.162889690151381
Coverage of cases: 97.02857142857142
Instances selection time: 7.2
Test time: 7.4
Accumulative iteration time: 346.8
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.1859920634920635
Kappa statistic: 0.7157376501926362
Training time: 50.2
		
Time end:Mon Nov 27 16.31.04 EET 2017