Mon Nov 27 16.31.27 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.27 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 75.14285714285714
Incorrectly Classified Instances: 29.142857142857142
Correctly Classified Instances: 70.85714285714286
Weighted Precision: 0.7141946124244091
Weighted AreaUnderROC: 0.7514172335600907
Root mean squared error: 0.4698992189274431
Relative absolute error: 62.28076563223656
Root relative squared error: 93.97984378548861
Weighted TruePositiveRate: 0.7085714285714285
Weighted MatthewsCorrelation: 0.3790566624293941
Weighted FMeasure: 0.7108062323538638
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7548678811518263
Mean absolute error: 0.3114038281611828
Coverage of cases: 89.14285714285714
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7085714285714285
Weighted FalsePositiveRate: 0.32365079365079363
Kappa statistic: 0.37835202340321783
Training time: 15.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 75.71428571428571
Incorrectly Classified Instances: 15.428571428571429
Correctly Classified Instances: 84.57142857142857
Weighted Precision: 0.8457535959974984
Weighted AreaUnderROC: 0.8669217687074829
Root mean squared error: 0.35530962349676154
Relative absolute error: 41.669836127506045
Root relative squared error: 71.0619246993523
Weighted TruePositiveRate: 0.8457142857142858
Weighted MatthewsCorrelation: 0.6585391589858012
Weighted FMeasure: 0.8419463459759483
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8610646658298305
Mean absolute error: 0.20834918063753022
Coverage of cases: 97.14285714285714
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8457142857142858
Weighted FalsePositiveRate: 0.21873015873015875
Kappa statistic: 0.6518824136152657
Training time: 10.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 73.71428571428571
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8632725023198269
Weighted AreaUnderROC: 0.8664965986394557
Root mean squared error: 0.34163355258594924
Relative absolute error: 38.06402979889725
Root relative squared error: 68.32671051718985
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6974546402604352
Weighted FMeasure: 0.8598762157382848
Iteration time: 11.0
Weighted AreaUnderPRC: 0.870166047465751
Mean absolute error: 0.19032014899448627
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.1951984126984127
Kappa statistic: 0.6916752312435765
Training time: 10.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 69.71428571428571
Incorrectly Classified Instances: 13.714285714285714
Correctly Classified Instances: 86.28571428571429
Weighted Precision: 0.8632725023198269
Weighted AreaUnderROC: 0.8921485260770974
Root mean squared error: 0.33200210034264355
Relative absolute error: 35.6463769134263
Root relative squared error: 66.4004200685287
Weighted TruePositiveRate: 0.8628571428571429
Weighted MatthewsCorrelation: 0.6974546402604352
Weighted FMeasure: 0.8598762157382848
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8971387533550108
Mean absolute error: 0.17823188456713152
Coverage of cases: 96.57142857142857
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8628571428571429
Weighted FalsePositiveRate: 0.1951984126984127
Kappa statistic: 0.6916752312435765
Training time: 10.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 72.85714285714286
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8841600000000001
Weighted AreaUnderROC: 0.8948412698412699
Root mean squared error: 0.3202126138297473
Relative absolute error: 33.781067453086735
Root relative squared error: 64.04252276594946
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7378647873726218
Weighted FMeasure: 0.8763884843732498
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9043299100531551
Mean absolute error: 0.16890533726543366
Coverage of cases: 96.57142857142857
Instances selection time: 9.0
Test time: 1.0
Accumulative iteration time: 83.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.18555555555555556
Kappa statistic: 0.7272727272727273
Training time: 15.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 70.0
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8891840607210626
Weighted AreaUnderROC: 0.8901643990929705
Root mean squared error: 0.30803363407181583
Relative absolute error: 30.310094496933317
Root relative squared error: 61.606726814363164
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.750301553451553
Weighted FMeasure: 0.8826048171275648
Iteration time: 13.0
Weighted AreaUnderPRC: 0.88787962789442
Mean absolute error: 0.15155047248466658
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.17539682539682538
Kappa statistic: 0.7412008281573498
Training time: 12.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 71.14285714285714
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8891840607210626
Weighted AreaUnderROC: 0.8942743764172334
Root mean squared error: 0.30562083631769377
Relative absolute error: 30.44422895929507
Root relative squared error: 61.12416726353875
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.750301553451553
Weighted FMeasure: 0.8826048171275648
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8917000785036114
Mean absolute error: 0.15222114479647536
Coverage of cases: 96.57142857142857
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 122.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.17539682539682538
Kappa statistic: 0.7412008281573498
Training time: 25.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 73.42857142857143
Incorrectly Classified Instances: 12.0
Correctly Classified Instances: 88.0
Weighted Precision: 0.8841600000000001
Weighted AreaUnderROC: 0.9075963718820862
Root mean squared error: 0.2916588073911687
Relative absolute error: 30.987751101310135
Root relative squared error: 58.33176147823374
Weighted TruePositiveRate: 0.88
Weighted MatthewsCorrelation: 0.7378647873726218
Weighted FMeasure: 0.8763884843732498
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9060595310043464
Mean absolute error: 0.15493875550655067
Coverage of cases: 96.57142857142857
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 140.0
Weighted Recall: 0.88
Weighted FalsePositiveRate: 0.18555555555555556
Kappa statistic: 0.7272727272727273
Training time: 17.0
		
Time end:Mon Nov 27 16.31.28 EET 2017