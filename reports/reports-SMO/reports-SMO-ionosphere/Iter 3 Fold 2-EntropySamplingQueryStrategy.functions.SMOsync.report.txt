Mon Nov 27 16.31.00 EET 2017
Dataset: ionosphere
Test set size: 175
Initial Labelled set size: 35
Initial Unlabelled set size: 141
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.31.00 EET 2017
		
Iteration: 1
Labeled set size: 35
Unlabelled set size: 141
	
Mean region size: 70.28571428571429
Incorrectly Classified Instances: 14.285714285714286
Correctly Classified Instances: 85.71428571428571
Weighted Precision: 0.8679736294240111
Weighted AreaUnderROC: 0.864795918367347
Root mean squared error: 0.3314941235123064
Relative absolute error: 35.943422243990675
Root relative squared error: 66.29882470246127
Weighted TruePositiveRate: 0.8571428571428571
Weighted MatthewsCorrelation: 0.6904105408077927
Weighted FMeasure: 0.8500442290681128
Iteration time: 28.0
Weighted AreaUnderPRC: 0.854090198664336
Mean absolute error: 0.17971711121995337
Coverage of cases: 94.28571428571429
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8571428571428571
Weighted FalsePositiveRate: 0.2331349206349206
Kappa statistic: 0.6680828465215082
Training time: 26.0
		
Iteration: 2
Labeled set size: 55
Unlabelled set size: 121
	
Mean region size: 72.85714285714286
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8298076923076924
Weighted AreaUnderROC: 0.7966269841269842
Root mean squared error: 0.3866914993758066
Relative absolute error: 42.40641824660888
Root relative squared error: 77.33829987516133
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.569138181139492
Weighted FMeasure: 0.7795252837977296
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8002203627747606
Mean absolute error: 0.2120320912330444
Coverage of cases: 92.57142857142857
Instances selection time: 8.0
Test time: 3.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.3416666666666667
Kappa statistic: 0.5136186770428017
Training time: 25.0
		
Iteration: 3
Labeled set size: 75
Unlabelled set size: 101
	
Mean region size: 62.857142857142854
Incorrectly Classified Instances: 12.571428571428571
Correctly Classified Instances: 87.42857142857143
Weighted Precision: 0.8949253731343284
Weighted AreaUnderROC: 0.9006519274376417
Root mean squared error: 0.3247544082872567
Relative absolute error: 29.93061805652267
Root relative squared error: 64.95088165745135
Weighted TruePositiveRate: 0.8742857142857143
Weighted MatthewsCorrelation: 0.7375275805057283
Weighted FMeasure: 0.8666103814884303
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8901118184776766
Mean absolute error: 0.14965309028261334
Coverage of cases: 94.28571428571429
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8742857142857143
Weighted FalsePositiveRate: 0.22349206349206346
Kappa statistic: 0.7046186895810956
Training time: 33.0
		
Iteration: 4
Labeled set size: 95
Unlabelled set size: 81
	
Mean region size: 66.28571428571429
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8946808510638299
Weighted AreaUnderROC: 0.8921485260770974
Root mean squared error: 0.3079627804210813
Relative absolute error: 29.66154930982262
Root relative squared error: 61.592556084216255
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7542267863267427
Weighted FMeasure: 0.8812121212121211
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8881892297432548
Mean absolute error: 0.1483077465491131
Coverage of cases: 94.85714285714286
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.18928571428571428
Kappa statistic: 0.7373949579831932
Training time: 27.0
		
Iteration: 5
Labeled set size: 115
Unlabelled set size: 61
	
Mean region size: 67.71428571428571
Incorrectly Classified Instances: 11.428571428571429
Correctly Classified Instances: 88.57142857142857
Weighted Precision: 0.8946808510638299
Weighted AreaUnderROC: 0.9197845804988662
Root mean squared error: 0.2975591450992577
Relative absolute error: 29.56707951393134
Root relative squared error: 59.51182901985154
Weighted TruePositiveRate: 0.8857142857142857
Weighted MatthewsCorrelation: 0.7542267863267427
Weighted FMeasure: 0.8812121212121211
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9132030974510909
Mean absolute error: 0.1478353975696567
Coverage of cases: 97.14285714285714
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 168.0
Weighted Recall: 0.8857142857142857
Weighted FalsePositiveRate: 0.18928571428571428
Kappa statistic: 0.7373949579831932
Training time: 31.0
		
Iteration: 6
Labeled set size: 135
Unlabelled set size: 41
	
Mean region size: 66.57142857142857
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.8993307086614173
Weighted AreaUnderROC: 0.9108560090702946
Root mean squared error: 0.2961792945415778
Relative absolute error: 29.460138282174462
Root relative squared error: 59.235858908315564
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7663386313695959
Weighted FMeasure: 0.8874997172905124
Iteration time: 46.0
Weighted AreaUnderPRC: 0.9046209476105092
Mean absolute error: 0.14730069141087232
Coverage of cases: 96.0
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 214.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.17912698412698413
Kappa statistic: 0.7514390371533229
Training time: 40.0
		
Iteration: 7
Labeled set size: 155
Unlabelled set size: 21
	
Mean region size: 72.28571428571429
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.89648
Weighted AreaUnderROC: 0.9122732426303853
Root mean squared error: 0.2944708879584622
Relative absolute error: 31.047163389568965
Root relative squared error: 58.89417759169244
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7642171012073582
Weighted FMeasure: 0.8881610096710355
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9081069118284284
Mean absolute error: 0.15523581694784483
Coverage of cases: 97.14285714285714
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 265.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1721825396825397
Kappa statistic: 0.7532467532467533
Training time: 47.0
		
Iteration: 8
Labeled set size: 175
Unlabelled set size: 1
	
Mean region size: 72.0
Incorrectly Classified Instances: 10.857142857142858
Correctly Classified Instances: 89.14285714285714
Weighted Precision: 0.89648
Weighted AreaUnderROC: 0.9112811791383221
Root mean squared error: 0.29445664761456697
Relative absolute error: 30.948218031359044
Root relative squared error: 58.891329522913395
Weighted TruePositiveRate: 0.8914285714285715
Weighted MatthewsCorrelation: 0.7642171012073582
Weighted FMeasure: 0.8881610096710355
Iteration time: 59.0
Weighted AreaUnderPRC: 0.9067895871012261
Mean absolute error: 0.1547410901567952
Coverage of cases: 97.14285714285714
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 324.0
Weighted Recall: 0.8914285714285715
Weighted FalsePositiveRate: 0.1721825396825397
Kappa statistic: 0.7532467532467533
Training time: 50.0
		
Time end:Mon Nov 27 16.31.01 EET 2017