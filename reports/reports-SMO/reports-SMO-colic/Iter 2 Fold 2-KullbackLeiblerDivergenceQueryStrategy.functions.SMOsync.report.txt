Mon Nov 27 15.56.35 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.56.35 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 57.06521739130435
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7686869151357689
Weighted AreaUnderROC: 0.8024916769556398
Root mean squared error: 0.44860557504941373
Relative absolute error: 45.002803861470376
Root relative squared error: 89.72111500988275
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.502310798467565
Weighted FMeasure: 0.7693488097580168
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7909575655278288
Mean absolute error: 0.22501401930735188
Coverage of cases: 83.69565217391305
Instances selection time: 11.0
Test time: 9.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.27985272069847433
Kappa statistic: 0.5010330578512396
Training time: 20.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 52.71739130434783
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7794296400187003
Weighted AreaUnderROC: 0.7983866522621043
Root mean squared error: 0.45673361620692937
Relative absolute error: 43.73210424889983
Root relative squared error: 91.34672324138587
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.524232311325891
Weighted FMeasure: 0.7794384057971016
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7928065533321211
Mean absolute error: 0.21866052124449914
Coverage of cases: 80.97826086956522
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 66.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.27348090660552077
Kappa statistic: 0.5218295218295219
Training time: 29.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 62.22826086956522
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7794296400187003
Weighted AreaUnderROC: 0.7852434077079108
Root mean squared error: 0.43702538291205917
Relative absolute error: 46.358815238453666
Root relative squared error: 87.40507658241184
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.524232311325891
Weighted FMeasure: 0.7794384057971016
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7722869028822292
Mean absolute error: 0.23179407619226833
Coverage of cases: 85.8695652173913
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 107.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.27348090660552077
Kappa statistic: 0.5218295218295219
Training time: 35.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 66.57608695652173
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.7356744704570791
Weighted AreaUnderROC: 0.7467038539553753
Root mean squared error: 0.48665484838932105
Relative absolute error: 58.09044859576913
Root relative squared error: 97.3309696778642
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.4018673803878
Weighted FMeasure: 0.7124023657952955
Iteration time: 33.0
Weighted AreaUnderPRC: 0.756938316244096
Mean absolute error: 0.29045224297884564
Coverage of cases: 84.78260869565217
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.39343196049034307
Kappa statistic: 0.3731924360400446
Training time: 31.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 67.3913043478261
Incorrectly Classified Instances: 30.97826086956522
Correctly Classified Instances: 69.02173913043478
Weighted Precision: 0.6803790412486065
Weighted AreaUnderROC: 0.7426470588235294
Root mean squared error: 0.4983196726588487
Relative absolute error: 61.85444251470011
Root relative squared error: 99.66393453176974
Weighted TruePositiveRate: 0.6902173913043478
Weighted MatthewsCorrelation: 0.29166827756908875
Weighted FMeasure: 0.6654476500067723
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7448830168244713
Mean absolute error: 0.30927221257350057
Coverage of cases: 86.95652173913044
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 179.0
Weighted Recall: 0.6902173913043478
Weighted FalsePositiveRate: 0.4432599876532322
Kappa statistic: 0.2708565072302557
Training time: 35.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7739773876275393
Weighted AreaUnderROC: 0.7856237322515212
Root mean squared error: 0.43820837791259437
Relative absolute error: 48.421440946036306
Root relative squared error: 87.64167558251887
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5084881745736276
Weighted FMeasure: 0.771310263235729
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7968646887007473
Mean absolute error: 0.24210720473018152
Coverage of cases: 86.41304347826087
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 224.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.29492239174530377
Kappa statistic: 0.5021119324181627
Training time: 43.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7963065585851142
Weighted AreaUnderROC: 0.8274594320486817
Root mean squared error: 0.40097437678008974
Relative absolute error: 45.86459146858087
Root relative squared error: 80.19487535601795
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5595771183495103
Weighted FMeasure: 0.7955426508002573
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8293229702172079
Mean absolute error: 0.22932295734290434
Coverage of cases: 92.3913043478261
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 272.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.25783799276832176
Kappa statistic: 0.5563086548488009
Training time: 43.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 79.8913043478261
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8103969754253307
Weighted AreaUnderROC: 0.8327839756592291
Root mean squared error: 0.396059478268448
Relative absolute error: 51.07097746561925
Root relative squared error: 79.2118956536896
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5930633506894811
Weighted FMeasure: 0.8100651067963033
Iteration time: 84.0
Weighted AreaUnderPRC: 0.829834901034969
Mean absolute error: 0.25535488732809625
Coverage of cases: 94.56521739130434
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 356.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.21495502248875561
Kappa statistic: 0.5930232558139535
Training time: 82.0
		
Time end:Mon Nov 27 15.56.36 EET 2017