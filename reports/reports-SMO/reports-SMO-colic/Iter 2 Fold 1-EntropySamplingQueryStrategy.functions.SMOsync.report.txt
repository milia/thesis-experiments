Mon Nov 27 15.56.25 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.56.25 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 52.98913043478261
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7565023291925465
Weighted AreaUnderROC: 0.8206769776876268
Root mean squared error: 0.4714646790142149
Relative absolute error: 46.72407535576156
Root relative squared error: 94.29293580284298
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.4723789188184357
Weighted FMeasure: 0.7551789943209251
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8237417283761929
Mean absolute error: 0.23362037677880781
Coverage of cases: 79.8913043478261
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.3105653055825029
Kappa statistic: 0.46736842105263166
Training time: 58.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.27173913043478
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7917874396135266
Weighted AreaUnderROC: 0.8244430174618572
Root mean squared error: 0.4515314393750493
Relative absolute error: 41.00569008930499
Root relative squared error: 90.30628787500986
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5450620424827898
Weighted FMeasure: 0.7875052877399765
Iteration time: 56.0
Weighted AreaUnderPRC: 0.7994927880646984
Mean absolute error: 0.20502845044652496
Coverage of cases: 79.8913043478261
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 121.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.27927947790810476
Kappa statistic: 0.5370762711864406
Training time: 47.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 59.51086956521739
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8486995341614907
Weighted AreaUnderROC: 0.8718306288032455
Root mean squared error: 0.3727425950398016
Relative absolute error: 33.92349547401674
Root relative squared error: 74.54851900796032
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6681395518422918
Weighted FMeasure: 0.8442048145678617
Iteration time: 47.0
Weighted AreaUnderPRC: 0.8713843713205016
Mean absolute error: 0.1696174773700837
Coverage of cases: 90.21739130434783
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 168.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.21090925125672458
Kappa statistic: 0.6610526315789473
Training time: 40.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 61.95652173913044
Incorrectly Classified Instances: 15.217391304347826
Correctly Classified Instances: 84.78260869565217
Weighted Precision: 0.8475881107065514
Weighted AreaUnderROC: 0.8796906693711968
Root mean squared error: 0.36362419564826265
Relative absolute error: 34.63361946235159
Root relative squared error: 72.72483912965254
Weighted TruePositiveRate: 0.8478260869565217
Weighted MatthewsCorrelation: 0.6680308587538919
Weighted FMeasure: 0.8449315287259952
Iteration time: 49.0
Weighted AreaUnderPRC: 0.8750832292783264
Mean absolute error: 0.17316809731175795
Coverage of cases: 92.3913043478261
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 217.0
Weighted Recall: 0.8478260869565217
Weighted FalsePositiveRate: 0.20482405855895583
Kappa statistic: 0.6631799163179916
Training time: 42.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 64.67391304347827
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8619472315124489
Weighted AreaUnderROC: 0.902003042596349
Root mean squared error: 0.3457862173513978
Relative absolute error: 32.530391834280294
Root relative squared error: 69.15724347027957
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6934221250520698
Weighted FMeasure: 0.8546088810852472
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9033276944382582
Mean absolute error: 0.16265195917140146
Coverage of cases: 92.93478260869566
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 265.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.20453743716377104
Kappa statistic: 0.6832627118644069
Training time: 41.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 69.56521739130434
Incorrectly Classified Instances: 14.130434782608695
Correctly Classified Instances: 85.8695652173913
Weighted Precision: 0.8589633754551295
Weighted AreaUnderROC: 0.9032707910750508
Root mean squared error: 0.34271966948594573
Relative absolute error: 34.23688178791856
Root relative squared error: 68.54393389718915
Weighted TruePositiveRate: 0.8586956521739131
Weighted MatthewsCorrelation: 0.6922654167213044
Weighted FMeasure: 0.8560078481027097
Iteration time: 58.0
Weighted AreaUnderPRC: 0.903440206530694
Mean absolute error: 0.1711844089395928
Coverage of cases: 95.1086956521739
Instances selection time: 7.0
Test time: 12.0
Accumulative iteration time: 323.0
Weighted Recall: 0.8586956521739131
Weighted FalsePositiveRate: 0.19236705176823352
Kappa statistic: 0.6872384937238495
Training time: 51.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 76.6304347826087
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8189211495946942
Weighted AreaUnderROC: 0.8935091277890467
Root mean squared error: 0.3670411396053368
Relative absolute error: 42.91210214659898
Root relative squared error: 73.40822792106736
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6078255671669001
Weighted FMeasure: 0.8176461480110404
Iteration time: 116.0
Weighted AreaUnderPRC: 0.9007030897149337
Mean absolute error: 0.2145605107329949
Coverage of cases: 97.82608695652173
Instances selection time: 4.0
Test time: 12.0
Accumulative iteration time: 439.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.23292397918687718
Kappa statistic: 0.6042752867570386
Training time: 112.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 79.34782608695652
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8356474988312295
Weighted AreaUnderROC: 0.8928752535496958
Root mean squared error: 0.360377685116005
Relative absolute error: 43.48765035127485
Root relative squared error: 72.075537023201
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6443253706933759
Weighted FMeasure: 0.834578804347826
Iteration time: 96.0
Weighted AreaUnderPRC: 0.8986831112363969
Mean absolute error: 0.21743825175637427
Coverage of cases: 98.3695652173913
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 535.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.21119587265190937
Kappa statistic: 0.6413721413721415
Training time: 91.0
		
Time end:Mon Nov 27 15.56.27 EET 2017