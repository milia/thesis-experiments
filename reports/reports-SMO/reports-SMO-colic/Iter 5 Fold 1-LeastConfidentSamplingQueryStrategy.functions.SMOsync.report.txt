Mon Nov 27 15.56.48 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.56.48 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7130457774814338
Weighted AreaUnderROC: 0.7459997023547049
Root mean squared error: 0.512746613720854
Relative absolute error: 56.32797726380518
Root relative squared error: 102.5493227441708
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.38320617615051356
Weighted FMeasure: 0.7144318597004018
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7553455481312416
Mean absolute error: 0.2816398863190259
Coverage of cases: 76.6304347826087
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.3421377546520857
Kappa statistic: 0.3822314049586777
Training time: 27.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.54347826086956
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.774602508626142
Weighted AreaUnderROC: 0.7657159471734721
Root mean squared error: 0.47260487556769293
Relative absolute error: 45.18278265633467
Root relative squared error: 94.52097511353858
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5075379836736784
Weighted FMeasure: 0.7701283530282907
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7513562802676583
Mean absolute error: 0.22591391328167337
Coverage of cases: 77.71739130434783
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.3010075844430726
Kappa statistic: 0.4989373007438895
Training time: 10.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 59.78260869565217
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779336522215083
Weighted AreaUnderROC: 0.8130070993914807
Root mean squared error: 0.4356848361069188
Relative absolute error: 44.11854670290252
Root relative squared error: 87.13696722138377
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5226235109494171
Weighted FMeasure: 0.7784736124657072
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8110143675071833
Mean absolute error: 0.2205927335145126
Coverage of cases: 85.8695652173913
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 57.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2795660993032896
Kappa statistic: 0.5188284518828452
Training time: 13.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 55.70652173913044
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8026009316770187
Weighted AreaUnderROC: 0.820106490872211
Root mean squared error: 0.43177331838077476
Relative absolute error: 41.53026167836355
Root relative squared error: 86.35466367615496
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5702592353303636
Weighted FMeasure: 0.7996919044443933
Iteration time: 20.0
Weighted AreaUnderPRC: 0.820399597812213
Mean absolute error: 0.20765130839181775
Coverage of cases: 84.23913043478261
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 77.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.26073727841961375
Kappa statistic: 0.5642105263157895
Training time: 18.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 57.33695652173913
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8026009316770187
Weighted AreaUnderROC: 0.8102180527383368
Root mean squared error: 0.4294132348323029
Relative absolute error: 41.48898221232466
Root relative squared error: 85.88264696646058
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5702592353303636
Weighted FMeasure: 0.7996919044443933
Iteration time: 36.0
Weighted AreaUnderPRC: 0.8105872290704655
Mean absolute error: 0.2074449110616233
Coverage of cases: 84.78260869565217
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 113.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.26073727841961375
Kappa statistic: 0.5642105263157895
Training time: 32.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 61.95652173913044
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779336522215083
Weighted AreaUnderROC: 0.8032454361054767
Root mean squared error: 0.4432602502920098
Relative absolute error: 46.63107571558216
Root relative squared error: 88.65205005840197
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5226235109494171
Weighted FMeasure: 0.7784736124657072
Iteration time: 50.0
Weighted AreaUnderPRC: 0.8074359886616375
Mean absolute error: 0.23315537857791083
Coverage of cases: 87.5
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 163.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2795660993032896
Kappa statistic: 0.5188284518828452
Training time: 41.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7794296400187003
Weighted AreaUnderROC: 0.8024847870182555
Root mean squared error: 0.43143374335309054
Relative absolute error: 46.643977008330175
Root relative squared error: 86.28674867061811
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.524232311325891
Weighted FMeasure: 0.7794384057971016
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8075459444665727
Mean absolute error: 0.2332198850416509
Coverage of cases: 86.95652173913044
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 211.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.27348090660552077
Kappa statistic: 0.5218295218295219
Training time: 42.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 73.6413043478261
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779815142666636
Weighted AreaUnderROC: 0.8086967545638946
Root mean squared error: 0.4195356237462304
Relative absolute error: 49.31896846067711
Root relative squared error: 83.90712474924608
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5261317229309753
Weighted FMeasure: 0.7803321997695398
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8073402760464085
Mean absolute error: 0.24659484230338555
Coverage of cases: 91.84782608695652
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 266.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.267395713907752
Kappa statistic: 0.5247933884297522
Training time: 49.0
		
Time end:Mon Nov 27 15.56.48 EET 2017