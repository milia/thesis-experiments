Mon Nov 27 15.57.02 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.02 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 33.69565217391305
Correctly Classified Instances: 66.30434782608695
Weighted Precision: 0.6591711956521739
Weighted AreaUnderROC: 0.6702848024517154
Root mean squared error: 0.5493830013537457
Relative absolute error: 64.91977287311788
Root relative squared error: 109.87660027074915
Weighted TruePositiveRate: 0.6630434782608695
Weighted MatthewsCorrelation: 0.26826649392852975
Weighted FMeasure: 0.6607936401598892
Iteration time: 43.0
Weighted AreaUnderPRC: 0.6870085156647114
Mean absolute error: 0.3245988643655894
Coverage of cases: 75.0
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.6630434782608695
Weighted FalsePositiveRate: 0.3983375959079284
Kappa statistic: 0.26796714579055425
Training time: 36.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 52.44565217391305
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7349100175187131
Weighted AreaUnderROC: 0.7605540060851927
Root mean squared error: 0.5216323320702103
Relative absolute error: 56.1757870736905
Root relative squared error: 104.32646641404206
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.36525823417500747
Weighted FMeasure: 0.6792945439045184
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7514359032576826
Mean absolute error: 0.2808789353684525
Coverage of cases: 74.45652173913044
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 74.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.4455860305141547
Kappa statistic: 0.30946882217090066
Training time: 23.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.81521739130435
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7682028290411149
Weighted AreaUnderROC: 0.769907785078049
Root mean squared error: 0.4845300237946844
Relative absolute error: 47.655934717812954
Root relative squared error: 96.90600475893689
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.481193322802977
Weighted FMeasure: 0.7530603776716535
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7558119801118781
Mean absolute error: 0.23827967358906477
Coverage of cases: 76.6304347826087
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 105.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.33780536202486994
Kappa statistic: 0.46074154852780796
Training time: 23.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.81521739130435
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7682028290411149
Weighted AreaUnderROC: 0.7672496472352058
Root mean squared error: 0.4848526853157745
Relative absolute error: 47.712602675104165
Root relative squared error: 96.97053706315491
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.481193322802977
Weighted FMeasure: 0.7530603776716535
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7527520507225293
Mean absolute error: 0.23856301337552083
Coverage of cases: 76.6304347826087
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 130.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.33780536202486994
Kappa statistic: 0.46074154852780796
Training time: 24.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.81521739130435
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.7314322799016126
Weighted AreaUnderROC: 0.7620408545727136
Root mean squared error: 0.5152057128668965
Relative absolute error: 53.83198829318746
Root relative squared error: 103.0411425733793
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.40199213931687516
Weighted FMeasure: 0.7166589699156102
Iteration time: 32.0
Weighted AreaUnderPRC: 0.743711428542714
Mean absolute error: 0.2691599414659373
Coverage of cases: 73.91304347826087
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 162.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.38126157509480546
Kappa statistic: 0.38144895718990124
Training time: 27.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 54.619565217391305
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7684782608695653
Weighted AreaUnderROC: 0.789407685862951
Root mean squared error: 0.47898917958825343
Relative absolute error: 49.07390725630201
Root relative squared error: 95.79783591765069
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.46999201581410516
Weighted FMeasure: 0.7427474297039515
Iteration time: 32.0
Weighted AreaUnderPRC: 0.785303512889824
Mean absolute error: 0.24536953628151004
Coverage of cases: 79.8913043478261
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 194.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.35924684716465294
Kappa statistic: 0.4390243902439025
Training time: 32.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 69.29347826086956
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.8336713995943205
Root mean squared error: 0.3990227556800452
Relative absolute error: 45.07646641678852
Root relative squared error: 79.80455113600904
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 52.0
Weighted AreaUnderPRC: 0.8349778310373054
Mean absolute error: 0.2253823320839426
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 246.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 51.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 74.72826086956522
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.791508152173913
Weighted AreaUnderROC: 0.8474898580121704
Root mean squared error: 0.392035739479358
Relative absolute error: 45.469177012847375
Root relative squared error: 78.4071478958716
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5519506024506531
Weighted FMeasure: 0.7920993278399322
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8542387759993588
Mean absolute error: 0.2273458850642369
Coverage of cases: 94.02173913043478
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 312.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.24885351441926098
Kappa statistic: 0.5513347022587268
Training time: 66.0
		
Time end:Mon Nov 27 15.57.03 EET 2017