Mon Nov 27 15.57.05 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 15.57.05 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 29.891304347826086
Correctly Classified Instances: 70.1086956521739
Weighted Precision: 0.6914623523771583
Weighted AreaUnderROC: 0.7867109643707558
Root mean squared error: 0.5302655895399089
Relative absolute error: 59.378492228133254
Root relative squared error: 106.05311790798177
Weighted TruePositiveRate: 0.7010869565217391
Weighted MatthewsCorrelation: 0.3260944896897391
Weighted FMeasure: 0.6862003780718335
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7842804796042726
Mean absolute error: 0.29689246114066625
Coverage of cases: 75.54347826086956
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7010869565217391
Weighted FalsePositiveRate: 0.4064622100714349
Kappa statistic: 0.31473456121343446
Training time: 13.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 53.80434782608695
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8219976269175354
Weighted AreaUnderROC: 0.8350397411588323
Root mean squared error: 0.4294232592051905
Relative absolute error: 39.40710588935568
Root relative squared error: 85.88465184103809
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6154297852266725
Weighted FMeasure: 0.8170505521049
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8095703875992133
Mean absolute error: 0.19703552944677838
Coverage of cases: 82.06521739130434
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.18742834465120378
Kappa statistic: 0.6128712871287127
Training time: 13.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 71.19565217391305
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.8071760985267271
Weighted AreaUnderROC: 0.8511663286004056
Root mean squared error: 0.432015775331768
Relative absolute error: 51.738031254630776
Root relative squared error: 86.4031550663536
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.5608222314288417
Weighted FMeasure: 0.7702697955227084
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8481496375408859
Mean absolute error: 0.2586901562731539
Coverage of cases: 90.76086956521739
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.18567554458065083
Kappa statistic: 0.5365510777881911
Training time: 14.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 79.34782608695652
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7720791707706016
Weighted AreaUnderROC: 0.860420892494929
Root mean squared error: 0.47229707321911346
Relative absolute error: 62.07482377922693
Root relative squared error: 94.4594146438227
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.4748338874822132
Weighted FMeasure: 0.7158470368520433
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8623136103396838
Mean absolute error: 0.31037411889613464
Coverage of cases: 91.84782608695652
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.22361980774318724
Kappa statistic: 0.44133822181484883
Training time: 14.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 77.98913043478261
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.8181497955930375
Weighted AreaUnderROC: 0.8608012170385394
Root mean squared error: 0.42277034138679187
Relative absolute error: 54.13903841615478
Root relative squared error: 84.55406827735837
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5833530809010921
Weighted FMeasure: 0.78095492131235
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8593826086536894
Mean absolute error: 0.2706951920807739
Coverage of cases: 92.3913043478261
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.17321853778992855
Kappa statistic: 0.5581068416119963
Training time: 19.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 83.42391304347827
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8071235956242052
Weighted AreaUnderROC: 0.8646044624746451
Root mean squared error: 0.38466988176662686
Relative absolute error: 50.315944109631936
Root relative squared error: 76.93397635332538
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5830115458642623
Weighted FMeasure: 0.8010958646237419
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8690178352365132
Mean absolute error: 0.2515797205481597
Coverage of cases: 96.73913043478261
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 104.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.20307125848840288
Kappa statistic: 0.5799605133267521
Training time: 25.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 87.5
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8242974958281688
Weighted AreaUnderROC: 0.8637170385395537
Root mean squared error: 0.3763849840966008
Relative absolute error: 51.70639117021844
Root relative squared error: 75.27699681932016
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6219060039332348
Weighted FMeasure: 0.8218569035849493
Iteration time: 30.0
Weighted AreaUnderPRC: 0.8676647580050492
Mean absolute error: 0.2585319558510922
Coverage of cases: 97.82608695652173
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.1903276303024958
Kappa statistic: 0.6208791208791209
Training time: 30.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 83.42391304347827
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8320730938878387
Weighted AreaUnderROC: 0.8569979716024341
Root mean squared error: 0.3648109930581133
Relative absolute error: 46.78154001361051
Root relative squared error: 72.96219861162267
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6395781232925778
Weighted FMeasure: 0.8317719517338685
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8577856728583797
Mean absolute error: 0.23390770006805256
Coverage of cases: 96.19565217391305
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 169.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.19004100890731104
Kappa statistic: 0.6395348837209303
Training time: 34.0
		
Time end:Mon Nov 27 15.57.05 EET 2017