Mon Nov 27 15.56.42 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.56.42 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 33.69565217391305
Correctly Classified Instances: 66.30434782608695
Weighted Precision: 0.6591711956521739
Weighted AreaUnderROC: 0.6702848024517154
Root mean squared error: 0.5493830013537457
Relative absolute error: 64.91977287311788
Root relative squared error: 109.87660027074915
Weighted TruePositiveRate: 0.6630434782608695
Weighted MatthewsCorrelation: 0.26826649392852975
Weighted FMeasure: 0.6607936401598892
Iteration time: 44.0
Weighted AreaUnderPRC: 0.6870085156647114
Mean absolute error: 0.3245988643655894
Coverage of cases: 75.0
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6630434782608695
Weighted FalsePositiveRate: 0.3983375959079284
Kappa statistic: 0.26796714579055425
Training time: 37.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 51.358695652173914
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7389408917271747
Weighted AreaUnderROC: 0.7911081224093836
Root mean squared error: 0.5000053025608685
Relative absolute error: 51.03093372443366
Root relative squared error: 100.0010605121737
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.41522903743724127
Weighted FMeasure: 0.7214449857523669
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7843438958212458
Mean absolute error: 0.2551546686221683
Coverage of cases: 76.08695652173913
Instances selection time: 10.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.37807566804832876
Kappa statistic: 0.3920704845814977
Training time: 15.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.54347826086956
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7734719596723376
Weighted AreaUnderROC: 0.7908063431519534
Root mean squared error: 0.47267846279743186
Relative absolute error: 45.08344261734184
Root relative squared error: 94.53569255948638
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.49404841911772657
Weighted FMeasure: 0.759599272058956
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7815870872687954
Mean absolute error: 0.2254172130867092
Coverage of cases: 78.26086956521739
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 109.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.3285342622806244
Kappa statistic: 0.47500000000000003
Training time: 36.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 57.33695652173913
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8404023512719165
Weighted AreaUnderROC: 0.7942444219066936
Root mean squared error: 0.4006636949711579
Relative absolute error: 37.60114259964312
Root relative squared error: 80.13273899423157
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6447221513815609
Weighted FMeasure: 0.8313464235624125
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7993358723678897
Mean absolute error: 0.18800571299821559
Coverage of cases: 87.5
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 149.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.2355366434429844
Kappa statistic: 0.6321961620469084
Training time: 35.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 61.41304347826087
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8087967644084934
Weighted AreaUnderROC: 0.7989350912778904
Root mean squared error: 0.40565963229486024
Relative absolute error: 40.829992420177206
Root relative squared error: 81.13192645897205
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.58227510211428
Weighted FMeasure: 0.80477705398172
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7915373415600296
Mean absolute error: 0.204149962100886
Coverage of cases: 89.67391304347827
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 188.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.257551371373137
Kappa statistic: 0.5749736008447731
Training time: 31.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 60.869565217391305
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8080235679922161
Weighted AreaUnderROC: 0.8026115618661258
Root mean squared error: 0.39224477688604487
Relative absolute error: 39.0213001158315
Root relative squared error: 78.44895537720897
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5828067472404553
Weighted FMeasure: 0.8057183753802112
Iteration time: 51.0
Weighted AreaUnderPRC: 0.7889557404970041
Mean absolute error: 0.1951065005791575
Coverage of cases: 89.1304347826087
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 239.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2514661786753682
Kappa statistic: 0.5776495278069256
Training time: 50.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 67.1195652173913
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8189211495946942
Weighted AreaUnderROC: 0.8244168356997972
Root mean squared error: 0.3913250844182451
Relative absolute error: 41.279506586165695
Root relative squared error: 78.26501688364903
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6078255671669001
Weighted FMeasure: 0.8176461480110404
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8164027658305694
Mean absolute error: 0.20639753293082846
Coverage of cases: 91.30434782608695
Instances selection time: 10.0
Test time: 4.0
Accumulative iteration time: 287.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.23292397918687718
Kappa statistic: 0.6042752867570386
Training time: 38.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8019167835437121
Weighted AreaUnderROC: 0.8373478701825556
Root mean squared error: 0.397259602587191
Relative absolute error: 44.84839625459025
Root relative squared error: 79.4519205174382
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5722695350728849
Weighted FMeasure: 0.8014945652173914
Iteration time: 61.0
Weighted AreaUnderPRC: 0.846071096743538
Mean absolute error: 0.22424198127295125
Coverage of cases: 94.02173913043478
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 348.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24856689302407622
Kappa statistic: 0.5696465696465697
Training time: 57.0
		
Time end:Mon Nov 27 15.56.43 EET 2017