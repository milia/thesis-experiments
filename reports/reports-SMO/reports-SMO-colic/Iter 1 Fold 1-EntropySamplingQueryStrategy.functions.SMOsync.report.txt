Mon Nov 27 15.56.21 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.56.21 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 33.69565217391305
Correctly Classified Instances: 66.30434782608695
Weighted Precision: 0.6591711956521739
Weighted AreaUnderROC: 0.6702848024517154
Root mean squared error: 0.5493830013537457
Relative absolute error: 64.91977287311788
Root relative squared error: 109.87660027074915
Weighted TruePositiveRate: 0.6630434782608695
Weighted MatthewsCorrelation: 0.26826649392852975
Weighted FMeasure: 0.6607936401598892
Iteration time: 367.0
Weighted AreaUnderPRC: 0.6870085156647114
Mean absolute error: 0.3245988643655894
Coverage of cases: 75.0
Instances selection time: 66.0
Test time: 82.0
Accumulative iteration time: 367.0
Weighted Recall: 0.6630434782608695
Weighted FalsePositiveRate: 0.3983375959079284
Kappa statistic: 0.26796714579055425
Training time: 301.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 51.358695652173914
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7389408917271747
Weighted AreaUnderROC: 0.7911081224093836
Root mean squared error: 0.5000053025608685
Relative absolute error: 51.03093372443366
Root relative squared error: 100.0010605121737
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.41522903743724127
Weighted FMeasure: 0.7214449857523669
Iteration time: 119.0
Weighted AreaUnderPRC: 0.7843438958212458
Mean absolute error: 0.2551546686221683
Coverage of cases: 76.08695652173913
Instances selection time: 24.0
Test time: 46.0
Accumulative iteration time: 486.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.37807566804832876
Kappa statistic: 0.3920704845814977
Training time: 95.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.54347826086956
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7734719596723376
Weighted AreaUnderROC: 0.7908063431519534
Root mean squared error: 0.47267846279743186
Relative absolute error: 45.08344261734184
Root relative squared error: 94.53569255948638
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.49404841911772657
Weighted FMeasure: 0.759599272058956
Iteration time: 119.0
Weighted AreaUnderPRC: 0.7815870872687954
Mean absolute error: 0.2254172130867092
Coverage of cases: 78.26086956521739
Instances selection time: 16.0
Test time: 45.0
Accumulative iteration time: 605.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.3285342622806244
Kappa statistic: 0.47500000000000003
Training time: 103.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 58.96739130434783
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7995532309033453
Weighted AreaUnderROC: 0.7872718052738337
Root mean squared error: 0.42956508387818343
Relative absolute error: 41.79754014271532
Root relative squared error: 85.91301677563669
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5572368337002412
Weighted FMeasure: 0.7914141036194466
Iteration time: 145.0
Weighted AreaUnderPRC: 0.7909479764165163
Mean absolute error: 0.2089877007135766
Coverage of cases: 86.41304347826087
Instances selection time: 8.0
Test time: 25.0
Accumulative iteration time: 750.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.28217876355939675
Kappa statistic: 0.5449197860962566
Training time: 137.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 60.32608695652174
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8115397188623734
Weighted AreaUnderROC: 0.7989350912778904
Root mean squared error: 0.4048408703777777
Relative absolute error: 39.62332742089424
Root relative squared error: 80.96817407555554
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5823917839948547
Weighted FMeasure: 0.8026890169373145
Iteration time: 140.0
Weighted AreaUnderPRC: 0.7896736726450039
Mean absolute error: 0.19811663710447122
Coverage of cases: 89.1304347826087
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 890.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2697217567686745
Kappa statistic: 0.5695187165775402
Training time: 130.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 65.48913043478261
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7875667429443173
Weighted AreaUnderROC: 0.8003296146044626
Root mean squared error: 0.41344116051720814
Relative absolute error: 43.8481246244033
Root relative squared error: 82.68823210344163
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5320818834056279
Weighted FMeasure: 0.780139190301579
Iteration time: 137.0
Weighted AreaUnderPRC: 0.8037490484741427
Mean absolute error: 0.21924062312201648
Coverage of cases: 90.21739130434783
Instances selection time: 8.0
Test time: 23.0
Accumulative iteration time: 1027.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.29463577035011906
Kappa statistic: 0.5203208556149731
Training time: 129.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 67.66304347826087
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8020715977283702
Weighted AreaUnderROC: 0.8303752535496958
Root mean squared error: 0.40087842267283574
Relative absolute error: 43.0349989954172
Root relative squared error: 80.17568453456715
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.573773571857796
Weighted FMeasure: 0.802298979792586
Iteration time: 432.0
Weighted AreaUnderPRC: 0.8329640584069647
Mean absolute error: 0.21517499497708598
Coverage of cases: 92.93478260869566
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 1459.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24248170032630748
Kappa statistic: 0.5723140495867769
Training time: 424.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 75.27173913043478
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.8362068965517241
Root mean squared error: 0.40823431026537005
Relative absolute error: 47.4331739093535
Root relative squared error: 81.64686205307402
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 424.0
Weighted AreaUnderPRC: 0.8483882417024522
Mean absolute error: 0.2371658695467675
Coverage of cases: 92.93478260869566
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 1883.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 419.0
		
Time end:Mon Nov 27 15.56.24 EET 2017