Fri Dec 01 13.56.08 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.56.08 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 50.99999999999992
Incorrectly Classified Instances: 16.6
Correctly Classified Instances: 83.4
Weighted Precision: 0.8343222408466139
Weighted AreaUnderROC: 0.9575296553628255
Root mean squared error: 0.2821563392802999
Relative absolute error: 31.105926432428596
Root relative squared error: 59.85439825796154
Weighted TruePositiveRate: 0.834
Weighted MatthewsCorrelation: 0.7511837773943709
Weighted FMeasure: 0.8338403001539033
Iteration time: 210.0
Weighted AreaUnderPRC: 0.9164238424547663
Mean absolute error: 0.13824856192190552
Coverage of cases: 98.28
Instances selection time: 46.0
Test time: 65.0
Accumulative iteration time: 210.0
Weighted Recall: 0.834
Weighted FalsePositiveRate: 0.08291566822374855
Kappa statistic: 0.7510367071478979
Training time: 164.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 51.69333333333325
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8410273706870577
Weighted AreaUnderROC: 0.9588178295150738
Root mean squared error: 0.2776949489313943
Relative absolute error: 31.03485165266097
Root relative squared error: 58.90799444719214
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7613051906198484
Weighted FMeasure: 0.8407204179339135
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9188021435543969
Mean absolute error: 0.13793267401182718
Coverage of cases: 98.36
Instances selection time: 44.0
Test time: 66.0
Accumulative iteration time: 319.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.07955428149332415
Kappa statistic: 0.7612210507521657
Training time: 65.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 51.50666666666659
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8483644028714465
Weighted AreaUnderROC: 0.9611705040073837
Root mean squared error: 0.2736799006225289
Relative absolute error: 30.360989937069828
Root relative squared error: 58.056274081395046
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7722049753839602
Weighted FMeasure: 0.8478970092516749
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9234097199173339
Mean absolute error: 0.13493773305364432
Coverage of cases: 98.72
Instances selection time: 44.0
Test time: 66.0
Accumulative iteration time: 427.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07591963693948485
Kappa statistic: 0.7720263355177212
Training time: 64.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 51.13333333333338
Incorrectly Classified Instances: 15.88
Correctly Classified Instances: 84.12
Weighted Precision: 0.8416504121978647
Weighted AreaUnderROC: 0.9602368858555145
Root mean squared error: 0.27481953288991084
Relative absolute error: 30.103763897857405
Root relative squared error: 58.29802659269248
Weighted TruePositiveRate: 0.8412
Weighted MatthewsCorrelation: 0.7620375629081408
Weighted FMeasure: 0.8410692186545857
Iteration time: 112.0
Weighted AreaUnderPRC: 0.921848819863865
Mean absolute error: 0.1337945062127002
Coverage of cases: 98.4
Instances selection time: 46.0
Test time: 68.0
Accumulative iteration time: 539.0
Weighted Recall: 0.8412
Weighted FalsePositiveRate: 0.07934282028172028
Kappa statistic: 0.7618285996217573
Training time: 66.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 50.85333333333348
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8462686852292253
Weighted AreaUnderROC: 0.9600869873681942
Root mean squared error: 0.275389822507219
Relative absolute error: 29.957194370634927
Root relative squared error: 58.41900328938414
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7691200994884188
Weighted FMeasure: 0.8459089141909555
Iteration time: 111.0
Weighted AreaUnderPRC: 0.9217508880237004
Mean absolute error: 0.1331430860917114
Coverage of cases: 98.28
Instances selection time: 47.0
Test time: 71.0
Accumulative iteration time: 650.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07696321041710183
Kappa statistic: 0.7690214163342776
Training time: 64.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 50.45333333333337
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8462427745971843
Weighted AreaUnderROC: 0.9604814597877521
Root mean squared error: 0.27459822681610085
Relative absolute error: 29.685215887037103
Root relative squared error: 58.251080485039836
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.769127128408431
Weighted FMeasure: 0.8458806332929917
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9220693402179769
Mean absolute error: 0.13193429283127664
Coverage of cases: 98.08
Instances selection time: 47.0
Test time: 71.0
Accumulative iteration time: 769.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07693574525629693
Kappa statistic: 0.7690266820376911
Training time: 72.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 51.14666666666677
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8463411629933852
Weighted AreaUnderROC: 0.9602874870895673
Root mean squared error: 0.2733217103128854
Relative absolute error: 30.023484478050324
Root relative squared error: 57.98029044232378
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7691749763401674
Weighted FMeasure: 0.8459002255011968
Iteration time: 116.0
Weighted AreaUnderPRC: 0.9216719957841931
Mean absolute error: 0.1334377087913354
Coverage of cases: 98.28
Instances selection time: 44.0
Test time: 67.0
Accumulative iteration time: 885.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07693574525629693
Kappa statistic: 0.7690266820376911
Training time: 72.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 50.946666666666744
Incorrectly Classified Instances: 15.32
Correctly Classified Instances: 84.68
Weighted Precision: 0.8471426104363472
Weighted AreaUnderROC: 0.9608442527344098
Root mean squared error: 0.2736986362283159
Relative absolute error: 29.999541750613506
Root relative squared error: 58.06024850356554
Weighted TruePositiveRate: 0.8468
Weighted MatthewsCorrelation: 0.7704004158890151
Weighted FMeasure: 0.8466397622046006
Iteration time: 115.0
Weighted AreaUnderPRC: 0.9225777241967724
Mean absolute error: 0.133331296669394
Coverage of cases: 98.28
Instances selection time: 43.0
Test time: 67.0
Accumulative iteration time: 1000.0
Weighted Recall: 0.8468
Weighted FalsePositiveRate: 0.07650595654288793
Kappa statistic: 0.7702349242915077
Training time: 72.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 51.360000000000134
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.845147735677901
Weighted AreaUnderROC: 0.9611323880017723
Root mean squared error: 0.2727200762794546
Relative absolute error: 29.94558830905663
Root relative squared error: 57.85266459087432
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7674038731093822
Weighted FMeasure: 0.8446229666990732
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9235167820833534
Mean absolute error: 0.13309150359580788
Coverage of cases: 98.48
Instances selection time: 44.0
Test time: 68.0
Accumulative iteration time: 1118.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.07750833227929754
Kappa statistic: 0.7672364414627246
Training time: 74.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 50.65333333333344
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8508784431655014
Weighted AreaUnderROC: 0.961783702029025
Root mean squared error: 0.2707021045818289
Relative absolute error: 29.436946527521737
Root relative squared error: 57.42458814938423
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.77586356828091
Weighted FMeasure: 0.8502569675353452
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9246305195559206
Mean absolute error: 0.13083087345565278
Coverage of cases: 98.08
Instances selection time: 43.0
Test time: 68.0
Accumulative iteration time: 1242.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07471266628167257
Kappa statistic: 0.7756341036162504
Training time: 81.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 50.786666666666704
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8539853701026574
Weighted AreaUnderROC: 0.9626249156711149
Root mean squared error: 0.26958592684415084
Relative absolute error: 29.47049679861512
Root relative squared error: 57.18781109518774
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7806068262818743
Weighted FMeasure: 0.853457023633471
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9257807116384785
Mean absolute error: 0.13097998577162337
Coverage of cases: 98.32
Instances selection time: 41.0
Test time: 67.0
Accumulative iteration time: 1366.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.0731308372320611
Kappa statistic: 0.7804293697674999
Training time: 83.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 50.82666666666673
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.853005517273716
Weighted AreaUnderROC: 0.9612256417632343
Root mean squared error: 0.27133394247941384
Relative absolute error: 29.62061335922062
Root relative squared error: 57.55862120798211
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7789504651542879
Weighted FMeasure: 0.8522064836445458
Iteration time: 123.0
Weighted AreaUnderPRC: 0.9229132235295574
Mean absolute error: 0.1316471704854256
Coverage of cases: 98.08
Instances selection time: 42.0
Test time: 68.0
Accumulative iteration time: 1489.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07368282538445804
Kappa statistic: 0.7786427308072451
Training time: 81.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 50.45333333333338
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.8511250857703108
Weighted AreaUnderROC: 0.9608428378449678
Root mean squared error: 0.2728183904261482
Relative absolute error: 29.51445436832792
Root relative squared error: 57.8735201708184
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.7763822223188127
Weighted FMeasure: 0.8506655092310735
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9225974881951282
Mean absolute error: 0.13117535274812472
Coverage of cases: 98.0
Instances selection time: 43.0
Test time: 70.0
Accumulative iteration time: 1615.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.07451493765047114
Kappa statistic: 0.776230951734756
Training time: 83.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 50.053333333333384
Incorrectly Classified Instances: 14.88
Correctly Classified Instances: 85.12
Weighted Precision: 0.851307333831412
Weighted AreaUnderROC: 0.9617121534059329
Root mean squared error: 0.2706094548637235
Relative absolute error: 29.065424925707294
Root relative squared error: 57.40493417620002
Weighted TruePositiveRate: 0.8512
Weighted MatthewsCorrelation: 0.776830295315438
Weighted FMeasure: 0.851151998732989
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9242732318529783
Mean absolute error: 0.12917966633647748
Coverage of cases: 98.12
Instances selection time: 43.0
Test time: 71.0
Accumulative iteration time: 1741.0
Weighted Recall: 0.8512
Weighted FalsePositiveRate: 0.07437213934087951
Kappa statistic: 0.776812552062072
Training time: 83.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 49.880000000000045
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8549950714999941
Weighted AreaUnderROC: 0.9618394372243998
Root mean squared error: 0.27053541821102195
Relative absolute error: 28.76944687767434
Root relative squared error: 57.38922863044553
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7819267984697422
Weighted FMeasure: 0.8543119384028791
Iteration time: 135.0
Weighted AreaUnderPRC: 0.9249455347326669
Mean absolute error: 0.1278642083452199
Coverage of cases: 98.04
Instances selection time: 44.0
Test time: 72.0
Accumulative iteration time: 1876.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.0727010485186521
Kappa statistic: 0.7816312005340678
Training time: 91.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 50.08000000000007
Incorrectly Classified Instances: 14.52
Correctly Classified Instances: 85.48
Weighted Precision: 0.855481708907201
Weighted AreaUnderROC: 0.9630670561901051
Root mean squared error: 0.2680144850918056
Relative absolute error: 28.62067641295076
Root relative squared error: 56.85445795939084
Weighted TruePositiveRate: 0.8548
Weighted MatthewsCorrelation: 0.7825826060493469
Weighted FMeasure: 0.8546671565584906
Iteration time: 140.0
Weighted AreaUnderPRC: 0.9269331084431822
Mean absolute error: 0.12720300627978176
Coverage of cases: 98.12
Instances selection time: 44.0
Test time: 70.0
Accumulative iteration time: 2016.0
Weighted Recall: 0.8548
Weighted FalsePositiveRate: 0.07248958730704821
Kappa statistic: 0.7822370719608694
Training time: 96.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 50.52000000000002
Incorrectly Classified Instances: 15.16
Correctly Classified Instances: 84.84
Weighted Precision: 0.8490512111511973
Weighted AreaUnderROC: 0.9627221432076851
Root mean squared error: 0.26857905902697937
Relative absolute error: 28.99090572014239
Root relative squared error: 56.97422217680361
Weighted TruePositiveRate: 0.8484
Weighted MatthewsCorrelation: 0.772961297837563
Weighted FMeasure: 0.8482410635386121
Iteration time: 142.0
Weighted AreaUnderPRC: 0.9262602930827992
Mean absolute error: 0.1288484698673001
Coverage of cases: 97.88
Instances selection time: 43.0
Test time: 69.0
Accumulative iteration time: 2158.0
Weighted Recall: 0.8484
Weighted FalsePositiveRate: 0.0757150420180822
Kappa statistic: 0.7726355961710635
Training time: 99.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 49.77333333333334
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8504885426687259
Weighted AreaUnderROC: 0.9622315521212862
Root mean squared error: 0.2691946248101786
Relative absolute error: 28.691183393870478
Root relative squared error: 57.10480339867358
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7752560409782981
Weighted FMeasure: 0.8498865487945062
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9250784052537673
Mean absolute error: 0.12751637063942495
Coverage of cases: 97.68
Instances selection time: 45.0
Test time: 72.0
Accumulative iteration time: 2297.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07495159265408136
Kappa statistic: 0.7750229116666758
Training time: 94.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 50.693333333333364
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8491018078144851
Weighted AreaUnderROC: 0.9620402263418356
Root mean squared error: 0.2707047549818193
Relative absolute error: 29.540405896926444
Root relative squared error: 57.42515038412604
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7733612564910235
Weighted FMeasure: 0.8486172583504004
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9251609891247039
Mean absolute error: 0.13129069287522926
Coverage of cases: 98.12
Instances selection time: 42.0
Test time: 69.0
Accumulative iteration time: 2446.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07554477854768565
Kappa statistic: 0.7732303327106966
Training time: 107.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 50.05333333333325
Incorrectly Classified Instances: 15.28
Correctly Classified Instances: 84.72
Weighted Precision: 0.8475946584824149
Weighted AreaUnderROC: 0.9626965505926331
Root mean squared error: 0.27002927286771544
Relative absolute error: 28.936213664196284
Root relative squared error: 57.281858989090125
Weighted TruePositiveRate: 0.8472
Weighted MatthewsCorrelation: 0.7709857219229652
Weighted FMeasure: 0.8470572683305342
Iteration time: 154.0
Weighted AreaUnderPRC: 0.9263586685179416
Mean absolute error: 0.1286053940630952
Coverage of cases: 98.04
Instances selection time: 43.0
Test time: 71.0
Accumulative iteration time: 2600.0
Weighted Recall: 0.8472
Weighted FalsePositiveRate: 0.07638375710389998
Kappa statistic: 0.7708222944072
Training time: 111.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 50.026666666666614
Incorrectly Classified Instances: 14.52
Correctly Classified Instances: 85.48
Weighted Precision: 0.8554107977492218
Weighted AreaUnderROC: 0.9635359289837674
Root mean squared error: 0.2675574364240598
Relative absolute error: 28.525056522823952
Root relative squared error: 56.75750329570224
Weighted TruePositiveRate: 0.8548
Weighted MatthewsCorrelation: 0.7825287140021971
Weighted FMeasure: 0.8546340202825804
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9280823325374313
Mean absolute error: 0.12677802899032928
Coverage of cases: 97.96
Instances selection time: 42.0
Test time: 71.0
Accumulative iteration time: 2751.0
Weighted Recall: 0.8548
Weighted FalsePositiveRate: 0.07254451762865802
Kappa statistic: 0.7822291290317007
Training time: 109.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 49.57333333333334
Incorrectly Classified Instances: 15.6
Correctly Classified Instances: 84.4
Weighted Precision: 0.8443693856510043
Weighted AreaUnderROC: 0.963441504339134
Root mean squared error: 0.2701513376019203
Relative absolute error: 28.662728620284163
Root relative squared error: 57.30775282948012
Weighted TruePositiveRate: 0.844
Weighted MatthewsCorrelation: 0.7662004326541201
Weighted FMeasure: 0.8437870718104994
Iteration time: 149.0
Weighted AreaUnderPRC: 0.928207435965494
Mean absolute error: 0.12738990497904132
Coverage of cases: 97.96
Instances selection time: 43.0
Test time: 72.0
Accumulative iteration time: 2900.0
Weighted Recall: 0.844
Weighted FalsePositiveRate: 0.07794498728290777
Kappa statistic: 0.7660344958739281
Training time: 106.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 49.86666666666667
Incorrectly Classified Instances: 15.48
Correctly Classified Instances: 84.52
Weighted Precision: 0.8457074135020229
Weighted AreaUnderROC: 0.9638610846859323
Root mean squared error: 0.26873919558515746
Relative absolute error: 28.712426898404484
Root relative squared error: 57.008192270664686
Weighted TruePositiveRate: 0.8452
Weighted MatthewsCorrelation: 0.7680869985614994
Weighted FMeasure: 0.8450078631910608
Iteration time: 159.0
Weighted AreaUnderPRC: 0.9291181123893364
Mean absolute error: 0.12761078621513164
Coverage of cases: 98.36
Instances selection time: 41.0
Test time: 71.0
Accumulative iteration time: 3059.0
Weighted Recall: 0.8452
Weighted FalsePositiveRate: 0.07731746993829734
Kappa statistic: 0.7678374060371392
Training time: 118.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 49.78666666666663
Incorrectly Classified Instances: 14.88
Correctly Classified Instances: 85.12
Weighted Precision: 0.8513695226452299
Weighted AreaUnderROC: 0.9644239236005958
Root mean squared error: 0.26669196589710076
Relative absolute error: 28.505445714317975
Root relative squared error: 56.5739092721433
Weighted TruePositiveRate: 0.8512
Weighted MatthewsCorrelation: 0.7768545296255875
Weighted FMeasure: 0.8510646047149477
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9297745748969215
Mean absolute error: 0.1266908698414138
Coverage of cases: 98.16
Instances selection time: 41.0
Test time: 71.0
Accumulative iteration time: 3216.0
Weighted Recall: 0.8512
Weighted FalsePositiveRate: 0.07439273821148318
Kappa statistic: 0.7768196755774011
Training time: 116.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 49.97333333333332
Incorrectly Classified Instances: 14.28
Correctly Classified Instances: 85.72
Weighted Precision: 0.8575317699206267
Weighted AreaUnderROC: 0.9656691444096472
Root mean squared error: 0.263494414469759
Relative absolute error: 28.154797391874595
Root relative squared error: 55.89560618290348
Weighted TruePositiveRate: 0.8572
Weighted MatthewsCorrelation: 0.7859779717019681
Weighted FMeasure: 0.8569537156218541
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9316644676244583
Mean absolute error: 0.12513243285277656
Coverage of cases: 98.2
Instances selection time: 41.0
Test time: 70.0
Accumulative iteration time: 3376.0
Weighted Recall: 0.8572
Weighted FalsePositiveRate: 0.07135814584144941
Kappa statistic: 0.7858315769922882
Training time: 119.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 49.746666666666634
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8556387031972176
Weighted AreaUnderROC: 0.9651621833646412
Root mean squared error: 0.26485799462783954
Relative absolute error: 28.35772549880331
Root relative squared error: 56.184865215844525
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7833962283341974
Weighted FMeasure: 0.8554313185129556
Iteration time: 163.0
Weighted AreaUnderPRC: 0.9311406218921345
Mean absolute error: 0.12603433555023752
Coverage of cases: 98.24
Instances selection time: 40.0
Test time: 70.0
Accumulative iteration time: 3539.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07218339181726129
Kappa statistic: 0.7834210687984273
Training time: 123.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 49.48000000000008
Incorrectly Classified Instances: 14.84
Correctly Classified Instances: 85.16
Weighted Precision: 0.8516837214388495
Weighted AreaUnderROC: 0.9644389880967439
Root mean squared error: 0.2666404142714739
Relative absolute error: 28.266098492933263
Root relative squared error: 56.56297352092471
Weighted TruePositiveRate: 0.8516
Weighted MatthewsCorrelation: 0.7774334599923057
Weighted FMeasure: 0.8513885050603585
Iteration time: 158.0
Weighted AreaUnderPRC: 0.9293224463761911
Mean absolute error: 0.12562710441303732
Coverage of cases: 98.04
Instances selection time: 42.0
Test time: 71.0
Accumulative iteration time: 3697.0
Weighted Recall: 0.8516
Weighted FalsePositiveRate: 0.07416754441947684
Kappa statistic: 0.777427741406311
Training time: 116.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.6533333333333
Incorrectly Classified Instances: 14.48
Correctly Classified Instances: 85.52
Weighted Precision: 0.855405558338547
Weighted AreaUnderROC: 0.9639202830935312
Root mean squared error: 0.2674860812234241
Relative absolute error: 28.490001283536294
Root relative squared error: 56.742366571829514
Weighted TruePositiveRate: 0.8552
Weighted MatthewsCorrelation: 0.7829077154663744
Weighted FMeasure: 0.8549903974318411
Iteration time: 163.0
Weighted AreaUnderPRC: 0.9280532576939576
Mean absolute error: 0.12662222792682856
Coverage of cases: 98.0
Instances selection time: 41.0
Test time: 71.0
Accumulative iteration time: 3860.0
Weighted Recall: 0.8552
Weighted FalsePositiveRate: 0.07234678899745658
Kappa statistic: 0.7828310291025617
Training time: 122.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 49.53333333333334
Incorrectly Classified Instances: 14.24
Correctly Classified Instances: 85.76
Weighted Precision: 0.8579025922499557
Weighted AreaUnderROC: 0.9646355435319185
Root mean squared error: 0.26556017510525615
Relative absolute error: 28.173878000597927
Root relative squared error: 56.333820189003944
Weighted TruePositiveRate: 0.8576
Weighted MatthewsCorrelation: 0.7865538266280864
Weighted FMeasure: 0.8574130171431233
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9291377725508971
Mean absolute error: 0.1252172355582136
Coverage of cases: 97.88
Instances selection time: 42.0
Test time: 73.0
Accumulative iteration time: 4031.0
Weighted Recall: 0.8576
Weighted FalsePositiveRate: 0.07114668462984552
Kappa statistic: 0.7864305148080443
Training time: 129.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.29333333333336
Incorrectly Classified Instances: 14.4
Correctly Classified Instances: 85.6
Weighted Precision: 0.856136385077211
Weighted AreaUnderROC: 0.9645041921231364
Root mean squared error: 0.26684683874229687
Relative absolute error: 28.20211688014247
Root relative squared error: 56.60676276386124
Weighted TruePositiveRate: 0.856
Weighted MatthewsCorrelation: 0.7840653766549253
Weighted FMeasure: 0.8558425084811171
Iteration time: 178.0
Weighted AreaUnderPRC: 0.9291783665300947
Mean absolute error: 0.12534274168952267
Coverage of cases: 98.0
Instances selection time: 42.0
Test time: 74.0
Accumulative iteration time: 4209.0
Weighted Recall: 0.856
Weighted FalsePositiveRate: 0.07195133173505372
Kappa statistic: 0.7840249494378411
Training time: 136.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 49.573333333333316
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.853627271269976
Weighted AreaUnderROC: 0.9646626473090286
Root mean squared error: 0.266489855570792
Relative absolute error: 28.37628053876882
Root relative squared error: 56.531035197459076
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7803918638049488
Weighted FMeasure: 0.853455848784359
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9295484348460672
Mean absolute error: 0.1261168023945287
Coverage of cases: 98.2
Instances selection time: 40.0
Test time: 71.0
Accumulative iteration time: 4380.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07317890126346968
Kappa statistic: 0.7804193582293786
Training time: 131.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 49.826666666666696
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.8540292564960124
Weighted AreaUnderROC: 0.9650996754051654
Root mean squared error: 0.26519279309347105
Relative absolute error: 28.408167759145865
Root relative squared error: 56.25588669545819
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7809522021353649
Weighted FMeasure: 0.8539490147656532
Iteration time: 177.0
Weighted AreaUnderPRC: 0.9303038529727533
Mean absolute error: 0.12625852337398222
Coverage of cases: 98.24
Instances selection time: 39.0
Test time: 70.0
Accumulative iteration time: 4557.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07302923666367682
Kappa statistic: 0.7810053259504729
Training time: 138.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 49.640000000000015
Incorrectly Classified Instances: 14.16
Correctly Classified Instances: 85.84
Weighted Precision: 0.8584693268351778
Weighted AreaUnderROC: 0.9656575120467255
Root mean squared error: 0.2635628222552592
Relative absolute error: 28.14081160661477
Root relative squared error: 55.91011766560741
Weighted TruePositiveRate: 0.8584
Weighted MatthewsCorrelation: 0.7875883117220912
Weighted FMeasure: 0.8583402483589434
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9314148902334717
Mean absolute error: 0.12507027380717733
Coverage of cases: 98.16
Instances selection time: 39.0
Test time: 72.0
Accumulative iteration time: 4732.0
Weighted Recall: 0.8584
Weighted FalsePositiveRate: 0.07080615768905246
Kappa statistic: 0.787609039359285
Training time: 136.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 49.573333333333395
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8555940262142405
Weighted AreaUnderROC: 0.9652237760114949
Root mean squared error: 0.26481413246341773
Relative absolute error: 28.201482725958453
Root relative squared error: 56.175560645674466
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7833614762341231
Weighted FMeasure: 0.8555155352835784
Iteration time: 176.0
Weighted AreaUnderPRC: 0.9306175380456269
Mean absolute error: 0.1253399232264826
Coverage of cases: 98.08
Instances selection time: 39.0
Test time: 72.0
Accumulative iteration time: 4908.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07219712439766372
Kappa statistic: 0.7834102057111071
Training time: 137.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 49.5600000000001
Incorrectly Classified Instances: 13.76
Correctly Classified Instances: 86.24
Weighted Precision: 0.8623925548774402
Weighted AreaUnderROC: 0.9659619822907535
Root mean squared error: 0.2627325472354555
Relative absolute error: 27.98111021449766
Root relative squared error: 55.73398973658152
Weighted TruePositiveRate: 0.8624
Weighted MatthewsCorrelation: 0.7935459809620912
Weighted FMeasure: 0.8623392784364055
Iteration time: 186.0
Weighted AreaUnderPRC: 0.9320563397007868
Mean absolute error: 0.1243604898422124
Coverage of cases: 98.24
Instances selection time: 38.0
Test time: 71.0
Accumulative iteration time: 5094.0
Weighted Recall: 0.8624
Weighted FalsePositiveRate: 0.06881513879663567
Kappa statistic: 0.7936059606598562
Training time: 148.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 49.8133333333334
Incorrectly Classified Instances: 14.04
Correctly Classified Instances: 85.96
Weighted Precision: 0.8596194776289234
Weighted AreaUnderROC: 0.9658456294617336
Root mean squared error: 0.26310072800706347
Relative absolute error: 28.17700316217
Root relative squared error: 55.812092672673465
Weighted TruePositiveRate: 0.8596
Weighted MatthewsCorrelation: 0.7893600386838905
Weighted FMeasure: 0.8595481314512122
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9317263550171784
Mean absolute error: 0.12523112516520057
Coverage of cases: 98.32
Instances selection time: 38.0
Test time: 72.0
Accumulative iteration time: 5283.0
Weighted Recall: 0.8596
Weighted FalsePositiveRate: 0.07021297179544816
Kappa statistic: 0.7894060819523534
Training time: 151.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.46666666666672
Incorrectly Classified Instances: 14.04
Correctly Classified Instances: 85.96
Weighted Precision: 0.8596015784672174
Weighted AreaUnderROC: 0.9663399537862531
Root mean squared error: 0.262285929869654
Relative absolute error: 27.91217617768175
Root relative squared error: 55.63924788619534
Weighted TruePositiveRate: 0.8596
Weighted MatthewsCorrelation: 0.7893574371091261
Weighted FMeasure: 0.8595383234668127
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9326689184793828
Mean absolute error: 0.1240541163452528
Coverage of cases: 98.28
Instances selection time: 39.0
Test time: 71.0
Accumulative iteration time: 5472.0
Weighted Recall: 0.8596
Weighted FalsePositiveRate: 0.07020610550524693
Kappa statistic: 0.7894070422285079
Training time: 150.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 49.653333333333386
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8587828748179867
Weighted AreaUnderROC: 0.9661300404532908
Root mean squared error: 0.2629390732909669
Relative absolute error: 28.04070345807674
Root relative squared error: 55.77780052888466
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.788126655948554
Weighted FMeasure: 0.8587545079318255
Iteration time: 199.0
Weighted AreaUnderPRC: 0.9323561205901535
Mean absolute error: 0.12462534870256388
Coverage of cases: 98.36
Instances selection time: 38.0
Test time: 71.0
Accumulative iteration time: 5671.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07063589421865593
Kappa statistic: 0.7882022535280224
Training time: 161.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 49.506666666666725
Incorrectly Classified Instances: 14.0
Correctly Classified Instances: 86.0
Weighted Precision: 0.8599809742921206
Weighted AreaUnderROC: 0.9663038265473908
Root mean squared error: 0.2625807899508842
Relative absolute error: 27.917575623420696
Root relative squared error: 55.70179715507707
Weighted TruePositiveRate: 0.86
Weighted MatthewsCorrelation: 0.789932377022905
Weighted FMeasure: 0.859954659917283
Iteration time: 197.0
Weighted AreaUnderPRC: 0.9325915990333249
Mean absolute error: 0.12407811388187033
Coverage of cases: 98.28
Instances selection time: 38.0
Test time: 71.0
Accumulative iteration time: 5868.0
Weighted Recall: 0.86
Weighted FalsePositiveRate: 0.07002897574464918
Kappa statistic: 0.7900022343762262
Training time: 159.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 50.080000000000034
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8591635450514638
Weighted AreaUnderROC: 0.966348972622294
Root mean squared error: 0.26238105722194277
Relative absolute error: 28.21319588285679
Root relative squared error: 55.659427444959256
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7887275161097539
Weighted FMeasure: 0.8591372581481511
Iteration time: 200.0
Weighted AreaUnderPRC: 0.9326042236775367
Mean absolute error: 0.12539198170158633
Coverage of cases: 98.32
Instances selection time: 37.0
Test time: 70.0
Accumulative iteration time: 6068.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07042443300705203
Kappa statistic: 0.7888041732295369
Training time: 163.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 50.16000000000001
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8592421210259179
Weighted AreaUnderROC: 0.9662684787408814
Root mean squared error: 0.26226257904023625
Relative absolute error: 28.238639514465014
Root relative squared error: 55.634294427247056
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7887865302668999
Weighted FMeasure: 0.8591134736944055
Iteration time: 200.0
Weighted AreaUnderPRC: 0.9324616799279002
Mean absolute error: 0.12550506450873397
Coverage of cases: 98.4
Instances selection time: 36.0
Test time: 69.0
Accumulative iteration time: 6268.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07039010155604591
Kappa statistic: 0.7888128401793171
Training time: 164.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 50.13333333333335
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8588071399847006
Weighted AreaUnderROC: 0.9663309168810275
Root mean squared error: 0.26214759318453124
Relative absolute error: 28.231390368247613
Root relative squared error: 55.60990224375419
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.7881622959705141
Weighted FMeasure: 0.8587207670364116
Iteration time: 207.0
Weighted AreaUnderPRC: 0.9324270382176801
Mean absolute error: 0.1254728460811011
Coverage of cases: 98.4
Instances selection time: 36.0
Test time: 69.0
Accumulative iteration time: 6475.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.0706015627676498
Kappa statistic: 0.7882099795457638
Training time: 171.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 50.28000000000004
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8592180634884662
Weighted AreaUnderROC: 0.9663820437115263
Root mean squared error: 0.2619204629288006
Relative absolute error: 28.235106853671155
Root relative squared error: 55.56172064054226
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.788765353879238
Weighted FMeasure: 0.8591312664844465
Iteration time: 200.0
Weighted AreaUnderPRC: 0.9324161544091149
Mean absolute error: 0.12548936379409462
Coverage of cases: 98.4
Instances selection time: 35.0
Test time: 70.0
Accumulative iteration time: 6675.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07040383413644836
Kappa statistic: 0.7888089882894583
Training time: 165.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 50.13333333333334
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8580101492251442
Weighted AreaUnderROC: 0.9664071866207083
Root mean squared error: 0.2618897465739018
Relative absolute error: 28.17775246611949
Root relative squared error: 55.55520471768958
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7869519453663825
Weighted FMeasure: 0.8579436059611583
Iteration time: 213.0
Weighted AreaUnderPRC: 0.9324820527539396
Mean absolute error: 0.1252344554049761
Coverage of cases: 98.4
Instances selection time: 36.0
Test time: 69.0
Accumulative iteration time: 6888.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.07101761890065635
Kappa statistic: 0.7870061512623516
Training time: 177.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 50.09333333333335
Incorrectly Classified Instances: 14.04
Correctly Classified Instances: 85.96
Weighted Precision: 0.8596219987095681
Weighted AreaUnderROC: 0.9664271795231325
Root mean squared error: 0.2617437948487694
Relative absolute error: 28.159838566712807
Root relative squared error: 55.52424368131948
Weighted TruePositiveRate: 0.8596
Weighted MatthewsCorrelation: 0.7893655045559175
Weighted FMeasure: 0.8595390264350854
Iteration time: 215.0
Weighted AreaUnderPRC: 0.9325952980017035
Mean absolute error: 0.12515483807427974
Coverage of cases: 98.4
Instances selection time: 35.0
Test time: 70.0
Accumulative iteration time: 7103.0
Weighted Recall: 0.8596
Weighted FalsePositiveRate: 0.07020610550524693
Kappa statistic: 0.789408002495905
Training time: 180.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 50.013333333333364
Incorrectly Classified Instances: 14.0
Correctly Classified Instances: 86.0
Weighted Precision: 0.8600487392603191
Weighted AreaUnderROC: 0.9663735984636856
Root mean squared error: 0.2619053240979787
Relative absolute error: 28.144723113797664
Root relative squared error: 55.55850920956224
Weighted TruePositiveRate: 0.86
Weighted MatthewsCorrelation: 0.7899804941282014
Weighted FMeasure: 0.8599343980940852
Iteration time: 226.0
Weighted AreaUnderPRC: 0.9325136908286822
Mean absolute error: 0.12508765828354576
Coverage of cases: 98.4
Instances selection time: 35.0
Test time: 70.0
Accumulative iteration time: 7329.0
Weighted Recall: 0.86
Weighted FalsePositiveRate: 0.07000151058384427
Kappa statistic: 0.7900098947337603
Training time: 191.0
		
Time end:Fri Dec 01 13.56.22 EET 2017