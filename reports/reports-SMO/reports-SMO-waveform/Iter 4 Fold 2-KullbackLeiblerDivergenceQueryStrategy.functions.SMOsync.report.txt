Fri Dec 01 13.51.37 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Fri Dec 01 13.51.37 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 46.7333333333333
Incorrectly Classified Instances: 16.52
Correctly Classified Instances: 83.48
Weighted Precision: 0.8347693480019323
Weighted AreaUnderROC: 0.9573371474250327
Root mean squared error: 0.28725424634540353
Relative absolute error: 28.605120621616777
Root relative squared error: 60.93582765463962
Weighted TruePositiveRate: 0.8348
Weighted MatthewsCorrelation: 0.7521075316809466
Weighted FMeasure: 0.8347749321223317
Iteration time: 264.0
Weighted AreaUnderPRC: 0.9175101997107322
Mean absolute error: 0.1271338694294085
Coverage of cases: 96.28
Instances selection time: 120.0
Test time: 87.0
Accumulative iteration time: 264.0
Weighted Recall: 0.8348
Weighted FalsePositiveRate: 0.08266440305557142
Kappa statistic: 0.7521935966825383
Training time: 144.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 46.42666666666662
Incorrectly Classified Instances: 16.68
Correctly Classified Instances: 83.32
Weighted Precision: 0.8332965997498096
Weighted AreaUnderROC: 0.9573192451995597
Root mean squared error: 0.28874262713422066
Relative absolute error: 28.699726189674568
Root relative squared error: 61.25156089926773
Weighted TruePositiveRate: 0.8332
Weighted MatthewsCorrelation: 0.7497489057084027
Weighted FMeasure: 0.8332235209578057
Iteration time: 184.0
Weighted AreaUnderPRC: 0.9175008443910603
Mean absolute error: 0.12755433862077645
Coverage of cases: 96.12
Instances selection time: 121.0
Test time: 85.0
Accumulative iteration time: 448.0
Weighted Recall: 0.8332
Weighted FalsePositiveRate: 0.08352398048238942
Kappa statistic: 0.7497741366547648
Training time: 63.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 47.14666666666659
Incorrectly Classified Instances: 16.24
Correctly Classified Instances: 83.76
Weighted Precision: 0.8377520968462432
Weighted AreaUnderROC: 0.958805040868882
Root mean squared error: 0.28388433803989344
Relative absolute error: 28.520890397436858
Root relative squared error: 60.2209621501987
Weighted TruePositiveRate: 0.8376
Weighted MatthewsCorrelation: 0.7563762278764256
Weighted FMeasure: 0.8376427788640595
Iteration time: 178.0
Weighted AreaUnderPRC: 0.9194637422605696
Mean absolute error: 0.12675951287749773
Coverage of cases: 96.56
Instances selection time: 116.0
Test time: 83.0
Accumulative iteration time: 626.0
Weighted Recall: 0.8376
Weighted FalsePositiveRate: 0.08133523295877118
Kappa statistic: 0.7563692635462892
Training time: 62.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 47.039999999999935
Incorrectly Classified Instances: 16.08
Correctly Classified Instances: 83.92
Weighted Precision: 0.8395243344259877
Weighted AreaUnderROC: 0.9598433316396484
Root mean squared error: 0.28207180633992
Relative absolute error: 28.115785270884707
Root relative squared error: 59.83646611334867
Weighted TruePositiveRate: 0.8392
Weighted MatthewsCorrelation: 0.7588598374864842
Weighted FMeasure: 0.839260825901814
Iteration time: 178.0
Weighted AreaUnderPRC: 0.9217415721509511
Mean absolute error: 0.12495904564837707
Coverage of cases: 96.56
Instances selection time: 115.0
Test time: 84.0
Accumulative iteration time: 804.0
Weighted Recall: 0.8392
Weighted FalsePositiveRate: 0.08057178359477035
Kappa statistic: 0.7587574641160728
Training time: 63.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 47.22666666666662
Incorrectly Classified Instances: 16.12
Correctly Classified Instances: 83.88
Weighted Precision: 0.8396399098539478
Weighted AreaUnderROC: 0.9601415925890322
Root mean squared error: 0.2817497439491795
Relative absolute error: 28.207343654867575
Root relative squared error: 59.768146363211336
Weighted TruePositiveRate: 0.8388
Weighted MatthewsCorrelation: 0.7585281696731435
Weighted FMeasure: 0.8388823771844611
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9222709389223266
Mean absolute error: 0.12536597179941203
Coverage of cases: 96.52
Instances selection time: 112.0
Test time: 84.0
Accumulative iteration time: 979.0
Weighted Recall: 0.8388
Weighted FalsePositiveRate: 0.08084504141818526
Kappa statistic: 0.7581330899380004
Training time: 63.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 48.10666666666648
Incorrectly Classified Instances: 16.2
Correctly Classified Instances: 83.8
Weighted Precision: 0.8397147769643125
Weighted AreaUnderROC: 0.959511048872808
Root mean squared error: 0.2836680490779871
Relative absolute error: 29.20270271403199
Root relative squared error: 60.17508033270077
Weighted TruePositiveRate: 0.838
Weighted MatthewsCorrelation: 0.7578060068541669
Weighted FMeasure: 0.8380678345006339
Iteration time: 170.0
Weighted AreaUnderPRC: 0.9207852004534237
Mean absolute error: 0.12978978984014278
Coverage of cases: 96.72
Instances selection time: 102.0
Test time: 79.0
Accumulative iteration time: 1149.0
Weighted Recall: 0.838
Weighted FalsePositiveRate: 0.08133662674340529
Kappa statistic: 0.7569039284325165
Training time: 68.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 48.133333333333205
Incorrectly Classified Instances: 16.4
Correctly Classified Instances: 83.6
Weighted Precision: 0.8380408265425331
Weighted AreaUnderROC: 0.9587952770042159
Root mean squared error: 0.28596949211354095
Relative absolute error: 29.607819960504294
Root relative squared error: 60.66329012578717
Weighted TruePositiveRate: 0.836
Weighted MatthewsCorrelation: 0.7549619350095237
Weighted FMeasure: 0.836104359150579
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9193503350218026
Mean absolute error: 0.13159031093557524
Coverage of cases: 96.8
Instances selection time: 98.0
Test time: 80.0
Accumulative iteration time: 1320.0
Weighted Recall: 0.836
Weighted FalsePositiveRate: 0.08236646764061982
Kappa statistic: 0.7538960062964206
Training time: 73.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 48.09333333333314
Incorrectly Classified Instances: 16.2
Correctly Classified Instances: 83.8
Weighted Precision: 0.8401107189925695
Weighted AreaUnderROC: 0.9602161727876631
Root mean squared error: 0.28416546019226285
Relative absolute error: 29.297834243304052
Root relative squared error: 60.28059716428336
Weighted TruePositiveRate: 0.838
Weighted MatthewsCorrelation: 0.7580099095314937
Weighted FMeasure: 0.8380860934054725
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9223360068485151
Mean absolute error: 0.1302125966369075
Coverage of cases: 96.72
Instances selection time: 98.0
Test time: 79.0
Accumulative iteration time: 1491.0
Weighted Recall: 0.838
Weighted FalsePositiveRate: 0.08137095819441141
Kappa statistic: 0.7568950564579717
Training time: 73.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 48.146666666666505
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8393992796796822
Weighted AreaUnderROC: 0.9608284732767599
Root mean squared error: 0.28374354636480165
Relative absolute error: 29.26093297966654
Root relative squared error: 60.19109572574109
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.7565040954301331
Weighted FMeasure: 0.8368391294622772
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9238374860307417
Mean absolute error: 0.1300485910207408
Coverage of cases: 96.68
Instances selection time: 96.0
Test time: 79.0
Accumulative iteration time: 1660.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08200534182922309
Kappa statistic: 0.7550808713358177
Training time: 73.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 48.38666666666657
Incorrectly Classified Instances: 16.8
Correctly Classified Instances: 83.2
Weighted Precision: 0.8347398227295442
Weighted AreaUnderROC: 0.9601397957893018
Root mean squared error: 0.2858126937617418
Relative absolute error: 29.72516653718224
Root relative squared error: 60.630028172436354
Weighted TruePositiveRate: 0.832
Weighted MatthewsCorrelation: 0.7493393654646375
Weighted FMeasure: 0.8321029423575493
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9225775218761906
Mean absolute error: 0.13211185127636613
Coverage of cases: 96.92
Instances selection time: 92.0
Test time: 77.0
Accumulative iteration time: 1826.0
Weighted Recall: 0.832
Weighted FalsePositiveRate: 0.08441928314484765
Kappa statistic: 0.7478773675515771
Training time: 74.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 48.2533333333332
Incorrectly Classified Instances: 17.12
Correctly Classified Instances: 82.88
Weighted Precision: 0.8316568898475023
Weighted AreaUnderROC: 0.9595015443370813
Root mean squared error: 0.28781123158059513
Relative absolute error: 29.891902667951673
Root relative squared error: 61.053982065687045
Weighted TruePositiveRate: 0.8288
Weighted MatthewsCorrelation: 0.7445709319504797
Weighted FMeasure: 0.8289551754814883
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9213949570002489
Mean absolute error: 0.1328529007464525
Coverage of cases: 96.8
Instances selection time: 94.0
Test time: 78.0
Accumulative iteration time: 1998.0
Weighted Recall: 0.8288
Weighted FalsePositiveRate: 0.08603544364546525
Kappa statistic: 0.7430738595420807
Training time: 78.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 48.17333333333322
Incorrectly Classified Instances: 16.88
Correctly Classified Instances: 83.12
Weighted Precision: 0.8346093895213781
Weighted AreaUnderROC: 0.9601759066936211
Root mean squared error: 0.2868889676597589
Relative absolute error: 29.880405869149644
Root relative squared error: 60.85834034394695
Weighted TruePositiveRate: 0.8312
Weighted MatthewsCorrelation: 0.7484824108810842
Weighted FMeasure: 0.8313449402048076
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9229028274645003
Mean absolute error: 0.13280180386288792
Coverage of cases: 96.8
Instances selection time: 92.0
Test time: 81.0
Accumulative iteration time: 2171.0
Weighted Recall: 0.8312
Weighted FalsePositiveRate: 0.08486967072886031
Kappa statistic: 0.7466640700734386
Training time: 81.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 48.58666666666652
Incorrectly Classified Instances: 17.36
Correctly Classified Instances: 82.64
Weighted Precision: 0.8313395884645157
Weighted AreaUnderROC: 0.9597878338010843
Root mean squared error: 0.2881047839718342
Relative absolute error: 30.229507568982676
Root relative squared error: 61.11625393163065
Weighted TruePositiveRate: 0.8264
Weighted MatthewsCorrelation: 0.7421150787102837
Weighted FMeasure: 0.8265477164251606
Iteration time: 179.0
Weighted AreaUnderPRC: 0.9220925753461313
Mean absolute error: 0.13435336697325698
Coverage of cases: 96.84
Instances selection time: 92.0
Test time: 77.0
Accumulative iteration time: 2350.0
Weighted Recall: 0.8264
Weighted FalsePositiveRate: 0.0873660075268996
Kappa statistic: 0.7394316728606679
Training time: 87.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 48.89333333333317
Incorrectly Classified Instances: 17.4
Correctly Classified Instances: 82.6
Weighted Precision: 0.8312756934574365
Weighted AreaUnderROC: 0.959525735217058
Root mean squared error: 0.2888284919572506
Relative absolute error: 30.468313743489496
Root relative squared error: 61.26977557885668
Weighted TruePositiveRate: 0.826
Weighted MatthewsCorrelation: 0.7417219984939059
Weighted FMeasure: 0.8261057931123731
Iteration time: 182.0
Weighted AreaUnderPRC: 0.9214673579570329
Mean absolute error: 0.13541472774884283
Coverage of cases: 97.04
Instances selection time: 91.0
Test time: 74.0
Accumulative iteration time: 2532.0
Weighted Recall: 0.826
Weighted FalsePositiveRate: 0.0875843350287047
Kappa statistic: 0.7388241346192872
Training time: 91.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 48.83999999999987
Incorrectly Classified Instances: 17.52
Correctly Classified Instances: 82.48
Weighted Precision: 0.8317555034266959
Weighted AreaUnderROC: 0.959256548886902
Root mean squared error: 0.2909864180086778
Relative absolute error: 30.775404128297293
Root relative squared error: 61.72754082213567
Weighted TruePositiveRate: 0.8248
Weighted MatthewsCorrelation: 0.7408879140973348
Weighted FMeasure: 0.8248480933544077
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9208250808833455
Mean absolute error: 0.13677957390354417
Coverage of cases: 96.92
Instances selection time: 89.0
Test time: 76.0
Accumulative iteration time: 2713.0
Weighted Recall: 0.8248
Weighted FalsePositiveRate: 0.08826678269492494
Kappa statistic: 0.736995319237243
Training time: 92.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 48.71999999999986
Incorrectly Classified Instances: 17.68
Correctly Classified Instances: 82.32
Weighted Precision: 0.8299023363663035
Weighted AreaUnderROC: 0.958881797320123
Root mean squared error: 0.2925340132397059
Relative absolute error: 30.840518091602988
Root relative squared error: 62.05583534685325
Weighted TruePositiveRate: 0.8232
Weighted MatthewsCorrelation: 0.7383211611516793
Weighted FMeasure: 0.8232801829079323
Iteration time: 184.0
Weighted AreaUnderPRC: 0.920327767802718
Mean absolute error: 0.13706896929601392
Coverage of cases: 96.84
Instances selection time: 88.0
Test time: 72.0
Accumulative iteration time: 2897.0
Weighted Recall: 0.8232
Weighted FalsePositiveRate: 0.08905769721973067
Kappa statistic: 0.7345982947039751
Training time: 96.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 48.41333333333314
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8260821778562649
Weighted AreaUnderROC: 0.9585542449212943
Root mean squared error: 0.2929532633414933
Relative absolute error: 30.922159353304654
Root relative squared error: 62.14477172384935
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7317010029388655
Weighted FMeasure: 0.8183998578881223
Iteration time: 180.0
Weighted AreaUnderPRC: 0.9193088387978158
Mean absolute error: 0.13743181934802132
Coverage of cases: 96.6
Instances selection time: 89.0
Test time: 72.0
Accumulative iteration time: 3077.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.09150596998636139
Kappa statistic: 0.7273754009503188
Training time: 91.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 48.41333333333314
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8281442907135311
Weighted AreaUnderROC: 0.9581837860145955
Root mean squared error: 0.29431264089137726
Relative absolute error: 30.95715261303774
Root relative squared error: 62.43313924896407
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7340310714900854
Weighted FMeasure: 0.8195088147111306
Iteration time: 186.0
Weighted AreaUnderPRC: 0.918612784472591
Mean absolute error: 0.13758734494683503
Coverage of cases: 96.6
Instances selection time: 89.0
Test time: 75.0
Accumulative iteration time: 3263.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.09094024925356198
Kappa statistic: 0.7291632887143363
Training time: 97.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 48.71999999999982
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8284717952533825
Weighted AreaUnderROC: 0.9579928616601534
Root mean squared error: 0.2961457970258064
Relative absolute error: 31.394821736204847
Root relative squared error: 62.822010389052636
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7331047371340952
Weighted FMeasure: 0.8182595509075292
Iteration time: 192.0
Weighted AreaUnderPRC: 0.9180884694752404
Mean absolute error: 0.13953254104979998
Coverage of cases: 96.56
Instances selection time: 86.0
Test time: 72.0
Accumulative iteration time: 3455.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.09160209804917854
Kappa statistic: 0.7273418035943318
Training time: 106.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 48.853333333333104
Incorrectly Classified Instances: 18.32
Correctly Classified Instances: 81.68
Weighted Precision: 0.8270880800687627
Weighted AreaUnderROC: 0.9574620465447744
Root mean squared error: 0.29768999199654017
Relative absolute error: 31.76099294538712
Root relative squared error: 63.149583609636636
Weighted TruePositiveRate: 0.8168
Weighted MatthewsCorrelation: 0.7307929936903239
Weighted FMeasure: 0.8166914398886245
Iteration time: 185.0
Weighted AreaUnderPRC: 0.9168799430872192
Mean absolute error: 0.14115996864616565
Coverage of cases: 96.44
Instances selection time: 82.0
Test time: 71.0
Accumulative iteration time: 3640.0
Weighted Recall: 0.8168
Weighted FalsePositiveRate: 0.09241361144458794
Kappa statistic: 0.7249382732657779
Training time: 103.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 48.83999999999976
Incorrectly Classified Instances: 18.24
Correctly Classified Instances: 81.76
Weighted Precision: 0.8283525784315425
Weighted AreaUnderROC: 0.9573530594382217
Root mean squared error: 0.2980759846630765
Relative absolute error: 31.77922757929689
Root relative squared error: 63.23146501923547
Weighted TruePositiveRate: 0.8176
Weighted MatthewsCorrelation: 0.732292824062679
Weighted FMeasure: 0.8174326354832727
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9164690313187657
Mean absolute error: 0.1412410114635424
Coverage of cases: 96.56
Instances selection time: 85.0
Test time: 70.0
Accumulative iteration time: 3833.0
Weighted Recall: 0.8176
Weighted FalsePositiveRate: 0.09203188676258754
Kappa statistic: 0.7261319160099438
Training time: 108.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 48.839999999999804
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8274673224909901
Weighted AreaUnderROC: 0.9570285498901032
Root mean squared error: 0.2994654331776264
Relative absolute error: 31.98006380396156
Root relative squared error: 63.52621155925982
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7299034554293132
Weighted FMeasure: 0.8154605125682907
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9159717238865652
Mean absolute error: 0.14213361690649648
Coverage of cases: 96.48
Instances selection time: 82.0
Test time: 70.0
Accumulative iteration time: 4022.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09307546024020451
Kappa statistic: 0.7231163382261859
Training time: 107.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 48.59999999999978
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8265560756200497
Weighted AreaUnderROC: 0.9577880726034298
Root mean squared error: 0.2986468992099631
Relative absolute error: 31.694246492420202
Root relative squared error: 63.352574283509945
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7286904413275183
Weighted FMeasure: 0.814557348563061
Iteration time: 188.0
Weighted AreaUnderPRC: 0.9173385348936497
Mean absolute error: 0.14086331774409044
Coverage of cases: 96.4
Instances selection time: 80.0
Test time: 70.0
Accumulative iteration time: 4210.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09347091750260739
Kappa statistic: 0.7219138382160778
Training time: 108.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 48.73333333333314
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8287272149499487
Weighted AreaUnderROC: 0.9577193567948445
Root mean squared error: 0.2989463682921962
Relative absolute error: 31.764798449728566
Root relative squared error: 63.41610126915076
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7314322956045535
Weighted FMeasure: 0.8161243883864827
Iteration time: 188.0
Weighted AreaUnderPRC: 0.9169920880942302
Mean absolute error: 0.1411768819987943
Coverage of cases: 96.48
Instances selection time: 80.0
Test time: 71.0
Accumulative iteration time: 4398.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.09268686926800287
Kappa statistic: 0.7243100182495957
Training time: 108.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 48.786666666666456
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8241343727832101
Weighted AreaUnderROC: 0.9568678769647527
Root mean squared error: 0.30139483827162383
Relative absolute error: 32.16577638764991
Root relative squared error: 63.93550018694625
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7236208599787507
Weighted FMeasure: 0.810455381405112
Iteration time: 194.0
Weighted AreaUnderPRC: 0.9153072443098765
Mean absolute error: 0.14295900616733362
Coverage of cases: 96.52
Instances selection time: 78.0
Test time: 71.0
Accumulative iteration time: 4592.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09553059929703643
Kappa statistic: 0.7158869073719238
Training time: 116.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 48.87999999999974
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8217797039879772
Weighted AreaUnderROC: 0.9560519585912107
Root mean squared error: 0.30273249940835906
Relative absolute error: 32.43864024421261
Root relative squared error: 64.2192609651608
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7192615450069363
Weighted FMeasure: 0.8073205278520971
Iteration time: 193.0
Weighted AreaUnderPRC: 0.913551470474808
Mean absolute error: 0.14417173441872336
Coverage of cases: 96.24
Instances selection time: 78.0
Test time: 70.0
Accumulative iteration time: 4785.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09716735866825772
Kappa statistic: 0.7110723791922136
Training time: 115.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 48.82666666666646
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8225899228443262
Weighted AreaUnderROC: 0.9555406287064415
Root mean squared error: 0.30459824997828233
Relative absolute error: 32.60021434269255
Root relative squared error: 64.61504642915942
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7197614572391356
Weighted FMeasure: 0.8072556821787604
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9128628136451181
Mean absolute error: 0.14488984152307868
Coverage of cases: 96.2
Instances selection time: 77.0
Test time: 70.0
Accumulative iteration time: 4978.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09719482382906261
Kappa statistic: 0.7110618267620422
Training time: 116.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 48.7599999999998
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8218612832416731
Weighted AreaUnderROC: 0.9551945652228906
Root mean squared error: 0.3049084357589707
Relative absolute error: 32.57446467316539
Root relative squared error: 64.68084676984515
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7197016062582963
Weighted FMeasure: 0.807688545786116
Iteration time: 195.0
Weighted AreaUnderPRC: 0.9122754020658864
Mean absolute error: 0.1447753985474024
Coverage of cases: 96.16
Instances selection time: 78.0
Test time: 70.0
Accumulative iteration time: 5173.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09695589745665384
Kappa statistic: 0.7116756928192832
Training time: 117.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 48.61333333333309
Incorrectly Classified Instances: 18.96
Correctly Classified Instances: 81.04
Weighted Precision: 0.8243827254382937
Weighted AreaUnderROC: 0.9556101238028263
Root mean squared error: 0.305120623526156
Relative absolute error: 32.51008259732884
Root relative squared error: 64.7258585925636
Weighted TruePositiveRate: 0.8104
Weighted MatthewsCorrelation: 0.7234209764268451
Weighted FMeasure: 0.8100186547214419
Iteration time: 205.0
Weighted AreaUnderPRC: 0.9130432242376851
Mean absolute error: 0.14448925598812884
Coverage of cases: 96.16
Instances selection time: 77.0
Test time: 71.0
Accumulative iteration time: 5378.0
Weighted Recall: 0.8104
Weighted FalsePositiveRate: 0.09575579308904278
Kappa statistic: 0.7152771470625972
Training time: 128.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 48.95999999999974
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8233917588448527
Weighted AreaUnderROC: 0.9560377854118076
Root mean squared error: 0.3049542147850258
Relative absolute error: 32.70707479190238
Root relative squared error: 64.69055796777305
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.720660526018889
Weighted FMeasure: 0.807509546826308
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9135482708851823
Mean absolute error: 0.14536477685290014
Coverage of cases: 96.28
Instances selection time: 73.0
Test time: 70.0
Accumulative iteration time: 5569.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09700396148806242
Kappa statistic: 0.7116559478770069
Training time: 118.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 48.693333333333044
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8229413438988122
Weighted AreaUnderROC: 0.9555910511278424
Root mean squared error: 0.30548101438082265
Relative absolute error: 32.63669825756666
Root relative squared error: 64.80230903772733
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7195890112333769
Weighted FMeasure: 0.8068421006364103
Iteration time: 199.0
Weighted AreaUnderPRC: 0.912493663577768
Mean absolute error: 0.1450519922558525
Coverage of cases: 96.12
Instances selection time: 74.0
Test time: 71.0
Accumulative iteration time: 5768.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09742001762106894
Kappa statistic: 0.7104531924157479
Training time: 125.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 48.613333333333046
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8220810319187148
Weighted AreaUnderROC: 0.9547020976877948
Root mean squared error: 0.307678340420084
Relative absolute error: 32.87305670703464
Root relative squared error: 65.26843228057918
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7176379890472899
Weighted FMeasure: 0.8051698985248209
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9109998889596139
Mean absolute error: 0.146102474253488
Coverage of cases: 96.0
Instances selection time: 74.0
Test time: 70.0
Accumulative iteration time: 5961.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09824526359688084
Kappa statistic: 0.7080409838370048
Training time: 119.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 48.77333333333307
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8203155514066055
Weighted AreaUnderROC: 0.9540381585043342
Root mean squared error: 0.30984761828250657
Relative absolute error: 33.32444516318862
Root relative squared error: 65.72860560661823
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7139987810989734
Weighted FMeasure: 0.8024038194076148
Iteration time: 203.0
Weighted AreaUnderPRC: 0.909738496534983
Mean absolute error: 0.14810864516972788
Coverage of cases: 95.96
Instances selection time: 73.0
Test time: 69.0
Accumulative iteration time: 6164.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09968429433690067
Kappa statistic: 0.7038249950557397
Training time: 130.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 48.99999999999972
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8193729021141589
Weighted AreaUnderROC: 0.9527308039847612
Root mean squared error: 0.3119407319374919
Relative absolute error: 33.736431138892875
Root relative squared error: 66.17262206438852
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7112506259775462
Weighted FMeasure: 0.7999347499602011
Iteration time: 202.0
Weighted AreaUnderPRC: 0.9074408723650409
Mean absolute error: 0.1499396939506357
Coverage of cases: 95.96
Instances selection time: 70.0
Test time: 74.0
Accumulative iteration time: 6366.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.10093246273592031
Kappa statistic: 0.700204005466741
Training time: 132.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 48.906666666666396
Incorrectly Classified Instances: 20.2
Correctly Classified Instances: 79.8
Weighted Precision: 0.8170351193855946
Weighted AreaUnderROC: 0.9526788602825587
Root mean squared error: 0.3120926983069667
Relative absolute error: 33.70821957242328
Root relative squared error: 66.20485899949888
Weighted TruePositiveRate: 0.798
Weighted MatthewsCorrelation: 0.7076402146560509
Weighted FMeasure: 0.7975910054935357
Iteration time: 203.0
Weighted AreaUnderPRC: 0.9072316720926872
Mean absolute error: 0.14981430921077085
Coverage of cases: 95.76
Instances selection time: 71.0
Test time: 70.0
Accumulative iteration time: 6569.0
Weighted Recall: 0.798
Weighted FalsePositiveRate: 0.10213943339373259
Kappa statistic: 0.6965992440094273
Training time: 132.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 48.53333333333309
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8197669983768752
Weighted AreaUnderROC: 0.9528592368588239
Root mean squared error: 0.3121419137260392
Relative absolute error: 33.52692670313855
Root relative squared error: 66.21529916646843
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7120479041476425
Weighted FMeasure: 0.8010649739540185
Iteration time: 208.0
Weighted AreaUnderPRC: 0.9074995882406452
Mean absolute error: 0.14900856312506092
Coverage of cases: 95.56
Instances selection time: 71.0
Test time: 69.0
Accumulative iteration time: 6777.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.10052327289311498
Kappa statistic: 0.7014164993517674
Training time: 137.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.07999999999969
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8244461631664027
Weighted AreaUnderROC: 0.9551841180539344
Root mean squared error: 0.3066531466323354
Relative absolute error: 33.11175705690055
Root relative squared error: 65.05095583677496
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7196383341456288
Weighted FMeasure: 0.8061510912479849
Iteration time: 203.0
Weighted AreaUnderPRC: 0.9116851556851289
Mean absolute error: 0.14716336469733646
Coverage of cases: 95.92
Instances selection time: 66.0
Test time: 73.0
Accumulative iteration time: 6980.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09789787036588654
Kappa statistic: 0.709230513397985
Training time: 137.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 48.65333333333312
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8240655991976109
Weighted AreaUnderROC: 0.9557764750778962
Root mean squared error: 0.30571983961344795
Relative absolute error: 32.666656470541284
Root relative squared error: 64.85297152017966
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7185649878182727
Weighted FMeasure: 0.8055407030053434
Iteration time: 209.0
Weighted AreaUnderPRC: 0.9133146839066785
Mean absolute error: 0.14518513986907305
Coverage of cases: 96.04
Instances selection time: 67.0
Test time: 74.0
Accumulative iteration time: 7189.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09831392649889308
Kappa statistic: 0.7080289865938445
Training time: 142.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 48.33333333333311
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8274421510012595
Weighted AreaUnderROC: 0.9557436930956201
Root mean squared error: 0.30463457892331125
Relative absolute error: 32.45209930887662
Root relative squared error: 64.62275296217442
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.7241503118639973
Weighted FMeasure: 0.8096523859386878
Iteration time: 206.0
Weighted AreaUnderPRC: 0.913315541837827
Mean absolute error: 0.14423155248389677
Coverage of cases: 95.88
Instances selection time: 69.0
Test time: 70.0
Accumulative iteration time: 7395.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.09628857615547015
Kappa statistic: 0.7140470654500611
Training time: 137.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 48.54666666666641
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8232722287813198
Weighted AreaUnderROC: 0.9554675311928335
Root mean squared error: 0.3052475728170675
Relative absolute error: 32.61990212283678
Root relative squared error: 64.7527886039047
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7188635893996661
Weighted FMeasure: 0.8062583394010895
Iteration time: 210.0
Weighted AreaUnderPRC: 0.9121989394525343
Mean absolute error: 0.14497734276816415
Coverage of cases: 96.12
Instances selection time: 64.0
Test time: 71.0
Accumulative iteration time: 7605.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09785667262467919
Kappa statistic: 0.7092477711198455
Training time: 146.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 48.853333333333154
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8290595432901963
Weighted AreaUnderROC: 0.9563710592372925
Root mean squared error: 0.3029864673217517
Relative absolute error: 32.43095929959426
Root relative squared error: 64.27313569528992
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7264319349909513
Weighted FMeasure: 0.8112864719927463
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9148236190190342
Mean absolute error: 0.14413759688708624
Coverage of cases: 96.28
Instances selection time: 63.0
Test time: 68.0
Accumulative iteration time: 7811.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09547019646985952
Kappa statistic: 0.7164590930968122
Training time: 143.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 48.85333333333311
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8290013320949609
Weighted AreaUnderROC: 0.9568421772355737
Root mean squared error: 0.3018596227819889
Relative absolute error: 32.296514764372844
Root relative squared error: 64.03409587066713
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7266923118481399
Weighted FMeasure: 0.8117127868687344
Iteration time: 205.0
Weighted AreaUnderPRC: 0.916381944544938
Mean absolute error: 0.14354006561943553
Coverage of cases: 96.44
Instances selection time: 61.0
Test time: 69.0
Accumulative iteration time: 8016.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09524500267785317
Kappa statistic: 0.7170701490625832
Training time: 144.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 48.61333333333311
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8277480948731609
Weighted AreaUnderROC: 0.9576091263838286
Root mean squared error: 0.2997633324661411
Relative absolute error: 31.94105133684129
Root relative squared error: 63.58940554136563
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.726026598184776
Weighted FMeasure: 0.8115233963278101
Iteration time: 213.0
Weighted AreaUnderPRC: 0.9175739875093197
Mean absolute error: 0.14196022816373974
Coverage of cases: 96.36
Instances selection time: 62.0
Test time: 70.0
Accumulative iteration time: 8229.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.0951969386464446
Kappa statistic: 0.7170830651328844
Training time: 151.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 48.546666666666454
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8294608078967093
Weighted AreaUnderROC: 0.9575607842315284
Root mean squared error: 0.29960856389968177
Relative absolute error: 31.862087159293498
Root relative squared error: 63.55657417050826
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.7288094951332972
Weighted FMeasure: 0.8135562567737393
Iteration time: 223.0
Weighted AreaUnderPRC: 0.917466854329242
Mean absolute error: 0.14160927626352732
Coverage of cases: 96.44
Instances selection time: 62.0
Test time: 74.0
Accumulative iteration time: 8452.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.09418083032963252
Kappa statistic: 0.7200940964361131
Training time: 161.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 48.266666666666474
Incorrectly Classified Instances: 18.48
Correctly Classified Instances: 81.52
Weighted Precision: 0.8298243672042795
Weighted AreaUnderROC: 0.9577906501364946
Root mean squared error: 0.298874840095971
Relative absolute error: 31.743084878346878
Root relative squared error: 63.40092784737169
Weighted TruePositiveRate: 0.8152
Weighted MatthewsCorrelation: 0.7305613806307015
Weighted FMeasure: 0.8149730403053754
Iteration time: 222.0
Weighted AreaUnderPRC: 0.9177067228635079
Mean absolute error: 0.1410803772370979
Coverage of cases: 96.32
Instances selection time: 61.0
Test time: 72.0
Accumulative iteration time: 8674.0
Weighted Recall: 0.8152
Weighted FalsePositiveRate: 0.09332811919301576
Kappa statistic: 0.7225106563115492
Training time: 161.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 48.46666666666647
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8336780340478286
Weighted AreaUnderROC: 0.9581847066756966
Root mean squared error: 0.29666073749990945
Relative absolute error: 31.56317996175544
Root relative squared error: 62.93124575939634
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.736731959387432
Weighted FMeasure: 0.819487568138104
Iteration time: 230.0
Weighted AreaUnderPRC: 0.9181161365095919
Mean absolute error: 0.14028079983002484
Coverage of cases: 96.48
Instances selection time: 59.0
Test time: 71.0
Accumulative iteration time: 8904.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.09109130763798895
Kappa statistic: 0.7291348555273786
Training time: 171.0
		
Time end:Fri Dec 01 13.51.52 EET 2017