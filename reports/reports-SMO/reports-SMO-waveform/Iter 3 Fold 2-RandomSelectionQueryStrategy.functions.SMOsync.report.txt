Fri Dec 01 13.58.25 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.58.25 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 48.80000000000031
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8418241206783356
Weighted AreaUnderROC: 0.9606342565723045
Root mean squared error: 0.2777532260712787
Relative absolute error: 28.773888835940085
Root relative squared error: 58.92035689543227
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7617366647358009
Weighted FMeasure: 0.8406066215499334
Iteration time: 154.0
Weighted AreaUnderPRC: 0.9211889601415764
Mean absolute error: 0.12788395038195652
Coverage of cases: 97.44
Instances selection time: 8.0
Test time: 73.0
Accumulative iteration time: 154.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.07964354326594009
Kappa statistic: 0.7612047185947606
Training time: 146.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 48.70666666666694
Incorrectly Classified Instances: 15.44
Correctly Classified Instances: 84.56
Weighted Precision: 0.8463064501101708
Weighted AreaUnderROC: 0.962092776445878
Root mean squared error: 0.27446424120342455
Relative absolute error: 28.33206331195258
Root relative squared error: 58.222657844448385
Weighted TruePositiveRate: 0.8456
Weighted MatthewsCorrelation: 0.7687992737776481
Weighted FMeasure: 0.8454277324181592
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9247329149597195
Mean absolute error: 0.1259202813864565
Coverage of cases: 97.36
Instances selection time: 8.0
Test time: 75.0
Accumulative iteration time: 221.0
Weighted Recall: 0.8456
Weighted FalsePositiveRate: 0.07718153791890695
Kappa statistic: 0.7684119684694697
Training time: 59.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 48.50666666666697
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8482530032076283
Weighted AreaUnderROC: 0.9628122860337489
Root mean squared error: 0.27384502426600105
Relative absolute error: 28.1875852342165
Root relative squared error: 58.09130209580506
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7717200446986959
Weighted FMeasure: 0.847479239205157
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9261687091138272
Mean absolute error: 0.12527815659651836
Coverage of cases: 97.68
Instances selection time: 8.0
Test time: 75.0
Accumulative iteration time: 291.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07626155766491202
Kappa statistic: 0.7713920078645949
Training time: 62.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 48.40000000000024
Incorrectly Classified Instances: 15.16
Correctly Classified Instances: 84.84
Weighted Precision: 0.8488540337274593
Weighted AreaUnderROC: 0.9634068928880412
Root mean squared error: 0.2728660422843368
Relative absolute error: 27.96778632557325
Root relative squared error: 57.8836286564368
Weighted TruePositiveRate: 0.8484
Weighted MatthewsCorrelation: 0.7728287622321499
Weighted FMeasure: 0.8482642378951596
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9277679927801437
Mean absolute error: 0.12430127255810391
Coverage of cases: 97.52
Instances selection time: 9.0
Test time: 75.0
Accumulative iteration time: 366.0
Weighted Recall: 0.8484
Weighted FalsePositiveRate: 0.07583176895150302
Kappa statistic: 0.7725993087018985
Training time: 66.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 48.34666666666686
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8463426450223042
Weighted AreaUnderROC: 0.9621138455489454
Root mean squared error: 0.2745009199112944
Relative absolute error: 28.041864965053016
Root relative squared error: 58.23043857336636
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7691225928438867
Weighted FMeasure: 0.8459255080281081
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9245070606448414
Mean absolute error: 0.12463051095579177
Coverage of cases: 97.6
Instances selection time: 9.0
Test time: 76.0
Accumulative iteration time: 445.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07707993735052267
Kappa statistic: 0.7689898170711365
Training time: 70.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 48.06666666666687
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8483074634583869
Weighted AreaUnderROC: 0.962218603020734
Root mean squared error: 0.2743330259534213
Relative absolute error: 27.85289755514979
Root relative squared error: 58.194822886526666
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7720502264091823
Weighted FMeasure: 0.8480368580408971
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9244763270250252
Mean absolute error: 0.12379065580066631
Coverage of cases: 97.36
Instances selection time: 9.0
Test time: 75.0
Accumulative iteration time: 521.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07613249193572284
Kappa statistic: 0.7719764314839583
Training time: 67.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 47.56000000000022
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8505530382959551
Weighted AreaUnderROC: 0.9634657448009534
Root mean squared error: 0.2728817828777279
Relative absolute error: 27.415182096075842
Root relative squared error: 57.88696774053482
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.7755879846552974
Weighted FMeasure: 0.8503959261340415
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9271558442359994
Mean absolute error: 0.12184525376033764
Coverage of cases: 97.52
Instances selection time: 7.0
Test time: 78.0
Accumulative iteration time: 599.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.0749049224073069
Kappa statistic: 0.7755839677186539
Training time: 71.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 47.62666666666691
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8493304589344159
Weighted AreaUnderROC: 0.963174528665316
Root mean squared error: 0.27305825136345563
Relative absolute error: 27.4130634088282
Root relative squared error: 57.92440235941196
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7737647749965617
Weighted FMeasure: 0.8492008959989682
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9262882989708525
Mean absolute error: 0.12183583737257034
Coverage of cases: 97.32
Instances selection time: 7.0
Test time: 78.0
Accumulative iteration time: 680.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.0755255734617161
Kappa statistic: 0.7737797125646227
Training time: 74.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 47.74666666666691
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.848135041050706
Weighted AreaUnderROC: 0.9629842247344012
Root mean squared error: 0.27347630971483355
Relative absolute error: 27.601330034454147
Root relative squared error: 58.01308592796926
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7720039801003326
Weighted FMeasure: 0.8479499266937172
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9260521246577129
Mean absolute error: 0.1226725779309079
Coverage of cases: 97.56
Instances selection time: 6.0
Test time: 77.0
Accumulative iteration time: 765.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07606382903371059
Kappa statistic: 0.7719951480567506
Training time: 79.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 47.86666666666687
Incorrectly Classified Instances: 15.28
Correctly Classified Instances: 84.72
Weighted Precision: 0.8474012181367019
Weighted AreaUnderROC: 0.9631545366302127
Root mean squared error: 0.27314944876236
Relative absolute error: 27.669892683302805
Root relative squared error: 57.943748249169516
Weighted TruePositiveRate: 0.8472
Weighted MatthewsCorrelation: 0.7708533741256017
Weighted FMeasure: 0.8471483182515543
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9268399597326652
Mean absolute error: 0.12297730081467971
Coverage of cases: 97.52
Instances selection time: 7.0
Test time: 77.0
Accumulative iteration time: 849.0
Weighted Recall: 0.8472
Weighted FalsePositiveRate: 0.076445553715711
Kappa statistic: 0.7707972128941087
Training time: 77.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 47.62666666666693
Incorrectly Classified Instances: 15.48
Correctly Classified Instances: 84.52
Weighted Precision: 0.8453026144407316
Weighted AreaUnderROC: 0.9621420149742947
Root mean squared error: 0.2755342932627769
Relative absolute error: 27.780250950168945
Root relative squared error: 58.449650164665584
Weighted TruePositiveRate: 0.8452
Weighted MatthewsCorrelation: 0.7678131940659476
Weighted FMeasure: 0.8450963496485614
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9245223252659774
Mean absolute error: 0.12346778200075144
Coverage of cases: 97.16
Instances selection time: 6.0
Test time: 79.0
Accumulative iteration time: 934.0
Weighted Recall: 0.8452
Weighted FalsePositiveRate: 0.07742733058151698
Kappa statistic: 0.7678056469666656
Training time: 79.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 47.93333333333352
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8490759446142855
Weighted AreaUnderROC: 0.9627362793813978
Root mean squared error: 0.27366388259006635
Relative absolute error: 27.711283498237304
Root relative squared error: 58.05287614358239
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.773298158577654
Weighted FMeasure: 0.8486948879243642
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9260028759687264
Mean absolute error: 0.12316125999216637
Coverage of cases: 97.44
Instances selection time: 6.0
Test time: 78.0
Accumulative iteration time: 1024.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07565463919090526
Kappa statistic: 0.773199310525904
Training time: 84.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 48.94666666666688
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8477858492835285
Weighted AreaUnderROC: 0.9639205482046098
Root mean squared error: 0.27061890820669665
Relative absolute error: 28.114738651461433
Root relative squared error: 57.406939533076375
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7714070434258294
Weighted FMeasure: 0.8475563922443311
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9289845019803855
Mean absolute error: 0.12495439400649584
Coverage of cases: 97.96
Instances selection time: 6.0
Test time: 72.0
Accumulative iteration time: 1120.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07631648798652184
Kappa statistic: 0.7713836676492167
Training time: 90.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 48.96000000000021
Incorrectly Classified Instances: 15.32
Correctly Classified Instances: 84.68
Weighted Precision: 0.8469584205060945
Weighted AreaUnderROC: 0.9640107751607977
Root mean squared error: 0.27013357753417405
Relative absolute error: 28.163605180901055
Root relative squared error: 57.30398535017881
Weighted TruePositiveRate: 0.8468
Weighted MatthewsCorrelation: 0.7701952008104219
Weighted FMeasure: 0.8467819586045942
Iteration time: 114.0
Weighted AreaUnderPRC: 0.9292574898812225
Mean absolute error: 0.12517157858178304
Coverage of cases: 98.04
Instances selection time: 8.0
Test time: 72.0
Accumulative iteration time: 1234.0
Weighted Recall: 0.8468
Weighted FalsePositiveRate: 0.07670507895872347
Kappa statistic: 0.7701835819150921
Training time: 106.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 48.74666666666684
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8493323961502458
Weighted AreaUnderROC: 0.9638963941857699
Root mean squared error: 0.2703939061176742
Relative absolute error: 28.130869009964027
Root relative squared error: 57.35920938219771
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7737942127021239
Weighted FMeasure: 0.8491720298262182
Iteration time: 107.0
Weighted AreaUnderPRC: 0.9288438087148719
Mean absolute error: 0.1250260844887296
Coverage of cases: 97.8
Instances selection time: 8.0
Test time: 70.0
Accumulative iteration time: 1341.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.0754981083009112
Kappa statistic: 0.7737828074933695
Training time: 99.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 48.946666666666765
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.8508641588150553
Weighted AreaUnderROC: 0.9636199648590494
Root mean squared error: 0.2702583427659937
Relative absolute error: 28.267347895053476
Root relative squared error: 57.3304520526216
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.7761306284612948
Weighted FMeasure: 0.8508013768566638
Iteration time: 111.0
Weighted AreaUnderPRC: 0.9279727960890161
Mean absolute error: 0.12563265731134937
Coverage of cases: 97.8
Instances selection time: 8.0
Test time: 70.0
Accumulative iteration time: 1452.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.0747209263565079
Kappa statistic: 0.7761809485223382
Training time: 103.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 49.33333333333349
Incorrectly Classified Instances: 15.28
Correctly Classified Instances: 84.72
Weighted Precision: 0.8472017778598835
Weighted AreaUnderROC: 0.9634862051893253
Root mean squared error: 0.2704902871558453
Relative absolute error: 28.51679902391181
Root relative squared error: 57.37965488789827
Weighted TruePositiveRate: 0.8472
Weighted MatthewsCorrelation: 0.7706976077842701
Weighted FMeasure: 0.8471965511447659
Iteration time: 107.0
Weighted AreaUnderPRC: 0.9277552820490935
Mean absolute error: 0.1267413289951642
Coverage of cases: 98.04
Instances selection time: 7.0
Test time: 68.0
Accumulative iteration time: 1559.0
Weighted Recall: 0.8472
Weighted FalsePositiveRate: 0.07650735032752204
Kappa statistic: 0.7707857153657816
Training time: 100.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 49.58666666666687
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.848095754400837
Weighted AreaUnderROC: 0.9636785704528354
Root mean squared error: 0.2705064589724238
Relative absolute error: 28.61553746890155
Root relative squared error: 57.383085448248316
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7719677811007466
Weighted FMeasure: 0.8479848644397784
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9281676089264042
Mean absolute error: 0.12718016652845193
Coverage of cases: 98.08
Instances selection time: 6.0
Test time: 67.0
Accumulative iteration time: 1672.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07611189306511917
Kappa statistic: 0.7719785112549005
Training time: 107.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 49.9866666666669
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8460791498879453
Weighted AreaUnderROC: 0.9635431482735618
Root mean squared error: 0.2710052292084264
Relative absolute error: 28.79367200500222
Root relative squared error: 57.48889059308774
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7689678420928737
Weighted FMeasure: 0.8459741997890847
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9279986860963308
Mean absolute error: 0.12797187557778825
Coverage of cases: 98.16
Instances selection time: 5.0
Test time: 67.0
Accumulative iteration time: 1782.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07710053622112635
Kappa statistic: 0.768980335606167
Training time: 105.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 49.97333333333356
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.844874481603234
Weighted AreaUnderROC: 0.9636946287541442
Root mean squared error: 0.27096014632942245
Relative absolute error: 28.738672975318632
Root relative squared error: 57.47932707025001
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7671611309261809
Weighted FMeasure: 0.8448015339486182
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9281241002690065
Mean absolute error: 0.12772743544586118
Coverage of cases: 98.24
Instances selection time: 5.0
Test time: 68.0
Accumulative iteration time: 1891.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.0777074546951331
Kappa statistic: 0.7671759353046731
Training time: 104.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 50.0933333333335
Incorrectly Classified Instances: 15.56
Correctly Classified Instances: 84.44
Weighted Precision: 0.8445276827031352
Weighted AreaUnderROC: 0.9640011054004455
Root mean squared error: 0.270305734161856
Relative absolute error: 28.712138868201116
Root relative squared error: 57.34050528583685
Weighted TruePositiveRate: 0.8444
Weighted MatthewsCorrelation: 0.7665756310181608
Weighted FMeasure: 0.844415644221077
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9288148933236018
Mean absolute error: 0.12760950608089444
Coverage of cases: 98.4
Instances selection time: 5.0
Test time: 66.0
Accumulative iteration time: 2010.0
Weighted Recall: 0.8444
Weighted FalsePositiveRate: 0.07792578219693821
Kappa statistic: 0.7665726796664283
Training time: 114.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 50.040000000000184
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.8449148510935706
Weighted AreaUnderROC: 0.9640192863250242
Root mean squared error: 0.27080129335238984
Relative absolute error: 28.745091960667448
Root relative squared error: 57.44562926506858
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7671729229715655
Weighted FMeasure: 0.8448171606346512
Iteration time: 115.0
Weighted AreaUnderPRC: 0.9288973588379679
Mean absolute error: 0.1277559642696337
Coverage of cases: 98.28
Instances selection time: 5.0
Test time: 68.0
Accumulative iteration time: 2125.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.07772118727553555
Kappa statistic: 0.7671727498986481
Training time: 110.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 49.76000000000019
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.844984001141797
Weighted AreaUnderROC: 0.9647245693932586
Root mean squared error: 0.26927254937982287
Relative absolute error: 28.572719091228404
Root relative squared error: 57.12133369615853
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7672114472704187
Weighted FMeasure: 0.8448264885216519
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9302920839226921
Mean absolute error: 0.1269898626276824
Coverage of cases: 98.28
Instances selection time: 4.0
Test time: 67.0
Accumulative iteration time: 2238.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.077734919855938
Kappa statistic: 0.7671663788251023
Training time: 109.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 49.58666666666685
Incorrectly Classified Instances: 15.44
Correctly Classified Instances: 84.56
Weighted Precision: 0.8457790942929534
Weighted AreaUnderROC: 0.9649694990716055
Root mean squared error: 0.2691925457723697
Relative absolute error: 28.39812150842559
Root relative squared error: 57.104362368153666
Weighted TruePositiveRate: 0.8456
Weighted MatthewsCorrelation: 0.7684138831153989
Weighted FMeasure: 0.8456281388621998
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9308389412234602
Mean absolute error: 0.126213873370781
Coverage of cases: 98.24
Instances selection time: 5.0
Test time: 67.0
Accumulative iteration time: 2363.0
Weighted Recall: 0.8456
Weighted FalsePositiveRate: 0.07733259630333392
Kappa statistic: 0.7683654957242433
Training time: 120.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 49.44000000000016
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8481917140927624
Weighted AreaUnderROC: 0.9651991121525387
Root mean squared error: 0.26867960476296543
Relative absolute error: 28.19478080547856
Root relative squared error: 56.99555114832415
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7720271946289935
Weighted FMeasure: 0.8480181649804741
Iteration time: 130.0
Weighted AreaUnderPRC: 0.9313050513947685
Mean absolute error: 0.1253101369132386
Coverage of cases: 98.24
Instances selection time: 5.0
Test time: 68.0
Accumulative iteration time: 2493.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07613249193572286
Kappa statistic: 0.7719649920655819
Training time: 125.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 49.38666666666686
Incorrectly Classified Instances: 15.32
Correctly Classified Instances: 84.68
Weighted Precision: 0.8469538518477002
Weighted AreaUnderROC: 0.9646825335309861
Root mean squared error: 0.2698198064376491
Relative absolute error: 28.285615756719494
Root relative squared error: 57.237424449150865
Weighted TruePositiveRate: 0.8468
Weighted MatthewsCorrelation: 0.7702042992118281
Weighted FMeasure: 0.8468183053025872
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9301321895432834
Mean absolute error: 0.12571384780764278
Coverage of cases: 98.12
Instances selection time: 4.0
Test time: 70.0
Accumulative iteration time: 2619.0
Weighted Recall: 0.8468
Weighted FalsePositiveRate: 0.07672567782932715
Kappa statistic: 0.7701678602735806
Training time: 122.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 49.10666666666681
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8502524612205019
Weighted AreaUnderROC: 0.9653971958070818
Root mean squared error: 0.2689879884710646
Relative absolute error: 28.13366389986273
Root relative squared error: 57.06096921168546
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7750576370273022
Weighted FMeasure: 0.8500084007827763
Iteration time: 133.0
Weighted AreaUnderPRC: 0.9319677781079015
Mean absolute error: 0.12503850622161272
Coverage of cases: 98.08
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 2752.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07514384877971568
Kappa statistic: 0.7749634000473835
Training time: 129.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.16000000000013
Incorrectly Classified Instances: 15.36
Correctly Classified Instances: 84.64
Weighted Precision: 0.8466305211828538
Weighted AreaUnderROC: 0.9651345833354413
Root mean squared error: 0.269774719330255
Relative absolute error: 28.165573031550398
Root relative squared error: 57.22786002933613
Weighted TruePositiveRate: 0.8464
Weighted MatthewsCorrelation: 0.7696266107849857
Weighted FMeasure: 0.8464284423277244
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9316238065217567
Mean absolute error: 0.12518032458466902
Coverage of cases: 98.12
Instances selection time: 4.0
Test time: 70.0
Accumulative iteration time: 2872.0
Weighted Recall: 0.8464
Weighted FalsePositiveRate: 0.07694400533113227
Kappa statistic: 0.7695667254351529
Training time: 116.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 49.04000000000016
Incorrectly Classified Instances: 15.32
Correctly Classified Instances: 84.68
Weighted Precision: 0.8469482502391147
Weighted AreaUnderROC: 0.9659362858863043
Root mean squared error: 0.26827300678230737
Relative absolute error: 27.88740944483762
Root relative squared error: 56.909298691522125
Weighted TruePositiveRate: 0.8468
Weighted MatthewsCorrelation: 0.7701979988016338
Weighted FMeasure: 0.8468057044046942
Iteration time: 130.0
Weighted AreaUnderPRC: 0.9333477661313202
Mean absolute error: 0.12394404197705666
Coverage of cases: 98.28
Instances selection time: 4.0
Test time: 71.0
Accumulative iteration time: 3002.0
Weighted Recall: 0.8468
Weighted FalsePositiveRate: 0.0767119452489247
Kappa statistic: 0.7701751973072933
Training time: 126.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.213333333333466
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8492318361802874
Weighted AreaUnderROC: 0.965895020060409
Root mean squared error: 0.2676913269251662
Relative absolute error: 27.98756655464426
Root relative squared error: 56.785905760082876
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7737433325216121
Weighted FMeasure: 0.8492033360725496
Iteration time: 131.0
Weighted AreaUnderPRC: 0.9333915798321272
Mean absolute error: 0.1243891846873084
Coverage of cases: 98.32
Instances selection time: 5.0
Test time: 69.0
Accumulative iteration time: 3133.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07548437572050874
Kappa statistic: 0.7737828074933695
Training time: 126.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 48.65333333333348
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8500224462696666
Weighted AreaUnderROC: 0.9660692307080735
Root mean squared error: 0.26719280983154037
Relative absolute error: 27.697953463140887
Root relative squared error: 56.68015431485081
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7749469346632468
Weighted FMeasure: 0.8499994353482141
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9336668662455541
Mean absolute error: 0.12310201539173786
Coverage of cases: 98.2
Instances selection time: 4.0
Test time: 71.0
Accumulative iteration time: 3259.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.0750683195875022
Kappa statistic: 0.7749859771260946
Training time: 122.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 48.92000000000013
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8504444732647019
Weighted AreaUnderROC: 0.9661435254135856
Root mean squared error: 0.2664047677090121
Relative absolute error: 27.78456344324598
Root relative squared error: 56.5129853362407
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.7755701825134813
Weighted FMeasure: 0.8503691619338444
Iteration time: 134.0
Weighted AreaUnderPRC: 0.9336526382993857
Mean absolute error: 0.12348694863664937
Coverage of cases: 98.2
Instances selection time: 4.0
Test time: 71.0
Accumulative iteration time: 3393.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07484999208569708
Kappa statistic: 0.7755921547017285
Training time: 130.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 49.05333333333354
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8496193298099107
Weighted AreaUnderROC: 0.96610789471592
Root mean squared error: 0.2665152080195037
Relative absolute error: 27.8360687056197
Root relative squared error: 56.53641326398019
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7743448958159859
Weighted FMeasure: 0.8496041095150567
Iteration time: 138.0
Weighted AreaUnderPRC: 0.9334620521073465
Mean absolute error: 0.12371586091386591
Coverage of cases: 98.24
Instances selection time: 4.0
Test time: 70.0
Accumulative iteration time: 3531.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07527291450890486
Kappa statistic: 0.7743838819845289
Training time: 134.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 48.4666666666668
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8499970355895548
Weighted AreaUnderROC: 0.9662179805734581
Root mean squared error: 0.26596901524000965
Relative absolute error: 27.486833141532102
Root relative squared error: 56.420548278515575
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7749596823722424
Weighted FMeasure: 0.8499775908513296
Iteration time: 133.0
Weighted AreaUnderPRC: 0.9338571858790037
Mean absolute error: 0.12216370285125436
Coverage of cases: 97.92
Instances selection time: 3.0
Test time: 73.0
Accumulative iteration time: 3664.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07502025555609361
Kappa statistic: 0.7749962379370984
Training time: 130.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 48.466666666666846
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8491793829461265
Weighted AreaUnderROC: 0.9663585140191577
Root mean squared error: 0.26546945485377604
Relative absolute error: 27.38759234837664
Root relative squared error: 56.314575517500195
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7737226228224464
Weighted FMeasure: 0.8491846280441899
Iteration time: 145.0
Weighted AreaUnderPRC: 0.9341733577070036
Mean absolute error: 0.1217226326594523
Coverage of cases: 98.0
Instances selection time: 4.0
Test time: 72.0
Accumulative iteration time: 3809.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07545691055970383
Kappa statistic: 0.7737920917715283
Training time: 141.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 48.52000000000021
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8543858911365125
Weighted AreaUnderROC: 0.9664859744819745
Root mean squared error: 0.2651849890583739
Relative absolute error: 27.290391363294862
Root relative squared error: 56.25423120961685
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7815320667753385
Weighted FMeasure: 0.8543896843681932
Iteration time: 138.0
Weighted AreaUnderPRC: 0.934315259435878
Mean absolute error: 0.12129062828131107
Coverage of cases: 97.96
Instances selection time: 3.0
Test time: 73.0
Accumulative iteration time: 3947.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.07285210690307906
Kappa statistic: 0.7815913684908828
Training time: 135.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 48.7200000000002
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8543770974959003
Weighted AreaUnderROC: 0.9665166060867134
Root mean squared error: 0.2643790603167588
Relative absolute error: 27.359966791955834
Root relative squared error: 56.08326790611209
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7815330996489042
Weighted FMeasure: 0.854378783355157
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9341678710075488
Mean absolute error: 0.12159985240869316
Coverage of cases: 98.0
Instances selection time: 4.0
Test time: 71.0
Accumulative iteration time: 4096.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.0728383743226766
Kappa statistic: 0.781595352349098
Training time: 145.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 48.706666666666834
Incorrectly Classified Instances: 14.68
Correctly Classified Instances: 85.32
Weighted Precision: 0.8532270959817512
Weighted AreaUnderROC: 0.9670126258789754
Root mean squared error: 0.26333652096380566
Relative absolute error: 27.19343786362289
Root relative squared error: 55.862111912273996
Weighted TruePositiveRate: 0.8532
Weighted MatthewsCorrelation: 0.7797477101306999
Weighted FMeasure: 0.8532033878392362
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9350461424463483
Mean absolute error: 0.12085972383832452
Coverage of cases: 98.16
Instances selection time: 4.0
Test time: 72.0
Accumulative iteration time: 4247.0
Weighted Recall: 0.8532
Weighted FalsePositiveRate: 0.07346589166728704
Kappa statistic: 0.7797872805133224
Training time: 147.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 48.560000000000194
Incorrectly Classified Instances: 14.48
Correctly Classified Instances: 85.52
Weighted Precision: 0.8551832345777197
Weighted AreaUnderROC: 0.9667970625287752
Root mean squared error: 0.2636649201588153
Relative absolute error: 27.217091831516214
Root relative squared error: 55.93177590159225
Weighted TruePositiveRate: 0.8552
Weighted MatthewsCorrelation: 0.7827379898560711
Weighted FMeasure: 0.8551810273238639
Iteration time: 154.0
Weighted AreaUnderPRC: 0.9346629505565368
Mean absolute error: 0.12096485258451707
Coverage of cases: 98.08
Instances selection time: 3.0
Test time: 72.0
Accumulative iteration time: 4401.0
Weighted Recall: 0.8552
Weighted FalsePositiveRate: 0.07243605077007252
Kappa statistic: 0.7827953778856414
Training time: 151.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 48.8933333333335
Incorrectly Classified Instances: 14.48
Correctly Classified Instances: 85.52
Weighted Precision: 0.8551851912786708
Weighted AreaUnderROC: 0.9672446874067179
Root mean squared error: 0.26280402538418013
Relative absolute error: 27.288111814248136
Root relative squared error: 55.74915254168247
Weighted TruePositiveRate: 0.8552
Weighted MatthewsCorrelation: 0.7827349456289416
Weighted FMeasure: 0.8551868473215815
Iteration time: 154.0
Weighted AreaUnderPRC: 0.9356809591672923
Mean absolute error: 0.1212804969522145
Coverage of cases: 98.24
Instances selection time: 3.0
Test time: 71.0
Accumulative iteration time: 4555.0
Weighted Recall: 0.8552
Weighted FalsePositiveRate: 0.07244978335047497
Kappa statistic: 0.7827914159167569
Training time: 151.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 48.81333333333353
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.853985762304922
Weighted AreaUnderROC: 0.9674438145928181
Root mean squared error: 0.26239443090578807
Relative absolute error: 27.220597498536122
Root relative squared error: 55.66226443172019
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7809367409508214
Weighted FMeasure: 0.8539855490288557
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9359582827938402
Mean absolute error: 0.12098043332682779
Coverage of cases: 98.08
Instances selection time: 3.0
Test time: 70.0
Accumulative iteration time: 4720.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07304296924407927
Kappa statistic: 0.7809933421976027
Training time: 162.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 48.98666666666687
Incorrectly Classified Instances: 14.32
Correctly Classified Instances: 85.68
Weighted Precision: 0.8567962077363284
Weighted AreaUnderROC: 0.9678136743253508
Root mean squared error: 0.2613337799557336
Relative absolute error: 27.139774762358815
Root relative squared error: 55.43726638794355
Weighted TruePositiveRate: 0.8568
Weighted MatthewsCorrelation: 0.7851533068628018
Weighted FMeasure: 0.856768062428122
Iteration time: 168.0
Weighted AreaUnderPRC: 0.9365923430098856
Mean absolute error: 0.12062122116603975
Coverage of cases: 98.32
Instances selection time: 3.0
Test time: 71.0
Accumulative iteration time: 4888.0
Weighted Recall: 0.8568
Weighted FalsePositiveRate: 0.07161767108446188
Kappa statistic: 0.7852003264955038
Training time: 165.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 49.20000000000016
Incorrectly Classified Instances: 14.04
Correctly Classified Instances: 85.96
Weighted Precision: 0.8596387630723717
Weighted AreaUnderROC: 0.9681064366049881
Root mean squared error: 0.25962773545765
Relative absolute error: 27.105200056593333
Root relative squared error: 55.07535969786328
Weighted TruePositiveRate: 0.8596
Weighted MatthewsCorrelation: 0.7893920158973571
Weighted FMeasure: 0.8595141374601666
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9371083523622586
Mean absolute error: 0.12046755580708204
Coverage of cases: 98.48
Instances selection time: 3.0
Test time: 70.0
Accumulative iteration time: 5061.0
Weighted Recall: 0.8596
Weighted FalsePositiveRate: 0.07018550663464326
Kappa statistic: 0.7894118434779228
Training time: 170.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 49.200000000000145
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.8612450815106051
Weighted AreaUnderROC: 0.9681648763959348
Root mean squared error: 0.25980091802049726
Relative absolute error: 27.07810727025204
Root relative squared error: 55.11209726723505
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.791798378551138
Weighted FMeasure: 0.8611442491409221
Iteration time: 177.0
Weighted AreaUnderPRC: 0.9371203558453525
Mean absolute error: 0.12034714342334298
Coverage of cases: 98.52
Instances selection time: 3.0
Test time: 75.0
Accumulative iteration time: 5238.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06938085952943507
Kappa statistic: 0.7918088606148922
Training time: 174.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 49.26666666666686
Incorrectly Classified Instances: 13.84
Correctly Classified Instances: 86.16
Weighted Precision: 0.861627976236428
Weighted AreaUnderROC: 0.9681599980051621
Root mean squared error: 0.25984981401467405
Relative absolute error: 27.0567446665688
Root relative squared error: 55.12246967395163
Weighted TruePositiveRate: 0.8616
Weighted MatthewsCorrelation: 0.7923853786677002
Weighted FMeasure: 0.8615539521532953
Iteration time: 170.0
Weighted AreaUnderPRC: 0.9372121393621464
Mean absolute error: 0.12025219851808412
Coverage of cases: 98.44
Instances selection time: 3.0
Test time: 70.0
Accumulative iteration time: 5408.0
Weighted Recall: 0.8616
Weighted FalsePositiveRate: 0.06918999718843485
Kappa statistic: 0.7924059953148554
Training time: 167.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 49.25333333333343
Incorrectly Classified Instances: 14.0
Correctly Classified Instances: 86.0
Weighted Precision: 0.8599950066693652
Weighted AreaUnderROC: 0.9678129677479508
Root mean squared error: 0.2603812916102302
Relative absolute error: 27.228215496886705
Root relative squared error: 55.235213097511576
Weighted TruePositiveRate: 0.86
Weighted MatthewsCorrelation: 0.78994913558173
Weighted FMeasure: 0.8599845278658984
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9364151072938028
Mean absolute error: 0.12101429109727481
Coverage of cases: 98.24
Instances selection time: 3.0
Test time: 70.0
Accumulative iteration time: 5577.0
Weighted Recall: 0.86
Weighted FalsePositiveRate: 0.0700358420348504
Kappa statistic: 0.7899936158059204
Training time: 166.0
		
Time end:Fri Dec 01 13.58.37 EET 2017