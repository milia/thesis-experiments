Fri Dec 01 13.47.18 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.47.18 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 49.13333333333299
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.8423850391381454
Weighted AreaUnderROC: 0.9566898447567923
Root mean squared error: 0.27954161319161297
Relative absolute error: 29.227675201836743
Root relative squared error: 59.29973109348477
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.7635459871632408
Weighted FMeasure: 0.8422412766694146
Iteration time: 155.0
Weighted AreaUnderPRC: 0.913002273820825
Mean absolute error: 0.12990077867483057
Coverage of cases: 96.92
Instances selection time: 56.0
Test time: 137.0
Accumulative iteration time: 155.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.07883202987053067
Kappa statistic: 0.7636122165206503
Training time: 99.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 48.23999999999985
Incorrectly Classified Instances: 14.32
Correctly Classified Instances: 85.68
Weighted Precision: 0.8569872269900821
Weighted AreaUnderROC: 0.9624265660381721
Root mean squared error: 0.2691306810111489
Relative absolute error: 27.54280282797116
Root relative squared error: 57.09123887050096
Weighted TruePositiveRate: 0.8568
Weighted MatthewsCorrelation: 0.7853342453867094
Weighted FMeasure: 0.8564155486417003
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9253650189294409
Mean absolute error: 0.12241245701320572
Coverage of cases: 97.2
Instances selection time: 56.0
Test time: 79.0
Accumulative iteration time: 281.0
Weighted Recall: 0.8568
Weighted FalsePositiveRate: 0.07153527560204717
Kappa statistic: 0.7852414569891428
Training time: 70.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 47.23999999999989
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8564506907005274
Weighted AreaUnderROC: 0.9636973576347003
Root mean squared error: 0.26757323134498157
Relative absolute error: 26.659169793652307
Root relative squared error: 56.76085390440987
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7846261713759725
Weighted FMeasure: 0.8561302124687732
Iteration time: 143.0
Weighted AreaUnderPRC: 0.928189423780527
Mean absolute error: 0.11848519908289969
Coverage of cases: 97.04
Instances selection time: 59.0
Test time: 84.0
Accumulative iteration time: 424.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07176733568425474
Kappa statistic: 0.7846297900074463
Training time: 84.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 47.50666666666658
Incorrectly Classified Instances: 14.52
Correctly Classified Instances: 85.48
Weighted Precision: 0.8549619069277873
Weighted AreaUnderROC: 0.9622913280200294
Root mean squared error: 0.2693452501074279
Relative absolute error: 27.145123490912752
Root relative squared error: 57.136755849404544
Weighted TruePositiveRate: 0.8548
Weighted MatthewsCorrelation: 0.7823200188628809
Weighted FMeasure: 0.8544095086317971
Iteration time: 232.0
Weighted AreaUnderPRC: 0.9254431033465377
Mean absolute error: 0.12064499329294613
Coverage of cases: 97.08
Instances selection time: 105.0
Test time: 91.0
Accumulative iteration time: 656.0
Weighted Recall: 0.8548
Weighted FalsePositiveRate: 0.0725376513384568
Kappa statistic: 0.7822420359973711
Training time: 127.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 47.106666666666555
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8536093109309604
Weighted AreaUnderROC: 0.9619340314302628
Root mean squared error: 0.27132937329814216
Relative absolute error: 27.047306138759385
Root relative squared error: 57.557651938263604
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7804233045828282
Weighted FMeasure: 0.8531802833054075
Iteration time: 150.0
Weighted AreaUnderPRC: 0.92460135743579
Mean absolute error: 0.12021024950559782
Coverage of cases: 97.0
Instances selection time: 75.0
Test time: 86.0
Accumulative iteration time: 806.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07317890126346968
Kappa statistic: 0.7804373783407111
Training time: 75.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 47.14666666666649
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8582970582792786
Weighted AreaUnderROC: 0.9616541140281198
Root mean squared error: 0.27190499951699193
Relative absolute error: 26.979077887843584
Root relative squared error: 57.67976069909684
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7871896267841166
Weighted FMeasure: 0.8576441117994448
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9239603526652139
Mean absolute error: 0.11990701283486094
Coverage of cases: 96.88
Instances selection time: 79.0
Test time: 143.0
Accumulative iteration time: 972.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.07093522341824164
Kappa statistic: 0.7870401386746626
Training time: 87.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 48.09333333333309
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8545879008648409
Weighted AreaUnderROC: 0.9622759736941582
Root mean squared error: 0.27013298273168546
Relative absolute error: 27.304272310789603
Root relative squared error: 57.30385917351686
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7817328741438939
Weighted FMeasure: 0.8540306702153666
Iteration time: 217.0
Weighted AreaUnderPRC: 0.9255062100905045
Mean absolute error: 0.1213523213812877
Coverage of cases: 97.28
Instances selection time: 58.0
Test time: 82.0
Accumulative iteration time: 1189.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.07273537996965822
Kappa statistic: 0.7816411562748654
Training time: 159.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 47.22666666666652
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.8527903821500645
Weighted AreaUnderROC: 0.9620851672953539
Root mean squared error: 0.27122104042322315
Relative absolute error: 27.114513352056402
Root relative squared error: 57.534671065119404
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7788604755551678
Weighted FMeasure: 0.8520182762218234
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9241476854361743
Mean absolute error: 0.12050894823136236
Coverage of cases: 97.12
Instances selection time: 59.0
Test time: 88.0
Accumulative iteration time: 1330.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07370342425506173
Kappa statistic: 0.778647776663885
Training time: 82.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 48.146666666666455
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8558154369148906
Weighted AreaUnderROC: 0.9607344650906965
Root mean squared error: 0.2713965269016467
Relative absolute error: 27.695144921583175
Root relative squared error: 57.57189736878935
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.783546970497811
Weighted FMeasure: 0.8552520361675952
Iteration time: 139.0
Weighted AreaUnderPRC: 0.921759970805256
Mean absolute error: 0.12308953298481469
Coverage of cases: 97.32
Instances selection time: 56.0
Test time: 93.0
Accumulative iteration time: 1469.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07212846149565147
Kappa statistic: 0.7834408170747977
Training time: 83.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 47.65333333333315
Incorrectly Classified Instances: 14.8
Correctly Classified Instances: 85.2
Weighted Precision: 0.8521060676786445
Weighted AreaUnderROC: 0.9614515483484404
Root mean squared error: 0.2725755629761526
Relative absolute error: 27.40217092936389
Root relative squared error: 57.82200868985337
Weighted TruePositiveRate: 0.852
Weighted MatthewsCorrelation: 0.7780903772309091
Weighted FMeasure: 0.8515855280751637
Iteration time: 143.0
Weighted AreaUnderPRC: 0.9235296383729881
Mean absolute error: 0.12178742635272898
Coverage of cases: 97.36
Instances selection time: 60.0
Test time: 105.0
Accumulative iteration time: 1612.0
Weighted Recall: 0.852
Weighted FalsePositiveRate: 0.07394921691767173
Kappa statistic: 0.7780418346750004
Training time: 83.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 46.879999999999846
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8558072592380714
Weighted AreaUnderROC: 0.9612572120982588
Root mean squared error: 0.27403878350529065
Relative absolute error: 27.145071115440523
Root relative squared error: 58.132404637410836
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7835616176739121
Weighted FMeasure: 0.8551396529865473
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9235679542752637
Mean absolute error: 0.12064476051306956
Coverage of cases: 96.96
Instances selection time: 62.0
Test time: 87.0
Accumulative iteration time: 1763.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07212846149565147
Kappa statistic: 0.7834467408554536
Training time: 89.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 46.50666666666656
Incorrectly Classified Instances: 14.28
Correctly Classified Instances: 85.72
Weighted Precision: 0.8574818633061055
Weighted AreaUnderROC: 0.9627741539827013
Root mean squared error: 0.27057770840865625
Relative absolute error: 26.52265614145645
Root relative squared error: 57.39819973610302
Weighted TruePositiveRate: 0.8572
Weighted MatthewsCorrelation: 0.7859808271869678
Weighted FMeasure: 0.8569013303980261
Iteration time: 159.0
Weighted AreaUnderPRC: 0.9260129407659531
Mean absolute error: 0.11787847173980702
Coverage of cases: 96.88
Instances selection time: 62.0
Test time: 88.0
Accumulative iteration time: 1922.0
Weighted Recall: 0.8572
Weighted FalsePositiveRate: 0.07131694810024207
Kappa statistic: 0.7858393884196818
Training time: 97.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 46.77333333333324
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8558488092414253
Weighted AreaUnderROC: 0.9640424153151518
Root mean squared error: 0.2692066734527704
Relative absolute error: 26.402658275856712
Root relative squared error: 57.1073593017378
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7835773376830373
Weighted FMeasure: 0.8551987444464081
Iteration time: 162.0
Weighted AreaUnderPRC: 0.9292592046925069
Mean absolute error: 0.11734514789269704
Coverage of cases: 96.96
Instances selection time: 61.0
Test time: 88.0
Accumulative iteration time: 2084.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07212159520545025
Kappa statistic: 0.783444766297911
Training time: 101.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 47.03999999999983
Incorrectly Classified Instances: 14.8
Correctly Classified Instances: 85.2
Weighted Precision: 0.8519460670965443
Weighted AreaUnderROC: 0.9637248425993317
Root mean squared error: 0.2690412122176054
Relative absolute error: 26.509602234510336
Root relative squared error: 57.07225967331521
Weighted TruePositiveRate: 0.852
Weighted MatthewsCorrelation: 0.7779632867674416
Weighted FMeasure: 0.8516629340431422
Iteration time: 201.0
Weighted AreaUnderPRC: 0.9279903660171407
Mean absolute error: 0.11782045437560205
Coverage of cases: 97.04
Instances selection time: 97.0
Test time: 129.0
Accumulative iteration time: 2285.0
Weighted Recall: 0.852
Weighted FalsePositiveRate: 0.0739972809490803
Kappa statistic: 0.7780307027931898
Training time: 104.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 46.999999999999794
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8534252985168312
Weighted AreaUnderROC: 0.9623707094178758
Root mean squared error: 0.27089486652192696
Relative absolute error: 26.80299071131884
Root relative squared error: 57.465479131883626
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7802420683796403
Weighted FMeasure: 0.8533952173739011
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9248791172451296
Mean absolute error: 0.11912440316141763
Coverage of cases: 97.08
Instances selection time: 62.0
Test time: 90.0
Accumulative iteration time: 2451.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07325443045568315
Kappa statistic: 0.7804143521179456
Training time: 104.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 47.10666666666647
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8490928052918302
Weighted AreaUnderROC: 0.9619694298304894
Root mean squared error: 0.27271559495135717
Relative absolute error: 27.05429104638206
Root relative squared error: 57.85171395762839
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7736833719514141
Weighted FMeasure: 0.849033334920524
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9253979435692475
Mean absolute error: 0.12024129353947638
Coverage of cases: 96.92
Instances selection time: 60.0
Test time: 87.0
Accumulative iteration time: 2611.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07544317797930139
Kappa statistic: 0.7738127207725837
Training time: 100.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 46.58666666666652
Incorrectly Classified Instances: 14.88
Correctly Classified Instances: 85.12
Weighted Precision: 0.8512270479295093
Weighted AreaUnderROC: 0.962357772170709
Root mean squared error: 0.27188352640059865
Relative absolute error: 26.655273627955868
Root relative squared error: 57.675205563232375
Weighted TruePositiveRate: 0.8512
Weighted MatthewsCorrelation: 0.776810598667196
Weighted FMeasure: 0.850924740009165
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9253954262667846
Mean absolute error: 0.11846788279091552
Coverage of cases: 97.0
Instances selection time: 79.0
Test time: 104.0
Accumulative iteration time: 2798.0
Weighted Recall: 0.8512
Weighted FalsePositiveRate: 0.07437213934087951
Kappa statistic: 0.7768308687542338
Training time: 108.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 46.53333333333323
Incorrectly Classified Instances: 14.72
Correctly Classified Instances: 85.28
Weighted Precision: 0.8528319014621542
Weighted AreaUnderROC: 0.9635958531943788
Root mean squared error: 0.27149191856431654
Relative absolute error: 26.458892352091585
Root relative squared error: 57.59213299625211
Weighted TruePositiveRate: 0.8528
Weighted MatthewsCorrelation: 0.7792243233004119
Weighted FMeasure: 0.8524887497160903
Iteration time: 171.0
Weighted AreaUnderPRC: 0.928363028671973
Mean absolute error: 0.1175950771204076
Coverage of cases: 97.04
Instances selection time: 62.0
Test time: 93.0
Accumulative iteration time: 2969.0
Weighted Recall: 0.8528
Weighted FalsePositiveRate: 0.07356749223567131
Kappa statistic: 0.779232549952835
Training time: 109.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 46.85333333333319
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8587853158355356
Weighted AreaUnderROC: 0.9653389138685329
Root mean squared error: 0.26660909636507246
Relative absolute error: 26.14572206063077
Root relative squared error: 56.556329989728
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.7881492053729312
Weighted FMeasure: 0.8586619486262478
Iteration time: 245.0
Weighted AreaUnderPRC: 0.9314948970952636
Mean absolute error: 0.11620320915835952
Coverage of cases: 97.28
Instances selection time: 60.0
Test time: 89.0
Accumulative iteration time: 3214.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07060842905785103
Kappa statistic: 0.7882148080206232
Training time: 185.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 47.13333333333321
Incorrectly Classified Instances: 14.4
Correctly Classified Instances: 85.6
Weighted Precision: 0.8560160432641274
Weighted AreaUnderROC: 0.9645755003537946
Root mean squared error: 0.2677149267021308
Relative absolute error: 26.50130342770006
Root relative squared error: 56.79091202878073
Weighted TruePositiveRate: 0.856
Weighted MatthewsCorrelation: 0.7839968795671782
Weighted FMeasure: 0.8557718883666247
Iteration time: 169.0
Weighted AreaUnderPRC: 0.930167174894401
Mean absolute error: 0.11778357078977858
Coverage of cases: 97.28
Instances selection time: 58.0
Test time: 89.0
Accumulative iteration time: 3383.0
Weighted Recall: 0.856
Weighted FalsePositiveRate: 0.07197879689585861
Kappa statistic: 0.7840249494378411
Training time: 111.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 47.11999999999989
Incorrectly Classified Instances: 14.4
Correctly Classified Instances: 85.6
Weighted Precision: 0.8561674500155027
Weighted AreaUnderROC: 0.9650717485405519
Root mean squared error: 0.26687675339353817
Relative absolute error: 26.261619153240964
Root relative squared error: 56.61310861968611
Weighted TruePositiveRate: 0.856
Weighted MatthewsCorrelation: 0.7840973946770815
Weighted FMeasure: 0.8557478149007562
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9312078497722827
Mean absolute error: 0.11671830734773816
Coverage of cases: 97.4
Instances selection time: 57.0
Test time: 89.0
Accumulative iteration time: 3589.0
Weighted Recall: 0.856
Weighted FalsePositiveRate: 0.07194446544485249
Kappa statistic: 0.7840328270102944
Training time: 149.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 46.98666666666655
Incorrectly Classified Instances: 13.84
Correctly Classified Instances: 86.16
Weighted Precision: 0.8621542702355196
Weighted AreaUnderROC: 0.9659101316809907
Root mean squared error: 0.2633935377005883
Relative absolute error: 25.73641379863703
Root relative squared error: 55.87420698864004
Weighted TruePositiveRate: 0.8616
Weighted MatthewsCorrelation: 0.7927511610639949
Weighted FMeasure: 0.861271231505469
Iteration time: 198.0
Weighted AreaUnderPRC: 0.9327435846580047
Mean absolute error: 0.11438406132727623
Coverage of cases: 97.28
Instances selection time: 76.0
Test time: 125.0
Accumulative iteration time: 3787.0
Weighted Recall: 0.8616
Weighted FalsePositiveRate: 0.06908013654521525
Kappa statistic: 0.7924457449578114
Training time: 122.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 47.11999999999993
Incorrectly Classified Instances: 14.28
Correctly Classified Instances: 85.72
Weighted Precision: 0.8576627707284357
Weighted AreaUnderROC: 0.964064482415431
Root mean squared error: 0.2669166633715527
Relative absolute error: 26.33870826297473
Root relative squared error: 56.62157480451343
Weighted TruePositiveRate: 0.8572
Weighted MatthewsCorrelation: 0.7860956451408955
Weighted FMeasure: 0.8568785596910032
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9282951524416356
Mean absolute error: 0.11706092561322157
Coverage of cases: 97.2
Instances selection time: 57.0
Test time: 142.0
Accumulative iteration time: 3968.0
Weighted Recall: 0.8572
Weighted FalsePositiveRate: 0.07128948293943715
Kappa statistic: 0.7858452466162948
Training time: 124.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 47.42666666666652
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8594084362218553
Weighted AreaUnderROC: 0.9650526401608591
Root mean squared error: 0.265131435806834
Relative absolute error: 26.076115229818583
Root relative squared error: 56.242870849421315
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7889285757461405
Weighted FMeasure: 0.8589036059648159
Iteration time: 186.0
Weighted AreaUnderPRC: 0.9306041342458587
Mean absolute error: 0.11589384546586091
Coverage of cases: 97.48
Instances selection time: 54.0
Test time: 86.0
Accumulative iteration time: 4154.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07034203752463734
Kappa statistic: 0.7888349858195494
Training time: 132.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 47.17333333333318
Incorrectly Classified Instances: 13.76
Correctly Classified Instances: 86.24
Weighted Precision: 0.8626210862985781
Weighted AreaUnderROC: 0.9641506983017134
Root mean squared error: 0.26660391094514124
Relative absolute error: 26.206119907413267
Root relative squared error: 56.55522999604901
Weighted TruePositiveRate: 0.8624
Weighted MatthewsCorrelation: 0.7937300274151139
Weighted FMeasure: 0.8621337164500663
Iteration time: 186.0
Weighted AreaUnderPRC: 0.9282787443579875
Mean absolute error: 0.1164716440329484
Coverage of cases: 97.2
Instances selection time: 54.0
Test time: 87.0
Accumulative iteration time: 4340.0
Weighted Recall: 0.8624
Weighted FalsePositiveRate: 0.06873960960442217
Kappa statistic: 0.7936332498107905
Training time: 132.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 46.99999999999987
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.8614221539172624
Weighted AreaUnderROC: 0.9657090004054726
Root mean squared error: 0.26350156657683227
Relative absolute error: 25.81462152536241
Root relative squared error: 55.89712337392685
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.7919302979652442
Weighted FMeasure: 0.8609277859562894
Iteration time: 179.0
Weighted AreaUnderPRC: 0.9313818983482646
Mean absolute error: 0.11473165122383346
Coverage of cases: 97.12
Instances selection time: 54.0
Test time: 86.0
Accumulative iteration time: 4519.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06935339436863017
Kappa statistic: 0.7918306924827003
Training time: 125.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 46.62666666666657
Incorrectly Classified Instances: 13.6
Correctly Classified Instances: 86.4
Weighted Precision: 0.8642247421026307
Weighted AreaUnderROC: 0.965408922997085
Root mean squared error: 0.26409776708061217
Relative absolute error: 25.764616134395883
Root relative squared error: 56.02359659967773
Weighted TruePositiveRate: 0.864
Weighted MatthewsCorrelation: 0.7961380625637494
Weighted FMeasure: 0.8637121630442429
Iteration time: 315.0
Weighted AreaUnderPRC: 0.9307655148652341
Mean absolute error: 0.11450940504176
Coverage of cases: 97.2
Instances selection time: 101.0
Test time: 87.0
Accumulative iteration time: 4834.0
Weighted Recall: 0.864
Weighted FalsePositiveRate: 0.06794182878941522
Kappa statistic: 0.7960328631850837
Training time: 214.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 46.31999999999993
Incorrectly Classified Instances: 13.4
Correctly Classified Instances: 86.6
Weighted Precision: 0.8660828733285696
Weighted AreaUnderROC: 0.9656753418509061
Root mean squared error: 0.26401386456332904
Relative absolute error: 25.33763379277836
Root relative squared error: 56.00579818799888
Weighted TruePositiveRate: 0.866
Weighted MatthewsCorrelation: 0.7990477237034267
Weighted FMeasure: 0.8657202864820218
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9315284656639057
Mean absolute error: 0.11261170574568212
Coverage of cases: 96.76
Instances selection time: 58.0
Test time: 89.0
Accumulative iteration time: 5040.0
Weighted Recall: 0.866
Weighted FalsePositiveRate: 0.0669669182138105
Kappa statistic: 0.7990277984749151
Training time: 148.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 46.29333333333326
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8649528052454368
Weighted AreaUnderROC: 0.9660079261563015
Root mean squared error: 0.2630403360627274
Relative absolute error: 25.212586785213553
Root relative squared error: 55.799281606662746
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7972958116801634
Weighted FMeasure: 0.8645055390303503
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9322879462223518
Mean absolute error: 0.11205594126761632
Coverage of cases: 96.8
Instances selection time: 57.0
Test time: 98.0
Accumulative iteration time: 5246.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06755323781721358
Kappa statistic: 0.7972308209152209
Training time: 149.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 46.759999999999955
Incorrectly Classified Instances: 13.84
Correctly Classified Instances: 86.16
Weighted Precision: 0.8616463823880808
Weighted AreaUnderROC: 0.9656792327968713
Root mean squared error: 0.2650984676238084
Relative absolute error: 25.744310087332906
Root relative squared error: 56.23587724168707
Weighted TruePositiveRate: 0.8616
Weighted MatthewsCorrelation: 0.7924404124702611
Weighted FMeasure: 0.8612421594690053
Iteration time: 185.0
Weighted AreaUnderPRC: 0.9319104811057587
Mean absolute error: 0.11441915594370235
Coverage of cases: 97.12
Instances selection time: 54.0
Test time: 90.0
Accumulative iteration time: 5431.0
Weighted Recall: 0.8616
Weighted FalsePositiveRate: 0.06916939831783118
Kappa statistic: 0.792432496768306
Training time: 131.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 46.453333333333234
Incorrectly Classified Instances: 13.8
Correctly Classified Instances: 86.2
Weighted Precision: 0.8621189831414864
Weighted AreaUnderROC: 0.96681575285981
Root mean squared error: 0.2615326170734959
Relative absolute error: 25.17072501918075
Root relative squared error: 55.47944611023995
Weighted TruePositiveRate: 0.862
Weighted MatthewsCorrelation: 0.7930605559204272
Weighted FMeasure: 0.8617558734958136
Iteration time: 253.0
Weighted AreaUnderPRC: 0.9339315238119121
Mean absolute error: 0.11186988897413719
Coverage of cases: 97.0
Instances selection time: 55.0
Test time: 89.0
Accumulative iteration time: 5684.0
Weighted Recall: 0.862
Weighted FalsePositiveRate: 0.06896480339642852
Kappa statistic: 0.7930276846169056
Training time: 198.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 46.9066666666665
Incorrectly Classified Instances: 13.96
Correctly Classified Instances: 86.04
Weighted Precision: 0.8604201588755095
Weighted AreaUnderROC: 0.9659314966850274
Root mean squared error: 0.2633020101088686
Relative absolute error: 25.619001204115282
Root relative squared error: 55.85479105440883
Weighted TruePositiveRate: 0.8604
Weighted MatthewsCorrelation: 0.7905957811938124
Weighted FMeasure: 0.8601594329795093
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9321550326618555
Mean absolute error: 0.11386222757384623
Coverage of cases: 97.08
Instances selection time: 53.0
Test time: 87.0
Accumulative iteration time: 5877.0
Weighted Recall: 0.8604
Weighted FalsePositiveRate: 0.06978318308203915
Kappa statistic: 0.7906251417329808
Training time: 140.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 46.81333333333315
Incorrectly Classified Instances: 13.96
Correctly Classified Instances: 86.04
Weighted Precision: 0.8604003213207184
Weighted AreaUnderROC: 0.966072865216169
Root mean squared error: 0.2629894198427527
Relative absolute error: 25.43925122638394
Root relative squared error: 55.78848064533779
Weighted TruePositiveRate: 0.8604
Weighted MatthewsCorrelation: 0.7905707461291557
Weighted FMeasure: 0.8601960322387673
Iteration time: 214.0
Weighted AreaUnderPRC: 0.9324295191939845
Mean absolute error: 0.11306333878392916
Coverage of cases: 97.04
Instances selection time: 53.0
Test time: 92.0
Accumulative iteration time: 6091.0
Weighted Recall: 0.8604
Weighted FalsePositiveRate: 0.0697969156624416
Kappa statistic: 0.7906213231244531
Training time: 161.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 46.59999999999988
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8632110649185928
Weighted AreaUnderROC: 0.9660905365897379
Root mean squared error: 0.26317786560144313
Relative absolute error: 25.39743917254037
Root relative squared error: 55.828456027494546
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7947919808903848
Weighted FMeasure: 0.8629676986163064
Iteration time: 201.0
Weighted AreaUnderPRC: 0.9325885737229046
Mean absolute error: 0.11287750743351328
Coverage of cases: 97.08
Instances selection time: 54.0
Test time: 97.0
Accumulative iteration time: 6292.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06837848379302544
Kappa statistic: 0.7948246374575341
Training time: 147.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 47.23999999999984
Incorrectly Classified Instances: 13.36
Correctly Classified Instances: 86.64
Weighted Precision: 0.8664295179574752
Weighted AreaUnderROC: 0.9662743584535958
Root mean squared error: 0.26180419582774966
Relative absolute error: 25.750679379327455
Root relative squared error: 55.53705666386766
Weighted TruePositiveRate: 0.8664
Weighted MatthewsCorrelation: 0.7995902634989168
Weighted FMeasure: 0.8662054016841859
Iteration time: 196.0
Weighted AreaUnderPRC: 0.9327766152738269
Mean absolute error: 0.11444746390812256
Coverage of cases: 97.32
Instances selection time: 51.0
Test time: 87.0
Accumulative iteration time: 6488.0
Weighted Recall: 0.8664
Weighted FalsePositiveRate: 0.06678978845321273
Kappa statistic: 0.7996204066577859
Training time: 145.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 47.51999999999979
Incorrectly Classified Instances: 13.4
Correctly Classified Instances: 86.6
Weighted Precision: 0.8660403795916938
Weighted AreaUnderROC: 0.9663779418620366
Root mean squared error: 0.2613640139905871
Relative absolute error: 25.874357637583476
Root relative squared error: 55.44367999526382
Weighted TruePositiveRate: 0.866
Weighted MatthewsCorrelation: 0.7989981400544283
Weighted FMeasure: 0.865797868462756
Iteration time: 207.0
Weighted AreaUnderPRC: 0.932895476459446
Mean absolute error: 0.1149971450559271
Coverage of cases: 97.48
Instances selection time: 50.0
Test time: 83.0
Accumulative iteration time: 6695.0
Weighted Recall: 0.866
Weighted FalsePositiveRate: 0.06698751708441418
Kappa statistic: 0.799021384124729
Training time: 157.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 47.79999999999972
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.864789862905627
Weighted AreaUnderROC: 0.9666623060182672
Root mean squared error: 0.2601019747478726
Relative absolute error: 25.98058494433853
Root relative squared error: 55.17596104326973
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7971781019703043
Weighted FMeasure: 0.8645666242567579
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9332083304468172
Mean absolute error: 0.11546926641928291
Coverage of cases: 97.56
Instances selection time: 48.0
Test time: 83.0
Accumulative iteration time: 6913.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06758756926821971
Kappa statistic: 0.7972225001913787
Training time: 170.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 48.053333333333086
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8631394702942454
Weighted AreaUnderROC: 0.967308342398041
Root mean squared error: 0.25968378854677093
Relative absolute error: 26.10904634853926
Root relative squared error: 55.08725035369044
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7947299449733668
Weighted FMeasure: 0.8630060131503129
Iteration time: 215.0
Weighted AreaUnderPRC: 0.9351772100506278
Mean absolute error: 0.11604020599350835
Coverage of cases: 97.84
Instances selection time: 47.0
Test time: 90.0
Accumulative iteration time: 7128.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06841281524403157
Kappa statistic: 0.7948171532859852
Training time: 168.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 47.67999999999978
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8655780356819652
Weighted AreaUnderROC: 0.9672617283825693
Root mean squared error: 0.25928195960327777
Relative absolute error: 25.822547154342914
Root relative squared error: 55.00200956244413
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.7983605028500634
Weighted FMeasure: 0.8654009971733915
Iteration time: 216.0
Weighted AreaUnderPRC: 0.934717850506648
Mean absolute error: 0.1147668762415246
Coverage of cases: 97.6
Instances selection time: 48.0
Test time: 85.0
Accumulative iteration time: 7344.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06719897829601806
Kappa statistic: 0.798418690619006
Training time: 168.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 47.67999999999978
Incorrectly Classified Instances: 13.36
Correctly Classified Instances: 86.64
Weighted Precision: 0.8664041949913842
Weighted AreaUnderROC: 0.9670581554542558
Root mean squared error: 0.25965730025056255
Relative absolute error: 25.923780223232225
Root relative squared error: 55.08163133752914
Weighted TruePositiveRate: 0.8664
Weighted MatthewsCorrelation: 0.7995500557651088
Weighted FMeasure: 0.8662783669645625
Iteration time: 219.0
Weighted AreaUnderPRC: 0.9344028439968568
Mean absolute error: 0.11521680099214375
Coverage of cases: 97.56
Instances selection time: 51.0
Test time: 84.0
Accumulative iteration time: 7563.0
Weighted Recall: 0.8664
Weighted FalsePositiveRate: 0.06682411990421887
Kappa statistic: 0.7996103561367949
Training time: 168.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 47.62666666666644
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8655833921515519
Weighted AreaUnderROC: 0.9671706544875542
Root mean squared error: 0.2595297044643003
Relative absolute error: 25.862359896565273
Root relative squared error: 55.05456418381407
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.7983551092528899
Weighted FMeasure: 0.8654314062790119
Iteration time: 221.0
Weighted AreaUnderPRC: 0.9346321784016355
Mean absolute error: 0.11494382176251287
Coverage of cases: 97.52
Instances selection time: 47.0
Test time: 83.0
Accumulative iteration time: 7784.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.0672058445862193
Kappa statistic: 0.7984159332046395
Training time: 174.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 47.99999999999974
Incorrectly Classified Instances: 13.36
Correctly Classified Instances: 86.64
Weighted Precision: 0.8663826977909186
Weighted AreaUnderROC: 0.9669596776746548
Root mean squared error: 0.2597848321383273
Relative absolute error: 26.08167198157087
Root relative squared error: 55.10868493632593
Weighted TruePositiveRate: 0.8664
Weighted MatthewsCorrelation: 0.7995492793987936
Weighted FMeasure: 0.8662595934568308
Iteration time: 222.0
Weighted AreaUnderPRC: 0.9338772969273458
Mean absolute error: 0.11591854214031552
Coverage of cases: 97.68
Instances selection time: 45.0
Test time: 82.0
Accumulative iteration time: 8006.0
Weighted Recall: 0.8664
Weighted FalsePositiveRate: 0.06681038732381642
Kappa statistic: 0.7996130972879611
Training time: 177.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 48.11999999999972
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8647678155819211
Weighted AreaUnderROC: 0.9669548190877085
Root mean squared error: 0.25957779898405103
Relative absolute error: 26.140336813406066
Root relative squared error: 55.06476657213017
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7971428831858629
Weighted FMeasure: 0.8646521116064425
Iteration time: 228.0
Weighted AreaUnderPRC: 0.9339362748461061
Mean absolute error: 0.11617927472624971
Coverage of cases: 97.76
Instances selection time: 45.0
Test time: 81.0
Accumulative iteration time: 8234.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06760816813882338
Kappa statistic: 0.7972141787846194
Training time: 183.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 48.33333333333304
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8655505088233213
Weighted AreaUnderROC: 0.9669098039782562
Root mean squared error: 0.2594773911443492
Relative absolute error: 26.207723230582452
Root relative squared error: 55.043466852828935
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.798326224184592
Weighted FMeasure: 0.8654572420883713
Iteration time: 243.0
Weighted AreaUnderPRC: 0.933671540706743
Mean absolute error: 0.11647876991370033
Coverage of cases: 97.84
Instances selection time: 45.0
Test time: 83.0
Accumulative iteration time: 8477.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06721957716662175
Kappa statistic: 0.7984122565348026
Training time: 198.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 48.61333333333306
Incorrectly Classified Instances: 13.72
Correctly Classified Instances: 86.28
Weighted Precision: 0.8627649938325187
Weighted AreaUnderROC: 0.9668155308256678
Root mean squared error: 0.25960440375553184
Relative absolute error: 26.430675063918162
Root relative squared error: 55.070410296427966
Weighted TruePositiveRate: 0.8628
Weighted MatthewsCorrelation: 0.7941203369503794
Weighted FMeasure: 0.8626961082723654
Iteration time: 221.0
Weighted AreaUnderPRC: 0.9336413283965751
Mean absolute error: 0.11746966695074794
Coverage of cases: 97.92
Instances selection time: 43.0
Test time: 137.0
Accumulative iteration time: 8698.0
Weighted Recall: 0.8628
Weighted FalsePositiveRate: 0.06863800903603791
Kappa statistic: 0.7942068817218753
Training time: 178.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 48.8266666666664
Incorrectly Classified Instances: 13.76
Correctly Classified Instances: 86.24
Weighted Precision: 0.8623600349283098
Weighted AreaUnderROC: 0.9671403971317699
Root mean squared error: 0.2588066466217973
Relative absolute error: 26.439541425169484
Root relative squared error: 54.901180452726884
Weighted TruePositiveRate: 0.8624
Weighted MatthewsCorrelation: 0.7935291968802973
Weighted FMeasure: 0.8622745099733722
Iteration time: 246.0
Weighted AreaUnderPRC: 0.9344791041283097
Mean absolute error: 0.1175090730007538
Coverage of cases: 98.04
Instances selection time: 43.0
Test time: 78.0
Accumulative iteration time: 8944.0
Weighted Recall: 0.8624
Weighted FalsePositiveRate: 0.06882200508683689
Kappa statistic: 0.7936106662007708
Training time: 203.0
		
Time end:Fri Dec 01 13.47.35 EET 2017