Fri Dec 01 13.58.12 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.58.12 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 51.61333333333325
Incorrectly Classified Instances: 16.48
Correctly Classified Instances: 83.52
Weighted Precision: 0.8352376235431573
Weighted AreaUnderROC: 0.9567024905997696
Root mean squared error: 0.28142598911164324
Relative absolute error: 31.23687823106433
Root relative squared error: 59.69946759089218
Weighted TruePositiveRate: 0.8352
Weighted MatthewsCorrelation: 0.7527199923175327
Weighted FMeasure: 0.8350557646067682
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9156200258083677
Mean absolute error: 0.1388305699158421
Coverage of cases: 98.32
Instances selection time: 7.0
Test time: 65.0
Accumulative iteration time: 189.0
Weighted Recall: 0.8352
Weighted FalsePositiveRate: 0.08250419936695612
Kappa statistic: 0.7528042319915482
Training time: 182.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 51.94666666666657
Incorrectly Classified Instances: 15.8
Correctly Classified Instances: 84.2
Weighted Precision: 0.8421617964795353
Weighted AreaUnderROC: 0.958070677595266
Root mean squared error: 0.2778273049438862
Relative absolute error: 31.07873291156852
Root relative squared error: 58.93607139738128
Weighted TruePositiveRate: 0.842
Weighted MatthewsCorrelation: 0.7630435584831531
Weighted FMeasure: 0.841765616478199
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9178516322273453
Mean absolute error: 0.13812770182919407
Coverage of cases: 98.52
Instances selection time: 8.0
Test time: 64.0
Accumulative iteration time: 257.0
Weighted Recall: 0.842
Weighted FalsePositiveRate: 0.07904847033126677
Kappa statistic: 0.7630163423930284
Training time: 60.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 51.760000000000026
Incorrectly Classified Instances: 16.08
Correctly Classified Instances: 83.92
Weighted Precision: 0.8393946472891932
Weighted AreaUnderROC: 0.959258174518168
Root mean squared error: 0.27631875141507317
Relative absolute error: 30.912695320682438
Root relative squared error: 58.61605886837931
Weighted TruePositiveRate: 0.8392
Weighted MatthewsCorrelation: 0.7588537079679614
Weighted FMeasure: 0.8389956830611538
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9201114979711681
Mean absolute error: 0.1373897569808115
Coverage of cases: 98.6
Instances selection time: 8.0
Test time: 63.0
Accumulative iteration time: 329.0
Weighted Recall: 0.8392
Weighted FalsePositiveRate: 0.0804669026277955
Kappa statistic: 0.7588085285304311
Training time: 64.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 51.88000000000004
Incorrectly Classified Instances: 15.44
Correctly Classified Instances: 84.56
Weighted Precision: 0.8456264236576432
Weighted AreaUnderROC: 0.9587940695812848
Root mean squared error: 0.2776146131058009
Relative absolute error: 31.041234526045837
Root relative squared error: 58.89095264507734
Weighted TruePositiveRate: 0.8456
Weighted MatthewsCorrelation: 0.7683526942155751
Weighted FMeasure: 0.8454319088914428
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9195479779153182
Mean absolute error: 0.13796104233798215
Coverage of cases: 98.84
Instances selection time: 7.0
Test time: 63.0
Accumulative iteration time: 404.0
Weighted Recall: 0.8456
Weighted FalsePositiveRate: 0.07725196305835964
Kappa statistic: 0.7684117461562349
Training time: 68.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 51.54666666666662
Incorrectly Classified Instances: 16.44
Correctly Classified Instances: 83.56
Weighted Precision: 0.8355374705728191
Weighted AreaUnderROC: 0.9579893753166135
Root mean squared error: 0.280265897743998
Relative absolute error: 31.174104127524927
Root relative squared error: 59.45337504903482
Weighted TruePositiveRate: 0.8356
Weighted MatthewsCorrelation: 0.7533278660962587
Weighted FMeasure: 0.8353346737523681
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9180932802475082
Mean absolute error: 0.13855157390011144
Coverage of cases: 98.76
Instances selection time: 12.0
Test time: 63.0
Accumulative iteration time: 481.0
Weighted Recall: 0.8356
Weighted FalsePositiveRate: 0.08224832639224093
Kappa statistic: 0.7534179511731545
Training time: 65.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 51.43999999999995
Incorrectly Classified Instances: 16.16
Correctly Classified Instances: 83.84
Weighted Precision: 0.8384133558969428
Weighted AreaUnderROC: 0.958421030443433
Root mean squared error: 0.2794644844356606
Relative absolute error: 31.124276758962942
Root relative squared error: 59.28336961357725
Weighted TruePositiveRate: 0.8384
Weighted MatthewsCorrelation: 0.7575764196796432
Weighted FMeasure: 0.8381559355198415
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9191432970514998
Mean absolute error: 0.13833011892872485
Coverage of cases: 98.6
Instances selection time: 9.0
Test time: 63.0
Accumulative iteration time: 561.0
Weighted Recall: 0.8384
Weighted FalsePositiveRate: 0.08084434080829053
Kappa statistic: 0.7576138063175922
Training time: 71.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 51.66666666666657
Incorrectly Classified Instances: 16.44
Correctly Classified Instances: 83.56
Weighted Precision: 0.8356706168274787
Weighted AreaUnderROC: 0.957665797291153
Root mean squared error: 0.28086103327413325
Relative absolute error: 31.46075814156366
Root relative squared error: 59.57962235975992
Weighted TruePositiveRate: 0.8356
Weighted MatthewsCorrelation: 0.7534353866134895
Weighted FMeasure: 0.8352774517940211
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9179479341066471
Mean absolute error: 0.13982559174028358
Coverage of cases: 98.64
Instances selection time: 8.0
Test time: 62.0
Accumulative iteration time: 642.0
Weighted Recall: 0.8356
Weighted FalsePositiveRate: 0.0822157824072372
Kappa statistic: 0.7534251703586098
Training time: 73.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 51.75999999999993
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8366995089833723
Weighted AreaUnderROC: 0.9581056736380764
Root mean squared error: 0.2802288035806821
Relative absolute error: 31.35674958789691
Root relative squared error: 59.44550618870787
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.7551151121447579
Weighted FMeasure: 0.8365059267447983
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9187766520280854
Mean absolute error: 0.1393633315017647
Coverage of cases: 98.8
Instances selection time: 7.0
Test time: 63.0
Accumulative iteration time: 726.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08164033586574526
Kappa statistic: 0.7552214621821959
Training time: 77.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 51.33333333333332
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8367643383170005
Weighted AreaUnderROC: 0.9581907748218491
Root mean squared error: 0.2797620815694522
Relative absolute error: 31.122762618317
Root relative squared error: 59.346499498986965
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.755179087806483
Weighted FMeasure: 0.8364634589067326
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9190352510263939
Mean absolute error: 0.13832338941474287
Coverage of cases: 98.52
Instances selection time: 7.0
Test time: 63.0
Accumulative iteration time: 816.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08161287155916547
Kappa statistic: 0.7552267488208888
Training time: 83.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 50.786666666666605
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8369222847969829
Weighted AreaUnderROC: 0.958162916842697
Root mean squared error: 0.2802399694172337
Relative absolute error: 30.78935724398705
Root relative squared error: 59.44787482033085
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.7553333261438542
Weighted FMeasure: 0.8362445631364591
Iteration time: 83.0
Weighted AreaUnderPRC: 0.919111283123367
Mean absolute error: 0.1368415877510542
Coverage of cases: 98.44
Instances selection time: 7.0
Test time: 65.0
Accumulative iteration time: 899.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08155865750529424
Kappa statistic: 0.7552490676669141
Training time: 76.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 51.013333333333314
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8406960300612111
Weighted AreaUnderROC: 0.9604460621320206
Root mean squared error: 0.2754946402849798
Relative absolute error: 30.303890023534798
Root relative squared error: 58.4412384978172
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7611357242037978
Weighted FMeasure: 0.8404717171531011
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9235193435814731
Mean absolute error: 0.13468395566015529
Coverage of cases: 98.64
Instances selection time: 6.0
Test time: 64.0
Accumulative iteration time: 986.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.0796045460085665
Kappa statistic: 0.7612295311315896
Training time: 81.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 51.559999999999945
Incorrectly Classified Instances: 15.68
Correctly Classified Instances: 84.32
Weighted Precision: 0.843247221782312
Weighted AreaUnderROC: 0.9619417267205794
Root mean squared error: 0.27198419295697684
Relative absolute error: 30.15693446350758
Root relative squared error: 57.69656016462849
Weighted TruePositiveRate: 0.8432
Weighted MatthewsCorrelation: 0.764815439138039
Weighted FMeasure: 0.8429171323269271
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9262246582881355
Mean absolute error: 0.1340308198378121
Coverage of cases: 98.72
Instances selection time: 6.0
Test time: 63.0
Accumulative iteration time: 1078.0
Weighted Recall: 0.8432
Weighted FalsePositiveRate: 0.07840944270174925
Kappa statistic: 0.7648234423992616
Training time: 86.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 51.66666666666661
Incorrectly Classified Instances: 16.2
Correctly Classified Instances: 83.8
Weighted Precision: 0.8381980663271071
Weighted AreaUnderROC: 0.9617968644564704
Root mean squared error: 0.27220667645006363
Relative absolute error: 30.31710232778461
Root relative squared error: 57.74375604062761
Weighted TruePositiveRate: 0.838
Weighted MatthewsCorrelation: 0.7570582560314376
Weighted FMeasure: 0.8378089258949601
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9259499879446863
Mean absolute error: 0.1347426770123767
Coverage of cases: 98.72
Instances selection time: 6.0
Test time: 63.0
Accumulative iteration time: 1173.0
Weighted Recall: 0.838
Weighted FalsePositiveRate: 0.08105322308542366
Kappa statistic: 0.7570110414182779
Training time: 89.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 51.35999999999997
Incorrectly Classified Instances: 15.96
Correctly Classified Instances: 84.04
Weighted Precision: 0.8403750673640783
Weighted AreaUnderROC: 0.9620117675832096
Root mean squared error: 0.27194033830998565
Relative absolute error: 30.05037455476165
Root relative squared error: 57.68725718914628
Weighted TruePositiveRate: 0.8404
Weighted MatthewsCorrelation: 0.7605260411042881
Weighted FMeasure: 0.8402247969068681
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9262645106354261
Mean absolute error: 0.13355722024338573
Coverage of cases: 98.6
Instances selection time: 8.0
Test time: 63.0
Accumulative iteration time: 1275.0
Weighted Recall: 0.8404
Weighted FalsePositiveRate: 0.07986184810185852
Kappa statistic: 0.7606088095958069
Training time: 94.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 51.50666666666657
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.8423442951480073
Weighted AreaUnderROC: 0.9610192544703469
Root mean squared error: 0.27363030346019457
Relative absolute error: 30.396070680934127
Root relative squared error: 58.04575293445078
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.7635083295318065
Weighted FMeasure: 0.842179432238483
Iteration time: 106.0
Weighted AreaUnderPRC: 0.9243823416372532
Mean absolute error: 0.13509364747081898
Coverage of cases: 98.64
Instances selection time: 8.0
Test time: 62.0
Accumulative iteration time: 1381.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.07886705236071345
Kappa statistic: 0.763613010739889
Training time: 98.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 51.573333333333274
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.8422691057233412
Weighted AreaUnderROC: 0.9618224498135894
Root mean squared error: 0.2728933444849799
Relative absolute error: 30.312465663955216
Root relative squared error: 57.889420327801616
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.7634777049519621
Weighted FMeasure: 0.8421450738903085
Iteration time: 112.0
Weighted AreaUnderPRC: 0.9260138484884168
Mean absolute error: 0.13472206961757938
Coverage of cases: 98.72
Instances selection time: 6.0
Test time: 62.0
Accumulative iteration time: 1493.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.07887141747984895
Kappa statistic: 0.7636118761393428
Training time: 106.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 51.34666666666668
Incorrectly Classified Instances: 15.68
Correctly Classified Instances: 84.32
Weighted Precision: 0.8430891674635739
Weighted AreaUnderROC: 0.9616581264942541
Root mean squared error: 0.27319616954356485
Relative absolute error: 30.18567367941979
Root relative squared error: 57.953659223533194
Weighted TruePositiveRate: 0.8432
Weighted MatthewsCorrelation: 0.764690025844919
Weighted FMeasure: 0.8429571875629451
Iteration time: 111.0
Weighted AreaUnderPRC: 0.9258524223818644
Mean absolute error: 0.1341585496863108
Coverage of cases: 98.64
Instances selection time: 8.0
Test time: 62.0
Accumulative iteration time: 1604.0
Weighted Recall: 0.8432
Weighted FalsePositiveRate: 0.0784557188400429
Kappa statistic: 0.764815428107916
Training time: 103.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 51.49333333333322
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.8422574126484944
Weighted AreaUnderROC: 0.9618356488776552
Root mean squared error: 0.27244562599777916
Relative absolute error: 30.159120093918418
Root relative squared error: 57.79444489429294
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.7634445997606317
Weighted FMeasure: 0.842211116578475
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9263323799703268
Mean absolute error: 0.13404053375074915
Coverage of cases: 98.68
Instances selection time: 6.0
Test time: 62.0
Accumulative iteration time: 1714.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.07888443507385046
Kappa statistic: 0.7636082453444024
Training time: 104.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 51.493333333333254
Incorrectly Classified Instances: 15.56
Correctly Classified Instances: 84.44
Weighted Precision: 0.8443041545229433
Weighted AreaUnderROC: 0.9620607438649633
Root mean squared error: 0.27203909265625004
Relative absolute error: 30.199167000218676
Root relative squared error: 57.70820614952084
Weighted TruePositiveRate: 0.8444
Weighted MatthewsCorrelation: 0.7664845323528593
Weighted FMeasure: 0.8441993342782982
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9268445500137417
Mean absolute error: 0.1342185200009725
Coverage of cases: 98.6
Instances selection time: 7.0
Test time: 62.0
Accumulative iteration time: 1834.0
Weighted Recall: 0.8444
Weighted FalsePositiveRate: 0.07786288958541397
Kappa statistic: 0.7666122855292898
Training time: 113.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 50.94666666666665
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8480195779054173
Weighted AreaUnderROC: 0.9629607154865047
Root mean squared error: 0.26970323324866663
Relative absolute error: 29.79815331691105
Root relative squared error: 57.21269554142066
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7719778949655092
Weighted FMeasure: 0.847735072236848
Iteration time: 112.0
Weighted AreaUnderPRC: 0.9286947716197814
Mean absolute error: 0.13243623696404974
Coverage of cases: 98.6
Instances selection time: 5.0
Test time: 62.0
Accumulative iteration time: 1946.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07604463448023434
Kappa statistic: 0.7720161594946149
Training time: 107.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 51.47999999999995
Incorrectly Classified Instances: 15.44
Correctly Classified Instances: 84.56
Weighted Precision: 0.8457361989361399
Weighted AreaUnderROC: 0.9627238658544353
Root mean squared error: 0.26926288670444815
Relative absolute error: 30.093161998334807
Root relative squared error: 57.119283933173975
Weighted TruePositiveRate: 0.8456
Weighted MatthewsCorrelation: 0.7684576661014438
Weighted FMeasure: 0.8453506611122505
Iteration time: 111.0
Weighted AreaUnderPRC: 0.9285692058583832
Mean absolute error: 0.13374738665926644
Coverage of cases: 98.56
Instances selection time: 5.0
Test time: 62.0
Accumulative iteration time: 2057.0
Weighted Recall: 0.8456
Weighted FalsePositiveRate: 0.07722521331106826
Kappa statistic: 0.7684159700347063
Training time: 106.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 51.73333333333327
Incorrectly Classified Instances: 15.32
Correctly Classified Instances: 84.68
Weighted Precision: 0.8468881633133651
Weighted AreaUnderROC: 0.9629175650938975
Root mean squared error: 0.26914994200662984
Relative absolute error: 30.263112127274386
Root relative squared error: 57.095324744656054
Weighted TruePositiveRate: 0.8468
Weighted MatthewsCorrelation: 0.7702145685451638
Weighted FMeasure: 0.8465721518133074
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9288261352196944
Mean absolute error: 0.13450272056566456
Coverage of cases: 98.72
Instances selection time: 5.0
Test time: 61.0
Accumulative iteration time: 2177.0
Weighted Recall: 0.8468
Weighted FalsePositiveRate: 0.07664540165044081
Kappa statistic: 0.7702117651576239
Training time: 115.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 51.3466666666666
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8460307704263745
Weighted AreaUnderROC: 0.9633981285655947
Root mean squared error: 0.26872166418619886
Relative absolute error: 29.947706691777977
Root relative squared error: 57.00447329933849
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7689633921398449
Weighted FMeasure: 0.8457950996147138
Iteration time: 117.0
Weighted AreaUnderPRC: 0.9298024796871992
Mean absolute error: 0.13310091863012496
Coverage of cases: 98.64
Instances selection time: 5.0
Test time: 63.0
Accumulative iteration time: 2294.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07706760908724763
Kappa statistic: 0.7690078352542283
Training time: 112.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 51.306666666666636
Incorrectly Classified Instances: 15.36
Correctly Classified Instances: 84.64
Weighted Precision: 0.8464276260254338
Weighted AreaUnderROC: 0.9641370162755902
Root mean squared error: 0.26750835846614407
Relative absolute error: 29.590965177999458
Root relative squared error: 56.74709228864754
Weighted TruePositiveRate: 0.8464
Weighted MatthewsCorrelation: 0.7695298686120796
Weighted FMeasure: 0.8462390645545531
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9311348738885161
Mean absolute error: 0.13151540079110932
Coverage of cases: 98.56
Instances selection time: 5.0
Test time: 63.0
Accumulative iteration time: 2412.0
Weighted Recall: 0.8464
Weighted FalsePositiveRate: 0.07689770182871396
Kappa statistic: 0.7696027279037017
Training time: 113.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 51.226666666666596
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.8447500953722104
Weighted AreaUnderROC: 0.9640178517968576
Root mean squared error: 0.2677186770546259
Relative absolute error: 29.6131085348966
Root relative squared error: 56.79170759868506
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7671151094072209
Weighted FMeasure: 0.8445887359480928
Iteration time: 121.0
Weighted AreaUnderPRC: 0.9307579547487418
Mean absolute error: 0.13161381571065217
Coverage of cases: 98.56
Instances selection time: 5.0
Test time: 64.0
Accumulative iteration time: 2533.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.0776662325795889
Kappa statistic: 0.7672105779513378
Training time: 116.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 51.39999999999992
Incorrectly Classified Instances: 15.36
Correctly Classified Instances: 84.64
Weighted Precision: 0.8463712376602646
Weighted AreaUnderROC: 0.9645335347526488
Root mean squared error: 0.26637229432954107
Relative absolute error: 29.58178376803587
Root relative squared error: 56.5060966921911
Weighted TruePositiveRate: 0.8464
Weighted MatthewsCorrelation: 0.7695069671778952
Weighted FMeasure: 0.8462562296311604
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9319037086850865
Mean absolute error: 0.13147459452460447
Coverage of cases: 98.68
Instances selection time: 4.0
Test time: 62.0
Accumulative iteration time: 2657.0
Weighted Recall: 0.8464
Weighted FalsePositiveRate: 0.0768818259975588
Kappa statistic: 0.7696032808492808
Training time: 120.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 51.29333333333327
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8495635048297808
Weighted AreaUnderROC: 0.9646883940841444
Root mean squared error: 0.266399200544573
Relative absolute error: 29.42576736072285
Root relative squared error: 56.511804362322636
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7742995249551754
Weighted FMeasure: 0.8494698268923466
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9320518827809092
Mean absolute error: 0.13078118826987994
Coverage of cases: 98.84
Instances selection time: 4.0
Test time: 63.0
Accumulative iteration time: 2782.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07528554852691884
Kappa statistic: 0.7744012633529253
Training time: 121.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 51.12000000000001
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8499707868407836
Weighted AreaUnderROC: 0.9643204808897954
Root mean squared error: 0.26722833767908444
Relative absolute error: 29.36410307840009
Root relative squared error: 56.68769090942663
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7748964669075082
Weighted FMeasure: 0.8498974912891987
Iteration time: 115.0
Weighted AreaUnderPRC: 0.9309206571887929
Mean absolute error: 0.1305071247928899
Coverage of cases: 98.72
Instances selection time: 5.0
Test time: 63.0
Accumulative iteration time: 2897.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07509397119951768
Kappa statistic: 0.7749972639667299
Training time: 110.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 50.98666666666668
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8476560791740833
Weighted AreaUnderROC: 0.964167714351249
Root mean squared error: 0.2675657394324351
Relative absolute error: 29.33764155650522
Root relative squared error: 56.75926462976017
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7713233416952224
Weighted FMeasure: 0.8475231075185494
Iteration time: 134.0
Weighted AreaUnderPRC: 0.9306943100142661
Mean absolute error: 0.1303895180289127
Coverage of cases: 98.64
Instances selection time: 4.0
Test time: 64.0
Accumulative iteration time: 3031.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07632296984651045
Kappa statistic: 0.7713905264234152
Training time: 130.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 50.9733333333333
Incorrectly Classified Instances: 15.28
Correctly Classified Instances: 84.72
Weighted Precision: 0.847108922249798
Weighted AreaUnderROC: 0.9646556051036324
Root mean squared error: 0.26675421144968875
Relative absolute error: 29.21175351861864
Root relative squared error: 56.587113547843394
Weighted TruePositiveRate: 0.8472
Weighted MatthewsCorrelation: 0.7706623702257921
Weighted FMeasure: 0.8470623490450484
Iteration time: 129.0
Weighted AreaUnderPRC: 0.9318326493195674
Mean absolute error: 0.12983001563830568
Coverage of cases: 98.84
Instances selection time: 4.0
Test time: 63.0
Accumulative iteration time: 3160.0
Weighted Recall: 0.8472
Weighted FalsePositiveRate: 0.0764914479864673
Kappa statistic: 0.7708027136958698
Training time: 125.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 51.15999999999999
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.8447941050511175
Weighted AreaUnderROC: 0.9644923802729214
Root mean squared error: 0.2675516145251135
Relative absolute error: 29.46625016117906
Root relative squared error: 56.756268284434945
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7671388494795275
Weighted FMeasure: 0.8446678751601115
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9314701197935883
Mean absolute error: 0.13096111182746312
Coverage of cases: 98.88
Instances selection time: 4.0
Test time: 63.0
Accumulative iteration time: 3299.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.07765829466401132
Kappa statistic: 0.767203315024794
Training time: 135.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 51.26666666666669
Incorrectly Classified Instances: 15.48
Correctly Classified Instances: 84.52
Weighted Precision: 0.8451375071079219
Weighted AreaUnderROC: 0.9646765621594934
Root mean squared error: 0.2671391064487806
Relative absolute error: 29.569568179736844
Root relative squared error: 56.66876210701418
Weighted TruePositiveRate: 0.8452
Weighted MatthewsCorrelation: 0.7676944365415624
Weighted FMeasure: 0.8450961142762881
Iteration time: 146.0
Weighted AreaUnderPRC: 0.9317505438456654
Mean absolute error: 0.13142030302105326
Coverage of cases: 98.96
Instances selection time: 4.0
Test time: 63.0
Accumulative iteration time: 3445.0
Weighted Recall: 0.8452
Weighted FalsePositiveRate: 0.07746528821803333
Kappa statistic: 0.7678019690393024
Training time: 142.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 51.21333333333333
Incorrectly Classified Instances: 15.56
Correctly Classified Instances: 84.44
Weighted Precision: 0.8443761478725219
Weighted AreaUnderROC: 0.9645311539264668
Root mean squared error: 0.26730928051012326
Relative absolute error: 29.5323735077234
Root relative squared error: 56.70486147684142
Weighted TruePositiveRate: 0.8444
Weighted MatthewsCorrelation: 0.7665223203495645
Weighted FMeasure: 0.8443069081928328
Iteration time: 156.0
Weighted AreaUnderPRC: 0.9313837967445812
Mean absolute error: 0.13125499336766017
Coverage of cases: 98.84
Instances selection time: 8.0
Test time: 63.0
Accumulative iteration time: 3601.0
Weighted Recall: 0.8444
Weighted FalsePositiveRate: 0.07786146046683716
Kappa statistic: 0.7665995145269903
Training time: 148.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 51.28
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8475657195579428
Weighted AreaUnderROC: 0.9651159397564757
Root mean squared error: 0.26572729923285
Relative absolute error: 29.389210365584347
Root relative squared error: 56.3692725701804
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7713248045298655
Weighted FMeasure: 0.8474418650757151
Iteration time: 140.0
Weighted AreaUnderPRC: 0.9324496009940569
Mean absolute error: 0.13061871273593104
Coverage of cases: 98.96
Instances selection time: 4.0
Test time: 62.0
Accumulative iteration time: 3741.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07626010331777326
Kappa statistic: 0.7714053399712583
Training time: 136.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 51.12000000000002
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8475227933502248
Weighted AreaUnderROC: 0.9654557084446309
Root mean squared error: 0.2652963327347601
Relative absolute error: 29.30190794686895
Root relative squared error: 56.277850770201326
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7712752227412075
Weighted FMeasure: 0.8474787276191881
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9331502119232644
Mean absolute error: 0.13023070198608483
Coverage of cases: 98.96
Instances selection time: 4.0
Test time: 63.0
Accumulative iteration time: 3890.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07628613850577624
Kappa statistic: 0.7714006217903088
Training time: 145.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 51.06666666666671
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8491225040205914
Weighted AreaUnderROC: 0.9652199101845116
Root mean squared error: 0.2655554721039876
Relative absolute error: 29.248185949325528
Root relative squared error: 56.332822531777275
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.773675883409743
Weighted FMeasure: 0.8490795075975303
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9326257034033412
Mean absolute error: 0.1299919375525585
Coverage of cases: 98.84
Instances selection time: 4.0
Test time: 64.0
Accumulative iteration time: 4047.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07548799977045625
Kappa statistic: 0.7737995295030213
Training time: 153.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 51.093333333333334
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8495439250593643
Weighted AreaUnderROC: 0.9647370459901969
Root mean squared error: 0.266467894061497
Relative absolute error: 29.307413371793817
Root relative squared error: 56.52637645781479
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7743040457835028
Weighted FMeasure: 0.8494434811532771
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9317241704196837
Mean absolute error: 0.13025517054130648
Coverage of cases: 98.76
Instances selection time: 3.0
Test time: 65.0
Accumulative iteration time: 4196.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07526459301733976
Kappa statistic: 0.7744065692807024
Training time: 146.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 51.32000000000003
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8495006672928394
Weighted AreaUnderROC: 0.9649067521387408
Root mean squared error: 0.26598099911961387
Relative absolute error: 29.569870791001158
Root relative squared error: 56.4230904432755
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7742563820438787
Weighted FMeasure: 0.8495120071713743
Iteration time: 144.0
Weighted AreaUnderPRC: 0.9319461274954752
Mean absolute error: 0.13142164796000577
Coverage of cases: 98.84
Instances selection time: 3.0
Test time: 62.0
Accumulative iteration time: 4340.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.0752877699681891
Kappa statistic: 0.7743982312821335
Training time: 141.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 51.52000000000002
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8491778444405566
Weighted AreaUnderROC: 0.9653693959133401
Root mean squared error: 0.2651441823589321
Relative absolute error: 29.52621353784529
Root relative squared error: 56.2455748014489
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7736977380905761
Weighted FMeasure: 0.849114734900729
Iteration time: 152.0
Weighted AreaUnderPRC: 0.933011469924701
Mean absolute error: 0.13122761572375746
Coverage of cases: 98.84
Instances selection time: 3.0
Test time: 62.0
Accumulative iteration time: 4492.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07549593768603383
Kappa statistic: 0.7737952949421347
Training time: 149.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 51.48000000000001
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8499178486129413
Weighted AreaUnderROC: 0.9650015000063143
Root mean squared error: 0.26545103280515986
Relative absolute error: 29.61615001345126
Root relative squared error: 56.31066761085023
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7748901631224506
Weighted FMeasure: 0.8498552989672058
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9323204619846452
Mean absolute error: 0.13162733339311733
Coverage of cases: 98.88
Instances selection time: 3.0
Test time: 62.0
Accumulative iteration time: 4652.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07505856897736028
Kappa statistic: 0.7750069837832232
Training time: 157.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 51.49333333333334
Incorrectly Classified Instances: 14.68
Correctly Classified Instances: 85.32
Weighted Precision: 0.8531374725163612
Weighted AreaUnderROC: 0.9654724944601523
Root mean squared error: 0.2643075761167098
Relative absolute error: 29.499414514884556
Root relative squared error: 56.06810381733139
Weighted TruePositiveRate: 0.8532
Weighted MatthewsCorrelation: 0.7796670610704514
Weighted FMeasure: 0.8531232839733773
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9330909918561144
Mean absolute error: 0.13110850895504308
Coverage of cases: 98.84
Instances selection time: 3.0
Test time: 61.0
Accumulative iteration time: 4809.0
Weighted Recall: 0.8532
Weighted FalsePositiveRate: 0.07349697916958928
Kappa statistic: 0.7797970052392712
Training time: 154.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 51.55999999999997
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8488187146648206
Weighted AreaUnderROC: 0.9657817267842416
Root mean squared error: 0.264055678739494
Relative absolute error: 29.497071853712804
Root relative squared error: 56.01466831425367
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7731183092076295
Weighted FMeasure: 0.8486889461994833
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9339942951601384
Mean absolute error: 0.13109809712761308
Coverage of cases: 98.84
Instances selection time: 3.0
Test time: 61.0
Accumulative iteration time: 4969.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07570267628530172
Kappa statistic: 0.7731968066110371
Training time: 157.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 51.453333333333276
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8499117844116784
Weighted AreaUnderROC: 0.965758564480216
Root mean squared error: 0.26419785078730607
Relative absolute error: 29.478373092203864
Root relative squared error: 56.0448275599846
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7748663734765686
Weighted FMeasure: 0.8498773785377923
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9339343617724917
Mean absolute error: 0.13101499152090668
Coverage of cases: 98.92
Instances selection time: 3.0
Test time: 63.0
Accumulative iteration time: 5135.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07508603328394009
Kappa statistic: 0.7750017999856003
Training time: 163.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 51.173333333333346
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8480202828443624
Weighted AreaUnderROC: 0.9652434252672354
Root mean squared error: 0.2653196182942796
Relative absolute error: 29.43645439180673
Root relative squared error: 56.282790383313305
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7719185358212844
Weighted FMeasure: 0.8479078533482746
Iteration time: 370.0
Weighted AreaUnderPRC: 0.9328872329740607
Mean absolute error: 0.1308286861858083
Coverage of cases: 98.76
Instances selection time: 3.0
Test time: 66.0
Accumulative iteration time: 5505.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07611043700953021
Kappa statistic: 0.771991645773901
Training time: 367.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 51.30666666666668
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8479495397520402
Weighted AreaUnderROC: 0.9650509609604137
Root mean squared error: 0.2654731518995376
Relative absolute error: 29.49290274655661
Root relative squared error: 56.31535977933869
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7719002489958379
Weighted FMeasure: 0.8478636183835517
Iteration time: 182.0
Weighted AreaUnderPRC: 0.9325121126285112
Mean absolute error: 0.13107956776247442
Coverage of cases: 98.8
Instances selection time: 3.0
Test time: 63.0
Accumulative iteration time: 5687.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07608225814366198
Kappa statistic: 0.772000182399854
Training time: 179.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 51.34666666666668
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8499137503695114
Weighted AreaUnderROC: 0.9654016426347676
Root mean squared error: 0.26487575557498017
Relative absolute error: 29.469241391894833
Root relative squared error: 56.18863288169355
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7749075438406171
Weighted FMeasure: 0.8498177729015737
Iteration time: 176.0
Weighted AreaUnderPRC: 0.9332212804004706
Mean absolute error: 0.13097440618619988
Coverage of cases: 98.88
Instances selection time: 2.0
Test time: 63.0
Accumulative iteration time: 5863.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07504412226478197
Kappa statistic: 0.7750097915738706
Training time: 174.0
		
Time end:Fri Dec 01 13.58.25 EET 2017