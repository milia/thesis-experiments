Fri Dec 01 13.59.02 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.59.02 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 49.14666666666679
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8466816719684007
Weighted AreaUnderROC: 0.9619827463859824
Root mean squared error: 0.27061969299008126
Relative absolute error: 28.111819312436083
Root relative squared error: 57.40710601077228
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7694991397682592
Weighted FMeasure: 0.8453601288468297
Iteration time: 156.0
Weighted AreaUnderPRC: 0.9237973654233942
Mean absolute error: 0.12494141916638318
Coverage of cases: 97.56
Instances selection time: 7.0
Test time: 72.0
Accumulative iteration time: 156.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07686708235428467
Kappa statistic: 0.7690656407822638
Training time: 149.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 48.86666666666685
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8495279544568195
Weighted AreaUnderROC: 0.9626160552656856
Root mean squared error: 0.2690927763294911
Relative absolute error: 27.895953196405646
Root relative squared error: 57.08319807326928
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.773731619415358
Weighted FMeasure: 0.8481692192379257
Iteration time: 68.0
Weighted AreaUnderPRC: 0.925018248451328
Mean absolute error: 0.1239820142062479
Coverage of cases: 97.8
Instances selection time: 7.0
Test time: 75.0
Accumulative iteration time: 224.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07546238306527095
Kappa statistic: 0.7732634136884596
Training time: 61.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 49.97333333333346
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8516855361706079
Weighted AreaUnderROC: 0.9626136695550862
Root mean squared error: 0.2674634918649661
Relative absolute error: 28.40778867939389
Root relative squared error: 56.73757464526502
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.7765008763359816
Weighted FMeasure: 0.8496133297789731
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9250035574476189
Mean absolute error: 0.12625683857508455
Coverage of cases: 98.08
Instances selection time: 8.0
Test time: 68.0
Accumulative iteration time: 294.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07462340450905662
Kappa statistic: 0.7756739916905808
Training time: 62.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 50.200000000000095
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.8531702443911862
Weighted AreaUnderROC: 0.9638555021751685
Root mean squared error: 0.26536811961118745
Relative absolute error: 28.355647792522362
Root relative squared error: 56.29307906633792
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7791529754771421
Weighted FMeasure: 0.8517669810884949
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9273499622354475
Mean absolute error: 0.12602510130009997
Coverage of cases: 98.36
Instances selection time: 9.0
Test time: 66.0
Accumulative iteration time: 367.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07367595909425681
Kappa statistic: 0.7786598857813031
Training time: 64.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 49.600000000000094
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8544489747885671
Weighted AreaUnderROC: 0.9649882553190259
Root mean squared error: 0.26358616038557925
Relative absolute error: 27.755369086940465
Root relative squared error: 55.915068430670274
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7810117996448187
Weighted FMeasure: 0.8529226989484454
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9297477435642489
Mean absolute error: 0.12335719594195821
Coverage of cases: 98.08
Instances selection time: 8.0
Test time: 69.0
Accumulative iteration time: 448.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.0730759069104513
Kappa statistic: 0.780460399734825
Training time: 73.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 49.50666666666678
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.8519447611567074
Weighted AreaUnderROC: 0.9638813320024777
Root mean squared error: 0.2651482104615044
Relative absolute error: 27.91388390710492
Root relative squared error: 56.246429291042155
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.7769934683971458
Weighted FMeasure: 0.8501049962729876
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9273994600122669
Mean absolute error: 0.12406170625380022
Coverage of cases: 97.92
Instances selection time: 6.0
Test time: 71.0
Accumulative iteration time: 522.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.0744531410386601
Kappa statistic: 0.7762656347134005
Training time: 68.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 49.76000000000011
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8509066604688931
Weighted AreaUnderROC: 0.964026787927761
Root mean squared error: 0.266134763194997
Relative absolute error: 28.110901172972845
Root relative squared error: 56.45570872939739
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.775251122887977
Weighted FMeasure: 0.8490613698693474
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9279491619989704
Mean absolute error: 0.12493733854654657
Coverage of cases: 97.96
Instances selection time: 9.0
Test time: 67.0
Accumulative iteration time: 605.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07505319322246565
Kappa statistic: 0.7744599936416914
Training time: 74.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 49.78666666666675
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8507645925552013
Weighted AreaUnderROC: 0.9643752972561582
Root mean squared error: 0.2654764851569073
Relative absolute error: 28.02213679794092
Root relative squared error: 56.316066870005564
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7755393440257501
Weighted FMeasure: 0.8493765030874442
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9286969161397688
Mean absolute error: 0.12454283021307133
Coverage of cases: 98.12
Instances selection time: 6.0
Test time: 69.0
Accumulative iteration time: 691.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07489666233247155
Kappa statistic: 0.7750577831566626
Training time: 80.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 49.906666666666794
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8507775818687884
Weighted AreaUnderROC: 0.9641671574498165
Root mean squared error: 0.2658092901424689
Relative absolute error: 28.185414133324247
Root relative squared error: 56.38666546863655
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7755290693027775
Weighted FMeasure: 0.8494619925098359
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9279571730809616
Mean absolute error: 0.12526850725921945
Coverage of cases: 98.12
Instances selection time: 6.0
Test time: 68.0
Accumulative iteration time: 776.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.0748554645912642
Kappa statistic: 0.7750629104052177
Training time: 79.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 50.06666666666677
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8544651303799634
Weighted AreaUnderROC: 0.9646246927696518
Root mean squared error: 0.2647306030227643
Relative absolute error: 28.183276103202573
Root relative squared error: 56.15784137550004
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.780961386857163
Weighted FMeasure: 0.8531592953233763
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9286023630376198
Mean absolute error: 0.12525900490312314
Coverage of cases: 98.2
Instances selection time: 6.0
Test time: 68.0
Accumulative iteration time: 862.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07304844174964639
Kappa statistic: 0.7804583980661145
Training time: 80.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 50.04000000000009
Incorrectly Classified Instances: 14.84
Correctly Classified Instances: 85.16
Weighted Precision: 0.8523894300315653
Weighted AreaUnderROC: 0.96459339809811
Root mean squared error: 0.26439396524399456
Relative absolute error: 28.249607265535182
Root relative squared error: 56.08642971864854
Weighted TruePositiveRate: 0.8516
Weighted MatthewsCorrelation: 0.7779262088722079
Weighted FMeasure: 0.8511162879986934
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9281546807925112
Mean absolute error: 0.12555381006904584
Coverage of cases: 98.04
Instances selection time: 7.0
Test time: 68.0
Accumulative iteration time: 951.0
Weighted Recall: 0.8516
Weighted FalsePositiveRate: 0.07405768377625722
Kappa statistic: 0.7774591958539029
Training time: 82.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 49.82666666666678
Incorrectly Classified Instances: 15.16
Correctly Classified Instances: 84.84
Weighted Precision: 0.8494825637920469
Weighted AreaUnderROC: 0.9632580412570082
Root mean squared error: 0.2671163143152369
Relative absolute error: 28.614012280911393
Root relative squared error: 56.66392716535825
Weighted TruePositiveRate: 0.8484
Weighted MatthewsCorrelation: 0.7732884709980918
Weighted FMeasure: 0.8479421590920094
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9254555628322869
Mean absolute error: 0.12717338791516233
Coverage of cases: 97.8
Instances selection time: 6.0
Test time: 67.0
Accumulative iteration time: 1060.0
Weighted Recall: 0.8484
Weighted FalsePositiveRate: 0.07561891395526503
Kappa statistic: 0.7726677268308226
Training time: 103.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 50.42666666666683
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8502095362527874
Weighted AreaUnderROC: 0.963443340601964
Root mean squared error: 0.26635109301860516
Relative absolute error: 28.82031488378531
Root relative squared error: 56.50159921497125
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.774455473742067
Weighted FMeasure: 0.8486880704882689
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9260448861843965
Mean absolute error: 0.12809028837237976
Coverage of cases: 98.04
Instances selection time: 5.0
Test time: 66.0
Accumulative iteration time: 1161.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.0752371892732646
Kappa statistic: 0.7738663385709168
Training time: 96.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 50.38666666666685
Incorrectly Classified Instances: 15.28
Correctly Classified Instances: 84.72
Weighted Precision: 0.8479998309665557
Weighted AreaUnderROC: 0.9633236902190058
Root mean squared error: 0.2666689362859391
Relative absolute error: 28.781454285354837
Root relative squared error: 56.56902395387714
Weighted TruePositiveRate: 0.8472
Weighted MatthewsCorrelation: 0.7713251589907609
Weighted FMeasure: 0.8467166326995134
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9257447578878353
Mean absolute error: 0.12791757460157765
Coverage of cases: 98.08
Instances selection time: 8.0
Test time: 66.0
Accumulative iteration time: 1271.0
Weighted Recall: 0.8472
Weighted FalsePositiveRate: 0.07626016388027793
Kappa statistic: 0.7708619955784762
Training time: 102.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 50.413333333333526
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8569610949089554
Weighted AreaUnderROC: 0.9644956237157326
Root mean squared error: 0.2642828395195419
Relative absolute error: 28.47174326560501
Root relative squared error: 56.06285639265111
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7849895234109201
Weighted FMeasure: 0.8558724325761919
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9279426726875989
Mean absolute error: 0.1265410811804673
Coverage of cases: 98.12
Instances selection time: 8.0
Test time: 66.0
Accumulative iteration time: 1374.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07168494020184003
Kappa statistic: 0.78465629945707
Training time: 95.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 50.0800000000002
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.856245669005226
Weighted AreaUnderROC: 0.9647492044860726
Root mean squared error: 0.26401896306091116
Relative absolute error: 28.171601380074765
Root relative squared error: 56.00687974266314
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7838428719237909
Weighted FMeasure: 0.8550774551295784
Iteration time: 107.0
Weighted AreaUnderPRC: 0.9283531099274909
Mean absolute error: 0.12520711724477732
Coverage of cases: 98.12
Instances selection time: 8.0
Test time: 66.0
Accumulative iteration time: 1481.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07207353117404165
Kappa statistic: 0.783457600278326
Training time: 99.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 50.520000000000174
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8570798813073288
Weighted AreaUnderROC: 0.9649600227238069
Root mean squared error: 0.26336143720221117
Relative absolute error: 28.22112906566824
Root relative squared error: 55.86739744461545
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7850491570058024
Weighted FMeasure: 0.8559799364158064
Iteration time: 122.0
Weighted AreaUnderPRC: 0.9287361294300259
Mean absolute error: 0.12542724029185945
Coverage of cases: 98.12
Instances selection time: 8.0
Test time: 65.0
Accumulative iteration time: 1603.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07166434133123635
Kappa statistic: 0.7846533542849424
Training time: 114.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 49.90666666666682
Incorrectly Classified Instances: 14.72
Correctly Classified Instances: 85.28
Weighted Precision: 0.8536517736086265
Weighted AreaUnderROC: 0.9652711506673674
Root mean squared error: 0.2631555409274005
Relative absolute error: 28.027043741262794
Root relative squared error: 55.82372024897355
Weighted TruePositiveRate: 0.8528
Weighted MatthewsCorrelation: 0.7797812948164232
Weighted FMeasure: 0.8522455158191559
Iteration time: 116.0
Weighted AreaUnderPRC: 0.929451498528332
Mean absolute error: 0.12456463885005745
Coverage of cases: 98.0
Instances selection time: 5.0
Test time: 66.0
Accumulative iteration time: 1719.0
Weighted Recall: 0.8528
Weighted FalsePositiveRate: 0.07345763159245171
Kappa statistic: 0.7792607297880207
Training time: 111.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 50.18666666666683
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.854343642671908
Weighted AreaUnderROC: 0.964794475166074
Root mean squared error: 0.2635642300442298
Relative absolute error: 28.23185427186146
Root relative squared error: 55.91041630274569
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7809019848857449
Weighted FMeasure: 0.8531176250137987
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9285862307737786
Mean absolute error: 0.12547490787494042
Coverage of cases: 98.0
Instances selection time: 6.0
Test time: 66.0
Accumulative iteration time: 1838.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07306904062025006
Kappa statistic: 0.7804553954946089
Training time: 113.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 50.38666666666684
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8568792600629702
Weighted AreaUnderROC: 0.9644629354041101
Root mean squared error: 0.26375560025409395
Relative absolute error: 28.341612749696885
Root relative squared error: 55.9510120546793
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7849263389452462
Weighted FMeasure: 0.8559758737856807
Iteration time: 114.0
Weighted AreaUnderPRC: 0.9277741564315647
Mean absolute error: 0.12596272333198674
Coverage of cases: 98.08
Instances selection time: 5.0
Test time: 65.0
Accumulative iteration time: 1952.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07169180649204125
Kappa statistic: 0.7846494272634545
Training time: 109.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 50.320000000000135
Incorrectly Classified Instances: 14.52
Correctly Classified Instances: 85.48
Weighted Precision: 0.855291593341682
Weighted AreaUnderROC: 0.9644453715783289
Root mean squared error: 0.2640521189104265
Relative absolute error: 28.229097534117752
Root relative squared error: 56.013913160471624
Weighted TruePositiveRate: 0.8548
Weighted MatthewsCorrelation: 0.7825401487193927
Weighted FMeasure: 0.8543629153321194
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9279614674875931
Mean absolute error: 0.1254626557071906
Coverage of cases: 98.04
Instances selection time: 5.0
Test time: 67.0
Accumulative iteration time: 2065.0
Weighted Recall: 0.8548
Weighted FalsePositiveRate: 0.07250331988745067
Kappa statistic: 0.7822469998075615
Training time: 108.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 50.16000000000016
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8543085225663848
Weighted AreaUnderROC: 0.9651505881519633
Root mean squared error: 0.2629177870703444
Relative absolute error: 28.082254582783804
Root relative squared error: 55.77328503960027
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7808941272774205
Weighted FMeasure: 0.8530472224192852
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9295617303095827
Mean absolute error: 0.12481002036792861
Coverage of cases: 98.04
Instances selection time: 5.0
Test time: 66.0
Accumulative iteration time: 2183.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.0730759069104513
Kappa statistic: 0.7804573972180712
Training time: 113.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 50.053333333333526
Incorrectly Classified Instances: 14.52
Correctly Classified Instances: 85.48
Weighted Precision: 0.8556670899385146
Weighted AreaUnderROC: 0.9650270028787825
Root mean squared error: 0.2641537873850652
Relative absolute error: 28.103588252725206
Root relative squared error: 56.03548030082659
Weighted TruePositiveRate: 0.8548
Weighted MatthewsCorrelation: 0.782792792004478
Weighted FMeasure: 0.8542323553615395
Iteration time: 129.0
Weighted AreaUnderPRC: 0.9297076145924017
Mean absolute error: 0.12490483667877929
Coverage of cases: 97.96
Instances selection time: 5.0
Test time: 66.0
Accumulative iteration time: 2312.0
Weighted Recall: 0.8548
Weighted FalsePositiveRate: 0.07245525585604208
Kappa statistic: 0.7822608972722509
Training time: 124.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 49.626666666666864
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8598236876961779
Weighted AreaUnderROC: 0.9660626671128549
Root mean squared error: 0.2618013035649558
Relative absolute error: 27.63016106067243
Root relative squared error: 55.5364431222773
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7892249482251049
Weighted FMeasure: 0.8587252938752928
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9318268578374095
Mean absolute error: 0.12280071582521139
Coverage of cases: 98.0
Instances selection time: 4.0
Test time: 68.0
Accumulative iteration time: 2430.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07027337462262508
Kappa statistic: 0.788853276541263
Training time: 114.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 49.20000000000018
Incorrectly Classified Instances: 13.96
Correctly Classified Instances: 86.04
Weighted Precision: 0.8612669065459244
Weighted AreaUnderROC: 0.966516104775252
Root mean squared error: 0.2611555226067161
Relative absolute error: 27.335888332011535
Root relative squared error: 55.39945229385757
Weighted TruePositiveRate: 0.8604
Weighted MatthewsCorrelation: 0.7911830412243216
Weighted FMeasure: 0.8598728863736442
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9322777559836203
Mean absolute error: 0.12149283703116294
Coverage of cases: 98.04
Instances selection time: 5.0
Test time: 69.0
Accumulative iteration time: 2556.0
Weighted Recall: 0.8604
Weighted FalsePositiveRate: 0.06965272356821588
Kappa statistic: 0.790657594282661
Training time: 121.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 49.146666666666846
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.851527566795826
Weighted AreaUnderROC: 0.9660727283240252
Root mean squared error: 0.2620020761946483
Relative absolute error: 27.486180201298545
Root relative squared error: 55.57903342865698
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.7766723267545469
Weighted FMeasure: 0.8504025805327761
Iteration time: 122.0
Weighted AreaUnderPRC: 0.9311870606280147
Mean absolute error: 0.12216080089466078
Coverage of cases: 98.0
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 2678.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.07446000732886134
Kappa statistic: 0.776255434953436
Training time: 118.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 49.306666666666864
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8547443493572217
Weighted AreaUnderROC: 0.9655564221857786
Root mean squared error: 0.26279138743873676
Relative absolute error: 27.73181990824323
Root relative squared error: 55.74647162860549
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7818358480512415
Weighted FMeasure: 0.8540270482860917
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9301262317590712
Mean absolute error: 0.12325253292552604
Coverage of cases: 97.92
Instances selection time: 5.0
Test time: 69.0
Accumulative iteration time: 2804.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.07271478109905455
Kappa statistic: 0.7816431473140908
Training time: 121.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.70666666666681
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8581760571126171
Weighted AreaUnderROC: 0.9662477053942299
Root mean squared error: 0.2612585793650745
Relative absolute error: 27.654540679463096
Root relative squared error: 55.42131393366227
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7871340265059793
Weighted FMeasure: 0.8576400055988422
Iteration time: 135.0
Weighted AreaUnderPRC: 0.931648636770106
Mean absolute error: 0.12290906968650323
Coverage of cases: 98.04
Instances selection time: 4.0
Test time: 68.0
Accumulative iteration time: 2939.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.07093522341824164
Kappa statistic: 0.7870372258929139
Training time: 131.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 49.60000000000016
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8597261691817779
Weighted AreaUnderROC: 0.9664221045396298
Root mean squared error: 0.26106118084189905
Relative absolute error: 27.683677991968594
Root relative squared error: 55.3794393833622
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7891550150814642
Weighted FMeasure: 0.8587883353380219
Iteration time: 145.0
Weighted AreaUnderPRC: 0.9322456130143179
Mean absolute error: 0.12303856885319434
Coverage of cases: 98.12
Instances selection time: 5.0
Test time: 68.0
Accumulative iteration time: 3084.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.0702802409128263
Kappa statistic: 0.7888494261263322
Training time: 140.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.78666666666681
Incorrectly Classified Instances: 14.4
Correctly Classified Instances: 85.6
Weighted Precision: 0.8565003886747585
Weighted AreaUnderROC: 0.966436037326599
Root mean squared error: 0.26118297631280046
Relative absolute error: 27.73993449358954
Root relative squared error: 55.40527610437986
Weighted TruePositiveRate: 0.856
Weighted MatthewsCorrelation: 0.7843348041826316
Weighted FMeasure: 0.855647686058061
Iteration time: 145.0
Weighted AreaUnderPRC: 0.9321714042361744
Mean absolute error: 0.12328859774928744
Coverage of cases: 98.2
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 3229.0
Weighted Recall: 0.856
Weighted FalsePositiveRate: 0.07189640141344392
Kappa statistic: 0.784042673167782
Training time: 141.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 49.13333333333353
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8592176988897371
Weighted AreaUnderROC: 0.9667776270245617
Root mean squared error: 0.2607465032530627
Relative absolute error: 27.465814702595665
Root relative squared error: 55.31268618627611
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.788488023221403
Weighted FMeasure: 0.858398758027236
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9329878176766165
Mean absolute error: 0.12207028756709241
Coverage of cases: 98.16
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 3380.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07049856841463141
Kappa statistic: 0.7882457050470226
Training time: 147.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 49.160000000000196
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8593612892829404
Weighted AreaUnderROC: 0.9669010827778264
Root mean squared error: 0.2607919373467417
Relative absolute error: 27.42569724189962
Root relative squared error: 55.32232421299736
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.7885727935246416
Weighted FMeasure: 0.8583773063622703
Iteration time: 132.0
Weighted AreaUnderPRC: 0.9333023879914002
Mean absolute error: 0.12189198774177665
Coverage of cases: 98.08
Instances selection time: 4.0
Test time: 68.0
Accumulative iteration time: 3512.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07047796954402774
Kappa statistic: 0.7882514972358723
Training time: 128.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 49.1733333333335
Incorrectly Classified Instances: 14.28
Correctly Classified Instances: 85.72
Weighted Precision: 0.8579297874131547
Weighted AreaUnderROC: 0.9665471289872366
Root mean squared error: 0.26166296260312344
Relative absolute error: 27.521874586933055
Root relative squared error: 55.50709657260904
Weighted TruePositiveRate: 0.8572
Weighted MatthewsCorrelation: 0.7862873200540614
Weighted FMeasure: 0.8567074721773764
Iteration time: 147.0
Weighted AreaUnderPRC: 0.9326845635438796
Mean absolute error: 0.12231944260859193
Coverage of cases: 98.04
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 3659.0
Weighted Recall: 0.8572
Weighted FalsePositiveRate: 0.07125515148843103
Kappa statistic: 0.785858914495444
Training time: 143.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 49.053333333333484
Incorrectly Classified Instances: 13.92
Correctly Classified Instances: 86.08
Weighted Precision: 0.8615104845069407
Weighted AreaUnderROC: 0.9672923871633621
Root mean squared error: 0.25980458469718315
Relative absolute error: 27.278331495707068
Root relative squared error: 55.11287508681975
Weighted TruePositiveRate: 0.8608
Weighted MatthewsCorrelation: 0.7916665609677705
Weighted FMeasure: 0.8603501831496644
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9342258973228705
Mean absolute error: 0.12123702886980975
Coverage of cases: 98.04
Instances selection time: 4.0
Test time: 69.0
Accumulative iteration time: 3808.0
Weighted Recall: 0.8608
Weighted FalsePositiveRate: 0.06946186122721568
Kappa statistic: 0.7912545744040679
Training time: 145.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 49.61333333333342
Incorrectly Classified Instances: 13.8
Correctly Classified Instances: 86.2
Weighted Precision: 0.8627674550415075
Weighted AreaUnderROC: 0.9680391186285344
Root mean squared error: 0.25763725283966116
Relative absolute error: 27.338489339534895
Root relative squared error: 54.65311457075912
Weighted TruePositiveRate: 0.862
Weighted MatthewsCorrelation: 0.7935151840633679
Weighted FMeasure: 0.8614668483739555
Iteration time: 148.0
Weighted AreaUnderPRC: 0.9356046154103486
Mean absolute error: 0.1215043970646001
Coverage of cases: 98.36
Instances selection time: 4.0
Test time: 67.0
Accumulative iteration time: 3956.0
Weighted Recall: 0.862
Weighted FalsePositiveRate: 0.06885494275320891
Kappa statistic: 0.79305882096073
Training time: 144.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 49.46666666666678
Incorrectly Classified Instances: 13.64
Correctly Classified Instances: 86.36
Weighted Precision: 0.8644168693718962
Weighted AreaUnderROC: 0.9685349276618259
Root mean squared error: 0.25584069451707203
Relative absolute error: 27.055006464666693
Root relative squared error: 54.27200699894916
Weighted TruePositiveRate: 0.8636
Weighted MatthewsCorrelation: 0.7959455843731305
Weighted FMeasure: 0.8630477166126383
Iteration time: 146.0
Weighted AreaUnderPRC: 0.9364440055928892
Mean absolute error: 0.12024447317629697
Coverage of cases: 98.28
Instances selection time: 4.0
Test time: 68.0
Accumulative iteration time: 4102.0
Weighted Recall: 0.8636
Weighted FalsePositiveRate: 0.06805029564800072
Kappa statistic: 0.7954600038532698
Training time: 142.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.64000000000007
Incorrectly Classified Instances: 13.28
Correctly Classified Instances: 86.72
Weighted Precision: 0.868012436528654
Weighted AreaUnderROC: 0.9685165700928974
Root mean squared error: 0.25518930571017645
Relative absolute error: 27.13161399475074
Root relative squared error: 54.13382656618569
Weighted TruePositiveRate: 0.8672
Weighted MatthewsCorrelation: 0.8013398949629471
Weighted FMeasure: 0.8666674003295257
Iteration time: 148.0
Weighted AreaUnderPRC: 0.9360446467912403
Mean absolute error: 0.12058495108778164
Coverage of cases: 98.24
Instances selection time: 3.0
Test time: 66.0
Accumulative iteration time: 4250.0
Weighted Recall: 0.8672
Weighted FalsePositiveRate: 0.06625013909658412
Kappa statistic: 0.8008575123504331
Training time: 145.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 49.49333333333344
Incorrectly Classified Instances: 13.72
Correctly Classified Instances: 86.28
Weighted Precision: 0.8632533207388376
Weighted AreaUnderROC: 0.9680994588640555
Root mean squared error: 0.25656587128908853
Relative absolute error: 27.172926578486333
Root relative squared error: 54.42584022286471
Weighted TruePositiveRate: 0.8628
Weighted MatthewsCorrelation: 0.7945190426267685
Weighted FMeasure: 0.8623047339362702
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9354602226281482
Mean absolute error: 0.12076856257105094
Coverage of cases: 98.32
Instances selection time: 3.0
Test time: 67.0
Accumulative iteration time: 4407.0
Weighted Recall: 0.8628
Weighted FalsePositiveRate: 0.06849381694181217
Kappa statistic: 0.7942509763780926
Training time: 154.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 49.813333333333446
Incorrectly Classified Instances: 14.04
Correctly Classified Instances: 85.96
Weighted Precision: 0.8599596059067187
Weighted AreaUnderROC: 0.9679052044311421
Root mean squared error: 0.2572077067417441
Relative absolute error: 27.39512126343246
Root relative squared error: 54.56199408315831
Weighted TruePositiveRate: 0.8596
Weighted MatthewsCorrelation: 0.7896675618756367
Weighted FMeasure: 0.8590881758757123
Iteration time: 159.0
Weighted AreaUnderPRC: 0.9352451121209884
Mean absolute error: 0.12175609450414483
Coverage of cases: 98.36
Instances selection time: 3.0
Test time: 72.0
Accumulative iteration time: 4566.0
Weighted Recall: 0.8596
Weighted FalsePositiveRate: 0.07010311115222854
Kappa statistic: 0.7894512054668306
Training time: 156.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 50.360000000000156
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8593498277711616
Weighted AreaUnderROC: 0.9674787624198541
Root mean squared error: 0.2580862149179004
Relative absolute error: 27.76088120081723
Root relative squared error: 54.7483538097647
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.7886000100720665
Weighted FMeasure: 0.8582106372445957
Iteration time: 156.0
Weighted AreaUnderPRC: 0.9345611391365317
Mean absolute error: 0.12338169422585495
Coverage of cases: 98.48
Instances selection time: 3.0
Test time: 65.0
Accumulative iteration time: 4722.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07047796954402774
Kappa statistic: 0.7882582543890525
Training time: 153.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 50.38666666666683
Incorrectly Classified Instances: 13.6
Correctly Classified Instances: 86.4
Weighted Precision: 0.8647185941187727
Weighted AreaUnderROC: 0.9675657110477165
Root mean squared error: 0.25809740676034465
Relative absolute error: 27.72916330313738
Root relative squared error: 54.750727958070584
Weighted TruePositiveRate: 0.864
Weighted MatthewsCorrelation: 0.7964931049362481
Weighted FMeasure: 0.8634402778653973
Iteration time: 170.0
Weighted AreaUnderPRC: 0.9347548886030113
Mean absolute error: 0.12324072579172225
Coverage of cases: 98.44
Instances selection time: 4.0
Test time: 65.0
Accumulative iteration time: 4892.0
Weighted Recall: 0.864
Weighted FalsePositiveRate: 0.0678594333070005
Kappa statistic: 0.7960579684830382
Training time: 166.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 50.64000000000017
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8639708749413221
Weighted AreaUnderROC: 0.967880338775531
Root mean squared error: 0.25721755770208116
Relative absolute error: 27.79836223266253
Root relative squared error: 54.56408378741497
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7953307634425524
Weighted FMeasure: 0.862595172158786
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9356980690432479
Mean absolute error: 0.12354827658961183
Coverage of cases: 98.52
Instances selection time: 3.0
Test time: 64.0
Accumulative iteration time: 5063.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06825489056940337
Kappa statistic: 0.7948611149766261
Training time: 168.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 50.37333333333351
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8637355363453991
Weighted AreaUnderROC: 0.9682697103365133
Root mean squared error: 0.256324539698846
Relative absolute error: 27.626988252752465
Root relative squared error: 54.3746460616722
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7951662801015693
Weighted FMeasure: 0.8627214202063389
Iteration time: 196.0
Weighted AreaUnderPRC: 0.9362652678323975
Mean absolute error: 0.12278661445667821
Coverage of cases: 98.44
Instances selection time: 3.0
Test time: 64.0
Accumulative iteration time: 5259.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06828235573020829
Kappa statistic: 0.794851763003159
Training time: 193.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 50.786666666666825
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8635286659139348
Weighted AreaUnderROC: 0.9681251637971598
Root mean squared error: 0.2568115378907135
Relative absolute error: 27.917651128668872
Root relative squared error: 54.47795397884072
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7950250150698268
Weighted FMeasure: 0.8628105069319688
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9360993137541873
Mean absolute error: 0.12407844946075113
Coverage of cases: 98.72
Instances selection time: 2.0
Test time: 64.0
Accumulative iteration time: 5424.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06830982089101319
Kappa statistic: 0.7948433454979631
Training time: 163.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 51.05333333333346
Incorrectly Classified Instances: 13.8
Correctly Classified Instances: 86.2
Weighted Precision: 0.8623710071109238
Weighted AreaUnderROC: 0.9684631064108744
Root mean squared error: 0.25607109879189466
Relative absolute error: 28.004638006567987
Root relative squared error: 54.32088312649159
Weighted TruePositiveRate: 0.862
Weighted MatthewsCorrelation: 0.7932530230154106
Weighted FMeasure: 0.8615966608447603
Iteration time: 184.0
Weighted AreaUnderPRC: 0.9367662317116987
Mean absolute error: 0.12446505780696941
Coverage of cases: 98.8
Instances selection time: 3.0
Test time: 62.0
Accumulative iteration time: 5608.0
Weighted Recall: 0.862
Weighted FalsePositiveRate: 0.06890300678461748
Kappa statistic: 0.7930456127469505
Training time: 181.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 51.0666666666668
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8657385173779192
Weighted AreaUnderROC: 0.9689308039847612
Root mean squared error: 0.2549793158594083
Relative absolute error: 27.888810132139696
Root relative squared error: 54.08928099194814
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.7985005902771479
Weighted FMeasure: 0.8652265975366142
Iteration time: 170.0
Weighted AreaUnderPRC: 0.9376320779404989
Mean absolute error: 0.12395026725395478
Coverage of cases: 98.76
Instances selection time: 3.0
Test time: 63.0
Accumulative iteration time: 5778.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06714404797440826
Kappa statistic: 0.7984370714538184
Training time: 167.0
		
Time end:Fri Dec 01 13.59.14 EET 2017