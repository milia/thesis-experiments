Fri Dec 01 13.48.24 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.48.24 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 50.99999999999992
Incorrectly Classified Instances: 16.6
Correctly Classified Instances: 83.4
Weighted Precision: 0.8343222408466139
Weighted AreaUnderROC: 0.9575296553628255
Root mean squared error: 0.2821563392802999
Relative absolute error: 31.105926432428596
Root relative squared error: 59.85439825796154
Weighted TruePositiveRate: 0.834
Weighted MatthewsCorrelation: 0.7511837773943709
Weighted FMeasure: 0.8338403001539033
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9164238424547663
Mean absolute error: 0.13824856192190552
Coverage of cases: 98.28
Instances selection time: 46.0
Test time: 67.0
Accumulative iteration time: 204.0
Weighted Recall: 0.834
Weighted FalsePositiveRate: 0.08291566822374855
Kappa statistic: 0.7510367071478979
Training time: 158.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 50.33333333333319
Incorrectly Classified Instances: 15.8
Correctly Classified Instances: 84.2
Weighted Precision: 0.8421923040441465
Weighted AreaUnderROC: 0.9611661896642818
Root mean squared error: 0.27591748416925965
Relative absolute error: 29.943041165701096
Root relative squared error: 58.530937231204476
Weighted TruePositiveRate: 0.842
Weighted MatthewsCorrelation: 0.7631044115780088
Weighted FMeasure: 0.8418631749452703
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9236364482331574
Mean absolute error: 0.13308018295867216
Coverage of cases: 98.48
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 317.0
Weighted Recall: 0.842
Weighted FalsePositiveRate: 0.07893363043891495
Kappa statistic: 0.7630284555430583
Training time: 66.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 49.94666666666651
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.8449218044841509
Weighted AreaUnderROC: 0.9620475449398694
Root mean squared error: 0.27447596744102437
Relative absolute error: 29.49231560311376
Root relative squared error: 58.22514535508577
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7672551090459713
Weighted FMeasure: 0.8446370760935037
Iteration time: 112.0
Weighted AreaUnderPRC: 0.9256154514237521
Mean absolute error: 0.13107695823606177
Coverage of cases: 98.24
Instances selection time: 48.0
Test time: 72.0
Accumulative iteration time: 429.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.07755639631070614
Kappa statistic: 0.767226889949673
Training time: 64.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 50.25333333333321
Incorrectly Classified Instances: 15.8
Correctly Classified Instances: 84.2
Weighted Precision: 0.8422569575253083
Weighted AreaUnderROC: 0.9614439328375631
Root mean squared error: 0.27464871948115227
Relative absolute error: 29.694991172227553
Root relative squared error: 58.261791596797245
Weighted TruePositiveRate: 0.842
Weighted MatthewsCorrelation: 0.7631117227004126
Weighted FMeasure: 0.8418943398881674
Iteration time: 114.0
Weighted AreaUnderPRC: 0.92411731813593
Mean absolute error: 0.1319777385432342
Coverage of cases: 98.28
Instances selection time: 47.0
Test time: 80.0
Accumulative iteration time: 543.0
Weighted Recall: 0.842
Weighted FalsePositiveRate: 0.07896109559971985
Kappa statistic: 0.7630252141172177
Training time: 67.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 49.30666666666652
Incorrectly Classified Instances: 15.6
Correctly Classified Instances: 84.4
Weighted Precision: 0.8444300076821977
Weighted AreaUnderROC: 0.9605992130508663
Root mean squared error: 0.2755338061980932
Relative absolute error: 29.03971555327487
Root relative squared error: 58.44954684264337
Weighted TruePositiveRate: 0.844
Weighted MatthewsCorrelation: 0.7662370968455869
Weighted FMeasure: 0.8438072270912218
Iteration time: 123.0
Weighted AreaUnderPRC: 0.9214383706143865
Mean absolute error: 0.12906540245900003
Coverage of cases: 97.64
Instances selection time: 50.0
Test time: 75.0
Accumulative iteration time: 666.0
Weighted Recall: 0.844
Weighted FalsePositiveRate: 0.07793125470250532
Kappa statistic: 0.7660355625944856
Training time: 73.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 49.01333333333322
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8491783170234456
Weighted AreaUnderROC: 0.9633517331600621
Root mean squared error: 0.2704997841194952
Relative absolute error: 28.17165528551928
Root relative squared error: 57.38166949811754
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7734079123357864
Weighted FMeasure: 0.8485710843563931
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9275403140516683
Mean absolute error: 0.1252073568245307
Coverage of cases: 97.88
Instances selection time: 52.0
Test time: 77.0
Accumulative iteration time: 786.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07554477854768565
Kappa statistic: 0.7732344683608091
Training time: 68.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 48.55999999999989
Incorrectly Classified Instances: 14.84
Correctly Classified Instances: 85.16
Weighted Precision: 0.8523521599453178
Weighted AreaUnderROC: 0.9647429573184174
Root mean squared error: 0.2679825867872
Relative absolute error: 27.699991192738455
Root relative squared error: 56.84769130714235
Weighted TruePositiveRate: 0.8516
Weighted MatthewsCorrelation: 0.7778573197562197
Weighted FMeasure: 0.8512854611803486
Iteration time: 129.0
Weighted AreaUnderPRC: 0.9308287539942331
Mean absolute error: 0.12311107196772705
Coverage of cases: 97.76
Instances selection time: 53.0
Test time: 77.0
Accumulative iteration time: 915.0
Weighted Recall: 0.8516
Weighted FalsePositiveRate: 0.07407828264686091
Kappa statistic: 0.7774500648334153
Training time: 76.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 48.77333333333332
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8500792614148525
Weighted AreaUnderROC: 0.9637762913504259
Root mean squared error: 0.2684150884737985
Relative absolute error: 27.928270455583856
Root relative squared error: 56.93943876978288
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7747061084543995
Weighted FMeasure: 0.8492440008461324
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9286511521437988
Mean absolute error: 0.12412564646926216
Coverage of cases: 97.84
Instances selection time: 51.0
Test time: 76.0
Accumulative iteration time: 1040.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07509439096367299
Kappa statistic: 0.7744497112836332
Training time: 74.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 49.2
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.852727335108678
Weighted AreaUnderROC: 0.9639009883844052
Root mean squared error: 0.2681023848563911
Relative absolute error: 28.196289074519207
Root relative squared error: 56.873104315271775
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7788122326261903
Weighted FMeasure: 0.8520745164297594
Iteration time: 128.0
Weighted AreaUnderPRC: 0.9289049509720285
Mean absolute error: 0.12531684033119705
Coverage of cases: 97.84
Instances selection time: 47.0
Test time: 75.0
Accumulative iteration time: 1168.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07371029054526294
Kappa statistic: 0.7786437399969767
Training time: 81.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 49.20000000000002
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.8508755071196338
Weighted AreaUnderROC: 0.9637944941025794
Root mean squared error: 0.26969685077614786
Relative absolute error: 28.303954276682084
Root relative squared error: 57.21134161454103
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.7762189777791287
Weighted FMeasure: 0.8506977947996085
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9289457764916104
Mean absolute error: 0.12579535234080985
Coverage of cases: 98.12
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 1292.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.07456986797208094
Kappa statistic: 0.7762176877539599
Training time: 77.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 49.20000000000003
Incorrectly Classified Instances: 14.56
Correctly Classified Instances: 85.44
Weighted Precision: 0.8544682166617636
Weighted AreaUnderROC: 0.9632443806642377
Root mean squared error: 0.2693969267853857
Relative absolute error: 28.258387474557868
Root relative squared error: 57.14771812822848
Weighted TruePositiveRate: 0.8544
Weighted MatthewsCorrelation: 0.7816161650262372
Weighted FMeasure: 0.8542853985684543
Iteration time: 129.0
Weighted AreaUnderPRC: 0.9275290206262653
Mean absolute error: 0.12559283322025777
Coverage of cases: 97.76
Instances selection time: 47.0
Test time: 75.0
Accumulative iteration time: 1421.0
Weighted Recall: 0.8544
Weighted FalsePositiveRate: 0.07276971142066434
Kappa statistic: 0.7816182567137389
Training time: 82.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 48.62666666666665
Incorrectly Classified Instances: 15.08
Correctly Classified Instances: 84.92
Weighted Precision: 0.8491477434702978
Weighted AreaUnderROC: 0.9633965169839496
Root mean squared error: 0.2699773380194313
Relative absolute error: 27.8777401484886
Root relative squared error: 57.270841944069645
Weighted TruePositiveRate: 0.8492
Weighted MatthewsCorrelation: 0.7737169278273938
Weighted FMeasure: 0.8491213659175649
Iteration time: 133.0
Weighted AreaUnderPRC: 0.9275960704554403
Mean absolute error: 0.12390106732661657
Coverage of cases: 97.72
Instances selection time: 49.0
Test time: 77.0
Accumulative iteration time: 1554.0
Weighted Recall: 0.8492
Weighted FalsePositiveRate: 0.07542944539889894
Kappa statistic: 0.7738055010502144
Training time: 84.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 49.10666666666668
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8487874535925719
Weighted AreaUnderROC: 0.9618660956322442
Root mean squared error: 0.2737949778409212
Relative absolute error: 28.598439951754706
Root relative squared error: 58.08068564584064
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7731497780574442
Weighted FMeasure: 0.8486960733866025
Iteration time: 136.0
Weighted AreaUnderPRC: 0.9251221907883072
Mean absolute error: 0.12710417756335485
Coverage of cases: 97.56
Instances selection time: 48.0
Test time: 78.0
Accumulative iteration time: 1690.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.0756065751594967
Kappa statistic: 0.7732127545146861
Training time: 88.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 48.786666666666655
Incorrectly Classified Instances: 15.6
Correctly Classified Instances: 84.4
Weighted Precision: 0.844054097775583
Weighted AreaUnderROC: 0.9619163907020313
Root mean squared error: 0.2731869793390863
Relative absolute error: 28.375771334947782
Root relative squared error: 57.95170968676102
Weighted TruePositiveRate: 0.844
Weighted MatthewsCorrelation: 0.7660174079544156
Weighted FMeasure: 0.8438387118646955
Iteration time: 136.0
Weighted AreaUnderPRC: 0.9245502277000615
Mean absolute error: 0.12611453926643518
Coverage of cases: 97.56
Instances selection time: 48.0
Test time: 78.0
Accumulative iteration time: 1826.0
Weighted Recall: 0.844
Weighted FalsePositiveRate: 0.07796558615351146
Kappa statistic: 0.7660238281333428
Training time: 88.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 48.82666666666667
Incorrectly Classified Instances: 15.44
Correctly Classified Instances: 84.56
Weighted Precision: 0.8458456965118785
Weighted AreaUnderROC: 0.9630909660581206
Root mean squared error: 0.27096168380661434
Relative absolute error: 28.088912517444125
Root relative squared error: 57.479653218414505
Weighted TruePositiveRate: 0.8456
Weighted MatthewsCorrelation: 0.7685406640365521
Weighted FMeasure: 0.8454182477444934
Iteration time: 275.0
Weighted AreaUnderPRC: 0.9270114323458065
Mean absolute error: 0.12483961118864113
Coverage of cases: 97.84
Instances selection time: 96.0
Test time: 86.0
Accumulative iteration time: 2101.0
Weighted Recall: 0.8456
Weighted FalsePositiveRate: 0.07712660759729714
Kappa statistic: 0.7684330862806322
Training time: 179.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 48.45333333333334
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8503821624228267
Weighted AreaUnderROC: 0.9644026862372802
Root mean squared error: 0.2686147476430314
Relative absolute error: 27.499368071191128
Root relative squared error: 56.98179287553007
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.7755531828771008
Weighted FMeasure: 0.8503162588628677
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9295721512502767
Mean absolute error: 0.12221941364973893
Coverage of cases: 97.68
Instances selection time: 50.0
Test time: 79.0
Accumulative iteration time: 2252.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07480192805428851
Kappa statistic: 0.7756095500575496
Training time: 101.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 48.17333333333337
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8488789988735567
Weighted AreaUnderROC: 0.9630408847829276
Root mean squared error: 0.27162534770800945
Relative absolute error: 27.745465397350625
Root relative squared error: 57.620437591946065
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7732407570600143
Weighted FMeasure: 0.8485680976933514
Iteration time: 154.0
Weighted AreaUnderPRC: 0.9264257768513878
Mean absolute error: 0.12331317954378113
Coverage of cases: 97.6
Instances selection time: 52.0
Test time: 78.0
Accumulative iteration time: 2406.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07555851112808812
Kappa statistic: 0.7732303327106966
Training time: 102.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 48.439999999999955
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.8462747873730723
Weighted AreaUnderROC: 0.9625605236016077
Root mean squared error: 0.27217655993132345
Relative absolute error: 28.103418708226986
Root relative squared error: 57.73736736223953
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7691927742583413
Weighted FMeasure: 0.845685092553902
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9256595612009809
Mean absolute error: 0.12490408314767608
Coverage of cases: 97.8
Instances selection time: 50.0
Test time: 77.0
Accumulative iteration time: 2566.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07690828009549203
Kappa statistic: 0.7690424777075001
Training time: 110.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 48.49333333333338
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8505963958313383
Weighted AreaUnderROC: 0.9637241598732554
Root mean squared error: 0.27031989586275273
Relative absolute error: 27.851215426309857
Root relative squared error: 57.34350943625801
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.7757148481832522
Weighted FMeasure: 0.8501849902909173
Iteration time: 150.0
Weighted AreaUnderPRC: 0.928800857555694
Mean absolute error: 0.12378317967248884
Coverage of cases: 97.8
Instances selection time: 50.0
Test time: 77.0
Accumulative iteration time: 2716.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07473326515227625
Kappa statistic: 0.7756330806585878
Training time: 100.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 48.69333333333333
Incorrectly Classified Instances: 15.24
Correctly Classified Instances: 84.76
Weighted Precision: 0.8477910767134011
Weighted AreaUnderROC: 0.963594756756249
Root mean squared error: 0.27098366350439956
Relative absolute error: 28.27900835841287
Root relative squared error: 57.484315816420214
Weighted TruePositiveRate: 0.8476
Weighted MatthewsCorrelation: 0.7715268149596772
Weighted FMeasure: 0.8472938991069117
Iteration time: 147.0
Weighted AreaUnderPRC: 0.9286372737479808
Mean absolute error: 0.12568448159294668
Coverage of cases: 98.0
Instances selection time: 47.0
Test time: 76.0
Accumulative iteration time: 2863.0
Weighted Recall: 0.8476
Weighted FalsePositiveRate: 0.07613109815108873
Kappa statistic: 0.7714399522963387
Training time: 100.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 49.18666666666664
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.8525263079445611
Weighted AreaUnderROC: 0.9641719077662079
Root mean squared error: 0.2686485088692713
Relative absolute error: 28.165419879419844
Root relative squared error: 56.98895471313469
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7786768590249591
Weighted FMeasure: 0.8521600745243515
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9292403398464925
Mean absolute error: 0.12517964390853323
Coverage of cases: 98.12
Instances selection time: 47.0
Test time: 84.0
Accumulative iteration time: 3023.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07374462199626908
Kappa statistic: 0.7786316291128325
Training time: 113.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 48.906666666666666
Incorrectly Classified Instances: 14.68
Correctly Classified Instances: 85.32
Weighted Precision: 0.8533054240590847
Weighted AreaUnderROC: 0.9652346290866172
Root mean squared error: 0.2658569570070453
Relative absolute error: 27.797218850118867
Root relative squared error: 56.39677713759051
Weighted TruePositiveRate: 0.8532
Weighted MatthewsCorrelation: 0.7798670880156349
Weighted FMeasure: 0.852970405540568
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9312545478889187
Mean absolute error: 0.12354319488941776
Coverage of cases: 98.08
Instances selection time: 45.0
Test time: 74.0
Accumulative iteration time: 3180.0
Weighted Recall: 0.8532
Weighted FalsePositiveRate: 0.07334916473386621
Kappa statistic: 0.7798284461647554
Training time: 112.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 49.51999999999999
Incorrectly Classified Instances: 14.48
Correctly Classified Instances: 85.52
Weighted Precision: 0.8553924617940099
Weighted AreaUnderROC: 0.964594493958026
Root mean squared error: 0.2660951148104913
Relative absolute error: 27.987895548502593
Root relative squared error: 56.44729803693327
Weighted TruePositiveRate: 0.8552
Weighted MatthewsCorrelation: 0.7829143205255658
Weighted FMeasure: 0.8549799170911095
Iteration time: 156.0
Weighted AreaUnderPRC: 0.930153863075817
Mean absolute error: 0.12439064688223432
Coverage of cases: 98.24
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 3336.0
Weighted Recall: 0.8552
Weighted FalsePositiveRate: 0.07233305641705412
Kappa statistic: 0.7828320192470821
Training time: 109.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 48.69333333333335
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8567471317808584
Weighted AreaUnderROC: 0.9658106122490281
Root mean squared error: 0.26458342367455756
Relative absolute error: 27.384631002470673
Root relative squared error: 56.12661992094976
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7848313734488922
Weighted FMeasure: 0.8561034947701534
Iteration time: 162.0
Weighted AreaUnderPRC: 0.9321601071224946
Mean absolute error: 0.12170947112209245
Coverage of cases: 97.92
Instances selection time: 46.0
Test time: 78.0
Accumulative iteration time: 3498.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07169867278224247
Kappa statistic: 0.7846405909414195
Training time: 116.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 48.85333333333336
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.8544714784675581
Weighted AreaUnderROC: 0.9647634132256313
Root mean squared error: 0.2654318070282377
Relative absolute error: 27.597588167504526
Root relative squared error: 56.306589207679664
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7812946746855227
Weighted FMeasure: 0.8537269771133422
Iteration time: 158.0
Weighted AreaUnderPRC: 0.9299744165144038
Mean absolute error: 0.1226559474111318
Coverage of cases: 97.96
Instances selection time: 45.0
Test time: 95.0
Accumulative iteration time: 3656.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07288504456945108
Kappa statistic: 0.7810442640915713
Training time: 113.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 48.60000000000005
Incorrectly Classified Instances: 14.76
Correctly Classified Instances: 85.24
Weighted Precision: 0.8525858186843185
Weighted AreaUnderROC: 0.9653548367232329
Root mean squared error: 0.26564914979128845
Relative absolute error: 27.571086803649084
Root relative squared error: 56.352694570158164
Weighted TruePositiveRate: 0.8524
Weighted MatthewsCorrelation: 0.7787216921422583
Weighted FMeasure: 0.8521568726749252
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9315614615772351
Mean absolute error: 0.12253816357177427
Coverage of cases: 97.84
Instances selection time: 46.0
Test time: 81.0
Accumulative iteration time: 3831.0
Weighted Recall: 0.8524
Weighted FalsePositiveRate: 0.07373088941586663
Kappa statistic: 0.7786326384037938
Training time: 129.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 48.81333333333338
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.8540686064398325
Weighted AreaUnderROC: 0.9651064470128385
Root mean squared error: 0.26634963255506283
Relative absolute error: 27.828503881008576
Root relative squared error: 56.50128940386891
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7810327371776817
Weighted FMeasure: 0.8538392902691728
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9304566591769137
Mean absolute error: 0.12368223947114981
Coverage of cases: 97.88
Instances selection time: 44.0
Test time: 77.0
Accumulative iteration time: 3996.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07296057376166457
Kappa statistic: 0.7810213022477174
Training time: 121.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.17333333333338
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8536907261750603
Weighted AreaUnderROC: 0.9643106131886258
Root mean squared error: 0.2680134497782392
Relative absolute error: 28.150470502664998
Root relative squared error: 56.854238336217804
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7804486195192938
Weighted FMeasure: 0.8534789792409072
Iteration time: 243.0
Weighted AreaUnderPRC: 0.9289337139345586
Mean absolute error: 0.12511320223406724
Coverage of cases: 97.92
Instances selection time: 43.0
Test time: 76.0
Accumulative iteration time: 4239.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.073158302392866
Kappa statistic: 0.7804163545899103
Training time: 200.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 49.29333333333335
Incorrectly Classified Instances: 14.96
Correctly Classified Instances: 85.04
Weighted Precision: 0.8503535783051859
Weighted AreaUnderROC: 0.9645736432752637
Root mean squared error: 0.2671167965503839
Relative absolute error: 28.33415804366112
Root relative squared error: 56.66402946288102
Weighted TruePositiveRate: 0.8504
Weighted MatthewsCorrelation: 0.775513695492743
Weighted FMeasure: 0.8503558214883838
Iteration time: 171.0
Weighted AreaUnderPRC: 0.9290321625525768
Mean absolute error: 0.12592959130516113
Coverage of cases: 98.2
Instances selection time: 42.0
Test time: 73.0
Accumulative iteration time: 4410.0
Weighted Recall: 0.8504
Weighted FalsePositiveRate: 0.07484999208569709
Kappa statistic: 0.7755962479692661
Training time: 129.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.00000000000005
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8536211271381324
Weighted AreaUnderROC: 0.9643988440058371
Root mean squared error: 0.26732725149232706
Relative absolute error: 27.947974402951264
Root relative squared error: 56.70867369785569
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7803906671480372
Weighted FMeasure: 0.853501352648568
Iteration time: 171.0
Weighted AreaUnderPRC: 0.928767740199443
Mean absolute error: 0.12421321956867287
Coverage of cases: 97.88
Instances selection time: 43.0
Test time: 75.0
Accumulative iteration time: 4581.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07317890126346968
Kappa statistic: 0.7804133508682671
Training time: 128.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 49.04000000000007
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8556155367734979
Weighted AreaUnderROC: 0.9651217954120173
Root mean squared error: 0.26502421849206775
Relative absolute error: 27.798891592661654
Root relative squared error: 56.220126622321764
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7833983635439586
Weighted FMeasure: 0.8554835870933241
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9305769614958552
Mean absolute error: 0.12355062930071904
Coverage of cases: 98.04
Instances selection time: 43.0
Test time: 75.0
Accumulative iteration time: 4750.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07218339181726129
Kappa statistic: 0.7834102057111071
Training time: 126.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 49.08000000000006
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8579984857252717
Weighted AreaUnderROC: 0.965225768970295
Root mean squared error: 0.26542905780006826
Relative absolute error: 27.783231803418747
Root relative squared error: 56.30600600831517
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7869726093788517
Weighted FMeasure: 0.8578847825184399
Iteration time: 182.0
Weighted AreaUnderPRC: 0.9307728179846861
Mean absolute error: 0.12348103023741723
Coverage of cases: 97.92
Instances selection time: 42.0
Test time: 75.0
Accumulative iteration time: 4932.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.07099015373985144
Kappa statistic: 0.7870129496126633
Training time: 140.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 49.10666666666675
Incorrectly Classified Instances: 13.84
Correctly Classified Instances: 86.16
Weighted Precision: 0.8615955387872646
Weighted AreaUnderROC: 0.9659442614798952
Root mean squared error: 0.26319483956498235
Relative absolute error: 27.606993275151684
Root relative squared error: 55.83205674891121
Weighted TruePositiveRate: 0.8616
Weighted MatthewsCorrelation: 0.7923613871113683
Weighted FMeasure: 0.8615008413443361
Iteration time: 176.0
Weighted AreaUnderPRC: 0.9320159317736696
Mean absolute error: 0.12269774788956361
Coverage of cases: 98.0
Instances selection time: 42.0
Test time: 74.0
Accumulative iteration time: 5108.0
Weighted Recall: 0.8616
Weighted FalsePositiveRate: 0.06919686347863609
Kappa statistic: 0.7924116747674111
Training time: 134.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 49.45333333333335
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.861210547940987
Weighted AreaUnderROC: 0.9661640725340441
Root mean squared error: 0.2628451154701909
Relative absolute error: 27.779391539633167
Root relative squared error: 55.7578690652198
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.791727156448805
Weighted FMeasure: 0.8611799352264737
Iteration time: 183.0
Weighted AreaUnderPRC: 0.9323273416143686
Mean absolute error: 0.12346396239837022
Coverage of cases: 98.24
Instances selection time: 41.0
Test time: 73.0
Accumulative iteration time: 5291.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06945638872164854
Kappa statistic: 0.791797468257214
Training time: 142.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 49.45333333333337
Incorrectly Classified Instances: 14.16
Correctly Classified Instances: 85.84
Weighted Precision: 0.8583841515812841
Weighted AreaUnderROC: 0.9662198972080217
Root mean squared error: 0.2624025465140682
Relative absolute error: 27.701104470954597
Root relative squared error: 55.66398601221469
Weighted TruePositiveRate: 0.8584
Weighted MatthewsCorrelation: 0.7875440349465958
Weighted FMeasure: 0.8583297237185512
Iteration time: 181.0
Weighted AreaUnderPRC: 0.9326340247389188
Mean absolute error: 0.12311601987090989
Coverage of cases: 98.16
Instances selection time: 41.0
Test time: 74.0
Accumulative iteration time: 5472.0
Weighted Recall: 0.8584
Weighted FalsePositiveRate: 0.0708130239792537
Kappa statistic: 0.7876071024184952
Training time: 140.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 49.360000000000035
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8579508944803378
Weighted AreaUnderROC: 0.9660868630522033
Root mean squared error: 0.26329030766707406
Relative absolute error: 27.809816209297118
Root relative squared error: 55.85230859162402
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7869228982406837
Weighted FMeasure: 0.8579305212617272
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9322053296285592
Mean absolute error: 0.12359918315243222
Coverage of cases: 98.16
Instances selection time: 40.0
Test time: 76.0
Accumulative iteration time: 5663.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.07102448519085756
Kappa statistic: 0.7870042087968342
Training time: 151.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.3066666666667
Incorrectly Classified Instances: 14.12
Correctly Classified Instances: 85.88
Weighted Precision: 0.8587906387621349
Weighted AreaUnderROC: 0.9662503215230732
Root mean squared error: 0.2627355100153079
Relative absolute error: 27.842457953388205
Root relative squared error: 55.73461823709896
Weighted TruePositiveRate: 0.8588
Weighted MatthewsCorrelation: 0.7881417520775059
Weighted FMeasure: 0.8587541980308022
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9325907285016647
Mean absolute error: 0.12374425757061482
Coverage of cases: 98.2
Instances selection time: 40.0
Test time: 74.0
Accumulative iteration time: 5854.0
Weighted Recall: 0.8588
Weighted FalsePositiveRate: 0.07062216163825348
Kappa statistic: 0.7882032193110665
Training time: 151.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 49.48
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.8612077766903465
Weighted AreaUnderROC: 0.9662903621136898
Root mean squared error: 0.26273432541283304
Relative absolute error: 27.86546919598616
Root relative squared error: 55.734366944966055
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.7917569690040669
Weighted FMeasure: 0.8611259745484579
Iteration time: 194.0
Weighted AreaUnderPRC: 0.9324319009601931
Mean absolute error: 0.12384652975993907
Coverage of cases: 98.24
Instances selection time: 40.0
Test time: 81.0
Accumulative iteration time: 6048.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06940832469023997
Kappa statistic: 0.7918088606148922
Training time: 154.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 49.76000000000003
Incorrectly Classified Instances: 14.24
Correctly Classified Instances: 85.76
Weighted Precision: 0.8576845664489012
Weighted AreaUnderROC: 0.966112728150561
Root mean squared error: 0.2626487934685054
Relative absolute error: 28.019899584534485
Root relative squared error: 55.71622287961341
Weighted TruePositiveRate: 0.8576
Weighted MatthewsCorrelation: 0.7863877703786774
Weighted FMeasure: 0.8575567595974917
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9318184060810836
Mean absolute error: 0.12453288704237607
Coverage of cases: 98.36
Instances selection time: 39.0
Test time: 73.0
Accumulative iteration time: 6239.0
Weighted Recall: 0.8576
Weighted FalsePositiveRate: 0.07121534753185779
Kappa statistic: 0.7864071425451533
Training time: 152.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 49.88000000000001
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8592950150013777
Weighted AreaUnderROC: 0.9658285484445683
Root mean squared error: 0.2631183058585542
Relative absolute error: 28.11835890738676
Root relative squared error: 55.8158214980698
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7888061095378689
Weighted FMeasure: 0.8591388549925858
Iteration time: 202.0
Weighted AreaUnderPRC: 0.9311837691252367
Mean absolute error: 0.12497048403283063
Coverage of cases: 98.28
Instances selection time: 38.0
Test time: 74.0
Accumulative iteration time: 6441.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07039696784624713
Kappa statistic: 0.7888109142519514
Training time: 164.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 49.72000000000001
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8593044151187372
Weighted AreaUnderROC: 0.9660366427165645
Root mean squared error: 0.26273307917061123
Relative absolute error: 27.992814033438272
Root relative squared error: 55.73410257706824
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7888096230010343
Weighted FMeasure: 0.8591436224974571
Iteration time: 209.0
Weighted AreaUnderPRC: 0.9317846679452925
Mean absolute error: 0.12441250681528179
Coverage of cases: 98.32
Instances selection time: 38.0
Test time: 73.0
Accumulative iteration time: 6650.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07039696784624713
Kappa statistic: 0.7888109142519514
Training time: 171.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 49.61333333333336
Incorrectly Classified Instances: 14.08
Correctly Classified Instances: 85.92
Weighted Precision: 0.8592731197203081
Weighted AreaUnderROC: 0.9660302887238892
Root mean squared error: 0.26288974401359083
Relative absolute error: 27.995780061411075
Root relative squared error: 55.76733620892157
Weighted TruePositiveRate: 0.8592
Weighted MatthewsCorrelation: 0.7887896766196203
Weighted FMeasure: 0.8591317922764014
Iteration time: 210.0
Weighted AreaUnderPRC: 0.9317004425618075
Mean absolute error: 0.12442568916182759
Coverage of cases: 98.28
Instances selection time: 38.0
Test time: 73.0
Accumulative iteration time: 6860.0
Weighted Recall: 0.8592
Weighted FalsePositiveRate: 0.07040383413644836
Kappa statistic: 0.7888109142519514
Training time: 172.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 49.76
Incorrectly Classified Instances: 14.28
Correctly Classified Instances: 85.72
Weighted Precision: 0.8572149643000241
Weighted AreaUnderROC: 0.9661791211293095
Root mean squared error: 0.2626782020349224
Relative absolute error: 28.01078163391496
Root relative squared error: 55.722461378634954
Weighted TruePositiveRate: 0.8572
Weighted MatthewsCorrelation: 0.785773528127055
Weighted FMeasure: 0.8571184763309182
Iteration time: 215.0
Weighted AreaUnderPRC: 0.9319349466414123
Mean absolute error: 0.1244923628174004
Coverage of cases: 98.32
Instances selection time: 38.0
Test time: 73.0
Accumulative iteration time: 7075.0
Weighted Recall: 0.8572
Weighted FalsePositiveRate: 0.07139247729245553
Kappa statistic: 0.7858110692839393
Training time: 177.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 49.653333333333336
Incorrectly Classified Instances: 14.24
Correctly Classified Instances: 85.76
Weighted Precision: 0.8576247636839879
Weighted AreaUnderROC: 0.9660993555083187
Root mean squared error: 0.2629238170832872
Relative absolute error: 27.995266305976408
Root relative squared error: 55.77456419851302
Weighted TruePositiveRate: 0.8576
Weighted MatthewsCorrelation: 0.786375731263312
Weighted FMeasure: 0.8575296464271865
Iteration time: 225.0
Weighted AreaUnderPRC: 0.9317923315720551
Mean absolute error: 0.12442340580434018
Coverage of cases: 98.36
Instances selection time: 37.0
Test time: 81.0
Accumulative iteration time: 7300.0
Weighted Recall: 0.8576
Weighted FalsePositiveRate: 0.07119474866125411
Kappa statistic: 0.7864100643577676
Training time: 188.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 49.89333333333332
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8556193744410986
Weighted AreaUnderROC: 0.9660055605386351
Root mean squared error: 0.26298035446677237
Relative absolute error: 28.102267130437525
Root relative squared error: 55.78655758868888
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7833605920810751
Weighted FMeasure: 0.8555360997597949
Iteration time: 217.0
Weighted AreaUnderPRC: 0.9317310786859653
Mean absolute error: 0.12489896502416736
Coverage of cases: 98.4
Instances selection time: 36.0
Test time: 72.0
Accumulative iteration time: 7517.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07221085697806619
Kappa statistic: 0.7834082304872414
Training time: 181.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 49.70666666666666
Incorrectly Classified Instances: 14.44
Correctly Classified Instances: 85.56
Weighted Precision: 0.8556174742681698
Weighted AreaUnderROC: 0.9660256990064118
Root mean squared error: 0.26306840695535083
Relative absolute error: 28.02246281112117
Root relative squared error: 55.805236342221136
Weighted TruePositiveRate: 0.8556
Weighted MatthewsCorrelation: 0.7833650629995816
Weighted FMeasure: 0.8555313001968124
Iteration time: 215.0
Weighted AreaUnderPRC: 0.9317635726127271
Mean absolute error: 0.1245442791605391
Coverage of cases: 98.36
Instances selection time: 37.0
Test time: 72.0
Accumulative iteration time: 7732.0
Weighted Recall: 0.8556
Weighted FalsePositiveRate: 0.07220399068786496
Kappa statistic: 0.7834092181036776
Training time: 178.0
		
Time end:Fri Dec 01 13.48.38 EET 2017