Fri Dec 01 14.00.24 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Fri Dec 01 14.00.24 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 51.09333333333324
Incorrectly Classified Instances: 17.0
Correctly Classified Instances: 83.0
Weighted Precision: 0.8299924254852508
Weighted AreaUnderROC: 0.9556351721807245
Root mean squared error: 0.28526379993580103
Relative absolute error: 31.40029950298669
Root relative squared error: 60.513590208494115
Weighted TruePositiveRate: 0.83
Weighted MatthewsCorrelation: 0.7449244594211395
Weighted FMeasure: 0.8299960108301071
Iteration time: 251.0
Weighted AreaUnderPRC: 0.9137700716054902
Mean absolute error: 0.1395568866799415
Coverage of cases: 97.8
Instances selection time: 48.0
Test time: 67.0
Accumulative iteration time: 251.0
Weighted Recall: 0.83
Weighted FalsePositiveRate: 0.08507304347739532
Kappa statistic: 0.7449850255206985
Training time: 203.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 51.23999999999993
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.836701995199556
Weighted AreaUnderROC: 0.9572683522040234
Root mean squared error: 0.2817410062394442
Relative absolute error: 30.92914161462038
Root relative squared error: 59.766292815069576
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.7551437385980259
Weighted FMeasure: 0.836612087742118
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9165667893641881
Mean absolute error: 0.13746285162053568
Coverage of cases: 98.0
Instances selection time: 55.0
Test time: 68.0
Accumulative iteration time: 375.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08164255730701554
Kappa statistic: 0.7551951430716385
Training time: 69.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 51.34666666666667
Incorrectly Classified Instances: 16.2
Correctly Classified Instances: 83.8
Weighted Precision: 0.8379866298775068
Weighted AreaUnderROC: 0.957682778808022
Root mean squared error: 0.2810927821556953
Relative absolute error: 30.759015324093767
Root relative squared error: 59.62878372146539
Weighted TruePositiveRate: 0.838
Weighted MatthewsCorrelation: 0.7570477670278527
Weighted FMeasure: 0.8376908176219083
Iteration time: 116.0
Weighted AreaUnderPRC: 0.9175046128476136
Mean absolute error: 0.13670673477375073
Coverage of cases: 98.12
Instances selection time: 47.0
Test time: 69.0
Accumulative iteration time: 491.0
Weighted Recall: 0.838
Weighted FalsePositiveRate: 0.08099694311709224
Kappa statistic: 0.7570074258530659
Training time: 69.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 51.41333333333327
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8401293410766051
Weighted AreaUnderROC: 0.9581286597150193
Root mean squared error: 0.27936079569518524
Relative absolute error: 30.638418847295004
Root relative squared error: 59.261373910120405
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.7601610506500727
Weighted FMeasure: 0.8395611830143964
Iteration time: 116.0
Weighted AreaUnderPRC: 0.9184314474145249
Mean absolute error: 0.13617075043242288
Coverage of cases: 98.12
Instances selection time: 46.0
Test time: 69.0
Accumulative iteration time: 607.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.07998405010352176
Kappa statistic: 0.760017355544847
Training time: 70.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 51.25333333333321
Incorrectly Classified Instances: 15.84
Correctly Classified Instances: 84.16
Weighted Precision: 0.8423871440642008
Weighted AreaUnderROC: 0.9578314712616418
Root mean squared error: 0.27881670586838625
Relative absolute error: 30.531742079686264
Root relative squared error: 59.14595502828915
Weighted TruePositiveRate: 0.8416
Weighted MatthewsCorrelation: 0.7630448179486453
Weighted FMeasure: 0.8408647508575504
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9175984223831314
Mean absolute error: 0.13569663146527292
Coverage of cases: 98.08
Instances selection time: 44.0
Test time: 69.0
Accumulative iteration time: 726.0
Weighted Recall: 0.8416
Weighted FalsePositiveRate: 0.07910201156648047
Kappa statistic: 0.7624448884138855
Training time: 75.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 51.079999999999835
Incorrectly Classified Instances: 15.84
Correctly Classified Instances: 84.16
Weighted Precision: 0.8425463963633807
Weighted AreaUnderROC: 0.9581308760843873
Root mean squared error: 0.2777884757803604
Relative absolute error: 30.32485192715393
Root relative squared error: 58.92783448793023
Weighted TruePositiveRate: 0.8416
Weighted MatthewsCorrelation: 0.7631737385534738
Weighted FMeasure: 0.8407654967868462
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9179042156204179
Mean absolute error: 0.13477711967624031
Coverage of cases: 97.88
Instances selection time: 47.0
Test time: 68.0
Accumulative iteration time: 846.0
Weighted Recall: 0.8416
Weighted FalsePositiveRate: 0.07908177061618983
Kappa statistic: 0.7624510445195538
Training time: 73.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 50.853333333333154
Incorrectly Classified Instances: 15.72
Correctly Classified Instances: 84.28
Weighted Precision: 0.8434734442743598
Weighted AreaUnderROC: 0.9586554303823946
Root mean squared error: 0.2765395228273215
Relative absolute error: 30.069005561694745
Root relative squared error: 58.662891557187194
Weighted TruePositiveRate: 0.8428
Weighted MatthewsCorrelation: 0.7647960734647377
Weighted FMeasure: 0.8420368828613726
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9191863412145492
Mean absolute error: 0.13364002471864395
Coverage of cases: 97.92
Instances selection time: 44.0
Test time: 69.0
Accumulative iteration time: 964.0
Weighted Recall: 0.8428
Weighted FalsePositiveRate: 0.078492591921408
Kappa statistic: 0.7642459060371765
Training time: 74.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 51.173333333333204
Incorrectly Classified Instances: 15.72
Correctly Classified Instances: 84.28
Weighted Precision: 0.8440084798293334
Weighted AreaUnderROC: 0.9584820801211085
Root mean squared error: 0.27677579741487984
Relative absolute error: 30.16369251519256
Root relative squared error: 58.71301296611256
Weighted TruePositiveRate: 0.8428
Weighted MatthewsCorrelation: 0.7651500740196693
Weighted FMeasure: 0.8418977772381684
Iteration time: 122.0
Weighted AreaUnderPRC: 0.9191066492387406
Mean absolute error: 0.13406085562307868
Coverage of cases: 97.92
Instances selection time: 43.0
Test time: 69.0
Accumulative iteration time: 1086.0
Weighted Recall: 0.8428
Weighted FalsePositiveRate: 0.07849251415800297
Kappa statistic: 0.7642473768471487
Training time: 79.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 51.10666666666652
Incorrectly Classified Instances: 15.96
Correctly Classified Instances: 84.04
Weighted Precision: 0.8420738821351682
Weighted AreaUnderROC: 0.9584384141277486
Root mean squared error: 0.27719645055824177
Relative absolute error: 30.146124111322894
Root relative squared error: 58.80224697317215
Weighted TruePositiveRate: 0.8404
Weighted MatthewsCorrelation: 0.761851426627661
Weighted FMeasure: 0.8393903327541251
Iteration time: 122.0
Weighted AreaUnderPRC: 0.9189275303587765
Mean absolute error: 0.1339827738281024
Coverage of cases: 97.88
Instances selection time: 43.0
Test time: 70.0
Accumulative iteration time: 1208.0
Weighted Recall: 0.8404
Weighted FalsePositiveRate: 0.07966078995412382
Kappa statistic: 0.7606595861894223
Training time: 79.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 50.7599999999998
Incorrectly Classified Instances: 16.4
Correctly Classified Instances: 83.6
Weighted Precision: 0.8379291538049068
Weighted AreaUnderROC: 0.9582655717772782
Root mean squared error: 0.278579360819436
Relative absolute error: 30.16373271829687
Root relative squared error: 59.09560654021103
Weighted TruePositiveRate: 0.836
Weighted MatthewsCorrelation: 0.7554125681427789
Weighted FMeasure: 0.8349346984923565
Iteration time: 142.0
Weighted AreaUnderPRC: 0.9187906336000425
Mean absolute error: 0.13406103430354227
Coverage of cases: 97.64
Instances selection time: 46.0
Test time: 67.0
Accumulative iteration time: 1350.0
Weighted Recall: 0.836
Weighted FalsePositiveRate: 0.08184410244168036
Kappa statistic: 0.7540677199126447
Training time: 96.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 50.613333333333124
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.839262288826465
Weighted AreaUnderROC: 0.9580241410577431
Root mean squared error: 0.2791735380556455
Relative absolute error: 30.176680492054874
Root relative squared error: 59.221650566096145
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7573018779688682
Weighted FMeasure: 0.8360875934190984
Iteration time: 141.0
Weighted AreaUnderPRC: 0.918317015842623
Mean absolute error: 0.13411857996468896
Coverage of cases: 97.52
Instances selection time: 45.0
Test time: 69.0
Accumulative iteration time: 1491.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08125270230562828
Kappa statistic: 0.7558667557942955
Training time: 96.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 50.34666666666642
Incorrectly Classified Instances: 16.36
Correctly Classified Instances: 83.64
Weighted Precision: 0.8384235431219953
Weighted AreaUnderROC: 0.9583571895530553
Root mean squared error: 0.27831409038172306
Relative absolute error: 29.863987493475694
Root relative squared error: 59.039334182604485
Weighted TruePositiveRate: 0.8364
Weighted MatthewsCorrelation: 0.7560916311629468
Weighted FMeasure: 0.8352488757287463
Iteration time: 148.0
Weighted AreaUnderPRC: 0.9188129792926275
Mean absolute error: 0.13272883330433705
Coverage of cases: 97.24
Instances selection time: 45.0
Test time: 69.0
Accumulative iteration time: 1639.0
Weighted Recall: 0.8364
Weighted FalsePositiveRate: 0.08164744543585528
Kappa statistic: 0.754669438362166
Training time: 103.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 50.14666666666643
Incorrectly Classified Instances: 16.12
Correctly Classified Instances: 83.88
Weighted Precision: 0.8414592637108097
Weighted AreaUnderROC: 0.9582836338322129
Root mean squared error: 0.2787252155682771
Relative absolute error: 29.783140775952337
Root relative squared error: 59.12654700480317
Weighted TruePositiveRate: 0.8388
Weighted MatthewsCorrelation: 0.7601333712746893
Weighted FMeasure: 0.8374212040366605
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9184058171349939
Mean absolute error: 0.1323695145597888
Coverage of cases: 97.28
Instances selection time: 44.0
Test time: 76.0
Accumulative iteration time: 1778.0
Weighted Recall: 0.8388
Weighted FalsePositiveRate: 0.0804378954164597
Kappa statistic: 0.7582759593625299
Training time: 95.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 49.77333333333317
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8438094312105008
Weighted AreaUnderROC: 0.9586787975473712
Root mean squared error: 0.27816252910340594
Relative absolute error: 29.546994652643377
Root relative squared error: 59.00718318030548
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.763374867104666
Weighted FMeasure: 0.8393090868912776
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9193358358914336
Mean absolute error: 0.13131997623397118
Coverage of cases: 97.08
Instances selection time: 44.0
Test time: 77.0
Accumulative iteration time: 1919.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.0794156353687348
Kappa statistic: 0.7612823480412197
Training time: 97.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 49.75999999999983
Incorrectly Classified Instances: 15.92
Correctly Classified Instances: 84.08
Weighted Precision: 0.8437504844269382
Weighted AreaUnderROC: 0.9585642572626061
Root mean squared error: 0.2785632376480277
Relative absolute error: 29.529488457425487
Root relative squared error: 59.09218629905991
Weighted TruePositiveRate: 0.8408
Weighted MatthewsCorrelation: 0.7633376770250367
Weighted FMeasure: 0.8392946364157542
Iteration time: 128.0
Weighted AreaUnderPRC: 0.9191832978213266
Mean absolute error: 0.13124217092189167
Coverage of cases: 97.04
Instances selection time: 43.0
Test time: 73.0
Accumulative iteration time: 2047.0
Weighted Recall: 0.8408
Weighted FalsePositiveRate: 0.07943008208131314
Kappa statistic: 0.7612812025861283
Training time: 85.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 49.7466666666665
Incorrectly Classified Instances: 15.64
Correctly Classified Instances: 84.36
Weighted Precision: 0.8470696568397141
Weighted AreaUnderROC: 0.9582498577855937
Root mean squared error: 0.27915088813843225
Relative absolute error: 29.606255083223722
Root relative squared error: 59.21684579307971
Weighted TruePositiveRate: 0.8436
Weighted MatthewsCorrelation: 0.7678670790367794
Weighted FMeasure: 0.8420300698606207
Iteration time: 135.0
Weighted AreaUnderPRC: 0.9187142068964619
Mean absolute error: 0.13158335592543938
Coverage of cases: 97.08
Instances selection time: 43.0
Test time: 74.0
Accumulative iteration time: 2182.0
Weighted Recall: 0.8436
Weighted FalsePositiveRate: 0.07799140883449376
Kappa statistic: 0.765488101430055
Training time: 92.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 49.69333333333315
Incorrectly Classified Instances: 16.36
Correctly Classified Instances: 83.64
Weighted Precision: 0.8409096822219648
Weighted AreaUnderROC: 0.9580988574893392
Root mean squared error: 0.2809516300782701
Relative absolute error: 29.84254759890365
Root relative squared error: 59.598840844127615
Weighted TruePositiveRate: 0.8364
Weighted MatthewsCorrelation: 0.757739888600347
Weighted FMeasure: 0.8346411250315179
Iteration time: 142.0
Weighted AreaUnderPRC: 0.9186076451901887
Mean absolute error: 0.13263354488401685
Coverage of cases: 97.08
Instances selection time: 43.0
Test time: 75.0
Accumulative iteration time: 2324.0
Weighted Recall: 0.8364
Weighted FalsePositiveRate: 0.08156275331144056
Kappa statistic: 0.7547007493082442
Training time: 99.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 49.57333333333315
Incorrectly Classified Instances: 16.44
Correctly Classified Instances: 83.56
Weighted Precision: 0.8402646216346235
Weighted AreaUnderROC: 0.9578821833311189
Root mean squared error: 0.2817074407132417
Relative absolute error: 29.880491780706503
Root relative squared error: 59.759172491712015
Weighted TruePositiveRate: 0.8356
Weighted MatthewsCorrelation: 0.7566514713270434
Weighted FMeasure: 0.8337777719093812
Iteration time: 145.0
Weighted AreaUnderPRC: 0.9181576502967
Mean absolute error: 0.13280218569202953
Coverage of cases: 96.92
Instances selection time: 43.0
Test time: 74.0
Accumulative iteration time: 2469.0
Weighted Recall: 0.8356
Weighted FalsePositiveRate: 0.08195892556024438
Kappa statistic: 0.7535032525575686
Training time: 102.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 49.49333333333314
Incorrectly Classified Instances: 16.04
Correctly Classified Instances: 83.96
Weighted Precision: 0.8450176381758125
Weighted AreaUnderROC: 0.957672090960705
Root mean squared error: 0.2825417597521727
Relative absolute error: 29.980760123385767
Root relative squared error: 59.93615828674236
Weighted TruePositiveRate: 0.8396
Weighted MatthewsCorrelation: 0.7630243345936077
Weighted FMeasure: 0.8379776105053984
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9176835534844998
Mean absolute error: 0.13324782277060404
Coverage of cases: 96.84
Instances selection time: 43.0
Test time: 73.0
Accumulative iteration time: 2618.0
Weighted Recall: 0.8396
Weighted FalsePositiveRate: 0.07995846016181797
Kappa statistic: 0.7594978555125489
Training time: 106.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 49.533333333333125
Incorrectly Classified Instances: 16.36
Correctly Classified Instances: 83.64
Weighted Precision: 0.8426629701505605
Weighted AreaUnderROC: 0.9581138865582332
Root mean squared error: 0.28335442303232744
Relative absolute error: 29.987225301341617
Root relative squared error: 60.10855020160797
Weighted TruePositiveRate: 0.8364
Weighted MatthewsCorrelation: 0.7587442271607641
Weighted FMeasure: 0.8346743429245942
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9186100281200643
Mean absolute error: 0.13327655689485227
Coverage of cases: 96.88
Instances selection time: 42.0
Test time: 74.0
Accumulative iteration time: 2787.0
Weighted Recall: 0.8364
Weighted FalsePositiveRate: 0.08152941700374337
Kappa statistic: 0.7547092230771483
Training time: 127.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 49.51999999999982
Incorrectly Classified Instances: 16.24
Correctly Classified Instances: 83.76
Weighted Precision: 0.843902312221038
Weighted AreaUnderROC: 0.9578062953136433
Root mean squared error: 0.2842934670264939
Relative absolute error: 30.09851994980834
Root relative squared error: 60.307751514440255
Weighted TruePositiveRate: 0.8376
Weighted MatthewsCorrelation: 0.7605824406047871
Weighted FMeasure: 0.8358590553315498
Iteration time: 162.0
Weighted AreaUnderPRC: 0.9179825259443504
Mean absolute error: 0.13377119977692659
Coverage of cases: 96.84
Instances selection time: 40.0
Test time: 74.0
Accumulative iteration time: 2949.0
Weighted Recall: 0.8376
Weighted FalsePositiveRate: 0.0809293643928253
Kappa statistic: 0.7565071368597817
Training time: 122.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 49.39999999999977
Incorrectly Classified Instances: 16.4
Correctly Classified Instances: 83.6
Weighted Precision: 0.843632282597483
Weighted AreaUnderROC: 0.9577049419003335
Root mean squared error: 0.2843457227956023
Relative absolute error: 30.015896683835326
Root relative squared error: 60.31883663704805
Weighted TruePositiveRate: 0.836
Weighted MatthewsCorrelation: 0.7590243589658173
Weighted FMeasure: 0.8341044723485583
Iteration time: 162.0
Weighted AreaUnderPRC: 0.9177549930212751
Mean absolute error: 0.13340398526149097
Coverage of cases: 96.84
Instances selection time: 41.0
Test time: 73.0
Accumulative iteration time: 3111.0
Weighted Recall: 0.836
Weighted FalsePositiveRate: 0.08171948744916269
Kappa statistic: 0.7541120855469242
Training time: 121.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 49.27999999999979
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.8442779479458262
Weighted AreaUnderROC: 0.9574997841457227
Root mean squared error: 0.2849855893739988
Relative absolute error: 30.09024904937675
Root relative squared error: 60.454572836039695
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7604728064901699
Weighted FMeasure: 0.8353832405689549
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9174364284983781
Mean absolute error: 0.13373444021945285
Coverage of cases: 96.6
Instances selection time: 40.0
Test time: 80.0
Accumulative iteration time: 3276.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08112379995738013
Kappa statistic: 0.7559085718672335
Training time: 125.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 49.33333333333315
Incorrectly Classified Instances: 16.4
Correctly Classified Instances: 83.6
Weighted Precision: 0.8438115563311114
Weighted AreaUnderROC: 0.9574946154682915
Root mean squared error: 0.28451560848198537
Relative absolute error: 30.02007746278941
Root relative squared error: 60.35487483330845
Weighted TruePositiveRate: 0.836
Weighted MatthewsCorrelation: 0.7591414235232329
Weighted FMeasure: 0.8341096798029808
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9171273375513842
Mean absolute error: 0.1334225665012869
Coverage of cases: 96.64
Instances selection time: 40.0
Test time: 73.0
Accumulative iteration time: 3442.0
Weighted Recall: 0.836
Weighted FalsePositiveRate: 0.08171297865216195
Kappa statistic: 0.754110669869705
Training time: 126.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 49.35999999999982
Incorrectly Classified Instances: 16.24
Correctly Classified Instances: 83.76
Weighted Precision: 0.8464249573421366
Weighted AreaUnderROC: 0.9576862403789473
Root mean squared error: 0.28460100182568665
Relative absolute error: 30.061450052503805
Root relative squared error: 60.37298949702827
Weighted TruePositiveRate: 0.8376
Weighted MatthewsCorrelation: 0.762227214463359
Weighted FMeasure: 0.8355113425587044
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9175228810172114
Mean absolute error: 0.1336064446777953
Coverage of cases: 96.6
Instances selection time: 41.0
Test time: 74.0
Accumulative iteration time: 3602.0
Weighted Recall: 0.8376
Weighted FalsePositiveRate: 0.08093428854443917
Kappa statistic: 0.7565056181230831
Training time: 119.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 49.58666666666649
Incorrectly Classified Instances: 15.88
Correctly Classified Instances: 84.12
Weighted Precision: 0.8498265151420935
Weighted AreaUnderROC: 0.9579136791912269
Root mean squared error: 0.2845485208163633
Relative absolute error: 30.085867631146503
Root relative squared error: 60.36185659375545
Weighted TruePositiveRate: 0.8412
Weighted MatthewsCorrelation: 0.7675423377330621
Weighted FMeasure: 0.8390278871649176
Iteration time: 165.0
Weighted AreaUnderPRC: 0.9179067073087015
Mean absolute error: 0.13371496724954063
Coverage of cases: 96.68
Instances selection time: 39.0
Test time: 72.0
Accumulative iteration time: 3767.0
Weighted Recall: 0.8412
Weighted FalsePositiveRate: 0.07913992494939728
Kappa statistic: 0.7619045334336412
Training time: 126.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 49.279999999999816
Incorrectly Classified Instances: 16.04
Correctly Classified Instances: 83.96
Weighted Precision: 0.8481145282935977
Weighted AreaUnderROC: 0.9576835964798575
Root mean squared error: 0.2853382314821313
Relative absolute error: 30.043770400235164
Root relative squared error: 60.52937952383741
Weighted TruePositiveRate: 0.8396
Weighted MatthewsCorrelation: 0.7650666911473388
Weighted FMeasure: 0.8374545709064168
Iteration time: 164.0
Weighted AreaUnderPRC: 0.9176615864983014
Mean absolute error: 0.13352786844549025
Coverage of cases: 96.64
Instances selection time: 40.0
Test time: 73.0
Accumulative iteration time: 3931.0
Weighted Recall: 0.8396
Weighted FalsePositiveRate: 0.07994457248171802
Kappa statistic: 0.7595033943007222
Training time: 124.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.213333333333125
Incorrectly Classified Instances: 16.04
Correctly Classified Instances: 83.96
Weighted Precision: 0.848144663318487
Weighted AreaUnderROC: 0.957025202789161
Root mean squared error: 0.2861669864166642
Relative absolute error: 30.096380637542936
Root relative squared error: 60.70518499408243
Weighted TruePositiveRate: 0.8396
Weighted MatthewsCorrelation: 0.765127362444169
Weighted FMeasure: 0.83735080204537
Iteration time: 163.0
Weighted AreaUnderPRC: 0.9166773507659595
Mean absolute error: 0.13376169172241367
Coverage of cases: 96.4
Instances selection time: 40.0
Test time: 74.0
Accumulative iteration time: 4094.0
Weighted Recall: 0.8396
Weighted FalsePositiveRate: 0.07994457248171802
Kappa statistic: 0.7595043174072816
Training time: 123.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 49.23999999999978
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.84581071964018
Weighted AreaUnderROC: 0.9561467880831293
Root mean squared error: 0.2879026903912216
Relative absolute error: 30.27705317421947
Root relative squared error: 61.073383409245025
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7615606961789179
Weighted FMeasure: 0.8349335789999219
Iteration time: 176.0
Weighted AreaUnderPRC: 0.9153466396849884
Mean absolute error: 0.1345646807743094
Coverage of cases: 96.4
Instances selection time: 40.0
Test time: 74.0
Accumulative iteration time: 4270.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08114539226284258
Kappa statistic: 0.7559081034191578
Training time: 136.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.09333333333315
Incorrectly Classified Instances: 16.24
Correctly Classified Instances: 83.76
Weighted Precision: 0.8461283274584243
Weighted AreaUnderROC: 0.9559027276457842
Root mean squared error: 0.28808566498949223
Relative absolute error: 30.195853846122517
Root relative squared error: 61.11219818301163
Weighted TruePositiveRate: 0.8376
Weighted MatthewsCorrelation: 0.762109897627208
Weighted FMeasure: 0.835366163411493
Iteration time: 180.0
Weighted AreaUnderPRC: 0.9150012024747741
Mean absolute error: 0.13420379487165626
Coverage of cases: 96.32
Instances selection time: 39.0
Test time: 74.0
Accumulative iteration time: 4450.0
Weighted Recall: 0.8376
Weighted FalsePositiveRate: 0.08094730613844066
Kappa statistic: 0.7565044498511918
Training time: 141.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 49.03999999999981
Incorrectly Classified Instances: 16.8
Correctly Classified Instances: 83.2
Weighted Precision: 0.84074871177114
Weighted AreaUnderROC: 0.9554559352320972
Root mean squared error: 0.28952189784296534
Relative absolute error: 30.356124776821652
Root relative squared error: 61.41686918002775
Weighted TruePositiveRate: 0.832
Weighted MatthewsCorrelation: 0.7538293707044872
Weighted FMeasure: 0.8297605865650834
Iteration time: 185.0
Weighted AreaUnderPRC: 0.9143499347798762
Mean absolute error: 0.13491611011920798
Coverage of cases: 96.32
Instances selection time: 40.0
Test time: 74.0
Accumulative iteration time: 4635.0
Weighted Recall: 0.832
Weighted FalsePositiveRate: 0.08372273332133773
Kappa statistic: 0.7481150610401168
Training time: 145.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 48.79999999999977
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.8462976482874114
Weighted AreaUnderROC: 0.955373127039531
Root mean squared error: 0.289357777030658
Relative absolute error: 30.176078771496588
Root relative squared error: 61.38205389823285
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7618411793391022
Weighted FMeasure: 0.8349525522066943
Iteration time: 184.0
Weighted AreaUnderPRC: 0.9142146363375585
Mean absolute error: 0.13411590565109657
Coverage of cases: 96.16
Instances selection time: 39.0
Test time: 76.0
Accumulative iteration time: 4819.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08114031258441867
Kappa statistic: 0.7559088060905971
Training time: 145.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 48.81333333333314
Incorrectly Classified Instances: 16.48
Correctly Classified Instances: 83.52
Weighted Precision: 0.8443235435060091
Weighted AreaUnderROC: 0.9552965750217681
Root mean squared error: 0.2891694699039113
Relative absolute error: 30.195827194264098
Root relative squared error: 61.342107924352334
Weighted TruePositiveRate: 0.8352
Weighted MatthewsCorrelation: 0.7588959738022911
Weighted FMeasure: 0.8328593548083656
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9138481763321792
Mean absolute error: 0.13420367641895217
Coverage of cases: 96.12
Instances selection time: 38.0
Test time: 77.0
Accumulative iteration time: 5009.0
Weighted Recall: 0.8352
Weighted FalsePositiveRate: 0.08213582288485215
Kappa statistic: 0.7529110911734087
Training time: 152.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 49.02666666666651
Incorrectly Classified Instances: 16.56
Correctly Classified Instances: 83.44
Weighted Precision: 0.8436104823960301
Weighted AreaUnderROC: 0.9552518570955246
Root mean squared error: 0.28882550426146314
Relative absolute error: 30.43200803859203
Root relative squared error: 61.26914179287125
Weighted TruePositiveRate: 0.8344
Weighted MatthewsCorrelation: 0.7577692903633979
Weighted FMeasure: 0.8319701580440085
Iteration time: 211.0
Weighted AreaUnderPRC: 0.9140118660247514
Mean absolute error: 0.13525336906040966
Coverage of cases: 96.12
Instances selection time: 38.0
Test time: 74.0
Accumulative iteration time: 5220.0
Weighted Recall: 0.8344
Weighted FalsePositiveRate: 0.08250961050550008
Kappa statistic: 0.7517210409581121
Training time: 173.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 49.70666666666652
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.8469866238343818
Weighted AreaUnderROC: 0.9564028626572257
Root mean squared error: 0.2869872575894763
Relative absolute error: 30.53075517442159
Root relative squared error: 60.87919078669461
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7622427877828039
Weighted FMeasure: 0.8349825680238296
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9161298132592608
Mean absolute error: 0.13569224521965215
Coverage of cases: 96.52
Instances selection time: 34.0
Test time: 71.0
Accumulative iteration time: 5407.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08115039417786148
Kappa statistic: 0.7559034188395063
Training time: 153.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 49.73333333333322
Incorrectly Classified Instances: 15.6
Correctly Classified Instances: 84.4
Weighted Precision: 0.8538933938612691
Weighted AreaUnderROC: 0.9573580336108782
Root mean squared error: 0.2846426671642283
Relative absolute error: 30.206597906769034
Root relative squared error: 60.38182805005523
Weighted TruePositiveRate: 0.844
Weighted MatthewsCorrelation: 0.7723317298391268
Weighted FMeasure: 0.8422280039635792
Iteration time: 188.0
Weighted AreaUnderPRC: 0.9179445030918819
Mean absolute error: 0.13425154625230745
Coverage of cases: 96.6
Instances selection time: 34.0
Test time: 73.0
Accumulative iteration time: 5595.0
Weighted Recall: 0.844
Weighted FalsePositiveRate: 0.07777634350262315
Kappa statistic: 0.7660852946581558
Training time: 154.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.98666666666656
Incorrectly Classified Instances: 15.8
Correctly Classified Instances: 84.2
Weighted Precision: 0.8516544526461602
Weighted AreaUnderROC: 0.9570341882787241
Root mean squared error: 0.2847501246230338
Relative absolute error: 30.435235719646915
Root relative squared error: 60.40462321939837
Weighted TruePositiveRate: 0.842
Weighted MatthewsCorrelation: 0.7692423450606738
Weighted FMeasure: 0.8399878442681986
Iteration time: 192.0
Weighted AreaUnderPRC: 0.9173686626380319
Mean absolute error: 0.13526771430954249
Coverage of cases: 96.68
Instances selection time: 33.0
Test time: 68.0
Accumulative iteration time: 5787.0
Weighted Recall: 0.842
Weighted FalsePositiveRate: 0.07884845248508375
Kappa statistic: 0.7630647875644881
Training time: 159.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 50.27999999999985
Incorrectly Classified Instances: 15.88
Correctly Classified Instances: 84.12
Weighted Precision: 0.8499137631733081
Weighted AreaUnderROC: 0.9567738872037731
Root mean squared error: 0.28363271611585855
Relative absolute error: 30.57598952581613
Root relative squared error: 60.167585079564624
Weighted TruePositiveRate: 0.8412
Weighted MatthewsCorrelation: 0.7674335404804515
Weighted FMeasure: 0.8392933449435672
Iteration time: 195.0
Weighted AreaUnderPRC: 0.9152988121047503
Mean absolute error: 0.13589328678140566
Coverage of cases: 96.8
Instances selection time: 32.0
Test time: 67.0
Accumulative iteration time: 5982.0
Weighted Recall: 0.8412
Weighted FalsePositiveRate: 0.07927573960031449
Kappa statistic: 0.7618594017908172
Training time: 163.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 50.386666666666514
Incorrectly Classified Instances: 15.8
Correctly Classified Instances: 84.2
Weighted Precision: 0.8499877474206844
Weighted AreaUnderROC: 0.9570578759398719
Root mean squared error: 0.28246297436416057
Relative absolute error: 30.53976271434903
Root relative squared error: 59.91944538210582
Weighted TruePositiveRate: 0.842
Weighted MatthewsCorrelation: 0.7682078632019256
Weighted FMeasure: 0.8400700083138684
Iteration time: 203.0
Weighted AreaUnderPRC: 0.9158755733592345
Mean absolute error: 0.13573227873044078
Coverage of cases: 96.8
Instances selection time: 32.0
Test time: 69.0
Accumulative iteration time: 6185.0
Weighted Recall: 0.842
Weighted FalsePositiveRate: 0.07889837918322448
Kappa statistic: 0.7630519390149678
Training time: 171.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 50.706666666666514
Incorrectly Classified Instances: 15.84
Correctly Classified Instances: 84.16
Weighted Precision: 0.8491965462123295
Weighted AreaUnderROC: 0.9579441713939731
Root mean squared error: 0.2805947436287914
Relative absolute error: 30.63663435148272
Root relative squared error: 59.52313379556562
Weighted TruePositiveRate: 0.8416
Weighted MatthewsCorrelation: 0.7673492327738565
Weighted FMeasure: 0.8395510398715738
Iteration time: 199.0
Weighted AreaUnderPRC: 0.9173080919293186
Mean absolute error: 0.13616281933992383
Coverage of cases: 97.08
Instances selection time: 30.0
Test time: 66.0
Accumulative iteration time: 6384.0
Weighted Recall: 0.8416
Weighted FalsePositiveRate: 0.0791636969554991
Kappa statistic: 0.7624300668507392
Training time: 169.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 50.853333333333204
Incorrectly Classified Instances: 15.88
Correctly Classified Instances: 84.12
Weighted Precision: 0.8491306123676166
Weighted AreaUnderROC: 0.9580912217996802
Root mean squared error: 0.2802888967095257
Relative absolute error: 30.611356842697102
Root relative squared error: 59.45825386638029
Weighted TruePositiveRate: 0.8412
Weighted MatthewsCorrelation: 0.7668700777970654
Weighted FMeasure: 0.8392403715244884
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9173477855886333
Mean absolute error: 0.1360504748564322
Coverage of cases: 97.24
Instances selection time: 30.0
Test time: 66.0
Accumulative iteration time: 6588.0
Weighted Recall: 0.8412
Weighted FalsePositiveRate: 0.07938345314876849
Kappa statistic: 0.7618233984693343
Training time: 174.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 50.38666666666656
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8583664028502673
Weighted AreaUnderROC: 0.9601441433365911
Root mean squared error: 0.2761845242623409
Relative absolute error: 29.78819687789764
Root relative squared error: 58.58758498940441
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7801105402975863
Weighted FMeasure: 0.8485101449710742
Iteration time: 208.0
Weighted AreaUnderPRC: 0.9209233132356767
Mean absolute error: 0.13239198612399014
Coverage of cases: 97.0
Instances selection time: 30.0
Test time: 69.0
Accumulative iteration time: 6796.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07500016001980679
Kappa statistic: 0.7750111954429149
Training time: 178.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 50.46666666666659
Incorrectly Classified Instances: 15.76
Correctly Classified Instances: 84.24
Weighted Precision: 0.8501187057190845
Weighted AreaUnderROC: 0.9588642571247056
Root mean squared error: 0.2787170490631526
Relative absolute error: 30.106421670624773
Root relative squared error: 59.124814627457525
Weighted TruePositiveRate: 0.8424
Weighted MatthewsCorrelation: 0.7684221595415304
Weighted FMeasure: 0.8404430209875201
Iteration time: 217.0
Weighted AreaUnderPRC: 0.9182842182176206
Mean absolute error: 0.13380631853611072
Coverage of cases: 96.92
Instances selection time: 32.0
Test time: 67.0
Accumulative iteration time: 7013.0
Weighted Recall: 0.8424
Weighted FalsePositiveRate: 0.07886436433901302
Kappa statistic: 0.7635984870303171
Training time: 185.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 50.6666666666666
Incorrectly Classified Instances: 15.04
Correctly Classified Instances: 84.96
Weighted Precision: 0.8574743050611654
Weighted AreaUnderROC: 0.9595198266358546
Root mean squared error: 0.2771792290840753
Relative absolute error: 30.007294925066773
Root relative squared error: 58.798593746822604
Weighted TruePositiveRate: 0.8496
Weighted MatthewsCorrelation: 0.7792491754977202
Weighted FMeasure: 0.847955798731621
Iteration time: 345.0
Weighted AreaUnderPRC: 0.9195627959441716
Mean absolute error: 0.13336575522251962
Coverage of cases: 97.04
Instances selection time: 28.0
Test time: 67.0
Accumulative iteration time: 7358.0
Weighted Recall: 0.8496
Weighted FalsePositiveRate: 0.07523007556992402
Kappa statistic: 0.7744031042132861
Training time: 317.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 50.506666666666554
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8569814809662406
Weighted AreaUnderROC: 0.959362711304686
Root mean squared error: 0.2775962600097357
Relative absolute error: 29.894820126717377
Root relative squared error: 58.8870593654723
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7780681805468418
Weighted FMeasure: 0.8472888968990159
Iteration time: 203.0
Weighted AreaUnderPRC: 0.9187323306768762
Mean absolute error: 0.13286586722985563
Coverage of cases: 96.88
Instances selection time: 31.0
Test time: 67.0
Accumulative iteration time: 7561.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07569490858517738
Kappa statistic: 0.7731865726451006
Training time: 172.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 50.39999999999995
Incorrectly Classified Instances: 14.92
Correctly Classified Instances: 85.08
Weighted Precision: 0.859343595157514
Weighted AreaUnderROC: 0.9587085132196164
Root mean squared error: 0.2786622128624993
Relative absolute error: 29.91409164460545
Root relative squared error: 59.11318211265659
Weighted TruePositiveRate: 0.8508
Weighted MatthewsCorrelation: 0.781252180147384
Weighted FMeasure: 0.8491465290388969
Iteration time: 226.0
Weighted AreaUnderPRC: 0.917424106848511
Mean absolute error: 0.1329515184204693
Coverage of cases: 96.88
Instances selection time: 30.0
Test time: 67.0
Accumulative iteration time: 7787.0
Weighted Recall: 0.8508
Weighted FalsePositiveRate: 0.0747268625913237
Kappa statistic: 0.776175146488266
Training time: 196.0
		
Time end:Fri Dec 01 14.00.39 EET 2017