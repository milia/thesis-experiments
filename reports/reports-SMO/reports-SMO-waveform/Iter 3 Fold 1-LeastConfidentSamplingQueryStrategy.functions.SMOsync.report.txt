Fri Dec 01 13.53.25 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.53.25 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 51.09333333333324
Incorrectly Classified Instances: 17.0
Correctly Classified Instances: 83.0
Weighted Precision: 0.8299924254852508
Weighted AreaUnderROC: 0.9556351721807245
Root mean squared error: 0.28526379993580103
Relative absolute error: 31.40029950298669
Root relative squared error: 60.513590208494115
Weighted TruePositiveRate: 0.83
Weighted MatthewsCorrelation: 0.7449244594211395
Weighted FMeasure: 0.8299960108301071
Iteration time: 219.0
Weighted AreaUnderPRC: 0.9137700716054902
Mean absolute error: 0.1395568866799415
Coverage of cases: 97.8
Instances selection time: 48.0
Test time: 67.0
Accumulative iteration time: 219.0
Weighted Recall: 0.83
Weighted FalsePositiveRate: 0.08507304347739532
Kappa statistic: 0.7449850255206985
Training time: 171.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 50.773333333333255
Incorrectly Classified Instances: 15.96
Correctly Classified Instances: 84.04
Weighted Precision: 0.8403418572238958
Weighted AreaUnderROC: 0.9587658327464269
Root mean squared error: 0.27948783330488813
Relative absolute error: 30.308760299411762
Root relative squared error: 59.2883226567064
Weighted TruePositiveRate: 0.8404
Weighted MatthewsCorrelation: 0.7605344153842164
Weighted FMeasure: 0.8403448606184173
Iteration time: 110.0
Weighted AreaUnderPRC: 0.9198823856482397
Mean absolute error: 0.13470560133071957
Coverage of cases: 97.88
Instances selection time: 50.0
Test time: 70.0
Accumulative iteration time: 329.0
Weighted Recall: 0.8404
Weighted FalsePositiveRate: 0.0798323178808185
Kappa statistic: 0.760593756285164
Training time: 60.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 50.4
Incorrectly Classified Instances: 16.24
Correctly Classified Instances: 83.76
Weighted Precision: 0.8376273368944014
Weighted AreaUnderROC: 0.958672142490539
Root mean squared error: 0.28105226408472733
Relative absolute error: 30.514105500284554
Root relative squared error: 59.62018854064278
Weighted TruePositiveRate: 0.8376
Weighted MatthewsCorrelation: 0.7563607078608711
Weighted FMeasure: 0.8375783489792317
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9198070337571326
Mean absolute error: 0.13561824666793199
Coverage of cases: 97.8
Instances selection time: 52.0
Test time: 69.0
Accumulative iteration time: 442.0
Weighted Recall: 0.8376
Weighted FalsePositiveRate: 0.08129266119650536
Kappa statistic: 0.7563735324205622
Training time: 61.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 49.453333333333305
Incorrectly Classified Instances: 15.52
Correctly Classified Instances: 84.48
Weighted Precision: 0.8448947291511689
Weighted AreaUnderROC: 0.9607692004621067
Root mean squared error: 0.27667947723007896
Relative absolute error: 29.087120875115268
Root relative squared error: 58.6925803693612
Weighted TruePositiveRate: 0.8448
Weighted MatthewsCorrelation: 0.7671459647883497
Weighted FMeasure: 0.8447968042633808
Iteration time: 115.0
Weighted AreaUnderPRC: 0.9233566790042225
Mean absolute error: 0.1292760927782907
Coverage of cases: 97.6
Instances selection time: 51.0
Test time: 73.0
Accumulative iteration time: 557.0
Weighted Recall: 0.8448
Weighted FalsePositiveRate: 0.07774147990644419
Kappa statistic: 0.767173364632914
Training time: 64.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 49.346666666666586
Incorrectly Classified Instances: 15.16
Correctly Classified Instances: 84.84
Weighted Precision: 0.8483192884362492
Weighted AreaUnderROC: 0.961545126729846
Root mean squared error: 0.274549753247606
Relative absolute error: 28.869991818415073
Root relative squared error: 58.24079768834253
Weighted TruePositiveRate: 0.8484
Weighted MatthewsCorrelation: 0.7725129963299714
Weighted FMeasure: 0.8483277057912814
Iteration time: 115.0
Weighted AreaUnderPRC: 0.9253219609109914
Mean absolute error: 0.12831107474851203
Coverage of cases: 97.72
Instances selection time: 51.0
Test time: 72.0
Accumulative iteration time: 672.0
Weighted Recall: 0.8484
Weighted FalsePositiveRate: 0.07583511540721781
Kappa statistic: 0.7725984354772362
Training time: 64.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 48.853333333333346
Incorrectly Classified Instances: 15.4
Correctly Classified Instances: 84.6
Weighted Precision: 0.846002854141429
Weighted AreaUnderROC: 0.9617462141753756
Root mean squared error: 0.2750371067776662
Relative absolute error: 28.489765733466818
Root relative squared error: 58.34418098412476
Weighted TruePositiveRate: 0.846
Weighted MatthewsCorrelation: 0.7689537787974792
Weighted FMeasure: 0.845976459319249
Iteration time: 118.0
Weighted AreaUnderPRC: 0.9255711718565272
Mean absolute error: 0.1266211810376309
Coverage of cases: 97.56
Instances selection time: 52.0
Test time: 75.0
Accumulative iteration time: 790.0
Weighted Recall: 0.846
Weighted FalsePositiveRate: 0.07702005935718724
Kappa statistic: 0.7689996303994088
Training time: 66.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 48.906666666666666
Incorrectly Classified Instances: 15.0
Correctly Classified Instances: 85.0
Weighted Precision: 0.8500286770459424
Weighted AreaUnderROC: 0.9630175057635297
Root mean squared error: 0.27162854127817937
Relative absolute error: 28.16491277068548
Root relative squared error: 57.62111505048307
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.7749833586890099
Weighted FMeasure: 0.8499408995863436
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9277040512964247
Mean absolute error: 0.12517739009193604
Coverage of cases: 97.64
Instances selection time: 51.0
Test time: 75.0
Accumulative iteration time: 909.0
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.07498998597431578
Kappa statistic: 0.775009035637129
Training time: 68.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 48.906666666666695
Incorrectly Classified Instances: 14.88
Correctly Classified Instances: 85.12
Weighted Precision: 0.8512475785539484
Weighted AreaUnderROC: 0.9640370942885296
Root mean squared error: 0.2694344699234509
Relative absolute error: 27.89628680182715
Root relative squared error: 57.15568223048236
Weighted TruePositiveRate: 0.8512
Weighted MatthewsCorrelation: 0.7767776174401242
Weighted FMeasure: 0.8511736464198173
Iteration time: 121.0
Weighted AreaUnderPRC: 0.9299224992703923
Mean absolute error: 0.12398349689701013
Coverage of cases: 97.76
Instances selection time: 49.0
Test time: 78.0
Accumulative iteration time: 1030.0
Weighted Recall: 0.8512
Weighted FalsePositiveRate: 0.07442176278911301
Kappa statistic: 0.7767995357430343
Training time: 72.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 48.76000000000003
Incorrectly Classified Instances: 14.72
Correctly Classified Instances: 85.28
Weighted Precision: 0.852801839652414
Weighted AreaUnderROC: 0.9640676674488561
Root mean squared error: 0.2691555736008309
Relative absolute error: 27.79652334188783
Root relative squared error: 57.0965193861906
Weighted TruePositiveRate: 0.8528
Weighted MatthewsCorrelation: 0.7791582635011087
Weighted FMeasure: 0.8527343944735909
Iteration time: 124.0
Weighted AreaUnderPRC: 0.9304906861028185
Mean absolute error: 0.12354010374172428
Coverage of cases: 97.76
Instances selection time: 49.0
Test time: 76.0
Accumulative iteration time: 1154.0
Weighted Recall: 0.8528
Weighted FalsePositiveRate: 0.07361275013765678
Kappa statistic: 0.7792053697254083
Training time: 75.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 48.95999999999999
Incorrectly Classified Instances: 15.2
Correctly Classified Instances: 84.8
Weighted Precision: 0.8481176137205059
Weighted AreaUnderROC: 0.9614333968179948
Root mean squared error: 0.27352150059395003
Relative absolute error: 28.418090762424555
Root relative squared error: 58.02267236109057
Weighted TruePositiveRate: 0.848
Weighted MatthewsCorrelation: 0.7720543333778325
Weighted FMeasure: 0.8478743055565974
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9251828541212647
Mean absolute error: 0.1263026256107764
Coverage of cases: 97.8
Instances selection time: 61.0
Test time: 74.0
Accumulative iteration time: 1293.0
Weighted Recall: 0.848
Weighted FalsePositiveRate: 0.07595581052689918
Kappa statistic: 0.7720214117490085
Training time: 78.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 48.96
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.854080253343319
Weighted AreaUnderROC: 0.961921880285125
Root mean squared error: 0.27271788288768134
Relative absolute error: 28.365739177490138
Root relative squared error: 57.852199302215325
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7810222862806926
Weighted FMeasure: 0.8538790339216237
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9268497126091192
Mean absolute error: 0.12606995189995676
Coverage of cases: 97.8
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 1418.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07297070874417555
Kappa statistic: 0.7810187798294419
Training time: 78.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 49.106666666666705
Incorrectly Classified Instances: 14.88
Correctly Classified Instances: 85.12
Weighted Precision: 0.8512282744436801
Weighted AreaUnderROC: 0.9615650925725642
Root mean squared error: 0.2734387525374528
Relative absolute error: 28.4819868477589
Root relative squared error: 58.005118847526816
Weighted TruePositiveRate: 0.8512
Weighted MatthewsCorrelation: 0.7767875536837031
Weighted FMeasure: 0.8511520518954728
Iteration time: 125.0
Weighted AreaUnderPRC: 0.9261613980081084
Mean absolute error: 0.12658660821226236
Coverage of cases: 97.6
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 1543.0
Weighted Recall: 0.8512
Weighted FalsePositiveRate: 0.07438556824426219
Kappa statistic: 0.7768075706872022
Training time: 78.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 48.65333333333332
Incorrectly Classified Instances: 14.36
Correctly Classified Instances: 85.64
Weighted Precision: 0.8565897978553297
Weighted AreaUnderROC: 0.9627882549906428
Root mean squared error: 0.2714443775848821
Relative absolute error: 27.877071078582386
Root relative squared error: 57.58204803156941
Weighted TruePositiveRate: 0.8564
Weighted MatthewsCorrelation: 0.7846999480319391
Weighted FMeasure: 0.8563326438382354
Iteration time: 130.0
Weighted AreaUnderPRC: 0.9286258616583722
Mean absolute error: 0.12389809368258897
Coverage of cases: 97.44
Instances selection time: 50.0
Test time: 75.0
Accumulative iteration time: 1673.0
Weighted Recall: 0.8564
Weighted FalsePositiveRate: 0.07173162850373994
Kappa statistic: 0.7846192981108894
Training time: 80.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 48.58666666666659
Incorrectly Classified Instances: 14.16
Correctly Classified Instances: 85.84
Weighted Precision: 0.8586031362380587
Weighted AreaUnderROC: 0.9620048612168263
Root mean squared error: 0.2724464515399646
Relative absolute error: 28.063228578051444
Root relative squared error: 57.79462001823619
Weighted TruePositiveRate: 0.8584
Weighted MatthewsCorrelation: 0.7877105211216981
Weighted FMeasure: 0.858287664588772
Iteration time: 139.0
Weighted AreaUnderPRC: 0.926952982521952
Mean absolute error: 0.1247254603468959
Coverage of cases: 97.44
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 1812.0
Weighted Recall: 0.8584
Weighted FalsePositiveRate: 0.07073032396559413
Kappa statistic: 0.7876233104654434
Training time: 92.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 48.91999999999997
Incorrectly Classified Instances: 14.6
Correctly Classified Instances: 85.4
Weighted Precision: 0.8542712426035504
Weighted AreaUnderROC: 0.9629205819898254
Root mean squared error: 0.2702982671486818
Relative absolute error: 27.97350061515261
Root relative squared error: 57.338921293141645
Weighted TruePositiveRate: 0.854
Weighted MatthewsCorrelation: 0.7811530718455392
Weighted FMeasure: 0.8539340526139613
Iteration time: 147.0
Weighted AreaUnderPRC: 0.9286926901801735
Mean absolute error: 0.12432666940067884
Coverage of cases: 97.48
Instances selection time: 47.0
Test time: 74.0
Accumulative iteration time: 1959.0
Weighted Recall: 0.854
Weighted FalsePositiveRate: 0.07291149277528546
Kappa statistic: 0.7810232991209733
Training time: 100.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 49.2933333333332
Incorrectly Classified Instances: 15.12
Correctly Classified Instances: 84.88
Weighted Precision: 0.8490057014345104
Weighted AreaUnderROC: 0.9626378776421647
Root mean squared error: 0.27165925868837293
Relative absolute error: 28.426091570668774
Root relative squared error: 57.62763119719757
Weighted TruePositiveRate: 0.8488
Weighted MatthewsCorrelation: 0.7733022374054195
Weighted FMeasure: 0.8487451438274853
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9279514485669966
Mean absolute error: 0.12633818475852848
Coverage of cases: 97.8
Instances selection time: 47.0
Test time: 71.0
Accumulative iteration time: 2098.0
Weighted Recall: 0.8488
Weighted FalsePositiveRate: 0.07553876053192134
Kappa statistic: 0.7732189044721232
Training time: 92.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 49.0533333333333
Incorrectly Classified Instances: 14.64
Correctly Classified Instances: 85.36
Weighted Precision: 0.8541516487452505
Weighted AreaUnderROC: 0.9643471263808994
Root mean squared error: 0.26811182339000694
Relative absolute error: 27.789622838365265
Root relative squared error: 56.87510653060904
Weighted TruePositiveRate: 0.8536
Weighted MatthewsCorrelation: 0.7807146925645405
Weighted FMeasure: 0.8535249467891491
Iteration time: 146.0
Weighted AreaUnderPRC: 0.9315532674267817
Mean absolute error: 0.12350943483717954
Coverage of cases: 97.76
Instances selection time: 49.0
Test time: 73.0
Accumulative iteration time: 2244.0
Weighted Recall: 0.8536
Weighted FalsePositiveRate: 0.07308727203493649
Kappa statistic: 0.7804305289392564
Training time: 97.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 49.0133333333333
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8650260829765779
Weighted AreaUnderROC: 0.9637760917686566
Root mean squared error: 0.2676686227110772
Relative absolute error: 27.66368581673808
Root relative squared error: 56.781089468959735
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7973351496906174
Weighted FMeasure: 0.8646451485359096
Iteration time: 158.0
Weighted AreaUnderPRC: 0.9296869858579304
Mean absolute error: 0.12294971474105872
Coverage of cases: 97.72
Instances selection time: 58.0
Test time: 72.0
Accumulative iteration time: 2402.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06752618054888951
Kappa statistic: 0.7972243006398113
Training time: 100.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 49.78666666666663
Incorrectly Classified Instances: 14.2
Correctly Classified Instances: 85.8
Weighted Precision: 0.8584100725566152
Weighted AreaUnderROC: 0.9643897000724916
Root mean squared error: 0.2659354015278143
Relative absolute error: 27.928100800802547
Root relative squared error: 56.41341773336532
Weighted TruePositiveRate: 0.858
Weighted MatthewsCorrelation: 0.7872406312252338
Weighted FMeasure: 0.8578374281125071
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9312031850521809
Mean absolute error: 0.1241248924480119
Coverage of cases: 98.04
Instances selection time: 42.0
Test time: 70.0
Accumulative iteration time: 2543.0
Weighted Recall: 0.858
Weighted FalsePositiveRate: 0.0708945147498205
Kappa statistic: 0.7870348240655688
Training time: 99.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 48.7066666666666
Incorrectly Classified Instances: 13.76
Correctly Classified Instances: 86.24
Weighted Precision: 0.862716208672064
Weighted AreaUnderROC: 0.9656038908256124
Root mean squared error: 0.2641249436931854
Relative absolute error: 27.05694671946575
Root relative squared error: 56.029361629789804
Weighted TruePositiveRate: 0.8624
Weighted MatthewsCorrelation: 0.7937818757496019
Weighted FMeasure: 0.8622652378119954
Iteration time: 140.0
Weighted AreaUnderPRC: 0.9334026349233902
Mean absolute error: 0.12025309653095945
Coverage of cases: 97.92
Instances selection time: 47.0
Test time: 75.0
Accumulative iteration time: 2683.0
Weighted Recall: 0.8624
Weighted FalsePositiveRate: 0.06870826626170526
Kappa statistic: 0.7936287929108131
Training time: 93.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 49.22666666666659
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.861478741630366
Weighted AreaUnderROC: 0.9634716196702319
Root mean squared error: 0.2686241280919936
Relative absolute error: 27.86509320947095
Root relative squared error: 56.9837827692516
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.7919543308822229
Weighted FMeasure: 0.8610681187022948
Iteration time: 153.0
Weighted AreaUnderPRC: 0.9290223078684685
Mean absolute error: 0.12384485870876036
Coverage of cases: 98.04
Instances selection time: 46.0
Test time: 73.0
Accumulative iteration time: 2836.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06931990734804801
Kappa statistic: 0.7918274454695893
Training time: 107.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 49.6266666666666
Incorrectly Classified Instances: 13.72
Correctly Classified Instances: 86.28
Weighted Precision: 0.8630929831880494
Weighted AreaUnderROC: 0.9651000797710833
Root mean squared error: 0.2637349803362056
Relative absolute error: 27.5506155856166
Root relative squared error: 55.9466379095494
Weighted TruePositiveRate: 0.8628
Weighted MatthewsCorrelation: 0.7943771738204611
Weighted FMeasure: 0.8625964504845001
Iteration time: 159.0
Weighted AreaUnderPRC: 0.9317514780807589
Mean absolute error: 0.12244718038051881
Coverage of cases: 98.2
Instances selection time: 44.0
Test time: 69.0
Accumulative iteration time: 2995.0
Weighted Recall: 0.8628
Weighted FalsePositiveRate: 0.06851375293374543
Kappa statistic: 0.7942317706146171
Training time: 115.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 48.719999999999956
Incorrectly Classified Instances: 13.88
Correctly Classified Instances: 86.12
Weighted Precision: 0.8614648157745279
Weighted AreaUnderROC: 0.9667150823928161
Root mean squared error: 0.2619895422511813
Relative absolute error: 26.774703543098802
Root relative squared error: 55.57637457773082
Weighted TruePositiveRate: 0.8612
Weighted MatthewsCorrelation: 0.791952555287491
Weighted FMeasure: 0.8610558204259494
Iteration time: 152.0
Weighted AreaUnderPRC: 0.9353966921659219
Mean absolute error: 0.11899868241377301
Coverage of cases: 98.04
Instances selection time: 47.0
Test time: 72.0
Accumulative iteration time: 3147.0
Weighted Recall: 0.8612
Weighted FalsePositiveRate: 0.06931697134748933
Kappa statistic: 0.7918277451981199
Training time: 105.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 49.51999999999997
Incorrectly Classified Instances: 13.68
Correctly Classified Instances: 86.32
Weighted Precision: 0.8633845329321038
Weighted AreaUnderROC: 0.9654456381697383
Root mean squared error: 0.2623066407288933
Relative absolute error: 27.375438757600957
Root relative squared error: 55.64364132289904
Weighted TruePositiveRate: 0.8632
Weighted MatthewsCorrelation: 0.7949056894773178
Weighted FMeasure: 0.8630155729645465
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9323407386193215
Mean absolute error: 0.12166861670044928
Coverage of cases: 98.0
Instances selection time: 43.0
Test time: 71.0
Accumulative iteration time: 3298.0
Weighted Recall: 0.8632
Weighted FalsePositiveRate: 0.06833376408176892
Kappa statistic: 0.7948273454114035
Training time: 108.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 48.719999999999956
Incorrectly Classified Instances: 13.36
Correctly Classified Instances: 86.64
Weighted Precision: 0.8665932381168292
Weighted AreaUnderROC: 0.9670793165510506
Root mean squared error: 0.2598677364744169
Relative absolute error: 26.44987237410009
Root relative squared error: 55.12627160179754
Weighted TruePositiveRate: 0.8664
Weighted MatthewsCorrelation: 0.7997182326531638
Weighted FMeasure: 0.8661787729692991
Iteration time: 153.0
Weighted AreaUnderPRC: 0.9358762532053552
Mean absolute error: 0.11755498832933428
Coverage of cases: 98.24
Instances selection time: 46.0
Test time: 75.0
Accumulative iteration time: 3451.0
Weighted Recall: 0.8664
Weighted FalsePositiveRate: 0.06673526516985868
Kappa statistic: 0.7996277635770785
Training time: 107.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 49.000000000000036
Incorrectly Classified Instances: 13.48
Correctly Classified Instances: 86.52
Weighted Precision: 0.8655653173346552
Weighted AreaUnderROC: 0.9669043111592817
Root mean squared error: 0.26084733554640116
Relative absolute error: 26.966301190684632
Root relative squared error: 55.33407594579078
Weighted TruePositiveRate: 0.8652
Weighted MatthewsCorrelation: 0.7980237062251158
Weighted FMeasure: 0.8649522639622679
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9358655927824886
Mean absolute error: 0.11985022751415447
Coverage of cases: 98.48
Instances selection time: 44.0
Test time: 78.0
Accumulative iteration time: 3608.0
Weighted Recall: 0.8652
Weighted FalsePositiveRate: 0.06730356611846645
Kappa statistic: 0.7978365511515517
Training time: 113.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 49.47999999999995
Incorrectly Classified Instances: 13.56
Correctly Classified Instances: 86.44
Weighted Precision: 0.8647932925298872
Weighted AreaUnderROC: 0.9665508813894413
Root mean squared error: 0.2608131965414284
Relative absolute error: 27.224224462673217
Root relative squared error: 55.32683396921502
Weighted TruePositiveRate: 0.8644
Weighted MatthewsCorrelation: 0.7968368257301108
Weighted FMeasure: 0.8641800513677612
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9353965363615212
Mean absolute error: 0.12099655316743708
Coverage of cases: 98.44
Instances selection time: 44.0
Test time: 71.0
Accumulative iteration time: 3768.0
Weighted Recall: 0.8644
Weighted FalsePositiveRate: 0.06769902380798187
Kappa statistic: 0.7966359872956882
Training time: 116.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 49.50666666666667
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8651915253444477
Weighted AreaUnderROC: 0.9656962484579775
Root mean squared error: 0.2619328888893118
Relative absolute error: 27.361010310019747
Root relative squared error: 55.56435658482433
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7974359083797078
Weighted FMeasure: 0.8646005246543326
Iteration time: 163.0
Weighted AreaUnderPRC: 0.9333777484484687
Mean absolute error: 0.12160449026675502
Coverage of cases: 98.16
Instances selection time: 44.0
Test time: 76.0
Accumulative iteration time: 3931.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06749657256444445
Kappa statistic: 0.7972344214846088
Training time: 119.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 48.999999999999986
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8652318207276342
Weighted AreaUnderROC: 0.9674498207142233
Root mean squared error: 0.2593952370039061
Relative absolute error: 27.07526349469406
Root relative squared error: 55.02603932788597
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7974645151515432
Weighted FMeasure: 0.8645829540376868
Iteration time: 161.0
Weighted AreaUnderPRC: 0.9367304617115324
Mean absolute error: 0.12033450442086305
Coverage of cases: 98.28
Instances selection time: 43.0
Test time: 74.0
Accumulative iteration time: 4092.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06749149288602053
Kappa statistic: 0.79723578382887
Training time: 118.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 49.213333333333395
Incorrectly Classified Instances: 13.2
Correctly Classified Instances: 86.8
Weighted Precision: 0.8683243252843913
Weighted AreaUnderROC: 0.9682095789504929
Root mean squared error: 0.25781145645298
Relative absolute error: 26.962161191207223
Root relative squared error: 54.6900687376446
Weighted TruePositiveRate: 0.868
Weighted MatthewsCorrelation: 0.8021943004449331
Weighted FMeasure: 0.867808250128898
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9383579691512861
Mean absolute error: 0.11983182751647711
Coverage of cases: 98.28
Instances selection time: 42.0
Test time: 71.0
Accumulative iteration time: 4258.0
Weighted Recall: 0.868
Weighted FalsePositiveRate: 0.06590966212795889
Kappa statistic: 0.8020304714698313
Training time: 124.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 49.05333333333335
Incorrectly Classified Instances: 13.36
Correctly Classified Instances: 86.64
Weighted Precision: 0.866837995960535
Weighted AreaUnderROC: 0.9679988070540685
Root mean squared error: 0.25807706356647203
Relative absolute error: 26.769087980171506
Root relative squared error: 54.74641251496909
Weighted TruePositiveRate: 0.8664
Weighted MatthewsCorrelation: 0.7998777117938023
Weighted FMeasure: 0.8661032185410029
Iteration time: 180.0
Weighted AreaUnderPRC: 0.9377714731365517
Mean absolute error: 0.11897372435631835
Coverage of cases: 98.2
Instances selection time: 44.0
Test time: 71.0
Accumulative iteration time: 4438.0
Weighted Recall: 0.8664
Weighted FalsePositiveRate: 0.0666912104728353
Kappa statistic: 0.7996403604457917
Training time: 136.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 48.75999999999997
Incorrectly Classified Instances: 13.08
Correctly Classified Instances: 86.92
Weighted Precision: 0.8695700732442152
Weighted AreaUnderROC: 0.9683494806202915
Root mean squared error: 0.25719512443555564
Relative absolute error: 26.397193306472094
Root relative squared error: 54.55932497294967
Weighted TruePositiveRate: 0.8692
Weighted MatthewsCorrelation: 0.8040197990459003
Weighted FMeasure: 0.8690190878348063
Iteration time: 173.0
Weighted AreaUnderPRC: 0.9384954640558426
Mean absolute error: 0.11732085913987651
Coverage of cases: 98.24
Instances selection time: 45.0
Test time: 76.0
Accumulative iteration time: 4611.0
Weighted Recall: 0.8692
Weighted FalsePositiveRate: 0.0653002424828864
Kappa statistic: 0.8038316066515362
Training time: 128.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 48.93333333333327
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8658232740353439
Weighted AreaUnderROC: 0.9680979557677052
Root mean squared error: 0.2582052061049161
Relative absolute error: 26.67058076812574
Root relative squared error: 54.773595652336766
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.7985274165390952
Weighted FMeasure: 0.865420108796228
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9382175762225446
Mean absolute error: 0.11853591452500384
Coverage of cases: 98.28
Instances selection time: 43.0
Test time: 73.0
Accumulative iteration time: 4780.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06712786462222044
Kappa statistic: 0.7984279298260433
Training time: 126.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 48.82666666666663
Incorrectly Classified Instances: 13.52
Correctly Classified Instances: 86.48
Weighted Precision: 0.8652021054263324
Weighted AreaUnderROC: 0.9679213467113487
Root mean squared error: 0.2587516365189986
Relative absolute error: 26.73058253378563
Root relative squared error: 54.88951104771005
Weighted TruePositiveRate: 0.8648
Weighted MatthewsCorrelation: 0.7974485302252451
Weighted FMeasure: 0.8645566360446013
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9381971288406983
Mean absolute error: 0.1188025890390478
Coverage of cases: 98.2
Instances selection time: 43.0
Test time: 71.0
Accumulative iteration time: 4955.0
Weighted Recall: 0.8648
Weighted FalsePositiveRate: 0.06749514344586761
Kappa statistic: 0.7972372434631207
Training time: 132.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 48.919999999999916
Incorrectly Classified Instances: 13.28
Correctly Classified Instances: 86.72
Weighted Precision: 0.867553917919735
Weighted AreaUnderROC: 0.9676801313456399
Root mean squared error: 0.25904251121343075
Relative absolute error: 26.79175321552489
Root relative squared error: 54.95121488838262
Weighted TruePositiveRate: 0.8672
Weighted MatthewsCorrelation: 0.8010134775380333
Weighted FMeasure: 0.8669966073810128
Iteration time: 182.0
Weighted AreaUnderPRC: 0.9373894384764684
Mean absolute error: 0.11907445873566672
Coverage of cases: 98.12
Instances selection time: 42.0
Test time: 73.0
Accumulative iteration time: 5137.0
Weighted Recall: 0.8672
Weighted FalsePositiveRate: 0.06630297613960905
Kappa statistic: 0.8008328546123031
Training time: 140.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 49.01333333333327
Incorrectly Classified Instances: 13.32
Correctly Classified Instances: 86.68
Weighted Precision: 0.8673344638006011
Weighted AreaUnderROC: 0.9678920986093753
Root mean squared error: 0.25836051053951614
Relative absolute error: 26.786494773479873
Root relative squared error: 54.80654069799297
Weighted TruePositiveRate: 0.8668
Weighted MatthewsCorrelation: 0.8005180137551426
Weighted FMeasure: 0.8666367479674797
Iteration time: 186.0
Weighted AreaUnderPRC: 0.9378732116916454
Mean absolute error: 0.11905108788213334
Coverage of cases: 98.32
Instances selection time: 43.0
Test time: 72.0
Accumulative iteration time: 5323.0
Weighted Recall: 0.8668
Weighted FalsePositiveRate: 0.06647288339814274
Kappa statistic: 0.8002359255711453
Training time: 143.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 49.06666666666665
Incorrectly Classified Instances: 13.24
Correctly Classified Instances: 86.76
Weighted Precision: 0.8679810058143037
Weighted AreaUnderROC: 0.9682659104137511
Root mean squared error: 0.2572516215631194
Relative absolute error: 26.74904270107796
Root relative squared error: 54.57130982355503
Weighted TruePositiveRate: 0.8676
Weighted MatthewsCorrelation: 0.8016300628033456
Weighted FMeasure: 0.8673938444757874
Iteration time: 188.0
Weighted AreaUnderPRC: 0.93847439583797
Mean absolute error: 0.11888463422701373
Coverage of cases: 98.24
Instances selection time: 41.0
Test time: 71.0
Accumulative iteration time: 5511.0
Weighted Recall: 0.8676
Weighted FalsePositiveRate: 0.06609909577749479
Kappa statistic: 0.8014336133179377
Training time: 147.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 49.74666666666664
Incorrectly Classified Instances: 13.28
Correctly Classified Instances: 86.72
Weighted Precision: 0.8675166677265501
Weighted AreaUnderROC: 0.9685830115518195
Root mean squared error: 0.25603883996025933
Relative absolute error: 26.979103918071782
Root relative squared error: 54.31403999491084
Weighted TruePositiveRate: 0.8672
Weighted MatthewsCorrelation: 0.8009883879972083
Weighted FMeasure: 0.8670088446538914
Iteration time: 195.0
Weighted AreaUnderPRC: 0.9389445738850223
Mean absolute error: 0.11990712852476404
Coverage of cases: 98.6
Instances selection time: 41.0
Test time: 70.0
Accumulative iteration time: 5706.0
Weighted Recall: 0.8672
Weighted FalsePositiveRate: 0.0663094849366098
Kappa statistic: 0.800831134077121
Training time: 154.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 49.8933333333333
Incorrectly Classified Instances: 13.4
Correctly Classified Instances: 86.6
Weighted Precision: 0.8663041341248285
Weighted AreaUnderROC: 0.9683983999927117
Root mean squared error: 0.2568743891813886
Relative absolute error: 27.173418953017336
Root relative squared error: 54.49128675099353
Weighted TruePositiveRate: 0.866
Weighted MatthewsCorrelation: 0.7991797580100838
Weighted FMeasure: 0.8658310296762768
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9386027619317825
Mean absolute error: 0.12077075090229984
Coverage of cases: 98.56
Instances selection time: 39.0
Test time: 69.0
Accumulative iteration time: 5895.0
Weighted Recall: 0.866
Weighted FalsePositiveRate: 0.0669109666661047
Kappa statistic: 0.7990292934901808
Training time: 150.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 50.15999999999993
Incorrectly Classified Instances: 13.4
Correctly Classified Instances: 86.6
Weighted Precision: 0.8663935277071628
Weighted AreaUnderROC: 0.9687285646421715
Root mean squared error: 0.2561241649790383
Relative absolute error: 27.281091716201107
Root relative squared error: 54.332140164725885
Weighted TruePositiveRate: 0.866
Weighted MatthewsCorrelation: 0.7992426191873349
Weighted FMeasure: 0.865766638017843
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9393158160630063
Mean absolute error: 0.12124929651644993
Coverage of cases: 98.72
Instances selection time: 38.0
Test time: 69.0
Accumulative iteration time: 6088.0
Weighted Recall: 0.866
Weighted FalsePositiveRate: 0.06689723451281479
Kappa statistic: 0.7990356591126471
Training time: 155.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 50.47999999999987
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8660087361201313
Weighted AreaUnderROC: 0.9684699450038537
Root mean squared error: 0.2566295094923027
Relative absolute error: 27.506262673891815
Root relative squared error: 54.43933992437528
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.7986494781577995
Weighted FMeasure: 0.8653753638944085
Iteration time: 199.0
Weighted AreaUnderPRC: 0.9388506840956379
Mean absolute error: 0.12225005632840864
Coverage of cases: 98.88
Instances selection time: 37.0
Test time: 66.0
Accumulative iteration time: 6287.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06709532063721671
Kappa statistic: 0.798435862291381
Training time: 162.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 50.519999999999854
Incorrectly Classified Instances: 13.4
Correctly Classified Instances: 86.6
Weighted Precision: 0.8663923808633681
Weighted AreaUnderROC: 0.9684854876872502
Root mean squared error: 0.25657439852466996
Relative absolute error: 27.49114133281055
Root relative squared error: 54.427649122696025
Weighted TruePositiveRate: 0.866
Weighted MatthewsCorrelation: 0.7992394290119832
Weighted FMeasure: 0.86578382224672
Iteration time: 212.0
Weighted AreaUnderPRC: 0.9388414886440513
Mean absolute error: 0.12218285036804746
Coverage of cases: 98.92
Instances selection time: 36.0
Test time: 68.0
Accumulative iteration time: 6499.0
Weighted Recall: 0.866
Weighted FalsePositiveRate: 0.06689651995352638
Kappa statistic: 0.7990348875435225
Training time: 176.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 50.453333333333184
Incorrectly Classified Instances: 13.44
Correctly Classified Instances: 86.56
Weighted Precision: 0.8659627465564587
Weighted AreaUnderROC: 0.9685241683620299
Root mean squared error: 0.2565090689796062
Relative absolute error: 27.410194505665125
Root relative squared error: 54.413790633398094
Weighted TruePositiveRate: 0.8656
Weighted MatthewsCorrelation: 0.798618298716813
Weighted FMeasure: 0.8654012381569939
Iteration time: 215.0
Weighted AreaUnderPRC: 0.9389291487044026
Mean absolute error: 0.12182308669184558
Coverage of cases: 98.92
Instances selection time: 37.0
Test time: 68.0
Accumulative iteration time: 6714.0
Weighted Recall: 0.8656
Weighted FalsePositiveRate: 0.06710182943421746
Kappa statistic: 0.798433153714877
Training time: 178.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 50.47999999999988
Incorrectly Classified Instances: 13.56
Correctly Classified Instances: 86.44
Weighted Precision: 0.8647802084142356
Weighted AreaUnderROC: 0.9684743996984826
Root mean squared error: 0.25652824552297665
Relative absolute error: 27.4563647666734
Root relative squared error: 54.41785859255519
Weighted TruePositiveRate: 0.8644
Weighted MatthewsCorrelation: 0.7968261996130849
Weighted FMeasure: 0.8642071377084088
Iteration time: 211.0
Weighted AreaUnderPRC: 0.9387682398729666
Mean absolute error: 0.12202828785188236
Coverage of cases: 98.92
Instances selection time: 36.0
Test time: 68.0
Accumulative iteration time: 6925.0
Weighted Recall: 0.8644
Weighted FalsePositiveRate: 0.06769973836727029
Kappa statistic: 0.7966340353278476
Training time: 175.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 50.22666666666658
Incorrectly Classified Instances: 13.48
Correctly Classified Instances: 86.52
Weighted Precision: 0.8655490695771562
Weighted AreaUnderROC: 0.9685205419980072
Root mean squared error: 0.2565041482218355
Relative absolute error: 27.34287761495503
Root relative squared error: 54.41274678304163
Weighted TruePositiveRate: 0.8652
Weighted MatthewsCorrelation: 0.7980111641364086
Weighted FMeasure: 0.8649787776060078
Iteration time: 218.0
Weighted AreaUnderPRC: 0.9388801216674899
Mean absolute error: 0.12152390051091182
Coverage of cases: 98.8
Instances selection time: 36.0
Test time: 68.0
Accumulative iteration time: 7143.0
Weighted Recall: 0.8652
Weighted FalsePositiveRate: 0.06730499523704328
Kappa statistic: 0.7978345136918225
Training time: 182.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 50.15999999999992
Incorrectly Classified Instances: 13.56
Correctly Classified Instances: 86.44
Weighted Precision: 0.864699015223509
Weighted AreaUnderROC: 0.9683937672274887
Root mean squared error: 0.25677826073857063
Relative absolute error: 27.350231157758195
Root relative squared error: 54.47089482885908
Weighted TruePositiveRate: 0.8644
Weighted MatthewsCorrelation: 0.7967787324635152
Weighted FMeasure: 0.864184616292979
Iteration time: 226.0
Weighted AreaUnderPRC: 0.9386245015159284
Mean absolute error: 0.12155658292337032
Coverage of cases: 98.84
Instances selection time: 36.0
Test time: 69.0
Accumulative iteration time: 7369.0
Weighted Recall: 0.8644
Weighted FalsePositiveRate: 0.06771418507984861
Kappa statistic: 0.7966329617295627
Training time: 190.0
		
Time end:Fri Dec 01 13.53.39 EET 2017