Mon Nov 27 16.00.53 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.00.53 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 67.4
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6844803370786517
Weighted AreaUnderROC: 0.7113333333333334
Root mean squared error: 0.5057689927119908
Relative absolute error: 63.08283174561048
Root relative squared error: 101.15379854239816
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.2486587886068334
Weighted FMeasure: 0.6861421055674395
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7426371325682519
Mean absolute error: 0.3154141587280524
Coverage of cases: 86.2
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 17.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.4422857142857143
Kappa statistic: 0.2485549132947976
Training time: 12.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 69.7
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7025036414684089
Weighted AreaUnderROC: 0.7365142857142856
Root mean squared error: 0.4705769509921673
Relative absolute error: 57.90104938580251
Root relative squared error: 94.11539019843346
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.2897556539358419
Weighted FMeasure: 0.7066754547588018
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7659945725863206
Mean absolute error: 0.2895052469290125
Coverage of cases: 89.6
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.43876190476190474
Kappa statistic: 0.2878486055776891
Training time: 24.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 68.8
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7087780898876405
Weighted AreaUnderROC: 0.7291619047619048
Root mean squared error: 0.475477453266703
Relative absolute error: 58.34456040119692
Root relative squared error: 95.09549065334059
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.30648641386423653
Weighted FMeasure: 0.7102850205237901
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7556852039050687
Mean absolute error: 0.2917228020059846
Coverage of cases: 88.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 68.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.40914285714285714
Kappa statistic: 0.30635838150289013
Training time: 18.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 72.5
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7244678248103744
Weighted AreaUnderROC: 0.7505142857142857
Root mean squared error: 0.4551738717353223
Relative absolute error: 57.12757438644817
Root relative squared error: 91.03477434706446
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.3429072399812674
Weighted FMeasure: 0.7274451176331732
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7746453689018755
Mean absolute error: 0.2856378719322408
Coverage of cases: 90.4
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 118.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4005714285714286
Kappa statistic: 0.3418467583497053
Training time: 46.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 74.1
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7415810276679843
Weighted AreaUnderROC: 0.7875047619047619
Root mean squared error: 0.4322977776929225
Relative absolute error: 53.95492498317116
Root relative squared error: 86.4595555385845
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.3785331616483273
Weighted FMeasure: 0.743611859838275
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7989633135452898
Mean absolute error: 0.2697746249158558
Coverage of cases: 93.8
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 152.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.40638095238095234
Kappa statistic: 0.37244897959183676
Training time: 30.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 75.0
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7427972027972027
Weighted AreaUnderROC: 0.7974857142857142
Root mean squared error: 0.4214904806692027
Relative absolute error: 52.539190360700594
Root relative squared error: 84.29809613384055
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.3792837247446084
Weighted FMeasure: 0.7438253638253639
Iteration time: 44.0
Weighted AreaUnderPRC: 0.808796373674573
Mean absolute error: 0.26269595180350297
Coverage of cases: 94.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 196.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.41314285714285715
Kappa statistic: 0.3711340206185567
Training time: 40.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 74.6
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7382750582750582
Weighted AreaUnderROC: 0.7864571428571427
Root mean squared error: 0.42707846321727594
Relative absolute error: 53.883707743252565
Root relative squared error: 85.41569264345519
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.3687480657239248
Weighted FMeasure: 0.7396257796257796
Iteration time: 203.0
Weighted AreaUnderPRC: 0.7944640278133854
Mean absolute error: 0.2694185387162628
Coverage of cases: 93.2
Instances selection time: 8.0
Test time: 9.0
Accumulative iteration time: 399.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.41866666666666663
Kappa statistic: 0.3608247422680413
Training time: 195.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 74.2
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.7583529523673634
Weighted AreaUnderROC: 0.7948190476190476
Root mean squared error: 0.42366951857382396
Relative absolute error: 52.41300486357083
Root relative squared error: 84.73390371476479
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.41395372018059506
Weighted FMeasure: 0.757414205738645
Iteration time: 86.0
Weighted AreaUnderPRC: 0.804006315533411
Mean absolute error: 0.26206502431785417
Coverage of cases: 93.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 485.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.39952380952380945
Kappa statistic: 0.40352697095435697
Training time: 82.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 73.8
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7480229070084538
Weighted AreaUnderROC: 0.7757333333333334
Root mean squared error: 0.4321150464725865
Relative absolute error: 53.99941940547197
Root relative squared error: 86.4230092945173
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.3932204306276497
Weighted FMeasure: 0.7495059288537549
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7859438901480287
Mean absolute error: 0.26999709702735986
Coverage of cases: 92.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 570.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.4
Kappa statistic: 0.38650306748466257
Training time: 81.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 75.0
Incorrectly Classified Instances: 23.4
Correctly Classified Instances: 76.6
Weighted Precision: 0.7537680435661458
Weighted AreaUnderROC: 0.7803809523809524
Root mean squared error: 0.4259744390694495
Relative absolute error: 53.23641470649546
Root relative squared error: 85.19488781388989
Weighted TruePositiveRate: 0.766
Weighted MatthewsCorrelation: 0.4033122363713253
Weighted FMeasure: 0.7531953223601865
Iteration time: 69.0
Weighted AreaUnderPRC: 0.79179444138436
Mean absolute error: 0.2661820735324773
Coverage of cases: 93.8
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 639.0
Weighted Recall: 0.766
Weighted FalsePositiveRate: 0.40504761904761905
Kappa statistic: 0.39315352697095446
Training time: 67.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 79.5
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.751841491841492
Weighted AreaUnderROC: 0.7765714285714286
Root mean squared error: 0.4232202706038589
Relative absolute error: 55.8729044061956
Root relative squared error: 84.64405412077178
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.40035504278597556
Weighted FMeasure: 0.7522245322245322
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7904339309046503
Mean absolute error: 0.279364522030978
Coverage of cases: 95.4
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 710.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.40209523809523806
Kappa statistic: 0.3917525773195877
Training time: 69.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 81.1
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.7583529523673634
Weighted AreaUnderROC: 0.7828761904761905
Root mean squared error: 0.4156454475569582
Relative absolute error: 56.331204676366816
Root relative squared error: 83.12908951139164
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.41395372018059506
Weighted FMeasure: 0.757414205738645
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7941692159051852
Mean absolute error: 0.2816560233818341
Coverage of cases: 95.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 781.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.39952380952380945
Kappa statistic: 0.40352697095435697
Training time: 70.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 83.4
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7391328061085356
Weighted AreaUnderROC: 0.7815809523809524
Root mean squared error: 0.41450828037916354
Relative absolute error: 57.63765068466809
Root relative squared error: 82.9016560758327
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.37241511683782696
Weighted FMeasure: 0.7411561264822136
Iteration time: 117.0
Weighted AreaUnderPRC: 0.7912539814389616
Mean absolute error: 0.2881882534233404
Coverage of cases: 96.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 898.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.41104761904761905
Kappa statistic: 0.3660531697341513
Training time: 116.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 83.5
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7407281317307024
Weighted AreaUnderROC: 0.7794476190476192
Root mean squared error: 0.4152736320081698
Relative absolute error: 58.07532730769901
Root relative squared error: 83.05472640163396
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.37490590310670785
Weighted FMeasure: 0.7421118939853484
Iteration time: 121.0
Weighted AreaUnderPRC: 0.7897075406589876
Mean absolute error: 0.29037663653849505
Coverage of cases: 96.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1019.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.414
Kappa statistic: 0.367283950617284
Training time: 120.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 86.2
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7378968253968253
Weighted AreaUnderROC: 0.7736190476190475
Root mean squared error: 0.4154175346655408
Relative absolute error: 60.174078676636626
Root relative squared error: 83.08350693310817
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.36695392738769383
Weighted FMeasure: 0.7388328214129004
Iteration time: 142.0
Weighted AreaUnderPRC: 0.7856448011300077
Mean absolute error: 0.3008703933831831
Coverage of cases: 97.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1161.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.4224761904761905
Kappa statistic: 0.3581780538302277
Training time: 141.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 87.4
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7411378655873408
Weighted AreaUnderROC: 0.768342857142857
Root mean squared error: 0.41712551214555144
Relative absolute error: 61.665094178781374
Root relative squared error: 83.42510242911028
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.3767062713101988
Weighted FMeasure: 0.7428708514117969
Iteration time: 113.0
Weighted AreaUnderPRC: 0.7818588514714381
Mean absolute error: 0.3083254708939069
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1274.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.41019047619047616
Kappa statistic: 0.3698770491803278
Training time: 112.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 88.9
Incorrectly Classified Instances: 24.6
Correctly Classified Instances: 75.4
Weighted Precision: 0.7397106690777576
Weighted AreaUnderROC: 0.7669142857142855
Root mean squared error: 0.41685084939465844
Relative absolute error: 62.21913722439526
Root relative squared error: 83.37016987893169
Weighted TruePositiveRate: 0.754
Weighted MatthewsCorrelation: 0.36967173887569355
Weighted FMeasure: 0.7397236478484013
Iteration time: 199.0
Weighted AreaUnderPRC: 0.7799154857774877
Mean absolute error: 0.3110956861219763
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1473.0
Weighted Recall: 0.754
Weighted FalsePositiveRate: 0.4254285714285715
Kappa statistic: 0.359375
Training time: 198.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 89.8
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7443399638336348
Weighted AreaUnderROC: 0.7686476190476189
Root mean squared error: 0.4156789187170582
Relative absolute error: 62.50844293088097
Root relative squared error: 83.13578374341164
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.3803868617416557
Weighted FMeasure: 0.7439557836557441
Iteration time: 192.0
Weighted AreaUnderPRC: 0.7802610718462492
Mean absolute error: 0.31254221465440485
Coverage of cases: 97.6
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 1665.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.4199047619047619
Kappa statistic: 0.3697916666666667
Training time: 192.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 92.4
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7297220176730487
Weighted AreaUnderROC: 0.7651428571428571
Root mean squared error: 0.4190763161204865
Relative absolute error: 64.30726162439564
Root relative squared error: 83.81526322409731
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.34963262055004185
Weighted FMeasure: 0.7320259004116758
Iteration time: 168.0
Weighted AreaUnderPRC: 0.7800523307308925
Mean absolute error: 0.3215363081219782
Coverage of cases: 98.8
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1833.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.4259047619047619
Kappa statistic: 0.3429158110882956
Training time: 167.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 91.4
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7322105600146349
Weighted AreaUnderROC: 0.7652380952380953
Root mean squared error: 0.41936840675982734
Relative absolute error: 64.03489543734842
Root relative squared error: 83.87368135196547
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.35583611777500773
Weighted FMeasure: 0.7345089278804733
Iteration time: 210.0
Weighted AreaUnderPRC: 0.7801096656725598
Mean absolute error: 0.32017447718674213
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 2043.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.4212380952380952
Kappa statistic: 0.3493852459016393
Training time: 209.0
		
Time end:Mon Nov 27 16.00.56 EET 2017