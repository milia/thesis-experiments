Mon Nov 27 15.59.18 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.18 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 69.3
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6852869002245079
Weighted AreaUnderROC: 0.6813142857142857
Root mean squared error: 0.5317781023465101
Relative absolute error: 69.79296904045856
Root relative squared error: 106.35562046930202
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.2485272234939796
Weighted FMeasure: 0.6735027790486459
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7201465364048026
Mean absolute error: 0.3489648452022928
Coverage of cases: 83.4
Instances selection time: 19.0
Test time: 7.0
Accumulative iteration time: 55.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.406
Kappa statistic: 0.24638989169675105
Training time: 36.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 67.2
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6909962242230613
Weighted AreaUnderROC: 0.7017333333333333
Root mean squared error: 0.5290963023859105
Relative absolute error: 67.48151551808512
Root relative squared error: 105.81926047718211
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.26067250375161366
Weighted FMeasure: 0.6750155529000005
Iteration time: 51.0
Weighted AreaUnderPRC: 0.7292217377008615
Mean absolute error: 0.3374075775904256
Coverage of cases: 82.4
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 106.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.3907619047619048
Kappa statistic: 0.25711743772242
Training time: 46.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 66.7
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.7007566204287516
Weighted AreaUnderROC: 0.6996190476190476
Root mean squared error: 0.5300852948734192
Relative absolute error: 67.88710301075292
Root relative squared error: 106.01705897468383
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.28186015152716337
Weighted FMeasure: 0.6801858612678393
Iteration time: 43.0
Weighted AreaUnderPRC: 0.7309347526702172
Mean absolute error: 0.3394355150537646
Coverage of cases: 81.6
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 149.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.37
Kappa statistic: 0.27631578947368424
Training time: 34.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 68.5
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6900656518288724
Weighted AreaUnderROC: 0.7084380952380952
Root mean squared error: 0.5129967859240151
Relative absolute error: 65.09059734655126
Root relative squared error: 102.59935718480303
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.261343789151941
Weighted FMeasure: 0.6843585757643053
Iteration time: 41.0
Weighted AreaUnderPRC: 0.7437726901442633
Mean absolute error: 0.3254529867327563
Coverage of cases: 84.0
Instances selection time: 9.0
Test time: 10.0
Accumulative iteration time: 190.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4114285714285714
Kappa statistic: 0.26062846580406657
Training time: 32.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 69.4
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.688770325203252
Weighted AreaUnderROC: 0.7073142857142857
Root mean squared error: 0.5134151834625792
Relative absolute error: 65.58569463837588
Root relative squared error: 102.68303669251584
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.2584303916966219
Weighted FMeasure: 0.6838684518393345
Iteration time: 46.0
Weighted AreaUnderPRC: 0.739729002420998
Mean absolute error: 0.3279284731918794
Coverage of cases: 84.4
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 236.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.41523809523809524
Kappa statistic: 0.2578849721706866
Training time: 40.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 68.2
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.7134838756222495
Weighted AreaUnderROC: 0.7221333333333332
Root mean squared error: 0.4963193737450773
Relative absolute error: 61.87130458485469
Root relative squared error: 99.26387474901546
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.31694885067363054
Weighted FMeasure: 0.7080316825819825
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7513005417600475
Mean absolute error: 0.30935652292427346
Coverage of cases: 84.6
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 280.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.3782857142857143
Kappa statistic: 0.3160813308687614
Training time: 38.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 71.2
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.707696219698254
Weighted AreaUnderROC: 0.716952380952381
Root mean squared error: 0.4937376134736805
Relative absolute error: 62.85272052053238
Root relative squared error: 98.7475226947361
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.30400794398086234
Weighted FMeasure: 0.7068227339233206
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7443541488793711
Mean absolute error: 0.3142636026026619
Coverage of cases: 86.2
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 332.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4002857142857143
Kappa statistic: 0.30397727272727265
Training time: 48.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 70.1
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.696787326967207
Weighted AreaUnderROC: 0.7179619047619049
Root mean squared error: 0.4991397644799371
Relative absolute error: 63.06736720068605
Root relative squared error: 99.82795289598741
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.27786923613070474
Weighted FMeasure: 0.6942196531791907
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7464835788905659
Mean absolute error: 0.31533683600343027
Coverage of cases: 87.0
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 397.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.4100952380952381
Kappa statistic: 0.2776735459662288
Training time: 61.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 70.2
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7034883720930233
Weighted AreaUnderROC: 0.71
Root mean squared error: 0.49752476788250655
Relative absolute error: 63.923416050811774
Root relative squared error: 99.50495357650131
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.29390259873217905
Weighted FMeasure: 0.7016443464994065
Iteration time: 81.0
Weighted AreaUnderPRC: 0.7407389557503585
Mean absolute error: 0.3196170802540589
Coverage of cases: 86.6
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 478.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4028571428571429
Kappa statistic: 0.2937853107344631
Training time: 77.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 72.4
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6981541661250488
Weighted AreaUnderROC: 0.7084380952380952
Root mean squared error: 0.49385307667153316
Relative absolute error: 64.4031629610276
Root relative squared error: 98.77061533430663
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.28116719917886396
Weighted FMeasure: 0.6959431448030797
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7386737468606382
Mean absolute error: 0.322015814805138
Coverage of cases: 88.4
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 541.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.40923809523809523
Kappa statistic: 0.28101503759398483
Training time: 61.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 75.5
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7011494252873562
Weighted AreaUnderROC: 0.7060190476190477
Root mean squared error: 0.4879588402766532
Relative absolute error: 65.29543408815199
Root relative squared error: 97.59176805533065
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2884380297214645
Weighted FMeasure: 0.7005635780565097
Iteration time: 139.0
Weighted AreaUnderPRC: 0.7345569268222101
Mean absolute error: 0.32647717044075997
Coverage of cases: 90.4
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 680.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4104761904761905
Kappa statistic: 0.28842504743833003
Training time: 137.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 78.3
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6751908096624439
Weighted AreaUnderROC: 0.697352380952381
Root mean squared error: 0.48734421445035514
Relative absolute error: 66.71270090449288
Root relative squared error: 97.46884289007103
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.22647513360257795
Weighted FMeasure: 0.6774243479606842
Iteration time: 111.0
Weighted AreaUnderPRC: 0.7277073044179179
Mean absolute error: 0.3335635045224644
Coverage of cases: 91.8
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 791.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.45714285714285724
Kappa statistic: 0.22630560928433274
Training time: 108.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 81.8
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.6828009828009828
Weighted AreaUnderROC: 0.6960380952380952
Root mean squared error: 0.4809866157627992
Relative absolute error: 68.01628123836511
Root relative squared error: 96.19732315255985
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.2447530590290138
Weighted FMeasure: 0.6833897397655786
Iteration time: 123.0
Weighted AreaUnderPRC: 0.7253538982768498
Mean absolute error: 0.34008140619182553
Coverage of cases: 93.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 914.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.44019047619047613
Kappa statistic: 0.24474187380497142
Training time: 121.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 81.2
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6868166461916462
Weighted AreaUnderROC: 0.6963809523809524
Root mean squared error: 0.4823846776124618
Relative absolute error: 67.74543793168874
Root relative squared error: 96.47693552249235
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.25431372539733466
Weighted FMeasure: 0.6873974645786726
Iteration time: 89.0
Weighted AreaUnderPRC: 0.7257271998657845
Mean absolute error: 0.33872718965844373
Coverage of cases: 92.6
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 1003.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.43466666666666665
Kappa statistic: 0.25430210325047786
Training time: 88.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 82.2
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6868166461916462
Weighted AreaUnderROC: 0.6974476190476191
Root mean squared error: 0.47816907082086985
Relative absolute error: 67.85142383974673
Root relative squared error: 95.63381416417397
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.25431372539733466
Weighted FMeasure: 0.6873974645786726
Iteration time: 162.0
Weighted AreaUnderPRC: 0.7273388934907
Mean absolute error: 0.33925711919873364
Coverage of cases: 93.4
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 1165.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.43466666666666665
Kappa statistic: 0.25430210325047786
Training time: 157.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 82.5
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6981077745783627
Weighted AreaUnderROC: 0.6948952380952381
Root mean squared error: 0.47753172721482096
Relative absolute error: 68.00680417895974
Root relative squared error: 95.50634544296419
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.28104846317335413
Weighted FMeasure: 0.6999155205622951
Iteration time: 126.0
Weighted AreaUnderPRC: 0.7258136254691726
Mean absolute error: 0.34003402089479867
Coverage of cases: 93.6
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1291.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.4248571428571428
Kappa statistic: 0.28088803088803077
Training time: 124.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 85.2
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6962883737064232
Weighted AreaUnderROC: 0.7019238095238095
Root mean squared error: 0.47003875901313874
Relative absolute error: 67.97895179058338
Root relative squared error: 94.00775180262775
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2768485003374742
Weighted FMeasure: 0.697119128698076
Iteration time: 185.0
Weighted AreaUnderPRC: 0.7316489233271884
Mean absolute error: 0.3398947589529169
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 1476.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4227619047619048
Kappa statistic: 0.27681992337164735
Training time: 184.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 85.7
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.7017723086448417
Weighted AreaUnderROC: 0.7092571428571428
Root mean squared error: 0.4645698196734887
Relative absolute error: 67.55773927317344
Root relative squared error: 92.91396393469775
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.28988065682046027
Weighted FMeasure: 0.702840909090909
Iteration time: 127.0
Weighted AreaUnderPRC: 0.7378065598826428
Mean absolute error: 0.33778869636586717
Coverage of cases: 95.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1603.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4163809523809524
Kappa statistic: 0.28982725527831077
Training time: 126.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.6
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7068949631449631
Weighted AreaUnderROC: 0.7141714285714286
Root mean squared error: 0.4606283987541428
Relative absolute error: 67.73592030573212
Root relative squared error: 92.12567975082855
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.3021170572389389
Weighted FMeasure: 0.7074360886441423
Iteration time: 148.0
Weighted AreaUnderPRC: 0.7415296867324228
Mean absolute error: 0.3386796015286606
Coverage of cases: 96.4
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1751.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.40704761904761905
Kappa statistic: 0.3021032504780114
Training time: 146.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 88.3
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7058045042953333
Weighted AreaUnderROC: 0.7191428571428572
Root mean squared error: 0.4566172684871097
Relative absolute error: 67.6914864915162
Root relative squared error: 91.32345369742194
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.2994793540661709
Weighted FMeasure: 0.7068565724815724
Iteration time: 217.0
Weighted AreaUnderPRC: 0.7447508483991158
Mean absolute error: 0.33845743245758103
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1968.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4108571428571428
Kappa statistic: 0.2994241842610363
Training time: 216.0
		
Time end:Mon Nov 27 15.59.21 EET 2017