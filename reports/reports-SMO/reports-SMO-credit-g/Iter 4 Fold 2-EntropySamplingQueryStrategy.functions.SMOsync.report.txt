Mon Nov 27 15.59.35 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.35 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 69.3
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6696969696969697
Weighted AreaUnderROC: 0.6826476190476191
Root mean squared error: 0.5146049332289101
Relative absolute error: 65.88905611886959
Root relative squared error: 102.92098664578202
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.21344230364711775
Weighted FMeasure: 0.67171966343392
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7274200718183282
Mean absolute error: 0.32944528059434797
Coverage of cases: 85.0
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.4635238095238095
Kappa statistic: 0.2133204633204634
Training time: 16.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 65.8
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6692005610098176
Weighted AreaUnderROC: 0.6872761904761906
Root mean squared error: 0.5292571132895417
Relative absolute error: 66.91762814058562
Root relative squared error: 105.85142265790834
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.21232321131452692
Weighted FMeasure: 0.667536266069112
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7301906391161149
Mean absolute error: 0.3345881407029281
Coverage of cases: 82.6
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.45171428571428573
Kappa statistic: 0.2122641509433964
Training time: 14.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 69.7
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6708599681416662
Weighted AreaUnderROC: 0.666
Root mean squared error: 0.5152919439679147
Relative absolute error: 66.21279311410292
Root relative squared error: 103.05838879358294
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.2153779582372826
Weighted FMeasure: 0.6754507793049858
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7119267530759942
Mean absolute error: 0.3310639655705146
Coverage of cases: 86.6
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 72.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.4753333333333334
Kappa statistic: 0.21442687747035596
Training time: 27.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 67.3
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6955307262569833
Weighted AreaUnderROC: 0.6995238095238095
Root mean squared error: 0.5023951639642212
Relative absolute error: 62.54817105030622
Root relative squared error: 100.47903279284425
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.27486725616723134
Weighted FMeasure: 0.6975853262131415
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7344210617104024
Mean absolute error: 0.3127408552515311
Coverage of cases: 85.0
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4295238095238095
Kappa statistic: 0.27466150870406175
Training time: 59.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 67.8
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6948965434035108
Weighted AreaUnderROC: 0.7075047619047619
Root mean squared error: 0.4952075941258239
Relative absolute error: 60.94516327897183
Root relative squared error: 99.04151882516477
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.27301151175462857
Weighted FMeasure: 0.6979675611222151
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7395051379262905
Mean absolute error: 0.3047258163948592
Coverage of cases: 86.0
Instances selection time: 9.0
Test time: 9.0
Accumulative iteration time: 203.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.4362857142857143
Kappa statistic: 0.27246093749999983
Training time: 54.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 68.1
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6928171928458748
Weighted AreaUnderROC: 0.7066857142857143
Root mean squared error: 0.49619567272720355
Relative absolute error: 61.35205402740964
Root relative squared error: 99.2391345454407
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.2676706730197826
Weighted FMeasure: 0.696582245417942
Iteration time: 70.0
Weighted AreaUnderPRC: 0.7384612244738062
Mean absolute error: 0.3067602701370482
Coverage of cases: 87.0
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 273.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.44390476190476186
Kappa statistic: 0.2667322834645668
Training time: 66.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 69.6
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6917954446720039
Weighted AreaUnderROC: 0.7093904761904762
Root mean squared error: 0.4879616854601233
Relative absolute error: 60.61320002934776
Root relative squared error: 97.59233709202466
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.2650042158956426
Weighted FMeasure: 0.6958626799776281
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7353959818252925
Mean absolute error: 0.3030660001467388
Coverage of cases: 87.6
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 338.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.4477142857142858
Kappa statistic: 0.26383399209486164
Training time: 61.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 67.4
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7135248655913979
Weighted AreaUnderROC: 0.7135619047619047
Root mean squared error: 0.48991808333018816
Relative absolute error: 59.49508492275708
Root relative squared error: 97.98361666603763
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.3160101124854019
Weighted FMeasure: 0.7172841228402319
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7401102600330163
Mean absolute error: 0.2974754246137854
Coverage of cases: 85.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 393.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.42304761904761906
Kappa statistic: 0.31411530815109334
Training time: 52.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 69.4
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.703339288471294
Weighted AreaUnderROC: 0.7092190476190476
Root mean squared error: 0.4841288576830125
Relative absolute error: 60.18659648920952
Root relative squared error: 96.8257715366025
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2900285956023177
Weighted FMeasure: 0.7077304095321886
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7362294848045867
Mean absolute error: 0.3009329824460476
Coverage of cases: 87.4
Instances selection time: 11.0
Test time: 4.0
Accumulative iteration time: 468.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4484761904761905
Kappa statistic: 0.28643724696356265
Training time: 64.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 73.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6991273520589036
Weighted AreaUnderROC: 0.7133904761904764
Root mean squared error: 0.4744870015524829
Relative absolute error: 60.437224045320846
Root relative squared error: 94.89740031049658
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.2787912047836247
Weighted FMeasure: 0.7035820158102767
Iteration time: 103.0
Weighted AreaUnderPRC: 0.7367128686436832
Mean absolute error: 0.30218612022660424
Coverage of cases: 89.8
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 571.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4607619047619048
Kappa statistic: 0.2740286298568506
Training time: 102.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 75.8
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7020979020979021
Weighted AreaUnderROC: 0.7145523809523809
Root mean squared error: 0.46918748747517897
Relative absolute error: 60.81052169219605
Root relative squared error: 93.83749749503579
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.28446279355845633
Weighted FMeasure: 0.706029106029106
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7384806900061016
Mean absolute error: 0.30405260846098026
Coverage of cases: 91.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 626.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.46285714285714286
Kappa statistic: 0.2783505154639175
Training time: 53.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 75.1
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7130974448016143
Weighted AreaUnderROC: 0.7190285714285715
Root mean squared error: 0.4651553496878368
Relative absolute error: 59.83293202575523
Root relative squared error: 93.03106993756735
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3097107966557101
Weighted FMeasure: 0.7160989792568739
Iteration time: 89.0
Weighted AreaUnderPRC: 0.7425042167525872
Mean absolute error: 0.2991646601287761
Coverage of cases: 92.0
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 715.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.45095238095238094
Kappa statistic: 0.30268595041322305
Training time: 81.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 77.8
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7162739322533138
Weighted AreaUnderROC: 0.723390476190476
Root mean squared error: 0.45845976702790386
Relative absolute error: 60.06795021471187
Root relative squared error: 91.69195340558078
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.31822849295572675
Weighted FMeasure: 0.7194646144934732
Iteration time: 91.0
Weighted AreaUnderPRC: 0.75229580132975
Mean absolute error: 0.30033975107355937
Coverage of cases: 93.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 806.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4424761904761905
Kappa statistic: 0.3121149897330594
Training time: 90.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 80.1
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.712462503408781
Weighted AreaUnderROC: 0.7305333333333333
Root mean squared error: 0.45031688562993377
Relative absolute error: 59.90014233834078
Root relative squared error: 90.06337712598676
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.30999917546835876
Weighted FMeasure: 0.716106719367589
Iteration time: 153.0
Weighted AreaUnderPRC: 0.7572843770958015
Mean absolute error: 0.2995007116917039
Coverage of cases: 94.0
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 959.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.4441904761904762
Kappa statistic: 0.30470347648261753
Training time: 151.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 83.0
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7294889609387062
Weighted AreaUnderROC: 0.7507809523809524
Root mean squared error: 0.434953175216602
Relative absolute error: 59.37192576014249
Root relative squared error: 86.9906350433204
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.35151875721411646
Weighted FMeasure: 0.7326044172315767
Iteration time: 120.0
Weighted AreaUnderPRC: 0.7773142206137372
Mean absolute error: 0.2968596288007124
Coverage of cases: 96.6
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1079.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.41533333333333333
Kappa statistic: 0.347165991902834
Training time: 118.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 85.5
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7464484166834036
Weighted AreaUnderROC: 0.7528761904761905
Root mean squared error: 0.42901571212890427
Relative absolute error: 60.33023966281399
Root relative squared error: 85.80314242578085
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.39069445254971474
Weighted FMeasure: 0.7484924199457363
Iteration time: 136.0
Weighted AreaUnderPRC: 0.7772259750217632
Mean absolute error: 0.30165119831406995
Coverage of cases: 96.8
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 1215.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.39704761904761904
Kappa statistic: 0.3851626016260162
Training time: 129.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 86.4
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7488685775135328
Weighted AreaUnderROC: 0.7540380952380954
Root mean squared error: 0.4279478017819406
Relative absolute error: 60.74082875605146
Root relative squared error: 85.58956035638812
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.3967390711332346
Weighted FMeasure: 0.7509175434303892
Iteration time: 170.0
Weighted AreaUnderPRC: 0.7793182414150461
Mean absolute error: 0.3037041437802573
Coverage of cases: 97.4
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 1385.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.3923809523809524
Kappa statistic: 0.3914807302231237
Training time: 164.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 88.7
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7387753766981399
Weighted AreaUnderROC: 0.7565333333333333
Root mean squared error: 0.4252802866259646
Relative absolute error: 61.78221803204175
Root relative squared error: 85.05605732519292
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.3739763397958435
Weighted FMeasure: 0.7415961813939127
Iteration time: 132.0
Weighted AreaUnderPRC: 0.7826930369525072
Mean absolute error: 0.30891109016020873
Coverage of cases: 98.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1517.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.4004761904761905
Kappa statistic: 0.3699596774193549
Training time: 131.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 89.2
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7344490721559563
Weighted AreaUnderROC: 0.7543619047619048
Root mean squared error: 0.4258986006224492
Relative absolute error: 62.30034831697677
Root relative squared error: 85.17972012448985
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.36378624879323196
Weighted FMeasure: 0.7374617202962154
Iteration time: 153.0
Weighted AreaUnderPRC: 0.7828611274889908
Mean absolute error: 0.31150174158488386
Coverage of cases: 98.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1670.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.406
Kappa statistic: 0.35987903225806456
Training time: 152.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 89.8
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.726985446985447
Weighted AreaUnderROC: 0.757942857142857
Root mean squared error: 0.4247961525988623
Relative absolute error: 62.18406952046141
Root relative squared error: 84.95923051977246
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.348245777110249
Weighted FMeasure: 0.7302380952380951
Iteration time: 287.0
Weighted AreaUnderPRC: 0.7837289635061547
Mean absolute error: 0.310920347602307
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1957.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4026666666666667
Kappa statistic: 0.34653465346534656
Training time: 286.0
		
Time end:Mon Nov 27 15.59.38 EET 2017