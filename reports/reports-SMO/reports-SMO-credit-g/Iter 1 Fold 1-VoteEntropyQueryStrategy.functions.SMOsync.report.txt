Mon Nov 27 16.01.34 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 16.01.34 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 71.5
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6969919295671314
Weighted AreaUnderROC: 0.7172190476190476
Root mean squared error: 0.47076070823065846
Relative absolute error: 58.604889645135636
Root relative squared error: 94.1521416461317
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2658522658241319
Weighted FMeasure: 0.6982392228293867
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7486313104849143
Mean absolute error: 0.2930244482256782
Coverage of cases: 90.4
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 34.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4933333333333333
Kappa statistic: 0.253731343283582
Training time: 24.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 66.2
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7113810741687979
Weighted AreaUnderROC: 0.7222095238095239
Root mean squared error: 0.4763362557748372
Relative absolute error: 56.23938123065121
Root relative squared error: 95.26725115496744
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2973516031334059
Weighted FMeasure: 0.7101375956736954
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7492420364710767
Mean absolute error: 0.28119690615325604
Coverage of cases: 85.8
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 66.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.48057142857142854
Kappa statistic: 0.2826552462526767
Training time: 25.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 69.1
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7305262812305066
Weighted AreaUnderROC: 0.7308571428571429
Root mean squared error: 0.46042484755073554
Relative absolute error: 54.79421028420369
Root relative squared error: 92.0849695101471
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.32134064088620906
Weighted FMeasure: 0.7134811424322866
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7578055930779853
Mean absolute error: 0.27397105142101846
Coverage of cases: 88.0
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 87.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.5012380952380951
Kappa statistic: 0.2881165919282511
Training time: 16.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 69.0
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.728390522875817
Weighted AreaUnderROC: 0.7363428571428572
Root mean squared error: 0.45609194927301483
Relative absolute error: 54.41999342775531
Root relative squared error: 91.21838985460296
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3132053198179722
Weighted FMeasure: 0.7092752058940847
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7689364731210535
Mean absolute error: 0.27209996713877654
Coverage of cases: 88.4
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 113.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.5097142857142857
Kappa statistic: 0.27765237020316036
Training time: 21.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 68.9
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7524572300316007
Weighted AreaUnderROC: 0.7288190476190475
Root mean squared error: 0.4572070026253269
Relative absolute error: 54.181173606625364
Root relative squared error: 91.44140052506538
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.35640899018217886
Weighted FMeasure: 0.7219535766057591
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7638663881005303
Mean absolute error: 0.27090586803312683
Coverage of cases: 88.8
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 138.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.4999047619047619
Kappa statistic: 0.3093607305936074
Training time: 21.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 68.8
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.722873900293255
Weighted AreaUnderROC: 0.7218857142857142
Root mean squared error: 0.4629799295268835
Relative absolute error: 55.21516650012066
Root relative squared error: 92.5959859053767
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.29913105659319217
Weighted FMeasure: 0.7033730158730159
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7581769527451785
Mean absolute error: 0.2760758325006033
Coverage of cases: 88.4
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 168.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.5190476190476191
Kappa statistic: 0.26303854875283444
Training time: 23.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 70.4
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7106301474813255
Weighted AreaUnderROC: 0.6968952380952381
Root mean squared error: 0.47326918936493206
Relative absolute error: 58.149461336375055
Root relative squared error: 94.65383787298641
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.2446236967323126
Weighted FMeasure: 0.6734287744948726
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7356426298241501
Mean absolute error: 0.29074730668187526
Coverage of cases: 88.0
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 191.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5737142857142856
Kappa statistic: 0.19239904988123507
Training time: 20.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 67.1
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6959550561797753
Weighted AreaUnderROC: 0.7033333333333334
Root mean squared error: 0.4831462748826118
Relative absolute error: 58.37448420214187
Root relative squared error: 96.62925497652236
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.23015104259530228
Weighted FMeasure: 0.6741954287467404
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7380247213015136
Mean absolute error: 0.29187242101070937
Coverage of cases: 86.6
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 216.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5648571428571428
Kappa statistic: 0.19186046511627894
Training time: 22.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 67.5
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7025665581306264
Weighted AreaUnderROC: 0.720704761904762
Root mean squared error: 0.4729364940089968
Relative absolute error: 56.67631167838068
Root relative squared error: 94.58729880179936
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.22952346853895997
Weighted FMeasure: 0.6686262564727383
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7572447180103206
Mean absolute error: 0.2833815583919034
Coverage of cases: 88.0
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 240.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5792380952380952
Kappa statistic: 0.180522565320665
Training time: 20.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 66.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6922950209894256
Weighted AreaUnderROC: 0.7103047619047619
Root mean squared error: 0.4856691114483704
Relative absolute error: 57.94564600959403
Root relative squared error: 97.13382228967407
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.20202066783792133
Weighted FMeasure: 0.6565315592257263
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7450612100507539
Mean absolute error: 0.28972823004797016
Coverage of cases: 85.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 279.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.597047619047619
Kappa statistic: 0.15264423076923064
Training time: 35.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 66.4
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7025029200734191
Weighted AreaUnderROC: 0.7200190476190477
Root mean squared error: 0.4823635164759141
Relative absolute error: 57.032740502898264
Root relative squared error: 96.47270329518281
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.2164508159944542
Weighted FMeasure: 0.6593897402775332
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7492673898056724
Mean absolute error: 0.2851637025144913
Coverage of cases: 85.8
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 309.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5953333333333334
Kappa statistic: 0.16062801932367154
Training time: 28.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 65.4
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7224943196162585
Weighted AreaUnderROC: 0.7260952380952382
Root mean squared error: 0.47879586792744705
Relative absolute error: 55.79351631589059
Root relative squared error: 95.7591735854894
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.23924177845226405
Weighted FMeasure: 0.6615942028985506
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7525271590722243
Mean absolute error: 0.27896758157945295
Coverage of cases: 85.2
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 344.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5965714285714285
Kappa statistic: 0.16870415647921766
Training time: 34.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 64.4
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7233378086509835
Weighted AreaUnderROC: 0.7247809523809524
Root mean squared error: 0.47986881363033007
Relative absolute error: 55.73000358630315
Root relative squared error: 95.97376272606601
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.24841925985473085
Weighted FMeasure: 0.6671862975314244
Iteration time: 38.0
Weighted AreaUnderPRC: 0.751176880918387
Mean absolute error: 0.2786500179315157
Coverage of cases: 84.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 382.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5880952380952381
Kappa statistic: 0.18082524271844655
Training time: 37.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 63.8
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6981306655394298
Weighted AreaUnderROC: 0.7081142857142858
Root mean squared error: 0.4893388421564935
Relative absolute error: 56.89113384179629
Root relative squared error: 97.8677684312987
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.2228325299304678
Weighted FMeasure: 0.6671542628847406
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7335455191756036
Mean absolute error: 0.28445566920898147
Coverage of cases: 83.4
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 428.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5800952380952381
Kappa statistic: 0.17654028436018954
Training time: 44.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 66.4
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6882608695652175
Weighted AreaUnderROC: 0.7106095238095238
Root mean squared error: 0.48582384496535186
Relative absolute error: 57.96815051141232
Root relative squared error: 97.16476899307037
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.19304683562633604
Weighted FMeasure: 0.6530734243014945
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7380349807086585
Mean absolute error: 0.2898407525570616
Coverage of cases: 85.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 473.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.6017142857142856
Kappa statistic: 0.14457831325301185
Training time: 44.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 73.4
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.720334928229665
Weighted AreaUnderROC: 0.7295619047619047
Root mean squared error: 0.4571561404433539
Relative absolute error: 57.767210188657195
Root relative squared error: 91.43122808867078
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2588158533262875
Weighted FMeasure: 0.6764063339387583
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7615031229173265
Mean absolute error: 0.28883605094328596
Coverage of cases: 91.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 548.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.572
Kappa statistic: 0.20047732696897383
Training time: 74.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 77.9
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7339285714285715
Weighted AreaUnderROC: 0.7501333333333333
Root mean squared error: 0.44286238028932384
Relative absolute error: 57.77745361411364
Root relative squared error: 88.57247605786476
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3072259023943795
Weighted FMeasure: 0.7007458854026557
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7819110313492861
Mean absolute error: 0.2888872680705682
Coverage of cases: 94.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 619.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.5325714285714286
Kappa statistic: 0.25754060324825984
Training time: 70.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 80.3
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7215732087227413
Weighted AreaUnderROC: 0.7641333333333333
Root mean squared error: 0.4325208843381456
Relative absolute error: 57.7254065334213
Root relative squared error: 86.50417686762913
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.3033137135403521
Weighted FMeasure: 0.7073577433474607
Iteration time: 113.0
Weighted AreaUnderPRC: 0.7870266566053871
Mean absolute error: 0.2886270326671065
Coverage of cases: 95.0
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 732.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.5076190476190476
Kappa statistic: 0.27293064876957496
Training time: 113.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 84.8
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7207958921694481
Weighted AreaUnderROC: 0.7598285714285714
Root mean squared error: 0.4278058936200646
Relative absolute error: 59.6382604625703
Root relative squared error: 85.56117872401292
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.31117106941320544
Weighted FMeasure: 0.7134069683908046
Iteration time: 118.0
Weighted AreaUnderPRC: 0.7823557976916019
Mean absolute error: 0.2981913023128515
Coverage of cases: 96.6
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 850.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.48857142857142855
Kappa statistic: 0.2888402625820569
Training time: 118.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 87.4
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.734770487393314
Weighted AreaUnderROC: 0.768
Root mean squared error: 0.4194964229897308
Relative absolute error: 60.10629322732836
Root relative squared error: 83.89928459794615
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.3571942677135163
Weighted FMeasure: 0.7346434486298288
Iteration time: 166.0
Weighted AreaUnderPRC: 0.790275119852681
Mean absolute error: 0.3005314661366418
Coverage of cases: 97.8
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 1016.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.4347619047619048
Kappa statistic: 0.3462343096234309
Training time: 166.0
		
Time end:Mon Nov 27 16.01.36 EET 2017