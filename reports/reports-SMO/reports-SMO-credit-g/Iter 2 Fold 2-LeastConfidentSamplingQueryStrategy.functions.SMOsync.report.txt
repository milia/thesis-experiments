Mon Nov 27 16.00.17 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.00.17 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 71.2
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6636762205240622
Weighted AreaUnderROC: 0.702990476190476
Root mean squared error: 0.5052009319686711
Relative absolute error: 65.25999893145514
Root relative squared error: 101.04018639373422
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.19853064677851034
Weighted FMeasure: 0.6680731946518425
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7326575792548785
Mean absolute error: 0.3262999946572757
Coverage of cases: 87.8
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.48257142857142854
Kappa statistic: 0.1978346456692914
Training time: 23.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 69.7
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6812994204462798
Weighted AreaUnderROC: 0.7008
Root mean squared error: 0.4953772461923187
Relative absolute error: 62.323981305963606
Root relative squared error: 99.07544923846375
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.2357184561577589
Weighted FMeasure: 0.6866573919205498
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7304468015212234
Mean absolute error: 0.311619906529818
Coverage of cases: 86.6
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 47.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.48961904761904756
Kappa statistic: 0.23037190082644615
Training time: 14.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 65.8
Incorrectly Classified Instances: 34.0
Correctly Classified Instances: 66.0
Weighted Precision: 0.6355854197349042
Weighted AreaUnderROC: 0.6624952380952381
Root mean squared error: 0.5443213769899659
Relative absolute error: 69.50025376371406
Root relative squared error: 108.86427539799317
Weighted TruePositiveRate: 0.66
Weighted MatthewsCorrelation: 0.1298037273898359
Weighted FMeasure: 0.644096898984257
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6944724927669705
Mean absolute error: 0.3475012688185703
Coverage of cases: 82.2
Instances selection time: 11.0
Test time: 8.0
Accumulative iteration time: 74.0
Weighted Recall: 0.66
Weighted FalsePositiveRate: 0.5419047619047619
Kappa statistic: 0.12731006160164268
Training time: 16.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 70.3
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6798684210526316
Weighted AreaUnderROC: 0.723047619047619
Root mean squared error: 0.49203833845987927
Relative absolute error: 61.898198947317205
Root relative squared error: 98.40766769197586
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.23503674680257022
Weighted FMeasure: 0.6853576864535769
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7360270307362059
Mean absolute error: 0.30949099473658603
Coverage of cases: 88.6
Instances selection time: 12.0
Test time: 8.0
Accumulative iteration time: 133.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.47695238095238096
Kappa statistic: 0.23232323232323224
Training time: 47.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 67.3
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6625
Weighted AreaUnderROC: 0.7143809523809523
Root mean squared error: 0.5016746301323366
Relative absolute error: 62.3556417738129
Root relative squared error: 100.33492602646731
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.19416079083690582
Weighted FMeasure: 0.6687975646879757
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7340396411481904
Mean absolute error: 0.3117782088690645
Coverage of cases: 86.0
Instances selection time: 11.0
Test time: 4.0
Accumulative iteration time: 201.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.499047619047619
Kappa statistic: 0.19191919191919207
Training time: 57.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 66.7
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6786605551311434
Weighted AreaUnderROC: 0.6969333333333333
Root mean squared error: 0.5100923645235643
Relative absolute error: 63.49793610854639
Root relative squared error: 102.01847290471287
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.23321542088955663
Weighted FMeasure: 0.6837136680278645
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7229460469702846
Mean absolute error: 0.31748968054273197
Coverage of cases: 83.4
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 272.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.4710476190476191
Kappa statistic: 0.23153692614770455
Training time: 64.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 69.3
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6885526315789474
Weighted AreaUnderROC: 0.7067428571428571
Root mean squared error: 0.49222538670835425
Relative absolute error: 61.784103036097484
Root relative squared error: 98.44507734167085
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.2554747247854024
Weighted FMeasure: 0.6936377473363775
Iteration time: 87.0
Weighted AreaUnderPRC: 0.7239561363374215
Mean absolute error: 0.3089205151804874
Coverage of cases: 86.4
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 359.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.46590476190476193
Kappa statistic: 0.25252525252525243
Training time: 79.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 69.9
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6848950662739323
Weighted AreaUnderROC: 0.7175428571428571
Root mean squared error: 0.48372718336994736
Relative absolute error: 60.765544006166614
Root relative squared error: 96.74543667398947
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.24495219523565806
Weighted FMeasure: 0.6901549473510001
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7311507087438701
Mean absolute error: 0.3038277200308331
Coverage of cases: 88.2
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 435.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4811428571428571
Kappa statistic: 0.24024640657084168
Training time: 72.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 72.5
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6845
Weighted AreaUnderROC: 0.7295238095238095
Root mean squared error: 0.4713837326735713
Relative absolute error: 59.69721443329248
Root relative squared error: 94.27674653471426
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.2400396792595916
Weighted FMeasure: 0.6885333333333333
Iteration time: 109.0
Weighted AreaUnderPRC: 0.7405087625644212
Mean absolute error: 0.2984860721664624
Coverage of cases: 91.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 544.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.49847619047619046
Kappa statistic: 0.23157894736842097
Training time: 105.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 74.2
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6865034383737626
Weighted AreaUnderROC: 0.7244761904761905
Root mean squared error: 0.4696858101499375
Relative absolute error: 61.05567804252857
Root relative squared error: 93.93716202998749
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.2442336118479428
Weighted FMeasure: 0.6901480756581586
Iteration time: 84.0
Weighted AreaUnderPRC: 0.741222101010054
Mean absolute error: 0.30527839021264286
Coverage of cases: 92.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 628.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.49761904761904757
Kappa statistic: 0.2352320675105484
Training time: 82.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 76.3
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6674630924630924
Weighted AreaUnderROC: 0.7132761904761905
Root mean squared error: 0.46875754172173023
Relative absolute error: 62.24404841598998
Root relative squared error: 93.75150834434605
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.2021547448528994
Weighted FMeasure: 0.6736062147727513
Iteration time: 82.0
Weighted AreaUnderPRC: 0.7362450362421832
Mean absolute error: 0.3112202420799499
Coverage of cases: 93.2
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 710.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.5129523809523809
Kappa statistic: 0.19624217118997897
Training time: 80.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 77.5
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6683515719993052
Weighted AreaUnderROC: 0.7133142857142857
Root mean squared error: 0.46782109061719274
Relative absolute error: 62.6833139215011
Root relative squared error: 93.56421812343855
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.2032754000487545
Weighted FMeasure: 0.6741408198979782
Iteration time: 117.0
Weighted AreaUnderPRC: 0.731721540354599
Mean absolute error: 0.3134165696075055
Coverage of cases: 93.2
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 827.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.515904761904762
Kappa statistic: 0.19642857142857137
Training time: 115.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 81.6
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6758793969849246
Weighted AreaUnderROC: 0.7216952380952381
Root mean squared error: 0.458334397237385
Relative absolute error: 63.40988780882788
Root relative squared error: 91.666879447477
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.22094231396891423
Weighted FMeasure: 0.6810542398777693
Iteration time: 100.0
Weighted AreaUnderPRC: 0.7467280629172781
Mean absolute error: 0.3170494390441394
Coverage of cases: 95.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 927.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.5057142857142857
Kappa statistic: 0.21383647798742136
Training time: 99.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 81.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6758245939454471
Weighted AreaUnderROC: 0.7275238095238097
Root mean squared error: 0.4585601726686945
Relative absolute error: 63.12011665381798
Root relative squared error: 91.7120345337389
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.22240701161373874
Weighted FMeasure: 0.6814743049263947
Iteration time: 175.0
Weighted AreaUnderPRC: 0.7504026565064379
Mean absolute error: 0.3156005832690899
Coverage of cases: 95.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1102.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4989523809523809
Kappa statistic: 0.216804979253112
Training time: 174.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 82.6
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7033340467527526
Weighted AreaUnderROC: 0.7339238095238095
Root mean squared error: 0.4518959126167612
Relative absolute error: 62.790841228312246
Root relative squared error: 90.37918252335224
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.28625591446935755
Weighted FMeasure: 0.7067876051971448
Iteration time: 105.0
Weighted AreaUnderPRC: 0.754262930007863
Mean absolute error: 0.3139542061415612
Coverage of cases: 95.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1207.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.46580952380952384
Kappa statistic: 0.27904564315352703
Training time: 104.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 84.9
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6966996699669967
Weighted AreaUnderROC: 0.7323047619047619
Root mean squared error: 0.4464162569443997
Relative absolute error: 63.26625115126151
Root relative squared error: 89.28325138887993
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.26849745354033344
Weighted FMeasure: 0.699698402651078
Iteration time: 116.0
Weighted AreaUnderPRC: 0.7544859489169835
Mean absolute error: 0.31633125575630755
Coverage of cases: 96.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1323.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4827619047619048
Kappa statistic: 0.259453781512605
Training time: 115.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 86.3
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6872503039777662
Weighted AreaUnderROC: 0.7307047619047619
Root mean squared error: 0.444403265769705
Relative absolute error: 63.82233012367743
Root relative squared error: 88.880653153941
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.24675676904314048
Weighted FMeasure: 0.6911792084000447
Iteration time: 176.0
Weighted AreaUnderPRC: 0.7511788026435021
Mean absolute error: 0.31911165061838714
Coverage of cases: 97.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1499.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.4938095238095238
Kappa statistic: 0.23844537815126046
Training time: 175.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 89.6
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6860722610722609
Weighted AreaUnderROC: 0.7370095238095238
Root mean squared error: 0.4367354156465277
Relative absolute error: 65.19955515160197
Root relative squared error: 87.34708312930553
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.24516639269394183
Weighted FMeasure: 0.6905617360832578
Iteration time: 148.0
Weighted AreaUnderPRC: 0.7548477000226761
Mean absolute error: 0.32599777575800987
Coverage of cases: 98.2
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1647.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4908571428571428
Kappa statistic: 0.2379958246346554
Training time: 147.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 90.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6973637230686459
Weighted AreaUnderROC: 0.7303238095238095
Root mean squared error: 0.43873398020261006
Relative absolute error: 66.13932430911132
Root relative squared error: 87.74679604052201
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2708633268764127
Weighted FMeasure: 0.7006778100544471
Iteration time: 171.0
Weighted AreaUnderPRC: 0.7520326054866662
Mean absolute error: 0.33069662154555657
Coverage of cases: 98.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1818.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.47895238095238096
Kappa statistic: 0.26255230125523
Training time: 170.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 90.3
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6868786514701657
Weighted AreaUnderROC: 0.731904761904762
Root mean squared error: 0.4388304459211709
Relative absolute error: 66.54139778982193
Root relative squared error: 87.76608918423418
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.24772925350864836
Weighted FMeasure: 0.6915406586021505
Iteration time: 176.0
Weighted AreaUnderPRC: 0.7517083113046936
Mean absolute error: 0.33270698894910966
Coverage of cases: 98.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1994.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.48704761904761906
Kappa statistic: 0.24116424116424112
Training time: 175.0
		
Time end:Mon Nov 27 16.00.20 EET 2017