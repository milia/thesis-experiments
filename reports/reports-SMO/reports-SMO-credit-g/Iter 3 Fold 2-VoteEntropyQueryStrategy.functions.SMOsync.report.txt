Mon Nov 27 16.01.45 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 16.01.45 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 75.0
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.6603583426651735
Weighted AreaUnderROC: 0.6390666666666667
Root mean squared error: 0.5243740916865496
Relative absolute error: 72.02856523837198
Root relative squared error: 104.87481833730992
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.19044285955909346
Weighted FMeasure: 0.6521052124482128
Iteration time: 30.0
Weighted AreaUnderPRC: 0.6826827138276553
Mean absolute error: 0.36014282619185994
Coverage of cases: 86.2
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 30.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.4488571428571429
Kappa statistic: 0.18956043956043955
Training time: 25.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 75.8
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6851549363439827
Weighted AreaUnderROC: 0.6837333333333333
Root mean squared error: 0.4872486066414666
Relative absolute error: 64.7147711333321
Root relative squared error: 97.44972132829332
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.24994041666303984
Weighted FMeasure: 0.6881788389513109
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7115281193244882
Mean absolute error: 0.3235738556666605
Coverage of cases: 89.8
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 49.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.44819047619047614
Kappa statistic: 0.249512670565302
Training time: 15.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 75.5
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.669342885375494
Weighted AreaUnderROC: 0.6800000000000002
Root mean squared error: 0.4938795521911178
Relative absolute error: 65.91867171698887
Root relative squared error: 98.77591043822356
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.21188160353261687
Weighted FMeasure: 0.6737983761038344
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7123866204330587
Mean absolute error: 0.3295933585849444
Coverage of cases: 89.4
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 76.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4761904761904762
Kappa statistic: 0.2110453648915188
Training time: 22.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 73.4
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.651427801724138
Weighted AreaUnderROC: 0.667295238095238
Root mean squared error: 0.5046167074446152
Relative absolute error: 66.9321061019527
Root relative squared error: 100.92334148892303
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.16749843760759447
Weighted FMeasure: 0.658634324230194
Iteration time: 29.0
Weighted AreaUnderPRC: 0.6941723020558618
Mean absolute error: 0.3346605305097635
Coverage of cases: 88.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 105.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.5177142857142857
Kappa statistic: 0.16496945010183317
Training time: 25.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 73.2
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6454375041842404
Weighted AreaUnderROC: 0.6561714285714285
Root mean squared error: 0.5097615129308142
Relative absolute error: 67.56457393158259
Root relative squared error: 101.95230258616283
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.15359755522403032
Weighted FMeasure: 0.6528779680242807
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6865850536921415
Mean absolute error: 0.3378228696579129
Coverage of cases: 88.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 143.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.5240952380952381
Kappa statistic: 0.15142276422764225
Training time: 34.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 75.7
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6424095792667924
Weighted AreaUnderROC: 0.666552380952381
Root mean squared error: 0.4962816221255035
Relative absolute error: 65.65939367019526
Root relative squared error: 99.2563244251007
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.14033937879401526
Weighted FMeasure: 0.6498133374386267
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6891958550816336
Mean absolute error: 0.32829696835097627
Coverage of cases: 90.4
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 181.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.5608571428571429
Kappa statistic: 0.13254310344827594
Training time: 34.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 72.5
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6594270150307677
Weighted AreaUnderROC: 0.6675238095238095
Root mean squared error: 0.49183231730847216
Relative absolute error: 63.522086391493794
Root relative squared error: 98.36646346169444
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.16829794503503004
Weighted FMeasure: 0.6578113763527617
Iteration time: 33.0
Weighted AreaUnderPRC: 0.6976003528485932
Mean absolute error: 0.31761043195746896
Coverage of cases: 88.6
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 214.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5713333333333334
Kappa statistic: 0.14977477477477455
Training time: 30.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 71.8
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6533804684047724
Weighted AreaUnderROC: 0.6760571428571429
Root mean squared error: 0.4910122671206246
Relative absolute error: 62.84517409365537
Root relative squared error: 98.20245342412493
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.15096014488300225
Weighted FMeasure: 0.6498802796666986
Iteration time: 29.0
Weighted AreaUnderPRC: 0.699839396862028
Mean absolute error: 0.3142258704682769
Coverage of cases: 88.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 243.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.5874285714285714
Kappa statistic: 0.13043478260869557
Training time: 25.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 72.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6731764705882353
Weighted AreaUnderROC: 0.6964190476190477
Root mean squared error: 0.48487889615885243
Relative absolute error: 61.562967746070626
Root relative squared error: 96.97577923177049
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.20167366934620648
Weighted FMeasure: 0.6712258064516128
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7159821158719322
Mean absolute error: 0.30781483873035315
Coverage of cases: 89.8
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 279.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.5488571428571429
Kappa statistic: 0.18333333333333318
Training time: 33.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 71.6
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6751557632398755
Weighted AreaUnderROC: 0.6942666666666667
Root mean squared error: 0.48582682356432527
Relative absolute error: 61.24393953517018
Root relative squared error: 97.16536471286506
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.203866594346794
Weighted FMeasure: 0.6713402348363787
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7128968668223736
Mean absolute error: 0.3062196976758509
Coverage of cases: 89.6
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 312.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.5518095238095239
Kappa statistic: 0.18344519015659952
Training time: 30.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 71.5
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.666098690042352
Weighted AreaUnderROC: 0.7003619047619047
Root mean squared error: 0.4814245197294754
Relative absolute error: 61.110628320127844
Root relative squared error: 96.28490394589508
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.1838018451761585
Weighted FMeasure: 0.6638479545071707
Iteration time: 56.0
Weighted AreaUnderPRC: 0.7200509551361675
Mean absolute error: 0.30555314160063923
Coverage of cases: 89.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 368.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.562
Kappa statistic: 0.1647982062780268
Training time: 55.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 71.1
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6333333333333333
Weighted AreaUnderROC: 0.6638857142857144
Root mean squared error: 0.5020450995654996
Relative absolute error: 64.53458899381427
Root relative squared error: 100.40901991309991
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.09912695235553012
Weighted FMeasure: 0.6267558528428094
Iteration time: 48.0
Weighted AreaUnderPRC: 0.6970897069574716
Mean absolute error: 0.32267294496907134
Coverage of cases: 88.2
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 416.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.6280952380952382
Kappa statistic: 0.07738095238095213
Training time: 46.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 71.4
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.632888888888889
Weighted AreaUnderROC: 0.6582857142857143
Root mean squared error: 0.5044023863623377
Relative absolute error: 64.75600413564986
Root relative squared error: 100.88047727246754
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.1018350154434631
Weighted FMeasure: 0.6295
Iteration time: 42.0
Weighted AreaUnderPRC: 0.6975114896043155
Mean absolute error: 0.3237800206782493
Coverage of cases: 87.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 458.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.6213333333333334
Kappa statistic: 0.08235294117647034
Training time: 41.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 70.4
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6368022328548645
Weighted AreaUnderROC: 0.656895238095238
Root mean squared error: 0.5035309215985909
Relative absolute error: 64.24472860182597
Root relative squared error: 100.70618431971819
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.1047587977749259
Weighted FMeasure: 0.6281087718400654
Iteration time: 48.0
Weighted AreaUnderPRC: 0.6958344555347193
Mean absolute error: 0.32122364300912987
Coverage of cases: 87.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 506.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.6272380952380953
Kappa statistic: 0.08114558472553698
Training time: 47.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 70.8
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6622486113771309
Weighted AreaUnderROC: 0.6886857142857143
Root mean squared error: 0.4912038635727748
Relative absolute error: 62.165395232648294
Root relative squared error: 98.24077271455496
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.15402232757219683
Weighted FMeasure: 0.6446136663620673
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7181939337848751
Mean absolute error: 0.31082697616324145
Coverage of cases: 89.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 563.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.6068571428571429
Kappa statistic: 0.12114014251781458
Training time: 56.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 78.8
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6578072502210434
Weighted AreaUnderROC: 0.6931428571428572
Root mean squared error: 0.4677630340699634
Relative absolute error: 62.50636604036951
Root relative squared error: 93.55260681399268
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.16221800605079348
Weighted FMeasure: 0.6546526440527329
Iteration time: 62.0
Weighted AreaUnderPRC: 0.7240405780370794
Mean absolute error: 0.31253183020184755
Coverage of cases: 92.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 625.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.578952380952381
Kappa statistic: 0.14204545454545436
Training time: 61.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 84.1
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7014279624893436
Weighted AreaUnderROC: 0.7163428571428572
Root mean squared error: 0.44833439007853537
Relative absolute error: 62.70969567815076
Root relative squared error: 89.66687801570707
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.27482496653239036
Weighted FMeasure: 0.7014849865893281
Iteration time: 87.0
Weighted AreaUnderPRC: 0.7425249087138399
Mean absolute error: 0.3135484783907538
Coverage of cases: 95.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 712.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.49161904761904757
Kappa statistic: 0.2612419700214133
Training time: 86.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 84.7
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6946792787466746
Weighted AreaUnderROC: 0.7197333333333333
Root mean squared error: 0.44597240843194413
Relative absolute error: 63.36462640156898
Root relative squared error: 89.19448168638883
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.26426433631576013
Weighted FMeasure: 0.6980646804176215
Iteration time: 127.0
Weighted AreaUnderPRC: 0.7436730698720765
Mean absolute error: 0.3168231320078449
Coverage of cases: 95.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 839.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.4836190476190476
Kappa statistic: 0.2557651991614256
Training time: 126.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.0
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6911860267341198
Weighted AreaUnderROC: 0.7216761904761906
Root mean squared error: 0.45076782032947493
Relative absolute error: 64.79557792670664
Root relative squared error: 90.15356406589498
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.26188533876711656
Weighted FMeasure: 0.6961171093192414
Iteration time: 135.0
Weighted AreaUnderPRC: 0.7459985992341778
Mean absolute error: 0.3239778896335332
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 974.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4612380952380952
Kappa statistic: 0.25907258064516125
Training time: 134.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 86.9
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7085438258962743
Weighted AreaUnderROC: 0.7426666666666667
Root mean squared error: 0.43867002724597276
Relative absolute error: 63.584818463007664
Root relative squared error: 87.73400544919456
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.3047052220223307
Weighted FMeasure: 0.7121922005157422
Iteration time: 170.0
Weighted AreaUnderPRC: 0.7601953337522939
Mean absolute error: 0.3179240923150383
Coverage of cases: 96.8
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1144.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.4256190476190476
Kappa statistic: 0.30335968379446643
Training time: 169.0
		
Time end:Mon Nov 27 16.01.47 EET 2017