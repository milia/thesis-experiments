Mon Nov 27 16.00.23 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.00.23 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 75.0
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.6603583426651735
Weighted AreaUnderROC: 0.6390666666666667
Root mean squared error: 0.5243740916865496
Relative absolute error: 72.02856523837198
Root relative squared error: 104.87481833730992
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.19044285955909346
Weighted FMeasure: 0.6521052124482128
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6826827138276553
Mean absolute error: 0.36014282619185994
Coverage of cases: 86.2
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.4488571428571429
Kappa statistic: 0.18956043956043955
Training time: 14.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 75.7
Incorrectly Classified Instances: 34.8
Correctly Classified Instances: 65.2
Weighted Precision: 0.6682674000423101
Weighted AreaUnderROC: 0.6768380952380953
Root mean squared error: 0.5108740065218038
Relative absolute error: 69.63680284262551
Root relative squared error: 102.17480130436076
Weighted TruePositiveRate: 0.652
Weighted MatthewsCorrelation: 0.20890160440448002
Weighted FMeasure: 0.6587113740959896
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7161095820367911
Mean absolute error: 0.34818401421312756
Coverage of cases: 88.0
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.652
Weighted FalsePositiveRate: 0.4348571428571429
Kappa statistic: 0.20765027322404384
Training time: 19.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 74.7
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.6744117647058825
Weighted AreaUnderROC: 0.6732571428571429
Root mean squared error: 0.5093925606524721
Relative absolute error: 68.50667103499465
Root relative squared error: 101.87851213049441
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.22454435656953592
Weighted FMeasure: 0.6709490416082282
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7112063272487705
Mean absolute error: 0.34253335517497324
Coverage of cases: 87.8
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.43942857142857145
Kappa statistic: 0.2242990654205609
Training time: 22.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 77.0
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.681223532970357
Weighted AreaUnderROC: 0.6803047619047619
Root mean squared error: 0.4939183031209895
Relative absolute error: 67.27179652851876
Root relative squared error: 98.78366062419789
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.24099756430674996
Weighted FMeasure: 0.6806011499269435
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7130759094840377
Mean absolute error: 0.3363589826425938
Coverage of cases: 90.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 101.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.4380952380952381
Kappa statistic: 0.24098671726755225
Training time: 28.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 75.5
Incorrectly Classified Instances: 31.4
Correctly Classified Instances: 68.6
Weighted Precision: 0.680714751377941
Weighted AreaUnderROC: 0.6916952380952383
Root mean squared error: 0.4897164026909349
Relative absolute error: 65.95418176545482
Root relative squared error: 97.94328053818698
Weighted TruePositiveRate: 0.686
Weighted MatthewsCorrelation: 0.23956894771367301
Weighted FMeasure: 0.6831372777107294
Iteration time: 43.0
Weighted AreaUnderPRC: 0.7193703662473514
Mean absolute error: 0.3297709088272741
Coverage of cases: 89.4
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 144.0
Weighted Recall: 0.686
Weighted FalsePositiveRate: 0.45076190476190475
Kappa statistic: 0.23934108527131787
Training time: 39.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 73.8
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7149262899262899
Weighted AreaUnderROC: 0.72
Root mean squared error: 0.4767716254250134
Relative absolute error: 61.87795158393629
Root relative squared error: 95.35432508500267
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.32123838997558063
Weighted FMeasure: 0.7154515382703303
Iteration time: 71.0
Weighted AreaUnderPRC: 0.7438343287298615
Mean absolute error: 0.30938975791968143
Coverage of cases: 90.2
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 215.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.396
Kappa statistic: 0.3212237093690248
Training time: 68.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 74.1
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7107936507936508
Weighted AreaUnderROC: 0.7262095238095239
Root mean squared error: 0.47041219523664024
Relative absolute error: 60.70756589681328
Root relative squared error: 94.08243904732805
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.31104631654389564
Weighted FMeasure: 0.7131034482758621
Iteration time: 183.0
Weighted AreaUnderPRC: 0.7509790045487039
Mean absolute error: 0.3035378294840664
Coverage of cases: 91.0
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 398.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.41123809523809524
Kappa statistic: 0.3106796116504854
Training time: 175.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 74.2
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.6856435260428759
Weighted AreaUnderROC: 0.7235428571428572
Root mean squared error: 0.4730090116075152
Relative absolute error: 61.462042702772266
Root relative squared error: 94.60180232150304
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.25148586783761784
Weighted FMeasure: 0.6867782555282556
Iteration time: 171.0
Weighted AreaUnderPRC: 0.7501893526266296
Mean absolute error: 0.30731021351386134
Coverage of cases: 90.4
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 569.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.43847619047619046
Kappa statistic: 0.2514395393474086
Training time: 164.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 74.8
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6948965434035108
Weighted AreaUnderROC: 0.7190095238095238
Root mean squared error: 0.4734012416419038
Relative absolute error: 61.51900780304042
Root relative squared error: 94.68024832838076
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.27301151175462857
Weighted FMeasure: 0.6979675611222151
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7441012384002991
Mean absolute error: 0.3075950390152021
Coverage of cases: 90.8
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 646.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.4362857142857143
Kappa statistic: 0.27246093749999983
Training time: 74.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 74.8
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7011023528714513
Weighted AreaUnderROC: 0.7261333333333332
Root mean squared error: 0.4695089543870419
Relative absolute error: 61.62045308414332
Root relative squared error: 93.90179087740837
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.28806468611725056
Weighted FMeasure: 0.7033196167100462
Iteration time: 86.0
Weighted AreaUnderPRC: 0.7500831946943992
Mean absolute error: 0.3081022654207166
Coverage of cases: 90.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 732.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.42314285714285715
Kappa statistic: 0.2877906976744184
Training time: 84.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 77.0
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6977401129943502
Weighted AreaUnderROC: 0.7205333333333332
Root mean squared error: 0.47330850799928675
Relative absolute error: 63.026253159238735
Root relative squared error: 94.66170159985735
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2802819595747497
Weighted FMeasure: 0.6988252457002456
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7479435583284689
Mean absolute error: 0.31513126579619366
Coverage of cases: 91.4
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 822.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.4219047619047619
Kappa statistic: 0.2802303262955852
Training time: 88.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 78.3
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7043360120252068
Weighted AreaUnderROC: 0.7212761904761905
Root mean squared error: 0.4723204154180942
Relative absolute error: 63.07161295439132
Root relative squared error: 94.46408308361885
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.29600756610477347
Weighted FMeasure: 0.7051424630371997
Iteration time: 107.0
Weighted AreaUnderPRC: 0.7502465965316895
Mean absolute error: 0.3153580647719566
Coverage of cases: 92.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 929.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4117142857142857
Kappa statistic: 0.2959770114942527
Training time: 106.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 78.8
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7054417866498405
Weighted AreaUnderROC: 0.7213333333333334
Root mean squared error: 0.47126521305858904
Relative absolute error: 63.588838541240555
Root relative squared error: 94.2530426117178
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2986675213467875
Weighted FMeasure: 0.7057180616319735
Iteration time: 96.0
Weighted AreaUnderPRC: 0.7489373930832847
Mean absolute error: 0.3179441927062028
Coverage of cases: 92.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1025.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4079047619047619
Kappa statistic: 0.2986641221374045
Training time: 95.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 80.3
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6992034968431277
Weighted AreaUnderROC: 0.7175047619047619
Root mean squared error: 0.4673976061900947
Relative absolute error: 64.39688414901448
Root relative squared error: 93.47952123801893
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.2837358360406192
Weighted FMeasure: 0.700531313859839
Iteration time: 110.0
Weighted AreaUnderPRC: 0.7463578031125092
Mean absolute error: 0.32198442074507244
Coverage of cases: 92.8
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1135.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.42104761904761906
Kappa statistic: 0.2836538461538461
Training time: 109.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 81.4
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6974263370236525
Weighted AreaUnderROC: 0.7219428571428571
Root mean squared error: 0.462971808813524
Relative absolute error: 64.61577429546081
Root relative squared error: 92.5943617627048
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2795833346792611
Weighted FMeasure: 0.6977103898396463
Iteration time: 114.0
Weighted AreaUnderPRC: 0.7495908297809099
Mean absolute error: 0.3230788714773041
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1249.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.41895238095238096
Kappa statistic: 0.2795801526717556
Training time: 113.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 83.1
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7083598311845984
Weighted AreaUnderROC: 0.7166285714285714
Root mean squared error: 0.46225916599301603
Relative absolute error: 65.42290967715144
Root relative squared error: 92.45183319860321
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.30558709898842307
Weighted FMeasure: 0.7091541302067617
Iteration time: 150.0
Weighted AreaUnderPRC: 0.7422543322501591
Mean absolute error: 0.3271145483857572
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1399.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.40619047619047616
Kappa statistic: 0.3055555555555554
Training time: 149.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 83.0
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7058045042953333
Weighted AreaUnderROC: 0.7158476190476191
Root mean squared error: 0.46226652015399383
Relative absolute error: 65.65466328916854
Root relative squared error: 92.45330403079876
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.2994793540661709
Weighted FMeasure: 0.7068565724815724
Iteration time: 149.0
Weighted AreaUnderPRC: 0.7420943297319474
Mean absolute error: 0.3282733164458427
Coverage of cases: 94.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1548.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4108571428571428
Kappa statistic: 0.2994241842610363
Training time: 148.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 85.3
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7021664609899904
Weighted AreaUnderROC: 0.7245333333333333
Root mean squared error: 0.4541895731901821
Relative absolute error: 65.3043341485003
Root relative squared error: 90.83791463803642
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2907064859628165
Weighted FMeasure: 0.703943500152063
Iteration time: 124.0
Weighted AreaUnderPRC: 0.748275194775051
Mean absolute error: 0.3265216707425015
Coverage of cases: 95.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1672.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.41933333333333334
Kappa statistic: 0.2905405405405404
Training time: 123.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 87.0
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7098006245496038
Weighted AreaUnderROC: 0.7307238095238096
Root mean squared error: 0.44572767554202813
Relative absolute error: 65.20103582878544
Root relative squared error: 89.14553510840562
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.3085202018184398
Weighted FMeasure: 0.7124765917602996
Iteration time: 148.0
Weighted AreaUnderPRC: 0.7514130253675589
Mean absolute error: 0.3260051791439272
Coverage of cases: 96.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1820.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.41504761904761905
Kappa statistic: 0.30799220272904476
Training time: 147.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 86.8
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7139082392505404
Weighted AreaUnderROC: 0.7313714285714286
Root mean squared error: 0.44593403244272106
Relative absolute error: 65.07424841987356
Root relative squared error: 89.1868064885442
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3182834993443398
Weighted FMeasure: 0.7165262172284644
Iteration time: 196.0
Weighted AreaUnderPRC: 0.7521436600926404
Mean absolute error: 0.32537124209936774
Coverage of cases: 95.8
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 2016.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4095238095238095
Kappa statistic: 0.3177387914230019
Training time: 196.0
		
Time end:Mon Nov 27 16.00.26 EET 2017