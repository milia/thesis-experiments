Mon Nov 27 15.59.46 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.46 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 72.5
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.6870900727110847
Weighted AreaUnderROC: 0.693047619047619
Root mean squared error: 0.531527841281769
Relative absolute error: 72.05847495869001
Root relative squared error: 106.3055682563538
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.24721542952316514
Weighted FMeasure: 0.6585699909823961
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7259685792551436
Mean absolute error: 0.36029237479345005
Coverage of cases: 85.2
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 25.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.3802857142857143
Kappa statistic: 0.2396907216494846
Training time: 19.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 78.2
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6966307825564335
Weighted AreaUnderROC: 0.6900571428571428
Root mean squared error: 0.4956811244480011
Relative absolute error: 67.93142585672119
Root relative squared error: 99.13622488960023
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.27615945714639306
Weighted FMeasure: 0.6879202088982993
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7224704859743446
Mean absolute error: 0.339657129283606
Coverage of cases: 89.4
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 56.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.3953333333333333
Kappa statistic: 0.2746350364963505
Training time: 22.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 78.6
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7230555555555556
Weighted AreaUnderROC: 0.7170095238095238
Root mean squared error: 0.4623376608270115
Relative absolute error: 62.16943598401434
Root relative squared error: 92.4675321654023
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.34020690871988585
Weighted FMeasure: 0.7252258377853326
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7468104516343005
Mean absolute error: 0.3108471799200717
Coverage of cases: 91.8
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 82.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.39466666666666667
Kappa statistic: 0.33980582524271846
Training time: 20.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 82.4
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.747328629032258
Weighted AreaUnderROC: 0.7416761904761904
Root mean squared error: 0.436651884689213
Relative absolute error: 59.889874626831286
Root relative squared error: 87.3303769378426
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.39601267260828843
Weighted FMeasure: 0.7500627752645529
Iteration time: 62.0
Weighted AreaUnderPRC: 0.7670943915460725
Mean absolute error: 0.2994493731341564
Coverage of cases: 94.0
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 144.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.37885714285714284
Kappa statistic: 0.39363817097415504
Training time: 54.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 79.6
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7244895226618464
Weighted AreaUnderROC: 0.7367428571428571
Root mean squared error: 0.44340468937909916
Relative absolute error: 59.22993380024584
Root relative squared error: 88.68093787581984
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.33915164878326165
Weighted FMeasure: 0.7277066695280286
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7552580235158897
Mean absolute error: 0.2961496690012292
Coverage of cases: 93.6
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 191.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.42466666666666664
Kappa statistic: 0.33434959349593485
Training time: 41.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 80.6
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.727579071829737
Weighted AreaUnderROC: 0.7401714285714286
Root mean squared error: 0.4350050918968835
Relative absolute error: 58.41327106229953
Root relative squared error: 87.0010183793767
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.3400778754227405
Weighted FMeasure: 0.7278329513623631
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7696458665063644
Mean absolute error: 0.29206635531149766
Coverage of cases: 93.8
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 226.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.444952380952381
Kappa statistic: 0.329140461215933
Training time: 30.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 81.1
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7182186607718523
Weighted AreaUnderROC: 0.7134857142857143
Root mean squared error: 0.44598423991019054
Relative absolute error: 60.29271051993989
Root relative squared error: 89.19684798203811
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.3010746245562794
Weighted FMeasure: 0.7082435274204854
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7419810727057177
Mean absolute error: 0.3014635525996994
Coverage of cases: 94.8
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 260.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.5008571428571429
Kappa statistic: 0.2754424778761062
Training time: 29.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 81.7
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7118181818181819
Weighted AreaUnderROC: 0.7148380952380953
Root mean squared error: 0.4476380754047853
Relative absolute error: 60.42035815425968
Root relative squared error: 89.52761508095706
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.26860765467512676
Weighted FMeasure: 0.6898372513562387
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7387806043943591
Mean absolute error: 0.3021017907712984
Coverage of cases: 94.0
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 310.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.5415238095238096
Kappa statistic: 0.22988505747126428
Training time: 45.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 81.4
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.6997010463378177
Weighted AreaUnderROC: 0.7159238095238095
Root mean squared error: 0.4510292812508224
Relative absolute error: 61.09264205583913
Root relative squared error: 90.20585625016449
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2362301496578584
Weighted FMeasure: 0.6757020396098137
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7415544486959912
Mean absolute error: 0.30546321027919565
Coverage of cases: 94.6
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 345.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.564
Kappa statistic: 0.1958041958041956
Training time: 30.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 79.8
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6974206977691298
Weighted AreaUnderROC: 0.7008
Root mean squared error: 0.4561830967703959
Relative absolute error: 60.624588802770496
Root relative squared error: 91.23661935407918
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.22459261536366165
Weighted FMeasure: 0.6689791027547224
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7313806774407628
Mean absolute error: 0.3031229440138525
Coverage of cases: 93.2
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 397.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5762857142857143
Kappa statistic: 0.1804245283018866
Training time: 49.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 77.9
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7172566371681417
Weighted AreaUnderROC: 0.6996952380952381
Root mean squared error: 0.45449405511969904
Relative absolute error: 59.21525770579984
Root relative squared error: 90.89881102393981
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.26074331599608525
Weighted FMeasure: 0.6800120909846596
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7344198793644005
Mean absolute error: 0.2960762885289992
Coverage of cases: 92.2
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 437.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.5643809523809523
Kappa statistic: 0.20803782505910168
Training time: 38.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 79.8
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7099137931034483
Weighted AreaUnderROC: 0.6760380952380951
Root mean squared error: 0.4628734227046303
Relative absolute error: 61.184332484283345
Root relative squared error: 92.57468454092606
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.22287124708010464
Weighted FMeasure: 0.6587461361654909
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7061086193251032
Mean absolute error: 0.3059216624214167
Coverage of cases: 92.2
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 484.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.5982857142857143
Kappa statistic: 0.1605839416058393
Training time: 44.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 78.1
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7301351576839646
Weighted AreaUnderROC: 0.6685523809523809
Root mean squared error: 0.4622933940836977
Relative absolute error: 60.492341736530484
Root relative squared error: 92.45867881673954
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.2652743083240304
Weighted FMeasure: 0.6740923414166324
Iteration time: 131.0
Weighted AreaUnderPRC: 0.698701345756549
Mean absolute error: 0.30246170868265243
Coverage of cases: 91.2
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 615.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.5787619047619048
Kappa statistic: 0.19685990338164258
Training time: 128.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 76.0
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6995185363505055
Weighted AreaUnderROC: 0.6736761904761904
Root mean squared error: 0.46569019184809646
Relative absolute error: 60.322032794718005
Root relative squared error: 93.1380383696193
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.19364525605059035
Weighted FMeasure: 0.645785522028616
Iteration time: 125.0
Weighted AreaUnderPRC: 0.6961171742760602
Mean absolute error: 0.30161016397359003
Coverage of cases: 90.8
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 740.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.6160952380952381
Kappa statistic: 0.13177339901477816
Training time: 123.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 75.5
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.6948936170212767
Weighted AreaUnderROC: 0.6723809523809524
Root mean squared error: 0.46730596426930154
Relative absolute error: 60.04015781097047
Root relative squared error: 93.46119285386031
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.18377261547332374
Weighted FMeasure: 0.6421138211382114
Iteration time: 140.0
Weighted AreaUnderPRC: 0.6952321820121308
Mean absolute error: 0.30020078905485237
Coverage of cases: 90.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 880.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.6207619047619047
Kappa statistic: 0.12345679012345658
Training time: 139.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 83.9
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6855055174470623
Weighted AreaUnderROC: 0.7085523809523809
Root mean squared error: 0.4654985291869726
Relative absolute error: 61.739071526993826
Root relative squared error: 93.09970583739452
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.1457765779990091
Weighted FMeasure: 0.6231773644001439
Iteration time: 119.0
Weighted AreaUnderPRC: 0.7376704539544646
Mean absolute error: 0.30869535763496914
Coverage of cases: 94.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 999.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.6461904761904763
Kappa statistic: 0.0845959595959594
Training time: 118.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 85.4
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7367114173696091
Weighted AreaUnderROC: 0.7608761904761905
Root mean squared error: 0.42905690189800316
Relative absolute error: 59.24359484317162
Root relative squared error: 85.81138037960064
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.31447530284267977
Weighted FMeasure: 0.7038361021254821
Iteration time: 108.0
Weighted AreaUnderPRC: 0.7806547696097336
Mean absolute error: 0.2962179742158581
Coverage of cases: 96.4
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1107.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.5279047619047618
Kappa statistic: 0.26504629629629634
Training time: 107.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 90.4
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7447819314641745
Weighted AreaUnderROC: 0.7733714285714286
Root mean squared error: 0.4180765068016726
Relative absolute error: 60.3250445521289
Root relative squared error: 83.61530136033451
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.35303727313713107
Weighted FMeasure: 0.7253664976030013
Iteration time: 142.0
Weighted AreaUnderPRC: 0.7873411623919382
Mean absolute error: 0.3016252227606445
Coverage of cases: 98.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1249.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.48552380952380947
Kappa statistic: 0.3176733780760627
Training time: 141.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 91.8
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.7516328331862312
Weighted AreaUnderROC: 0.786704761904762
Root mean squared error: 0.40579236742445224
Relative absolute error: 60.37061589861633
Root relative squared error: 81.15847348489045
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.385070000857583
Weighted FMeasure: 0.7428615540704471
Iteration time: 122.0
Weighted AreaUnderPRC: 0.8031943325447856
Mean absolute error: 0.30185307949308166
Coverage of cases: 99.4
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 1371.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.444
Kappa statistic: 0.3628509719222462
Training time: 122.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 93.6
Incorrectly Classified Instances: 22.0
Correctly Classified Instances: 78.0
Weighted Precision: 0.7699300699300698
Weighted AreaUnderROC: 0.7904571428571429
Root mean squared error: 0.40356470104560804
Relative absolute error: 61.563605409900504
Root relative squared error: 80.71294020912161
Weighted TruePositiveRate: 0.78
Weighted MatthewsCorrelation: 0.44249767886870983
Weighted FMeasure: 0.7690228690228691
Iteration time: 158.0
Weighted AreaUnderPRC: 0.8042531640032845
Mean absolute error: 0.3078180270495025
Coverage of cases: 99.6
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 1529.0
Weighted Recall: 0.78
Weighted FalsePositiveRate: 0.38
Kappa statistic: 0.43298969072164956
Training time: 158.0
		
Time end:Mon Nov 27 15.59.49 EET 2017