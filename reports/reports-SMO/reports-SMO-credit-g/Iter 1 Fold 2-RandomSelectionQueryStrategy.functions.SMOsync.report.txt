Mon Nov 27 16.01.09 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Mon Nov 27 16.01.09 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 67.9
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7069811719906918
Weighted AreaUnderROC: 0.7153904761904762
Root mean squared error: 0.5064036585225311
Relative absolute error: 63.10297553060094
Root relative squared error: 101.28073170450622
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.3005251151081993
Weighted FMeasure: 0.6979399517861056
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7480932388708506
Mean absolute error: 0.31551487765300473
Coverage of cases: 84.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.37961904761904763
Kappa statistic: 0.29872495446265934
Training time: 14.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 75.0
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.7009163160355306
Weighted AreaUnderROC: 0.7015047619047619
Root mean squared error: 0.49258716574843564
Relative absolute error: 65.20855284768552
Root relative squared error: 98.51743314968712
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.28781590867080314
Weighted FMeasure: 0.6993890789008137
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7344718930581172
Mean absolute error: 0.32604276423842765
Coverage of cases: 89.8
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 33.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.4075238095238095
Kappa statistic: 0.28773584905660377
Training time: 17.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 76.9
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.6992548761779532
Weighted AreaUnderROC: 0.6870666666666667
Root mean squared error: 0.5008007711035227
Relative absolute error: 67.76798658061021
Root relative squared error: 100.16015422070454
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.28349674628949434
Weighted FMeasure: 0.6952370304114491
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7200493051932279
Mean absolute error: 0.33883993290305103
Coverage of cases: 88.6
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 58.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.4024761904761905
Kappa statistic: 0.28305400372439465
Training time: 24.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 81.4
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7173869129211458
Weighted AreaUnderROC: 0.7104571428571429
Root mean squared error: 0.4753202703252476
Relative absolute error: 65.85723227258345
Root relative squared error: 95.06405406504952
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.3262163609272455
Weighted FMeasure: 0.7119772003849286
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7413132931648363
Mean absolute error: 0.3292861613629172
Coverage of cases: 92.2
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 116.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.3727619047619048
Kappa statistic: 0.32532347504621056
Training time: 52.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 82.4
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7513719512195122
Weighted AreaUnderROC: 0.7456380952380952
Root mean squared error: 0.44559936331633426
Relative absolute error: 62.03299762378136
Root relative squared error: 89.11987266326685
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.40716730778100874
Weighted FMeasure: 0.7470947614714676
Iteration time: 60.0
Weighted AreaUnderPRC: 0.7629989908600409
Mean absolute error: 0.3101649881189068
Coverage of cases: 93.4
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 176.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.32685714285714285
Kappa statistic: 0.4063079777365492
Training time: 52.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 85.6
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7445742036191246
Weighted AreaUnderROC: 0.7600571428571429
Root mean squared error: 0.4344808110378537
Relative absolute error: 61.31542543914006
Root relative squared error: 86.89616220757074
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.39180289494126974
Weighted FMeasure: 0.745259134732819
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7741585317041636
Mean absolute error: 0.3065771271957003
Coverage of cases: 95.8
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 244.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.3564761904761905
Kappa statistic: 0.39176245210727967
Training time: 63.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 88.3
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7348254301997456
Weighted AreaUnderROC: 0.7582476190476191
Root mean squared error: 0.43634966441387407
Relative absolute error: 63.82053978761098
Root relative squared error: 87.26993288277481
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.36830460701498985
Weighted FMeasure: 0.7321737175614347
Iteration time: 76.0
Weighted AreaUnderPRC: 0.7739438640993171
Mean absolute error: 0.31910269893805493
Coverage of cases: 96.8
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 320.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.3557142857142857
Kappa statistic: 0.36797752808988765
Training time: 69.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 89.1
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7111879599402423
Weighted AreaUnderROC: 0.7210285714285715
Root mean squared error: 0.455306015079673
Relative absolute error: 67.33604943400339
Root relative squared error: 91.0612030159346
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.31207489602033495
Weighted FMeasure: 0.708366936900229
Iteration time: 152.0
Weighted AreaUnderPRC: 0.7469388077601898
Mean absolute error: 0.33668024717001693
Coverage of cases: 97.0
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 472.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.3888571428571429
Kappa statistic: 0.3117977528089887
Training time: 144.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 87.1
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.6988636363636364
Weighted AreaUnderROC: 0.7205523809523808
Root mean squared error: 0.45353446908587525
Relative absolute error: 66.7315642634671
Root relative squared error: 90.70689381717504
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.2829957245022972
Weighted FMeasure: 0.6994206390179545
Iteration time: 110.0
Weighted AreaUnderPRC: 0.7497119092615309
Mean absolute error: 0.33365782131733557
Coverage of cases: 96.2
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 582.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.41809523809523813
Kappa statistic: 0.28298279158699796
Training time: 102.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 88.8
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7340298738487733
Weighted AreaUnderROC: 0.7525523809523809
Root mean squared error: 0.43549315763737967
Relative absolute error: 64.74175982684378
Root relative squared error: 87.09863152747593
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3666702347861451
Weighted FMeasure: 0.7349662162162163
Iteration time: 108.0
Weighted AreaUnderPRC: 0.7697599800991083
Mean absolute error: 0.3237087991342189
Coverage of cases: 97.2
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 690.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.3721904761904761
Kappa statistic: 0.36660268714011507
Training time: 101.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 91.0
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7269462734465777
Weighted AreaUnderROC: 0.7481333333333332
Root mean squared error: 0.4370163202117125
Relative absolute error: 67.03081934525959
Root relative squared error: 87.4032640423425
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.3495613700167716
Weighted FMeasure: 0.7242381240076996
Iteration time: 144.0
Weighted AreaUnderPRC: 0.7718357373267741
Mean absolute error: 0.3351540967262979
Coverage of cases: 98.8
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 834.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.3667619047619048
Kappa statistic: 0.349250936329588
Training time: 138.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 93.1
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7051798731701535
Weighted AreaUnderROC: 0.746247619047619
Root mean squared error: 0.4330738869338258
Relative absolute error: 67.06500573287276
Root relative squared error: 86.61477738676517
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.29776383379796606
Weighted FMeasure: 0.7073560845099095
Iteration time: 145.0
Weighted AreaUnderPRC: 0.7756041469791599
Mean absolute error: 0.3353250286643638
Coverage of cases: 99.4
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 979.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.4176190476190476
Kappa statistic: 0.29748062015503857
Training time: 145.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 93.8
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7327705733626947
Weighted AreaUnderROC: 0.764095238095238
Root mean squared error: 0.4214113712721653
Relative absolute error: 66.04581142048148
Root relative squared error: 84.28227425443306
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.36261455262386894
Weighted FMeasure: 0.735581084270989
Iteration time: 91.0
Weighted AreaUnderPRC: 0.7897201285045751
Mean absolute error: 0.3302290571024074
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1070.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.3895238095238095
Kappa statistic: 0.36149312377210213
Training time: 90.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 93.7
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7252922071205445
Weighted AreaUnderROC: 0.7590666666666667
Root mean squared error: 0.4236154979504705
Relative absolute error: 66.24071353116129
Root relative squared error: 84.7230995900941
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3444062281490187
Weighted FMeasure: 0.728521721053856
Iteration time: 115.0
Weighted AreaUnderPRC: 0.7826860439029621
Mean absolute error: 0.33120356765580644
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1185.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4035238095238095
Kappa statistic: 0.34288537549407117
Training time: 114.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 93.3
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7272
Weighted AreaUnderROC: 0.770952380952381
Root mean squared error: 0.4176708730353598
Relative absolute error: 65.22161694532336
Root relative squared error: 83.53417460707196
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.34772731516848904
Weighted FMeasure: 0.7306081504702194
Iteration time: 103.0
Weighted AreaUnderPRC: 0.7932046495315148
Mean absolute error: 0.3261080847266168
Coverage of cases: 99.4
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 1288.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.4094285714285714
Kappa statistic: 0.34500000000000003
Training time: 103.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 93.6
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7301227676137726
Weighted AreaUnderROC: 0.772457142857143
Root mean squared error: 0.41552097371451835
Relative absolute error: 64.6767665685455
Root relative squared error: 83.10419474290367
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.3535961577906205
Weighted FMeasure: 0.733327259198518
Iteration time: 118.0
Weighted AreaUnderPRC: 0.7915407918112135
Mean absolute error: 0.32338383284272754
Coverage of cases: 99.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1406.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.41152380952380946
Kappa statistic: 0.34979838709677424
Training time: 117.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 93.6
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7232894736842105
Weighted AreaUnderROC: 0.7715238095238094
Root mean squared error: 0.416799398918033
Relative absolute error: 64.38260620284841
Root relative squared error: 83.3598797836066
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3372266367167312
Weighted FMeasure: 0.7267579908675798
Iteration time: 174.0
Weighted AreaUnderPRC: 0.7909713741514977
Mean absolute error: 0.321913031014242
Coverage of cases: 99.6
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1580.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4217142857142857
Kappa statistic: 0.3333333333333333
Training time: 173.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.4
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7226195758274914
Weighted AreaUnderROC: 0.7715809523809524
Root mean squared error: 0.4177139652929459
Relative absolute error: 63.838137177495824
Root relative squared error: 83.54279305858918
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.33506978546485616
Weighted FMeasure: 0.7260092977734278
Iteration time: 315.0
Weighted AreaUnderPRC: 0.7925835286058676
Mean absolute error: 0.3191906858874791
Coverage of cases: 99.2
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 1895.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.42552380952380947
Kappa statistic: 0.33062880324543603
Training time: 311.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.4
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7207566273932253
Weighted AreaUnderROC: 0.7731238095238095
Root mean squared error: 0.41622246964541554
Relative absolute error: 63.7828685961778
Root relative squared error: 83.2444939290831
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.3286965354871651
Weighted FMeasure: 0.7236517097995409
Iteration time: 452.0
Weighted AreaUnderPRC: 0.7943657096882522
Mean absolute error: 0.318914342980889
Coverage of cases: 99.4
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2347.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.4369523809523809
Kappa statistic: 0.3223819301848048
Training time: 446.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 94.1
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7232832544419291
Weighted AreaUnderROC: 0.7712380952380953
Root mean squared error: 0.4163027389225099
Relative absolute error: 64.2127031794636
Root relative squared error: 83.26054778450198
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.33496596423981667
Weighted FMeasure: 0.7261470043491495
Iteration time: 216.0
Weighted AreaUnderPRC: 0.7943443350894287
Mean absolute error: 0.321063515897318
Coverage of cases: 99.6
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 2563.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.43228571428571433
Kappa statistic: 0.3288934426229507
Training time: 216.0
		
Time end:Mon Nov 27 16.01.13 EET 2017