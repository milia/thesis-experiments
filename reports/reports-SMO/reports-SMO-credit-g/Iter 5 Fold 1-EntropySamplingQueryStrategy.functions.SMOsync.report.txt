Mon Nov 27 15.59.38 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.38 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 79.1
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6773877129581828
Weighted AreaUnderROC: 0.6868761904761905
Root mean squared error: 0.48319580000102763
Relative absolute error: 66.58593716349974
Root relative squared error: 96.63916000020552
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.23187286801044518
Weighted FMeasure: 0.6776912103588282
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7303736716087919
Mean absolute error: 0.33292968581749866
Coverage of cases: 91.4
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 47.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.44657142857142856
Kappa statistic: 0.23187022900763374
Training time: 41.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 77.0
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6946227309821567
Weighted AreaUnderROC: 0.7045142857142858
Root mean squared error: 0.468887148865199
Relative absolute error: 62.65402485253331
Root relative squared error: 93.7774297730398
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.26953187506505144
Weighted FMeasure: 0.6994390736323923
Iteration time: 30.0
Weighted AreaUnderPRC: 0.744708525726001
Mean absolute error: 0.31327012426266654
Coverage of cases: 92.6
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 77.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.4595238095238095
Kappa statistic: 0.26619433198380554
Training time: 26.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 78.0
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6999236058059587
Weighted AreaUnderROC: 0.7001904761904761
Root mean squared error: 0.4713648435470691
Relative absolute error: 63.48356924034223
Root relative squared error: 94.27296870941382
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.28347736504678867
Weighted FMeasure: 0.7042517415325487
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7389749921715661
Mean absolute error: 0.31741784620171115
Coverage of cases: 93.0
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 105.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4434285714285715
Kappa statistic: 0.281437125748503
Training time: 23.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 75.2
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7110918972332017
Weighted AreaUnderROC: 0.7117142857142857
Root mean squared error: 0.47454126934188046
Relative absolute error: 62.21679766117316
Root relative squared error: 94.9082538683761
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.3108916986413163
Weighted FMeasure: 0.7145735790908551
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7423238754074439
Mean absolute error: 0.3110839883058658
Coverage of cases: 90.4
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4209523809523809
Kappa statistic: 0.30966469428007876
Training time: 25.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 77.5
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7194591627789154
Weighted AreaUnderROC: 0.716
Root mean squared error: 0.46740663141252764
Relative absolute error: 61.28301459392581
Root relative squared error: 93.48132628250553
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.32986024271588926
Weighted FMeasure: 0.7230852195275401
Iteration time: 44.0
Weighted AreaUnderPRC: 0.7411145546805246
Mean absolute error: 0.30641507296962905
Coverage of cases: 92.4
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 178.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.41666666666666663
Kappa statistic: 0.3276892430278883
Training time: 40.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 74.6
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7209269662921348
Weighted AreaUnderROC: 0.7236190476190476
Root mean squared error: 0.465549219343489
Relative absolute error: 60.07182386828338
Root relative squared error: 93.1098438686978
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.33540022649293816
Weighted FMeasure: 0.7223564780019656
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7443548246164571
Mean absolute error: 0.3003591193414169
Coverage of cases: 90.6
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 216.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.39257142857142857
Kappa statistic: 0.33526011560693636
Training time: 34.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 74.8
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7101333333333333
Weighted AreaUnderROC: 0.7215047619047619
Root mean squared error: 0.4676018453965457
Relative absolute error: 60.55640516495067
Root relative squared error: 93.52036907930913
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.30741110471417155
Weighted FMeasure: 0.7141567398119122
Iteration time: 63.0
Weighted AreaUnderPRC: 0.7514275387042663
Mean absolute error: 0.30278202582475333
Coverage of cases: 90.4
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 279.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.43152380952380953
Kappa statistic: 0.305
Training time: 58.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 76.1
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7076349275998244
Weighted AreaUnderROC: 0.7288000000000001
Root mean squared error: 0.46410518066347267
Relative absolute error: 60.93799584595197
Root relative squared error: 92.82103613269453
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.302239464549685
Weighted FMeasure: 0.7114938928906983
Iteration time: 54.0
Weighted AreaUnderPRC: 0.7554900589999816
Mean absolute error: 0.3046899792297598
Coverage of cases: 92.2
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 333.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.42942857142857144
Kappa statistic: 0.300595238095238
Training time: 50.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 76.8
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6825334176497524
Weighted AreaUnderROC: 0.7192190476190476
Root mean squared error: 0.47018524324807626
Relative absolute error: 62.52528866983526
Root relative squared error: 94.03704864961526
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.2415051567618935
Weighted FMeasure: 0.6878481871238467
Iteration time: 66.0
Weighted AreaUnderPRC: 0.7501447769628137
Mean absolute error: 0.31262644334917633
Coverage of cases: 92.0
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 399.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.47228571428571425
Kappa statistic: 0.2389112903225806
Training time: 62.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 75.6
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6911860267341198
Weighted AreaUnderROC: 0.7237333333333333
Root mean squared error: 0.4692335028478244
Relative absolute error: 61.77848623137717
Root relative squared error: 93.84670056956487
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.26188533876711656
Weighted FMeasure: 0.6961171093192414
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7525684706710669
Mean absolute error: 0.30889243115688586
Coverage of cases: 91.2
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 483.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4612380952380952
Kappa statistic: 0.25907258064516125
Training time: 83.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 78.9
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6999236058059587
Weighted AreaUnderROC: 0.7247047619047619
Root mean squared error: 0.462276373579344
Relative absolute error: 62.36606143129077
Root relative squared error: 92.4552747158688
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.28347736504678867
Weighted FMeasure: 0.7042517415325487
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7545664023339097
Mean absolute error: 0.3118303071564538
Coverage of cases: 93.8
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 568.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4434285714285715
Kappa statistic: 0.281437125748503
Training time: 84.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 79.9
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6867007902298851
Weighted AreaUnderROC: 0.7329523809523808
Root mean squared error: 0.45491059562807296
Relative absolute error: 61.96359101395349
Root relative squared error: 90.98211912561459
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.25021371543850535
Weighted FMeasure: 0.6919382925979801
Iteration time: 88.0
Weighted AreaUnderPRC: 0.76160499066703
Mean absolute error: 0.30981795506976745
Coverage of cases: 95.0
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 656.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4735238095238095
Kappa statistic: 0.24643584521384926
Training time: 86.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 80.5
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.6848950662739323
Weighted AreaUnderROC: 0.736095238095238
Root mean squared error: 0.4529907880295476
Relative absolute error: 62.05022448457821
Root relative squared error: 90.59815760590952
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.24495219523565806
Weighted FMeasure: 0.6901549473510001
Iteration time: 88.0
Weighted AreaUnderPRC: 0.7657733696511209
Mean absolute error: 0.31025112242289105
Coverage of cases: 95.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 744.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.4811428571428571
Kappa statistic: 0.24024640657084168
Training time: 87.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 82.4
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.6893552922273549
Weighted AreaUnderROC: 0.7389142857142857
Root mean squared error: 0.4485140712457445
Relative absolute error: 62.41428285821636
Root relative squared error: 89.7028142491489
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.2566831627569366
Weighted FMeasure: 0.6944494688596963
Iteration time: 116.0
Weighted AreaUnderPRC: 0.7706353461005633
Mean absolute error: 0.3120714142910818
Coverage of cases: 96.6
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 860.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.4688571428571429
Kappa statistic: 0.25304878048780466
Training time: 114.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 83.2
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6963705741414501
Weighted AreaUnderROC: 0.7374666666666667
Root mean squared error: 0.4461305941547559
Relative absolute error: 62.57642997576271
Root relative squared error: 89.22611883095118
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.27340049979647774
Weighted FMeasure: 0.7011010521164669
Iteration time: 142.0
Weighted AreaUnderPRC: 0.7705817315544509
Mean absolute error: 0.3128821498788135
Coverage of cases: 96.4
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1002.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.45866666666666667
Kappa statistic: 0.2697768762677483
Training time: 141.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 83.3
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.6981177899210687
Weighted AreaUnderROC: 0.7381714285714286
Root mean squared error: 0.44575828800377637
Relative absolute error: 62.66199087193767
Root relative squared error: 89.15165760075527
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.2784294055683611
Weighted FMeasure: 0.7027149321266968
Iteration time: 142.0
Weighted AreaUnderPRC: 0.7728566423244404
Mean absolute error: 0.31330995435968834
Coverage of cases: 96.6
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 1144.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.4510476190476191
Kappa statistic: 0.2756539235412473
Training time: 136.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 84.1
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.6937470710316663
Weighted AreaUnderROC: 0.7313142857142857
Root mean squared error: 0.4468952232225906
Relative absolute error: 63.6707879214576
Root relative squared error: 89.37904464451812
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.2669917235102272
Weighted FMeasure: 0.6986066189432376
Iteration time: 260.0
Weighted AreaUnderPRC: 0.7650863272134346
Mean absolute error: 0.31835393960728803
Coverage of cases: 96.6
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 1404.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.4633333333333334
Kappa statistic: 0.26321138211382095
Training time: 254.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 85.5
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.6885314685314686
Weighted AreaUnderROC: 0.7327428571428571
Root mean squared error: 0.4453659320186646
Relative absolute error: 64.10361241555314
Root relative squared error: 89.07318640373292
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.25285581649640554
Weighted FMeasure: 0.6934303534303534
Iteration time: 296.0
Weighted AreaUnderPRC: 0.7663436328429234
Mean absolute error: 0.32051806207776573
Coverage of cases: 97.6
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 1700.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.4794285714285714
Kappa statistic: 0.24742268041237106
Training time: 288.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.0
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.6804095027466648
Weighted AreaUnderROC: 0.7307238095238096
Root mean squared error: 0.44550933533920245
Relative absolute error: 64.58499379871797
Root relative squared error: 89.10186706784049
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.23304849542300854
Weighted FMeasure: 0.685693188304853
Iteration time: 337.0
Weighted AreaUnderPRC: 0.764432879571195
Mean absolute error: 0.32292496899358986
Coverage of cases: 97.6
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 2037.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.49342857142857144
Kappa statistic: 0.22717842323651452
Training time: 336.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 86.8
Incorrectly Classified Instances: 30.2
Correctly Classified Instances: 69.8
Weighted Precision: 0.6739844953657284
Weighted AreaUnderROC: 0.7273523809523811
Root mean squared error: 0.44511977430757893
Relative absolute error: 64.8799867787108
Root relative squared error: 89.02395486151579
Weighted TruePositiveRate: 0.698
Weighted MatthewsCorrelation: 0.21690648885322286
Weighted FMeasure: 0.6794492859448332
Iteration time: 150.0
Weighted AreaUnderPRC: 0.7619385255870785
Mean absolute error: 0.324399933893554
Coverage of cases: 97.8
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 2187.0
Weighted Recall: 0.698
Weighted FalsePositiveRate: 0.5065714285714286
Kappa statistic: 0.21025104602510442
Training time: 150.0
		
Time end:Mon Nov 27 15.59.41 EET 2017