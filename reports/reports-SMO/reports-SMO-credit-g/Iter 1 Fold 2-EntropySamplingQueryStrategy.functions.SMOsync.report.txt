Mon Nov 27 15.59.14 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.14 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 72.5
Incorrectly Classified Instances: 35.4
Correctly Classified Instances: 64.6
Weighted Precision: 0.6870900727110847
Weighted AreaUnderROC: 0.693047619047619
Root mean squared error: 0.531527841281769
Relative absolute error: 72.05847495869001
Root relative squared error: 106.3055682563538
Weighted TruePositiveRate: 0.646
Weighted MatthewsCorrelation: 0.24721542952316514
Weighted FMeasure: 0.6585699909823961
Iteration time: 48.0
Weighted AreaUnderPRC: 0.7259685792551436
Mean absolute error: 0.36029237479345005
Coverage of cases: 85.2
Instances selection time: 14.0
Test time: 14.0
Accumulative iteration time: 48.0
Weighted Recall: 0.646
Weighted FalsePositiveRate: 0.3802857142857143
Kappa statistic: 0.2396907216494846
Training time: 34.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 75.1
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7381341469721259
Weighted AreaUnderROC: 0.7446095238095238
Root mean squared error: 0.46763871088330744
Relative absolute error: 61.1292003006453
Root relative squared error: 93.52774217666149
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.37237198385807607
Weighted FMeasure: 0.72499696097897
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7625243741508397
Mean absolute error: 0.3056460015032265
Coverage of cases: 89.8
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 103.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.32657142857142857
Kappa statistic: 0.36827956989247307
Training time: 42.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 76.7
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7329170924331908
Weighted AreaUnderROC: 0.771447619047619
Root mean squared error: 0.4567774366074126
Relative absolute error: 60.589349150210126
Root relative squared error: 91.35548732148251
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.36053400290265
Weighted FMeasure: 0.720766441322441
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7903095970859174
Mean absolute error: 0.30294674575105063
Coverage of cases: 92.8
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 171.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.33590476190476193
Kappa statistic: 0.3570143884892085
Training time: 56.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 80.8
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7227920227920227
Weighted AreaUnderROC: 0.7509333333333332
Root mean squared error: 0.455755471275991
Relative absolute error: 62.78159688553255
Root relative squared error: 91.15109425519819
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.3394500514782104
Weighted FMeasure: 0.7189847942754919
Iteration time: 80.0
Weighted AreaUnderPRC: 0.7668107591095812
Mean absolute error: 0.31390798442766277
Coverage of cases: 94.0
Instances selection time: 13.0
Test time: 16.0
Accumulative iteration time: 251.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.36933333333333335
Kappa statistic: 0.3389199255121042
Training time: 67.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 82.6
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7188691650230112
Weighted AreaUnderROC: 0.7635428571428572
Root mean squared error: 0.44670119908845235
Relative absolute error: 62.105456161609865
Root relative squared error: 89.34023981769047
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.33012450061342435
Weighted FMeasure: 0.7150268336314848
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7813506543149972
Mean absolute error: 0.3105272808080493
Coverage of cases: 95.0
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 326.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.3748571428571429
Kappa statistic: 0.3296089385474859
Training time: 69.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 80.5
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7370160314579552
Weighted AreaUnderROC: 0.7629333333333334
Root mean squared error: 0.44794604718803993
Relative absolute error: 60.65276290194801
Root relative squared error: 89.58920943760799
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.37383086746795074
Weighted FMeasure: 0.7364959486897285
Iteration time: 96.0
Weighted AreaUnderPRC: 0.7811702472470303
Mean absolute error: 0.30326381450974005
Coverage of cases: 94.4
Instances selection time: 10.0
Test time: 10.0
Accumulative iteration time: 422.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.36076190476190484
Kappa statistic: 0.37381404174573046
Training time: 86.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 79.9
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7267148805610344
Weighted AreaUnderROC: 0.7787809523809524
Root mean squared error: 0.4396183529575053
Relative absolute error: 58.6347547305909
Root relative squared error: 87.92367059150106
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.34877560234299637
Weighted FMeasure: 0.7229427549194992
Iteration time: 88.0
Weighted AreaUnderPRC: 0.7931577151537085
Mean absolute error: 0.2931737736529545
Coverage of cases: 94.8
Instances selection time: 10.0
Test time: 10.0
Accumulative iteration time: 510.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.36380952380952386
Kappa statistic: 0.34823091247672244
Training time: 78.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 78.1
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7638611296172921
Weighted AreaUnderROC: 0.7954095238095238
Root mean squared error: 0.42682578740105914
Relative absolute error: 54.97520452577505
Root relative squared error: 85.36515748021183
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.4374562974219879
Weighted FMeasure: 0.7617295998798891
Iteration time: 124.0
Weighted AreaUnderPRC: 0.8096929961611546
Mean absolute error: 0.2748760226288752
Coverage of cases: 94.6
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 634.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.3161904761904762
Kappa statistic: 0.4371482176360225
Training time: 119.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 77.9
Incorrectly Classified Instances: 23.2
Correctly Classified Instances: 76.8
Weighted Precision: 0.7698220854290219
Weighted AreaUnderROC: 0.8013904761904762
Root mean squared error: 0.4233035642021846
Relative absolute error: 54.28218174529231
Root relative squared error: 84.66071284043693
Weighted TruePositiveRate: 0.768
Weighted MatthewsCorrelation: 0.4518765865192701
Weighted FMeasure: 0.768859649122807
Iteration time: 109.0
Weighted AreaUnderPRC: 0.8118755309366097
Mean absolute error: 0.27141090872646156
Coverage of cases: 94.8
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 743.0
Weighted Recall: 0.768
Weighted FalsePositiveRate: 0.3127619047619048
Kappa statistic: 0.4517958412098299
Training time: 104.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 81.9
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.764
Weighted AreaUnderROC: 0.8019809523809525
Root mean squared error: 0.41196649394139884
Relative absolute error: 55.76305710139443
Root relative squared error: 82.39329878827976
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.4380952380952381
Weighted FMeasure: 0.764
Iteration time: 137.0
Weighted AreaUnderPRC: 0.8126398020642307
Mean absolute error: 0.27881528550697215
Coverage of cases: 95.4
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 880.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.32590476190476186
Kappa statistic: 0.4380952380952382
Training time: 130.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 85.0
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.7719378338999513
Weighted AreaUnderROC: 0.8015238095238096
Root mean squared error: 0.40569483300712317
Relative absolute error: 56.893599183293134
Root relative squared error: 81.13896660142463
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.45686278684506487
Weighted FMeasure: 0.7728861642024282
Iteration time: 181.0
Weighted AreaUnderPRC: 0.8108166641273555
Mean absolute error: 0.28446799591646565
Coverage of cases: 97.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1061.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.3216190476190476
Kappa statistic: 0.45673076923076933
Training time: 176.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 86.5
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7549843862599087
Weighted AreaUnderROC: 0.7970095238095238
Root mean squared error: 0.40588399221537563
Relative absolute error: 57.84330599059704
Root relative squared error: 81.17679844307513
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.4159164746033398
Weighted FMeasure: 0.7570224719101124
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8103720656130051
Mean absolute error: 0.2892165299529852
Coverage of cases: 97.8
Instances selection time: 2.0
Test time: 19.0
Accumulative iteration time: 1216.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.35428571428571426
Kappa statistic: 0.4152046783625731
Training time: 153.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 87.7
Incorrectly Classified Instances: 23.2
Correctly Classified Instances: 76.8
Weighted Precision: 0.761830193295816
Weighted AreaUnderROC: 0.7964952380952381
Root mean squared error: 0.40489679357170183
Relative absolute error: 58.121283156388415
Root relative squared error: 80.97935871434036
Weighted TruePositiveRate: 0.768
Weighted MatthewsCorrelation: 0.4315901468729745
Weighted FMeasure: 0.7640569675033442
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8089556467398277
Mean absolute error: 0.2906064157819421
Coverage of cases: 98.2
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 1361.0
Weighted Recall: 0.768
Weighted FalsePositiveRate: 0.3508571428571428
Kappa statistic: 0.43025540275049123
Training time: 139.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 90.2
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7666289592760182
Weighted AreaUnderROC: 0.8029714285714287
Root mean squared error: 0.4009769257352179
Relative absolute error: 59.20056920113108
Root relative squared error: 80.19538514704358
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.4433111365075938
Weighted FMeasure: 0.7686548745372275
Iteration time: 193.0
Weighted AreaUnderPRC: 0.8164419711639475
Mean absolute error: 0.2960028460056554
Coverage of cases: 99.0
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 1554.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.3415238095238095
Kappa statistic: 0.442270058708415
Training time: 185.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 90.7
Incorrectly Classified Instances: 21.2
Correctly Classified Instances: 78.8
Weighted Precision: 0.7800242865816637
Weighted AreaUnderROC: 0.8056380952380953
Root mean squared error: 0.39933587285403643
Relative absolute error: 59.498621533414244
Root relative squared error: 79.8671745708073
Weighted TruePositiveRate: 0.788
Weighted MatthewsCorrelation: 0.4715008911814582
Weighted FMeasure: 0.7811651583710407
Iteration time: 146.0
Weighted AreaUnderPRC: 0.8195827750552814
Mean absolute error: 0.2974931076670712
Coverage of cases: 99.2
Instances selection time: 2.0
Test time: 12.0
Accumulative iteration time: 1700.0
Weighted Recall: 0.788
Weighted FalsePositiveRate: 0.3460952380952381
Kappa statistic: 0.46680080482897385
Training time: 144.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 90.3
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.7643551908952556
Weighted AreaUnderROC: 0.802895238095238
Root mean squared error: 0.40096027054180616
Relative absolute error: 59.27992746509474
Root relative squared error: 80.19205410836123
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.43350563936318154
Weighted FMeasure: 0.765769760830761
Iteration time: 229.0
Weighted AreaUnderPRC: 0.8161260909946523
Mean absolute error: 0.2963996373254737
Coverage of cases: 99.2
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1929.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.37114285714285716
Kappa statistic: 0.4281376518218624
Training time: 228.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 91.4
Incorrectly Classified Instances: 22.2
Correctly Classified Instances: 77.8
Weighted Precision: 0.7690595084934255
Weighted AreaUnderROC: 0.7996952380952381
Root mean squared error: 0.40221014365273433
Relative absolute error: 60.281027645868434
Root relative squared error: 80.44202873054687
Weighted TruePositiveRate: 0.778
Weighted MatthewsCorrelation: 0.4453069768141243
Weighted FMeasure: 0.7705374090777944
Iteration time: 185.0
Weighted AreaUnderPRC: 0.8117908621432025
Mean absolute error: 0.3014051382293422
Coverage of cases: 99.0
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 2114.0
Weighted Recall: 0.778
Weighted FalsePositiveRate: 0.36180952380952386
Kappa statistic: 0.44052419354838723
Training time: 180.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 93.1
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.7604068994090583
Weighted AreaUnderROC: 0.7924571428571429
Root mean squared error: 0.4043637770169494
Relative absolute error: 61.963584820727306
Root relative squared error: 80.87275540338989
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.4249267948089012
Weighted FMeasure: 0.7622684868823997
Iteration time: 172.0
Weighted AreaUnderPRC: 0.8065618922616224
Mean absolute error: 0.3098179241036365
Coverage of cases: 99.2
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 2286.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.3728571428571429
Kappa statistic: 0.42036290322580655
Training time: 163.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 93.3
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.760852688102478
Weighted AreaUnderROC: 0.7918857142857143
Root mean squared error: 0.40391848693348487
Relative absolute error: 62.48621907150491
Root relative squared error: 80.78369738669697
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.426627864867195
Weighted FMeasure: 0.7628973502426047
Iteration time: 206.0
Weighted AreaUnderPRC: 0.8033164264203437
Mean absolute error: 0.31243109535752456
Coverage of cases: 99.0
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 2492.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.369047619047619
Kappa statistic: 0.4226907630522089
Training time: 201.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 93.1
Incorrectly Classified Instances: 22.8
Correctly Classified Instances: 77.2
Weighted Precision: 0.7627808136004858
Weighted AreaUnderROC: 0.7924380952380953
Root mean squared error: 0.40397033360147516
Relative absolute error: 62.154075046917775
Root relative squared error: 80.79406672029504
Weighted TruePositiveRate: 0.772
Weighted MatthewsCorrelation: 0.4308542626313325
Weighted FMeasure: 0.7646493212669683
Iteration time: 205.0
Weighted AreaUnderPRC: 0.8036282612130661
Mean absolute error: 0.3107703752345889
Coverage of cases: 99.0
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 2697.0
Weighted Recall: 0.772
Weighted FalsePositiveRate: 0.3681904761904761
Kappa statistic: 0.4265593561368209
Training time: 198.0
		
Time end:Mon Nov 27 15.59.18 EET 2017