Mon Nov 27 15.59.08 EET 2017
Dataset: german_credit
Test set size: 500
Initial Labelled set size: 100
Initial Unlabelled set size: 400
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.59.08 EET 2017
		
Iteration: 1
Labeled set size: 100
Unlabelled set size: 400
	
Mean region size: 71.5
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.6969919295671314
Weighted AreaUnderROC: 0.7172190476190476
Root mean squared error: 0.47076070823065846
Relative absolute error: 58.604889645135636
Root relative squared error: 94.1521416461317
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.2658522658241319
Weighted FMeasure: 0.6982392228293867
Iteration time: 570.0
Weighted AreaUnderPRC: 0.7486313104849143
Mean absolute error: 0.2930244482256782
Coverage of cases: 90.4
Instances selection time: 125.0
Test time: 215.0
Accumulative iteration time: 570.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.4933333333333333
Kappa statistic: 0.253731343283582
Training time: 445.0
		
Iteration: 2
Labeled set size: 120
Unlabelled set size: 380
	
Mean region size: 68.5
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.659926035075554
Weighted AreaUnderROC: 0.7100571428571428
Root mean squared error: 0.4832610738224052
Relative absolute error: 59.500003792491576
Root relative squared error: 96.65221476448103
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.17711699131952155
Weighted FMeasure: 0.663369350515637
Iteration time: 190.0
Weighted AreaUnderPRC: 0.7399462927799422
Mean absolute error: 0.29750001896245787
Coverage of cases: 87.8
Instances selection time: 55.0
Test time: 73.0
Accumulative iteration time: 760.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.5501904761904761
Kappa statistic: 0.16484716157205215
Training time: 135.0
		
Iteration: 3
Labeled set size: 140
Unlabelled set size: 360
	
Mean region size: 67.6
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.6955535593981585
Weighted AreaUnderROC: 0.728952380952381
Root mean squared error: 0.47461957167023017
Relative absolute error: 57.35369568618884
Root relative squared error: 94.92391433404603
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.24843400603493257
Weighted FMeasure: 0.6877792590797075
Iteration time: 138.0
Weighted AreaUnderPRC: 0.7538734498781576
Mean absolute error: 0.2867684784309442
Coverage of cases: 87.8
Instances selection time: 21.0
Test time: 42.0
Accumulative iteration time: 898.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.5305714285714286
Kappa statistic: 0.22433035714285718
Training time: 117.0
		
Iteration: 4
Labeled set size: 160
Unlabelled set size: 340
	
Mean region size: 66.1
Incorrectly Classified Instances: 26.0
Correctly Classified Instances: 74.0
Weighted Precision: 0.7215732087227413
Weighted AreaUnderROC: 0.7489714285714286
Root mean squared error: 0.46867935080494316
Relative absolute error: 55.18939280093483
Root relative squared error: 93.73587016098864
Weighted TruePositiveRate: 0.74
Weighted MatthewsCorrelation: 0.3033137135403521
Weighted FMeasure: 0.7073577433474607
Iteration time: 121.0
Weighted AreaUnderPRC: 0.7701523997919059
Mean absolute error: 0.27594696400467417
Coverage of cases: 86.0
Instances selection time: 21.0
Test time: 34.0
Accumulative iteration time: 1019.0
Weighted Recall: 0.74
Weighted FalsePositiveRate: 0.5076190476190476
Kappa statistic: 0.27293064876957496
Training time: 100.0
		
Iteration: 5
Labeled set size: 180
Unlabelled set size: 320
	
Mean region size: 67.4
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7022709543208806
Weighted AreaUnderROC: 0.7478476190476191
Root mean squared error: 0.46990441753568557
Relative absolute error: 56.196843174549315
Root relative squared error: 93.98088350713711
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.27095380791264556
Weighted FMeasure: 0.6985725556904723
Iteration time: 169.0
Weighted AreaUnderPRC: 0.7677362245533874
Mean absolute error: 0.2809842158727466
Coverage of cases: 87.4
Instances selection time: 11.0
Test time: 39.0
Accumulative iteration time: 1188.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.506
Kappa statistic: 0.25218340611353696
Training time: 158.0
		
Iteration: 6
Labeled set size: 200
Unlabelled set size: 300
	
Mean region size: 66.8
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7073720498541501
Weighted AreaUnderROC: 0.7464380952380952
Root mean squared error: 0.471253348535167
Relative absolute error: 55.91107758643885
Root relative squared error: 94.2506697070334
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.2807302275691662
Weighted FMeasure: 0.7017884586155066
Iteration time: 119.0
Weighted AreaUnderPRC: 0.7647295491342699
Mean absolute error: 0.27955538793219425
Coverage of cases: 87.6
Instances selection time: 17.0
Test time: 42.0
Accumulative iteration time: 1307.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.5042857142857143
Kappa statistic: 0.2598684210526316
Training time: 102.0
		
Iteration: 7
Labeled set size: 220
Unlabelled set size: 280
	
Mean region size: 70.0
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7050480769230769
Weighted AreaUnderROC: 0.7587619047619049
Root mean squared error: 0.45895023828238696
Relative absolute error: 55.55786140875738
Root relative squared error: 91.7900476564774
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.27783119144576074
Weighted FMeasure: 0.7013590413068219
Iteration time: 177.0
Weighted AreaUnderPRC: 0.7754482875486708
Mean absolute error: 0.2777893070437869
Coverage of cases: 90.6
Instances selection time: 14.0
Test time: 34.0
Accumulative iteration time: 1484.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.5013333333333333
Kappa statistic: 0.25925925925925913
Training time: 163.0
		
Iteration: 8
Labeled set size: 240
Unlabelled set size: 260
	
Mean region size: 72.6
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.6942458587619879
Weighted AreaUnderROC: 0.762057142857143
Root mean squared error: 0.4559697609908042
Relative absolute error: 56.76873110297136
Root relative squared error: 91.19395219816084
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.2590977571821282
Weighted FMeasure: 0.6955428347766525
Iteration time: 231.0
Weighted AreaUnderPRC: 0.7789710427097348
Mean absolute error: 0.2838436555148568
Coverage of cases: 91.6
Instances selection time: 9.0
Test time: 31.0
Accumulative iteration time: 1715.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.498
Kappa statistic: 0.24679487179487164
Training time: 222.0
		
Iteration: 9
Labeled set size: 260
Unlabelled set size: 240
	
Mean region size: 73.2
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7009756097560976
Weighted AreaUnderROC: 0.7616190476190476
Root mean squared error: 0.45436888061314623
Relative absolute error: 56.597530572149644
Root relative squared error: 90.87377612262925
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.2726392706363988
Weighted FMeasure: 0.7003947368421052
Iteration time: 157.0
Weighted AreaUnderPRC: 0.7818912988696783
Mean absolute error: 0.28298765286074823
Coverage of cases: 92.4
Instances selection time: 12.0
Test time: 23.0
Accumulative iteration time: 1872.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.49542857142857144
Kappa statistic: 0.2580645161290322
Training time: 145.0
		
Iteration: 10
Labeled set size: 280
Unlabelled set size: 220
	
Mean region size: 74.0
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7046003898635477
Weighted AreaUnderROC: 0.763104761904762
Root mean squared error: 0.45292904581979404
Relative absolute error: 56.50663992291383
Root relative squared error: 90.58580916395881
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.28368816544899667
Weighted FMeasure: 0.7052250304095148
Iteration time: 198.0
Weighted AreaUnderPRC: 0.7863438484628461
Mean absolute error: 0.28253319961456913
Coverage of cases: 92.8
Instances selection time: 6.0
Test time: 30.0
Accumulative iteration time: 2070.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.48314285714285715
Kappa statistic: 0.27127659574468077
Training time: 192.0
		
Iteration: 11
Labeled set size: 300
Unlabelled set size: 200
	
Mean region size: 74.8
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7117388114453412
Weighted AreaUnderROC: 0.7660190476190477
Root mean squared error: 0.44702827554599345
Relative absolute error: 56.06501562353613
Root relative squared error: 89.40565510919869
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.2993630556338965
Weighted FMeasure: 0.7111718275652702
Iteration time: 328.0
Weighted AreaUnderPRC: 0.7881505329544563
Mean absolute error: 0.28032507811768065
Coverage of cases: 93.4
Instances selection time: 11.0
Test time: 25.0
Accumulative iteration time: 2398.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.4767619047619048
Kappa statistic: 0.28571428571428564
Training time: 317.0
		
Iteration: 12
Labeled set size: 320
Unlabelled set size: 180
	
Mean region size: 76.9
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7151590720169273
Weighted AreaUnderROC: 0.7691999999999999
Root mean squared error: 0.43730409271597925
Relative absolute error: 56.22495318122576
Root relative squared error: 87.46081854319586
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.30994669126891394
Weighted FMeasure: 0.7157909935347249
Iteration time: 230.0
Weighted AreaUnderPRC: 0.7931749517306447
Mean absolute error: 0.2811247659061288
Coverage of cases: 93.6
Instances selection time: 10.0
Test time: 23.0
Accumulative iteration time: 2628.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4644761904761905
Kappa statistic: 0.29852320675105476
Training time: 220.0
		
Iteration: 13
Labeled set size: 340
Unlabelled set size: 160
	
Mean region size: 78.3
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.71775
Weighted AreaUnderROC: 0.7712571428571429
Root mean squared error: 0.4330731606155206
Relative absolute error: 56.31808825566862
Root relative squared error: 86.61463212310412
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.31641594084218894
Weighted FMeasure: 0.7183999999999998
Iteration time: 222.0
Weighted AreaUnderPRC: 0.7937986470070069
Mean absolute error: 0.2815904412783431
Coverage of cases: 95.0
Instances selection time: 7.0
Test time: 30.0
Accumulative iteration time: 2850.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.45980952380952383
Kappa statistic: 0.3052631578947368
Training time: 215.0
		
Iteration: 14
Labeled set size: 360
Unlabelled set size: 140
	
Mean region size: 80.6
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7155984019454577
Weighted AreaUnderROC: 0.7626095238095238
Root mean squared error: 0.42991994345445983
Relative absolute error: 58.37131741623347
Root relative squared error: 85.98398869089196
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3119788225347195
Weighted FMeasure: 0.7167367911531444
Iteration time: 178.0
Weighted AreaUnderPRC: 0.7832618541924466
Mean absolute error: 0.29185658708116735
Coverage of cases: 96.0
Instances selection time: 9.0
Test time: 32.0
Accumulative iteration time: 3028.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.46066666666666667
Kappa statistic: 0.30147058823529416
Training time: 169.0
		
Iteration: 15
Labeled set size: 380
Unlabelled set size: 120
	
Mean region size: 81.7
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7119349005424955
Weighted AreaUnderROC: 0.7674095238095238
Root mean squared error: 0.42661954644906713
Relative absolute error: 58.55892007931869
Root relative squared error: 85.32390928981343
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.3053810016799208
Weighted FMeasure: 0.7143308330043426
Iteration time: 205.0
Weighted AreaUnderPRC: 0.7869841129491563
Mean absolute error: 0.29279460039659344
Coverage of cases: 96.4
Instances selection time: 6.0
Test time: 23.0
Accumulative iteration time: 3233.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.4585714285714286
Kappa statistic: 0.29687499999999994
Training time: 199.0
		
Iteration: 16
Labeled set size: 400
Unlabelled set size: 100
	
Mean region size: 81.5
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7221825946174241
Weighted AreaUnderROC: 0.7643809523809524
Root mean squared error: 0.42851935646086403
Relative absolute error: 58.81429076091462
Root relative squared error: 85.7038712921728
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.33085146536941046
Weighted FMeasure: 0.7245108613529667
Iteration time: 166.0
Weighted AreaUnderPRC: 0.7838910870612198
Mean absolute error: 0.2940714538045731
Coverage of cases: 96.0
Instances selection time: 8.0
Test time: 39.0
Accumulative iteration time: 3399.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.4399047619047619
Kappa statistic: 0.3233471074380165
Training time: 158.0
		
Iteration: 17
Labeled set size: 420
Unlabelled set size: 80
	
Mean region size: 83.0
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7258227848101265
Weighted AreaUnderROC: 0.7631428571428571
Root mean squared error: 0.42669612261145173
Relative absolute error: 59.16262047003977
Root relative squared error: 85.33922452229035
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.33752637027780713
Weighted FMeasure: 0.727027240426372
Iteration time: 275.0
Weighted AreaUnderPRC: 0.7839187803887712
Mean absolute error: 0.29581310235019886
Coverage of cases: 96.2
Instances selection time: 5.0
Test time: 33.0
Accumulative iteration time: 3674.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.442
Kappa statistic: 0.328125
Training time: 270.0
		
Iteration: 18
Labeled set size: 440
Unlabelled set size: 60
	
Mean region size: 84.8
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7160671052309799
Weighted AreaUnderROC: 0.756
Root mean squared error: 0.429035088180299
Relative absolute error: 60.85646783245288
Root relative squared error: 85.8070176360598
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.3140287972949645
Weighted FMeasure: 0.717660629342138
Iteration time: 301.0
Weighted AreaUnderPRC: 0.7786058765946877
Mean absolute error: 0.3042823391622644
Coverage of cases: 97.2
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 3975.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.4568571428571429
Kappa statistic: 0.30439330543933046
Training time: 300.0
		
Iteration: 19
Labeled set size: 460
Unlabelled set size: 40
	
Mean region size: 86.5
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7362259431668171
Weighted AreaUnderROC: 0.7637904761904762
Root mean squared error: 0.42318643766313463
Relative absolute error: 61.10888523016863
Root relative squared error: 84.63728753262693
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.36440433719335463
Weighted FMeasure: 0.7379185914485247
Iteration time: 205.0
Weighted AreaUnderPRC: 0.7861732837001189
Mean absolute error: 0.30554442615084315
Coverage of cases: 98.2
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 4180.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.4195238095238095
Kappa statistic: 0.3569958847736626
Training time: 202.0
		
Iteration: 20
Labeled set size: 480
Unlabelled set size: 20
	
Mean region size: 87.5
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7322105600146349
Weighted AreaUnderROC: 0.7641142857142857
Root mean squared error: 0.42266799452683573
Relative absolute error: 61.32708048353413
Root relative squared error: 84.53359890536714
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.35583611777500773
Weighted FMeasure: 0.7345089278804733
Iteration time: 228.0
Weighted AreaUnderPRC: 0.7874447147715067
Mean absolute error: 0.3066354024176707
Coverage of cases: 98.4
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 4408.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.4212380952380952
Kappa statistic: 0.3493852459016393
Training time: 227.0
		
Time end:Mon Nov 27 15.59.14 EET 2017