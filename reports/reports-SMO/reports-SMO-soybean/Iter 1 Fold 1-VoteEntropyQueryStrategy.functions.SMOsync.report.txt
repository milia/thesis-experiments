Fri Dec 01 00.28.12 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Fri Dec 01 00.28.12 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.75684826100342
Incorrectly Classified Instances: 45.6140350877193
Correctly Classified Instances: 54.3859649122807
Weighted Precision: 0.6566666608095673
Weighted AreaUnderROC: 0.8939957100796292
Root mean squared error: 0.20287728781525424
Relative absolute error: 50.85014992407446
Root relative squared error: 90.85540710983703
Weighted TruePositiveRate: 0.543859649122807
Weighted MatthewsCorrelation: 0.5187940068144848
Weighted FMeasure: 0.5114913086590301
Iteration time: 2925.0
Weighted AreaUnderPRC: 0.6995765871164714
Mean absolute error: 0.050709290783010987
Coverage of cases: 64.91228070175438
Instances selection time: 1613.0
Test time: 2113.0
Accumulative iteration time: 2925.0
Weighted Recall: 0.543859649122807
Weighted FalsePositiveRate: 0.06857346505341966
Kappa statistic: 0.4829380808854172
Training time: 1312.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 12.773160972607002
Incorrectly Classified Instances: 34.7953216374269
Correctly Classified Instances: 65.2046783625731
Weighted Precision: 0.6308396682548573
Weighted AreaUnderROC: 0.9229912861517168
Root mean squared error: 0.17463833344606672
Relative absolute error: 42.16760354991938
Root relative squared error: 78.20903489539278
Weighted TruePositiveRate: 0.652046783625731
Weighted MatthewsCorrelation: 0.5902043609485631
Weighted FMeasure: 0.6009948721740439
Iteration time: 2430.0
Weighted AreaUnderPRC: 0.7601581266386421
Mean absolute error: 0.04205079578385349
Coverage of cases: 76.90058479532163
Instances selection time: 1259.0
Test time: 1752.0
Accumulative iteration time: 5355.0
Weighted Recall: 0.652046783625731
Weighted FalsePositiveRate: 0.05198674490126988
Kappa statistic: 0.6076431401660127
Training time: 1171.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 12.911665127731649
Incorrectly Classified Instances: 31.28654970760234
Correctly Classified Instances: 68.71345029239767
Weighted Precision: 0.6356954978154795
Weighted AreaUnderROC: 0.9384875869809297
Root mean squared error: 0.15714945639663938
Relative absolute error: 36.56509657418925
Root relative squared error: 70.37691597596717
Weighted TruePositiveRate: 0.6871345029239766
Weighted MatthewsCorrelation: 0.619335211516989
Weighted FMeasure: 0.6321970234626602
Iteration time: 2547.0
Weighted AreaUnderPRC: 0.8136539241479284
Mean absolute error: 0.03646380821802838
Coverage of cases: 81.87134502923976
Instances selection time: 1240.0
Test time: 1832.0
Accumulative iteration time: 7902.0
Weighted Recall: 0.6871345029239766
Weighted FalsePositiveRate: 0.041046133671290216
Kappa statistic: 0.6499354283254413
Training time: 1307.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 12.60387811634351
Incorrectly Classified Instances: 29.82456140350877
Correctly Classified Instances: 70.17543859649123
Weighted Precision: 0.6664138858011787
Weighted AreaUnderROC: 0.9400171652598784
Root mean squared error: 0.15986125255040948
Relative absolute error: 37.46858926970278
Root relative squared error: 71.59135129400056
Weighted TruePositiveRate: 0.7017543859649122
Weighted MatthewsCorrelation: 0.6445324194844313
Weighted FMeasure: 0.6618780783823657
Iteration time: 2337.0
Weighted AreaUnderPRC: 0.8019244794579851
Mean absolute error: 0.037364798163692856
Coverage of cases: 80.70175438596492
Instances selection time: 1012.0
Test time: 1762.0
Accumulative iteration time: 10239.0
Weighted Recall: 0.7017543859649122
Weighted FalsePositiveRate: 0.03977434969435904
Kappa statistic: 0.667521277913859
Training time: 1325.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 12.726992920898764
Incorrectly Classified Instances: 29.239766081871345
Correctly Classified Instances: 70.76023391812865
Weighted Precision: 0.6700581300666449
Weighted AreaUnderROC: 0.944785774524061
Root mean squared error: 0.15712245217018941
Relative absolute error: 36.8134289307439
Root relative squared error: 70.36482255725963
Weighted TruePositiveRate: 0.7076023391812866
Weighted MatthewsCorrelation: 0.6482695508741285
Weighted FMeasure: 0.6648996775818313
Iteration time: 2161.0
Weighted AreaUnderPRC: 0.8021706006652347
Mean absolute error: 0.036711452673318344
Coverage of cases: 82.45614035087719
Instances selection time: 842.0
Test time: 1737.0
Accumulative iteration time: 12400.0
Weighted Recall: 0.7076023391812866
Weighted FalsePositiveRate: 0.038870030932928734
Kappa statistic: 0.6742608960682719
Training time: 1319.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 11.480455524776895
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.7073701902723953
Weighted AreaUnderROC: 0.9635844764964528
Root mean squared error: 0.146728161499263
Relative absolute error: 33.13054257919013
Root relative squared error: 65.70990272520342
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6818110585861972
Weighted FMeasure: 0.691167842520499
Iteration time: 2010.0
Weighted AreaUnderPRC: 0.8242905000132181
Mean absolute error: 0.03303876822301537
Coverage of cases: 84.7953216374269
Instances selection time: 743.0
Test time: 1645.0
Accumulative iteration time: 14410.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.031762350593869
Kappa statistic: 0.7016074730902366
Training time: 1267.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 11.449676823638075
Incorrectly Classified Instances: 26.023391812865498
Correctly Classified Instances: 73.9766081871345
Weighted Precision: 0.7250674946632093
Weighted AreaUnderROC: 0.9634052105913868
Root mean squared error: 0.14649529808504375
Relative absolute error: 32.557141143717004
Root relative squared error: 65.60561850232313
Weighted TruePositiveRate: 0.7397660818713451
Weighted MatthewsCorrelation: 0.692423606303524
Weighted FMeasure: 0.6983700407080314
Iteration time: 1939.0
Weighted AreaUnderPRC: 0.8244951656002938
Mean absolute error: 0.0324669551571696
Coverage of cases: 83.04093567251462
Instances selection time: 670.0
Test time: 1731.0
Accumulative iteration time: 16349.0
Weighted Recall: 0.7397660818713451
Weighted FalsePositiveRate: 0.03086973256338445
Kappa statistic: 0.7113239757207891
Training time: 1269.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 8.97199138196368
Incorrectly Classified Instances: 22.22222222222222
Correctly Classified Instances: 77.77777777777777
Weighted Precision: 0.7391369680584138
Weighted AreaUnderROC: 0.9738923466258049
Root mean squared error: 0.13593072753319219
Relative absolute error: 26.844822363612966
Root relative squared error: 60.87444149988245
Weighted TruePositiveRate: 0.7777777777777778
Weighted MatthewsCorrelation: 0.7250989272623735
Weighted FMeasure: 0.7329300913959457
Iteration time: 1986.0
Weighted AreaUnderPRC: 0.8476251634224679
Mean absolute error: 0.02677045997479433
Coverage of cases: 85.67251461988305
Instances selection time: 722.0
Test time: 1880.0
Accumulative iteration time: 18335.0
Weighted Recall: 0.7777777777777778
Weighted FalsePositiveRate: 0.027615999513741493
Kappa statistic: 0.7535509074014375
Training time: 1264.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 8.633425669436747
Incorrectly Classified Instances: 22.514619883040936
Correctly Classified Instances: 77.48538011695906
Weighted Precision: 0.7439868723018772
Weighted AreaUnderROC: 0.972470430210903
Root mean squared error: 0.13400268810528518
Relative absolute error: 25.918181249530907
Root relative squared error: 60.010999322432674
Weighted TruePositiveRate: 0.7748538011695907
Weighted MatthewsCorrelation: 0.725364056517602
Weighted FMeasure: 0.7299839723722612
Iteration time: 2002.0
Weighted AreaUnderPRC: 0.8509204053352596
Mean absolute error: 0.025846385733604458
Coverage of cases: 85.67251461988305
Instances selection time: 568.0
Test time: 1935.0
Accumulative iteration time: 20337.0
Weighted Recall: 0.7748538011695907
Weighted FalsePositiveRate: 0.02578383854541115
Kappa statistic: 0.7506887443551366
Training time: 1434.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.87934749153586
Incorrectly Classified Instances: 21.637426900584796
Correctly Classified Instances: 78.3625730994152
Weighted Precision: 0.7533133137518959
Weighted AreaUnderROC: 0.9755626289870619
Root mean squared error: 0.13760619134378121
Relative absolute error: 25.752339679342665
Root relative squared error: 61.624771653879215
Weighted TruePositiveRate: 0.783625730994152
Weighted MatthewsCorrelation: 0.7350287615026211
Weighted FMeasure: 0.7393548381090908
Iteration time: 1838.0
Weighted AreaUnderPRC: 0.8613409597416563
Mean absolute error: 0.025681003558347484
Coverage of cases: 83.62573099415205
Instances selection time: 453.0
Test time: 2082.0
Accumulative iteration time: 22175.0
Weighted Recall: 0.783625730994152
Weighted FalsePositiveRate: 0.024383444514021975
Kappa statistic: 0.7605993529712242
Training time: 1385.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.756232686980612
Incorrectly Classified Instances: 21.637426900584796
Correctly Classified Instances: 78.3625730994152
Weighted Precision: 0.7557372614357879
Weighted AreaUnderROC: 0.9751587676668364
Root mean squared error: 0.1374100500041336
Relative absolute error: 25.730562333163785
Root relative squared error: 61.536932835366514
Weighted TruePositiveRate: 0.783625730994152
Weighted MatthewsCorrelation: 0.7360809224550486
Weighted FMeasure: 0.740133490746567
Iteration time: 1636.0
Weighted AreaUnderPRC: 0.8662096418012153
Mean absolute error: 0.02565928653722727
Coverage of cases: 83.62573099415205
Instances selection time: 306.0
Test time: 2707.0
Accumulative iteration time: 23811.0
Weighted Recall: 0.783625730994152
Weighted FalsePositiveRate: 0.02440148073074516
Kappa statistic: 0.760594823671863
Training time: 1330.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.58694983071714
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.799516900561029
Weighted AreaUnderROC: 0.9761246499715927
Root mean squared error: 0.1264574306888121
Relative absolute error: 22.29983798606419
Root relative squared error: 56.63197428860809
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.7772544913366818
Weighted FMeasure: 0.7823100331253908
Iteration time: 1391.0
Weighted AreaUnderPRC: 0.8809988111804257
Mean absolute error: 0.022238065581670855
Coverage of cases: 87.13450292397661
Instances selection time: 197.0
Test time: 2267.0
Accumulative iteration time: 25202.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.022416127035261465
Kappa statistic: 0.7963900963900964
Training time: 1194.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.663896583564183
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8101972435468295
Weighted AreaUnderROC: 0.9796996137746155
Root mean squared error: 0.11323325836554564
Relative absolute error: 18.908561909877932
Root relative squared error: 50.70973640254604
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8119463124923414
Weighted FMeasure: 0.8230224918747396
Iteration time: 1319.0
Weighted AreaUnderPRC: 0.8843217181579566
Mean absolute error: 0.018856183622039103
Coverage of cases: 89.47368421052632
Instances selection time: 113.0
Test time: 2488.0
Accumulative iteration time: 26521.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.018173117957122394
Kappa statistic: 0.8324413959448255
Training time: 1206.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 6.586642043705778
Incorrectly Classified Instances: 11.695906432748538
Correctly Classified Instances: 88.30409356725146
Weighted Precision: 0.8626110983744771
Weighted AreaUnderROC: 0.9841382589282082
Root mean squared error: 0.10165482998235567
Relative absolute error: 14.648886507383413
Root relative squared error: 45.52451909322967
Weighted TruePositiveRate: 0.8830409356725146
Weighted MatthewsCorrelation: 0.8586886889437451
Weighted FMeasure: 0.8675835869769472
Iteration time: 1470.0
Weighted AreaUnderPRC: 0.917703658444601
Mean absolute error: 0.0146083078743991
Coverage of cases: 91.81286549707602
Instances selection time: 56.0
Test time: 2707.0
Accumulative iteration time: 27991.0
Weighted Recall: 0.8830409356725146
Weighted FalsePositiveRate: 0.013733810228340982
Kappa statistic: 0.8715348208248817
Training time: 1414.0
		
Time end:Fri Dec 01 00.29.09 EET 2017