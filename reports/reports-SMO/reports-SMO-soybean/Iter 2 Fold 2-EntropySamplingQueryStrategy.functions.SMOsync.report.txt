Thu Nov 30 23.30.11 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Thu Nov 30 23.30.11 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 6.698564593301451
Incorrectly Classified Instances: 32.551319648093845
Correctly Classified Instances: 67.44868035190616
Weighted Precision: 0.7742953786134236
Weighted AreaUnderROC: 0.9468352854049364
Root mean squared error: 0.1764464524276261
Relative absolute error: 35.98803471433007
Root relative squared error: 79.01877258432617
Weighted TruePositiveRate: 0.6744868035190615
Weighted MatthewsCorrelation: 0.6645748113859288
Weighted FMeasure: 0.6753085175565512
Iteration time: 4009.0
Weighted AreaUnderPRC: 0.7968841719615873
Mean absolute error: 0.03588834486747629
Coverage of cases: 72.14076246334311
Instances selection time: 2268.0
Test time: 3184.0
Accumulative iteration time: 4009.0
Weighted Recall: 0.6744868035190615
Weighted FalsePositiveRate: 0.04920981520633155
Kappa statistic: 0.63671177656205
Training time: 1741.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 8.921129803982126
Incorrectly Classified Instances: 26.392961876832846
Correctly Classified Instances: 73.60703812316716
Weighted Precision: 0.7832229942943862
Weighted AreaUnderROC: 0.9522269163954127
Root mean squared error: 0.14986444058166412
Relative absolute error: 30.254349655285772
Root relative squared error: 67.11443605621436
Weighted TruePositiveRate: 0.7360703812316716
Weighted MatthewsCorrelation: 0.7112780371177821
Weighted FMeasure: 0.7202782766762648
Iteration time: 3472.0
Weighted AreaUnderPRC: 0.8277826983756127
Mean absolute error: 0.030170542592529014
Coverage of cases: 87.09677419354838
Instances selection time: 1671.0
Test time: 2265.0
Accumulative iteration time: 7481.0
Weighted Recall: 0.7360703812316716
Weighted FalsePositiveRate: 0.038294017429141534
Kappa statistic: 0.7064450098521226
Training time: 1801.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 8.0876678499769
Incorrectly Classified Instances: 20.234604105571847
Correctly Classified Instances: 79.76539589442815
Weighted Precision: 0.8555504115505425
Weighted AreaUnderROC: 0.9619208362337509
Root mean squared error: 0.13480298230014148
Relative absolute error: 24.208063272604008
Root relative squared error: 60.36939850877985
Weighted TruePositiveRate: 0.7976539589442815
Weighted MatthewsCorrelation: 0.7895174977068545
Weighted FMeasure: 0.7928275270610987
Iteration time: 3499.0
Weighted AreaUnderPRC: 0.8664648559995136
Mean absolute error: 0.024141004925588697
Coverage of cases: 87.68328445747801
Instances selection time: 1780.0
Test time: 2551.0
Accumulative iteration time: 10980.0
Weighted Recall: 0.7976539589442815
Weighted FalsePositiveRate: 0.02827344661866192
Kappa statistic: 0.775963360406768
Training time: 1719.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 6.77573699645009
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.890445563938505
Weighted AreaUnderROC: 0.971592476576063
Root mean squared error: 0.12053424847843519
Relative absolute error: 19.0764957233959
Root relative squared error: 53.97937016073946
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.837294509148361
Weighted FMeasure: 0.8395928680296953
Iteration time: 3260.0
Weighted AreaUnderPRC: 0.8871960439289905
Mean absolute error: 0.01902365224493791
Coverage of cases: 89.73607038123167
Instances selection time: 1618.0
Test time: 2473.0
Accumulative iteration time: 14240.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.022070210162227103
Kappa statistic: 0.828498495933802
Training time: 1642.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 7.2233369347121785
Incorrectly Classified Instances: 14.07624633431085
Correctly Classified Instances: 85.92375366568915
Weighted Precision: 0.8862436022941541
Weighted AreaUnderROC: 0.9743369625166173
Root mean squared error: 0.1100557596529568
Relative absolute error: 17.01066356071423
Root relative squared error: 49.28674350752009
Weighted TruePositiveRate: 0.8592375366568915
Weighted MatthewsCorrelation: 0.8473957253900656
Weighted FMeasure: 0.8570297372039647
Iteration time: 3266.0
Weighted AreaUnderPRC: 0.8909446567748015
Mean absolute error: 0.01696354260902265
Coverage of cases: 92.08211143695014
Instances selection time: 1469.0
Test time: 2443.0
Accumulative iteration time: 17506.0
Weighted Recall: 0.8592375366568915
Weighted FalsePositiveRate: 0.02069676738362184
Kappa statistic: 0.8450792208529728
Training time: 1797.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 6.328137058188008
Incorrectly Classified Instances: 8.211143695014663
Correctly Classified Instances: 91.78885630498533
Weighted Precision: 0.9175081958492096
Weighted AreaUnderROC: 0.9858366344403652
Root mean squared error: 0.08742578832972218
Relative absolute error: 11.581016973474384
Root relative squared error: 39.152266259733146
Weighted TruePositiveRate: 0.9178885630498533
Weighted MatthewsCorrelation: 0.9061640780125669
Weighted FMeasure: 0.9168807301560209
Iteration time: 3184.0
Weighted AreaUnderPRC: 0.9264571396294833
Mean absolute error: 0.011548936594046579
Coverage of cases: 94.72140762463343
Instances selection time: 1475.0
Test time: 2760.0
Accumulative iteration time: 20690.0
Weighted Recall: 0.9178885630498533
Weighted FalsePositiveRate: 0.011814582599304742
Kappa statistic: 0.9099177296399729
Training time: 1709.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 6.019447445593464
Incorrectly Classified Instances: 7.038123167155425
Correctly Classified Instances: 92.96187683284458
Weighted Precision: 0.928474868954806
Weighted AreaUnderROC: 0.9906232043615818
Root mean squared error: 0.08236124834751034
Relative absolute error: 9.73486955095579
Root relative squared error: 36.884191568558634
Weighted TruePositiveRate: 0.9296187683284457
Weighted MatthewsCorrelation: 0.9190849738834869
Weighted FMeasure: 0.9278951217944134
Iteration time: 3187.0
Weighted AreaUnderPRC: 0.9551475463820651
Mean absolute error: 0.009707903153307797
Coverage of cases: 94.72140762463343
Instances selection time: 1369.0
Test time: 2906.0
Accumulative iteration time: 23877.0
Weighted Recall: 0.9296187683284457
Weighted FalsePositiveRate: 0.010054101505854814
Kappa statistic: 0.9228004641027817
Training time: 1818.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 6.019447445593465
Incorrectly Classified Instances: 8.211143695014663
Correctly Classified Instances: 91.78885630498533
Weighted Precision: 0.917880320481366
Weighted AreaUnderROC: 0.9910471933239976
Root mean squared error: 0.08485393948442897
Relative absolute error: 10.169318912044284
Root relative squared error: 38.000504145893856
Weighted TruePositiveRate: 0.9178885630498533
Weighted MatthewsCorrelation: 0.9069582812430206
Weighted FMeasure: 0.9176295275160323
Iteration time: 2921.0
Weighted AreaUnderPRC: 0.9340182841210725
Mean absolute error: 0.010141149053562257
Coverage of cases: 94.72140762463343
Instances selection time: 1188.0
Test time: 2740.0
Accumulative iteration time: 26798.0
Weighted Recall: 0.9178885630498533
Weighted FalsePositiveRate: 0.011100121915055536
Kappa statistic: 0.9099763343736151
Training time: 1733.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 5.571847507331386
Incorrectly Classified Instances: 6.744868035190616
Correctly Classified Instances: 93.25513196480938
Weighted Precision: 0.9329642238208118
Weighted AreaUnderROC: 0.9916623507051474
Root mean squared error: 0.07933317016287356
Relative absolute error: 8.519289439442236
Root relative squared error: 35.52811431028949
Weighted TruePositiveRate: 0.9325513196480938
Weighted MatthewsCorrelation: 0.9238694201657831
Weighted FMeasure: 0.9323356985083036
Iteration time: 2906.0
Weighted AreaUnderPRC: 0.9459154234795027
Mean absolute error: 0.008495690299720865
Coverage of cases: 94.72140762463343
Instances selection time: 1105.0
Test time: 3122.0
Accumulative iteration time: 29704.0
Weighted Recall: 0.9325513196480938
Weighted FalsePositiveRate: 0.008870986621404115
Kappa statistic: 0.926075686884396
Training time: 1801.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 5.587281987961113
Incorrectly Classified Instances: 7.038123167155425
Correctly Classified Instances: 92.96187683284458
Weighted Precision: 0.9291365092240178
Weighted AreaUnderROC: 0.9908520835080428
Root mean squared error: 0.08174589895787428
Relative absolute error: 8.995901879835047
Root relative squared error: 36.60861700862522
Weighted TruePositiveRate: 0.9296187683284457
Weighted MatthewsCorrelation: 0.9197697348640821
Weighted FMeasure: 0.9290704064693652
Iteration time: 2833.0
Weighted AreaUnderPRC: 0.9346884035226461
Mean absolute error: 0.00897098248404611
Coverage of cases: 94.42815249266862
Instances selection time: 1029.0
Test time: 3165.0
Accumulative iteration time: 32537.0
Weighted Recall: 0.9296187683284457
Weighted FalsePositiveRate: 0.009685419033634015
Kappa statistic: 0.9228521332554062
Training time: 1804.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 5.679888871739475
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9361295553441416
Weighted AreaUnderROC: 0.9916186839484945
Root mean squared error: 0.07853879013195013
Relative absolute error: 8.969031839980097
Root relative squared error: 35.17236368937624
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.9268241729113901
Weighted FMeasure: 0.9347318943486783
Iteration time: 2685.0
Weighted AreaUnderPRC: 0.946976561863306
Mean absolute error: 0.008944186876434525
Coverage of cases: 95.30791788856305
Instances selection time: 831.0
Test time: 3095.0
Accumulative iteration time: 35222.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.008839573436558236
Kappa statistic: 0.9292777887760778
Training time: 1854.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 6.142923290631283
Incorrectly Classified Instances: 5.865102639296188
Correctly Classified Instances: 94.13489736070382
Weighted Precision: 0.941136264372054
Weighted AreaUnderROC: 0.9959263716722847
Root mean squared error: 0.0687674680856372
Relative absolute error: 8.78221228777234
Root relative squared error: 30.79643057197612
Weighted TruePositiveRate: 0.9413489736070382
Weighted MatthewsCorrelation: 0.9337198730960332
Weighted FMeasure: 0.9412136305353811
Iteration time: 2325.0
Weighted AreaUnderPRC: 0.9707875728322393
Mean absolute error: 0.008757884829911551
Coverage of cases: 98.53372434017595
Instances selection time: 498.0
Test time: 2816.0
Accumulative iteration time: 37547.0
Weighted Recall: 0.9413489736070382
Weighted FalsePositiveRate: 0.007521976196395877
Kappa statistic: 0.9357313154348502
Training time: 1827.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 6.173792251890738
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9366712502812028
Weighted AreaUnderROC: 0.9956754128482197
Root mean squared error: 0.07020127403293532
Relative absolute error: 8.912194898901923
Root relative squared error: 31.438538047194843
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.9272821583233697
Weighted FMeasure: 0.9355097846658817
Iteration time: 2163.0
Weighted AreaUnderPRC: 0.968815166698385
Mean absolute error: 0.008887507378406429
Coverage of cases: 98.53372434017595
Instances selection time: 335.0
Test time: 2814.0
Accumulative iteration time: 39710.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.008447986704145038
Kappa statistic: 0.9293264248704662
Training time: 1828.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 6.173792251890738
Incorrectly Classified Instances: 6.744868035190616
Correctly Classified Instances: 93.25513196480938
Weighted Precision: 0.9353735324983572
Weighted AreaUnderROC: 0.9956463107748872
Root mean squared error: 0.071880254915873
Relative absolute error: 9.248027128171202
Root relative squared error: 32.190443266806454
Weighted TruePositiveRate: 0.9325513196480938
Weighted MatthewsCorrelation: 0.9245440519178237
Weighted FMeasure: 0.9329774847515291
Iteration time: 2022.0
Weighted AreaUnderPRC: 0.9692385965877588
Mean absolute error: 0.009222409324492139
Coverage of cases: 98.82697947214076
Instances selection time: 184.0
Test time: 2791.0
Accumulative iteration time: 41732.0
Weighted Recall: 0.9325513196480938
Weighted FalsePositiveRate: 0.008536583420750721
Kappa statistic: 0.9261633763568409
Training time: 1838.0
		
Time end:Thu Nov 30 23.31.32 EET 2017