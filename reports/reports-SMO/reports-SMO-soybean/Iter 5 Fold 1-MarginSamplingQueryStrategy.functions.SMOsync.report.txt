Fri Dec 01 00.16.07 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 00.16.07 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 8.556478916589747
Incorrectly Classified Instances: 47.36842105263158
Correctly Classified Instances: 52.63157894736842
Weighted Precision: 0.6043207041884566
Weighted AreaUnderROC: 0.8626892410142535
Root mean squared error: 0.20725322105555116
Relative absolute error: 51.20213621654541
Root relative squared error: 92.81510008638494
Weighted TruePositiveRate: 0.5263157894736842
Weighted MatthewsCorrelation: 0.4709986024148113
Weighted FMeasure: 0.47331603850152076
Iteration time: 2871.0
Weighted AreaUnderPRC: 0.6523097501184186
Mean absolute error: 0.05106030204420086
Coverage of cases: 63.74269005847953
Instances selection time: 1806.0
Test time: 2244.0
Accumulative iteration time: 2871.0
Weighted Recall: 0.5263157894736842
Weighted FalsePositiveRate: 0.07207608798146595
Kappa statistic: 0.4606675946927292
Training time: 1065.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 9.418282548476494
Incorrectly Classified Instances: 37.42690058479532
Correctly Classified Instances: 62.57309941520468
Weighted Precision: 0.6268922787679296
Weighted AreaUnderROC: 0.9053445110696052
Root mean squared error: 0.18107552328618265
Relative absolute error: 40.980513016437634
Root relative squared error: 81.09182926762247
Weighted TruePositiveRate: 0.6257309941520468
Weighted MatthewsCorrelation: 0.5586474568144151
Weighted FMeasure: 0.5685310775632487
Iteration time: 2675.0
Weighted AreaUnderPRC: 0.7246081813699756
Mean absolute error: 0.040866993589799665
Coverage of cases: 71.9298245614035
Instances selection time: 1460.0
Test time: 1994.0
Accumulative iteration time: 5546.0
Weighted Recall: 0.6257309941520468
Weighted FalsePositiveRate: 0.05574937993624791
Kappa statistic: 0.5789149672951136
Training time: 1215.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.125577100646378
Incorrectly Classified Instances: 30.4093567251462
Correctly Classified Instances: 69.5906432748538
Weighted Precision: 0.6671999039466622
Weighted AreaUnderROC: 0.9329290098659294
Root mean squared error: 0.16519756874483546
Relative absolute error: 33.303780560559886
Root relative squared error: 73.98113669350221
Weighted TruePositiveRate: 0.695906432748538
Weighted MatthewsCorrelation: 0.6350510187386064
Weighted FMeasure: 0.6488992301231642
Iteration time: 3137.0
Weighted AreaUnderPRC: 0.7618256900406735
Mean absolute error: 0.033211526320780234
Coverage of cases: 78.94736842105263
Instances selection time: 1551.0
Test time: 2226.0
Accumulative iteration time: 8683.0
Weighted Recall: 0.695906432748538
Weighted FalsePositiveRate: 0.04597141727241516
Kappa statistic: 0.6591764965168313
Training time: 1586.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 7.894736842105279
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.8053281820392234
Weighted AreaUnderROC: 0.9712273359094815
Root mean squared error: 0.13377975706149878
Relative absolute error: 23.626892449419785
Root relative squared error: 59.91116315566038
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7747655932920919
Weighted FMeasure: 0.7823607345646039
Iteration time: 3324.0
Weighted AreaUnderPRC: 0.8717494659394257
Mean absolute error: 0.023561443993881437
Coverage of cases: 86.84210526315789
Instances selection time: 1659.0
Test time: 2793.0
Accumulative iteration time: 12007.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.02856192143241879
Kappa statistic: 0.7855840109430807
Training time: 1665.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.4330563250231
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8314648317928927
Weighted AreaUnderROC: 0.9796849914659108
Root mean squared error: 0.11635663681022686
Relative absolute error: 19.272695668145825
Root relative squared error: 52.10849238556183
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8172012599701262
Weighted FMeasure: 0.8287361637813607
Iteration time: 3208.0
Weighted AreaUnderPRC: 0.9214267703735619
Mean absolute error: 0.019219308699536173
Coverage of cases: 90.93567251461988
Instances selection time: 1514.0
Test time: 2558.0
Accumulative iteration time: 15215.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.021862985045297482
Kappa statistic: 0.8320204023802777
Training time: 1694.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 6.124961526623584
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8955995929540163
Weighted AreaUnderROC: 0.9817808365950215
Root mean squared error: 0.10015908357401923
Relative absolute error: 13.360141446494385
Root relative squared error: 44.85467255532526
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8755356207674363
Weighted FMeasure: 0.8886069187803802
Iteration time: 3122.0
Weighted AreaUnderPRC: 0.9034921893286665
Mean absolute error: 0.013323132744426652
Coverage of cases: 92.10526315789474
Instances selection time: 1465.0
Test time: 2750.0
Accumulative iteration time: 18337.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.016616790824359354
Kappa statistic: 0.8778973082162822
Training time: 1657.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 6.094182825484773
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.9069101828456275
Weighted AreaUnderROC: 0.9856404180212571
Root mean squared error: 0.09474691633136759
Relative absolute error: 12.146286651560702
Root relative squared error: 42.43091845513531
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8899242069355627
Weighted FMeasure: 0.9013680822346049
Iteration time: 3126.0
Weighted AreaUnderPRC: 0.9102837360363688
Mean absolute error: 0.012112640428149284
Coverage of cases: 93.5672514619883
Instances selection time: 1329.0
Test time: 2792.0
Accumulative iteration time: 21463.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.012827675523854979
Kappa statistic: 0.8910215557638238
Training time: 1797.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 6.278855032317648
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9209690461741298
Weighted AreaUnderROC: 0.9853363193984451
Root mean squared error: 0.08840881766610839
Relative absolute error: 11.122852269378809
Root relative squared error: 39.59250050931367
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9079914697380773
Weighted FMeasure: 0.9185960545867838
Iteration time: 2903.0
Weighted AreaUnderPRC: 0.9253612632143811
Mean absolute error: 0.011092041044255974
Coverage of cases: 94.15204678362574
Instances selection time: 1164.0
Test time: 2914.0
Accumulative iteration time: 24366.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.011132803697800528
Kappa statistic: 0.9102597743374442
Training time: 1739.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 5.971068020929526
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9172437730418047
Weighted AreaUnderROC: 0.9842441323695288
Root mean squared error: 0.09109366262859764
Relative absolute error: 11.280672402782672
Root relative squared error: 40.7948661596071
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9006598148930038
Weighted FMeasure: 0.9098044815546285
Iteration time: 2732.0
Weighted AreaUnderPRC: 0.9336897377620674
Mean absolute error: 0.011249424002775064
Coverage of cases: 93.85964912280701
Instances selection time: 993.0
Test time: 2867.0
Accumulative iteration time: 27098.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.011064813300070116
Kappa statistic: 0.9006726751484944
Training time: 1739.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 6.309633733456466
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.926094234364911
Weighted AreaUnderROC: 0.9889260803994299
Root mean squared error: 0.0835326511948115
Relative absolute error: 10.938656597828285
Root relative squared error: 37.40878593654963
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9129034071009838
Weighted FMeasure: 0.9215613771115798
Iteration time: 2631.0
Weighted AreaUnderPRC: 0.954361972763853
Mean absolute error: 0.010908355610022761
Coverage of cases: 96.78362573099415
Instances selection time: 866.0
Test time: 2959.0
Accumulative iteration time: 29729.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.009613536404912434
Kappa statistic: 0.9134923460306159
Training time: 1765.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 6.602031394275186
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9278705154491184
Weighted AreaUnderROC: 0.9917139227340559
Root mean squared error: 0.08047884401317383
Relative absolute error: 10.697338964296007
Root relative squared error: 36.04118635105392
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9140933060012424
Weighted FMeasure: 0.9218343173292721
Iteration time: 2377.0
Weighted AreaUnderPRC: 0.9503673009163994
Mean absolute error: 0.010667706446389464
Coverage of cases: 96.78362573099415
Instances selection time: 630.0
Test time: 2691.0
Accumulative iteration time: 32106.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.00886283981697467
Kappa statistic: 0.9135190821821587
Training time: 1747.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 6.694367497691624
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9270929281894192
Weighted AreaUnderROC: 0.9925829802551601
Root mean squared error: 0.0802256317609741
Relative absolute error: 10.860065803432416
Root relative squared error: 35.92778922066763
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9136631244573593
Weighted FMeasure: 0.9217729798286096
Iteration time: 2249.0
Weighted AreaUnderPRC: 0.9501038607636364
Mean absolute error: 0.010829982518658462
Coverage of cases: 97.36842105263158
Instances selection time: 486.0
Test time: 2684.0
Accumulative iteration time: 34355.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.009206419979831938
Kappa statistic: 0.9134915355861384
Training time: 1763.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 6.571252693136379
Incorrectly Classified Instances: 6.7251461988304095
Correctly Classified Instances: 93.27485380116958
Weighted Precision: 0.9360126083530338
Weighted AreaUnderROC: 0.9945862044717995
Root mean squared error: 0.07039846958206769
Relative absolute error: 9.308836695358664
Root relative squared error: 31.52684897686867
Weighted TruePositiveRate: 0.9327485380116959
Weighted MatthewsCorrelation: 0.9252081085162912
Weighted FMeasure: 0.9321305019245522
Iteration time: 2080.0
Weighted AreaUnderPRC: 0.9590883499260398
Mean absolute error: 0.009283050444125066
Coverage of cases: 98.24561403508773
Instances selection time: 335.0
Test time: 2708.0
Accumulative iteration time: 36435.0
Weighted Recall: 0.9327485380116959
Weighted FalsePositiveRate: 0.008481142250540677
Kappa statistic: 0.9262295081967213
Training time: 1745.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 6.648199445983409
Incorrectly Classified Instances: 6.7251461988304095
Correctly Classified Instances: 93.27485380116958
Weighted Precision: 0.9357878824044236
Weighted AreaUnderROC: 0.9943584313498177
Root mean squared error: 0.071557421955416
Relative absolute error: 9.528499559153927
Root relative squared error: 32.04586759563767
Weighted TruePositiveRate: 0.9327485380116959
Weighted MatthewsCorrelation: 0.9248568239548112
Weighted FMeasure: 0.9318682138626737
Iteration time: 1952.0
Weighted AreaUnderPRC: 0.9587923784156779
Mean absolute error: 0.009502104823533086
Coverage of cases: 97.953216374269
Instances selection time: 127.0
Test time: 2672.0
Accumulative iteration time: 38387.0
Weighted Recall: 0.9327485380116959
Weighted FalsePositiveRate: 0.008858847447522982
Kappa statistic: 0.9262032085561497
Training time: 1825.0
		
Time end:Fri Dec 01 00.17.23 EET 2017