Mon Nov 27 16.02.43 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.02.43 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 98.56770833333333
Incorrectly Classified Instances: 21.354166666666668
Correctly Classified Instances: 78.64583333333333
Weighted Precision: 0.7837958172779601
Weighted AreaUnderROC: 0.842089552238806
Root mean squared error: 0.3882230933765526
Relative absolute error: 65.64189160404798
Root relative squared error: 77.64461867531051
Weighted TruePositiveRate: 0.7864583333333334
Weighted MatthewsCorrelation: 0.5113227370093286
Weighted FMeasure: 0.7770616207754332
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8440580956808007
Mean absolute error: 0.32820945802023993
Coverage of cases: 99.47916666666667
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7864583333333334
Weighted FalsePositiveRate: 0.3187568407960199
Kappa statistic: 0.4987902712339235
Training time: 15.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 98.56770833333333
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7884825453571297
Weighted AreaUnderROC: 0.8353432835820894
Root mean squared error: 0.39279406871804023
Relative absolute error: 66.19733696746202
Root relative squared error: 78.55881374360804
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5171044084716158
Weighted FMeasure: 0.7780060888222984
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8305996064856206
Mean absolute error: 0.33098668483731014
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.3242863805970149
Kappa statistic: 0.5002891844997108
Training time: 16.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 98.30729166666667
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7746386054421769
Weighted AreaUnderROC: 0.8314626865671642
Root mean squared error: 0.39228449690883666
Relative absolute error: 64.87047785523524
Root relative squared error: 78.45689938176733
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.48487472907308216
Weighted FMeasure: 0.7631028974089635
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8322512945040043
Mean absolute error: 0.3243523892761762
Coverage of cases: 99.21875
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.34511629353233825
Kappa statistic: 0.46645986816595575
Training time: 12.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 98.046875
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8006367370892019
Weighted AreaUnderROC: 0.8453432835820895
Root mean squared error: 0.3851573478259265
Relative absolute error: 63.261254671657404
Root relative squared error: 77.03146956518529
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5490719672556438
Weighted FMeasure: 0.7940054899324562
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8428661770817089
Mean absolute error: 0.31630627335828704
Coverage of cases: 99.21875
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.2965310945273632
Kappa statistic: 0.537173306267445
Training time: 16.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 97.13541666666667
Incorrectly Classified Instances: 21.614583333333332
Correctly Classified Instances: 78.38541666666667
Weighted Precision: 0.780195140759659
Weighted AreaUnderROC: 0.8436716417910448
Root mean squared error: 0.38553993866049974
Relative absolute error: 62.13848119224979
Root relative squared error: 77.10798773209994
Weighted TruePositiveRate: 0.7838541666666666
Weighted MatthewsCorrelation: 0.5057524947463641
Weighted FMeasure: 0.7753691635729778
Iteration time: 20.0
Weighted AreaUnderPRC: 0.838237315791703
Mean absolute error: 0.310692405961249
Coverage of cases: 98.95833333333333
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 95.0
Weighted Recall: 0.7838541666666666
Weighted FalsePositiveRate: 0.3166899875621891
Kappa statistic: 0.4954726777686316
Training time: 16.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 95.44270833333333
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8006367370892019
Weighted AreaUnderROC: 0.8428059701492537
Root mean squared error: 0.3848105632642173
Relative absolute error: 60.56627407403591
Root relative squared error: 76.96211265284346
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5490719672556438
Weighted FMeasure: 0.7940054899324562
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8441519178066642
Mean absolute error: 0.30283137037017954
Coverage of cases: 99.21875
Instances selection time: 10.0
Test time: 5.0
Accumulative iteration time: 122.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.2965310945273632
Kappa statistic: 0.537173306267445
Training time: 17.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 98.046875
Incorrectly Classified Instances: 19.53125
Correctly Classified Instances: 80.46875
Weighted Precision: 0.8038637471203259
Weighted AreaUnderROC: 0.8459402985074626
Root mean squared error: 0.385884501085141
Relative absolute error: 62.90354220400951
Root relative squared error: 77.1769002170282
Weighted TruePositiveRate: 0.8046875
Weighted MatthewsCorrelation: 0.5552276560168371
Weighted FMeasure: 0.7964068328586901
Iteration time: 23.0
Weighted AreaUnderPRC: 0.843303842055021
Mean absolute error: 0.31451771102004755
Coverage of cases: 99.47916666666667
Instances selection time: 10.0
Test time: 5.0
Accumulative iteration time: 145.0
Weighted Recall: 0.8046875
Weighted FalsePositiveRate: 0.29513526119402983
Kappa statistic: 0.5424213536701621
Training time: 13.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 97.39583333333333
Incorrectly Classified Instances: 19.270833333333332
Correctly Classified Instances: 80.72916666666667
Weighted Precision: 0.8099950987691086
Weighted AreaUnderROC: 0.8461492537313432
Root mean squared error: 0.3838298797489363
Relative absolute error: 61.67654820110666
Root relative squared error: 76.76597594978726
Weighted TruePositiveRate: 0.8072916666666666
Weighted MatthewsCorrelation: 0.5618841477787524
Weighted FMeasure: 0.7968516991585846
Iteration time: 20.0
Weighted AreaUnderPRC: 0.843385647036555
Mean absolute error: 0.3083827410055333
Coverage of cases: 99.21875
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 165.0
Weighted Recall: 0.8072916666666666
Weighted FalsePositiveRate: 0.30412748756218905
Kappa statistic: 0.5426216842647437
Training time: 14.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 98.69791666666667
Incorrectly Classified Instances: 19.270833333333332
Correctly Classified Instances: 80.72916666666667
Weighted Precision: 0.8089271521154316
Weighted AreaUnderROC: 0.845044776119403
Root mean squared error: 0.3873602374056835
Relative absolute error: 64.51744758774306
Root relative squared error: 77.4720474811367
Weighted TruePositiveRate: 0.8072916666666666
Weighted MatthewsCorrelation: 0.5616266322992681
Weighted FMeasure: 0.7975247725795972
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8427109189218452
Mean absolute error: 0.3225872379387153
Coverage of cases: 99.47916666666667
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 188.0
Weighted Recall: 0.8072916666666666
Weighted FalsePositiveRate: 0.30066480099502485
Kappa statistic: 0.5443232841565104
Training time: 18.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 98.56770833333333
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8013073236287521
Weighted AreaUnderROC: 0.8455820895522388
Root mean squared error: 0.3850764670373429
Relative absolute error: 63.51994867026157
Root relative squared error: 77.01529340746858
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5489180773868335
Weighted FMeasure: 0.793374185108938
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8427438619447062
Mean absolute error: 0.31759974335130786
Coverage of cases: 99.47916666666667
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 212.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.29999378109452735
Kappa statistic: 0.5354641538265632
Training time: 18.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 98.4375
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8020833333333334
Weighted AreaUnderROC: 0.8454328358208955
Root mean squared error: 0.38474604373813437
Relative absolute error: 63.27097658950815
Root relative squared error: 76.94920874762687
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5488658591525652
Weighted FMeasure: 0.7927233177091751
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8426999830828068
Mean absolute error: 0.31635488294754077
Coverage of cases: 99.21875
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 236.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.30345646766169154
Kappa statistic: 0.5337423312883436
Training time: 20.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 98.046875
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7980545587453483
Weighted AreaUnderROC: 0.8453432835820895
Root mean squared error: 0.3840246682332032
Relative absolute error: 62.33434391273182
Root relative squared error: 76.80493364664063
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5427374732452808
Weighted FMeasure: 0.7909776817349217
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8427381314449817
Mean absolute error: 0.3116717195636591
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 255.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.30138961442786066
Kappa statistic: 0.5302192564346997
Training time: 18.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 96.875
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7922453703703703
Weighted AreaUnderROC: 0.8449253731343284
Root mean squared error: 0.3823708856525297
Relative absolute error: 60.540050791193735
Root relative squared error: 76.47417713050594
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5302472904737245
Weighted FMeasure: 0.7855485306111533
Iteration time: 22.0
Weighted AreaUnderPRC: 0.8408447554626846
Mean absolute error: 0.30270025395596867
Coverage of cases: 99.21875
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 277.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.30764396766169155
Kappa statistic: 0.5180171591992375
Training time: 17.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 97.13541666666667
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7917062079441161
Weighted AreaUnderROC: 0.8452537313432836
Root mean squared error: 0.3824320990921313
Relative absolute error: 60.63232512704375
Root relative squared error: 76.48641981842626
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5305715309102751
Weighted FMeasure: 0.7861947460513886
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8409898524159294
Mean absolute error: 0.30316162563521876
Coverage of cases: 99.21875
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 308.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.30418128109452736
Kappa statistic: 0.5197872475147218
Training time: 27.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 96.09375
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7917062079441161
Weighted AreaUnderROC: 0.8454029850746269
Root mean squared error: 0.3819204030395154
Relative absolute error: 59.61494989850989
Root relative squared error: 76.38408060790309
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5305715309102751
Weighted FMeasure: 0.7861947460513886
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8411330475794395
Mean absolute error: 0.2980747494925495
Coverage of cases: 99.21875
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 337.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.30418128109452736
Kappa statistic: 0.5197872475147218
Training time: 29.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 96.74479166666667
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7864361819953926
Weighted AreaUnderROC: 0.8451940298507464
Root mean squared error: 0.38231383003248537
Relative absolute error: 59.89138799671516
Root relative squared error: 76.46276600649708
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5177571077021682
Weighted FMeasure: 0.7801193794873852
Iteration time: 35.0
Weighted AreaUnderPRC: 0.8414776245304406
Mean absolute error: 0.2994569399835758
Coverage of cases: 99.21875
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 372.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.3138983208955224
Kappa statistic: 0.505815061963775
Training time: 31.0
		
Time end:Mon Nov 27 16.02.44 EET 2017