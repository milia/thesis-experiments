Mon Nov 27 16.03.08 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 16.03.08 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 93.50260416666667
Incorrectly Classified Instances: 26.328125
Correctly Classified Instances: 73.671875
Weighted Precision: 0.7341406421727202
Weighted AreaUnderROC: 0.7834477611940299
Root mean squared error: 0.42832287851169515
Relative absolute error: 65.72976121178284
Root relative squared error: 85.66457570233902
Weighted TruePositiveRate: 0.7367187500000001
Weighted MatthewsCorrelation: 0.3991910325309779
Weighted FMeasure: 0.7242439827852689
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7869442233962316
Mean absolute error: 0.3286488060589142
Coverage of cases: 98.56770833333334
Instances selection time: 1.6
Test time: 4.8
Accumulative iteration time: 15.0
Weighted Recall: 0.7367187500000001
Weighted FalsePositiveRate: 0.3734650186567164
Kappa statistic: 0.3848212832083781
Training time: 13.4
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 92.98177083333333
Incorrectly Classified Instances: 25.651041666666664
Correctly Classified Instances: 74.34895833333334
Weighted Precision: 0.745465571955649
Weighted AreaUnderROC: 0.7940358208955224
Root mean squared error: 0.42681532945196815
Relative absolute error: 65.24012007650421
Root relative squared error: 85.36306589039363
Weighted TruePositiveRate: 0.7434895833333333
Weighted MatthewsCorrelation: 0.4241510502306839
Weighted FMeasure: 0.7336015345921989
Iteration time: 10.7
Weighted AreaUnderPRC: 0.7967017846308047
Mean absolute error: 0.326200600382521
Coverage of cases: 98.359375
Instances selection time: 1.5
Test time: 5.6
Accumulative iteration time: 25.7
Weighted Recall: 0.7434895833333333
Weighted FalsePositiveRate: 0.3511373445273632
Kappa statistic: 0.40945809083830653
Training time: 9.2
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 91.6796875
Incorrectly Classified Instances: 26.380208333333332
Correctly Classified Instances: 73.61979166666666
Weighted Precision: 0.7481732911455066
Weighted AreaUnderROC: 0.7946268656716419
Root mean squared error: 0.4340058549989683
Relative absolute error: 65.99116023088794
Root relative squared error: 86.80117099979365
Weighted TruePositiveRate: 0.7361979166666667
Weighted MatthewsCorrelation: 0.4175862783783474
Weighted FMeasure: 0.7227886983492599
Iteration time: 11.4
Weighted AreaUnderPRC: 0.7958016670816025
Mean absolute error: 0.32995580115443973
Coverage of cases: 97.91666666666667
Instances selection time: 1.3
Test time: 5.4
Accumulative iteration time: 37.1
Weighted Recall: 0.7361979166666667
Weighted FalsePositiveRate: 0.3560844838308458
Kappa statistic: 0.392203563429826
Training time: 10.1
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 89.97395833333334
Incorrectly Classified Instances: 27.682291666666668
Correctly Classified Instances: 72.31770833333333
Weighted Precision: 0.7498153802295373
Weighted AreaUnderROC: 0.7951164179104477
Root mean squared error: 0.44557716520938345
Relative absolute error: 67.01749424431681
Root relative squared error: 89.11543304187667
Weighted TruePositiveRate: 0.7231770833333334
Weighted MatthewsCorrelation: 0.4051169233313936
Weighted FMeasure: 0.7057320948543782
Iteration time: 12.2
Weighted AreaUnderPRC: 0.7955938583313151
Mean absolute error: 0.3350874712215841
Coverage of cases: 97.50000000000001
Instances selection time: 1.5
Test time: 4.1
Accumulative iteration time: 49.3
Weighted Recall: 0.7231770833333334
Weighted FalsePositiveRate: 0.3630636504975124
Kappa statistic: 0.3661937543546115
Training time: 10.7
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.17968749999999
Incorrectly Classified Instances: 29.739583333333332
Correctly Classified Instances: 70.26041666666667
Weighted Precision: 0.7435765253956724
Weighted AreaUnderROC: 0.7942626865671643
Root mean squared error: 0.4574324611917187
Relative absolute error: 68.80769267488493
Root relative squared error: 91.48649223834374
Weighted TruePositiveRate: 0.7026041666666666
Weighted MatthewsCorrelation: 0.37417309019484546
Weighted FMeasure: 0.6790991827880803
Iteration time: 11.1
Weighted AreaUnderPRC: 0.7944679233339741
Mean absolute error: 0.3440384633744247
Coverage of cases: 97.13541666666667
Instances selection time: 1.2
Test time: 4.5
Accumulative iteration time: 60.4
Weighted Recall: 0.7026041666666666
Weighted FalsePositiveRate: 0.3806698383084578
Kappa statistic: 0.3223493260479754
Training time: 9.9
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 87.60416666666667
Incorrectly Classified Instances: 30.442708333333332
Correctly Classified Instances: 69.55729166666666
Weighted Precision: 0.7518234468619891
Weighted AreaUnderROC: 0.7969283582089554
Root mean squared error: 0.46653571764705404
Relative absolute error: 69.52039082797742
Root relative squared error: 93.30714352941081
Weighted TruePositiveRate: 0.6955729166666667
Weighted MatthewsCorrelation: 0.3760059059981137
Weighted FMeasure: 0.6685669451398368
Iteration time: 11.1
Weighted AreaUnderPRC: 0.7972848358082444
Mean absolute error: 0.34760195413988704
Coverage of cases: 96.58854166666666
Instances selection time: 1.0
Test time: 4.8
Accumulative iteration time: 71.5
Weighted Recall: 0.6955729166666667
Weighted FalsePositiveRate: 0.3820147077114428
Kappa statistic: 0.3122663288358984
Training time: 10.1
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 88.21614583333333
Incorrectly Classified Instances: 29.921875000000007
Correctly Classified Instances: 70.078125
Weighted Precision: 0.7531290923692278
Weighted AreaUnderROC: 0.8024567164179105
Root mean squared error: 0.4618867509401706
Relative absolute error: 68.76581292425746
Root relative squared error: 92.37735018803411
Weighted TruePositiveRate: 0.70078125
Weighted MatthewsCorrelation: 0.3746216542414077
Weighted FMeasure: 0.6706021177835098
Iteration time: 13.1
Weighted AreaUnderPRC: 0.8034643388534187
Mean absolute error: 0.34382906462128726
Coverage of cases: 97.1875
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 84.6
Weighted Recall: 0.70078125
Weighted FalsePositiveRate: 0.39168871268656724
Kappa statistic: 0.30915806888660924
Training time: 12.1
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.421875
Incorrectly Classified Instances: 29.557291666666664
Correctly Classified Instances: 70.44270833333334
Weighted Precision: 0.7428753472148169
Weighted AreaUnderROC: 0.7968447761194031
Root mean squared error: 0.45904187326069507
Relative absolute error: 67.78186587823788
Root relative squared error: 91.80837465213901
Weighted TruePositiveRate: 0.7044270833333333
Weighted MatthewsCorrelation: 0.35507654443498077
Weighted FMeasure: 0.6690920191868346
Iteration time: 12.5
Weighted AreaUnderPRC: 0.7994779649158616
Mean absolute error: 0.3389093293911894
Coverage of cases: 96.71875000000001
Instances selection time: 0.9
Test time: 4.5
Accumulative iteration time: 97.1
Weighted Recall: 0.7044270833333333
Weighted FalsePositiveRate: 0.4101643967661691
Kappa statistic: 0.2955470282501048
Training time: 11.6
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 86.95312499999999
Incorrectly Classified Instances: 29.140625
Correctly Classified Instances: 70.85937499999999
Weighted Precision: 0.7420969328570506
Weighted AreaUnderROC: 0.795668656716418
Root mean squared error: 0.4569078265883209
Relative absolute error: 66.81906443506567
Root relative squared error: 91.38156531766418
Weighted TruePositiveRate: 0.70859375
Weighted MatthewsCorrelation: 0.3454500716654048
Weighted FMeasure: 0.6667509452217664
Iteration time: 12.4
Weighted AreaUnderPRC: 0.7966233960305364
Mean absolute error: 0.33409532217532834
Coverage of cases: 96.45833333333334
Instances selection time: 0.7
Test time: 4.4
Accumulative iteration time: 109.5
Weighted Recall: 0.70859375
Weighted FalsePositiveRate: 0.4273221082089552
Kappa statistic: 0.2867170974893795
Training time: 11.7
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 86.96614583333333
Incorrectly Classified Instances: 29.270833333333332
Correctly Classified Instances: 70.72916666666666
Weighted Precision: 0.7263267974543257
Weighted AreaUnderROC: 0.7951283582089552
Root mean squared error: 0.45757570940194164
Relative absolute error: 66.32105022916444
Root relative squared error: 91.51514188038834
Weighted TruePositiveRate: 0.7072916666666667
Weighted MatthewsCorrelation: 0.3202420343582387
Weighted FMeasure: 0.6613245903275701
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7952587503461447
Mean absolute error: 0.33160525114582223
Coverage of cases: 96.58854166666666
Instances selection time: 0.6
Test time: 4.3
Accumulative iteration time: 121.5
Weighted Recall: 0.7072916666666667
Weighted FalsePositiveRate: 0.4432558457711443
Kappa statistic: 0.27137978917076405
Training time: 11.4
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 87.0703125
Incorrectly Classified Instances: 28.4375
Correctly Classified Instances: 71.5625
Weighted Precision: 0.742945057721164
Weighted AreaUnderROC: 0.7993223880597016
Root mean squared error: 0.45126578035042886
Relative absolute error: 65.18577887664453
Root relative squared error: 90.2531560700858
Weighted TruePositiveRate: 0.7156250000000001
Weighted MatthewsCorrelation: 0.34228029540477833
Weighted FMeasure: 0.6698049685866089
Iteration time: 14.8
Weighted AreaUnderPRC: 0.8004140246049865
Mean absolute error: 0.32592889438322264
Coverage of cases: 96.66666666666666
Instances selection time: 0.8
Test time: 5.0
Accumulative iteration time: 136.3
Weighted Recall: 0.7156250000000001
Weighted FalsePositiveRate: 0.44259813432835815
Kappa statistic: 0.28692803853871257
Training time: 14.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 91.27604166666666
Incorrectly Classified Instances: 27.161458333333332
Correctly Classified Instances: 72.83854166666667
Weighted Precision: 0.7432541780546201
Weighted AreaUnderROC: 0.8172537313432837
Root mean squared error: 0.4286906971386359
Relative absolute error: 63.64996324633945
Root relative squared error: 85.73813942772719
Weighted TruePositiveRate: 0.7283854166666666
Weighted MatthewsCorrelation: 0.3773491370766407
Weighted FMeasure: 0.69729132471768
Iteration time: 14.4
Weighted AreaUnderPRC: 0.8161977089424376
Mean absolute error: 0.31824981623169724
Coverage of cases: 98.671875
Instances selection time: 1.2
Test time: 4.4
Accumulative iteration time: 150.7
Weighted Recall: 0.7283854166666666
Weighted FalsePositiveRate: 0.4170600435323383
Kappa statistic: 0.3338665419000796
Training time: 13.2
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 93.19010416666666
Incorrectly Classified Instances: 25.104166666666664
Correctly Classified Instances: 74.89583333333334
Weighted Precision: 0.753649518992944
Weighted AreaUnderROC: 0.8230328358208954
Root mean squared error: 0.4112260651669087
Relative absolute error: 62.43934877672598
Root relative squared error: 82.24521303338175
Weighted TruePositiveRate: 0.7489583333333333
Weighted MatthewsCorrelation: 0.4266579823314296
Weighted FMeasure: 0.730844926638023
Iteration time: 14.6
Weighted AreaUnderPRC: 0.8224715887579472
Mean absolute error: 0.31219674388362983
Coverage of cases: 98.90624999999999
Instances selection time: 0.7
Test time: 4.2
Accumulative iteration time: 165.3
Weighted Recall: 0.7489583333333333
Weighted FalsePositiveRate: 0.378677736318408
Kappa statistic: 0.39919891593302426
Training time: 13.9
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 93.99739583333334
Incorrectly Classified Instances: 23.828125
Correctly Classified Instances: 76.171875
Weighted Precision: 0.761064440086915
Weighted AreaUnderROC: 0.8255253731343284
Root mean squared error: 0.405169513927156
Relative absolute error: 62.03400040638779
Root relative squared error: 81.0339027854312
Weighted TruePositiveRate: 0.76171875
Weighted MatthewsCorrelation: 0.45678582657944594
Weighted FMeasure: 0.7502111496858394
Iteration time: 13.7
Weighted AreaUnderPRC: 0.8236985987845564
Mean absolute error: 0.31017000203193895
Coverage of cases: 99.24479166666666
Instances selection time: 0.5
Test time: 4.1
Accumulative iteration time: 179.0
Weighted Recall: 0.76171875
Weighted FalsePositiveRate: 0.3486381529850746
Kappa statistic: 0.4405111157696154
Training time: 13.2
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 95.05208333333333
Incorrectly Classified Instances: 23.28125
Correctly Classified Instances: 76.71875
Weighted Precision: 0.7634033134301176
Weighted AreaUnderROC: 0.8263283582089553
Root mean squared error: 0.40129072669699556
Relative absolute error: 62.13693897617043
Root relative squared error: 80.2581453393991
Weighted TruePositiveRate: 0.7671875
Weighted MatthewsCorrelation: 0.46839732521816035
Weighted FMeasure: 0.7582504799816601
Iteration time: 17.0
Weighted AreaUnderPRC: 0.824776141602019
Mean absolute error: 0.31068469488085215
Coverage of cases: 99.375
Instances selection time: 0.6
Test time: 4.5
Accumulative iteration time: 196.0
Weighted Recall: 0.7671875
Weighted FalsePositiveRate: 0.33497257462686564
Kappa statistic: 0.4578528166963206
Training time: 16.4
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 95.67708333333334
Incorrectly Classified Instances: 23.359375000000004
Correctly Classified Instances: 76.64062499999999
Weighted Precision: 0.7616084714783641
Weighted AreaUnderROC: 0.8273552238805969
Root mean squared error: 0.39886829269580293
Relative absolute error: 62.364715156781266
Root relative squared error: 79.77365853916059
Weighted TruePositiveRate: 0.7664062500000001
Weighted MatthewsCorrelation: 0.4671759215291404
Weighted FMeasure: 0.7586867896162691
Iteration time: 17.6
Weighted AreaUnderPRC: 0.8260834504134766
Mean absolute error: 0.31182357578390635
Coverage of cases: 99.45312500000001
Instances selection time: 1.2
Test time: 4.0
Accumulative iteration time: 213.6
Weighted Recall: 0.7664062500000001
Weighted FalsePositiveRate: 0.3305435634328358
Kappa statistic: 0.4591866614537311
Training time: 16.4
		
Time end:Mon Nov 27 16.03.16 EET 2017