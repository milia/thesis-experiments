Mon Nov 27 16.02.44 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.02.44 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.49479166666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7323875523012552
Weighted AreaUnderROC: 0.780865671641791
Root mean squared error: 0.44365161800040126
Relative absolute error: 65.72562104834002
Root relative squared error: 88.73032360008025
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.4102415846142742
Weighted FMeasure: 0.7288773720781934
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7937213407725864
Mean absolute error: 0.3286281052417001
Coverage of cases: 97.65625
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3093087686567164
Kappa statistic: 0.4094557384732556
Training time: 19.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 91.015625
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7360865865616916
Weighted AreaUnderROC: 0.7912835820895522
Root mean squared error: 0.43595318668354305
Relative absolute error: 65.28114260406977
Root relative squared error: 87.1906373367086
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.41728820495774643
Weighted FMeasure: 0.7299056064073226
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7963989917907549
Mean absolute error: 0.3264057130203489
Coverage of cases: 98.4375
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.2989207089552239
Kappa statistic: 0.41541495099460646
Training time: 14.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 86.58854166666667
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7239583333333334
Weighted AreaUnderROC: 0.7945074626865671
Root mean squared error: 0.43699608627365333
Relative absolute error: 62.41321055508844
Root relative squared error: 87.39921725473067
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.39247761194029857
Weighted FMeasure: 0.7239583333333334
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8041550824815505
Mean absolute error: 0.3120660527754422
Coverage of cases: 96.875
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.3314807213930348
Kappa statistic: 0.3924776119402987
Training time: 15.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 87.5
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7348404962760298
Weighted AreaUnderROC: 0.7946567164179106
Root mean squared error: 0.4317121382653912
Relative absolute error: 62.32547718973003
Root relative squared error: 86.34242765307823
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.41625344367946404
Weighted FMeasure: 0.735781051964512
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8070403906819675
Mean absolute error: 0.31162738594865014
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.32450155472636816
Kappa statistic: 0.41607949412827455
Training time: 16.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.19270833333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7491582491582491
Weighted AreaUnderROC: 0.8014029850746268
Root mean squared error: 0.4235484618309472
Relative absolute error: 62.064644279262424
Root relative squared error: 84.70969236618944
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4479088554955104
Weighted FMeasure: 0.7495581583440675
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8103630904841648
Mean absolute error: 0.31032322139631213
Coverage of cases: 97.91666666666667
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 101.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3036716417910448
Kappa statistic: 0.44787922358015814
Training time: 16.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 91.015625
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7575096899224806
Weighted AreaUnderROC: 0.8168059701492538
Root mean squared error: 0.413280295317013
Relative absolute error: 61.25013299903747
Root relative squared error: 82.6560590634026
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4658143321288654
Weighted FMeasure: 0.7586172521704019
Iteration time: 20.0
Weighted AreaUnderPRC: 0.821555129890795
Mean absolute error: 0.3062506649951873
Coverage of cases: 98.95833333333333
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 121.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.30155099502487565
Kappa statistic: 0.46531057028695966
Training time: 13.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 92.44791666666667
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7575096899224806
Weighted AreaUnderROC: 0.820089552238806
Root mean squared error: 0.4103314258797994
Relative absolute error: 61.71448043253832
Root relative squared error: 82.06628517595989
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4658143321288654
Weighted FMeasure: 0.7586172521704019
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8229283928883936
Mean absolute error: 0.3085724021626916
Coverage of cases: 99.47916666666667
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 145.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.30155099502487565
Kappa statistic: 0.46531057028695966
Training time: 16.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7480839657375693
Weighted AreaUnderROC: 0.8229850746268657
Root mean squared error: 0.4081853349319893
Relative absolute error: 61.340211973727286
Root relative squared error: 81.63706698639785
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44427789981020976
Weighted FMeasure: 0.7494326434277414
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8266920681006827
Mean absolute error: 0.30670105986863644
Coverage of cases: 99.47916666666667
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31958924129353233
Kappa statistic: 0.4429854027972881
Training time: 17.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 95.96354166666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.741711582477032
Weighted AreaUnderROC: 0.8209253731343283
Root mean squared error: 0.40719932493194466
Relative absolute error: 63.67295561139623
Root relative squared error: 81.43986498638893
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4293695882982311
Weighted FMeasure: 0.7429945755340982
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8260144764975209
Mean absolute error: 0.31836477805698116
Coverage of cases: 99.73958333333333
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.33276896766169156
Kappa statistic: 0.42720059051485526
Training time: 18.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 95.3125
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7471324906367042
Weighted AreaUnderROC: 0.8203582089552239
Root mean squared error: 0.40798478018435314
Relative absolute error: 63.156236816905185
Root relative squared error: 81.59695603687062
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44123984905464003
Weighted FMeasure: 0.7482936564509212
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8258234766376468
Mean absolute error: 0.31578118408452593
Coverage of cases: 99.73958333333333
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 217.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32651461442786067
Kappa statistic: 0.4390108876176416
Training time: 19.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 96.09375
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7438271604938271
Weighted AreaUnderROC: 0.8171641791044776
Root mean squared error: 0.40901259638477067
Relative absolute error: 64.20141215026507
Root relative squared error: 81.80251927695413
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43311973304262197
Weighted FMeasure: 0.7447270471464019
Iteration time: 25.0
Weighted AreaUnderPRC: 0.823035524458601
Mean absolute error: 0.32100706075132535
Coverage of cases: 99.73958333333333
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 242.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3348358208955224
Kappa statistic: 0.43005565862708717
Training time: 20.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 95.57291666666667
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7492994639376218
Weighted AreaUnderROC: 0.8171044776119403
Root mean squared error: 0.4095919498607413
Relative absolute error: 63.46601629472208
Root relative squared error: 81.91838997214826
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.44507817260894106
Weighted FMeasure: 0.7500452336641853
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8232941128897285
Mean absolute error: 0.3173300814736104
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 256.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.32858146766169155
Kappa statistic: 0.44192949907235624
Training time: 13.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 95.05208333333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7438271604938271
Weighted AreaUnderROC: 0.8174925373134329
Root mean squared error: 0.40991351812299454
Relative absolute error: 62.95712879980759
Root relative squared error: 81.98270362459891
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43311973304262197
Weighted FMeasure: 0.7447270471464019
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8237471583709747
Mean absolute error: 0.31478564399903797
Coverage of cases: 99.73958333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 269.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3348358208955224
Kappa statistic: 0.43005565862708717
Training time: 13.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 95.3125
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7438271604938271
Weighted AreaUnderROC: 0.816865671641791
Root mean squared error: 0.4099707065001531
Relative absolute error: 63.249129102559785
Root relative squared error: 81.99414130003062
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43311973304262197
Weighted FMeasure: 0.7447270471464019
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8228128446291533
Mean absolute error: 0.31624564551279893
Coverage of cases: 99.73958333333333
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 283.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3348358208955224
Kappa statistic: 0.43005565862708717
Training time: 14.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 94.66145833333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7438271604938271
Weighted AreaUnderROC: 0.8166567164179105
Root mean squared error: 0.4104872144002885
Relative absolute error: 62.72697302935503
Root relative squared error: 82.0974428800577
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43311973304262197
Weighted FMeasure: 0.7447270471464019
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8225767962647338
Mean absolute error: 0.31363486514677513
Coverage of cases: 99.73958333333333
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 311.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3348358208955224
Kappa statistic: 0.43005565862708717
Training time: 25.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 94.140625
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7467389620710091
Weighted AreaUnderROC: 0.8186268656716417
Root mean squared error: 0.40998147240995975
Relative absolute error: 62.22781706336512
Root relative squared error: 81.99629448199195
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.43979920937510375
Weighted FMeasure: 0.7476938099217679
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8243427582363595
Mean absolute error: 0.3111390853168256
Coverage of cases: 99.73958333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 336.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.32997730099502487
Kappa statistic: 0.43700228409161046
Training time: 25.0
		
Time end:Mon Nov 27 16.02.45 EET 2017