Mon Nov 27 16.02.32 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 16.02.32 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.49479166666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7323875523012552
Weighted AreaUnderROC: 0.780865671641791
Root mean squared error: 0.44365161800040126
Relative absolute error: 65.72562104834002
Root relative squared error: 88.73032360008025
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.4102415846142742
Weighted FMeasure: 0.7288773720781934
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7937213407725864
Mean absolute error: 0.3286281052417001
Coverage of cases: 97.65625
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3093087686567164
Kappa statistic: 0.4094557384732556
Training time: 20.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7273748233393903
Weighted AreaUnderROC: 0.7891343283582088
Root mean squared error: 0.4371284530290417
Relative absolute error: 62.99963850329869
Root relative squared error: 87.42569060580834
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.39989014646365645
Weighted FMeasure: 0.7281896344396345
Iteration time: 24.0
Weighted AreaUnderPRC: 0.797992672748734
Mean absolute error: 0.31499819251649347
Coverage of cases: 97.65625
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.33215174129353237
Kappa statistic: 0.3997835758085847
Training time: 20.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 90.10416666666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7225116478479937
Weighted AreaUnderROC: 0.7939402985074627
Root mean squared error: 0.4320483547059262
Relative absolute error: 63.07436081284112
Root relative squared error: 86.40967094118524
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3779438446024362
Weighted FMeasure: 0.7186343820974201
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8027533245264801
Mean absolute error: 0.3153718040642056
Coverage of cases: 99.47916666666667
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 75.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3896215796019901
Kappa statistic: 0.36692489916127796
Training time: 20.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 90.88541666666667
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7010817902623409
Weighted AreaUnderROC: 0.7966567164179105
Root mean squared error: 0.4340523360307454
Relative absolute error: 64.04144363593424
Root relative squared error: 86.81046720614908
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3127727379659907
Weighted FMeasure: 0.6826937031581685
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8025971271383338
Mean absolute error: 0.3202072181796712
Coverage of cases: 99.73958333333333
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 100.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.4561912313432836
Kappa statistic: 0.28593446357970925
Training time: 19.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.453125
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.7003580729166666
Weighted AreaUnderROC: 0.7946865671641792
Root mean squared error: 0.43915751980821316
Relative absolute error: 63.65865742199307
Root relative squared error: 87.83150396164262
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3029802220036369
Weighted FMeasure: 0.6746854510012404
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8015259046119928
Mean absolute error: 0.31829328710996535
Coverage of cases: 99.21875
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 128.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.47143781094527365
Kappa statistic: 0.2695652173913045
Training time: 21.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 87.5
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7004532553290085
Weighted AreaUnderROC: 0.7826268656716419
Root mean squared error: 0.4533000100196303
Relative absolute error: 64.88059529757987
Root relative squared error: 90.66000200392605
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.2850871823875993
Weighted FMeasure: 0.6585986863984038
Iteration time: 27.0
Weighted AreaUnderPRC: 0.791231888375942
Mean absolute error: 0.3244029764878994
Coverage of cases: 99.21875
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 155.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.4984682835820895
Kappa statistic: 0.23851934316726958
Training time: 24.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 88.671875
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7089766437934059
Weighted AreaUnderROC: 0.794955223880597
Root mean squared error: 0.45459115560682456
Relative absolute error: 64.92483570214728
Root relative squared error: 90.91823112136491
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.27763085812034655
Weighted FMeasure: 0.6444285513530876
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8020331144679419
Mean absolute error: 0.3246241785107364
Coverage of cases: 99.47916666666667
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 180.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.520640236318408
Kappa statistic: 0.21440261865793786
Training time: 23.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7059673299006569
Weighted AreaUnderROC: 0.7972537313432836
Root mean squared error: 0.45239582673861184
Relative absolute error: 63.702176698618494
Root relative squared error: 90.47916534772237
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.2771251819745542
Weighted FMeasure: 0.6465924245031404
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8032868134274822
Mean absolute error: 0.3185108834930925
Coverage of cases: 99.21875
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 204.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.5171775497512437
Kappa statistic: 0.21763163489476295
Training time: 19.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7256580225330226
Weighted AreaUnderROC: 0.8032835820895522
Root mean squared error: 0.45745601426507077
Relative absolute error: 64.24598230130111
Root relative squared error: 91.49120285301416
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.28235667096021905
Weighted FMeasure: 0.6351239907871445
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8062094717080627
Mean absolute error: 0.32122991150650554
Coverage of cases: 98.95833333333333
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 230.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.5344909825870647
Kappa statistic: 0.20121554156718038
Training time: 23.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 84.375
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7132287080203747
Weighted AreaUnderROC: 0.8051641791044776
Root mean squared error: 0.4619196783253616
Relative absolute error: 63.87732916454962
Root relative squared error: 92.38393566507233
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.2628627907644758
Weighted FMeasure: 0.6287783210617036
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8109723732817066
Mean absolute error: 0.3193866458227481
Coverage of cases: 98.4375
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 257.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.5407453358208956
Kappa statistic: 0.1873236379422617
Training time: 25.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 82.16145833333333
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.738855975932705
Weighted AreaUnderROC: 0.8207761194029851
Root mean squared error: 0.4556601293923355
Relative absolute error: 61.75089656421996
Root relative squared error: 91.1320258784671
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3164085591811218
Weighted FMeasure: 0.6524352390608009
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8215991475306562
Mean absolute error: 0.3087544828210998
Coverage of cases: 98.17708333333333
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 281.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.5150569029850747
Kappa statistic: 0.23541651718447304
Training time: 22.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7529757377547917
Weighted AreaUnderROC: 0.8252835820895522
Root mean squared error: 0.43950081314044287
Relative absolute error: 63.79414980888238
Root relative squared error: 87.90016262808858
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.34287743390550923
Weighted FMeasure: 0.6629126346183861
Iteration time: 30.0
Weighted AreaUnderPRC: 0.8271937031037194
Mean absolute error: 0.3189707490444119
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 311.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.5039440298507463
Kappa statistic: 0.25762566232278394
Training time: 29.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 92.31770833333333
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7273749689749317
Weighted AreaUnderROC: 0.8222686567164179
Root mean squared error: 0.4180949681494091
Relative absolute error: 62.61811073533985
Root relative squared error: 83.61899362988183
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.3616849074274604
Weighted FMeasure: 0.7007122532041191
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8257862842051775
Mean absolute error: 0.31309055367669925
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 343.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.4394950248756219
Kappa statistic: 0.3270423294688594
Training time: 27.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 93.359375
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7437462775461584
Weighted AreaUnderROC: 0.8233134328358208
Root mean squared error: 0.4111127756646158
Relative absolute error: 62.17414071851768
Root relative squared error: 82.22255513292316
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42107976712987666
Weighted FMeasure: 0.7364562583678934
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8257197464059313
Mean absolute error: 0.3108707035925884
Coverage of cases: 99.73958333333333
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 371.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.37292537313432844
Kappa statistic: 0.4066443471542622
Training time: 25.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 96.09375
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7341508720218313
Weighted AreaUnderROC: 0.8218507462686567
Root mean squared error: 0.4068227048916326
Relative absolute error: 63.95561866470377
Root relative squared error: 81.36454097832652
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.40647635009072
Weighted FMeasure: 0.732066833659335
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8252950765007636
Mean absolute error: 0.31977809332351886
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 403.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3667248134328358
Kappa statistic: 0.3982143987842715
Training time: 29.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 97.00520833333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7399945596518177
Weighted AreaUnderROC: 0.8214925373134329
Root mean squared error: 0.4064444537170333
Relative absolute error: 64.4011527794312
Root relative squared error: 81.28889074340665
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4211698994317592
Weighted FMeasure: 0.7389944086761422
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8247192400255822
Mean absolute error: 0.322005763897156
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 435.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.35354508706467663
Kappa statistic: 0.4146709409768056
Training time: 31.0
		
Time end:Mon Nov 27 16.02.33 EET 2017