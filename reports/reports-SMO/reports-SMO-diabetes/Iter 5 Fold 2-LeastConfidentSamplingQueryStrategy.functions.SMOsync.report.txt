Mon Nov 27 16.02.50 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 16.02.50 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 97.00520833333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7271411877129962
Weighted AreaUnderROC: 0.7982985074626866
Root mean squared error: 0.42310038055323657
Relative absolute error: 66.56013517641638
Root relative squared error: 84.62007611064732
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3401192864759704
Weighted FMeasure: 0.6821281789438093
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8048557157095441
Mean absolute error: 0.3328006758820819
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.4713840174129353
Kappa statistic: 0.28957742582474577
Training time: 14.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 96.61458333333333
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7835266433128275
Weighted AreaUnderROC: 0.8415522388059702
Root mean squared error: 0.39562158694266125
Relative absolute error: 61.82751695741141
Root relative squared error: 79.12431738853225
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.4865193042953336
Weighted FMeasure: 0.7567534988905957
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8437972849395505
Mean absolute error: 0.30913758478705705
Coverage of cases: 99.73958333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.3693550995024875
Kappa statistic: 0.452083886381736
Training time: 12.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 97.78645833333333
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7667100694444445
Weighted AreaUnderROC: 0.8373731343283582
Root mean squared error: 0.39324267882975267
Relative absolute error: 62.954465713923305
Root relative squared error: 78.64853576595053
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.4731602234073839
Weighted FMeasure: 0.7599954205053607
Iteration time: 9.0
Weighted AreaUnderPRC: 0.840253324643133
Mean absolute error: 0.3147723285696165
Coverage of cases: 99.73958333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.3409825870646766
Kappa statistic: 0.46012269938650313
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 97.65625
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7884825453571297
Weighted AreaUnderROC: 0.846
Root mean squared error: 0.3893244186941456
Relative absolute error: 62.73094445605487
Root relative squared error: 77.86488373882912
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5171044084716158
Weighted FMeasure: 0.7780060888222984
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8437241013441833
Mean absolute error: 0.31365472228027436
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.3242863805970149
Kappa statistic: 0.5002891844997108
Training time: 8.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 97.65625
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7726056134259259
Weighted AreaUnderROC: 0.8309850746268657
Root mean squared error: 0.39294903494785766
Relative absolute error: 62.93544470122102
Root relative squared error: 78.58980698957153
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.48577782936491404
Weighted FMeasure: 0.7654500700393297
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8388180624505089
Mean absolute error: 0.3146772235061051
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.33472823383084577
Kappa statistic: 0.4723926380368097
Training time: 8.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 95.57291666666667
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.7954701548451548
Weighted AreaUnderROC: 0.8527462686567165
Root mean squared error: 0.38105659608109543
Relative absolute error: 59.603223453564816
Root relative squared error: 76.21131921621908
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5363862972609986
Weighted FMeasure: 0.7879366636644365
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8528244806805588
Mean absolute error: 0.29801611726782407
Coverage of cases: 99.47916666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 66.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.30624813432835823
Kappa statistic: 0.5232395262956833
Training time: 9.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 97.52604166666667
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7965635133981909
Weighted AreaUnderROC: 0.8530149253731342
Root mean squared error: 0.38171265387797854
Relative absolute error: 61.770587176971844
Root relative squared error: 76.3425307755957
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5437477692072599
Weighted FMeasure: 0.792809994516113
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8520761863581644
Mean absolute error: 0.30885293588485924
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.2910015547263682
Kappa statistic: 0.5353573448991136
Training time: 9.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 95.05208333333333
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7936234671887331
Weighted AreaUnderROC: 0.8492835820895523
Root mean squared error: 0.38388948605548817
Relative absolute error: 59.00792669525304
Root relative squared error: 76.77789721109764
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5298863708568443
Weighted FMeasure: 0.7841953027737493
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8506880530426316
Mean absolute error: 0.2950396334762652
Coverage of cases: 99.47916666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 86.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.3145693407960199
Kappa statistic: 0.5144375440169026
Training time: 9.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 97.13541666666667
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7995494521337947
Weighted AreaUnderROC: 0.8522089552238805
Root mean squared error: 0.3826865488220521
Relative absolute error: 60.63386615238356
Root relative squared error: 76.53730976441042
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5425482480447116
Weighted FMeasure: 0.7896587128301102
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8530739403150532
Mean absolute error: 0.3031693307619178
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 97.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.30831498756218906
Kappa statistic: 0.5267302644215378
Training time: 10.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 97.91666666666667
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8020833333333334
Weighted AreaUnderROC: 0.853044776119403
Root mean squared error: 0.38357973654249417
Relative absolute error: 61.978545653746195
Root relative squared error: 76.71594730849883
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5488658591525652
Weighted FMeasure: 0.7927233177091751
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8532175891778794
Mean absolute error: 0.309892728268731
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.30345646766169154
Kappa statistic: 0.5337423312883436
Training time: 13.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 98.046875
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.7954701548451548
Weighted AreaUnderROC: 0.8523582089552239
Root mean squared error: 0.38306606453460607
Relative absolute error: 62.23363428779598
Root relative squared error: 76.61321290692122
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5363862972609986
Weighted FMeasure: 0.7879366636644365
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8526878507210816
Mean absolute error: 0.3111681714389799
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 122.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.30624813432835823
Kappa statistic: 0.5232395262956833
Training time: 10.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 97.52604166666667
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7974617415363444
Weighted AreaUnderROC: 0.8524776119402985
Root mean squared error: 0.38204883135683476
Relative absolute error: 61.340599027156806
Root relative squared error: 76.40976627136695
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5429810489922307
Weighted FMeasure: 0.7916075372905939
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8527383048133038
Mean absolute error: 0.30670299513578403
Coverage of cases: 99.47916666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.2979269278606965
Kappa statistic: 0.5319445323877666
Training time: 11.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 97.13541666666667
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7974617415363444
Weighted AreaUnderROC: 0.8519701492537314
Root mean squared error: 0.3818161270839616
Relative absolute error: 60.823568030248076
Root relative squared error: 76.36322541679232
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5429810489922307
Weighted FMeasure: 0.7916075372905939
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8520911508326737
Mean absolute error: 0.3041178401512404
Coverage of cases: 99.47916666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 147.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.2979269278606965
Kappa statistic: 0.5319445323877666
Training time: 12.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 97.91666666666667
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.796187789351852
Weighted AreaUnderROC: 0.8528955223880597
Root mean squared error: 0.38261252997883105
Relative absolute error: 60.96079744510256
Root relative squared error: 76.52250599576621
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.536248253195035
Weighted FMeasure: 0.7872686681752062
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8561952590269857
Mean absolute error: 0.3048039872255128
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 160.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.3097108208955224
Kappa statistic: 0.5214723926380368
Training time: 12.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 97.00520833333333
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7922453703703703
Weighted AreaUnderROC: 0.8526567164179104
Root mean squared error: 0.38202362160876074
Relative absolute error: 59.85376207850032
Root relative squared error: 76.40472432175216
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5302472904737245
Weighted FMeasure: 0.7855485306111533
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8560355693984248
Mean absolute error: 0.2992688103925016
Coverage of cases: 99.73958333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 173.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.30764396766169155
Kappa statistic: 0.5180171591992375
Training time: 13.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 96.22395833333333
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.7954701548451548
Weighted AreaUnderROC: 0.8528656716417911
Root mean squared error: 0.3817944145776247
Relative absolute error: 59.17124651179553
Root relative squared error: 76.35888291552494
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5363862972609986
Weighted FMeasure: 0.7879366636644365
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8560873157213607
Mean absolute error: 0.29585623255897764
Coverage of cases: 99.73958333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 188.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.30624813432835823
Kappa statistic: 0.5232395262956833
Training time: 14.0
		
Time end:Mon Nov 27 16.02.50 EET 2017