Thu Nov 30 21.29.51 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Thu Nov 30 21.29.51 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 861.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 147.0
Test time: 91.0
Accumulative iteration time: 861.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 714.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999270301541822
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 470.0
Weighted AreaUnderPRC: 0.9990214274497757
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 107.0
Test time: 88.0
Accumulative iteration time: 1331.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 363.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 588.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 102.0
Test time: 86.0
Accumulative iteration time: 1919.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 486.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992706525648435
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 623.0
Weighted AreaUnderPRC: 0.9990215988670504
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 100.0
Test time: 83.0
Accumulative iteration time: 2542.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 523.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992706525648435
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 564.0
Weighted AreaUnderPRC: 0.9990215988670504
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 103.0
Test time: 86.0
Accumulative iteration time: 3106.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 461.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992700675264744
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 612.0
Weighted AreaUnderPRC: 0.9990213133485584
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 106.0
Test time: 86.0
Accumulative iteration time: 3718.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 506.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 642.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 99.0
Test time: 91.0
Accumulative iteration time: 4360.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 543.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.978008658348558E-46
Relative absolute error: 1.0604422311370287E-45
Root relative squared error: 5.956017316697116E-44
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 523.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.302211155685143E-48
Coverage of cases: 100.0
Instances selection time: 96.0
Test time: 89.0
Accumulative iteration time: 4883.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 427.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.534623855627719E-34
Relative absolute error: 1.3104400727750352E-33
Root relative squared error: 9.069247711255439E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 653.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.552200363875176E-36
Coverage of cases: 100.0
Instances selection time: 98.0
Test time: 85.0
Accumulative iteration time: 5536.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 555.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4525423085005985E-31
Relative absolute error: 9.507994231583657E-31
Root relative squared error: 6.905084617001197E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 620.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.753997115791828E-33
Coverage of cases: 100.0
Instances selection time: 102.0
Test time: 83.0
Accumulative iteration time: 6156.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 518.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0878515324516624E-28
Relative absolute error: 3.3102706575273164E-28
Root relative squared error: 2.1757030649033248E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 632.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.6551353287636582E-30
Coverage of cases: 100.0
Instances selection time: 101.0
Test time: 86.0
Accumulative iteration time: 6788.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 531.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.4762345750321363E-25
Relative absolute error: 7.522725645351705E-25
Root relative squared error: 4.9524691500642724E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 765.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7613628226758526E-27
Coverage of cases: 100.0
Instances selection time: 97.0
Test time: 86.0
Accumulative iteration time: 7553.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 668.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.420802951543185E-25
Relative absolute error: 2.4829455072253876E-24
Root relative squared error: 1.884160590308637E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 622.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2414727536126938E-26
Coverage of cases: 100.0
Instances selection time: 99.0
Test time: 87.0
Accumulative iteration time: 8175.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 523.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.7256804682116004E-74
Relative absolute error: 3.837400632949202E-74
Root relative squared error: 3.4513609364232006E-72
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 701.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.918700316474601E-76
Coverage of cases: 100.0
Instances selection time: 101.0
Test time: 82.0
Accumulative iteration time: 8876.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 600.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.440155167304583E-73
Relative absolute error: 1.0387826724676042E-72
Root relative squared error: 4.880310334609166E-71
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 716.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.193913362338021E-75
Coverage of cases: 100.0
Instances selection time: 105.0
Test time: 84.0
Accumulative iteration time: 9592.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 611.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3253995832877134E-71
Relative absolute error: 5.702566792875714E-71
Root relative squared error: 2.6507991665754267E-69
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 779.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.851283396437857E-73
Coverage of cases: 100.0
Instances selection time: 100.0
Test time: 82.0
Accumulative iteration time: 10371.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 679.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.931010252324095E-70
Relative absolute error: 8.294958368541683E-70
Root relative squared error: 3.86202050464819E-68
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 700.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1474791842708415E-72
Coverage of cases: 100.0
Instances selection time: 97.0
Test time: 86.0
Accumulative iteration time: 11071.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 603.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.149721039554472E-69
Relative absolute error: 3.0291329107654443E-68
Root relative squared error: 1.4299442079108943E-66
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 696.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.514566455382722E-70
Coverage of cases: 100.0
Instances selection time: 94.0
Test time: 86.0
Accumulative iteration time: 11767.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 602.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.8897438585055556E-67
Relative absolute error: 1.9301515467573042E-66
Root relative squared error: 9.779487717011112E-65
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 802.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.65075773378652E-69
Coverage of cases: 100.0
Instances selection time: 95.0
Test time: 86.0
Accumulative iteration time: 12569.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 707.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.6737493344385526E-65
Relative absolute error: 7.250378087920168E-65
Root relative squared error: 3.347498668877105E-63
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1819.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.625189043960084E-67
Coverage of cases: 100.0
Instances selection time: 124.0
Test time: 86.0
Accumulative iteration time: 14388.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1695.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.36045748312476E-64
Relative absolute error: 2.9647541621986937E-63
Root relative squared error: 1.872091496624952E-61
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 783.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.482377081099347E-65
Coverage of cases: 100.0
Instances selection time: 96.0
Test time: 85.0
Accumulative iteration time: 15171.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 687.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.3298759152862048E-32
Relative absolute error: 8.174407980878184E-32
Root relative squared error: 4.65975183057241E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 837.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.087203990439092E-34
Coverage of cases: 100.0
Instances selection time: 89.0
Test time: 88.0
Accumulative iteration time: 16008.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 748.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4160473663200325E-31
Relative absolute error: 5.621057096857541E-31
Root relative squared error: 2.832094732640065E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 780.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8105285484287705E-33
Coverage of cases: 100.0
Instances selection time: 127.0
Test time: 84.0
Accumulative iteration time: 16788.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 653.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.2414351016964879E-30
Relative absolute error: 5.3668128103164874E-30
Root relative squared error: 2.482870203392976E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 766.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.683406405158244E-32
Coverage of cases: 100.0
Instances selection time: 87.0
Test time: 90.0
Accumulative iteration time: 17554.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 679.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.821283934415213E-30
Relative absolute error: 1.889443431309998E-29
Root relative squared error: 9.642567868830427E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 715.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.44721715654999E-32
Coverage of cases: 100.0
Instances selection time: 86.0
Test time: 88.0
Accumulative iteration time: 18269.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 629.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.869353702866509E-29
Relative absolute error: 1.1280285762851732E-28
Root relative squared error: 5.7387074057330176E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 615.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.640142881425866E-31
Coverage of cases: 100.0
Instances selection time: 84.0
Test time: 89.0
Accumulative iteration time: 18884.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 531.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.373997601733739E-28
Relative absolute error: 5.203438580365781E-28
Root relative squared error: 2.747995203467478E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 759.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.6017192901828907E-30
Coverage of cases: 100.0
Instances selection time: 86.0
Test time: 86.0
Accumulative iteration time: 19643.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 673.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.07882094280819E-28
Relative absolute error: 3.783618345675037E-27
Root relative squared error: 1.815764188561638E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 784.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8918091728375185E-29
Coverage of cases: 100.0
Instances selection time: 83.0
Test time: 89.0
Accumulative iteration time: 20427.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 701.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.056818497334507E-27
Relative absolute error: 1.5606590482543038E-26
Root relative squared error: 8.113636994669014E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 864.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.803295241271518E-29
Coverage of cases: 100.0
Instances selection time: 82.0
Test time: 89.0
Accumulative iteration time: 21291.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 782.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.9334262361558547E-26
Relative absolute error: 8.160250806099762E-26
Root relative squared error: 3.86685247231171E-24
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 766.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.080125403049881E-28
Coverage of cases: 100.0
Instances selection time: 80.0
Test time: 87.0
Accumulative iteration time: 22057.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 686.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.839343191938866E-25
Relative absolute error: 7.6658194462078E-25
Root relative squared error: 3.678686383877732E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 764.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.8329097231039E-27
Coverage of cases: 100.0
Instances selection time: 86.0
Test time: 88.0
Accumulative iteration time: 22821.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 678.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.189912200725436E-25
Relative absolute error: 3.718853257427548E-24
Root relative squared error: 1.8379824401450873E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 831.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8594266287137737E-26
Coverage of cases: 100.0
Instances selection time: 87.0
Test time: 88.0
Accumulative iteration time: 23652.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 744.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.22052265234028E-24
Relative absolute error: 3.10617905346191E-23
Root relative squared error: 1.444104530468056E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 935.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.5530895267309549E-25
Coverage of cases: 100.0
Instances selection time: 83.0
Test time: 90.0
Accumulative iteration time: 24587.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 852.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.1488113232382506E-23
Relative absolute error: 8.252171236109145E-23
Root relative squared error: 4.2976226464765014E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 782.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1260856180545727E-25
Coverage of cases: 100.0
Instances selection time: 77.0
Test time: 85.0
Accumulative iteration time: 25369.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 705.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.8940390596879085E-22
Relative absolute error: 7.349937963634528E-22
Root relative squared error: 3.788078119375817E-20
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 724.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6749689818172644E-24
Coverage of cases: 100.0
Instances selection time: 80.0
Test time: 85.0
Accumulative iteration time: 26093.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 644.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.959594170419066E-22
Relative absolute error: 3.9052456598914535E-21
Root relative squared error: 1.9919188340838132E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 670.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.9526228299457267E-23
Coverage of cases: 100.0
Instances selection time: 81.0
Test time: 89.0
Accumulative iteration time: 26763.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 589.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.293756835103379E-21
Relative absolute error: 2.6621533292340148E-20
Root relative squared error: 1.2587513670206758E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 754.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3310766646170073E-22
Coverage of cases: 100.0
Instances selection time: 78.0
Test time: 88.0
Accumulative iteration time: 27517.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 676.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.899435087260674E-20
Relative absolute error: 1.1470604938776996E-19
Root relative squared error: 5.7988701745213474E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 710.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.735302469388498E-22
Coverage of cases: 100.0
Instances selection time: 77.0
Test time: 90.0
Accumulative iteration time: 28227.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 633.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6325089966479598E-19
Relative absolute error: 1.1288168581250097E-18
Root relative squared error: 5.2650179932959193E-17
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 909.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.6440842906250486E-21
Coverage of cases: 100.0
Instances selection time: 77.0
Test time: 84.0
Accumulative iteration time: 29136.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 832.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3996291553764663E-18
Relative absolute error: 5.51717114626589E-18
Root relative squared error: 2.7992583107529325E-16
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 744.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.758585573132945E-20
Coverage of cases: 100.0
Instances selection time: 79.0
Test time: 86.0
Accumulative iteration time: 29880.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 665.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1033126459249843E-17
Relative absolute error: 6.346058748568411E-17
Root relative squared error: 2.2066252918499685E-15
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 888.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1730293742842055E-19
Coverage of cases: 100.0
Instances selection time: 75.0
Test time: 85.0
Accumulative iteration time: 30768.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 813.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.968534764523344E-17
Relative absolute error: 3.3103227614632525E-16
Root relative squared error: 1.1937069529046687E-14
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 718.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.6551613807316263E-18
Coverage of cases: 100.0
Instances selection time: 76.0
Test time: 86.0
Accumulative iteration time: 31486.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 642.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.791407023723393E-16
Relative absolute error: 2.346991104963632E-15
Root relative squared error: 7.582814047446786E-14
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 942.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1734955524818161E-17
Coverage of cases: 100.0
Instances selection time: 74.0
Test time: 87.0
Accumulative iteration time: 32428.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 868.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.682285872550214E-16
Relative absolute error: 4.1560699654292994E-15
Root relative squared error: 1.536457174510043E-13
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 886.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.0780349827146497E-17
Coverage of cases: 100.0
Instances selection time: 79.0
Test time: 85.0
Accumulative iteration time: 33314.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 807.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.040112610985562E-14
Relative absolute error: 5.974927912997897E-14
Root relative squared error: 2.0802252219711243E-12
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 824.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9874639564989485E-16
Coverage of cases: 100.0
Instances selection time: 77.0
Test time: 91.0
Accumulative iteration time: 34138.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 747.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.128049441156864E-14
Relative absolute error: 1.830673649269979E-13
Root relative squared error: 6.256098882313728E-12
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 756.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.153368246349895E-16
Coverage of cases: 100.0
Instances selection time: 80.0
Test time: 90.0
Accumulative iteration time: 34894.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 676.0
		
Time end:Thu Nov 30 21.30.40 EET 2017