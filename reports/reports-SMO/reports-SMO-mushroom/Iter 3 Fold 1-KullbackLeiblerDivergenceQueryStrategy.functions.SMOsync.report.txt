Thu Nov 30 21.31.26 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Thu Nov 30 21.31.26 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994883387087831
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 429.0
Weighted AreaUnderPRC: 0.9993355620688832
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 118.0
Test time: 78.0
Accumulative iteration time: 429.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 311.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994883387087831
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 366.0
Weighted AreaUnderPRC: 0.9993355620688832
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 99.0
Test time: 83.0
Accumulative iteration time: 795.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 267.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 448.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 104.0
Test time: 82.0
Accumulative iteration time: 1243.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 344.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 493.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 101.0
Test time: 84.0
Accumulative iteration time: 1736.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 392.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 446.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 99.0
Test time: 87.0
Accumulative iteration time: 2182.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 347.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 412.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 100.0
Test time: 83.0
Accumulative iteration time: 2594.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 312.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 467.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 112.0
Test time: 85.0
Accumulative iteration time: 3061.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 355.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 425.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 101.0
Test time: 87.0
Accumulative iteration time: 3486.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 324.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 443.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 97.0
Test time: 85.0
Accumulative iteration time: 3929.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 346.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 415.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 98.0
Test time: 82.0
Accumulative iteration time: 4344.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 317.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 438.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 99.0
Test time: 86.0
Accumulative iteration time: 4782.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 339.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 478.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 95.0
Test time: 87.0
Accumulative iteration time: 5260.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 383.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.049236829148202856
Correctly Classified Instances: 99.9507631708518
Weighted Precision: 0.9995080992947397
Weighted AreaUnderROC: 0.9994792121102243
Root mean squared error: 0.022189373390928115
Relative absolute error: 0.09847365829640572
Root relative squared error: 4.437874678185623
Weighted TruePositiveRate: 0.999507631708518
Weighted MatthewsCorrelation: 0.999014440317274
Weighted FMeasure: 0.9995076228639044
Iteration time: 526.0
Weighted AreaUnderPRC: 0.9993314757751204
Mean absolute error: 4.923682914820286E-4
Coverage of cases: 99.9507631708518
Instances selection time: 128.0
Test time: 201.0
Accumulative iteration time: 5786.0
Weighted Recall: 0.999507631708518
Weighted FalsePositiveRate: 5.290821681706783E-4
Kappa statistic: 0.999013954653566
Training time: 398.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 482.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 98.0
Test time: 85.0
Accumulative iteration time: 6268.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 384.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 522.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 92.0
Test time: 83.0
Accumulative iteration time: 6790.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 430.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 598.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 97.0
Test time: 80.0
Accumulative iteration time: 7388.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 501.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 461.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 98.0
Test time: 82.0
Accumulative iteration time: 7849.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 363.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 492.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 97.0
Test time: 85.0
Accumulative iteration time: 8341.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 395.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 605.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 92.0
Test time: 85.0
Accumulative iteration time: 8946.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 513.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 604.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 88.0
Test time: 84.0
Accumulative iteration time: 9550.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 516.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 568.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 89.0
Test time: 78.0
Accumulative iteration time: 10118.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 479.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 0.9998677294579573
Root mean squared error: 0.015690256395005604
Relative absolute error: 0.04923682914820286
Root relative squared error: 3.138051279001121
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 561.0
Weighted AreaUnderPRC: 0.9997539328063473
Mean absolute error: 2.461841457410143E-4
Coverage of cases: 99.9753815854259
Instances selection time: 86.0
Test time: 86.0
Accumulative iteration time: 10679.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 475.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.796808619916195E-38
Relative absolute error: 6.254959800048895E-38
Root relative squared error: 5.59361723983239E-36
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 610.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1274799000244476E-40
Coverage of cases: 100.0
Instances selection time: 83.0
Test time: 87.0
Accumulative iteration time: 11289.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 527.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.5308958308059435E-37
Relative absolute error: 3.4264525383529065E-37
Root relative squared error: 3.061791661611887E-35
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 657.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.7132262691764532E-39
Coverage of cases: 100.0
Instances selection time: 88.0
Test time: 77.0
Accumulative iteration time: 11946.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 569.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.1542236155848345E-37
Relative absolute error: 1.1507080233526852E-36
Root relative squared error: 1.030844723116967E-34
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 579.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.753540116763426E-39
Coverage of cases: 100.0
Instances selection time: 85.0
Test time: 81.0
Accumulative iteration time: 12525.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 494.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.5406200106423204E-36
Relative absolute error: 5.67048423160465E-36
Root relative squared error: 5.081240021284641E-34
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 611.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.835242115802325E-38
Coverage of cases: 100.0
Instances selection time: 84.0
Test time: 81.0
Accumulative iteration time: 13136.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 527.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.0773166658056063E-35
Relative absolute error: 4.6334731353038146E-35
Root relative squared error: 4.1546333316112124E-33
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 551.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.3167365676519072E-37
Coverage of cases: 100.0
Instances selection time: 82.0
Test time: 86.0
Accumulative iteration time: 13687.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 469.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.301889495427391E-35
Relative absolute error: 1.6285760312900654E-34
Root relative squared error: 1.4603778990854784E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 549.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.142880156450327E-37
Coverage of cases: 100.0
Instances selection time: 84.0
Test time: 83.0
Accumulative iteration time: 14236.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 465.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.9939345169484834E-34
Relative absolute error: 6.67602113218621E-34
Root relative squared error: 5.987869033896967E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 644.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.338010566093105E-36
Coverage of cases: 100.0
Instances selection time: 81.0
Test time: 84.0
Accumulative iteration time: 14880.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 563.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.016378540613094E-33
Relative absolute error: 2.266034251194476E-33
Root relative squared error: 2.032757081226188E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 511.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1330171255972379E-35
Coverage of cases: 100.0
Instances selection time: 82.0
Test time: 81.0
Accumulative iteration time: 15391.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 429.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.097724736096862E-32
Relative absolute error: 2.4443902421156826E-32
Root relative squared error: 2.195449472193724E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 657.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2221951210578414E-34
Coverage of cases: 100.0
Instances selection time: 81.0
Test time: 84.0
Accumulative iteration time: 16048.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 576.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.822149993125112E-32
Relative absolute error: 1.5192447392117909E-31
Root relative squared error: 1.3644299986250224E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 706.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.5962236960589545E-34
Coverage of cases: 100.0
Instances selection time: 86.0
Test time: 81.0
Accumulative iteration time: 16754.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 620.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.583511500202534E-31
Relative absolute error: 1.4657877135643054E-30
Root relative squared error: 1.3167023000405068E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 671.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.328938567821527E-33
Coverage of cases: 100.0
Instances selection time: 79.0
Test time: 86.0
Accumulative iteration time: 17425.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 592.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.8318189679974395E-30
Relative absolute error: 6.301141477767155E-30
Root relative squared error: 5.663637935994879E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 570.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1505707388835773E-32
Coverage of cases: 100.0
Instances selection time: 78.0
Test time: 80.0
Accumulative iteration time: 17995.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 492.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.1486845715315573E-29
Relative absolute error: 4.780662570898382E-29
Root relative squared error: 4.297369143063115E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 520.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.3903312854491912E-31
Coverage of cases: 100.0
Instances selection time: 75.0
Test time: 86.0
Accumulative iteration time: 18515.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 445.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.640811398425612E-29
Relative absolute error: 1.6995583596032072E-28
Root relative squared error: 1.5281622796851223E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 609.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.497791798016036E-31
Coverage of cases: 100.0
Instances selection time: 74.0
Test time: 81.0
Accumulative iteration time: 19124.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 535.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.6388922589627437E-28
Relative absolute error: 8.09195275718554E-28
Root relative squared error: 7.277784517925488E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 557.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0459763785927706E-30
Coverage of cases: 100.0
Instances selection time: 74.0
Test time: 83.0
Accumulative iteration time: 19681.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 483.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.750803580053772E-31
Relative absolute error: 2.3490688244553442E-30
Root relative squared error: 9.501607160107544E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 609.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.174534412227672E-32
Coverage of cases: 100.0
Instances selection time: 78.0
Test time: 82.0
Accumulative iteration time: 20290.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 531.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.75243497354309E-30
Relative absolute error: 1.3600149498421736E-29
Root relative squared error: 5.50486994708618E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 727.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.800074749210868E-32
Coverage of cases: 100.0
Instances selection time: 75.0
Test time: 82.0
Accumulative iteration time: 21017.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 652.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.5116903914990215E-29
Relative absolute error: 7.460257686129027E-29
Root relative squared error: 3.023380782998043E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 557.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7301288430645133E-31
Coverage of cases: 100.0
Instances selection time: 77.0
Test time: 84.0
Accumulative iteration time: 21574.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 480.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.6085439340923146E-29
Relative absolute error: 2.281638572563866E-28
Root relative squared error: 9.217087868184629E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 869.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.140819286281933E-30
Coverage of cases: 100.0
Instances selection time: 76.0
Test time: 84.0
Accumulative iteration time: 22443.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 793.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.659057154007183E-28
Relative absolute error: 1.3145700638798124E-27
Root relative squared error: 5.318114308014366E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 768.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.572850319399062E-30
Coverage of cases: 100.0
Instances selection time: 75.0
Test time: 81.0
Accumulative iteration time: 23211.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 693.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0058215685019049E-27
Relative absolute error: 4.980205717961717E-27
Root relative squared error: 2.01164313700381E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 696.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.4901028589808584E-29
Coverage of cases: 100.0
Instances selection time: 74.0
Test time: 89.0
Accumulative iteration time: 23907.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 622.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.331779280825825E-27
Relative absolute error: 1.64759056714226E-26
Root relative squared error: 6.66355856165165E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 667.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.2379528357113E-29
Coverage of cases: 100.0
Instances selection time: 74.0
Test time: 82.0
Accumulative iteration time: 24574.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 593.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.5389287706404834E-26
Relative absolute error: 7.615983550712528E-26
Root relative squared error: 3.077857541280967E-24
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 644.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.807991775356264E-28
Coverage of cases: 100.0
Instances selection time: 72.0
Test time: 75.0
Accumulative iteration time: 25218.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 572.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.624464884429133E-26
Relative absolute error: 2.7826053065388483E-25
Root relative squared error: 1.1248929768858266E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 678.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3913026532694242E-27
Coverage of cases: 100.0
Instances selection time: 73.0
Test time: 75.0
Accumulative iteration time: 25896.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 605.0
		
Time end:Thu Nov 30 21.32.05 EET 2017