Thu Nov 30 21.41.55 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 21.41.55 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9996000291666786
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 347.0
Weighted AreaUnderPRC: 0.9992591181695447
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 62.0
Test time: 83.0
Accumulative iteration time: 347.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 285.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 396.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 59.0
Test time: 81.0
Accumulative iteration time: 743.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 337.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 408.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 60.0
Test time: 80.0
Accumulative iteration time: 1151.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 348.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 361.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 53.0
Test time: 83.0
Accumulative iteration time: 1512.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 308.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9996030713661982
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 445.0
Weighted AreaUnderPRC: 0.9992623734641826
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 55.0
Test time: 81.0
Accumulative iteration time: 1957.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 390.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9996029543585243
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 376.0
Weighted AreaUnderPRC: 0.9992622477317281
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 60.0
Test time: 84.0
Accumulative iteration time: 2333.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 316.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9996029543585243
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 398.0
Weighted AreaUnderPRC: 0.9992622477317281
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 55.0
Test time: 83.0
Accumulative iteration time: 2731.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 343.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 501.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 58.0
Test time: 83.0
Accumulative iteration time: 3232.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 443.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 589.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 57.0
Test time: 80.0
Accumulative iteration time: 3821.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 532.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 444.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 56.0
Test time: 86.0
Accumulative iteration time: 4265.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 388.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999603188373872
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 418.0
Weighted AreaUnderPRC: 0.9992624991324549
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 58.0
Test time: 80.0
Accumulative iteration time: 4683.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 360.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.593905504887563E-80
Relative absolute error: 1.4830143587038758E-79
Root relative squared error: 1.3187811009775127E-77
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 493.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.415071793519379E-82
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 84.0
Accumulative iteration time: 5176.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 436.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.314879435269952E-77
Relative absolute error: 2.9176488457433167E-77
Root relative squared error: 2.629758870539904E-75
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 483.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4588244228716582E-79
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 85.0
Accumulative iteration time: 5659.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 432.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.359942705937426E-76
Relative absolute error: 5.236582147920509E-76
Root relative squared error: 4.719885411874852E-74
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 449.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.6182910739602545E-78
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 81.0
Accumulative iteration time: 6108.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 392.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.2190775682561925E-41
Relative absolute error: 5.14635393575173E-41
Root relative squared error: 4.438155136512385E-39
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 513.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.573176967875865E-43
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 80.0
Accumulative iteration time: 6621.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 458.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.2506427962249707E-80
Relative absolute error: 2.7880768756242103E-80
Root relative squared error: 2.5012855924499417E-78
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 522.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.394038437812105E-82
Coverage of cases: 100.0
Instances selection time: 54.0
Test time: 80.0
Accumulative iteration time: 7143.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 468.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.2663655747358488E-40
Relative absolute error: 5.312592369113611E-40
Root relative squared error: 4.5327311494716975E-38
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 554.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.6562961845568057E-42
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 84.0
Accumulative iteration time: 7697.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 497.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.667015976048619E-43
Relative absolute error: 1.6170527801417357E-42
Root relative squared error: 7.334031952097238E-41
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 525.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.085263900708678E-45
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 80.0
Accumulative iteration time: 8222.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 473.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.86060145213694E-42
Relative absolute error: 8.14670475117407E-42
Root relative squared error: 3.72120290427388E-40
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 558.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.073352375587035E-44
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 82.0
Accumulative iteration time: 8780.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 505.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3004407051597684E-41
Relative absolute error: 5.745954747879064E-41
Root relative squared error: 2.6008814103195366E-39
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 543.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.872977373939532E-43
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 85.0
Accumulative iteration time: 9323.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 492.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.74452756677586E-41
Relative absolute error: 4.307790277480741E-40
Root relative squared error: 1.9489055133551718E-38
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 697.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.1538951387403703E-42
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 80.0
Accumulative iteration time: 10020.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 647.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.388733787645673E-39
Relative absolute error: 6.052301870641266E-39
Root relative squared error: 2.777467575291346E-37
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 531.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0261509353206333E-41
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 83.0
Accumulative iteration time: 10551.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 473.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.888202315113863E-39
Relative absolute error: 3.033548590298912E-38
Root relative squared error: 1.3776404630227727E-36
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 554.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.516774295149456E-40
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 80.0
Accumulative iteration time: 11105.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 503.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.2599375386575256E-38
Relative absolute error: 1.8770954032400979E-37
Root relative squared error: 8.519875077315052E-36
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 516.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.38547701620049E-40
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 79.0
Accumulative iteration time: 11621.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 463.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.713128656650736E-37
Relative absolute error: 1.6326106474334606E-36
Root relative squared error: 7.426257313301472E-35
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 490.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.163053237167304E-39
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 80.0
Accumulative iteration time: 12111.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 440.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.08064468659578E-36
Relative absolute error: 8.971675829694518E-36
Root relative squared error: 4.16128937319156E-34
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 538.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.4858379148472587E-38
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 78.0
Accumulative iteration time: 12649.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 494.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.1986961686712755E-36
Relative absolute error: 2.2740058302068291E-35
Root relative squared error: 1.0397392337342551E-33
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 634.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1370029151034145E-37
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 79.0
Accumulative iteration time: 13283.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 588.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.2232130981095875E-35
Relative absolute error: 9.664948062451407E-35
Root relative squared error: 4.446426196219175E-33
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 605.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.8324740312257035E-37
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 76.0
Accumulative iteration time: 13888.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 561.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4660287653798157E-34
Relative absolute error: 6.451604098216354E-34
Root relative squared error: 2.9320575307596314E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 682.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.225802049108177E-36
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 82.0
Accumulative iteration time: 14570.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 639.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.828840418187247E-34
Relative absolute error: 3.426770621449598E-33
Root relative squared error: 1.5657680836374495E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 683.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.7133853107247988E-35
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 86.0
Accumulative iteration time: 15253.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 638.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.849986376021777E-33
Relative absolute error: 2.1418614678563118E-32
Root relative squared error: 9.699972752043554E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 693.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0709307339281558E-34
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 77.0
Accumulative iteration time: 15946.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 647.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.621043578060536E-32
Relative absolute error: 1.158328492161159E-31
Root relative squared error: 5.242087156121072E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 683.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.791642460805796E-34
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 78.0
Accumulative iteration time: 16629.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 633.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.216074007379572E-32
Relative absolute error: 4.07232913756716E-31
Root relative squared error: 1.8432148014759145E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 608.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.0361645687835796E-33
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 84.0
Accumulative iteration time: 17237.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 563.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.168239794509829E-34
Relative absolute error: 2.2402381736314375E-33
Root relative squared error: 8.336479589019658E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 628.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1201190868157188E-35
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 78.0
Accumulative iteration time: 17865.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 583.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6549105757025128E-33
Relative absolute error: 1.4384660906395797E-32
Root relative squared error: 5.309821151405026E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 627.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.192330453197899E-35
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 85.0
Accumulative iteration time: 18492.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 583.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.160937366231573E-33
Relative absolute error: 4.45739084648612E-32
Root relative squared error: 1.6321874732463145E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 658.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.22869542324306E-34
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 86.0
Accumulative iteration time: 19150.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 615.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5782642394441275E-32
Relative absolute error: 1.9438823624439319E-31
Root relative squared error: 7.156528478888255E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 772.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.719411812219659E-34
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 82.0
Accumulative iteration time: 19922.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 727.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.370923500041508E-31
Relative absolute error: 1.2932955727306338E-30
Root relative squared error: 4.741847000083016E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 661.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.466477863653169E-33
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 78.0
Accumulative iteration time: 20583.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 616.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4740896633855671E-30
Relative absolute error: 8.016712792312245E-30
Root relative squared error: 2.9481793267711344E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 715.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.008356396156122E-32
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 83.0
Accumulative iteration time: 21298.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 671.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.1459803362916704E-31
Relative absolute error: 1.7245351950732444E-30
Root relative squared error: 6.291960672583341E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 635.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.622675975366222E-33
Coverage of cases: 100.0
Instances selection time: 39.0
Test time: 82.0
Accumulative iteration time: 21933.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 596.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1951539417013876E-30
Relative absolute error: 6.527530387404893E-30
Root relative squared error: 2.3903078834027754E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 726.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2637651937024463E-32
Coverage of cases: 100.0
Instances selection time: 37.0
Test time: 86.0
Accumulative iteration time: 22659.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 689.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.5887082646492295E-30
Relative absolute error: 1.8964627972391672E-29
Root relative squared error: 7.177416529298459E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 724.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.482313986195836E-32
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 85.0
Accumulative iteration time: 23383.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 684.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.7335483199492633E-29
Relative absolute error: 9.25791812681364E-29
Root relative squared error: 3.4670966398985264E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 807.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.62895906340682E-31
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 82.0
Accumulative iteration time: 24190.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 766.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.186262360471214E-29
Relative absolute error: 2.771942580838737E-28
Root relative squared error: 1.0372524720942429E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 748.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3859712904193687E-30
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 83.0
Accumulative iteration time: 24938.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 705.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.112080653193612E-28
Relative absolute error: 2.219072102348764E-27
Root relative squared error: 8.224161306387225E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 736.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.109536051174382E-29
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 85.0
Accumulative iteration time: 25674.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 696.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.99726248356231E-28
Relative absolute error: 4.864698763683714E-27
Root relative squared error: 1.7994524967124618E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 693.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.432349381841857E-29
Coverage of cases: 100.0
Instances selection time: 39.0
Test time: 80.0
Accumulative iteration time: 26367.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 654.0
		
Time end:Thu Nov 30 21.42.34 EET 2017