Thu Nov 30 21.37.07 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 21.37.07 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 536.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 65.0
Test time: 88.0
Accumulative iteration time: 536.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 471.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.999270301541822
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 397.0
Weighted AreaUnderPRC: 0.9990214274497757
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 58.0
Test time: 85.0
Accumulative iteration time: 933.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 339.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 516.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 59.0
Test time: 84.0
Accumulative iteration time: 1449.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 457.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992706525648435
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 588.0
Weighted AreaUnderPRC: 0.9990215988670504
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 81.0
Test time: 93.0
Accumulative iteration time: 2037.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 507.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992706525648435
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 482.0
Weighted AreaUnderPRC: 0.9990215988670504
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 56.0
Test time: 84.0
Accumulative iteration time: 2519.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 426.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992700675264744
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 543.0
Weighted AreaUnderPRC: 0.9990213133485584
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 53.0
Test time: 85.0
Accumulative iteration time: 3062.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 490.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.07385524372230429
Correctly Classified Instances: 99.9261447562777
Weighted Precision: 0.9992624991324549
Weighted AreaUnderROC: 0.9992694824881052
Root mean squared error: 0.0271763212599322
Relative absolute error: 0.14771048744460857
Root relative squared error: 5.43526425198644
Weighted TruePositiveRate: 0.9992614475627769
Weighted MatthewsCorrelation: 0.9985219980135681
Weighted FMeasure: 0.999261427527573
Iteration time: 536.0
Weighted AreaUnderPRC: 0.9990210281837473
Mean absolute error: 7.385524372230429E-4
Coverage of cases: 99.9261447562777
Instances selection time: 59.0
Test time: 85.0
Accumulative iteration time: 3598.0
Weighted Recall: 0.9992614475627769
Weighted FalsePositiveRate: 7.936232522560174E-4
Kappa statistic: 0.9985209057698268
Training time: 477.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.978008658348558E-46
Relative absolute error: 1.0604422311370287E-45
Root relative squared error: 5.956017316697116E-44
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 475.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.302211155685143E-48
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 81.0
Accumulative iteration time: 4073.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 420.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.534623855627719E-34
Relative absolute error: 1.3104400727750352E-33
Root relative squared error: 9.069247711255439E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 588.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.552200363875176E-36
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 84.0
Accumulative iteration time: 4661.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 531.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.4525423085005985E-31
Relative absolute error: 9.507994231583657E-31
Root relative squared error: 6.905084617001197E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 573.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.753997115791828E-33
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 84.0
Accumulative iteration time: 5234.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 517.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0878515324516624E-28
Relative absolute error: 3.3102706575273164E-28
Root relative squared error: 2.1757030649033248E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 582.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.6551353287636582E-30
Coverage of cases: 100.0
Instances selection time: 61.0
Test time: 83.0
Accumulative iteration time: 5816.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 521.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.4762345750321363E-25
Relative absolute error: 7.522725645351705E-25
Root relative squared error: 4.9524691500642724E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 729.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.7613628226758526E-27
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 82.0
Accumulative iteration time: 6545.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 674.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.420802951543185E-25
Relative absolute error: 2.4829455072253876E-24
Root relative squared error: 1.884160590308637E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 585.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2414727536126938E-26
Coverage of cases: 100.0
Instances selection time: 59.0
Test time: 85.0
Accumulative iteration time: 7130.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 526.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.7256804682116004E-74
Relative absolute error: 3.837400632949202E-74
Root relative squared error: 3.4513609364232006E-72
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1002.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.918700316474601E-76
Coverage of cases: 100.0
Instances selection time: 85.0
Test time: 129.0
Accumulative iteration time: 8132.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 917.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.440155167304583E-73
Relative absolute error: 1.0387826724676042E-72
Root relative squared error: 4.880310334609166E-71
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 685.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.193913362338021E-75
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 83.0
Accumulative iteration time: 8817.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 629.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3253995832877134E-71
Relative absolute error: 5.702566792875714E-71
Root relative squared error: 2.6507991665754267E-69
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 738.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.851283396437857E-73
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 88.0
Accumulative iteration time: 9555.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 682.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.931010252324095E-70
Relative absolute error: 8.294958368541683E-70
Root relative squared error: 3.86202050464819E-68
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 677.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.1474791842708415E-72
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 78.0
Accumulative iteration time: 10232.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 619.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.149721039554472E-69
Relative absolute error: 3.0291329107654443E-68
Root relative squared error: 1.4299442079108943E-66
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 658.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.514566455382722E-70
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 84.0
Accumulative iteration time: 10890.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 606.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.8897438585055556E-67
Relative absolute error: 1.9301515467573042E-66
Root relative squared error: 9.779487717011112E-65
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 783.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.65075773378652E-69
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 80.0
Accumulative iteration time: 11673.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 730.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.6737493344385526E-65
Relative absolute error: 7.250378087920168E-65
Root relative squared error: 3.347498668877105E-63
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 773.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.625189043960084E-67
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 85.0
Accumulative iteration time: 12446.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 722.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.36045748312476E-64
Relative absolute error: 2.9647541621986937E-63
Root relative squared error: 1.872091496624952E-61
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 739.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.482377081099347E-65
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 89.0
Accumulative iteration time: 13185.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 692.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.39231007063154E-32
Relative absolute error: 4.7125902829088817E-32
Root relative squared error: 2.78462014126308E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 761.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.356295141454441E-34
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 87.0
Accumulative iteration time: 13946.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 711.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6620106634733936E-31
Relative absolute error: 1.0658233268624109E-30
Root relative squared error: 5.324021326946787E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 710.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.329116634312054E-33
Coverage of cases: 100.0
Instances selection time: 49.0
Test time: 91.0
Accumulative iteration time: 14656.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 661.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.638890671349416E-31
Relative absolute error: 4.07222502947577E-30
Root relative squared error: 1.9277781342698832E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 645.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.0361125147378848E-32
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 86.0
Accumulative iteration time: 15301.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 598.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.839966507837944E-30
Relative absolute error: 1.911528604822733E-29
Root relative squared error: 9.679933015675888E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 606.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.557643024113666E-32
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 86.0
Accumulative iteration time: 15907.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 560.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.3811742239816015E-29
Relative absolute error: 9.272006677479127E-29
Root relative squared error: 4.762348447963203E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 545.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.636003338739564E-31
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 86.0
Accumulative iteration time: 16452.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 500.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.235295916224072E-29
Relative absolute error: 2.60410996765515E-28
Root relative squared error: 1.4470591832448145E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 666.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.302054983827575E-30
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 84.0
Accumulative iteration time: 17118.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 618.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.625812796059883E-28
Relative absolute error: 2.561850753692335E-27
Root relative squared error: 1.3251625592119767E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 774.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2809253768461675E-29
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 117.0
Accumulative iteration time: 17892.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 726.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.5807204680697706E-27
Relative absolute error: 9.912288045948062E-27
Root relative squared error: 5.161440936139541E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 802.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.956144022974031E-29
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 90.0
Accumulative iteration time: 18694.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 754.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4268728933878403E-26
Relative absolute error: 6.210777708695829E-26
Root relative squared error: 2.8537457867756806E-24
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 726.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.105388854347914E-28
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 82.0
Accumulative iteration time: 19420.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 675.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.7566287496237E-26
Relative absolute error: 2.7780822011942027E-25
Root relative squared error: 1.35132574992474E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 886.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3890411005971013E-27
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 87.0
Accumulative iteration time: 20306.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 840.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0345170731811667E-24
Relative absolute error: 4.487244110931836E-24
Root relative squared error: 2.0690341463623334E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 821.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.243622055465918E-26
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 87.0
Accumulative iteration time: 21127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 775.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.018582927764207E-24
Relative absolute error: 1.6789209503531374E-23
Root relative squared error: 8.037165855528414E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 758.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.394604751765687E-26
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 79.0
Accumulative iteration time: 21885.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 713.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.87476491776896E-23
Relative absolute error: 1.6176668182626348E-22
Root relative squared error: 7.74952983553792E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1215.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.088334091313174E-25
Coverage of cases: 100.0
Instances selection time: 70.0
Test time: 91.0
Accumulative iteration time: 23100.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1145.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.269760259815728E-22
Relative absolute error: 4.988117476710394E-22
Root relative squared error: 2.5395205196314557E-20
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 662.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.494058738355197E-24
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 87.0
Accumulative iteration time: 23762.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 618.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.2736109279954415E-21
Relative absolute error: 5.316825387314205E-21
Root relative squared error: 2.547221855990883E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 887.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.6584126936571027E-23
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 79.0
Accumulative iteration time: 24649.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 843.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.283547425902195E-21
Relative absolute error: 2.9258988538706427E-20
Root relative squared error: 1.4567094851804391E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1009.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4629494269353214E-22
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 87.0
Accumulative iteration time: 25658.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 968.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.7849029267686074E-20
Relative absolute error: 1.114040739705344E-19
Root relative squared error: 5.569805853537215E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 694.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.570203698526721E-22
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 80.0
Accumulative iteration time: 26352.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 651.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.5940692505547087E-19
Relative absolute error: 6.101779706975954E-19
Root relative squared error: 3.1881385011094173E-17
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 784.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.0508898534879768E-21
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 88.0
Accumulative iteration time: 27136.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 738.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3103933281558258E-18
Relative absolute error: 5.242919322123264E-18
Root relative squared error: 2.6207866563116515E-16
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 823.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.621459661061632E-20
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 85.0
Accumulative iteration time: 27959.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 783.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4625012229371576E-17
Relative absolute error: 8.186217456770001E-17
Root relative squared error: 2.9250024458743154E-15
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 862.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.093108728385001E-19
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 79.0
Accumulative iteration time: 28821.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 822.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.747359655990923E-34
Relative absolute error: 3.0375296726796267E-32
Root relative squared error: 1.7494719311981846E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 740.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.5187648363398132E-34
Coverage of cases: 100.0
Instances selection time: 98.0
Test time: 157.0
Accumulative iteration time: 29561.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 642.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.730958467101087E-33
Relative absolute error: 9.109776070509605E-32
Root relative squared error: 5.461916934202174E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 699.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.554888035254803E-34
Coverage of cases: 100.0
Instances selection time: 35.0
Test time: 83.0
Accumulative iteration time: 30260.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 664.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.627537060594826E-33
Relative absolute error: 2.2751959405848004E-31
Root relative squared error: 1.525507412118965E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 748.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1375979702924002E-33
Coverage of cases: 100.0
Instances selection time: 38.0
Test time: 84.0
Accumulative iteration time: 31008.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 710.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6653340720881546E-32
Relative absolute error: 9.156712422279523E-31
Root relative squared error: 5.330668144176309E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 774.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.578356211139762E-33
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 79.0
Accumulative iteration time: 31782.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 732.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.633340953626805E-32
Relative absolute error: 2.2994529009756447E-30
Root relative squared error: 1.326668190725361E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 751.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1497264504878225E-32
Coverage of cases: 100.0
Instances selection time: 33.0
Test time: 88.0
Accumulative iteration time: 32533.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 718.0
		
Time end:Thu Nov 30 21.37.53 EET 2017