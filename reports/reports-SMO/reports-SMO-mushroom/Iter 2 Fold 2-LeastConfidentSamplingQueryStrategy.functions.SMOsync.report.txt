Thu Nov 30 21.37.53 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 21.37.53 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9990738721903537
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.34465780403741997
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 398.0
Weighted AreaUnderPRC: 0.9982821741358502
Mean absolute error: 0.0017232890201870998
Coverage of cases: 99.8276710979813
Instances selection time: 57.0
Test time: 87.0
Accumulative iteration time: 398.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 341.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.17232890201870998
Correctly Classified Instances: 99.8276710979813
Weighted Precision: 0.9982824253441622
Weighted AreaUnderROC: 0.9990737551826798
Root mean squared error: 0.041512516428025656
Relative absolute error: 0.3446578040374202
Root relative squared error: 8.30250328560513
Weighted TruePositiveRate: 0.9982767109798129
Weighted MatthewsCorrelation: 0.9965544712751208
Weighted FMeasure: 0.9982765989627084
Iteration time: 448.0
Weighted AreaUnderPRC: 0.9982820487238474
Mean absolute error: 0.001723289020187101
Coverage of cases: 99.8276710979813
Instances selection time: 59.0
Test time: 85.0
Accumulative iteration time: 846.0
Weighted Recall: 0.9982767109798129
Weighted FalsePositiveRate: 0.001851787588597374
Kappa statistic: 0.9965485354763794
Training time: 389.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.01230920728705
Incorrectly Classified Instances: 0.024618414574101428
Correctly Classified Instances: 99.9753815854259
Weighted Precision: 0.9997539328063473
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.013595346079516636
Relative absolute error: 0.04266291885979656
Root relative squared error: 2.719069215903327
Weighted TruePositiveRate: 0.9997538158542589
Weighted MatthewsCorrelation: 0.9995071075347904
Weighted FMeasure: 0.9997538136580851
Iteration time: 440.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.133145942989828E-4
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 84.0
Accumulative iteration time: 1286.0
Weighted Recall: 0.9997538158542589
Weighted FalsePositiveRate: 2.6454108408533914E-4
Kappa statistic: 0.9995069860633139
Training time: 383.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.3390953948935973E-26
Relative absolute error: 2.9717049149531285E-26
Root relative squared error: 2.6781907897871946E-24
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 483.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4858524574765644E-28
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 84.0
Accumulative iteration time: 1769.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 425.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.260667938046165E-34
Relative absolute error: 5.01879874407033E-34
Root relative squared error: 4.52133587609233E-32
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 364.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.509399372035165E-36
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 82.0
Accumulative iteration time: 2133.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 311.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.6840940024924048E-43
Relative absolute error: 3.740906399204654E-43
Root relative squared error: 3.3681880049848093E-41
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 449.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8704531996023272E-45
Coverage of cases: 100.0
Instances selection time: 62.0
Test time: 80.0
Accumulative iteration time: 2582.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 387.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.2525358029183907E-48
Relative absolute error: 1.1667354012340996E-47
Root relative squared error: 1.0505071605836781E-45
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 350.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.833677006170498E-50
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 77.0
Accumulative iteration time: 2932.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 294.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.5753878439943306E-50
Relative absolute error: 5.956592012468509E-50
Root relative squared error: 5.150775687988661E-48
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 599.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.978296006234254E-52
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 123.0
Accumulative iteration time: 3531.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 544.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.681627870642949E-52
Relative absolute error: 8.182981923119364E-52
Root relative squared error: 7.3632557412858985E-50
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 386.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.091490961559682E-54
Coverage of cases: 100.0
Instances selection time: 59.0
Test time: 86.0
Accumulative iteration time: 3917.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 327.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.376920528816914E-57
Relative absolute error: 1.8633840535031076E-56
Root relative squared error: 1.675384105763383E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 424.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.316920267515539E-59
Coverage of cases: 100.0
Instances selection time: 59.0
Test time: 85.0
Accumulative iteration time: 4341.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 365.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0838198483347822E-62
Relative absolute error: 2.40498618395671E-62
Root relative squared error: 2.1676396966695643E-60
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 407.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.202493091978355E-64
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 79.0
Accumulative iteration time: 4748.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 352.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8984685570370603E-70
Relative absolute error: 8.662313677148494E-70
Root relative squared error: 7.79693711407412E-68
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 452.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3311568385742473E-72
Coverage of cases: 100.0
Instances selection time: 60.0
Test time: 81.0
Accumulative iteration time: 5200.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 392.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.363351274731694E-74
Relative absolute error: 1.413859708488758E-73
Root relative squared error: 1.2726702549463388E-71
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.06929854244379E-76
Coverage of cases: 100.0
Instances selection time: 110.0
Test time: 79.0
Accumulative iteration time: 6042.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 732.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.424200713681039E-76
Relative absolute error: 8.991558146475752E-76
Root relative squared error: 6.848401427362077E-74
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 466.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.495779073237876E-78
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 82.0
Accumulative iteration time: 6508.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 415.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.769842213993359E-76
Relative absolute error: 6.164526823346131E-76
Root relative squared error: 5.5396844279867174E-74
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 531.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.082263411673066E-78
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 82.0
Accumulative iteration time: 7039.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 478.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.7633159360733344E-77
Relative absolute error: 6.959820674832607E-77
Root relative squared error: 5.526631872146669E-75
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 786.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.4799103374163034E-79
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 80.0
Accumulative iteration time: 7825.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 736.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4719826913070792E-77
Relative absolute error: 4.442568418737921E-77
Root relative squared error: 2.9439653826141584E-75
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 472.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.2212842093689606E-79
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 80.0
Accumulative iteration time: 8297.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 416.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.43443393735521E-76
Relative absolute error: 1.9872829188837804E-75
Root relative squared error: 1.2868867874710421E-73
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 546.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.936414594418902E-78
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 79.0
Accumulative iteration time: 8843.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 490.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.770414075940528E-74
Relative absolute error: 5.8647443505221305E-74
Root relative squared error: 3.540828151881056E-72
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 502.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9323721752610653E-76
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 80.0
Accumulative iteration time: 9345.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 447.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.70719793656402E-73
Relative absolute error: 1.8449242094023978E-72
Root relative squared error: 1.1414395873128039E-70
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 545.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.224621047011989E-75
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 77.0
Accumulative iteration time: 9890.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 490.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1364865847161104E-71
Relative absolute error: 3.692842319297372E-71
Root relative squared error: 2.272973169432221E-69
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 511.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8464211596486858E-73
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 81.0
Accumulative iteration time: 10401.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 458.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.7984969085224703E-69
Relative absolute error: 1.0307387304563517E-68
Root relative squared error: 7.596993817044941E-67
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 569.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.153693652281758E-71
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 81.0
Accumulative iteration time: 10970.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 517.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.283727727569212E-68
Relative absolute error: 1.109964076005606E-67
Root relative squared error: 6.567455455138424E-66
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 540.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.54982038002803E-70
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 83.0
Accumulative iteration time: 11510.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 492.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.915543227726532E-66
Relative absolute error: 2.0972088077509912E-65
Root relative squared error: 1.5831086455453065E-63
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 617.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0486044038754956E-67
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 78.0
Accumulative iteration time: 12127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 562.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.447554568941546E-65
Relative absolute error: 1.0038707269143436E-64
Root relative squared error: 6.895109137883092E-63
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 574.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.019353634571718E-67
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 81.0
Accumulative iteration time: 12701.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 523.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.74165183272609E-61
Relative absolute error: 2.1833865424044643E-60
Root relative squared error: 1.9483303665452182E-58
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 526.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0916932712022322E-62
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 79.0
Accumulative iteration time: 13227.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 480.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4627910353271976E-59
Relative absolute error: 3.3134483181959964E-59
Root relative squared error: 2.925582070654395E-57
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 519.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.6567241590979982E-61
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 79.0
Accumulative iteration time: 13746.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 477.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.411698847802471E-58
Relative absolute error: 1.4500711391041335E-57
Root relative squared error: 1.2823397695604943E-55
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 604.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.250355695520667E-60
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 78.0
Accumulative iteration time: 14350.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 558.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1520109279835996E-56
Relative absolute error: 2.59170523214834E-56
Root relative squared error: 2.3040218559671993E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 673.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2958526160741702E-58
Coverage of cases: 100.0
Instances selection time: 49.0
Test time: 76.0
Accumulative iteration time: 15023.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 624.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.924196717834095E-57
Relative absolute error: 1.1132038906766332E-56
Root relative squared error: 9.84839343566819E-55
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 561.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.566019453383166E-59
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 79.0
Accumulative iteration time: 15584.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 516.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.584269525598422E-56
Relative absolute error: 1.2563820079106126E-55
Root relative squared error: 1.1168539051196844E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 600.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.281910039553063E-58
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 77.0
Accumulative iteration time: 16184.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 553.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.841222218062366E-54
Relative absolute error: 6.39466864433431E-54
Root relative squared error: 5.682444436124732E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 558.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1973343221671547E-56
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 81.0
Accumulative iteration time: 16742.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 508.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.37289135747121E-53
Relative absolute error: 5.312022979164414E-53
Root relative squared error: 4.7457827149424193E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 663.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.656011489582207E-55
Coverage of cases: 100.0
Instances selection time: 40.0
Test time: 79.0
Accumulative iteration time: 17405.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 623.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.096378590736379E-52
Relative absolute error: 1.1473032418160137E-51
Root relative squared error: 1.0192757181472758E-49
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 787.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.736516209080068E-54
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 81.0
Accumulative iteration time: 18192.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 741.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.625402242319793E-51
Relative absolute error: 2.161219217017E-50
Root relative squared error: 1.9250804484639586E-48
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 779.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0806096085085E-52
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 82.0
Accumulative iteration time: 18971.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 737.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.374082488376632E-49
Relative absolute error: 5.354226620839946E-49
Root relative squared error: 4.748164976753264E-47
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 913.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.677113310419973E-51
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 85.0
Accumulative iteration time: 19884.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 872.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.476329670308698E-45
Relative absolute error: 5.670909731777877E-45
Root relative squared error: 2.9526593406173958E-43
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.835454865888938E-47
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 81.0
Accumulative iteration time: 20726.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 800.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.682511462603325E-44
Relative absolute error: 6.20438267415351E-44
Root relative squared error: 3.36502292520665E-42
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 877.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.102191337076755E-46
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 81.0
Accumulative iteration time: 21603.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 833.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.73781252939541E-43
Relative absolute error: 3.72776087193804E-42
Root relative squared error: 1.947562505879082E-40
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 795.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.86388043596902E-44
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 79.0
Accumulative iteration time: 22398.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 750.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.0725075254055214E-41
Relative absolute error: 7.926463312411443E-41
Root relative squared error: 4.145015050811043E-39
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 703.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.963231656205722E-43
Coverage of cases: 100.0
Instances selection time: 39.0
Test time: 88.0
Accumulative iteration time: 23101.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 664.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.6002307401033447E-21
Relative absolute error: 6.1312152751315276E-21
Root relative squared error: 3.2004614802066893E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 719.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.065607637565764E-23
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 84.0
Accumulative iteration time: 23820.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 676.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.142937488894904E-22
Relative absolute error: 1.2074251990363108E-21
Root relative squared error: 6.285874977789808E-20
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 842.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.037125995181554E-24
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 83.0
Accumulative iteration time: 24662.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 801.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.504031930706135E-21
Relative absolute error: 5.764895091178444E-21
Root relative squared error: 3.00806386141227E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 804.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.882447545589222E-23
Coverage of cases: 100.0
Instances selection time: 54.0
Test time: 84.0
Accumulative iteration time: 25466.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 750.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.810917840146148E-21
Relative absolute error: 3.768525371742772E-20
Root relative squared error: 1.9621835680292296E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 811.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.884262685871386E-22
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 80.0
Accumulative iteration time: 26277.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 769.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.578145646149987E-20
Relative absolute error: 1.3674168249354478E-19
Root relative squared error: 7.156291292299974E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 979.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.837084124677239E-22
Coverage of cases: 100.0
Instances selection time: 38.0
Test time: 84.0
Accumulative iteration time: 27256.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 941.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.479578453856438E-19
Relative absolute error: 5.629396667705127E-19
Root relative squared error: 2.959156907712876E-17
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 791.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.8146983338525635E-21
Coverage of cases: 100.0
Instances selection time: 38.0
Test time: 85.0
Accumulative iteration time: 28047.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 753.0
		
Time end:Thu Nov 30 21.38.35 EET 2017