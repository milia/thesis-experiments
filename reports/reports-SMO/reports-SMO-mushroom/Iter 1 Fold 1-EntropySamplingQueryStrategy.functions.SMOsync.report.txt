Thu Nov 30 21.19.19 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Thu Nov 30 21.19.19 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.2055051624760406E-8
Relative absolute error: 1.3199981977615942E-7
Root relative squared error: 8.411010324952081E-6
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2771.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.599990988807971E-10
Coverage of cases: 100.0
Instances selection time: 566.0
Test time: 785.0
Accumulative iteration time: 2771.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 2205.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.057992977133252E-89
Relative absolute error: 1.7476819917983967E-88
Root relative squared error: 1.4115985954266504E-86
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 2282.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.738409958991984E-91
Coverage of cases: 100.0
Instances selection time: 155.0
Test time: 260.0
Accumulative iteration time: 5053.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 2127.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.507417354805467E-101
Relative absolute error: 2.998671637178581E-99
Root relative squared error: 1.5014834709610935E-98
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 810.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4993358185892905E-101
Coverage of cases: 100.0
Instances selection time: 146.0
Test time: 254.0
Accumulative iteration time: 5863.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 664.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.820033853931542E-102
Relative absolute error: 4.5204486095632423E-100
Root relative squared error: 1.3640067707863085E-99
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 947.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.2602243047816212E-102
Coverage of cases: 100.0
Instances selection time: 108.0
Test time: 197.0
Accumulative iteration time: 6810.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 839.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.320654914049313E-102
Relative absolute error: 1.8279906311662993E-100
Root relative squared error: 6.641309828098627E-100
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 757.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.139953155831496E-103
Coverage of cases: 100.0
Instances selection time: 110.0
Test time: 191.0
Accumulative iteration time: 7567.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 647.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.4178679140814946E-52
Relative absolute error: 3.0467755927023735E-50
Root relative squared error: 8.83573582816299E-50
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 727.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.5233877963511868E-52
Coverage of cases: 100.0
Instances selection time: 123.0
Test time: 200.0
Accumulative iteration time: 8294.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 604.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.372543570270761E-52
Relative absolute error: 1.6208383148669947E-50
Root relative squared error: 4.745087140541522E-50
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 759.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.104191574334974E-53
Coverage of cases: 100.0
Instances selection time: 124.0
Test time: 188.0
Accumulative iteration time: 9053.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 635.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1049320250643075E-52
Relative absolute error: 7.28929079416235E-51
Root relative squared error: 2.209864050128615E-50
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 751.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.6446453970811748E-53
Coverage of cases: 100.0
Instances selection time: 95.0
Test time: 185.0
Accumulative iteration time: 9804.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 656.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.270445943501076E-53
Relative absolute error: 4.8867923839227704E-51
Root relative squared error: 1.4540891887002152E-50
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 620.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.443396191961385E-53
Coverage of cases: 100.0
Instances selection time: 99.0
Test time: 159.0
Accumulative iteration time: 10424.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 521.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.0420935495979226E-53
Relative absolute error: 2.660494253027734E-51
Root relative squared error: 8.084187099195845E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 657.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.330247126513867E-53
Coverage of cases: 100.0
Instances selection time: 89.0
Test time: 139.0
Accumulative iteration time: 11081.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 568.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.7215363628845005E-53
Relative absolute error: 1.8480124901943874E-51
Root relative squared error: 5.443072725769001E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 697.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.240062450971937E-54
Coverage of cases: 100.0
Instances selection time: 76.0
Test time: 147.0
Accumulative iteration time: 11778.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 621.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4413503331860996E-53
Relative absolute error: 9.608789776799044E-52
Root relative squared error: 2.882700666372199E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 703.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.804394888399522E-54
Coverage of cases: 100.0
Instances selection time: 89.0
Test time: 143.0
Accumulative iteration time: 12481.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 614.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.34153154828435E-54
Relative absolute error: 6.32300899017026E-52
Root relative squared error: 1.86830630965687E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 579.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1615044950851303E-54
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 145.0
Accumulative iteration time: 13060.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 521.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.364711454300353E-54
Relative absolute error: 4.134773746244678E-52
Root relative squared error: 1.2729422908600707E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 839.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.0673868731223393E-54
Coverage of cases: 100.0
Instances selection time: 97.0
Test time: 145.0
Accumulative iteration time: 13899.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 742.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.239898334629307E-54
Relative absolute error: 2.8268577200394883E-52
Root relative squared error: 8.479796669258614E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 740.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.413428860019744E-54
Coverage of cases: 100.0
Instances selection time: 87.0
Test time: 143.0
Accumulative iteration time: 14639.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 653.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.9317347262562345E-54
Relative absolute error: 1.9230020587231846E-52
Root relative squared error: 5.863469452512469E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 700.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.615010293615923E-55
Coverage of cases: 100.0
Instances selection time: 64.0
Test time: 120.0
Accumulative iteration time: 15339.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 636.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.7804833612604056E-54
Relative absolute error: 1.1888570227906091E-52
Root relative squared error: 3.560966722520811E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 650.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.944285113953046E-55
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 99.0
Accumulative iteration time: 15989.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 598.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.2223352569866277E-54
Relative absolute error: 8.194207383228001E-53
Root relative squared error: 2.4446705139732555E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 544.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.0971036916140004E-55
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 91.0
Accumulative iteration time: 16533.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 488.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.499770753318254E-55
Relative absolute error: 6.293285806421237E-53
Root relative squared error: 1.899954150663651E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 782.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.1466429032106185E-55
Coverage of cases: 100.0
Instances selection time: 81.0
Test time: 127.0
Accumulative iteration time: 17315.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 701.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.953671695101123E-55
Relative absolute error: 4.36812470443554E-53
Root relative squared error: 1.3907343390202245E-52
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 905.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.1840623522177697E-55
Coverage of cases: 100.0
Instances selection time: 79.0
Test time: 137.0
Accumulative iteration time: 18220.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 826.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.479695557250646E-55
Relative absolute error: 2.8476958954485976E-53
Root relative squared error: 8.959391114501292E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 996.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4238479477242987E-55
Coverage of cases: 100.0
Instances selection time: 81.0
Test time: 83.0
Accumulative iteration time: 19216.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 915.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.468746906853832E-55
Relative absolute error: 2.2379936128976725E-53
Root relative squared error: 6.937493813707664E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 754.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.1189968064488363E-55
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 89.0
Accumulative iteration time: 19970.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 704.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.5611241522398566E-55
Relative absolute error: 1.635821222523109E-53
Root relative squared error: 5.122248304479713E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 706.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.179106112615544E-56
Coverage of cases: 100.0
Instances selection time: 60.0
Test time: 90.0
Accumulative iteration time: 20676.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 646.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.8940624962357846E-55
Relative absolute error: 1.266310452654086E-53
Root relative squared error: 3.788124992471569E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 643.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.33155226327043E-56
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 84.0
Accumulative iteration time: 21319.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 590.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4281385641743958E-55
Relative absolute error: 9.291854435992657E-54
Root relative squared error: 2.8562771283487914E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 856.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.645927217996328E-56
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 91.0
Accumulative iteration time: 22175.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 804.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0509191367677116E-55
Relative absolute error: 6.799345825065564E-54
Root relative squared error: 2.101838273535423E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 729.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.3996729125327818E-56
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 90.0
Accumulative iteration time: 22904.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 677.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.589546322081878E-56
Relative absolute error: 5.467973643312462E-54
Root relative squared error: 1.7179092644163757E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 896.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.7339868216562306E-56
Coverage of cases: 100.0
Instances selection time: 49.0
Test time: 83.0
Accumulative iteration time: 23800.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 847.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.033496487696664E-56
Relative absolute error: 4.6870331450419214E-54
Root relative squared error: 1.4066992975393328E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.343516572520961E-56
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 125.0
Accumulative iteration time: 24680.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 828.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.357073696327501E-56
Relative absolute error: 3.5297214699423516E-54
Root relative squared error: 1.0714147392655002E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 858.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.7648607349711758E-56
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 127.0
Accumulative iteration time: 25538.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 800.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.369713868593965E-56
Relative absolute error: 2.824580759127464E-54
Root relative squared error: 8.739427737187931E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 894.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.412290379563732E-56
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 142.0
Accumulative iteration time: 26432.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 846.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.1766521175334395E-56
Relative absolute error: 2.0884025372154E-54
Root relative squared error: 6.353304235066879E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 805.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0442012686077E-56
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 92.0
Accumulative iteration time: 27237.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 759.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.728513947637642E-56
Relative absolute error: 1.8247647391745627E-54
Root relative squared error: 5.457027895275284E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1065.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.123823695872813E-57
Coverage of cases: 100.0
Instances selection time: 69.0
Test time: 87.0
Accumulative iteration time: 28302.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 996.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.2253631530724247E-56
Relative absolute error: 1.446394707272984E-54
Root relative squared error: 4.4507263061448496E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 978.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.23197353636492E-57
Coverage of cases: 100.0
Instances selection time: 49.0
Test time: 90.0
Accumulative iteration time: 29280.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 929.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.888676982364228E-56
Relative absolute error: 1.1827111089546404E-54
Root relative squared error: 3.777353964728456E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1340.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.913555544773202E-57
Coverage of cases: 100.0
Instances selection time: 69.0
Test time: 129.0
Accumulative iteration time: 30620.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1271.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.662601918009602E-56
Relative absolute error: 1.0963099793108637E-54
Root relative squared error: 3.3252038360192037E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1135.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.481549896554319E-57
Coverage of cases: 100.0
Instances selection time: 66.0
Test time: 126.0
Accumulative iteration time: 31755.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1069.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8750701895201506E-29
Relative absolute error: 2.6693764790645744E-27
Root relative squared error: 7.750140379040302E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1435.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3346882395322872E-29
Coverage of cases: 100.0
Instances selection time: 72.0
Test time: 127.0
Accumulative iteration time: 33190.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1363.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.647073899788916E-29
Relative absolute error: 2.50618521805837E-27
Root relative squared error: 7.294147799577833E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1210.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2530926090291849E-29
Coverage of cases: 100.0
Instances selection time: 68.0
Test time: 82.0
Accumulative iteration time: 34400.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1142.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.1990300030628973E-29
Relative absolute error: 2.1821610732114994E-27
Root relative squared error: 6.3980600061257946E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 975.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0910805366057497E-29
Coverage of cases: 100.0
Instances selection time: 65.0
Test time: 128.0
Accumulative iteration time: 35375.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 910.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.9229547108081313E-29
Relative absolute error: 1.9988438366445892E-27
Root relative squared error: 5.845909421616262E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 765.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.994219183222946E-30
Coverage of cases: 100.0
Instances selection time: 70.0
Test time: 131.0
Accumulative iteration time: 36140.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 695.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.004058940740264E-29
Relative absolute error: 2.01694189317812E-27
Root relative squared error: 6.008117881480529E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1321.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.00847094658906E-29
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 132.0
Accumulative iteration time: 37461.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 1274.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6665934058592224E-29
Relative absolute error: 1.820035282300819E-27
Root relative squared error: 5.3331868117184445E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 959.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.100176411504095E-30
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 113.0
Accumulative iteration time: 38420.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 901.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.799087622306592E-29
Relative absolute error: 1.88901124950783E-27
Root relative squared error: 5.5981752446131835E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 952.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.44505624753915E-30
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 104.0
Accumulative iteration time: 39372.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 895.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.559138346062726E-29
Relative absolute error: 1.7335516451968922E-27
Root relative squared error: 5.118276692125452E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 812.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.667758225984461E-30
Coverage of cases: 100.0
Instances selection time: 62.0
Test time: 92.0
Accumulative iteration time: 40184.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 750.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.4437858915695062E-29
Relative absolute error: 1.6749023542686144E-27
Root relative squared error: 4.8875717831390125E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 759.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.374511771343072E-30
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 90.0
Accumulative iteration time: 40943.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 715.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6444797776364257E-29
Relative absolute error: 1.774396777150224E-27
Root relative squared error: 5.288959555272851E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 843.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.871983885751121E-30
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 80.0
Accumulative iteration time: 41786.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 785.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.594412710712759E-29
Relative absolute error: 1.760456662840819E-27
Root relative squared error: 5.188825421425518E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 1008.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 8.802283314204095E-30
Coverage of cases: 100.0
Instances selection time: 62.0
Test time: 122.0
Accumulative iteration time: 42794.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 946.0
		
Time end:Thu Nov 30 21.20.15 EET 2017