Thu Nov 30 21.39.12 EET 2017
Dataset: mushroom
Test set size: 4062
Initial Labelled set size: 812
Initial Unlabelled set size: 3250
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 21.39.12 EET 2017
		
Iteration: 1
Labeled set size: 812
Unlabelled set size: 3250
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.114276882438776E-38
Relative absolute error: 1.1348259999807263E-37
Root relative squared error: 1.0228553764877551E-35
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 575.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.674129999903631E-40
Coverage of cases: 100.0
Instances selection time: 62.0
Test time: 124.0
Accumulative iteration time: 575.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 513.0
		
Iteration: 2
Labeled set size: 832
Unlabelled set size: 3230
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1905047225580678E-43
Relative absolute error: 2.641655420049764E-43
Root relative squared error: 2.3810094451161356E-41
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 446.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.320827710024882E-45
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 84.0
Accumulative iteration time: 1021.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 388.0
		
Iteration: 3
Labeled set size: 852
Unlabelled set size: 3210
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1042370726952079E-48
Relative absolute error: 2.450232891050421E-48
Root relative squared error: 2.208474145390416E-46
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 644.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2251164455252105E-50
Coverage of cases: 100.0
Instances selection time: 61.0
Test time: 84.0
Accumulative iteration time: 1665.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 583.0
		
Iteration: 4
Labeled set size: 872
Unlabelled set size: 3190
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 8.356071224495612E-54
Relative absolute error: 1.854159857473238E-53
Root relative squared error: 1.6712142448991224E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 342.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.27079928736619E-56
Coverage of cases: 100.0
Instances selection time: 60.0
Test time: 86.0
Accumulative iteration time: 2007.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 282.0
		
Iteration: 5
Labeled set size: 892
Unlabelled set size: 3170
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.7435118594475446E-61
Relative absolute error: 1.0525578780223989E-60
Root relative squared error: 9.487023718895089E-59
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 522.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.262789390111995E-63
Coverage of cases: 100.0
Instances selection time: 60.0
Test time: 87.0
Accumulative iteration time: 2529.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 462.0
		
Iteration: 6
Labeled set size: 912
Unlabelled set size: 3150
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.8809126819512231E-66
Relative absolute error: 4.1736370964958384E-66
Root relative squared error: 3.7618253639024466E-64
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 457.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.0868185482479193E-68
Coverage of cases: 100.0
Instances selection time: 60.0
Test time: 84.0
Accumulative iteration time: 2986.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 397.0
		
Iteration: 7
Labeled set size: 932
Unlabelled set size: 3130
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.726673589533267E-71
Relative absolute error: 2.671381146185123E-70
Root relative squared error: 1.9453347179066534E-68
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 513.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.3356905730925615E-72
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 86.0
Accumulative iteration time: 3499.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 456.0
		
Iteration: 8
Labeled set size: 952
Unlabelled set size: 3110
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.0522333494114262E-69
Relative absolute error: 4.5821066803098957E-69
Root relative squared error: 4.104466698822853E-67
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 480.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.2910533401549477E-71
Coverage of cases: 100.0
Instances selection time: 57.0
Test time: 85.0
Accumulative iteration time: 3979.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 423.0
		
Iteration: 9
Labeled set size: 972
Unlabelled set size: 3090
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.0699586425449626E-67
Relative absolute error: 4.61835076672033E-67
Root relative squared error: 4.139917285089925E-65
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 510.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.3091753833601647E-69
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 85.0
Accumulative iteration time: 4489.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 452.0
		
Iteration: 10
Labeled set size: 992
Unlabelled set size: 3070
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.955880752393384E-65
Relative absolute error: 6.590773321487082E-65
Root relative squared error: 5.911761504786768E-63
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 518.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.295386660743541E-67
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 83.0
Accumulative iteration time: 5007.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 462.0
		
Iteration: 11
Labeled set size: 1012
Unlabelled set size: 3050
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1137709283786877E-63
Relative absolute error: 2.4837926315147475E-63
Root relative squared error: 2.2275418567573756E-61
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 481.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2418963157573739E-65
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 82.0
Accumulative iteration time: 5488.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 423.0
		
Iteration: 12
Labeled set size: 1032
Unlabelled set size: 3030
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.5221226981517385E-61
Relative absolute error: 5.617214299512057E-61
Root relative squared error: 5.044245396303477E-59
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 517.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.808607149756029E-63
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 84.0
Accumulative iteration time: 6005.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 462.0
		
Iteration: 13
Labeled set size: 1052
Unlabelled set size: 3010
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.1656874933802696E-57
Relative absolute error: 4.814336004877433E-57
Root relative squared error: 4.3313749867605395E-55
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 478.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.4071680024387165E-59
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 88.0
Accumulative iteration time: 6483.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 422.0
		
Iteration: 14
Labeled set size: 1072
Unlabelled set size: 2990
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.633681581688267E-56
Relative absolute error: 5.888688271887261E-56
Root relative squared error: 5.267363163376534E-54
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 538.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.9443441359436306E-58
Coverage of cases: 100.0
Instances selection time: 54.0
Test time: 83.0
Accumulative iteration time: 7021.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 484.0
		
Iteration: 15
Labeled set size: 1092
Unlabelled set size: 2970
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.352345243643945E-56
Relative absolute error: 2.089958879908087E-55
Root relative squared error: 1.8704690487287888E-53
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 583.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.0449794399540434E-57
Coverage of cases: 100.0
Instances selection time: 56.0
Test time: 86.0
Accumulative iteration time: 7604.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 527.0
		
Iteration: 16
Labeled set size: 1112
Unlabelled set size: 2950
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.844090502715045E-54
Relative absolute error: 1.3046701459862564E-53
Root relative squared error: 1.168818100543009E-51
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 677.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.523350729931282E-56
Coverage of cases: 100.0
Instances selection time: 55.0
Test time: 83.0
Accumulative iteration time: 8281.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 622.0
		
Iteration: 17
Labeled set size: 1132
Unlabelled set size: 2930
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.367564575290152E-27
Relative absolute error: 1.1194120523394899E-26
Root relative squared error: 8.735129150580304E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 506.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.59706026169745E-29
Coverage of cases: 100.0
Instances selection time: 58.0
Test time: 78.0
Accumulative iteration time: 8787.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 448.0
		
Iteration: 18
Labeled set size: 1152
Unlabelled set size: 2910
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.0792050290770736E-26
Relative absolute error: 1.2994839443900903E-25
Root relative squared error: 1.0158410058154148E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 534.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.497419721950451E-28
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 83.0
Accumulative iteration time: 9321.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 483.0
		
Iteration: 19
Labeled set size: 1172
Unlabelled set size: 2890
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 5.168338230359701E-25
Relative absolute error: 1.31767121769262E-24
Root relative squared error: 1.0336676460719401E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 623.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.588356088463099E-27
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 80.0
Accumulative iteration time: 9944.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 571.0
		
Iteration: 20
Labeled set size: 1192
Unlabelled set size: 2870
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.124739493657772E-24
Relative absolute error: 1.590077701876551E-23
Root relative squared error: 1.2249478987315543E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 674.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 7.950388509382755E-26
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 80.0
Accumulative iteration time: 10618.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 621.0
		
Iteration: 21
Labeled set size: 1212
Unlabelled set size: 2850
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.8608082442004626E-23
Relative absolute error: 9.503886984973809E-23
Root relative squared error: 7.721616488400925E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 533.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.751943492486905E-25
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 86.0
Accumulative iteration time: 11151.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 480.0
		
Iteration: 22
Labeled set size: 1232
Unlabelled set size: 2830
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.995276102606955E-22
Relative absolute error: 2.204732955204326E-21
Root relative squared error: 1.599055220521391E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 537.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.102366477602163E-23
Coverage of cases: 100.0
Instances selection time: 52.0
Test time: 82.0
Accumulative iteration time: 11688.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 485.0
		
Iteration: 23
Labeled set size: 1252
Unlabelled set size: 2810
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.0330706391937846E-20
Relative absolute error: 2.888359992934536E-20
Root relative squared error: 2.066141278387569E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 585.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4441799964672682E-22
Coverage of cases: 100.0
Instances selection time: 51.0
Test time: 88.0
Accumulative iteration time: 12273.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 534.0
		
Iteration: 24
Labeled set size: 1272
Unlabelled set size: 2790
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.8058762997776844E-20
Relative absolute error: 7.166067310715795E-20
Root relative squared error: 5.6117525995553684E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 603.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.5830336553578977E-22
Coverage of cases: 100.0
Instances selection time: 53.0
Test time: 88.0
Accumulative iteration time: 12876.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 550.0
		
Iteration: 25
Labeled set size: 1292
Unlabelled set size: 2770
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.1183555138642214E-19
Relative absolute error: 7.993602947966543E-19
Root relative squared error: 6.236711027728443E-17
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 562.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.996801473983272E-21
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 85.0
Accumulative iteration time: 13438.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 514.0
		
Iteration: 26
Labeled set size: 1312
Unlabelled set size: 2750
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 7.502648650132454E-18
Relative absolute error: 2.4940986931837887E-17
Root relative squared error: 1.5005297300264907E-15
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 593.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.2470493465918944E-19
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 84.0
Accumulative iteration time: 14031.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 548.0
		
Iteration: 27
Labeled set size: 1332
Unlabelled set size: 2730
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.0259903291788246E-34
Relative absolute error: 1.86830299100313E-33
Root relative squared error: 1.2051980658357649E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 614.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.34151495501565E-36
Coverage of cases: 100.0
Instances selection time: 48.0
Test time: 85.0
Accumulative iteration time: 14645.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 566.0
		
Iteration: 28
Labeled set size: 1352
Unlabelled set size: 2710
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.9962397724298358E-33
Relative absolute error: 8.655577399785339E-33
Root relative squared error: 5.992479544859672E-31
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 710.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.3277886998926697E-35
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 86.0
Accumulative iteration time: 15355.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 665.0
		
Iteration: 29
Labeled set size: 1372
Unlabelled set size: 2690
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.29413974021565E-32
Relative absolute error: 1.0184358706226823E-31
Root relative squared error: 6.5882794804313E-30
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 713.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 5.092179353113411E-34
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 87.0
Accumulative iteration time: 16068.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 666.0
		
Iteration: 30
Labeled set size: 1392
Unlabelled set size: 2670
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.4992602380526684E-31
Relative absolute error: 4.658534432180301E-31
Root relative squared error: 2.998520476105337E-29
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 716.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.3292672160901505E-33
Coverage of cases: 100.0
Instances selection time: 47.0
Test time: 87.0
Accumulative iteration time: 16784.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 669.0
		
Iteration: 31
Labeled set size: 1412
Unlabelled set size: 2650
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.877308189144875E-31
Relative absolute error: 3.163956282157747E-30
Root relative squared error: 1.975461637828975E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 801.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.5819781410788734E-32
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 80.0
Accumulative iteration time: 17585.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 755.0
		
Iteration: 32
Labeled set size: 1432
Unlabelled set size: 2630
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 3.1031601171133577E-30
Relative absolute error: 9.628528750711732E-30
Root relative squared error: 6.206320234226716E-28
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 673.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.8142643753558663E-32
Coverage of cases: 100.0
Instances selection time: 46.0
Test time: 86.0
Accumulative iteration time: 18258.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 627.0
		
Iteration: 33
Labeled set size: 1452
Unlabelled set size: 2610
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.024039591747906E-29
Relative absolute error: 6.426524678775638E-29
Root relative squared error: 4.048079183495812E-27
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 869.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.2132623393878192E-31
Coverage of cases: 100.0
Instances selection time: 50.0
Test time: 82.0
Accumulative iteration time: 19127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 819.0
		
Iteration: 34
Labeled set size: 1472
Unlabelled set size: 2590
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 6.086685166911436E-29
Relative absolute error: 1.8376397465540544E-28
Root relative squared error: 1.2173370333822872E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 611.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 9.188198732770272E-31
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 86.0
Accumulative iteration time: 19738.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 567.0
		
Iteration: 35
Labeled set size: 1492
Unlabelled set size: 2570
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.3412461749626585E-28
Relative absolute error: 1.3377270025455708E-27
Root relative squared error: 8.682492349925317E-26
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 626.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.688635012727854E-30
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 89.0
Accumulative iteration time: 20364.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 582.0
		
Iteration: 36
Labeled set size: 1512
Unlabelled set size: 2550
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6007314814052857E-27
Relative absolute error: 8.21721884664101E-27
Root relative squared error: 5.201462962810571E-25
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 763.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.108609423320505E-29
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 85.0
Accumulative iteration time: 21127.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 721.0
		
Iteration: 37
Labeled set size: 1532
Unlabelled set size: 2530
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.7204773012288507E-26
Relative absolute error: 5.435024668039862E-26
Root relative squared error: 3.4409546024577014E-24
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 773.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 2.717512334019931E-28
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 86.0
Accumulative iteration time: 21900.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 729.0
		
Iteration: 38
Labeled set size: 1552
Unlabelled set size: 2510
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1589261389696748E-25
Relative absolute error: 3.687773088312951E-25
Root relative squared error: 2.3178522779393496E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 694.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.8438865441564755E-27
Coverage of cases: 100.0
Instances selection time: 45.0
Test time: 82.0
Accumulative iteration time: 22594.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 649.0
		
Iteration: 39
Labeled set size: 1572
Unlabelled set size: 2490
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.472383568295356E-25
Relative absolute error: 1.3487354511981634E-24
Root relative squared error: 8.944767136590711E-23
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 917.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.743677255990817E-27
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 91.0
Accumulative iteration time: 23511.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 876.0
		
Iteration: 40
Labeled set size: 1592
Unlabelled set size: 2470
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.1101215391800233E-24
Relative absolute error: 6.04117954200457E-24
Root relative squared error: 4.220243078360047E-22
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 780.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 3.020589771002285E-26
Coverage of cases: 100.0
Instances selection time: 44.0
Test time: 83.0
Accumulative iteration time: 24291.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 736.0
		
Iteration: 41
Labeled set size: 1612
Unlabelled set size: 2450
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.420927883239865E-24
Relative absolute error: 2.971283318033328E-23
Root relative squared error: 1.884185576647973E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 713.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.4856416590166639E-25
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 85.0
Accumulative iteration time: 25004.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 671.0
		
Iteration: 42
Labeled set size: 1632
Unlabelled set size: 2430
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.075571702508934E-23
Relative absolute error: 1.249419736649067E-22
Root relative squared error: 8.151143405017868E-21
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 682.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.247098683245335E-25
Coverage of cases: 100.0
Instances selection time: 38.0
Test time: 89.0
Accumulative iteration time: 25686.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 644.0
		
Iteration: 43
Labeled set size: 1652
Unlabelled set size: 2410
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 2.6034640751269332E-22
Relative absolute error: 8.161804193576E-22
Root relative squared error: 5.2069281502538666E-20
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 792.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 4.080902096788E-24
Coverage of cases: 100.0
Instances selection time: 43.0
Test time: 87.0
Accumulative iteration time: 26478.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 749.0
		
Iteration: 44
Labeled set size: 1672
Unlabelled set size: 2390
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 1.1799932631218883E-21
Relative absolute error: 3.3863059465697638E-21
Root relative squared error: 2.3599865262437767E-19
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 880.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.693152973284882E-23
Coverage of cases: 100.0
Instances selection time: 41.0
Test time: 88.0
Accumulative iteration time: 27358.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 839.0
		
Iteration: 45
Labeled set size: 1692
Unlabelled set size: 2370
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 9.056401040953138E-21
Relative absolute error: 2.87927868180184E-20
Root relative squared error: 1.8112802081906276E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 824.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 1.43963934090092E-22
Coverage of cases: 100.0
Instances selection time: 42.0
Test time: 83.0
Accumulative iteration time: 28182.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 782.0
		
Iteration: 46
Labeled set size: 1712
Unlabelled set size: 2350
	
Mean region size: 50.0
Incorrectly Classified Instances: 0.0
Correctly Classified Instances: 100.0
Weighted Precision: 1.0
Weighted AreaUnderROC: 1.0
Root mean squared error: 4.643304079091315E-20
Relative absolute error: 1.2568121834763532E-19
Root relative squared error: 9.28660815818263E-18
Weighted TruePositiveRate: 1.0
Weighted MatthewsCorrelation: 1.0
Weighted FMeasure: 1.0
Iteration time: 789.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 6.284060917381766E-22
Coverage of cases: 100.0
Instances selection time: 38.0
Test time: 85.0
Accumulative iteration time: 28971.0
Weighted Recall: 1.0
Weighted FalsePositiveRate: 0.0
Kappa statistic: 1.0
Training time: 751.0
		
Time end:Thu Nov 30 21.39.55 EET 2017