Fri Dec 01 13.20.00 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.20.00 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 51.1520737327189
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9519870810193392
Weighted AreaUnderROC: 0.9918098818474759
Root mean squared error: 0.2236293791728568
Relative absolute error: 10.461907959446732
Root relative squared error: 44.72587583457136
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8967979565784406
Weighted FMeasure: 0.9496375638311122
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9915238209789233
Mean absolute error: 0.05230953979723366
Coverage of cases: 94.93087557603687
Instances selection time: 6.0
Test time: 13.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0407874525022233
Kappa statistic: 0.8947855600123419
Training time: 11.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 56.22119815668203
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9847833870390262
Root mean squared error: 0.20238315434516665
Relative absolute error: 12.932208427325751
Root relative squared error: 40.47663086903333
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9857142855833891
Mean absolute error: 0.06466104213662875
Coverage of cases: 97.23502304147465
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 22.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 59.44700460829493
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9960615825277479
Root mean squared error: 0.1818277970282414
Relative absolute error: 12.458625625390964
Root relative squared error: 36.36555940564828
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9962187671342688
Mean absolute error: 0.06229312812695482
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 10.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9663394109396914
Weighted AreaUnderROC: 0.9950769781596849
Root mean squared error: 0.16849893099811014
Relative absolute error: 9.889001245779703
Root relative squared error: 33.699786199622025
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.9263495937247721
Weighted FMeasure: 0.9633999408943144
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9951726670566744
Mean absolute error: 0.04944500622889852
Coverage of cases: 99.07834101382488
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 80.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.023284016492845016
Kappa statistic: 0.9236453201970445
Training time: 23.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 56.45161290322581
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9588194921070694
Weighted AreaUnderROC: 0.996330110991765
Root mean squared error: 0.18050805472308626
Relative absolute error: 11.28236978629405
Root relative squared error: 36.10161094461725
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.909080728844557
Weighted FMeasure: 0.9543114579557811
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9964044305374167
Mean absolute error: 0.05641184893147025
Coverage of cases: 99.07834101382488
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.02910502061605627
Kappa statistic: 0.9049662783568363
Training time: 25.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 56.45161290322581
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9588194921070694
Weighted AreaUnderROC: 0.996330110991765
Root mean squared error: 0.1804972656516511
Relative absolute error: 11.165889570631315
Root relative squared error: 36.099453130330225
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.909080728844557
Weighted FMeasure: 0.9543114579557811
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9964044305374167
Mean absolute error: 0.05582944785315657
Coverage of cases: 99.07834101382488
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 141.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.02910502061605627
Kappa statistic: 0.9049662783568363
Training time: 25.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 58.064516129032256
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9588194921070694
Weighted AreaUnderROC: 0.9967776584317938
Root mean squared error: 0.1884484607604996
Relative absolute error: 11.512846411457833
Root relative squared error: 37.689692152099916
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.909080728844557
Weighted FMeasure: 0.9543114579557811
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9968532797137383
Mean absolute error: 0.057564232057289166
Coverage of cases: 99.07834101382488
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.02910502061605627
Kappa statistic: 0.9049662783568363
Training time: 27.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 58.064516129032256
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9946294307196563
Root mean squared error: 0.19105875843383205
Relative absolute error: 12.677307662877526
Root relative squared error: 38.21175168676641
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 38.0
Weighted AreaUnderPRC: 0.9947990303568469
Mean absolute error: 0.06338653831438763
Coverage of cases: 99.53917050691244
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 211.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 33.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 58.064516129032256
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9945399212316505
Root mean squared error: 0.19097768108415714
Relative absolute error: 12.523108251640421
Root relative squared error: 38.19553621683143
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 41.0
Weighted AreaUnderPRC: 0.9947054040284142
Mean absolute error: 0.06261554125820211
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 252.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 40.0
		
Time end:Fri Dec 01 13.20.01 EET 2017