Fri Dec 01 13.19.36 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.19.36 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.14678899082569
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9302542809417096
Weighted AreaUnderROC: 0.9797563623559788
Root mean squared error: 0.2764942239095506
Relative absolute error: 15.928319020622835
Root relative squared error: 55.29884478191012
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8463557857579238
Weighted FMeasure: 0.9228310270836502
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9699666436040408
Mean absolute error: 0.07964159510311418
Coverage of cases: 93.57798165137615
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 33.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.057768170940839705
Kappa statistic: 0.8399965460668337
Training time: 24.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 52.981651376146786
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.9666844349680169
Root mean squared error: 0.18013232034281404
Relative absolute error: 8.711451787827826
Root relative squared error: 36.02646406856281
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 26.0
Weighted AreaUnderPRC: 0.959469507317975
Mean absolute error: 0.043557258939139136
Coverage of cases: 97.70642201834862
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 59.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 19.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 52.293577981651374
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9900497512437813
Root mean squared error: 0.20184255072197332
Relative absolute error: 10.583644229168756
Root relative squared error: 40.36851014439466
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9902537332186233
Mean absolute error: 0.05291822114584378
Coverage of cases: 96.78899082568807
Instances selection time: 8.0
Test time: 9.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 22.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9494688233054542
Weighted AreaUnderROC: 0.9950248756218906
Root mean squared error: 0.20056536087762272
Relative absolute error: 10.41697671287163
Root relative squared error: 40.11307217552454
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8932839909857925
Weighted FMeasure: 0.9494839998390723
Iteration time: 37.0
Weighted AreaUnderPRC: 0.995207997077766
Mean absolute error: 0.05208488356435815
Coverage of cases: 99.08256880733946
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 126.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.05828328866806205
Kappa statistic: 0.8932419196865817
Training time: 30.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 54.12844036697248
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.944954128440367
Weighted AreaUnderROC: 0.9946695095948827
Root mean squared error: 0.20075810842684538
Relative absolute error: 10.987354388843878
Root relative squared error: 40.15162168536908
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8837953091684436
Weighted FMeasure: 0.944954128440367
Iteration time: 32.0
Weighted AreaUnderPRC: 0.994873047751559
Mean absolute error: 0.054936771944219386
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 12.0
Accumulative iteration time: 158.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.06115881927192347
Kappa statistic: 0.8837953091684435
Training time: 28.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 57.79816513761468
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9502283314537143
Weighted AreaUnderROC: 0.9944918265813788
Root mean squared error: 0.19443161765354966
Relative absolute error: 11.551623500236214
Root relative squared error: 38.88632353070993
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8945566834252947
Weighted FMeasure: 0.9496982893446182
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9947052955302637
Mean absolute error: 0.057758117501181074
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 190.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04939913799286659
Kappa statistic: 0.8941840967257965
Training time: 26.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 57.79816513761468
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9502283314537143
Weighted AreaUnderROC: 0.9944918265813788
Root mean squared error: 0.19447432433029144
Relative absolute error: 11.546835557666023
Root relative squared error: 38.894864866058285
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8945566834252947
Weighted FMeasure: 0.9496982893446182
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9947052955302637
Mean absolute error: 0.057734177788330114
Coverage of cases: 100.0
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 233.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04939913799286659
Kappa statistic: 0.8941840967257965
Training time: 34.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 57.3394495412844
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9544904991886003
Weighted AreaUnderROC: 0.9941364605543711
Root mean squared error: 0.17427964547166835
Relative absolute error: 10.181013690693968
Root relative squared error: 34.85592909433367
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9037590324881913
Weighted FMeasure: 0.9542258235983915
Iteration time: 50.0
Weighted AreaUnderPRC: 0.9942117773617192
Mean absolute error: 0.050905068453469836
Coverage of cases: 99.08256880733946
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 283.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04652360738900517
Kappa statistic: 0.9035910136210863
Training time: 42.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9588474694561553
Weighted AreaUnderROC: 0.9832089552238807
Root mean squared error: 0.18142577035326882
Relative absolute error: 9.422597049695918
Root relative squared error: 36.28515407065376
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9130822475220538
Weighted FMeasure: 0.9587604279190397
Iteration time: 42.0
Weighted AreaUnderPRC: 0.9792279133502476
Mean absolute error: 0.04711298524847959
Coverage of cases: 98.62385321100918
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 325.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.04364807678514374
Kappa statistic: 0.9130396241467955
Training time: 40.0
		
Time end:Fri Dec 01 13.19.36 EET 2017