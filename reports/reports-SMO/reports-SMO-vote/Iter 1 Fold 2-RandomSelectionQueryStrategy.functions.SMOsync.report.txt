Fri Dec 01 13.20.03 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.20.03 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.23041474654378
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9340630677075662
Weighted AreaUnderROC: 0.9806442950694708
Root mean squared error: 0.2628934758062446
Relative absolute error: 13.916397955070178
Root relative squared error: 52.57869516124892
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8558660091846084
Weighted FMeasure: 0.9298265263963672
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9780664340497118
Mean absolute error: 0.06958198977535089
Coverage of cases: 93.08755760368663
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.10067507478373354
Kappa statistic: 0.8507223113964686
Training time: 10.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.46082949308756
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9274645089043818
Weighted AreaUnderROC: 0.9888387443262534
Root mean squared error: 0.25993496861125615
Relative absolute error: 14.076844765762575
Root relative squared error: 51.98699372225123
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8444815297246527
Weighted FMeasure: 0.9255003531904874
Iteration time: 19.0
Weighted AreaUnderPRC: 0.987717806174347
Mean absolute error: 0.07038422382881288
Coverage of cases: 93.54838709677419
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.09919961193305846
Kappa statistic: 0.8418367346938775
Training time: 14.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 62.21198156682028
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9862602935911206
Root mean squared error: 0.18724954518466086
Relative absolute error: 12.862320860094856
Root relative squared error: 37.44990903693217
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9852301312000905
Mean absolute error: 0.06431160430047428
Coverage of cases: 98.61751152073732
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 9.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9641046625292686
Weighted AreaUnderROC: 0.9946741854636592
Root mean squared error: 0.1697817350167867
Relative absolute error: 9.617496252467053
Root relative squared error: 33.95634700335734
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.923666504256351
Weighted FMeasure: 0.9632808965600179
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9947195601728827
Mean absolute error: 0.048087481262335266
Coverage of cases: 99.07834101382488
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.032055946317406415
Kappa statistic: 0.9229813664596275
Training time: 16.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 57.83410138248848
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9641046625292686
Weighted AreaUnderROC: 0.9957482993197279
Root mean squared error: 0.1619883995691086
Relative absolute error: 9.807531384377645
Root relative squared error: 32.39767991382172
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.923666504256351
Weighted FMeasure: 0.9632808965600179
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9958553580178484
Mean absolute error: 0.049037656921888224
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 81.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.032055946317406415
Kappa statistic: 0.9229813664596275
Training time: 11.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 58.294930875576036
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9591597501497389
Weighted AreaUnderROC: 0.9968224131757968
Root mean squared error: 0.16319461298545046
Relative absolute error: 10.933653559774923
Root relative squared error: 32.638922597090094
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9135480351708758
Weighted FMeasure: 0.9586526109213916
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9968770674301733
Mean absolute error: 0.05466826779887462
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 93.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03935241329129274
Kappa statistic: 0.9131652661064424
Training time: 12.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 58.064516129032256
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9641046625292686
Weighted AreaUnderROC: 0.993689581095596
Root mean squared error: 0.17769666603313444
Relative absolute error: 11.365060352653664
Root relative squared error: 35.539333206626885
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.923666504256351
Weighted FMeasure: 0.9632808965600179
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9937452968720908
Mean absolute error: 0.056825301763268325
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 122.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.032055946317406415
Kappa statistic: 0.9229813664596275
Training time: 29.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 60.13824884792627
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9650664634662603
Weighted AreaUnderROC: 0.9885875402792696
Root mean squared error: 0.16569145793831883
Relative absolute error: 11.725484244759791
Root relative squared error: 33.138291587663765
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.9248456730523413
Weighted FMeasure: 0.9633438863694704
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9892595355731705
Mean absolute error: 0.05862742122379896
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 139.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.027669981405125714
Kappa statistic: 0.9233147804576377
Training time: 16.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 60.13824884792627
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9650664634662603
Weighted AreaUnderROC: 0.9881399928392409
Root mean squared error: 0.16584141028422472
Relative absolute error: 11.404188315707353
Root relative squared error: 33.168282056844944
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.9248456730523413
Weighted FMeasure: 0.9633438863694704
Iteration time: 19.0
Weighted AreaUnderPRC: 0.988824404460539
Mean absolute error: 0.057020941578536766
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 158.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.027669981405125714
Kappa statistic: 0.9233147804576377
Training time: 19.0
		
Time end:Fri Dec 01 13.20.04 EET 2017