Fri Dec 01 13.19.32 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.19.32 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 51.61290322580645
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9012606599925843
Weighted AreaUnderROC: 0.9557808692237506
Root mean squared error: 0.30727361161846134
Relative absolute error: 19.95804516556116
Root relative squared error: 61.454722323692266
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7862182634359883
Weighted FMeasure: 0.8969062963029081
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9491078486889012
Mean absolute error: 0.09979022582780579
Coverage of cases: 91.70506912442396
Instances selection time: 12.0
Test time: 23.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.13859244886409575
Kappa statistic: 0.7805662805662806
Training time: 36.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9447004608294931
Weighted AreaUnderROC: 0.9871106337271751
Root mean squared error: 0.2296764717032802
Relative absolute error: 12.74063006741812
Root relative squared error: 45.93529434065604
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834586466165414
Weighted FMeasure: 0.9447004608294931
Iteration time: 60.0
Weighted AreaUnderPRC: 0.987204729768617
Mean absolute error: 0.0637031503370906
Coverage of cases: 97.23502304147465
Instances selection time: 11.0
Test time: 23.0
Accumulative iteration time: 108.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06124181421295173
Kappa statistic: 0.8834586466165414
Training time: 49.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 55.99078341013825
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9493376203528006
Weighted AreaUnderROC: 0.9918098818474759
Root mean squared error: 0.20547692130181922
Relative absolute error: 12.801751990370366
Root relative squared error: 41.09538426036384
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8928470185421117
Weighted FMeasure: 0.9491305911979853
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9921256728443811
Mean absolute error: 0.06400875995185183
Coverage of cases: 98.61751152073732
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 183.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0627172770636268
Kappa statistic: 0.8924629454430779
Training time: 65.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9355427360489758
Weighted AreaUnderROC: 0.9902434658073755
Root mean squared error: 0.2197982475260777
Relative absolute error: 12.64644531051326
Root relative squared error: 43.959649505215545
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.863495245829256
Weighted FMeasure: 0.9351749861584637
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9905453776203326
Mean absolute error: 0.0632322265525663
Coverage of cases: 97.23502304147465
Instances selection time: 5.0
Test time: 19.0
Accumulative iteration time: 275.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.0802207130730051
Kappa statistic: 0.8628318584070795
Training time: 87.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 54.60829493087557
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9446163708948918
Weighted AreaUnderROC: 0.9886322950232724
Root mean squared error: 0.210286069429035
Relative absolute error: 12.57392677822262
Root relative squared error: 42.057213885807
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831133236786068
Weighted FMeasure: 0.944573672639523
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9885952866322917
Mean absolute error: 0.06286963389111311
Coverage of cases: 97.6958525345622
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 347.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06562777912523243
Kappa statistic: 0.8829452485840151
Training time: 66.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 54.60829493087557
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9450988701373817
Weighted AreaUnderROC: 0.9905119942713928
Root mean squared error: 0.2029641808013725
Relative absolute error: 12.166529307698
Root relative squared error: 40.5928361602745
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8841329033278216
Weighted FMeasure: 0.9448162842849749
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9904967192244256
Mean absolute error: 0.06083264653848999
Coverage of cases: 98.15668202764977
Instances selection time: 5.0
Test time: 19.0
Accumulative iteration time: 426.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.05685584930067103
Kappa statistic: 0.8839675608234561
Training time: 74.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9586567764935886
Weighted AreaUnderROC: 0.9941371285356249
Root mean squared error: 0.18516824396854514
Relative absolute error: 9.675037898429489
Root relative squared error: 37.03364879370903
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9128281065680445
Weighted FMeasure: 0.958569797635297
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9943358691210785
Mean absolute error: 0.04837518949214745
Coverage of cases: 98.15668202764977
Instances selection time: 3.0
Test time: 15.0
Accumulative iteration time: 531.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.04373837820357345
Kappa statistic: 0.912785245389184
Training time: 102.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9494594905410846
Weighted AreaUnderROC: 0.9828589330469031
Root mean squared error: 0.20006352485369497
Relative absolute error: 10.633518903172371
Root relative squared error: 40.012704970738994
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8934461399217367
Weighted FMeasure: 0.9493630859986963
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9811798131513221
Mean absolute error: 0.05316759451586186
Coverage of cases: 98.61751152073732
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 600.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0539453472390654
Kappa statistic: 0.8934041888090029
Training time: 63.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9494594905410846
Weighted AreaUnderROC: 0.9828589330469031
Root mean squared error: 0.2001226306467064
Relative absolute error: 10.635794297085937
Root relative squared error: 40.02452612934128
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8934461399217367
Weighted FMeasure: 0.9493630859986963
Iteration time: 53.0
Weighted AreaUnderPRC: 0.9811798131513221
Mean absolute error: 0.05317897148542968
Coverage of cases: 98.61751152073732
Instances selection time: 10.0
Test time: 11.0
Accumulative iteration time: 653.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0539453472390654
Kappa statistic: 0.8934041888090029
Training time: 43.0
		
Time end:Fri Dec 01 13.19.34 EET 2017