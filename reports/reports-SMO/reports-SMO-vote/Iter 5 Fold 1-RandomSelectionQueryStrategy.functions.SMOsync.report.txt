Fri Dec 01 13.20.08 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.20.08 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9522113370267673
Weighted AreaUnderROC: 0.9848081023454158
Root mean squared error: 0.22386137108977952
Relative absolute error: 10.948613779091373
Root relative squared error: 44.7722742179559
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8971104851783512
Weighted FMeasure: 0.9498738092727337
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9844772936518661
Mean absolute error: 0.05474306889545686
Coverage of cases: 95.41284403669725
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04051498731767115
Kappa statistic: 0.8951097891698013
Training time: 15.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 56.19266055045872
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9445895235276709
Weighted AreaUnderROC: 0.9795688986261355
Root mean squared error: 0.23778635710123439
Relative absolute error: 15.565570982428811
Root relative squared error: 47.55727142024688
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8797962471180342
Weighted FMeasure: 0.9408469713243465
Iteration time: 24.0
Weighted AreaUnderPRC: 0.96332092231481
Mean absolute error: 0.07782785491214406
Coverage of cases: 95.41284403669725
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.046266048525393996
Kappa statistic: 0.8765786952356067
Training time: 19.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.974591329068941
Root mean squared error: 0.206882217970751
Relative absolute error: 11.976243239258643
Root relative squared error: 41.376443594150196
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9718632262611329
Mean absolute error: 0.05988121619629322
Coverage of cases: 96.3302752293578
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 17.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9775230987917556
Root mean squared error: 0.19825743512034785
Relative absolute error: 11.040520025155026
Root relative squared error: 39.65148702406957
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9750624087064478
Mean absolute error: 0.055202600125775124
Coverage of cases: 96.78899082568807
Instances selection time: 9.0
Test time: 4.0
Accumulative iteration time: 104.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 27.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9767235252309879
Root mean squared error: 0.20116543707606882
Relative absolute error: 10.830521522955937
Root relative squared error: 40.23308741521377
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 40.0
Weighted AreaUnderPRC: 0.9757110758122327
Mean absolute error: 0.05415260761477969
Coverage of cases: 96.3302752293578
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 33.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 52.981651376146786
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9861407249466951
Root mean squared error: 0.21185338623577918
Relative absolute error: 11.363444884821613
Root relative squared error: 42.37067724715583
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 32.0
Weighted AreaUnderPRC: 0.9862045163341349
Mean absolute error: 0.05681722442410806
Coverage of cases: 96.3302752293578
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 176.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 28.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9593495149616872
Weighted AreaUnderROC: 0.9888948116560057
Root mean squared error: 0.18889563317194769
Relative absolute error: 10.263186959246152
Root relative squared error: 37.779126634389534
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9138039551373668
Weighted FMeasure: 0.9588440549183241
Iteration time: 40.0
Weighted AreaUnderPRC: 0.9898503973378672
Mean absolute error: 0.051315934796230755
Coverage of cases: 98.1651376146789
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 216.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03920600144754602
Kappa statistic: 0.9134233518665609
Training time: 36.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 53.211009174311926
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9593495149616872
Weighted AreaUnderROC: 0.9886282871357498
Root mean squared error: 0.18947200046243992
Relative absolute error: 10.096247295570453
Root relative squared error: 37.894400092487984
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9138039551373668
Weighted FMeasure: 0.9588440549183241
Iteration time: 36.0
Weighted AreaUnderPRC: 0.9896170099600137
Mean absolute error: 0.050481236477852263
Coverage of cases: 97.70642201834862
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 252.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03920600144754602
Kappa statistic: 0.9134233518665609
Training time: 29.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9836531627576403
Root mean squared error: 0.189906371053908
Relative absolute error: 10.730435082022314
Root relative squared error: 37.9812742107816
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 39.0
Weighted AreaUnderPRC: 0.9850280775800504
Mean absolute error: 0.05365217541011157
Coverage of cases: 96.78899082568807
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 291.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 35.0
		
Time end:Fri Dec 01 13.20.09 EET 2017