Fri Dec 01 13.19.35 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.19.35 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 52.07373271889401
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9030488830623409
Weighted AreaUnderROC: 0.9766380236305048
Root mean squared error: 0.3000986610228978
Relative absolute error: 19.114537408989506
Root relative squared error: 60.019732204579554
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7956420835574817
Weighted FMeasure: 0.903117288438708
Iteration time: 26.0
Weighted AreaUnderPRC: 0.978288691016928
Mean absolute error: 0.09557268704494752
Coverage of cases: 92.62672811059907
Instances selection time: 3.0
Test time: 21.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.10936615732880588
Kappa statistic: 0.7956043956043956
Training time: 23.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 58.525345622119815
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9035190615835778
Weighted AreaUnderROC: 0.9822771213748658
Root mean squared error: 0.25035805402620437
Relative absolute error: 18.53261633738278
Root relative squared error: 50.07161080524087
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7947695843356162
Weighted FMeasure: 0.9023627144244255
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9832150924257088
Mean absolute error: 0.0926630816869139
Coverage of cases: 99.07834101382488
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.12252405206564797
Kappa statistic: 0.7928730512249442
Training time: 19.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 52.764976958525345
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9245040997692345
Weighted AreaUnderROC: 0.9873791621911923
Root mean squared error: 0.2612565366692482
Relative absolute error: 15.478875886173135
Root relative squared error: 52.25130733384964
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8358419742527421
Weighted FMeasure: 0.9204700632492161
Iteration time: 42.0
Weighted AreaUnderPRC: 0.988085819059099
Mean absolute error: 0.07739437943086568
Coverage of cases: 95.85253456221199
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 96.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.11088204381922548
Kappa statistic: 0.8308186195826646
Training time: 34.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9454272810039809
Weighted AreaUnderROC: 0.9911385606874329
Root mean squared error: 0.21246573324454723
Relative absolute error: 12.266657309150784
Root relative squared error: 42.493146648909445
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834462279296256
Weighted FMeasure: 0.9442863166772084
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9914959808991418
Mean absolute error: 0.061333286545753915
Coverage of cases: 97.23502304147465
Instances selection time: 5.0
Test time: 11.0
Accumulative iteration time: 130.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.07439970894979384
Kappa statistic: 0.881904761904762
Training time: 29.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 54.83870967741935
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9307663925785367
Weighted AreaUnderROC: 0.9901539563193699
Root mean squared error: 0.22496269235153132
Relative absolute error: 13.943880324034607
Root relative squared error: 44.992538470306265
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8537258064222453
Weighted FMeasure: 0.9306326243608891
Iteration time: 32.0
Weighted AreaUnderPRC: 0.990479289564805
Mean absolute error: 0.06971940162017304
Coverage of cases: 97.6958525345622
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.08313121513461072
Kappa statistic: 0.8533585619678334
Training time: 25.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 55.29953917050691
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9307663925785367
Weighted AreaUnderROC: 0.9892588614393125
Root mean squared error: 0.22491626280429314
Relative absolute error: 13.812465234331686
Root relative squared error: 44.98325256085863
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8537258064222453
Weighted FMeasure: 0.9306326243608891
Iteration time: 37.0
Weighted AreaUnderPRC: 0.9894770125909623
Mean absolute error: 0.06906232617165843
Coverage of cases: 98.61751152073732
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.08313121513461072
Kappa statistic: 0.8533585619678334
Training time: 32.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 55.29953917050691
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9353556546318151
Weighted AreaUnderROC: 0.9891693519513067
Root mean squared error: 0.21987232404143922
Relative absolute error: 13.87037283718471
Root relative squared error: 43.97446480828784
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.8636004847377396
Weighted FMeasure: 0.9353359514127767
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9894057406233401
Mean absolute error: 0.06935186418592355
Coverage of cases: 98.15668202764977
Instances selection time: 1.0
Test time: 11.0
Accumulative iteration time: 247.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.07583474816072439
Kappa statistic: 0.8634361233480174
Training time: 47.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9538770871579683
Weighted AreaUnderROC: 0.9931972789115646
Root mean squared error: 0.1952979671734938
Relative absolute error: 11.16098672196964
Root relative squared error: 39.059593434698755
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9026261626194743
Weighted FMeasure: 0.9538113938662691
Iteration time: 53.0
Weighted AreaUnderPRC: 0.9932561254018809
Mean absolute error: 0.0558049336098482
Coverage of cases: 98.61751152073732
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 300.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.05542081008974049
Kappa statistic: 0.9024543738200125
Training time: 47.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9541676477513538
Weighted AreaUnderROC: 0.9900644468313641
Root mean squared error: 0.18821805005862915
Relative absolute error: 10.144520831261875
Root relative squared error: 37.64361001172583
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9027162753101454
Weighted FMeasure: 0.953696418684617
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9907473136750197
Mean absolute error: 0.05072260415630937
Coverage of cases: 98.15668202764977
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 352.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.05980677500202118
Kappa statistic: 0.9020227560050569
Training time: 49.0
		
Time end:Fri Dec 01 13.19.36 EET 2017