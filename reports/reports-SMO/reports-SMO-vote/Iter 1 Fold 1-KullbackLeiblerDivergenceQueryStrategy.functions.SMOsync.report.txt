Fri Dec 01 13.19.41 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Fri Dec 01 13.19.41 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.37614678899082
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9207930124418752
Weighted AreaUnderROC: 0.9758033228353645
Root mean squared error: 0.28845578007023465
Relative absolute error: 17.380274945282828
Root relative squared error: 57.69115601404693
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8268336917495275
Weighted FMeasure: 0.9160248008871863
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9745331823155969
Mean absolute error: 0.08690137472641414
Coverage of cases: 92.66055045871559
Instances selection time: 10.0
Test time: 8.0
Accumulative iteration time: 51.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.11839068093347159
Kappa statistic: 0.8209200438116102
Training time: 41.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 51.14678899082569
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9502283314537143
Weighted AreaUnderROC: 0.9816098081023454
Root mean squared error: 0.2293256908044055
Relative absolute error: 11.337660326145404
Root relative squared error: 45.8651381608811
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8945566834252947
Weighted FMeasure: 0.9496982893446182
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9828371238175159
Mean absolute error: 0.056688301630727016
Coverage of cases: 94.95412844036697
Instances selection time: 9.0
Test time: 10.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04939913799286659
Kappa statistic: 0.8941840967257965
Training time: 19.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 52.522935779816514
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9540856620115041
Weighted AreaUnderROC: 0.9927149964463396
Root mean squared error: 0.192064608213624
Relative absolute error: 9.170449025631049
Root relative squared error: 38.412921642724804
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9029015489816212
Weighted FMeasure: 0.9540220003356955
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9927075986621308
Mean absolute error: 0.04585224512815524
Coverage of cases: 97.70642201834862
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 106.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.05540775806420062
Kappa statistic: 0.902730679992861
Training time: 24.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.91743119266055
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9588086043587283
Weighted AreaUnderROC: 0.9888018948507789
Root mean squared error: 0.18995961963186547
Relative absolute error: 7.843682794430075
Root relative squared error: 37.9919239263731
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.912652266202488
Weighted FMeasure: 0.9585687950290678
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9833435451180832
Mean absolute error: 0.039218413972150375
Coverage of cases: 97.24770642201835
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 158.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.05253222746033919
Kappa statistic: 0.9122618728199625
Training time: 50.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9494688233054542
Weighted AreaUnderROC: 0.9899609097370291
Root mean squared error: 0.19070270100353467
Relative absolute error: 10.55972938749839
Root relative squared error: 38.14054020070694
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8932839909857925
Weighted FMeasure: 0.9494839998390723
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9896160983892139
Mean absolute error: 0.05279864693749195
Coverage of cases: 99.08256880733946
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 188.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.05828328866806205
Kappa statistic: 0.8932419196865817
Training time: 25.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.9636276354326281
Weighted AreaUnderROC: 0.9955579246624022
Root mean squared error: 0.17128726486895834
Relative absolute error: 9.10803088260177
Root relative squared error: 34.25745297379167
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9230444151313418
Weighted FMeasure: 0.9633806588787133
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9956632764919564
Mean absolute error: 0.045540154413008856
Coverage of cases: 99.54128440366972
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 221.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.03633047084368459
Kappa statistic: 0.9228728108968689
Training time: 32.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 55.73394495412844
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9540856620115041
Weighted AreaUnderROC: 0.9957356076759061
Root mean squared error: 0.16162188588998389
Relative absolute error: 9.642734999657586
Root relative squared error: 32.32437717799678
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9029015489816212
Weighted FMeasure: 0.9540220003356955
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9958165063988672
Mean absolute error: 0.04821367499828793
Coverage of cases: 99.54128440366972
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 255.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.05540775806420062
Kappa statistic: 0.902730679992861
Training time: 29.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 56.19266055045872
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9948471926083866
Root mean squared error: 0.17775498545896212
Relative absolute error: 11.441435076427387
Root relative squared error: 35.550997091792425
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9952410539489428
Mean absolute error: 0.05720717538213694
Coverage of cases: 98.62385321100918
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 289.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 31.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 56.42201834862385
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9917377398720683
Root mean squared error: 0.1832124490929033
Relative absolute error: 12.082024024107865
Root relative squared error: 36.64248981858066
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 37.0
Weighted AreaUnderPRC: 0.9926130962447264
Mean absolute error: 0.060410120120539326
Coverage of cases: 98.62385321100918
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 326.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 36.0
		
Time end:Fri Dec 01 13.19.42 EET 2017