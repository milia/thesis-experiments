Fri Dec 01 13.20.13 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Fri Dec 01 13.20.13 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9357798165137615
Weighted AreaUnderROC: 0.9796108742004264
Root mean squared error: 0.2345394030904163
Relative absolute error: 12.117996482991947
Root relative squared error: 46.90788061808326
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8644278606965174
Weighted FMeasure: 0.9357798165137615
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9803738956032628
Mean absolute error: 0.060589982414959734
Coverage of cases: 95.41284403669725
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07135195581724404
Kappa statistic: 0.8644278606965176
Training time: 10.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 51.14678899082569
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9507568363707345
Weighted AreaUnderROC: 0.988095238095238
Root mean squared error: 0.21063968494996826
Relative absolute error: 9.816360241842608
Root relative squared error: 42.12793698999365
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8939184241669917
Weighted FMeasure: 0.9490865576386117
Iteration time: 11.0
Weighted AreaUnderPRC: 0.989001536421454
Mean absolute error: 0.04908180120921304
Coverage of cases: 96.3302752293578
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.07160951468085522
Kappa statistic: 0.8917967692446529
Training time: 10.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 51.37614678899082
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9111541750509861
Weighted AreaUnderROC: 0.9764048368902538
Root mean squared error: 0.29888247588477773
Relative absolute error: 18.6474725017599
Root relative squared error: 59.77649517695555
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.8004637145150094
Weighted FMeasure: 0.9011201678173196
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9732652970330148
Mean absolute error: 0.0932373625087995
Coverage of cases: 91.74311926605505
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.14478557409544676
Kappa statistic: 0.7886621733911919
Training time: 21.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.68807339449541
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9167209233501036
Weighted AreaUnderROC: 0.955777306129901
Root mean squared error: 0.3088974449140346
Relative absolute error: 19.478194996773745
Root relative squared error: 61.77948898280692
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.8052248734199617
Weighted FMeasure: 0.9002888797181042
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9483756695574999
Mean absolute error: 0.09739097498386873
Coverage of cases: 90.8256880733945
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.1536697247706422
Kappa statistic: 0.7866927592954991
Training time: 11.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.45871559633027
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9101595079232695
Weighted AreaUnderROC: 0.9653677060307898
Root mean squared error: 0.31561731910002444
Relative absolute error: 20.258481950686924
Root relative squared error: 63.123463820004886
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7935238368915335
Weighted FMeasure: 0.8957679009284514
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9597005164645287
Mean absolute error: 0.10129240975343462
Coverage of cases: 90.36697247706422
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.15654525537450362
Kappa statistic: 0.777054667162514
Training time: 11.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.22935779816514
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9101595079232695
Weighted AreaUnderROC: 0.9642592248456278
Root mean squared error: 0.31792851469855554
Relative absolute error: 20.36855407152073
Root relative squared error: 63.58570293971111
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7935238368915335
Weighted FMeasure: 0.8957679009284514
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9585694187801449
Mean absolute error: 0.10184277035760365
Coverage of cases: 89.90825688073394
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 83.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.15654525537450362
Kappa statistic: 0.777054667162514
Training time: 12.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.45871559633027
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9101595079232695
Weighted AreaUnderROC: 0.9357675906183368
Root mean squared error: 0.314227062978313
Relative absolute error: 20.003038765811077
Root relative squared error: 62.845412595662594
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7935238368915335
Weighted FMeasure: 0.8957679009284514
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9272058874204193
Mean absolute error: 0.10001519382905538
Coverage of cases: 90.36697247706422
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.15654525537450362
Kappa statistic: 0.777054667162514
Training time: 12.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 58.71559633027523
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9383626746012067
Weighted AreaUnderROC: 0.9914712153518125
Root mean squared error: 0.21616199508892955
Relative absolute error: 15.524041430549834
Root relative squared error: 43.23239901778591
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8656038695451582
Weighted FMeasure: 0.9349025714975575
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9920143803815908
Mean absolute error: 0.07762020715274917
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 118.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.09356233250523269
Kappa statistic: 0.861348355442486
Training time: 22.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 58.25688073394495
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9465727246142234
Weighted AreaUnderROC: 0.9907604832977968
Root mean squared error: 0.2056537805858796
Relative absolute error: 13.99743764955218
Root relative squared error: 41.13075611717592
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8844484190442157
Weighted FMeasure: 0.9443757479058635
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9909576317953351
Mean absolute error: 0.0699871882477609
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 132.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.07892712062231437
Kappa statistic: 0.8816931982633864
Training time: 14.0
		
Time end:Fri Dec 01 13.20.14 EET 2017