Fri Dec 01 13.20.02 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Fri Dec 01 13.20.02 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.775736693019915
Incorrectly Classified Instances: 6.344649727307319
Correctly Classified Instances: 93.65535027269269
Weighted Precision: 0.9390106949400936
Weighted AreaUnderROC: 0.9773471228271783
Root mean squared error: 0.24228380105255884
Relative absolute error: 14.437221439805
Root relative squared error: 48.45676021051177
Weighted TruePositiveRate: 0.936553502726927
Weighted MatthewsCorrelation: 0.8686779236413432
Weighted FMeasure: 0.936408771190321
Iteration time: 22.2
Weighted AreaUnderPRC: 0.9728530828532931
Mean absolute error: 0.07218610719902499
Coverage of cases: 95.03614763454952
Instances selection time: 5.4
Test time: 6.7
Accumulative iteration time: 22.2
Weighted Recall: 0.936553502726927
Weighted FalsePositiveRate: 0.06985129945755937
Kappa statistic: 0.8660195181026717
Training time: 16.8
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.218936287151735
Incorrectly Classified Instances: 6.2992009470257475
Correctly Classified Instances: 93.70079905297425
Weighted Precision: 0.939852133664807
Weighted AreaUnderROC: 0.9746868646512812
Root mean squared error: 0.23699195531256775
Relative absolute error: 13.992647272453407
Root relative squared error: 47.398391062513554
Weighted TruePositiveRate: 0.9370079905297425
Weighted MatthewsCorrelation: 0.8697431316935151
Weighted FMeasure: 0.9366880126717302
Iteration time: 19.5
Weighted AreaUnderPRC: 0.9680021037166963
Mean absolute error: 0.06996323636226702
Coverage of cases: 95.03424512746797
Instances selection time: 4.4
Test time: 4.0
Accumulative iteration time: 41.7
Weighted Recall: 0.9370079905297425
Weighted FalsePositiveRate: 0.07310756783120514
Kappa statistic: 0.8663686305975544
Training time: 15.1
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 55.45311376992348
Incorrectly Classified Instances: 5.333150128947702
Correctly Classified Instances: 94.66684987105228
Weighted Precision: 0.9480764716653761
Weighted AreaUnderROC: 0.9788530310832145
Root mean squared error: 0.21506395488095884
Relative absolute error: 13.007072221710061
Root relative squared error: 43.012790976191766
Weighted TruePositiveRate: 0.9466684987105228
Weighted MatthewsCorrelation: 0.8887080670686588
Weighted FMeasure: 0.9464706428551581
Iteration time: 19.4
Weighted AreaUnderPRC: 0.9758108487266457
Mean absolute error: 0.06503536110855028
Coverage of cases: 96.64651418424724
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 61.1
Weighted Recall: 0.9466684987105228
Weighted FalsePositiveRate: 0.060928028131089315
Kappa statistic: 0.8869547321762623
Training time: 15.4
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.8411617976578
Incorrectly Classified Instances: 5.194055722318522
Correctly Classified Instances: 94.80594427768148
Weighted Precision: 0.9492725442285055
Weighted AreaUnderROC: 0.9792307032361617
Root mean squared error: 0.21228532219328486
Relative absolute error: 12.470507277630725
Root relative squared error: 42.45706443865698
Weighted TruePositiveRate: 0.9480594427768146
Weighted MatthewsCorrelation: 0.8914292846747236
Weighted FMeasure: 0.9478669996995907
Iteration time: 27.4
Weighted AreaUnderPRC: 0.9764663047538888
Mean absolute error: 0.06235253638815362
Coverage of cases: 96.50805394664525
Instances selection time: 3.8
Test time: 5.1
Accumulative iteration time: 88.5
Weighted Recall: 0.9480594427768146
Weighted FalsePositiveRate: 0.05873632154088915
Kappa statistic: 0.889879137972007
Training time: 23.6
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 54.671289054242585
Incorrectly Classified Instances: 5.286010231260305
Correctly Classified Instances: 94.71398976873968
Weighted Precision: 0.948095152073812
Weighted AreaUnderROC: 0.9807770447592544
Root mean squared error: 0.21198692665403573
Relative absolute error: 12.597397067543154
Root relative squared error: 42.397385330807154
Weighted TruePositiveRate: 0.9471398976873969
Weighted MatthewsCorrelation: 0.8891478348214218
Weighted FMeasure: 0.9469224167887995
Iteration time: 23.3
Weighted AreaUnderPRC: 0.9781718940925506
Mean absolute error: 0.06298698533771578
Coverage of cases: 96.92301188009978
Instances selection time: 3.1
Test time: 5.7
Accumulative iteration time: 111.8
Weighted Recall: 0.9471398976873969
Weighted FalsePositiveRate: 0.061074921814879835
Kappa statistic: 0.8877911647196038
Training time: 20.2
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 54.026761932947196
Incorrectly Classified Instances: 4.9649093138291125
Correctly Classified Instances: 95.03509068617089
Weighted Precision: 0.9511902490692773
Weighted AreaUnderROC: 0.986653844552011
Root mean squared error: 0.20338628441897075
Relative absolute error: 11.767282036879822
Root relative squared error: 40.67725688379415
Weighted TruePositiveRate: 0.9503509068617089
Weighted MatthewsCorrelation: 0.8961332900957496
Weighted FMeasure: 0.9503161524727675
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9859569231489201
Mean absolute error: 0.05883641018439911
Coverage of cases: 97.42738764638736
Instances selection time: 2.6
Test time: 5.4
Accumulative iteration time: 136.8
Weighted Recall: 0.9503509068617089
Weighted FalsePositiveRate: 0.05417576752081934
Kappa statistic: 0.895224350687317
Training time: 22.4
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 55.702764976958534
Incorrectly Classified Instances: 4.826660465902846
Correctly Classified Instances: 95.17333953409715
Weighted Precision: 0.9525134557569646
Weighted AreaUnderROC: 0.9863847988621425
Root mean squared error: 0.2004036960958055
Relative absolute error: 12.245547481073533
Root relative squared error: 40.080739219161096
Weighted TruePositiveRate: 0.9517333953409715
Weighted MatthewsCorrelation: 0.8988899124264714
Weighted FMeasure: 0.9516507383772714
Iteration time: 28.2
Weighted AreaUnderPRC: 0.9859656658660297
Mean absolute error: 0.061227737405367674
Coverage of cases: 97.79499429247876
Instances selection time: 3.7
Test time: 4.9
Accumulative iteration time: 165.0
Weighted Recall: 0.9517333953409715
Weighted FalsePositiveRate: 0.05375804652116082
Kappa statistic: 0.8979489541903745
Training time: 24.5
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 55.842282162939156
Incorrectly Classified Instances: 4.827717414281486
Correctly Classified Instances: 95.17228258571852
Weighted Precision: 0.9527349035983235
Weighted AreaUnderROC: 0.9853209008294685
Root mean squared error: 0.19392964404627638
Relative absolute error: 11.962717220762773
Root relative squared error: 38.78592880925528
Weighted TruePositiveRate: 0.9517228258571852
Weighted MatthewsCorrelation: 0.8995593183584789
Weighted FMeasure: 0.9518546688889182
Iteration time: 27.2
Weighted AreaUnderPRC: 0.9843250710822253
Mean absolute error: 0.059813586103813855
Coverage of cases: 97.93218619202636
Instances selection time: 3.6
Test time: 5.4
Accumulative iteration time: 192.2
Weighted Recall: 0.9517228258571852
Weighted FalsePositiveRate: 0.04757907094752292
Kappa statistic: 0.8988122980871234
Training time: 23.6
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 55.38219253371666
Incorrectly Classified Instances: 4.367099310869657
Correctly Classified Instances: 95.63290068913037
Weighted Precision: 0.9570421955617021
Weighted AreaUnderROC: 0.9864534878647294
Root mean squared error: 0.18907411718074554
Relative absolute error: 11.262061888847523
Root relative squared error: 37.81482343614911
Weighted TruePositiveRate: 0.9563290068913034
Weighted MatthewsCorrelation: 0.908781452139174
Weighted FMeasure: 0.9563861661654094
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9859806414235504
Mean absolute error: 0.056310309444237615
Coverage of cases: 97.84086585211179
Instances selection time: 3.1
Test time: 6.0
Accumulative iteration time: 221.2
Weighted Recall: 0.9563290068913034
Weighted FalsePositiveRate: 0.044244691625527036
Kappa statistic: 0.9081776670518256
Training time: 25.9
		
Time end:Fri Dec 01 13.20.10 EET 2017