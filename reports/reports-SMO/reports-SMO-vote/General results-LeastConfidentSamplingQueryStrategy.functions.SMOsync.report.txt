Fri Dec 01 13.19.49 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Fri Dec 01 13.19.49 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.21823870122184
Incorrectly Classified Instances: 7.263560647697966
Correctly Classified Instances: 92.73643935230203
Weighted Precision: 0.9306310942066149
Weighted AreaUnderROC: 0.9820587416874924
Root mean squared error: 0.25898489609795694
Relative absolute error: 14.51059464593549
Root relative squared error: 51.7969792195914
Weighted TruePositiveRate: 0.9273643935230205
Weighted MatthewsCorrelation: 0.8494949672120352
Weighted FMeasure: 0.9268014672007407
Iteration time: 22.9
Weighted AreaUnderPRC: 0.9800948857148892
Mean absolute error: 0.07255297322967744
Coverage of cases: 93.83947068025199
Instances selection time: 4.6
Test time: 5.5
Accumulative iteration time: 22.9
Weighted Recall: 0.9273643935230205
Weighted FalsePositiveRate: 0.0836899088037595
Kappa statistic: 0.8453659489416457
Training time: 18.3
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.84401555828013
Incorrectly Classified Instances: 5.838582843613918
Correctly Classified Instances: 94.16141715638608
Weighted Precision: 0.9431221215159244
Weighted AreaUnderROC: 0.9812463647111949
Root mean squared error: 0.22323123436374415
Relative absolute error: 12.908873483399526
Root relative squared error: 44.646246872748826
Weighted TruePositiveRate: 0.9416141715638607
Weighted MatthewsCorrelation: 0.8778530969958485
Weighted FMeasure: 0.9412452248108629
Iteration time: 14.5
Weighted AreaUnderPRC: 0.9768325308395077
Mean absolute error: 0.06454436741699762
Coverage of cases: 96.41821333446075
Instances selection time: 3.0
Test time: 4.8
Accumulative iteration time: 37.4
Weighted Recall: 0.9416141715638607
Weighted FalsePositiveRate: 0.070312783766008
Kappa statistic: 0.8756918211104668
Training time: 11.5
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 55.81733818120324
Incorrectly Classified Instances: 5.424893248213757
Correctly Classified Instances: 94.57510675178625
Weighted Precision: 0.9469560364154415
Weighted AreaUnderROC: 0.9900609399297817
Root mean squared error: 0.2063611529563177
Relative absolute error: 12.58158873495527
Root relative squared error: 41.27223059126354
Weighted TruePositiveRate: 0.9457510675178623
Weighted MatthewsCorrelation: 0.8865831127262658
Weighted FMeasure: 0.9455739515551205
Iteration time: 20.1
Weighted AreaUnderPRC: 0.9898865465340376
Mean absolute error: 0.06290794367477634
Coverage of cases: 97.9323975817021
Instances selection time: 2.8
Test time: 5.6
Accumulative iteration time: 57.5
Weighted Recall: 0.9457510675178623
Weighted FalsePositiveRate: 0.061058926718101834
Kappa statistic: 0.8850589577699001
Training time: 17.3
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 55.31148268718556
Incorrectly Classified Instances: 5.240561450978734
Correctly Classified Instances: 94.75943854902127
Weighted Precision: 0.9480804119638929
Weighted AreaUnderROC: 0.9902680809165771
Root mean squared error: 0.1995113476459806
Relative absolute error: 11.836561409096884
Root relative squared error: 39.90226952919612
Weighted TruePositiveRate: 0.9475943854902127
Weighted MatthewsCorrelation: 0.8897560656038428
Weighted FMeasure: 0.9474744695374036
Iteration time: 22.2
Weighted AreaUnderPRC: 0.990093021294092
Mean absolute error: 0.05918280704548442
Coverage of cases: 98.3904790090052
Instances selection time: 3.6
Test time: 5.6
Accumulative iteration time: 79.7
Weighted Recall: 0.9475943854902127
Weighted FalsePositiveRate: 0.06035576655481446
Kappa statistic: 0.8890320280054842
Training time: 18.6
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 55.748213757240094
Incorrectly Classified Instances: 5.103369551431107
Correctly Classified Instances: 94.8966304485689
Weighted Precision: 0.9496673020551656
Weighted AreaUnderROC: 0.9904840392559224
Root mean squared error: 0.1978955349168683
Relative absolute error: 11.99711754202885
Root relative squared error: 39.57910698337366
Weighted TruePositiveRate: 0.9489663044856889
Weighted MatthewsCorrelation: 0.8931770086221509
Weighted FMeasure: 0.9489882676863998
Iteration time: 20.6
Weighted AreaUnderPRC: 0.9902678410806349
Mean absolute error: 0.05998558771014424
Coverage of cases: 98.71200270578785
Instances selection time: 3.5
Test time: 4.7
Accumulative iteration time: 100.3
Weighted Recall: 0.9489663044856889
Weighted FalsePositiveRate: 0.05463065192144249
Kappa statistic: 0.8925216591366321
Training time: 17.1
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 56.45922293155202
Incorrectly Classified Instances: 5.103369551431108
Correctly Classified Instances: 94.8966304485689
Weighted Precision: 0.9498421959272255
Weighted AreaUnderROC: 0.991259231500927
Root mean squared error: 0.19464838960291764
Relative absolute error: 12.038591564614125
Root relative squared error: 38.92967792058353
Weighted TruePositiveRate: 0.9489663044856889
Weighted MatthewsCorrelation: 0.8934552780996189
Weighted FMeasure: 0.9490302952768447
Iteration time: 22.4
Weighted AreaUnderPRC: 0.9910329027362639
Mean absolute error: 0.060192957823070624
Coverage of cases: 98.94199467298017
Instances selection time: 3.1
Test time: 5.6
Accumulative iteration time: 122.7
Weighted Recall: 0.9489663044856889
Weighted FalsePositiveRate: 0.052859432828935085
Kappa statistic: 0.8927092086362775
Training time: 19.3
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 56.3205513042743
Incorrectly Classified Instances: 5.149452500739864
Correctly Classified Instances: 94.85054749926014
Weighted Precision: 0.9494737276625257
Weighted AreaUnderROC: 0.9917148615141265
Root mean squared error: 0.19418974692310947
Relative absolute error: 11.858755454174688
Root relative squared error: 38.83794938462189
Weighted TruePositiveRate: 0.9485054749926013
Weighted MatthewsCorrelation: 0.8926422856732537
Weighted FMeasure: 0.9485977825710158
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9914521007772142
Mean absolute error: 0.05929377727087344
Coverage of cases: 98.89591172367142
Instances selection time: 3.6
Test time: 4.4
Accumulative iteration time: 148.7
Weighted Recall: 0.9485054749926013
Weighted FalsePositiveRate: 0.0522676790101078
Kappa statistic: 0.8918595797339073
Training time: 22.4
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 56.115186234304325
Incorrectly Classified Instances: 4.781000295945546
Correctly Classified Instances: 95.21899970405445
Weighted Precision: 0.9530979791893353
Weighted AreaUnderROC: 0.991107432761007
Root mean squared error: 0.19059878483887688
Relative absolute error: 11.60146279483833
Root relative squared error: 38.119756967775366
Weighted TruePositiveRate: 0.9521899970405444
Weighted MatthewsCorrelation: 0.9004043924448233
Weighted FMeasure: 0.9523130832945229
Iteration time: 25.2
Weighted AreaUnderPRC: 0.9910893451672855
Mean absolute error: 0.058007313974191656
Coverage of cases: 98.6673994842092
Instances selection time: 3.0
Test time: 4.2
Accumulative iteration time: 173.9
Weighted Recall: 0.9521899970405444
Weighted FalsePositiveRate: 0.0472999734741659
Kappa statistic: 0.8997433580924502
Training time: 22.2
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 55.49560309474485
Incorrectly Classified Instances: 4.5970912780619795
Correctly Classified Instances: 95.40290872193802
Weighted Precision: 0.9550186730013115
Weighted AreaUnderROC: 0.9884543782163296
Root mean squared error: 0.19166531939433368
Relative absolute error: 11.411902073813776
Root relative squared error: 38.33306387886674
Weighted TruePositiveRate: 0.9540290872193802
Weighted MatthewsCorrelation: 0.9043089148016504
Weighted FMeasure: 0.9541206026227733
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9879970601124235
Mean absolute error: 0.05705951036906888
Coverage of cases: 98.30000422779351
Instances selection time: 2.8
Test time: 5.0
Accumulative iteration time: 201.9
Weighted Recall: 0.9540290872193802
Weighted FalsePositiveRate: 0.046137155898540796
Kappa statistic: 0.9035013682148136
Training time: 25.2
		
Time end:Fri Dec 01 13.19.56 EET 2017