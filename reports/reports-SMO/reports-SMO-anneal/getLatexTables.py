#!/usr/bin/python
import sys, getopt

def createTable(inputfile):
    f = open(str(inputfile),'r')
    alist = []
    for line in f:
        if line[0:9] == "Correctly":
#        print line[32:]
            alist.append(round(float(line[32:]),4))

    print '\\begin{table}[H]'
    print '\centering'
    print '\label{colic}'
    print '\\begin{tabular}{|l|l|l|l|l|l|}'
    print '\hline  & NB      & SMO     & IBk     & J48     & RF      \\\\ \hline'
    print 'Passive &         &         &         &         &         \\\\ \hline'
    lam = len(alist)
    for i in range(lam):
        if i%5 == 4:
            print alist[i], '\\\\ \hline'
        elif i == 0:
            print 'Entropy Sampling &', alist[i], '&',
        elif i == 5:
            print 'Least Confident Sampling &', alist[i], '&',
        elif i == 10:
            print 'Margin Sampling &', alist[i], '&',
        elif i == 15: 
            print 'Vote Entropy &', alist[i], '&',
        elif i == 20:
            print 'Kullback-Leibler &', alist[i], '&',
        elif i == 25:
            print 'Random Sampling &', alist[i], '&',
        else:
            print alist[i], '& ',
    print '\end{tabular}'
    print '\caption{\en{colic.arff}}'
    print '\end{table}'


def main(argv):
    inputfile = ''
    #outputfile = ''
    try:
       opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
       print 'test.py -i <inputfile>'
       sys.exit(2)
    for opt, arg in opts:
       if opt == '-h':
          print 'test.py -i <inputfile>'
          sys.exit()
       elif opt in ("-i", "--ifile"):
          inputfile = arg
#       elif opt in ("-o", "--ofile"):
#          outputfile = arg
#    print 'Input file is "', inputfile
#    print 'Output file is "', outputfile
    createTable(inputfile)

if __name__ == "__main__":
   main(sys.argv[1:])
