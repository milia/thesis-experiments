Mon Nov 27 15.36.45 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.36.45 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.55753526354864
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9240845582285651
Weighted AreaUnderROC: 0.9384640841529605
Root mean squared error: 0.15920463738986831
Relative absolute error: 10.560054523335765
Root relative squared error: 42.719086984434234
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7986994077636813
Weighted FMeasure: 0.9201463874202814
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9462844223852904
Mean absolute error: 0.0293334847870435
Coverage of cases: 92.87305122494432
Instances selection time: 74.0
Test time: 96.0
Accumulative iteration time: 169.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.14557872308111403
Kappa statistic: 0.8046577095329495
Training time: 95.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.7780252412769
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.952821727581309
Weighted AreaUnderROC: 0.9660083154893744
Root mean squared error: 0.13043337039125313
Relative absolute error: 6.694491752723847
Root relative squared error: 34.99894592751021
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8656964105675636
Weighted FMeasure: 0.9434604388377633
Iteration time: 193.0
Weighted AreaUnderPRC: 0.9679349356902011
Mean absolute error: 0.018595810424232723
Coverage of cases: 94.87750556792874
Instances selection time: 101.0
Test time: 141.0
Accumulative iteration time: 362.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.09533303942369505
Kappa statistic: 0.8692983344302132
Training time: 92.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9510903073999974
Weighted AreaUnderROC: 0.9819422072693608
Root mean squared error: 0.12977005451394438
Relative absolute error: 6.935981863125253
Root relative squared error: 34.82095960044414
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8696658144835407
Weighted FMeasure: 0.9483299132775033
Iteration time: 194.0
Weighted AreaUnderPRC: 0.9729978114172951
Mean absolute error: 0.019266616286458843
Coverage of cases: 95.10022271714922
Instances selection time: 95.0
Test time: 143.0
Accumulative iteration time: 556.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.08102436112869749
Kappa statistic: 0.8770567296442884
Training time: 99.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 17.780252412769098
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9571833292974877
Weighted AreaUnderROC: 0.9823301009105372
Root mean squared error: 0.11550319539449225
Relative absolute error: 6.406461819608389
Root relative squared error: 30.99275958246319
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.879349485896712
Weighted FMeasure: 0.952757743053137
Iteration time: 192.0
Weighted AreaUnderPRC: 0.9871645391621617
Mean absolute error: 0.017795727276689793
Coverage of cases: 97.55011135857461
Instances selection time: 89.0
Test time: 138.0
Accumulative iteration time: 748.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.06040194410481115
Kappa statistic: 0.8848239806513084
Training time: 103.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 17.11210096510762
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9653695916341153
Weighted AreaUnderROC: 0.9858202939023956
Root mean squared error: 0.10863000673503354
Relative absolute error: 5.429317847738775
Root relative squared error: 29.148489534695543
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8998898539535537
Weighted FMeasure: 0.9608854448196573
Iteration time: 197.0
Weighted AreaUnderPRC: 0.9869154552709533
Mean absolute error: 0.015081438465940893
Coverage of cases: 97.10467706013362
Instances selection time: 87.0
Test time: 147.0
Accumulative iteration time: 945.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.0322065466754928
Kappa statistic: 0.9031469453299141
Training time: 110.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9911994890310628
Root mean squared error: 0.09686363189111293
Relative absolute error: 4.449183432427614
Root relative squared error: 25.991239854725528
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 186.0
Weighted AreaUnderPRC: 0.9933649841680998
Mean absolute error: 0.01235884286785436
Coverage of cases: 97.55011135857461
Instances selection time: 78.0
Test time: 135.0
Accumulative iteration time: 1131.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 108.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9916581266555139
Root mean squared error: 0.09696476960913428
Relative absolute error: 4.442129482735416
Root relative squared error: 26.018377952235724
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9927109924256973
Mean absolute error: 0.012339248563153809
Coverage of cases: 97.55011135857461
Instances selection time: 70.0
Test time: 136.0
Accumulative iteration time: 1318.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 117.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9912411627175061
Root mean squared error: 0.0969689404221032
Relative absolute error: 4.437361967481576
Root relative squared error: 26.01949709879412
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9926777887296677
Mean absolute error: 0.012326005465226476
Coverage of cases: 97.55011135857461
Instances selection time: 64.0
Test time: 132.0
Accumulative iteration time: 1505.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 123.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9851742229272841
Root mean squared error: 0.09704219870235309
Relative absolute error: 4.443489132939082
Root relative squared error: 26.039154358140536
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9882054574364787
Mean absolute error: 0.012343025369275106
Coverage of cases: 97.55011135857461
Instances selection time: 57.0
Test time: 121.0
Accumulative iteration time: 1711.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 149.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9842177938404003
Root mean squared error: 0.09699940191555355
Relative absolute error: 4.444627907982639
Root relative squared error: 26.027670775200253
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 191.0
Weighted AreaUnderPRC: 0.9874553281954213
Mean absolute error: 0.012346188633284984
Coverage of cases: 97.55011135857461
Instances selection time: 52.0
Test time: 118.0
Accumulative iteration time: 1902.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 139.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9714706941500948
Weighted AreaUnderROC: 0.9845258044310999
Root mean squared error: 0.09642971990127483
Relative absolute error: 4.423973577774566
Root relative squared error: 25.87480905006191
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9202262397971408
Weighted FMeasure: 0.9691156369733839
Iteration time: 180.0
Weighted AreaUnderPRC: 0.9871154576182888
Mean absolute error: 0.012288815493818118
Coverage of cases: 97.55011135857461
Instances selection time: 46.0
Test time: 111.0
Accumulative iteration time: 2082.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.0310901699876708
Kappa statistic: 0.9236004764335546
Training time: 134.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 17.520415738678523
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9821064854472428
Weighted AreaUnderROC: 0.989992863434245
Root mean squared error: 0.06913938459343852
Relative absolute error: 3.668580917891223
Root relative squared error: 18.552043664811713
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9530977389056638
Weighted FMeasure: 0.981991858987177
Iteration time: 168.0
Weighted AreaUnderPRC: 0.9899749448845184
Mean absolute error: 0.01019050254969774
Coverage of cases: 99.33184855233853
Instances selection time: 37.0
Test time: 97.0
Accumulative iteration time: 2250.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.029156530303013344
Kappa statistic: 0.9554187558953482
Training time: 131.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 17.03786191536745
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9886941458743531
Weighted AreaUnderROC: 0.9973067237713819
Root mean squared error: 0.05245865811651458
Relative absolute error: 2.039911894730187
Root relative squared error: 14.076135066833793
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9704981890425319
Weighted FMeasure: 0.9884949338557781
Iteration time: 178.0
Weighted AreaUnderPRC: 0.995847759261756
Mean absolute error: 0.005666421929806018
Coverage of cases: 99.55456570155901
Instances selection time: 36.0
Test time: 106.0
Accumulative iteration time: 2428.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.028578322440071288
Kappa statistic: 0.9718141870684243
Training time: 142.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.37193763919821
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9852000862130901
Weighted AreaUnderROC: 0.996853017786933
Root mean squared error: 0.06794738773612044
Relative absolute error: 2.986284434944034
Root relative squared error: 18.232197344580193
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595783592357646
Weighted FMeasure: 0.9845574702015075
Iteration time: 306.0
Weighted AreaUnderPRC: 0.9943483017155045
Mean absolute error: 0.008295234541511123
Coverage of cases: 99.33184855233853
Instances selection time: 31.0
Test time: 107.0
Accumulative iteration time: 2734.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02177119205614512
Kappa statistic: 0.9611678074575603
Training time: 275.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 17.40905716406829
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9852000862130901
Weighted AreaUnderROC: 0.9970195352816773
Root mean squared error: 0.06810215600115753
Relative absolute error: 2.9745707810538065
Root relative squared error: 18.273726027946108
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595783592357646
Weighted FMeasure: 0.9845574702015075
Iteration time: 306.0
Weighted AreaUnderPRC: 0.9944230637493985
Mean absolute error: 0.008262696614038268
Coverage of cases: 99.33184855233853
Instances selection time: 27.0
Test time: 105.0
Accumulative iteration time: 3040.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02177119205614512
Kappa statistic: 0.9611678074575603
Training time: 279.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 17.446176688938376
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9852000862130901
Weighted AreaUnderROC: 0.9965456157018632
Root mean squared error: 0.06830582754862038
Relative absolute error: 2.9573891776083836
Root relative squared error: 18.32837683897125
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595783592357646
Weighted FMeasure: 0.9845574702015075
Iteration time: 244.0
Weighted AreaUnderPRC: 0.9942487413378757
Mean absolute error: 0.008214969937800983
Coverage of cases: 99.33184855233853
Instances selection time: 27.0
Test time: 107.0
Accumulative iteration time: 3284.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02177119205614512
Kappa statistic: 0.9611678074575603
Training time: 217.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 17.371937639198208
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9852000862130901
Weighted AreaUnderROC: 0.9967227029426693
Root mean squared error: 0.06745146842637469
Relative absolute error: 2.89076978812182
Root relative squared error: 18.099128230026636
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595783592357646
Weighted FMeasure: 0.9845574702015075
Iteration time: 199.0
Weighted AreaUnderPRC: 0.9945433303750846
Mean absolute error: 0.008029916078116086
Coverage of cases: 99.33184855233853
Instances selection time: 24.0
Test time: 102.0
Accumulative iteration time: 3483.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02177119205614512
Kappa statistic: 0.9611678074575603
Training time: 175.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 17.334818114328126
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9852000862130901
Weighted AreaUnderROC: 0.996909710318976
Root mean squared error: 0.06900594103667132
Relative absolute error: 3.018826493974101
Root relative squared error: 18.516237001120814
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9595783592357646
Weighted FMeasure: 0.9845574702015075
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9938580355572993
Mean absolute error: 0.008385629149927975
Coverage of cases: 99.10913140311804
Instances selection time: 13.0
Test time: 113.0
Accumulative iteration time: 3670.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02177119205614512
Kappa statistic: 0.9611678074575603
Training time: 174.0
		
Time end:Mon Nov 27 15.36.52 EET 2017