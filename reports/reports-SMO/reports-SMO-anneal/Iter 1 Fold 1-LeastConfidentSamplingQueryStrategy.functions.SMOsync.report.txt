Mon Nov 27 15.35.44 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.35.44 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.334818114328115
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.884653520789327
Weighted AreaUnderROC: 0.8932049356678003
Root mean squared error: 0.15653123606588062
Relative absolute error: 10.06337286913639
Root relative squared error: 42.00173813344531
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7885030302572898
Weighted FMeasure: 0.9016562182908775
Iteration time: 231.0
Weighted AreaUnderPRC: 0.898037760050146
Mean absolute error: 0.027953813525378585
Coverage of cases: 93.3184855233853
Instances selection time: 99.0
Test time: 132.0
Accumulative iteration time: 231.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.16023792804242773
Kappa statistic: 0.7927601213240144
Training time: 132.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 17.223459539717897
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8833133458968648
Weighted AreaUnderROC: 0.8474890446380898
Root mean squared error: 0.1497667405274628
Relative absolute error: 9.871037237763613
Root relative squared error: 40.18663351055775
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.773728712353113
Weighted FMeasure: 0.9032809628905528
Iteration time: 138.0
Weighted AreaUnderPRC: 0.8673512478610323
Mean absolute error: 0.02741954788267643
Coverage of cases: 94.43207126948775
Instances selection time: 74.0
Test time: 95.0
Accumulative iteration time: 369.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.1873169487839241
Kappa statistic: 0.7958435861773832
Training time: 64.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.96362286562732
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.8943446177823224
Weighted AreaUnderROC: 0.8765660750384963
Root mean squared error: 0.14300361169209616
Relative absolute error: 8.34309787450732
Root relative squared error: 38.371895612581476
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8053410876876319
Weighted FMeasure: 0.914310566843138
Iteration time: 156.0
Weighted AreaUnderPRC: 0.8980510812644538
Mean absolute error: 0.023175271873631217
Coverage of cases: 94.20935412026726
Instances selection time: 76.0
Test time: 114.0
Accumulative iteration time: 525.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15856336301069474
Kappa statistic: 0.8278829376619258
Training time: 80.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8924773082326697
Weighted AreaUnderROC: 0.8845171420441162
Root mean squared error: 0.14321216011782997
Relative absolute error: 8.24362436659442
Root relative squared error: 38.42785502736644
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.7994718171306185
Weighted FMeasure: 0.912213537787925
Iteration time: 206.0
Weighted AreaUnderPRC: 0.9019135733216219
Mean absolute error: 0.02289895657387316
Coverage of cases: 94.20935412026726
Instances selection time: 137.0
Test time: 205.0
Accumulative iteration time: 731.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15884245718265025
Kappa statistic: 0.8226324660275992
Training time: 69.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8924773082326697
Weighted AreaUnderROC: 0.8770414715856135
Root mean squared error: 0.14323719979129082
Relative absolute error: 8.247447403449355
Root relative squared error: 38.43457387680559
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.7994718171306185
Weighted FMeasure: 0.912213537787925
Iteration time: 138.0
Weighted AreaUnderPRC: 0.9039238999192729
Mean absolute error: 0.022909576120692426
Coverage of cases: 94.20935412026726
Instances selection time: 66.0
Test time: 100.0
Accumulative iteration time: 869.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15884245718265025
Kappa statistic: 0.8226324660275992
Training time: 72.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.852264291017065
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.8924773082326697
Weighted AreaUnderROC: 0.8741581236298139
Root mean squared error: 0.1434716473079734
Relative absolute error: 8.332606655029018
Root relative squared error: 38.49748274694058
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.7994718171306185
Weighted FMeasure: 0.912213537787925
Iteration time: 144.0
Weighted AreaUnderPRC: 0.9018897440305497
Mean absolute error: 0.023146129597302598
Coverage of cases: 94.20935412026726
Instances selection time: 65.0
Test time: 100.0
Accumulative iteration time: 1013.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.15884245718265025
Kappa statistic: 0.8226324660275992
Training time: 79.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.889241808988138
Weighted AreaUnderROC: 0.8569731076569798
Root mean squared error: 0.14844439323114925
Relative absolute error: 8.857479582642931
Root relative squared error: 39.831810497227316
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.7883637799821963
Weighted FMeasure: 0.9081489553160134
Iteration time: 134.0
Weighted AreaUnderPRC: 0.8819320424328542
Mean absolute error: 0.024604109951785674
Coverage of cases: 93.76391982182628
Instances selection time: 55.0
Test time: 98.0
Accumulative iteration time: 1147.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.15888249622071235
Kappa statistic: 0.8124771600104412
Training time: 79.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.889241808988138
Weighted AreaUnderROC: 0.8603375710276654
Root mean squared error: 0.14832931234693908
Relative absolute error: 8.810430776892506
Root relative squared error: 39.800931055626755
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.7883637799821963
Weighted FMeasure: 0.9081489553160134
Iteration time: 140.0
Weighted AreaUnderPRC: 0.884398084923928
Mean absolute error: 0.024473418824701162
Coverage of cases: 93.76391982182628
Instances selection time: 55.0
Test time: 114.0
Accumulative iteration time: 1287.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.15888249622071235
Kappa statistic: 0.8124771600104412
Training time: 85.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.889241808988138
Weighted AreaUnderROC: 0.8579030036827632
Root mean squared error: 0.14830898111961296
Relative absolute error: 8.809014491417905
Root relative squared error: 39.79547561486269
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.7883637799821963
Weighted FMeasure: 0.9081489553160134
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8852094199960246
Mean absolute error: 0.024469484698382823
Coverage of cases: 93.76391982182628
Instances selection time: 53.0
Test time: 109.0
Accumulative iteration time: 1426.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.15888249622071235
Kappa statistic: 0.8124771600104412
Training time: 86.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9432487334491789
Weighted AreaUnderROC: 0.9041316695917783
Root mean squared error: 0.1350988899486408
Relative absolute error: 7.501970887799479
Root relative squared error: 36.25083619319105
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8334658393295052
Weighted FMeasure: 0.9291326631747694
Iteration time: 168.0
Weighted AreaUnderPRC: 0.9121677301981599
Mean absolute error: 0.020838808021665014
Coverage of cases: 95.10022271714922
Instances selection time: 49.0
Test time: 110.0
Accumulative iteration time: 1594.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.13038798510040742
Kappa statistic: 0.8437153538739202
Training time: 119.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.88938381588715
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9540225943277302
Weighted AreaUnderROC: 0.9702195224156362
Root mean squared error: 0.12038299682678287
Relative absolute error: 6.132447216001701
Root relative squared error: 32.302147708779515
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8755220939615015
Weighted FMeasure: 0.9517880653744579
Iteration time: 178.0
Weighted AreaUnderPRC: 0.9623686246156338
Mean absolute error: 0.017034575600004556
Coverage of cases: 96.21380846325167
Instances selection time: 49.0
Test time: 114.0
Accumulative iteration time: 1772.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.052622306909922534
Kappa statistic: 0.8810739224656874
Training time: 129.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 17.40905716406828
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9529112313076677
Weighted AreaUnderROC: 0.9794583135164773
Root mean squared error: 0.11523146877075963
Relative absolute error: 6.541562199107369
Root relative squared error: 30.919847678227672
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8705047871859422
Weighted FMeasure: 0.9492940286590746
Iteration time: 175.0
Weighted AreaUnderPRC: 0.9709840093342909
Mean absolute error: 0.0181710061086314
Coverage of cases: 97.32739420935413
Instances selection time: 39.0
Test time: 105.0
Accumulative iteration time: 1947.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.08068147515600667
Kappa statistic: 0.8772675314348193
Training time: 136.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 17.483296213808455
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9675456595558485
Weighted AreaUnderROC: 0.9776696378019215
Root mean squared error: 0.1009752533504178
Relative absolute error: 5.403437695427989
Root relative squared error: 27.094503664415846
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9098816644624556
Weighted FMeasure: 0.9652301655049254
Iteration time: 214.0
Weighted AreaUnderPRC: 0.9608257331513037
Mean absolute error: 0.015009549153966488
Coverage of cases: 97.99554565701558
Instances selection time: 64.0
Test time: 118.0
Accumulative iteration time: 2161.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.03095494569257484
Kappa statistic: 0.9135083072477727
Training time: 150.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.483296213808455
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9675456595558485
Weighted AreaUnderROC: 0.9788919874643198
Root mean squared error: 0.1009478990911067
Relative absolute error: 5.401350930091251
Root relative squared error: 27.08716374642059
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9098816644624556
Weighted FMeasure: 0.9652301655049254
Iteration time: 297.0
Weighted AreaUnderPRC: 0.9717248557968814
Mean absolute error: 0.01500375258358666
Coverage of cases: 97.99554565701558
Instances selection time: 32.0
Test time: 112.0
Accumulative iteration time: 2458.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.03095494569257484
Kappa statistic: 0.9135083072477727
Training time: 265.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 17.483296213808455
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9675456595558485
Weighted AreaUnderROC: 0.9777968679719752
Root mean squared error: 0.10097223807798299
Relative absolute error: 5.404484109495344
Root relative squared error: 27.093694581919653
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9098816644624556
Weighted FMeasure: 0.9652301655049254
Iteration time: 291.0
Weighted AreaUnderPRC: 0.9641205494454721
Mean absolute error: 0.015012455859709142
Coverage of cases: 97.99554565701558
Instances selection time: 28.0
Test time: 109.0
Accumulative iteration time: 2749.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.03095494569257484
Kappa statistic: 0.9135083072477727
Training time: 263.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 17.483296213808455
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9675456595558485
Weighted AreaUnderROC: 0.9782703657083415
Root mean squared error: 0.10319479394756995
Relative absolute error: 5.51777530332083
Root relative squared error: 27.69006890290217
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9098816644624556
Weighted FMeasure: 0.9652301655049254
Iteration time: 285.0
Weighted AreaUnderPRC: 0.9663306015395359
Mean absolute error: 0.015327153620335487
Coverage of cases: 97.99554565701558
Instances selection time: 25.0
Test time: 106.0
Accumulative iteration time: 3034.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.03095494569257484
Kappa statistic: 0.9135083072477727
Training time: 260.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 17.63177431328878
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9675456595558485
Weighted AreaUnderROC: 0.9785447630141263
Root mean squared error: 0.10387971672114649
Relative absolute error: 5.718967587977452
Root relative squared error: 27.87385296862875
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9098816644624556
Weighted FMeasure: 0.9652301655049254
Iteration time: 287.0
Weighted AreaUnderPRC: 0.9695688498084704
Mean absolute error: 0.015886021077714984
Coverage of cases: 97.99554565701558
Instances selection time: 22.0
Test time: 104.0
Accumulative iteration time: 3321.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.03095494569257484
Kappa statistic: 0.9135083072477727
Training time: 265.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 17.85449146250928
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9770380667393985
Weighted AreaUnderROC: 0.9835401024173949
Root mean squared error: 0.09425909345658141
Relative absolute error: 5.509562750601451
Root relative squared error: 25.29236885597072
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.9366887377087836
Weighted FMeasure: 0.9759477135865425
Iteration time: 283.0
Weighted AreaUnderPRC: 0.9828299987797694
Mean absolute error: 0.015304340973892766
Coverage of cases: 98.66369710467706
Instances selection time: 11.0
Test time: 102.0
Accumulative iteration time: 3604.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.02955947483279733
Kappa statistic: 0.9394730392156863
Training time: 272.0
		
Time end:Mon Nov 27 15.35.51 EET 2017