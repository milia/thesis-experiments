Mon Nov 27 15.35.36 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.35.36 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.55753526354864
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9240845582285651
Weighted AreaUnderROC: 0.9384640841529605
Root mean squared error: 0.15920463738986831
Relative absolute error: 10.560054523335765
Root relative squared error: 42.719086984434234
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7986994077636813
Weighted FMeasure: 0.9201463874202814
Iteration time: 242.0
Weighted AreaUnderPRC: 0.9462844223852904
Mean absolute error: 0.0293334847870435
Coverage of cases: 92.87305122494432
Instances selection time: 147.0
Test time: 97.0
Accumulative iteration time: 242.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.14557872308111403
Kappa statistic: 0.8046577095329495
Training time: 95.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.778025241276904
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9409301334008571
Weighted AreaUnderROC: 0.9708446544004474
Root mean squared error: 0.13848625955910493
Relative absolute error: 7.528226957798578
Root relative squared error: 37.15976283886082
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8495629853566578
Weighted FMeasure: 0.9387795131843236
Iteration time: 324.0
Weighted AreaUnderPRC: 0.9689366159275035
Mean absolute error: 0.02091174154944029
Coverage of cases: 94.20935412026726
Instances selection time: 221.0
Test time: 154.0
Accumulative iteration time: 566.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.08862973987856632
Kappa statistic: 0.8552008136736872
Training time: 103.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 17.706013363028948
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9450290834890517
Weighted AreaUnderROC: 0.9714344331839552
Root mean squared error: 0.1446875998041165
Relative absolute error: 9.17800263955383
Root relative squared error: 38.82375703959496
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8365923431458542
Weighted FMeasure: 0.9328899625637476
Iteration time: 335.0
Weighted AreaUnderPRC: 0.9693111072466195
Mean absolute error: 0.02549445177653816
Coverage of cases: 95.10022271714922
Instances selection time: 237.0
Test time: 169.0
Accumulative iteration time: 901.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.04049970714701981
Kappa statistic: 0.8348543711638814
Training time: 98.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 17.037861915367472
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9436776863480729
Weighted AreaUnderROC: 0.973698396209853
Root mean squared error: 0.13665540283041022
Relative absolute error: 7.516091494743452
Root relative squared error: 36.66849242656991
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8537353131016046
Weighted FMeasure: 0.9416561057932158
Iteration time: 304.0
Weighted AreaUnderPRC: 0.9720305281224307
Mean absolute error: 0.02087803192984271
Coverage of cases: 94.87750556792874
Instances selection time: 201.0
Test time: 157.0
Accumulative iteration time: 1205.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.07407436575605003
Kappa statistic: 0.8576341463414635
Training time: 103.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.815144766146982
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579518799743437
Weighted AreaUnderROC: 0.9791941864943637
Root mean squared error: 0.11942186254079862
Relative absolute error: 5.78913727993049
Root relative squared error: 32.044248316903534
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824601518282472
Weighted FMeasure: 0.9497114641315086
Iteration time: 281.0
Weighted AreaUnderPRC: 0.9715075268342711
Mean absolute error: 0.016080936888695645
Coverage of cases: 95.99109131403118
Instances selection time: 172.0
Test time: 144.0
Accumulative iteration time: 1486.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09449575690782855
Kappa statistic: 0.8850721818367975
Training time: 109.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.889383815887143
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579518799743437
Weighted AreaUnderROC: 0.980763912483943
Root mean squared error: 0.12009204668311335
Relative absolute error: 5.842335870133174
Root relative squared error: 32.224077592862514
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824601518282472
Weighted FMeasure: 0.9497114641315086
Iteration time: 290.0
Weighted AreaUnderPRC: 0.9697296831209931
Mean absolute error: 0.016228710750369767
Coverage of cases: 95.99109131403118
Instances selection time: 153.0
Test time: 134.0
Accumulative iteration time: 1776.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09449575690782855
Kappa statistic: 0.8850721818367975
Training time: 137.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 17.03786191536747
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9491702302435786
Weighted AreaUnderROC: 0.9849657431975278
Root mean squared error: 0.11886290297985426
Relative absolute error: 6.000397851087318
Root relative squared error: 31.89426372791013
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8723607359189454
Weighted FMeasure: 0.9480527237834959
Iteration time: 377.0
Weighted AreaUnderPRC: 0.9737758891532706
Mean absolute error: 0.016667771808575718
Coverage of cases: 96.21380846325167
Instances selection time: 261.0
Test time: 128.0
Accumulative iteration time: 2153.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.08067383446238134
Kappa statistic: 0.8766298646150772
Training time: 116.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 17.706013363028962
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9474162433953289
Weighted AreaUnderROC: 0.9469563313301983
Root mean squared error: 0.12655464647832143
Relative absolute error: 7.4243049543404105
Root relative squared error: 33.9581750872779
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8493855499008651
Weighted FMeasure: 0.9371131538972047
Iteration time: 250.0
Weighted AreaUnderPRC: 0.955088057120314
Mean absolute error: 0.020623069317612045
Coverage of cases: 96.88195991091314
Instances selection time: 113.0
Test time: 118.0
Accumulative iteration time: 2403.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.09606790242924731
Kappa statistic: 0.8539015080407986
Training time: 137.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 17.520415738678537
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.949267008739575
Weighted AreaUnderROC: 0.9665346548862084
Root mean squared error: 0.12284442328040525
Relative absolute error: 6.723103433716038
Root relative squared error: 32.962617733409424
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8660756356245889
Weighted FMeasure: 0.9434567733148175
Iteration time: 223.0
Weighted AreaUnderPRC: 0.968754806057468
Mean absolute error: 0.018675287315877698
Coverage of cases: 97.32739420935413
Instances selection time: 102.0
Test time: 117.0
Accumulative iteration time: 2626.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.0747134450567751
Kappa statistic: 0.8667787558105033
Training time: 121.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.225686711210095
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9539395764418023
Weighted AreaUnderROC: 0.9499622934798867
Root mean squared error: 0.112641849105405
Relative absolute error: 6.365276533256038
Root relative squared error: 30.224979805315282
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8953859106218043
Weighted FMeasure: 0.9502798192128781
Iteration time: 212.0
Weighted AreaUnderPRC: 0.9538931138340384
Mean absolute error: 0.017681323703488818
Coverage of cases: 97.7728285077951
Instances selection time: 91.0
Test time: 106.0
Accumulative iteration time: 2838.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.07346648449965161
Kappa statistic: 0.8805578779356997
Training time: 121.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.926503340757225
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9544340814213951
Weighted AreaUnderROC: 0.965956596310547
Root mean squared error: 0.12426485970008423
Relative absolute error: 6.308118346829664
Root relative squared error: 33.34376082046366
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8724879429865663
Weighted FMeasure: 0.9491099883427165
Iteration time: 217.0
Weighted AreaUnderPRC: 0.9625200069903932
Mean absolute error: 0.01752255096341556
Coverage of cases: 95.5456570155902
Instances selection time: 84.0
Test time: 108.0
Accumulative iteration time: 3055.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1221329659932359
Kappa statistic: 0.876654806132594
Training time: 133.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.96362286562731
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9497730788183335
Weighted AreaUnderROC: 0.9651445325272167
Root mean squared error: 0.12740925277004628
Relative absolute error: 6.656272695983884
Root relative squared error: 34.187490018753394
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8609123627559064
Weighted FMeasure: 0.9453672453816658
Iteration time: 314.0
Weighted AreaUnderPRC: 0.9604784746308913
Mean absolute error: 0.01848964637773283
Coverage of cases: 95.32293986636971
Instances selection time: 125.0
Test time: 104.0
Accumulative iteration time: 3369.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12217300503129801
Kappa statistic: 0.8660918049792532
Training time: 189.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 17.260579064587972
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9543315715741637
Weighted AreaUnderROC: 0.954614761906005
Root mean squared error: 0.12446924442703548
Relative absolute error: 6.813303916967329
Root relative squared error: 33.398602997626746
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.873041486667226
Weighted FMeasure: 0.9497125283340472
Iteration time: 374.0
Weighted AreaUnderPRC: 0.958430973011363
Mean absolute error: 0.01892584421379795
Coverage of cases: 95.99109131403118
Instances selection time: 68.0
Test time: 97.0
Accumulative iteration time: 3743.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.121614816687387
Kappa statistic: 0.8768030730636565
Training time: 306.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.14922048997772
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.958990258663291
Weighted AreaUnderROC: 0.944504285211913
Root mean squared error: 0.1172559183525684
Relative absolute error: 6.072750240285834
Root relative squared error: 31.46306450406113
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8848908412051516
Weighted FMeasure: 0.9534423501965785
Iteration time: 203.0
Weighted AreaUnderPRC: 0.9519605302259174
Mean absolute error: 0.01686875066746048
Coverage of cases: 95.99109131403118
Instances selection time: 60.0
Test time: 94.0
Accumulative iteration time: 3946.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.12157477764932488
Kappa statistic: 0.8875428420775111
Training time: 143.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 17.000742390497386
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9639309887028432
Weighted AreaUnderROC: 0.926993978043296
Root mean squared error: 0.11387481806283116
Relative absolute error: 5.832409332605504
Root relative squared error: 30.555820093669517
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8977345497186676
Weighted FMeasure: 0.9578497887456188
Iteration time: 354.0
Weighted AreaUnderPRC: 0.9351366004992722
Mean absolute error: 0.016201137035015127
Coverage of cases: 96.21380846325167
Instances selection time: 51.0
Test time: 86.0
Accumulative iteration time: 4300.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.12101658930541388
Kappa statistic: 0.8985998193315267
Training time: 303.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.963622865627304
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9639309887028432
Weighted AreaUnderROC: 0.9239297575105141
Root mean squared error: 0.11384766879496025
Relative absolute error: 5.816660832293362
Root relative squared error: 30.548535180649672
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8977345497186676
Weighted FMeasure: 0.9578497887456188
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9301896142632897
Mean absolute error: 0.016157391200814733
Coverage of cases: 96.21380846325167
Instances selection time: 44.0
Test time: 86.0
Accumulative iteration time: 4504.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.12101658930541388
Kappa statistic: 0.8985998193315267
Training time: 160.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.974179910158167
Weighted AreaUnderROC: 0.9918043407792175
Root mean squared error: 0.09398516589887977
Relative absolute error: 3.6750231217582594
Root relative squared error: 25.21886637917896
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9277183330049208
Weighted FMeasure: 0.969049996160049
Iteration time: 242.0
Weighted AreaUnderPRC: 0.9850921838871123
Mean absolute error: 0.010208397560439508
Coverage of cases: 97.32739420935413
Instances selection time: 68.0
Test time: 193.0
Accumulative iteration time: 4746.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.08542347480382158
Kappa statistic: 0.9297852376980819
Training time: 174.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.974179910158167
Weighted AreaUnderROC: 0.9874536244173275
Root mean squared error: 0.09397647437613184
Relative absolute error: 3.6748208427532933
Root relative squared error: 25.216534198895875
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9277183330049208
Weighted FMeasure: 0.969049996160049
Iteration time: 198.0
Weighted AreaUnderPRC: 0.9813839632630443
Mean absolute error: 0.010207835674314603
Coverage of cases: 97.32739420935413
Instances selection time: 17.0
Test time: 110.0
Accumulative iteration time: 4944.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.08542347480382158
Kappa statistic: 0.9297852376980819
Training time: 181.0
		
Time end:Mon Nov 27 15.35.44 EET 2017