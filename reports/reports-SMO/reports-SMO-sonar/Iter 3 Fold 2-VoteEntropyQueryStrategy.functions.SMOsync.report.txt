Thu Nov 30 23.24.14 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Thu Nov 30 23.24.14 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 57.69230769230769
Incorrectly Classified Instances: 33.65384615384615
Correctly Classified Instances: 66.34615384615384
Weighted Precision: 0.6676540850453894
Weighted AreaUnderROC: 0.6980025183150182
Root mean squared error: 0.564005968853986
Relative absolute error: 68.60142801661914
Root relative squared error: 112.8011937707972
Weighted TruePositiveRate: 0.6634615384615384
Weighted MatthewsCorrelation: 0.32027051564938225
Weighted FMeasure: 0.6546061167747914
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6742135507455413
Mean absolute error: 0.3430071400830957
Coverage of cases: 72.11538461538461
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6634615384615384
Weighted FalsePositiveRate: 0.3598901098901099
Kappa statistic: 0.3095599393019727
Training time: 25.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 58.65384615384615
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7223264540337712
Weighted AreaUnderROC: 0.7942708333333334
Root mean squared error: 0.5059486180772769
Relative absolute error: 57.73793441883856
Root relative squared error: 101.18972361545538
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.43719546038628915
Weighted FMeasure: 0.7183893436371955
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8070339291644129
Mean absolute error: 0.28868967209419283
Coverage of cases: 79.8076923076923
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2925824175824176
Kappa statistic: 0.43308270676691724
Training time: 24.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 62.01923076923077
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7115384615384616
Weighted AreaUnderROC: 0.7976190476190477
Root mean squared error: 0.49782059392462674
Relative absolute error: 57.750156571806514
Root relative squared error: 99.56411878492534
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4196428571428571
Weighted FMeasure: 0.7115384615384616
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7962804374738107
Mean absolute error: 0.28875078285903255
Coverage of cases: 83.65384615384616
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 84.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.2918956043956044
Kappa statistic: 0.41964285714285715
Training time: 29.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 72.11538461538461
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7594089526292916
Weighted AreaUnderROC: 0.8571428571428571
Root mean squared error: 0.43140654475234225
Relative absolute error: 51.18681969020271
Root relative squared error: 86.28130895046844
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5150762664099506
Weighted FMeasure: 0.7588736649045205
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8657075932908824
Mean absolute error: 0.25593409845101356
Coverage of cases: 96.15384615384616
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 112.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.24771062271062275
Kappa statistic: 0.5142002989536621
Training time: 25.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 71.63461538461539
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7787918296392873
Weighted AreaUnderROC: 0.8467261904761904
Root mean squared error: 0.42117380595429177
Relative absolute error: 50.28275191226631
Root relative squared error: 84.23476119085835
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5540064493362841
Weighted FMeasure: 0.7781637717121589
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8568796885431744
Mean absolute error: 0.25141375956133155
Coverage of cases: 92.3076923076923
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 139.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.22825091575091574
Kappa statistic: 0.5530642750373692
Training time: 26.0
		
Time end:Thu Nov 30 23.24.15 EET 2017