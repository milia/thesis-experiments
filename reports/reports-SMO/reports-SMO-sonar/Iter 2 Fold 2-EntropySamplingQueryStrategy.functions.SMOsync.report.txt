Thu Nov 30 23.23.49 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Thu Nov 30 23.23.49 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 53.36538461538461
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6557382847038019
Weighted AreaUnderROC: 0.7041695283882785
Root mean squared error: 0.585157707952018
Relative absolute error: 70.94588607102655
Root relative squared error: 117.0315415904036
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.28454055573833337
Weighted FMeasure: 0.626137221557069
Iteration time: 48.0
Weighted AreaUnderPRC: 0.6733585968077082
Mean absolute error: 0.35472943035513277
Coverage of cases: 68.26923076923077
Instances selection time: 13.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.38827838827838834
Kappa statistic: 0.2633996937212865
Training time: 35.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 51.44230769230769
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7206431986093004
Weighted AreaUnderROC: 0.7832245879120878
Root mean squared error: 0.5269435256788971
Relative absolute error: 56.239826156288494
Root relative squared error: 105.38870513577943
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4372159005572837
Weighted FMeasure: 0.7202934512892438
Iteration time: 47.0
Weighted AreaUnderPRC: 0.7631540469474405
Mean absolute error: 0.2811991307814425
Coverage of cases: 73.07692307692308
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 95.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2866300366300366
Kappa statistic: 0.43647234678624813
Training time: 44.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 68.75
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.75
Weighted AreaUnderROC: 0.8225446428571429
Root mean squared error: 0.46032137762277486
Relative absolute error: 54.278906794044865
Root relative squared error: 92.06427552455497
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.49702380952380953
Weighted FMeasure: 0.75
Iteration time: 49.0
Weighted AreaUnderPRC: 0.8271081762860821
Mean absolute error: 0.27139453397022434
Coverage of cases: 88.46153846153847
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 144.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.25297619047619047
Kappa statistic: 0.49702380952380953
Training time: 41.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 72.59615384615384
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7637952132957684
Weighted AreaUnderROC: 0.8325892857142858
Root mean squared error: 0.4375124393707133
Relative absolute error: 54.0634347717316
Root relative squared error: 87.50248787414266
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5223540902551571
Weighted FMeasure: 0.7599490358813021
Iteration time: 72.0
Weighted AreaUnderPRC: 0.8404562853312002
Mean absolute error: 0.270317173858658
Coverage of cases: 93.26923076923077
Instances selection time: 7.0
Test time: 20.0
Accumulative iteration time: 216.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.23580586080586083
Kappa statistic: 0.519940915805022
Training time: 65.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 83.17307692307692
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7593533752548309
Weighted AreaUnderROC: 0.8433779761904762
Root mean squared error: 0.41478538381778834
Relative absolute error: 55.33461638697488
Root relative squared error: 82.95707676355767
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5157450888530956
Weighted FMeasure: 0.7594138511697897
Iteration time: 48.0
Weighted AreaUnderPRC: 0.8491648865122531
Mean absolute error: 0.2766730819348744
Coverage of cases: 96.15384615384616
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 264.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.2447344322344322
Kappa statistic: 0.5156482861400893
Training time: 47.0
		
Time end:Thu Nov 30 23.23.49 EET 2017