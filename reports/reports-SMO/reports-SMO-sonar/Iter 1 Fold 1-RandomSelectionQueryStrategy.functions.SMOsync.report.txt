Thu Nov 30 23.24.08 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Thu Nov 30 23.24.08 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 53.36538461538461
Incorrectly Classified Instances: 28.846153846153847
Correctly Classified Instances: 71.15384615384616
Weighted Precision: 0.7183257918552036
Weighted AreaUnderROC: 0.7448489010989012
Root mean squared error: 0.5298194842171722
Relative absolute error: 58.96212558061301
Root relative squared error: 105.96389684343444
Weighted TruePositiveRate: 0.7115384615384616
Weighted MatthewsCorrelation: 0.4210206685678799
Weighted FMeasure: 0.7048918823112373
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7162439989534686
Mean absolute error: 0.29481062790306506
Coverage of cases: 74.03846153846153
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.7115384615384616
Weighted FalsePositiveRate: 0.30975274725274726
Kappa statistic: 0.4090909090909092
Training time: 44.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 66.82692307692308
Incorrectly Classified Instances: 32.69230769230769
Correctly Classified Instances: 67.3076923076923
Weighted Precision: 0.6723561296274939
Weighted AreaUnderROC: 0.7734375
Root mean squared error: 0.5164128790078722
Relative absolute error: 63.37675852322901
Root relative squared error: 103.28257580157445
Weighted TruePositiveRate: 0.6730769230769231
Weighted MatthewsCorrelation: 0.34055502554360106
Weighted FMeasure: 0.6724667642920722
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7877577080706791
Mean absolute error: 0.316883792616145
Coverage of cases: 85.57692307692308
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6730769230769231
Weighted FalsePositiveRate: 0.3337912087912088
Kappa statistic: 0.3402985074626867
Training time: 17.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 66.34615384615384
Incorrectly Classified Instances: 24.03846153846154
Correctly Classified Instances: 75.96153846153847
Weighted Precision: 0.7593533752548309
Weighted AreaUnderROC: 0.8188244047619049
Root mean squared error: 0.44868007526666426
Relative absolute error: 51.293079914381224
Root relative squared error: 89.73601505333285
Weighted TruePositiveRate: 0.7596153846153846
Weighted MatthewsCorrelation: 0.5157450888530956
Weighted FMeasure: 0.7594138511697897
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8129054802624954
Mean absolute error: 0.2564653995719061
Coverage of cases: 89.42307692307692
Instances selection time: 12.0
Test time: 6.0
Accumulative iteration time: 111.0
Weighted Recall: 0.7596153846153846
Weighted FalsePositiveRate: 0.2447344322344322
Kappa statistic: 0.5156482861400893
Training time: 26.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 71.15384615384616
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7862137862137862
Weighted AreaUnderROC: 0.84375
Root mean squared error: 0.41777256445903815
Relative absolute error: 49.42873235228149
Root relative squared error: 83.55451289180763
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5647413283197535
Weighted FMeasure: 0.77898929549415
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8323927454097965
Mean absolute error: 0.24714366176140748
Coverage of cases: 92.3076923076923
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 137.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.21336996336996336
Kappa statistic: 0.5596465390279823
Training time: 20.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 76.4423076923077
Incorrectly Classified Instances: 21.153846153846153
Correctly Classified Instances: 78.84615384615384
Weighted Precision: 0.7941880341880343
Weighted AreaUnderROC: 0.8218005952380951
Root mean squared error: 0.4179429706504604
Relative absolute error: 51.204931688365775
Root relative squared error: 83.58859413009208
Weighted TruePositiveRate: 0.7884615384615384
Weighted MatthewsCorrelation: 0.5820355934981687
Weighted FMeasure: 0.7886963203278408
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7867555160443442
Mean absolute error: 0.25602465844182887
Coverage of cases: 93.26923076923077
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7884615384615384
Weighted FalsePositiveRate: 0.20512820512820512
Kappa statistic: 0.5781710914454277
Training time: 27.0
		
Time end:Thu Nov 30 23.24.08 EET 2017