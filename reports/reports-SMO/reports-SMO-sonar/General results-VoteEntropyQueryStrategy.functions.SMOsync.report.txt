Thu Nov 30 23.24.12 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Thu Nov 30 23.24.12 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 52.74038461538462
Incorrectly Classified Instances: 32.11538461538462
Correctly Classified Instances: 67.88461538461539
Weighted Precision: 0.6986253005260668
Weighted AreaUnderROC: 0.7392274912587412
Root mean squared error: 0.5558960616047269
Relative absolute error: 64.06882192018563
Root relative squared error: 111.17921232094538
Weighted TruePositiveRate: 0.6788461538461539
Weighted MatthewsCorrelation: 0.37049380908926166
Weighted FMeasure: 0.667535812853205
Iteration time: 24.7
Weighted AreaUnderPRC: 0.7132457409864161
Mean absolute error: 0.3203441096009282
Coverage of cases: 70.57692307692307
Instances selection time: 3.2
Test time: 4.0
Accumulative iteration time: 24.7
Weighted Recall: 0.6788461538461539
Weighted FalsePositiveRate: 0.33134615384615385
Kappa statistic: 0.3489629832044081
Training time: 21.5
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 58.02884615384615
Incorrectly Classified Instances: 28.653846153846153
Correctly Classified Instances: 71.34615384615384
Weighted Precision: 0.7247639808261499
Weighted AreaUnderROC: 0.777199329688169
Root mean squared error: 0.5071645882928817
Relative absolute error: 57.12584685381148
Root relative squared error: 101.43291765857636
Weighted TruePositiveRate: 0.7134615384615385
Weighted MatthewsCorrelation: 0.43480254302498633
Weighted FMeasure: 0.7097582850380337
Iteration time: 23.8
Weighted AreaUnderPRC: 0.7606256986580718
Mean absolute error: 0.2856292342690574
Coverage of cases: 78.65384615384616
Instances selection time: 2.5
Test time: 2.9
Accumulative iteration time: 48.5
Weighted Recall: 0.7134615384615385
Weighted FalsePositiveRate: 0.29006868131868135
Kappa statistic: 0.42328378427849184
Training time: 21.3
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 60.33653846153847
Incorrectly Classified Instances: 27.78846153846154
Correctly Classified Instances: 72.21153846153847
Weighted Precision: 0.7313873269572775
Weighted AreaUnderROC: 0.7896845230662196
Root mean squared error: 0.4922598634272884
Relative absolute error: 55.76669753640799
Root relative squared error: 98.45197268545769
Weighted TruePositiveRate: 0.7221153846153846
Weighted MatthewsCorrelation: 0.45237718039467445
Weighted FMeasure: 0.7203346248676874
Iteration time: 21.7
Weighted AreaUnderPRC: 0.7733993934649273
Mean absolute error: 0.27883348768204
Coverage of cases: 81.92307692307693
Instances selection time: 2.0
Test time: 3.7
Accumulative iteration time: 70.2
Weighted Recall: 0.7221153846153846
Weighted FalsePositiveRate: 0.2755306598163741
Kappa statistic: 0.4447603787688001
Training time: 19.7
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 74.13461538461539
Incorrectly Classified Instances: 25.000000000000004
Correctly Classified Instances: 75.0
Weighted Precision: 0.7573277552993102
Weighted AreaUnderROC: 0.8380879908781695
Root mean squared error: 0.43618617899820034
Relative absolute error: 54.333689743855324
Root relative squared error: 87.23723579964006
Weighted TruePositiveRate: 0.7500000000000002
Weighted MatthewsCorrelation: 0.5054904119753175
Weighted FMeasure: 0.748455179450473
Iteration time: 24.6
Weighted AreaUnderPRC: 0.8396956525999777
Mean absolute error: 0.2716684487192767
Coverage of cases: 93.75000000000001
Instances selection time: 1.8
Test time: 4.0
Accumulative iteration time: 94.8
Weighted Recall: 0.7500000000000002
Weighted FalsePositiveRate: 0.24993429189857758
Kappa statistic: 0.4988719641576365
Training time: 22.8
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 75.86538461538461
Incorrectly Classified Instances: 23.94230769230769
Correctly Classified Instances: 76.05769230769229
Weighted Precision: 0.7663473305297978
Weighted AreaUnderROC: 0.8438802566481138
Root mean squared error: 0.4222213920570244
Relative absolute error: 53.65898648534287
Root relative squared error: 84.44427841140488
Weighted TruePositiveRate: 0.760576923076923
Weighted MatthewsCorrelation: 0.525099226293243
Weighted FMeasure: 0.7595959378902638
Iteration time: 23.6
Weighted AreaUnderPRC: 0.8486811916113772
Mean absolute error: 0.26829493242671437
Coverage of cases: 94.32692307692308
Instances selection time: 1.6
Test time: 5.1
Accumulative iteration time: 118.4
Weighted Recall: 0.760576923076923
Weighted FalsePositiveRate: 0.23979306407877837
Kappa statistic: 0.519961440760008
Training time: 22.0
		
Time end:Thu Nov 30 23.24.16 EET 2017