Thu Nov 30 23.24.02 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 23.24.02 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 51.92307692307692
Incorrectly Classified Instances: 35.57692307692308
Correctly Classified Instances: 64.42307692307692
Weighted Precision: 0.6880008012820513
Weighted AreaUnderROC: 0.7385899814471243
Root mean squared error: 0.5914590247014667
Relative absolute error: 71.06211354827714
Root relative squared error: 118.29180494029335
Weighted TruePositiveRate: 0.6442307692307693
Weighted MatthewsCorrelation: 0.337099931231621
Weighted FMeasure: 0.6310165800761413
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7145162925534471
Mean absolute error: 0.3553105677413857
Coverage of cases: 65.38461538461539
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6442307692307693
Weighted FalsePositiveRate: 0.3325424575424575
Kappa statistic: 0.3039073806078148
Training time: 31.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.96153846153846
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7309430682312038
Weighted AreaUnderROC: 0.7907984872270586
Root mean squared error: 0.5150987555087657
Relative absolute error: 53.987360313859135
Root relative squared error: 103.01975110175314
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.4587044097705052
Weighted FMeasure: 0.7297642633588881
Iteration time: 23.0
Weighted AreaUnderPRC: 0.761184937203277
Mean absolute error: 0.26993680156929567
Coverage of cases: 74.03846153846153
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.2754816611959469
Kappa statistic: 0.4573238911666044
Training time: 19.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 66.34615384615384
Incorrectly Classified Instances: 22.115384615384617
Correctly Classified Instances: 77.88461538461539
Weighted Precision: 0.7802301864801866
Weighted AreaUnderROC: 0.836734693877551
Root mean squared error: 0.44019516901217076
Relative absolute error: 50.75914189722413
Root relative squared error: 88.03903380243415
Weighted TruePositiveRate: 0.7788461538461539
Weighted MatthewsCorrelation: 0.5563557139463666
Weighted FMeasure: 0.7777088502894954
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8451873034202015
Mean absolute error: 0.2537957094861207
Coverage of cases: 87.5
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 93.0
Weighted Recall: 0.7788461538461539
Weighted FalsePositiveRate: 0.2281968031968032
Kappa statistic: 0.5537313432835821
Training time: 24.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 71.15384615384616
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7503766478342749
Weighted AreaUnderROC: 0.8523191094619667
Root mean squared error: 0.4265778764999782
Relative absolute error: 51.13414881811508
Root relative squared error: 85.31557529999564
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.49758400114469625
Weighted FMeasure: 0.7490668159761105
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8605186997591555
Mean absolute error: 0.2556707440905754
Coverage of cases: 92.3076923076923
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 125.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.25612244897959185
Kappa statistic: 0.4960864703689899
Training time: 27.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 75.0
Incorrectly Classified Instances: 23.076923076923077
Correctly Classified Instances: 76.92307692307692
Weighted Precision: 0.7692307692307693
Weighted AreaUnderROC: 0.846382189239332
Root mean squared error: 0.4164260742416062
Relative absolute error: 51.84250070760289
Root relative squared error: 83.28521484832125
Weighted TruePositiveRate: 0.7692307692307693
Weighted MatthewsCorrelation: 0.5369202226345083
Weighted FMeasure: 0.7692307692307693
Iteration time: 37.0
Weighted AreaUnderPRC: 0.8476491662453836
Mean absolute error: 0.25921250353801445
Coverage of cases: 94.23076923076923
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 162.0
Weighted Recall: 0.7692307692307693
Weighted FalsePositiveRate: 0.23231054659626085
Kappa statistic: 0.5369202226345084
Training time: 30.0
		
Time end:Thu Nov 30 23.24.03 EET 2017