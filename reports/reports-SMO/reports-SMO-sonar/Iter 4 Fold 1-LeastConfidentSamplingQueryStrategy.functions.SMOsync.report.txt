Thu Nov 30 23.24.01 EET 2017
Dataset: sonar
Test set size: 104
Initial Labelled set size: 20
Initial Unlabelled set size: 84
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Thu Nov 30 23.24.01 EET 2017
		
Iteration: 1
Labeled set size: 20
Unlabelled set size: 84
	
Mean region size: 54.32692307692308
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7209249084249085
Weighted AreaUnderROC: 0.7821892393320966
Root mean squared error: 0.5072208611754506
Relative absolute error: 54.90824697047201
Root relative squared error: 101.44417223509012
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4399037715332818
Weighted FMeasure: 0.7209725586014246
Iteration time: 33.0
Weighted AreaUnderPRC: 0.749011486496392
Mean absolute error: 0.27454123485236004
Coverage of cases: 76.92307692307692
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.2818217496788925
Kappa statistic: 0.4398216939078752
Training time: 26.0
		
Iteration: 2
Labeled set size: 40
Unlabelled set size: 64
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7305108105779998
Weighted AreaUnderROC: 0.7690202654488368
Root mean squared error: 0.5188745235577598
Relative absolute error: 53.84703180630858
Root relative squared error: 103.77490471155195
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.45887810378851523
Weighted FMeasure: 0.7303685897435895
Iteration time: 22.0
Weighted AreaUnderPRC: 0.71248582347607
Mean absolute error: 0.2692351590315429
Coverage of cases: 73.07692307692308
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.273255316112459
Kappa statistic: 0.45853477129044246
Training time: 20.0
		
Iteration: 3
Labeled set size: 60
Unlabelled set size: 44
	
Mean region size: 69.71153846153847
Incorrectly Classified Instances: 27.884615384615383
Correctly Classified Instances: 72.11538461538461
Weighted Precision: 0.7228180473372781
Weighted AreaUnderROC: 0.8126159554730983
Root mean squared error: 0.4619775213668803
Relative absolute error: 55.416612091386654
Root relative squared error: 92.39550427337606
Weighted TruePositiveRate: 0.7211538461538461
Weighted MatthewsCorrelation: 0.4430456239044162
Weighted FMeasure: 0.7213860674349247
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8093545847905488
Mean absolute error: 0.27708306045693326
Coverage of cases: 89.42307692307692
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 87.0
Weighted Recall: 0.7211538461538461
Weighted FalsePositiveRate: 0.27736905951191665
Kappa statistic: 0.4423076923076923
Training time: 26.0
		
Iteration: 4
Labeled set size: 80
Unlabelled set size: 24
	
Mean region size: 69.23076923076923
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7307692307692307
Weighted AreaUnderROC: 0.8545454545454546
Root mean squared error: 0.42970332715508724
Relative absolute error: 50.02389061758253
Root relative squared error: 85.94066543101745
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.45974025974025967
Weighted FMeasure: 0.7307692307692307
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8576054859412358
Mean absolute error: 0.25011945308791267
Coverage of cases: 93.26923076923077
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 115.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.271028971028971
Kappa statistic: 0.45974025974025967
Training time: 24.0
		
Iteration: 5
Labeled set size: 100
Unlabelled set size: 4
	
Mean region size: 73.5576923076923
Incorrectly Classified Instances: 26.923076923076923
Correctly Classified Instances: 73.07692307692308
Weighted Precision: 0.7305108105779998
Weighted AreaUnderROC: 0.8523191094619667
Root mean squared error: 0.4171141352626127
Relative absolute error: 50.488046010951784
Root relative squared error: 83.42282705252254
Weighted TruePositiveRate: 0.7307692307692307
Weighted MatthewsCorrelation: 0.45887810378851523
Weighted FMeasure: 0.7303685897435895
Iteration time: 39.0
Weighted AreaUnderPRC: 0.8554005277270573
Mean absolute error: 0.2524402300547589
Coverage of cases: 95.1923076923077
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 154.0
Weighted Recall: 0.7307692307692307
Weighted FalsePositiveRate: 0.273255316112459
Kappa statistic: 0.45853477129044246
Training time: 29.0
		
Time end:Thu Nov 30 23.24.01 EET 2017