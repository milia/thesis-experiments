Mon Nov 27 15.43.48 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Mon Nov 27 15.43.48 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 5.678466076696169
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.4770397743849071
Weighted AreaUnderROC: 0.87846182863581
Root mean squared error: 0.17439544683212901
Relative absolute error: 43.99332898278441
Root relative squared error: 87.27351458501539
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.45522724912112084
Weighted FMeasure: 0.4903056754078909
Iteration time: 2212.0
Weighted AreaUnderPRC: 0.6054121903013342
Mean absolute error: 0.03513356134041816
Coverage of cases: 65.48672566371681
Instances selection time: 1619.0
Test time: 1015.0
Accumulative iteration time: 2212.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.08858896833987859
Kappa statistic: 0.4820968203953022
Training time: 593.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 5.862831858407083
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.4105434879771163
Weighted AreaUnderROC: 0.8592801046506453
Root mean squared error: 0.19080009782120044
Relative absolute error: 52.39290224615223
Root relative squared error: 95.48296943812828
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.35169849800046066
Weighted FMeasure: 0.3997197772542233
Iteration time: 1858.0
Weighted AreaUnderPRC: 0.548418143169734
Mean absolute error: 0.04184155387713552
Coverage of cases: 57.52212389380531
Instances selection time: 1356.0
Test time: 1098.0
Accumulative iteration time: 4070.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.12887149309541673
Kappa statistic: 0.3826341463414634
Training time: 502.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 9.218289085545724
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5550671435106624
Weighted AreaUnderROC: 0.8830641706720036
Root mean squared error: 0.16456073939990334
Relative absolute error: 47.06263681165695
Root relative squared error: 82.35188676664748
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5224870524517673
Weighted FMeasure: 0.542303194524223
Iteration time: 1164.0
Weighted AreaUnderPRC: 0.6518073421308322
Mean absolute error: 0.03758474467597609
Coverage of cases: 68.14159292035399
Instances selection time: 519.0
Test time: 844.0
Accumulative iteration time: 5234.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.0698121076752427
Kappa statistic: 0.5630481939073847
Training time: 645.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 6.821533923303835
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6116942214904759
Weighted AreaUnderROC: 0.921738500475147
Root mean squared error: 0.15270290165157607
Relative absolute error: 36.332300811279794
Root relative squared error: 76.41781455046433
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.5747625052409023
Weighted FMeasure: 0.5937583234247114
Iteration time: 1366.0
Weighted AreaUnderPRC: 0.7153232837419151
Mean absolute error: 0.029015379120119318
Coverage of cases: 72.56637168141593
Instances selection time: 425.0
Test time: 1080.0
Accumulative iteration time: 6600.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.07504228215724137
Kappa statistic: 0.6129400570884871
Training time: 941.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.116519174041303
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.6765651271188977
Weighted AreaUnderROC: 0.9350965517753398
Root mean squared error: 0.1329406496825578
Relative absolute error: 28.797672911795573
Root relative squared error: 66.52810001502085
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.6880725676656272
Weighted FMeasure: 0.7134440231891299
Iteration time: 1220.0
Weighted AreaUnderPRC: 0.7609887375761146
Mean absolute error: 0.02299814156150344
Coverage of cases: 79.64601769911505
Instances selection time: 94.0
Test time: 1131.0
Accumulative iteration time: 7820.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.04857070110394912
Kappa statistic: 0.7252665045820086
Training time: 1126.0
		
Time end:Mon Nov 27 15.44.01 EET 2017