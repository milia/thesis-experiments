Mon Nov 27 15.49.18 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Mon Nov 27 15.49.18 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.415929203539831
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.4595812366244433
Weighted AreaUnderROC: 0.8787974329509715
Root mean squared error: 0.18905755097105614
Relative absolute error: 52.40450577623915
Root relative squared error: 94.61093871310874
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3815788879105806
Weighted FMeasure: 0.4317815384192262
Iteration time: 1915.0
Weighted AreaUnderPRC: 0.5947054460964704
Mean absolute error: 0.0418508205851909
Coverage of cases: 61.06194690265487
Instances selection time: 765.0
Test time: 965.0
Accumulative iteration time: 1915.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.1049053153731409
Kappa statistic: 0.39051854655563967
Training time: 1150.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 5.89970501474927
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.6159757801583605
Weighted AreaUnderROC: 0.9045165112451635
Root mean squared error: 0.17157223483681186
Relative absolute error: 43.512507127181166
Root relative squared error: 85.86068163710554
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.5212216216546584
Weighted FMeasure: 0.5587206476011762
Iteration time: 2158.0
Weighted AreaUnderPRC: 0.6979964291402485
Mean absolute error: 0.03474957166406822
Coverage of cases: 67.2566371681416
Instances selection time: 841.0
Test time: 1281.0
Accumulative iteration time: 4073.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08353743539708881
Kappa statistic: 0.5079981069569333
Training time: 1317.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 5.7153392330383515
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.64117342834157
Weighted AreaUnderROC: 0.9380795336070343
Root mean squared error: 0.16638175231868416
Relative absolute error: 40.32141688013162
Root relative squared error: 83.26318462684742
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5557928436119793
Weighted FMeasure: 0.5725410942140785
Iteration time: 2748.0
Weighted AreaUnderPRC: 0.7488660642547867
Mean absolute error: 0.032201131536216156
Coverage of cases: 69.02654867256638
Instances selection time: 753.0
Test time: 1502.0
Accumulative iteration time: 6821.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.09317491429109617
Kappa statistic: 0.5508918185343156
Training time: 1995.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 6.010324483775814
Incorrectly Classified Instances: 22.123893805309734
Correctly Classified Instances: 77.87610619469027
Weighted Precision: 0.73586529006883
Weighted AreaUnderROC: 0.9537876782106205
Root mean squared error: 0.12983522828591892
Relative absolute error: 26.148506175383314
Root relative squared error: 64.9740397200118
Weighted TruePositiveRate: 0.7787610619469026
Weighted MatthewsCorrelation: 0.7232412438760777
Weighted FMeasure: 0.7469536417069227
Iteration time: 2434.0
Weighted AreaUnderPRC: 0.8144491742566466
Mean absolute error: 0.02088248757061857
Coverage of cases: 82.30088495575221
Instances selection time: 437.0
Test time: 1360.0
Accumulative iteration time: 9255.0
Weighted Recall: 0.7787610619469026
Weighted FalsePositiveRate: 0.04477244676198872
Kappa statistic: 0.7352141718998969
Training time: 1997.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.264011799410035
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.712899459193707
Weighted AreaUnderROC: 0.954609092510605
Root mean squared error: 0.1341645325824564
Relative absolute error: 29.884914933111517
Root relative squared error: 67.14057335681328
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6702268588827343
Weighted FMeasure: 0.6996459712029284
Iteration time: 2132.0
Weighted AreaUnderPRC: 0.8104253937601583
Mean absolute error: 0.02386642512019317
Coverage of cases: 85.84070796460178
Instances selection time: 152.0
Test time: 1268.0
Accumulative iteration time: 11387.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.04461028168975394
Kappa statistic: 0.6663898883660854
Training time: 1980.0
		
Time end:Mon Nov 27 15.49.36 EET 2017