Mon Nov 27 15.55.01 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMOsync-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Mon Nov 27 15.55.01 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.1946902654867255
Incorrectly Classified Instances: 45.13274336283186
Correctly Classified Instances: 54.86725663716814
Weighted Precision: 0.4471618843300259
Weighted AreaUnderROC: 0.8564159767337609
Root mean squared error: 0.18072978458176994
Relative absolute error: 48.639528190178616
Root relative squared error: 90.44343632334979
Weighted TruePositiveRate: 0.5486725663716814
Weighted MatthewsCorrelation: 0.4282401356648461
Weighted FMeasure: 0.4724757690686895
Iteration time: 1427.0
Weighted AreaUnderPRC: 0.5804781934599436
Mean absolute error: 0.038844067651878714
Coverage of cases: 63.716814159292035
Instances selection time: 748.0
Test time: 975.0
Accumulative iteration time: 1427.0
Weighted Recall: 0.5486725663716814
Weighted FalsePositiveRate: 0.08960319234144831
Kappa statistic: 0.45129962867752066
Training time: 679.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 6.305309734513278
Incorrectly Classified Instances: 52.21238938053097
Correctly Classified Instances: 47.78761061946903
Weighted Precision: 0.59508439855785
Weighted AreaUnderROC: 0.8468250495547913
Root mean squared error: 0.195633421218938
Relative absolute error: 55.454054897150975
Root relative squared error: 97.90173166907454
Weighted TruePositiveRate: 0.4778761061946903
Weighted MatthewsCorrelation: 0.4394709057103157
Weighted FMeasure: 0.4403749643887637
Iteration time: 1380.0
Weighted AreaUnderPRC: 0.5532505898339386
Mean absolute error: 0.04428622439703024
Coverage of cases: 56.63716814159292
Instances selection time: 693.0
Test time: 1057.0
Accumulative iteration time: 2807.0
Weighted Recall: 0.4778761061946903
Weighted FalsePositiveRate: 0.062423910262319283
Kappa statistic: 0.39517372766034653
Training time: 687.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.853982300884958
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.5554131708998965
Weighted AreaUnderROC: 0.883165191954724
Root mean squared error: 0.17407610110679173
Relative absolute error: 47.997717323749036
Root relative squared error: 87.11370293669461
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.4762638558556284
Weighted FMeasure: 0.49168006554995114
Iteration time: 771.0
Weighted AreaUnderPRC: 0.6254747776475303
Mean absolute error: 0.038331510362716203
Coverage of cases: 68.14159292035399
Instances selection time: 238.0
Test time: 736.0
Accumulative iteration time: 3578.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.08052520243936084
Kappa statistic: 0.48087380461278834
Training time: 533.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 6.157817109144542
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.5832671469305684
Weighted AreaUnderROC: 0.9274476610002231
Root mean squared error: 0.15818237984369543
Relative absolute error: 38.50886191847071
Root relative squared error: 79.15993499342822
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5583998738497965
Weighted FMeasure: 0.5905366636430083
Iteration time: 685.0
Weighted AreaUnderPRC: 0.7347430062018974
Mean absolute error: 0.030753605004334213
Coverage of cases: 71.68141592920354
Instances selection time: 131.0
Test time: 831.0
Accumulative iteration time: 4263.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.08429326989703603
Kappa statistic: 0.5866384289564883
Training time: 554.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.448377581120944
Incorrectly Classified Instances: 25.663716814159294
Correctly Classified Instances: 74.33628318584071
Weighted Precision: 0.7154214885409574
Weighted AreaUnderROC: 0.9519941947946269
Root mean squared error: 0.12908453995180638
Relative absolute error: 28.858819841923747
Root relative squared error: 64.59836930850707
Weighted TruePositiveRate: 0.7433628318584071
Weighted MatthewsCorrelation: 0.6898236378686091
Weighted FMeasure: 0.7089518396584013
Iteration time: 1311.0
Weighted AreaUnderPRC: 0.7981132385483274
Mean absolute error: 0.023046974179314075
Coverage of cases: 83.1858407079646
Instances selection time: 61.0
Test time: 1138.0
Accumulative iteration time: 5574.0
Weighted Recall: 0.7433628318584071
Weighted FalsePositiveRate: 0.03950445113223331
Kappa statistic: 0.6954743982901218
Training time: 1250.0
		
Time end:Mon Nov 27 15.55.12 EET 2017