Tue Oct 31 11.27.35 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.27.35 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 93.76811594202898
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7932072907085064
Weighted AreaUnderROC: 0.8812998444002313
Root mean squared error: 0.37704417366209303
Relative absolute error: 60.86555897167975
Root relative squared error: 75.40883473241861
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.574509363146284
Weighted FMeasure: 0.7831022474888265
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8719544520520714
Mean absolute error: 0.3043277948583987
Coverage of cases: 99.71014492753623
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.20546855830261246
Kappa statistic: 0.56750296688786
Training time: 4.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 88.84057971014492
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8186734867860187
Weighted AreaUnderROC: 0.8829754243025356
Root mean squared error: 0.378529649427801
Relative absolute error: 57.49826411645718
Root relative squared error: 75.7059298855602
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6284328238225309
Weighted FMeasure: 0.8121272068627676
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8700377152643364
Mean absolute error: 0.2874913205822859
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.1795822358080496
Kappa statistic: 0.6237731733914941
Training time: 5.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 87.97101449275362
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8157090262023372
Weighted AreaUnderROC: 0.8579069121181573
Root mean squared error: 0.3917393468781701
Relative absolute error: 58.36549144791672
Root relative squared error: 78.34786937563402
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6247434840375208
Weighted FMeasure: 0.8121186958001079
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8319132869032959
Mean absolute error: 0.2918274572395836
Coverage of cases: 96.81159420289855
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.18335594900584656
Kappa statistic: 0.6223666705959618
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 87.68115942028986
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8124468907765634
Weighted AreaUnderROC: 0.8823719948008589
Root mean squared error: 0.3732123980731954
Relative absolute error: 55.649220707521366
Root relative squared error: 74.64247961463909
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6202640298599387
Weighted FMeasure: 0.8118496662212025
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8657364886452867
Mean absolute error: 0.2782461035376068
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.18964547100217483
Kappa statistic: 0.6199989832748717
Training time: 5.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8372785829307567
Weighted AreaUnderROC: 0.8858679638898168
Root mean squared error: 0.3636999242097347
Relative absolute error: 49.6530157748265
Root relative squared error: 72.73998484194694
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6693924133304229
Weighted FMeasure: 0.8351818699814513
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8705402762566591
Mean absolute error: 0.24826507887413252
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.16214372925049
Kappa statistic: 0.6680172195492529
Training time: 6.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 84.78260869565217
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8655013907187818
Weighted AreaUnderROC: 0.897589189018736
Root mean squared error: 0.35241529297692586
Relative absolute error: 48.58697565615576
Root relative squared error: 70.48305859538517
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7196349068252764
Weighted FMeasure: 0.8554210582255127
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8743203754178086
Mean absolute error: 0.24293487828077878
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.13194742127134568
Kappa statistic: 0.711490215755143
Training time: 7.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 85.3623188405797
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8504302414419471
Weighted AreaUnderROC: 0.8933913457359849
Root mean squared error: 0.35022482592114407
Relative absolute error: 46.40945582774168
Root relative squared error: 70.04496518422881
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6947266547695771
Weighted FMeasure: 0.846804475037011
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8684091393181131
Mean absolute error: 0.2320472791387084
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.14776390617501475
Kappa statistic: 0.6920835929474765
Training time: 8.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 88.26086956521739
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8450011585990046
Weighted AreaUnderROC: 0.8925381091326913
Root mean squared error: 0.35829003384416586
Relative absolute error: 52.07699875229692
Root relative squared error: 71.65800676883318
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6856470103536297
Weighted FMeasure: 0.8437855194111465
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8732882243826192
Mean absolute error: 0.2603849937614846
Coverage of cases: 98.55072463768116
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15513257514168055
Kappa statistic: 0.6848995331844937
Training time: 10.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 90.28985507246377
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.840443775226384
Weighted AreaUnderROC: 0.8962981740923921
Root mean squared error: 0.35327678875225876
Relative absolute error: 51.75518687138987
Root relative squared error: 70.65535775045176
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6765985715096595
Weighted FMeasure: 0.8401603586321862
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8864595793543038
Mean absolute error: 0.25877593435694934
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 65.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.16753286170540896
Kappa statistic: 0.6760231521794805
Training time: 10.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 87.53623188405797
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8233104500381389
Weighted AreaUnderROC: 0.905878793791382
Root mean squared error: 0.35014833425507536
Relative absolute error: 49.99014176040066
Root relative squared error: 70.02966685101507
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6424980815590747
Weighted FMeasure: 0.8232424724874776
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8989960259326928
Mean absolute error: 0.2499507088020033
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.18029726552376224
Kappa statistic: 0.6424870466321242
Training time: 11.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 89.56521739130434
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8279607237801218
Weighted AreaUnderROC: 0.898754462776771
Root mean squared error: 0.3560413284776111
Relative absolute error: 52.544324866808
Root relative squared error: 71.20826569552221
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6466174509281681
Weighted FMeasure: 0.8207890499194846
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8787502403401264
Mean absolute error: 0.26272162433404
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.17005527290070882
Kappa statistic: 0.6413601153549512
Training time: 13.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 86.08695652173913
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8520941185192555
Weighted AreaUnderROC: 0.9226161159577959
Root mean squared error: 0.32883580971562715
Relative absolute error: 44.62799346301926
Root relative squared error: 65.76716194312543
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.699034839862723
Weighted FMeasure: 0.8496572659616138
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9161375638029041
Mean absolute error: 0.22313996731509628
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.14668475920467725
Kappa statistic: 0.6973275745714672
Training time: 12.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 87.3913043478261
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8364126172208014
Weighted AreaUnderROC: 0.8984141929851012
Root mean squared error: 0.35396391247249065
Relative absolute error: 49.68771336890123
Root relative squared error: 70.79278249449813
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6661252712976317
Weighted FMeasure: 0.8323604034857697
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8886130724848683
Mean absolute error: 0.24843856684450616
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.16196497182156183
Kappa statistic: 0.6632446987546281
Training time: 15.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 84.20289855072464
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8620039798551504
Weighted AreaUnderROC: 0.9119172768956517
Root mean squared error: 0.33669027425157083
Relative absolute error: 44.429349315280945
Root relative squared error: 67.33805485031417
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7180543783469291
Weighted FMeasure: 0.858366401449312
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8982996948774136
Mean absolute error: 0.2221467465764047
Coverage of cases: 97.97101449275362
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 133.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.13589989189807083
Kappa statistic: 0.715322567064648
Training time: 15.0
		
Time end:Tue Oct 31 11.27.36 EET 2017