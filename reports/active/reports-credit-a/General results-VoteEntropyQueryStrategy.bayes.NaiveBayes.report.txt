Tue Oct 31 11.27.37 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.27.37 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.18840579710145
Incorrectly Classified Instances: 21.536231884057973
Correctly Classified Instances: 78.46376811594203
Weighted Precision: 0.7944138311999598
Weighted AreaUnderROC: 0.8755039286851156
Root mean squared error: 0.42914430574478607
Relative absolute error: 43.96172222723944
Root relative squared error: 85.82886114895722
Weighted TruePositiveRate: 0.7846376811594202
Weighted MatthewsCorrelation: 0.5691585840439969
Weighted FMeasure: 0.7798483715567105
Iteration time: 5.4
Weighted AreaUnderPRC: 0.8616349270670851
Mean absolute error: 0.2198086111361972
Coverage of cases: 87.65217391304347
Instances selection time: 3.7
Test time: 4.9
Accumulative iteration time: 5.4
Weighted Recall: 0.7846376811594202
Weighted FalsePositiveRate: 0.24103289720633403
Kappa statistic: 0.5550476015237523
Training time: 1.7
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 59.82608695652173
Incorrectly Classified Instances: 22.43478260869565
Correctly Classified Instances: 77.56521739130434
Weighted Precision: 0.7884313861841015
Weighted AreaUnderROC: 0.8797855888495952
Root mean squared error: 0.4351167273632696
Relative absolute error: 44.63210875695733
Root relative squared error: 87.02334547265393
Weighted TruePositiveRate: 0.7756521739130434
Weighted MatthewsCorrelation: 0.5521509323544285
Weighted FMeasure: 0.7688418386981891
Iteration time: 3.8
Weighted AreaUnderPRC: 0.8676750246972091
Mean absolute error: 0.2231605437847867
Coverage of cases: 87.33333333333333
Instances selection time: 3.3
Test time: 4.3
Accumulative iteration time: 9.2
Weighted Recall: 0.7756521739130434
Weighted FalsePositiveRate: 0.25583458907726875
Kappa statistic: 0.5335341190068308
Training time: 0.5
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 59.724637681159415
Incorrectly Classified Instances: 22.434782608695652
Correctly Classified Instances: 77.56521739130434
Weighted Precision: 0.7904049346872406
Weighted AreaUnderROC: 0.8853246917415033
Root mean squared error: 0.4336890974951211
Relative absolute error: 44.334832261794965
Root relative squared error: 86.7378194990242
Weighted TruePositiveRate: 0.7756521739130434
Weighted MatthewsCorrelation: 0.5535165275453798
Weighted FMeasure: 0.7680633663846362
Iteration time: 4.6
Weighted AreaUnderPRC: 0.8758200625027042
Mean absolute error: 0.22167416130897483
Coverage of cases: 87.5072463768116
Instances selection time: 3.0
Test time: 4.8
Accumulative iteration time: 13.8
Weighted Recall: 0.7756521739130434
Weighted FalsePositiveRate: 0.2576208123520777
Kappa statistic: 0.5325521647873979
Training time: 1.6
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 59.18840579710145
Incorrectly Classified Instances: 22.260869565217387
Correctly Classified Instances: 77.7391304347826
Weighted Precision: 0.7947801066524909
Weighted AreaUnderROC: 0.8876915689643095
Root mean squared error: 0.43515461758365886
Relative absolute error: 44.28695353778393
Root relative squared error: 87.03092351673178
Weighted TruePositiveRate: 0.7773913043478262
Weighted MatthewsCorrelation: 0.5591952157380398
Weighted FMeasure: 0.7691117386196794
Iteration time: 3.5
Weighted AreaUnderPRC: 0.8789213111692508
Mean absolute error: 0.22143476768891962
Coverage of cases: 87.07246376811594
Instances selection time: 2.5
Test time: 4.6
Accumulative iteration time: 17.3
Weighted Recall: 0.7773913043478262
Weighted FalsePositiveRate: 0.2577556431858522
Kappa statistic: 0.5352735657517047
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 59.1159420289855
Incorrectly Classified Instances: 22.49275362318841
Correctly Classified Instances: 77.5072463768116
Weighted Precision: 0.7937330926830872
Weighted AreaUnderROC: 0.8894308397165279
Root mean squared error: 0.4399346698420807
Relative absolute error: 45.1261771933709
Root relative squared error: 87.98693396841614
Weighted TruePositiveRate: 0.7750724637681159
Weighted MatthewsCorrelation: 0.5552774477056427
Weighted FMeasure: 0.7660378313080404
Iteration time: 3.8
Weighted AreaUnderPRC: 0.8795977875744517
Mean absolute error: 0.22563088596685446
Coverage of cases: 86.55072463768116
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 21.1
Weighted Recall: 0.7750724637681159
Weighted FalsePositiveRate: 0.2610398976042555
Kappa statistic: 0.5298044372173457
Training time: 1.8
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 59.666666666666664
Incorrectly Classified Instances: 22.753623188405797
Correctly Classified Instances: 77.24637681159422
Weighted Precision: 0.7942273231872088
Weighted AreaUnderROC: 0.8890764283989698
Root mean squared error: 0.4453165270630583
Relative absolute error: 46.45146141107207
Root relative squared error: 89.06330541261168
Weighted TruePositiveRate: 0.772463768115942
Weighted MatthewsCorrelation: 0.5522142425429925
Weighted FMeasure: 0.7621024405626978
Iteration time: 3.2
Weighted AreaUnderPRC: 0.8797821969712315
Mean absolute error: 0.23225730705536032
Coverage of cases: 86.05797101449275
Instances selection time: 2.1
Test time: 5.2
Accumulative iteration time: 24.3
Weighted Recall: 0.772463768115942
Weighted FalsePositiveRate: 0.265547204191467
Kappa statistic: 0.52322592113635
Training time: 1.1
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 59.463768115942024
Incorrectly Classified Instances: 23.130434782608695
Correctly Classified Instances: 76.8695652173913
Weighted Precision: 0.7906047284126997
Weighted AreaUnderROC: 0.8892679348831244
Root mean squared error: 0.4473684655343789
Relative absolute error: 46.68873367178255
Root relative squared error: 89.47369310687576
Weighted TruePositiveRate: 0.768695652173913
Weighted MatthewsCorrelation: 0.5444350178268018
Weighted FMeasure: 0.7578751224340791
Iteration time: 3.4
Weighted AreaUnderPRC: 0.8797775767763557
Mean absolute error: 0.23344366835891278
Coverage of cases: 85.82608695652173
Instances selection time: 2.4
Test time: 5.3
Accumulative iteration time: 27.7
Weighted Recall: 0.768695652173913
Weighted FalsePositiveRate: 0.2696983967986549
Kappa statistic: 0.515112682015838
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 58.89855072463767
Incorrectly Classified Instances: 23.565217391304348
Correctly Classified Instances: 76.43478260869564
Weighted Precision: 0.7889420236425055
Weighted AreaUnderROC: 0.8891814166487275
Root mean squared error: 0.45303487602149606
Relative absolute error: 47.362323254458786
Root relative squared error: 90.60697520429919
Weighted TruePositiveRate: 0.7643478260869565
Weighted MatthewsCorrelation: 0.5375221649548942
Weighted FMeasure: 0.7525579948427736
Iteration time: 2.7
Weighted AreaUnderPRC: 0.8801934211157902
Mean absolute error: 0.23681161627229388
Coverage of cases: 85.27536231884059
Instances selection time: 1.7
Test time: 5.2
Accumulative iteration time: 30.4
Weighted Recall: 0.7643478260869565
Weighted FalsePositiveRate: 0.2767155014214088
Kappa statistic: 0.5050161900653715
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 59.7536231884058
Incorrectly Classified Instances: 23.36231884057971
Correctly Classified Instances: 76.63768115942028
Weighted Precision: 0.7897333726038146
Weighted AreaUnderROC: 0.8934134654558523
Root mean squared error: 0.4482068865824136
Relative absolute error: 47.056595926633136
Root relative squared error: 89.6413773164827
Weighted TruePositiveRate: 0.7663768115942028
Weighted MatthewsCorrelation: 0.5407523361875924
Weighted FMeasure: 0.7551120687614367
Iteration time: 2.9
Weighted AreaUnderPRC: 0.885519704584856
Mean absolute error: 0.23528297963316563
Coverage of cases: 86.3768115942029
Instances selection time: 1.8
Test time: 4.6
Accumulative iteration time: 33.3
Weighted Recall: 0.7663768115942028
Weighted FalsePositiveRate: 0.2742199453771465
Kappa statistic: 0.5095560999190016
Training time: 1.1
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 59.65217391304348
Incorrectly Classified Instances: 22.60869565217391
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7952562235210271
Weighted AreaUnderROC: 0.8947580368543763
Root mean squared error: 0.4426513424844921
Relative absolute error: 45.902415684640985
Root relative squared error: 88.53026849689843
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.554940609929632
Weighted FMeasure: 0.7639867239558957
Iteration time: 2.3
Weighted AreaUnderPRC: 0.886951011374568
Mean absolute error: 0.22951207842320492
Coverage of cases: 86.72463768115942
Instances selection time: 1.1
Test time: 4.1
Accumulative iteration time: 35.6
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26497853222333845
Kappa statistic: 0.5262133267659297
Training time: 1.2
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 60.0
Incorrectly Classified Instances: 22.811594202898554
Correctly Classified Instances: 77.18840579710145
Weighted Precision: 0.7921234906601086
Weighted AreaUnderROC: 0.8955890040299771
Root mean squared error: 0.44140107593294325
Relative absolute error: 45.8631875446962
Root relative squared error: 88.28021518658863
Weighted TruePositiveRate: 0.7718840579710144
Weighted MatthewsCorrelation: 0.549834646015021
Weighted FMeasure: 0.7621272175504541
Iteration time: 1.5
Weighted AreaUnderPRC: 0.8882943198992763
Mean absolute error: 0.22931593772348102
Coverage of cases: 87.53623188405797
Instances selection time: 0.8
Test time: 4.2
Accumulative iteration time: 37.1
Weighted Recall: 0.7718840579710144
Weighted FalsePositiveRate: 0.2667057112475877
Kappa statistic: 0.522186664928362
Training time: 0.7
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 59.82608695652175
Incorrectly Classified Instances: 22.405797101449274
Correctly Classified Instances: 77.59420289855072
Weighted Precision: 0.7946789364126833
Weighted AreaUnderROC: 0.8949589088133489
Root mean squared error: 0.43846437775355546
Relative absolute error: 45.2484234128908
Root relative squared error: 87.6928755507111
Weighted TruePositiveRate: 0.7759420289855072
Weighted MatthewsCorrelation: 0.557153778471495
Weighted FMeasure: 0.7670696079615937
Iteration time: 1.9
Weighted AreaUnderPRC: 0.887441206094341
Mean absolute error: 0.22624211706445402
Coverage of cases: 87.44927536231886
Instances selection time: 0.5
Test time: 4.2
Accumulative iteration time: 39.0
Weighted Recall: 0.7759420289855072
Weighted FalsePositiveRate: 0.26138875656198923
Kappa statistic: 0.5313544323648653
Training time: 1.4
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 59.71014492753623
Incorrectly Classified Instances: 22.46376811594203
Correctly Classified Instances: 77.53623188405797
Weighted Precision: 0.7926062257710857
Weighted AreaUnderROC: 0.8940863470976448
Root mean squared error: 0.43627030922491883
Relative absolute error: 44.99155759968982
Root relative squared error: 87.25406184498378
Weighted TruePositiveRate: 0.7753623188405798
Weighted MatthewsCorrelation: 0.5547782735541132
Weighted FMeasure: 0.7669426227299947
Iteration time: 2.1
Weighted AreaUnderPRC: 0.8861402662526073
Mean absolute error: 0.2249577879984491
Coverage of cases: 87.53623188405797
Instances selection time: 0.7
Test time: 3.9
Accumulative iteration time: 41.1
Weighted Recall: 0.7753623188405798
Weighted FalsePositiveRate: 0.2612411566322409
Kappa statistic: 0.5305791354826075
Training time: 1.4
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 59.81159420289855
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7904935481919864
Weighted AreaUnderROC: 0.8934995139362968
Root mean squared error: 0.43505631524197697
Relative absolute error: 44.91699056102529
Root relative squared error: 87.01126304839539
Weighted TruePositiveRate: 0.773913043478261
Weighted MatthewsCorrelation: 0.5512302895168854
Weighted FMeasure: 0.7656015439815478
Iteration time: 1.4
Weighted AreaUnderPRC: 0.8847755421743624
Mean absolute error: 0.22458495280512647
Coverage of cases: 87.59420289855073
Instances selection time: 0.4
Test time: 4.3
Accumulative iteration time: 42.5
Weighted Recall: 0.773913043478261
Weighted FalsePositiveRate: 0.262418785791845
Kappa statistic: 0.52770393760448
Training time: 1.0
		
Time end:Tue Oct 31 11.27.43 EET 2017