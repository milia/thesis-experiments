Tue Oct 31 11.26.15 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.15 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 91.8840579710145
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8330696338221422
Weighted AreaUnderROC: 0.8966065651474256
Root mean squared error: 0.35343736488510497
Relative absolute error: 53.154672587878885
Root relative squared error: 70.68747297702099
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6597350694035489
Weighted FMeasure: 0.8294615854185594
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8769219551587738
Mean absolute error: 0.2657733629393944
Coverage of cases: 98.84057971014492
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.16555992759043067
Kappa statistic: 0.657225131771719
Training time: 4.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 92.89855072463769
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8114861584730511
Weighted AreaUnderROC: 0.8900110664053299
Root mean squared error: 0.3683595282721237
Relative absolute error: 57.19095364355529
Root relative squared error: 73.67190565442473
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6185747167327266
Weighted FMeasure: 0.8115333136367664
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8816598308250307
Mean absolute error: 0.28595476821777643
Coverage of cases: 99.71014492753623
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.1934191841999718
Kappa statistic: 0.6185640659284586
Training time: 5.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 92.7536231884058
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8028985507246377
Weighted AreaUnderROC: 0.8811146816610054
Root mean squared error: 0.37399747460832167
Relative absolute error: 59.07795691479395
Root relative squared error: 74.79949492166433
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6012103080165907
Weighted FMeasure: 0.8028985507246377
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8750681556101365
Mean absolute error: 0.29538978457396975
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.20168824270804694
Kappa statistic: 0.6012103080165907
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 90.57971014492753
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8366239788485721
Weighted AreaUnderROC: 0.8826767890278019
Root mean squared error: 0.36757369127458145
Relative absolute error: 54.34901130898054
Root relative squared error: 73.5147382549163
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6685233461971621
Weighted FMeasure: 0.8351347266801811
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8636790007970558
Mean absolute error: 0.2717450565449027
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 31.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.16340163364975566
Kappa statistic: 0.6676019675123813
Training time: 6.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 90.57971014492753
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8346111215676434
Weighted AreaUnderROC: 0.9049453429945121
Root mean squared error: 0.3490651036951742
Relative absolute error: 52.17828226879288
Root relative squared error: 69.81302073903484
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.664807567460014
Weighted FMeasure: 0.834348008036993
Iteration time: 8.0
Weighted AreaUnderPRC: 0.888918773081079
Mean absolute error: 0.2608914113439644
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17346486884388093
Kappa statistic: 0.664242175895098
Training time: 7.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 90.14492753623189
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8403342857113567
Weighted AreaUnderROC: 0.8951738450486458
Root mean squared error: 0.36709021639772316
Relative absolute error: 55.233303581761625
Root relative squared error: 73.41804327954463
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6721601022448253
Weighted FMeasure: 0.8360828706708006
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8856708825327076
Mean absolute error: 0.27616651790880814
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17867524386987166
Kappa statistic: 0.667401184409861
Training time: 8.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 87.97101449275362
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8149664192294097
Weighted AreaUnderROC: 0.8825635135787651
Root mean squared error: 0.37171847490382826
Relative absolute error: 53.97661266496665
Root relative squared error: 74.34369498076565
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6234768692569581
Weighted FMeasure: 0.8133550829203002
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8672596795282181
Mean absolute error: 0.26988306332483325
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.1986295592259626
Kappa statistic: 0.6213342479849082
Training time: 8.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 87.97101449275362
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8346899569006347
Weighted AreaUnderROC: 0.9043519156312235
Root mean squared error: 0.3473990864772838
Relative absolute error: 48.88427725580633
Root relative squared error: 69.47981729545675
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6655214861927256
Weighted FMeasure: 0.8347292134968568
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8883039342940073
Mean absolute error: 0.24442138627903165
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.16969115564608397
Kappa statistic: 0.6655100270449558
Training time: 10.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 87.2463768115942
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.816514378447757
Weighted AreaUnderROC: 0.8890494322431495
Root mean squared error: 0.3640176767818802
Relative absolute error: 52.31491509310837
Root relative squared error: 72.80353535637603
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.624307185349558
Weighted FMeasure: 0.8126661379094865
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8713247814225856
Mean absolute error: 0.26157457546554186
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.20240327242375955
Kappa statistic: 0.6198870678969839
Training time: 10.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 79.85507246376811
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8580723899510346
Weighted AreaUnderROC: 0.90806734050531
Root mean squared error: 0.3460660835433333
Relative absolute error: 43.75712635178603
Root relative squared error: 69.21321670866666
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7128297065631617
Weighted FMeasure: 0.8580144451128918
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8950781509917929
Mean absolute error: 0.21878563175893015
Coverage of cases: 97.97101449275362
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.1447052226929304
Kappa statistic: 0.7128174636880998
Training time: 11.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8287784679089027
Weighted AreaUnderROC: 0.9018498043424062
Root mean squared error: 0.3550480208413262
Relative absolute error: 49.39852698269333
Root relative squared error: 71.00960416826524
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6530165634103685
Weighted FMeasure: 0.8285356574417998
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8897137259807958
Mean absolute error: 0.24699263491346665
Coverage of cases: 98.84057971014492
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 103.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.17939687598235288
Kappa statistic: 0.6524611996107155
Training time: 13.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8113329830612788
Weighted AreaUnderROC: 0.8917066998560282
Root mean squared error: 0.36488468138259306
Relative absolute error: 50.674629101311055
Root relative squared error: 72.97693627651861
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6181793931684901
Weighted FMeasure: 0.811401665571302
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8796182186034849
Mean absolute error: 0.25337314550655526
Coverage of cases: 98.84057971014492
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.1946770885992375
Kappa statistic: 0.6180833489449394
Training time: 12.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 87.3913043478261
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8378103444131996
Weighted AreaUnderROC: 0.8968008431359217
Root mean squared error: 0.35915912811443435
Relative absolute error: 50.227567654298525
Root relative squared error: 71.83182562288687
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6706753034651035
Weighted FMeasure: 0.8370280823155056
Iteration time: 14.0
Weighted AreaUnderPRC: 0.882046393270828
Mean absolute error: 0.2511378382714926
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 130.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1723857218735434
Kappa statistic: 0.6695063122241609
Training time: 14.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 86.52173913043478
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8561894662424885
Weighted AreaUnderROC: 0.9023460680756379
Root mean squared error: 0.3485002048103166
Relative absolute error: 49.125952225319914
Root relative squared error: 69.70004096206333
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7065956459202511
Weighted FMeasure: 0.8541836585314846
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8894244894162462
Mean absolute error: 0.24562976112659957
Coverage of cases: 98.84057971014492
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 145.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.15710550925665884
Kappa statistic: 0.7041673812382095
Training time: 15.0
		
Time end:Tue Oct 31 11.26.16 EET 2017