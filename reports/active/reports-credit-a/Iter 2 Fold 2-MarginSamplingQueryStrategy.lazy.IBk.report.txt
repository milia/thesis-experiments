Tue Oct 31 11.26.39 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.39 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7760672295431232
Weighted AreaUnderROC: 0.7736961990888692
Root mean squared error: 0.4719465908407744
Relative absolute error: 47.323943661971974
Root relative squared error: 94.38931816815487
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5442558760941668
Weighted FMeasure: 0.7716708052717582
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7142268606352811
Mean absolute error: 0.23661971830985987
Coverage of cases: 77.10144927536231
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2236220945758847
Kappa statistic: 0.5416014935163228
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7595802268644964
Weighted AreaUnderROC: 0.7569354729040593
Root mean squared error: 0.4851937781447854
Relative absolute error: 49.256250995381485
Root relative squared error: 97.03875562895709
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.513556769051582
Weighted FMeasure: 0.7594938560075516
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6979177877413006
Mean absolute error: 0.24628125497690742
Coverage of cases: 75.94202898550725
Instances selection time: 9.0
Test time: 11.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.24554934404695386
Kappa statistic: 0.5135479486961693
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7587636181909045
Weighted AreaUnderROC: 0.7537907119058951
Root mean squared error: 0.48613383222739287
Relative absolute error: 49.05078992035502
Root relative squared error: 97.22676644547857
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.511191665840205
Weighted FMeasure: 0.7585683451801545
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6957645525063556
Mean absolute error: 0.24525394960177513
Coverage of cases: 75.94202898550725
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.25183886604328215
Kappa statistic: 0.5104709804256774
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.762919587104658
Weighted AreaUnderROC: 0.7532637519548515
Root mean squared error: 0.48384988487301683
Relative absolute error: 48.337205443079945
Root relative squared error: 96.76997697460337
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5166381223352069
Weighted FMeasure: 0.7599784891965293
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6968875137418501
Mean absolute error: 0.24168602721539972
Coverage of cases: 76.23188405797102
Instances selection time: 9.0
Test time: 22.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2557913366700073
Kappa statistic: 0.5129803057430106
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7740193076184583
Weighted AreaUnderROC: 0.7598592506969469
Root mean squared error: 0.47539083325842085
Relative absolute error: 46.51502063537766
Root relative squared error: 95.07816665168417
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.535691942726687
Weighted FMeasure: 0.7675716110047022
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7051024048105223
Mean absolute error: 0.2325751031768883
Coverage of cases: 77.10144927536231
Instances selection time: 24.0
Test time: 30.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.25129599135972913
Kappa statistic: 0.5286966746787944
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.797101449275363
Correctly Classified Instances: 74.20289855072464
Weighted Precision: 0.7489098928796473
Weighted AreaUnderROC: 0.7267627660297816
Root mean squared error: 0.5049633731288886
Relative absolute error: 52.1603525722519
Root relative squared error: 100.99267462577772
Weighted TruePositiveRate: 0.7420289855072464
Weighted MatthewsCorrelation: 0.4782620789579403
Weighted FMeasure: 0.7352772606623779
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6743541272841841
Mean absolute error: 0.2608017628612595
Coverage of cases: 74.20289855072464
Instances selection time: 21.0
Test time: 33.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7420289855072464
Weighted FalsePositiveRate: 0.2885034534476829
Kappa statistic: 0.46492985971943895
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7526411549294845
Weighted AreaUnderROC: 0.7293805670769021
Root mean squared error: 0.5024230880905353
Relative absolute error: 51.52743000227644
Root relative squared error: 100.48461761810705
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.4848475955042318
Weighted FMeasure: 0.7379810201004047
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6772153790559919
Mean absolute error: 0.2576371500113822
Coverage of cases: 74.4927536231884
Instances selection time: 10.0
Test time: 19.0
Accumulative iteration time: 89.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.28616640207807975
Kappa statistic: 0.4706005440468717
Training time: 0.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.405797101449274
Correctly Classified Instances: 71.59420289855072
Weighted Precision: 0.7284750507825245
Weighted AreaUnderROC: 0.6962840824097369
Root mean squared error: 0.530460120752385
Relative absolute error: 57.220962978226495
Root relative squared error: 106.092024150477
Weighted TruePositiveRate: 0.7159420289855073
Weighted MatthewsCorrelation: 0.4276504162596007
Weighted FMeasure: 0.7041096891070594
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6479594691618858
Mean absolute error: 0.2861048148911325
Coverage of cases: 71.59420289855072
Instances selection time: 9.0
Test time: 21.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7159420289855073
Weighted FalsePositiveRate: 0.32337386416603353
Kappa statistic: 0.40584141712357663
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.536231884057973
Correctly Classified Instances: 72.46376811594203
Weighted Precision: 0.7449073989321129
Weighted AreaUnderROC: 0.7028795811518325
Root mean squared error: 0.5224911193297443
Relative absolute error: 55.461446765794676
Root relative squared error: 104.49822386594887
Weighted TruePositiveRate: 0.7246376811594203
Weighted MatthewsCorrelation: 0.4515580327380788
Weighted FMeasure: 0.71043436290761
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6564137918836158
Mean absolute error: 0.2773072338289734
Coverage of cases: 72.46376811594203
Instances selection time: 8.0
Test time: 23.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7246376811594203
Weighted FalsePositiveRate: 0.31887851885575536
Kappa statistic: 0.42139641627681174
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7452256404548817
Weighted AreaUnderROC: 0.7241449649826613
Root mean squared error: 0.5087305731203817
Relative absolute error: 52.55499740169758
Root relative squared error: 101.74611462407634
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.47172543277070117
Weighted FMeasure: 0.7325741890959282
Iteration time: 7.0
Weighted AreaUnderPRC: 0.671525704143688
Mean absolute error: 0.2627749870084879
Coverage of cases: 73.91304347826087
Instances selection time: 7.0
Test time: 24.0
Accumulative iteration time: 113.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.29084050481728607
Kappa statistic: 0.45926648322942415
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.768874563920558
Weighted AreaUnderROC: 0.7591283062487251
Root mean squared error: 0.47977739930336444
Relative absolute error: 46.7725546820685
Root relative squared error: 95.95547986067288
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5286013515590237
Weighted FMeasure: 0.7658326723868579
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7028762184769834
Mean absolute error: 0.2338627734103425
Coverage of cases: 76.81159420289855
Instances selection time: 6.0
Test time: 25.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2498593295315353
Kappa statistic: 0.5248588348712299
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7676694851769287
Weighted AreaUnderROC: 0.7616441150472564
Root mean squared error: 0.4798980747301356
Relative absolute error: 46.74535584441465
Root relative squared error: 95.97961494602713
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5285983698877461
Weighted FMeasure: 0.7669468188117494
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7041188757384058
Mean absolute error: 0.23372677922207322
Coverage of cases: 76.81159420289855
Instances selection time: 5.0
Test time: 26.0
Accumulative iteration time: 124.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2448277119344727
Kappa statistic: 0.5272677445875582
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7763033483258371
Weighted AreaUnderROC: 0.7713843747875161
Root mean squared error: 0.47091766753788067
Relative absolute error: 44.99370893331461
Root relative squared error: 94.18353350757613
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5466292662517664
Weighted FMeasure: 0.7760212358900228
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7137240727462356
Mean absolute error: 0.22496854466657304
Coverage of cases: 77.68115942028986
Instances selection time: 4.0
Test time: 28.0
Accumulative iteration time: 128.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.23404284462786623
Kappa statistic: 0.5458586203949056
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.770521211110648
Weighted AreaUnderROC: 0.7667777248929082
Root mean squared error: 0.4770859842321289
Relative absolute error: 46.12461141030694
Root relative squared error: 95.41719684642578
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5354703576629749
Weighted FMeasure: 0.7706044079298392
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7084631986117631
Mean absolute error: 0.2306230570515347
Coverage of cases: 77.10144927536231
Instances selection time: 2.0
Test time: 29.0
Accumulative iteration time: 130.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2374590429678069
Kappa statistic: 0.5352386474088979
Training time: 0.0
		
Time end:Tue Oct 31 11.26.40 EET 2017