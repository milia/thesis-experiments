Tue Oct 31 11.25.50 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.50 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7592521323070522
Weighted AreaUnderROC: 0.7506638071895425
Root mean squared error: 0.49048925589142883
Relative absolute error: 48.11594202898551
Root relative squared error: 98.09785117828577
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5096539093023086
Weighted FMeasure: 0.7574439859430708
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6942266836637605
Mean absolute error: 0.24057971014492754
Coverage of cases: 75.94202898550725
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.2580926754759875
Kappa statistic: 0.5070495274492589
Training time: 7.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8221035881114932
Weighted AreaUnderROC: 0.801470588235294
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6277035422284755
Weighted FMeasure: 0.8107951077801918
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7524426698924035
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21155157715260017
Kappa statistic: 0.6160267111853088
Training time: 12.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8221035881114932
Weighted AreaUnderROC: 0.801470588235294
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6277035422284755
Weighted FMeasure: 0.8107951077801918
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7524426698924035
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21155157715260017
Kappa statistic: 0.6160267111853088
Training time: 11.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8115516062884485
Weighted AreaUnderROC: 0.784466911764706
Root mean squared error: 0.4472135954999579
Relative absolute error: 40.0
Root relative squared error: 89.44271909999159
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6008964908521492
Weighted FMeasure: 0.7946131165829946
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7359209072690318
Mean absolute error: 0.2
Coverage of cases: 80.0
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.23106617647058825
Kappa statistic: 0.5840540965560623
Training time: 18.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8236283105848323
Weighted AreaUnderROC: 0.792279411764706
Root mean squared error: 0.4373835248681493
Relative absolute error: 38.26086956521739
Root relative squared error: 87.47670497362986
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6216631603967672
Weighted FMeasure: 0.8029087568891611
Iteration time: 13.0
Weighted AreaUnderPRC: 0.746043254247413
Mean absolute error: 0.19130434782608696
Coverage of cases: 80.8695652173913
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 70.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22413682864450127
Kappa statistic: 0.6013236684525686
Training time: 11.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8218439452839952
Weighted AreaUnderROC: 0.7929432189542484
Root mean squared error: 0.4373835248681493
Relative absolute error: 38.26086956521739
Root relative squared error: 87.47670497362986
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6201925723347433
Weighted FMeasure: 0.803335656614876
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7459201863093079
Mean absolute error: 0.19130434782608696
Coverage of cases: 80.8695652173913
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 82.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22280921426541628
Kappa statistic: 0.6018673940411247
Training time: 11.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8258936901874018
Weighted AreaUnderROC: 0.7955473856209151
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6271428503993228
Weighted FMeasure: 0.8061072238575495
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7493452665877762
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 94.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.22049943165672065
Kappa statistic: 0.6076321452942104
Training time: 11.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8258936901874018
Weighted AreaUnderROC: 0.7955473856209151
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6271428503993228
Weighted FMeasure: 0.8061072238575495
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7493452665877762
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.22049943165672065
Kappa statistic: 0.6076321452942104
Training time: 16.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7914066496163682
Weighted AreaUnderROC: 0.7353962418300654
Root mean squared error: 0.49048925589142883
Relative absolute error: 48.11594202898551
Root relative squared error: 98.09785117828577
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5326408923643765
Weighted FMeasure: 0.7451876348676272
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6926037123524058
Mean absolute error: 0.24057971014492754
Coverage of cases: 75.94202898550725
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 124.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.28862780619494177
Kappa statistic: 0.49134026112443385
Training time: 12.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8258936901874018
Weighted AreaUnderROC: 0.7955473856209151
Root mean squared error: 0.4340573661412156
Relative absolute error: 37.68115942028986
Root relative squared error: 86.81147322824312
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6271428503993228
Weighted FMeasure: 0.8061072238575495
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7493452665877762
Mean absolute error: 0.18840579710144928
Coverage of cases: 81.15942028985508
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 143.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.22049943165672065
Kappa statistic: 0.6076321452942104
Training time: 18.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8272536919026487
Weighted AreaUnderROC: 0.8034109477124183
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6354825655252121
Weighted FMeasure: 0.8132260847356839
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7558256480886973
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 168.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.21056940892298948
Kappa statistic: 0.6212556850854725
Training time: 24.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8272536919026487
Weighted AreaUnderROC: 0.8034109477124183
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6354825655252121
Weighted FMeasure: 0.8132260847356839
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7558256480886973
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 201.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.21056940892298948
Kappa statistic: 0.6212556850854725
Training time: 33.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8349584764961584
Weighted AreaUnderROC: 0.8289930555555556
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6642336378092137
Weighted FMeasure: 0.834002779772954
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7776759254000852
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 235.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17679649758454105
Kappa statistic: 0.6628259863175762
Training time: 33.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8478217031834959
Weighted AreaUnderROC: 0.8454861111111113
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6865690771926479
Weighted FMeasure: 0.8410873795796014
Iteration time: 44.0
Weighted AreaUnderPRC: 0.793263378481113
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 279.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1496074879227053
Kappa statistic: 0.6814725285793422
Training time: 43.0
		
Time end:Tue Oct 31 11.25.51 EET 2017