Tue Oct 31 11.25.48 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.48 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.36231884057971
Correctly Classified Instances: 82.6376811594203
Weighted Precision: 0.8300927867892443
Weighted AreaUnderROC: 0.8265875751776198
Root mean squared error: 0.41565242424112875
Relative absolute error: 34.72463768115942
Root relative squared error: 83.13048484822575
Weighted TruePositiveRate: 0.8263768115942028
Weighted MatthewsCorrelation: 0.6528487206317224
Weighted FMeasure: 0.8262876197250899
Iteration time: 15.3
Weighted AreaUnderPRC: 0.773403037923688
Mean absolute error: 0.1736231884057971
Coverage of cases: 82.6376811594203
Instances selection time: 2.0
Test time: 3.8
Accumulative iteration time: 15.3
Weighted Recall: 0.8263768115942028
Weighted FalsePositiveRate: 0.17320166123896324
Kappa statistic: 0.6497260025747436
Training time: 13.3
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.15942028985507
Correctly Classified Instances: 82.84057971014494
Weighted Precision: 0.8334178226683393
Weighted AreaUnderROC: 0.8239090228448195
Root mean squared error: 0.41378007124285004
Relative absolute error: 34.31884057971014
Root relative squared error: 82.75601424857004
Weighted TruePositiveRate: 0.8284057971014492
Weighted MatthewsCorrelation: 0.6565171628039796
Weighted FMeasure: 0.826998398906289
Iteration time: 14.1
Weighted AreaUnderPRC: 0.7727758741571767
Mean absolute error: 0.1715942028985507
Coverage of cases: 82.84057971014494
Instances selection time: 2.2
Test time: 4.4
Accumulative iteration time: 29.4
Weighted Recall: 0.8284057971014492
Weighted FalsePositiveRate: 0.18058775141181055
Kappa statistic: 0.65042924288539
Training time: 11.9
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463767
Weighted Precision: 0.8360429884255801
Weighted AreaUnderROC: 0.8235044626191742
Root mean squared error: 0.4126239178641793
Relative absolute error: 34.20289855072464
Root relative squared error: 82.52478357283584
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6592002344353757
Weighted FMeasure: 0.8269580561000562
Iteration time: 17.2
Weighted AreaUnderPRC: 0.7736860127765286
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463767
Instances selection time: 1.3
Test time: 4.4
Accumulative iteration time: 46.6
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.18197658200802821
Kappa statistic: 0.6506633942974792
Training time: 15.9
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8249221870007883
Weighted AreaUnderROC: 0.8073161536945415
Root mean squared error: 0.42958188587010787
Relative absolute error: 37.10144927536232
Root relative squared error: 85.91637717402159
Weighted TruePositiveRate: 0.8144927536231883
Weighted MatthewsCorrelation: 0.6324845575784579
Weighted FMeasure: 0.811386773072815
Iteration time: 19.2
Weighted AreaUnderPRC: 0.7573539002873562
Mean absolute error: 0.18550724637681157
Coverage of cases: 81.44927536231884
Instances selection time: 2.6
Test time: 4.4
Accumulative iteration time: 65.8
Weighted Recall: 0.8144927536231883
Weighted FalsePositiveRate: 0.19986044623410554
Kappa statistic: 0.6198825317682725
Training time: 16.6
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.434782608695652
Correctly Classified Instances: 81.56521739130434
Weighted Precision: 0.8308015367984257
Weighted AreaUnderROC: 0.8060441906415112
Root mean squared error: 0.42834699365044193
Relative absolute error: 36.869565217391305
Root relative squared error: 85.66939873008839
Weighted TruePositiveRate: 0.8156521739130435
Weighted MatthewsCorrelation: 0.638561715165238
Weighted FMeasure: 0.8112734090670368
Iteration time: 20.5
Weighted AreaUnderPRC: 0.7585116645959785
Mean absolute error: 0.18434782608695652
Coverage of cases: 81.56521739130434
Instances selection time: 2.4
Test time: 3.7
Accumulative iteration time: 86.3
Weighted Recall: 0.8156521739130435
Weighted FalsePositiveRate: 0.20356379263002097
Kappa statistic: 0.6203763499709346
Training time: 18.1
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.86956521739128
Weighted Precision: 0.8284875028970878
Weighted AreaUnderROC: 0.7973344727189622
Root mean squared error: 0.43618885677576075
Relative absolute error: 38.26086956521739
Root relative squared error: 87.23777135515215
Weighted TruePositiveRate: 0.8086956521739129
Weighted MatthewsCorrelation: 0.6281250734771604
Weighted FMeasure: 0.8029324200008912
Iteration time: 20.5
Weighted AreaUnderPRC: 0.7510959420996305
Mean absolute error: 0.191304347826087
Coverage of cases: 80.86956521739128
Instances selection time: 2.2
Test time: 3.9
Accumulative iteration time: 106.8
Weighted Recall: 0.8086956521739129
Weighted FalsePositiveRate: 0.21402670673598853
Kappa statistic: 0.6046155373752946
Training time: 18.3
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8258621075143833
Weighted AreaUnderROC: 0.7904911737124645
Root mean squared error: 0.44289619935898716
Relative absolute error: 39.42028985507246
Root relative squared error: 88.57923987179745
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6189741390016041
Weighted FMeasure: 0.796191649905104
Iteration time: 19.3
Weighted AreaUnderPRC: 0.7448331578950862
Mean absolute error: 0.19710144927536233
Coverage of cases: 80.28985507246377
Instances selection time: 2.2
Test time: 3.9
Accumulative iteration time: 126.1
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.22191620329970868
Kappa statistic: 0.5919541515340342
Training time: 17.1
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.72463768115942
Correctly Classified Instances: 79.27536231884059
Weighted Precision: 0.8184656306526392
Weighted AreaUnderROC: 0.7777320250049885
Root mean squared error: 0.4532824911428497
Relative absolute error: 41.44927536231884
Root relative squared error: 90.65649822856994
Weighted TruePositiveRate: 0.7927536231884058
Weighted MatthewsCorrelation: 0.5991134764773655
Weighted FMeasure: 0.783725031409424
Iteration time: 22.9
Weighted AreaUnderPRC: 0.7332930722471003
Mean absolute error: 0.20724637681159425
Coverage of cases: 79.27536231884059
Instances selection time: 1.5
Test time: 3.5
Accumulative iteration time: 149.0
Weighted Recall: 0.7927536231884058
Weighted FalsePositiveRate: 0.23728957317842875
Kappa statistic: 0.5679327852502931
Training time: 21.4
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.130434782608695
Correctly Classified Instances: 78.8695652173913
Weighted Precision: 0.8197485489218618
Weighted AreaUnderROC: 0.7724425839469534
Root mean squared error: 0.45735695373269863
Relative absolute error: 42.26086956521739
Root relative squared error: 91.47139074653973
Weighted TruePositiveRate: 0.788695652173913
Weighted MatthewsCorrelation: 0.5946451499592802
Weighted FMeasure: 0.777578462302661
Iteration time: 33.4
Weighted AreaUnderPRC: 0.7294810744798752
Mean absolute error: 0.21130434782608692
Coverage of cases: 78.8695652173913
Instances selection time: 1.5
Test time: 3.3
Accumulative iteration time: 182.4
Weighted Recall: 0.788695652173913
Weighted FalsePositiveRate: 0.24381048428000632
Kappa statistic: 0.5579405174300769
Training time: 31.9
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971016
Weighted Precision: 0.8366561514793689
Weighted AreaUnderROC: 0.8134585229855864
Root mean squared error: 0.41947351453526033
Relative absolute error: 35.36231884057972
Root relative squared error: 83.89470290705206
Weighted TruePositiveRate: 0.8231884057971015
Weighted MatthewsCorrelation: 0.6522687128334718
Weighted FMeasure: 0.8192864143929939
Iteration time: 38.2
Weighted AreaUnderPRC: 0.7665703873282969
Mean absolute error: 0.17681159420289855
Coverage of cases: 82.31884057971016
Instances selection time: 1.4
Test time: 3.9
Accumulative iteration time: 220.6
Weighted Recall: 0.8231884057971015
Weighted FalsePositiveRate: 0.1962713598259286
Kappa statistic: 0.6358073403140553
Training time: 36.8
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.347826086956523
Correctly Classified Instances: 83.65217391304347
Weighted Precision: 0.8434227556680307
Weighted AreaUnderROC: 0.8301063136562732
Root mean squared error: 0.4035157980215412
Relative absolute error: 32.69565217391305
Root relative squared error: 80.70315960430824
Weighted TruePositiveRate: 0.8365217391304348
Weighted MatthewsCorrelation: 0.6742495659768017
Weighted FMeasure: 0.8346165338738494
Iteration time: 46.1
Weighted AreaUnderPRC: 0.7818496861155805
Mean absolute error: 0.1634782608695652
Coverage of cases: 83.65217391304347
Instances selection time: 1.3
Test time: 3.8
Accumulative iteration time: 266.7
Weighted Recall: 0.8365217391304348
Weighted FalsePositiveRate: 0.17630911181788828
Kappa statistic: 0.6656449277227516
Training time: 44.8
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.17391304347826
Correctly Classified Instances: 83.82608695652173
Weighted Precision: 0.8440245238456482
Weighted AreaUnderROC: 0.8349367767494116
Root mean squared error: 0.4014463973100389
Relative absolute error: 32.34782608695652
Root relative squared error: 80.28927946200778
Weighted TruePositiveRate: 0.8382608695652174
Weighted MatthewsCorrelation: 0.677648033081989
Weighted FMeasure: 0.8371317615772289
Iteration time: 41.3
Weighted AreaUnderPRC: 0.7853602579215108
Mean absolute error: 0.16173913043478258
Coverage of cases: 83.82608695652173
Instances selection time: 0.8
Test time: 3.8
Accumulative iteration time: 308.0
Weighted Recall: 0.8382608695652174
Weighted FalsePositiveRate: 0.1683873160663944
Kappa statistic: 0.6711834896185053
Training time: 40.5
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8479737544645098
Weighted AreaUnderROC: 0.8446439028967132
Root mean squared error: 0.39532776591152163
Relative absolute error: 31.304347826086957
Root relative squared error: 79.06555318230434
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.688520824587665
Weighted FMeasure: 0.8435265329970528
Iteration time: 44.7
Weighted AreaUnderPRC: 0.793581196191676
Mean absolute error: 0.1565217391304348
Coverage of cases: 84.34782608695652
Instances selection time: 1.0
Test time: 3.4
Accumulative iteration time: 352.7
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15419045507613888
Kappa statistic: 0.6848772117093176
Training time: 43.7
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.159420289855074
Correctly Classified Instances: 84.84057971014491
Weighted Precision: 0.8568292050081296
Weighted AreaUnderROC: 0.8527995240417285
Root mean squared error: 0.3888738576392
Relative absolute error: 30.318840579710148
Root relative squared error: 77.77477152784
Weighted TruePositiveRate: 0.8484057971014494
Weighted MatthewsCorrelation: 0.703351914362188
Weighted FMeasure: 0.8486173748279586
Iteration time: 67.7
Weighted AreaUnderPRC: 0.8028213990933777
Mean absolute error: 0.1515942028985507
Coverage of cases: 84.84057971014491
Instances selection time: 0.5
Test time: 3.8
Accumulative iteration time: 420.4
Weighted Recall: 0.8484057971014494
Weighted FalsePositiveRate: 0.1428067490179925
Kappa statistic: 0.6966555402783026
Training time: 67.2
		
Time end:Tue Oct 31 11.25.58 EET 2017