Tue Oct 31 11.26.47 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.47 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 98.84057971014492
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8345171700060245
Weighted AreaUnderROC: 0.8666768790849673
Root mean squared error: 0.36878147925526955
Relative absolute error: 43.56241234221599
Root relative squared error: 73.7562958510539
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6644617426715108
Weighted FMeasure: 0.8344721796537528
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8256028554256453
Mean absolute error: 0.21781206171107997
Coverage of cases: 98.84057971014492
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17281365444728616
Kappa statistic: 0.6641733695373737
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 97.97101449275362
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8438768432974062
Weighted AreaUnderROC: 0.834541802832244
Root mean squared error: 0.3749062118125631
Relative absolute error: 43.65135133251093
Root relative squared error: 74.98124236251262
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6817508724058804
Weighted FMeasure: 0.8410350081024176
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7937410410794381
Mean absolute error: 0.21825675666255465
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1549179454390452
Kappa statistic: 0.6797954741051991
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8606663821128704
Weighted AreaUnderROC: 0.8578941993464053
Root mean squared error: 0.3493968299985101
Relative absolute error: 47.097548534221524
Root relative squared error: 69.87936599970202
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.711346663322964
Weighted FMeasure: 0.8526319450215195
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8079367316073001
Mean absolute error: 0.2354877426711076
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13638551435066779
Kappa statistic: 0.7050243926972789
Training time: 2.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 90.72463768115942
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7957715919603061
Weighted AreaUnderROC: 0.7454895152505447
Root mean squared error: 0.43110411095851653
Relative absolute error: 51.7150964608716
Root relative squared error: 86.2208221917033
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5661063089359003
Weighted FMeasure: 0.7757830953535974
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7471553962790366
Mean absolute error: 0.258575482304358
Coverage of cases: 90.14492753623189
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.251562944018187
Kappa statistic: 0.5466491458607097
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 78.84057971014492
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7775802364304574
Weighted AreaUnderROC: 0.7161117919389978
Root mean squared error: 0.4518406389875848
Relative absolute error: 50.47172025325478
Root relative squared error: 90.36812779751696
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5331688629518833
Weighted FMeasure: 0.7616189777150013
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7376687001509958
Mean absolute error: 0.2523586012662739
Coverage of cases: 86.08695652173913
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2644394714407502
Kappa statistic: 0.517415023080151
Training time: 3.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8556710380729535
Weighted AreaUnderROC: 0.911900871459695
Root mean squared error: 0.3313683050243328
Relative absolute error: 41.99310033978234
Root relative squared error: 66.27366100486655
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7058182583546474
Weighted FMeasure: 0.8543106307081463
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8852935834201041
Mean absolute error: 0.20996550169891168
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.1566451761864166
Kappa statistic: 0.704035412806259
Training time: 3.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 96.23188405797102
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8298282118671254
Weighted AreaUnderROC: 0.8312169946481007
Root mean squared error: 0.3740471813076393
Relative absolute error: 45.059879869535195
Root relative squared error: 74.80943626152786
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6549703103239107
Weighted FMeasure: 0.8292321578819818
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8163465287573155
Mean absolute error: 0.22529939934767598
Coverage of cases: 95.94202898550725
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.1721227621483376
Kappa statistic: 0.6546898060970022
Training time: 4.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 65.3623188405797
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8966843681917213
Root mean squared error: 0.35304836752051294
Relative absolute error: 43.165805494980205
Root relative squared error: 70.60967350410259
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8634005226956699
Mean absolute error: 0.21582902747490101
Coverage of cases: 94.4927536231884
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 6.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.36060520657467154
Relative absolute error: 42.57480673755127
Root relative squared error: 72.1210413149343
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.21287403368775634
Coverage of cases: 92.7536231884058
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 8.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.3607785186325392
Relative absolute error: 42.06722642632425
Root relative squared error: 72.15570372650784
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.21033613213162125
Coverage of cases: 92.7536231884058
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 5.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.36129294734512585
Relative absolute error: 41.425631995909896
Root relative squared error: 72.25858946902517
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.20712815997954948
Coverage of cases: 92.7536231884058
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 5.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.361519991920516
Relative absolute error: 41.08491981288153
Root relative squared error: 72.3039983841032
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.20542459906440766
Coverage of cases: 92.7536231884058
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 4.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.3618748739665171
Relative absolute error: 40.704619888402085
Root relative squared error: 72.37497479330341
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.20352309944201044
Coverage of cases: 92.7536231884058
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 5.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8382931586110995
Weighted AreaUnderROC: 0.8652982026143791
Root mean squared error: 0.36214917152852893
Relative absolute error: 40.4299588318336
Root relative squared error: 72.42983430570578
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.672215442862269
Weighted FMeasure: 0.8378740680593488
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8377035831326936
Mean absolute error: 0.202149794159168
Coverage of cases: 92.7536231884058
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16386579994316566
Kappa statistic: 0.6720309635363618
Training time: 5.0
		
Time end:Tue Oct 31 11.26.48 EET 2017