Tue Oct 31 11.27.39 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.27.39 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8086593486381397
Weighted AreaUnderROC: 0.8629890120299327
Root mean squared error: 0.4176586982951496
Relative absolute error: 41.47125604323425
Root relative squared error: 83.53173965902992
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6023918815935821
Weighted FMeasure: 0.7993278722957361
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8381144498255558
Mean absolute error: 0.20735628021617125
Coverage of cases: 87.53623188405797
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.22211832196646772
Kappa statistic: 0.5925810149004898
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 61.01449275362319
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.800501159598006
Weighted AreaUnderROC: 0.885409118357488
Root mean squared error: 0.4174642637222867
Relative absolute error: 42.65154001200326
Root relative squared error: 83.49285274445734
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5773008132217676
Weighted FMeasure: 0.7822434975630942
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8695829611268314
Mean absolute error: 0.2132577000600163
Coverage of cases: 89.27536231884058
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.24428815004262575
Kappa statistic: 0.5593407170227286
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7839036234095341
Weighted AreaUnderROC: 0.8840966005967603
Root mean squared error: 0.4232116749582316
Relative absolute error: 42.640394780897104
Root relative squared error: 84.64233499164632
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.545600821435149
Weighted FMeasure: 0.7675785032721261
Iteration time: 3.0
Weighted AreaUnderPRC: 0.868010308679329
Mean absolute error: 0.2132019739044855
Coverage of cases: 88.98550724637681
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.25849229184427397
Kappa statistic: 0.5294796475031472
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 58.98550724637681
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7910597443894086
Weighted AreaUnderROC: 0.8854998460736953
Root mean squared error: 0.432412139223339
Relative absolute error: 43.73036068085407
Root relative squared error: 86.4824278446678
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5549043368909844
Weighted FMeasure: 0.7692769000115328
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8732145981840005
Mean absolute error: 0.21865180340427035
Coverage of cases: 88.1159420289855
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.25883773799374826
Kappa statistic: 0.5339228380441077
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7859368077334998
Weighted AreaUnderROC: 0.8837107487922706
Root mean squared error: 0.443780661098468
Relative absolute error: 45.26692866361641
Root relative squared error: 88.7561322196936
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.539585133818378
Weighted FMeasure: 0.7588283911039871
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8716239701891928
Mean absolute error: 0.22633464331808206
Coverage of cases: 87.53623188405797
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.271077543336175
Kappa statistic: 0.5141016161402767
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7790150842945874
Weighted AreaUnderROC: 0.881002533863787
Root mean squared error: 0.45533166061819774
Relative absolute error: 47.44405556625811
Root relative squared error: 91.06633212363955
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5227854572898346
Weighted FMeasure: 0.7488101590976927
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8681244740364964
Mean absolute error: 0.23722027783129054
Coverage of cases: 86.3768115942029
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.2819897342995169
Kappa statistic: 0.4948399047367029
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 58.98550724637681
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7769905170871354
Weighted AreaUnderROC: 0.8822885585393578
Root mean squared error: 0.4560413058654395
Relative absolute error: 47.38005484203228
Root relative squared error: 91.2082611730879
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5212058784571323
Weighted FMeasure: 0.7494652673663169
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8688200462265904
Mean absolute error: 0.2369002742101614
Coverage of cases: 85.79710144927536
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.28066211992043194
Kappa statistic: 0.4955340626816765
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 58.26086956521739
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.774649795699456
Weighted AreaUnderROC: 0.8816682651321398
Root mean squared error: 0.4615910498986397
Relative absolute error: 47.97329648980717
Root relative squared error: 92.31820997972794
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5155694202378064
Weighted FMeasure: 0.7461181133770325
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8677458012031128
Mean absolute error: 0.23986648244903586
Coverage of cases: 85.21739130434783
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.28429951690821254
Kappa statistic: 0.4891051406811931
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 58.11594202898551
Incorrectly Classified Instances: 24.92753623188406
Correctly Classified Instances: 75.07246376811594
Weighted Precision: 0.7679780950297452
Weighted AreaUnderROC: 0.8782225359950743
Root mean squared error: 0.46866431989594926
Relative absolute error: 49.10093077645601
Root relative squared error: 93.73286397918986
Weighted TruePositiveRate: 0.7507246376811594
Weighted MatthewsCorrelation: 0.5027470838780859
Weighted FMeasure: 0.7400733065526761
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8639460659236604
Mean absolute error: 0.24550465388228004
Coverage of cases: 84.6376811594203
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7507246376811594
Weighted FalsePositiveRate: 0.2902466965046889
Kappa statistic: 0.47694097736407864
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 56.95652173913044
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7812613272311212
Weighted AreaUnderROC: 0.8820502687790092
Root mean squared error: 0.469219035585547
Relative absolute error: 48.55920558169725
Root relative squared error: 93.8438071171094
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5207538122988019
Weighted FMeasure: 0.7440253089044442
Iteration time: 2.0
Weighted AreaUnderPRC: 0.870566014278459
Mean absolute error: 0.24279602790848626
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.2882823600454675
Kappa statistic: 0.4869888475836431
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 58.26086956521739
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7813214963691666
Weighted AreaUnderROC: 0.8844861525528085
Root mean squared error: 0.45662770114392387
Relative absolute error: 47.220700407527765
Root relative squared error: 91.32554022878477
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5283917565975269
Weighted FMeasure: 0.7521629202013889
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8725838931807943
Mean absolute error: 0.23610350203763883
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2783523373117363
Kappa statistic: 0.5012693039983076
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 57.10144927536232
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7836285560923242
Weighted AreaUnderROC: 0.8834989521170787
Root mean squared error: 0.46255475965596127
Relative absolute error: 47.58078142258916
Root relative squared error: 92.51095193119225
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5339914322162129
Weighted FMeasure: 0.7555022488755623
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8702325219532853
Mean absolute error: 0.23790390711294582
Coverage of cases: 83.47826086956522
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27471494032395566
Kappa statistic: 0.5076898684001904
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 57.53623188405797
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7816733510040382
Weighted AreaUnderROC: 0.8824777102870134
Root mean squared error: 0.4592087597865013
Relative absolute error: 47.48182630849697
Root relative squared error: 91.84175195730026
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5324590491452523
Weighted FMeasure: 0.7561198579545787
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8697106815527229
Mean absolute error: 0.23740913154248486
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2733873259448707
Kappa statistic: 0.5083654404391197
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 26.3768115942029
Correctly Classified Instances: 73.6231884057971
Weighted Precision: 0.752189884649512
Weighted AreaUnderROC: 0.8783473051056172
Root mean squared error: 0.4656982844751446
Relative absolute error: 49.29711924739284
Root relative squared error: 93.13965689502892
Weighted TruePositiveRate: 0.736231884057971
Weighted MatthewsCorrelation: 0.47134560480784016
Weighted FMeasure: 0.7245990900950606
Iteration time: 1.0
Weighted AreaUnderPRC: 0.863583556824017
Mean absolute error: 0.24648559623696423
Coverage of cases: 85.5072463768116
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.736231884057971
Weighted FalsePositiveRate: 0.305778452685422
Kappa statistic: 0.44614977507277065
Training time: 1.0
		
Time end:Tue Oct 31 11.27.39 EET 2017