Tue Oct 31 11.25.25 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.25 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.3768115942029
Correctly Classified Instances: 73.6231884057971
Weighted Precision: 0.7353632018720266
Weighted AreaUnderROC: 0.7297035425307676
Root mean squared error: 0.5064942265294221
Relative absolute error: 54.08450704225367
Root relative squared error: 101.29884530588443
Weighted TruePositiveRate: 0.736231884057971
Weighted MatthewsCorrelation: 0.46358676556509687
Weighted FMeasure: 0.7350387006101134
Iteration time: 27.0
Weighted AreaUnderPRC: 0.6723918659133618
Mean absolute error: 0.27042253521126836
Coverage of cases: 73.6231884057971
Instances selection time: 22.0
Test time: 9.0
Accumulative iteration time: 27.0
Weighted Recall: 0.736231884057971
Weighted FalsePositiveRate: 0.27682479899643575
Kappa statistic: 0.46260762397083244
Training time: 5.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7556984670881711
Weighted AreaUnderROC: 0.7535867274087169
Root mean squared error: 0.49100172604240955
Relative absolute error: 50.3901895206244
Root relative squared error: 98.2003452084819
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5050147497756362
Weighted FMeasure: 0.7541482766283404
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6941125271564694
Mean absolute error: 0.25195094760312203
Coverage of cases: 75.3623188405797
Instances selection time: 15.0
Test time: 13.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.24644973358836325
Kappa statistic: 0.5043187234833758
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.797101449275363
Correctly Classified Instances: 74.20289855072464
Weighted Precision: 0.7489855072463767
Weighted AreaUnderROC: 0.7456313320187665
Root mean squared error: 0.5033925410142874
Relative absolute error: 52.46637942290102
Root relative squared error: 100.67850820285749
Weighted TruePositiveRate: 0.7420289855072464
Weighted MatthewsCorrelation: 0.4884805973230539
Weighted FMeasure: 0.7427587909351742
Iteration time: 17.0
Weighted AreaUnderPRC: 0.686121163020974
Mean absolute error: 0.2623318971145051
Coverage of cases: 74.20289855072464
Instances selection time: 16.0
Test time: 13.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7420289855072464
Weighted FalsePositiveRate: 0.2507663214697133
Kappa statistic: 0.4848586527975841
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7526323062854857
Weighted AreaUnderROC: 0.7461582919698103
Root mean squared error: 0.5068975196488755
Relative absolute error: 52.90408230998999
Root relative squared error: 101.37950392977511
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4909830226523326
Weighted FMeasure: 0.739481183777859
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6869873233309337
Mean absolute error: 0.26452041154994993
Coverage of cases: 73.91304347826087
Instances selection time: 17.0
Test time: 14.0
Accumulative iteration time: 78.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.24681385084298813
Kappa statistic: 0.482603479304139
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7485291869884808
Weighted AreaUnderROC: 0.737573944380227
Root mean squared error: 0.5185545624240582
Relative absolute error: 55.09549860831172
Root relative squared error: 103.71091248481163
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.4768531743785993
Weighted FMeasure: 0.7270554134697357
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6793649953606237
Mean absolute error: 0.2754774930415586
Coverage of cases: 72.7536231884058
Instances selection time: 18.0
Test time: 16.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.25238834312360375
Kappa statistic: 0.4629194130701201
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.115942028985508
Correctly Classified Instances: 71.8840579710145
Weighted Precision: 0.7459101435197896
Weighted AreaUnderROC: 0.7309784456381315
Root mean squared error: 0.5271671505075773
Relative absolute error: 56.74379184676669
Root relative squared error: 105.43343010151547
Weighted TruePositiveRate: 0.7188405797101449
Weighted MatthewsCorrelation: 0.4666446628141043
Weighted FMeasure: 0.7174499307491634
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6737249443105701
Mean absolute error: 0.28371895923383345
Coverage of cases: 71.8840579710145
Instances selection time: 18.0
Test time: 18.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7188405797101449
Weighted FalsePositiveRate: 0.25688368843388193
Kappa statistic: 0.4481456440362131
Training time: 5.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7540608910389368
Weighted AreaUnderROC: 0.7394608009791255
Root mean squared error: 0.5192669408842873
Relative absolute error: 54.969269292055586
Root relative squared error: 103.85338817685745
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.48327882531751054
Weighted FMeasure: 0.7263263409673081
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6818222008328505
Mean absolute error: 0.27484634646027795
Coverage of cases: 72.7536231884058
Instances selection time: 19.0
Test time: 19.0
Accumulative iteration time: 139.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.2486146299258068
Kappa statistic: 0.4648868061514091
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7739529713921893
Weighted AreaUnderROC: 0.7669987080981845
Root mean squared error: 0.4881821330772387
Relative absolute error: 48.607734047667996
Root relative squared error: 97.63642661544773
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5328032348667032
Weighted FMeasure: 0.7596871312010138
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7078683079210354
Mean absolute error: 0.24303867023834
Coverage of cases: 75.94202898550725
Instances selection time: 18.0
Test time: 21.0
Accumulative iteration time: 157.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.2254228736587034
Kappa statistic: 0.523139436127163
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.7954441602728048
Weighted AreaUnderROC: 0.7925477663697559
Root mean squared error: 0.45801851353013046
Relative absolute error: 42.818244557374975
Root relative squared error: 91.6037027060261
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5817820816560386
Weighted FMeasure: 0.7890044015535699
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7339867092394614
Mean absolute error: 0.21409122278687487
Coverage of cases: 78.84057971014492
Instances selection time: 16.0
Test time: 23.0
Accumulative iteration time: 174.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2033102643619375
Kappa statistic: 0.5774683331935241
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8099221569957356
Weighted AreaUnderROC: 0.8082545726524786
Root mean squared error: 0.4389434335538713
Relative absolute error: 39.32790576823159
Root relative squared error: 87.78868671077426
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6130796222488447
Weighted FMeasure: 0.8063377325939572
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7509913115336992
Mean absolute error: 0.19663952884115796
Coverage of cases: 80.57971014492753
Instances selection time: 30.0
Test time: 31.0
Accumulative iteration time: 208.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.18928795614431856
Kappa statistic: 0.6107471835373761
Training time: 4.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8160977798896545
Weighted AreaUnderROC: 0.814850071394574
Root mean squared error: 0.4291291246004002
Relative absolute error: 37.565645221669435
Root relative squared error: 85.82582492008004
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6272313886388072
Weighted FMeasure: 0.8148569118946923
Iteration time: 13.0
Weighted AreaUnderPRC: 0.75858880728322
Mean absolute error: 0.18782822610834718
Coverage of cases: 81.44927536231884
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 221.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.1847926108340404
Kappa statistic: 0.6265475948853257
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8240146309478655
Weighted AreaUnderROC: 0.8227034745359353
Root mean squared error: 0.41905632469537796
Relative absolute error: 35.80656407191626
Root relative squared error: 83.81126493907558
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6436587046496708
Weighted FMeasure: 0.8234281482998979
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7676296414760679
Mean absolute error: 0.17903282035958132
Coverage of cases: 82.31884057971014
Instances selection time: 9.0
Test time: 27.0
Accumulative iteration time: 231.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.1777814567252309
Kappa statistic: 0.6433836612271872
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8209133976300281
Weighted AreaUnderROC: 0.8194567212891821
Root mean squared error: 0.42256927443147746
Relative absolute error: 36.35397735215982
Root relative squared error: 84.51385488629549
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6374846369675911
Weighted FMeasure: 0.8204909958533148
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7640702145510808
Mean absolute error: 0.1817698867607991
Coverage of cases: 82.02898550724638
Instances selection time: 7.0
Test time: 28.0
Accumulative iteration time: 238.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.18137641249409975
Kappa statistic: 0.6373100922409116
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8113329830612788
Weighted AreaUnderROC: 0.8084585571496566
Root mean squared error: 0.43275457343515616
Relative absolute error: 38.05770830596788
Root relative squared error: 86.55091468703124
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6181793931684901
Weighted FMeasure: 0.811401665571302
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7526438600926857
Mean absolute error: 0.1902885415298394
Coverage of cases: 81.15942028985508
Instances selection time: 3.0
Test time: 30.0
Accumulative iteration time: 242.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.1946770885992375
Kappa statistic: 0.6180833489449394
Training time: 1.0
		
Time end:Tue Oct 31 11.25.26 EET 2017