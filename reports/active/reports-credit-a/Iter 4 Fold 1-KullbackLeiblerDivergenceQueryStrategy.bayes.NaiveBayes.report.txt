Tue Oct 31 11.25.01 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.01 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 64.05797101449275
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8207688640138965
Weighted AreaUnderROC: 0.9048392062138865
Root mean squared error: 0.3723907008204953
Relative absolute error: 37.222312066371586
Root relative squared error: 74.47814016409906
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6347011213347913
Weighted FMeasure: 0.8191424663520102
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8967106262262443
Mean absolute error: 0.18611156033185794
Coverage of cases: 94.4927536231884
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.1936558681443592
Kappa statistic: 0.6325121121533863
Training time: 2.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8204682274247492
Weighted AreaUnderROC: 0.9089053767642323
Root mean squared error: 0.3779232123381234
Relative absolute error: 36.92855154652586
Root relative squared error: 75.58464246762469
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6302756226308095
Weighted FMeasure: 0.8151305148078847
Iteration time: 5.0
Weighted AreaUnderPRC: 0.902265043694802
Mean absolute error: 0.1846427577326293
Coverage of cases: 92.46376811594203
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.2026037226484797
Kappa statistic: 0.6243194192377497
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 61.594202898550726
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8137374974484588
Weighted AreaUnderROC: 0.907963466183575
Root mean squared error: 0.38359830388266786
Relative absolute error: 37.635900585213776
Root relative squared error: 76.71966077653357
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6177647427078065
Weighted FMeasure: 0.8095375314292172
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8998490728246447
Mean absolute error: 0.1881795029260689
Coverage of cases: 92.17391304347827
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.20722328786587102
Kappa statistic: 0.6129149189581069
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 61.01449275362319
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8159540064678403
Weighted AreaUnderROC: 0.9120029956427015
Root mean squared error: 0.39258276813999443
Relative absolute error: 38.14339745174733
Root relative squared error: 78.51655362799889
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6154769319039032
Weighted FMeasure: 0.8048824548983228
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9097403799693017
Mean absolute error: 0.19071698725873668
Coverage of cases: 91.8840579710145
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 21.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.21749875674907643
Kappa statistic: 0.6040275459098498
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 60.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8162819245399133
Weighted AreaUnderROC: 0.9180623638344227
Root mean squared error: 0.39562720014680347
Relative absolute error: 38.249111636185745
Root relative squared error: 79.12544002936069
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6120358280993908
Weighted FMeasure: 0.8009749966119291
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9150569546347679
Mean absolute error: 0.19124555818092873
Coverage of cases: 91.8840579710145
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22379138249502697
Kappa statistic: 0.5966602104381512
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8300019082627779
Weighted AreaUnderROC: 0.9209899237472767
Root mean squared error: 0.393714701353792
Relative absolute error: 38.17159378680826
Root relative squared error: 78.7429402707584
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6341528989442213
Weighted FMeasure: 0.8088812188016108
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9168012545167484
Mean absolute error: 0.1908579689340413
Coverage of cases: 91.30434782608695
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.218189649048025
Kappa statistic: 0.6134047694085514
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8195455575400528
Weighted AreaUnderROC: 0.9207175925925927
Root mean squared error: 0.39672897760431725
Relative absolute error: 38.25407573671321
Root relative squared error: 79.34579552086345
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6146823411049339
Weighted FMeasure: 0.8001412922839357
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9162099520235909
Mean absolute error: 0.19127037868356606
Coverage of cases: 91.30434782608695
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22644661125319693
Kappa statistic: 0.59555928822634
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 61.594202898550726
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8276071420034897
Weighted AreaUnderROC: 0.9233728213507625
Root mean squared error: 0.39893383566872165
Relative absolute error: 39.737758545102515
Root relative squared error: 79.78676713374433
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6249812704024563
Weighted FMeasure: 0.8020051333903709
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9181508774498677
Mean absolute error: 0.19868879272551257
Coverage of cases: 91.8840579710145
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 37.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.2267920574026712
Kappa statistic: 0.6002317497103129
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 62.46376811594203
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8297865835014983
Weighted AreaUnderROC: 0.9220111655773422
Root mean squared error: 0.39015636602293463
Relative absolute error: 39.18704407535395
Root relative squared error: 78.03127320458692
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6303940830374314
Weighted FMeasure: 0.8052337467629822
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9162866457296233
Mean absolute error: 0.19593522037676978
Coverage of cases: 93.33333333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 40.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.22315466041489057
Kappa statistic: 0.6065582399073637
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 61.73913043478261
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8405731085607752
Weighted AreaUnderROC: 0.9266748366013072
Root mean squared error: 0.3859269690837059
Relative absolute error: 38.07350805694881
Root relative squared error: 77.18539381674118
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6502086646096457
Weighted FMeasure: 0.8140048222758032
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9217250501833147
Mean absolute error: 0.19036754028474404
Coverage of cases: 93.04347826086956
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.21489769820971868
Kappa statistic: 0.6244601285157485
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 61.44927536231884
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8233321103161498
Weighted AreaUnderROC: 0.9237132352941178
Root mean squared error: 0.3975288122384707
Relative absolute error: 39.62847393351616
Root relative squared error: 79.50576244769414
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6178124586796901
Weighted FMeasure: 0.7992409389710737
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9176577739439834
Mean absolute error: 0.1981423696675808
Coverage of cases: 92.46376811594203
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22910184001136685
Kappa statistic: 0.594452339596821
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8236283105848323
Weighted AreaUnderROC: 0.9215345860566448
Root mean squared error: 0.3902512616246337
Relative absolute error: 39.0071204695056
Root relative squared error: 78.05025232492675
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6216631603967672
Weighted FMeasure: 0.8029087568891611
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9155199783239826
Mean absolute error: 0.195035602347528
Coverage of cases: 93.33333333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22413682864450127
Kappa statistic: 0.6013236684525686
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8145405154109826
Weighted AreaUnderROC: 0.9228281590413944
Root mean squared error: 0.40941920806651166
Relative absolute error: 40.93277957320772
Root relative squared error: 81.88384161330234
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5960312985760083
Weighted FMeasure: 0.786228517466359
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9167717349984751
Mean absolute error: 0.20466389786603859
Coverage of cases: 91.01449275362319
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.24365142796248934
Kappa statistic: 0.5690610650762654
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8146799997177195
Weighted AreaUnderROC: 0.921398420479303
Root mean squared error: 0.415776416507282
Relative absolute error: 41.85045353370203
Root relative squared error: 83.1552833014564
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5925034383961295
Weighted FMeasure: 0.7823869543231707
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9144875010510678
Mean absolute error: 0.20925226766851016
Coverage of cases: 90.43478260869566
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24861643932935495
Kappa statistic: 0.5620901205838799
Training time: 1.0
		
Time end:Tue Oct 31 11.25.02 EET 2017