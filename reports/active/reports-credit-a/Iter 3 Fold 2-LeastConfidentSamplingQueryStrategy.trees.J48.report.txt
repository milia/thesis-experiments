Tue Oct 31 11.26.10 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.10 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 96.66666666666667
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8688853239794365
Weighted AreaUnderROC: 0.8292309784456381
Root mean squared error: 0.37213021601910606
Relative absolute error: 44.27047473330789
Root relative squared error: 74.42604320382121
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.7101112920363145
Weighted FMeasure: 0.8400972721665326
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7969395999390095
Mean absolute error: 0.22135237366653948
Coverage of cases: 96.81159420289855
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.13482734732450188
Kappa statistic: 0.6863273436595969
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.82608695652174
Correctly Classified Instances: 72.17391304347827
Weighted Precision: 0.726428707043858
Weighted AreaUnderROC: 0.7065513021010403
Root mean squared error: 0.45655336938898805
Relative absolute error: 72.34729143991893
Root relative squared error: 91.31067387779761
Weighted TruePositiveRate: 0.7217391304347827
Weighted MatthewsCorrelation: 0.4346985995447285
Weighted FMeasure: 0.7147458017023235
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6545360548635244
Mean absolute error: 0.3617364571995947
Coverage of cases: 100.0
Instances selection time: 15.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7217391304347827
Weighted FalsePositiveRate: 0.30863652623270194
Kappa statistic: 0.42321758211138594
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 100.0
Incorrectly Classified Instances: 27.82608695652174
Correctly Classified Instances: 72.17391304347827
Weighted Precision: 0.726428707043858
Weighted AreaUnderROC: 0.7065513021010403
Root mean squared error: 0.4643787481212114
Relative absolute error: 69.82459225392397
Root relative squared error: 92.87574962424227
Weighted TruePositiveRate: 0.7217391304347827
Weighted MatthewsCorrelation: 0.4346985995447285
Weighted FMeasure: 0.7147458017023235
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6545360548635244
Mean absolute error: 0.34912296126961984
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7217391304347827
Weighted FalsePositiveRate: 0.30863652623270194
Kappa statistic: 0.42321758211138594
Training time: 2.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 100.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8303557079680493
Weighted AreaUnderROC: 0.9052322023526213
Root mean squared error: 0.3728175252889127
Relative absolute error: 48.340437624325176
Root relative squared error: 74.56350505778254
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6246220473224778
Weighted FMeasure: 0.7979648335086956
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8854164559920534
Mean absolute error: 0.2417021881216259
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.23079880132008518
Kappa statistic: 0.5940534939674401
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 100.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8289939825408932
Weighted AreaUnderROC: 0.9047392398177737
Root mean squared error: 0.3619242380164251
Relative absolute error: 49.79240581455718
Root relative squared error: 72.38484760328502
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6376651562577733
Weighted FMeasure: 0.8129880721864186
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8677659991351414
Mean absolute error: 0.2489620290727859
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.21012945624828164
Kappa statistic: 0.6217302772411634
Training time: 3.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 90.14492753623189
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8467454772233076
Weighted AreaUnderROC: 0.9062785344255865
Root mean squared error: 0.3526826355604239
Relative absolute error: 43.35955168234091
Root relative squared error: 70.53652711208478
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6884673433997163
Weighted FMeasure: 0.845681880575121
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8849908074044878
Mean absolute error: 0.21679775841170454
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.16411666336546832
Kappa statistic: 0.6870132315434518
Training time: 3.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 97.97101449275362
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8467454772233076
Weighted AreaUnderROC: 0.9040517036647243
Root mean squared error: 0.3544708060228621
Relative absolute error: 42.43533752427378
Root relative squared error: 70.89416120457241
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6884673433997163
Weighted FMeasure: 0.845681880575121
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8742852077808352
Mean absolute error: 0.21217668762136888
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.16411666336546832
Kappa statistic: 0.6870132315434518
Training time: 3.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.3545713233088465
Relative absolute error: 40.817937682516316
Root relative squared error: 70.9142646617693
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.20408968841258157
Coverage of cases: 97.3913043478261
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 4.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.35467897481301036
Relative absolute error: 39.33087289919243
Root relative squared error: 70.93579496260207
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.19665436449596213
Coverage of cases: 97.3913043478261
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 4.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.35512951755895844
Relative absolute error: 38.40496463787761
Root relative squared error: 71.02590351179168
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.19202482318938807
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 4.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 96.52173913043478
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.35543497639756366
Relative absolute error: 37.46693162597076
Root relative squared error: 71.08699527951273
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.1873346581298538
Coverage of cases: 97.3913043478261
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 8.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.35553283808482483
Relative absolute error: 36.71344109434869
Root relative squared error: 71.10656761696497
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.18356720547174343
Coverage of cases: 94.78260869565217
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 5.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.35566718256529817
Relative absolute error: 36.18320701654054
Root relative squared error: 71.13343651305964
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.1809160350827027
Coverage of cases: 94.78260869565217
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 4.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 73.33333333333333
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8578937553282181
Weighted AreaUnderROC: 0.8551540082953695
Root mean squared error: 0.3558017724605741
Relative absolute error: 35.78980753945707
Root relative squared error: 71.16035449211482
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7124682556527246
Weighted FMeasure: 0.857925113356947
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8271328178335781
Mean absolute error: 0.17894903769728535
Coverage of cases: 94.78260869565217
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.14596312709219608
Kappa statistic: 0.7124559881614533
Training time: 5.0
		
Time end:Tue Oct 31 11.26.10 EET 2017