Tue Oct 31 11.25.05 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.05 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8202477948422141
Weighted AreaUnderROC: 0.8150540558917523
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.635203437805827
Weighted FMeasure: 0.8195668054207387
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7611767355200473
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19018174328895932
Kappa statistic: 0.6340962742481782
Training time: 11.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8202477948422141
Weighted AreaUnderROC: 0.8150540558917523
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.635203437805827
Weighted FMeasure: 0.8195668054207387
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7611767355200473
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19018174328895932
Kappa statistic: 0.6340962742481782
Training time: 12.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8202477948422141
Weighted AreaUnderROC: 0.8150540558917523
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.635203437805827
Weighted FMeasure: 0.8195668054207387
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7611767355200473
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19018174328895932
Kappa statistic: 0.6340962742481782
Training time: 12.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8202477948422141
Weighted AreaUnderROC: 0.8150540558917523
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.635203437805827
Weighted FMeasure: 0.8195668054207387
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7611767355200473
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19018174328895932
Kappa statistic: 0.6340962742481782
Training time: 13.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8174343521308653
Weighted AreaUnderROC: 0.811807302644999
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6292882439695533
Weighted FMeasure: 0.81656525426854
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7577236097900905
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.19377669905782813
Kappa statistic: 0.6279591242874994
Training time: 37.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8146351371597903
Weighted AreaUnderROC: 0.8085605493982458
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6233834206212556
Weighted FMeasure: 0.8135574550493995
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7542909727884725
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.1973716548266969
Kappa statistic: 0.6218141956700466
Training time: 18.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8146351371597903
Weighted AreaUnderROC: 0.8085605493982458
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6233834206212556
Weighted FMeasure: 0.8135574550493995
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7542909727884725
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 171.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.1973716548266969
Kappa statistic: 0.6218141956700466
Training time: 52.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8174343521308653
Weighted AreaUnderROC: 0.811807302644999
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6292882439695533
Weighted FMeasure: 0.81656525426854
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7577236097900905
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 228.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.19377669905782813
Kappa statistic: 0.6279591242874994
Training time: 52.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8596662274923146
Weighted AreaUnderROC: 0.8556979669545115
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7079624083365731
Weighted FMeasure: 0.8496379005545331
Iteration time: 39.0
Weighted AreaUnderPRC: 0.805415783544328
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 267.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.13787942840981768
Kappa statistic: 0.6999498243853488
Training time: 38.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7997233887394071
Weighted AreaUnderROC: 0.7536717209492078
Root mean squared error: 0.47548602137364576
Relative absolute error: 45.21739130434783
Root relative squared error: 95.09720427472915
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5592649219196406
Weighted FMeasure: 0.7632453179802793
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7084228533207312
Mean absolute error: 0.22608695652173913
Coverage of cases: 77.3913043478261
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 297.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2665696015798451
Kappa statistic: 0.5258651067728521
Training time: 29.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8258253437383872
Weighted AreaUnderROC: 0.8228054667845244
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6474183215084576
Weighted FMeasure: 0.8258438956142325
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7688795825052914
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 332.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.1804760229526904
Kappa statistic: 0.647239263803681
Training time: 35.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8398303677968318
Weighted AreaUnderROC: 0.8389372407697017
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6747845306181525
Weighted FMeasure: 0.8380516376017713
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7857945731020252
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 361.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.15980667788088684
Kappa statistic: 0.6736376228926653
Training time: 29.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8596662274923146
Weighted AreaUnderROC: 0.8556979669545115
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7079624083365731
Weighted FMeasure: 0.8496379005545331
Iteration time: 41.0
Weighted AreaUnderPRC: 0.805415783544328
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 402.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.13787942840981768
Kappa statistic: 0.6999498243853488
Training time: 40.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8596662274923146
Weighted AreaUnderROC: 0.8556979669545115
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.7079624083365731
Weighted FMeasure: 0.8496379005545331
Iteration time: 54.0
Weighted AreaUnderPRC: 0.805415783544328
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 456.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.13787942840981768
Kappa statistic: 0.6999498243853488
Training time: 54.0
		
Time end:Tue Oct 31 11.25.06 EET 2017