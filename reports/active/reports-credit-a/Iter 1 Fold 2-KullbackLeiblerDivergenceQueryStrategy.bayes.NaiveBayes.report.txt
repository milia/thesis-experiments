Tue Oct 31 11.24.57 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.24.57 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 61.594202898550726
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8128605839913999
Weighted AreaUnderROC: 0.8988880381322905
Root mean squared error: 0.41753147611013364
Relative absolute error: 43.027505655858455
Root relative squared error: 83.50629522202672
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5883692266882594
Weighted FMeasure: 0.7793068171796453
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8865173763469378
Mean absolute error: 0.21513752827929228
Coverage of cases: 88.40579710144928
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.24985272713476675
Kappa statistic: 0.5571245186136072
Training time: 2.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.8133158984417566
Weighted AreaUnderROC: 0.8998132605690082
Root mean squared error: 0.42773851028299364
Relative absolute error: 43.47284920854659
Root relative squared error: 85.54770205659872
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5851238508296823
Weighted FMeasure: 0.7753865837248803
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8856941165265507
Mean absolute error: 0.21736424604273297
Coverage of cases: 88.1159420289855
Instances selection time: 11.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2547055873029012
Kappa statistic: 0.5501797166819367
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 60.869565217391305
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8050705595748366
Weighted AreaUnderROC: 0.9010374631817837
Root mean squared error: 0.43756757186542644
Relative absolute error: 45.066036536222285
Root relative squared error: 87.51351437308529
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5636628025852272
Weighted FMeasure: 0.7619090834089597
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8882802428420495
Mean absolute error: 0.2253301826811114
Coverage of cases: 87.82608695652173
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2690854103783764
Kappa statistic: 0.5246254946297343
Training time: 4.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.8150484046514294
Weighted AreaUnderROC: 0.9022501362360229
Root mean squared error: 0.4336539529880994
Relative absolute error: 44.31731693628315
Root relative squared error: 86.73079059761987
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5792713272092039
Weighted FMeasure: 0.7673306392288365
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8887123515667015
Mean absolute error: 0.22158658468141576
Coverage of cases: 87.82608695652173
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.2644113076391701
Kappa statistic: 0.5362082994304311
Training time: 5.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8130900025456684
Weighted AreaUnderROC: 0.9034816310482142
Root mean squared error: 0.44006203614046197
Relative absolute error: 45.46259320746665
Root relative squared error: 88.0124072280924
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5739725756842539
Weighted FMeasure: 0.7639146718775442
Iteration time: 9.0
Weighted AreaUnderPRC: 0.890109154473461
Mean absolute error: 0.22731296603733323
Coverage of cases: 85.79710144927536
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 57.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.26800626340803896
Kappa statistic: 0.5297980423739315
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 59.42028985507246
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.8091851897085165
Weighted AreaUnderROC: 0.9038252513098859
Root mean squared error: 0.45154836101410545
Relative absolute error: 47.47802788773955
Root relative squared error: 90.30967220282109
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.563358207164813
Weighted FMeasure: 0.7570376934878874
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8889378454883428
Mean absolute error: 0.23739013943869774
Coverage of cases: 85.5072463768116
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2751961749457766
Kappa statistic: 0.5169523066834447
Training time: 4.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7940304282248017
Weighted AreaUnderROC: 0.9018306376831303
Root mean squared error: 0.4600630089337406
Relative absolute error: 48.858846835112885
Root relative squared error: 92.01260178674812
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5285129010887364
Weighted FMeasure: 0.7369053415011965
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8861851179140261
Mean absolute error: 0.24429423417556442
Coverage of cases: 84.92753623188406
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 78.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.2955080051597238
Kappa statistic: 0.47889826743669484
Training time: 4.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 58.98550724637681
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7975346457186195
Weighted AreaUnderROC: 0.8986388222900857
Root mean squared error: 0.46468420508933067
Relative absolute error: 49.23718839789235
Root relative squared error: 92.93684101786613
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5313343592174239
Weighted FMeasure: 0.7360263199777427
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8832873382898054
Mean absolute error: 0.24618594198946175
Coverage of cases: 84.6376811594203
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.2967659095589894
Kappa statistic: 0.47821213145673563
Training time: 5.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.8080082488400069
Weighted AreaUnderROC: 0.8982572628828036
Root mean squared error: 0.44778006686003285
Relative absolute error: 47.07369860944489
Root relative squared error: 89.55601337200657
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5660860921437441
Weighted FMeasure: 0.7612077613138057
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8856438864682636
Mean absolute error: 0.23536849304722446
Coverage of cases: 85.79710144927536
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 95.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2703433147776421
Kappa statistic: 0.5240032546786004
Training time: 5.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 59.85507246376812
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.795439002386357
Weighted AreaUnderROC: 0.8931949983395465
Root mean squared error: 0.45203495007399236
Relative absolute error: 47.51106059713623
Root relative squared error: 90.40699001479847
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5483804206110338
Weighted FMeasure: 0.7565006587615284
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8813591763493552
Mean absolute error: 0.23755530298568114
Coverage of cases: 86.08695652173913
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 105.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.27375951311758273
Kappa statistic: 0.5130729332063088
Training time: 4.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7929271338882322
Weighted AreaUnderROC: 0.8895275147494587
Root mean squared error: 0.44434242727105844
Relative absolute error: 46.48285525835052
Root relative squared error: 88.8684854542117
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5463354574646199
Weighted FMeasure: 0.7571746851079788
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8787302572710312
Mean absolute error: 0.2324142762917526
Coverage of cases: 87.82608695652173
Instances selection time: 6.0
Test time: 8.0
Accumulative iteration time: 114.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.27250160871831713
Kappa statistic: 0.5137078018183099
Training time: 3.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7975800427022935
Weighted AreaUnderROC: 0.8915145405470922
Root mean squared error: 0.4473746070892705
Relative absolute error: 46.75311762551036
Root relative squared error: 89.4749214178541
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5538255131822595
Weighted FMeasure: 0.7598799480365817
Iteration time: 5.0
Weighted AreaUnderPRC: 0.881021320970731
Mean absolute error: 0.2337655881275518
Coverage of cases: 86.66666666666667
Instances selection time: 3.0
Test time: 12.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.270164557348714
Kappa statistic: 0.5194731924046616
Training time: 2.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.7994798244031511
Weighted AreaUnderROC: 0.8928932589528994
Root mean squared error: 0.43412753814066046
Relative absolute error: 44.74578384735043
Root relative squared error: 86.82550762813209
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5664034134178505
Weighted FMeasure: 0.7711124842402228
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8825086831380298
Mean absolute error: 0.22372891923675217
Coverage of cases: 87.53623188405797
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 124.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.25686388124357623
Kappa statistic: 0.5398223875881919
Training time: 2.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7972484350779756
Weighted AreaUnderROC: 0.8888927977705579
Root mean squared error: 0.437986165777213
Relative absolute error: 45.46490635988168
Root relative squared error: 87.5972331554426
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5609152518615144
Weighted FMeasure: 0.767810331047307
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8769426659193694
Mean absolute error: 0.2273245317994084
Coverage of cases: 87.82608695652173
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 128.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.26045883701244504
Kappa statistic: 0.5334644632163117
Training time: 2.0
		
Time end:Tue Oct 31 11.24.58 EET 2017