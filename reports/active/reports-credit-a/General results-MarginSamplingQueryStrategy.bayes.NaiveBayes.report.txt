Tue Oct 31 11.26.20 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.20 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.18840579710145
Incorrectly Classified Instances: 21.536231884057973
Correctly Classified Instances: 78.46376811594203
Weighted Precision: 0.7944138311999598
Weighted AreaUnderROC: 0.8755039286851156
Root mean squared error: 0.42914430574478607
Relative absolute error: 43.96172222723944
Root relative squared error: 85.82886114895722
Weighted TruePositiveRate: 0.7846376811594202
Weighted MatthewsCorrelation: 0.5691585840439969
Weighted FMeasure: 0.7798483715567105
Iteration time: 3.8
Weighted AreaUnderPRC: 0.8616349270670851
Mean absolute error: 0.2198086111361972
Coverage of cases: 87.65217391304347
Instances selection time: 3.6
Test time: 6.3
Accumulative iteration time: 3.8
Weighted Recall: 0.7846376811594202
Weighted FalsePositiveRate: 0.24103289720633403
Kappa statistic: 0.5550476015237523
Training time: 0.2
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 60.20289855072464
Incorrectly Classified Instances: 21.94202898550725
Correctly Classified Instances: 78.05797101449275
Weighted Precision: 0.7956267617405043
Weighted AreaUnderROC: 0.8819152464400627
Root mean squared error: 0.4316093942713331
Relative absolute error: 44.45092613176819
Root relative squared error: 86.3218788542666
Weighted TruePositiveRate: 0.7805797101449274
Weighted MatthewsCorrelation: 0.5641141235688363
Weighted FMeasure: 0.7733423258423752
Iteration time: 3.1
Weighted AreaUnderPRC: 0.8697390088256617
Mean absolute error: 0.22225463065884096
Coverage of cases: 87.76811594202897
Instances selection time: 3.1
Test time: 4.1
Accumulative iteration time: 6.9
Weighted Recall: 0.7805797101449274
Weighted FalsePositiveRate: 0.2524032473894303
Kappa statistic: 0.5430510915194872
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 60.84057971014492
Incorrectly Classified Instances: 21.65217391304348
Correctly Classified Instances: 78.34782608695653
Weighted Precision: 0.8015218902282358
Weighted AreaUnderROC: 0.8884010178189421
Root mean squared error: 0.4250593552120468
Relative absolute error: 43.6660317702467
Root relative squared error: 85.01187104240935
Weighted TruePositiveRate: 0.7834782608695652
Weighted MatthewsCorrelation: 0.572408232106173
Weighted FMeasure: 0.7755344684369918
Iteration time: 2.4
Weighted AreaUnderPRC: 0.8762621428207227
Mean absolute error: 0.2183301588512335
Coverage of cases: 88.78260869565217
Instances selection time: 2.3
Test time: 4.1
Accumulative iteration time: 9.3
Weighted Recall: 0.7834782608695652
Weighted FalsePositiveRate: 0.25200457648068986
Kappa statistic: 0.5479381639682854
Training time: 0.1
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 61.782608695652165
Incorrectly Classified Instances: 21.565217391304348
Correctly Classified Instances: 78.43478260869564
Weighted Precision: 0.8021624820649601
Weighted AreaUnderROC: 0.8932240018790374
Root mean squared error: 0.42097077454461773
Relative absolute error: 43.75729268795578
Root relative squared error: 84.19415490892354
Weighted TruePositiveRate: 0.7843478260869565
Weighted MatthewsCorrelation: 0.573937000877932
Weighted FMeasure: 0.7763966552135737
Iteration time: 2.6
Weighted AreaUnderPRC: 0.880921315415787
Mean absolute error: 0.21878646343977892
Coverage of cases: 89.85507246376811
Instances selection time: 2.3
Test time: 4.3
Accumulative iteration time: 11.9
Weighted Recall: 0.7843478260869565
Weighted FalsePositiveRate: 0.25125193234710297
Kappa statistic: 0.5496425421570965
Training time: 0.3
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 62.05797101449275
Incorrectly Classified Instances: 21.507246376811594
Correctly Classified Instances: 78.4927536231884
Weighted Precision: 0.803814763759666
Weighted AreaUnderROC: 0.8971304233957931
Root mean squared error: 0.418760854215429
Relative absolute error: 43.56698058876547
Root relative squared error: 83.75217084308579
Weighted TruePositiveRate: 0.784927536231884
Weighted MatthewsCorrelation: 0.5759895708866328
Weighted FMeasure: 0.7766928288719709
Iteration time: 2.5
Weighted AreaUnderPRC: 0.8854004199566363
Mean absolute error: 0.21783490294382735
Coverage of cases: 90.02898550724638
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.4
Weighted Recall: 0.784927536231884
Weighted FalsePositiveRate: 0.2515459280952136
Kappa statistic: 0.5504569381523842
Training time: 0.5
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 62.449275362318836
Incorrectly Classified Instances: 22.115942028985508
Correctly Classified Instances: 77.8840579710145
Weighted Precision: 0.7992500461691797
Weighted AreaUnderROC: 0.8977421171727664
Root mean squared error: 0.42144814731923474
Relative absolute error: 44.46730142769903
Root relative squared error: 84.28962946384698
Weighted TruePositiveRate: 0.778840579710145
Weighted MatthewsCorrelation: 0.5644633323586451
Weighted FMeasure: 0.7695865809070925
Iteration time: 3.1
Weighted AreaUnderPRC: 0.8871978757424628
Mean absolute error: 0.22233650713849512
Coverage of cases: 90.26086956521739
Instances selection time: 2.8
Test time: 4.2
Accumulative iteration time: 17.5
Weighted Recall: 0.778840579710145
Weighted FalsePositiveRate: 0.25927781648015047
Kappa statistic: 0.5369275140552447
Training time: 0.3
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 62.463768115942024
Incorrectly Classified Instances: 22.57971014492754
Correctly Classified Instances: 77.42028985507247
Weighted Precision: 0.7965899607826843
Weighted AreaUnderROC: 0.8976400724563451
Root mean squared error: 0.4269325338293414
Relative absolute error: 45.0049351651778
Root relative squared error: 85.38650676586829
Weighted TruePositiveRate: 0.7742028985507248
Weighted MatthewsCorrelation: 0.5564800574863141
Weighted FMeasure: 0.7641849595860425
Iteration time: 2.9
Weighted AreaUnderPRC: 0.886664716410068
Mean absolute error: 0.22502467582588906
Coverage of cases: 90.28985507246378
Instances selection time: 2.7
Test time: 4.4
Accumulative iteration time: 20.4
Weighted Recall: 0.7742028985507248
Weighted FalsePositiveRate: 0.26541984939579377
Kappa statistic: 0.5266308569757442
Training time: 0.2
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 61.3768115942029
Incorrectly Classified Instances: 22.956521739130434
Correctly Classified Instances: 77.04347826086956
Weighted Precision: 0.7944405497267514
Weighted AreaUnderROC: 0.8977558293995583
Root mean squared error: 0.43342156453417563
Relative absolute error: 45.49167349460406
Root relative squared error: 86.68431290683513
Weighted TruePositiveRate: 0.7704347826086957
Weighted MatthewsCorrelation: 0.5498573258908912
Weighted FMeasure: 0.7595623429699181
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8875072398116197
Mean absolute error: 0.22745836747302023
Coverage of cases: 88.98550724637681
Instances selection time: 1.3
Test time: 4.1
Accumulative iteration time: 22.4
Weighted Recall: 0.7704347826086957
Weighted FalsePositiveRate: 0.2703885236243785
Kappa statistic: 0.5181016374670493
Training time: 0.7
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 60.826086956521735
Incorrectly Classified Instances: 22.92753623188406
Correctly Classified Instances: 77.07246376811594
Weighted Precision: 0.7926018126118617
Weighted AreaUnderROC: 0.8965575028717303
Root mean squared error: 0.4340055179656018
Relative absolute error: 45.295570924634795
Root relative squared error: 86.80110359312036
Weighted TruePositiveRate: 0.7707246376811596
Weighted MatthewsCorrelation: 0.5487625681966035
Weighted FMeasure: 0.7604988719420573
Iteration time: 1.8
Weighted AreaUnderPRC: 0.885446069423528
Mean absolute error: 0.22647785462317396
Coverage of cases: 88.86956521739131
Instances selection time: 1.3
Test time: 4.0
Accumulative iteration time: 24.2
Weighted Recall: 0.7707246376811596
Weighted FalsePositiveRate: 0.2689893667839699
Kappa statistic: 0.519291894054318
Training time: 0.5
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 60.07246376811595
Incorrectly Classified Instances: 22.84057971014493
Correctly Classified Instances: 77.15942028985506
Weighted Precision: 0.7905594850942281
Weighted AreaUnderROC: 0.8940343565128985
Root mean squared error: 0.4351257770138438
Relative absolute error: 45.008964683439466
Root relative squared error: 87.02515540276877
Weighted TruePositiveRate: 0.7715942028985507
Weighted MatthewsCorrelation: 0.5482684769882199
Weighted FMeasure: 0.7623201907095376
Iteration time: 1.8
Weighted AreaUnderPRC: 0.8833382461500576
Mean absolute error: 0.22504482341719734
Coverage of cases: 88.31884057971016
Instances selection time: 1.4
Test time: 3.8
Accumulative iteration time: 26.0
Weighted Recall: 0.7715942028985507
Weighted FalsePositiveRate: 0.26622135359620885
Kappa statistic: 0.5220589125263204
Training time: 0.4
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 59.69565217391305
Incorrectly Classified Instances: 22.3768115942029
Correctly Classified Instances: 77.62318840579711
Weighted Precision: 0.791493557364683
Weighted AreaUnderROC: 0.8923207761974987
Root mean squared error: 0.43413122954816047
Relative absolute error: 44.514040131507684
Root relative squared error: 86.82624590963209
Weighted TruePositiveRate: 0.776231884057971
Weighted MatthewsCorrelation: 0.5549843747129914
Weighted FMeasure: 0.7684025721538866
Iteration time: 1.7
Weighted AreaUnderPRC: 0.8812875427164018
Mean absolute error: 0.22257020065753844
Coverage of cases: 88.08695652173914
Instances selection time: 1.3
Test time: 4.0
Accumulative iteration time: 27.7
Weighted Recall: 0.776231884057971
Weighted FalsePositiveRate: 0.2586395502420077
Kappa statistic: 0.5331258212541925
Training time: 0.4
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 59.43478260869565
Incorrectly Classified Instances: 21.97101449275362
Correctly Classified Instances: 78.02898550724638
Weighted Precision: 0.7928428391788892
Weighted AreaUnderROC: 0.8908267628483907
Root mean squared error: 0.4314057179396881
Relative absolute error: 43.95798722116969
Root relative squared error: 86.28114358793763
Weighted TruePositiveRate: 0.7802898550724638
Weighted MatthewsCorrelation: 0.5612724121335886
Weighted FMeasure: 0.7735425165471155
Iteration time: 1.7
Weighted AreaUnderPRC: 0.8790177255045792
Mean absolute error: 0.21978993610584846
Coverage of cases: 87.8840579710145
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.4
Weighted Recall: 0.7802898550724638
Weighted FalsePositiveRate: 0.2522698943117038
Kappa statistic: 0.5426571665422133
Training time: 0.7
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 59.289855072463766
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7963315592083526
Weighted AreaUnderROC: 0.8906936713610893
Root mean squared error: 0.42752427350930733
Relative absolute error: 43.18751264240717
Root relative squared error: 85.50485470186145
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5707219530283342
Weighted FMeasure: 0.7795785065142464
Iteration time: 1.5
Weighted AreaUnderPRC: 0.8794956978953593
Mean absolute error: 0.2159375632120358
Coverage of cases: 87.97101449275361
Instances selection time: 0.8
Test time: 3.9
Accumulative iteration time: 30.9
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2452115449612303
Kappa statistic: 0.5543072332815621
Training time: 0.7
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 60.05797101449275
Incorrectly Classified Instances: 21.391304347826086
Correctly Classified Instances: 78.6086956521739
Weighted Precision: 0.8008923064351064
Weighted AreaUnderROC: 0.8953359246559179
Root mean squared error: 0.42471340405248836
Relative absolute error: 43.32151103512634
Root relative squared error: 84.94268081049766
Weighted TruePositiveRate: 0.7860869565217391
Weighted MatthewsCorrelation: 0.5751059351360799
Weighted FMeasure: 0.7791155050506062
Iteration time: 1.4
Weighted AreaUnderPRC: 0.8863094004680809
Mean absolute error: 0.21660755517563174
Coverage of cases: 88.52173913043478
Instances selection time: 0.7
Test time: 4.1
Accumulative iteration time: 32.3
Weighted Recall: 0.7860869565217391
Weighted FalsePositiveRate: 0.2477579542481342
Kappa statistic: 0.5541381731381618
Training time: 0.7
		
Time end:Tue Oct 31 11.26.26 EET 2017