Tue Oct 31 11.27.43 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.27.43 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 63.333333333333336
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7846052712040603
Weighted AreaUnderROC: 0.8900803376906319
Root mean squared error: 0.42656818229995636
Relative absolute error: 45.77270700284991
Root relative squared error: 85.31363645999127
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5423227125332433
Weighted FMeasure: 0.7632840922196245
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8799847907539383
Mean absolute error: 0.22886353501424958
Coverage of cases: 91.01449275362319
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.26478491759022454
Kappa statistic: 0.521816937733565
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 64.4927536231884
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.7949951690821255
Weighted AreaUnderROC: 0.8967184095860566
Root mean squared error: 0.4090153082326638
Relative absolute error: 42.82956648708619
Root relative squared error: 81.80306164653275
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.573093283333174
Weighted FMeasure: 0.7839895689754931
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8923289563486637
Mean absolute error: 0.21414783243543095
Coverage of cases: 92.7536231884058
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.23897769252628584
Kappa statistic: 0.5617332289219525
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 65.21739130434783
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8025469796413593
Weighted AreaUnderROC: 0.8981141067538126
Root mean squared error: 0.3913112978234085
Relative absolute error: 40.56894283458897
Root relative squared error: 78.2622595646817
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.5902105792578582
Weighted FMeasure: 0.7934257508926695
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8925828465023431
Mean absolute error: 0.20284471417294483
Coverage of cases: 93.04347826086956
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.22806550156294403
Kappa statistic: 0.5805981035740335
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 65.94202898550725
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8104288613247639
Weighted AreaUnderROC: 0.9007352941176471
Root mean squared error: 0.379833875339796
Relative absolute error: 39.183539616874306
Root relative squared error: 75.9667750679592
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6115715333516538
Weighted FMeasure: 0.8067419450975197
Iteration time: 2.0
Weighted AreaUnderPRC: 0.895338934095427
Mean absolute error: 0.19591769808437154
Coverage of cases: 94.20289855072464
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.20953307047456665
Kappa statistic: 0.6072241771889878
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 65.65217391304348
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8211996191685178
Weighted AreaUnderROC: 0.8985226034858388
Root mean squared error: 0.3752645141670168
Relative absolute error: 38.52015844543858
Root relative squared error: 75.05290283340335
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6348881677817824
Weighted FMeasure: 0.8189265367316342
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8906332070880271
Mean absolute error: 0.19260079222719292
Coverage of cases: 93.91304347826087
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19498348252344416
Kappa statistic: 0.6320189925681254
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 66.23188405797102
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8321779035388439
Weighted AreaUnderROC: 0.8994076797385621
Root mean squared error: 0.37407735664054287
Relative absolute error: 38.75681358266522
Root relative squared error: 74.81547132810857
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6583440079023681
Weighted FMeasure: 0.8310003316214496
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8928032273595352
Mean absolute error: 0.19378406791332609
Coverage of cases: 93.91304347826087
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.18043389457232167
Kappa statistic: 0.6566810788552605
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 65.94202898550725
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8288692407527579
Weighted AreaUnderROC: 0.8982502723311547
Root mean squared error: 0.3723870628047875
Relative absolute error: 38.56859616907061
Root relative squared error: 74.4774125609575
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6523841534458908
Weighted FMeasure: 0.8283525597830278
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8919967206191586
Mean absolute error: 0.19284298084535306
Coverage of cases: 93.6231884057971
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.18141606280193237
Kappa statistic: 0.6514614475779523
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 64.34782608695652
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8066849043151617
Weighted AreaUnderROC: 0.8923951525054467
Root mean squared error: 0.39747149268912263
Relative absolute error: 41.165778691733315
Root relative squared error: 79.49429853782452
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6009786471531676
Weighted FMeasure: 0.7999969701560371
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8860029306211806
Mean absolute error: 0.2058288934586666
Coverage of cases: 91.8840579710145
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.21946309320829782
Kappa statistic: 0.5936818040112232
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 66.81159420289855
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8073333333333332
Weighted AreaUnderROC: 0.910437091503268
Root mean squared error: 0.3802234976451202
Relative absolute error: 40.19026335265461
Root relative squared error: 76.04469952902404
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5975935538102428
Weighted FMeasure: 0.7958257569768361
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9068036345256489
Mean absolute error: 0.20095131676327305
Coverage of cases: 95.07246376811594
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.22708333333333333
Kappa statistic: 0.5857478465152702
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 66.3768115942029
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8086593486381397
Weighted AreaUnderROC: 0.9110838779956428
Root mean squared error: 0.38706725848120094
Relative absolute error: 40.47367099690808
Root relative squared error: 77.41345169624019
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6023918815935821
Weighted FMeasure: 0.7993278722957361
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9075743880162301
Mean absolute error: 0.20236835498454042
Coverage of cases: 95.3623188405797
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.22211832196646772
Kappa statistic: 0.5925810149004898
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 65.21739130434783
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.806153054473278
Weighted AreaUnderROC: 0.910777505446623
Root mean squared error: 0.39383710038936104
Relative absolute error: 40.67898030680021
Root relative squared error: 78.7674200778722
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.5967063528645827
Weighted FMeasure: 0.7961971522453449
Iteration time: 2.0
Weighted AreaUnderPRC: 0.90604857270322
Mean absolute error: 0.20339490153400108
Coverage of cases: 94.20289855072464
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.22575571895424837
Kappa statistic: 0.5863093686460561
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 63.768115942028984
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7912686669765554
Weighted AreaUnderROC: 0.9085988562091505
Root mean squared error: 0.4084230790546113
Relative absolute error: 42.30388124014843
Root relative squared error: 81.68461581092225
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5626347961769974
Weighted FMeasure: 0.7772108171029055
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9035924254249682
Mean absolute error: 0.21151940620074214
Coverage of cases: 92.17391304347827
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.24758010088093205
Kappa statistic: 0.5485002355650946
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 63.768115942028984
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7965503973676877
Weighted AreaUnderROC: 0.903322440087146
Root mean squared error: 0.4044327896332795
Relative absolute error: 42.10384218110122
Root relative squared error: 80.8865579266559
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5704647384016804
Weighted FMeasure: 0.779497554386376
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8957834124612346
Mean absolute error: 0.2105192109055061
Coverage of cases: 92.7536231884058
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2465979326513214
Kappa statistic: 0.5536088963491397
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 62.608695652173914
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7839036234095341
Weighted AreaUnderROC: 0.9010076252723311
Root mean squared error: 0.4167772682365501
Relative absolute error: 43.537695389444075
Root relative squared error: 83.35545364731001
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.545600821435149
Weighted FMeasure: 0.7675785032721261
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8937047755904481
Mean absolute error: 0.21768847694722038
Coverage of cases: 91.30434782608695
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.25849229184427397
Kappa statistic: 0.5294796475031472
Training time: 1.0
		
Time end:Tue Oct 31 11.27.43 EET 2017