Tue Oct 31 11.25.35 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.35 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 94.78260869565217
Incorrectly Classified Instances: 27.246376811594203
Correctly Classified Instances: 72.7536231884058
Weighted Precision: 0.7337044851918995
Weighted AreaUnderROC: 0.8169020371843043
Root mean squared error: 0.42121931861342926
Relative absolute error: 71.29887573776831
Root relative squared error: 84.24386372268586
Weighted TruePositiveRate: 0.7275362318840579
Weighted MatthewsCorrelation: 0.4476594184368774
Weighted FMeasure: 0.7201160896527049
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8115433660676667
Mean absolute error: 0.3564943786888416
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7275362318840579
Weighted FalsePositiveRate: 0.30396242349349567
Kappa statistic: 0.434505126595522
Training time: 12.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 91.44927536231884
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8232965771493538
Weighted AreaUnderROC: 0.9082792084613163
Root mean squared error: 0.35228892292403247
Relative absolute error: 52.779689884120465
Root relative squared error: 70.45778458480649
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6411240638555858
Weighted FMeasure: 0.8223885795298561
Iteration time: 6.0
Weighted AreaUnderPRC: 0.900003822118546
Mean absolute error: 0.2638984494206023
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.18784469191935615
Kappa statistic: 0.6397699457386897
Training time: 5.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 91.59420289855072
Incorrectly Classified Instances: 26.666666666666668
Correctly Classified Instances: 73.33333333333333
Weighted Precision: 0.7521496047811838
Weighted AreaUnderROC: 0.8440665639846155
Root mean squared error: 0.4192072257196489
Relative absolute error: 66.66312906022317
Root relative squared error: 83.84144514392979
Weighted TruePositiveRate: 0.7333333333333333
Weighted MatthewsCorrelation: 0.4687586707344948
Weighted FMeasure: 0.7207508878741755
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8456458057482144
Mean absolute error: 0.33331564530111585
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7333333333333333
Weighted FalsePositiveRate: 0.3080936515491489
Kappa statistic: 0.44076397209105633
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 84.92753623188406
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8329261436478644
Weighted AreaUnderROC: 0.8995285691620771
Root mean squared error: 0.38550644458607836
Relative absolute error: 55.40524749336729
Root relative squared error: 77.10128891721567
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6268113799375712
Weighted FMeasure: 0.7974459828909073
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8851595555984455
Mean absolute error: 0.27702623746683647
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.23205670571935083
Kappa statistic: 0.593525243111119
Training time: 13.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 89.56521739130434
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8283878338798247
Weighted AreaUnderROC: 0.9121090913032639
Root mean squared error: 0.3693017882810637
Relative absolute error: 55.16374313520308
Root relative squared error: 73.86035765621274
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6336003037736492
Weighted FMeasure: 0.8094407418912034
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9041928024208196
Mean absolute error: 0.2758187156760154
Coverage of cases: 99.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.2149823164164161
Kappa statistic: 0.6149822138522704
Training time: 8.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 87.2463768115942
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7927846388090002
Weighted AreaUnderROC: 0.8716427551506084
Root mean squared error: 0.4140564540715409
Relative absolute error: 59.05090695228701
Root relative squared error: 82.81129081430818
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5312888675510898
Weighted FMeasure: 0.7412549407114625
Iteration time: 8.0
Weighted AreaUnderPRC: 0.867819800563733
Mean absolute error: 0.29525453476143504
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.29065514499158934
Kappa statistic: 0.4860426346965558
Training time: 7.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 83.04347826086956
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.8216911972029549
Weighted AreaUnderROC: 0.9191389686267901
Root mean squared error: 0.38415704031184766
Relative absolute error: 53.27238277168115
Root relative squared error: 76.83140806236953
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5847938399819812
Weighted FMeasure: 0.7658973273103707
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9164174803714015
Mean absolute error: 0.26636191385840574
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.26692711643770145
Kappa statistic: 0.5349909552016457
Training time: 7.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 86.23188405797102
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.8090274459839678
Weighted AreaUnderROC: 0.8789503765829739
Root mean squared error: 0.4252924504031856
Relative absolute error: 61.28914607924795
Root relative squared error: 85.05849008063711
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5288835454366689
Weighted FMeasure: 0.7211920588378375
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8714352626988708
Mean absolute error: 0.30644573039623973
Coverage of cases: 97.10144927536231
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.31258239446265856
Kappa statistic: 0.4558554683299279
Training time: 11.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 84.92753623188406
Incorrectly Classified Instances: 28.115942028985508
Correctly Classified Instances: 71.8840579710145
Weighted Precision: 0.8066027315843086
Weighted AreaUnderROC: 0.8712603088542081
Root mean squared error: 0.44788773305496865
Relative absolute error: 63.85017216187695
Root relative squared error: 89.57754661099374
Weighted TruePositiveRate: 0.7188405797101449
Weighted MatthewsCorrelation: 0.49033808323960587
Weighted FMeasure: 0.6841387644678266
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8636060918960835
Mean absolute error: 0.31925086080938475
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 87.0
Weighted Recall: 0.7188405797101449
Weighted FalsePositiveRate: 0.34745280518100913
Kappa statistic: 0.3949885198777864
Training time: 10.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 87.53623188405797
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.800799844278105
Weighted AreaUnderROC: 0.8859433001932433
Root mean squared error: 0.41331087204386036
Relative absolute error: 60.56940822200995
Root relative squared error: 82.66217440877207
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.5145354426470448
Weighted FMeasure: 0.7148555147205159
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8805701635966249
Mean absolute error: 0.30284704111004973
Coverage of cases: 97.97101449275362
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 100.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.3185144016011304
Kappa statistic: 0.4434885471556081
Training time: 12.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 85.65217391304348
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8347000571256864
Weighted AreaUnderROC: 0.9078778911353462
Root mean squared error: 0.3672056634207118
Relative absolute error: 51.927409126014545
Root relative squared error: 73.44113268414236
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6459963627961006
Weighted FMeasure: 0.8153957187071034
Iteration time: 13.0
Weighted AreaUnderPRC: 0.902114865696784
Mean absolute error: 0.2596370456300727
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.20905030927794416
Kappa statistic: 0.627014019669387
Training time: 12.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 84.78260869565217
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8406169811142973
Weighted AreaUnderROC: 0.906946312659948
Root mean squared error: 0.3560851777084247
Relative absolute error: 48.703902501401416
Root relative squared error: 71.21703554168494
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6686775463538066
Weighted FMeasure: 0.8322985041426331
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9022438161321664
Mean absolute error: 0.24351951250700707
Coverage of cases: 98.26086956521739
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 127.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18604391283653746
Kappa statistic: 0.6599457019834338
Training time: 14.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 87.97101449275362
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8390254148288174
Weighted AreaUnderROC: 0.9044208466243522
Root mean squared error: 0.3528366424166921
Relative absolute error: 51.262700473498555
Root relative squared error: 70.56732848333841
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6712687645546125
Weighted FMeasure: 0.8364967581604669
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9003312538410932
Mean absolute error: 0.2563135023674928
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 143.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.17615943507134038
Kappa statistic: 0.6682464454976303
Training time: 16.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 87.68115942028986
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8369971627942643
Weighted AreaUnderROC: 0.8908115331060927
Root mean squared error: 0.3630174295898965
Relative absolute error: 52.12477748401278
Root relative squared error: 72.6034859179793
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6659255967825302
Weighted FMeasure: 0.8332657149063553
Iteration time: 16.0
Weighted AreaUnderPRC: 0.882987885614118
Mean absolute error: 0.2606238874200639
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 159.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.18101229523947482
Kappa statistic: 0.6616774193548386
Training time: 16.0
		
Time end:Tue Oct 31 11.25.36 EET 2017