Tue Oct 31 11.24.28 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.24.28 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8326333909068857
Weighted AreaUnderROC: 0.8303528931801183
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6569199860886925
Weighted FMeasure: 0.826584955736658
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7756726010246847
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.1653811701615025
Kappa statistic: 0.652497985495568
Training time: 24.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8260869565217391
Weighted AreaUnderROC: 0.82406337118379
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.64812674236758
Weighted FMeasure: 0.8260869565217391
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7697281093593409
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.17796021415415905
Kappa statistic: 0.64812674236758
Training time: 18.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.820001153216779
Weighted AreaUnderROC: 0.8163119602910179
Root mean squared error: 0.4239223336031451
Relative absolute error: 35.94202898550725
Root relative squared error: 84.78446672062901
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6354167845820854
Weighted FMeasure: 0.8198941663871416
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7618654448447276
Mean absolute error: 0.17971014492753623
Coverage of cases: 82.02898550724638
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.187665934490428
Kappa statistic: 0.6350203050882163
Training time: 14.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8031884057971015
Weighted AreaUnderROC: 0.7962024886108656
Root mean squared error: 0.44396108982135174
Relative absolute error: 39.42028985507247
Root relative squared error: 88.79221796427035
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.5997286473531601
Weighted FMeasure: 0.8016897756028192
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7410330962882948
Mean absolute error: 0.19710144927536233
Coverage of cases: 80.28985507246377
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 78.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.2104935735029065
Kappa statistic: 0.597667638483965
Training time: 15.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8283878338798247
Weighted AreaUnderROC: 0.7997552186033862
Root mean squared error: 0.4307055216465324
Relative absolute error: 37.10144927536232
Root relative squared error: 86.14110432930649
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6336003037736492
Weighted FMeasure: 0.8094407418912034
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7531126320699497
Mean absolute error: 0.1855072463768116
Coverage of cases: 81.44927536231884
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.2149823164164161
Kappa statistic: 0.6149822138522704
Training time: 15.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8197863457651705
Weighted AreaUnderROC: 0.7906439110627591
Root mean squared error: 0.4406845794337767
Relative absolute error: 38.84057971014493
Root relative squared error: 88.13691588675535
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6157225431794425
Weighted FMeasure: 0.8002981164078359
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7430898261270801
Mean absolute error: 0.19420289855072465
Coverage of cases: 80.57971014492753
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 121.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22450927932375692
Kappa statistic: 0.5966742859136991
Training time: 24.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8238663929837655
Weighted AreaUnderROC: 0.7932617121098797
Root mean squared error: 0.4373835248681493
Relative absolute error: 38.26086956521739
Root relative squared error: 87.47670497362986
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6226950789526957
Weighted FMeasure: 0.8030676568369242
Iteration time: 35.0
Weighted AreaUnderPRC: 0.7465175705272413
Mean absolute error: 0.19130434782608696
Coverage of cases: 80.8695652173913
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 156.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22217222795415376
Kappa statistic: 0.6024374061528791
Training time: 31.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8213888888888888
Weighted AreaUnderROC: 0.7822635479703542
Root mean squared error: 0.4472135954999579
Relative absolute error: 40.0
Root relative squared error: 89.44271909999159
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6099041177602682
Weighted FMeasure: 0.7924499905938421
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7371855402526479
Mean absolute error: 0.2
Coverage of cases: 80.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 177.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.2354729040592915
Kappa statistic: 0.5824782951854777
Training time: 20.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8104818751909275
Weighted AreaUnderROC: 0.7737811926293601
Root mean squared error: 0.45683219257612856
Relative absolute error: 41.73913043478261
Root relative squared error: 91.36643851522571
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5900091782210957
Weighted FMeasure: 0.7836882632029245
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7270937363154657
Mean absolute error: 0.20869565217391303
Coverage of cases: 79.1304347826087
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24374196256736663
Kappa statistic: 0.5646077262847927
Training time: 15.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8320870006397106
Weighted AreaUnderROC: 0.8152580403889305
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6509773550461329
Weighted FMeasure: 0.8233297985153764
Iteration time: 40.0
Weighted AreaUnderPRC: 0.7662912446515523
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 233.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.19557087574387824
Kappa statistic: 0.6418189368770764
Training time: 37.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8347691154422789
Weighted AreaUnderROC: 0.8300299177262528
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6647546009569711
Weighted FMeasure: 0.8341975382562506
Iteration time: 64.0
Weighted AreaUnderPRC: 0.7780718988331923
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 297.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17472277324314656
Kappa statistic: 0.6638174202923326
Training time: 56.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8492753623188406
Weighted AreaUnderROC: 0.8475215883592847
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6950431767185694
Weighted FMeasure: 0.8492753623188406
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7968952968270063
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 346.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.15423218560027116
Kappa statistic: 0.6950431767185694
Training time: 44.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.8498420051464826
Weighted AreaUnderROC: 0.8487794927585504
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6959990305593058
Weighted FMeasure: 0.849444061038264
Iteration time: 68.0
Weighted AreaUnderPRC: 0.7978455999241224
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 414.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.15171637680173988
Kappa statistic: 0.6958084644601195
Training time: 62.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8728358331452489
Weighted AreaUnderROC: 0.8680560277418915
Root mean squared error: 0.3730019232961255
Relative absolute error: 27.82608695652174
Root relative squared error: 74.6003846592251
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7329789088444882
Weighted FMeasure: 0.8611643330876935
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8204319192520104
Mean absolute error: 0.1391304347826087
Coverage of cases: 86.08695652173913
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 447.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.12475750973360807
Kappa statistic: 0.7233729787518376
Training time: 32.0
		
Time end:Tue Oct 31 11.24.29 EET 2017