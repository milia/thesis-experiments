Tue Oct 31 11.28.15 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.28.15 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 94.78260869565217
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8392654604374831
Weighted AreaUnderROC: 0.8984545206971678
Root mean squared error: 0.36457755200309616
Relative absolute error: 58.20281012362817
Root relative squared error: 72.91551040061923
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6736330697086078
Weighted FMeasure: 0.8380221191748617
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8886506208579277
Mean absolute error: 0.29101405061814084
Coverage of cases: 99.71014492753623
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.16121057118499574
Kappa statistic: 0.6728971962616823
Training time: 3.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 89.27536231884058
Incorrectly Classified Instances: 14.202898550724637
Correctly Classified Instances: 85.79710144927536
Weighted Precision: 0.8579074646610061
Weighted AreaUnderROC: 0.9207005718954248
Root mean squared error: 0.33449995319660314
Relative absolute error: 48.49029753312815
Root relative squared error: 66.89999063932063
Weighted TruePositiveRate: 0.8579710144927536
Weighted MatthewsCorrelation: 0.7115288603210921
Weighted FMeasure: 0.8575798270449666
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9130115835535477
Mean absolute error: 0.24245148766564076
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8579710144927536
Weighted FalsePositiveRate: 0.15035255044046603
Kappa statistic: 0.7109218693890114
Training time: 4.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 87.68115942028986
Incorrectly Classified Instances: 17.681159420289855
Correctly Classified Instances: 82.31884057971014
Weighted Precision: 0.8232293310085401
Weighted AreaUnderROC: 0.8989651416122003
Root mean squared error: 0.3596882915598582
Relative absolute error: 52.51124618131426
Root relative squared error: 71.93765831197165
Weighted TruePositiveRate: 0.8231884057971014
Weighted MatthewsCorrelation: 0.6405220536564713
Weighted FMeasure: 0.8223538520377227
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8872251307436879
Mean absolute error: 0.2625562309065713
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8231884057971014
Weighted FalsePositiveRate: 0.1886908567774936
Kappa statistic: 0.6391646520240727
Training time: 5.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.7945952906664489
Weighted AreaUnderROC: 0.8834668348015534
Root mean squared error: 0.3864778251898349
Relative absolute error: 56.803429625652036
Root relative squared error: 77.29556503796698
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5767838127675158
Weighted FMeasure: 0.7882320860475686
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8744879330506334
Mean absolute error: 0.2840171481282602
Coverage of cases: 99.42028985507247
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.23135745240125033
Kappa statistic: 0.5697807336589421
Training time: 6.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 89.1304347826087
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7611568446808722
Weighted AreaUnderROC: 0.8574857026143791
Root mean squared error: 0.4064431586742554
Relative absolute error: 61.50038225002194
Root relative squared error: 81.28863173485108
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5016638985735677
Weighted FMeasure: 0.7469871726022397
Iteration time: 7.0
Weighted AreaUnderPRC: 0.848270089829925
Mean absolute error: 0.3075019112501097
Coverage of cases: 98.55072463768116
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.27864361324239845
Kappa statistic: 0.48760287256906226
Training time: 6.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 86.66666666666667
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7808695652173913
Weighted AreaUnderROC: 0.871783088235294
Root mean squared error: 0.4186947308432745
Relative absolute error: 62.289593458018246
Root relative squared error: 83.7389461686549
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5051101322233998
Weighted FMeasure: 0.7276754795800582
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8588278485187552
Mean absolute error: 0.3114479672900912
Coverage of cases: 97.97101449275362
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.3068147911338448
Kappa statistic: 0.45882352941176463
Training time: 6.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 85.5072463768116
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7875567266871614
Weighted AreaUnderROC: 0.8814848856209151
Root mean squared error: 0.42228526345868317
Relative absolute error: 60.641664713091146
Root relative squared error: 84.45705269173664
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5103847014210097
Weighted FMeasure: 0.725822918500264
Iteration time: 8.0
Weighted AreaUnderPRC: 0.870550849144837
Mean absolute error: 0.30320832356545574
Coverage of cases: 97.97101449275362
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.30947001989201484
Kappa statistic: 0.4573144573144572
Training time: 7.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 82.7536231884058
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7970900249587717
Weighted AreaUnderROC: 0.9040448576773704
Root mean squared error: 0.4092416897262455
Relative absolute error: 57.5653626173826
Root relative squared error: 81.8483379452491
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5295615415967245
Weighted FMeasure: 0.7356273698549369
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8941527720388771
Mean absolute error: 0.287826813086913
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.29988544330775785
Kappa statistic: 0.47618026901023525
Training time: 8.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 85.07246376811594
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7990403032641684
Weighted AreaUnderROC: 0.8736743096997253
Root mean squared error: 0.4234730241568094
Relative absolute error: 60.487813058998356
Root relative squared error: 84.69460483136187
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5349618242735318
Weighted FMeasure: 0.7391830255259888
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8553344059989033
Mean absolute error: 0.30243906529499176
Coverage of cases: 97.3913043478261
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.29624804631997725
Kappa statistic: 0.48270322373353314
Training time: 8.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 86.81159420289855
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8390170132325143
Weighted AreaUnderROC: 0.9159423249976318
Root mean squared error: 0.3617066270836533
Relative absolute error: 51.49154906233516
Root relative squared error: 72.34132541673065
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6559727472220466
Weighted FMeasure: 0.8215875085729399
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9048419113979319
Mean absolute error: 0.2574577453116758
Coverage of cases: 98.26086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20364006109690252
Kappa statistic: 0.6385542168674698
Training time: 10.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 86.95652173913044
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8187745179638743
Weighted AreaUnderROC: 0.8974710204129962
Root mean squared error: 0.3702025238772138
Relative absolute error: 53.03714576449947
Root relative squared error: 74.04050477544276
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6251734815388194
Weighted FMeasure: 0.8117618542645054
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8821675700910643
Mean absolute error: 0.26518572882249736
Coverage of cases: 99.1304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.20756873401534526
Kappa statistic: 0.6175828743635041
Training time: 11.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 90.8695652173913
Incorrectly Classified Instances: 13.91304347826087
Correctly Classified Instances: 86.08695652173913
Weighted Precision: 0.8606956759943545
Weighted AreaUnderROC: 0.9190590366581416
Root mean squared error: 0.3360892871420097
Relative absolute error: 49.922235243583465
Root relative squared error: 67.21785742840194
Weighted TruePositiveRate: 0.8608695652173913
Weighted MatthewsCorrelation: 0.7175858859263237
Weighted FMeasure: 0.8606653387991605
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9087400104219664
Mean absolute error: 0.2496111762179173
Coverage of cases: 99.71014492753623
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 94.0
Weighted Recall: 0.8608695652173913
Weighted FalsePositiveRate: 0.14538753907360044
Kappa statistic: 0.71738685234487
Training time: 14.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 86.52173913043478
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8360987074030551
Weighted AreaUnderROC: 0.9018115942028986
Root mean squared error: 0.3494060978699102
Relative absolute error: 49.31967734564199
Root relative squared error: 69.88121957398205
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6673949387278373
Weighted FMeasure: 0.8350962390517692
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8893966944016047
Mean absolute error: 0.24659838672820997
Coverage of cases: 98.55072463768116
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 108.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.16484796817277636
Kappa statistic: 0.666836086404066
Training time: 14.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 88.98550724637681
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8277072643802322
Weighted AreaUnderROC: 0.8976317549966846
Root mean squared error: 0.3598848509025812
Relative absolute error: 54.38111301770627
Root relative squared error: 71.97697018051625
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6502430325659478
Weighted FMeasure: 0.8264522705444948
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8891847788408791
Mean absolute error: 0.27190556508853136
Coverage of cases: 99.42028985507247
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 126.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.17310493037794827
Kappa statistic: 0.6495327102803738
Training time: 17.0
		
Time end:Tue Oct 31 11.28.16 EET 2017