Tue Oct 31 11.26.21 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.26.21 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8086593486381397
Weighted AreaUnderROC: 0.8629890120299327
Root mean squared error: 0.4176586982951496
Relative absolute error: 41.47125604323425
Root relative squared error: 83.53173965902992
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6023918815935821
Weighted FMeasure: 0.7993278722957361
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8381144498255558
Mean absolute error: 0.20735628021617125
Coverage of cases: 87.53623188405797
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.22211832196646772
Kappa statistic: 0.5925810149004898
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 58.26086956521739
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7905590062111801
Weighted AreaUnderROC: 0.887350217864924
Root mean squared error: 0.4338092713211232
Relative absolute error: 43.166907703975205
Root relative squared error: 86.76185426422464
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5507571540634773
Weighted FMeasure: 0.7654420851589708
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8697356221675746
Mean absolute error: 0.21583453851987602
Coverage of cases: 88.1159420289855
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.2638027493606138
Kappa statistic: 0.5268987341772152
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 59.56521739130435
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8013936496482049
Weighted AreaUnderROC: 0.8939723051056172
Root mean squared error: 0.4281076915355388
Relative absolute error: 42.75402521413619
Root relative squared error: 85.62153830710776
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.570602398803706
Weighted FMeasure: 0.774185053661647
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8769549506618237
Mean absolute error: 0.21377012607068097
Coverage of cases: 89.27536231884058
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2555457871554419
Kappa statistic: 0.5447828152214071
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 61.30434782608695
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.8061739997975738
Weighted AreaUnderROC: 0.9022368156199678
Root mean squared error: 0.42819996939060906
Relative absolute error: 44.946602224296036
Root relative squared error: 85.63999387812181
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.570810617111842
Weighted FMeasure: 0.7690561264822133
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8889185687844412
Mean absolute error: 0.2247330111214802
Coverage of cases: 89.56521739130434
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.26316602728047744
Kappa statistic: 0.5364870598218073
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 62.89855072463768
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8146799997177195
Weighted AreaUnderROC: 0.9077515215023206
Root mean squared error: 0.4113140643342337
Relative absolute error: 42.56057345297796
Root relative squared error: 82.26281286684673
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5925034383961295
Weighted FMeasure: 0.7823869543231707
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8935251170723632
Mean absolute error: 0.2128028672648898
Coverage of cases: 92.17391304347827
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24861643932935495
Kappa statistic: 0.5620901205838799
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 65.5072463768116
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8013936496482049
Weighted AreaUnderROC: 0.9077137799564271
Root mean squared error: 0.3984604349770979
Relative absolute error: 42.605123856504626
Root relative squared error: 79.69208699541957
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.570602398803706
Weighted FMeasure: 0.774185053661647
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8961284582194918
Mean absolute error: 0.21302561928252312
Coverage of cases: 94.4927536231884
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2555457871554419
Kappa statistic: 0.5447828152214071
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 65.94202898550725
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.794820216766816
Weighted AreaUnderROC: 0.9016884531590414
Root mean squared error: 0.4077989058088863
Relative absolute error: 43.599990881752845
Root relative squared error: 81.55978116177725
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5578879489175548
Weighted FMeasure: 0.7681633217592909
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8903353114334359
Mean absolute error: 0.2179999544087642
Coverage of cases: 94.20289855072464
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2614929667519182
Kappa statistic: 0.532643690293978
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 61.30434782608695
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7935794007758027
Weighted AreaUnderROC: 0.897739651416122
Root mean squared error: 0.45244379013699343
Relative absolute error: 48.82480512406182
Root relative squared error: 90.48875802739869
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5267568724361734
Weighted FMeasure: 0.7365220064123358
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8862632204794046
Mean absolute error: 0.2441240256203091
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.2985578289286729
Kappa statistic: 0.47690908117942965
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 62.17391304347826
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.790285807980449
Weighted AreaUnderROC: 0.8947440087145969
Root mean squared error: 0.4502763610743862
Relative absolute error: 48.74582327124682
Root relative squared error: 90.05527221487723
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.524126564728329
Weighted FMeasure: 0.7373899167737936
Iteration time: 2.0
Weighted AreaUnderPRC: 0.882785692621813
Mean absolute error: 0.2437291163562341
Coverage of cases: 88.98550724637681
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.297230214549588
Kappa statistic: 0.4776358681130765
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 61.88405797101449
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7893014167073765
Weighted AreaUnderROC: 0.8951865468409586
Root mean squared error: 0.4502736054164034
Relative absolute error: 48.259248333179656
Root relative squared error: 90.05472108328067
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5271562626971862
Weighted FMeasure: 0.7417175709665829
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8851343290190296
Mean absolute error: 0.24129624166589828
Coverage of cases: 88.69565217391305
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.29226520318272237
Kappa statistic: 0.4848549488054607
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 60.72463768115942
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.78596326995441
Weighted AreaUnderROC: 0.8953227124183006
Root mean squared error: 0.4464143159412749
Relative absolute error: 47.27419266350606
Root relative squared error: 89.28286318825498
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5282841303856266
Weighted FMeasure: 0.7467076306392185
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8858161523021593
Mean absolute error: 0.23637096331753027
Coverage of cases: 88.69565217391305
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.2859725774367718
Kappa statistic: 0.49274592124143063
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 60.0
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7811803756883848
Weighted AreaUnderROC: 0.8907271241830065
Root mean squared error: 0.44880385733659145
Relative absolute error: 47.330869947489674
Root relative squared error: 89.76077146731829
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5244881490827233
Weighted FMeasure: 0.7481324380599816
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8799508957997961
Mean absolute error: 0.23665434973744837
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.28331734867860187
Kappa statistic: 0.4941438338014733
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7798493861145529
Weighted AreaUnderROC: 0.8871868191721133
Root mean squared error: 0.4455897816772661
Relative absolute error: 46.6361960551172
Root relative squared error: 89.11795633545321
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310436832107464
Weighted FMeasure: 0.7567160941114337
Iteration time: 1.0
Weighted AreaUnderPRC: 0.876035256773732
Mean absolute error: 0.233180980275586
Coverage of cases: 87.82608695652173
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27205971156578573
Kappa statistic: 0.5090391609128763
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7887092423316339
Weighted AreaUnderROC: 0.8873875153926305
Root mean squared error: 0.44111202215844986
Relative absolute error: 45.76979862705323
Root relative squared error: 88.22240443168997
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5492990877808881
Weighted FMeasure: 0.7660060667340748
Iteration time: 2.0
Weighted AreaUnderPRC: 0.875509457938417
Mean absolute error: 0.22884899313526613
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26247513498152886
Kappa statistic: 0.5275466132940061
Training time: 1.0
		
Time end:Tue Oct 31 11.26.22 EET 2017