Tue Oct 31 11.28.01 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.28.01 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8155708969839406
Weighted AreaUnderROC: 0.8142211191949411
Root mean squared error: 0.4248294522269728
Relative absolute error: 38.87323943661984
Root relative squared error: 84.96589044539456
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6264646292035497
Weighted FMeasure: 0.8147850048413928
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7580403915257412
Mean absolute error: 0.1943661971830992
Coverage of cases: 81.44927536231884
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.18605051523330604
Kappa statistic: 0.6260795935647756
Training time: 0.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8142405495813394
Weighted AreaUnderROC: 0.8098184537975115
Root mean squared error: 0.4260879280164777
Relative absolute error: 38.48383500557435
Root relative squared error: 85.21758560329555
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6234639630354963
Weighted FMeasure: 0.8139219620958751
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7549375081754279
Mean absolute error: 0.19241917502787176
Coverage of cases: 81.44927536231884
Instances selection time: 8.0
Test time: 11.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.19485584602816564
Kappa statistic: 0.6227704503519442
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8191687754006595
Weighted AreaUnderROC: 0.8092914938464677
Root mean squared error: 0.4235559126337296
Relative absolute error: 37.66549157853499
Root relative squared error: 84.71118252674592
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6300840475017232
Weighted FMeasure: 0.815714737528077
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7567278449572086
Mean absolute error: 0.18832745789267494
Coverage of cases: 81.73913043478261
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 25.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.19880831665489074
Kappa statistic: 0.6260645161290324
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8060096964040341
Weighted AreaUnderROC: 0.7930577276127014
Root mean squared error: 0.44062516780838507
Relative absolute error: 40.34517092598734
Root relative squared error: 88.12503356167701
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6012696896113201
Weighted FMeasure: 0.8003959790728378
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7399361764245046
Mean absolute error: 0.2017258546299367
Coverage of cases: 80.28985507246377
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.21678309549923483
Kappa statistic: 0.5950983776320331
Training time: 0.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8002346991742537
Weighted AreaUnderROC: 0.782059563473176
Root mean squared error: 0.45068300361966623
Relative absolute error: 41.93876571647949
Root relative squared error: 90.13660072393324
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5854891160980241
Weighted FMeasure: 0.7904107962976405
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7298722605680049
Mean absolute error: 0.20969382858239743
Coverage of cases: 79.42028985507247
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.23008377160437257
Kappa statistic: 0.5753367660061373
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.7989146537842191
Weighted AreaUnderROC: 0.7781838580267899
Root mean squared error: 0.4541904511435546
Relative absolute error: 42.42054411390798
Root relative squared error: 90.83809022871093
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5807070298899321
Weighted FMeasure: 0.7868845055293803
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7265785199187846
Mean absolute error: 0.21210272056953988
Coverage of cases: 79.1304347826087
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.234936631772507
Kappa statistic: 0.5685252735799895
Training time: 2.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.792930056710775
Weighted AreaUnderROC: 0.7678146460868974
Root mean squared error: 0.46383441780498397
Relative absolute error: 44.07011154108813
Root relative squared error: 92.76688356099679
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5648440070451537
Weighted FMeasure: 0.7769190398840263
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7169628229363579
Mean absolute error: 0.22035055770544065
Coverage of cases: 78.26086956521739
Instances selection time: 10.0
Test time: 19.0
Accumulative iteration time: 67.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.24697940347837913
Kappa statistic: 0.5490981963927857
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.778497157112627
Weighted AreaUnderROC: 0.7483341266063779
Root mean squared error: 0.48226511633692765
Relative absolute error: 47.45930352359362
Root relative squared error: 96.45302326738553
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310974235492204
Weighted FMeasure: 0.7575028756180497
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6981948701337067
Mean absolute error: 0.23729651761796808
Coverage of cases: 76.52173913043478
Instances selection time: 9.0
Test time: 21.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.268549138091592
Kappa statistic: 0.5111348249741967
Training time: 0.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7840466544447713
Weighted AreaUnderROC: 0.7632079961922894
Root mean squared error: 0.47039853201920523
Relative absolute error: 45.11700859526949
Root relative squared error: 94.07970640384104
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.550526740138165
Weighted FMeasure: 0.7718710650208929
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7107396433432711
Mean absolute error: 0.22558504297634746
Coverage of cases: 77.68115942028986
Instances selection time: 9.0
Test time: 22.0
Accumulative iteration time: 85.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.25039560181831977
Kappa statistic: 0.5382649957415745
Training time: 0.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7887414011888065
Weighted AreaUnderROC: 0.7748351125314477
Root mean squared error: 0.46130198436659
Relative absolute error: 43.353542352329896
Root relative squared error: 92.260396873318
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5656455889274427
Weighted FMeasure: 0.7824536104913751
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7206566215322846
Mean absolute error: 0.21676771176164947
Coverage of cases: 78.55072463768116
Instances selection time: 7.0
Test time: 24.0
Accumulative iteration time: 92.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.23583702131391637
Kappa statistic: 0.5588082812013964
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.7894547964113182
Weighted AreaUnderROC: 0.7799687223770994
Root mean squared error: 0.45830801699040513
Relative absolute error: 42.74453179314394
Root relative squared error: 91.66160339808103
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5703481320337115
Weighted FMeasure: 0.7864631085642796
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7245140576709574
Mean absolute error: 0.2137226589657197
Coverage of cases: 78.84057971014492
Instances selection time: 6.0
Test time: 27.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2284683523472506
Kappa statistic: 0.5667096774193549
Training time: 0.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.7942107586173538
Weighted AreaUnderROC: 0.7877201332698714
Root mean squared error: 0.4521000966659398
Relative absolute error: 41.56382289954697
Root relative squared error: 90.42001933318797
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5819057551406358
Weighted FMeasure: 0.793054920726721
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7316362213952065
Mean absolute error: 0.20781911449773485
Coverage of cases: 79.42028985507247
Instances selection time: 6.0
Test time: 26.0
Accumulative iteration time: 104.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.21876263201098164
Kappa statistic: 0.5801840711604711
Training time: 0.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8054478532739402
Weighted AreaUnderROC: 0.8013360984565173
Root mean squared error: 0.43927707011498196
Relative absolute error: 39.23388787921145
Root relative squared error: 87.8554140229964
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6058525472117865
Weighted FMeasure: 0.8052862550610269
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7453948639851962
Mean absolute error: 0.19616943939605727
Coverage of cases: 80.57971014492753
Instances selection time: 4.0
Test time: 27.0
Accumulative iteration time: 109.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.20312490453624077
Kappa statistic: 0.6053372944731856
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8143016642755797
Weighted AreaUnderROC: 0.81170531039641
Root mean squared error: 0.4294129527693189
Relative absolute error: 37.481500941372225
Root relative squared error: 85.88259055386378
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6242388480392322
Weighted FMeasure: 0.8143696125793313
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7561090118576032
Mean absolute error: 0.18740750470686113
Coverage of cases: 81.44927536231884
Instances selection time: 2.0
Test time: 29.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.19108213283036865
Kappa statistic: 0.6241957994349321
Training time: 0.0
		
Time end:Tue Oct 31 11.28.02 EET 2017