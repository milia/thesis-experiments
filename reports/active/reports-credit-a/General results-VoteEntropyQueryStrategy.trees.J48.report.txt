Tue Oct 31 11.28.06 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.28.06 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 80.05797101449276
Incorrectly Classified Instances: 18.347826086956523
Correctly Classified Instances: 81.65217391304347
Weighted Precision: 0.8275789625271992
Weighted AreaUnderROC: 0.8242005505729713
Root mean squared error: 0.39333385225780876
Relative absolute error: 45.81324522097965
Root relative squared error: 78.66677045156175
Weighted TruePositiveRate: 0.8165217391304347
Weighted MatthewsCorrelation: 0.6388789887353965
Weighted FMeasure: 0.8144334431074904
Iteration time: 2.4
Weighted AreaUnderPRC: 0.7951642157687393
Mean absolute error: 0.2290662261048982
Coverage of cases: 92.98550724637681
Instances selection time: 0.3
Test time: 1.4
Accumulative iteration time: 2.4
Weighted Recall: 0.8165217391304347
Weighted FalsePositiveRate: 0.18968074501820956
Kappa statistic: 0.6272105006180368
Training time: 2.1
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 84.23188405797102
Incorrectly Classified Instances: 16.81159420289855
Correctly Classified Instances: 83.18840579710145
Weighted Precision: 0.8423956613220737
Weighted AreaUnderROC: 0.843391625500351
Root mean squared error: 0.37236958003854814
Relative absolute error: 46.181832165767666
Root relative squared error: 74.47391600770963
Weighted TruePositiveRate: 0.8318840579710145
Weighted MatthewsCorrelation: 0.6691303688517356
Weighted FMeasure: 0.8298081147405334
Iteration time: 1.9
Weighted AreaUnderPRC: 0.8109284870814353
Mean absolute error: 0.23090916082883836
Coverage of cases: 95.1304347826087
Instances selection time: 0.4
Test time: 1.0
Accumulative iteration time: 4.3
Weighted Recall: 0.8318840579710145
Weighted FalsePositiveRate: 0.17450029274828252
Kappa statistic: 0.6576275232766524
Training time: 1.5
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 76.69565217391305
Incorrectly Classified Instances: 19.275362318840582
Correctly Classified Instances: 80.72463768115942
Weighted Precision: 0.8249340894248002
Weighted AreaUnderROC: 0.8421380950742188
Root mean squared error: 0.3916170873869381
Relative absolute error: 46.74246606506837
Root relative squared error: 78.32341747738762
Weighted TruePositiveRate: 0.8072463768115942
Weighted MatthewsCorrelation: 0.6262122592450087
Weighted FMeasure: 0.8032479650787924
Iteration time: 2.2
Weighted AreaUnderPRC: 0.8153557089775543
Mean absolute error: 0.2337123303253419
Coverage of cases: 93.99999999999999
Instances selection time: 0.2
Test time: 1.0
Accumulative iteration time: 6.5
Weighted Recall: 0.8072463768115942
Weighted FalsePositiveRate: 0.20087183141593315
Kappa statistic: 0.6082689927977698
Training time: 2.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 77.52173913043478
Incorrectly Classified Instances: 19.681159420289852
Correctly Classified Instances: 80.31884057971016
Weighted Precision: 0.8258633754680045
Weighted AreaUnderROC: 0.8624564609518822
Root mean squared error: 0.3890642565036513
Relative absolute error: 45.92460510123401
Root relative squared error: 77.81285130073024
Weighted TruePositiveRate: 0.8031884057971015
Weighted MatthewsCorrelation: 0.6207778217463629
Weighted FMeasure: 0.7969300913072692
Iteration time: 2.3
Weighted AreaUnderPRC: 0.830964478718926
Mean absolute error: 0.22962302550616998
Coverage of cases: 95.10144927536231
Instances selection time: 0.3
Test time: 1.0
Accumulative iteration time: 8.8
Weighted Recall: 0.8031884057971015
Weighted FalsePositiveRate: 0.2154902979366322
Kappa statistic: 0.5958929435119565
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 73.97101449275362
Incorrectly Classified Instances: 18.086956521739133
Correctly Classified Instances: 81.91304347826086
Weighted Precision: 0.8362125589813638
Weighted AreaUnderROC: 0.8491285073214065
Root mean squared error: 0.3907602727756081
Relative absolute error: 46.89696473275771
Root relative squared error: 78.15205455512164
Weighted TruePositiveRate: 0.8191304347826087
Weighted MatthewsCorrelation: 0.6465960985135443
Weighted FMeasure: 0.8136979976495393
Iteration time: 2.7
Weighted AreaUnderPRC: 0.8235613824589605
Mean absolute error: 0.2344848236637885
Coverage of cases: 93.71014492753622
Instances selection time: 0.6
Test time: 0.6
Accumulative iteration time: 11.5
Weighted Recall: 0.8191304347826087
Weighted FalsePositiveRate: 0.2021911916095343
Kappa statistic: 0.6260005289959756
Training time: 2.1
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 74.92753623188406
Incorrectly Classified Instances: 18.695652173913047
Correctly Classified Instances: 81.30434782608697
Weighted Precision: 0.8330535330048073
Weighted AreaUnderROC: 0.8322285402142772
Root mean squared error: 0.4008352787897308
Relative absolute error: 47.604175157001016
Root relative squared error: 80.16705575794617
Weighted TruePositiveRate: 0.8130434782608695
Weighted MatthewsCorrelation: 0.636474998700768
Weighted FMeasure: 0.8066642011153619
Iteration time: 2.8
Weighted AreaUnderPRC: 0.814131991669403
Mean absolute error: 0.23802087578500508
Coverage of cases: 93.13043478260869
Instances selection time: 0.3
Test time: 1.3
Accumulative iteration time: 14.3
Weighted Recall: 0.8130434782608695
Weighted FalsePositiveRate: 0.2110045672458845
Kappa statistic: 0.6124275762572688
Training time: 2.5
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 73.18840579710145
Incorrectly Classified Instances: 20.318840579710148
Correctly Classified Instances: 79.68115942028984
Weighted Precision: 0.8254858797031297
Weighted AreaUnderROC: 0.8381152405102397
Root mean squared error: 0.40840158481638633
Relative absolute error: 49.2432278685247
Root relative squared error: 81.68031696327725
Weighted TruePositiveRate: 0.7968115942028986
Weighted MatthewsCorrelation: 0.6100642485762927
Weighted FMeasure: 0.7871505978501128
Iteration time: 3.2
Weighted AreaUnderPRC: 0.8189468123106745
Mean absolute error: 0.24621613934262349
Coverage of cases: 92.31884057971014
Instances selection time: 0.4
Test time: 1.1
Accumulative iteration time: 17.5
Weighted Recall: 0.7968115942028986
Weighted FalsePositiveRate: 0.2310169216813435
Kappa statistic: 0.5767194725222963
Training time: 2.8
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 78.15942028985506
Incorrectly Classified Instances: 21.333333333333336
Correctly Classified Instances: 78.66666666666666
Weighted Precision: 0.8231365483098984
Weighted AreaUnderROC: 0.8240776098007292
Root mean squared error: 0.41431499772106084
Relative absolute error: 50.22277622600117
Root relative squared error: 82.86299954421217
Weighted TruePositiveRate: 0.7866666666666665
Weighted MatthewsCorrelation: 0.5947829529470765
Weighted FMeasure: 0.7738803981141908
Iteration time: 3.3
Weighted AreaUnderPRC: 0.8015373397456166
Mean absolute error: 0.2511138811300059
Coverage of cases: 94.08695652173913
Instances selection time: 0.2
Test time: 0.9
Accumulative iteration time: 20.8
Weighted Recall: 0.7866666666666665
Weighted FalsePositiveRate: 0.24728720672784427
Kappa statistic: 0.5527387493291572
Training time: 3.1
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 80.63768115942028
Incorrectly Classified Instances: 21.507246376811594
Correctly Classified Instances: 78.4927536231884
Weighted Precision: 0.8229870985132667
Weighted AreaUnderROC: 0.8644211634823161
Root mean squared error: 0.40419519528330505
Relative absolute error: 49.92247241860134
Root relative squared error: 80.83903905666102
Weighted TruePositiveRate: 0.784927536231884
Weighted MatthewsCorrelation: 0.5921525501925119
Weighted FMeasure: 0.7712486614877859
Iteration time: 3.7
Weighted AreaUnderPRC: 0.8304203336929186
Mean absolute error: 0.2496123620930067
Coverage of cases: 96.72463768115941
Instances selection time: 0.4
Test time: 0.9
Accumulative iteration time: 24.5
Weighted Recall: 0.784927536231884
Weighted FalsePositiveRate: 0.2497451732859124
Kappa statistic: 0.5485166360202107
Training time: 3.3
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 76.47826086956522
Incorrectly Classified Instances: 18.028985507246375
Correctly Classified Instances: 81.97101449275362
Weighted Precision: 0.832535235183216
Weighted AreaUnderROC: 0.865955036028373
Root mean squared error: 0.3786937520632263
Relative absolute error: 45.27019977005377
Root relative squared error: 75.73875041264526
Weighted TruePositiveRate: 0.8197101449275361
Weighted MatthewsCorrelation: 0.6446514940339928
Weighted FMeasure: 0.815615675181445
Iteration time: 4.2
Weighted AreaUnderPRC: 0.8373402317441831
Mean absolute error: 0.22635099885026883
Coverage of cases: 95.07246376811595
Instances selection time: 0.4
Test time: 0.8
Accumulative iteration time: 28.7
Weighted Recall: 0.8197101449275361
Weighted FalsePositiveRate: 0.1981586133994879
Kappa statistic: 0.6289405008578404
Training time: 3.8
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 77.30434782608697
Incorrectly Classified Instances: 16.985507246376812
Correctly Classified Instances: 83.01449275362319
Weighted Precision: 0.8399861409920677
Weighted AreaUnderROC: 0.8828586501520087
Root mean squared error: 0.36169949673776336
Relative absolute error: 42.92511773940501
Root relative squared error: 72.33989934755267
Weighted TruePositiveRate: 0.8301449275362318
Weighted MatthewsCorrelation: 0.6633897000106819
Weighted FMeasure: 0.8270844445830227
Iteration time: 4.5
Weighted AreaUnderPRC: 0.8509231065627192
Mean absolute error: 0.21462558869702505
Coverage of cases: 95.8840579710145
Instances selection time: 0.3
Test time: 1.0
Accumulative iteration time: 33.2
Weighted Recall: 0.8301449275362318
Weighted FalsePositiveRate: 0.1856647559459394
Kappa statistic: 0.6511381383243823
Training time: 4.2
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 83.46376811594203
Incorrectly Classified Instances: 15.91304347826087
Correctly Classified Instances: 84.08695652173913
Weighted Precision: 0.8469439023692165
Weighted AreaUnderROC: 0.8851829803758988
Root mean squared error: 0.35335729740808597
Relative absolute error: 42.21951437019401
Root relative squared error: 70.67145948161719
Weighted TruePositiveRate: 0.8408695652173914
Weighted MatthewsCorrelation: 0.6827136649002913
Weighted FMeasure: 0.8393790903680541
Iteration time: 4.9
Weighted AreaUnderPRC: 0.8536557738509181
Mean absolute error: 0.21109757185097008
Coverage of cases: 96.7536231884058
Instances selection time: 0.1
Test time: 0.9
Accumulative iteration time: 38.1
Weighted Recall: 0.8408695652173914
Weighted FalsePositiveRate: 0.16933838978116894
Kappa statistic: 0.6754102442196862
Training time: 4.8
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 81.94202898550725
Incorrectly Classified Instances: 15.56521739130435
Correctly Classified Instances: 84.43478260869566
Weighted Precision: 0.8484241541031758
Weighted AreaUnderROC: 0.8597026792358708
Root mean squared error: 0.3598087490929983
Relative absolute error: 40.54203688202399
Root relative squared error: 71.96174981859967
Weighted TruePositiveRate: 0.8443478260869565
Weighted MatthewsCorrelation: 0.687866429291596
Weighted FMeasure: 0.8431758944808948
Iteration time: 5.4
Weighted AreaUnderPRC: 0.8314099426825006
Mean absolute error: 0.20271018441011995
Coverage of cases: 94.78260869565216
Instances selection time: 0.2
Test time: 0.9
Accumulative iteration time: 43.5
Weighted Recall: 0.8443478260869565
Weighted FalsePositiveRate: 0.16495017628056402
Kappa statistic: 0.6826670347081374
Training time: 5.2
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 85.73913043478261
Incorrectly Classified Instances: 14.956521739130437
Correctly Classified Instances: 85.04347826086958
Weighted Precision: 0.8525847586760558
Weighted AreaUnderROC: 0.8744824907195943
Root mean squared error: 0.35347748239977766
Relative absolute error: 40.0273065526201
Root relative squared error: 70.69549647995555
Weighted TruePositiveRate: 0.8504347826086956
Weighted MatthewsCorrelation: 0.6990315448214359
Weighted FMeasure: 0.849976254090808
Iteration time: 5.8
Weighted AreaUnderPRC: 0.846791218691496
Mean absolute error: 0.20013653276310048
Coverage of cases: 95.6231884057971
Instances selection time: 0.2
Test time: 0.9
Accumulative iteration time: 49.3
Weighted Recall: 0.8504347826086956
Weighted FalsePositiveRate: 0.15446031416829714
Kappa statistic: 0.6965423140220011
Training time: 5.6
		
Time end:Tue Oct 31 11.28.11 EET 2017