Tue Oct 31 11.25.00 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.00 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8298136645962731
Weighted AreaUnderROC: 0.9030731978781851
Root mean squared error: 0.4067219082991395
Relative absolute error: 39.46643471249497
Root relative squared error: 81.3443816598279
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6268377218920657
Weighted FMeasure: 0.8015279182114369
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8931339960787658
Mean absolute error: 0.19733217356247487
Coverage of cases: 89.27536231884058
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.22811967178175618
Kappa statistic: 0.5996835443037976
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 59.85507246376812
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7885976971988315
Weighted AreaUnderROC: 0.8925618073316282
Root mean squared error: 0.44964211062001547
Relative absolute error: 46.761700046623346
Root relative squared error: 89.92842212400309
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.530389165719191
Weighted FMeasure: 0.745959741981795
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8826235269512286
Mean absolute error: 0.23380850023311672
Coverage of cases: 87.53623188405797
Instances selection time: 13.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.28730019181585675
Kappa statistic: 0.49204406364749087
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 58.84057971014493
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8160155949616278
Weighted AreaUnderROC: 0.903795319456285
Root mean squared error: 0.4442096966492753
Relative absolute error: 45.44423253222681
Root relative squared error: 88.84193932985505
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5752011799636968
Weighted FMeasure: 0.7628686354546576
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8959709766058119
Mean absolute error: 0.22722116266113407
Coverage of cases: 86.3768115942029
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2721138817845979
Kappa statistic: 0.5274561075831155
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 57.2463768115942
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8127365728900255
Weighted AreaUnderROC: 0.9028232156389124
Root mean squared error: 0.4454741418645014
Relative absolute error: 44.955677123506895
Root relative squared error: 89.09482837290028
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5725023040489121
Weighted FMeasure: 0.7636078058410516
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8952143948177365
Mean absolute error: 0.22477838561753447
Coverage of cases: 84.6376811594203
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2707862674055129
Kappa statistic: 0.5281108446576074
Training time: 4.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 57.391304347826086
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.8068523877596085
Weighted AreaUnderROC: 0.9071956107322156
Root mean squared error: 0.45466106730698225
Relative absolute error: 46.74407414004376
Root relative squared error: 90.93221346139644
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5564626596783527
Weighted FMeasure: 0.7532367149758454
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8958947347001702
Mean absolute error: 0.23372037070021878
Coverage of cases: 84.05797101449275
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.28169845836885476
Kappa statistic: 0.5087044750614118
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 57.10144927536232
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.8068523877596085
Weighted AreaUnderROC: 0.9036402091029648
Root mean squared error: 0.4551362531088295
Relative absolute error: 46.401476406100855
Root relative squared error: 91.0272506217659
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5564626596783527
Weighted FMeasure: 0.7532367149758454
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8922322704875697
Mean absolute error: 0.23200738203050428
Coverage of cases: 83.47826086956522
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.28169845836885476
Kappa statistic: 0.5087044750614118
Training time: 0.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 57.2463768115942
Incorrectly Classified Instances: 24.057971014492754
Correctly Classified Instances: 75.94202898550725
Weighted Precision: 0.7944006551729441
Weighted AreaUnderROC: 0.9004668111205835
Root mean squared error: 0.46411316709179584
Relative absolute error: 47.94268511195688
Root relative squared error: 92.82263341835917
Weighted TruePositiveRate: 0.7594202898550725
Weighted MatthewsCorrelation: 0.5350465043086027
Weighted FMeasure: 0.7443908667887867
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8909707371909202
Mean absolute error: 0.23971342555978437
Coverage of cases: 83.47826086956522
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7594202898550725
Weighted FalsePositiveRate: 0.2899554205740267
Kappa statistic: 0.49063450557660493
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 25.797101449275363
Correctly Classified Instances: 74.20289855072464
Weighted Precision: 0.7787558601651555
Weighted AreaUnderROC: 0.9030350123141043
Root mean squared error: 0.4716640703930506
Relative absolute error: 49.19282764268478
Root relative squared error: 94.33281407861011
Weighted TruePositiveRate: 0.7420289855072464
Weighted MatthewsCorrelation: 0.4995678956273046
Weighted FMeasure: 0.7241230420082103
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8927798155792193
Mean absolute error: 0.24596413821342392
Coverage of cases: 83.18840579710145
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7420289855072464
Weighted FalsePositiveRate: 0.3104521881216255
Kappa statistic: 0.45229303794081455
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 57.2463768115942
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7808695652173913
Weighted AreaUnderROC: 0.9033566294875438
Root mean squared error: 0.4726657071080805
Relative absolute error: 49.58203918995113
Root relative squared error: 94.5331414216161
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5051101322233998
Weighted FMeasure: 0.7276754795800582
Iteration time: 3.0
Weighted AreaUnderPRC: 0.892386982035546
Mean absolute error: 0.24791019594975566
Coverage of cases: 83.47826086956522
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 57.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.3068147911338448
Kappa statistic: 0.45882352941176463
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 56.3768115942029
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.7964582315583781
Weighted AreaUnderROC: 0.9100247466136214
Root mean squared error: 0.46217686352934834
Relative absolute error: 46.89059080880128
Root relative squared error: 92.43537270586967
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5404917158890419
Weighted FMeasure: 0.7478671526102356
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8993823198768877
Mean absolute error: 0.23445295404400637
Coverage of cases: 83.47826086956522
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.2863180235862461
Kappa statistic: 0.4971203071672355
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 24.92753623188406
Correctly Classified Instances: 75.07246376811594
Weighted Precision: 0.7821446018563751
Weighted AreaUnderROC: 0.9069004866439331
Root mean squared error: 0.467282895156949
Relative absolute error: 47.842210832910965
Root relative squared error: 93.4565790313898
Weighted TruePositiveRate: 0.7507246376811594
Weighted MatthewsCorrelation: 0.5138208095053304
Weighted FMeasure: 0.7355679893229301
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8963909878685664
Mean absolute error: 0.23921105416455482
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7507246376811594
Weighted FalsePositiveRate: 0.2982123827791986
Kappa statistic: 0.472589590443686
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 57.53623188405797
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.795049885583524
Weighted AreaUnderROC: 0.9092682876290613
Root mean squared error: 0.45418893113219644
Relative absolute error: 46.200199465241965
Root relative squared error: 90.83778622643929
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5468766861981964
Weighted FMeasure: 0.7562145799089945
Iteration time: 9.0
Weighted AreaUnderPRC: 0.900582694633773
Mean absolute error: 0.23100099732620982
Coverage of cases: 85.79710144927536
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2763880008525149
Kappa statistic: 0.5114179500796601
Training time: 4.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 57.82608695652174
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7947372013406665
Weighted AreaUnderROC: 0.9038783508572512
Root mean squared error: 0.4451475151776285
Relative absolute error: 45.14802835663435
Root relative squared error: 89.0295030355257
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5503637977933682
Weighted FMeasure: 0.7602706338161268
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8900226369069633
Mean absolute error: 0.22574014178317173
Coverage of cases: 86.08695652173913
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 73.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.27142298948564936
Kappa statistic: 0.5185224442206795
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 57.82608695652174
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8013936496482049
Weighted AreaUnderROC: 0.9049676754759876
Root mean squared error: 0.4342549054436123
Relative absolute error: 43.135942947490705
Root relative squared error: 86.85098108872246
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.570602398803706
Weighted FMeasure: 0.774185053661647
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8936733205448665
Mean absolute error: 0.2156797147374535
Coverage of cases: 86.3768115942029
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 75.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2555457871554419
Kappa statistic: 0.5447828152214071
Training time: 2.0
		
Time end:Tue Oct 31 11.25.01 EET 2017