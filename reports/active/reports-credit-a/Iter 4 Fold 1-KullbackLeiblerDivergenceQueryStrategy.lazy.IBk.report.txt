Tue Oct 31 11.25.23 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.23 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8062757693192475
Weighted AreaUnderROC: 0.8042790032679739
Root mean squared error: 0.4346616703883854
Relative absolute error: 40.56338028169022
Root relative squared error: 86.93233407767707
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6074424088855748
Weighted FMeasure: 0.805975255625761
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7476525180809996
Mean absolute error: 0.2028169014084511
Coverage of cases: 80.57971014492753
Instances selection time: 17.0
Test time: 10.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.19723909491332764
Kappa statistic: 0.6073485195943537
Training time: 9.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8204313362717891
Weighted AreaUnderROC: 0.8139808006535947
Root mean squared error: 0.4193819616711725
Relative absolute error: 37.34989648033142
Root relative squared error: 83.8763923342345
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6346068826762286
Weighted FMeasure: 0.8193451820781014
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7607685436453201
Mean absolute error: 0.1867494824016571
Coverage of cases: 82.02898550724638
Instances selection time: 16.0
Test time: 11.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.19232825376527424
Kappa statistic: 0.6330039118797611
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8178781410328426
Weighted AreaUnderROC: 0.804125816993464
Root mean squared error: 0.42690272394592
Relative absolute error: 38.234756495625994
Root relative squared error: 85.380544789184
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6245365837541138
Weighted FMeasure: 0.8120549237271226
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7527745403409593
Mean absolute error: 0.19117378247812997
Coverage of cases: 81.44927536231884
Instances selection time: 17.0
Test time: 13.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.20624111963626032
Kappa statistic: 0.618098796180988
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.8208840866317006
Weighted AreaUnderROC: 0.8021343954248366
Root mean squared error: 0.4274732112529303
Relative absolute error: 38.06173249253232
Root relative squared error: 85.49464225058607
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.62675448626503
Weighted FMeasure: 0.8111321151018693
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7524764978912242
Mean absolute error: 0.19030866246266162
Coverage of cases: 81.44927536231884
Instances selection time: 18.0
Test time: 15.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.2102239627735152
Kappa statistic: 0.6165468375534021
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.420289855072465
Correctly Classified Instances: 80.57971014492753
Weighted Precision: 0.8148338359753149
Weighted AreaUnderROC: 0.7916666666666666
Root mean squared error: 0.4378065011559314
Relative absolute error: 39.6506382570304
Root relative squared error: 87.56130023118628
Weighted TruePositiveRate: 0.8057971014492754
Weighted MatthewsCorrelation: 0.6108846203769794
Weighted FMeasure: 0.8013674234490608
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7424619722770847
Mean absolute error: 0.198253191285152
Coverage of cases: 80.57971014492753
Instances selection time: 18.0
Test time: 17.0
Accumulative iteration time: 99.0
Weighted Recall: 0.8057971014492754
Weighted FalsePositiveRate: 0.22246376811594204
Kappa statistic: 0.5972084269956611
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8028737843467647
Weighted AreaUnderROC: 0.7753267973856209
Root mean squared error: 0.4541904511435546
Relative absolute error: 42.42054411390798
Root relative squared error: 90.83809022871093
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5828966968849462
Weighted FMeasure: 0.7854570799435011
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7261503146079903
Mean absolute error: 0.21210272056953988
Coverage of cases: 79.1304347826087
Instances selection time: 19.0
Test time: 19.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.2406507530548451
Kappa statistic: 0.565673520772136
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.869565217391305
Correctly Classified Instances: 79.1304347826087
Weighted Precision: 0.8081581921675617
Weighted AreaUnderROC: 0.7733353758169933
Root mean squared error: 0.4544642658296035
Relative absolute error: 42.349191896198505
Root relative squared error: 90.8928531659207
Weighted TruePositiveRate: 0.7913043478260869
Weighted MatthewsCorrelation: 0.5871401790916723
Weighted FMeasure: 0.7840056000622231
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7263927236884189
Mean absolute error: 0.2117459594809925
Coverage of cases: 79.1304347826087
Instances selection time: 18.0
Test time: 20.0
Accumulative iteration time: 138.0
Weighted Recall: 0.7913043478260869
Weighted FalsePositiveRate: 0.24463359619210004
Kappa statistic: 0.5638891815021595
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7969046341027017
Weighted AreaUnderROC: 0.7569955065359478
Root mean squared error: 0.4702077596070971
Relative absolute error: 45.16244247544473
Root relative squared error: 94.04155192141943
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5595625397343743
Weighted FMeasure: 0.7675762118940529
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7105223800801018
Mean absolute error: 0.22581221237722363
Coverage of cases: 77.68115942028986
Instances selection time: 18.0
Test time: 22.0
Accumulative iteration time: 157.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.26282058113100315
Kappa statistic: 0.532001479837218
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7887092423316339
Weighted AreaUnderROC: 0.7557189542483661
Root mean squared error: 0.4734429574649847
Relative absolute error: 45.69169960474308
Root relative squared error: 94.68859149299695
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5492990877808881
Weighted FMeasure: 0.7660060667340748
Iteration time: 19.0
Weighted AreaUnderPRC: 0.7070834315034424
Mean absolute error: 0.2284584980237154
Coverage of cases: 77.3913043478261
Instances selection time: 18.0
Test time: 23.0
Accumulative iteration time: 176.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.26247513498152886
Kappa statistic: 0.5275466132940061
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8098044248241877
Weighted AreaUnderROC: 0.7897263071895425
Root mean squared error: 0.4422067305717872
Relative absolute error: 39.90299670881697
Root relative squared error: 88.44134611435744
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6032503215793309
Weighted FMeasure: 0.798969802016454
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7392165350259525
Mean absolute error: 0.19951498354408487
Coverage of cases: 80.28985507246377
Instances selection time: 20.0
Test time: 24.0
Accumulative iteration time: 199.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.22344593634555274
Kappa statistic: 0.5920283806343906
Training time: 3.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8276604554865424
Weighted AreaUnderROC: 0.817861519607843
Root mean squared error: 0.4155035123254758
Relative absolute error: 35.263917856569805
Root relative squared error: 83.10070246509515
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6471243609469265
Weighted FMeasure: 0.8245457495133028
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7666438623338434
Mean absolute error: 0.17631958928284902
Coverage of cases: 82.6086956521739
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 212.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.19036391730605284
Kappa statistic: 0.6434108527131783
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8112313372741535
Weighted AreaUnderROC: 0.8074959150326796
Root mean squared error: 0.4325768381715088
Relative absolute error: 38.10946760296844
Root relative squared error: 86.51536763430177
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6173077158851957
Weighted FMeasure: 0.8112402048683147
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7522224536147943
Mean absolute error: 0.1905473380148422
Coverage of cases: 81.15942028985508
Instances selection time: 10.0
Test time: 28.0
Accumulative iteration time: 223.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.19660237283319124
Kappa statistic: 0.6170398073671808
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8120631381500948
Weighted AreaUnderROC: 0.8101511437908495
Root mean squared error: 0.4326713802352771
Relative absolute error: 38.0819236683908
Root relative squared error: 86.53427604705543
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6191651605254206
Weighted FMeasure: 0.8117670390399174
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7540999353979821
Mean absolute error: 0.190409618341954
Coverage of cases: 81.15942028985508
Instances selection time: 7.0
Test time: 28.0
Accumulative iteration time: 231.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.19129191531685139
Kappa statistic: 0.6190694593079551
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.8225507754448805
Weighted AreaUnderROC: 0.8212826797385621
Root mean squared error: 0.4226504588428941
Relative absolute error: 36.329086212180925
Root relative squared error: 84.53009176857881
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6394169472343616
Weighted FMeasure: 0.820730827979363
Iteration time: 4.0
Weighted AreaUnderPRC: 0.76588528507533
Mean absolute error: 0.18164543106090464
Coverage of cases: 82.02898550724638
Instances selection time: 3.0
Test time: 29.0
Accumulative iteration time: 235.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.17772449559533957
Kappa statistic: 0.6383280917114741
Training time: 1.0
		
Time end:Tue Oct 31 11.25.24 EET 2017