Tue Oct 31 11.24.49 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.24.49 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 51.88405797101449
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7866588327457893
Weighted AreaUnderROC: 0.8053647922757871
Root mean squared error: 0.4376209178801302
Relative absolute error: 42.37829803047191
Root relative squared error: 87.52418357602603
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.568003634592994
Weighted FMeasure: 0.7858451618478606
Iteration time: 4.0
Weighted AreaUnderPRC: 0.755346223613191
Mean absolute error: 0.21189149015235956
Coverage of cases: 82.02898550724638
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.21571055092566588
Kappa statistic: 0.5676545300592718
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 61.88405797101449
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8213858143595806
Weighted AreaUnderROC: 0.8608485755082612
Root mean squared error: 0.37722972786217795
Relative absolute error: 42.91306208256585
Root relative squared error: 75.44594557243559
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6134772026963674
Weighted FMeasure: 0.7961876520539758
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8359809391777595
Mean absolute error: 0.21456531041282925
Coverage of cases: 92.17391304347827
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.23062004389115703
Kappa statistic: 0.5893288520113433
Training time: 2.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 65.3623188405797
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8170113611908842
Weighted AreaUnderROC: 0.8881315020058476
Root mean squared error: 0.3770638227889054
Relative absolute error: 45.052402528542466
Root relative squared error: 75.41276455778107
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5845652594874229
Weighted FMeasure: 0.7707319515409766
Iteration time: 3.0
Weighted AreaUnderPRC: 0.849164668704847
Mean absolute error: 0.22526201264271234
Coverage of cases: 93.91304347826087
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.26081635187030133
Kappa statistic: 0.5426101712891765
Training time: 2.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 64.92753623188406
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8227506242380384
Weighted AreaUnderROC: 0.887706534303393
Root mean squared error: 0.3655064399352061
Relative absolute error: 44.41535329896019
Root relative squared error: 73.10128798704122
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6253488154835638
Weighted FMeasure: 0.807051185589162
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8512462510977622
Mean absolute error: 0.22207676649480096
Coverage of cases: 93.6231884057971
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.2160614633867536
Kappa statistic: 0.6097217146138987
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.3558218376924455
Relative absolute error: 46.652771175387684
Root relative squared error: 71.1643675384891
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.23326385587693843
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 3.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 64.92753623188406
Incorrectly Classified Instances: 18.840579710144926
Correctly Classified Instances: 81.15942028985508
Weighted Precision: 0.8227506242380384
Weighted AreaUnderROC: 0.887706534303393
Root mean squared error: 0.3567098033913766
Relative absolute error: 42.61168060955972
Root relative squared error: 71.34196067827531
Weighted TruePositiveRate: 0.8115942028985508
Weighted MatthewsCorrelation: 0.6253488154835638
Weighted FMeasure: 0.807051185589162
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8512462510977622
Mean absolute error: 0.2130584030477986
Coverage of cases: 93.6231884057971
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8115942028985508
Weighted FalsePositiveRate: 0.2160614633867536
Kappa statistic: 0.6097217146138987
Training time: 3.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.3548345080602963
Relative absolute error: 43.16790322787317
Root relative squared error: 70.96690161205926
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.21583951613936583
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 4.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.35489971437427165
Relative absolute error: 42.171516685941604
Root relative squared error: 70.97994287485433
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.21085758342970803
Coverage of cases: 94.78260869565217
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 3.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.3552078604996077
Relative absolute error: 41.460300098528194
Root relative squared error: 71.04157209992154
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.20730150049264096
Coverage of cases: 94.78260869565217
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 3.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.35609227027342666
Relative absolute error: 40.639206380719216
Root relative squared error: 71.21845405468534
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.20319603190359609
Coverage of cases: 94.78260869565217
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 4.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.3566609836319932
Relative absolute error: 40.17127125121528
Root relative squared error: 71.33219672639865
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.2008563562560764
Coverage of cases: 94.78260869565217
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 4.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.3570532234196543
Relative absolute error: 39.86897687551797
Root relative squared error: 71.41064468393085
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.19934488437758985
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 8.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.35733903967719527
Relative absolute error: 39.65759810153414
Root relative squared error: 71.46780793543905
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.1982879905076707
Coverage of cases: 94.78260869565217
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 90.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 8.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 73.47826086956522
Incorrectly Classified Instances: 18.55072463768116
Correctly Classified Instances: 81.44927536231884
Weighted Precision: 0.826679018569318
Weighted AreaUnderROC: 0.8837628340246142
Root mean squared error: 0.35755621670627785
Relative absolute error: 39.50148175212123
Root relative squared error: 71.51124334125556
Weighted TruePositiveRate: 0.8144927536231884
Weighted MatthewsCorrelation: 0.6321750434165828
Weighted FMeasure: 0.8098305344682156
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8435013613953803
Mean absolute error: 0.19750740876060616
Coverage of cases: 94.78260869565217
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 98.0
Weighted Recall: 0.8144927536231884
Weighted FalsePositiveRate: 0.21372441201715048
Kappa statistic: 0.6154783880742573
Training time: 4.0
		
Time end:Tue Oct 31 11.24.50 EET 2017