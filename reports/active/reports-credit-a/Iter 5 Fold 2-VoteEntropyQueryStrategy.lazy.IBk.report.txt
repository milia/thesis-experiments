Tue Oct 31 11.28.05 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.28.05 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.7902144678064411
Weighted AreaUnderROC: 0.7880412581699346
Root mean squared error: 0.45678121411018674
Relative absolute error: 44.50704225352123
Root relative squared error: 91.35624282203734
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5725077083471023
Weighted FMeasure: 0.7861733729408587
Iteration time: 8.0
Weighted AreaUnderPRC: 0.729463783877409
Mean absolute error: 0.22253521126760614
Coverage of cases: 78.55072463768116
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2094247300369423
Kappa statistic: 0.5700282942603071
Training time: 1.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.794863708042352
Weighted AreaUnderROC: 0.7842626633986928
Root mean squared error: 0.47036025638320655
Relative absolute error: 46.421404682274435
Root relative squared error: 94.0720512766413
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5689127904331734
Weighted FMeasure: 0.7738370631863796
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7265859101312151
Mean absolute error: 0.23210702341137218
Coverage of cases: 77.3913043478261
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.20538771668087524
Kappa statistic: 0.5538201352964583
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7724585984100052
Weighted AreaUnderROC: 0.7471915849673204
Root mean squared error: 0.5145768637343129
Relative absolute error: 54.74343909126517
Root relative squared error: 102.91537274686257
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.506078017289223
Weighted FMeasure: 0.7273830408337898
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6916163247445637
Mean absolute error: 0.27371719545632583
Coverage of cases: 73.04347826086956
Instances selection time: 9.0
Test time: 12.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.23605161267405514
Kappa statistic: 0.4751435441919812
Training time: 0.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.7751432423323222
Weighted AreaUnderROC: 0.7478553921568627
Root mean squared error: 0.5152746738892161
Relative absolute error: 54.61666113508129
Root relative squared error: 103.05493477784321
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.5089826355846991
Weighted FMeasure: 0.7269937859882459
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6926716587779584
Mean absolute error: 0.27308330567540645
Coverage of cases: 73.04347826086956
Instances selection time: 9.0
Test time: 15.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.23472399829497018
Kappa statistic: 0.47581238053227465
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.115942028985508
Correctly Classified Instances: 71.8840579710145
Weighted Precision: 0.7770852396364472
Weighted AreaUnderROC: 0.7394301470588235
Root mean squared error: 0.5267630512704361
Relative absolute error: 56.81159420289853
Root relative squared error: 105.35261025408722
Weighted TruePositiveRate: 0.7188405797101449
Weighted MatthewsCorrelation: 0.5004693178293057
Weighted FMeasure: 0.7129201283517773
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6863918769240803
Mean absolute error: 0.28405797101449265
Coverage of cases: 71.8840579710145
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7188405797101449
Weighted FalsePositiveRate: 0.23998028559249787
Kappa statistic: 0.4567281936395071
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.565217391304348
Correctly Classified Instances: 70.43478260869566
Weighted Precision: 0.7721376589448992
Weighted AreaUnderROC: 0.7270731209150327
Root mean squared error: 0.5405815870959145
Relative absolute error: 59.60844139333843
Root relative squared error: 108.1163174191829
Weighted TruePositiveRate: 0.7043478260869566
Weighted MatthewsCorrelation: 0.4818424768012764
Weighted FMeasure: 0.6960837552317378
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6757764313202341
Mean absolute error: 0.29804220696669215
Coverage of cases: 70.43478260869566
Instances selection time: 8.0
Test time: 18.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7043478260869566
Weighted FalsePositiveRate: 0.25020158425689115
Kappa statistic: 0.43124515128006213
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.985507246376812
Correctly Classified Instances: 71.01449275362319
Weighted Precision: 0.7827365234210748
Weighted AreaUnderROC: 0.733609068627451
Root mean squared error: 0.5355813218776603
Relative absolute error: 58.41110858183476
Root relative squared error: 107.11626437553205
Weighted TruePositiveRate: 0.7101449275362319
Weighted MatthewsCorrelation: 0.49806468526571984
Weighted FMeasure: 0.7014227575909593
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6829259249779701
Mean absolute error: 0.2920555429091738
Coverage of cases: 71.01449275362319
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7101449275362319
Weighted FalsePositiveRate: 0.24292679028132994
Kappa statistic: 0.4430992736077483
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.92753623188406
Correctly Classified Instances: 75.07246376811594
Weighted Precision: 0.795989733219258
Weighted AreaUnderROC: 0.768075980392157
Root mean squared error: 0.4969255718781246
Relative absolute error: 50.33037983377976
Root relative squared error: 99.38511437562492
Weighted TruePositiveRate: 0.7507246376811594
Weighted MatthewsCorrelation: 0.5496589732245432
Weighted FMeasure: 0.747724817715366
Iteration time: 9.0
Weighted AreaUnderPRC: 0.713280601668521
Mean absolute error: 0.2516518991688988
Coverage of cases: 75.07246376811594
Instances selection time: 8.0
Test time: 21.0
Accumulative iteration time: 72.0
Weighted Recall: 0.7507246376811594
Weighted FalsePositiveRate: 0.2145726768968457
Kappa statistic: 0.5149583128984796
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.8068825365660942
Weighted AreaUnderROC: 0.7921262254901962
Root mean squared error: 0.4673342742389596
Relative absolute error: 44.54231758579581
Root relative squared error: 93.46685484779192
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5874811098133883
Weighted FMeasure: 0.7791139462544145
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7357669227615145
Mean absolute error: 0.22271158792897902
Coverage of cases: 77.97101449275362
Instances selection time: 7.0
Test time: 22.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.19545769394714407
Kappa statistic: 0.5669408383708254
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.812920941905957
Weighted AreaUnderROC: 0.8038194444444444
Root mean squared error: 0.45185523862380017
Relative absolute error: 41.62826953057343
Root relative squared error: 90.37104772476003
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.6068749914396385
Weighted FMeasure: 0.7943308506390724
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7472556199939449
Mean absolute error: 0.20814134765286713
Coverage of cases: 79.42028985507247
Instances selection time: 7.0
Test time: 23.0
Accumulative iteration time: 88.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.18656400966183573
Kappa statistic: 0.5930725143284327
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.130434782608695
Correctly Classified Instances: 80.8695652173913
Weighted Precision: 0.8218420844222697
Weighted AreaUnderROC: 0.8161764705882353
Root mean squared error: 0.4357821978845106
Relative absolute error: 38.716508904219424
Root relative squared error: 87.15643957690212
Weighted TruePositiveRate: 0.808695652173913
Weighted MatthewsCorrelation: 0.6294667876305177
Weighted FMeasure: 0.8091299084114116
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7602487423430123
Mean absolute error: 0.1935825445210971
Coverage of cases: 80.8695652173913
Instances selection time: 5.0
Test time: 25.0
Accumulative iteration time: 94.0
Weighted Recall: 0.808695652173913
Weighted FalsePositiveRate: 0.17634271099744245
Kappa statistic: 0.6200060078101531
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.971014492753625
Correctly Classified Instances: 82.02898550724638
Weighted Precision: 0.825897577527026
Weighted AreaUnderROC: 0.823937908496732
Root mean squared error: 0.42247702029934525
Relative absolute error: 36.38228995467923
Root relative squared error: 84.49540405986905
Weighted TruePositiveRate: 0.8202898550724638
Weighted MatthewsCorrelation: 0.6437472990999348
Weighted FMeasure: 0.8208627016182254
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7685673207128954
Mean absolute error: 0.18191144977339616
Coverage of cases: 82.02898550724638
Instances selection time: 10.0
Test time: 44.0
Accumulative iteration time: 105.0
Weighted Recall: 0.8202898550724638
Weighted FalsePositiveRate: 0.17241403807899972
Kappa statistic: 0.6402260571197901
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8283293127251516
Weighted AreaUnderROC: 0.8271548202614379
Root mean squared error: 0.4156981725235772
Relative absolute error: 35.20201314133922
Root relative squared error: 83.13963450471545
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6511036841911243
Weighted FMeasure: 0.8265137044961577
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7725061470976324
Mean absolute error: 0.1760100657066961
Coverage of cases: 82.6086956521739
Instances selection time: 3.0
Test time: 28.0
Accumulative iteration time: 108.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.1717773159988633
Kappa statistic: 0.6499949274627168
Training time: 0.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8308967556294713
Weighted AreaUnderROC: 0.8297589869281046
Root mean squared error: 0.41229879928417684
Relative absolute error: 34.60046411839399
Root relative squared error: 82.45975985683536
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6565078350319579
Weighted FMeasure: 0.8293763975155279
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7755356483123413
Mean absolute error: 0.17300232059196993
Coverage of cases: 82.89855072463769
Instances selection time: 2.0
Test time: 29.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.16946753339016768
Kappa statistic: 0.6556012385158113
Training time: 0.0
		
Time end:Tue Oct 31 11.28.06 EET 2017