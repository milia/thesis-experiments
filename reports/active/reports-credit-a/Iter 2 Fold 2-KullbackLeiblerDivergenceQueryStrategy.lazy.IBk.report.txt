Tue Oct 31 11.25.20 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.20 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7760672295431232
Weighted AreaUnderROC: 0.7736961990888692
Root mean squared error: 0.4719465908407744
Relative absolute error: 47.323943661971974
Root relative squared error: 94.38931816815487
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5442558760941668
Weighted FMeasure: 0.7716708052717582
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7142268606352811
Mean absolute error: 0.23661971830985987
Coverage of cases: 77.10144927536231
Instances selection time: 17.0
Test time: 10.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2236220945758847
Kappa statistic: 0.5416014935163228
Training time: 4.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.771527700033978
Weighted AreaUnderROC: 0.7692935336914395
Root mean squared error: 0.47336414851357567
Relative absolute error: 46.98837394489575
Root relative squared error: 94.67282970271513
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5376556501131963
Weighted FMeasure: 0.7712126493659567
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7102861652129678
Mean absolute error: 0.23494186972447875
Coverage of cases: 77.10144927536231
Instances selection time: 16.0
Test time: 11.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.23242742537074432
Kappa statistic: 0.5375727447021498
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7675641874494226
Weighted AreaUnderROC: 0.7635309716461549
Root mean squared error: 0.47727049885754097
Relative absolute error: 47.342995169082
Root relative squared error: 95.4540997715082
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5293887796311302
Weighted FMeasure: 0.7676053759834086
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7052581091930309
Mean absolute error: 0.23671497584541001
Coverage of cases: 76.81159420289855
Instances selection time: 17.0
Test time: 13.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.24105399873667574
Kappa statistic: 0.5290584581783435
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7684555203400272
Weighted AreaUnderROC: 0.759757258448358
Root mean squared error: 0.47791433856706367
Relative absolute error: 47.19548622635244
Root relative squared error: 95.58286771341274
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.52848435409297
Weighted FMeasure: 0.7661369315342328
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7031565140776691
Mean absolute error: 0.2359774311317622
Coverage of cases: 76.81159420289855
Instances selection time: 21.0
Test time: 15.0
Accumulative iteration time: 77.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.24860142513226965
Kappa statistic: 0.525463360957326
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7793159944575516
Weighted AreaUnderROC: 0.7663527571904536
Root mean squared error: 0.46933583466161294
Relative absolute error: 45.37095690565312
Root relative squared error: 93.86716693232259
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5473214965511017
Weighted FMeasure: 0.773808129838242
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7114243392291048
Mean absolute error: 0.22685478452826557
Coverage of cases: 77.68115942028986
Instances selection time: 20.0
Test time: 17.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2441060798219915
Kappa statistic: 0.5412154810631574
Training time: 1.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.751491284925223
Weighted AreaUnderROC: 0.730009519276535
Root mean squared error: 0.5021188690490863
Relative absolute error: 51.587422662937556
Root relative squared error: 100.42377380981726
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.48406771051269204
Weighted FMeasure: 0.7385169848937965
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6773395003023771
Mean absolute error: 0.25793711331468777
Coverage of cases: 74.4927536231884
Instances selection time: 19.0
Test time: 18.0
Accumulative iteration time: 118.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.2849084976788141
Kappa statistic: 0.47128278360210357
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.7526411549294845
Weighted AreaUnderROC: 0.7293805670769021
Root mean squared error: 0.5024230880905353
Relative absolute error: 51.52743000227646
Root relative squared error: 100.48461761810705
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.4848475955042318
Weighted FMeasure: 0.7379810201004047
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6772153790559919
Mean absolute error: 0.2576371500113823
Coverage of cases: 74.4927536231884
Instances selection time: 17.0
Test time: 20.0
Accumulative iteration time: 136.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.28616640207807975
Kappa statistic: 0.4706005440468717
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.82608695652174
Correctly Classified Instances: 72.17391304347827
Weighted Precision: 0.7335870606031168
Weighted AreaUnderROC: 0.7027775889032434
Root mean squared error: 0.525019796853664
Relative absolute error: 56.07253245415203
Root relative squared error: 105.00395937073279
Weighted TruePositiveRate: 0.7217391304347827
Weighted MatthewsCorrelation: 0.4393410702036244
Weighted FMeasure: 0.710878927461334
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6536736586558533
Mean absolute error: 0.2803626622707602
Coverage of cases: 72.17391304347827
Instances selection time: 17.0
Test time: 21.0
Accumulative iteration time: 154.0
Weighted Recall: 0.7217391304347827
Weighted FalsePositiveRate: 0.31618395262829585
Kappa statistic: 0.41872301590087413
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.956521739130434
Correctly Classified Instances: 73.04347826086956
Weighted Precision: 0.751820594965675
Weighted AreaUnderROC: 0.7087441354457061
Root mean squared error: 0.516962338747914
Relative absolute error: 54.31206474684743
Root relative squared error: 103.39246774958279
Weighted TruePositiveRate: 0.7304347826086957
Weighted MatthewsCorrelation: 0.4646110299140015
Weighted FMeasure: 0.7165304815832392
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6622676439049735
Mean absolute error: 0.27156032373423716
Coverage of cases: 73.04347826086956
Instances selection time: 16.0
Test time: 23.0
Accumulative iteration time: 171.0
Weighted Recall: 0.7304347826086957
Weighted FalsePositiveRate: 0.3129465117172834
Kappa statistic: 0.4335775443551947
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.63768115942029
Correctly Classified Instances: 75.3623188405797
Weighted Precision: 0.7603170412094897
Weighted AreaUnderROC: 0.7391208268171618
Root mean squared error: 0.49439814169630036
Relative absolute error: 49.67954269877018
Root relative squared error: 98.87962833926008
Weighted TruePositiveRate: 0.7536231884057971
Weighted MatthewsCorrelation: 0.5021854077414678
Weighted FMeasure: 0.7476823196165967
Iteration time: 15.0
Weighted AreaUnderPRC: 0.686258534465678
Mean absolute error: 0.24839771349385087
Coverage of cases: 75.3623188405797
Instances selection time: 15.0
Test time: 24.0
Accumulative iteration time: 186.0
Weighted Recall: 0.7536231884057971
Weighted FalsePositiveRate: 0.2753815347714733
Kappa statistic: 0.48963608834125205
Training time: 0.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7775692048155817
Weighted AreaUnderROC: 0.768239613789352
Root mean squared error: 0.4706961602384027
Relative absolute error: 45.04625915824369
Root relative squared error: 94.13923204768054
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5464537658465068
Weighted FMeasure: 0.7747624569787607
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7121223699169509
Mean absolute error: 0.22523129579121848
Coverage of cases: 77.68115942028986
Instances selection time: 12.0
Test time: 26.0
Accumulative iteration time: 199.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.24033236662419452
Kappa statistic: 0.5429677419354839
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.779410898172644
Weighted AreaUnderROC: 0.7733732236350037
Root mean squared error: 0.46774741525146796
Relative absolute error: 44.442452313362296
Root relative squared error: 93.54948305029359
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5522946325711234
Weighted FMeasure: 0.7785994778711618
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7162456091523829
Mean absolute error: 0.22221226156681148
Coverage of cases: 77.97101449275362
Instances selection time: 10.0
Test time: 27.0
Accumulative iteration time: 210.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.23296369765752878
Kappa statistic: 0.5509043573581803
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.7879965017491255
Weighted AreaUnderROC: 0.7831134833752634
Root mean squared error: 0.45852350811175635
Relative absolute error: 42.68978051167333
Root relative squared error: 91.70470162235127
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5702543331928074
Weighted FMeasure: 0.7876564963632683
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7260419138256959
Mean absolute error: 0.21344890255836663
Coverage of cases: 78.84057971014492
Instances selection time: 6.0
Test time: 29.0
Accumulative iteration time: 217.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.22217883035092229
Kappa statistic: 0.569450380374391
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7822836550592048
Weighted AreaUnderROC: 0.7791357856802883
Root mean squared error: 0.46485146988992754
Relative absolute error: 43.81978195192435
Root relative squared error: 92.9702939779855
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5594138551316972
Weighted FMeasure: 0.7823865371976563
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7210496176153718
Mean absolute error: 0.21909890975962173
Coverage of cases: 78.26086956521739
Instances selection time: 4.0
Test time: 30.0
Accumulative iteration time: 222.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2243371242915973
Kappa statistic: 0.5593269410903147
Training time: 1.0
		
Time end:Tue Oct 31 11.25.21 EET 2017