Tue Oct 31 11.25.09 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.09 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8447248833210513
Weighted AreaUnderROC: 0.8434946895424836
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6828181846106068
Weighted FMeasure: 0.8410642334785802
Iteration time: 46.0
Weighted AreaUnderPRC: 0.7910126949544142
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15359033105996023
Kappa statistic: 0.6802163911218969
Training time: 36.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8431199943755383
Weighted AreaUnderROC: 0.8421670751633986
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6807745808384493
Weighted FMeasure: 0.8409949759941283
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7896696121218698
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 76.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.15624555981813015
Kappa statistic: 0.6793734475591827
Training time: 23.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 37.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 30.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 141.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 24.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 200.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 57.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 52.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 252.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 47.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8405972771190162
Weighted AreaUnderROC: 0.839562908496732
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6754113361729748
Weighted FMeasure: 0.8381257332380675
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7865890155493181
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 297.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1585553424268258
Kappa statistic: 0.673758865248227
Training time: 40.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.652173913043478
Correctly Classified Instances: 84.34782608695652
Weighted Precision: 0.8471804802705287
Weighted AreaUnderROC: 0.8460988562091505
Root mean squared error: 0.39562828403747224
Relative absolute error: 31.30434782608696
Root relative squared error: 79.12565680749445
Weighted TruePositiveRate: 0.8434782608695652
Weighted MatthewsCorrelation: 0.6881106075177796
Weighted FMeasure: 0.8439409518213865
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7940888436164777
Mean absolute error: 0.1565217391304348
Coverage of cases: 84.34782608695652
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 354.0
Weighted Recall: 0.8434782608695652
Weighted FalsePositiveRate: 0.15128054845126457
Kappa statistic: 0.6858241424668623
Training time: 56.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8204682274247492
Weighted AreaUnderROC: 0.8073937908496731
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6302756226308095
Weighted FMeasure: 0.8151305148078847
Iteration time: 34.0
Weighted AreaUnderPRC: 0.756164994425864
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 388.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.2026037226484797
Kappa statistic: 0.6243194192377497
Training time: 33.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.52173913043478
Correctly Classified Instances: 83.47826086956522
Weighted Precision: 0.8349584764961584
Weighted AreaUnderROC: 0.8289930555555556
Root mean squared error: 0.4064694223485302
Relative absolute error: 33.04347826086956
Root relative squared error: 81.29388446970603
Weighted TruePositiveRate: 0.8347826086956521
Weighted MatthewsCorrelation: 0.6642336378092137
Weighted FMeasure: 0.834002779772954
Iteration time: 51.0
Weighted AreaUnderPRC: 0.7776759254000852
Mean absolute error: 0.16521739130434782
Coverage of cases: 83.47826086956522
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 439.0
Weighted Recall: 0.8347826086956521
Weighted FalsePositiveRate: 0.17679649758454105
Kappa statistic: 0.6628259863175762
Training time: 48.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.36231884057971
Correctly Classified Instances: 84.6376811594203
Weighted Precision: 0.8514534161490683
Weighted AreaUnderROC: 0.8500306372549019
Root mean squared error: 0.39194794093833063
Relative absolute error: 30.72463768115942
Root relative squared error: 78.38958818766612
Weighted TruePositiveRate: 0.8463768115942029
Weighted MatthewsCorrelation: 0.6956469719546821
Weighted FMeasure: 0.8468614968293855
Iteration time: 91.0
Weighted AreaUnderPRC: 0.7986034217089483
Mean absolute error: 0.1536231884057971
Coverage of cases: 84.6376811594203
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 530.0
Weighted Recall: 0.8463768115942029
Weighted FalsePositiveRate: 0.14631553708439898
Kappa statistic: 0.6922494319616258
Training time: 90.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.782608695652174
Correctly Classified Instances: 85.21739130434783
Weighted Precision: 0.8606663821128704
Weighted AreaUnderROC: 0.8578941993464053
Root mean squared error: 0.3844815820771156
Relative absolute error: 29.565217391304348
Root relative squared error: 76.89631641542312
Weighted TruePositiveRate: 0.8521739130434782
Weighted MatthewsCorrelation: 0.711346663322964
Weighted FMeasure: 0.8526319450215195
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8079367316073001
Mean absolute error: 0.14782608695652175
Coverage of cases: 85.21739130434783
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 638.0
Weighted Recall: 0.8521739130434782
Weighted FalsePositiveRate: 0.13638551435066779
Kappa statistic: 0.7050243926972789
Training time: 104.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8642313225953611
Weighted AreaUnderROC: 0.8611621732026145
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7179386085551988
Weighted FMeasure: 0.8555116381203337
Iteration time: 107.0
Weighted AreaUnderPRC: 0.8118751808606347
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 745.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.1327481173628872
Kappa statistic: 0.7109971853638923
Training time: 103.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 14.492753623188406
Correctly Classified Instances: 85.5072463768116
Weighted Precision: 0.8642313225953611
Weighted AreaUnderROC: 0.8611621732026145
Root mean squared error: 0.38069349381344053
Relative absolute error: 28.985507246376812
Root relative squared error: 76.13869876268811
Weighted TruePositiveRate: 0.855072463768116
Weighted MatthewsCorrelation: 0.7179386085551988
Weighted FMeasure: 0.8555116381203337
Iteration time: 122.0
Weighted AreaUnderPRC: 0.8118751808606347
Mean absolute error: 0.14492753623188406
Coverage of cases: 85.5072463768116
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 867.0
Weighted Recall: 0.855072463768116
Weighted FalsePositiveRate: 0.1327481173628872
Kappa statistic: 0.7109971853638923
Training time: 121.0
		
Time end:Tue Oct 31 11.25.11 EET 2017