Tue Oct 31 11.25.01 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.01 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 26.3768115942029
Correctly Classified Instances: 73.6231884057971
Weighted Precision: 0.7397089553184788
Weighted AreaUnderROC: 0.8392113387788326
Root mean squared error: 0.48363513432760685
Relative absolute error: 52.10843239708255
Root relative squared error: 96.72702686552137
Weighted TruePositiveRate: 0.736231884057971
Weighted MatthewsCorrelation: 0.47200521578017723
Weighted FMeasure: 0.7369266611533785
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8307404490107112
Mean absolute error: 0.26054216198541275
Coverage of cases: 83.18840579710145
Instances selection time: 12.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.736231884057971
Weighted FalsePositiveRate: 0.2617299462052478
Kappa statistic: 0.4706536950547135
Training time: 3.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 57.391304347826086
Incorrectly Classified Instances: 24.347826086956523
Correctly Classified Instances: 75.65217391304348
Weighted Precision: 0.7559280721812134
Weighted AreaUnderROC: 0.8465819786102053
Root mean squared error: 0.46207409729575777
Relative absolute error: 47.96016356296777
Root relative squared error: 92.41481945915156
Weighted TruePositiveRate: 0.7565217391304347
Weighted MatthewsCorrelation: 0.5049021072043686
Weighted FMeasure: 0.7552941597523368
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8350978654530989
Mean absolute error: 0.23980081781483886
Coverage of cases: 84.34782608695652
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7565217391304347
Weighted FalsePositiveRate: 0.2566917262114166
Kappa statistic: 0.5036311318169361
Training time: 1.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 59.42028985507246
Incorrectly Classified Instances: 22.028985507246375
Correctly Classified Instances: 77.97101449275362
Weighted Precision: 0.7835219376613064
Weighted AreaUnderROC: 0.8646457419960721
Root mean squared error: 0.43680083298590994
Relative absolute error: 44.29742900785571
Root relative squared error: 87.36016659718199
Weighted TruePositiveRate: 0.7797101449275362
Weighted MatthewsCorrelation: 0.5540994568509788
Weighted FMeasure: 0.7762177447861436
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8549648893075469
Mean absolute error: 0.22148714503927855
Coverage of cases: 87.53623188405797
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7797101449275362
Weighted FalsePositiveRate: 0.24302693285165397
Kappa statistic: 0.5463039867109634
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 59.130434782608695
Incorrectly Classified Instances: 22.608695652173914
Correctly Classified Instances: 77.3913043478261
Weighted Precision: 0.7856969583877588
Weighted AreaUnderROC: 0.8683280070714624
Root mean squared error: 0.438318619361027
Relative absolute error: 45.06315626681466
Root relative squared error: 87.6637238722054
Weighted TruePositiveRate: 0.7739130434782608
Weighted MatthewsCorrelation: 0.5479803320577225
Weighted FMeasure: 0.7672617762618195
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8633139835650524
Mean absolute error: 0.2253157813340733
Coverage of cases: 86.66666666666667
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7739130434782608
Weighted FalsePositiveRate: 0.25776427078498554
Kappa statistic: 0.5301532981806752
Training time: 1.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 61.15942028985507
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7861065044494472
Weighted AreaUnderROC: 0.870945808118583
Root mean squared error: 0.4491939591644478
Relative absolute error: 47.95982817523062
Root relative squared error: 89.83879183288957
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5371130223558437
Weighted FMeasure: 0.7551212628979627
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8630281886679425
Mean absolute error: 0.2397991408761531
Coverage of cases: 86.3768115942029
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2735807556886546
Kappa statistic: 0.5085902192835915
Training time: 5.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7840055259219463
Weighted AreaUnderROC: 0.8677500509961245
Root mean squared error: 0.454401729813625
Relative absolute error: 48.35692392768859
Root relative squared error: 90.880345962725
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5354325336771291
Weighted FMeasure: 0.7557485300627516
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8603902865050804
Mean absolute error: 0.24178461963844297
Coverage of cases: 85.21739130434783
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27232285128938893
Kappa statistic: 0.5092288509158603
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 59.27536231884058
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7883508110443422
Weighted AreaUnderROC: 0.8685659889848373
Root mean squared error: 0.45447337258182363
Relative absolute error: 48.026130355973784
Root relative squared error: 90.89467451636473
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5389190292518433
Weighted FMeasure: 0.7544721918217093
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8583380334158877
Mean absolute error: 0.24013065177986892
Coverage of cases: 84.05797101449275
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2748386600879203
Kappa statistic: 0.5079499234060536
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 59.85507246376812
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.790745868336201
Weighted AreaUnderROC: 0.8701978649622628
Root mean squared error: 0.45331820681346674
Relative absolute error: 47.92096492654247
Root relative squared error: 90.66364136269335
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.540855800852102
Weighted FMeasure: 0.75380095937928
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8651267163157306
Mean absolute error: 0.23960482463271232
Coverage of cases: 84.92753623188406
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2760965644871859
Kappa statistic: 0.5073079567693366
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 60.289855072463766
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7840055259219463
Weighted AreaUnderROC: 0.8827769089549193
Root mean squared error: 0.45076886665057797
Relative absolute error: 47.61236876569094
Root relative squared error: 90.1537733301156
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5354325336771291
Weighted FMeasure: 0.7557485300627516
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8785740388933737
Mean absolute error: 0.2380618438284547
Coverage of cases: 86.08695652173913
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27232285128938893
Kappa statistic: 0.5092288509158603
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.7951570048309178
Weighted AreaUnderROC: 0.8831168831168831
Root mean squared error: 0.44536954751475405
Relative absolute error: 47.00642334082378
Root relative squared error: 89.07390950295081
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5592165126708873
Weighted FMeasure: 0.7683862213873311
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8788991407532205
Mean absolute error: 0.23503211670411892
Coverage of cases: 86.66666666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.2592009326131794
Kappa statistic: 0.5340699815837938
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 61.01449275362319
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7927950243349352
Weighted AreaUnderROC: 0.8849867410076834
Root mean squared error: 0.44854359951226325
Relative absolute error: 47.64037288918857
Root relative squared error: 89.70871990245264
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5499270734389475
Weighted FMeasure: 0.7611676514683835
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8806995076203883
Mean absolute error: 0.23820186444594285
Coverage of cases: 86.66666666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2676487485501826
Kappa statistic: 0.5207237941160955
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 61.15942028985507
Incorrectly Classified Instances: 23.18840579710145
Correctly Classified Instances: 76.81159420289855
Weighted Precision: 0.7863012716431442
Weighted AreaUnderROC: 0.8865846195689128
Root mean squared error: 0.4447199488045335
Relative absolute error: 47.30499401257994
Root relative squared error: 88.9439897609067
Weighted TruePositiveRate: 0.7681159420289855
Weighted MatthewsCorrelation: 0.5409909447750052
Weighted FMeasure: 0.7590657728844449
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8815228712047444
Mean absolute error: 0.23652497006289971
Coverage of cases: 87.53623188405797
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7681159420289855
Weighted FalsePositiveRate: 0.2687278955205201
Kappa statistic: 0.5156025132507283
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 61.30434782608695
Incorrectly Classified Instances: 23.768115942028984
Correctly Classified Instances: 76.23188405797102
Weighted Precision: 0.777844290138818
Weighted AreaUnderROC: 0.8871285782280547
Root mean squared error: 0.444864577447529
Relative absolute error: 47.406151933647195
Root relative squared error: 88.9729154895058
Weighted TruePositiveRate: 0.7623188405797101
Weighted MatthewsCorrelation: 0.5268174588350663
Weighted FMeasure: 0.7536449664255529
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8823312046558036
Mean absolute error: 0.23703075966823597
Coverage of cases: 88.1159420289855
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7623188405797101
Weighted FalsePositiveRate: 0.27340199825972644
Kappa statistic: 0.5041365771576806
Training time: 1.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 61.44927536231884
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.778497157112627
Weighted AreaUnderROC: 0.8914462500849935
Root mean squared error: 0.4344210823978893
Relative absolute error: 46.07430298105942
Root relative squared error: 86.88421647957786
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310974235492204
Weighted FMeasure: 0.7575028756180497
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8872155708265477
Mean absolute error: 0.2303715149052971
Coverage of cases: 88.40579710144928
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.268549138091592
Kappa statistic: 0.5111348249741967
Training time: 1.0
		
Time end:Tue Oct 31 11.25.01 EET 2017