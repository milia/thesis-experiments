Tue Oct 31 11.24.59 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.24.59 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 57.53623188405797
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.7930977602108037
Weighted AreaUnderROC: 0.8707257610740424
Root mean squared error: 0.4300511080065533
Relative absolute error: 41.63788912451236
Root relative squared error: 86.01022160131066
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.572590808109371
Weighted FMeasure: 0.7848739680600533
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8533858902290902
Mean absolute error: 0.2081894456225618
Coverage of cases: 86.08695652173913
Instances selection time: 10.0
Test time: 9.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.23475787434357887
Kappa statistic: 0.5639338585403861
Training time: 4.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 57.10144927536232
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.797777191026619
Weighted AreaUnderROC: 0.8772198588269611
Root mean squared error: 0.4269698309130391
Relative absolute error: 41.23566770409736
Root relative squared error: 85.39396618260781
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5760834523867254
Weighted FMeasure: 0.783303639200136
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8584796668825947
Mean absolute error: 0.20617833852048678
Coverage of cases: 86.08695652173913
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2397894919406415
Kappa statistic: 0.56168746410484
Training time: 0.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 56.52173913043478
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7832939963374745
Weighted AreaUnderROC: 0.8895407195429959
Root mean squared error: 0.43963854420099224
Relative absolute error: 43.500342450385475
Root relative squared error: 87.92770884019845
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5423556165748107
Weighted FMeasure: 0.7640203960173078
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8797719412046108
Mean absolute error: 0.21750171225192738
Coverage of cases: 86.3768115942029
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.2613592265538544
Kappa statistic: 0.5238220032496462
Training time: 1.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 55.94202898550725
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.778497157112627
Weighted AreaUnderROC: 0.8901714947924828
Root mean squared error: 0.4507835800442811
Relative absolute error: 44.762231355119994
Root relative squared error: 90.15671600885622
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5310974235492204
Weighted FMeasure: 0.7575028756180497
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8805385536448838
Mean absolute error: 0.22381115677559996
Coverage of cases: 85.5072463768116
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.268549138091592
Kappa statistic: 0.5111348249741967
Training time: 2.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 55.507246376811594
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.7769066725588466
Weighted AreaUnderROC: 0.8951393549162727
Root mean squared error: 0.45377004078286276
Relative absolute error: 45.085747714128935
Root relative squared error: 90.75400815657255
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5298739483926808
Weighted FMeasure: 0.7580462288278724
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8880493128073775
Mean absolute error: 0.2254287385706447
Coverage of cases: 84.34782608695652
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.2672912336923263
Kappa statistic: 0.5117668640914094
Training time: 0.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 60.289855072463766
Incorrectly Classified Instances: 25.507246376811594
Correctly Classified Instances: 74.4927536231884
Weighted Precision: 0.784602143372758
Weighted AreaUnderROC: 0.9000135989664787
Root mean squared error: 0.46826657376246716
Relative absolute error: 50.59359228639961
Root relative squared error: 93.65331475249343
Weighted TruePositiveRate: 0.744927536231884
Weighted MatthewsCorrelation: 0.5095229755656677
Weighted FMeasure: 0.7271680661491222
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8977037105446211
Mean absolute error: 0.25296796143199807
Coverage of cases: 85.21739130434783
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 38.0
Weighted Recall: 0.744927536231884
Weighted FalsePositiveRate: 0.3050349680670646
Kappa statistic: 0.46015149898645036
Training time: 1.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 59.710144927536234
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7771669720695208
Weighted AreaUnderROC: 0.9006935472904058
Root mean squared error: 0.46868944312792804
Relative absolute error: 50.568095139025495
Root relative squared error: 93.73788862558561
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4959372713713594
Weighted FMeasure: 0.720967340379784
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8966571080780763
Mean absolute error: 0.25284047569512746
Coverage of cases: 85.21739130434783
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.3109669752055365
Kappa statistic: 0.447882214872506
Training time: 1.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 58.84057971014493
Incorrectly Classified Instances: 24.92753623188406
Correctly Classified Instances: 75.07246376811594
Weighted Precision: 0.7886954699897066
Weighted AreaUnderROC: 0.9016114775277079
Root mean squared error: 0.4693482081230699
Relative absolute error: 50.27477672660372
Root relative squared error: 93.86964162461398
Weighted TruePositiveRate: 0.7507246376811594
Weighted MatthewsCorrelation: 0.5204280266305392
Weighted FMeasure: 0.7342447665056361
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8958462067269334
Mean absolute error: 0.2513738836330186
Coverage of cases: 84.05797101449275
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7507246376811594
Weighted FalsePositiveRate: 0.29784505652932697
Kappa statistic: 0.4731140787043614
Training time: 1.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 58.55072463768116
Incorrectly Classified Instances: 25.217391304347824
Correctly Classified Instances: 74.78260869565217
Weighted Precision: 0.7834636945093346
Weighted AreaUnderROC: 0.9021894336030462
Root mean squared error: 0.47438155289163886
Relative absolute error: 50.80751192720213
Root relative squared error: 94.87631057832778
Weighted TruePositiveRate: 0.7478260869565218
Weighted MatthewsCorrelation: 0.5124368020420779
Weighted FMeasure: 0.7315876241106682
Iteration time: 3.0
Weighted AreaUnderPRC: 0.896444504593871
Mean absolute error: 0.25403755963601066
Coverage of cases: 83.76811594202898
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7478260869565218
Weighted FalsePositiveRate: 0.30018210789893013
Kappa statistic: 0.4673374860245967
Training time: 1.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 60.57971014492754
Incorrectly Classified Instances: 23.47826086956522
Correctly Classified Instances: 76.52173913043478
Weighted Precision: 0.793299771167048
Weighted AreaUnderROC: 0.900606730700061
Root mean squared error: 0.4536893499663155
Relative absolute error: 47.88758955450892
Root relative squared error: 90.7378699932631
Weighted TruePositiveRate: 0.7652173913043478
Weighted MatthewsCorrelation: 0.5429290129695375
Weighted FMeasure: 0.7531071936370147
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8932204299119232
Mean absolute error: 0.2394379477725446
Coverage of cases: 86.66666666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7652173913043478
Weighted FalsePositiveRate: 0.27735446888645154
Kappa statistic: 0.5066643128254922
Training time: 1.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 60.14492753623188
Incorrectly Classified Instances: 22.89855072463768
Correctly Classified Instances: 77.10144927536231
Weighted Precision: 0.7951101684929893
Weighted AreaUnderROC: 0.9058195693069355
Root mean squared error: 0.4448834749258246
Relative absolute error: 46.666841561618156
Root relative squared error: 88.97669498516491
Weighted TruePositiveRate: 0.7710144927536232
Weighted MatthewsCorrelation: 0.5518091474639819
Weighted FMeasure: 0.7605346068384572
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8988924196528957
Mean absolute error: 0.23333420780809078
Coverage of cases: 87.82608695652173
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7710144927536232
Weighted FalsePositiveRate: 0.26890665294944827
Kappa statistic: 0.5200993080133116
Training time: 1.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 60.43478260869565
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8061720641059116
Weighted AreaUnderROC: 0.9042481003327804
Root mean squared error: 0.4292255032427826
Relative absolute error: 43.79550168051081
Root relative squared error: 85.84510064855651
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5755551756051555
Weighted FMeasure: 0.773260428609225
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8982752378749839
Mean absolute error: 0.21897750840255403
Coverage of cases: 88.40579710144928
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 55.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2557847342732387
Kappa statistic: 0.5449909437811034
Training time: 1.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 60.869565217391305
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.801978570021131
Weighted AreaUnderROC: 0.9016114775277079
Root mean squared error: 0.42562741257775744
Relative absolute error: 43.47969549123885
Root relative squared error: 85.12548251555148
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5757567188378312
Weighted FMeasure: 0.7782042095881503
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8967321795073494
Mean absolute error: 0.21739847745619426
Coverage of cases: 88.98550724637681
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.24841606530657293
Kappa statistic: 0.5530931624829324
Training time: 2.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 61.15942028985507
Incorrectly Classified Instances: 20.579710144927535
Correctly Classified Instances: 79.42028985507247
Weighted Precision: 0.8088432914519871
Weighted AreaUnderROC: 0.8999116067178895
Root mean squared error: 0.4168406210979395
Relative absolute error: 42.61436882849862
Root relative squared error: 83.3681242195879
Weighted TruePositiveRate: 0.7942028985507247
Weighted MatthewsCorrelation: 0.5922822893033304
Weighted FMeasure: 0.7879170647750486
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8921012035072089
Mean absolute error: 0.21307184414249308
Coverage of cases: 88.69565217391305
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7942028985507247
Weighted FalsePositiveRate: 0.2376311979999665
Kappa statistic: 0.5720425598825934
Training time: 1.0
		
Time end:Tue Oct 31 11.25.00 EET 2017