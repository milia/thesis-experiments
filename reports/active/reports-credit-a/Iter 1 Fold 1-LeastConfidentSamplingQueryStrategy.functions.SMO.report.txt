Tue Oct 31 11.25.48 EET 2017
Dataset: credit-rating
Test set size: 345
Initial Labelled set size: 69
Initial Unlabelled set size: 276
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.25.48 EET 2017
		
Iteration: 1
Labeled set size: 69
Unlabelled set size: 276
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.26086956521739
Correctly Classified Instances: 81.73913043478261
Weighted Precision: 0.8171722907203115
Weighted AreaUnderROC: 0.8120404411764705
Root mean squared error: 0.42732738696715183
Relative absolute error: 36.52173913043478
Root relative squared error: 85.46547739343036
Weighted TruePositiveRate: 0.8173913043478261
Weighted MatthewsCorrelation: 0.6287209528004792
Weighted FMeasure: 0.8167154451920468
Iteration time: 39.0
Weighted AreaUnderPRC: 0.7579755522952346
Mean absolute error: 0.1826086956521739
Coverage of cases: 81.73913043478261
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8173913043478261
Weighted FalsePositiveRate: 0.1933104219948849
Kappa statistic: 0.6278317152103561
Training time: 33.0
		
Iteration: 2
Labeled set size: 89
Unlabelled set size: 256
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8168776371308016
Weighted AreaUnderROC: 0.7824754901960784
Root mean squared error: 0.4472135954999579
Relative absolute error: 40.0
Root relative squared error: 89.44271909999159
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.6052308343219491
Weighted FMeasure: 0.7932481311791657
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7362281618317607
Mean absolute error: 0.2
Coverage of cases: 80.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 60.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.23504901960784313
Kappa statistic: 0.5823464392862783
Training time: 16.0
		
Iteration: 3
Labeled set size: 109
Unlabelled set size: 236
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.44927536231884
Correctly Classified Instances: 78.55072463768116
Weighted Precision: 0.8129899799019679
Weighted AreaUnderROC: 0.7641441993464053
Root mean squared error: 0.4631336239393426
Relative absolute error: 42.89855072463768
Root relative squared error: 92.62672478786853
Weighted TruePositiveRate: 0.7855072463768116
Weighted MatthewsCorrelation: 0.5837887153656698
Weighted FMeasure: 0.7751335968379446
Iteration time: 19.0
Weighted AreaUnderPRC: 0.720887317326574
Mean absolute error: 0.2144927536231884
Coverage of cases: 78.55072463768116
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 79.0
Weighted Recall: 0.7855072463768116
Weighted FalsePositiveRate: 0.2572188476840011
Kappa statistic: 0.5486847687738651
Training time: 15.0
		
Iteration: 4
Labeled set size: 129
Unlabelled set size: 216
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8082940269929481
Weighted AreaUnderROC: 0.7615400326797387
Root mean squared error: 0.4662524041201569
Relative absolute error: 43.47826086956522
Root relative squared error: 93.25048082403138
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.576239446504013
Weighted FMeasure: 0.7724088295722725
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7174405091854671
Mean absolute error: 0.21739130434782608
Coverage of cases: 78.26086956521739
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 100.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2595286302926968
Kappa statistic: 0.5429010546398856
Training time: 15.0
		
Iteration: 5
Labeled set size: 149
Unlabelled set size: 196
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8125465838509317
Weighted AreaUnderROC: 0.768075980392157
Root mean squared error: 0.4599936987596142
Relative absolute error: 42.31884057971014
Root relative squared error: 91.99873975192284
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5870852728923276
Weighted FMeasure: 0.7790739953509828
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7237807183364839
Mean absolute error: 0.21159420289855072
Coverage of cases: 78.84057971014492
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 125.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.25225383631713555
Kappa statistic: 0.5557025668166182
Training time: 19.0
		
Iteration: 6
Labeled set size: 169
Unlabelled set size: 176
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.159420289855074
Correctly Classified Instances: 78.84057971014492
Weighted Precision: 0.8150724398190891
Weighted AreaUnderROC: 0.7674121732026145
Root mean squared error: 0.4599936987596142
Relative absolute error: 42.31884057971014
Root relative squared error: 91.99873975192284
Weighted TruePositiveRate: 0.7884057971014493
Weighted MatthewsCorrelation: 0.5891772708593355
Weighted FMeasure: 0.7784779274503453
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7240603794535884
Mean absolute error: 0.21159420289855072
Coverage of cases: 78.84057971014492
Instances selection time: 7.0
Test time: 3.0
Accumulative iteration time: 150.0
Weighted Recall: 0.7884057971014493
Weighted FalsePositiveRate: 0.2535814506962205
Kappa statistic: 0.5550903598494886
Training time: 18.0
		
Iteration: 7
Labeled set size: 189
Unlabelled set size: 156
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.71014492753623
Correctly Classified Instances: 80.28985507246377
Weighted Precision: 0.8255685524805598
Weighted AreaUnderROC: 0.7837520424836601
Root mean squared error: 0.44396108982135174
Relative absolute error: 39.42028985507247
Root relative squared error: 88.79221796427035
Weighted TruePositiveRate: 0.8028985507246377
Weighted MatthewsCorrelation: 0.6160827180252161
Weighted FMeasure: 0.7950041324383891
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7401293284960282
Mean absolute error: 0.19710144927536233
Coverage of cases: 80.28985507246377
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 179.0
Weighted Recall: 0.8028985507246377
Weighted FalsePositiveRate: 0.23539446575731743
Kappa statistic: 0.5869863737192352
Training time: 23.0
		
Iteration: 8
Labeled set size: 209
Unlabelled set size: 136
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.318840579710145
Correctly Classified Instances: 77.68115942028986
Weighted Precision: 0.8096580168413815
Weighted AreaUnderROC: 0.7536764705882353
Root mean squared error: 0.4724282017376836
Relative absolute error: 44.63768115942029
Root relative squared error: 94.48564034753672
Weighted TruePositiveRate: 0.7768115942028986
Weighted MatthewsCorrelation: 0.5699704467430113
Weighted FMeasure: 0.7643240979831112
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7117855301598213
Mean absolute error: 0.22318840579710145
Coverage of cases: 77.68115942028986
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 195.0
Weighted Recall: 0.7768115942028986
Weighted FalsePositiveRate: 0.26945865302642796
Kappa statistic: 0.528763769889841
Training time: 15.0
		
Iteration: 9
Labeled set size: 229
Unlabelled set size: 116
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.28985507246377
Correctly Classified Instances: 79.71014492753623
Weighted Precision: 0.8239017963302163
Weighted AreaUnderROC: 0.7765522875816994
Root mean squared error: 0.45044261646145084
Relative absolute error: 40.57971014492754
Root relative squared error: 90.08852329229016
Weighted TruePositiveRate: 0.7971014492753623
Weighted MatthewsCorrelation: 0.6074610406243708
Weighted FMeasure: 0.7878697637482628
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7339719043292284
Mean absolute error: 0.2028985507246377
Coverage of cases: 79.71014492753623
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 211.0
Weighted Recall: 0.7971014492753623
Weighted FalsePositiveRate: 0.24399687411196364
Kappa statistic: 0.5736680436394449
Training time: 15.0
		
Iteration: 10
Labeled set size: 249
Unlabelled set size: 96
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.10144927536232
Correctly Classified Instances: 82.89855072463769
Weighted Precision: 0.8430483322931834
Weighted AreaUnderROC: 0.8138276143790849
Root mean squared error: 0.41353898577234915
Relative absolute error: 34.20289855072463
Root relative squared error: 82.70779715446983
Weighted TruePositiveRate: 0.8289855072463768
Weighted MatthewsCorrelation: 0.6629168610262626
Weighted FMeasure: 0.8243793315709663
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7697587957979957
Mean absolute error: 0.17101449275362318
Coverage of cases: 82.89855072463769
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 231.0
Weighted Recall: 0.8289855072463768
Weighted FalsePositiveRate: 0.20133027848820687
Kappa statistic: 0.6443361115479372
Training time: 19.0
		
Iteration: 11
Labeled set size: 269
Unlabelled set size: 76
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.942028985507246
Correctly Classified Instances: 84.05797101449275
Weighted Precision: 0.8505169082125602
Weighted AreaUnderROC: 0.827563316993464
Root mean squared error: 0.3992747047523452
Relative absolute error: 31.88405797101449
Root relative squared error: 79.85494095046904
Weighted TruePositiveRate: 0.8405797101449275
Weighted MatthewsCorrelation: 0.6833445004799835
Weighted FMeasure: 0.8372524149815361
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7833262619897781
Mean absolute error: 0.15942028985507245
Coverage of cases: 84.05797101449275
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 253.0
Weighted Recall: 0.8405797101449275
Weighted FalsePositiveRate: 0.1854530761579994
Kappa statistic: 0.6697990080918819
Training time: 21.0
		
Iteration: 12
Labeled set size: 289
Unlabelled set size: 56
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.231884057971016
Correctly Classified Instances: 83.76811594202898
Weighted Precision: 0.8467019146861045
Weighted AreaUnderROC: 0.8249591503267973
Root mean squared error: 0.4028881241482679
Relative absolute error: 32.463768115942024
Root relative squared error: 80.57762482965359
Weighted TruePositiveRate: 0.8376811594202899
Weighted MatthewsCorrelation: 0.6766099835267646
Weighted FMeasure: 0.8344457193076679
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7797804177835111
Mean absolute error: 0.16231884057971013
Coverage of cases: 83.76811594202898
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 273.0
Weighted Recall: 0.8376811594202899
Weighted FalsePositiveRate: 0.1877628587666951
Kappa statistic: 0.6640233722871453
Training time: 19.0
		
Iteration: 13
Labeled set size: 309
Unlabelled set size: 36
	
Mean region size: 50.0
Incorrectly Classified Instances: 15.072463768115941
Correctly Classified Instances: 84.92753623188406
Weighted Precision: 0.850823656494734
Weighted AreaUnderROC: 0.8499795751633986
Root mean squared error: 0.38823271073050947
Relative absolute error: 30.144927536231886
Root relative squared error: 77.6465421461019
Weighted TruePositiveRate: 0.8492753623188406
Weighted MatthewsCorrelation: 0.6970231068512678
Weighted FMeasure: 0.8495919678052287
Iteration time: 29.0
Weighted AreaUnderPRC: 0.799051219731749
Mean absolute error: 0.15072463768115943
Coverage of cases: 84.92753623188406
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 302.0
Weighted Recall: 0.8492753623188406
Weighted FalsePositiveRate: 0.1493162119920432
Kappa statistic: 0.6962616822429907
Training time: 28.0
		
Iteration: 14
Labeled set size: 329
Unlabelled set size: 16
	
Mean region size: 50.0
Incorrectly Classified Instances: 13.333333333333334
Correctly Classified Instances: 86.66666666666667
Weighted Precision: 0.8788033957687641
Weighted AreaUnderROC: 0.874234068627451
Root mean squared error: 0.3651483716701107
Relative absolute error: 26.666666666666668
Root relative squared error: 73.02967433402215
Weighted TruePositiveRate: 0.8666666666666667
Weighted MatthewsCorrelation: 0.7445749743269653
Weighted FMeasure: 0.8670120433290722
Iteration time: 40.0
Weighted AreaUnderPRC: 0.8279421653515326
Mean absolute error: 0.13333333333333333
Coverage of cases: 86.66666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 342.0
Weighted Recall: 0.8666666666666667
Weighted FalsePositiveRate: 0.11819852941176472
Kappa statistic: 0.7348105073190296
Training time: 40.0
		
Time end:Tue Oct 31 11.25.49 EET 2017