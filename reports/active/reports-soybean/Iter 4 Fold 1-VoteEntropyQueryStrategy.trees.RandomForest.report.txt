Wed Nov 01 09.42.00 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.42.00 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 31.22499230532467
Incorrectly Classified Instances: 34.7953216374269
Correctly Classified Instances: 65.2046783625731
Weighted Precision: 0.657198191073999
Weighted AreaUnderROC: 0.9180643601612591
Root mean squared error: 0.16893898240506217
Relative absolute error: 63.65930221351771
Root relative squared error: 75.6566700414034
Weighted TruePositiveRate: 0.652046783625731
Weighted MatthewsCorrelation: 0.6099020507907156
Weighted FMeasure: 0.6233783304349957
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7045342565143907
Mean absolute error: 0.0634829606561401
Coverage of cases: 91.52046783625731
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 12.0
Weighted Recall: 0.652046783625731
Weighted FalsePositiveRate: 0.037445131291799515
Kappa statistic: 0.6147956044788127
Training time: 9.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 25.746383502616172
Incorrectly Classified Instances: 33.04093567251462
Correctly Classified Instances: 66.95906432748538
Weighted Precision: 0.673147604973578
Weighted AreaUnderROC: 0.9230175531256627
Root mean squared error: 0.16405982508251643
Relative absolute error: 57.40760272546228
Root relative squared error: 73.47161606287962
Weighted TruePositiveRate: 0.6695906432748538
Weighted MatthewsCorrelation: 0.6334013063010819
Weighted FMeasure: 0.6422843539487927
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7539260027914041
Mean absolute error: 0.05724857889519835
Coverage of cases: 90.05847953216374
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 21.0
Weighted Recall: 0.6695906432748538
Weighted FalsePositiveRate: 0.035698729464635
Kappa statistic: 0.6334509446847257
Training time: 7.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 23.853493382579266
Incorrectly Classified Instances: 34.50292397660819
Correctly Classified Instances: 65.49707602339181
Weighted Precision: 0.6439823189212969
Weighted AreaUnderROC: 0.9187104835255033
Root mean squared error: 0.16713044618984185
Relative absolute error: 56.3886689024215
Root relative squared error: 74.8467455009277
Weighted TruePositiveRate: 0.6549707602339181
Weighted MatthewsCorrelation: 0.6086129940657645
Weighted FMeasure: 0.6206893773445057
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7284340377143762
Mean absolute error: 0.05623246760352332
Coverage of cases: 88.01169590643275
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6549707602339181
Weighted FalsePositiveRate: 0.04187680515080323
Kappa statistic: 0.6155839207468089
Training time: 8.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 21.883656509695285
Incorrectly Classified Instances: 40.93567251461988
Correctly Classified Instances: 59.06432748538012
Weighted Precision: 0.6561548669453808
Weighted AreaUnderROC: 0.9250967803315417
Root mean squared error: 0.17201172520282232
Relative absolute error: 56.58509501375566
Root relative squared error: 77.03274964519099
Weighted TruePositiveRate: 0.5906432748538012
Weighted MatthewsCorrelation: 0.56557527235567
Weighted FMeasure: 0.5689318158601315
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7245573418543315
Mean absolute error: 0.056428349598205585
Coverage of cases: 89.76608187134502
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 43.0
Weighted Recall: 0.5906432748538012
Weighted FalsePositiveRate: 0.04816258377228921
Kappa statistic: 0.5448496140537663
Training time: 9.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 22.606955986457358
Incorrectly Classified Instances: 39.76608187134503
Correctly Classified Instances: 60.23391812865497
Weighted Precision: 0.6981657125598835
Weighted AreaUnderROC: 0.9087494602486299
Root mean squared error: 0.16874743599693062
Relative absolute error: 55.49263083275893
Root relative squared error: 75.57088899080563
Weighted TruePositiveRate: 0.6023391812865497
Weighted MatthewsCorrelation: 0.5805026625983944
Weighted FMeasure: 0.5746688482694436
Iteration time: 11.0
Weighted AreaUnderPRC: 0.719462077015083
Mean absolute error: 0.0553389116337767
Coverage of cases: 85.96491228070175
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 54.0
Weighted Recall: 0.6023391812865497
Weighted FalsePositiveRate: 0.04944728303143888
Kappa statistic: 0.5564836799496524
Training time: 9.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 21.17574638350259
Incorrectly Classified Instances: 30.116959064327485
Correctly Classified Instances: 69.88304093567251
Weighted Precision: 0.6880874449493578
Weighted AreaUnderROC: 0.9457565084001975
Root mean squared error: 0.15302122620547148
Relative absolute error: 49.58258686692282
Root relative squared error: 68.52815291973373
Weighted TruePositiveRate: 0.6988304093567251
Weighted MatthewsCorrelation: 0.6523587555625668
Weighted FMeasure: 0.6552092038985934
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8035831608154742
Mean absolute error: 0.0494452389808653
Coverage of cases: 92.10526315789474
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 66.0
Weighted Recall: 0.6988304093567251
Weighted FalsePositiveRate: 0.03607700364753968
Kappa statistic: 0.6642680823079782
Training time: 10.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 20.298553401046455
Incorrectly Classified Instances: 29.82456140350877
Correctly Classified Instances: 70.17543859649123
Weighted Precision: 0.693105926859858
Weighted AreaUnderROC: 0.9308440803736667
Root mean squared error: 0.1525018332350442
Relative absolute error: 48.22131965712116
Root relative squared error: 68.29555093512356
Weighted TruePositiveRate: 0.7017543859649122
Weighted MatthewsCorrelation: 0.6488019991936712
Weighted FMeasure: 0.647841117575085
Iteration time: 12.0
Weighted AreaUnderPRC: 0.779773473333968
Mean absolute error: 0.048087742594359474
Coverage of cases: 89.18128654970761
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 78.0
Weighted Recall: 0.7017543859649122
Weighted FalsePositiveRate: 0.036419322238211545
Kappa statistic: 0.6670706916462268
Training time: 10.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 20.19082794706062
Incorrectly Classified Instances: 24.853801169590643
Correctly Classified Instances: 75.14619883040936
Weighted Precision: 0.7262956638242679
Weighted AreaUnderROC: 0.9431908045006815
Root mean squared error: 0.14499196440627277
Relative absolute error: 45.7823450941859
Root relative squared error: 64.93237412451458
Weighted TruePositiveRate: 0.7514619883040936
Weighted MatthewsCorrelation: 0.701887184410786
Weighted FMeasure: 0.7036757518016914
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8328821621308711
Mean absolute error: 0.04565552419364839
Coverage of cases: 90.93567251461988
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 99.0
Weighted Recall: 0.7514619883040936
Weighted FalsePositiveRate: 0.03232241260893133
Kappa statistic: 0.7233062382212408
Training time: 19.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 18.436441982148345
Incorrectly Classified Instances: 28.362573099415204
Correctly Classified Instances: 71.6374269005848
Weighted Precision: 0.7307078061894023
Weighted AreaUnderROC: 0.9500215009497018
Root mean squared error: 0.1431466610141839
Relative absolute error: 42.975440219024485
Root relative squared error: 64.10598398131596
Weighted TruePositiveRate: 0.716374269005848
Weighted MatthewsCorrelation: 0.6703771867515069
Weighted FMeasure: 0.6671968270792981
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8542810514956974
Mean absolute error: 0.04285639467825194
Coverage of cases: 91.52046783625731
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 114.0
Weighted Recall: 0.716374269005848
Weighted FalsePositiveRate: 0.03699131739962487
Kappa statistic: 0.6838102136906918
Training time: 13.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 18.374884579870724
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.7887920598895066
Weighted AreaUnderROC: 0.9626898815845741
Root mean squared error: 0.13330866049438447
Relative absolute error: 40.098821835474595
Root relative squared error: 59.700189956766906
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7736610607786799
Weighted FMeasure: 0.7761674510471173
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8672792568618949
Mean absolute error: 0.039987744766678615
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 130.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.020933353080586116
Kappa statistic: 0.7931933710009638
Training time: 15.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 16.789781471221936
Incorrectly Classified Instances: 18.128654970760234
Correctly Classified Instances: 81.87134502923976
Weighted Precision: 0.8138257876082474
Weighted AreaUnderROC: 0.9695514958924438
Root mean squared error: 0.11952962319221189
Relative absolute error: 32.59776253715542
Root relative squared error: 53.52946450419414
Weighted TruePositiveRate: 0.8187134502923976
Weighted MatthewsCorrelation: 0.789761906889384
Weighted FMeasure: 0.7949550194321422
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8793847201989733
Mean absolute error: 0.03250746402597245
Coverage of cases: 95.02923976608187
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 150.0
Weighted Recall: 0.8187134502923976
Weighted FalsePositiveRate: 0.02233959100915274
Kappa statistic: 0.7993223675493555
Training time: 18.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 17.15912588488769
Incorrectly Classified Instances: 19.883040935672515
Correctly Classified Instances: 80.11695906432749
Weighted Precision: 0.7788551847762373
Weighted AreaUnderROC: 0.966844095008327
Root mean squared error: 0.12435004367125582
Relative absolute error: 35.73408063823462
Root relative squared error: 55.68821410983236
Weighted TruePositiveRate: 0.8011695906432749
Weighted MatthewsCorrelation: 0.7620274105224885
Weighted FMeasure: 0.7688808645860223
Iteration time: 20.0
Weighted AreaUnderPRC: 0.8723411208833982
Mean absolute error: 0.03563509426527584
Coverage of cases: 95.90643274853801
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 170.0
Weighted Recall: 0.8011695906432749
Weighted FalsePositiveRate: 0.023702219994087448
Kappa statistic: 0.7799477688202566
Training time: 18.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 16.897506925207768
Incorrectly Classified Instances: 16.666666666666668
Correctly Classified Instances: 83.33333333333333
Weighted Precision: 0.8101681019087732
Weighted AreaUnderROC: 0.9669843735803587
Root mean squared error: 0.12118157202308068
Relative absolute error: 34.27123924509486
Root relative squared error: 54.26926384383179
Weighted TruePositiveRate: 0.8333333333333334
Weighted MatthewsCorrelation: 0.7980312130295802
Weighted FMeasure: 0.803584519152399
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8768574215108996
Mean absolute error: 0.034176305064360826
Coverage of cases: 94.15204678362574
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 193.0
Weighted Recall: 0.8333333333333334
Weighted FalsePositiveRate: 0.01859258639156018
Kappa statistic: 0.8161671790422663
Training time: 21.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 18.728839642967095
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8303795087975369
Weighted AreaUnderROC: 0.9745025620616642
Root mean squared error: 0.11660317848315663
Relative absolute error: 34.32143059042042
Root relative squared error: 52.21890220178518
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.81927883444405
Weighted FMeasure: 0.8291243705409665
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8983058605002657
Mean absolute error: 0.03422635737548881
Coverage of cases: 96.49122807017544
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 219.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.016811860924182936
Kappa statistic: 0.8260574722387047
Training time: 26.0
		
Time end:Wed Nov 01 09.42.00 EET 2017