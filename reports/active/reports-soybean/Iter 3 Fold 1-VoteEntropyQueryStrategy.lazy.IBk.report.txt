Wed Nov 01 09.41.42 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.41.42 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 73.79193598030126
Incorrectly Classified Instances: 21.05263157894737
Correctly Classified Instances: 78.94736842105263
Weighted Precision: 0.8279691508658786
Weighted AreaUnderROC: 0.9059788020524262
Root mean squared error: 0.13353588809471567
Relative absolute error: 37.72769388647672
Root relative squared error: 59.8019502689093
Weighted TruePositiveRate: 0.7894736842105263
Weighted MatthewsCorrelation: 0.77625212783875
Weighted FMeasure: 0.7854268705770692
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7431719545085451
Mean absolute error: 0.03762318503914609
Coverage of cases: 92.98245614035088
Instances selection time: 12.0
Test time: 16.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7894736842105263
Weighted FalsePositiveRate: 0.020843544206428945
Kappa statistic: 0.7697486535008977
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 65.83564173591859
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8588666992310089
Weighted AreaUnderROC: 0.9328024039238628
Root mean squared error: 0.1140321736545963
Relative absolute error: 29.634844828209246
Root relative squared error: 51.067518067584224
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8283887422927485
Weighted FMeasure: 0.8383487702130079
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8175175609017954
Mean absolute error: 0.029552753845305882
Coverage of cases: 98.24561403508773
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.01875397258466946
Kappa statistic: 0.8295531482735274
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 58.89504462911655
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8588666992310089
Weighted AreaUnderROC: 0.9337821481022777
Root mean squared error: 0.11361174032682246
Relative absolute error: 27.360548111039698
Root relative squared error: 50.87923360475061
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8283887422927485
Weighted FMeasure: 0.8383487702130079
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8181108040330842
Mean absolute error: 0.0272847571190426
Coverage of cases: 97.953216374269
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 38.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.01875397258466946
Kappa statistic: 0.8295531482735274
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 52.04678362573087
Incorrectly Classified Instances: 18.42105263157895
Correctly Classified Instances: 81.57894736842105
Weighted Precision: 0.8427743507764965
Weighted AreaUnderROC: 0.9192579266572015
Root mean squared error: 0.12407377844696293
Relative absolute error: 28.289814241997107
Root relative squared error: 55.5644930679468
Weighted TruePositiveRate: 0.8157894736842105
Weighted MatthewsCorrelation: 0.7984684272998133
Weighted FMeasure: 0.806596752912981
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7800206180344527
Mean absolute error: 0.02821144910559293
Coverage of cases: 96.49122807017544
Instances selection time: 12.0
Test time: 23.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8157894736842105
Weighted FalsePositiveRate: 0.023778070585939306
Kappa statistic: 0.7971492053927844
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 44.10587873191753
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.8516987713805494
Weighted AreaUnderROC: 0.914068810633411
Root mean squared error: 0.12664228023792745
Relative absolute error: 27.3370455827847
Root relative squared error: 56.7147562560713
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.79384919474926
Weighted FMeasure: 0.7983765687093356
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7844948293432558
Mean absolute error: 0.027261319694743987
Coverage of cases: 94.73684210526316
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 64.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.025541731116245362
Kappa statistic: 0.7874075818224628
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 35.964912280701846
Incorrectly Classified Instances: 19.883040935672515
Correctly Classified Instances: 80.11695906432749
Weighted Precision: 0.849832559937733
Weighted AreaUnderROC: 0.9084701085777901
Root mean squared error: 0.12785704389651514
Relative absolute error: 27.258095518475397
Root relative squared error: 57.25876908240464
Weighted TruePositiveRate: 0.8011695906432749
Weighted MatthewsCorrelation: 0.7888741591576875
Weighted FMeasure: 0.7922184704016307
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7878929372621081
Mean absolute error: 0.027182588328673762
Coverage of cases: 92.10526315789474
Instances selection time: 13.0
Test time: 28.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8011695906432749
Weighted FalsePositiveRate: 0.02494840409847584
Kappa statistic: 0.781216779401112
Training time: 2.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 31.702062172976305
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.849363177433353
Weighted AreaUnderROC: 0.90927458297882
Root mean squared error: 0.1264544753169797
Relative absolute error: 26.2195569696707
Root relative squared error: 56.6306507717478
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7927647976871853
Weighted FMeasure: 0.7966769721906143
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7918013135117583
Mean absolute error: 0.026146926617954393
Coverage of cases: 92.39766081871345
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 93.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.02440593074370065
Kappa statistic: 0.787579638813864
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 28.454909202831605
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.8625158836302145
Weighted AreaUnderROC: 0.9208822862201015
Root mean squared error: 0.12006139274235297
Relative absolute error: 24.127800447443594
Root relative squared error: 53.76760914565188
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.8214477286586496
Weighted FMeasure: 0.8275563052977787
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8132642175471991
Mean absolute error: 0.024060964435124017
Coverage of cases: 92.98245614035088
Instances selection time: 12.0
Test time: 34.0
Accumulative iteration time: 106.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.019839163019038188
Kappa statistic: 0.8197848936230275
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 25.423207140658718
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8744831257111959
Weighted AreaUnderROC: 0.9228530556258651
Root mean squared error: 0.11310705128129721
Relative absolute error: 22.042696501874335
Root relative squared error: 50.653216541979006
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8373410011293595
Weighted FMeasure: 0.8414287665393435
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8303132694163714
Mean absolute error: 0.021981636400761305
Coverage of cases: 92.69005847953217
Instances selection time: 12.0
Test time: 36.0
Accumulative iteration time: 119.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.017453404054331857
Kappa statistic: 0.8358662613981762
Training time: 1.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 21.91443521083411
Incorrectly Classified Instances: 14.32748538011696
Correctly Classified Instances: 85.67251461988305
Weighted Precision: 0.8806157071618061
Weighted AreaUnderROC: 0.9280584651406012
Root mean squared error: 0.11133550588901546
Relative absolute error: 21.09183064531631
Root relative squared error: 49.85985776060628
Weighted TruePositiveRate: 0.8567251461988304
Weighted MatthewsCorrelation: 0.8433178412015513
Weighted FMeasure: 0.8454266728401146
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8361226184307724
Mean absolute error: 0.02103340452164526
Coverage of cases: 91.2280701754386
Instances selection time: 11.0
Test time: 39.0
Accumulative iteration time: 130.0
Weighted Recall: 0.8567251461988304
Weighted FalsePositiveRate: 0.016711020632292562
Kappa statistic: 0.8422613164657047
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 18.097876269621462
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.8872916273803549
Weighted AreaUnderROC: 0.9356233643735159
Root mean squared error: 0.10518410082101058
Relative absolute error: 18.96795666144968
Root relative squared error: 47.10504761024565
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8560348734415881
Weighted FMeasure: 0.8583730883837261
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8503293656289038
Mean absolute error: 0.018915413845213145
Coverage of cases: 90.64327485380117
Instances selection time: 9.0
Test time: 41.0
Accumulative iteration time: 140.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.015786579633007856
Kappa statistic: 0.858322427574779
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 15.343182517697727
Incorrectly Classified Instances: 11.403508771929825
Correctly Classified Instances: 88.59649122807018
Weighted Precision: 0.902353529627612
Weighted AreaUnderROC: 0.944124033242667
Root mean squared error: 0.09972745040627408
Relative absolute error: 17.433399588228706
Root relative squared error: 44.66137242005677
Weighted TruePositiveRate: 0.8859649122807017
Weighted MatthewsCorrelation: 0.8723972413001027
Weighted FMeasure: 0.8748932737101663
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8653974282563789
Mean absolute error: 0.017385107622610498
Coverage of cases: 91.81286549707602
Instances selection time: 8.0
Test time: 42.0
Accumulative iteration time: 148.0
Weighted Recall: 0.8859649122807017
Weighted FalsePositiveRate: 0.01467452605171497
Kappa statistic: 0.8745072211506798
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 12.04986149584487
Incorrectly Classified Instances: 10.526315789473685
Correctly Classified Instances: 89.47368421052632
Weighted Precision: 0.9072271768686718
Weighted AreaUnderROC: 0.9558867495466219
Root mean squared error: 0.09376903318856837
Relative absolute error: 16.229412965998247
Root relative squared error: 41.99298884753046
Weighted TruePositiveRate: 0.8947368421052632
Weighted MatthewsCorrelation: 0.8838151923977755
Weighted FMeasure: 0.8891596009311101
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8901200621118228
Mean absolute error: 0.016184456143377896
Coverage of cases: 93.27485380116958
Instances selection time: 5.0
Test time: 45.0
Accumulative iteration time: 154.0
Weighted Recall: 0.8947368421052632
Weighted FalsePositiveRate: 0.012854788771055049
Kappa statistic: 0.884406305452019
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 8.987380732533078
Incorrectly Classified Instances: 8.771929824561404
Correctly Classified Instances: 91.2280701754386
Weighted Precision: 0.9204311074410236
Weighted AreaUnderROC: 0.9654364968455328
Root mean squared error: 0.08621041982942468
Relative absolute error: 14.152998575735197
Root relative squared error: 38.60798256453925
Weighted TruePositiveRate: 0.9122807017543859
Weighted MatthewsCorrelation: 0.9030689132775245
Weighted FMeasure: 0.9089082525306881
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9068767602793717
Mean absolute error: 0.014113793593531068
Coverage of cases: 94.44444444444444
Instances selection time: 2.0
Test time: 47.0
Accumulative iteration time: 158.0
Weighted Recall: 0.9122807017543859
Weighted FalsePositiveRate: 0.009807548009800339
Kappa statistic: 0.90389569029309
Training time: 2.0
		
Time end:Wed Nov 01 09.41.43 EET 2017