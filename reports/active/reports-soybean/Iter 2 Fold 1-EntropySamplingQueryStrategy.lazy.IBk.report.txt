Wed Nov 01 09.23.00 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.23.00 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 71.76054170513967
Incorrectly Classified Instances: 19.883040935672515
Correctly Classified Instances: 80.11695906432749
Weighted Precision: 0.7641590782998815
Weighted AreaUnderROC: 0.8844030531899758
Root mean squared error: 0.13574791513402631
Relative absolute error: 37.6284576520421
Root relative squared error: 60.792571838030184
Weighted TruePositiveRate: 0.8011695906432749
Weighted MatthewsCorrelation: 0.7589026413332222
Weighted FMeasure: 0.7709442639927091
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7242242238954315
Mean absolute error: 0.037524223697327636
Coverage of cases: 94.73684210526316
Instances selection time: 36.0
Test time: 24.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8011695906432749
Weighted FalsePositiveRate: 0.023355880982947955
Kappa statistic: 0.7804214819850442
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 64.61988304093546
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.8150709780181056
Weighted AreaUnderROC: 0.9205229391623194
Root mean squared error: 0.12181922423789379
Relative absolute error: 30.879921554641463
Root relative squared error: 54.554826373749336
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.803053095861215
Weighted FMeasure: 0.8168257201704859
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7830869021786752
Mean absolute error: 0.03079438160573691
Coverage of cases: 95.32163742690058
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 48.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.02040788754378945
Kappa statistic: 0.819925533115198
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 56.57125269313643
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8211750363390927
Weighted AreaUnderROC: 0.9291986447247138
Root mean squared error: 0.11774366048621043
Relative absolute error: 27.935486129416816
Root relative squared error: 52.72964915529953
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8117329047851295
Weighted FMeasure: 0.8249611303551079
Iteration time: 12.0
Weighted AreaUnderPRC: 0.806833035637649
Mean absolute error: 0.027858102511330033
Coverage of cases: 95.6140350877193
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 60.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.018766423632192372
Kappa statistic: 0.8263567640753695
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 49.261311172668506
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.8233799420202929
Weighted AreaUnderROC: 0.9319906423097309
Root mean squared error: 0.11599865941176131
Relative absolute error: 25.811775785324716
Root relative squared error: 51.948177829783035
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.8091071923681732
Weighted FMeasure: 0.8213172908200006
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8136760680397789
Mean absolute error: 0.025740275021376675
Coverage of cases: 94.73684210526316
Instances selection time: 13.0
Test time: 23.0
Accumulative iteration time: 73.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.020297142278813702
Kappa statistic: 0.822931375317707
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 41.95136965220078
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.8183739591374143
Weighted AreaUnderROC: 0.9342014845971178
Root mean squared error: 0.11435097185351588
Relative absolute error: 24.43197291316344
Root relative squared error: 51.21028683416541
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.8050183291955016
Weighted FMeasure: 0.8195793666276552
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8295792314615944
Mean absolute error: 0.024364294317836332
Coverage of cases: 93.85964912280701
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 87.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.021484204420219533
Kappa statistic: 0.8197119457780289
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 36.411203447214646
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8237432830468244
Weighted AreaUnderROC: 0.9398634427137103
Root mean squared error: 0.11374118833642174
Relative absolute error: 23.49182928124041
Root relative squared error: 50.93720486283649
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8118387331178218
Weighted FMeasure: 0.8262089423189888
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8297493900619667
Mean absolute error: 0.023426754961901997
Coverage of cases: 94.73684210526316
Instances selection time: 14.0
Test time: 28.0
Accumulative iteration time: 101.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.020575400879837044
Kappa statistic: 0.8262097001863248
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 31.040320098491865
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.8208568534290929
Weighted AreaUnderROC: 0.9448184816922923
Root mean squared error: 0.11304073640762546
Relative absolute error: 22.43374367317684
Root relative squared error: 50.623518467296684
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.8083663456878029
Weighted FMeasure: 0.8229129649377238
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8350273357391159
Mean absolute error: 0.02237160033890231
Coverage of cases: 95.02923976608187
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.02102980265002829
Kappa statistic: 0.8229913613008866
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 27.71622037550014
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8270406149254156
Weighted AreaUnderROC: 0.9492128305893299
Root mean squared error: 0.11087601007288686
Relative absolute error: 21.431627540790263
Root relative squared error: 49.65407977584908
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8155070458213228
Weighted FMeasure: 0.8295779778869862
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8413273158646335
Mean absolute error: 0.021372260151480783
Coverage of cases: 95.6140350877193
Instances selection time: 13.0
Test time: 33.0
Accumulative iteration time: 127.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.0201436727900376
Kappa statistic: 0.8294633448743038
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 23.453370267774794
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8244555450931745
Weighted AreaUnderROC: 0.9503618483432957
Root mean squared error: 0.11131653765484921
Relative absolute error: 20.641794281546314
Root relative squared error: 49.85136313483576
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8122694623311256
Weighted FMeasure: 0.8258365917218948
Iteration time: 11.0
Weighted AreaUnderPRC: 0.841083024870202
Mean absolute error: 0.020584614796002047
Coverage of cases: 95.6140350877193
Instances selection time: 11.0
Test time: 36.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.02023174437462155
Kappa statistic: 0.8262424025741866
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 19.944598337950204
Incorrectly Classified Instances: 15.789473684210526
Correctly Classified Instances: 84.21052631578948
Weighted Precision: 0.8256929380925814
Weighted AreaUnderROC: 0.9517863959809115
Root mean squared error: 0.11134518089748154
Relative absolute error: 20.129305956845815
Root relative squared error: 49.8641905611994
Weighted TruePositiveRate: 0.8421052631578947
Weighted MatthewsCorrelation: 0.8128575272019232
Weighted FMeasure: 0.8262264552696529
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8426668837520762
Mean absolute error: 0.02007354610655002
Coverage of cases: 94.15204678362574
Instances selection time: 10.0
Test time: 38.0
Accumulative iteration time: 148.0
Weighted Recall: 0.8421052631578947
Weighted FalsePositiveRate: 0.020065324256278355
Kappa statistic: 0.8262718242022877
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 17.282240689442975
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8926913603486107
Weighted AreaUnderROC: 0.9664790718202985
Root mean squared error: 0.09363020242889002
Relative absolute error: 15.731623793841262
Root relative squared error: 41.930815672180096
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8748603535356912
Weighted FMeasure: 0.8871318116790669
Iteration time: 23.0
Weighted AreaUnderPRC: 0.8948101876120952
Mean absolute error: 0.015688045888595303
Coverage of cases: 93.27485380116958
Instances selection time: 23.0
Test time: 50.0
Accumulative iteration time: 171.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.01532867971700224
Kappa statistic: 0.8778846876644365
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 14.404432132964004
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8956983061862406
Weighted AreaUnderROC: 0.9637674259107031
Root mean squared error: 0.09363194012857581
Relative absolute error: 15.591750260242907
Root relative squared error: 41.93159387369338
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8755289266147248
Weighted FMeasure: 0.8862900733345431
Iteration time: 8.0
Weighted AreaUnderPRC: 0.895791627357902
Mean absolute error: 0.015548559816308854
Coverage of cases: 93.27485380116958
Instances selection time: 8.0
Test time: 42.0
Accumulative iteration time: 179.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.015075301338623861
Kappa statistic: 0.877868621370172
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 11.603570329332118
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.8985974767865035
Weighted AreaUnderROC: 0.9631616957353789
Root mean squared error: 0.09366210830916614
Relative absolute error: 15.358757852182949
Root relative squared error: 41.94510421957198
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.8789193421842864
Weighted FMeasure: 0.8892897577690576
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8952982793915688
Mean absolute error: 0.01531621281658147
Coverage of cases: 93.27485380116958
Instances selection time: 5.0
Test time: 44.0
Accumulative iteration time: 185.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.014620899568432618
Kappa statistic: 0.8810826050183254
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 8.879655278547244
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.9080397627074196
Weighted AreaUnderROC: 0.9636892393958226
Root mean squared error: 0.09183561192402516
Relative absolute error: 15.228979308524188
Root relative squared error: 41.127136499066246
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8869967696185671
Weighted FMeasure: 0.8954990651231252
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8968198486132781
Mean absolute error: 0.015186793770273562
Coverage of cases: 94.15204678362574
Instances selection time: 3.0
Test time: 47.0
Accumulative iteration time: 188.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.013712096028050132
Kappa statistic: 0.887510572314632
Training time: 0.0
		
Time end:Wed Nov 01 09.23.01 EET 2017