Wed Nov 01 09.27.23 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.27.23 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.633733456448155
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.7254820365545388
Weighted AreaUnderROC: 0.9646798455675958
Root mean squared error: 0.14967014731037812
Relative absolute error: 30.808229080290534
Root relative squared error: 67.02742486609303
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6871652165537357
Weighted FMeasure: 0.6916262086195485
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8281349316969276
Mean absolute error: 0.030722887725497756
Coverage of cases: 87.42690058479532
Instances selection time: 26.0
Test time: 37.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.034696928779716606
Kappa statistic: 0.7013695769781988
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 8.771929824561415
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.810251508628058
Weighted AreaUnderROC: 0.977245744640543
Root mean squared error: 0.12535978209408755
Relative absolute error: 23.395726053023015
Root relative squared error: 56.14040960430468
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7785376988085482
Weighted FMeasure: 0.7824414036021233
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8939076814584771
Mean absolute error: 0.023330917947613185
Coverage of cases: 91.2280701754386
Instances selection time: 31.0
Test time: 37.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.025568283406285006
Kappa statistic: 0.7864097881319846
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.648815020006165
Incorrectly Classified Instances: 14.32748538011696
Correctly Classified Instances: 85.67251461988305
Weighted Precision: 0.8734406361375384
Weighted AreaUnderROC: 0.9852062827737457
Root mean squared error: 0.10535605918401622
Relative absolute error: 19.032950077758244
Root relative squared error: 47.18205646246889
Weighted TruePositiveRate: 0.8567251461988304
Weighted MatthewsCorrelation: 0.8420784465354546
Weighted FMeasure: 0.8499399783827521
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9276815286436929
Mean absolute error: 0.01898022722435742
Coverage of cases: 95.90643274853801
Instances selection time: 22.0
Test time: 37.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8567251461988304
Weighted FalsePositiveRate: 0.01871654025539791
Kappa statistic: 0.8422137900515032
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.279470606340418
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.9101645589954066
Weighted AreaUnderROC: 0.9906144639287737
Root mean squared error: 0.09281809787928878
Relative absolute error: 15.497318016680197
Root relative squared error: 41.567127403944966
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.8824702438572591
Weighted FMeasure: 0.8869080573288382
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9537163652767155
Mean absolute error: 0.015454389157908366
Coverage of cases: 95.6140350877193
Instances selection time: 20.0
Test time: 37.0
Accumulative iteration time: 100.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.014351334558549856
Kappa statistic: 0.88101775236949
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.925515543244087
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.9173890123678385
Weighted AreaUnderROC: 0.9900301183019776
Root mean squared error: 0.09070266557093566
Relative absolute error: 14.318782432395937
Root relative squared error: 40.61976426803915
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8917524609159965
Weighted FMeasure: 0.8949561541523089
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9578044713831051
Mean absolute error: 0.014279118215131807
Coverage of cases: 95.02923976608187
Instances selection time: 19.0
Test time: 37.0
Accumulative iteration time: 119.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.012147154533201242
Kappa statistic: 0.8908271523800582
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.787011388119436
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9325067502634582
Weighted AreaUnderROC: 0.9923794480574232
Root mean squared error: 0.08346728401451632
Relative absolute error: 12.31854857060085
Root relative squared error: 37.37951227146224
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9141077285429504
Weighted FMeasure: 0.9196543955841678
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9646662822521106
Mean absolute error: 0.012284425167358297
Coverage of cases: 96.19883040935673
Instances selection time: 18.0
Test time: 37.0
Accumulative iteration time: 137.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.010751365801886195
Kappa statistic: 0.9133714221384143
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.525392428439526
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9297562903674957
Weighted AreaUnderROC: 0.9911965286225485
Root mean squared error: 0.08382499601417269
Relative absolute error: 11.82568271533195
Root relative squared error: 37.53970797255249
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9132943108187067
Weighted FMeasure: 0.9194317125900324
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9593163764667929
Mean absolute error: 0.011792924591466868
Coverage of cases: 95.90643274853801
Instances selection time: 14.0
Test time: 37.0
Accumulative iteration time: 152.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.010717317110119383
Kappa statistic: 0.9133437813793297
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.38688827331487
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9344083086915779
Weighted AreaUnderROC: 0.9925869127808465
Root mean squared error: 0.08384102475388416
Relative absolute error: 11.393188089445287
Root relative squared error: 37.54688619189694
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.9166974981334703
Weighted FMeasure: 0.9228707984869338
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9644111399236114
Mean absolute error: 0.01136162801163529
Coverage of cases: 95.90643274853801
Instances selection time: 12.0
Test time: 38.0
Accumulative iteration time: 164.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.010995575711142727
Kappa statistic: 0.9165532709578731
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.217605417051402
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9928094964996959
Root mean squared error: 0.0834193009448802
Relative absolute error: 10.989609129124432
Root relative squared error: 37.35802380731174
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9649966701206288
Mean absolute error: 0.010959166998572939
Coverage of cases: 95.6140350877193
Instances selection time: 11.0
Test time: 48.0
Accumulative iteration time: 175.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.140658664204373
Incorrectly Classified Instances: 7.309941520467836
Correctly Classified Instances: 92.69005847953217
Weighted Precision: 0.9370664692444751
Weighted AreaUnderROC: 0.9928061832962135
Root mean squared error: 0.08362379921417573
Relative absolute error: 10.92943586821803
Root relative squared error: 37.449605145519605
Weighted TruePositiveRate: 0.9269005847953217
Weighted MatthewsCorrelation: 0.9196424295270319
Weighted FMeasure: 0.9258021033572966
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9634519356493914
Mean absolute error: 0.010899160422599793
Coverage of cases: 95.6140350877193
Instances selection time: 8.0
Test time: 38.0
Accumulative iteration time: 184.0
Weighted Recall: 0.9269005847953217
Weighted FalsePositiveRate: 0.010907504126558778
Kappa statistic: 0.9197627605364165
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.048322560787937
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9351142507352391
Weighted AreaUnderROC: 0.9929713802309449
Root mean squared error: 0.08470739978705064
Relative absolute error: 10.905228335213321
Root relative squared error: 37.93487864386533
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.916574750862787
Weighted FMeasure: 0.9227801257075224
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9632761133045018
Mean absolute error: 0.010875019946473214
Coverage of cases: 95.32163742690058
Instances selection time: 7.0
Test time: 37.0
Accumulative iteration time: 192.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.01136190589675002
Kappa statistic: 0.9165524878469941
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 6.909818405663284
Incorrectly Classified Instances: 7.60233918128655
Correctly Classified Instances: 92.39766081871345
Weighted Precision: 0.9351142507352391
Weighted AreaUnderROC: 0.9932272779142716
Root mean squared error: 0.08527273816845093
Relative absolute error: 10.868179980246362
Root relative squared error: 38.18805655919568
Weighted TruePositiveRate: 0.9239766081871345
Weighted MatthewsCorrelation: 0.916574750862787
Weighted FMeasure: 0.9227801257075224
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9640233127452214
Mean absolute error: 0.010838074218528324
Coverage of cases: 94.73684210526316
Instances selection time: 6.0
Test time: 42.0
Accumulative iteration time: 198.0
Weighted Recall: 0.9239766081871345
Weighted FalsePositiveRate: 0.01136190589675002
Kappa statistic: 0.9165524878469941
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 6.863650353955063
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9307731133389027
Weighted AreaUnderROC: 0.9938764185487922
Root mean squared error: 0.08638452075074672
Relative absolute error: 10.829682715922194
Root relative squared error: 38.68595092785509
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9130034816319632
Weighted FMeasure: 0.9197643567302259
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9651999193221171
Mean absolute error: 0.010799683594825554
Coverage of cases: 94.44444444444444
Instances selection time: 4.0
Test time: 37.0
Accumulative iteration time: 203.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.011449977481333971
Kappa statistic: 0.9133722348349813
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 6.8328716528162525
Incorrectly Classified Instances: 7.894736842105263
Correctly Classified Instances: 92.10526315789474
Weighted Precision: 0.9307731133389027
Weighted AreaUnderROC: 0.9937977247566483
Root mean squared error: 0.08666160509870462
Relative absolute error: 10.883704459095485
Root relative squared error: 38.81003880140948
Weighted TruePositiveRate: 0.9210526315789473
Weighted MatthewsCorrelation: 0.9130034816319632
Weighted FMeasure: 0.9197643567302259
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9643031049137923
Mean absolute error: 0.010853555693280913
Coverage of cases: 94.44444444444444
Instances selection time: 2.0
Test time: 37.0
Accumulative iteration time: 205.0
Weighted Recall: 0.9210526315789473
Weighted FalsePositiveRate: 0.011449977481333971
Kappa statistic: 0.9133722348349813
Training time: 0.0
		
Time end:Wed Nov 01 09.27.24 EET 2017