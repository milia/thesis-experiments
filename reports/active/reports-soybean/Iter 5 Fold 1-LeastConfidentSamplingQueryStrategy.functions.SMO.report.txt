Wed Nov 01 09.30.01 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.30.01 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 26.900584795321638
Correctly Classified Instances: 73.09941520467837
Weighted Precision: 0.6719216132081478
Weighted AreaUnderROC: 0.8981308242887049
Root mean squared error: 0.21618784314934567
Relative absolute error: 95.5929667040771
Root relative squared error: 96.81633027063039
Weighted TruePositiveRate: 0.7309941520467836
Weighted MatthewsCorrelation: 0.6678648219952337
Weighted FMeasure: 0.6873342010573581
Iteration time: 785.0
Weighted AreaUnderPRC: 0.6339597954767437
Mean absolute error: 0.0953281662422938
Coverage of cases: 88.30409356725146
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 785.0
Weighted Recall: 0.7309941520467836
Weighted FalsePositiveRate: 0.037606156860589726
Kappa statistic: 0.7006991676575506
Training time: 767.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 20.46783625730994
Correctly Classified Instances: 79.53216374269006
Weighted Precision: 0.7386551828072296
Weighted AreaUnderROC: 0.9084687025531574
Root mean squared error: 0.2159541199040597
Relative absolute error: 95.49382716049313
Root relative squared error: 96.71166098591071
Weighted TruePositiveRate: 0.7953216374269005
Weighted MatthewsCorrelation: 0.7414406675168137
Weighted FMeasure: 0.7528652708422763
Iteration time: 771.0
Weighted AreaUnderPRC: 0.6923739597300862
Mean absolute error: 0.0952293013234843
Coverage of cases: 88.30409356725146
Instances selection time: 18.0
Test time: 27.0
Accumulative iteration time: 1556.0
Weighted Recall: 0.7953216374269005
Weighted FalsePositiveRate: 0.030441610313173974
Kappa statistic: 0.7723209190854795
Training time: 753.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 73.69959987688499
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.780372270138352
Weighted AreaUnderROC: 0.9262666840381233
Root mean squared error: 0.2153789081036671
Relative absolute error: 95.37588183421434
Root relative squared error: 96.45406141494918
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7704193398720139
Weighted FMeasure: 0.775903630266985
Iteration time: 879.0
Weighted AreaUnderPRC: 0.723850417021177
Mean absolute error: 0.09511168271556084
Coverage of cases: 91.2280701754386
Instances selection time: 18.0
Test time: 30.0
Accumulative iteration time: 2435.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.026969216910115584
Kappa statistic: 0.792263012034471
Training time: 861.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 73.69959987688499
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.7772419263647333
Weighted AreaUnderROC: 0.9261933078100348
Root mean squared error: 0.21539057855677393
Relative absolute error: 95.38139329805914
Root relative squared error: 96.4592878440857
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7693555779957488
Weighted FMeasure: 0.7750536375339326
Iteration time: 891.0
Weighted AreaUnderPRC: 0.7223769394586966
Mean absolute error: 0.09511717891219278
Coverage of cases: 91.2280701754386
Instances selection time: 21.0
Test time: 30.0
Accumulative iteration time: 3326.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.026543879947588788
Kappa statistic: 0.7923418466092369
Training time: 870.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 73.68421052631558
Incorrectly Classified Instances: 19.5906432748538
Correctly Classified Instances: 80.4093567251462
Weighted Precision: 0.7421233874268514
Weighted AreaUnderROC: 0.9250250375437357
Root mean squared error: 0.2153852370612707
Relative absolute error: 95.3795561434442
Root relative squared error: 96.45689573921409
Weighted TruePositiveRate: 0.804093567251462
Weighted MatthewsCorrelation: 0.7493830280337588
Weighted FMeasure: 0.7629010987174157
Iteration time: 912.0
Weighted AreaUnderPRC: 0.702919547994102
Mean absolute error: 0.0951153468466488
Coverage of cases: 91.2280701754386
Instances selection time: 15.0
Test time: 30.0
Accumulative iteration time: 4238.0
Weighted Recall: 0.804093567251462
Weighted FalsePositiveRate: 0.02729529497069004
Kappa statistic: 0.782562486952231
Training time: 897.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.7745025994148801
Weighted AreaUnderROC: 0.9463087421195262
Root mean squared error: 0.21475490318508086
Relative absolute error: 95.19426289034026
Root relative squared error: 96.17461061231266
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.7894309929718735
Weighted FMeasure: 0.8010703387551892
Iteration time: 1008.0
Weighted AreaUnderPRC: 0.7574788545582073
Mean absolute error: 0.09493056687125427
Coverage of cases: 94.15204678362574
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 5246.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.02190593364826012
Kappa statistic: 0.8218901798142203
Training time: 993.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.817039569545835
Weighted AreaUnderROC: 0.9616020996676484
Root mean squared error: 0.2141649664374321
Relative absolute error: 94.98772651794043
Root relative squared error: 95.91041670498139
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8217017116970474
Weighted FMeasure: 0.8320876715461237
Iteration time: 1166.0
Weighted AreaUnderPRC: 0.7835697219120621
Mean absolute error: 0.09472460262176968
Coverage of cases: 97.07602339181287
Instances selection time: 15.0
Test time: 35.0
Accumulative iteration time: 6412.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.018725121256921412
Kappa statistic: 0.8449990085828399
Training time: 1151.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 14.32748538011696
Correctly Classified Instances: 85.67251461988305
Weighted Precision: 0.8164622716001161
Weighted AreaUnderROC: 0.9613343712862077
Root mean squared error: 0.21415599666134322
Relative absolute error: 94.984116670276
Root relative squared error: 95.90639973163256
Weighted TruePositiveRate: 0.8567251461988304
Weighted MatthewsCorrelation: 0.8192388121025945
Weighted FMeasure: 0.8298147815166835
Iteration time: 1134.0
Weighted AreaUnderPRC: 0.7812807850303688
Mean absolute error: 0.09472100277368327
Coverage of cases: 97.07602339181287
Instances selection time: 13.0
Test time: 35.0
Accumulative iteration time: 7546.0
Weighted Recall: 0.8567251461988304
Weighted FalsePositiveRate: 0.01961825874269401
Kappa statistic: 0.8416711544457357
Training time: 1121.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 13.742690058479532
Correctly Classified Instances: 86.25730994152046
Weighted Precision: 0.821719522471402
Weighted AreaUnderROC: 0.9620571432470845
Root mean squared error: 0.21413264890715614
Relative absolute error: 94.97509205111491
Root relative squared error: 95.89594380660215
Weighted TruePositiveRate: 0.8625730994152047
Weighted MatthewsCorrelation: 0.8251968222889586
Weighted FMeasure: 0.8356463539117235
Iteration time: 1148.0
Weighted AreaUnderPRC: 0.7888482507638555
Mean absolute error: 0.09471200315346723
Coverage of cases: 97.07602339181287
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 8694.0
Weighted Recall: 0.8625730994152047
Weighted FalsePositiveRate: 0.01946015179024929
Kappa statistic: 0.8480962416246917
Training time: 1137.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 78.99353647276041
Incorrectly Classified Instances: 13.450292397660819
Correctly Classified Instances: 86.54970760233918
Weighted Precision: 0.8232893361818616
Weighted AreaUnderROC: 0.9623809111103085
Root mean squared error: 0.21413717119727108
Relative absolute error: 94.97689697494715
Root relative squared error: 95.89796904320643
Weighted TruePositiveRate: 0.8654970760233918
Weighted MatthewsCorrelation: 0.8279244654679678
Weighted FMeasure: 0.8379917659381476
Iteration time: 1147.0
Weighted AreaUnderPRC: 0.7918010438727512
Mean absolute error: 0.09471380307751046
Coverage of cases: 97.07602339181287
Instances selection time: 8.0
Test time: 35.0
Accumulative iteration time: 9841.0
Weighted Recall: 0.8654970760233918
Weighted FalsePositiveRate: 0.01902850004280807
Kappa statistic: 0.8513254264518263
Training time: 1139.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 78.99353647276041
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.819524803034286
Weighted AreaUnderROC: 0.9619644450791909
Root mean squared error: 0.21415262982649627
Relative absolute error: 94.984116670276
Root relative squared error: 95.90489194752331
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8211434361694617
Weighted FMeasure: 0.830591985304576
Iteration time: 1181.0
Weighted AreaUnderPRC: 0.7895452078679465
Mean absolute error: 0.09472100277368327
Coverage of cases: 97.07602339181287
Instances selection time: 7.0
Test time: 35.0
Accumulative iteration time: 11022.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.02000555365144063
Kappa statistic: 0.8447997125920603
Training time: 1174.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 79.00892582332983
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.8247610746758403
Weighted AreaUnderROC: 0.9664081694184787
Root mean squared error: 0.2140244787237328
Relative absolute error: 94.9299689553096
Root relative squared error: 95.84750148879561
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8339788528819406
Weighted FMeasure: 0.8454352629072778
Iteration time: 1158.0
Weighted AreaUnderPRC: 0.8138744686193534
Mean absolute error: 0.09466700505238713
Coverage of cases: 97.07602339181287
Instances selection time: 6.0
Test time: 34.0
Accumulative iteration time: 12180.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.01743724998273788
Kappa statistic: 0.8580738868401445
Training time: 1152.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.8843811843444648
Weighted AreaUnderROC: 0.9719003127442208
Root mean squared error: 0.21375309975556997
Relative absolute error: 94.81445383004785
Root relative squared error: 95.72596867997804
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8837008382254363
Weighted FMeasure: 0.8930412431402625
Iteration time: 1182.0
Weighted AreaUnderPRC: 0.8648093756428874
Mean absolute error: 0.09455180991362197
Coverage of cases: 97.07602339181287
Instances selection time: 4.0
Test time: 38.0
Accumulative iteration time: 13362.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.013124205536738041
Kappa statistic: 0.897091596377895
Training time: 1178.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 6.432748538011696
Correctly Classified Instances: 93.5672514619883
Weighted Precision: 0.9363832554192175
Weighted AreaUnderROC: 0.9885410081814863
Root mean squared error: 0.21298700572842905
Relative absolute error: 94.48956754024935
Root relative squared error: 95.38288550162005
Weighted TruePositiveRate: 0.935672514619883
Weighted MatthewsCorrelation: 0.9269987226039077
Weighted FMeasure: 0.9351798402847215
Iteration time: 1327.0
Weighted AreaUnderPRC: 0.9143081206885145
Mean absolute error: 0.09422782358584506
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 37.0
Accumulative iteration time: 14689.0
Weighted Recall: 0.935672514619883
Weighted FalsePositiveRate: 0.008795549627368603
Kappa statistic: 0.9294766046790641
Training time: 1325.0
		
Time end:Wed Nov 01 09.30.17 EET 2017