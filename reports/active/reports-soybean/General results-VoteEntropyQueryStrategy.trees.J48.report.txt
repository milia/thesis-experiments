Wed Nov 01 09.41.49 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.41.49 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 10.085201040879733
Incorrectly Classified Instances: 36.952118811202006
Correctly Classified Instances: 63.04788118879799
Weighted Precision: 0.6038842390476284
Weighted AreaUnderROC: 0.8481339223271416
Root mean squared error: 0.17806285851334763
Relative absolute error: 43.0237361584365
Root relative squared error: 79.74265466375728
Weighted TruePositiveRate: 0.6304788118879799
Weighted MatthewsCorrelation: 0.5764098164873186
Weighted FMeasure: 0.5892623815673405
Iteration time: 3.8
Weighted AreaUnderPRC: 0.5464185748159227
Mean absolute error: 0.042904556833898266
Coverage of cases: 74.87712438476446
Instances selection time: 1.8
Test time: 4.0
Accumulative iteration time: 3.8
Weighted Recall: 0.6304788118879799
Weighted FalsePositiveRate: 0.03354761407236796
Kappa statistic: 0.5931441336436956
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 10.51707766612603
Incorrectly Classified Instances: 38.12230968427913
Correctly Classified Instances: 61.87769031572087
Weighted Precision: 0.5875940217292508
Weighted AreaUnderROC: 0.8559559955969924
Root mean squared error: 0.1800088209339598
Relative absolute error: 44.33036204125014
Root relative squared error: 80.61412337200501
Weighted TruePositiveRate: 0.6187769031572088
Weighted MatthewsCorrelation: 0.559853522366976
Weighted FMeasure: 0.5714735085533381
Iteration time: 3.3
Weighted AreaUnderPRC: 0.5338007927264069
Mean absolute error: 0.04420756325443267
Coverage of cases: 77.18929533021213
Instances selection time: 1.3
Test time: 4.0
Accumulative iteration time: 7.1
Weighted Recall: 0.6187769031572088
Weighted FalsePositiveRate: 0.03659151757106559
Kappa statistic: 0.5789672694340826
Training time: 2.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 10.766651412706299
Incorrectly Classified Instances: 38.97360703812318
Correctly Classified Instances: 61.02639296187683
Weighted Precision: 0.589926081360691
Weighted AreaUnderROC: 0.8509748705258182
Root mean squared error: 0.18371142325783474
Relative absolute error: 45.192865119016915
Root relative squared error: 82.27227567246274
Weighted TruePositiveRate: 0.6102639296187683
Weighted MatthewsCorrelation: 0.5544111724252656
Weighted FMeasure: 0.5652547237740656
Iteration time: 3.7
Weighted AreaUnderPRC: 0.5247906843617767
Mean absolute error: 0.04506767712699787
Coverage of cases: 75.43370890569531
Instances selection time: 1.4
Test time: 3.9
Accumulative iteration time: 10.8
Weighted Recall: 0.6102639296187683
Weighted FalsePositiveRate: 0.038548041120596545
Kappa statistic: 0.5693874893085366
Training time: 2.3
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 10.170126788391485
Incorrectly Classified Instances: 37.85906604242767
Correctly Classified Instances: 62.140933957572315
Weighted Precision: 0.601760503407676
Weighted AreaUnderROC: 0.8598232431005048
Root mean squared error: 0.1811741920363507
Relative absolute error: 43.62080991303714
Root relative squared error: 81.136016517768
Weighted TruePositiveRate: 0.6214093395757234
Weighted MatthewsCorrelation: 0.5640120987787792
Weighted FMeasure: 0.5717102021462895
Iteration time: 4.1
Weighted AreaUnderPRC: 0.5492691130310356
Mean absolute error: 0.04349997664458035
Coverage of cases: 75.90077343897377
Instances selection time: 1.3
Test time: 4.0
Accumulative iteration time: 14.9
Weighted Recall: 0.6214093395757234
Weighted FalsePositiveRate: 0.0385498514001426
Kappa statistic: 0.5816029809133415
Training time: 2.8
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 10.087249945618296
Incorrectly Classified Instances: 36.89261031366295
Correctly Classified Instances: 63.10738968633706
Weighted Precision: 0.5979692379100384
Weighted AreaUnderROC: 0.8746930236021541
Root mean squared error: 0.17610560481362297
Relative absolute error: 42.15928884338739
Root relative squared error: 78.86612933349133
Weighted TruePositiveRate: 0.6310738968633707
Weighted MatthewsCorrelation: 0.5672802577964959
Weighted FMeasure: 0.5769773749035859
Iteration time: 4.4
Weighted AreaUnderPRC: 0.5813831782431158
Mean absolute error: 0.042042504109749565
Coverage of cases: 78.03750578793023
Instances selection time: 1.4
Test time: 4.4
Accumulative iteration time: 19.3
Weighted Recall: 0.6310738968633707
Weighted FalsePositiveRate: 0.03858076883050957
Kappa statistic: 0.5926064868547299
Training time: 3.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 10.42965622627853
Incorrectly Classified Instances: 33.49771055203992
Correctly Classified Instances: 66.50228944796008
Weighted Precision: 0.6359611971016259
Weighted AreaUnderROC: 0.889791168946747
Root mean squared error: 0.16731910383592316
Relative absolute error: 38.95845350667624
Root relative squared error: 74.93123286480997
Weighted TruePositiveRate: 0.6650228944796008
Weighted MatthewsCorrelation: 0.6072912744897405
Weighted FMeasure: 0.6158271343069178
Iteration time: 4.6
Weighted AreaUnderPRC: 0.6208971407971117
Mean absolute error: 0.03885053535291848
Coverage of cases: 81.25739568863509
Instances selection time: 1.1
Test time: 4.3
Accumulative iteration time: 23.9
Weighted Recall: 0.6650228944796008
Weighted FalsePositiveRate: 0.03572657066088732
Kappa statistic: 0.629757074321479
Training time: 3.5
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 9.32911457529455
Incorrectly Classified Instances: 28.666718114935435
Correctly Classified Instances: 71.33328188506457
Weighted Precision: 0.6696081275285183
Weighted AreaUnderROC: 0.8992851265829929
Root mean squared error: 0.15645005970276224
Relative absolute error: 33.904076527327454
Root relative squared error: 70.06370215081317
Weighted TruePositiveRate: 0.7133328188506458
Weighted MatthewsCorrelation: 0.65909632517355
Weighted FMeasure: 0.6688436288040516
Iteration time: 5.3
Weighted AreaUnderPRC: 0.6636715515072888
Mean absolute error: 0.03381015941783377
Coverage of cases: 82.07765258699047
Instances selection time: 1.4
Test time: 5.1
Accumulative iteration time: 29.2
Weighted Recall: 0.7133328188506458
Weighted FalsePositiveRate: 0.029600786513501613
Kappa statistic: 0.6828062676247294
Training time: 3.9
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 9.326348102596885
Incorrectly Classified Instances: 28.462811476393817
Correctly Classified Instances: 71.53718852360619
Weighted Precision: 0.6686338019408524
Weighted AreaUnderROC: 0.9073929027049896
Root mean squared error: 0.1549738106887364
Relative absolute error: 33.51211487622786
Root relative squared error: 69.40258721473934
Weighted TruePositiveRate: 0.715371885236062
Weighted MatthewsCorrelation: 0.6598287302774992
Weighted FMeasure: 0.6689245778673069
Iteration time: 5.3
Weighted AreaUnderPRC: 0.67462273243164
Mean absolute error: 0.03341928353308071
Coverage of cases: 83.27819793863938
Instances selection time: 1.0
Test time: 4.2
Accumulative iteration time: 34.5
Weighted Recall: 0.715371885236062
Weighted FalsePositiveRate: 0.029541974685753036
Kappa statistic: 0.6846881854681216
Training time: 4.3
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 9.21689416730076
Incorrectly Classified Instances: 28.024129238051138
Correctly Classified Instances: 71.97587076194887
Weighted Precision: 0.6753730696995233
Weighted AreaUnderROC: 0.9116340141769443
Root mean squared error: 0.15324447420989318
Relative absolute error: 32.456078666367844
Root relative squared error: 68.62813103234859
Weighted TruePositiveRate: 0.7197587076194887
Weighted MatthewsCorrelation: 0.665792739823859
Weighted FMeasure: 0.6732517611954083
Iteration time: 5.1
Weighted AreaUnderPRC: 0.6934616696524174
Mean absolute error: 0.03236617263128124
Coverage of cases: 83.80657165886367
Instances selection time: 0.8
Test time: 4.4
Accumulative iteration time: 39.6
Weighted Recall: 0.7197587076194887
Weighted FalsePositiveRate: 0.02771916259817513
Kappa statistic: 0.6902449914536298
Training time: 4.3
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 9.023805204218043
Incorrectly Classified Instances: 23.808029359812043
Correctly Classified Instances: 76.19197064018796
Weighted Precision: 0.7041632458492558
Weighted AreaUnderROC: 0.92581384335467
Root mean squared error: 0.1403231504702259
Relative absolute error: 28.149451356119158
Root relative squared error: 62.841519128139154
Weighted TruePositiveRate: 0.7619197064018797
Weighted MatthewsCorrelation: 0.7065141187080852
Weighted FMeasure: 0.7140485398182862
Iteration time: 5.5
Weighted AreaUnderPRC: 0.7391467246575896
Mean absolute error: 0.028071475036573375
Coverage of cases: 86.58820805679889
Instances selection time: 0.8
Test time: 4.0
Accumulative iteration time: 45.1
Weighted Recall: 0.7619197064018797
Weighted FalsePositiveRate: 0.024465655848153516
Kappa statistic: 0.7368026178498599
Training time: 4.7
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 8.673419928893091
Incorrectly Classified Instances: 20.994666529471285
Correctly Classified Instances: 79.00533347052871
Weighted Precision: 0.7311409228313667
Weighted AreaUnderROC: 0.931095200838531
Root mean squared error: 0.13163427644380193
Relative absolute error: 24.8710325682055
Root relative squared error: 58.95034335633115
Weighted TruePositiveRate: 0.7900533347052872
Weighted MatthewsCorrelation: 0.7375051095290013
Weighted FMeasure: 0.7462432361628016
Iteration time: 5.4
Weighted AreaUnderPRC: 0.7667462309217618
Mean absolute error: 0.024802137741146976
Coverage of cases: 87.78841041998936
Instances selection time: 0.6
Test time: 4.4
Accumulative iteration time: 50.5
Weighted Recall: 0.7900533347052872
Weighted FalsePositiveRate: 0.0231605046765648
Kappa statistic: 0.7676264903680551
Training time: 4.8
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 8.622603480971824
Incorrectly Classified Instances: 19.35595342216734
Correctly Classified Instances: 80.64404657783265
Weighted Precision: 0.7587617088775269
Weighted AreaUnderROC: 0.9349884709302698
Root mean squared error: 0.12686590409999626
Relative absolute error: 23.41342555261594
Root relative squared error: 56.81490269063049
Weighted TruePositiveRate: 0.8064404657783266
Weighted MatthewsCorrelation: 0.7604287625813695
Weighted FMeasure: 0.7679436505652614
Iteration time: 6.1
Weighted AreaUnderPRC: 0.7830116611433537
Mean absolute error: 0.023348568418121364
Coverage of cases: 88.22734989967589
Instances selection time: 0.8
Test time: 4.0
Accumulative iteration time: 56.6
Weighted Recall: 0.8064404657783266
Weighted FalsePositiveRate: 0.020538658302242992
Kappa statistic: 0.7861644304567459
Training time: 5.3
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 8.608586084236173
Incorrectly Classified Instances: 17.12987257978769
Correctly Classified Instances: 82.8701274202123
Weighted Precision: 0.7915825197411126
Weighted AreaUnderROC: 0.9471129278714845
Root mean squared error: 0.11920475543604367
Relative absolute error: 21.448513787750898
Root relative squared error: 53.383977581723045
Weighted TruePositiveRate: 0.8287012742021231
Weighted MatthewsCorrelation: 0.7902487137507059
Weighted FMeasure: 0.7980684172785281
Iteration time: 6.2
Weighted AreaUnderPRC: 0.8161703382428247
Mean absolute error: 0.021389099622134047
Coverage of cases: 90.48232751967898
Instances selection time: 0.1
Test time: 4.7
Accumulative iteration time: 62.8
Weighted Recall: 0.8287012742021231
Weighted FalsePositiveRate: 0.018339413419061375
Kappa statistic: 0.8110456566859516
Training time: 6.1
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 9.404125248553814
Incorrectly Classified Instances: 14.4368986983588
Correctly Classified Instances: 85.5631013016412
Weighted Precision: 0.8443847149292697
Weighted AreaUnderROC: 0.9630820011657224
Root mean squared error: 0.10931245349376388
Relative absolute error: 20.368899435165652
Root relative squared error: 48.953865517933515
Weighted TruePositiveRate: 0.8556310130164121
Weighted MatthewsCorrelation: 0.8311792741690548
Weighted FMeasure: 0.8382940308374673
Iteration time: 7.2
Weighted AreaUnderPRC: 0.8535347386631462
Mean absolute error: 0.020312475891024098
Coverage of cases: 93.90921095505136
Instances selection time: 0.3
Test time: 4.4
Accumulative iteration time: 70.0
Weighted Recall: 0.8556310130164121
Weighted FalsePositiveRate: 0.015097878195271489
Kappa statistic: 0.8412180797531814
Training time: 6.9
		
Time end:Wed Nov 01 09.41.54 EET 2017