Wed Nov 01 09.31.18 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.31.18 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.17205293936599
Incorrectly Classified Instances: 30.994152046783626
Correctly Classified Instances: 69.00584795321637
Weighted Precision: 0.6627661574856866
Weighted AreaUnderROC: 0.927088996691525
Root mean squared error: 0.16215389848052442
Relative absolute error: 34.736530450992504
Root relative squared error: 72.61807676722844
Weighted TruePositiveRate: 0.6900584795321637
Weighted MatthewsCorrelation: 0.6343856566752706
Weighted FMeasure: 0.6442151113362783
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7770689541847251
Mean absolute error: 0.03464030737495128
Coverage of cases: 80.99415204678363
Instances selection time: 29.0
Test time: 37.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6900584795321637
Weighted FalsePositiveRate: 0.0414627508543618
Kappa statistic: 0.654387369866148
Training time: 1.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 8.571868267159152
Incorrectly Classified Instances: 22.22222222222222
Correctly Classified Instances: 77.77777777777777
Weighted Precision: 0.7518671312850964
Weighted AreaUnderROC: 0.9602054196012632
Root mean squared error: 0.13703786118664382
Relative absolute error: 25.85862510475039
Root relative squared error: 61.370253918771596
Weighted TruePositiveRate: 0.7777777777777778
Weighted MatthewsCorrelation: 0.7300634716252374
Weighted FMeasure: 0.7376041889285784
Iteration time: 25.0
Weighted AreaUnderPRC: 0.8365205189855256
Mean absolute error: 0.025786994564294247
Coverage of cases: 85.96491228070175
Instances selection time: 24.0
Test time: 36.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7777777777777778
Weighted FalsePositiveRate: 0.030637341732107106
Kappa statistic: 0.7529301052271367
Training time: 1.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.356417359187468
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.8216997199941424
Weighted AreaUnderROC: 0.9854970499594535
Root mean squared error: 0.1242316719810725
Relative absolute error: 22.339427279231696
Root relative squared error: 55.635203207440576
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7828545842510735
Weighted FMeasure: 0.7876346402796894
Iteration time: 22.0
Weighted AreaUnderPRC: 0.8990185399537935
Mean absolute error: 0.022277545209206318
Coverage of cases: 91.52046783625731
Instances selection time: 22.0
Test time: 37.0
Accumulative iteration time: 77.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.026338133334754904
Kappa statistic: 0.79253670511739
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.24869190520162
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8808204597045483
Weighted AreaUnderROC: 0.9878956610771903
Root mean squared error: 0.11110264242486928
Relative absolute error: 18.950570862385508
Root relative squared error: 49.75557351536699
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8334576591504903
Weighted FMeasure: 0.833904286761874
Iteration time: 21.0
Weighted AreaUnderPRC: 0.930094306372463
Mean absolute error: 0.01889807620625718
Coverage of cases: 93.5672514619883
Instances selection time: 20.0
Test time: 37.0
Accumulative iteration time: 98.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.02019336161836037
Kappa statistic: 0.8320156422681289
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 8.187134502924003
Incorrectly Classified Instances: 8.47953216374269
Correctly Classified Instances: 91.52046783625731
Weighted Precision: 0.9266815825295032
Weighted AreaUnderROC: 0.9928001488133048
Root mean squared error: 0.08784619394788323
Relative absolute error: 14.360259052128415
Root relative squared error: 39.34053831325186
Weighted TruePositiveRate: 0.9152046783625731
Weighted MatthewsCorrelation: 0.9073206108454918
Weighted FMeasure: 0.9132076220946583
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9572994852210951
Mean absolute error: 0.014320479941180818
Coverage of cases: 96.19883040935673
Instances selection time: 18.0
Test time: 37.0
Accumulative iteration time: 116.0
Weighted Recall: 0.9152046783625731
Weighted FalsePositiveRate: 0.011777834955524442
Kappa statistic: 0.9068697415865385
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.771622037550048
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9317756349081846
Weighted AreaUnderROC: 0.9932498253243166
Root mean squared error: 0.08787474633243796
Relative absolute error: 13.501351613105374
Root relative squared error: 39.35332505025239
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9110718908504122
Weighted FMeasure: 0.9150107669417156
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9581307053671804
Mean absolute error: 0.013463951747141212
Coverage of cases: 96.19883040935673
Instances selection time: 17.0
Test time: 38.0
Accumulative iteration time: 133.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010826114975773807
Kappa statistic: 0.9100870398016958
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.648507232994803
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.918393805275456
Weighted AreaUnderROC: 0.9931686677745264
Root mean squared error: 0.08787193574129337
Relative absolute error: 13.020112348742908
Root relative squared error: 39.35206637114932
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8976099621319819
Weighted FMeasure: 0.9038029118982306
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9567452294458924
Mean absolute error: 0.012984045555533208
Coverage of cases: 96.19883040935673
Instances selection time: 15.0
Test time: 37.0
Accumulative iteration time: 149.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.012266016859556477
Kappa statistic: 0.8972452256210918
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.571560480147771
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9217437369815497
Weighted AreaUnderROC: 0.9931964459501411
Root mean squared error: 0.0874882321725178
Relative absolute error: 12.658474761419859
Root relative squared error: 39.18023075403309
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9010151541108368
Weighted FMeasure: 0.9059810832087996
Iteration time: 13.0
Weighted AreaUnderPRC: 0.958077229884522
Mean absolute error: 0.012623409734380028
Coverage of cases: 96.19883040935673
Instances selection time: 13.0
Test time: 36.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.011822990100740245
Kappa statistic: 0.9004217189980182
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.3714989227454915
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.9173366422592429
Weighted AreaUnderROC: 0.9923289114306124
Root mean squared error: 0.08835936749014547
Relative absolute error: 12.42672369855757
Root relative squared error: 39.57035502463598
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8977889735616856
Weighted FMeasure: 0.903429127261474
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9534383189407064
Mean absolute error: 0.012392300641220955
Coverage of cases: 96.49122807017544
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.011877012993557383
Kappa statistic: 0.8972114472485465
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.309941520467868
Incorrectly Classified Instances: 8.47953216374269
Correctly Classified Instances: 91.52046783625731
Weighted Precision: 0.922515967337699
Weighted AreaUnderROC: 0.9929838604221369
Root mean squared error: 0.08667849381299454
Relative absolute error: 12.054040715620133
Root relative squared error: 38.817602146862775
Weighted TruePositiveRate: 0.9152046783625731
Weighted MatthewsCorrelation: 0.9066578225626287
Weighted FMeasure: 0.9135150079300492
Iteration time: 9.0
Weighted AreaUnderPRC: 0.957266086829099
Mean absolute error: 0.012020650021117137
Coverage of cases: 96.78362573099415
Instances selection time: 9.0
Test time: 37.0
Accumulative iteration time: 182.0
Weighted Recall: 0.9152046783625731
Weighted FalsePositiveRate: 0.011666906889975074
Kappa statistic: 0.9068277468811063
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.309941520467867
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.9147644281769919
Weighted AreaUnderROC: 0.9919703809684232
Root mean squared error: 0.08731661912394924
Relative absolute error: 12.046321057622754
Root relative squared error: 39.10337654545723
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8972338745217963
Weighted FMeasure: 0.9049256332588033
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9534344102569369
Mean absolute error: 0.01201295174721393
Coverage of cases: 96.19883040935673
Instances selection time: 7.0
Test time: 37.0
Accumulative iteration time: 190.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.012663782014941512
Kappa statistic: 0.8971873062397836
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.186826715912621
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9282902267806388
Weighted AreaUnderROC: 0.9926183771291722
Root mean squared error: 0.08732026970385658
Relative absolute error: 11.859751256998216
Root relative squared error: 39.10501140033547
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9114039836764146
Weighted FMeasure: 0.9175769123765164
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9553573294429005
Mean absolute error: 0.011826898760441537
Coverage of cases: 95.6140350877193
Instances selection time: 5.0
Test time: 37.0
Accumulative iteration time: 196.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010846174934176537
Kappa statistic: 0.9100997014589084
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.171437365343215
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9282902267806388
Weighted AreaUnderROC: 0.9925997444088376
Root mean squared error: 0.08761277641461973
Relative absolute error: 11.918531434963628
Root relative squared error: 39.23600593686017
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9114039836764146
Weighted FMeasure: 0.9175769123765164
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9549344762239947
Mean absolute error: 0.011885516112429205
Coverage of cases: 95.6140350877193
Instances selection time: 4.0
Test time: 37.0
Accumulative iteration time: 201.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010846174934176537
Kappa statistic: 0.9100997014589084
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 7.0791012619267795
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9196155371593968
Weighted AreaUnderROC: 0.9927374751752231
Root mean squared error: 0.0894954881785802
Relative absolute error: 12.135380639714116
Root relative squared error: 40.07914883189371
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.901356852333233
Weighted FMeasure: 0.9080671410866988
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9535322394691139
Mean absolute error: 0.012101764626861827
Coverage of cases: 95.02923976608187
Instances selection time: 2.0
Test time: 37.0
Accumulative iteration time: 203.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.011843050059142973
Kappa statistic: 0.9004768699309101
Training time: 0.0
		
Time end:Wed Nov 01 09.31.19 EET 2017