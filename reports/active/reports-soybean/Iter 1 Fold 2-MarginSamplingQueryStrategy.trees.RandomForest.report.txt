Wed Nov 01 09.34.47 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.34.47 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 26.176879148016624
Incorrectly Classified Instances: 32.25806451612903
Correctly Classified Instances: 67.74193548387096
Weighted Precision: 0.6229553402725057
Weighted AreaUnderROC: 0.8985226585082543
Root mean squared error: 0.16021360940589746
Relative absolute error: 56.81255227992672
Root relative squared error: 71.74914877787887
Weighted TruePositiveRate: 0.6774193548387096
Weighted MatthewsCorrelation: 0.6113189332574338
Weighted FMeasure: 0.6235626050757679
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7338067513623387
Mean absolute error: 0.056655176788847086
Coverage of cases: 84.4574780058651
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6774193548387096
Weighted FalsePositiveRate: 0.03739931500212161
Kappa statistic: 0.6406055379898438
Training time: 5.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 27.303596233986703
Incorrectly Classified Instances: 32.84457478005865
Correctly Classified Instances: 67.15542521994135
Weighted Precision: 0.6472066667403592
Weighted AreaUnderROC: 0.9120967184965122
Root mean squared error: 0.16126545322095195
Relative absolute error: 58.21736012729002
Root relative squared error: 72.22020051098252
Weighted TruePositiveRate: 0.6715542521994134
Weighted MatthewsCorrelation: 0.6212260467254037
Weighted FMeasure: 0.6365845637551778
Iteration time: 9.0
Weighted AreaUnderPRC: 0.732816798172222
Mean absolute error: 0.05805609320173017
Coverage of cases: 88.85630498533725
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6715542521994134
Weighted FalsePositiveRate: 0.03772646277016737
Kappa statistic: 0.6342848387930786
Training time: 7.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 24.571693162525058
Incorrectly Classified Instances: 25.219941348973606
Correctly Classified Instances: 74.7800586510264
Weighted Precision: 0.7457978026101189
Weighted AreaUnderROC: 0.9500656420970759
Root mean squared error: 0.14231714629009662
Relative absolute error: 48.65512147839032
Root relative squared error: 63.734498839868365
Weighted TruePositiveRate: 0.7478005865102639
Weighted MatthewsCorrelation: 0.711572307502058
Weighted FMeasure: 0.7187846645428828
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8358222430775756
Mean absolute error: 0.04852034274853371
Coverage of cases: 92.96187683284458
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7478005865102639
Weighted FalsePositiveRate: 0.02849426776604544
Kappa statistic: 0.7205264311513061
Training time: 8.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 27.720327210989318
Incorrectly Classified Instances: 20.821114369501466
Correctly Classified Instances: 79.17888563049853
Weighted Precision: 0.7827765149895929
Weighted AreaUnderROC: 0.9643698845953118
Root mean squared error: 0.14455928664819448
Relative absolute error: 52.627568603195854
Root relative squared error: 64.73860618587095
Weighted TruePositiveRate: 0.7917888563049853
Weighted MatthewsCorrelation: 0.7621879590866693
Weighted FMeasure: 0.7749967875797579
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8693418863599
Mean absolute error: 0.05248178586468331
Coverage of cases: 95.89442815249267
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7917888563049853
Weighted FalsePositiveRate: 0.023422189156003293
Kappa statistic: 0.7704683352294274
Training time: 15.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 26.639913566908454
Incorrectly Classified Instances: 15.835777126099707
Correctly Classified Instances: 84.1642228739003
Weighted Precision: 0.8708130899763111
Weighted AreaUnderROC: 0.9856274933373168
Root mean squared error: 0.13492612014295804
Relative absolute error: 48.59652799188489
Root relative squared error: 60.42454385777483
Weighted TruePositiveRate: 0.841642228739003
Weighted MatthewsCorrelation: 0.830442607629528
Weighted FMeasure: 0.8338570208116037
Iteration time: 18.0
Weighted AreaUnderPRC: 0.908842361654495
Mean absolute error: 0.048461911570855165
Coverage of cases: 99.12023460410558
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 64.0
Weighted Recall: 0.841642228739003
Weighted FalsePositiveRate: 0.019185347806080548
Kappa statistic: 0.8252941176470588
Training time: 15.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 24.108658743633253
Incorrectly Classified Instances: 17.008797653958943
Correctly Classified Instances: 82.99120234604105
Weighted Precision: 0.8595992323289692
Weighted AreaUnderROC: 0.9829415360958191
Root mean squared error: 0.13178219379013567
Relative absolute error: 45.06165065019128
Root relative squared error: 59.01658581680803
Weighted TruePositiveRate: 0.8299120234604106
Weighted MatthewsCorrelation: 0.8181744756653598
Weighted FMeasure: 0.8248527177963516
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9048579386378895
Mean absolute error: 0.04493682613315514
Coverage of cases: 99.41348973607037
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 86.0
Weighted Recall: 0.8299120234604106
Weighted FalsePositiveRate: 0.02086885154204874
Kappa statistic: 0.8124348007510954
Training time: 20.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 22.673252045068683
Incorrectly Classified Instances: 14.36950146627566
Correctly Classified Instances: 85.63049853372434
Weighted Precision: 0.8685275532443976
Weighted AreaUnderROC: 0.9775162020540888
Root mean squared error: 0.12544058189759139
Relative absolute error: 41.93604462796965
Root relative squared error: 56.17659452732287
Weighted TruePositiveRate: 0.8563049853372434
Weighted MatthewsCorrelation: 0.8415683055691886
Weighted FMeasure: 0.8480757744618789
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9058560389688401
Mean absolute error: 0.04181987829936069
Coverage of cases: 97.36070381231671
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 108.0
Weighted Recall: 0.8563049853372434
Weighted FalsePositiveRate: 0.017350257608376805
Kappa statistic: 0.8416943789140588
Training time: 20.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 22.349127951844405
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8876304322036599
Weighted AreaUnderROC: 0.9862981583106556
Root mean squared error: 0.1195338103513461
Relative absolute error: 40.43070934505904
Root relative squared error: 53.53133965765202
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.866671295157897
Weighted FMeasure: 0.8758154733495598
Iteration time: 24.0
Weighted AreaUnderPRC: 0.943110016959267
Mean absolute error: 0.040318712920280844
Coverage of cases: 99.12023460410558
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.014361703613245104
Kappa statistic: 0.8678906537905489
Training time: 21.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 23.969748417965718
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.8985940518726225
Weighted AreaUnderROC: 0.9828029223832451
Root mean squared error: 0.12062271538182928
Relative absolute error: 41.56976704291546
Root relative squared error: 54.01898867402979
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.8779639252878371
Weighted FMeasure: 0.8857042573339718
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9334374101075721
Mean absolute error: 0.041454615333655666
Coverage of cases: 98.24046920821114
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 158.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.012717901836875068
Kappa statistic: 0.8775618190923435
Training time: 24.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 22.76585892884702
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.8875583326044155
Weighted AreaUnderROC: 0.9881908608880408
Root mean squared error: 0.11797897290453639
Relative absolute error: 39.45105391717491
Root relative squared error: 52.8350301262068
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8605131460585005
Weighted FMeasure: 0.8649145527438238
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9416314580978434
Mean absolute error: 0.03934177121934375
Coverage of cases: 98.82697947214076
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 184.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.014716078750925104
Kappa statistic: 0.8610734757686295
Training time: 24.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 19.771569686680024
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9095993474315938
Weighted AreaUnderROC: 0.9923405335864872
Root mean squared error: 0.11029820970303346
Relative absolute error: 35.398796364105195
Root relative squared error: 49.39532095470864
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.8875986954577575
Weighted FMeasure: 0.8931399192724416
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9525558334752823
Mean absolute error: 0.0353007387564487
Coverage of cases: 99.70674486803519
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 212.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.01387645111825363
Kappa statistic: 0.8901688107462913
Training time: 27.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 19.91048001234759
Incorrectly Classified Instances: 12.316715542521994
Correctly Classified Instances: 87.68328445747801
Weighted Precision: 0.8840738738848147
Weighted AreaUnderROC: 0.9863491145148409
Root mean squared error: 0.11277172727521019
Relative absolute error: 35.98867479643073
Root relative squared error: 50.50304695219979
Weighted TruePositiveRate: 0.8768328445747801
Weighted MatthewsCorrelation: 0.8638984275505073
Weighted FMeasure: 0.870922763069141
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9279282365798822
Mean absolute error: 0.03588898317649633
Coverage of cases: 98.24046920821114
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 243.0
Weighted Recall: 0.8768328445747801
Weighted FalsePositiveRate: 0.013624902678459973
Kappa statistic: 0.8645481628599802
Training time: 30.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 16.499459793177987
Incorrectly Classified Instances: 9.3841642228739
Correctly Classified Instances: 90.6158357771261
Weighted Precision: 0.9078639958097678
Weighted AreaUnderROC: 0.9950903041175096
Root mean squared error: 0.09929272668767808
Relative absolute error: 28.90295995742927
Root relative squared error: 44.46668823012759
Weighted TruePositiveRate: 0.906158357771261
Weighted MatthewsCorrelation: 0.8947355580048559
Weighted FMeasure: 0.9024003203490085
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9602291816795883
Mean absolute error: 0.028822896356439417
Coverage of cases: 99.41348973607037
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 274.0
Weighted Recall: 0.906158357771261
Weighted FalsePositiveRate: 0.011133172818101623
Kappa statistic: 0.8969185134803227
Training time: 30.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 16.514894273807684
Incorrectly Classified Instances: 8.79765395894428
Correctly Classified Instances: 91.20234604105572
Weighted Precision: 0.9076903512478135
Weighted AreaUnderROC: 0.9950487871443836
Root mean squared error: 0.09740988416555241
Relative absolute error: 28.5940595318395
Root relative squared error: 43.62348677710334
Weighted TruePositiveRate: 0.9120234604105572
Weighted MatthewsCorrelation: 0.8971306192746847
Weighted FMeasure: 0.9031605519168868
Iteration time: 35.0
Weighted AreaUnderPRC: 0.9713748843056808
Mean absolute error: 0.02851485161069892
Coverage of cases: 99.70674486803519
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 309.0
Weighted Recall: 0.9120234604105572
Weighted FalsePositiveRate: 0.01133740154947656
Kappa statistic: 0.9031754294638209
Training time: 35.0
		
Time end:Wed Nov 01 09.34.48 EET 2017