Wed Nov 01 09.30.57 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.30.57 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 26.176879148016624
Incorrectly Classified Instances: 32.25806451612903
Correctly Classified Instances: 67.74193548387096
Weighted Precision: 0.6229553402725057
Weighted AreaUnderROC: 0.8985226585082543
Root mean squared error: 0.16021360940589746
Relative absolute error: 56.81255227992672
Root relative squared error: 71.74914877787887
Weighted TruePositiveRate: 0.6774193548387096
Weighted MatthewsCorrelation: 0.6113189332574338
Weighted FMeasure: 0.6235626050757679
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7338067513623387
Mean absolute error: 0.056655176788847086
Coverage of cases: 84.4574780058651
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6774193548387096
Weighted FalsePositiveRate: 0.03739931500212161
Kappa statistic: 0.6406055379898438
Training time: 5.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 27.473375520913677
Incorrectly Classified Instances: 32.25806451612903
Correctly Classified Instances: 67.74193548387096
Weighted Precision: 0.691025708534315
Weighted AreaUnderROC: 0.9232084294329925
Root mean squared error: 0.16452018811207494
Relative absolute error: 61.02884686158945
Root relative squared error: 73.6777824155516
Weighted TruePositiveRate: 0.6774193548387096
Weighted MatthewsCorrelation: 0.6390917193138117
Weighted FMeasure: 0.6512786337327161
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7556105026630132
Mean absolute error: 0.060859791884134054
Coverage of cases: 90.3225806451613
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6774193548387096
Weighted FalsePositiveRate: 0.03415739017531278
Kappa statistic: 0.6429149412156694
Training time: 11.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 26.902299737613774
Incorrectly Classified Instances: 23.46041055718475
Correctly Classified Instances: 76.53958944281526
Weighted Precision: 0.8030276580144848
Weighted AreaUnderROC: 0.973481223151112
Root mean squared error: 0.147513327350949
Relative absolute error: 53.791575727718026
Root relative squared error: 66.06152692065618
Weighted TruePositiveRate: 0.7653958944281525
Weighted MatthewsCorrelation: 0.7516988176180236
Weighted FMeasure: 0.7647003486623605
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8696001199336667
Mean absolute error: 0.05364256859273867
Coverage of cases: 99.12023460410558
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7653958944281525
Weighted FalsePositiveRate: 0.02583376066190848
Kappa statistic: 0.7416103886263107
Training time: 13.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 26.377527396203103
Incorrectly Classified Instances: 16.422287390029325
Correctly Classified Instances: 83.57771260997067
Weighted Precision: 0.8646435452484538
Weighted AreaUnderROC: 0.9728411684750526
Root mean squared error: 0.1424294960723602
Relative absolute error: 52.459309597479084
Root relative squared error: 63.784812925373856
Weighted TruePositiveRate: 0.8357771260997068
Weighted MatthewsCorrelation: 0.8261860804331362
Weighted FMeasure: 0.8349078029968605
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8867979407363901
Mean absolute error: 0.05231399295039512
Coverage of cases: 97.36070381231671
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 56.0
Weighted Recall: 0.8357771260997068
Weighted FalsePositiveRate: 0.018951650631646687
Kappa statistic: 0.8195903559821631
Training time: 15.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 22.73498996758757
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8997011453911338
Weighted AreaUnderROC: 0.9889416203332394
Root mean squared error: 0.12084861273018409
Relative absolute error: 40.92276360845723
Root relative squared error: 54.120153253716495
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8744280137037361
Weighted FMeasure: 0.8813461021678498
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9511701304018638
Mean absolute error: 0.04080940415247849
Coverage of cases: 99.41348973607037
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 74.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.014992127230559213
Kappa statistic: 0.8741959285606176
Training time: 15.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 26.979472140762443
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.8696690863560151
Weighted AreaUnderROC: 0.9795876785450439
Root mean squared error: 0.13146942965982805
Relative absolute error: 46.96998676673097
Root relative squared error: 58.87651931308816
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8413997040009249
Weighted FMeasure: 0.8527322743428045
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9126409946483853
Mean absolute error: 0.046839876000064534
Coverage of cases: 98.82697947214076
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 94.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.0178931421547263
Kappa statistic: 0.8386822086818303
Training time: 18.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 24.818644852600656
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8917252753763016
Weighted AreaUnderROC: 0.9843414430486812
Root mean squared error: 0.12632350526999225
Relative absolute error: 44.603970452711685
Root relative squared error: 56.57199789312161
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8743378813146867
Weighted FMeasure: 0.8847092892196082
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9305470580948766
Mean absolute error: 0.04448041374785692
Coverage of cases: 98.82697947214076
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 114.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.014340820043041576
Kappa statistic: 0.8741899779579403
Training time: 18.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 21.006328137058198
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9016906701618256
Weighted AreaUnderROC: 0.9889077866686921
Root mean squared error: 0.10724164310434965
Relative absolute error: 35.08359580118359
Root relative squared error: 48.02648560762614
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.8888570703432377
Weighted FMeasure: 0.8993363694510125
Iteration time: 23.0
Weighted AreaUnderPRC: 0.956553457419441
Mean absolute error: 0.034986411325280346
Coverage of cases: 98.53372434017595
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 137.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.012027973794929881
Kappa statistic: 0.8905276277523888
Training time: 21.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 22.76585892884703
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8858928570211018
Weighted AreaUnderROC: 0.9852961555221914
Root mean squared error: 0.11996861207991373
Relative absolute error: 40.70398755506006
Root relative squared error: 53.72605878312183
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8715440395131333
Weighted FMeasure: 0.8804641967438379
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9419896274724496
Mean absolute error: 0.040591234126930054
Coverage of cases: 98.82697947214076
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 163.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.013229367285493909
Kappa statistic: 0.874254214691616
Training time: 23.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 20.89828677265011
Incorrectly Classified Instances: 12.903225806451612
Correctly Classified Instances: 87.09677419354838
Weighted Precision: 0.8766120413523383
Weighted AreaUnderROC: 0.9907019850496596
Root mean squared error: 0.11695350958356711
Relative absolute error: 38.36102413810936
Root relative squared error: 52.375792483067016
Weighted TruePositiveRate: 0.8709677419354839
Weighted MatthewsCorrelation: 0.8577292570005138
Weighted FMeasure: 0.8662600782036747
Iteration time: 27.0
Weighted AreaUnderPRC: 0.9397346002424377
Mean absolute error: 0.038254760913350394
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 190.0
Weighted Recall: 0.8709677419354839
Weighted FalsePositiveRate: 0.014371946749592341
Kappa statistic: 0.8580578023745328
Training time: 25.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 18.552245716931623
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8888185159638774
Weighted AreaUnderROC: 0.9858696973605445
Root mean squared error: 0.11190793899157786
Relative absolute error: 34.72918040314416
Root relative squared error: 50.11621293538471
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8719525132730548
Weighted FMeasure: 0.8823286965125576
Iteration time: 29.0
Weighted AreaUnderPRC: 0.940336323473681
Mean absolute error: 0.034632977687346286
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 219.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.015040434051243452
Kappa statistic: 0.8741066103732593
Training time: 28.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 17.950300972372272
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8664436614974856
Weighted AreaUnderROC: 0.9905915913140426
Root mean squared error: 0.1094717259284728
Relative absolute error: 32.356757731368624
Root relative squared error: 49.02519317640473
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8587193433402679
Weighted FMeasure: 0.8681564781119393
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9348327818532053
Mean absolute error: 0.03226712682352578
Coverage of cases: 99.70674486803519
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 250.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.01443727957035709
Kappa statistic: 0.8676505391103496
Training time: 30.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 17.471832072850763
Incorrectly Classified Instances: 9.3841642228739
Correctly Classified Instances: 90.6158357771261
Weighted Precision: 0.9184307136247372
Weighted AreaUnderROC: 0.9923094830997025
Root mean squared error: 0.10089652635772912
Relative absolute error: 30.721010999845436
Root relative squared error: 45.18492472428755
Weighted TruePositiveRate: 0.906158357771261
Weighted MatthewsCorrelation: 0.9000395756881466
Weighted FMeasure: 0.906437557816011
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9622990937593072
Mean absolute error: 0.030635911246383533
Coverage of cases: 99.41348973607037
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 283.0
Weighted Recall: 0.906158357771261
Weighted FalsePositiveRate: 0.009789590050516288
Kappa statistic: 0.8970410628019323
Training time: 32.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 17.348356227812943
Incorrectly Classified Instances: 14.36950146627566
Correctly Classified Instances: 85.63049853372434
Weighted Precision: 0.8711819144588067
Weighted AreaUnderROC: 0.992626994752207
Root mean squared error: 0.10820769048538695
Relative absolute error: 31.83291355663016
Root relative squared error: 48.4591147552095
Weighted TruePositiveRate: 0.8563049853372434
Weighted MatthewsCorrelation: 0.844701970593659
Weighted FMeasure: 0.8513499407641693
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9479803073661891
Mean absolute error: 0.031744733740684095
Coverage of cases: 99.41348973607037
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 317.0
Weighted Recall: 0.8563049853372434
Weighted FalsePositiveRate: 0.015383488742654465
Kappa statistic: 0.8418127769152118
Training time: 34.0
		
Time end:Wed Nov 01 09.30.58 EET 2017