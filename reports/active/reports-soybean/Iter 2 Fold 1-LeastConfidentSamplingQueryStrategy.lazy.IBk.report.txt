Wed Nov 01 09.30.38 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.30.38 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 71.76054170513967
Incorrectly Classified Instances: 19.883040935672515
Correctly Classified Instances: 80.11695906432749
Weighted Precision: 0.7641590782998815
Weighted AreaUnderROC: 0.8844030531899758
Root mean squared error: 0.13574791513402631
Relative absolute error: 37.6284576520421
Root relative squared error: 60.792571838030184
Weighted TruePositiveRate: 0.8011695906432749
Weighted MatthewsCorrelation: 0.7589026413332222
Weighted FMeasure: 0.7709442639927091
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7242242238954315
Mean absolute error: 0.037524223697327636
Coverage of cases: 94.73684210526316
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8011695906432749
Weighted FalsePositiveRate: 0.023355880982947955
Kappa statistic: 0.7804214819850442
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 63.83502616189578
Incorrectly Classified Instances: 17.251461988304094
Correctly Classified Instances: 82.74853801169591
Weighted Precision: 0.8151812291053057
Weighted AreaUnderROC: 0.9108356003807015
Root mean squared error: 0.12523779051863823
Relative absolute error: 31.906847875483866
Root relative squared error: 56.08577759314771
Weighted TruePositiveRate: 0.827485380116959
Weighted MatthewsCorrelation: 0.7957978410437423
Weighted FMeasure: 0.8073133205572567
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7756384768642905
Mean absolute error: 0.03181846325533045
Coverage of cases: 94.15204678362574
Instances selection time: 11.0
Test time: 18.0
Accumulative iteration time: 22.0
Weighted Recall: 0.827485380116959
Weighted FalsePositiveRate: 0.022503753225577773
Kappa statistic: 0.8097635479126598
Training time: 1.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 56.46352723915057
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.8162023553613943
Weighted AreaUnderROC: 0.9326068365341965
Root mean squared error: 0.11796405062665163
Relative absolute error: 28.285282593746786
Root relative squared error: 52.8283474184142
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.807261317924858
Weighted FMeasure: 0.8215619083240974
Iteration time: 12.0
Weighted AreaUnderPRC: 0.812316334283365
Mean absolute error: 0.028206930010384858
Coverage of cases: 95.90643274853801
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.01958715558799091
Kappa statistic: 0.8231411485952836
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 48.96891351184979
Incorrectly Classified Instances: 16.666666666666668
Correctly Classified Instances: 83.33333333333333
Weighted Precision: 0.813888242906075
Weighted AreaUnderROC: 0.927356520313825
Root mean squared error: 0.11904263637533852
Relative absolute error: 26.807224543191058
Root relative squared error: 53.31137510650638
Weighted TruePositiveRate: 0.8333333333333334
Weighted MatthewsCorrelation: 0.8007488375944537
Weighted FMeasure: 0.8143430343986177
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8132003637505848
Mean absolute error: 0.026732966303459463
Coverage of cases: 93.85964912280701
Instances selection time: 26.0
Test time: 24.0
Accumulative iteration time: 60.0
Weighted Recall: 0.8333333333333334
Weighted FalsePositiveRate: 0.020862289313980695
Kappa statistic: 0.8166478555304739
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 43.36718990458612
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8267610392283489
Weighted AreaUnderROC: 0.9366952622137089
Root mean squared error: 0.11544537043370172
Relative absolute error: 25.087886805801105
Root relative squared error: 51.700396050499954
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8155434758546111
Weighted FMeasure: 0.8293812385123853
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8241330331168137
Mean absolute error: 0.02501839127448332
Coverage of cases: 94.73684210526316
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 74.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.01941101241882301
Kappa statistic: 0.8295723795554553
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 36.857494613727425
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8267610392283489
Weighted AreaUnderROC: 0.9373627019582393
Root mean squared error: 0.11583011442541291
Relative absolute error: 24.311782651382345
Root relative squared error: 51.87269760468781
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8155434758546111
Weighted FMeasure: 0.8293812385123853
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8262683958431559
Mean absolute error: 0.02424443699306848
Coverage of cases: 94.73684210526316
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 88.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.01941101241882301
Kappa statistic: 0.8295723795554553
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 31.455832563865826
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8316343335753272
Weighted AreaUnderROC: 0.9401068366496582
Root mean squared error: 0.11389515802777261
Relative absolute error: 22.815404781093378
Root relative squared error: 51.006157770975705
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8215493004957386
Weighted FMeasure: 0.8350313751294743
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8317392987135033
Mean absolute error: 0.022752204213832932
Coverage of cases: 95.02923976608187
Instances selection time: 14.0
Test time: 31.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.019234869249655114
Kappa statistic: 0.8358986903507452
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 27.854724530624793
Incorrectly Classified Instances: 15.2046783625731
Correctly Classified Instances: 84.7953216374269
Weighted Precision: 0.8294907169580266
Weighted AreaUnderROC: 0.944453123381752
Root mean squared error: 0.1127897505506413
Relative absolute error: 21.897692702574712
Root relative squared error: 50.51111839304159
Weighted TruePositiveRate: 0.847953216374269
Weighted MatthewsCorrelation: 0.8187125170037011
Weighted FMeasure: 0.8323769522347301
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8360541128133273
Mean absolute error: 0.021837034274035917
Coverage of cases: 95.32163742690058
Instances selection time: 13.0
Test time: 33.0
Accumulative iteration time: 115.0
Weighted Recall: 0.847953216374269
Weighted FalsePositiveRate: 0.01968927101984636
Kappa statistic: 0.8326243270714905
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 23.56109572176063
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8268735245740934
Weighted AreaUnderROC: 0.9483352166335364
Root mean squared error: 0.11228485589710226
Relative absolute error: 20.902012222645105
Root relative squared error: 50.28500925195014
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8155963287912438
Weighted FMeasure: 0.8291795304326235
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8384814214497189
Mean absolute error: 0.020844111911779234
Coverage of cases: 95.6140350877193
Instances selection time: 12.0
Test time: 37.0
Accumulative iteration time: 127.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.019777342604430304
Kappa statistic: 0.8294601358598499
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 20.036934441366636
Incorrectly Classified Instances: 15.497076023391813
Correctly Classified Instances: 84.50292397660819
Weighted Precision: 0.8281109175735002
Weighted AreaUnderROC: 0.9498729481589082
Root mean squared error: 0.1123181758111015
Relative absolute error: 20.382706966084957
Root relative squared error: 50.29993105213717
Weighted TruePositiveRate: 0.8450292397660819
Weighted MatthewsCorrelation: 0.8161843936620414
Weighted FMeasure: 0.8295693939803818
Iteration time: 11.0
Weighted AreaUnderPRC: 0.840095444753948
Mean absolute error: 0.020326245173935317
Coverage of cases: 94.15204678362574
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8450292397660819
Weighted FalsePositiveRate: 0.019610922486087114
Kappa statistic: 0.8294890126429862
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 17.31301939058179
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8926913603486107
Weighted AreaUnderROC: 0.9662726986748003
Root mean squared error: 0.09399526965229558
Relative absolute error: 15.895067938304255
Root relative squared error: 42.09430529471081
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8748603535356912
Weighted FMeasure: 0.8871318116790669
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8946504344777071
Mean absolute error: 0.01585103727919552
Coverage of cases: 93.27485380116958
Instances selection time: 9.0
Test time: 40.0
Accumulative iteration time: 147.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.01532867971700224
Kappa statistic: 0.8778846876644365
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 14.481378885811035
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8961463056943905
Weighted AreaUnderROC: 0.9639577251724457
Root mean squared error: 0.09375929858778344
Relative absolute error: 15.610271865863975
Root relative squared error: 41.988629359453256
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8755700791379762
Weighted FMeasure: 0.8865189957712807
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8955899457791461
Mean absolute error: 0.01556703011554316
Coverage of cases: 93.27485380116958
Instances selection time: 7.0
Test time: 42.0
Accumulative iteration time: 154.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.015441631524231158
Kappa statistic: 0.8778272886231597
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 11.60357032933212
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.8985974767865035
Weighted AreaUnderROC: 0.9632704760439054
Root mean squared error: 0.09364049717315834
Relative absolute error: 15.340067802793394
Root relative squared error: 41.935426011719144
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.8789193421842864
Weighted FMeasure: 0.8892897577690576
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8955129615286962
Mean absolute error: 0.015297574540181913
Coverage of cases: 93.27485380116958
Instances selection time: 5.0
Test time: 44.0
Accumulative iteration time: 160.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.014620899568432618
Kappa statistic: 0.8810826050183254
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 8.895044629116649
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.9080397627074196
Weighted AreaUnderROC: 0.9636349351181248
Root mean squared error: 0.09181609294379509
Relative absolute error: 15.223403390603439
Root relative squared error: 41.118395230320644
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8869967696185671
Weighted FMeasure: 0.8954990651231252
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8967976797794517
Mean absolute error: 0.015181233298108826
Coverage of cases: 94.15204678362574
Instances selection time: 3.0
Test time: 47.0
Accumulative iteration time: 163.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.013712096028050132
Kappa statistic: 0.887510572314632
Training time: 0.0
		
Time end:Wed Nov 01 09.30.39 EET 2017