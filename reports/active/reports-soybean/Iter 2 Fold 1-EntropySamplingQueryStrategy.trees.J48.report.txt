Wed Nov 01 09.23.12 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.23.12 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.556786703601121
Incorrectly Classified Instances: 35.08771929824562
Correctly Classified Instances: 64.91228070175438
Weighted Precision: 0.6182756846502959
Weighted AreaUnderROC: 0.8648904275912963
Root mean squared error: 0.17665014594757888
Relative absolute error: 42.397150539748694
Root relative squared error: 79.10999352817967
Weighted TruePositiveRate: 0.6491228070175439
Weighted MatthewsCorrelation: 0.598801853933033
Weighted FMeasure: 0.6078153912539709
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5649928276621184
Mean absolute error: 0.042279706909445054
Coverage of cases: 76.90058479532163
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6491228070175439
Weighted FalsePositiveRate: 0.02428241829333246
Kappa statistic: 0.6167959886831564
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 10.095413973530347
Incorrectly Classified Instances: 28.65497076023392
Correctly Classified Instances: 71.34502923976608
Weighted Precision: 0.7016242320623799
Weighted AreaUnderROC: 0.8713978650611917
Root mean squared error: 0.16406078180781827
Relative absolute error: 36.16798084087878
Root relative squared error: 73.47204451728042
Weighted TruePositiveRate: 0.7134502923976608
Weighted MatthewsCorrelation: 0.6713782807571855
Weighted FMeasure: 0.6788404469674377
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6274701659672615
Mean absolute error: 0.036067792528300495
Coverage of cases: 76.3157894736842
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7134502923976608
Weighted FalsePositiveRate: 0.021853100607487564
Kappa statistic: 0.6864704066455252
Training time: 5.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 12.665435518621177
Incorrectly Classified Instances: 34.21052631578947
Correctly Classified Instances: 65.78947368421052
Weighted Precision: 0.644519214872223
Weighted AreaUnderROC: 0.867108980805246
Root mean squared error: 0.16938885142845075
Relative absolute error: 44.06454349354694
Root relative squared error: 75.85813681822316
Weighted TruePositiveRate: 0.6578947368421053
Weighted MatthewsCorrelation: 0.610626612720051
Weighted FMeasure: 0.6214878801679553
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6168339590853109
Mean absolute error: 0.04394248104619679
Coverage of cases: 77.48538011695906
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6578947368421053
Weighted FalsePositiveRate: 0.02997042470228489
Kappa statistic: 0.6253300623607184
Training time: 3.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 7.648507232994787
Incorrectly Classified Instances: 24.853801169590643
Correctly Classified Instances: 75.14619883040936
Weighted Precision: 0.7304771509022054
Weighted AreaUnderROC: 0.8933474933386145
Root mean squared error: 0.1508141646994625
Relative absolute error: 28.08948087861236
Root relative squared error: 67.53975508671705
Weighted TruePositiveRate: 0.7514619883040936
Weighted MatthewsCorrelation: 0.7172135990958007
Weighted FMeasure: 0.7293838838632838
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6844899841808231
Mean absolute error: 0.028011670682272963
Coverage of cases: 80.4093567251462
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7514619883040936
Weighted FalsePositiveRate: 0.01701619732605392
Kappa statistic: 0.7291025999440872
Training time: 7.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 9.40289319790706
Incorrectly Classified Instances: 22.22222222222222
Correctly Classified Instances: 77.77777777777777
Weighted Precision: 0.8350564333762628
Weighted AreaUnderROC: 0.9054347688301779
Root mean squared error: 0.14819047978756886
Relative absolute error: 29.640734262896608
Root relative squared error: 66.36477900454908
Weighted TruePositiveRate: 0.7777777777777778
Weighted MatthewsCorrelation: 0.7700310451666145
Weighted FMeasure: 0.7708408174751696
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7154772316900341
Mean absolute error: 0.029558626965769733
Coverage of cases: 82.74853801169591
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7777777777777778
Weighted FalsePositiveRate: 0.0152365733167513
Kappa statistic: 0.757602887278628
Training time: 8.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 8.079409048938157
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8855531542221092
Weighted AreaUnderROC: 0.9328334870728039
Root mean squared error: 0.1242841722713622
Relative absolute error: 20.20592964877514
Root relative squared error: 55.6587146379167
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8476871207446433
Weighted FMeasure: 0.8549965066848605
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8050336501288424
Mean absolute error: 0.020149957544485078
Coverage of cases: 87.71929824561404
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.012021484459993355
Kappa statistic: 0.8373493975903614
Training time: 9.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.402277623884274
Incorrectly Classified Instances: 16.666666666666668
Correctly Classified Instances: 83.33333333333333
Weighted Precision: 0.8651000029718565
Weighted AreaUnderROC: 0.9284666082553497
Root mean squared error: 0.12647825204917987
Relative absolute error: 20.542925255776808
Root relative squared error: 56.641298807844166
Weighted TruePositiveRate: 0.8333333333333334
Weighted MatthewsCorrelation: 0.828059295393482
Weighted FMeasure: 0.8371207081004512
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7877106567224053
Mean absolute error: 0.02048601964565018
Coverage of cases: 86.84210526315789
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 72.0
Weighted Recall: 0.8333333333333334
Weighted FalsePositiveRate: 0.013397641249548765
Kappa statistic: 0.8181495923431408
Training time: 10.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 11.080332409972314
Incorrectly Classified Instances: 24.853801169590643
Correctly Classified Instances: 75.14619883040936
Weighted Precision: 0.7420640275903434
Weighted AreaUnderROC: 0.9157036083024055
Root mean squared error: 0.14736585277976474
Relative absolute error: 35.80396001123664
Root relative squared error: 65.99548275007609
Weighted TruePositiveRate: 0.7514619883040936
Weighted MatthewsCorrelation: 0.7105098533893848
Weighted FMeasure: 0.7334375564939761
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6899275075943417
Mean absolute error: 0.03570478006660749
Coverage of cases: 88.01169590643275
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 89.0
Weighted Recall: 0.7514619883040936
Weighted FalsePositiveRate: 0.03254961880068541
Kappa statistic: 0.7269550842522496
Training time: 10.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 9.172052939365946
Incorrectly Classified Instances: 22.22222222222222
Correctly Classified Instances: 77.77777777777777
Weighted Precision: 0.7949556219153083
Weighted AreaUnderROC: 0.9196020434369694
Root mean squared error: 0.1433237443702197
Relative absolute error: 30.57013618489613
Root relative squared error: 64.18528798117848
Weighted TruePositiveRate: 0.7777777777777778
Weighted MatthewsCorrelation: 0.7569199268721106
Weighted FMeasure: 0.7785234658271567
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7486857405023943
Mean absolute error: 0.030485454367209702
Coverage of cases: 87.13450292397661
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7777777777777778
Weighted FalsePositiveRate: 0.024453044507675784
Kappa statistic: 0.7569341837020966
Training time: 12.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 9.710680209295159
Incorrectly Classified Instances: 16.374269005847953
Correctly Classified Instances: 83.62573099415205
Weighted Precision: 0.8334651293100046
Weighted AreaUnderROC: 0.9426307740259126
Root mean squared error: 0.1282862045249493
Relative absolute error: 25.575863490625622
Root relative squared error: 57.45096192977455
Weighted TruePositiveRate: 0.8362573099415205
Weighted MatthewsCorrelation: 0.8165227106282112
Weighted FMeasure: 0.827866623269893
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7842036722045471
Mean absolute error: 0.025505016223338797
Coverage of cases: 88.88888888888889
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 119.0
Weighted Recall: 0.8362573099415205
Weighted FalsePositiveRate: 0.015878287830333116
Kappa statistic: 0.8204000487635623
Training time: 10.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 10.264696829793774
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8394182778161705
Weighted AreaUnderROC: 0.9606961602745582
Root mean squared error: 0.11618586447811464
Relative absolute error: 23.153375926991743
Root relative squared error: 52.03201467864729
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8340370870781104
Weighted FMeasure: 0.8444614344735719
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8183439419539943
Mean absolute error: 0.023089239151570913
Coverage of cases: 92.10526315789474
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.015540045339268815
Kappa statistic: 0.8455357226869408
Training time: 7.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 10.772545398584164
Incorrectly Classified Instances: 14.035087719298245
Correctly Classified Instances: 85.96491228070175
Weighted Precision: 0.8451515004339677
Weighted AreaUnderROC: 0.9650268536505973
Root mean squared error: 0.11266794499829945
Relative absolute error: 22.41022591940283
Root relative squared error: 50.456569689411765
Weighted TruePositiveRate: 0.8596491228070176
Weighted MatthewsCorrelation: 0.8293165609953732
Weighted FMeasure: 0.8355485000564439
Iteration time: 19.0
Weighted AreaUnderPRC: 0.8173861584343189
Mean absolute error: 0.022348147731260638
Coverage of cases: 93.27485380116958
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 151.0
Weighted Recall: 0.8596491228070176
Weighted FalsePositiveRate: 0.01737119506929015
Kappa statistic: 0.8452167681834468
Training time: 18.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 8.971991381963681
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8577221222345662
Weighted AreaUnderROC: 0.9582933385231843
Root mean squared error: 0.10797446665093562
Relative absolute error: 19.606169341828597
Root relative squared error: 48.354669123788746
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8541156741562635
Weighted FMeasure: 0.8644811935075866
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8445993242157138
Mean absolute error: 0.01955185862343035
Coverage of cases: 93.27485380116958
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 167.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.014034508844644416
Kappa statistic: 0.8649187481191694
Training time: 11.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 9.633733456448134
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.8989557643134817
Weighted AreaUnderROC: 0.9710318789356567
Root mean squared error: 0.09827208119899146
Relative absolute error: 17.66002803030984
Root relative squared error: 44.00960817751033
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.8797807250536799
Weighted FMeasure: 0.8876373303867049
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8704221640425287
Mean absolute error: 0.01761110828507368
Coverage of cases: 95.6140350877193
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 191.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.01292049522592817
Kappa statistic: 0.8811138878971796
Training time: 21.0
		
Time end:Wed Nov 01 09.23.13 EET 2017