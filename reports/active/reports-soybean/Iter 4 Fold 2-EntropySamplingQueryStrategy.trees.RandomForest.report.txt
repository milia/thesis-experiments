Wed Nov 01 09.23.28 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.23.28 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 28.399444358697338
Incorrectly Classified Instances: 29.325513196480937
Correctly Classified Instances: 70.67448680351906
Weighted Precision: 0.6993662598711304
Weighted AreaUnderROC: 0.9430210469599806
Root mean squared error: 0.15553533723183482
Relative absolute error: 55.7066242780521
Root relative squared error: 69.65405805815207
Weighted TruePositiveRate: 0.7067448680351907
Weighted MatthewsCorrelation: 0.654257683007325
Weighted FMeasure: 0.663499164300974
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7800972516233888
Mean absolute error: 0.055552312299443025
Coverage of cases: 94.72140762463343
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7067448680351907
Weighted FalsePositiveRate: 0.03666586335360625
Kappa statistic: 0.6731430981433378
Training time: 6.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 31.6098163296805
Incorrectly Classified Instances: 31.671554252199414
Correctly Classified Instances: 68.32844574780059
Weighted Precision: 0.7097556449043672
Weighted AreaUnderROC: 0.9382713358622032
Root mean squared error: 0.16267709074654957
Relative absolute error: 61.14255670995492
Root relative squared error: 72.85238020670323
Weighted TruePositiveRate: 0.6832844574780058
Weighted MatthewsCorrelation: 0.6612389884997415
Weighted FMeasure: 0.6734162889183969
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7758484230599229
Mean absolute error: 0.060973186746769985
Coverage of cases: 94.42815249266862
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6832844574780058
Weighted FalsePositiveRate: 0.025923117912643404
Kappa statistic: 0.6526348553588439
Training time: 10.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 28.183361629881148
Incorrectly Classified Instances: 29.325513196480937
Correctly Classified Instances: 70.67448680351906
Weighted Precision: 0.7267461773499743
Weighted AreaUnderROC: 0.9369680542221115
Root mean squared error: 0.1612469636102345
Relative absolute error: 59.130728171517546
Root relative squared error: 72.21192022920663
Weighted TruePositiveRate: 0.7067448680351907
Weighted MatthewsCorrelation: 0.6809916748085693
Weighted FMeasure: 0.691272330429316
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7616894056260096
Mean absolute error: 0.05896693114057203
Coverage of cases: 92.37536656891496
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7067448680351907
Weighted FalsePositiveRate: 0.027689461262647652
Kappa statistic: 0.6760772095144009
Training time: 11.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 27.61228584658122
Incorrectly Classified Instances: 24.633431085043988
Correctly Classified Instances: 75.366568914956
Weighted Precision: 0.7779963386490107
Weighted AreaUnderROC: 0.9595863513992311
Root mean squared error: 0.14814368445972073
Relative absolute error: 53.28253494944596
Root relative squared error: 66.34382245190454
Weighted TruePositiveRate: 0.7536656891495601
Weighted MatthewsCorrelation: 0.7343087110643284
Weighted FMeasure: 0.7411781074347522
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8303006324274034
Mean absolute error: 0.053134937899724975
Coverage of cases: 95.89442815249267
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7536656891495601
Weighted FalsePositiveRate: 0.020904994969451657
Kappa statistic: 0.7289502072332936
Training time: 15.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 29.078561506405293
Incorrectly Classified Instances: 29.912023460410555
Correctly Classified Instances: 70.08797653958945
Weighted Precision: 0.7342160824770113
Weighted AreaUnderROC: 0.9460933798562762
Root mean squared error: 0.156942041214484
Relative absolute error: 57.44242238138688
Root relative squared error: 70.28402834414585
Weighted TruePositiveRate: 0.7008797653958945
Weighted MatthewsCorrelation: 0.6745382272478311
Weighted FMeasure: 0.6825653618510724
Iteration time: 20.0
Weighted AreaUnderPRC: 0.7850730214679116
Mean absolute error: 0.05728330209778244
Coverage of cases: 92.37536656891496
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 73.0
Weighted Recall: 0.7008797653958945
Weighted FalsePositiveRate: 0.029850900163379147
Kappa statistic: 0.6693411921285294
Training time: 18.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 24.86494829448989
Incorrectly Classified Instances: 14.36950146627566
Correctly Classified Instances: 85.63049853372434
Weighted Precision: 0.868927311979351
Weighted AreaUnderROC: 0.981858602259469
Root mean squared error: 0.12866552928738847
Relative absolute error: 45.20059237145777
Root relative squared error: 57.62083656724328
Weighted TruePositiveRate: 0.8563049853372434
Weighted MatthewsCorrelation: 0.8441074900643005
Weighted FMeasure: 0.8550846318900805
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9227343811586726
Mean absolute error: 0.04507538297430732
Coverage of cases: 98.24046920821114
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 95.0
Weighted Recall: 0.8563049853372434
Weighted FalsePositiveRate: 0.01705046094492217
Kappa statistic: 0.841839728905969
Training time: 20.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 22.93563821577407
Incorrectly Classified Instances: 13.489736070381232
Correctly Classified Instances: 86.51026392961877
Weighted Precision: 0.8819260923687091
Weighted AreaUnderROC: 0.9817700364844626
Root mean squared error: 0.12436095510247407
Relative absolute error: 42.713151887966774
Root relative squared error: 55.69310062293671
Weighted TruePositiveRate: 0.8651026392961877
Weighted MatthewsCorrelation: 0.8570103825805098
Weighted FMeasure: 0.864057768854629
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9201244335821833
Mean absolute error: 0.04259483290766808
Coverage of cases: 97.94721407624634
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 118.0
Weighted Recall: 0.8651026392961877
Weighted FalsePositiveRate: 0.012636830716890656
Kappa statistic: 0.852049574616589
Training time: 21.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 21.315017749652718
Incorrectly Classified Instances: 10.850439882697946
Correctly Classified Instances: 89.14956011730206
Weighted Precision: 0.8995795782334981
Weighted AreaUnderROC: 0.99111471591953
Root mean squared error: 0.1168415723193935
Relative absolute error: 38.675280565306025
Root relative squared error: 52.32566313730942
Weighted TruePositiveRate: 0.8914956011730205
Weighted MatthewsCorrelation: 0.8820950084303413
Weighted FMeasure: 0.892373878846517
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9510352394806557
Mean absolute error: 0.038568146824128235
Coverage of cases: 99.70674486803519
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 142.0
Weighted Recall: 0.8914956011730205
Weighted FalsePositiveRate: 0.011996168557807557
Kappa statistic: 0.8810076203410291
Training time: 22.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 19.879611051088126
Incorrectly Classified Instances: 11.730205278592376
Correctly Classified Instances: 88.26979472140762
Weighted Precision: 0.8869287960576158
Weighted AreaUnderROC: 0.9803041169777345
Root mean squared error: 0.11406305140474984
Relative absolute error: 35.728749352525625
Root relative squared error: 51.08134618304774
Weighted TruePositiveRate: 0.8826979472140762
Weighted MatthewsCorrelation: 0.870432790979082
Weighted FMeasure: 0.8804234377017739
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9209909904558098
Mean absolute error: 0.035629777747671294
Coverage of cases: 98.24046920821114
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 167.0
Weighted Recall: 0.8826979472140762
Weighted FalsePositiveRate: 0.013142671851778627
Kappa statistic: 0.8711882974001568
Training time: 24.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 21.500231517209425
Incorrectly Classified Instances: 13.489736070381232
Correctly Classified Instances: 86.51026392961877
Weighted Precision: 0.8863153317845399
Weighted AreaUnderROC: 0.9796942754405104
Root mean squared error: 0.12433678807536633
Relative absolute error: 41.253185251196534
Root relative squared error: 55.68227779939568
Weighted TruePositiveRate: 0.8651026392961877
Weighted MatthewsCorrelation: 0.8568836554909264
Weighted FMeasure: 0.8650163776325196
Iteration time: 28.0
Weighted AreaUnderPRC: 0.9035760496794631
Mean absolute error: 0.04113891049980854
Coverage of cases: 98.24046920821114
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 195.0
Weighted Recall: 0.8651026392961877
Weighted FalsePositiveRate: 0.015396478748458188
Kappa statistic: 0.8516956764269304
Training time: 26.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 20.250038586201583
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.881090776604597
Weighted AreaUnderROC: 0.9817656153429234
Root mean squared error: 0.1159238557241117
Relative absolute error: 37.29299924763326
Root relative squared error: 51.914678173079665
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8612012146149212
Weighted FMeasure: 0.870012114336847
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9297454724688738
Mean absolute error: 0.03718969454057643
Coverage of cases: 98.24046920821114
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 224.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.01472336471822419
Kappa statistic: 0.8612942589842308
Training time: 27.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 18.505942275042457
Incorrectly Classified Instances: 13.782991202346041
Correctly Classified Instances: 86.21700879765396
Weighted Precision: 0.8728046489190184
Weighted AreaUnderROC: 0.9847549524741334
Root mean squared error: 0.11606312619701556
Relative absolute error: 35.16470261706046
Root relative squared error: 51.97704826709227
Weighted TruePositiveRate: 0.8621700879765396
Weighted MatthewsCorrelation: 0.8506899228319085
Weighted FMeasure: 0.8608302164460304
Iteration time: 48.0
Weighted AreaUnderPRC: 0.9248251663471885
Mean absolute error: 0.035067293468537056
Coverage of cases: 98.82697947214076
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 272.0
Weighted Recall: 0.8621700879765396
Weighted FalsePositiveRate: 0.014614263643994535
Kappa statistic: 0.8486305251227805
Training time: 47.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 17.888563049853385
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9039127803784186
Weighted AreaUnderROC: 0.9870662028646939
Root mean squared error: 0.10951080460758895
Relative absolute error: 32.56139611098325
Root relative squared error: 49.04269394910652
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.885268337167526
Weighted FMeasure: 0.8944981142896792
Iteration time: 30.0
Weighted AreaUnderPRC: 0.9392293602853846
Mean absolute error: 0.03247119833782292
Coverage of cases: 98.53372434017595
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 302.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.012486264562013183
Kappa statistic: 0.8840749414519906
Training time: 30.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 16.592066676956332
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.8995229948686397
Weighted AreaUnderROC: 0.9878498351418703
Root mean squared error: 0.10562670348156782
Relative absolute error: 29.40281687768119
Root relative squared error: 47.30326026059143
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8834800888555997
Weighted FMeasure: 0.8931383145878283
Iteration time: 34.0
Weighted AreaUnderPRC: 0.9450351413120474
Mean absolute error: 0.029321368631482887
Coverage of cases: 98.53372434017595
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 336.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.01214672894493239
Kappa statistic: 0.8841504270277922
Training time: 33.0
		
Time end:Wed Nov 01 09.23.29 EET 2017