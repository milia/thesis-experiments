Wed Nov 01 09.19.32 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.19.32 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.17205293936599
Incorrectly Classified Instances: 30.994152046783626
Correctly Classified Instances: 69.00584795321637
Weighted Precision: 0.6627661574856866
Weighted AreaUnderROC: 0.927088996691525
Root mean squared error: 0.16215389848052442
Relative absolute error: 34.736530450992504
Root relative squared error: 72.61807676722844
Weighted TruePositiveRate: 0.6900584795321637
Weighted MatthewsCorrelation: 0.6343856566752706
Weighted FMeasure: 0.6442151113362783
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7770689541847251
Mean absolute error: 0.03464030737495128
Coverage of cases: 80.99415204678363
Instances selection time: 34.0
Test time: 50.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6900584795321637
Weighted FalsePositiveRate: 0.0414627508543618
Kappa statistic: 0.654387369866148
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 7.5869498307171614
Incorrectly Classified Instances: 23.391812865497077
Correctly Classified Instances: 76.60818713450293
Weighted Precision: 0.7253803533504656
Weighted AreaUnderROC: 0.9393195769208693
Root mean squared error: 0.1434916977733412
Relative absolute error: 26.64986788063639
Root relative squared error: 64.2605032750165
Weighted TruePositiveRate: 0.7660818713450293
Weighted MatthewsCorrelation: 0.7143263980333326
Weighted FMeasure: 0.7271031240032689
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7961251745774506
Mean absolute error: 0.0265760455319368
Coverage of cases: 83.62573099415205
Instances selection time: 48.0
Test time: 79.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7660818713450293
Weighted FalsePositiveRate: 0.03434122202902939
Kappa statistic: 0.7400524455592293
Training time: 1.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 7.987072945521712
Incorrectly Classified Instances: 20.46783625730994
Correctly Classified Instances: 79.53216374269006
Weighted Precision: 0.8326080968839258
Weighted AreaUnderROC: 0.9821177249666104
Root mean squared error: 0.13039193459111117
Relative absolute error: 23.50719397258475
Root relative squared error: 58.393980068891146
Weighted TruePositiveRate: 0.7953216374269005
Weighted MatthewsCorrelation: 0.7739623416943355
Weighted FMeasure: 0.7759593656315862
Iteration time: 24.0
Weighted AreaUnderPRC: 0.8977753773552546
Mean absolute error: 0.023442077091774476
Coverage of cases: 89.76608187134502
Instances selection time: 23.0
Test time: 39.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7953216374269005
Weighted FalsePositiveRate: 0.029819878007508755
Kappa statistic: 0.7731622733044021
Training time: 1.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.01785164666053
Incorrectly Classified Instances: 14.32748538011696
Correctly Classified Instances: 85.67251461988305
Weighted Precision: 0.8763253624076734
Weighted AreaUnderROC: 0.9891703552631794
Root mean squared error: 0.10937017288333421
Relative absolute error: 18.30243995036317
Root relative squared error: 48.97971424005522
Weighted TruePositiveRate: 0.8567251461988304
Weighted MatthewsCorrelation: 0.8373542643123772
Weighted FMeasure: 0.8401536971814032
Iteration time: 22.0
Weighted AreaUnderPRC: 0.926825701691949
Mean absolute error: 0.018251740670722435
Coverage of cases: 92.69005847953217
Instances selection time: 21.0
Test time: 39.0
Accumulative iteration time: 129.0
Weighted Recall: 0.8567251461988304
Weighted FalsePositiveRate: 0.019798158840656423
Kappa statistic: 0.8418265736641907
Training time: 1.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.940904893813506
Incorrectly Classified Instances: 8.47953216374269
Correctly Classified Instances: 91.52046783625731
Weighted Precision: 0.9275122517995325
Weighted AreaUnderROC: 0.9922439319230839
Root mean squared error: 0.08738672595962857
Relative absolute error: 13.424076462157855
Root relative squared error: 39.134772790770946
Weighted TruePositiveRate: 0.9152046783625731
Weighted MatthewsCorrelation: 0.9076360028983793
Weighted FMeasure: 0.9125805603046316
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9507128489883474
Mean absolute error: 0.013386890654783573
Coverage of cases: 96.49122807017544
Instances selection time: 20.0
Test time: 39.0
Accumulative iteration time: 150.0
Weighted Recall: 0.9152046783625731
Weighted FalsePositiveRate: 0.010914186560357756
Kappa statistic: 0.9069195611573584
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.8024007386888625
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.9122189386121273
Weighted AreaUnderROC: 0.9929774340614862
Root mean squared error: 0.0880970092512527
Relative absolute error: 13.510579084039527
Root relative squared error: 39.4528620077492
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8904973382807291
Weighted FMeasure: 0.8967689873188837
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9547653777547856
Mean absolute error: 0.013473153657214052
Coverage of cases: 96.19883040935673
Instances selection time: 18.0
Test time: 52.0
Accumulative iteration time: 169.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.013174820399938964
Kappa statistic: 0.89076151287977
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.725453985841828
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9227781944338962
Weighted AreaUnderROC: 0.9929324590511753
Root mean squared error: 0.0872946225258173
Relative absolute error: 12.892761475294819
Root relative squared error: 39.09352571444589
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9016919152850346
Weighted FMeasure: 0.9059888723430217
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9538877753205092
Mean absolute error: 0.012857047454587744
Coverage of cases: 96.19883040935673
Instances selection time: 15.0
Test time: 38.0
Accumulative iteration time: 185.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.01145665991513295
Kappa statistic: 0.9004329410880814
Training time: 1.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.4330563250231165
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.919206061511169
Weighted AreaUnderROC: 0.9931025587114989
Root mean squared error: 0.08782246519302456
Relative absolute error: 12.435687313994027
Root relative squared error: 39.32991176305439
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8980990745266203
Weighted FMeasure: 0.9033788826170116
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9567440031980402
Mean absolute error: 0.012401239426697755
Coverage of cases: 96.49122807017544
Instances selection time: 13.0
Test time: 38.0
Accumulative iteration time: 199.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.011911061685324196
Kappa statistic: 0.8972442608328247
Training time: 1.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.494613727300741
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9185965756141193
Weighted AreaUnderROC: 0.9932884592255213
Root mean squared error: 0.08668433200612428
Relative absolute error: 12.256712638774538
Root relative squared error: 38.820216689965655
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9008336738651308
Weighted FMeasure: 0.907757558061146
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9594920670699969
Mean absolute error: 0.012222760526201864
Coverage of cases: 96.78362573099415
Instances selection time: 11.0
Test time: 38.0
Accumulative iteration time: 211.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.012191344028027086
Kappa statistic: 0.900430135802701
Training time: 1.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.325330871037273
Incorrectly Classified Instances: 8.771929824561404
Correctly Classified Instances: 91.2280701754386
Weighted Precision: 0.9195919907295119
Weighted AreaUnderROC: 0.9923682797139409
Root mean squared error: 0.08706444362060922
Relative absolute error: 11.989905246125065
Root relative squared error: 38.99044370676536
Weighted TruePositiveRate: 0.9122807017543859
Weighted MatthewsCorrelation: 0.9036253639428641
Weighted FMeasure: 0.9104796417367885
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9557559209275723
Mean absolute error: 0.011956692212202386
Coverage of cases: 96.49122807017544
Instances selection time: 9.0
Test time: 49.0
Accumulative iteration time: 221.0
Weighted Recall: 0.9122807017543859
Weighted FalsePositiveRate: 0.011736942257835845
Kappa statistic: 0.9036112885648792
Training time: 1.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.309941520467868
Incorrectly Classified Instances: 8.47953216374269
Correctly Classified Instances: 91.52046783625731
Weighted Precision: 0.9236889334229458
Weighted AreaUnderROC: 0.9922958992032982
Root mean squared error: 0.08722044132821859
Relative absolute error: 12.00852347489625
Root relative squared error: 39.06030483013545
Weighted TruePositiveRate: 0.9152046783625731
Weighted MatthewsCorrelation: 0.9077491340568162
Weighted FMeasure: 0.9142772046729098
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9547377448205319
Mean absolute error: 0.011975258866932655
Coverage of cases: 96.19883040935673
Instances selection time: 17.0
Test time: 60.0
Accumulative iteration time: 239.0
Weighted Recall: 0.9152046783625731
Weighted FalsePositiveRate: 0.010916210302037306
Kappa statistic: 0.9068854798430254
Training time: 1.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.186826715912621
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9282902267806388
Weighted AreaUnderROC: 0.9926096227081895
Root mean squared error: 0.08729840557547401
Relative absolute error: 11.847636563438867
Root relative squared error: 39.09521989382091
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9114039836764146
Weighted FMeasure: 0.9175769123765164
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9551299090400415
Mean absolute error: 0.011814817625590111
Coverage of cases: 95.6140350877193
Instances selection time: 6.0
Test time: 38.0
Accumulative iteration time: 246.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010846174934176537
Kappa statistic: 0.9100997014589084
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.171437365343215
Incorrectly Classified Instances: 8.187134502923977
Correctly Classified Instances: 91.81286549707602
Weighted Precision: 0.9282902267806388
Weighted AreaUnderROC: 0.9925997444088376
Root mean squared error: 0.08761277641461973
Relative absolute error: 11.918531434963628
Root relative squared error: 39.23600593686017
Weighted TruePositiveRate: 0.9181286549707602
Weighted MatthewsCorrelation: 0.9114039836764146
Weighted FMeasure: 0.9175769123765164
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9549344762239947
Mean absolute error: 0.011885516112429205
Coverage of cases: 95.6140350877193
Instances selection time: 4.0
Test time: 38.0
Accumulative iteration time: 251.0
Weighted Recall: 0.9181286549707602
Weighted FalsePositiveRate: 0.010846174934176537
Kappa statistic: 0.9100997014589084
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 7.063711911357372
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9196155371593968
Weighted AreaUnderROC: 0.9926883165003336
Root mean squared error: 0.08941351919378905
Relative absolute error: 12.116595110766918
Root relative squared error: 40.04244030939827
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.901356852333233
Weighted FMeasure: 0.9080671410866988
Iteration time: 5.0
Weighted AreaUnderPRC: 0.95351907445728
Mean absolute error: 0.012083031135390939
Coverage of cases: 95.02923976608187
Instances selection time: 4.0
Test time: 38.0
Accumulative iteration time: 256.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.011843050059142973
Kappa statistic: 0.9004768699309101
Training time: 1.0
		
Time end:Wed Nov 01 09.19.34 EET 2017