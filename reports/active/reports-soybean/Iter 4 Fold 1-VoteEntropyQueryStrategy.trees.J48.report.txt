Wed Nov 01 09.41.52 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 09.41.52 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 8.818097876269642
Incorrectly Classified Instances: 37.134502923976605
Correctly Classified Instances: 62.865497076023395
Weighted Precision: 0.6406455655386456
Weighted AreaUnderROC: 0.8571421026320346
Root mean squared error: 0.18195308031443735
Relative absolute error: 42.26304596675031
Root relative squared error: 81.48482940047512
Weighted TruePositiveRate: 0.6286549707602339
Weighted MatthewsCorrelation: 0.5902770410057474
Weighted FMeasure: 0.5949877434882256
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5562316794361397
Mean absolute error: 0.0421459738172583
Coverage of cases: 75.43859649122807
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6286549707602339
Weighted FalsePositiveRate: 0.02853463959355033
Kappa statistic: 0.5932270058158592
Training time: 2.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 9.479839950754092
Incorrectly Classified Instances: 40.64327485380117
Correctly Classified Instances: 59.35672514619883
Weighted Precision: 0.5815468820808257
Weighted AreaUnderROC: 0.8270036234936082
Root mean squared error: 0.19426578841805237
Relative absolute error: 47.05366828548807
Root relative squared error: 86.99888235053837
Weighted TruePositiveRate: 0.5935672514619883
Weighted MatthewsCorrelation: 0.5360269679773593
Weighted FMeasure: 0.5448430539862218
Iteration time: 3.0
Weighted AreaUnderPRC: 0.4794380946288301
Mean absolute error: 0.04692332571406054
Coverage of cases: 72.22222222222223
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5935672514619883
Weighted FalsePositiveRate: 0.04056726804144552
Kappa statistic: 0.5511683897464948
Training time: 2.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.310249307479248
Incorrectly Classified Instances: 40.64327485380117
Correctly Classified Instances: 59.35672514619883
Weighted Precision: 0.5815468820808257
Weighted AreaUnderROC: 0.8270036234936082
Root mean squared error: 0.19492654284799663
Relative absolute error: 46.43330325197898
Root relative squared error: 87.29479084467636
Weighted TruePositiveRate: 0.5935672514619883
Weighted MatthewsCorrelation: 0.5360269679773593
Weighted FMeasure: 0.5448430539862218
Iteration time: 4.0
Weighted AreaUnderPRC: 0.4794380946288301
Mean absolute error: 0.046304679143248144
Coverage of cases: 68.71345029239767
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 10.0
Weighted Recall: 0.5935672514619883
Weighted FalsePositiveRate: 0.04056726804144552
Kappa statistic: 0.5511683897464948
Training time: 2.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 7.586949830717159
Incorrectly Classified Instances: 40.64327485380117
Correctly Classified Instances: 59.35672514619883
Weighted Precision: 0.5764014935969759
Weighted AreaUnderROC: 0.8270036234936082
Root mean squared error: 0.19510028115506356
Relative absolute error: 45.865790456379564
Root relative squared error: 87.372596816893
Weighted TruePositiveRate: 0.5935672514619883
Weighted MatthewsCorrelation: 0.5334833229859228
Weighted FMeasure: 0.5433310805585719
Iteration time: 4.0
Weighted AreaUnderPRC: 0.4794380946288301
Mean absolute error: 0.04573873840525426
Coverage of cases: 68.42105263157895
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5935672514619883
Weighted FalsePositiveRate: 0.04154476924597771
Kappa statistic: 0.550761205454597
Training time: 3.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.710064635272409
Incorrectly Classified Instances: 38.01169590643275
Correctly Classified Instances: 61.98830409356725
Weighted Precision: 0.6178674660341672
Weighted AreaUnderROC: 0.8428502529834071
Root mean squared error: 0.18826055108569317
Relative absolute error: 42.987423571306756
Root relative squared error: 84.30953112498602
Weighted TruePositiveRate: 0.6198830409356725
Weighted MatthewsCorrelation: 0.5675811922656865
Weighted FMeasure: 0.5750842885468497
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5109845008042322
Mean absolute error: 0.04286834483565254
Coverage of cases: 71.05263157894737
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6198830409356725
Weighted FalsePositiveRate: 0.03708882312864923
Kappa statistic: 0.5812731331054163
Training time: 4.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 9.295167743921217
Incorrectly Classified Instances: 28.65497076023392
Correctly Classified Instances: 71.34502923976608
Weighted Precision: 0.6966112400631728
Weighted AreaUnderROC: 0.8927178511685523
Root mean squared error: 0.16269911595064504
Relative absolute error: 34.595725418399276
Root relative squared error: 72.86224384844608
Weighted TruePositiveRate: 0.7134502923976608
Weighted MatthewsCorrelation: 0.6770866986029519
Weighted FMeasure: 0.6880470127635729
Iteration time: 4.0
Weighted AreaUnderPRC: 0.641634262685772
Mean absolute error: 0.03449989238399958
Coverage of cases: 80.99415204678363
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7134502923976608
Weighted FalsePositiveRate: 0.02609515014906764
Kappa statistic: 0.6837993886561756
Training time: 3.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 8.602646968297954
Incorrectly Classified Instances: 23.391812865497077
Correctly Classified Instances: 76.60818713450293
Weighted Precision: 0.7242214548592263
Weighted AreaUnderROC: 0.9162628339837274
Root mean squared error: 0.14475943020825366
Relative absolute error: 27.55221212474967
Root relative squared error: 64.8282373359391
Weighted TruePositiveRate: 0.7660818713450293
Weighted MatthewsCorrelation: 0.7222507927335543
Weighted FMeasure: 0.7307703467659685
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7268074086173125
Mean absolute error: 0.02747589020750684
Coverage of cases: 85.67251461988305
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7660818713450293
Weighted FalsePositiveRate: 0.02228032337118418
Kappa statistic: 0.7416869653883192
Training time: 4.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 8.556478916589727
Incorrectly Classified Instances: 23.976608187134502
Correctly Classified Instances: 76.0233918128655
Weighted Precision: 0.6989150928321971
Weighted AreaUnderROC: 0.9259408028745888
Root mean squared error: 0.14110161060308377
Relative absolute error: 26.74658843632147
Root relative squared error: 63.19014027272971
Weighted TruePositiveRate: 0.7602339181286549
Weighted MatthewsCorrelation: 0.7054161589617685
Weighted FMeasure: 0.7126783195431255
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7460413392539877
Mean absolute error: 0.026672498163644916
Coverage of cases: 85.08771929824562
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7602339181286549
Weighted FalsePositiveRate: 0.025669442384236175
Kappa statistic: 0.7341977309562399
Training time: 4.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 8.310249307479229
Incorrectly Classified Instances: 23.68421052631579
Correctly Classified Instances: 76.3157894736842
Weighted Precision: 0.7041020530275595
Weighted AreaUnderROC: 0.9246532421170172
Root mean squared error: 0.14058662182739484
Relative absolute error: 25.86988676993585
Root relative squared error: 62.95951063756413
Weighted TruePositiveRate: 0.7631578947368421
Weighted MatthewsCorrelation: 0.7097840958694186
Weighted FMeasure: 0.7164743880545812
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7492108486900636
Mean absolute error: 0.02579822503373127
Coverage of cases: 84.7953216374269
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7631578947368421
Weighted FalsePositiveRate: 0.025115160320150327
Kappa statistic: 0.7375138576991955
Training time: 4.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 8.80270852570022
Incorrectly Classified Instances: 21.92982456140351
Correctly Classified Instances: 78.0701754385965
Weighted Precision: 0.7193417310856193
Weighted AreaUnderROC: 0.9468450700822164
Root mean squared error: 0.13158920818571865
Relative absolute error: 24.094522439946626
Root relative squared error: 58.93016024475675
Weighted TruePositiveRate: 0.7807017543859649
Weighted MatthewsCorrelation: 0.7236170366706804
Weighted FMeasure: 0.7266069653702838
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7936722154965828
Mean absolute error: 0.024027778610473303
Coverage of cases: 87.42690058479532
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7807017543859649
Weighted FalsePositiveRate: 0.023176491209573734
Kappa statistic: 0.7571804532631539
Training time: 4.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 8.110187750076948
Incorrectly Classified Instances: 16.95906432748538
Correctly Classified Instances: 83.04093567251462
Weighted Precision: 0.7779476332642216
Weighted AreaUnderROC: 0.9527598004136995
Root mean squared error: 0.11625908731899404
Relative absolute error: 19.63947325794834
Root relative squared error: 52.064806377952216
Weighted TruePositiveRate: 0.8304093567251462
Weighted MatthewsCorrelation: 0.7848214870725959
Weighted FMeasure: 0.7929890968003539
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8199543452521505
Mean absolute error: 0.019585070284934804
Coverage of cases: 90.64327485380117
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8304093567251462
Weighted FalsePositiveRate: 0.02076844026416424
Kappa statistic: 0.812331475822398
Training time: 5.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.40227762388427
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.7963904757760809
Weighted AreaUnderROC: 0.9351036245440225
Root mean squared error: 0.12209248169406224
Relative absolute error: 20.027789855647807
Root relative squared error: 54.677200433918124
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7889418251408504
Weighted FMeasure: 0.7975337565322761
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7898449558471371
Mean absolute error: 0.01997231121338857
Coverage of cases: 87.13450292397661
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.019593605630658293
Kappa statistic: 0.8063621179379264
Training time: 5.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 8.895044629116661
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.7435492473799326
Weighted AreaUnderROC: 0.9472024116523301
Root mean squared error: 0.12442409299574121
Relative absolute error: 22.537108941504275
Root relative squared error: 55.7213759368401
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7601028900526786
Weighted FMeasure: 0.7741476821207574
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7983269902436064
Mean absolute error: 0.022474679276846567
Coverage of cases: 89.18128654970761
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.021936003010686343
Kappa statistic: 0.7931386447405727
Training time: 6.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 9.541397353031728
Incorrectly Classified Instances: 14.32748538011696
Correctly Classified Instances: 85.67251461988305
Weighted Precision: 0.840273363170756
Weighted AreaUnderROC: 0.9521035805202017
Root mean squared error: 0.11258565193673645
Relative absolute error: 21.0977790508469
Root relative squared error: 50.41971603422376
Weighted TruePositiveRate: 0.8567251461988304
Weighted MatthewsCorrelation: 0.8272321479509377
Weighted FMeasure: 0.8366889428621468
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8393174204411501
Mean absolute error: 0.0210393364495982
Coverage of cases: 91.2280701754386
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 68.0
Weighted Recall: 0.8567251461988304
Weighted FalsePositiveRate: 0.01846482253026303
Kappa statistic: 0.8420010559662091
Training time: 7.0
		
Time end:Wed Nov 01 09.41.53 EET 2017