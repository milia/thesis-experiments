Wed Nov 01 09.34.38 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.34.38 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 71.10665226115105
Incorrectly Classified Instances: 26.686217008797655
Correctly Classified Instances: 73.31378299120234
Weighted Precision: 0.6996434124157739
Weighted AreaUnderROC: 0.8748125328344621
Root mean squared error: 0.1511942706293806
Relative absolute error: 43.0321802317011
Root relative squared error: 67.70997955777278
Weighted TruePositiveRate: 0.7331378299120235
Weighted MatthewsCorrelation: 0.6852800810648367
Weighted FMeasure: 0.6999319121986068
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6649740687847709
Mean absolute error: 0.04291297751637821
Coverage of cases: 92.37536656891496
Instances selection time: 11.0
Test time: 16.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7331378299120235
Weighted FalsePositiveRate: 0.02933641165730614
Kappa statistic: 0.7051714473021634
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 63.929618768328275
Incorrectly Classified Instances: 20.821114369501466
Correctly Classified Instances: 79.17888563049853
Weighted Precision: 0.758561193031239
Weighted AreaUnderROC: 0.8942490968484795
Root mean squared error: 0.1364197971727244
Relative absolute error: 35.36264914534564
Root relative squared error: 61.09346365698672
Weighted TruePositiveRate: 0.7917888563049853
Weighted MatthewsCorrelation: 0.7495498978361563
Weighted FMeasure: 0.7641290079010077
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7249564546575276
Mean absolute error: 0.03526469166848904
Coverage of cases: 94.42815249266862
Instances selection time: 12.0
Test time: 19.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7917888563049853
Weighted FalsePositiveRate: 0.027300465784003216
Kappa statistic: 0.7692102378342309
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 58.14168853218086
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8309777784747597
Weighted AreaUnderROC: 0.9232966464066764
Root mean squared error: 0.1209929385592574
Relative absolute error: 28.994072884284222
Root relative squared error: 54.184787309593965
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8171836838125031
Weighted FMeasure: 0.8289698858522878
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7916418749752797
Mean absolute error: 0.028913756892915272
Coverage of cases: 95.60117302052785
Instances selection time: 14.0
Test time: 41.0
Accumulative iteration time: 37.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.02056125199041217
Kappa statistic: 0.8284350021833647
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 51.56659978391717
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.8390861135618841
Weighted AreaUnderROC: 0.9392180877699203
Root mean squared error: 0.11465864344076841
Relative absolute error: 25.87497024785851
Root relative squared error: 51.34807272245772
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8275429127578032
Weighted FMeasure: 0.8398484983436786
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8270458881033895
Mean absolute error: 0.025803294429997632
Coverage of cases: 95.30791788856305
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.019155058033644128
Kappa statistic: 0.8384269130537787
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 44.605649019910516
Incorrectly Classified Instances: 13.196480938416423
Correctly Classified Instances: 86.80351906158357
Weighted Precision: 0.8589239477802528
Weighted AreaUnderROC: 0.9440122531889631
Root mean squared error: 0.11187310201185738
Relative absolute error: 23.990026271382565
Root relative squared error: 50.10061174114032
Weighted TruePositiveRate: 0.8680351906158358
Weighted MatthewsCorrelation: 0.8462203850846731
Weighted FMeasure: 0.8548339874198508
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8374410711738853
Mean absolute error: 0.023923571904980054
Coverage of cases: 93.841642228739
Instances selection time: 15.0
Test time: 26.0
Accumulative iteration time: 67.0
Weighted Recall: 0.8680351906158358
Weighted FalsePositiveRate: 0.016857132840694334
Kappa statistic: 0.8546241734088713
Training time: 1.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 38.13860163605503
Incorrectly Classified Instances: 12.903225806451612
Correctly Classified Instances: 87.09677419354838
Weighted Precision: 0.8580520145707282
Weighted AreaUnderROC: 0.9444888090276488
Root mean squared error: 0.10942497696530291
Relative absolute error: 22.350820880812858
Root relative squared error: 49.004257387453166
Weighted TruePositiveRate: 0.8709677419354839
Weighted MatthewsCorrelation: 0.848099261197904
Weighted FMeasure: 0.8562710482410759
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8387298958430855
Mean absolute error: 0.022288907249564265
Coverage of cases: 93.54838709677419
Instances selection time: 15.0
Test time: 29.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8709677419354839
Weighted FalsePositiveRate: 0.016054075439839447
Kappa statistic: 0.857885713744471
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 34.1410711529557
Incorrectly Classified Instances: 13.196480938416423
Correctly Classified Instances: 86.80351906158357
Weighted Precision: 0.8551464708563629
Weighted AreaUnderROC: 0.9415027998462211
Root mean squared error: 0.11272740121994637
Relative absolute error: 22.280380890969482
Root relative squared error: 50.483196224501604
Weighted TruePositiveRate: 0.8680351906158358
Weighted MatthewsCorrelation: 0.8444907859117159
Weighted FMeasure: 0.8550807286219989
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8240797202972777
Mean absolute error: 0.022218662384346492
Coverage of cases: 93.54838709677419
Instances selection time: 16.0
Test time: 31.0
Accumulative iteration time: 98.0
Weighted Recall: 0.8680351906158358
Weighted FalsePositiveRate: 0.017396175871720248
Kappa statistic: 0.8545993784111583
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 29.40268559962953
Incorrectly Classified Instances: 13.196480938416423
Correctly Classified Instances: 86.80351906158357
Weighted Precision: 0.8551464708563629
Weighted AreaUnderROC: 0.9392651621310569
Root mean squared error: 0.11264287434543307
Relative absolute error: 21.42455656704108
Root relative squared error: 50.44534219126639
Weighted TruePositiveRate: 0.8680351906158358
Weighted MatthewsCorrelation: 0.8444907859117159
Weighted FMeasure: 0.8550807286219989
Iteration time: 13.0
Weighted AreaUnderPRC: 0.824794425772561
Mean absolute error: 0.0213652087649165
Coverage of cases: 93.25513196480938
Instances selection time: 13.0
Test time: 33.0
Accumulative iteration time: 111.0
Weighted Recall: 0.8680351906158358
Weighted FalsePositiveRate: 0.017396175871720248
Kappa statistic: 0.8545993784111583
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 25.466893039049292
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.8928829756896418
Weighted AreaUnderROC: 0.9583800112632922
Root mean squared error: 0.10501314954869652
Relative absolute error: 19.429513675338182
Root relative squared error: 47.028489767772015
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8681900690938281
Weighted FMeasure: 0.8767686201010464
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8608513111834795
Mean absolute error: 0.01937569230781664
Coverage of cases: 95.30791788856305
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 123.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.015658367682299154
Kappa statistic: 0.8676943750473162
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 21.39219015280139
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9065484336452079
Weighted AreaUnderROC: 0.961882841236013
Root mean squared error: 0.09949427369575893
Relative absolute error: 17.600520730565368
Root relative squared error: 44.55694789235075
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8857430554617836
Weighted FMeasure: 0.8947792030845607
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8747044046244973
Mean absolute error: 0.017551765825494704
Coverage of cases: 95.60117302052785
Instances selection time: 11.0
Test time: 37.0
Accumulative iteration time: 134.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.012771569642552395
Kappa statistic: 0.8841274635656573
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 18.10464577866961
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9117042097687259
Weighted AreaUnderROC: 0.9600681102525023
Root mean squared error: 0.09818230915648084
Relative absolute error: 16.798329546313823
Root relative squared error: 43.96940517816419
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.891834348401228
Weighted FMeasure: 0.9003795088008046
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8764566554545931
Mean absolute error: 0.016751796777487615
Coverage of cases: 92.66862170087977
Instances selection time: 14.0
Test time: 41.0
Accumulative iteration time: 148.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.012594376209341033
Kappa statistic: 0.890494540783558
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 15.002315172094457
Incorrectly Classified Instances: 9.970674486803519
Correctly Classified Instances: 90.02932551319648
Weighted Precision: 0.9105506549494818
Weighted AreaUnderROC: 0.961254737666977
Root mean squared error: 0.09689799335600743
Relative absolute error: 16.235871547788232
Root relative squared error: 43.394244517421
Weighted TruePositiveRate: 0.9002932551319648
Weighted MatthewsCorrelation: 0.890894004532271
Weighted FMeasure: 0.8991122260736881
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8802238617569642
Mean absolute error: 0.0161908968343596
Coverage of cases: 92.96187683284458
Instances selection time: 8.0
Test time: 42.0
Accumulative iteration time: 156.0
Weighted Recall: 0.9002932551319648
Weighted FalsePositiveRate: 0.01258300119796602
Kappa statistic: 0.8904935064935064
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 11.853681123630196
Incorrectly Classified Instances: 9.090909090909092
Correctly Classified Instances: 90.9090909090909
Weighted Precision: 0.9199512744504256
Weighted AreaUnderROC: 0.9612489951763403
Root mean squared error: 0.09472886990765167
Relative absolute error: 15.35330162504787
Root relative squared error: 42.42283664769771
Weighted TruePositiveRate: 0.9090909090909091
Weighted MatthewsCorrelation: 0.9007752992684449
Weighted FMeasure: 0.9086961852217447
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8817686288495149
Mean absolute error: 0.01531077170364898
Coverage of cases: 93.25513196480938
Instances selection time: 5.0
Test time: 45.0
Accumulative iteration time: 161.0
Weighted Recall: 0.9090909090909091
Weighted FalsePositiveRate: 0.011964814312080563
Kappa statistic: 0.9002001472781858
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 8.828522920203731
Incorrectly Classified Instances: 9.3841642228739
Correctly Classified Instances: 90.6158357771261
Weighted Precision: 0.9172201508981876
Weighted AreaUnderROC: 0.9615644664637242
Root mean squared error: 0.09398274847449713
Relative absolute error: 14.896098799904559
Root relative squared error: 42.08869788188198
Weighted TruePositiveRate: 0.906158357771261
Weighted MatthewsCorrelation: 0.8976580200834126
Weighted FMeasure: 0.9058169815451507
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8799012333832709
Mean absolute error: 0.014854835368326007
Coverage of cases: 93.25513196480938
Instances selection time: 3.0
Test time: 47.0
Accumulative iteration time: 164.0
Weighted Recall: 0.906158357771261
Weighted FalsePositiveRate: 0.012053411028686246
Kappa statistic: 0.8970138548076559
Training time: 0.0
		
Time end:Wed Nov 01 09.34.39 EET 2017