Wed Nov 01 09.35.06 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 09.35.06 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 9.91074176669745
Incorrectly Classified Instances: 32.748538011695906
Correctly Classified Instances: 67.25146198830409
Weighted Precision: 0.6307722657418052
Weighted AreaUnderROC: 0.9502095532694966
Root mean squared error: 0.16307628394167162
Relative absolute error: 35.82342668640862
Root relative squared error: 73.0311526097103
Weighted TruePositiveRate: 0.672514619883041
Weighted MatthewsCorrelation: 0.6011522708015512
Weighted FMeasure: 0.606137306353707
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7563369327885688
Mean absolute error: 0.03572419281747151
Coverage of cases: 79.23976608187135
Instances selection time: 2.0
Test time: 37.0
Accumulative iteration time: 2.0
Weighted Recall: 0.672514619883041
Weighted FalsePositiveRate: 0.041029711572457446
Kappa statistic: 0.6338819751103975
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 9.002770083102503
Incorrectly Classified Instances: 25.43859649122807
Correctly Classified Instances: 74.56140350877193
Weighted Precision: 0.7322375641431902
Weighted AreaUnderROC: 0.9616570915684313
Root mean squared error: 0.1462526278051166
Relative absolute error: 29.47594159009268
Root relative squared error: 65.49694242865482
Weighted TruePositiveRate: 0.7456140350877193
Weighted MatthewsCorrelation: 0.6962697832301067
Weighted FMeasure: 0.7004615450553252
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8253238138994388
Mean absolute error: 0.029394290782364153
Coverage of cases: 85.96491228070175
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7456140350877193
Weighted FalsePositiveRate: 0.03211566778310351
Kappa statistic: 0.7170785512565728
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 8.58725761772854
Incorrectly Classified Instances: 23.099415204678362
Correctly Classified Instances: 76.90058479532163
Weighted Precision: 0.7417686154709873
Weighted AreaUnderROC: 0.9635505842859972
Root mean squared error: 0.1391864750343976
Relative absolute error: 26.46286763629756
Root relative squared error: 62.33247688597367
Weighted TruePositiveRate: 0.7690058479532164
Weighted MatthewsCorrelation: 0.7209843191859624
Weighted FMeasure: 0.7299002934358626
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8423172006013862
Mean absolute error: 0.026389563293814974
Coverage of cases: 85.38011695906432
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7690058479532164
Weighted FalsePositiveRate: 0.03078545823640473
Kappa statistic: 0.7433822481835018
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 8.341028008618048
Incorrectly Classified Instances: 19.5906432748538
Correctly Classified Instances: 80.4093567251462
Weighted Precision: 0.8037098956426261
Weighted AreaUnderROC: 0.9670635824826254
Root mean squared error: 0.12649147405866293
Relative absolute error: 22.701122289677624
Root relative squared error: 56.647220077136204
Weighted TruePositiveRate: 0.804093567251462
Weighted MatthewsCorrelation: 0.7721627349259684
Weighted FMeasure: 0.7812570891534826
Iteration time: 1.0
Weighted AreaUnderPRC: 0.878205290795773
Mean absolute error: 0.02263823829441556
Coverage of cases: 88.01169590643275
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 5.0
Weighted Recall: 0.804093567251462
Weighted FalsePositiveRate: 0.026693317253994667
Kappa statistic: 0.7831058440452076
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 7.97168359495231
Incorrectly Classified Instances: 19.29824561403509
Correctly Classified Instances: 80.70175438596492
Weighted Precision: 0.8084775043231034
Weighted AreaUnderROC: 0.9812023379685062
Root mean squared error: 0.12421245723934982
Relative absolute error: 21.458164680561886
Root relative squared error: 55.626598187132366
Weighted TruePositiveRate: 0.8070175438596491
Weighted MatthewsCorrelation: 0.7758562168831571
Weighted FMeasure: 0.7841357782476845
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8848154447193384
Mean absolute error: 0.021398723781169936
Coverage of cases: 88.01169590643275
Instances selection time: 0.0
Test time: 38.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8070175438596491
Weighted FalsePositiveRate: 0.026216165461053396
Kappa statistic: 0.7864198932667196
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 7.648507232994788
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.8300549026965758
Weighted AreaUnderROC: 0.9847127554385312
Root mean squared error: 0.11970002027712787
Relative absolute error: 19.70728147748364
Root relative squared error: 53.60577416254519
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7976786729468356
Weighted FMeasure: 0.8027864194519951
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9023027676946384
Mean absolute error: 0.01965269067006696
Coverage of cases: 89.47368421052632
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.022776448282281214
Kappa statistic: 0.8061590780275836
Training time: 1.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 7.679285934133604
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8704142937323714
Weighted AreaUnderROC: 0.9880894343332155
Root mean squared error: 0.10926909007092775
Relative absolute error: 17.544226458953062
Root relative squared error: 48.93444589004967
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8365127213346246
Weighted FMeasure: 0.8427244231186001
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9219966707830453
Mean absolute error: 0.017495627493692958
Coverage of cases: 92.98245614035088
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.01913271127343417
Kappa statistic: 0.835677610815394
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 7.556171129578356
Incorrectly Classified Instances: 12.865497076023392
Correctly Classified Instances: 87.13450292397661
Weighted Precision: 0.8805203333572127
Weighted AreaUnderROC: 0.9897301493759557
Root mean squared error: 0.10402870229409632
Relative absolute error: 16.023227047668765
Root relative squared error: 46.5876205257881
Weighted TruePositiveRate: 0.8713450292397661
Weighted MatthewsCorrelation: 0.8556331380566579
Weighted FMeasure: 0.863322282704485
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9322453575544339
Mean absolute error: 0.01597884137717675
Coverage of cases: 92.98245614035088
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8713450292397661
Weighted FalsePositiveRate: 0.017127031430423427
Kappa statistic: 0.8584050811573748
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 7.5561711295783605
Incorrectly Classified Instances: 11.988304093567251
Correctly Classified Instances: 88.01169590643275
Weighted Precision: 0.8849723007617744
Weighted AreaUnderROC: 0.9923481001993377
Root mean squared error: 0.10004964173180962
Relative absolute error: 15.103196122353587
Root relative squared error: 44.80566074510302
Weighted TruePositiveRate: 0.8801169590643275
Weighted MatthewsCorrelation: 0.8648829332871375
Weighted FMeasure: 0.8725425635944672
Iteration time: 0.0
Weighted AreaUnderPRC: 0.9478061318290736
Mean absolute error: 0.01506135901398155
Coverage of cases: 95.02923976608187
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8801169590643275
Weighted FalsePositiveRate: 0.014698960597152849
Kappa statistic: 0.8682786608048698
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 7.463835026161924
Incorrectly Classified Instances: 9.941520467836257
Correctly Classified Instances: 90.05847953216374
Weighted Precision: 0.9112258701559823
Weighted AreaUnderROC: 0.9928664848957724
Root mean squared error: 0.09308603943553022
Relative absolute error: 13.832788100260098
Root relative squared error: 41.68712082181896
Weighted TruePositiveRate: 0.9005847953216374
Weighted MatthewsCorrelation: 0.8918464142338032
Weighted FMeasure: 0.8996482766255469
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9545248730812825
Mean absolute error: 0.013794470127683322
Coverage of cases: 95.02923976608187
Instances selection time: 1.0
Test time: 36.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9005847953216374
Weighted FalsePositiveRate: 0.01263988878538605
Kappa statistic: 0.890839451005426
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 7.309941520467863
Incorrectly Classified Instances: 9.35672514619883
Correctly Classified Instances: 90.64327485380117
Weighted Precision: 0.91449053409126
Weighted AreaUnderROC: 0.992714473002167
Root mean squared error: 0.09016791184432286
Relative absolute error: 12.922659674576959
Root relative squared error: 40.38028321001578
Weighted TruePositiveRate: 0.9064327485380117
Weighted MatthewsCorrelation: 0.8970615856330184
Weighted FMeasure: 0.9042137978429146
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9546182389269559
Mean absolute error: 0.012886862833373258
Coverage of cases: 94.73684210526316
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9064327485380117
Weighted FalsePositiveRate: 0.011368085462798822
Kappa statistic: 0.8973522046202763
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 7.202216066482018
Incorrectly Classified Instances: 9.649122807017545
Correctly Classified Instances: 90.35087719298245
Weighted Precision: 0.9130156019687384
Weighted AreaUnderROC: 0.9930767514460701
Root mean squared error: 0.08949219688743736
Relative absolute error: 12.490501442800216
Root relative squared error: 40.07767487884593
Weighted TruePositiveRate: 0.9035087719298246
Weighted MatthewsCorrelation: 0.894829661883514
Weighted FMeasure: 0.9028538404166958
Iteration time: 1.0
Weighted AreaUnderPRC: 0.954588860794441
Mean absolute error: 0.012455901715811958
Coverage of cases: 94.73684210526316
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9035087719298246
Weighted FalsePositiveRate: 0.011444782036007762
Kappa statistic: 0.894215898546242
Training time: 1.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 7.0791012619267715
Incorrectly Classified Instances: 8.771929824561404
Correctly Classified Instances: 91.2280701754386
Weighted Precision: 0.9202693410020172
Weighted AreaUnderROC: 0.9930751132777301
Root mean squared error: 0.08770400175810054
Relative absolute error: 11.884374072324139
Root relative squared error: 39.27685977422136
Weighted TruePositiveRate: 0.9122807017543859
Weighted MatthewsCorrelation: 0.9035659663794834
Weighted FMeasure: 0.9115256828927805
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9556311037513242
Mean absolute error: 0.01185145336852279
Coverage of cases: 95.02923976608187
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9122807017543859
Weighted FalsePositiveRate: 0.010825612108023627
Kappa statistic: 0.9038641730070087
Training time: 1.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 7.094490612496177
Incorrectly Classified Instances: 8.771929824561404
Correctly Classified Instances: 91.2280701754386
Weighted Precision: 0.9202288160736912
Weighted AreaUnderROC: 0.9931726393518117
Root mean squared error: 0.0875051061914852
Relative absolute error: 11.786912029277334
Root relative squared error: 39.187787518416975
Weighted TruePositiveRate: 0.9122807017543859
Weighted MatthewsCorrelation: 0.9035673892439675
Weighted FMeasure: 0.9116052423579419
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9562394363589998
Mean absolute error: 0.011754261303434564
Coverage of cases: 94.73684210526316
Instances selection time: 0.0
Test time: 37.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9122807017543859
Weighted FalsePositiveRate: 0.011191942293630922
Kappa statistic: 0.903799272399955
Training time: 1.0
		
Time end:Wed Nov 01 09.35.07 EET 2017