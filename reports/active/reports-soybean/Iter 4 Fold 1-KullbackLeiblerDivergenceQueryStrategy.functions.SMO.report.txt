Wed Nov 01 09.25.36 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 09.25.36 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.8206744488369605
Weighted AreaUnderROC: 0.9378209379534502
Root mean squared error: 0.21458607612878355
Relative absolute error: 95.1180101670287
Root relative squared error: 96.0990040666208
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7978154263777604
Weighted FMeasure: 0.8052064257532847
Iteration time: 1674.0
Weighted AreaUnderPRC: 0.7574223599967254
Mean absolute error: 0.09485452537432308
Coverage of cases: 94.73684210526316
Instances selection time: 47.0
Test time: 47.0
Accumulative iteration time: 1674.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.022912922065524175
Kappa statistic: 0.8064899425693836
Training time: 1627.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.8162278592103154
Weighted AreaUnderROC: 0.9371673201764045
Root mean squared error: 0.2146011881624456
Relative absolute error: 95.1252723311536
Root relative squared error: 96.10577175355809
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.7879369004583282
Weighted FMeasure: 0.79507093982999
Iteration time: 1142.0
Weighted AreaUnderPRC: 0.7470398789549607
Mean absolute error: 0.09486176742164987
Coverage of cases: 94.73684210526316
Instances selection time: 43.0
Test time: 32.0
Accumulative iteration time: 2816.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.024364198960681854
Kappa statistic: 0.793513329937171
Training time: 1099.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 18.71345029239766
Correctly Classified Instances: 81.28654970760233
Weighted Precision: 0.8142892352208746
Weighted AreaUnderROC: 0.9375517759152825
Root mean squared error: 0.21459098951756697
Relative absolute error: 95.12164124909116
Root relative squared error: 96.10120445062145
Weighted TruePositiveRate: 0.8128654970760234
Weighted MatthewsCorrelation: 0.78688006094926
Weighted FMeasure: 0.7944486827311656
Iteration time: 1138.0
Weighted AreaUnderPRC: 0.750429862960298
Mean absolute error: 0.09485814639798647
Coverage of cases: 94.73684210526316
Instances selection time: 40.0
Test time: 32.0
Accumulative iteration time: 3954.0
Weighted Recall: 0.8128654970760234
Weighted FalsePositiveRate: 0.024364198960681857
Kappa statistic: 0.793583432354439
Training time: 1098.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 19.005847953216374
Correctly Classified Instances: 80.99415204678363
Weighted Precision: 0.811920940747317
Weighted AreaUnderROC: 0.9370899816249392
Root mean squared error: 0.21459654796319194
Relative absolute error: 95.1252723311536
Root relative squared error: 96.10369371319781
Weighted TruePositiveRate: 0.8099415204678363
Weighted MatthewsCorrelation: 0.7839734418893138
Weighted FMeasure: 0.7915444289089191
Iteration time: 1124.0
Weighted AreaUnderPRC: 0.7489567486088139
Mean absolute error: 0.09486176742164987
Coverage of cases: 94.73684210526316
Instances selection time: 37.0
Test time: 31.0
Accumulative iteration time: 5078.0
Weighted Recall: 0.8099415204678363
Weighted FalsePositiveRate: 0.024452270545265806
Kappa statistic: 0.7903818953323904
Training time: 1087.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 16.08187134502924
Correctly Classified Instances: 83.91812865497076
Weighted Precision: 0.852932820915277
Weighted AreaUnderROC: 0.9549370150210109
Root mean squared error: 0.21388375109286625
Relative absolute error: 94.86499169734985
Root relative squared error: 95.78447882947445
Weighted TruePositiveRate: 0.8391812865497076
Weighted MatthewsCorrelation: 0.8217923033044405
Weighted FMeasure: 0.828759427373294
Iteration time: 1251.0
Weighted AreaUnderPRC: 0.7891037822981122
Mean absolute error: 0.0946022077868317
Coverage of cases: 97.6608187134503
Instances selection time: 35.0
Test time: 36.0
Accumulative iteration time: 6329.0
Weighted Recall: 0.8391812865497076
Weighted FalsePositiveRate: 0.019908252843353368
Kappa statistic: 0.8232308993515647
Training time: 1216.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 12.573099415204679
Correctly Classified Instances: 87.42690058479532
Weighted Precision: 0.8852018278877679
Weighted AreaUnderROC: 0.9663836770661192
Root mean squared error: 0.21380278029997357
Relative absolute error: 94.8288932207056
Root relative squared error: 95.74821733154388
Weighted TruePositiveRate: 0.8742690058479532
Weighted MatthewsCorrelation: 0.8588582270040132
Weighted FMeasure: 0.8606901824428864
Iteration time: 1280.0
Weighted AreaUnderPRC: 0.8373011276698669
Mean absolute error: 0.09456620930596762
Coverage of cases: 97.6608187134503
Instances selection time: 31.0
Test time: 37.0
Accumulative iteration time: 7609.0
Weighted Recall: 0.8742690058479532
Weighted FalsePositiveRate: 0.016332582574594957
Kappa statistic: 0.8615084709051013
Training time: 1249.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.8865352751059665
Weighted AreaUnderROC: 0.966653502290973
Root mean squared error: 0.21380238644611332
Relative absolute error: 94.8288932207056
Root relative squared error: 95.74804095027815
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8612452739627826
Weighted FMeasure: 0.862793871593234
Iteration time: 1258.0
Weighted AreaUnderPRC: 0.8358086984715621
Mean absolute error: 0.09456620930596764
Coverage of cases: 97.6608187134503
Instances selection time: 27.0
Test time: 36.0
Accumulative iteration time: 8867.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.01613155918278209
Kappa statistic: 0.8646986238143233
Training time: 1231.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 11.695906432748538
Correctly Classified Instances: 88.30409356725146
Weighted Precision: 0.890538338319556
Weighted AreaUnderROC: 0.9673417681699358
Root mean squared error: 0.21377767066503725
Relative absolute error: 94.81986860154453
Root relative squared error: 95.73697237589072
Weighted TruePositiveRate: 0.8830409356725146
Weighted MatthewsCorrelation: 0.8670083151875547
Weighted FMeasure: 0.8682306661941154
Iteration time: 1269.0
Weighted AreaUnderPRC: 0.8423979726904409
Mean absolute error: 0.09455720968575161
Coverage of cases: 97.6608187134503
Instances selection time: 24.0
Test time: 36.0
Accumulative iteration time: 10136.0
Weighted Recall: 0.8830409356725146
Weighted FalsePositiveRate: 0.015222755642399603
Kappa statistic: 0.871199781566881
Training time: 1245.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 11.695906432748538
Correctly Classified Instances: 88.30409356725146
Weighted Precision: 0.890538338319556
Weighted AreaUnderROC: 0.9676663901708463
Root mean squared error: 0.21377058035180846
Relative absolute error: 94.81806367771229
Root relative squared error: 95.73379709046633
Weighted TruePositiveRate: 0.8830409356725146
Weighted MatthewsCorrelation: 0.8670083151875547
Weighted FMeasure: 0.8682306661941154
Iteration time: 1278.0
Weighted AreaUnderPRC: 0.842738158505199
Mean absolute error: 0.09455540976170838
Coverage of cases: 97.6608187134503
Instances selection time: 20.0
Test time: 35.0
Accumulative iteration time: 11414.0
Weighted Recall: 0.8830409356725146
Weighted FalsePositiveRate: 0.015222755642399603
Kappa statistic: 0.871199781566881
Training time: 1258.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 10.818713450292398
Correctly Classified Instances: 89.18128654970761
Weighted Precision: 0.8982706320167622
Weighted AreaUnderROC: 0.9676261393789399
Root mean squared error: 0.21376654270724166
Relative absolute error: 94.81445383004785
Root relative squared error: 95.73198889475931
Weighted TruePositiveRate: 0.8918128654970761
Weighted MatthewsCorrelation: 0.8761125406021335
Weighted FMeasure: 0.8770412773903838
Iteration time: 1283.0
Weighted AreaUnderPRC: 0.8471149420763984
Mean absolute error: 0.09455180991362197
Coverage of cases: 97.6608187134503
Instances selection time: 18.0
Test time: 35.0
Accumulative iteration time: 12697.0
Weighted Recall: 0.8918128654970761
Weighted FalsePositiveRate: 0.01461488438343226
Kappa statistic: 0.8808575544445386
Training time: 1265.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 11.11111111111111
Correctly Classified Instances: 88.88888888888889
Weighted Precision: 0.8851254931587341
Weighted AreaUnderROC: 0.96650594199562
Root mean squared error: 0.21374591013773359
Relative absolute error: 94.80903905855122
Root relative squared error: 95.72274892254474
Weighted TruePositiveRate: 0.8888888888888888
Weighted MatthewsCorrelation: 0.8698898025309504
Weighted FMeasure: 0.8754072262315762
Iteration time: 1299.0
Weighted AreaUnderPRC: 0.8375976587283046
Mean absolute error: 0.09454641014149236
Coverage of cases: 97.6608187134503
Instances selection time: 13.0
Test time: 35.0
Accumulative iteration time: 13996.0
Weighted Recall: 0.8888888888888888
Weighted FalsePositiveRate: 0.015001036085373436
Kappa statistic: 0.8776490081812105
Training time: 1286.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 10.526315789473685
Correctly Classified Instances: 89.47368421052632
Weighted Precision: 0.8897273082990359
Weighted AreaUnderROC: 0.9710027172949035
Root mean squared error: 0.21371178077629152
Relative absolute error: 94.79459966789354
Root relative squared error: 95.70746462403302
Weighted TruePositiveRate: 0.8947368421052632
Weighted MatthewsCorrelation: 0.8759126570588477
Weighted FMeasure: 0.8807281444114602
Iteration time: 1294.0
Weighted AreaUnderPRC: 0.8456677697207045
Mean absolute error: 0.09453201074914674
Coverage of cases: 97.6608187134503
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 15290.0
Weighted Recall: 0.8947368421052632
Weighted FalsePositiveRate: 0.014080857533615935
Kappa statistic: 0.884052511630535
Training time: 1284.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 10.23391812865497
Correctly Classified Instances: 89.76608187134502
Weighted Precision: 0.8875058279308773
Weighted AreaUnderROC: 0.9723188927880502
Root mean squared error: 0.21364867881534017
Relative absolute error: 94.76933073424254
Root relative squared error: 95.67920540185294
Weighted TruePositiveRate: 0.8976608187134503
Weighted MatthewsCorrelation: 0.8757172624906674
Weighted FMeasure: 0.882704268332423
Iteration time: 1302.0
Weighted AreaUnderPRC: 0.8571347591322023
Mean absolute error: 0.09450681181254188
Coverage of cases: 97.6608187134503
Instances selection time: 7.0
Test time: 34.0
Accumulative iteration time: 16592.0
Weighted Recall: 0.8976608187134503
Weighted FalsePositiveRate: 0.015046276460353824
Kappa statistic: 0.8872031662269129
Training time: 1295.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 78.97814712219102
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.8968576660905632
Weighted AreaUnderROC: 0.9733970385744147
Root mean squared error: 0.2136150759442815
Relative absolute error: 94.75489134358483
Root relative squared error: 95.66415688379075
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.891336911868246
Weighted FMeasure: 0.9007410760642525
Iteration time: 1307.0
Weighted AreaUnderPRC: 0.8682223804688082
Mean absolute error: 0.09449241242019622
Coverage of cases: 97.6608187134503
Instances selection time: 3.0
Test time: 34.0
Accumulative iteration time: 17899.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.012896464228106587
Kappa statistic: 0.900327166064982
Training time: 1304.0
		
Time end:Wed Nov 01 09.25.55 EET 2017