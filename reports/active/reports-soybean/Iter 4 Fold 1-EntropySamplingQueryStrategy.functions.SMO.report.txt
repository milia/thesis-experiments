Wed Nov 01 09.21.47 EET 2017
Dataset: soybean
Test set size: 342
Initial Labelled set size: 68
Initial Unlabelled set size: 273
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.21.47 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 273
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 17.54385964912281
Correctly Classified Instances: 82.45614035087719
Weighted Precision: 0.8206744488369605
Weighted AreaUnderROC: 0.9378209379534502
Root mean squared error: 0.21458607612878355
Relative absolute error: 95.1180101670287
Root relative squared error: 96.0990040666208
Weighted TruePositiveRate: 0.8245614035087719
Weighted MatthewsCorrelation: 0.7978154263777604
Weighted FMeasure: 0.8052064257532847
Iteration time: 967.0
Weighted AreaUnderPRC: 0.7574223599967254
Mean absolute error: 0.09485452537432308
Coverage of cases: 94.73684210526316
Instances selection time: 24.0
Test time: 34.0
Accumulative iteration time: 967.0
Weighted Recall: 0.8245614035087719
Weighted FalsePositiveRate: 0.022912922065524175
Kappa statistic: 0.8064899425693836
Training time: 943.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 253
	
Mean region size: 78.94736842105219
Incorrectly Classified Instances: 17.251461988304094
Correctly Classified Instances: 82.74853801169591
Weighted Precision: 0.8155411608143522
Weighted AreaUnderROC: 0.9454833304105037
Root mean squared error: 0.21455279612490935
Relative absolute error: 95.10530137981011
Root relative squared error: 96.08410013955644
Weighted TruePositiveRate: 0.827485380116959
Weighted MatthewsCorrelation: 0.7965220410786704
Weighted FMeasure: 0.8022338297790357
Iteration time: 993.0
Weighted AreaUnderPRC: 0.7558801869123652
Mean absolute error: 0.09484185179150122
Coverage of cases: 94.73684210526316
Instances selection time: 21.0
Test time: 33.0
Accumulative iteration time: 1960.0
Weighted Recall: 0.827485380116959
Weighted FalsePositiveRate: 0.021028451809541552
Kappa statistic: 0.8099570524412296
Training time: 972.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 233
	
Mean region size: 78.96275777162161
Incorrectly Classified Instances: 14.912280701754385
Correctly Classified Instances: 85.08771929824562
Weighted Precision: 0.8566784868881574
Weighted AreaUnderROC: 0.9580452668485367
Root mean squared error: 0.2139826218633334
Relative absolute error: 94.90650494549078
Root relative squared error: 95.82875655123814
Weighted TruePositiveRate: 0.8508771929824561
Weighted MatthewsCorrelation: 0.8300041941419302
Weighted FMeasure: 0.8354815637971633
Iteration time: 1076.0
Weighted AreaUnderPRC: 0.7957859555799509
Mean absolute error: 0.09464360603982543
Coverage of cases: 97.07602339181287
Instances selection time: 22.0
Test time: 35.0
Accumulative iteration time: 3036.0
Weighted Recall: 0.8508771929824561
Weighted FalsePositiveRate: 0.01942026467503863
Kappa statistic: 0.8358986903507452
Training time: 1054.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 213
	
Mean region size: 79.00892582332983
Incorrectly Classified Instances: 13.742690058479532
Correctly Classified Instances: 86.25730994152046
Weighted Precision: 0.8903211239915011
Weighted AreaUnderROC: 0.9741228662829187
Root mean squared error: 0.21329447428917683
Relative absolute error: 94.61410728467206
Root relative squared error: 95.52058046768079
Weighted TruePositiveRate: 0.8625730994152047
Weighted MatthewsCorrelation: 0.8517507694279178
Weighted FMeasure: 0.8555716423332134
Iteration time: 1213.0
Weighted AreaUnderPRC: 0.8277520322316971
Mean absolute error: 0.09435201834482616
Coverage of cases: 100.0
Instances selection time: 21.0
Test time: 77.0
Accumulative iteration time: 4249.0
Weighted Recall: 0.8625730994152047
Weighted FalsePositiveRate: 0.017602657594273652
Kappa statistic: 0.848973992784125
Training time: 1192.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 193
	
Mean region size: 79.05509387503805
Incorrectly Classified Instances: 12.280701754385966
Correctly Classified Instances: 87.71929824561404
Weighted Precision: 0.9068527527877122
Weighted AreaUnderROC: 0.9763384915826016
Root mean squared error: 0.2132443051181866
Relative absolute error: 94.59605804634991
Root relative squared error: 95.49811299237211
Weighted TruePositiveRate: 0.8771929824561403
Weighted MatthewsCorrelation: 0.8698264020078394
Weighted FMeasure: 0.8732645622118934
Iteration time: 1213.0
Weighted AreaUnderPRC: 0.8407849699019874
Mean absolute error: 0.0943340191043941
Coverage of cases: 100.0
Instances selection time: 20.0
Test time: 38.0
Accumulative iteration time: 5462.0
Weighted Recall: 0.8771929824561403
Weighted FalsePositiveRate: 0.015342023754692445
Kappa statistic: 0.8652684500806663
Training time: 1193.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 173
	
Mean region size: 78.99353647276043
Incorrectly Classified Instances: 10.526315789473685
Correctly Classified Instances: 89.47368421052632
Weighted Precision: 0.9160386133667616
Weighted AreaUnderROC: 0.9784592151164997
Root mean squared error: 0.21319449443279287
Relative absolute error: 94.57259403653113
Root relative squared error: 95.47580605920773
Weighted TruePositiveRate: 0.8947368421052632
Weighted MatthewsCorrelation: 0.8874890842851648
Weighted FMeasure: 0.8918195498803506
Iteration time: 1241.0
Weighted AreaUnderPRC: 0.860207516984517
Mean absolute error: 0.09431062009183244
Coverage of cases: 100.0
Instances selection time: 18.0
Test time: 44.0
Accumulative iteration time: 6703.0
Weighted Recall: 0.8947368421052632
Weighted FalsePositiveRate: 0.012981943319152275
Kappa statistic: 0.8844768052844918
Training time: 1223.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 153
	
Mean region size: 78.99353647276043
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9231845608679591
Weighted AreaUnderROC: 0.9810305077508407
Root mean squared error: 0.21312812792684013
Relative absolute error: 94.54552017904793
Root relative squared error: 95.44608486181927
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9018962983293772
Weighted FMeasure: 0.9070908963895139
Iteration time: 1243.0
Weighted AreaUnderPRC: 0.8729443764615161
Mean absolute error: 0.09428362123118436
Coverage of cases: 100.0
Instances selection time: 16.0
Test time: 38.0
Accumulative iteration time: 7946.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.010343604282588762
Kappa statistic: 0.9006894226085653
Training time: 1227.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 133
	
Mean region size: 78.99353647276043
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9223003034182511
Weighted AreaUnderROC: 0.9796901217605967
Root mean squared error: 0.21310582829089125
Relative absolute error: 94.53830048371906
Root relative squared error: 95.43609832008089
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9019087079764617
Weighted FMeasure: 0.9083073315679993
Iteration time: 1248.0
Weighted AreaUnderPRC: 0.8745376939894585
Mean absolute error: 0.09427642153501152
Coverage of cases: 100.0
Instances selection time: 14.0
Test time: 42.0
Accumulative iteration time: 9194.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.010732608148587858
Kappa statistic: 0.9007433482502294
Training time: 1234.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 113
	
Mean region size: 79.00892582332983
Incorrectly Classified Instances: 6.7251461988304095
Correctly Classified Instances: 93.27485380116958
Weighted Precision: 0.938821477888348
Weighted AreaUnderROC: 0.9832967811813018
Root mean squared error: 0.2130461288545164
Relative absolute error: 94.51483647390032
Root relative squared error: 95.40936286509489
Weighted TruePositiveRate: 0.9327485380116959
Weighted MatthewsCorrelation: 0.9257620934037074
Weighted FMeasure: 0.932569981079266
Iteration time: 1216.0
Weighted AreaUnderPRC: 0.9041171813030159
Mean absolute error: 0.0942530225224499
Coverage of cases: 100.0
Instances selection time: 13.0
Test time: 37.0
Accumulative iteration time: 10410.0
Weighted Recall: 0.9327485380116959
Weighted FalsePositiveRate: 0.00853996470673706
Kappa statistic: 0.9263365891574501
Training time: 1203.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 93
	
Mean region size: 79.02431517389924
Incorrectly Classified Instances: 9.064327485380117
Correctly Classified Instances: 90.93567251461988
Weighted Precision: 0.9209355820561254
Weighted AreaUnderROC: 0.9808519762260163
Root mean squared error: 0.21306794072435203
Relative absolute error: 94.52386109306143
Root relative squared error: 95.41913096843994
Weighted TruePositiveRate: 0.9093567251461988
Weighted MatthewsCorrelation: 0.9011108006785923
Weighted FMeasure: 0.9099527571365059
Iteration time: 1229.0
Weighted AreaUnderPRC: 0.880233538925478
Mean absolute error: 0.09426202214266598
Coverage of cases: 100.0
Instances selection time: 10.0
Test time: 37.0
Accumulative iteration time: 11639.0
Weighted Recall: 0.9093567251461988
Weighted FalsePositiveRate: 0.012197928891017033
Kappa statistic: 0.9006745362563239
Training time: 1219.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 73
	
Mean region size: 79.02431517389925
Incorrectly Classified Instances: 8.47953216374269
Correctly Classified Instances: 91.52046783625731
Weighted Precision: 0.9230655654296724
Weighted AreaUnderROC: 0.980188647082098
Root mean squared error: 0.2130592211847259
Relative absolute error: 94.51844632156477
Root relative squared error: 95.41522605956092
Weighted TruePositiveRate: 0.9152046783625731
Weighted MatthewsCorrelation: 0.9061964007782926
Weighted FMeasure: 0.9150033174607275
Iteration time: 1220.0
Weighted AreaUnderPRC: 0.8866406616739048
Mean absolute error: 0.09425662237053632
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 39.0
Accumulative iteration time: 12859.0
Weighted Recall: 0.9152046783625731
Weighted FalsePositiveRate: 0.01166683054761685
Kappa statistic: 0.9070495398399281
Training time: 1212.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 53
	
Mean region size: 79.03970452446863
Incorrectly Classified Instances: 8.771929824561404
Correctly Classified Instances: 91.2280701754386
Weighted Precision: 0.9229719150878757
Weighted AreaUnderROC: 0.979072943040656
Root mean squared error: 0.21305924588647537
Relative absolute error: 94.52205616922913
Root relative squared error: 95.41523712185148
Weighted TruePositiveRate: 0.9122807017543859
Weighted MatthewsCorrelation: 0.9034102986533185
Weighted FMeasure: 0.9122098333957058
Iteration time: 1253.0
Weighted AreaUnderPRC: 0.8776173058301117
Mean absolute error: 0.0942602222186227
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 37.0
Accumulative iteration time: 14112.0
Weighted Recall: 0.9122807017543859
Weighted FalsePositiveRate: 0.012819767654897648
Kappa statistic: 0.9037830335540259
Training time: 1247.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 33
	
Mean region size: 79.02431517389923
Incorrectly Classified Instances: 4.970760233918129
Correctly Classified Instances: 95.02923976608187
Weighted Precision: 0.9527647030856272
Weighted AreaUnderROC: 0.990923716490457
Root mean squared error: 0.21297047401239227
Relative absolute error: 94.4841527687527
Root relative squared error: 95.3754820322277
Weighted TruePositiveRate: 0.9502923976608187
Weighted MatthewsCorrelation: 0.9435854988752954
Weighted FMeasure: 0.9490480076159391
Iteration time: 1252.0
Weighted AreaUnderPRC: 0.926769272916369
Mean absolute error: 0.09422242381371543
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 43.0
Accumulative iteration time: 15364.0
Weighted Recall: 0.9502923976608187
Weighted FalsePositiveRate: 0.007667955036376089
Kappa statistic: 0.9454402132092116
Training time: 1249.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 13
	
Mean region size: 79.03970452446863
Incorrectly Classified Instances: 6.7251461988304095
Correctly Classified Instances: 93.27485380116958
Weighted Precision: 0.9363021297087487
Weighted AreaUnderROC: 0.9886171061420301
Root mean squared error: 0.21299847091841576
Relative absolute error: 94.4967872355782
Root relative squared error: 95.38802000689194
Weighted TruePositiveRate: 0.9327485380116959
Weighted MatthewsCorrelation: 0.9244623746159913
Weighted FMeasure: 0.9318607805449911
Iteration time: 1309.0
Weighted AreaUnderPRC: 0.9043798564003904
Mean absolute error: 0.09423502328201787
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 37.0
Accumulative iteration time: 16673.0
Weighted Recall: 0.9327485380116959
Weighted FalsePositiveRate: 0.009627580252183932
Kappa statistic: 0.9261852032581359
Training time: 1307.0
		
Time end:Wed Nov 01 09.22.05 EET 2017