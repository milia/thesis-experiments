Wed Nov 01 09.34.31 EET 2017
Dataset: soybean
Test set size: 341
Initial Labelled set size: 68
Initial Unlabelled set size: 274
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 09.34.31 EET 2017
		
Iteration: 1
Labeled set size: 68
Unlabelled set size: 274
	
Mean region size: 71.89381077326709
Incorrectly Classified Instances: 24.04692082111437
Correctly Classified Instances: 75.95307917888563
Weighted Precision: 0.770785198722806
Weighted AreaUnderROC: 0.8834324881365856
Root mean squared error: 0.14331187951218152
Relative absolute error: 40.32924399223529
Root relative squared error: 64.17997449084663
Weighted TruePositiveRate: 0.7595307917888563
Weighted MatthewsCorrelation: 0.7350110057324433
Weighted FMeasure: 0.7477894018419529
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6856906395809856
Mean absolute error: 0.040217528634916434
Coverage of cases: 95.01466275659824
Instances selection time: 10.0
Test time: 16.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7595307917888563
Weighted FalsePositiveRate: 0.024182592309285444
Kappa statistic: 0.7358636715724245
Training time: 0.0
		
Iteration: 2
Labeled set size: 88
Unlabelled set size: 254
	
Mean region size: 63.590060194474276
Incorrectly Classified Instances: 20.821114369501466
Correctly Classified Instances: 79.17888563049853
Weighted Precision: 0.8313806990999308
Weighted AreaUnderROC: 0.8982370401579604
Root mean squared error: 0.13588733258636093
Relative absolute error: 35.40530568184338
Root relative squared error: 60.8550077544725
Weighted TruePositiveRate: 0.7917888563049853
Weighted MatthewsCorrelation: 0.7813802962291743
Weighted FMeasure: 0.7880814266596695
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7354697762432502
Mean absolute error: 0.03530723004283581
Coverage of cases: 92.96187683284458
Instances selection time: 12.0
Test time: 18.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7917888563049853
Weighted FalsePositiveRate: 0.020627251121077335
Kappa statistic: 0.7713289949658566
Training time: 0.0
		
Iteration: 3
Labeled set size: 108
Unlabelled set size: 234
	
Mean region size: 57.987343725883605
Incorrectly Classified Instances: 15.542521994134898
Correctly Classified Instances: 84.4574780058651
Weighted Precision: 0.8689599378197708
Weighted AreaUnderROC: 0.9273665201252008
Root mean squared error: 0.12044568960469267
Relative absolute error: 29.001848049120532
Root relative squared error: 53.939710459972986
Weighted TruePositiveRate: 0.844574780058651
Weighted MatthewsCorrelation: 0.8342445941420611
Weighted FMeasure: 0.8442721555097897
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7936452447348131
Mean absolute error: 0.028921510519898847
Coverage of cases: 95.30791788856305
Instances selection time: 12.0
Test time: 21.0
Accumulative iteration time: 34.0
Weighted Recall: 0.844574780058651
Weighted FalsePositiveRate: 0.016332262170848762
Kappa statistic: 0.8298563386115869
Training time: 0.0
		
Iteration: 4
Labeled set size: 128
Unlabelled set size: 214
	
Mean region size: 50.424448217317405
Incorrectly Classified Instances: 13.196480938416423
Correctly Classified Instances: 86.80351906158357
Weighted Precision: 0.889059490097459
Weighted AreaUnderROC: 0.9411527886720342
Root mean squared error: 0.1134290212116681
Relative absolute error: 25.80269613709214
Root relative squared error: 50.797405718677844
Weighted TruePositiveRate: 0.8680351906158358
Weighted MatthewsCorrelation: 0.8605745393994607
Weighted FMeasure: 0.8713339724429483
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8311282511450695
Mean absolute error: 0.025731220524524246
Coverage of cases: 94.72140762463343
Instances selection time: 13.0
Test time: 23.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8680351906158358
Weighted FalsePositiveRate: 0.01367788000500476
Kappa statistic: 0.855785496785835
Training time: 0.0
		
Iteration: 5
Labeled set size: 148
Unlabelled set size: 194
	
Mean region size: 44.71369038431862
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.8719711921690794
Weighted AreaUnderROC: 0.9350037338264942
Root mean squared error: 0.11871445393848352
Relative absolute error: 26.115087585165394
Root relative squared error: 53.16440375634761
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8428570746704745
Weighted FMeasure: 0.8572882490368998
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8126783309069585
Mean absolute error: 0.026042746622325832
Coverage of cases: 92.96187683284458
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 60.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.016264245640780786
Kappa statistic: 0.8397089377544209
Training time: 0.0
		
Iteration: 6
Labeled set size: 168
Unlabelled set size: 174
	
Mean region size: 38.52446365179819
Incorrectly Classified Instances: 14.662756598240469
Correctly Classified Instances: 85.33724340175954
Weighted Precision: 0.8732456055275971
Weighted AreaUnderROC: 0.936788454926497
Root mean squared error: 0.11627642355501464
Relative absolute error: 24.623420110016056
Root relative squared error: 52.07257013898414
Weighted TruePositiveRate: 0.8533724340175953
Weighted MatthewsCorrelation: 0.8437041336626637
Weighted FMeasure: 0.8571095036276793
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8216818472404543
Mean absolute error: 0.02455521119004393
Coverage of cases: 92.66862170087977
Instances selection time: 14.0
Test time: 28.0
Accumulative iteration time: 74.0
Weighted Recall: 0.8533724340175953
Weighted FalsePositiveRate: 0.015306202816397716
Kappa statistic: 0.8398504644806177
Training time: 0.0
		
Iteration: 7
Labeled set size: 188
Unlabelled set size: 154
	
Mean region size: 32.798271338169485
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.8815265399176331
Weighted AreaUnderROC: 0.9518143677608102
Root mean squared error: 0.10623670159449705
Relative absolute error: 21.47345455535171
Root relative squared error: 47.576438335294824
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8612358759811759
Weighted FMeasure: 0.8746915317305393
Iteration time: 13.0
Weighted AreaUnderPRC: 0.85757975750785
Mean absolute error: 0.021413971301735966
Coverage of cases: 95.30791788856305
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 87.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.015377736461657122
Kappa statistic: 0.8617806475939105
Training time: 0.0
		
Iteration: 8
Labeled set size: 208
Unlabelled set size: 134
	
Mean region size: 29.109430467664733
Incorrectly Classified Instances: 13.782991202346041
Correctly Classified Instances: 86.21700879765396
Weighted Precision: 0.8688972324134572
Weighted AreaUnderROC: 0.9493369149112316
Root mean squared error: 0.109856531740359
Relative absolute error: 21.443442288109072
Root relative squared error: 49.19752241578705
Weighted TruePositiveRate: 0.8621700879765396
Weighted MatthewsCorrelation: 0.8470347476295698
Weighted FMeasure: 0.8614169815655011
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8450777080133182
Mean absolute error: 0.021384042170967683
Coverage of cases: 94.72140762463343
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 99.0
Weighted Recall: 0.8621700879765396
Weighted FalsePositiveRate: 0.017955747410222034
Kappa statistic: 0.8486290954769123
Training time: 0.0
		
Iteration: 9
Labeled set size: 228
Unlabelled set size: 114
	
Mean region size: 24.834079333230516
Incorrectly Classified Instances: 12.609970674486803
Correctly Classified Instances: 87.3900293255132
Weighted Precision: 0.879436625696432
Weighted AreaUnderROC: 0.9583995967578186
Root mean squared error: 0.1029860657411207
Relative absolute error: 19.290461165091045
Root relative squared error: 46.12069212040415
Weighted TruePositiveRate: 0.873900293255132
Weighted MatthewsCorrelation: 0.8596126700208925
Weighted FMeasure: 0.8737369869189946
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8716989458171359
Mean absolute error: 0.019237024984578497
Coverage of cases: 95.89442815249267
Instances selection time: 11.0
Test time: 35.0
Accumulative iteration time: 110.0
Weighted Recall: 0.873900293255132
Weighted FalsePositiveRate: 0.017244053082953518
Kappa statistic: 0.8614659309927818
Training time: 0.0
		
Iteration: 10
Labeled set size: 248
Unlabelled set size: 94
	
Mean region size: 21.531100478468925
Incorrectly Classified Instances: 12.023460410557185
Correctly Classified Instances: 87.97653958944281
Weighted Precision: 0.882951781551282
Weighted AreaUnderROC: 0.960405760130659
Root mean squared error: 0.10051237582903925
Relative absolute error: 18.221073181932173
Root relative squared error: 45.01288894319331
Weighted TruePositiveRate: 0.8797653958944281
Weighted MatthewsCorrelation: 0.8650890199259926
Weighted FMeasure: 0.8790152468055495
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8782189818222027
Mean absolute error: 0.018170599295001776
Coverage of cases: 96.18768328445748
Instances selection time: 10.0
Test time: 37.0
Accumulative iteration time: 120.0
Weighted Recall: 0.8797653958944281
Weighted FalsePositiveRate: 0.016352398965492947
Kappa statistic: 0.8679966765488981
Training time: 0.0
		
Iteration: 11
Labeled set size: 268
Unlabelled set size: 74
	
Mean region size: 18.228121623707423
Incorrectly Classified Instances: 11.436950146627566
Correctly Classified Instances: 88.56304985337243
Weighted Precision: 0.8913906415659572
Weighted AreaUnderROC: 0.9652984306062757
Root mean squared error: 0.09718534170209041
Relative absolute error: 17.104686188074
Root relative squared error: 43.52292896133708
Weighted TruePositiveRate: 0.8856304985337243
Weighted MatthewsCorrelation: 0.8724382822669999
Weighted FMeasure: 0.8835207237850867
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8903349154999735
Mean absolute error: 0.017057304785891122
Coverage of cases: 93.54838709677419
Instances selection time: 9.0
Test time: 40.0
Accumulative iteration time: 129.0
Weighted Recall: 0.8856304985337243
Weighted FalsePositiveRate: 0.015080610245715378
Kappa statistic: 0.8744192634560907
Training time: 0.0
		
Iteration: 12
Labeled set size: 288
Unlabelled set size: 54
	
Mean region size: 15.017749652724184
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.8931235128002947
Weighted AreaUnderROC: 0.9668732887359639
Root mean squared error: 0.09531336378954618
Relative absolute error: 16.347019863339835
Root relative squared error: 42.68459305308249
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.8752155014264105
Weighted FMeasure: 0.886275388817881
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8951479078789156
Mean absolute error: 0.01630173725984042
Coverage of cases: 93.841642228739
Instances selection time: 7.0
Test time: 59.0
Accumulative iteration time: 136.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.014634783186985096
Kappa statistic: 0.8776797092556757
Training time: 0.0
		
Iteration: 13
Labeled set size: 308
Unlabelled set size: 34
	
Mean region size: 11.96172248803828
Incorrectly Classified Instances: 11.143695014662757
Correctly Classified Instances: 88.85630498533725
Weighted Precision: 0.8931235128002947
Weighted AreaUnderROC: 0.9662365542190527
Root mean squared error: 0.09626346244001663
Relative absolute error: 16.209371822601707
Root relative squared error: 43.11007981216025
Weighted TruePositiveRate: 0.8885630498533724
Weighted MatthewsCorrelation: 0.8752155014264105
Weighted FMeasure: 0.886275388817881
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8924639608390308
Mean absolute error: 0.016164470515614034
Coverage of cases: 93.54838709677419
Instances selection time: 13.0
Test time: 62.0
Accumulative iteration time: 149.0
Weighted Recall: 0.8885630498533724
Weighted FalsePositiveRate: 0.014634783186985096
Kappa statistic: 0.8776797092556757
Training time: 0.0
		
Iteration: 14
Labeled set size: 328
Unlabelled set size: 14
	
Mean region size: 8.967433245871273
Incorrectly Classified Instances: 10.557184750733137
Correctly Classified Instances: 89.44281524926686
Weighted Precision: 0.9015914660041869
Weighted AreaUnderROC: 0.9677382821107109
Root mean squared error: 0.09456034333776636
Relative absolute error: 15.609646068848745
Root relative squared error: 42.34736467012623
Weighted TruePositiveRate: 0.8944281524926686
Weighted MatthewsCorrelation: 0.8828612155482395
Weighted FMeasure: 0.892171008493865
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8969096796085839
Mean absolute error: 0.015566406052037667
Coverage of cases: 93.841642228739
Instances selection time: 3.0
Test time: 46.0
Accumulative iteration time: 153.0
Weighted Recall: 0.8944281524926686
Weighted FalsePositiveRate: 0.013731676939428327
Kappa statistic: 0.8841187131853194
Training time: 1.0
		
Time end:Wed Nov 01 09.34.32 EET 2017