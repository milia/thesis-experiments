Tue Oct 31 11.23.00 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.00 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 71.19565217391305
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7964321334902399
Weighted AreaUnderROC: 0.8080491004497751
Root mean squared error: 0.4183005168616369
Relative absolute error: 48.984371459106704
Root relative squared error: 83.66010337232737
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5601872640687252
Weighted FMeasure: 0.7903442897385387
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7923799269047944
Mean absolute error: 0.24492185729553353
Coverage of cases: 90.76086956521739
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.21552826527912514
Kappa statistic: 0.5572556762092792
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 72.28260869565217
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7902687382471048
Weighted AreaUnderROC: 0.821794525531352
Root mean squared error: 0.39626078936955034
Relative absolute error: 46.785695485902785
Root relative squared error: 79.25215787391006
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5495766927014777
Weighted FMeasure: 0.7889424173590689
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8113912821209305
Mean absolute error: 0.23392847742951392
Coverage of cases: 92.93478260869566
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.2337838433724314
Kappa statistic: 0.5492462311557788
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 76.3586956521739
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8410983432713871
Weighted AreaUnderROC: 0.8211537613546168
Root mean squared error: 0.38043526138338823
Relative absolute error: 46.28588606607141
Root relative squared error: 76.08705227667765
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.656693046464772
Weighted FMeasure: 0.8404231765600122
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8041415358452096
Mean absolute error: 0.23142943033035707
Coverage of cases: 95.1086956521739
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.2019247729076638
Kappa statistic: 0.6544041450777202
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 79.34782608695652
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8299568240279703
Weighted AreaUnderROC: 0.8239097363083164
Root mean squared error: 0.3782278501009506
Relative absolute error: 47.46715118443894
Root relative squared error: 75.64557002019012
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6339652647860852
Weighted FMeasure: 0.8300837019889901
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8068078135364303
Mean absolute error: 0.2373357559221947
Coverage of cases: 95.65217391304348
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.20829658700061734
Kappa statistic: 0.6328527291452112
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 68.75
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8356204710144928
Weighted AreaUnderROC: 0.8294478679777759
Root mean squared error: 0.3792675194785697
Relative absolute error: 42.154887673199255
Root relative squared error: 75.85350389571394
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6465119719580276
Weighted FMeasure: 0.8358678903999465
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8156901132738185
Mean absolute error: 0.21077443836599627
Coverage of cases: 93.47826086956522
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1990254872563718
Kappa statistic: 0.6457905544147845
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 69.02173913043478
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8251825551014939
Weighted AreaUnderROC: 0.8458018233530293
Root mean squared error: 0.37016702923916156
Relative absolute error: 40.624822625014204
Root relative squared error: 74.03340584783231
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.624661855288119
Weighted FMeasure: 0.8255323161228417
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8326819539384634
Mean absolute error: 0.20312411312507103
Coverage of cases: 94.02173913043478
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20539730134932535
Kappa statistic: 0.6244897959183674
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 69.02173913043478
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8164868693472813
Weighted AreaUnderROC: 0.8832403651115619
Root mean squared error: 0.37573726210910785
Relative absolute error: 40.593615942675335
Root relative squared error: 75.14745242182157
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6060096813907437
Weighted FMeasure: 0.815752993068683
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8855420441500448
Mean absolute error: 0.2029680797133767
Coverage of cases: 95.1086956521739
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.2056839227445101
Kappa statistic: 0.6058467741935484
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 67.1195652173913
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8072808833678399
Weighted AreaUnderROC: 0.8841277890466531
Root mean squared error: 0.3801302598795566
Relative absolute error: 40.583640642431824
Root relative squared error: 76.02605197591133
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5857929269761979
Weighted FMeasure: 0.805426610003269
Iteration time: 1.0
Weighted AreaUnderPRC: 0.888076342236838
Mean absolute error: 0.20291820321215912
Coverage of cases: 94.56521739130434
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.2120557368374636
Kappa statistic: 0.5851703406813628
Training time: 0.0
		
Time end:Tue Oct 31 11.23.00 EET 2017