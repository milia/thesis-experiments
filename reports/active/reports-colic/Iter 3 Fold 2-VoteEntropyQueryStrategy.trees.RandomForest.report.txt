Tue Oct 31 11.23.53 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.53 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.754514629477714
Weighted AreaUnderROC: 0.8005831643002028
Root mean squared error: 0.4291179518376665
Relative absolute error: 72.93751373686348
Root relative squared error: 85.8235903675333
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.3864606134965403
Weighted FMeasure: 0.680418207318628
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7999510494770908
Mean absolute error: 0.3646875686843174
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.4484853161654466
Kappa statistic: 0.3156359393232206
Training time: 3.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 96.46739130434783
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7760649978041283
Weighted AreaUnderROC: 0.8412778904665316
Root mean squared error: 0.4129417212018031
Relative absolute error: 69.89215450031409
Root relative squared error: 82.58834424036063
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.5101113982193565
Weighted FMeasure: 0.7593528816986855
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8464076139425557
Mean absolute error: 0.3494607725015704
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.22855851486021697
Kappa statistic: 0.5009643201542913
Training time: 4.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 97.55434782608695
Incorrectly Classified Instances: 34.23913043478261
Correctly Classified Instances: 65.76086956521739
Weighted Precision: 0.7572477634125915
Weighted AreaUnderROC: 0.8640973630831644
Root mean squared error: 0.45224976496801805
Relative absolute error: 78.94464203266591
Root relative squared error: 90.44995299360362
Weighted TruePositiveRate: 0.657608695652174
Weighted MatthewsCorrelation: 0.41559303896666294
Weighted FMeasure: 0.6571131359442122
Iteration time: 7.0
Weighted AreaUnderPRC: 0.86130299009601
Mean absolute error: 0.39472321016332956
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.657608695652174
Weighted FalsePositiveRate: 0.24330849281241732
Kappa statistic: 0.3605472197705208
Training time: 6.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 94.83695652173913
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.7677790482796741
Weighted AreaUnderROC: 0.8504056795131846
Root mean squared error: 0.45211474881093877
Relative absolute error: 76.89470178331285
Root relative squared error: 90.42294976218776
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.44499119216908306
Weighted FMeasure: 0.6806267590345815
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8435865383357543
Mean absolute error: 0.3844735089165643
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.23056486462651027
Kappa statistic: 0.3947368421052632
Training time: 7.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 96.19565217391305
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8198803546629634
Weighted AreaUnderROC: 0.89579107505071
Root mean squared error: 0.3780341740535487
Relative absolute error: 61.704460646395795
Root relative squared error: 75.60683481070974
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6120047714437539
Weighted FMeasure: 0.8166683827474273
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9019056656105545
Mean absolute error: 0.308522303231979
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.1935135373489726
Kappa statistic: 0.6105577689243027
Training time: 8.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 92.66304347826087
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8155484642999601
Weighted AreaUnderROC: 0.8804513184584178
Root mean squared error: 0.3719738106103002
Relative absolute error: 56.475407540543465
Root relative squared error: 74.39476212206004
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.6022252499594984
Weighted FMeasure: 0.8114793419141243
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8896214797666787
Mean absolute error: 0.2823770377027173
Coverage of cases: 99.45652173913044
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 45.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.19669944439544934
Kappa statistic: 0.6002979145978155
Training time: 12.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 93.20652173913044
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8522822754955989
Weighted AreaUnderROC: 0.9188640973630832
Root mean squared error: 0.3249817756046503
Relative absolute error: 47.97386592579815
Root relative squared error: 64.99635512093006
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6806106079353813
Weighted FMeasure: 0.8514284747282873
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9246665779563471
Mean absolute error: 0.23986932962899074
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18946776611694152
Kappa statistic: 0.678238341968912
Training time: 13.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 95.92391304347827
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8256502329192547
Weighted AreaUnderROC: 0.90276369168357
Root mean squared error: 0.3583047303927425
Relative absolute error: 59.4179355710509
Root relative squared error: 71.6609460785485
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6191993935863278
Weighted FMeasure: 0.8219483595061274
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9096309224643812
Mean absolute error: 0.2970896778552545
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.23582326483816915
Kappa statistic: 0.6126315789473684
Training time: 21.0
		
Time end:Tue Oct 31 11.23.53 EET 2017