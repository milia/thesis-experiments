Tue Oct 31 11.23.39 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.39 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.56521739130435
Correctly Classified Instances: 70.43478260869566
Weighted Precision: 0.7007711762697155
Weighted AreaUnderROC: 0.6794117647058823
Root mean squared error: 0.539639680642219
Relative absolute error: 59.1304347826087
Root relative squared error: 107.92793612844382
Weighted TruePositiveRate: 0.7043478260869567
Weighted MatthewsCorrelation: 0.35702910433114876
Weighted FMeasure: 0.6983984752791861
Iteration time: 10.6
Weighted AreaUnderPRC: 0.6556374531511303
Mean absolute error: 0.29565217391304344
Coverage of cases: 70.43478260869566
Instances selection time: 1.4
Test time: 3.3
Accumulative iteration time: 10.6
Weighted Recall: 0.7043478260869567
Weighted FalsePositiveRate: 0.3455242966751918
Kappa statistic: 0.3560628706800998
Training time: 9.2
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.22826086956522
Correctly Classified Instances: 72.77173913043478
Weighted Precision: 0.7376397403321606
Weighted AreaUnderROC: 0.6872971602434077
Root mean squared error: 0.5176263038888052
Relative absolute error: 54.45652173913044
Root relative squared error: 103.52526077776103
Weighted TruePositiveRate: 0.7277173913043478
Weighted MatthewsCorrelation: 0.4096675414932043
Weighted FMeasure: 0.7103959824509419
Iteration time: 11.7
Weighted AreaUnderPRC: 0.6649750338854439
Mean absolute error: 0.2722826086956522
Coverage of cases: 72.77173913043478
Instances selection time: 1.6
Test time: 1.3
Accumulative iteration time: 22.3
Weighted Recall: 0.7277173913043478
Weighted FalsePositiveRate: 0.3531230708175324
Kappa statistic: 0.3874614256880103
Training time: 10.1
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.5
Correctly Classified Instances: 72.49999999999999
Weighted Precision: 0.7359816565243443
Weighted AreaUnderROC: 0.6787525354969575
Root mean squared error: 0.522613181747388
Relative absolute error: 55.0
Root relative squared error: 104.52263634947761
Weighted TruePositiveRate: 0.7250000000000002
Weighted MatthewsCorrelation: 0.3992579757974594
Weighted FMeasure: 0.7032750274982056
Iteration time: 12.2
Weighted AreaUnderPRC: 0.6574403762866735
Mean absolute error: 0.275
Coverage of cases: 72.49999999999999
Instances selection time: 0.8
Test time: 4.0
Accumulative iteration time: 34.5
Weighted Recall: 0.7250000000000002
Weighted FalsePositiveRate: 0.36749492900608516
Kappa statistic: 0.3708038983754388
Training time: 11.4
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.206521739130437
Correctly Classified Instances: 71.79347826086956
Weighted Precision: 0.7370868780869075
Weighted AreaUnderROC: 0.6682809330628803
Root mean squared error: 0.5291662733888162
Relative absolute error: 56.413043478260875
Root relative squared error: 105.83325467776324
Weighted TruePositiveRate: 0.7179347826086956
Weighted MatthewsCorrelation: 0.38840617761581153
Weighted FMeasure: 0.6910591714219563
Iteration time: 14.0
Weighted AreaUnderPRC: 0.650676675387819
Mean absolute error: 0.2820652173913043
Coverage of cases: 71.79347826086956
Instances selection time: 0.9
Test time: 3.7
Accumulative iteration time: 48.5
Weighted Recall: 0.7179347826086956
Weighted FalsePositiveRate: 0.38137291648293503
Kappa statistic: 0.35036671777576744
Training time: 13.1
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.684782608695656
Correctly Classified Instances: 73.31521739130434
Weighted Precision: 0.7612329456340742
Weighted AreaUnderROC: 0.6730476673427991
Root mean squared error: 0.5147624865441583
Relative absolute error: 53.36956521739131
Root relative squared error: 102.95249730883168
Weighted TruePositiveRate: 0.7331521739130434
Weighted MatthewsCorrelation: 0.4147814859105945
Weighted FMeasure: 0.7001415082904101
Iteration time: 15.3
Weighted AreaUnderPRC: 0.6580265696523016
Mean absolute error: 0.2668478260869565
Coverage of cases: 73.31521739130434
Instances selection time: 1.1
Test time: 3.3
Accumulative iteration time: 63.8
Weighted Recall: 0.7331521739130434
Weighted FalsePositiveRate: 0.38705683922744505
Kappa statistic: 0.3648208011744622
Training time: 14.2
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.695652173913043
Correctly Classified Instances: 76.30434782608695
Weighted Precision: 0.7718827893046073
Weighted AreaUnderROC: 0.7107505070993915
Root mean squared error: 0.4850634607558709
Relative absolute error: 47.391304347826086
Root relative squared error: 97.01269215117419
Weighted TruePositiveRate: 0.7630434782608697
Weighted MatthewsCorrelation: 0.47694002900730526
Weighted FMeasure: 0.7441985234268687
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6877694012577358
Mean absolute error: 0.23695652173913045
Coverage of cases: 76.30434782608695
Instances selection time: 0.7
Test time: 3.9
Accumulative iteration time: 83.8
Weighted Recall: 0.7630434782608697
Weighted FalsePositiveRate: 0.3415424640620866
Kappa statistic: 0.447444778940413
Training time: 19.3
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.021739130434785
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8130556295002606
Weighted AreaUnderROC: 0.7806795131845841
Root mean squared error: 0.4355713143152311
Relative absolute error: 38.04347826086957
Root relative squared error: 87.11426286304622
Weighted TruePositiveRate: 0.8097826086956521
Weighted MatthewsCorrelation: 0.5884132386289915
Weighted FMeasure: 0.8052711596413245
Iteration time: 24.2
Weighted AreaUnderPRC: 0.746007186922023
Mean absolute error: 0.19021739130434784
Coverage of cases: 80.97826086956522
Instances selection time: 0.7
Test time: 3.6
Accumulative iteration time: 108.0
Weighted Recall: 0.8097826086956521
Weighted FalsePositiveRate: 0.24842358232648384
Kappa statistic: 0.5784896974166612
Training time: 23.5
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.043478260869563
Correctly Classified Instances: 81.95652173913044
Weighted Precision: 0.819475630349712
Weighted AreaUnderROC: 0.7984787018255578
Root mean squared error: 0.423868245068218
Relative absolute error: 36.086956521739125
Root relative squared error: 84.7736490136436
Weighted TruePositiveRate: 0.8195652173913043
Weighted MatthewsCorrelation: 0.6095003568599193
Weighted FMeasure: 0.817666461591276
Iteration time: 32.4
Weighted AreaUnderPRC: 0.7602703993443138
Mean absolute error: 0.18043478260869564
Coverage of cases: 81.95652173913044
Instances selection time: 0.4
Test time: 3.2
Accumulative iteration time: 140.4
Weighted Recall: 0.8195652173913043
Weighted FalsePositiveRate: 0.2226078137401888
Kappa statistic: 0.6064509308664873
Training time: 32.0
		
Time end:Tue Oct 31 11.23.43 EET 2017