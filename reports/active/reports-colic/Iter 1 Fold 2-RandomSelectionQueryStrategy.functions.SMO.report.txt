Tue Oct 31 11.23.20 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.20 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8335295480590397
Weighted AreaUnderROC: 0.7963995943204868
Root mean squared error: 0.4104610345325914
Relative absolute error: 33.69565217391305
Root relative squared error: 82.09220690651829
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6318550616768159
Weighted FMeasure: 0.8261946083872442
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7651586596174574
Mean absolute error: 0.16847826086956522
Coverage of cases: 83.15217391304348
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.23872255048946114
Kappa statistic: 0.6211477151965994
Training time: 7.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8099587322858806
Weighted AreaUnderROC: 0.7730730223123732
Root mean squared error: 0.4361391879943235
Relative absolute error: 38.04347826086957
Root relative squared error: 87.2278375988647
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.582128230475561
Weighted FMeasure: 0.8037681062436627
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7402242558701737
Mean absolute error: 0.19021739130434784
Coverage of cases: 80.97826086956522
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.26363656407090574
Kappa statistic: 0.5722635494155155
Training time: 7.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8189211495946942
Weighted AreaUnderROC: 0.7938640973630832
Root mean squared error: 0.4234947769299599
Relative absolute error: 35.869565217391305
Root relative squared error: 84.69895538599198
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6078255671669001
Weighted FMeasure: 0.8176461480110404
Iteration time: 10.0
Weighted AreaUnderPRC: 0.757036765883823
Mean absolute error: 0.1793478260869565
Coverage of cases: 82.06521739130434
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.23292397918687718
Kappa statistic: 0.6042752867570386
Training time: 9.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7907117869636611
Weighted AreaUnderROC: 0.763184584178499
Root mean squared error: 0.4544466295731929
Relative absolute error: 41.30434782608695
Root relative squared error: 90.88932591463858
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5468580689168295
Weighted FMeasure: 0.789549931842422
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7263390820117891
Mean absolute error: 0.20652173913043478
Coverage of cases: 79.34782608695652
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.26710909251256726
Kappa statistic: 0.5428870292887028
Training time: 10.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7628108177019047
Weighted AreaUnderROC: 0.7385902636916836
Root mean squared error: 0.4834207816942845
Relative absolute error: 46.73913043478261
Root relative squared error: 96.6841563388569
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.4892701161705071
Weighted FMeasure: 0.7633860893820872
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7009768083944745
Mean absolute error: 0.23369565217391305
Coverage of cases: 76.6304347826087
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.28912382044271984
Kappa statistic: 0.4875647668393781
Training time: 11.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7906732117812062
Weighted AreaUnderROC: 0.7662271805273833
Root mean squared error: 0.4544466295731929
Relative absolute error: 41.30434782608695
Root relative squared error: 90.88932591463858
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5482509231993881
Weighted FMeasure: 0.7904664855072465
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7281122324531983
Mean absolute error: 0.20652173913043478
Coverage of cases: 79.34782608695652
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.2610238998147985
Kappa statistic: 0.5457380457380457
Training time: 17.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8020715977283702
Weighted AreaUnderROC: 0.7809330628803245
Root mean squared error: 0.4423258684646914
Relative absolute error: 39.130434782608695
Root relative squared error: 88.46517369293828
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.573773571857796
Weighted FMeasure: 0.802298979792586
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7416368351128967
Mean absolute error: 0.1956521739130435
Coverage of cases: 80.43478260869566
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24248170032630748
Kappa statistic: 0.5723140495867769
Training time: 28.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8076138540899042
Weighted AreaUnderROC: 0.7822008113590264
Root mean squared error: 0.4361391879943235
Relative absolute error: 38.04347826086957
Root relative squared error: 87.2278375988647
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5837013427582051
Weighted FMeasure: 0.8065943994056488
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7449048572618627
Mean absolute error: 0.19021739130434784
Coverage of cases: 80.97826086956522
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 122.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.24538098597759944
Kappa statistic: 0.5802919708029198
Training time: 25.0
		
Time end:Tue Oct 31 11.23.21 EET 2017