Tue Oct 31 11.23.52 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.52 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 96.46739130434783
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8251825551014939
Weighted AreaUnderROC: 0.8286637931034483
Root mean squared error: 0.39731898016538025
Relative absolute error: 67.85917963198337
Root relative squared error: 79.46379603307605
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.624661855288119
Weighted FMeasure: 0.8255323161228417
Iteration time: 3.0
Weighted AreaUnderPRC: 0.822987809157835
Mean absolute error: 0.33929589815991684
Coverage of cases: 98.3695652173913
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20539730134932535
Kappa statistic: 0.6244897959183674
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 86.41304347826087
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7863879165127216
Weighted AreaUnderROC: 0.8216346789840373
Root mean squared error: 0.39848120087522826
Relative absolute error: 58.04073290604536
Root relative squared error: 79.69624017504565
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.532401399274306
Weighted FMeasure: 0.7813416041000814
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8139328020399696
Mean absolute error: 0.29020366453022683
Coverage of cases: 96.73913043478261
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.28855057765235026
Kappa statistic: 0.5233793836344313
Training time: 3.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 85.05434782608695
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8064470163000036
Weighted AreaUnderROC: 0.8612848906429139
Root mean squared error: 0.3857592115556275
Relative absolute error: 54.332804410432146
Root relative squared error: 77.15184231112549
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5597787412015698
Weighted FMeasure: 0.7875170691593297
Iteration time: 5.0
Weighted AreaUnderPRC: 0.866635699195513
Mean absolute error: 0.2716640220521607
Coverage of cases: 97.28260869565217
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.30043434165270305
Kappa statistic: 0.5359869138495092
Training time: 4.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 88.8586956521739
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.7395323346729995
Weighted AreaUnderROC: 0.8317628869388835
Root mean squared error: 0.4378367071181712
Relative absolute error: 64.553431701072
Root relative squared error: 87.56734142363423
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.2898796377358003
Weighted FMeasure: 0.6107498016299713
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8258110277701575
Mean absolute error: 0.32276715850536
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.5255533997707029
Kappa statistic: 0.1905339805825245
Training time: 5.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 82.8804347826087
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8138547560604824
Weighted AreaUnderROC: 0.8459685598377282
Root mean squared error: 0.3949262058741222
Relative absolute error: 54.32186880948817
Root relative squared error: 78.98524117482444
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5640728548164448
Weighted FMeasure: 0.7844891446806086
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8385575156393962
Mean absolute error: 0.27160934404744086
Coverage of cases: 95.65217391304348
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.31260472704824055
Kappa statistic: 0.5298342541436464
Training time: 5.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 86.41304347826087
Incorrectly Classified Instances: 27.717391304347824
Correctly Classified Instances: 72.28260869565217
Weighted Precision: 0.7752861080415094
Weighted AreaUnderROC: 0.846475659229209
Root mean squared error: 0.42358872697171984
Relative absolute error: 60.85850339670101
Root relative squared error: 84.71774539434396
Weighted TruePositiveRate: 0.7228260869565217
Weighted MatthewsCorrelation: 0.39798218668268487
Weighted FMeasure: 0.6729859064668462
Iteration time: 10.0
Weighted AreaUnderPRC: 0.838574542339112
Mean absolute error: 0.30429251698350507
Coverage of cases: 97.28260869565217
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7228260869565217
Weighted FalsePositiveRate: 0.46065570156098423
Kappa statistic: 0.3059171597633136
Training time: 10.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 90.76086956521739
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8238815374921235
Weighted AreaUnderROC: 0.8611815415821501
Root mean squared error: 0.37510149727319425
Relative absolute error: 55.330242621547576
Root relative squared error: 75.02029945463885
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.5980586126161953
Weighted FMeasure: 0.8053898869048691
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8654101992970407
Mean absolute error: 0.2766512131077379
Coverage of cases: 98.91304347826087
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.27870623511773523
Kappa statistic: 0.575
Training time: 12.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 92.3913043478261
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8092397544052313
Weighted AreaUnderROC: 0.8504056795131846
Root mean squared error: 0.3777103172277115
Relative absolute error: 58.18890378354637
Root relative squared error: 75.5420634455423
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5905795816894683
Weighted FMeasure: 0.8094862916658028
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8649656920388568
Mean absolute error: 0.2909445189177319
Coverage of cases: 99.45652173913044
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 61.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.22104021518652442
Kappa statistic: 0.5905391658189219
Training time: 15.0
		
Time end:Tue Oct 31 11.23.52 EET 2017