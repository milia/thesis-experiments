Tue Oct 31 11.21.47 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.47 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 62.5
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7495950863465219
Weighted AreaUnderROC: 0.8180381316694595
Root mean squared error: 0.4666438033678881
Relative absolute error: 52.701241908842356
Root relative squared error: 93.32876067357762
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.4596468440706653
Weighted FMeasure: 0.742188883344333
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8141680699286448
Mean absolute error: 0.2635062095442118
Coverage of cases: 86.41304347826087
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.26854219948849106
Kappa statistic: 0.4566929133858267
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 70.1086956521739
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.8005975082634122
Weighted AreaUnderROC: 0.8448275862068965
Root mean squared error: 0.41160801394007207
Relative absolute error: 46.83225411360108
Root relative squared error: 82.32160278801442
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5696951000563544
Weighted FMeasure: 0.795527087646653
Iteration time: 6.0
Weighted AreaUnderPRC: 0.847412181883805
Mean absolute error: 0.2341612705680054
Coverage of cases: 91.84782608695652
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.21234235823264838
Kappa statistic: 0.5673267326732673
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 70.92391304347827
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7814139925414798
Weighted AreaUnderROC: 0.844447261663286
Root mean squared error: 0.40165311335298465
Relative absolute error: 46.72275536085766
Root relative squared error: 80.33062267059692
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.530758439133696
Weighted FMeasure: 0.7819153951535521
Iteration time: 5.0
Weighted AreaUnderPRC: 0.848840807332489
Mean absolute error: 0.2336137768042883
Coverage of cases: 93.47826086956522
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 18.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2552253285122145
Kappa statistic: 0.5306122448979592
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 70.3804347826087
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8103969754253307
Weighted AreaUnderROC: 0.8525608519269777
Root mean squared error: 0.3908939643989501
Relative absolute error: 44.074678396050366
Root relative squared error: 78.17879287979001
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5930633506894811
Weighted FMeasure: 0.8100651067963033
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8581484492590298
Mean absolute error: 0.22037339198025183
Coverage of cases: 92.93478260869566
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 23.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.21495502248875561
Kappa statistic: 0.5930232558139535
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 70.1086956521739
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.805677236569685
Weighted AreaUnderROC: 0.8548427991886409
Root mean squared error: 0.3942329255656439
Relative absolute error: 44.263735524904455
Root relative squared error: 78.84658511312878
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5828179631345256
Weighted FMeasure: 0.8049149338374291
Iteration time: 3.0
Weighted AreaUnderPRC: 0.858900543718271
Mean absolute error: 0.22131867762452226
Coverage of cases: 92.93478260869566
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.2181409295352324
Kappa statistic: 0.5826612903225807
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 71.19565217391305
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8028132361068632
Weighted AreaUnderROC: 0.8553498985801218
Root mean squared error: 0.3957070862997476
Relative absolute error: 44.61974238951094
Root relative squared error: 79.14141725994952
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5758759537627943
Weighted FMeasure: 0.8002638009891858
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8596204991360025
Mean absolute error: 0.2230987119475547
Coverage of cases: 94.56521739130434
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.2152416438839404
Kappa statistic: 0.5749250749250749
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 69.02173913043478
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8242974958281688
Weighted AreaUnderROC: 0.860420892494929
Root mean squared error: 0.3919053124704393
Relative absolute error: 42.87825153413064
Root relative squared error: 78.38106249408786
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6219060039332348
Weighted FMeasure: 0.8218569035849493
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8619655072572538
Mean absolute error: 0.2143912576706532
Coverage of cases: 92.93478260869566
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.1903276303024958
Kappa statistic: 0.6208791208791209
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 65.48913043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8137081676868388
Weighted AreaUnderROC: 0.8586460446247465
Root mean squared error: 0.40759698183684906
Relative absolute error: 43.81874840294003
Root relative squared error: 81.51939636736981
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5963521554537511
Weighted FMeasure: 0.8066416625082498
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8601635757344777
Mean absolute error: 0.21909374201470014
Coverage of cases: 89.67391304347827
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 36.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.19380015874415732
Kappa statistic: 0.5925196850393701
Training time: 1.0
		
Time end:Tue Oct 31 11.21.47 EET 2017