Tue Oct 31 11.23.22 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.22 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7584239130434783
Weighted AreaUnderROC: 0.7373225152129818
Root mean squared error: 0.48900964692182575
Relative absolute error: 47.82608695652174
Root relative squared error: 97.80192938436515
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.48102957532012225
Weighted FMeasure: 0.7592729059199214
Iteration time: 9.0
Weighted AreaUnderPRC: 0.698523156899811
Mean absolute error: 0.2391304347826087
Coverage of cases: 76.08695652173913
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.28622453479142784
Kappa statistic: 0.48049281314168385
Training time: 8.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.748587570621469
Weighted AreaUnderROC: 0.7287018255578094
Root mean squared error: 0.5
Relative absolute error: 50.0
Root relative squared error: 100.0
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4603308770178786
Weighted FMeasure: 0.7492027044265849
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6899664114147789
Mean absolute error: 0.25
Coverage of cases: 75.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 20.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.29259634888438135
Kappa statistic: 0.46020408163265303
Training time: 10.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.756206679269061
Weighted AreaUnderROC: 0.7390973630831644
Root mean squared error: 0.49453535504684026
Relative absolute error: 48.91304347826087
Root relative squared error: 98.90707100936805
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.47677641918173974
Weighted FMeasure: 0.7557979944523899
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6984010712035287
Mean absolute error: 0.24456521739130435
Coverage of cases: 75.54347826086956
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.27724005644236704
Kappa statistic: 0.47674418604651164
Training time: 12.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7704718519014762
Weighted AreaUnderROC: 0.7520283975659229
Root mean squared error: 0.47776654295295456
Relative absolute error: 45.65217391304348
Root relative squared error: 95.55330859059092
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5072825850950902
Weighted FMeasure: 0.7710111649112299
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7114208228839288
Mean absolute error: 0.22826086956521738
Coverage of cases: 77.17391304347827
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.2676823353029368
Kappa statistic: 0.5071428571428571
Training time: 12.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7887208569628229
Weighted AreaUnderROC: 0.7740872210953347
Root mean squared error: 0.4603873605336385
Relative absolute error: 42.391304347826086
Root relative squared error: 92.0774721067277
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5465485780863846
Weighted FMeasure: 0.7883582618587378
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7313405797101449
Mean absolute error: 0.21195652173913043
Coverage of cases: 78.80434782608695
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.23986903607020021
Kappa statistic: 0.5465116279069766
Training time: 15.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8142404144614903
Weighted AreaUnderROC: 0.7986815415821501
Root mean squared error: 0.42986347681054754
Relative absolute error: 36.95652173913043
Root relative squared error: 85.9726953621095
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6011860012495134
Weighted FMeasure: 0.8146280858805194
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7574113099208418
Mean absolute error: 0.18478260869565216
Coverage of cases: 81.52173913043478
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 79.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.2178543081400476
Kappa statistic: 0.6010204081632652
Training time: 17.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8251825551014939
Weighted AreaUnderROC: 0.8103448275862069
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.624661855288119
Weighted FMeasure: 0.8255323161228417
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7695509450339478
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.20539730134932535
Kappa statistic: 0.6244897959183674
Training time: 22.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7692697768762677
Root mean squared error: 0.4544466295731929
Relative absolute error: 41.30434782608695
Root relative squared error: 90.88932591463858
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7299654167237307
Mean absolute error: 0.20652173913043478
Coverage of cases: 79.34782608695652
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 131.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 29.0
		
Time end:Tue Oct 31 11.23.23 EET 2017