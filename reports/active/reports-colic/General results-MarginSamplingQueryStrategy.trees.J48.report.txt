Tue Oct 31 11.23.09 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.09 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 79.91847826086956
Incorrectly Classified Instances: 21.793478260869566
Correctly Classified Instances: 78.20652173913042
Weighted Precision: 0.7376895747924176
Weighted AreaUnderROC: 0.742317444219067
Root mean squared error: 0.4158312039526245
Relative absolute error: 57.07010617910014
Root relative squared error: 83.16624079052488
Weighted TruePositiveRate: 0.7820652173913044
Weighted MatthewsCorrelation: 0.49363469488506934
Weighted FMeasure: 0.7533737802525181
Iteration time: 2.4
Weighted AreaUnderPRC: 0.72529845759859
Mean absolute error: 0.2853505308955006
Coverage of cases: 93.36956521739131
Instances selection time: 1.1
Test time: 1.9
Accumulative iteration time: 2.4
Weighted Recall: 0.7820652173913044
Weighted FalsePositiveRate: 0.29083803686392096
Kappa statistic: 0.491596168507267
Training time: 1.3
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 84.97282608695652
Incorrectly Classified Instances: 25.65217391304348
Correctly Classified Instances: 74.34782608695653
Weighted Precision: 0.6519411158395287
Weighted AreaUnderROC: 0.6776548306728989
Root mean squared error: 0.44035907568422294
Relative absolute error: 62.02705284117303
Root relative squared error: 88.07181513684459
Weighted TruePositiveRate: 0.7434782608695653
Weighted MatthewsCorrelation: 0.36325025876116346
Weighted FMeasure: 0.6823179215788178
Iteration time: 1.8
Weighted AreaUnderPRC: 0.6774109914978896
Mean absolute error: 0.3101352642058652
Coverage of cases: 94.78260869565217
Instances selection time: 0.8
Test time: 1.3
Accumulative iteration time: 4.2
Weighted Recall: 0.7434782608695653
Weighted FalsePositiveRate: 0.3956080783137843
Kappa statistic: 0.35680754286468036
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 85.02717391304347
Incorrectly Classified Instances: 23.152173913043477
Correctly Classified Instances: 76.8478260869565
Weighted Precision: 0.7007909451975416
Weighted AreaUnderROC: 0.7165758113590264
Root mean squared error: 0.43369159375564437
Relative absolute error: 56.69286581781789
Root relative squared error: 86.73831875112887
Weighted TruePositiveRate: 0.7684782608695653
Weighted MatthewsCorrelation: 0.4418697352784505
Weighted FMeasure: 0.724441331463904
Iteration time: 2.4
Weighted AreaUnderPRC: 0.7077271940107209
Mean absolute error: 0.2834643290890895
Coverage of cases: 94.94565217391303
Instances selection time: 1.1
Test time: 1.1
Accumulative iteration time: 6.6
Weighted Recall: 0.7684782608695653
Weighted FalsePositiveRate: 0.33592247993650237
Kappa statistic: 0.4380095394323236
Training time: 1.3
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 82.20108695652173
Incorrectly Classified Instances: 22.77173913043478
Correctly Classified Instances: 77.2282608695652
Weighted Precision: 0.7037728200152384
Weighted AreaUnderROC: 0.7202712981744421
Root mean squared error: 0.43319136269061287
Relative absolute error: 54.8921108880224
Root relative squared error: 86.63827253812259
Weighted TruePositiveRate: 0.7722826086956521
Weighted MatthewsCorrelation: 0.44848390783354725
Weighted FMeasure: 0.7279656527347866
Iteration time: 2.3
Weighted AreaUnderPRC: 0.7129001723704672
Mean absolute error: 0.27446055444011197
Coverage of cases: 94.29347826086956
Instances selection time: 0.8
Test time: 0.9
Accumulative iteration time: 8.9
Weighted Recall: 0.7722826086956521
Weighted FalsePositiveRate: 0.3349093835435224
Kappa statistic: 0.444834576483579
Training time: 1.5
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 85.24456521739131
Incorrectly Classified Instances: 22.77173913043478
Correctly Classified Instances: 77.2282608695652
Weighted Precision: 0.7037701125604755
Weighted AreaUnderROC: 0.7200621196754564
Root mean squared error: 0.4357130662427857
Relative absolute error: 55.44005442697155
Root relative squared error: 87.14261324855714
Weighted TruePositiveRate: 0.7722826086956522
Weighted MatthewsCorrelation: 0.4481385234630132
Weighted FMeasure: 0.7277543371433461
Iteration time: 2.5
Weighted AreaUnderPRC: 0.7130482481273892
Mean absolute error: 0.2772002721348577
Coverage of cases: 95.16304347826086
Instances selection time: 0.6
Test time: 1.7
Accumulative iteration time: 11.4
Weighted Recall: 0.7722826086956522
Weighted FalsePositiveRate: 0.33673494135285303
Kappa statistic: 0.44414340869028257
Training time: 1.9
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 90.27173913043478
Incorrectly Classified Instances: 20.217391304347824
Correctly Classified Instances: 79.78260869565216
Weighted Precision: 0.7529644187678602
Weighted AreaUnderROC: 0.7492203346855983
Root mean squared error: 0.41129126221652224
Relative absolute error: 53.790803779903776
Root relative squared error: 82.25825244330444
Weighted TruePositiveRate: 0.7978260869565218
Weighted MatthewsCorrelation: 0.5235277299522288
Weighted FMeasure: 0.7673037369328934
Iteration time: 2.6
Weighted AreaUnderPRC: 0.7388053394268953
Mean absolute error: 0.26895401889951887
Coverage of cases: 96.03260869565217
Instances selection time: 0.5
Test time: 1.1
Accumulative iteration time: 14.0
Weighted Recall: 0.7978260869565218
Weighted FalsePositiveRate: 0.29072669547579155
Kappa statistic: 0.5186625787361927
Training time: 2.1
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 91.84782608695653
Incorrectly Classified Instances: 15.16304347826087
Correctly Classified Instances: 84.83695652173913
Weighted Precision: 0.8512493124829195
Weighted AreaUnderROC: 0.8244865618661258
Root mean squared error: 0.36868702007210746
Relative absolute error: 46.92511082146933
Root relative squared error: 73.7374040144215
Weighted TruePositiveRate: 0.8483695652173913
Weighted MatthewsCorrelation: 0.6734172118239657
Weighted FMeasure: 0.845695295235173
Iteration time: 3.2
Weighted AreaUnderPRC: 0.8031642638059997
Mean absolute error: 0.23462555410734662
Coverage of cases: 96.57608695652175
Instances selection time: 0.5
Test time: 1.4
Accumulative iteration time: 17.2
Weighted Recall: 0.8483695652173913
Weighted FalsePositiveRate: 0.19963731369609314
Kappa statistic: 0.6661976521054963
Training time: 2.7
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 89.80978260869566
Incorrectly Classified Instances: 14.999999999999996
Correctly Classified Instances: 85.0
Weighted Precision: 0.8517044648324832
Weighted AreaUnderROC: 0.8230231391657113
Root mean squared error: 0.36489691910256356
Relative absolute error: 44.63546071708133
Root relative squared error: 72.97938382051271
Weighted TruePositiveRate: 0.85
Weighted MatthewsCorrelation: 0.6746832621398753
Weighted FMeasure: 0.8468928485047023
Iteration time: 3.6
Weighted AreaUnderPRC: 0.8053863976219547
Mean absolute error: 0.22317730358540663
Coverage of cases: 95.81521739130436
Instances selection time: 0.4
Test time: 1.2
Accumulative iteration time: 20.8
Weighted Recall: 0.85
Weighted FalsePositiveRate: 0.20294117647058824
Kappa statistic: 0.6677480650583394
Training time: 3.2
		
Time end:Tue Oct 31 11.23.12 EET 2017