Tue Oct 31 11.22.19 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.19 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 45.108695652173914
Correctly Classified Instances: 54.891304347826086
Weighted Precision: 0.558476266453929
Weighted AreaUnderROC: 0.5266227180527383
Root mean squared error: 0.6716300741641481
Relative absolute error: 90.21739130434783
Root relative squared error: 134.32601483282963
Weighted TruePositiveRate: 0.5489130434782609
Weighted MatthewsCorrelation: 0.052302441063480025
Weighted FMeasure: 0.5529367251106381
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5470692929363
Mean absolute error: 0.45108695652173914
Coverage of cases: 54.891304347826086
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5489130434782609
Weighted FalsePositiveRate: 0.4956676073727842
Kappa statistic: 0.05213505461767635
Training time: 10.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 42.93478260869565
Correctly Classified Instances: 57.06521739130435
Weighted Precision: 0.6063093054677908
Weighted AreaUnderROC: 0.5773326572008114
Root mean squared error: 0.6552463857870233
Relative absolute error: 85.86956521739131
Root relative squared error: 131.04927715740467
Weighted TruePositiveRate: 0.5706521739130435
Weighted MatthewsCorrelation: 0.14931870311466008
Weighted FMeasure: 0.5780598313679599
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5752561271381017
Mean absolute error: 0.42934782608695654
Coverage of cases: 57.06521739130435
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 23.0
Weighted Recall: 0.5706521739130435
Weighted FalsePositiveRate: 0.4159868595114208
Kappa statistic: 0.14373232799245994
Training time: 10.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 41.30434782608695
Correctly Classified Instances: 58.69565217391305
Weighted Precision: 0.6477040869985906
Weighted AreaUnderROC: 0.6146044624746451
Root mean squared error: 0.6426845869171515
Relative absolute error: 82.6086956521739
Root relative squared error: 128.5369173834303
Weighted TruePositiveRate: 0.5869565217391305
Weighted MatthewsCorrelation: 0.2238797128447637
Weighted FMeasure: 0.5916062587263069
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5991069036063151
Mean absolute error: 0.41304347826086957
Coverage of cases: 58.69565217391305
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.5869565217391305
Weighted FalsePositiveRate: 0.3577475967898404
Kappa statistic: 0.20545454545454556
Training time: 11.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.7620987609499557
Weighted AreaUnderROC: 0.7487322515212982
Root mean squared error: 0.5054055625027248
Relative absolute error: 51.08695652173913
Root relative squared error: 101.08111250054496
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.48255388302506
Weighted FMeasure: 0.7484239843639079
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7041820024863533
Mean absolute error: 0.2554347826086957
Coverage of cases: 74.45652173913044
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.247100714348708
Kappa statistic: 0.47575169738118334
Training time: 13.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7965759142807378
Weighted AreaUnderROC: 0.7674949290060853
Root mean squared error: 0.44842720314644063
Relative absolute error: 40.21739130434783
Root relative squared error: 89.68544062928812
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5584569648852683
Weighted FMeasure: 0.7946165682590803
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7313345801243057
Mean absolute error: 0.20108695652173914
Coverage of cases: 79.8913043478261
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.2639231854660905
Kappa statistic: 0.5535152151101784
Training time: 14.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8262598058768781
Weighted AreaUnderROC: 0.8182048681541582
Root mean squared error: 0.4234947769299599
Relative absolute error: 35.869565217391305
Root relative squared error: 84.69895538599198
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.625138700330166
Weighted FMeasure: 0.8222519509476033
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7733523669956008
Mean absolute error: 0.1793478260869565
Coverage of cases: 82.06521739130434
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 80.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.18424243760472708
Kappa statistic: 0.6231380337636544
Training time: 15.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8310482148898761
Weighted AreaUnderROC: 0.8176977687626775
Root mean squared error: 0.4104610345325914
Relative absolute error: 33.69565217391305
Root relative squared error: 82.09220690651829
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.637378308231614
Weighted FMeasure: 0.8312592869039968
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7767163497861379
Mean absolute error: 0.16847826086956522
Coverage of cases: 83.15217391304348
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 112.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.1961262016050798
Kappa statistic: 0.6373346897253307
Training time: 31.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8092397544052313
Weighted AreaUnderROC: 0.7943711967545639
Root mean squared error: 0.4361391879943235
Relative absolute error: 38.04347826086957
Root relative squared error: 87.2278375988647
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5905795816894683
Weighted FMeasure: 0.8094862916658028
Iteration time: 30.0
Weighted AreaUnderPRC: 0.752457765339364
Mean absolute error: 0.19021739130434784
Coverage of cases: 80.97826086956522
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 142.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.22104021518652442
Kappa statistic: 0.5905391658189219
Training time: 29.0
		
Time end:Tue Oct 31 11.22.19 EET 2017