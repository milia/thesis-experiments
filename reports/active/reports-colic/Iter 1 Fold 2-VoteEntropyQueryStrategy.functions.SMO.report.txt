Tue Oct 31 11.23.39 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.39 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 41.84782608695652
Correctly Classified Instances: 58.15217391304348
Weighted Precision: 0.5107957342083675
Weighted AreaUnderROC: 0.48554766734279925
Root mean squared error: 0.6468989572333266
Relative absolute error: 83.69565217391305
Root relative squared error: 129.3797914466653
Weighted TruePositiveRate: 0.5815217391304348
Weighted MatthewsCorrelation: -0.04071766168078152
Weighted FMeasure: 0.5174941561477325
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5276783357705888
Mean absolute error: 0.41847826086956524
Coverage of cases: 58.15217391304348
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5815217391304348
Weighted FalsePositiveRate: 0.6104264044448363
Kappa statistic: -0.03325554259043157
Training time: 7.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.69565217391305
Correctly Classified Instances: 66.30434782608695
Weighted Precision: 0.6706133540372671
Weighted AreaUnderROC: 0.5593306288032455
Root mean squared error: 0.5804795618616821
Relative absolute error: 67.3913043478261
Root relative squared error: 116.09591237233643
Weighted TruePositiveRate: 0.6630434782608695
Weighted MatthewsCorrelation: 0.2032720961160264
Weighted FMeasure: 0.5895955442801738
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5712998751012692
Mean absolute error: 0.33695652173913043
Coverage of cases: 66.30434782608695
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6630434782608695
Weighted FalsePositiveRate: 0.5443822206543787
Kappa statistic: 0.14096385542168655
Training time: 8.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.608695652173914
Correctly Classified Instances: 67.3913043478261
Weighted Precision: 0.6815482502651113
Weighted AreaUnderROC: 0.577079107505071
Root mean squared error: 0.5710402407201608
Relative absolute error: 65.21739130434783
Root relative squared error: 114.20804814403216
Weighted TruePositiveRate: 0.6739130434782609
Weighted MatthewsCorrelation: 0.23906346071213475
Weighted FMeasure: 0.6129305477131565
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5832587947807645
Mean absolute error: 0.32608695652173914
Coverage of cases: 67.3913043478261
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6739130434782609
Weighted FalsePositiveRate: 0.5197548284681188
Kappa statistic: 0.18052256532066513
Training time: 8.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.15217391304348
Correctly Classified Instances: 66.84782608695652
Weighted Precision: 0.7008840384126426
Weighted AreaUnderROC: 0.5605983772819473
Root mean squared error: 0.5757792451369143
Relative absolute error: 66.30434782608695
Root relative squared error: 115.15584902738286
Weighted TruePositiveRate: 0.6684782608695652
Weighted MatthewsCorrelation: 0.22829936838026169
Weighted FMeasure: 0.5876906043042488
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5744071155908287
Mean absolute error: 0.33152173913043476
Coverage of cases: 66.84782608695652
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6684782608695652
Weighted FalsePositiveRate: 0.5472815063056707
Kappa statistic: 0.14555420219244813
Training time: 24.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.608695652173914
Correctly Classified Instances: 67.3913043478261
Weighted Precision: 0.7850790513833992
Weighted AreaUnderROC: 0.5588235294117647
Root mean squared error: 0.5710402407201608
Relative absolute error: 65.21739130434783
Root relative squared error: 114.20804814403216
Weighted TruePositiveRate: 0.6739130434782609
Weighted MatthewsCorrelation: 0.27846024303636535
Weighted FMeasure: 0.5786965925833045
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5795024918370854
Mean absolute error: 0.32608695652173914
Coverage of cases: 67.3913043478261
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 60.0
Weighted Recall: 0.6739130434782609
Weighted FalsePositiveRate: 0.5562659846547314
Kappa statistic: 0.14392059553349867
Training time: 9.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.7573197272331577
Weighted AreaUnderROC: 0.6549188640973631
Root mean squared error: 0.51604684654214
Relative absolute error: 53.2608695652174
Root relative squared error: 103.209369308428
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.4104430465783512
Weighted FMeasure: 0.6993223630129622
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6454643207849091
Mean absolute error: 0.266304347826087
Coverage of cases: 73.3695652173913
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 78.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.42385792397918687
Kappa statistic: 0.35155350978135796
Training time: 18.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8285751905845494
Weighted AreaUnderROC: 0.7756085192697768
Root mean squared error: 0.4234947769299599
Relative absolute error: 35.869565217391305
Root relative squared error: 84.69895538599198
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6100941657362244
Weighted FMeasure: 0.8117202268431002
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7491738853002043
Mean absolute error: 0.1793478260869565
Coverage of cases: 82.06521739130434
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 96.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.2694351353734897
Kappa statistic: 0.5888407367280606
Training time: 18.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8356474988312295
Weighted AreaUnderROC: 0.8128803245436107
Root mean squared error: 0.4037864265436241
Relative absolute error: 32.608695652173914
Root relative squared error: 80.75728530872482
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6443253706933759
Weighted FMeasure: 0.834578804347826
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7765460800455313
Mean absolute error: 0.16304347826086957
Coverage of cases: 83.69565217391305
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 119.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.21119587265190937
Kappa statistic: 0.6413721413721415
Training time: 22.0
		
Time end:Tue Oct 31 11.23.40 EET 2017