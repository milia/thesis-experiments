Tue Oct 31 11.23.19 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.19 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 58.69565217391305
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7704718519014762
Weighted AreaUnderROC: 0.7792188464591234
Root mean squared error: 0.4533562147168706
Relative absolute error: 47.37048623763961
Root relative squared error: 90.67124294337412
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5072825850950902
Weighted FMeasure: 0.7710111649112299
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7551708990901735
Mean absolute error: 0.23685243118819807
Coverage of cases: 84.78260869565217
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.2676823353029368
Kappa statistic: 0.5071428571428571
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 60.05434782608695
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8032982738214867
Weighted AreaUnderROC: 0.8148095070112003
Root mean squared error: 0.43056807197115415
Relative absolute error: 44.18107814548422
Root relative squared error: 86.11361439423084
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5777101472109074
Weighted FMeasure: 0.8037238556381969
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8218487907181009
Mean absolute error: 0.2209053907274211
Coverage of cases: 85.32608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.23031131493076992
Kappa statistic: 0.5775510204081633
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 61.95652173913044
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7705868465249048
Weighted AreaUnderROC: 0.8124200767263428
Root mean squared error: 0.44036900061554163
Relative absolute error: 46.981764482715064
Root relative squared error: 88.07380012310833
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.5068308785071335
Weighted FMeasure: 0.7678741470955402
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8160806190451337
Mean absolute error: 0.23490882241357533
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.25261266425610723
Kappa statistic: 0.5059940059940059
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 60.59782608695652
Incorrectly Classified Instances: 23.91304347826087
Correctly Classified Instances: 76.08695652173913
Weighted Precision: 0.7662536053840402
Weighted AreaUnderROC: 0.8011233353911279
Root mean squared error: 0.45384343021103524
Relative absolute error: 49.36756818179137
Root relative squared error: 90.76868604220705
Weighted TruePositiveRate: 0.7608695652173914
Weighted MatthewsCorrelation: 0.4971914782691508
Weighted FMeasure: 0.7627473188496117
Iteration time: 0.0
Weighted AreaUnderPRC: 0.8069599137433972
Mean absolute error: 0.24683784090895686
Coverage of cases: 84.23913043478261
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7608695652173914
Weighted FalsePositiveRate: 0.255798571302584
Kappa statistic: 0.49601593625498014
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7791973896092889
Weighted AreaUnderROC: 0.8136409736308317
Root mean squared error: 0.4489407888098969
Relative absolute error: 49.03102091474312
Root relative squared error: 89.78815776197938
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5239604148860362
Weighted FMeasure: 0.7740036231884059
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8283163256102313
Mean absolute error: 0.2451551045737156
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.23725637181409295
Kappa statistic: 0.5217821782178218
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 63.31521739130435
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7920711062462106
Weighted AreaUnderROC: 0.8195993914807304
Root mean squared error: 0.4317289702725988
Relative absolute error: 45.980927551814474
Root relative squared error: 86.34579405451976
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.552860928677574
Weighted FMeasure: 0.7894672496913039
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8343497212950118
Mean absolute error: 0.22990463775907238
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.22769865067466266
Kappa statistic: 0.5519480519480519
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7877043050956094
Weighted AreaUnderROC: 0.8258113590263692
Root mean squared error: 0.43211151938474585
Relative absolute error: 45.86084509563299
Root relative squared error: 86.42230387694917
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5431167955389921
Weighted FMeasure: 0.784315744408738
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8410581489962305
Mean absolute error: 0.22930422547816495
Coverage of cases: 88.04347826086956
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.23088455772113942
Kappa statistic: 0.5418326693227092
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 63.858695652173914
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7877043050956094
Weighted AreaUnderROC: 0.8294878296146045
Root mean squared error: 0.42692193325833566
Relative absolute error: 45.04724190531345
Root relative squared error: 85.38438665166713
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5431167955389921
Weighted FMeasure: 0.784315744408738
Iteration time: 1.0
Weighted AreaUnderPRC: 0.844925633954012
Mean absolute error: 0.22523620952656725
Coverage of cases: 89.1304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.23088455772113942
Kappa statistic: 0.5418326693227092
Training time: 0.0
		
Time end:Tue Oct 31 11.23.19 EET 2017