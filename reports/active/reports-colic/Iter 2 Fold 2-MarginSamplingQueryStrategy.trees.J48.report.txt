Tue Oct 31 11.23.10 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.10 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 52.44565217391305
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8043478260869565
Weighted AreaUnderROC: 0.7639452332657201
Root mean squared error: 0.4372962170819613
Relative absolute error: 44.40836940836942
Root relative squared error: 87.45924341639227
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5801217038539555
Weighted FMeasure: 0.8043478260869567
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7340999464692994
Mean absolute error: 0.2220418470418471
Coverage of cases: 80.97826086956522
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.22422612223300115
Kappa statistic: 0.5801217038539555
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8436161411894475
Weighted AreaUnderROC: 0.7894269776876268
Root mean squared error: 0.40067368198485104
Relative absolute error: 39.85075914423745
Root relative squared error: 80.13473639697021
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6560620296549327
Weighted FMeasure: 0.8382438447277107
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7751710697722745
Mean absolute error: 0.19925379572118726
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 3.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.22018035100097014
Kappa statistic: 0.6478352692713832
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7871450304259635
Root mean squared error: 0.41914365610663185
Relative absolute error: 40.23842034331108
Root relative squared error: 83.82873122132636
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7742355662065594
Mean absolute error: 0.20119210171655538
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7871450304259635
Root mean squared error: 0.419622267967848
Relative absolute error: 39.71387402861678
Root relative squared error: 83.92445359356961
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7742355662065594
Mean absolute error: 0.1985693701430839
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 54.891304347826086
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7875253549695739
Root mean squared error: 0.41992179548271624
Relative absolute error: 39.40566439130671
Root relative squared error: 83.98435909654324
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7745082698383396
Mean absolute error: 0.19702832195653355
Coverage of cases: 85.8695652173913
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 82.06521739130434
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7871450304259635
Root mean squared error: 0.4112744987089232
Relative absolute error: 47.208055740572426
Root relative squared error: 82.25489974178464
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7742355662065594
Mean absolute error: 0.23604027870286212
Coverage of cases: 92.93478260869566
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 97.01086956521739
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7871450304259635
Root mean squared error: 0.40393755283638755
Relative absolute error: 55.48798312671309
Root relative squared error: 80.78751056727751
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7742355662065594
Mean absolute error: 0.27743991563356546
Coverage of cases: 97.28260869565217
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 2.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.01086956521739
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7967278658978264
Weighted AreaUnderROC: 0.7871450304259635
Root mean squared error: 0.40483400333285907
Relative absolute error: 52.51874900938105
Root relative squared error: 80.96680066657181
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5627795800501374
Weighted FMeasure: 0.7971966765675044
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7742355662065594
Mean absolute error: 0.26259374504690525
Coverage of cases: 97.28260869565217
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.24566760737278423
Kappa statistic: 0.5617919670442842
Training time: 3.0
		
Time end:Tue Oct 31 11.23.10 EET 2017