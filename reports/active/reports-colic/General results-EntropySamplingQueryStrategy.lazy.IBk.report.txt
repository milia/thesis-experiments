Tue Oct 31 11.21.57 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.57 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.989130434782613
Correctly Classified Instances: 72.01086956521739
Weighted Precision: 0.7204460610338199
Weighted AreaUnderROC: 0.6911574543610548
Root mean squared error: 0.5142424121128422
Relative absolute error: 58.29241140453961
Root relative squared error: 102.84848242256842
Weighted TruePositiveRate: 0.7201086956521741
Weighted MatthewsCorrelation: 0.39321259231226763
Weighted FMeasure: 0.7149220443612683
Iteration time: 17.4
Weighted AreaUnderPRC: 0.6613868421329903
Mean absolute error: 0.2914620570226981
Coverage of cases: 72.01086956521739
Instances selection time: 17.1
Test time: 12.7
Accumulative iteration time: 17.4
Weighted Recall: 0.7201086956521741
Weighted FalsePositiveRate: 0.33871924331951675
Kappa statistic: 0.3866178307635845
Training time: 0.3
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.55434782608696
Correctly Classified Instances: 72.44565217391306
Weighted Precision: 0.7276783759139468
Weighted AreaUnderROC: 0.6863843813387425
Root mean squared error: 0.5144361667832926
Relative absolute error: 56.65022751782006
Root relative squared error: 102.88723335665853
Weighted TruePositiveRate: 0.7244565217391304
Weighted MatthewsCorrelation: 0.39901347331981396
Weighted FMeasure: 0.7128368392910787
Iteration time: 11.5
Weighted AreaUnderPRC: 0.6604714576615434
Mean absolute error: 0.28325113758910037
Coverage of cases: 72.44565217391306
Instances selection time: 11.1
Test time: 12.5
Accumulative iteration time: 28.9
Weighted Recall: 0.7244565217391304
Weighted FalsePositiveRate: 0.35260053796631097
Kappa statistic: 0.3838595307710406
Training time: 0.4
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.554347826086957
Correctly Classified Instances: 72.44565217391305
Weighted Precision: 0.7351017114234211
Weighted AreaUnderROC: 0.6820867139959432
Root mean squared error: 0.5161276016542058
Relative absolute error: 56.25356527530441
Root relative squared error: 103.22552033084114
Weighted TruePositiveRate: 0.7244565217391303
Weighted MatthewsCorrelation: 0.4026529792934962
Weighted FMeasure: 0.7074459169487801
Iteration time: 9.1
Weighted AreaUnderPRC: 0.6599588676004418
Mean absolute error: 0.28126782637652215
Coverage of cases: 72.44565217391305
Instances selection time: 9.0
Test time: 15.7
Accumulative iteration time: 38.0
Weighted Recall: 0.7244565217391303
Weighted FalsePositiveRate: 0.3635538848222947
Kappa statistic: 0.37710012979805413
Training time: 0.1
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.90217391304348
Correctly Classified Instances: 73.09782608695652
Weighted Precision: 0.7460645900341959
Weighted AreaUnderROC: 0.6835889959432049
Root mean squared error: 0.5117551235425077
Relative absolute error: 54.74327427071232
Root relative squared error: 102.35102470850154
Weighted TruePositiveRate: 0.7309782608695652
Weighted MatthewsCorrelation: 0.41746329307855384
Weighted FMeasure: 0.7110770839998718
Iteration time: 9.6
Weighted AreaUnderPRC: 0.6630749519924659
Mean absolute error: 0.27371637135356164
Coverage of cases: 73.09782608695652
Instances selection time: 9.2
Test time: 16.2
Accumulative iteration time: 47.6
Weighted Recall: 0.7309782608695652
Weighted FalsePositiveRate: 0.3645989505247376
Kappa statistic: 0.38481767886122525
Training time: 0.4
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.97826086956522
Correctly Classified Instances: 74.02173913043478
Weighted Precision: 0.756710126074642
Weighted AreaUnderROC: 0.6825747971602436
Root mean squared error: 0.5039214056755952
Relative absolute error: 52.768991427797644
Root relative squared error: 100.78428113511902
Weighted TruePositiveRate: 0.7402173913043478
Weighted MatthewsCorrelation: 0.43179836864999854
Weighted FMeasure: 0.7159804170156121
Iteration time: 7.9
Weighted AreaUnderPRC: 0.6656778515880355
Mean absolute error: 0.26384495713898826
Coverage of cases: 74.02173913043478
Instances selection time: 7.6
Test time: 17.6
Accumulative iteration time: 55.5
Weighted Recall: 0.7402173913043478
Weighted FalsePositiveRate: 0.3713532939412647
Kappa statistic: 0.39257084569936823
Training time: 0.3
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.771739130434778
Correctly Classified Instances: 77.22826086956522
Weighted Precision: 0.7816974681923209
Weighted AreaUnderROC: 0.723294878296146
Root mean squared error: 0.47305936938313875
Relative absolute error: 46.3287937116811
Root relative squared error: 94.61187387662773
Weighted TruePositiveRate: 0.7722826086956521
Weighted MatthewsCorrelation: 0.5036330451944907
Weighted FMeasure: 0.758922419137187
Iteration time: 6.9
Weighted AreaUnderPRC: 0.6991671738663187
Mean absolute error: 0.23164396855840552
Coverage of cases: 77.22826086956522
Instances selection time: 6.7
Test time: 21.1
Accumulative iteration time: 62.4
Weighted Recall: 0.7722826086956521
Weighted FalsePositiveRate: 0.3203049210688773
Kappa statistic: 0.47843642343193366
Training time: 0.2
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.84782608695652
Correctly Classified Instances: 78.15217391304348
Weighted Precision: 0.7810618892092969
Weighted AreaUnderROC: 0.7505704868154158
Root mean squared error: 0.463954913612081
Relative absolute error: 44.40289691484177
Root relative squared error: 92.7909827224162
Weighted TruePositiveRate: 0.7815217391304348
Weighted MatthewsCorrelation: 0.5227586410432875
Weighted FMeasure: 0.7763180352218518
Iteration time: 4.2
Weighted AreaUnderPRC: 0.7158780306226903
Mean absolute error: 0.22201448457420883
Coverage of cases: 78.15217391304348
Instances selection time: 4.0
Test time: 25.7
Accumulative iteration time: 66.6
Weighted Recall: 0.7815217391304348
Weighted FalsePositiveRate: 0.2802032807125849
Kappa statistic: 0.5155609805528549
Training time: 0.2
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.847826086956523
Correctly Classified Instances: 78.15217391304347
Weighted Precision: 0.7818608148144631
Weighted AreaUnderROC: 0.7599771805273834
Root mean squared error: 0.46435598708200887
Relative absolute error: 44.32403489742462
Root relative squared error: 92.87119741640177
Weighted TruePositiveRate: 0.7815217391304347
Weighted MatthewsCorrelation: 0.5286759860473107
Weighted FMeasure: 0.7795723562743568
Iteration time: 2.3
Weighted AreaUnderPRC: 0.7213131993216936
Mean absolute error: 0.22162017448712307
Coverage of cases: 78.15217391304347
Instances selection time: 2.1
Test time: 29.2
Accumulative iteration time: 68.9
Weighted Recall: 0.7815217391304347
Weighted FalsePositiveRate: 0.26133918334950174
Kappa statistic: 0.5255278158275253
Training time: 0.2
		
Time end:Tue Oct 31 11.22.02 EET 2017