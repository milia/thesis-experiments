Tue Oct 31 11.21.45 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.45 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 60.32608695652174
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7739947499261165
Weighted AreaUnderROC: 0.7583878097715848
Root mean squared error: 0.45979511187090744
Relative absolute error: 49.694604894732684
Root relative squared error: 91.95902237418149
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5131876776411164
Weighted FMeasure: 0.7743913875503622
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7712663821796183
Mean absolute error: 0.24847302447366343
Coverage of cases: 82.6086956521739
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.2766668136519975
Kappa statistic: 0.5113989637305699
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 65.76086956521739
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7851786821503283
Weighted AreaUnderROC: 0.7678890334244642
Root mean squared error: 0.4352866907134335
Relative absolute error: 48.497968637903675
Root relative squared error: 87.0573381426867
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5371052391117257
Weighted FMeasure: 0.7853966857186372
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7643167063423195
Mean absolute error: 0.24248984318951838
Coverage of cases: 88.58695652173913
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.26420980686127526
Kappa statistic: 0.5352331606217615
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 67.1195652173913
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8019167835437121
Weighted AreaUnderROC: 0.7734809066055207
Root mean squared error: 0.42301974308231555
Relative absolute error: 47.062426862632535
Root relative squared error: 84.60394861646311
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5722695350728849
Weighted FMeasure: 0.8014945652173914
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7835315132835007
Mean absolute error: 0.23531213431316267
Coverage of cases: 89.1304347826087
Instances selection time: 2.0
Test time: 12.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24856689302407622
Kappa statistic: 0.5696465696465697
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 64.67391304347827
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7900415601023019
Root mean squared error: 0.421812720582615
Relative absolute error: 45.404497482786375
Root relative squared error: 84.362544116523
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7978482740251667
Mean absolute error: 0.22702248741393188
Coverage of cases: 88.58695652173913
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 62.5
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7768900476232473
Root mean squared error: 0.42897233986207023
Relative absolute error: 45.1353801075117
Root relative squared error: 85.79446797241404
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7866615143915743
Mean absolute error: 0.2256769005375585
Coverage of cases: 86.41304347826087
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7909433701975029
Weighted AreaUnderROC: 0.7808669194814356
Root mean squared error: 0.4351912620573136
Relative absolute error: 45.99263865522192
Root relative squared error: 87.03825241146272
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5499526473943857
Weighted FMeasure: 0.7913155897810629
Iteration time: 9.0
Weighted AreaUnderPRC: 0.790491298080264
Mean absolute error: 0.2299631932761096
Coverage of cases: 86.95652173913044
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.25493870711702976
Kappa statistic: 0.5485537190082644
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7659014825442373
Weighted AreaUnderROC: 0.8195993914807304
Root mean squared error: 0.4545530373949183
Relative absolute error: 50.839775495297005
Root relative squared error: 90.91060747898366
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.49147345893553096
Weighted FMeasure: 0.7536391437308868
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8280945210472059
Mean absolute error: 0.254198877476485
Coverage of cases: 86.95652173913044
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 28.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.24391480730223125
Kappa statistic: 0.48540856031128404
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 62.5
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.7590716507981176
Weighted AreaUnderROC: 0.8222616632860039
Root mean squared error: 0.45626372916660424
Relative absolute error: 51.131667127087255
Root relative squared error: 91.25274583332084
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.4777923540708875
Weighted FMeasure: 0.748126947834348
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8325132363043358
Mean absolute error: 0.2556583356354363
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.2531859070464768
Kappa statistic: 0.47268292682926827
Training time: 2.0
		
Time end:Tue Oct 31 11.21.45 EET 2017