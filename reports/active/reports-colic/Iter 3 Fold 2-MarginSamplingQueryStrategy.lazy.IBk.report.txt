Tue Oct 31 11.23.07 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.07 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.6748544531153227
Weighted AreaUnderROC: 0.6404665314401623
Root mean squared error: 0.5471010172390033
Relative absolute error: 64.9885583524028
Root relative squared error: 109.42020344780066
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.29779523820065634
Weighted FMeasure: 0.6756659654978587
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6201418846503913
Mean absolute error: 0.324942791762014
Coverage of cases: 68.47826086956522
Instances selection time: 11.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.4038495458153276
Kappa statistic: 0.29343220338983067
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.32608695652174
Correctly Classified Instances: 64.67391304347827
Weighted Precision: 0.6904975347377856
Weighted AreaUnderROC: 0.6650608519269777
Root mean squared error: 0.5842745291644923
Relative absolute error: 71.66416791604195
Root relative squared error: 116.85490583289845
Weighted TruePositiveRate: 0.6467391304347826
Weighted MatthewsCorrelation: 0.3191626302563672
Weighted FMeasure: 0.6525503482025222
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6346736005596484
Mean absolute error: 0.3583208395802097
Coverage of cases: 64.67391304347827
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6467391304347826
Weighted FalsePositiveRate: 0.3166174265808272
Kappa statistic: 0.30335507921714816
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.58695652173913
Correctly Classified Instances: 61.41304347826087
Weighted Precision: 0.6840446749102513
Weighted AreaUnderROC: 0.6483265720081136
Root mean squared error: 0.6133024044878203
Relative absolute error: 77.75919732441471
Root relative squared error: 122.66048089756406
Weighted TruePositiveRate: 0.6141304347826086
Weighted MatthewsCorrelation: 0.29139931449653156
Weighted FMeasure: 0.6174062227915237
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6229264580912575
Mean absolute error: 0.38879598662207354
Coverage of cases: 61.41304347826087
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6141304347826086
Weighted FalsePositiveRate: 0.31747729076638154
Kappa statistic: 0.26375112714156895
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.32608695652174
Correctly Classified Instances: 64.67391304347827
Weighted Precision: 0.6991763869943831
Weighted AreaUnderROC: 0.6678498985801217
Root mean squared error: 0.588396168500768
Relative absolute error: 71.25659766371805
Root relative squared error: 117.6792337001536
Weighted TruePositiveRate: 0.6467391304347826
Weighted MatthewsCorrelation: 0.3320324883199954
Weighted FMeasure: 0.6519386228802876
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6389349307838014
Mean absolute error: 0.3562829883185903
Coverage of cases: 64.67391304347827
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6467391304347826
Weighted FalsePositiveRate: 0.3044470411852897
Kappa statistic: 0.3110599078341013
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.7329751317523057
Weighted AreaUnderROC: 0.7155172413793104
Root mean squared error: 0.5371925832522191
Relative absolute error: 59.395725865880536
Root relative squared error: 107.43851665044382
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.4165033640279559
Weighted FMeasure: 0.7114911339685746
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6748691875035803
Mean absolute error: 0.29697862932940267
Coverage of cases: 70.65217391304348
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.2754872563718141
Kappa statistic: 0.4063097514340344
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7643577470883444
Weighted AreaUnderROC: 0.7512677484787018
Root mean squared error: 0.49099216279666524
Relative absolute error: 49.65343415248893
Root relative squared error: 98.19843255933304
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.49171441868211396
Weighted FMeasure: 0.7580895650829294
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7074428418162407
Mean absolute error: 0.24826717076244464
Coverage of cases: 75.54347826086956
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.252899285651292
Kappa statistic: 0.4891411648568608
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7727030979922881
Weighted AreaUnderROC: 0.7598884381338742
Root mean squared error: 0.48039310678302677
Relative absolute error: 47.41331865712718
Root relative squared error: 96.07862135660535
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.5105714484768289
Weighted FMeasure: 0.7683889057802102
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7158633362815998
Mean absolute error: 0.2370665932856359
Coverage of cases: 76.6304347826087
Instances selection time: 3.0
Test time: 19.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.2465274715583385
Kappa statistic: 0.5089374379344588
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7749956866804694
Weighted AreaUnderROC: 0.7611561866125761
Root mean squared error: 0.47510809545323884
Relative absolute error: 46.26282364435756
Root relative squared error: 95.02161909064777
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5165810914259279
Weighted FMeasure: 0.7729977116704805
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7178922589041917
Mean absolute error: 0.2313141182217878
Coverage of cases: 77.17391304347827
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.2494267572096305
Kappa statistic: 0.5160320641282564
Training time: 0.0
		
Time end:Tue Oct 31 11.23.07 EET 2017