Tue Oct 31 11.22.26 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.26 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.69565217391305
Correctly Classified Instances: 66.30434782608695
Weighted Precision: 0.6527804736024845
Weighted AreaUnderROC: 0.6201825557809331
Root mean squared error: 0.5656098379086149
Relative absolute error: 69.1075514874142
Root relative squared error: 113.12196758172297
Weighted TruePositiveRate: 0.6630434782608695
Weighted MatthewsCorrelation: 0.2521482066665974
Weighted FMeasure: 0.655024946543122
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6048413554044019
Mean absolute error: 0.345537757437071
Coverage of cases: 66.30434782608695
Instances selection time: 8.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6630434782608695
Weighted FalsePositiveRate: 0.4226783666990034
Kappa statistic: 0.24947368421052613
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.608695652173914
Correctly Classified Instances: 67.3913043478261
Weighted Precision: 0.6595410628019324
Weighted AreaUnderROC: 0.6039553752535497
Root mean squared error: 0.5614530330663541
Relative absolute error: 66.41679160419798
Root relative squared error: 112.29060661327081
Weighted TruePositiveRate: 0.6739130434782609
Weighted MatthewsCorrelation: 0.2516118872540159
Weighted FMeasure: 0.6492010405053883
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5974793047217432
Mean absolute error: 0.3320839580209899
Coverage of cases: 67.3913043478261
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 20.0
Weighted Recall: 0.6739130434782609
Weighted FalsePositiveRate: 0.4589029014904313
Kappa statistic: 0.23503325942350325
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.15217391304348
Correctly Classified Instances: 66.84782608695652
Weighted Precision: 0.6528973516551156
Weighted AreaUnderROC: 0.600025354969574
Root mean squared error: 0.5684935207509418
Relative absolute error: 67.16146172667914
Root relative squared error: 113.69870415018836
Weighted TruePositiveRate: 0.6684782608695652
Weighted MatthewsCorrelation: 0.23389222478796806
Weighted FMeasure: 0.6390776909209708
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5942281629797401
Mean absolute error: 0.3358073086333957
Coverage of cases: 66.84782608695652
Instances selection time: 11.0
Test time: 22.0
Accumulative iteration time: 32.0
Weighted Recall: 0.6684782608695652
Weighted FalsePositiveRate: 0.47425919393244553
Kappa statistic: 0.2144456886898095
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.6710804939086433
Weighted AreaUnderROC: 0.6021171399594321
Root mean squared error: 0.5605939480481731
Relative absolute error: 64.85148967700036
Root relative squared error: 112.11878960963462
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.2570055656512476
Weighted FMeasure: 0.6414802624597356
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5975653156357773
Mean absolute error: 0.3242574483850018
Coverage of cases: 67.93478260869566
Instances selection time: 11.0
Test time: 12.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.4800577652350296
Kappa statistic: 0.22457142857142864
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.608695652173914
Correctly Classified Instances: 67.3913043478261
Weighted Precision: 0.6685788069937767
Weighted AreaUnderROC: 0.5891227180527383
Root mean squared error: 0.566282947801138
Relative absolute error: 65.79779427973983
Root relative squared error: 113.2565895602276
Weighted TruePositiveRate: 0.6739130434782609
Weighted MatthewsCorrelation: 0.23891325208245728
Weighted FMeasure: 0.6260558957980243
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5894040963815257
Mean absolute error: 0.32898897139869915
Coverage of cases: 67.3913043478261
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 55.0
Weighted Recall: 0.6739130434782609
Weighted FalsePositiveRate: 0.5014992503748126
Kappa statistic: 0.19767441860465115
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7469362745098039
Weighted AreaUnderROC: 0.6963742393509128
Root mean squared error: 0.4965324528467555
Relative absolute error: 50.70899967344158
Root relative squared error: 99.3064905693511
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4425954754423892
Weighted FMeasure: 0.7383689107827038
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6719454351309
Mean absolute error: 0.2535449983672079
Coverage of cases: 75.0
Instances selection time: 8.0
Test time: 16.0
Accumulative iteration time: 64.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3473630831643002
Kappa statistic: 0.42872570194384446
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7628171007395624
Weighted AreaUnderROC: 0.7198909736308317
Root mean squared error: 0.480498122800178
Relative absolute error: 47.399647350395554
Root relative squared error: 96.09962456003561
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.4826745680730509
Weighted FMeasure: 0.7589151019565
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6913858975177104
Mean absolute error: 0.23699823675197776
Coverage of cases: 76.6304347826087
Instances selection time: 6.0
Test time: 18.0
Accumulative iteration time: 70.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.3134645912337949
Kappa statistic: 0.47449521785334736
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7814139925414798
Weighted AreaUnderROC: 0.7593813387423936
Root mean squared error: 0.46372401465717883
Relative absolute error: 44.10119259547524
Root relative squared error: 92.74480293143577
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.530758439133696
Weighted FMeasure: 0.7819153951535521
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7207455055424322
Mean absolute error: 0.2205059629773762
Coverage of cases: 78.26086956521739
Instances selection time: 2.0
Test time: 20.0
Accumulative iteration time: 73.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2552253285122145
Kappa statistic: 0.5306122448979592
Training time: 1.0
		
Time end:Tue Oct 31 11.22.26 EET 2017