Tue Oct 31 11.22.00 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.00 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7191463901076985
Weighted AreaUnderROC: 0.7015720081135902
Root mean squared error: 0.5230447552266734
Relative absolute error: 59.8398169336385
Root relative squared error: 104.60895104533468
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.3960041966234916
Weighted FMeasure: 0.7145258606128173
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6644905135186695
Mean absolute error: 0.2991990846681925
Coverage of cases: 71.19565217391305
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.30881250551194994
Kappa statistic: 0.39473684210526333
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7068911565808458
Weighted AreaUnderROC: 0.6802738336713996
Root mean squared error: 0.5276440179593241
Relative absolute error: 59.0704647676162
Root relative squared error: 105.52880359186483
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.3696823088174607
Weighted FMeasure: 0.7083595985407122
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6498704393478905
Mean absolute error: 0.295352323838081
Coverage of cases: 71.19565217391305
Instances selection time: 16.0
Test time: 12.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.3514088543963312
Kappa statistic: 0.36839378238341974
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.7391580108465853
Weighted AreaUnderROC: 0.7061359026369168
Root mean squared error: 0.4990486332485012
Relative absolute error: 52.34113712374583
Root relative squared error: 99.80972664970024
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.434701247032975
Weighted FMeasure: 0.7378434724897381
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6741067253623854
Mean absolute error: 0.26170568561872914
Coverage of cases: 74.45652173913044
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.33229341211747065
Kappa statistic: 0.4292502639915522
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7701622323166776
Weighted AreaUnderROC: 0.7276876267748478
Root mean squared error: 0.4729763289573965
Relative absolute error: 46.76131322094056
Root relative squared error: 94.5952657914793
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.49410070741422735
Weighted FMeasure: 0.7625467943994105
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6970032164320178
Mean absolute error: 0.2338065661047028
Coverage of cases: 77.17391304347827
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.3163638768850868
Kappa statistic: 0.4817596566523605
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7666666666666666
Weighted AreaUnderROC: 0.6800202839756594
Root mean squared error: 0.4958170330952565
Relative absolute error: 50.847457627118686
Root relative squared error: 99.1634066190513
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.44776464690284373
Weighted FMeasure: 0.7243107769423559
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6646234108009933
Mean absolute error: 0.25423728813559343
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 43.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.38995943204868155
Kappa statistic: 0.4015837104072398
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7770026786727044
Weighted AreaUnderROC: 0.6843306288032455
Root mean squared error: 0.4909921627966648
Relative absolute error: 49.65343415248897
Root relative squared error: 98.19843255933296
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.4638371266037136
Weighted FMeasure: 0.7290891766265049
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6700455698714002
Mean absolute error: 0.24826717076244484
Coverage of cases: 75.54347826086956
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.38677352500220474
Kappa statistic: 0.4125993189557322
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7928701733049559
Weighted AreaUnderROC: 0.7540567951318459
Root mean squared error: 0.4516055774500567
Relative absolute error: 42.04733076499723
Root relative squared error: 90.32111549001134
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.544697804118317
Weighted FMeasure: 0.7863721365123889
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7215363794995175
Mean absolute error: 0.21023665382498613
Coverage of cases: 79.34782608695652
Instances selection time: 3.0
Test time: 19.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.2853646706058735
Kappa statistic: 0.5341151385927505
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7855838465545241
Weighted AreaUnderROC: 0.7527890466531441
Root mean squared error: 0.45782807849509566
Relative absolute error: 43.03859306301902
Root relative squared error: 91.56561569901913
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5330838170871784
Weighted FMeasure: 0.7824658601510595
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7180292672626827
Mean absolute error: 0.2151929653150951
Coverage of cases: 78.80434782608695
Instances selection time: 2.0
Test time: 20.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.2824653849545815
Kappa statistic: 0.5263991552270326
Training time: 0.0
		
Time end:Tue Oct 31 11.22.00 EET 2017