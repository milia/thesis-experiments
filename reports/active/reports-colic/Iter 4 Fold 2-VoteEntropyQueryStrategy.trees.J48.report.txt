Tue Oct 31 11.23.50 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.50 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 79.6195652173913
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8080628803245437
Root mean squared error: 0.38941518907116945
Relative absolute error: 46.89613526570046
Root relative squared error: 77.88303781423389
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7872292242651734
Mean absolute error: 0.23448067632850228
Coverage of cases: 91.30434782608695
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 1.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 79.6195652173913
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8079361054766735
Root mean squared error: 0.3746262477992039
Relative absolute error: 39.936518085495145
Root relative squared error: 74.92524955984078
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7871377851008282
Mean absolute error: 0.19968259042747571
Coverage of cases: 91.30434782608695
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 96.73913043478261
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8080628803245437
Root mean squared error: 0.36771055946050635
Relative absolute error: 40.51828463298708
Root relative squared error: 73.54211189210127
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7872292242651734
Mean absolute error: 0.20259142316493542
Coverage of cases: 97.28260869565217
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 70.1086956521739
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8080628803245437
Root mean squared error: 0.3690097499923435
Relative absolute error: 39.03266966276471
Root relative squared error: 73.8019499984687
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7872292242651734
Mean absolute error: 0.19516334831382356
Coverage of cases: 91.84782608695652
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 70.1086956521739
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8080628803245437
Root mean squared error: 0.36982091612340073
Relative absolute error: 38.26885174525961
Root relative squared error: 73.96418322468014
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7872292242651734
Mean absolute error: 0.19134425872629804
Coverage of cases: 91.84782608695652
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 67.66304347826087
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8089503042596349
Root mean squared error: 0.36643963063081486
Relative absolute error: 38.961596867007756
Root relative squared error: 73.28792612616297
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7852895544465106
Mean absolute error: 0.1948079843350388
Coverage of cases: 91.30434782608695
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 3.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 100.0
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.8521094627813994
Weighted AreaUnderROC: 0.8547794117647058
Root mean squared error: 0.3629815794604013
Relative absolute error: 39.20677044363213
Root relative squared error: 72.59631589208027
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6814223879433835
Weighted FMeasure: 0.8520083856033139
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8226306883752319
Mean absolute error: 0.19603385221816066
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.18338257341917275
Kappa statistic: 0.680226570545829
Training time: 2.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.55434782608695
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.7995689655172413
Root mean squared error: 0.3800403941642933
Relative absolute error: 45.424930078820395
Root relative squared error: 76.00807883285866
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7829265479357793
Mean absolute error: 0.22712465039410198
Coverage of cases: 98.3695652173913
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 3.0
		
Time end:Tue Oct 31 11.23.50 EET 2017