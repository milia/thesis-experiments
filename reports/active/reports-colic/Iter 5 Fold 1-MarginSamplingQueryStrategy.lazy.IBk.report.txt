Tue Oct 31 11.23.08 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.08 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.891304347826086
Correctly Classified Instances: 70.1086956521739
Weighted Precision: 0.7163386124269759
Weighted AreaUnderROC: 0.6990365111561865
Root mean squared error: 0.5327976152420911
Relative absolute error: 61.89931350114421
Root relative squared error: 106.55952304841823
Weighted TruePositiveRate: 0.7010869565217391
Weighted MatthewsCorrelation: 0.3870660453515445
Weighted FMeasure: 0.7052549389550881
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6616783179904505
Mean absolute error: 0.3094965675057211
Coverage of cases: 70.1086956521739
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7010869565217391
Weighted FalsePositiveRate: 0.3030139342093659
Kappa statistic: 0.3829268292682927
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.743814372951297
Weighted AreaUnderROC: 0.7274340770791075
Root mean squared error: 0.4969120214403091
Relative absolute error: 52.75520134669509
Root relative squared error: 99.38240428806182
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.4501834020630314
Weighted FMeasure: 0.7441673059512207
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6885646124370978
Mean absolute error: 0.26377600673347545
Coverage of cases: 74.45652173913044
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.29578225593085816
Kappa statistic: 0.4501525940996948
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.751626885477693
Weighted AreaUnderROC: 0.7302231237322515
Root mean squared error: 0.48832095149627286
Relative absolute error: 50.20921107877637
Root relative squared error: 97.66419029925457
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46535255469989784
Weighted FMeasure: 0.7523807912138121
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6930939052050437
Mean absolute error: 0.25104605539388186
Coverage of cases: 75.54347826086956
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.3015808272334421
Kappa statistic: 0.46373056994818657
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7507852994348245
Weighted AreaUnderROC: 0.7243914807302232
Root mean squared error: 0.48956855579233505
Relative absolute error: 49.94465737886371
Root relative squared error: 97.91371115846701
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46105783546452
Weighted FMeasure: 0.7502093397745572
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6894743393088545
Mean absolute error: 0.24972328689431855
Coverage of cases: 75.54347826086956
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.31375121262897965
Kappa statistic: 0.45697796432319
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7943543153796236
Weighted AreaUnderROC: 0.7527890466531441
Root mean squared error: 0.45065829411041186
Relative absolute error: 42.294623005750495
Root relative squared error: 90.13165882208237
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5447212698664644
Weighted FMeasure: 0.7851613854089905
Iteration time: 6.0
Weighted AreaUnderPRC: 0.721567504909368
Mean absolute error: 0.21147311502875246
Coverage of cases: 79.34782608695652
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.2914498633036423
Kappa statistic: 0.5311158798283261
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7928701733049559
Weighted AreaUnderROC: 0.7557682555780935
Root mean squared error: 0.4511994763232604
Relative absolute error: 42.15109994986636
Root relative squared error: 90.23989526465208
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.544697804118317
Weighted FMeasure: 0.7863721365123889
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7229443766203117
Mean absolute error: 0.21075549974933178
Coverage of cases: 79.34782608695652
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.2853646706058735
Kappa statistic: 0.5341151385927505
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7917874396135266
Weighted AreaUnderROC: 0.7595081135902637
Root mean squared error: 0.4516052174387175
Relative absolute error: 42.04049511163145
Root relative squared error: 90.32104348774351
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5450620424827898
Weighted FMeasure: 0.7875052877399765
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7258513502811436
Mean absolute error: 0.21020247555815727
Coverage of cases: 79.34782608695652
Instances selection time: 3.0
Test time: 19.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.27927947790810476
Kappa statistic: 0.5370762711864406
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7681860682561945
Weighted AreaUnderROC: 0.7444852941176471
Root mean squared error: 0.47510796057633636
Relative absolute error: 46.259787645693415
Root relative squared error: 95.02159211526727
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.500213699452394
Weighted FMeasure: 0.7684103260869565
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7068783203327405
Mean absolute error: 0.23129893822846706
Coverage of cases: 77.17391304347827
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.2859379133962431
Kappa statistic: 0.4979209979209979
Training time: 0.0
		
Time end:Tue Oct 31 11.23.08 EET 2017