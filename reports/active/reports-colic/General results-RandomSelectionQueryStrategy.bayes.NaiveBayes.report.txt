Tue Oct 31 11.23.16 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.16 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 27.77173913043478
Correctly Classified Instances: 72.22826086956522
Weighted Precision: 0.7355227538010376
Weighted AreaUnderROC: 0.7571771099744244
Root mean squared error: 0.4892831228914053
Relative absolute error: 56.980374655281025
Root relative squared error: 97.85662457828104
Weighted TruePositiveRate: 0.7222826086956522
Weighted MatthewsCorrelation: 0.42707534738820135
Weighted FMeasure: 0.7243456788033474
Iteration time: 3.3
Weighted AreaUnderPRC: 0.7422683619202212
Mean absolute error: 0.28490187327640515
Coverage of cases: 83.26086956521739
Instances selection time: 3.0
Test time: 3.6
Accumulative iteration time: 3.3
Weighted Recall: 0.7222826086956522
Weighted FalsePositiveRate: 0.288154819648999
Kappa statistic: 0.42196695066643697
Training time: 0.3
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 65.35326086956522
Incorrectly Classified Instances: 22.77173913043478
Correctly Classified Instances: 77.22826086956522
Weighted Precision: 0.7798151828952778
Weighted AreaUnderROC: 0.8112515433459742
Root mean squared error: 0.43926923221821096
Relative absolute error: 49.06524967759212
Root relative squared error: 87.8538464436422
Weighted TruePositiveRate: 0.7722826086956522
Weighted MatthewsCorrelation: 0.5242311212732028
Weighted FMeasure: 0.7735988812418239
Iteration time: 2.2
Weighted AreaUnderPRC: 0.8082858310527922
Mean absolute error: 0.24532624838796066
Coverage of cases: 88.20652173913042
Instances selection time: 1.9
Test time: 3.7
Accumulative iteration time: 5.5
Weighted Recall: 0.7722826086956522
Weighted FalsePositiveRate: 0.24302297380721405
Kappa statistic: 0.5210771304042547
Training time: 0.3
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 64.61956521739131
Incorrectly Classified Instances: 21.793478260869563
Correctly Classified Instances: 78.20652173913044
Weighted Precision: 0.7874464374813648
Weighted AreaUnderROC: 0.8209357637357793
Root mean squared error: 0.42829247710622936
Relative absolute error: 46.598137024817156
Root relative squared error: 85.65849542124587
Weighted TruePositiveRate: 0.7820652173913043
Weighted MatthewsCorrelation: 0.5417881845674415
Weighted FMeasure: 0.7833845340368838
Iteration time: 1.4
Weighted AreaUnderPRC: 0.820043048871779
Mean absolute error: 0.2329906851240858
Coverage of cases: 88.96739130434784
Instances selection time: 1.4
Test time: 2.8
Accumulative iteration time: 6.9
Weighted Recall: 0.7820652173913043
Weighted FalsePositiveRate: 0.23424574477467147
Kappa statistic: 0.539743424923578
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 65.02717391304347
Incorrectly Classified Instances: 21.30434782608696
Correctly Classified Instances: 78.69565217391305
Weighted Precision: 0.7921557156292411
Weighted AreaUnderROC: 0.8342759227004146
Root mean squared error: 0.4189581512567627
Relative absolute error: 45.09869457214678
Root relative squared error: 83.79163025135256
Weighted TruePositiveRate: 0.7869565217391304
Weighted MatthewsCorrelation: 0.5523742313203923
Weighted FMeasure: 0.7885225037784082
Iteration time: 1.6
Weighted AreaUnderPRC: 0.8342452676340211
Mean absolute error: 0.2254934728607339
Coverage of cases: 89.40217391304347
Instances selection time: 1.4
Test time: 3.4
Accumulative iteration time: 8.5
Weighted Recall: 0.7869565217391304
Weighted FalsePositiveRate: 0.2265102742746274
Kappa statistic: 0.5508005691547696
Training time: 0.2
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 64.97282608695652
Incorrectly Classified Instances: 20.923913043478258
Correctly Classified Instances: 79.07608695652173
Weighted Precision: 0.7959296146572925
Weighted AreaUnderROC: 0.8359533468559837
Root mean squared error: 0.4193521623323354
Relative absolute error: 45.07841734375968
Root relative squared error: 83.87043246646708
Weighted TruePositiveRate: 0.7907608695652174
Weighted MatthewsCorrelation: 0.5604918264188699
Weighted FMeasure: 0.7923338170878874
Iteration time: 0.9
Weighted AreaUnderPRC: 0.8368725417582954
Mean absolute error: 0.22539208671879835
Coverage of cases: 89.34782608695653
Instances selection time: 0.4
Test time: 2.5
Accumulative iteration time: 9.4
Weighted Recall: 0.7907608695652174
Weighted FalsePositiveRate: 0.22184606226298614
Kappa statistic: 0.5589348020775122
Training time: 0.5
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 64.02173913043478
Incorrectly Classified Instances: 20.760869565217387
Correctly Classified Instances: 79.23913043478262
Weighted Precision: 0.7965475247716817
Weighted AreaUnderROC: 0.8369041582150102
Root mean squared error: 0.4166876023486649
Relative absolute error: 44.03156900264766
Root relative squared error: 83.337520469733
Weighted TruePositiveRate: 0.792391304347826
Weighted MatthewsCorrelation: 0.5622291245877358
Weighted FMeasure: 0.7937358390359017
Iteration time: 1.2
Weighted AreaUnderPRC: 0.8393626033336681
Mean absolute error: 0.2201578450132383
Coverage of cases: 89.29347826086958
Instances selection time: 0.7
Test time: 2.6
Accumulative iteration time: 10.6
Weighted Recall: 0.792391304347826
Weighted FalsePositiveRate: 0.22271584795837368
Kappa statistic: 0.5610801741183382
Training time: 0.5
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 63.96739130434783
Incorrectly Classified Instances: 21.358695652173914
Correctly Classified Instances: 78.6413043478261
Weighted Precision: 0.7917944325293885
Weighted AreaUnderROC: 0.8349264705882353
Root mean squared error: 0.4206693200573241
Relative absolute error: 44.55380089358797
Root relative squared error: 84.13386401146481
Weighted TruePositiveRate: 0.7864130434782608
Weighted MatthewsCorrelation: 0.5515824595476365
Weighted FMeasure: 0.7880687904127094
Iteration time: 1.2
Weighted AreaUnderPRC: 0.8367995159134323
Mean absolute error: 0.22276900446793985
Coverage of cases: 88.96739130434783
Instances selection time: 0.7
Test time: 2.7
Accumulative iteration time: 11.8
Weighted Recall: 0.7864130434782608
Weighted FalsePositiveRate: 0.22622034570949817
Kappa statistic: 0.549990090472727
Training time: 0.5
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 64.34782608695653
Incorrectly Classified Instances: 21.358695652173914
Correctly Classified Instances: 78.6413043478261
Weighted Precision: 0.7919778674917135
Weighted AreaUnderROC: 0.8386536511156187
Root mean squared error: 0.4199022751240003
Relative absolute error: 44.75405644690031
Root relative squared error: 83.98045502480005
Weighted TruePositiveRate: 0.7864130434782608
Weighted MatthewsCorrelation: 0.5519016540665251
Weighted FMeasure: 0.788110902508577
Iteration time: 1.4
Weighted AreaUnderPRC: 0.8418014496664025
Mean absolute error: 0.22377028223450152
Coverage of cases: 89.18478260869566
Instances selection time: 1.0
Test time: 2.4
Accumulative iteration time: 13.2
Weighted Recall: 0.7864130434782608
Weighted FalsePositiveRate: 0.22561182643972127
Kappa statistic: 0.5502359257635238
Training time: 0.4
		
Time end:Tue Oct 31 11.23.20 EET 2017