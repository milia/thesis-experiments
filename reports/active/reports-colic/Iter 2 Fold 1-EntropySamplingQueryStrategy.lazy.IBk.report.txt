Tue Oct 31 11.21.58 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.58 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.759844716664252
Weighted AreaUnderROC: 0.745182555780933
Root mean squared error: 0.48206419138950407
Relative absolute error: 51.60183066361562
Root relative squared error: 96.41283827790082
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.4838158534219132
Weighted FMeasure: 0.7570775957976583
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7028120725666496
Mean absolute error: 0.2580091533180781
Coverage of cases: 75.54347826086956
Instances selection time: 11.0
Test time: 13.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.26506967104682955
Kappa statistic: 0.4830169830169831
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7986767485822306
Weighted AreaUnderROC: 0.7449290060851927
Root mean squared error: 0.4468753349767676
Relative absolute error: 43.32833583208397
Root relative squared error: 89.37506699535352
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5460535158669609
Weighted FMeasure: 0.7824945794819126
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7176275992438563
Mean absolute error: 0.21664167916041985
Coverage of cases: 79.34782608695652
Instances selection time: 10.0
Test time: 12.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.3036202486991798
Kappa statistic: 0.525
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.8055881968925448
Weighted AreaUnderROC: 0.718052738336714
Root mean squared error: 0.46041452397582794
Relative absolute error: 44.927536231884076
Root relative squared error: 92.08290479516559
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5306313240437446
Weighted FMeasure: 0.762339110165197
Iteration time: 6.0
Weighted AreaUnderPRC: 0.701235261726755
Mean absolute error: 0.22463768115942037
Coverage of cases: 78.26086956521739
Instances selection time: 6.0
Test time: 11.0
Accumulative iteration time: 30.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.3465032189787459
Kappa statistic: 0.48314606741573046
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.8035969679633869
Weighted AreaUnderROC: 0.7003042596348884
Root mean squared error: 0.47297632895739666
Relative absolute error: 46.76131322094058
Root relative squared error: 94.59526579147933
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5101605221958811
Weighted FMeasure: 0.7459831278390656
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6880009607253009
Mean absolute error: 0.23380656610470288
Coverage of cases: 77.17391304347827
Instances selection time: 10.0
Test time: 26.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.37113061116500573
Kappa statistic: 0.44988610478359903
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.8035969679633869
Weighted AreaUnderROC: 0.7003042596348884
Root mean squared error: 0.47377617047555043
Relative absolute error: 46.573323507737676
Root relative squared error: 94.75523409511008
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5101605221958811
Weighted FMeasure: 0.7459831278390656
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6880009607253009
Mean absolute error: 0.23286661753868837
Coverage of cases: 77.17391304347827
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.37113061116500573
Kappa statistic: 0.44988610478359903
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8283282655716121
Weighted AreaUnderROC: 0.7444219066937119
Root mean squared error: 0.4391686978116455
Relative absolute error: 40.012602394454944
Root relative squared error: 87.8337395623291
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5828883134399147
Weighted FMeasure: 0.7878626580415681
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7270799061771267
Mean absolute error: 0.20006301197227472
Coverage of cases: 80.43478260869566
Instances selection time: 9.0
Test time: 24.0
Accumulative iteration time: 65.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.3155040126995326
Kappa statistic: 0.5379464285714287
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7981733243993012
Weighted AreaUnderROC: 0.7614097363083164
Root mean squared error: 0.4456249652924186
Relative absolute error: 40.97413318657126
Root relative squared error: 89.12499305848371
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5572648148749334
Weighted FMeasure: 0.792554855171872
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7281891438712258
Mean absolute error: 0.2048706659328563
Coverage of cases: 79.8913043478261
Instances selection time: 4.0
Test time: 18.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.276093570861628
Kappa statistic: 0.5478214665249734
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.7963065585851142
Weighted AreaUnderROC: 0.7705375253549696
Root mean squared error: 0.44593622210735795
Relative absolute error: 40.88910600879332
Root relative squared error: 89.18724442147159
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5595771183495103
Weighted FMeasure: 0.7955426508002573
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7330441350805806
Mean absolute error: 0.2044455300439666
Coverage of cases: 79.8913043478261
Instances selection time: 3.0
Test time: 33.0
Accumulative iteration time: 72.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.25783799276832176
Kappa statistic: 0.5563086548488009
Training time: 0.0
		
Time end:Tue Oct 31 11.21.59 EET 2017