Tue Oct 31 11.21.47 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.47 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 64.1304347826087
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.708043581436619
Weighted AreaUnderROC: 0.7045677528882617
Root mean squared error: 0.519270754570208
Relative absolute error: 63.99033005746859
Root relative squared error: 103.8541509140416
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.3659764198558469
Weighted FMeasure: 0.6899475981150098
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6913005672094341
Mean absolute error: 0.31995165028734296
Coverage of cases: 79.8913043478261
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.3064864626510274
Kappa statistic: 0.35865384615384627
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 67.1195652173913
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.775422275933785
Weighted AreaUnderROC: 0.8024847870182555
Root mean squared error: 0.42953001888355297
Relative absolute error: 49.318895495192194
Root relative squared error: 85.90600377671059
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5177230928185285
Weighted FMeasure: 0.7760831310468587
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7818447496897797
Mean absolute error: 0.24659447747596097
Coverage of cases: 88.58695652173913
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.26449642825646
Kappa statistic: 0.5174002047082907
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 68.75
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7739947499261165
Weighted AreaUnderROC: 0.7936105476673427
Root mean squared error: 0.42256457329701047
Relative absolute error: 48.09401586471768
Root relative squared error: 84.5129146594021
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5131876776411164
Weighted FMeasure: 0.7743913875503622
Iteration time: 5.0
Weighted AreaUnderPRC: 0.802127188372005
Mean absolute error: 0.2404700793235884
Coverage of cases: 89.67391304347827
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.2766668136519975
Kappa statistic: 0.5113989637305699
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 67.93478260869566
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.802087051712239
Weighted AreaUnderROC: 0.7912018255578094
Root mean squared error: 0.40652799589528466
Relative absolute error: 45.91335464203877
Root relative squared error: 81.30559917905693
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5710926268842421
Weighted FMeasure: 0.8006262512191364
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7973454951995925
Mean absolute error: 0.22956677321019384
Coverage of cases: 89.67391304347827
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.25465208572184495
Kappa statistic: 0.5669456066945607
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 67.93478260869566
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779336522215083
Weighted AreaUnderROC: 0.7877789046653144
Root mean squared error: 0.41317078939319785
Relative absolute error: 47.081423969539145
Root relative squared error: 82.63415787863957
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5226235109494171
Weighted FMeasure: 0.7784736124657072
Iteration time: 2.0
Weighted AreaUnderPRC: 0.791571261569278
Mean absolute error: 0.23540711984769572
Coverage of cases: 89.67391304347827
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 18.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2795660993032896
Kappa statistic: 0.5188284518828452
Training time: 1.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 66.30434782608695
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.7794296400187003
Weighted AreaUnderROC: 0.7990618661257607
Root mean squared error: 0.4166722830676136
Relative absolute error: 45.529673412887526
Root relative squared error: 83.33445661352272
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.524232311325891
Weighted FMeasure: 0.7794384057971016
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8049616388919429
Mean absolute error: 0.2276483670644376
Coverage of cases: 89.67391304347827
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.27348090660552077
Kappa statistic: 0.5218295218295219
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 64.94565217391305
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.7681860682561945
Weighted AreaUnderROC: 0.7862576064908723
Root mean squared error: 0.4294478174860465
Relative absolute error: 46.71502477182734
Root relative squared error: 85.8895634972093
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.500213699452394
Weighted FMeasure: 0.7684103260869565
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7899112289234513
Mean absolute error: 0.23357512385913667
Coverage of cases: 89.1304347826087
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.2859379133962431
Kappa statistic: 0.4979209979209979
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 63.31521739130435
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.775422275933785
Weighted AreaUnderROC: 0.782707910750507
Root mean squared error: 0.44800703370472417
Relative absolute error: 48.8373509634868
Root relative squared error: 89.60140674094484
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5177230928185285
Weighted FMeasure: 0.7760831310468587
Iteration time: 2.0
Weighted AreaUnderPRC: 0.786962612288432
Mean absolute error: 0.244186754817434
Coverage of cases: 85.8695652173913
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.26449642825646
Kappa statistic: 0.5174002047082907
Training time: 1.0
		
Time end:Tue Oct 31 11.21.47 EET 2017