Tue Oct 31 11.22.37 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.37 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 79.8913043478261
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7111016822243299
Weighted AreaUnderROC: 0.7524087221095336
Root mean squared error: 0.466804949269623
Relative absolute error: 63.44800646623706
Root relative squared error: 93.36098985392461
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.37998531224981297
Weighted FMeasure: 0.7115078130939301
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7710290098890983
Mean absolute error: 0.3172400323311853
Coverage of cases: 93.47826086956522
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.33315327630302494
Kappa statistic: 0.37995930824008156
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 76.6304347826087
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7338386043706985
Weighted AreaUnderROC: 0.783341784989858
Root mean squared error: 0.45954763407985816
Relative absolute error: 59.21025440339975
Root relative squared error: 91.90952681597163
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.4235142030907381
Weighted FMeasure: 0.721505119000133
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7984013956912699
Mean absolute error: 0.29605127201699877
Coverage of cases: 91.84782608695652
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 5.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.2812858276743981
Kappa statistic: 0.41828793774319056
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 80.16304347826087
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7851786821503283
Weighted AreaUnderROC: 0.8084432048681541
Root mean squared error: 0.4043558574602999
Relative absolute error: 52.32840655174612
Root relative squared error: 80.87117149205997
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5371052391117257
Weighted FMeasure: 0.7853966857186372
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8154606660339501
Mean absolute error: 0.2616420327587306
Coverage of cases: 95.1086956521739
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.26420980686127526
Kappa statistic: 0.5352331606217615
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 71.73913043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8020715977283702
Weighted AreaUnderROC: 0.8212474645030426
Root mean squared error: 0.39643266181183406
Relative absolute error: 47.05900259184966
Root relative squared error: 79.28653236236681
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.573773571857796
Weighted FMeasure: 0.802298979792586
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8328738711318764
Mean absolute error: 0.23529501295924832
Coverage of cases: 92.93478260869566
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.24248170032630748
Kappa statistic: 0.5723140495867769
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 71.46739130434783
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8080235679922161
Weighted AreaUnderROC: 0.8170638945233265
Root mean squared error: 0.3957519726275598
Relative absolute error: 46.76475056388556
Root relative squared error: 79.15039452551196
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5828067472404553
Weighted FMeasure: 0.8057183753802112
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8255252588513623
Mean absolute error: 0.23382375281942783
Coverage of cases: 92.3913043478261
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2514661786753682
Kappa statistic: 0.5776495278069256
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 69.02173913043478
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.79636261437454
Weighted AreaUnderROC: 0.8093306288032455
Root mean squared error: 0.4081785349589317
Relative absolute error: 46.640178329711866
Root relative squared error: 81.63570699178634
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5610228005823349
Weighted FMeasure: 0.7964019838869123
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8155907108044456
Mean absolute error: 0.2332008916485593
Coverage of cases: 91.30434782608695
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.251752800070553
Kappa statistic: 0.5590673575129533
Training time: 1.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 64.94565217391305
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7856515465211117
Weighted AreaUnderROC: 0.8059077079107505
Root mean squared error: 0.4211458567217266
Relative absolute error: 46.27354188802572
Root relative squared error: 84.22917134434532
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5390510184714881
Weighted FMeasure: 0.7862343347603423
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8152542800317973
Mean absolute error: 0.2313677094401286
Coverage of cases: 88.58695652173913
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.25812461416350646
Kappa statistic: 0.5381050463439752
Training time: 1.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 64.40217391304348
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.8010529510266678
Weighted AreaUnderROC: 0.83329107505071
Root mean squared error: 0.4107210243397078
Relative absolute error: 43.96176758968149
Root relative squared error: 82.14420486794157
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5727061783961418
Weighted FMeasure: 0.799765883135527
Iteration time: 1.0
Weighted AreaUnderPRC: 0.834939009751241
Mean absolute error: 0.21980883794840747
Coverage of cases: 89.67391304347827
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.22132683658170915
Kappa statistic: 0.5723618090452262
Training time: 1.0
		
Time end:Tue Oct 31 11.22.37 EET 2017