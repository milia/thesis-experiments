Tue Oct 31 11.23.40 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.40 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7344553529686769
Weighted AreaUnderROC: 0.7079107505070994
Root mean squared error: 0.5107539184552492
Relative absolute error: 52.17391304347826
Root relative squared error: 102.15078369104984
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.42815786383190296
Weighted FMeasure: 0.7353260869565218
Iteration time: 20.0
Weighted AreaUnderPRC: 0.6736183100595564
Mean absolute error: 0.2608695652173913
Coverage of cases: 73.91304347826087
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.3233089337684099
Kappa statistic: 0.4261954261954261
Training time: 19.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8091550048071788
Weighted AreaUnderROC: 0.7991886409736308
Root mean squared error: 0.4423258684646914
Relative absolute error: 39.130434782608695
Root relative squared error: 88.46517369293828
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5890421128088332
Weighted FMeasure: 0.8058841699678642
Iteration time: 29.0
Weighted AreaUnderPRC: 0.75428001467699
Mean absolute error: 0.1956521739130435
Coverage of cases: 80.43478260869566
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.20597054413969484
Kappa statistic: 0.5876494023904383
Training time: 22.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7833363330162241
Weighted AreaUnderROC: 0.769523326572008
Root mean squared error: 0.49453535504684026
Relative absolute error: 48.91304347826087
Root relative squared error: 98.90707100936805
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.5206585432391779
Weighted FMeasure: 0.7596207480828064
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7227764943730793
Mean absolute error: 0.24456521739130435
Coverage of cases: 75.54347826086956
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 74.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.21638812946467945
Kappa statistic: 0.5066730219256436
Training time: 24.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.7839288020873698
Weighted AreaUnderROC: 0.7614097363083164
Root mean squared error: 0.51604684654214
Relative absolute error: 53.2608695652174
Root relative squared error: 103.209369308428
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.5061836877422102
Weighted FMeasure: 0.7378766153611013
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7155328021682409
Mean absolute error: 0.266304347826087
Coverage of cases: 73.3695652173913
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.2108761795572802
Kappa statistic: 0.47775718257645977
Training time: 21.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.8090331828059739
Weighted AreaUnderROC: 0.795892494929006
Root mean squared error: 0.4720445815349666
Relative absolute error: 44.565217391304344
Root relative squared error: 94.40891630699332
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5713276279829781
Weighted FMeasure: 0.7810294996944969
Iteration time: 36.0
Weighted AreaUnderPRC: 0.7479549727971889
Mean absolute error: 0.22282608695652173
Coverage of cases: 77.71739130434783
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 134.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.1853889231854661
Kappa statistic: 0.5530805687203793
Training time: 32.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8112975675904738
Weighted AreaUnderROC: 0.8022312373225153
Root mean squared error: 0.4423258684646914
Relative absolute error: 39.130434782608695
Root relative squared error: 88.46517369293828
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5925624426415135
Weighted FMeasure: 0.8062888198757764
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7566669246104128
Mean absolute error: 0.1956521739130435
Coverage of cases: 80.43478260869566
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 179.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.1998853514419261
Kappa statistic: 0.5900990099009902
Training time: 41.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8272965021248774
Weighted AreaUnderROC: 0.8164300202839757
Root mean squared error: 0.41702882811414954
Relative absolute error: 34.78260869565217
Root relative squared error: 83.4057656228299
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6292013996469619
Weighted FMeasure: 0.8265910522999371
Iteration time: 50.0
Weighted AreaUnderPRC: 0.7737126369799736
Mean absolute error: 0.17391304347826086
Coverage of cases: 82.6086956521739
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 229.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.1932269159537878
Kappa statistic: 0.6290322580645161
Training time: 49.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8299568240279703
Weighted AreaUnderROC: 0.8116125760649089
Root mean squared error: 0.4104610345325914
Relative absolute error: 33.69565217391305
Root relative squared error: 82.09220690651829
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6339652647860852
Weighted FMeasure: 0.8300837019889901
Iteration time: 58.0
Weighted AreaUnderPRC: 0.7728698463038556
Mean absolute error: 0.16847826086956522
Coverage of cases: 83.15217391304348
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 287.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.20829658700061734
Kappa statistic: 0.6328527291452112
Training time: 57.0
		
Time end:Tue Oct 31 11.23.41 EET 2017