Tue Oct 31 11.23.05 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.05 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7080267558528428
Weighted AreaUnderROC: 0.6494041582150103
Root mean squared error: 0.523042106090272
Relative absolute error: 59.825901416290485
Root relative squared error: 104.6084212180544
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.34676782897844444
Weighted FMeasure: 0.6889250079010338
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6324367382074629
Mean absolute error: 0.2991295070814524
Coverage of cases: 71.19565217391305
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.4183459740717876
Kappa statistic: 0.3220244716351503
Training time: 0.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.347826086956523
Correctly Classified Instances: 70.65217391304348
Weighted Precision: 0.7094179061784898
Weighted AreaUnderROC: 0.6331769776876268
Root mean squared error: 0.5325921409647849
Relative absolute error: 60.11073410663098
Root relative squared error: 106.51842819295697
Weighted TruePositiveRate: 0.7065217391304348
Weighted MatthewsCorrelation: 0.3319272258337758
Weighted FMeasure: 0.6734068786502271
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6225266063669548
Mean absolute error: 0.3005536705331549
Coverage of cases: 70.65217391304348
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7065217391304348
Weighted FalsePositiveRate: 0.4458726519093395
Kappa statistic: 0.29271070615034167
Training time: 0.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7291455121696052
Weighted AreaUnderROC: 0.6285496957403651
Root mean squared error: 0.5299265171682138
Relative absolute error: 58.69565217391305
Root relative squared error: 105.98530343364277
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.350713748666735
Weighted FMeasure: 0.6714294096445125
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6225846246774147
Mean absolute error: 0.29347826086956524
Coverage of cases: 71.19565217391305
Instances selection time: 6.0
Test time: 10.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.4548571302584002
Kappa statistic: 0.2937427578215528
Training time: 0.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7413953685280047
Weighted AreaUnderROC: 0.6288032454361054
Root mean squared error: 0.5263072646373376
Relative absolute error: 57.409050576752485
Root relative squared error: 105.26145292746752
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.3682074826211989
Weighted FMeasure: 0.675915109691621
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6260642637972684
Mean absolute error: 0.2870452528837624
Coverage of cases: 71.73913043478261
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 26.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.45167122321192343
Kappa statistic: 0.3046511627906976
Training time: 0.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.804347826086957
Correctly Classified Instances: 71.19565217391305
Weighted Precision: 0.7438563327032136
Weighted AreaUnderROC: 0.6184710953346856
Root mean squared error: 0.5322389365237153
Relative absolute error: 58.32719233603545
Root relative squared error: 106.44778730474307
Weighted TruePositiveRate: 0.7119565217391305
Weighted MatthewsCorrelation: 0.35747593856530263
Weighted FMeasure: 0.6641340138436356
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6194275950892209
Mean absolute error: 0.29163596168017725
Coverage of cases: 71.19565217391305
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7119565217391305
Weighted FalsePositiveRate: 0.46702751565393774
Kappa statistic: 0.28378378378378394
Training time: 0.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.26086956521739
Correctly Classified Instances: 71.73913043478261
Weighted Precision: 0.7413953685280047
Weighted AreaUnderROC: 0.6288032454361054
Root mean squared error: 0.5278296093160305
Relative absolute error: 57.15185885318208
Root relative squared error: 105.5659218632061
Weighted TruePositiveRate: 0.717391304347826
Weighted MatthewsCorrelation: 0.3682074826211989
Weighted FMeasure: 0.675915109691621
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6260642637972684
Mean absolute error: 0.2857592942659104
Coverage of cases: 71.73913043478261
Instances selection time: 4.0
Test time: 17.0
Accumulative iteration time: 36.0
Weighted Recall: 0.717391304347826
Weighted FalsePositiveRate: 0.45167122321192343
Kappa statistic: 0.3046511627906976
Training time: 0.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.630434782608695
Correctly Classified Instances: 73.3695652173913
Weighted Precision: 0.7314322799016126
Weighted AreaUnderROC: 0.6785623732251522
Root mean squared error: 0.5128092255550438
Relative absolute error: 53.849086301000035
Root relative squared error: 102.56184511100876
Weighted TruePositiveRate: 0.7336956521739131
Weighted MatthewsCorrelation: 0.40199213931687516
Weighted FMeasure: 0.7166589699156102
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6556213026416404
Mean absolute error: 0.2692454315050002
Coverage of cases: 73.3695652173913
Instances selection time: 4.0
Test time: 18.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7336956521739131
Weighted FalsePositiveRate: 0.38126157509480546
Kappa statistic: 0.38144895718990124
Training time: 0.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.08695652173913
Correctly Classified Instances: 73.91304347826087
Weighted Precision: 0.7333209463644245
Weighted AreaUnderROC: 0.7007479716024341
Root mean squared error: 0.5079073537727042
Relative absolute error: 52.70824880837049
Root relative squared error: 101.58147075454085
Weighted TruePositiveRate: 0.7391304347826086
Weighted MatthewsCorrelation: 0.421428640341723
Weighted FMeasure: 0.7315856266189177
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6696692286414619
Mean absolute error: 0.2635412440418525
Coverage of cases: 73.91304347826087
Instances selection time: 1.0
Test time: 21.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7391304347826086
Weighted FalsePositiveRate: 0.3415645118617162
Kappa statistic: 0.41525423728813554
Training time: 1.0
		
Time end:Tue Oct 31 11.23.05 EET 2017