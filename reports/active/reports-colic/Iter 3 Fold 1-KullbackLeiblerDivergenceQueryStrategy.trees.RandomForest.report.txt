Tue Oct 31 11.22.32 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.32 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 91.57608695652173
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8257878997009432
Weighted AreaUnderROC: 0.8549929998236176
Root mean squared error: 0.3864289802122661
Relative absolute error: 60.481011372564595
Root relative squared error: 77.28579604245323
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6085682748905133
Weighted FMeasure: 0.8128778087205248
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8560489027234645
Mean absolute error: 0.302405056862823
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.263349942675721
Kappa statistic: 0.5914962325080731
Training time: 3.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 96.73913043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8043478260869565
Weighted AreaUnderROC: 0.8705628803245437
Root mean squared error: 0.3762663931010245
Relative absolute error: 62.03387670185446
Root relative squared error: 75.2532786202049
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5801217038539555
Weighted FMeasure: 0.8043478260869567
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8792496650711199
Mean absolute error: 0.3101693835092723
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.22422612223300115
Kappa statistic: 0.5801217038539555
Training time: 5.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 96.73913043478261
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.769047619047619
Weighted AreaUnderROC: 0.842292089249493
Root mean squared error: 0.4131462599904081
Relative absolute error: 72.23677525506152
Root relative squared error: 82.62925199808163
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4963159944121107
Weighted FMeasure: 0.7538986354775828
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8407464732882367
Mean absolute error: 0.36118387627530757
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.23782961460446247
Kappa statistic: 0.488394584139265
Training time: 7.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 89.94565217391305
Incorrectly Classified Instances: 25.543478260869566
Correctly Classified Instances: 74.45652173913044
Weighted Precision: 0.7950786638766177
Weighted AreaUnderROC: 0.8727814401622718
Root mean squared error: 0.4416159297261819
Relative absolute error: 72.1331101360656
Root relative squared error: 88.32318594523639
Weighted TruePositiveRate: 0.7445652173913043
Weighted MatthewsCorrelation: 0.5287680229858007
Weighted FMeasure: 0.7485755290198319
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8830301776399282
Mean absolute error: 0.36066555068032796
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7445652173913043
Weighted FalsePositiveRate: 0.19841917276655788
Kappa statistic: 0.4990732159406858
Training time: 8.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 95.65217391304348
Incorrectly Classified Instances: 22.82608695652174
Correctly Classified Instances: 77.17391304347827
Weighted Precision: 0.8151144598155468
Weighted AreaUnderROC: 0.8856490872210954
Root mean squared error: 0.4109967627331854
Relative absolute error: 68.36888373186073
Root relative squared error: 82.19935254663709
Weighted TruePositiveRate: 0.7717391304347826
Weighted MatthewsCorrelation: 0.5752646463162591
Weighted FMeasure: 0.7755598527852285
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8812312933546324
Mean absolute error: 0.3418444186593037
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7717391304347826
Weighted FalsePositiveRate: 0.17640444483640533
Kappa statistic: 0.5485981308411214
Training time: 12.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 97.28260869565217
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8198803546629634
Weighted AreaUnderROC: 0.8935091277890467
Root mean squared error: 0.3732656608177358
Relative absolute error: 62.281637023055794
Root relative squared error: 74.65313216354717
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.6120047714437539
Weighted FMeasure: 0.8166683827474273
Iteration time: 15.0
Weighted AreaUnderPRC: 0.8981087453610065
Mean absolute error: 0.31140818511527896
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.1935135373489726
Kappa statistic: 0.6105577689243027
Training time: 14.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8369565217391305
Weighted AreaUnderROC: 0.8573782961460445
Root mean squared error: 0.3752662217788264
Relative absolute error: 62.243209521823964
Root relative squared error: 75.05324435576529
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6501014198782962
Weighted FMeasure: 0.8369565217391305
Iteration time: 45.0
Weighted AreaUnderPRC: 0.8624449276419774
Mean absolute error: 0.3112160476091198
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 100.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.1868551018608343
Kappa statistic: 0.6501014198782963
Training time: 41.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 98.6413043478261
Incorrectly Classified Instances: 14.673913043478262
Correctly Classified Instances: 85.32608695652173
Weighted Precision: 0.852306006014446
Weighted AreaUnderROC: 0.8727180527383368
Root mean squared error: 0.36660329557418037
Relative absolute error: 61.211110137862704
Root relative squared error: 73.32065911483608
Weighted TruePositiveRate: 0.8532608695652174
Weighted MatthewsCorrelation: 0.6826161055658738
Weighted FMeasure: 0.852542549713785
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8718947246464377
Mean absolute error: 0.3060555506893135
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 128.0
Weighted Recall: 0.8532608695652174
Weighted FalsePositiveRate: 0.177297380721404
Kappa statistic: 0.6821903787103378
Training time: 28.0
		
Time end:Tue Oct 31 11.22.33 EET 2017