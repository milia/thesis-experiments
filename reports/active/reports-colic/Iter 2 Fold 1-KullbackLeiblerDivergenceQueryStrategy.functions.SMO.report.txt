Tue Oct 31 11.22.17 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.17 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7524225883909679
Weighted AreaUnderROC: 0.7299695740365112
Root mean squared error: 0.49453535504684026
Relative absolute error: 48.91304347826087
Root relative squared error: 98.90707100936805
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46786533373554035
Weighted FMeasure: 0.7533473093388565
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6921865309367459
Mean absolute error: 0.24456521739130435
Coverage of cases: 75.54347826086956
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.29549563453567335
Kappa statistic: 0.46704428424304845
Training time: 13.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.52173913043478
Correctly Classified Instances: 68.47826086956522
Weighted Precision: 0.7038706256627784
Weighted AreaUnderROC: 0.5887423935091277
Root mean squared error: 0.5614422421802155
Relative absolute error: 63.04347826086957
Root relative squared error: 112.28844843604311
Weighted TruePositiveRate: 0.6847826086956522
Weighted MatthewsCorrelation: 0.27523753700410253
Weighted FMeasure: 0.6258328627893845
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5930281479090784
Mean absolute error: 0.31521739130434784
Coverage of cases: 68.47826086956522
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6847826086956522
Weighted FalsePositiveRate: 0.5072978216773966
Kappa statistic: 0.2078384798099764
Training time: 8.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.434782608695652
Correctly Classified Instances: 69.56521739130434
Weighted Precision: 0.7175376957985654
Weighted AreaUnderROC: 0.6034482758620691
Root mean squared error: 0.5516772843673705
Relative absolute error: 60.86956521739131
Root relative squared error: 110.3354568734741
Weighted TruePositiveRate: 0.6956521739130435
Weighted MatthewsCorrelation: 0.30779946155510407
Weighted FMeasure: 0.6430542522503737
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6043039086517348
Mean absolute error: 0.30434782608695654
Coverage of cases: 69.56521739130434
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6956521739130435
Weighted FalsePositiveRate: 0.48875562218890556
Kappa statistic: 0.24056603773584911
Training time: 14.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.97826086956522
Correctly Classified Instances: 69.02173913043478
Weighted Precision: 0.6982718074924802
Weighted AreaUnderROC: 0.6021805273833671
Root mean squared error: 0.5565811788909613
Relative absolute error: 61.95652173913043
Root relative squared error: 111.31623577819225
Weighted TruePositiveRate: 0.6902173913043478
Weighted MatthewsCorrelation: 0.28788101153254303
Weighted FMeasure: 0.6428203493561138
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6009487760221611
Mean absolute error: 0.30978260869565216
Coverage of cases: 69.02173913043478
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 52.0
Weighted Recall: 0.6902173913043478
Weighted FalsePositiveRate: 0.4858563365376135
Kappa statistic: 0.23512252042006998
Training time: 12.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.891304347826086
Correctly Classified Instances: 70.1086956521739
Weighted Precision: 0.7170194148208914
Weighted AreaUnderROC: 0.6138438133874239
Root mean squared error: 0.5467294060851866
Relative absolute error: 59.78260869565217
Root relative squared error: 109.34588121703732
Weighted TruePositiveRate: 0.7010869565217391
Weighted MatthewsCorrelation: 0.3207408788538755
Weighted FMeasure: 0.6553529686769517
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6110431156448027
Mean absolute error: 0.29891304347826086
Coverage of cases: 70.1086956521739
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7010869565217391
Weighted FalsePositiveRate: 0.4733993297468913
Kappa statistic: 0.26196032672112024
Training time: 16.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.17391304347826
Correctly Classified Instances: 72.82608695652173
Weighted Precision: 0.7686141304347827
Weighted AreaUnderROC: 0.6414807302231237
Root mean squared error: 0.5212860351426869
Relative absolute error: 54.347826086956516
Root relative squared error: 104.25720702853738
Weighted TruePositiveRate: 0.7282608695652174
Weighted MatthewsCorrelation: 0.40555067363324804
Weighted FMeasure: 0.684940138626339
Iteration time: 38.0
Weighted AreaUnderPRC: 0.6382723889413988
Mean absolute error: 0.2717391304347826
Coverage of cases: 72.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7282608695652174
Weighted FalsePositiveRate: 0.4452994091189699
Kappa statistic: 0.32669789227166274
Training time: 37.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 18.47826086956522
Correctly Classified Instances: 81.52173913043478
Weighted Precision: 0.8238815374921235
Weighted AreaUnderROC: 0.7682555780933064
Root mean squared error: 0.42986347681054754
Relative absolute error: 36.95652173913043
Root relative squared error: 85.9726953621095
Weighted TruePositiveRate: 0.8152173913043478
Weighted MatthewsCorrelation: 0.5980586126161953
Weighted FMeasure: 0.8053898869048691
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7423597983616886
Mean absolute error: 0.18478260869565216
Coverage of cases: 81.52173913043478
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 138.0
Weighted Recall: 0.8152173913043478
Weighted FalsePositiveRate: 0.27870623511773523
Kappa statistic: 0.575
Training time: 30.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 16.304347826086957
Correctly Classified Instances: 83.69565217391305
Weighted Precision: 0.8371748835403726
Weighted AreaUnderROC: 0.8067951318458418
Root mean squared error: 0.4037864265436241
Relative absolute error: 32.608695652173914
Root relative squared error: 80.75728530872482
Weighted TruePositiveRate: 0.8369565217391305
Weighted MatthewsCorrelation: 0.6436694727143099
Weighted FMeasure: 0.8330765870369945
Iteration time: 61.0
Weighted AreaUnderPRC: 0.7735137156528491
Mean absolute error: 0.16304347826086957
Coverage of cases: 83.69565217391305
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 199.0
Weighted Recall: 0.8369565217391305
Weighted FalsePositiveRate: 0.2233662580474469
Kappa statistic: 0.636842105263158
Training time: 60.0
		
Time end:Tue Oct 31 11.22.18 EET 2017