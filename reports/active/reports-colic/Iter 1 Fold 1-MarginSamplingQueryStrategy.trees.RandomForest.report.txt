Tue Oct 31 11.23.12 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.12 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 23.369565217391305
Correctly Classified Instances: 76.6304347826087
Weighted Precision: 0.7706294057325617
Weighted AreaUnderROC: 0.7883493914807304
Root mean squared error: 0.4157503022289195
Relative absolute error: 71.37578033613401
Root relative squared error: 83.15006044578391
Weighted TruePositiveRate: 0.7663043478260869
Weighted MatthewsCorrelation: 0.4818121612243985
Weighted FMeasure: 0.7513537899259436
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7909582064053776
Mean absolute error: 0.3568789016806701
Coverage of cases: 98.91304347826087
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.7663043478260869
Weighted FalsePositiveRate: 0.3438905547226387
Kappa statistic: 0.45718990120746417
Training time: 2.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 99.72826086956522
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8080235679922161
Weighted AreaUnderROC: 0.785496957403651
Root mean squared error: 0.41587775198906557
Relative absolute error: 72.55986955176996
Root relative squared error: 83.17555039781311
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5828067472404553
Weighted FMeasure: 0.8057183753802112
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7704583976382028
Mean absolute error: 0.3627993477588498
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2514661786753682
Kappa statistic: 0.5776495278069256
Training time: 4.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 92.66304347826087
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.802087051712239
Weighted AreaUnderROC: 0.8154158215010143
Root mean squared error: 0.3849323639227497
Relative absolute error: 56.49705646406563
Root relative squared error: 76.98647278454995
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5710926268842421
Weighted FMeasure: 0.8006262512191364
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8231199818878024
Mean absolute error: 0.28248528232032816
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.25465208572184495
Kappa statistic: 0.5669456066945607
Training time: 4.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 93.75
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8026009316770187
Weighted AreaUnderROC: 0.8002028397565922
Root mean squared error: 0.39318330408658764
Relative absolute error: 60.577871767996704
Root relative squared error: 78.63666081731753
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5702592353303636
Weighted FMeasure: 0.7996919044443933
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7999874355898037
Mean absolute error: 0.3028893588399835
Coverage of cases: 98.3695652173913
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.26073727841961375
Kappa statistic: 0.5642105263157895
Training time: 7.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 94.83695652173913
Incorrectly Classified Instances: 17.934782608695652
Correctly Classified Instances: 82.06521739130434
Weighted Precision: 0.8235262068214013
Weighted AreaUnderROC: 0.83341784989858
Root mean squared error: 0.3787356645046801
Relative absolute error: 58.9392084973155
Root relative squared error: 75.74713290093602
Weighted TruePositiveRate: 0.8206521739130435
Weighted MatthewsCorrelation: 0.6075467342894683
Weighted FMeasure: 0.8139639302551821
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8297724063008588
Mean absolute error: 0.2946960424865775
Coverage of cases: 97.82608695652173
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.8206521739130435
Weighted FalsePositiveRate: 0.25726474997795223
Kappa statistic: 0.5941176470588234
Training time: 10.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.3695652173913
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.807804185274541
Weighted AreaUnderROC: 0.8293610547667342
Root mean squared error: 0.3866929998740424
Relative absolute error: 63.80818733870059
Root relative squared error: 77.33859997480847
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5865081416287865
Weighted FMeasure: 0.8081590183746662
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8482651779041125
Mean absolute error: 0.319040936693503
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 47.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.23321060058206192
Kappa statistic: 0.5854788877445933
Training time: 13.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 95.92391304347827
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8268673355629879
Weighted AreaUnderROC: 0.820867139959432
Root mean squared error: 0.3802503060333383
Relative absolute error: 58.02674673580427
Root relative squared error: 76.05006120666766
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.6192420837674298
Weighted FMeasure: 0.8210570844126118
Iteration time: 17.0
Weighted AreaUnderPRC: 0.8348163640262267
Mean absolute error: 0.29013373367902134
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 64.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.2419084575359379
Kappa statistic: 0.6101694915254238
Training time: 16.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 97.28260869565217
Incorrectly Classified Instances: 16.847826086956523
Correctly Classified Instances: 83.15217391304348
Weighted Precision: 0.8335295480590397
Weighted AreaUnderROC: 0.8518002028397565
Root mean squared error: 0.36682949765814005
Relative absolute error: 57.56014831210414
Root relative squared error: 73.365899531628
Weighted TruePositiveRate: 0.8315217391304348
Weighted MatthewsCorrelation: 0.6318550616768159
Weighted FMeasure: 0.8261946083872442
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8603986081432105
Mean absolute error: 0.2878007415605207
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8315217391304348
Weighted FalsePositiveRate: 0.23872255048946114
Kappa statistic: 0.6211477151965994
Training time: 18.0
		
Time end:Tue Oct 31 11.23.13 EET 2017