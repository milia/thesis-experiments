Tue Oct 31 11.22.12 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.22.12 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 62.5
Incorrectly Classified Instances: 24.456521739130434
Correctly Classified Instances: 75.54347826086956
Weighted Precision: 0.7524225883909679
Weighted AreaUnderROC: 0.7578393707557984
Root mean squared error: 0.45817646145524676
Relative absolute error: 49.72611463171887
Root relative squared error: 91.63529229104935
Weighted TruePositiveRate: 0.7554347826086957
Weighted MatthewsCorrelation: 0.46786533373554035
Weighted FMeasure: 0.7533473093388565
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7486951583053395
Mean absolute error: 0.24863057315859435
Coverage of cases: 85.32608695652173
Instances selection time: 13.0
Test time: 12.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7554347826086957
Weighted FalsePositiveRate: 0.29549563453567335
Kappa statistic: 0.46704428424304845
Training time: 13.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 60.59782608695652
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7739773876275393
Weighted AreaUnderROC: 0.7777223520592645
Root mean squared error: 0.44376252156134116
Relative absolute error: 45.848105004218596
Root relative squared error: 88.75250431226823
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5084881745736276
Weighted FMeasure: 0.771310263235729
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7786319688187163
Mean absolute error: 0.22924052502109296
Coverage of cases: 85.8695652173913
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.29492239174530377
Kappa statistic: 0.5021119324181627
Training time: 2.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 59.51086956521739
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7875667429443173
Weighted AreaUnderROC: 0.7812527559749537
Root mean squared error: 0.4343416913775524
Relative absolute error: 43.52106959108853
Root relative squared error: 86.86833827551048
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5320818834056279
Weighted FMeasure: 0.780139190301579
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7811173142104847
Mean absolute error: 0.21760534795544267
Coverage of cases: 84.78260869565217
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.29463577035011906
Kappa statistic: 0.5203208556149731
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 59.78260869565217
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7943543153796236
Weighted AreaUnderROC: 0.7832398139165712
Root mean squared error: 0.4330406118428002
Relative absolute error: 43.364297079616975
Root relative squared error: 86.60812236856003
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5447212698664644
Weighted FMeasure: 0.7851613854089905
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7833616000622928
Mean absolute error: 0.21682148539808488
Coverage of cases: 85.8695652173913
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.2914498633036423
Kappa statistic: 0.5311158798283261
Training time: 1.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 57.880434782608695
Incorrectly Classified Instances: 21.195652173913043
Correctly Classified Instances: 78.80434782608695
Weighted Precision: 0.7891517959840941
Weighted AreaUnderROC: 0.7803570641150014
Root mean squared error: 0.4316993554206063
Relative absolute error: 43.1365180720715
Root relative squared error: 86.33987108412126
Weighted TruePositiveRate: 0.7880434782608695
Weighted MatthewsCorrelation: 0.5321511484711404
Weighted FMeasure: 0.7788555921242566
Iteration time: 13.0
Weighted AreaUnderPRC: 0.784136181897881
Mean absolute error: 0.2156825903603575
Coverage of cases: 85.32608695652173
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 60.0
Weighted Recall: 0.7880434782608695
Weighted FalsePositiveRate: 0.3007209630478878
Kappa statistic: 0.5172228202368137
Training time: 6.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 58.69565217391305
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8115397188623734
Weighted AreaUnderROC: 0.79276308536908
Root mean squared error: 0.4212313745408605
Relative absolute error: 41.092386110392674
Root relative squared error: 84.2462749081721
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5823917839948547
Weighted FMeasure: 0.8026890169373145
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7914390035401572
Mean absolute error: 0.20546193055196338
Coverage of cases: 86.41304347826087
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 66.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.2697217567686745
Kappa statistic: 0.5695187165775402
Training time: 2.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 60.32608695652174
Incorrectly Classified Instances: 21.73913043478261
Correctly Classified Instances: 78.26086956521739
Weighted Precision: 0.779336522215083
Weighted AreaUnderROC: 0.7919624746450304
Root mean squared error: 0.42621556111372355
Relative absolute error: 44.148139631156184
Root relative squared error: 85.24311222274471
Weighted TruePositiveRate: 0.782608695652174
Weighted MatthewsCorrelation: 0.5226235109494171
Weighted FMeasure: 0.7784736124657072
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7939266968403339
Mean absolute error: 0.22074069815578093
Coverage of cases: 86.95652173913044
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 74.0
Weighted Recall: 0.782608695652174
Weighted FalsePositiveRate: 0.2795660993032896
Kappa statistic: 0.5188284518828452
Training time: 4.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 63.04347826086956
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.7765270636782643
Weighted AreaUnderROC: 0.7904411764705882
Root mean squared error: 0.434753032008304
Relative absolute error: 46.06514935015515
Root relative squared error: 86.9506064016608
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5203814918762498
Weighted FMeasure: 0.7768267988085118
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7959812736669415
Mean absolute error: 0.23032574675077577
Coverage of cases: 86.41304347826087
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 78.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.2584112355586913
Kappa statistic: 0.5203458799593084
Training time: 2.0
		
Time end:Tue Oct 31 11.22.13 EET 2017