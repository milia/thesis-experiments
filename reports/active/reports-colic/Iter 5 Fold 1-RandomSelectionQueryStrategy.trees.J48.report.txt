Tue Oct 31 11.23.30 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.30 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 83.42391304347827
Incorrectly Classified Instances: 32.06521739130435
Correctly Classified Instances: 67.93478260869566
Weighted Precision: 0.7379656989196879
Weighted AreaUnderROC: 0.8331009127789046
Root mean squared error: 0.4762363529232614
Relative absolute error: 62.85225442834139
Root relative squared error: 95.24727058465228
Weighted TruePositiveRate: 0.6793478260869565
Weighted MatthewsCorrelation: 0.40678915594270604
Weighted FMeasure: 0.6836787768730295
Iteration time: 7.0
Weighted AreaUnderPRC: 0.819898420900728
Mean absolute error: 0.31426127214170696
Coverage of cases: 95.1086956521739
Instances selection time: 6.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6793478260869565
Weighted FalsePositiveRate: 0.26099082811535407
Kappa statistic: 0.3780934922089826
Training time: 1.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 83.42391304347827
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8736405301622693
Weighted AreaUnderROC: 0.8260015212981745
Root mean squared error: 0.3488584511173031
Relative absolute error: 43.57802377613034
Root relative squared error: 69.77169022346062
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7181488054802831
Weighted FMeasure: 0.8657928133094587
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8170301859370283
Mean absolute error: 0.21789011888065168
Coverage of cases: 95.1086956521739
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.19208043037304878
Kappa statistic: 0.7076271186440678
Training time: 1.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.58695652173913
Correctly Classified Instances: 86.41304347826087
Weighted Precision: 0.866829059043417
Weighted AreaUnderROC: 0.8694687582679248
Root mean squared error: 0.33726266426195195
Relative absolute error: 47.86641464632263
Root relative squared error: 67.45253285239039
Weighted TruePositiveRate: 0.8641304347826086
Weighted MatthewsCorrelation: 0.7052533146820344
Weighted FMeasure: 0.8605550385583716
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8463174329332107
Mean absolute error: 0.23933207323161315
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8641304347826086
Weighted FalsePositiveRate: 0.19526633741952554
Kappa statistic: 0.6964097148891234
Training time: 1.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.58695652173913
Correctly Classified Instances: 86.41304347826087
Weighted Precision: 0.866829059043417
Weighted AreaUnderROC: 0.8237829614604463
Root mean squared error: 0.33966557156694516
Relative absolute error: 50.4413319367945
Root relative squared error: 67.93311431338903
Weighted TruePositiveRate: 0.8641304347826086
Weighted MatthewsCorrelation: 0.7052533146820344
Weighted FMeasure: 0.8605550385583716
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8179548839126162
Mean absolute error: 0.2522066596839725
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8641304347826086
Weighted FalsePositiveRate: 0.19526633741952554
Kappa statistic: 0.6964097148891234
Training time: 2.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8736405301622693
Weighted AreaUnderROC: 0.8260015212981745
Root mean squared error: 0.336687757706718
Relative absolute error: 46.93904721746268
Root relative squared error: 67.3375515413436
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7181488054802831
Weighted FMeasure: 0.8657928133094587
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8170301859370283
Mean absolute error: 0.2346952360873134
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.19208043037304878
Kappa statistic: 0.7076271186440678
Training time: 2.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.58695652173913
Correctly Classified Instances: 86.41304347826087
Weighted Precision: 0.866829059043417
Weighted AreaUnderROC: 0.8266050798130347
Root mean squared error: 0.3372992554354318
Relative absolute error: 45.018465478823536
Root relative squared error: 67.45985108708636
Weighted TruePositiveRate: 0.8641304347826086
Weighted MatthewsCorrelation: 0.7052533146820344
Weighted FMeasure: 0.8605550385583716
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8182674643588439
Mean absolute error: 0.2250923273941177
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8641304347826086
Weighted FalsePositiveRate: 0.19526633741952554
Kappa statistic: 0.6964097148891234
Training time: 2.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.58695652173913
Correctly Classified Instances: 86.41304347826087
Weighted Precision: 0.866829059043417
Weighted AreaUnderROC: 0.8266050798130347
Root mean squared error: 0.33800771746482755
Relative absolute error: 46.62887784343362
Root relative squared error: 67.6015434929655
Weighted TruePositiveRate: 0.8641304347826086
Weighted MatthewsCorrelation: 0.7052533146820344
Weighted FMeasure: 0.8605550385583716
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8182674643588439
Mean absolute error: 0.2331443892171681
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8641304347826086
Weighted FalsePositiveRate: 0.19526633741952554
Kappa statistic: 0.6964097148891234
Training time: 3.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 100.0
Incorrectly Classified Instances: 13.043478260869565
Correctly Classified Instances: 86.95652173913044
Weighted Precision: 0.8736405301622693
Weighted AreaUnderROC: 0.8716404665314401
Root mean squared error: 0.3345719435909418
Relative absolute error: 46.85347251157332
Root relative squared error: 66.91438871818835
Weighted TruePositiveRate: 0.8695652173913043
Weighted MatthewsCorrelation: 0.7181488054802831
Weighted FMeasure: 0.8657928133094587
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8480875989973179
Mean absolute error: 0.23426736255786662
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8695652173913043
Weighted FalsePositiveRate: 0.19208043037304878
Kappa statistic: 0.7076271186440678
Training time: 3.0
		
Time end:Tue Oct 31 11.23.31 EET 2017