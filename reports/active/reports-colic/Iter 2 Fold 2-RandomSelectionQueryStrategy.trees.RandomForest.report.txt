Tue Oct 31 11.23.33 EET 2017
Dataset: horse-colic
Test set size: 184
Initial Labelled set size: 36
Initial Unlabelled set size: 148
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.23.33 EET 2017
		
Iteration: 1
Labeled set size: 36
Unlabelled set size: 148
	
Mean region size: 98.91304347826087
Incorrectly Classified Instances: 32.608695652173914
Correctly Classified Instances: 67.3913043478261
Weighted Precision: 0.7042142049542603
Weighted AreaUnderROC: 0.7737068965517241
Root mean squared error: 0.4407635248166196
Relative absolute error: 80.95223429773263
Root relative squared error: 88.15270496332393
Weighted TruePositiveRate: 0.6739130434782609
Weighted MatthewsCorrelation: 0.354511171206314
Weighted FMeasure: 0.6795345546033493
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7951688268280414
Mean absolute error: 0.40476117148866314
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6739130434782609
Weighted FalsePositiveRate: 0.30677308404621223
Kappa statistic: 0.34410646387832694
Training time: 3.0
		
Iteration: 2
Labeled set size: 56
Unlabelled set size: 128
	
Mean region size: 97.82608695652173
Incorrectly Classified Instances: 20.652173913043477
Correctly Classified Instances: 79.34782608695652
Weighted Precision: 0.7907117869636611
Weighted AreaUnderROC: 0.8465390466531441
Root mean squared error: 0.388733104875883
Relative absolute error: 64.20507162959103
Root relative squared error: 77.7466209751766
Weighted TruePositiveRate: 0.7934782608695652
Weighted MatthewsCorrelation: 0.5468580689168295
Weighted FMeasure: 0.789549931842422
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8465639434432689
Mean absolute error: 0.3210253581479552
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7934782608695652
Weighted FalsePositiveRate: 0.26710909251256726
Kappa statistic: 0.5428870292887028
Training time: 4.0
		
Iteration: 3
Labeled set size: 76
Unlabelled set size: 108
	
Mean region size: 95.92391304347827
Incorrectly Classified Instances: 22.282608695652176
Correctly Classified Instances: 77.71739130434783
Weighted Precision: 0.777882797731569
Weighted AreaUnderROC: 0.8582657200811359
Root mean squared error: 0.38386076644047057
Relative absolute error: 61.62842967817731
Root relative squared error: 76.77215328809412
Weighted TruePositiveRate: 0.7771739130434783
Weighted MatthewsCorrelation: 0.5232911917848363
Weighted FMeasure: 0.7775048393899552
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8615431828019509
Mean absolute error: 0.30814214839088655
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7771739130434783
Weighted FalsePositiveRate: 0.2523260428609225
Kappa statistic: 0.5232558139534884
Training time: 12.0
		
Iteration: 4
Labeled set size: 96
Unlabelled set size: 88
	
Mean region size: 96.73913043478261
Incorrectly Classified Instances: 19.565217391304348
Correctly Classified Instances: 80.43478260869566
Weighted Precision: 0.8043478260869565
Weighted AreaUnderROC: 0.8382352941176471
Root mean squared error: 0.39207341039075205
Relative absolute error: 64.90102371987938
Root relative squared error: 78.4146820781504
Weighted TruePositiveRate: 0.8043478260869565
Weighted MatthewsCorrelation: 0.5801217038539555
Weighted FMeasure: 0.8043478260869567
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8488985769713914
Mean absolute error: 0.3245051185993969
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.8043478260869565
Weighted FalsePositiveRate: 0.22422612223300115
Kappa statistic: 0.5801217038539555
Training time: 8.0
		
Iteration: 5
Labeled set size: 116
Unlabelled set size: 68
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 19.02173913043478
Correctly Classified Instances: 80.97826086956522
Weighted Precision: 0.8075465465987518
Weighted AreaUnderROC: 0.8608012170385394
Root mean squared error: 0.3720436631167485
Relative absolute error: 59.56371012976434
Root relative squared error: 74.4087326233497
Weighted TruePositiveRate: 0.8097826086956522
Weighted MatthewsCorrelation: 0.5849403620529443
Weighted FMeasure: 0.8074072820551872
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8697567364091094
Mean absolute error: 0.2978185506488217
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 50.0
Weighted Recall: 0.8097826086956522
Weighted FalsePositiveRate: 0.23929579327983072
Kappa statistic: 0.5829015544041452
Training time: 10.0
		
Iteration: 6
Labeled set size: 136
Unlabelled set size: 48
	
Mean region size: 98.09782608695652
Incorrectly Classified Instances: 15.76086956521739
Correctly Classified Instances: 84.23913043478261
Weighted Precision: 0.8410331434046848
Weighted AreaUnderROC: 0.8640973630831644
Root mean squared error: 0.37250343055155805
Relative absolute error: 62.09811730916376
Root relative squared error: 74.5006861103116
Weighted TruePositiveRate: 0.842391304347826
Weighted MatthewsCorrelation: 0.6576938263647343
Weighted FMeasure: 0.841046043796152
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8600767048580191
Mean absolute error: 0.3104905865458188
Coverage of cases: 99.45652173913044
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 64.0
Weighted Recall: 0.842391304347826
Weighted FalsePositiveRate: 0.19583958020989506
Kappa statistic: 0.65653964984552
Training time: 13.0
		
Iteration: 7
Labeled set size: 156
Unlabelled set size: 28
	
Mean region size: 94.83695652173913
Incorrectly Classified Instances: 20.108695652173914
Correctly Classified Instances: 79.8913043478261
Weighted Precision: 0.797389055956831
Weighted AreaUnderROC: 0.8770283975659229
Root mean squared error: 0.36867328732423627
Relative absolute error: 56.494614715285365
Root relative squared error: 73.73465746484726
Weighted TruePositiveRate: 0.7989130434782609
Weighted MatthewsCorrelation: 0.5648353821749129
Weighted FMeasure: 0.797928679237409
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8860603291725514
Mean absolute error: 0.2824730735764268
Coverage of cases: 99.45652173913044
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7989130434782609
Weighted FalsePositiveRate: 0.23958241467501543
Kappa statistic: 0.5644831115660184
Training time: 16.0
		
Iteration: 8
Labeled set size: 176
Unlabelled set size: 8
	
Mean region size: 96.19565217391305
Incorrectly Classified Instances: 17.391304347826086
Correctly Classified Instances: 82.6086956521739
Weighted Precision: 0.8244039270687237
Weighted AreaUnderROC: 0.892368154158215
Root mean squared error: 0.3510335932169934
Relative absolute error: 54.81962381054899
Root relative squared error: 70.20671864339867
Weighted TruePositiveRate: 0.8260869565217391
Weighted MatthewsCorrelation: 0.620306758819879
Weighted FMeasure: 0.8235507246376812
Iteration time: 26.0
Weighted AreaUnderPRC: 0.8960481437566814
Mean absolute error: 0.27409811905274495
Coverage of cases: 98.91304347826087
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 106.0
Weighted Recall: 0.8260869565217391
Weighted FalsePositiveRate: 0.22365287944263162
Kappa statistic: 0.6174636174636174
Training time: 25.0
		
Time end:Tue Oct 31 11.23.33 EET 2017