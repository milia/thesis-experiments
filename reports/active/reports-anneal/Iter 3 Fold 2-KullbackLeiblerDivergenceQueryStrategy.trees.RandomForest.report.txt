Tue Oct 31 11.07.54 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.07.54 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.839643652561207
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9185491568565065
Weighted AreaUnderROC: 0.963396745895419
Root mean squared error: 0.15110000686011357
Relative absolute error: 19.602627690163168
Root relative squared error: 40.54438640878802
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7774814677789446
Weighted FMeasure: 0.909746708913862
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9675185427699712
Mean absolute error: 0.05445174358378604
Coverage of cases: 98.88641425389756
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2562704244114647
Kappa statistic: 0.7675615823758645
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 28.247958426132122
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.904934316305992
Weighted AreaUnderROC: 0.9457254875652162
Root mean squared error: 0.16397119592489717
Relative absolute error: 22.223046390648978
Root relative squared error: 43.998088852801004
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7484709844581463
Weighted FMeasure: 0.9017970570053518
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9505241341473146
Mean absolute error: 0.061730684418468774
Coverage of cases: 98.88641425389756
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 13.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.2573617384426451
Kappa statistic: 0.7465317470398126
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 26.98589458054933
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9196867893564648
Weighted AreaUnderROC: 0.9635786894964375
Root mean squared error: 0.1578377365842426
Relative absolute error: 19.809713798639166
Root relative squared error: 42.35230901004884
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7830154966748892
Weighted FMeasure: 0.9129657542673195
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9672243818562872
Mean absolute error: 0.05502698277399714
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.2421370094496252
Kappa statistic: 0.777474122428173
Training time: 5.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 25.612472160356294
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9223346713280033
Weighted AreaUnderROC: 0.9763318516939491
Root mean squared error: 0.14523787621694076
Relative absolute error: 16.289843404767222
Root relative squared error: 38.97141169545374
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7952549534895241
Weighted FMeasure: 0.9180362950757391
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9726555230630959
Mean absolute error: 0.04524956501324184
Coverage of cases: 98.66369710467706
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.22806876131798595
Kappa statistic: 0.7919223693634231
Training time: 5.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 26.61469933184853
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.8839184788082953
Weighted AreaUnderROC: 0.976167080591737
Root mean squared error: 0.165260242050747
Relative absolute error: 19.35150524720158
Root relative squared error: 44.34397622442496
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.6409705435785298
Weighted FMeasure: 0.8488346913150571
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9671823758671492
Mean absolute error: 0.05375418124222607
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.39864288241783397
Kappa statistic: 0.6048280631168669
Training time: 6.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 22.56867112100956
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9353577054701505
Weighted AreaUnderROC: 0.9791288838859245
Root mean squared error: 0.131771813964731
Relative absolute error: 11.409819310004227
Root relative squared error: 35.358088025231524
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8338008727849646
Weighted FMeasure: 0.9329436985255496
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9762062039287254
Mean absolute error: 0.031693942527789204
Coverage of cases: 98.66369710467706
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 41.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.19220281830859853
Kappa statistic: 0.8325783731528793
Training time: 5.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 22.717149220489897
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9222148509690565
Weighted AreaUnderROC: 0.9681984515546896
Root mean squared error: 0.14273048473847372
Relative absolute error: 12.663293239007936
Root relative squared error: 38.29860796040701
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7898555122562875
Weighted FMeasure: 0.9148800257106477
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9652183979868671
Mean absolute error: 0.03517581455279947
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 49.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.782934493594392
Training time: 7.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 24.684484038604257
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.899061610777449
Weighted AreaUnderROC: 0.9708634208938937
Root mean squared error: 0.15366252056321034
Relative absolute error: 15.895667781412708
Root relative squared error: 41.23198098879591
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7038105688535127
Weighted FMeasure: 0.8765156671619679
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9700683555787755
Mean absolute error: 0.044154632726145976
Coverage of cases: 97.55011135857461
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 57.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.3345752763149678
Kappa statistic: 0.6817523752073593
Training time: 6.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 21.714922048997696
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9249932098430116
Weighted AreaUnderROC: 0.9822187235919725
Root mean squared error: 0.1234767493359538
Relative absolute error: 11.040780186745605
Root relative squared error: 33.13228861870753
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8071746819798342
Weighted FMeasure: 0.9234617743245591
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9777346875794022
Mean absolute error: 0.03066883385207082
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.20698572112482566
Kappa statistic: 0.8077751691755283
Training time: 8.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.60579064587964
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9185491568565065
Weighted AreaUnderROC: 0.9716658577322462
Root mean squared error: 0.14560121642532078
Relative absolute error: 14.431116058684788
Root relative squared error: 39.06890610404133
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7777754281939286
Weighted FMeasure: 0.9106833798664944
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9696166851031814
Mean absolute error: 0.04008643349634624
Coverage of cases: 98.44097995545657
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 75.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2562704244114647
Kappa statistic: 0.7678953490041784
Training time: 8.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.34595397178906
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.926191904214344
Weighted AreaUnderROC: 0.962713401542905
Root mean squared error: 0.14429106682366294
Relative absolute error: 13.5454132954164
Root relative squared error: 38.71735607564119
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8077979931869887
Weighted FMeasure: 0.9229845574575132
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9584385383864183
Mean absolute error: 0.03762614804282296
Coverage of cases: 97.55011135857461
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 94.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.21383151551734902
Kappa statistic: 0.8062068389396302
Training time: 17.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 22.976985894580494
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9203771422495023
Weighted AreaUnderROC: 0.9483067978219978
Root mean squared error: 0.14390897091937796
Relative absolute error: 13.581707500767079
Root relative squared error: 38.61482898573254
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7839182349523448
Weighted FMeasure: 0.9129581619322946
Iteration time: 23.0
Weighted AreaUnderPRC: 0.949809179312221
Mean absolute error: 0.03772696527990818
Coverage of cases: 97.99554565701558
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 117.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.24915180151114624
Kappa statistic: 0.7754294206750695
Training time: 20.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.083890126206303
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.9077811804008908
Weighted AreaUnderROC: 0.926951654352006
Root mean squared error: 0.17656652329516898
Relative absolute error: 16.810773721399634
Root relative squared error: 47.37776983665599
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7381854384482356
Weighted FMeasure: 0.8930615551815242
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9296033186780401
Mean absolute error: 0.04669659367055407
Coverage of cases: 94.87750556792874
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 142.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.29898216181337556
Kappa statistic: 0.7224315572564027
Training time: 23.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 22.902746844840298
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9222148509690565
Weighted AreaUnderROC: 0.969533074355447
Root mean squared error: 0.1491162628848281
Relative absolute error: 14.332073694115369
Root relative squared error: 40.01209204334474
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7901908424281205
Weighted FMeasure: 0.9154507187598079
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9638967969144115
Mean absolute error: 0.03981131581698674
Coverage of cases: 97.10467706013362
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 164.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.782798605676887
Training time: 21.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 22.828507795100137
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9185491568565065
Weighted AreaUnderROC: 0.9695755310401565
Root mean squared error: 0.15636463463309944
Relative absolute error: 15.069037357729357
Root relative squared error: 41.957034277983595
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7765603688199226
Weighted FMeasure: 0.9085057069267789
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9627609097311721
Mean absolute error: 0.04185843710480335
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 174.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2562704244114647
Kappa statistic: 0.7683344560216703
Training time: 9.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 21.937639198218182
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9321748275824967
Weighted AreaUnderROC: 0.9865210260238413
Root mean squared error: 0.12258362157090277
Relative absolute error: 10.90281234988438
Root relative squared error: 32.892637291277936
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8267895033834608
Weighted FMeasure: 0.9307602367697082
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9816512995169506
Mean absolute error: 0.03028558986078964
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 186.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19247564681639362
Kappa statistic: 0.8270772870445511
Training time: 11.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 22.717149220489954
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9529960648855869
Weighted AreaUnderROC: 0.9833506423790167
Root mean squared error: 0.11560253295386089
Relative absolute error: 10.357447396219458
Root relative squared error: 31.01941464671925
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8923932829139732
Weighted FMeasure: 0.9550220781537627
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9757426917985016
Mean absolute error: 0.02877068721172043
Coverage of cases: 97.99554565701558
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 197.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.08706044585059218
Kappa statistic: 0.8967420467612113
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 20.6013363028953
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9757382078650604
Weighted AreaUnderROC: 0.9754908275400334
Root mean squared error: 0.08561936072447693
Relative absolute error: 6.433692048367745
Root relative squared error: 22.97408529240084
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9545856205486681
Weighted FMeasure: 0.9799060535752248
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9760316154642901
Mean absolute error: 0.017871366801021336
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 210.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.04298456590970589
Kappa statistic: 0.9601955395701677
Training time: 12.0
		
Time end:Tue Oct 31 11.07.55 EET 2017