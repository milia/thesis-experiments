Tue Oct 31 11.10.54 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.54 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.786933927245723
Incorrectly Classified Instances: 12.026726057906458
Correctly Classified Instances: 87.97327394209354
Weighted Precision: 0.8724053011963343
Weighted AreaUnderROC: 0.8667691307974401
Root mean squared error: 0.18159752195431988
Relative absolute error: 16.568391364208274
Root relative squared error: 48.727728436244604
Weighted TruePositiveRate: 0.8797327394209354
Weighted MatthewsCorrelation: 0.661229147473797
Weighted FMeasure: 0.8629617888029182
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8507174797316449
Mean absolute error: 0.04602330934502253
Coverage of cases: 93.98663697104676
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8797327394209354
Weighted FalsePositiveRate: 0.28181270569175265
Kappa statistic: 0.6649994473306069
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.48997772828507
Incorrectly Classified Instances: 12.026726057906458
Correctly Classified Instances: 87.97327394209354
Weighted Precision: 0.8681576674391104
Weighted AreaUnderROC: 0.8665396687533917
Root mean squared error: 0.18777028909670193
Relative absolute error: 17.220037295776677
Root relative squared error: 50.38405566900182
Weighted TruePositiveRate: 0.8797327394209354
Weighted MatthewsCorrelation: 0.6786184341455198
Weighted FMeasure: 0.8686295681597186
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8519047799577063
Mean absolute error: 0.04783343693271251
Coverage of cases: 93.54120267260579
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8797327394209354
Weighted FalsePositiveRate: 0.22654111017790426
Kappa statistic: 0.683352705332306
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 21.306607275426895
Incorrectly Classified Instances: 13.3630289532294
Correctly Classified Instances: 86.6369710467706
Weighted Precision: 0.8627593399487933
Weighted AreaUnderROC: 0.8647250551996934
Root mean squared error: 0.19268230529050387
Relative absolute error: 18.399547563140022
Root relative squared error: 51.70208792291234
Weighted TruePositiveRate: 0.8663697104677061
Weighted MatthewsCorrelation: 0.659594375529605
Weighted FMeasure: 0.8577215751432966
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8512585573671426
Mean absolute error: 0.05110985434205511
Coverage of cases: 93.54120267260579
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8663697104677061
Weighted FalsePositiveRate: 0.19319867548530853
Kappa statistic: 0.6689563646641026
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 20.93541202672606
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.8928696880465166
Weighted AreaUnderROC: 0.9246602436238136
Root mean squared error: 0.2106372397420942
Relative absolute error: 21.292289798601356
Root relative squared error: 56.51990239874943
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.6875187876083559
Weighted FMeasure: 0.8609858611781215
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9015531297964461
Mean absolute error: 0.05914524944055874
Coverage of cases: 91.53674832962137
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.08100505960797233
Kappa statistic: 0.6761458255990327
Training time: 1.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 21.046770601336316
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.8964854928106599
Weighted AreaUnderROC: 0.9461242935781647
Root mean squared error: 0.2032825348943257
Relative absolute error: 20.12337366297631
Root relative squared error: 54.54642799946251
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.6918023191604765
Weighted FMeasure: 0.8618469230465498
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9270117999910035
Mean absolute error: 0.055898260174933635
Coverage of cases: 93.3184855233853
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.07381500421329323
Kappa statistic: 0.6784446714621226
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.935412026726063
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.9073596925736845
Weighted AreaUnderROC: 0.9438397094817295
Root mean squared error: 0.21109772178254596
Relative absolute error: 20.89402795397582
Root relative squared error: 56.643462696133575
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.6954271717730844
Weighted FMeasure: 0.8638911269049747
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9297182372599644
Mean absolute error: 0.05803896653882114
Coverage of cases: 91.31403118040089
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.06659537313114372
Kappa statistic: 0.6828700818480203
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 20.74981440237567
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.910863247304363
Weighted AreaUnderROC: 0.9455921219164457
Root mean squared error: 0.20676770074938516
Relative absolute error: 20.10429701850269
Root relative squared error: 55.481596131235406
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.7029204309071321
Weighted FMeasure: 0.8684606481387588
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9291814697391426
Mean absolute error: 0.055845269495840254
Coverage of cases: 91.75946547884188
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.0662420238078612
Kappa statistic: 0.6902088690961549
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 20.786933927245755
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.9073596925736845
Weighted AreaUnderROC: 0.9483481921734107
Root mean squared error: 0.20878762557542804
Relative absolute error: 20.34515110458297
Root relative squared error: 56.023598837691964
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.6954271717730844
Weighted FMeasure: 0.8638911269049747
Iteration time: 1.0
Weighted AreaUnderPRC: 0.935625955574507
Mean absolute error: 0.05651430862384102
Coverage of cases: 91.75946547884188
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.06659537313114372
Kappa statistic: 0.6828700818480203
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 20.786933927245755
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.916684039790944
Weighted AreaUnderROC: 0.9487722557096653
Root mean squared error: 0.20730975085076875
Relative absolute error: 20.13516368987108
Root relative squared error: 55.627043436103925
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.716391893629785
Weighted FMeasure: 0.8685109656981522
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9300756131458718
Mean absolute error: 0.05593101024964134
Coverage of cases: 91.75946547884188
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.038576650363466615
Kappa statistic: 0.6977510326890712
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 20.74981440237568
Incorrectly Classified Instances: 15.590200445434299
Correctly Classified Instances: 84.40979955456571
Weighted Precision: 0.9127465735691128
Weighted AreaUnderROC: 0.9509093724995548
Root mean squared error: 0.21378022808910357
Relative absolute error: 21.019588991134256
Root relative squared error: 57.36325467031775
Weighted TruePositiveRate: 0.844097995545657
Weighted MatthewsCorrelation: 0.7032306824250867
Weighted FMeasure: 0.8605181514321532
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9356542647317644
Mean absolute error: 0.05838774719759457
Coverage of cases: 91.0913140311804
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 11.0
Weighted Recall: 0.844097995545657
Weighted FalsePositiveRate: 0.039517763718130546
Kappa statistic: 0.6833026006872022
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 20.564216778025266
Incorrectly Classified Instances: 15.812917594654788
Correctly Classified Instances: 84.18708240534521
Weighted Precision: 0.91375954606391
Weighted AreaUnderROC: 0.9492285510596796
Root mean squared error: 0.2132625036485678
Relative absolute error: 20.78872111416137
Root relative squared error: 57.224334625199624
Weighted TruePositiveRate: 0.8418708240534521
Weighted MatthewsCorrelation: 0.7010924537189979
Weighted FMeasure: 0.8595286814866814
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9335460012708225
Mean absolute error: 0.05774644753933656
Coverage of cases: 91.0913140311804
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8418708240534521
Weighted FalsePositiveRate: 0.039548750734084155
Kappa statistic: 0.6798590050011047
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 20.60133630289536
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.9126141534280148
Weighted AreaUnderROC: 0.9517593414248456
Root mean squared error: 0.21241431580617282
Relative absolute error: 20.652361043771844
Root relative squared error: 56.996741944405535
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.6977619302344253
Weighted FMeasure: 0.8574198378259457
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9354096601767817
Mean absolute error: 0.057367669566032335
Coverage of cases: 91.31403118040089
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.03982784490603966
Kappa statistic: 0.6762991889456292
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 20.415738678544944
Incorrectly Classified Instances: 15.812917594654788
Correctly Classified Instances: 84.18708240534521
Weighted Precision: 0.9149389752735131
Weighted AreaUnderROC: 0.9525173537259793
Root mean squared error: 0.21088605287469075
Relative absolute error: 20.38844825865748
Root relative squared error: 56.58666596813109
Weighted TruePositiveRate: 0.8418708240534521
Weighted MatthewsCorrelation: 0.7015484941338155
Weighted FMeasure: 0.859968709878903
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9352740183274806
Mean absolute error: 0.05663457849627022
Coverage of cases: 91.31403118040089
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8418708240534521
Weighted FalsePositiveRate: 0.039753589754712645
Kappa statistic: 0.6797496584424979
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 20.3043801039347
Incorrectly Classified Instances: 16.258351893095767
Correctly Classified Instances: 83.74164810690424
Weighted Precision: 0.911499656980257
Weighted AreaUnderROC: 0.9509386533373189
Root mean squared error: 0.21117254424274684
Relative absolute error: 20.17856052942173
Root relative squared error: 56.66353966900393
Weighted TruePositiveRate: 0.8374164810690423
Weighted MatthewsCorrelation: 0.6944640282033215
Weighted FMeasure: 0.8553189874761647
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9358626950904136
Mean absolute error: 0.056051557026170915
Coverage of cases: 90.86859688195992
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8374164810690423
Weighted FalsePositiveRate: 0.04010693907799516
Kappa statistic: 0.6727601285916814
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 20.118782479584286
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.918374892287622
Weighted AreaUnderROC: 0.9494808284164896
Root mean squared error: 0.2102444416746603
Relative absolute error: 19.966529388814017
Root relative squared error: 56.41450361712389
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.7116491250897693
Weighted FMeasure: 0.8662694810541585
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9343666484061139
Mean absolute error: 0.05546258163559394
Coverage of cases: 90.86859688195992
Instances selection time: 0.0
Test time: 25.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.03891630723884614
Kappa statistic: 0.6905582356995177
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.93318485523389
Incorrectly Classified Instances: 15.144766146993318
Correctly Classified Instances: 84.85523385300668
Weighted Precision: 0.9190565373124793
Weighted AreaUnderROC: 0.9527490647536455
Root mean squared error: 0.20712788863962514
Relative absolute error: 19.431119828292942
Root relative squared error: 55.578244684105265
Weighted TruePositiveRate: 0.8485523385300668
Weighted MatthewsCorrelation: 0.7119128477458653
Weighted FMeasure: 0.8665124655287804
Iteration time: 1.0
Weighted AreaUnderPRC: 0.936586539374565
Mean absolute error: 0.05397533285636875
Coverage of cases: 90.86859688195992
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8485523385300668
Weighted FalsePositiveRate: 0.03901872674916039
Kappa statistic: 0.6905049112528003
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 19.78470675575356
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.9214440015707639
Weighted AreaUnderROC: 0.9504508246065106
Root mean squared error: 0.2070054026729584
Relative absolute error: 19.172238854583654
Root relative squared error: 55.54537825037447
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.7187868102618993
Weighted FMeasure: 0.8707296400819353
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9350454765916237
Mean absolute error: 0.053256219040509624
Coverage of cases: 90.64587973273942
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.03846053840524939
Kappa statistic: 0.6978188381412707
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.524870081662993
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.9226898971204678
Weighted AreaUnderROC: 0.9538820864055665
Root mean squared error: 0.2056807960053221
Relative absolute error: 18.898361102871053
Root relative squared error: 55.18994898410036
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.7222774951044323
Weighted FMeasure: 0.8728516901594899
Iteration time: 2.0
Weighted AreaUnderPRC: 0.937472108300341
Mean absolute error: 0.05249544750797463
Coverage of cases: 90.86859688195992
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 24.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.038181444233293886
Kappa statistic: 0.7015085655842496
Training time: 1.0
		
Time end:Tue Oct 31 11.10.55 EET 2017