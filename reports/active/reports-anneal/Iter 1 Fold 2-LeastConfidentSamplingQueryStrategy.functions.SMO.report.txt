Tue Oct 31 11.08.17 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.08.17 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9239285862995947
Weighted AreaUnderROC: 0.9155568457901988
Root mean squared error: 0.3129791550521431
Relative absolute error: 80.60579064588107
Root relative squared error: 83.98111994844506
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8073284886912842
Weighted FMeasure: 0.9249068425987657
Iteration time: 157.0
Weighted AreaUnderPRC: 0.9009349040951071
Mean absolute error: 0.22390497401633408
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 157.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.12394605275838778
Kappa statistic: 0.8146623971180547
Training time: 153.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9351793115229158
Weighted AreaUnderROC: 0.9375878758519836
Root mean squared error: 0.3127682384198914
Relative absolute error: 80.55233853006813
Root relative squared error: 83.92452507716904
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8313413418831314
Weighted FMeasure: 0.932031395962732
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9168133072659894
Mean absolute error: 0.2237564959168537
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 248.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.08230560986862634
Kappa statistic: 0.8368024425113281
Training time: 87.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9351793115229158
Weighted AreaUnderROC: 0.9375903782918625
Root mean squared error: 0.3127682384198914
Relative absolute error: 80.55233853006813
Root relative squared error: 83.92452507716904
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8313413418831314
Weighted FMeasure: 0.932031395962732
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9168137650607462
Mean absolute error: 0.2237564959168537
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 337.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.08230560986862634
Kappa statistic: 0.8368024425113281
Training time: 85.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579860690104083
Weighted AreaUnderROC: 0.9443916763787017
Root mean squared error: 0.31197603107612976
Relative absolute error: 80.37416481069172
Root relative squared error: 83.71195354041798
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824953060894929
Weighted FMeasure: 0.9497211776520353
Iteration time: 104.0
Weighted AreaUnderPRC: 0.9328046125692457
Mean absolute error: 0.22326156891858592
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 441.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09445189725870574
Kappa statistic: 0.8851295171090502
Training time: 100.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579860690104083
Weighted AreaUnderROC: 0.9443816666191861
Root mean squared error: 0.31197603107612976
Relative absolute error: 80.37416481069172
Root relative squared error: 83.71195354041798
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824953060894929
Weighted FMeasure: 0.9497211776520353
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9327718867376716
Mean absolute error: 0.22326156891858592
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 542.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09445189725870574
Kappa statistic: 0.8851295171090502
Training time: 98.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579860690104083
Weighted AreaUnderROC: 0.9443405076080313
Root mean squared error: 0.31197603107612976
Relative absolute error: 80.37416481069172
Root relative squared error: 83.71195354041798
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824953060894929
Weighted FMeasure: 0.9497211776520353
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9326222857343777
Mean absolute error: 0.22326156891858592
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 651.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09445189725870574
Kappa statistic: 0.8851295171090502
Training time: 106.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9579860690104083
Weighted AreaUnderROC: 0.9442968461569977
Root mean squared error: 0.31197603107612976
Relative absolute error: 80.37416481069172
Root relative squared error: 83.71195354041798
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8824953060894929
Weighted FMeasure: 0.9497211776520353
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9325028585885471
Mean absolute error: 0.22326156891858592
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 754.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.09445189725870574
Kappa statistic: 0.8851295171090502
Training time: 100.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9532511179532119
Weighted AreaUnderROC: 0.9286506332569318
Root mean squared error: 0.3121346333966608
Relative absolute error: 80.409799554567
Root relative squared error: 83.75451100882957
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8626857031173091
Weighted FMeasure: 0.9390476505086308
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9227829119310282
Mean absolute error: 0.22336055431823942
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 853.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.12922652623691275
Kappa statistic: 0.8700399957899168
Training time: 96.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9532511179532119
Weighted AreaUnderROC: 0.9286481308170529
Root mean squared error: 0.3121346333966608
Relative absolute error: 80.409799554567
Root relative squared error: 83.75451100882957
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8626857031173091
Weighted FMeasure: 0.9390476505086308
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9227824793025332
Mean absolute error: 0.22336055431823942
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 953.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.12922652623691275
Kappa statistic: 0.8700399957899168
Training time: 97.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9552882449971501
Weighted AreaUnderROC: 0.9323293501717079
Root mean squared error: 0.31205534231261134
Relative absolute error: 80.39198218262936
Root relative squared error: 83.73323497835625
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8698904002030199
Weighted FMeasure: 0.9430755842996659
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9263484839676905
Mean absolute error: 0.2233110616184127
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1055.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1221079033365943
Kappa statistic: 0.8764705882352942
Training time: 100.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9552882449971501
Weighted AreaUnderROC: 0.9323293501717079
Root mean squared error: 0.31205534231261134
Relative absolute error: 80.39198218262936
Root relative squared error: 83.73323497835625
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8698904002030199
Weighted FMeasure: 0.9430755842996659
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9263484839676905
Mean absolute error: 0.2233110616184127
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1156.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1221079033365943
Kappa statistic: 0.8764705882352942
Training time: 99.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9552882449971501
Weighted AreaUnderROC: 0.9322904136071589
Root mean squared error: 0.31205534231261134
Relative absolute error: 80.39198218262936
Root relative squared error: 83.73323497835625
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8698904002030199
Weighted FMeasure: 0.9430755842996659
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9261973868515302
Mean absolute error: 0.2233110616184127
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1265.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1221079033365943
Kappa statistic: 0.8764705882352942
Training time: 107.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9552882449971501
Weighted AreaUnderROC: 0.9323343550514658
Root mean squared error: 0.31205534231261134
Relative absolute error: 80.39198218262936
Root relative squared error: 83.73323497835625
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8698904002030199
Weighted FMeasure: 0.9430755842996659
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9263493676344037
Mean absolute error: 0.2233110616184127
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1385.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.1221079033365943
Kappa statistic: 0.8764705882352942
Training time: 118.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9532511179532119
Weighted AreaUnderROC: 0.9286556381366896
Root mean squared error: 0.3121346333966608
Relative absolute error: 80.409799554567
Root relative squared error: 83.75451100882957
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8626857031173091
Weighted FMeasure: 0.9390476505086308
Iteration time: 243.0
Weighted AreaUnderPRC: 0.9227837955977414
Mean absolute error: 0.22336055431823942
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1628.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.12922652623691275
Kappa statistic: 0.8700399957899168
Training time: 242.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9493646212607368
Weighted AreaUnderROC: 0.9261714479437748
Root mean squared error: 0.3122931551688596
Relative absolute error: 80.44543429844225
Root relative squared error: 83.7970468638556
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8507475714859383
Weighted FMeasure: 0.9347735118221256
Iteration time: 256.0
Weighted AreaUnderPRC: 0.918071522741977
Mean absolute error: 0.22345953971789292
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1884.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.12977218325250295
Kappa statistic: 0.8593101287307099
Training time: 248.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9493646212607368
Weighted AreaUnderROC: 0.9261299156082559
Root mean squared error: 0.3122931551688596
Relative absolute error: 80.44543429844225
Root relative squared error: 83.7970468638556
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8507475714859383
Weighted FMeasure: 0.9347735118221256
Iteration time: 262.0
Weighted AreaUnderPRC: 0.9179103524847391
Mean absolute error: 0.22345953971789292
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2146.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.12977218325250295
Kappa statistic: 0.8593101287307099
Training time: 261.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9474335444105572
Weighted AreaUnderROC: 0.9228930381407867
Root mean squared error: 0.3122931551688596
Relative absolute error: 80.44543429844224
Root relative squared error: 83.7970468638556
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8418854884290143
Weighted FMeasure: 0.928985754371675
Iteration time: 366.0
Weighted AreaUnderPRC: 0.9182950447929435
Mean absolute error: 0.2234595397178929
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2512.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.13638518817529333
Kappa statistic: 0.8530335960616932
Training time: 365.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9474335444105572
Weighted AreaUnderROC: 0.9229397620182456
Root mean squared error: 0.3122931551688596
Relative absolute error: 80.44543429844224
Root relative squared error: 83.7970468638556
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8418854884290143
Weighted FMeasure: 0.928985754371675
Iteration time: 250.0
Weighted AreaUnderPRC: 0.9184710284929422
Mean absolute error: 0.2234595397178929
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 2762.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.13638518817529333
Kappa statistic: 0.8530335960616932
Training time: 250.0
		
Time end:Tue Oct 31 11.08.21 EET 2017