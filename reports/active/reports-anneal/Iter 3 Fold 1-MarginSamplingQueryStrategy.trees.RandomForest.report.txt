Tue Oct 31 11.10.38 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.38 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 30.846325167037907
Incorrectly Classified Instances: 10.913140311804009
Correctly Classified Instances: 89.08685968819599
Weighted Precision: 0.8949190996573732
Weighted AreaUnderROC: 0.9705354329575463
Root mean squared error: 0.1557989026962332
Relative absolute error: 21.859403439222724
Root relative squared error: 41.80523246983853
Weighted TruePositiveRate: 0.89086859688196
Weighted MatthewsCorrelation: 0.7008882426228984
Weighted FMeasure: 0.8869798321426571
Iteration time: 6.0
Weighted AreaUnderPRC: 0.967626692346095
Mean absolute error: 0.0607205651089514
Coverage of cases: 99.33184855233853
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.89086859688196
Weighted FalsePositiveRate: 0.23017829908261309
Kappa statistic: 0.7118854927843693
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 26.985894580549356
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9736005437783588
Weighted AreaUnderROC: 0.9916388194238464
Root mean squared error: 0.11677132685920708
Relative absolute error: 14.590954593350503
Root relative squared error: 31.333034961604238
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9278254766261695
Weighted FMeasure: 0.9712121499907993
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9801730516879076
Mean absolute error: 0.040530429425973215
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05771483322405358
Kappa statistic: 0.9321290907716727
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 27.02301410541944
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.986900877303322
Weighted AreaUnderROC: 0.9943828220009879
Root mean squared error: 0.09525686698017626
Relative absolute error: 12.308478465373838
Root relative squared error: 25.560099586959634
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9639663851290184
Weighted FMeasure: 0.9847334782216901
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9916769875520445
Mean absolute error: 0.03419021795937143
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.028682153278868718
Kappa statistic: 0.9662997247935953
Training time: 6.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 26.577579806978445
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9850431828069208
Weighted AreaUnderROC: 0.9886215066159671
Root mean squared error: 0.10340045313061645
Relative absolute error: 13.12381861407531
Root relative squared error: 27.74525305252086
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9583291018710073
Weighted FMeasure: 0.9826288364906218
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9876956756223801
Mean absolute error: 0.03645505170576439
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02878598411766615
Kappa statistic: 0.9608407465550324
Training time: 6.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 27.802524127691147
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9825660605170627
Weighted AreaUnderROC: 0.9879070671448614
Root mean squared error: 0.09226481746630875
Relative absolute error: 12.022006238671121
Root relative squared error: 24.757248454353277
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9523093453285544
Weighted FMeasure: 0.9803033256324966
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9885594379912813
Mean absolute error: 0.033394461774086115
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.02924034162277972
Kappa statistic: 0.9553921812130545
Training time: 7.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 26.243504083147688
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9825492255168993
Weighted AreaUnderROC: 0.9959685218632306
Root mean squared error: 0.09614267881000951
Relative absolute error: 11.872111445467322
Root relative squared error: 25.797787842973314
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.951960908159634
Weighted FMeasure: 0.9802659978096698
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9931318564689334
Mean absolute error: 0.03297808734852001
Coverage of cases: 99.77728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.042919399079505656
Kappa statistic: 0.9547356217551287
Training time: 10.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 26.05790645879731
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9822910414489512
Weighted AreaUnderROC: 0.9896684185576935
Root mean squared error: 0.09799107993748973
Relative absolute error: 11.847379569644538
Root relative squared error: 26.29376591146128
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9520637179471689
Weighted FMeasure: 0.9802151721662131
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9879435325006171
Mean absolute error: 0.03290938769345672
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 56.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.03607987035114269
Kappa statistic: 0.9550494306094356
Training time: 9.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 24.387527839643656
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9892885624443282
Weighted AreaUnderROC: 0.9873715900408514
Root mean squared error: 0.0808818006747499
Relative absolute error: 8.578666151955916
Root relative squared error: 21.702864534159605
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9699329014164648
Weighted FMeasure: 0.9869738723444444
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9873562579849464
Mean absolute error: 0.02382962819987731
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.021563530378550258
Kappa statistic: 0.9720291046821659
Training time: 9.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 24.832962138084596
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9826068592115695
Weighted AreaUnderROC: 0.987991692994985
Root mean squared error: 0.09215109394215694
Relative absolute error: 9.925064508677973
Root relative squared error: 24.72673323067596
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9520034471849563
Weighted FMeasure: 0.9802764573275005
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9860090593864623
Mean absolute error: 0.02756962363521632
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 76.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.03607987035114269
Kappa statistic: 0.9550831561835688
Training time: 9.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 24.127691165553102
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9846915539999009
Weighted AreaUnderROC: 0.9929341049561118
Root mean squared error: 0.09093410009787886
Relative absolute error: 9.54381061393888
Root relative squared error: 24.40017951499541
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9580917916573093
Weighted FMeasure: 0.9825092324052433
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9885581936055662
Mean absolute error: 0.02651058503871885
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 87.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02896124745082422
Kappa statistic: 0.9608261042975372
Training time: 10.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.160356347438757
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9829264642399764
Weighted AreaUnderROC: 0.9899253679796631
Root mean squared error: 0.08057537149252206
Relative absolute error: 6.899422139740222
Root relative squared error: 21.620640956349465
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.952662924644126
Weighted FMeasure: 0.9803554079348081
Iteration time: 11.0
Weighted AreaUnderPRC: 0.988151899204174
Mean absolute error: 0.019165061499278203
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.022400812894416762
Kappa statistic: 0.9557297443861076
Training time: 10.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 22.9027468448404
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9802112381701009
Weighted AreaUnderROC: 0.9966291765677351
Root mean squared error: 0.07926724483883514
Relative absolute error: 7.685464264776981
Root relative squared error: 21.269633741850672
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9521854469360539
Weighted FMeasure: 0.9807903545528652
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9929927847203909
Mean absolute error: 0.021348511846602514
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.028981266969855275
Kappa statistic: 0.9554176492491001
Training time: 10.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 23.719376391982166
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.978018088677435
Weighted AreaUnderROC: 0.9972594436960217
Root mean squared error: 0.08664753998173326
Relative absolute error: 8.816614922033876
Root relative squared error: 23.24997473787449
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9401400206137772
Weighted FMeasure: 0.9758377786755654
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9926234495479221
Mean absolute error: 0.024490597005649414
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 121.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.04347758742341666
Kappa statistic: 0.9438328746559919
Training time: 11.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 23.08834446919082
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9846779539817209
Weighted AreaUnderROC: 0.9974923703236847
Root mean squared error: 0.07963074478898349
Relative absolute error: 7.923434565657053
Root relative squared error: 21.367171013652612
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9578076314436935
Weighted FMeasure: 0.9823658354275828
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9935280048497857
Mean absolute error: 0.022009540460158263
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 133.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.03615130284550332
Kappa statistic: 0.9604942306241988
Training time: 12.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 20.267260579064597
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9942754371665784
Root mean squared error: 0.07607339108410179
Relative absolute error: 5.123907063741367
Root relative squared error: 20.41263284915746
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9883772755033101
Mean absolute error: 0.01423307517705921
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 10.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 23.19970304380102
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9824594829903784
Weighted AreaUnderROC: 0.9962410026283247
Root mean squared error: 0.08180553262189269
Relative absolute error: 8.182363932619598
Root relative squared error: 21.950727825375548
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9520532028713978
Weighted FMeasure: 0.9802856284486279
Iteration time: 13.0
Weighted AreaUnderPRC: 0.99449245375221
Mean absolute error: 0.022728788701720883
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 157.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.03607987035114269
Kappa statistic: 0.9550662997247935
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 22.4201930215293
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.984602350404036
Weighted AreaUnderROC: 0.998019740589341
Root mean squared error: 0.08192308946367714
Relative absolute error: 7.427022621976882
Root relative squared error: 21.982271636109573
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9577876253860453
Weighted FMeasure: 0.9824164609485688
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9938811745324609
Mean absolute error: 0.02063061839438002
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 168.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.042815568240708216
Kappa statistic: 0.9602323050838881
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.75204157386786
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9802112381701009
Weighted AreaUnderROC: 0.9982507960511048
Root mean squared error: 0.07697019248512921
Relative absolute error: 6.818478846210885
Root relative squared error: 20.653269916559186
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9521854469360539
Weighted FMeasure: 0.9807903545528652
Iteration time: 12.0
Weighted AreaUnderPRC: 0.995701050469918
Mean absolute error: 0.01894021901725227
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 180.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.028981266969855275
Kappa statistic: 0.9554176492491001
Training time: 12.0
		
Time end:Tue Oct 31 11.10.39 EET 2017