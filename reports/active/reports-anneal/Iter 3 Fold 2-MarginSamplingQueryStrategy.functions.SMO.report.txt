Tue Oct 31 11.09.57 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.09.57 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.8891536135144559
Weighted AreaUnderROC: 0.8555726671444007
Root mean squared error: 0.3116782181475819
Relative absolute error: 78.44097995545687
Root relative squared error: 83.63204194608062
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.71512093805074
Weighted FMeasure: 0.8920597165126226
Iteration time: 54.0
Weighted AreaUnderPRC: 0.8509866508302321
Mean absolute error: 0.21789161098737803
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 54.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.22324875850581985
Kappa statistic: 0.7270769190110603
Training time: 51.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.9034314380273376
Weighted AreaUnderROC: 0.8742142336676935
Root mean squared error: 0.31184830840168865
Relative absolute error: 78.47916003818038
Root relative squared error: 83.67768195053976
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7234951248527001
Weighted FMeasure: 0.8942791763044471
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8660381509029199
Mean absolute error: 0.21799766677272112
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 110.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.16716908602224884
Kappa statistic: 0.7355647714166524
Training time: 53.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.9006845191513323
Weighted AreaUnderROC: 0.8708896090246667
Root mean squared error: 0.3113316758544592
Relative absolute error: 78.36461979000984
Root relative squared error: 83.5390548911405
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7427798923947402
Weighted FMeasure: 0.9023118328541507
Iteration time: 55.0
Weighted AreaUnderPRC: 0.8653942397381058
Mean absolute error: 0.21767949941669185
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 165.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.194774266904546
Kappa statistic: 0.7549795361527967
Training time: 52.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9105886068623573
Weighted AreaUnderROC: 0.8926086331970383
Root mean squared error: 0.310393468956676
Relative absolute error: 78.1546293350305
Root relative squared error: 83.28730756309221
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7761793964673551
Weighted FMeasure: 0.9137517363707398
Iteration time: 58.0
Weighted AreaUnderPRC: 0.8838152079197416
Mean absolute error: 0.2170961925973048
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 223.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.15233535801043033
Kappa statistic: 0.7899906454630496
Training time: 55.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9303239357560614
Weighted AreaUnderROC: 0.8801716055697385
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8199716792114883
Weighted FMeasure: 0.9265604296467538
Iteration time: 59.0
Weighted AreaUnderPRC: 0.8865099163535226
Mean absolute error: 0.2166719694559326
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 282.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.19942527204771443
Kappa statistic: 0.8202214613133043
Training time: 57.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.933449708059953
Weighted AreaUnderROC: 0.8813371067352396
Root mean squared error: 0.3096298471458049
Relative absolute error: 77.98281896277474
Root relative squared error: 83.08240632970708
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8268827432412421
Weighted FMeasure: 0.9287219377127995
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8891065719332679
Mean absolute error: 0.21661894156326103
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 342.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19932144120891698
Kappa statistic: 0.8256455773444651
Training time: 57.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.933449708059953
Weighted AreaUnderROC: 0.8813371067352396
Root mean squared error: 0.3096298471458049
Relative absolute error: 77.98281896277474
Root relative squared error: 83.08240632970708
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8268827432412421
Weighted FMeasure: 0.9287219377127995
Iteration time: 61.0
Weighted AreaUnderPRC: 0.8891065719332679
Mean absolute error: 0.21661894156326103
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 403.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19932144120891698
Kappa statistic: 0.8256455773444651
Training time: 59.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9315519963500801
Weighted AreaUnderROC: 0.8775831124623352
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.820657155651276
Weighted FMeasure: 0.9261076555198292
Iteration time: 62.0
Weighted AreaUnderPRC: 0.8854505669400754
Mean absolute error: 0.21667196945593256
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 465.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.20644006410923546
Kappa statistic: 0.8186086035885435
Training time: 59.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.933449708059953
Weighted AreaUnderROC: 0.8813872180938142
Root mean squared error: 0.3096298471458049
Relative absolute error: 77.98281896277474
Root relative squared error: 83.08240632970708
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8268827432412421
Weighted FMeasure: 0.9287219377127995
Iteration time: 64.0
Weighted AreaUnderPRC: 0.8891263063642116
Mean absolute error: 0.21661894156326103
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 529.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19932144120891698
Kappa statistic: 0.8256455773444651
Training time: 61.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9282094057149737
Weighted AreaUnderROC: 0.876417611296834
Root mean squared error: 0.30980106202025454
Relative absolute error: 78.02099904549826
Root relative squared error: 83.12834810147051
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8136589520973297
Weighted FMeasure: 0.9239621498608203
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8828653728054422
Mean absolute error: 0.21672499734860412
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 594.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.20654389494803288
Kappa statistic: 0.8131916900119268
Training time: 63.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9239049740163325
Weighted AreaUnderROC: 0.8689096227510251
Root mean squared error: 0.3099721823232218
Relative absolute error: 78.05917912822179
Root relative squared error: 83.17426449704233
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8008255383517202
Weighted FMeasure: 0.9184794407740825
Iteration time: 146.0
Weighted AreaUnderPRC: 0.8756121829476337
Mean absolute error: 0.21683105313394724
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 740.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.2207811407486698
Kappa statistic: 0.7989420950994935
Training time: 144.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9239049740163325
Weighted AreaUnderROC: 0.8689597341095997
Root mean squared error: 0.3099721823232218
Relative absolute error: 78.05917912822179
Root relative squared error: 83.17426449704233
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8008255383517202
Weighted FMeasure: 0.9184794407740825
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8756319173785774
Mean absolute error: 0.21683105313394724
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 879.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.2207811407486698
Kappa statistic: 0.7989420950994935
Training time: 138.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9239049740163325
Weighted AreaUnderROC: 0.8689096227510251
Root mean squared error: 0.3099721823232218
Relative absolute error: 78.05917912822179
Root relative squared error: 83.17426449704233
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8008255383517202
Weighted FMeasure: 0.9184794407740825
Iteration time: 137.0
Weighted AreaUnderPRC: 0.8756121829476337
Mean absolute error: 0.21683105313394724
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1016.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.2207811407486698
Kappa statistic: 0.7989420950994935
Training time: 135.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9315519963500801
Weighted AreaUnderROC: 0.8775831124623352
Root mean squared error: 0.30971546641426556
Relative absolute error: 78.00190900413651
Root relative squared error: 83.10538039024252
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.820657155651276
Weighted FMeasure: 0.9261076555198292
Iteration time: 143.0
Weighted AreaUnderPRC: 0.8854505669400754
Mean absolute error: 0.21667196945593256
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1159.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.20644006410923546
Kappa statistic: 0.8186086035885435
Training time: 141.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9239049740163325
Weighted AreaUnderROC: 0.8689597341095997
Root mean squared error: 0.3099721823232218
Relative absolute error: 78.05917912822179
Root relative squared error: 83.17426449704233
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8008255383517202
Weighted FMeasure: 0.9184794407740825
Iteration time: 143.0
Weighted AreaUnderPRC: 0.8756319173785774
Mean absolute error: 0.21683105313394724
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1302.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.2207811407486698
Kappa statistic: 0.7989420950994935
Training time: 142.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9471789161098737
Weighted AreaUnderROC: 0.9206247009101743
Root mean squared error: 0.30894403878722454
Relative absolute error: 77.83009863188062
Root relative squared error: 82.8983846365883
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8755010746215264
Weighted FMeasure: 0.9483535814754948
Iteration time: 147.0
Weighted AreaUnderPRC: 0.9214332137372037
Mean absolute error: 0.21619471842188845
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1449.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.13535766594484824
Kappa statistic: 0.8802315345834778
Training time: 146.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9421040480328035
Weighted AreaUnderROC: 0.9181247009101744
Root mean squared error: 0.30911563352167293
Relative absolute error: 77.86827871460413
Root relative squared error: 82.94442833548521
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8626469773718614
Weighted FMeasure: 0.9439448013484881
Iteration time: 155.0
Weighted AreaUnderPRC: 0.9161278157417637
Mean absolute error: 0.21630077420723154
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1604.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.13590332296043844
Kappa statistic: 0.8692763749933831
Training time: 154.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9397238318199361
Weighted AreaUnderROC: 0.9168747009101743
Root mean squared error: 0.3092013951782507
Relative absolute error: 77.8873687559659
Root relative squared error: 82.96744060276171
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8563909444164335
Weighted FMeasure: 0.941769879404726
Iteration time: 160.0
Weighted AreaUnderPRC: 0.9136139639129947
Mean absolute error: 0.2163538020999031
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 1764.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.13617615146823356
Kappa statistic: 0.8638622671605786
Training time: 160.0
		
Time end:Tue Oct 31 11.09.59 EET 2017