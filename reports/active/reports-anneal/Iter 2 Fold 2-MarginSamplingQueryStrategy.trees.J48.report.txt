Tue Oct 31 11.10.28 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.28 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.343726800296864
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.910750757904324
Weighted AreaUnderROC: 0.9421004516636471
Root mean squared error: 0.1933752108263425
Relative absolute error: 19.308541042828
Root relative squared error: 51.88801398852687
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.6978000278982975
Weighted FMeasure: 0.8675939182122184
Iteration time: 2.0
Weighted AreaUnderPRC: 0.910902742046262
Mean absolute error: 0.05363483623007725
Coverage of cases: 93.98663697104676
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.1598554635469267
Kappa statistic: 0.6503743635819108
Training time: 2.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 30.326651818856593
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.8613954241749342
Weighted AreaUnderROC: 0.7440603999362844
Root mean squared error: 0.2118656081330994
Relative absolute error: 26.600288222193313
Root relative squared error: 56.84950822559338
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.5795181671642411
Weighted FMeasure: 0.8320482134269793
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7738592048308894
Mean absolute error: 0.0738896895060918
Coverage of cases: 93.54120267260579
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.35074053088291335
Kappa statistic: 0.5902191769281533
Training time: 2.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 30.326651818856593
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.8613954241749342
Weighted AreaUnderROC: 0.7440603999362844
Root mean squared error: 0.21223733536063577
Relative absolute error: 24.84250715876569
Root relative squared error: 56.94925310757645
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.5795181671642411
Weighted FMeasure: 0.8320482134269793
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7738592048308894
Mean absolute error: 0.06900696432990401
Coverage of cases: 93.54120267260579
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.35074053088291335
Kappa statistic: 0.5902191769281533
Training time: 3.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 30.326651818856593
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.8613954241749342
Weighted AreaUnderROC: 0.7440603999362844
Root mean squared error: 0.21279561036158853
Relative absolute error: 23.647216035635065
Root relative squared error: 57.09905400984875
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.5795181671642411
Weighted FMeasure: 0.8320482134269793
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7738592048308894
Mean absolute error: 0.06568671121009674
Coverage of cases: 93.54120267260579
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.35074053088291335
Kappa statistic: 0.5902191769281533
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 31.55159613956938
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22185964646811912
Relative absolute error: 25.927912367619847
Root relative squared error: 59.53119011601455
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.0720219787989433
Coverage of cases: 91.98218262806236
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 31.55159613956938
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.2226432982834896
Relative absolute error: 25.127295520318143
Root relative squared error: 59.7414659635977
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06979804311199414
Coverage of cases: 91.98218262806236
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 4.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 31.55159613956938
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22332510639932404
Relative absolute error: 24.4979336136158
Root relative squared error: 59.924414278951716
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06804981559337654
Coverage of cases: 91.98218262806236
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 4.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 31.55159613956938
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22391697390911733
Relative absolute error: 23.990187534295476
Root relative squared error: 60.083228997220274
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06663940981748677
Coverage of cases: 91.98218262806236
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 5.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 31.55159613956938
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.2244323711782646
Relative absolute error: 23.571912129745257
Root relative squared error: 60.221524596728
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06547753369373617
Coverage of cases: 91.98218262806236
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 5.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22488352768539627
Relative absolute error: 23.221373632814476
Root relative squared error: 60.34258259094054
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06450381564670624
Coverage of cases: 83.96436525612472
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 45.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 6.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22528079931731912
Relative absolute error: 22.923350266510088
Root relative squared error: 60.44918175588195
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06367597296252739
Coverage of cases: 83.96436525612472
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 52.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 6.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22563273209978657
Relative absolute error: 22.666863257878745
Root relative squared error: 60.54361523089489
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.06296350904966255
Coverage of cases: 83.96436525612472
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 6.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.22594631893097206
Relative absolute error: 22.443794752000688
Root relative squared error: 60.62775940746043
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.0623438743111124
Coverage of cases: 83.96436525612472
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 66.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 7.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.2262272782070546
Relative absolute error: 22.24801291350833
Root relative squared error: 60.703148692287996
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.061800035870855856
Coverage of cases: 83.96436525612472
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 74.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 7.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 16.035634743875278
Correctly Classified Instances: 83.96436525612472
Weighted Precision: 0.8051115769247565
Weighted AreaUnderROC: 0.6640267656555195
Root mean squared error: 0.2264803008928543
Relative absolute error: 22.074799586657765
Root relative squared error: 60.771041803323726
Weighted TruePositiveRate: 0.8396436525612472
Weighted MatthewsCorrelation: 0.502373836712331
Weighted FMeasure: 0.7943596710612265
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7225509047505405
Mean absolute error: 0.0613188877407154
Coverage of cases: 83.96436525612472
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 82.0
Weighted Recall: 0.8396436525612472
Weighted FalsePositiveRate: 0.4917631879849161
Kappa statistic: 0.46649063454080364
Training time: 8.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.78470675575345
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9591477825072378
Weighted AreaUnderROC: 0.9519870088331951
Root mean squared error: 0.11829785148968099
Relative absolute error: 7.581702853388123
Root relative squared error: 31.742644502772333
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.895658870489916
Weighted FMeasure: 0.9588134477584703
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9396313172563558
Mean absolute error: 0.021060285703855687
Coverage of cases: 96.65924276169265
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 92.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.08666370184947209
Kappa statistic: 0.8968974843088229
Training time: 10.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 17.446176688938326
Incorrectly Classified Instances: 2.4498886414253898
Correctly Classified Instances: 97.55011135857461
Weighted Precision: 0.9747673807034745
Weighted AreaUnderROC: 0.9614361857615003
Root mean squared error: 0.08997782398434648
Relative absolute error: 4.1195683801474825
Root relative squared error: 24.143583707581282
Weighted TruePositiveRate: 0.9755011135857461
Weighted MatthewsCorrelation: 0.9346849739593155
Weighted FMeasure: 0.9749901671924104
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9576316638502576
Mean absolute error: 0.01144324550040956
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 102.0
Weighted Recall: 0.9755011135857461
Weighted FalsePositiveRate: 0.04367287027560578
Kappa statistic: 0.9385115283103431
Training time: 9.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 17.446176688938326
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9726376073814826
Weighted AreaUnderROC: 0.9673897600370931
Root mean squared error: 0.09373535813906134
Relative absolute error: 3.873782105511564
Root relative squared error: 25.151835923307644
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9289818699962081
Weighted FMeasure: 0.972823851687451
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9600108213800268
Mean absolute error: 0.010760505848643126
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 113.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.043951964447561276
Kappa statistic: 0.933164632331051
Training time: 10.0
		
Time end:Tue Oct 31 11.10.29 EET 2017