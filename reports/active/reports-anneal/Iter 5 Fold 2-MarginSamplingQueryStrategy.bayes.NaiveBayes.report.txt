Tue Oct 31 11.09.37 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.09.37 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.04454342984406
Incorrectly Classified Instances: 18.93095768374165
Correctly Classified Instances: 81.06904231625835
Weighted Precision: 0.8240113164389333
Weighted AreaUnderROC: 0.9104587472425957
Root mean squared error: 0.2244799595615666
Relative absolute error: 22.526814868337286
Root relative squared error: 60.23429389992831
Weighted TruePositiveRate: 0.8106904231625836
Weighted MatthewsCorrelation: 0.5352460602457214
Weighted FMeasure: 0.787588226803962
Iteration time: 18.0
Weighted AreaUnderPRC: 0.8472248544590922
Mean absolute error: 0.06257448574538073
Coverage of cases: 87.52783964365256
Instances selection time: 18.0
Test time: 28.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8106904231625836
Weighted FalsePositiveRate: 0.33834132612091394
Kappa statistic: 0.4672817620948606
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.193021529324454
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9382740492003019
Weighted AreaUnderROC: 0.9466247065679372
Root mean squared error: 0.14603169104705552
Relative absolute error: 11.652644707003365
Root relative squared error: 39.18441456605582
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.840434619955683
Weighted FMeasure: 0.930314060765399
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9291306360523289
Mean absolute error: 0.03236845751945347
Coverage of cases: 95.32293986636971
Instances selection time: 17.0
Test time: 25.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.05652862042916128
Kappa statistic: 0.8344493737883131
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 19.413511507052696
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9370018656695142
Weighted AreaUnderROC: 0.9498528881582757
Root mean squared error: 0.13600148294576575
Relative absolute error: 9.843797504921959
Root relative squared error: 36.493027308901446
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8521879752504008
Weighted FMeasure: 0.9372101094528307
Iteration time: 17.0
Weighted AreaUnderPRC: 0.93092905041411
Mean absolute error: 0.027343881958116278
Coverage of cases: 95.7683741648107
Instances selection time: 17.0
Test time: 25.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.06874077453174907
Kappa statistic: 0.8524105186267348
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 21.529324424647427
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9624000296445087
Weighted AreaUnderROC: 0.9714957994506538
Root mean squared error: 0.11771590507180135
Relative absolute error: 10.349937522085867
Root relative squared error: 31.586491892815367
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8972979319207135
Weighted FMeasure: 0.958340105971423
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9633462921694641
Mean absolute error: 0.028749826450238235
Coverage of cases: 97.7728285077951
Instances selection time: 15.0
Test time: 25.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.03222656619452386
Kappa statistic: 0.8977662200704647
Training time: 1.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.89829250185604
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9645679011277046
Weighted AreaUnderROC: 0.9704360615074312
Root mean squared error: 0.11202281800373617
Relative absolute error: 9.18633916973615
Root relative squared error: 30.058876330493113
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8997559422530241
Weighted FMeasure: 0.960810830351208
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9626749629436308
Mean absolute error: 0.025517608804822383
Coverage of cases: 97.7728285077951
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 83.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.03194747202256836
Kappa statistic: 0.9032003066162029
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 20.267260579064654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9660676778132693
Weighted AreaUnderROC: 0.9699758376245656
Root mean squared error: 0.10849378446175695
Relative absolute error: 8.167620287083704
Root relative squared error: 29.11193726312402
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9047583547902909
Weighted FMeasure: 0.9628651415452494
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9624749094356757
Mean absolute error: 0.02268783413078784
Coverage of cases: 97.7728285077951
Instances selection time: 13.0
Test time: 25.0
Accumulative iteration time: 97.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.03166837785061286
Kappa statistic: 0.9082572115384615
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.673348181143346
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9617058671345977
Weighted AreaUnderROC: 0.9691784377953775
Root mean squared error: 0.11485048287053339
Relative absolute error: 7.992299285753718
Root relative squared error: 30.81761843366269
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8899481594377859
Weighted FMeasure: 0.9567390078760376
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9612994517146463
Mean absolute error: 0.022200831349315664
Coverage of cases: 97.7728285077951
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 110.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.03250566036647936
Kappa statistic: 0.893191874018745
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.26503340757242
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9617058671345977
Weighted AreaUnderROC: 0.9686092498903497
Root mean squared error: 0.11771562443258914
Relative absolute error: 7.917329648795808
Root relative squared error: 31.586416589412686
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8899481594377859
Weighted FMeasure: 0.9567390078760376
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9601846532591154
Mean absolute error: 0.021992582357765914
Coverage of cases: 97.55011135857461
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 122.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.03250566036647936
Kappa statistic: 0.893191874018745
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.042316258351928
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9590132406689525
Weighted AreaUnderROC: 0.9689129660379864
Root mean squared error: 0.11883703446109306
Relative absolute error: 7.86655712118355
Root relative squared error: 31.88732247593886
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8803915983919195
Weighted FMeasure: 0.9527134395806207
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9603185495891764
Mean absolute error: 0.021851547558842978
Coverage of cases: 97.55011135857461
Instances selection time: 10.0
Test time: 25.0
Accumulative iteration time: 132.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.033063848710390364
Kappa statistic: 0.8833215213796362
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.78247958426136
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9590132406689525
Weighted AreaUnderROC: 0.9673392899801658
Root mean squared error: 0.12201859884315278
Relative absolute error: 7.930057059289865
Root relative squared error: 32.74102578390818
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8803915983919195
Weighted FMeasure: 0.9527134395806207
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9577691569462785
Mean absolute error: 0.022027936275804964
Coverage of cases: 97.32739420935413
Instances selection time: 9.0
Test time: 25.0
Accumulative iteration time: 142.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.033063848710390364
Kappa statistic: 0.8833215213796362
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 18.6711210096511
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9471840363808784
Weighted AreaUnderROC: 0.9661033732146302
Root mean squared error: 0.13071465519576395
Relative absolute error: 8.69958263986925
Root relative squared error: 35.07442256078108
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8477294244809578
Weighted FMeasure: 0.9381460107487857
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9545609706911593
Mean absolute error: 0.024165507332969897
Coverage of cases: 96.21380846325167
Instances selection time: 8.0
Test time: 26.0
Accumulative iteration time: 151.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.034141191248288516
Kappa statistic: 0.8500817463789809
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.63400148478102
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9447850627082838
Weighted AreaUnderROC: 0.9660470722480283
Root mean squared error: 0.13488384405563736
Relative absolute error: 9.071216836963456
Root relative squared error: 36.193133324986505
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8390089831639927
Weighted FMeasure: 0.9341676744167764
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9546267880993201
Mean absolute error: 0.025197824547120464
Coverage of cases: 95.99109131403118
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 159.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.03469937959219952
Kappa statistic: 0.8408129188682266
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.2628062360802
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9403642212395965
Weighted AreaUnderROC: 0.9590450674329911
Root mean squared error: 0.1492922825568105
Relative absolute error: 10.202586535905006
Root relative squared error: 40.05932307757633
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8221616829020104
Weighted FMeasure: 0.926319278510783
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9418683844413553
Mean absolute error: 0.028340518155291403
Coverage of cases: 95.99109131403118
Instances selection time: 6.0
Test time: 25.0
Accumulative iteration time: 166.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.035815756280021524
Kappa statistic: 0.822641807552536
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.928730512249448
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.9214763288168419
Weighted AreaUnderROC: 0.9269417369320172
Root mean squared error: 0.2130736876247288
Relative absolute error: 18.026878099549613
Root relative squared error: 57.173669969454195
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.7197637363768495
Weighted FMeasure: 0.8739087978590395
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9280041229536696
Mean absolute error: 0.050074661387637324
Coverage of cases: 87.75055679287306
Instances selection time: 5.0
Test time: 26.0
Accumulative iteration time: 172.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.038755418110786394
Kappa statistic: 0.7030906649304143
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.2628062360802
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.9214763288168419
Weighted AreaUnderROC: 0.9297491272431141
Root mean squared error: 0.21230348859586232
Relative absolute error: 18.000510550644417
Root relative squared error: 56.96700388328423
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.7197637363768495
Weighted FMeasure: 0.8739087978590395
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9273590559907778
Mean absolute error: 0.05000141819623399
Coverage of cases: 88.41870824053453
Instances selection time: 4.0
Test time: 25.0
Accumulative iteration time: 177.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.038755418110786394
Kappa statistic: 0.7030906649304143
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 18.41128433556053
Incorrectly Classified Instances: 14.25389755011136
Correctly Classified Instances: 85.74610244988864
Weighted Precision: 0.9217782915342352
Weighted AreaUnderROC: 0.9346465461550135
Root mean squared error: 0.21042285571232325
Relative absolute error: 18.045266096460974
Root relative squared error: 56.46237712708644
Weighted TruePositiveRate: 0.8574610244988864
Weighted MatthewsCorrelation: 0.7226696355207465
Weighted FMeasure: 0.8755277768652613
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9280422500222243
Mean absolute error: 0.05012573915683553
Coverage of cases: 88.64142538975501
Instances selection time: 4.0
Test time: 25.0
Accumulative iteration time: 182.0
Weighted Recall: 0.8574610244988864
Weighted FalsePositiveRate: 0.03865158727198896
Kappa statistic: 0.7066976953069181
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 18.52264291017078
Incorrectly Classified Instances: 14.031180400890868
Correctly Classified Instances: 85.96881959910912
Weighted Precision: 0.9228747573256054
Weighted AreaUnderROC: 0.9350379539796748
Root mean squared error: 0.20774297659743893
Relative absolute error: 17.774308602246734
Root relative squared error: 55.743290102402845
Weighted TruePositiveRate: 0.8596881959910914
Weighted MatthewsCorrelation: 0.7260922954854283
Weighted FMeasure: 0.8776042383773939
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9275151089476257
Mean absolute error: 0.04937307945068488
Coverage of cases: 88.64142538975501
Instances selection time: 2.0
Test time: 25.0
Accumulative iteration time: 186.0
Weighted Recall: 0.8596881959910914
Weighted FalsePositiveRate: 0.03837249310003347
Kappa statistic: 0.7104174771196332
Training time: 2.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 18.448403860430613
Incorrectly Classified Instances: 14.031180400890868
Correctly Classified Instances: 85.96881959910912
Weighted Precision: 0.9228747573256054
Weighted AreaUnderROC: 0.9320143195311659
Root mean squared error: 0.20880006995939002
Relative absolute error: 18.020432449246176
Root relative squared error: 56.02693801630922
Weighted TruePositiveRate: 0.8596881959910914
Weighted MatthewsCorrelation: 0.7260922954854283
Weighted FMeasure: 0.8776042383773939
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9249119459801945
Mean absolute error: 0.050056756803461104
Coverage of cases: 88.19599109131403
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 188.0
Weighted Recall: 0.8596881959910914
Weighted FalsePositiveRate: 0.03837249310003347
Kappa statistic: 0.7104174771196332
Training time: 1.0
		
Time end:Tue Oct 31 11.09.39 EET 2017