Tue Oct 31 11.12.33 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.12.33 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9284553483351421
Weighted AreaUnderROC: 0.8866015573231417
Root mean squared error: 0.31457225566418656
Relative absolute error: 80.96213808463379
Root relative squared error: 84.40859370006815
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8133378392131961
Weighted FMeasure: 0.922083592354471
Iteration time: 87.0
Weighted AreaUnderPRC: 0.8932285826834975
Mean absolute error: 0.2248948280128694
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 87.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.18634444197296163
Kappa statistic: 0.7987230863275147
Training time: 82.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9261850153448971
Weighted AreaUnderROC: 0.8834962653861343
Root mean squared error: 0.31457225566418656
Relative absolute error: 80.96213808463378
Root relative squared error: 84.40859370006815
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8067862983751686
Weighted FMeasure: 0.9191659730356797
Iteration time: 83.0
Weighted AreaUnderPRC: 0.8893913685977939
Mean absolute error: 0.22489482801286936
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 170.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.19346306487328008
Kappa statistic: 0.791712714720369
Training time: 78.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9261850153448971
Weighted AreaUnderROC: 0.8834823106775366
Root mean squared error: 0.31457225566418656
Relative absolute error: 80.96213808463378
Root relative squared error: 84.40859370006815
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8067862983751686
Weighted FMeasure: 0.9191659730356797
Iteration time: 84.0
Weighted AreaUnderPRC: 0.88938840570429
Mean absolute error: 0.22489482801286936
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 254.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.19346306487328008
Kappa statistic: 0.791712714720369
Training time: 79.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9261850153448971
Weighted AreaUnderROC: 0.8834962653861343
Root mean squared error: 0.31457225566418656
Relative absolute error: 80.96213808463378
Root relative squared error: 84.40859370006815
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8067862983751686
Weighted FMeasure: 0.9191659730356797
Iteration time: 86.0
Weighted AreaUnderPRC: 0.8893913685977939
Mean absolute error: 0.22489482801286936
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 340.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.19346306487328008
Kappa statistic: 0.791712714720369
Training time: 81.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9261850153448971
Weighted AreaUnderROC: 0.8834962653861343
Root mean squared error: 0.31457225566418656
Relative absolute error: 80.96213808463378
Root relative squared error: 84.40859370006815
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8067862983751686
Weighted FMeasure: 0.9191659730356797
Iteration time: 88.0
Weighted AreaUnderPRC: 0.8893913685977939
Mean absolute error: 0.22489482801286936
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 428.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.19346306487328008
Kappa statistic: 0.791712714720369
Training time: 85.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9261850153448971
Weighted AreaUnderROC: 0.8817920172546179
Root mean squared error: 0.3147295496665477
Relative absolute error: 80.99777282850904
Root relative squared error: 84.45080010987606
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.8067862983751686
Weighted FMeasure: 0.9191659730356797
Iteration time: 86.0
Weighted AreaUnderPRC: 0.8893337751434479
Mean absolute error: 0.2249938134125229
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 514.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.19346306487328008
Kappa statistic: 0.791712714720369
Training time: 83.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9250214417131237
Weighted AreaUnderROC: 0.8754528389680343
Root mean squared error: 0.31472954966654776
Relative absolute error: 80.99777282850903
Root relative squared error: 84.45080010987606
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7949802104281658
Weighted FMeasure: 0.9145162882396268
Iteration time: 90.0
Weighted AreaUnderPRC: 0.8832581260277215
Mean absolute error: 0.22499381341252284
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 604.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2076164993541506
Kappa statistic: 0.7777380543141973
Training time: 87.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9250214417131237
Weighted AreaUnderROC: 0.8754787966777337
Root mean squared error: 0.31472954966654776
Relative absolute error: 80.99777282850903
Root relative squared error: 84.45080010987606
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7949802104281658
Weighted FMeasure: 0.9145162882396268
Iteration time: 91.0
Weighted AreaUnderPRC: 0.8832703077792073
Mean absolute error: 0.22499381341252284
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 695.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2076164993541506
Kappa statistic: 0.7777380543141973
Training time: 89.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9250214417131237
Weighted AreaUnderROC: 0.8754787966777337
Root mean squared error: 0.31472954966654776
Relative absolute error: 80.99777282850903
Root relative squared error: 84.45080010987606
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7949802104281658
Weighted FMeasure: 0.9145162882396268
Iteration time: 92.0
Weighted AreaUnderPRC: 0.8832703077792073
Mean absolute error: 0.22499381341252284
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 787.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2076164993541506
Kappa statistic: 0.7777380543141973
Training time: 89.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9250214417131237
Weighted AreaUnderROC: 0.8754787966777337
Root mean squared error: 0.31472954966654776
Relative absolute error: 80.99777282850903
Root relative squared error: 84.45080010987606
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7949802104281658
Weighted FMeasure: 0.9145162882396268
Iteration time: 90.0
Weighted AreaUnderPRC: 0.8832703077792073
Mean absolute error: 0.22499381341252284
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 877.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2076164993541506
Kappa statistic: 0.7777380543141973
Training time: 88.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9293248248578753
Weighted AreaUnderROC: 0.8788044064557251
Root mean squared error: 0.3144935791615269
Relative absolute error: 80.94432071269611
Root relative squared error: 84.38748257908668
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8137599582185642
Weighted FMeasure: 0.9202952529807825
Iteration time: 93.0
Weighted AreaUnderPRC: 0.888628052583409
Mean absolute error: 0.22484533531304252
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 970.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.20755644079705746
Kappa statistic: 0.7933789795149976
Training time: 91.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9293248248578753
Weighted AreaUnderROC: 0.8788295249312013
Root mean squared error: 0.3144935791615269
Relative absolute error: 80.94432071269611
Root relative squared error: 84.38748257908668
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8137599582185642
Weighted FMeasure: 0.9202952529807825
Iteration time: 96.0
Weighted AreaUnderPRC: 0.8886492055418618
Mean absolute error: 0.22484533531304252
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1066.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.20755644079705746
Kappa statistic: 0.7933789795149976
Training time: 94.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9293248248578753
Weighted AreaUnderROC: 0.8788295249312013
Root mean squared error: 0.3144935791615269
Relative absolute error: 80.94432071269611
Root relative squared error: 84.38748257908668
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8137599582185642
Weighted FMeasure: 0.9202952529807825
Iteration time: 95.0
Weighted AreaUnderPRC: 0.8886492055418618
Mean absolute error: 0.22484533531304252
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 1161.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.20755644079705746
Kappa statistic: 0.7933789795149976
Training time: 93.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9293248248578753
Weighted AreaUnderROC: 0.8788044064557251
Root mean squared error: 0.3144935791615269
Relative absolute error: 80.94432071269611
Root relative squared error: 84.38748257908668
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8137599582185642
Weighted FMeasure: 0.9202952529807825
Iteration time: 95.0
Weighted AreaUnderPRC: 0.888628052583409
Mean absolute error: 0.22484533531304252
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 1256.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.20755644079705746
Kappa statistic: 0.7933789795149976
Training time: 94.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9298551037845907
Weighted AreaUnderROC: 0.8857560640857908
Root mean squared error: 0.3137057288854634
Relative absolute error: 80.7661469933197
Root relative squared error: 84.17608016628226
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8133266956762263
Weighted FMeasure: 0.9189580278612393
Iteration time: 233.0
Weighted AreaUnderPRC: 0.8899765221927167
Mean absolute error: 0.22435040831477468
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1489.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.20755644079705746
Kappa statistic: 0.7932059566509888
Training time: 232.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9539636310237646
Weighted AreaUnderROC: 0.914439352227848
Root mean squared error: 0.312847348820649
Relative absolute error: 80.57015590200575
Root relative squared error: 83.9457526252276
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8670308928259347
Weighted FMeasure: 0.9452891996712297
Iteration time: 245.0
Weighted AreaUnderPRC: 0.9164272628035732
Mean absolute error: 0.2238059886166804
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1734.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.15660970380700623
Kappa statistic: 0.8660846223716497
Training time: 244.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9636790128077957
Weighted AreaUnderROC: 0.9375585843822528
Root mean squared error: 0.31245159651532417
Relative absolute error: 80.48106904231763
Root relative squared error: 83.83956113839257
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8972593689525682
Weighted FMeasure: 0.9574526258333793
Iteration time: 243.0
Weighted AreaUnderPRC: 0.9365577301223547
Mean absolute error: 0.22355852511754673
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1977.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.11417706057705093
Kappa statistic: 0.8993499215422551
Training time: 242.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9816383934780705
Weighted AreaUnderROC: 0.9741852213190938
Root mean squared error: 0.31094314410880025
Relative absolute error: 80.14253897550252
Root relative squared error: 83.43480088377528
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9522521737257544
Weighted FMeasure: 0.98113186260357
Iteration time: 256.0
Weighted AreaUnderPRC: 0.9699519110022305
Mean absolute error: 0.22261816382083813
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2233.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.043094662412663715
Kappa statistic: 0.9547116524194972
Training time: 255.0
		
Time end:Tue Oct 31 11.12.36 EET 2017