Tue Oct 31 11.06.38 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.06.38 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 20.267260579064562
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9554920370765934
Weighted AreaUnderROC: 0.9766543215650879
Root mean squared error: 0.11605763976625251
Relative absolute error: 8.774558748907621
Root relative squared error: 31.141532619062964
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8833094123885586
Weighted FMeasure: 0.9505359645094003
Iteration time: 43.0
Weighted AreaUnderPRC: 0.9570614589103557
Mean absolute error: 0.024373774302520926
Coverage of cases: 98.21826280623608
Instances selection time: 42.0
Test time: 27.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.05375379025015808
Kappa statistic: 0.8794851460989446
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.489977728285066
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9430666625902295
Weighted AreaUnderROC: 0.9719635520904585
Root mean squared error: 0.12956117167206657
Relative absolute error: 10.029075116282046
Root relative squared error: 34.76491045239148
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8487299862571782
Weighted FMeasure: 0.9376857309634025
Iteration time: 47.0
Weighted AreaUnderPRC: 0.9460790942768349
Mean absolute error: 0.02785854198967207
Coverage of cases: 97.99554565701558
Instances selection time: 46.0
Test time: 55.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.07592814447449878
Kappa statistic: 0.8466174586713842
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 19.821826280623572
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.938924871094775
Weighted AreaUnderROC: 0.9674447459224949
Root mean squared error: 0.14068371386807121
Relative absolute error: 10.838691683839839
Root relative squared error: 37.74940170433664
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8372035108800512
Weighted FMeasure: 0.9334023197814033
Iteration time: 62.0
Weighted AreaUnderPRC: 0.939119384350804
Mean absolute error: 0.03010747689955481
Coverage of cases: 96.65924276169265
Instances selection time: 61.0
Test time: 53.0
Accumulative iteration time: 152.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.08331959588261235
Kappa statistic: 0.8356615628621972
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 19.858945805493665
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.938924871094775
Weighted AreaUnderROC: 0.9694925007563374
Root mean squared error: 0.13440762881957347
Relative absolute error: 10.439395668022408
Root relative squared error: 36.06535136821531
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8372035108800512
Weighted FMeasure: 0.9334023197814033
Iteration time: 62.0
Weighted AreaUnderPRC: 0.9451542911065197
Mean absolute error: 0.028998321300061955
Coverage of cases: 97.99554565701558
Instances selection time: 58.0
Test time: 53.0
Accumulative iteration time: 214.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.08331959588261235
Kappa statistic: 0.8356615628621972
Training time: 4.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 19.784706755753497
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9378161333914271
Weighted AreaUnderROC: 0.9733514458097854
Root mean squared error: 0.13309245143548876
Relative absolute error: 10.123769988388755
Root relative squared error: 35.712452244221254
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8391681937682107
Weighted FMeasure: 0.9344573327017975
Iteration time: 58.0
Weighted AreaUnderPRC: 0.9488457836595353
Mean absolute error: 0.028121583301079597
Coverage of cases: 98.21826280623608
Instances selection time: 55.0
Test time: 52.0
Accumulative iteration time: 272.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.09673835615986398
Kappa statistic: 0.838248447204969
Training time: 3.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 19.74758723088342
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9323447951391891
Weighted AreaUnderROC: 0.9699687582731682
Root mean squared error: 0.1392402278255157
Relative absolute error: 10.863702994863136
Root relative squared error: 37.36207375444948
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.825959513086794
Weighted FMeasure: 0.9295820819333388
Iteration time: 51.0
Weighted AreaUnderPRC: 0.9452066948418575
Mean absolute error: 0.03017695276350841
Coverage of cases: 98.21826280623608
Instances selection time: 50.0
Test time: 55.0
Accumulative iteration time: 323.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.1109756019605009
Kappa statistic: 0.8258252621568186
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.71046770601334
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9292436242516194
Weighted AreaUnderROC: 0.9698076749157815
Root mean squared error: 0.13977748613130447
Relative absolute error: 10.808775461695644
Root relative squared error: 37.506235285635896
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8156447653448823
Weighted FMeasure: 0.9257233268500136
Iteration time: 50.0
Weighted AreaUnderPRC: 0.9450433952782406
Mean absolute error: 0.0300243762824876
Coverage of cases: 98.21826280623608
Instances selection time: 46.0
Test time: 54.0
Accumulative iteration time: 373.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.11152125897609111
Kappa statistic: 0.8159378881987577
Training time: 4.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.524870081662918
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9236366866303378
Weighted AreaUnderROC: 0.9704811967895197
Root mean squared error: 0.14229484950536309
Relative absolute error: 10.879090554434153
Root relative squared error: 38.18171476105149
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8022324010160642
Weighted FMeasure: 0.9207754692469023
Iteration time: 42.0
Weighted AreaUnderPRC: 0.9448659315135363
Mean absolute error: 0.030219695984539014
Coverage of cases: 98.21826280623608
Instances selection time: 36.0
Test time: 27.0
Accumulative iteration time: 415.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.12575850477672804
Kappa statistic: 0.8033511024351179
Training time: 6.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.116555308092
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9135380451979622
Weighted AreaUnderROC: 0.9669757040383834
Root mean squared error: 0.1493964833147443
Relative absolute error: 11.424488698572484
Root relative squared error: 40.08728307494196
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7767333217575758
Weighted FMeasure: 0.9112832731054102
Iteration time: 21.0
Weighted AreaUnderPRC: 0.9402848414299454
Mean absolute error: 0.031734690829367695
Coverage of cases: 98.21826280623608
Instances selection time: 20.0
Test time: 25.0
Accumulative iteration time: 436.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.14738720198547853
Kappa statistic: 0.7792582694634933
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 19.116555308092003
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9063741015493447
Weighted AreaUnderROC: 0.9677151228182397
Root mean squared error: 0.1475189674150604
Relative absolute error: 11.3129269078511
Root relative squared error: 39.58349269327837
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7612517068118692
Weighted FMeasure: 0.9052843674887112
Iteration time: 19.0
Weighted AreaUnderPRC: 0.941874779352973
Mean absolute error: 0.03142479696625274
Coverage of cases: 98.44097995545657
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 455.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.16847024217863882
Kappa statistic: 0.7644582629612569
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 19.042316258351832
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.8894973849476041
Weighted AreaUnderROC: 0.9658172654169896
Root mean squared error: 0.15394766289771497
Relative absolute error: 11.86151516824268
Root relative squared error: 41.308492705981756
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.720730420856353
Weighted FMeasure: 0.8897222566833195
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9389860677313685
Mean absolute error: 0.032948653245118226
Coverage of cases: 98.21826280623608
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 481.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.20433618518802624
Kappa statistic: 0.7258781045412033
Training time: 8.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 19.005196733481753
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.88632369766601
Weighted AreaUnderROC: 0.9659812641198336
Root mean squared error: 0.1566087572722485
Relative absolute error: 12.07954771267707
Root relative squared error: 42.02253925590166
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7135351731302753
Weighted FMeasure: 0.8868728579728439
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9383151834370758
Mean absolute error: 0.03355429920188041
Coverage of cases: 98.21826280623608
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 496.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.2114548080883447
Kappa statistic: 0.718976151047711
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 19.042316258351836
Incorrectly Classified Instances: 11.135857461024498
Correctly Classified Instances: 88.8641425389755
Weighted Precision: 0.8798204078255378
Weighted AreaUnderROC: 0.9661656630802249
Root mean squared error: 0.15634828000918255
Relative absolute error: 12.069120970947328
Root relative squared error: 41.95264587188464
Weighted TruePositiveRate: 0.888641425389755
Weighted MatthewsCorrelation: 0.6989900438403114
Weighted FMeasure: 0.8810569397309932
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9381750068757649
Mean absolute error: 0.033525336030408914
Coverage of cases: 98.44097995545657
Instances selection time: 22.0
Test time: 49.0
Accumulative iteration time: 520.0
Weighted Recall: 0.888641425389755
Weighted FalsePositiveRate: 0.22569205388898161
Kappa statistic: 0.7050128112476184
Training time: 2.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.96807720861167
Incorrectly Classified Instances: 11.804008908685969
Correctly Classified Instances: 88.19599109131403
Weighted Precision: 0.8714032663565389
Weighted AreaUnderROC: 0.9644965067630917
Root mean squared error: 0.16040646289786548
Relative absolute error: 12.484664488634278
Root relative squared error: 43.041570608391226
Weighted TruePositiveRate: 0.8819599109131403
Weighted MatthewsCorrelation: 0.6788948476708773
Weighted FMeasure: 0.8733732626724806
Iteration time: 26.0
Weighted AreaUnderPRC: 0.9361226111507724
Mean absolute error: 0.03467962357953932
Coverage of cases: 98.21826280623608
Instances selection time: 22.0
Test time: 49.0
Accumulative iteration time: 546.0
Weighted Recall: 0.8819599109131403
Weighted FalsePositiveRate: 0.24020212819741366
Kappa statistic: 0.6861051020946551
Training time: 4.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 18.93095768374159
Incorrectly Classified Instances: 11.58129175946548
Correctly Classified Instances: 88.41870824053453
Weighted Precision: 0.8730735913491317
Weighted AreaUnderROC: 0.9654637216684716
Root mean squared error: 0.15862693142331172
Relative absolute error: 12.298953456459119
Root relative squared error: 42.564072206966905
Weighted TruePositiveRate: 0.8841870824053452
Weighted MatthewsCorrelation: 0.684220273045031
Weighted FMeasure: 0.8750749016716315
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9382065118705171
Mean absolute error: 0.03416375960127499
Coverage of cases: 98.44097995545657
Instances selection time: 15.0
Test time: 46.0
Accumulative iteration time: 566.0
Weighted Recall: 0.8841870824053452
Weighted FalsePositiveRate: 0.23992929968961854
Kappa statistic: 0.6908327705610507
Training time: 5.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.227913882702254
Incorrectly Classified Instances: 12.026726057906458
Correctly Classified Instances: 87.97327394209354
Weighted Precision: 0.8698094640505364
Weighted AreaUnderROC: 0.9640582827030292
Root mean squared error: 0.16129422375830735
Relative absolute error: 12.768955178647323
Root relative squared error: 43.279781844196634
Weighted TruePositiveRate: 0.8797327394209354
Weighted MatthewsCorrelation: 0.6736589683360655
Weighted FMeasure: 0.8716806477303635
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9365513775516116
Mean absolute error: 0.03546931994068665
Coverage of cases: 98.44097995545657
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 577.0
Weighted Recall: 0.8797327394209354
Weighted FalsePositiveRate: 0.24047495670520877
Kappa statistic: 0.681413836147428
Training time: 4.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 19.302152932442414
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9376458202773172
Weighted AreaUnderROC: 0.9700773907281098
Root mean squared error: 0.13234729007052903
Relative absolute error: 9.853456683362985
Root relative squared error: 35.51250446827048
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8339713158901335
Weighted FMeasure: 0.9362112652409877
Iteration time: 5.0
Weighted AreaUnderPRC: 0.953694012073183
Mean absolute error: 0.027370713009341352
Coverage of cases: 97.7728285077951
Instances selection time: 4.0
Test time: 25.0
Accumulative iteration time: 582.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.09673835615986398
Kappa statistic: 0.8395954469301272
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.079435783221992
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9621468486084193
Weighted AreaUnderROC: 0.9736446015702618
Root mean squared error: 0.12116435378615394
Relative absolute error: 8.314893174359408
Root relative squared error: 32.51180778188107
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8895669769198364
Weighted FMeasure: 0.9553325446792119
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9597960021105834
Mean absolute error: 0.02309692548433146
Coverage of cases: 97.32739420935413
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 586.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.02609778417226953
Kappa statistic: 0.8886671704529355
Training time: 1.0
		
Time end:Tue Oct 31 11.06.41 EET 2017