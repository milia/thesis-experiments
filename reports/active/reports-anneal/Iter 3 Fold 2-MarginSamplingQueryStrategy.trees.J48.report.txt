Tue Oct 31 11.10.30 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.30 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.00965107646632
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9395276767498324
Weighted AreaUnderROC: 0.9201972451761964
Root mean squared error: 0.13105895634268297
Relative absolute error: 8.31652037206873
Root relative squared error: 35.16680825309013
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8568032380732746
Weighted FMeasure: 0.9435910769760135
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9196046650271119
Mean absolute error: 0.02310144547796846
Coverage of cases: 96.21380846325167
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12265356035218448
Kappa statistic: 0.8662357680401022
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9395276767498324
Weighted AreaUnderROC: 0.9213212382657202
Root mean squared error: 0.12968198560370142
Relative absolute error: 7.2542157174674555
Root relative squared error: 34.79732823204323
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8568032380732746
Weighted FMeasure: 0.9435910769760135
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9196046650271119
Mean absolute error: 0.020150599215187175
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12265356035218448
Kappa statistic: 0.8662357680401022
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9395276767498324
Weighted AreaUnderROC: 0.9213212382657202
Root mean squared error: 0.1300054552819608
Relative absolute error: 6.7141072301592315
Root relative squared error: 34.88412425475299
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8568032380732746
Weighted FMeasure: 0.9435910769760135
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9196046650271119
Mean absolute error: 0.018650297861553235
Coverage of cases: 94.87750556792874
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12265356035218448
Kappa statistic: 0.8662357680401022
Training time: 2.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9395276767498324
Weighted AreaUnderROC: 0.9213212382657202
Root mean squared error: 0.13018713171733215
Relative absolute error: 6.528168242725238
Root relative squared error: 34.932873157881005
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8568032380732746
Weighted FMeasure: 0.9435910769760135
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9196046650271119
Mean absolute error: 0.018133800674236592
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12265356035218448
Kappa statistic: 0.8662357680401022
Training time: 2.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9395276767498324
Weighted AreaUnderROC: 0.9213212382657202
Root mean squared error: 0.13029274620732284
Relative absolute error: 6.434050977480882
Root relative squared error: 34.961212499364386
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8568032380732746
Weighted FMeasure: 0.9435910769760135
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9196046650271119
Mean absolute error: 0.017872363826335606
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12265356035218448
Kappa statistic: 0.8662357680401022
Training time: 2.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 18.299925760950206
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9448201090689591
Root mean squared error: 0.1098017380454541
Relative absolute error: 5.398663697104726
Root relative squared error: 29.462898038071373
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 8.0
Weighted AreaUnderPRC: 0.941441344388897
Mean absolute error: 0.014996288047512978
Coverage of cases: 97.55011135857461
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 38.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 4.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 3.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 59.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 4.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.953849669133457
Weighted AreaUnderROC: 0.9410981308411215
Root mean squared error: 0.1123416149778334
Relative absolute error: 4.543429844098039
Root relative squared error: 30.14441853510543
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8946812349699343
Weighted FMeasure: 0.9578276337996161
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9383279235883897
Mean absolute error: 0.012620638455827763
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 64.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07994182295027372
Kappa statistic: 0.903332024670407
Training time: 4.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.778025241276893
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9557777688928182
Weighted AreaUnderROC: 0.9740146327248506
Root mean squared error: 0.11011680824270202
Relative absolute error: 4.54342984409804
Root relative squared error: 29.547440243519052
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.9064705031081642
Weighted FMeasure: 0.9568974002031296
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9537355685449314
Mean absolute error: 0.012620638455827764
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.03913988510292862
Kappa statistic: 0.9009534547415379
Training time: 4.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9636615042468526
Weighted AreaUnderROC: 0.9744042056074766
Root mean squared error: 0.1055265722982818
Relative absolute error: 4.0089086859688585
Root relative squared error: 28.315750690980657
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9253606520955198
Weighted FMeasure: 0.9635896684736031
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9548278445583309
Mean absolute error: 0.011135857461024497
Coverage of cases: 96.65924276169265
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.017784016401973232
Kappa statistic: 0.9183408707882198
Training time: 8.0
		
Time end:Tue Oct 31 11.10.31 EET 2017