Tue Oct 31 11.10.29 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.29 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 18.856718634001414
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9558649813438233
Weighted AreaUnderROC: 0.9789344753318913
Root mean squared error: 0.09654597419904531
Relative absolute error: 5.879732739420987
Root relative squared error: 25.906003351560862
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9000770931386607
Weighted FMeasure: 0.9595121506917188
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9713996615702016
Mean absolute error: 0.016332590942835915
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.08618932482532743
Kappa statistic: 0.9081000869876681
Training time: 1.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 17.07498144023757
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9781573014944243
Weighted AreaUnderROC: 0.9796964325493156
Root mean squared error: 0.07825626968610129
Relative absolute error: 2.614819719496806
Root relative squared error: 20.998360642041536
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9550353984256039
Weighted FMeasure: 0.9800611249258727
Iteration time: 4.0
Weighted AreaUnderPRC: 0.97597846303762
Mean absolute error: 0.0072633881097132775
Coverage of cases: 98.21826280623608
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.021882663588567868
Kappa statistic: 0.9555951144736191
Training time: 3.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 18.5226429101707
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9742971709817154
Weighted AreaUnderROC: 0.9836394122580705
Root mean squared error: 0.07893904073876173
Relative absolute error: 3.672229774679699
Root relative squared error: 21.18156734045968
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9529108774708785
Weighted FMeasure: 0.9779936181625305
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9737953478309803
Mean absolute error: 0.010200638262999063
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.02222554956125869
Kappa statistic: 0.9555610540640851
Training time: 3.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9849664668257959
Weighted AreaUnderROC: 0.9816049892204259
Root mean squared error: 0.0716298878549524
Relative absolute error: 2.281994175089965
Root relative squared error: 19.220315815723318
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9583301872153854
Weighted FMeasure: 0.9825436579951105
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9775259947730931
Mean absolute error: 0.006338872708583173
Coverage of cases: 98.44097995545657
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02212171872246126
Kappa statistic: 0.9611236177423745
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 17.07498144023756
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9705287389029037
Weighted AreaUnderROC: 0.9811381621763948
Root mean squared error: 0.08630572702244242
Relative absolute error: 3.6596132929069527
Root relative squared error: 23.158256696366728
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.915250163671132
Weighted FMeasure: 0.9654836625883396
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9768988424818243
Mean absolute error: 0.010165592480296989
Coverage of cases: 98.44097995545657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.02435447209810527
Kappa statistic: 0.9190329638623742
Training time: 3.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.740905716406814
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9849664668257959
Weighted AreaUnderROC: 0.9815238740733818
Root mean squared error: 0.07302616040037849
Relative absolute error: 2.3008394723316994
Root relative squared error: 19.594975054926042
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9583301872153854
Weighted FMeasure: 0.9825436579951105
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9773595994907945
Mean absolute error: 0.006391220756476879
Coverage of cases: 98.44097995545657
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.02212171872246126
Kappa statistic: 0.9611236177423745
Training time: 4.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 5.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 5.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 6.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 5.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 51.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 5.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 6.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 5.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 71.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 7.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 80.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 8.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 100.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 14.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 120.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 18.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9870855813473395
Weighted AreaUnderROC: 0.9822925781425678
Root mean squared error: 0.06661038362843252
Relative absolute error: 1.9209354120267474
Root relative squared error: 17.873441496061794
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9640858698694522
Weighted FMeasure: 0.9847495980905757
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9796961955535289
Mean absolute error: 0.0053359317000742455
Coverage of cases: 98.66369710467706
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 140.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.02184262455050576
Kappa statistic: 0.9665565955756388
Training time: 14.0
		
Time end:Tue Oct 31 11.10.30 EET 2017