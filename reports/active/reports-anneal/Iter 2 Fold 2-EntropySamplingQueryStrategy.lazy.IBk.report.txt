Tue Oct 31 11.06.01 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.06.01 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.33333333333331
Incorrectly Classified Instances: 11.804008908685969
Correctly Classified Instances: 88.19599109131403
Weighted Precision: 0.8952753359935923
Weighted AreaUnderROC: 0.839998427286268
Root mean squared error: 0.19343127584672906
Relative absolute error: 19.585980541554566
Root relative squared error: 51.90305781213618
Weighted TruePositiveRate: 0.8819599109131403
Weighted MatthewsCorrelation: 0.7030973560717216
Weighted FMeasure: 0.8848644943456664
Iteration time: 15.0
Weighted AreaUnderPRC: 0.840444525212811
Mean absolute error: 0.054405501504317694
Coverage of cases: 91.0913140311804
Instances selection time: 15.0
Test time: 16.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8819599109131403
Weighted FalsePositiveRate: 0.20196305634060469
Kappa statistic: 0.6978580769670267
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 12.47216035634744
Correctly Classified Instances: 87.52783964365256
Weighted Precision: 0.8720492631803802
Weighted AreaUnderROC: 0.7977110391519828
Root mean squared error: 0.1994564321394988
Relative absolute error: 19.403118040089222
Root relative squared error: 53.51977689761944
Weighted TruePositiveRate: 0.8752783964365256
Weighted MatthewsCorrelation: 0.6629668878321109
Weighted FMeasure: 0.8653952243608292
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8065787273729635
Mean absolute error: 0.05389755011135842
Coverage of cases: 87.52783964365256
Instances selection time: 13.0
Test time: 19.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8752783964365256
Weighted FalsePositiveRate: 0.27985631813256023
Kappa statistic: 0.654244932757625
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 11.58129175946548
Correctly Classified Instances: 88.41870824053453
Weighted Precision: 0.8804007198108784
Weighted AreaUnderROC: 0.8025940331538413
Root mean squared error: 0.1927768330480956
Relative absolute error: 17.724325661965025
Root relative squared error: 51.72745238192063
Weighted TruePositiveRate: 0.8841870824053452
Weighted MatthewsCorrelation: 0.6871873867498057
Weighted FMeasure: 0.8732222259013822
Iteration time: 14.0
Weighted AreaUnderPRC: 0.814143488174399
Mean absolute error: 0.04923423794990236
Coverage of cases: 88.41870824053453
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 42.0
Weighted Recall: 0.8841870824053452
Weighted FalsePositiveRate: 0.27899901609766264
Kappa statistic: 0.6734910779213513
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.9020385184230932
Weighted AreaUnderROC: 0.8245252826902966
Root mean squared error: 0.17372833440703406
Relative absolute error: 14.661398088943308
Root relative squared error: 46.61620384223348
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7333617947700335
Weighted FMeasure: 0.8926501462476408
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8382957714651059
Mean absolute error: 0.0407261058026199
Coverage of cases: 90.64587973273942
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 56.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.25740823194680096
Kappa statistic: 0.7356009197465089
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.904877335202611
Weighted AreaUnderROC: 0.8257784155223769
Root mean squared error: 0.17192349104125765
Relative absolute error: 14.010563156220291
Root relative squared error: 46.13191354767961
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7401362726742055
Weighted FMeasure: 0.8948068964578609
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8407602064664139
Mean absolute error: 0.038918230989500424
Coverage of cases: 90.86859688195992
Instances selection time: 14.0
Test time: 23.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2571291377748455
Kappa statistic: 0.7408351165671809
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.904877335202611
Weighted AreaUnderROC: 0.8267203387142976
Root mean squared error: 0.1721409712822844
Relative absolute error: 13.690701559020177
Root relative squared error: 46.19026962000347
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7401362726742055
Weighted FMeasure: 0.8948068964578609
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8412158387480668
Mean absolute error: 0.038029726552833445
Coverage of cases: 90.86859688195992
Instances selection time: 14.0
Test time: 25.0
Accumulative iteration time: 84.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.2571291377748455
Kappa statistic: 0.7408351165671809
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9141153813606522
Weighted AreaUnderROC: 0.8383900683039548
Root mean squared error: 0.16373666573028245
Relative absolute error: 12.397249604454856
Root relative squared error: 43.93515779784883
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7666054429734057
Weighted FMeasure: 0.9039431052937771
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8535527607280267
Mean absolute error: 0.0344368044568187
Coverage of cases: 91.75946547884188
Instances selection time: 14.0
Test time: 27.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.24259277828322195
Kappa statistic: 0.7659679373397572
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9141153813606522
Weighted AreaUnderROC: 0.8406128934323462
Root mean squared error: 0.16387988152917762
Relative absolute error: 12.169711938325614
Root relative squared error: 43.97358662926262
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7666054429734057
Weighted FMeasure: 0.9039431052937771
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8546275138685578
Mean absolute error: 0.03380475538423748
Coverage of cases: 91.75946547884188
Instances selection time: 14.0
Test time: 29.0
Accumulative iteration time: 112.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.24259277828322195
Kappa statistic: 0.7659679373397572
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9141153813606522
Weighted AreaUnderROC: 0.8406128934323462
Root mean squared error: 0.16400650463499744
Relative absolute error: 11.990785623826488
Root relative squared error: 44.00756317391885
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.7666054429734057
Weighted FMeasure: 0.9039431052937771
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8546275138685578
Mean absolute error: 0.03330773784396213
Coverage of cases: 91.75946547884188
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 125.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.24259277828322195
Kappa statistic: 0.7659679373397572
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9168353917797124
Weighted AreaUnderROC: 0.8413702514835908
Root mean squared error: 0.16189009200028903
Relative absolute error: 11.578860926836922
Root relative squared error: 43.43967007156115
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.773516442614775
Weighted FMeasure: 0.9061502360865498
Iteration time: 13.0
Weighted AreaUnderPRC: 0.85707186664675
Mean absolute error: 0.032163502574546685
Coverage of cases: 91.98218262806236
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 138.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2424161036215807
Kappa statistic: 0.7712975932764549
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9168353917797124
Weighted AreaUnderROC: 0.8418148165092693
Root mean squared error: 0.16198538405889268
Relative absolute error: 11.443898496413746
Root relative squared error: 43.46523961405159
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.773516442614775
Weighted FMeasure: 0.9061502360865498
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8572863822488883
Mean absolute error: 0.031788606934482315
Coverage of cases: 91.98218262806236
Instances selection time: 13.0
Test time: 34.0
Accumulative iteration time: 151.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2424161036215807
Kappa statistic: 0.7712975932764549
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.917961901151495
Weighted AreaUnderROC: 0.8375713432549023
Root mean squared error: 0.16207085647292924
Relative absolute error: 11.332374752196694
Root relative squared error: 43.48817426941
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.774407318258293
Weighted FMeasure: 0.9060159550906345
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8556671633966874
Mean absolute error: 0.03147881875610162
Coverage of cases: 91.98218262806236
Instances selection time: 21.0
Test time: 36.0
Accumulative iteration time: 172.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.2492556323499437
Kappa statistic: 0.7693361493235915
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9235178561832371
Weighted AreaUnderROC: 0.8513920605665712
Root mean squared error: 0.1552555438251677
Relative absolute error: 10.442798618601099
Root relative squared error: 41.65943398521293
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.79327507287743
Weighted FMeasure: 0.9131316925545645
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8670547372083299
Mean absolute error: 0.029007773940558323
Coverage of cases: 92.65033407572383
Instances selection time: 10.0
Test time: 37.0
Accumulative iteration time: 182.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.22789976364898828
Kappa statistic: 0.7911686774157176
Training time: 0.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.9216559675094982
Weighted AreaUnderROC: 0.846784584531481
Root mean squared error: 0.1576515429059852
Relative absolute error: 10.614089426211153
Root relative squared error: 42.30234800346113
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7870340725526654
Weighted FMeasure: 0.9107466985314915
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8631638747722731
Mean absolute error: 0.029483581739475132
Coverage of cases: 92.42761692650335
Instances selection time: 9.0
Test time: 39.0
Accumulative iteration time: 191.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.23501838654930676
Kappa statistic: 0.7838993247738629
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9329710467706013
Weighted AreaUnderROC: 0.8479500856969824
Root mean squared error: 0.15537749011346133
Relative absolute error: 10.269648681658277
Root relative squared error: 41.69215560804033
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7952998175342111
Weighted FMeasure: 0.91234364506596
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8653450911936434
Mean absolute error: 0.02852680189349493
Coverage of cases: 92.65033407572383
Instances selection time: 8.0
Test time: 40.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.23491455571050932
Kappa statistic: 0.7892948052502097
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9329710467706013
Weighted AreaUnderROC: 0.8479500856969824
Root mean squared error: 0.15543014926961213
Relative absolute error: 10.196224970211459
Root relative squared error: 41.70628554237525
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7952998175342111
Weighted FMeasure: 0.91234364506596
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8653450911936434
Mean absolute error: 0.02832284713947599
Coverage of cases: 92.65033407572383
Instances selection time: 7.0
Test time: 42.0
Accumulative iteration time: 207.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.23491455571050932
Kappa statistic: 0.7892948052502097
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9329710467706013
Weighted AreaUnderROC: 0.8479500856969824
Root mean squared error: 0.15547821759831829
Relative absolute error: 10.12987856457517
Root relative squared error: 41.719183628441456
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7952998175342111
Weighted FMeasure: 0.91234364506596
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8653450911936434
Mean absolute error: 0.02813855156826408
Coverage of cases: 92.65033407572383
Instances selection time: 5.0
Test time: 44.0
Accumulative iteration time: 212.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.23491455571050932
Kappa statistic: 0.7892948052502097
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9534934394828696
Weighted AreaUnderROC: 0.9019838139633162
Root mean squared error: 0.12701778511146694
Relative absolute error: 7.170316753278969
Root relative squared error: 34.08244822328426
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8619178688826348
Weighted FMeasure: 0.9374557826422908
Iteration time: 2.0
Weighted AreaUnderPRC: 0.909584467459015
Mean absolute error: 0.019917546536885825
Coverage of cases: 95.10022271714922
Instances selection time: 2.0
Test time: 45.0
Accumulative iteration time: 214.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14977017507864326
Kappa statistic: 0.8669378738078559
Training time: 0.0
		
Time end:Tue Oct 31 11.06.02 EET 2017