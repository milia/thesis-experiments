Tue Oct 31 11.05.57 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.05.57 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 33.259094283593136
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.900215847057743
Weighted AreaUnderROC: 0.8547327489727893
Root mean squared error: 0.17452455920897134
Relative absolute error: 17.068563245045198
Root relative squared error: 46.82985337613385
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.7372514124891859
Weighted FMeasure: 0.9006288070727406
Iteration time: 31.0
Weighted AreaUnderPRC: 0.8547259282583005
Mean absolute error: 0.04741267568068063
Coverage of cases: 92.65033407572383
Instances selection time: 31.0
Test time: 34.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.19572308608833178
Kappa statistic: 0.7499287620133152
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.932208314440162
Weighted AreaUnderROC: 0.9201068156564953
Root mean squared error: 0.15130511595346544
Relative absolute error: 13.306557705875086
Root relative squared error: 40.599422953852596
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8172752725467782
Weighted FMeasure: 0.9273974703876415
Iteration time: 33.0
Weighted AreaUnderPRC: 0.9056803896974993
Mean absolute error: 0.036962660294097095
Coverage of cases: 92.87305122494432
Instances selection time: 33.0
Test time: 41.0
Accumulative iteration time: 64.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.09003505661473524
Kappa statistic: 0.8264168267430202
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9308406916424733
Weighted AreaUnderROC: 0.9188908216511251
Root mean squared error: 0.15389295536285064
Relative absolute error: 12.857551011225894
Root relative squared error: 41.29381313396118
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8125208919749723
Weighted FMeasure: 0.9253854011420535
Iteration time: 31.0
Weighted AreaUnderPRC: 0.9039047242988238
Mean absolute error: 0.035715419475627126
Coverage of cases: 92.65033407572383
Instances selection time: 30.0
Test time: 20.0
Accumulative iteration time: 95.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.09030788512253034
Kappa statistic: 0.8216237690511159
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9460945427709175
Weighted AreaUnderROC: 0.9243647372431069
Root mean squared error: 0.14209049559109088
Relative absolute error: 11.056136821686437
Root relative squared error: 38.12688085179779
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8386798980133546
Weighted FMeasure: 0.9355021248916867
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9138476303131771
Mean absolute error: 0.030711491171350906
Coverage of cases: 93.76391982182628
Instances selection time: 14.0
Test time: 21.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.08978873092854318
Kappa statistic: 0.845659006089177
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9518054693490087
Weighted AreaUnderROC: 0.9303612976118809
Root mean squared error: 0.13178516712862567
Relative absolute error: 9.589145313690898
Root relative squared error: 35.36167105509351
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8590199816987423
Weighted FMeasure: 0.9435513261793885
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9217772361743107
Mean absolute error: 0.02663651476025223
Coverage of cases: 94.65478841870824
Instances selection time: 16.0
Test time: 28.0
Accumulative iteration time: 125.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.08869741689736278
Kappa statistic: 0.8657764934482586
Training time: 0.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9618294922188633
Weighted AreaUnderROC: 0.9367846014415391
Root mean squared error: 0.11437962782655109
Relative absolute error: 7.7294736165839275
Root relative squared error: 30.691274767355523
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8917815278272273
Weighted FMeasure: 0.9560163425225883
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9337837059366493
Mean absolute error: 0.02147076004606625
Coverage of cases: 95.99109131403118
Instances selection time: 19.0
Test time: 26.0
Accumulative iteration time: 144.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.08706044585059218
Kappa statistic: 0.8970786745791204
Training time: 0.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9426659853235343
Root mean squared error: 0.10795550776939385
Relative absolute error: 6.938340869623074
Root relative squared error: 28.9675024701447
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9403513771920419
Mean absolute error: 0.019273169082286126
Coverage of cases: 96.43652561247217
Instances selection time: 20.0
Test time: 27.0
Accumulative iteration time: 164.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 0.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9426659853235343
Root mean squared error: 0.1080049187885455
Relative absolute error: 6.711762668161303
Root relative squared error: 28.980760837863976
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9403513771920419
Mean absolute error: 0.018643785189336768
Coverage of cases: 96.43652561247217
Instances selection time: 14.0
Test time: 28.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9426659853235343
Root mean squared error: 0.10805216807731789
Relative absolute error: 6.520727667459023
Root relative squared error: 28.99343915245401
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 23.0
Weighted AreaUnderPRC: 0.9403513771920419
Mean absolute error: 0.018113132409608216
Coverage of cases: 96.43652561247217
Instances selection time: 23.0
Test time: 31.0
Accumulative iteration time: 202.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 0.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9426659853235343
Root mean squared error: 0.1080965911676785
Relative absolute error: 6.357480676012081
Root relative squared error: 29.005359118432043
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9403513771920419
Mean absolute error: 0.017659668544477827
Coverage of cases: 96.43652561247217
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 215.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9430948081051793
Root mean squared error: 0.10813666984898872
Relative absolute error: 6.209644733310871
Root relative squared error: 29.016113357135023
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 12.0
Weighted AreaUnderPRC: 0.940569473176593
Mean absolute error: 0.01724901314808558
Coverage of cases: 96.43652561247217
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 227.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9429518671779643
Root mean squared error: 0.10817563495269034
Relative absolute error: 6.088976724834233
Root relative squared error: 29.026568791610284
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9404967641566856
Mean absolute error: 0.01691382423565048
Coverage of cases: 96.43652561247217
Instances selection time: 12.0
Test time: 35.0
Accumulative iteration time: 239.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 0.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9658187486311287
Weighted AreaUnderROC: 0.9429518671779643
Root mean squared error: 0.10821130587141448
Relative absolute error: 5.980743637382443
Root relative squared error: 29.036140303500726
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9038119329430673
Weighted FMeasure: 0.9609194414311737
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9404967641566856
Mean absolute error: 0.01661317677050662
Coverage of cases: 96.43652561247217
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 250.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.07966899444247862
Kappa statistic: 0.9085667740005854
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9677561629508811
Weighted AreaUnderROC: 0.944178268526756
Root mean squared error: 0.10481884842675153
Relative absolute error: 5.621962947965211
Root relative squared error: 28.125848448655738
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9096272823234413
Weighted FMeasure: 0.9630652850325294
Iteration time: 9.0
Weighted AreaUnderPRC: 0.942847397281904
Mean absolute error: 0.015616563744347653
Coverage of cases: 96.65924276169265
Instances selection time: 9.0
Test time: 38.0
Accumulative iteration time: 259.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07939616593468352
Kappa statistic: 0.9139604997572754
Training time: 0.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9677561629508811
Weighted AreaUnderROC: 0.944178268526756
Root mean squared error: 0.10484724099365257
Relative absolute error: 5.535929305266281
Root relative squared error: 28.13346697381331
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9096272823234413
Weighted FMeasure: 0.9630652850325294
Iteration time: 8.0
Weighted AreaUnderPRC: 0.942847397281904
Mean absolute error: 0.015377581403517296
Coverage of cases: 96.65924276169265
Instances selection time: 8.0
Test time: 39.0
Accumulative iteration time: 267.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07939616593468352
Kappa statistic: 0.9139604997572754
Training time: 0.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9677561629508811
Weighted AreaUnderROC: 0.944178268526756
Root mean squared error: 0.10487366240873543
Relative absolute error: 5.458608325628541
Root relative squared error: 28.14055658343575
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9096272823234413
Weighted FMeasure: 0.9630652850325294
Iteration time: 6.0
Weighted AreaUnderPRC: 0.942847397281904
Mean absolute error: 0.015162800904523575
Coverage of cases: 96.65924276169265
Instances selection time: 6.0
Test time: 40.0
Accumulative iteration time: 273.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07939616593468352
Kappa statistic: 0.9139604997572754
Training time: 0.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9677561629508811
Weighted AreaUnderROC: 0.9450359140900457
Root mean squared error: 0.10489689254704866
Relative absolute error: 5.379150611137069
Root relative squared error: 28.14678988364316
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9096272823234413
Weighted FMeasure: 0.9630652850325294
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9432825993154209
Mean absolute error: 0.014942085030936154
Coverage of cases: 96.65924276169265
Instances selection time: 9.0
Test time: 59.0
Accumulative iteration time: 282.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07939616593468352
Kappa statistic: 0.9139604997572754
Training time: 0.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9677561629508811
Weighted AreaUnderROC: 0.9453217959444755
Root mean squared error: 0.10491954954527699
Relative absolute error: 5.313094659055093
Root relative squared error: 28.152869394227718
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9096272823234413
Weighted FMeasure: 0.9630652850325294
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9434277523953386
Mean absolute error: 0.014758596275152889
Coverage of cases: 96.65924276169265
Instances selection time: 3.0
Test time: 43.0
Accumulative iteration time: 285.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07939616593468352
Kappa statistic: 0.9139604997572754
Training time: 0.0
		
Time end:Tue Oct 31 11.05.58 EET 2017