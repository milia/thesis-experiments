Tue Oct 31 11.13.15 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.13.15 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 26.83741648106899
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9343667279586132
Weighted AreaUnderROC: 0.9669873443307658
Root mean squared error: 0.1294917102003393
Relative absolute error: 15.660111487694584
Root relative squared error: 34.74627198367956
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8437553336982315
Weighted FMeasure: 0.9368871025114905
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9683923111144682
Mean absolute error: 0.043500309688040084
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15024439959987285
Kappa statistic: 0.8508127217873234
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 26.095025983667377
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9425977491976624
Weighted AreaUnderROC: 0.9558303776464865
Root mean squared error: 0.12998698034076314
Relative absolute error: 14.94514572385551
Root relative squared error: 34.879166907825194
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8637193991955378
Weighted FMeasure: 0.9461546260628917
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9585421852063998
Mean absolute error: 0.04151429367737601
Coverage of cases: 97.7728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.12204273650639405
Kappa statistic: 0.8714638906961613
Training time: 4.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 23.86785449146244
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9357306428899246
Weighted AreaUnderROC: 0.9721008084516556
Root mean squared error: 0.13234671104252377
Relative absolute error: 13.495329990452289
Root relative squared error: 35.5123490987528
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8368121937134373
Weighted FMeasure: 0.9312892929449623
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9692962169058741
Mean absolute error: 0.03748702775125599
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.16454681223071002
Kappa statistic: 0.8427362862377412
Training time: 4.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 24.758723088344404
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9295974099269949
Weighted AreaUnderROC: 0.9467916377553578
Root mean squared error: 0.14306470358883083
Relative absolute error: 15.504938572953185
Root relative squared error: 38.38828828865825
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8186226247712416
Weighted FMeasure: 0.9249054600348982
Iteration time: 6.0
Weighted AreaUnderPRC: 0.946804896351967
Mean absolute error: 0.04306927381375842
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.19274847532418873
Kappa statistic: 0.8211671313400449
Training time: 5.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 24.313288789903407
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9240623608017817
Weighted AreaUnderROC: 0.968736549175765
Root mean squared error: 0.14171391179911672
Relative absolute error: 15.219318884441119
Root relative squared error: 38.02583281682833
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7958790232002732
Weighted FMeasure: 0.9162172397833475
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9650712511135041
Mean absolute error: 0.0422758857901138
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.23491455571050932
Kappa statistic: 0.7896388210574138
Training time: 5.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 23.459539717891527
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9265494190484737
Weighted AreaUnderROC: 0.9616896497320025
Root mean squared error: 0.14941791675198632
Relative absolute error: 14.992602589043946
Root relative squared error: 40.093034269661715
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8067915418956728
Weighted FMeasure: 0.9199884172351146
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9534269896314433
Mean absolute error: 0.04164611830289943
Coverage of cases: 97.32739420935413
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.21383151551734902
Kappa statistic: 0.8056656986484977
Training time: 6.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 22.828507795100098
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9627662612072412
Weighted AreaUnderROC: 0.9627149183130285
Root mean squared error: 0.11587678853108226
Relative absolute error: 10.959132426463437
Root relative squared error: 31.093005140384307
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9186486630099232
Weighted FMeasure: 0.966481084593018
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9606984117459392
Mean absolute error: 0.03044203451795369
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 42.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.07851251358109797
Kappa statistic: 0.9250195897080169
Training time: 6.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 23.459539717891502
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9317852665912106
Weighted AreaUnderROC: 0.9511846145913284
Root mean squared error: 0.13599222512965842
Relative absolute error: 13.051294199171327
Root relative squared error: 36.49054317616474
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8244643463451868
Weighted FMeasure: 0.9263293312215588
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9482453401626028
Mean absolute error: 0.03625359499769777
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.1856298524238703
Kappa statistic: 0.8279575778309956
Training time: 6.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 22.086117297698483
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9205930011493738
Weighted AreaUnderROC: 0.9745870409797517
Root mean squared error: 0.13974297268278732
Relative absolute error: 13.033963629036105
Root relative squared error: 37.49697435559322
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7947149302851035
Weighted FMeasure: 0.9180685404477874
Iteration time: 7.0
Weighted AreaUnderPRC: 0.96732983148271
Mean absolute error: 0.036205454525099934
Coverage of cases: 98.21826280623608
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 56.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.22088497158746725
Kappa statistic: 0.79341642964698
Training time: 7.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.754268745359962
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9277870991240588
Weighted AreaUnderROC: 0.9653810045337687
Root mean squared error: 0.14801357879365382
Relative absolute error: 14.376030559829532
Root relative squared error: 39.71621085307594
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8070316013343767
Weighted FMeasure: 0.9184314736653267
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9594969432512063
Mean absolute error: 0.039933418221748306
Coverage of cases: 97.7728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 64.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.2206773099098724
Kappa statistic: 0.8040239918900653
Training time: 7.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 21.677802524127554
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9315519963500801
Weighted AreaUnderROC: 0.9600637434622356
Root mean squared error: 0.13441069990013899
Relative absolute error: 11.595842920622738
Root relative squared error: 36.06617542560438
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8158835786090103
Weighted FMeasure: 0.9174868537185296
Iteration time: 8.0
Weighted AreaUnderPRC: 0.957607506694056
Mean absolute error: 0.03221067477950729
Coverage of cases: 97.55011135857461
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 72.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.20644006410923546
Kappa statistic: 0.8178728284891041
Training time: 7.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 21.826280623607904
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9271826789110249
Weighted AreaUnderROC: 0.9560549564378686
Root mean squared error: 0.1365894137119426
Relative absolute error: 12.655267951060754
Root relative squared error: 36.65078568800965
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.819212977970638
Weighted FMeasure: 0.9278238832473766
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9543977098870778
Mean absolute error: 0.035153522086279525
Coverage of cases: 97.55011135857461
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 80.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.18556468559367006
Kappa statistic: 0.8228724561976277
Training time: 8.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.93763919821812
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9240623608017817
Weighted AreaUnderROC: 0.9674592270663245
Root mean squared error: 0.14576565743784764
Relative absolute error: 14.049472355535887
Root relative squared error: 39.11303025791721
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.7967570217115582
Weighted FMeasure: 0.9181112767161453
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9670877232510102
Mean absolute error: 0.03902631209871041
Coverage of cases: 98.21826280623608
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 90.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.23491455571050932
Kappa statistic: 0.789719427217121
Training time: 9.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 21.974758723088204
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9353577054701505
Weighted AreaUnderROC: 0.9785867890201508
Root mean squared error: 0.13409434719586105
Relative absolute error: 12.554288915583438
Root relative squared error: 35.98128908740861
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8330208879427574
Weighted FMeasure: 0.9305745355964623
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9725956026886772
Mean absolute error: 0.0348730247655092
Coverage of cases: 98.21826280623608
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 101.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.19220281830859853
Kappa statistic: 0.8321472086840939
Training time: 10.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 21.789161098737804
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.946430571083742
Weighted AreaUnderROC: 0.9742204911907054
Root mean squared error: 0.13071208383600375
Relative absolute error: 12.082551669954903
Root relative squared error: 35.073732592554876
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8755141120872313
Weighted FMeasure: 0.9492301719346932
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9703640826990829
Mean absolute error: 0.033562643527652175
Coverage of cases: 98.21826280623608
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 112.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1283428738833272
Kappa statistic: 0.8812279286309469
Training time: 10.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 22.234595397178836
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9487082405345211
Weighted AreaUnderROC: 0.9788775103349286
Root mean squared error: 0.11023531184244145
Relative absolute error: 10.056185444727275
Root relative squared error: 29.579238096070547
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8752934398830154
Weighted FMeasure: 0.9466598081623808
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9769463710617848
Mean absolute error: 0.027933848457575485
Coverage of cases: 98.66369710467706
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 124.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.13552666361384594
Kappa statistic: 0.8799962582352234
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 22.82850779510014
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.958349803441073
Weighted AreaUnderROC: 0.9860385785786208
Root mean squared error: 0.10525716282116894
Relative absolute error: 10.045466431730835
Root relative squared error: 28.243460542427815
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9067552364252194
Weighted FMeasure: 0.9619099424203547
Iteration time: 13.0
Weighted AreaUnderPRC: 0.983105162464635
Mean absolute error: 0.027904073421474263
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 137.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07905817059668817
Kappa statistic: 0.9140483422241508
Training time: 12.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 20.82405345211576
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9715484428421389
Weighted AreaUnderROC: 0.9905842896080809
Root mean squared error: 0.08163905743607446
Relative absolute error: 6.069313539230857
Root relative squared error: 21.906057845528768
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9427736342828257
Weighted FMeasure: 0.9755268898252117
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9864661158594897
Mean absolute error: 0.0168592042756411
Coverage of cases: 98.88641425389756
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 149.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.04353022292529609
Kappa statistic: 0.9491998441173144
Training time: 12.0
		
Time end:Tue Oct 31 11.13.16 EET 2017