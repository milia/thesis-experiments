Tue Oct 31 11.10.35 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.35 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 27.394209354120274
Incorrectly Classified Instances: 8.68596881959911
Correctly Classified Instances: 91.31403118040089
Weighted Precision: 0.9159106958552881
Weighted AreaUnderROC: 0.9809941236524915
Root mean squared error: 0.13485470385295104
Relative absolute error: 16.935253522012804
Root relative squared error: 36.185314188096335
Weighted TruePositiveRate: 0.9131403118040089
Weighted MatthewsCorrelation: 0.7539585154171803
Weighted FMeasure: 0.8989271360955271
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9722062033143444
Mean absolute error: 0.04704237089447954
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9131403118040089
Weighted FalsePositiveRate: 0.23655152675727992
Kappa statistic: 0.757310752002661
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 24.944320712694886
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9889571357349215
Weighted AreaUnderROC: 0.9953885178235695
Root mean squared error: 0.09858974040045144
Relative absolute error: 11.963412366200211
Root relative squared error: 26.45440337033614
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9711055330359251
Weighted FMeasure: 0.9888831510018773
Iteration time: 6.0
Weighted AreaUnderPRC: 0.99373801079081
Mean absolute error: 0.033231701017222476
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.014381115677496469
Kappa statistic: 0.9722791593608772
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 25.3526354862658
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9858412274722022
Weighted AreaUnderROC: 0.9984755574769513
Root mean squared error: 0.08168329060007636
Relative absolute error: 9.416010594273821
Root relative squared error: 21.91792684891695
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9630103359837093
Weighted FMeasure: 0.9840135780198667
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9961042507769189
Mean absolute error: 0.02615558498409369
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.008755593947089066
Kappa statistic: 0.9615516355540333
Training time: 7.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 25.129918337045282
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 0.9971607831654554
Root mean squared error: 0.06911681429950274
Relative absolute error: 8.544243940921392
Root relative squared error: 18.545987419430208
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.994124196453
Weighted FMeasure: 0.9976153705024027
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9973625204682671
Mean absolute error: 0.02373401094700363
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944102781166747
Training time: 7.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 28.619153674832937
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9890246015802007
Weighted AreaUnderROC: 0.9911914294591432
Root mean squared error: 0.10380677074084986
Relative absolute error: 14.539214614256206
Root relative squared error: 27.854279508153315
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9703427728018447
Weighted FMeasure: 0.9885559182198577
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9923547073150834
Mean absolute error: 0.04038670726182239
Coverage of cases: 99.77728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.03559311450159232
Kappa statistic: 0.9716375672739217
Training time: 7.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 24.536005939124014
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9866369710467706
Weighted AreaUnderROC: 0.9989144570746197
Root mean squared error: 0.0752471676337196
Relative absolute error: 8.677258778520597
Root relative squared error: 20.19093383319838
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.964715425811194
Weighted FMeasure: 0.9866369710467706
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9972186037954377
Mean absolute error: 0.02410349660700142
Coverage of cases: 99.77728285077951
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.021921545235576653
Kappa statistic: 0.9666022017256769
Training time: 7.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 25.018559762435054
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9876417119007489
Weighted AreaUnderROC: 0.9989374219303073
Root mean squared error: 0.0696716230996065
Relative absolute error: 8.400763132715644
Root relative squared error: 18.69485824241586
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9655046398109415
Weighted FMeasure: 0.9867088721448751
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9978166823591842
Mean absolute error: 0.023335453146432115
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.008482765439293966
Kappa statistic: 0.9670623907275862
Training time: 8.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 21.492204899777327
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 0.9987810311315213
Root mean squared error: 0.06309967546074602
Relative absolute error: 6.12400994466224
Root relative squared error: 16.9314196426085
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.994124196453
Weighted FMeasure: 0.9976153705024027
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9977536154811422
Mean absolute error: 0.01701113873517272
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944102781166747
Training time: 8.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 24.05345211581295
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9847395659045414
Weighted AreaUnderROC: 0.9930882002475778
Root mean squared error: 0.07865102656705485
Relative absolute error: 8.718948975450724
Root relative squared error: 21.10428502848929
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9588635831011024
Weighted FMeasure: 0.9843729331350105
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9938582739125209
Mean absolute error: 0.024219302709585104
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 74.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.022447182732135796
Kappa statistic: 0.961155329246589
Training time: 10.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 23.014105419450658
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955902004454342
Weighted AreaUnderROC: 0.9994087693445035
Root mean squared error: 0.06708402897158536
Relative absolute error: 7.053021011487316
Root relative squared error: 18.000533878203708
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882200602907801
Weighted FMeasure: 0.9953978216784453
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9982368830796475
Mean absolute error: 0.019591725031909018
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 85.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.007391451408113564
Kappa statistic: 0.9888611865689229
Training time: 10.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.01187824795844
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955715543585228
Weighted AreaUnderROC: 0.9994021741291217
Root mean squared error: 0.06277281720498822
Relative absolute error: 5.960392933192839
Root relative squared error: 16.843714369142724
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9881948887487969
Weighted FMeasure: 0.9953815862834994
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9988537675575304
Mean absolute error: 0.01655664703664661
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 96.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.014237245800636927
Kappa statistic: 0.9887796284032836
Training time: 10.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 21.640682999257614
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 0.998055019531894
Root mean squared error: 0.057959127685826714
Relative absolute error: 5.376335376894301
Root relative squared error: 15.55206593065191
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.994124196453
Weighted FMeasure: 0.9976153705024027
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9973924250561782
Mean absolute error: 0.014934264935817353
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 107.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944102781166747
Training time: 10.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 22.123236822568693
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955715543585228
Weighted AreaUnderROC: 0.99874810182919
Root mean squared error: 0.06317581400765016
Relative absolute error: 6.227688189658427
Root relative squared error: 16.951849758598783
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882705998156146
Weighted FMeasure: 0.9953645130682539
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9977666014574355
Mean absolute error: 0.017299133860162127
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 119.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.014237245800636927
Kappa statistic: 0.9887755612219388
Training time: 11.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 21.083890126206388
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 0.9986765601134003
Root mean squared error: 0.056663682878934196
Relative absolute error: 4.999571478804248
Root relative squared error: 15.204461612734617
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.994124196453
Weighted FMeasure: 0.9976153705024027
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9977336716018272
Mean absolute error: 0.013887698552233886
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 132.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944102781166747
Training time: 12.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 21.863400148478092
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 0.9943529052976183
Root mean squared error: 0.05773489060170213
Relative absolute error: 5.509239792645348
Root relative squared error: 15.491896807070441
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.994124196453
Weighted FMeasure: 0.9976153705024027
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9949223331734309
Mean absolute error: 0.015303443868459145
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 145.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944102781166747
Training time: 12.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 22.494432071269497
Incorrectly Classified Instances: 1.1135857461024499
Correctly Classified Instances: 98.88641425389756
Weighted Precision: 0.9888952099673994
Weighted AreaUnderROC: 0.9980246107945902
Root mean squared error: 0.07228333523400213
Relative absolute error: 7.237507340623441
Root relative squared error: 19.39565414683623
Weighted TruePositiveRate: 0.9888641425389755
Weighted MatthewsCorrelation: 0.9703450943772808
Weighted FMeasure: 0.9886783114811718
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9973652628285168
Mean absolute error: 0.020104187057287136
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 159.0
Weighted Recall: 0.9888641425389755
Weighted FalsePositiveRate: 0.02874732010906896
Kappa statistic: 0.97184070241455
Training time: 13.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 21.75204157386788
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9911943046204152
Weighted AreaUnderROC: 0.9982032914334468
Root mean squared error: 0.06922890403455259
Relative absolute error: 6.315206688541527
Root relative squared error: 18.576064251488376
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.9763048124563996
Weighted FMeasure: 0.9908526300990994
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9974483563151456
Mean absolute error: 0.017542240801504065
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 171.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.028474491601273855
Kappa statistic: 0.9773937341875717
Training time: 12.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 22.01187824795844
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955715543585228
Weighted AreaUnderROC: 0.9994783231407631
Root mean squared error: 0.06735010098136837
Relative absolute error: 6.4684646429385895
Root relative squared error: 18.071928490297886
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882705998156146
Weighted FMeasure: 0.9953645130682539
Iteration time: 14.0
Weighted AreaUnderPRC: 0.998423699517213
Mean absolute error: 0.017967957341495904
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 185.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.014237245800636927
Kappa statistic: 0.9887755612219388
Training time: 14.0
		
Time end:Tue Oct 31 11.10.36 EET 2017