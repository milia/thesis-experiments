Tue Oct 31 11.07.17 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.07.17 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.8883672007280023
Weighted AreaUnderROC: 0.8549377427379425
Root mean squared error: 0.311404664174734
Relative absolute error: 78.38370983137168
Root relative squared error: 83.55863971262426
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7423986035626717
Weighted FMeasure: 0.8956891939860783
Iteration time: 82.0
Weighted AreaUnderPRC: 0.8589690639829562
Mean absolute error: 0.2177325273093636
Coverage of cases: 99.10913140311804
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 82.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.22245677580403758
Kappa statistic: 0.7561803396641371
Training time: 73.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.8883672007280023
Weighted AreaUnderROC: 0.8563436447423968
Root mean squared error: 0.3112525858596101
Relative absolute error: 78.34552974864818
Root relative squared error: 83.51783281856176
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7423986035626717
Weighted FMeasure: 0.8956891939860783
Iteration time: 75.0
Weighted AreaUnderPRC: 0.8588945583301428
Mean absolute error: 0.21762647152402054
Coverage of cases: 99.10913140311804
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 157.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.22245677580403758
Kappa statistic: 0.7561803396641371
Training time: 67.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.8846478569877967
Weighted AreaUnderROC: 0.8537734388414768
Root mean squared error: 0.3112525858596101
Relative absolute error: 78.34552974864818
Root relative squared error: 83.51783281856176
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7402826550165881
Weighted FMeasure: 0.8937602838295936
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8583552852871031
Mean absolute error: 0.21762647152402054
Coverage of cases: 99.10913140311804
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 225.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.22930257019656095
Kappa statistic: 0.7541275360731594
Training time: 61.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.8846478569877967
Weighted AreaUnderROC: 0.8537734388414768
Root mean squared error: 0.3112525858596101
Relative absolute error: 78.34552974864818
Root relative squared error: 83.51783281856176
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7402826550165881
Weighted FMeasure: 0.8937602838295936
Iteration time: 62.0
Weighted AreaUnderPRC: 0.8583552852871031
Mean absolute error: 0.21762647152402054
Coverage of cases: 99.10913140311804
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 287.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.22930257019656095
Kappa statistic: 0.7541275360731594
Training time: 55.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 62.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 349.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 56.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 97.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 11.0
Test time: 10.0
Accumulative iteration time: 446.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 86.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 68.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 514.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 62.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 581.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 61.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 67.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 648.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 64.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 65.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 713.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 62.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 66.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 779.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 63.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 147.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 926.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 144.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 139.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 1065.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 137.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 142.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 1207.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 140.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8557560562005277
Root mean squared error: 0.3107410575515644
Relative absolute error: 78.23098950047765
Root relative squared error: 83.38057537025709
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 145.0
Weighted AreaUnderPRC: 0.8661466779056911
Mean absolute error: 0.2173083041679913
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 1352.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 143.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 7.572383073496659
Correctly Classified Instances: 92.42761692650335
Weighted Precision: 0.8776714211249586
Weighted AreaUnderROC: 0.8639290422716208
Root mean squared error: 0.31014320821124525
Relative absolute error: 78.09735921094537
Root relative squared error: 83.22015555842624
Weighted TruePositiveRate: 0.9242761692650334
Weighted MatthewsCorrelation: 0.7719136898152377
Weighted FMeasure: 0.8980775770810574
Iteration time: 143.0
Weighted AreaUnderPRC: 0.869930974043012
Mean absolute error: 0.21693710891929055
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1495.0
Weighted Recall: 0.9242761692650334
Weighted FalsePositiveRate: 0.24203317861082776
Kappa statistic: 0.7817334362757714
Training time: 142.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.8832436692799607
Weighted AreaUnderROC: 0.8769923668443486
Root mean squared error: 0.30988663398337973
Relative absolute error: 78.04008908686008
Root relative squared error: 83.15130946865243
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.7905732310925944
Weighted FMeasure: 0.9051287242823991
Iteration time: 160.0
Weighted AreaUnderPRC: 0.8824069717956652
Mean absolute error: 0.21677802524127585
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 1655.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.2206773099098724
Kappa statistic: 0.8034622498976293
Training time: 159.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.8978149641846745
Weighted AreaUnderROC: 0.8927461251231562
Root mean squared error: 0.3125413442913519
Relative absolute error: 80.49888641425511
Root relative squared error: 83.86364299375585
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.8128298170083778
Weighted FMeasure: 0.9158349720647962
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8951990409117357
Mean absolute error: 0.22360801781737308
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1909.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.19932144120891698
Kappa statistic: 0.8249878193081367
Training time: 253.0
		
Time end:Tue Oct 31 11.07.20 EET 2017