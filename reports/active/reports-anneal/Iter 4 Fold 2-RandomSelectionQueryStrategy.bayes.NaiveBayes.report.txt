Tue Oct 31 11.10.52 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.52 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 19.8218262806236
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9292450988108334
Weighted AreaUnderROC: 0.9316788979814542
Root mean squared error: 0.1509790273612387
Relative absolute error: 12.192519645671723
Root relative squared error: 40.511924202783845
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8219478240325337
Weighted FMeasure: 0.9242564910675606
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9039473645766397
Mean absolute error: 0.03386811012686556
Coverage of cases: 95.32293986636971
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 1.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.09115287346751869
Kappa statistic: 0.8187101589360217
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 19.45063103192277
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9199716890395683
Weighted AreaUnderROC: 0.9266260515026484
Root mean squared error: 0.16651307046423036
Relative absolute error: 13.201819625266857
Root relative squared error: 44.680145364028
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.8007189579431854
Weighted FMeasure: 0.9144347062686715
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9003888193516223
Mean absolute error: 0.03667172118129646
Coverage of cases: 92.87305122494432
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.07814943654545678
Kappa statistic: 0.7980804506562207
Training time: 0.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 19.153674832962118
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9225371449492021
Weighted AreaUnderROC: 0.9257342490387945
Root mean squared error: 0.16620264579904043
Relative absolute error: 12.755021654566278
Root relative squared error: 44.596849685637125
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.8050546369998987
Weighted FMeasure: 0.9148822452472977
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8997282308867518
Mean absolute error: 0.035430615707128195
Coverage of cases: 92.87305122494432
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.06428885009141237
Kappa statistic: 0.8009101516919486
Training time: 0.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 19.302152932442468
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9173891846402805
Weighted AreaUnderROC: 0.9299554152253547
Root mean squared error: 0.1693812341514203
Relative absolute error: 13.388827544139962
Root relative squared error: 45.44975444104639
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7919755414682721
Weighted FMeasure: 0.9091471055793657
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9057156040407038
Mean absolute error: 0.03719118762261064
Coverage of cases: 93.3184855233853
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.06426234726980933
Kappa statistic: 0.7877288870439554
Training time: 0.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 19.710467706013354
Incorrectly Classified Instances: 10.913140311804009
Correctly Classified Instances: 89.08685968819599
Weighted Precision: 0.9108770288251493
Weighted AreaUnderROC: 0.9379338566365633
Root mean squared error: 0.16905246473009694
Relative absolute error: 13.64467188002559
Root relative squared error: 45.36153634804611
Weighted TruePositiveRate: 0.89086859688196
Weighted MatthewsCorrelation: 0.7611051146138247
Weighted FMeasure: 0.8953969455648085
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9215742304035498
Mean absolute error: 0.03790186633340404
Coverage of cases: 94.87750556792874
Instances selection time: 0.0
Test time: 26.0
Accumulative iteration time: 5.0
Weighted Recall: 0.89086859688196
Weighted FalsePositiveRate: 0.06492399631119111
Kappa statistic: 0.7537082022635427
Training time: 1.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 18.968077208611703
Incorrectly Classified Instances: 11.58129175946548
Correctly Classified Instances: 88.41870824053453
Weighted Precision: 0.9076132998453873
Weighted AreaUnderROC: 0.9264812888105954
Root mean squared error: 0.18622162261127184
Relative absolute error: 14.99975766249983
Root relative squared error: 49.96850484469413
Weighted TruePositiveRate: 0.8841870824053452
Weighted MatthewsCorrelation: 0.7492878024241104
Weighted FMeasure: 0.8894865544292893
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9039034934473841
Mean absolute error: 0.04166599350694356
Coverage of cases: 92.20489977728285
Instances selection time: 1.0
Test time: 37.0
Accumulative iteration time: 7.0
Weighted Recall: 0.8841870824053452
Weighted FalsePositiveRate: 0.06574248183457641
Kappa statistic: 0.7411759489180559
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.8589458054937
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.911522763870858
Weighted AreaUnderROC: 0.9383243660363115
Root mean squared error: 0.17289990566811886
Relative absolute error: 14.027864306785395
Root relative squared error: 46.39391308526603
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7647889992581158
Weighted FMeasure: 0.8970745340887184
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9205982219999297
Mean absolute error: 0.03896628974107015
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.06482016547239368
Kappa statistic: 0.7578617412113654
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 18.782479584261292
Incorrectly Classified Instances: 11.35857461024499
Correctly Classified Instances: 88.64142538975501
Weighted Precision: 0.908673138115665
Weighted AreaUnderROC: 0.927000133042598
Root mean squared error: 0.1849411207733992
Relative absolute error: 14.685521337424369
Root relative squared error: 49.62491014611854
Weighted TruePositiveRate: 0.8864142538975501
Weighted MatthewsCorrelation: 0.7531842030561273
Weighted FMeasure: 0.8914485809431094
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9043773686010667
Mean absolute error: 0.040793114826178395
Coverage of cases: 92.20489977728285
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8864142538975501
Weighted FalsePositiveRate: 0.06546965332678131
Kappa statistic: 0.7453261413557248
Training time: 0.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 18.930957683741656
Incorrectly Classified Instances: 11.35857461024499
Correctly Classified Instances: 88.64142538975501
Weighted Precision: 0.908673138115665
Weighted AreaUnderROC: 0.928690493761121
Root mean squared error: 0.1843042159292217
Relative absolute error: 14.692197100862025
Root relative squared error: 49.454010642904954
Weighted TruePositiveRate: 0.8864142538975501
Weighted MatthewsCorrelation: 0.7531842030561273
Weighted FMeasure: 0.8914485809431094
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9052346995546787
Mean absolute error: 0.04081165861350522
Coverage of cases: 92.20489977728285
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8864142538975501
Weighted FalsePositiveRate: 0.06546965332678131
Kappa statistic: 0.7453261413557248
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 19.153674832962192
Incorrectly Classified Instances: 10.913140311804009
Correctly Classified Instances: 89.08685968819599
Weighted Precision: 0.9122043619976752
Weighted AreaUnderROC: 0.9352102847010254
Root mean squared error: 0.1830892267856754
Relative absolute error: 14.79901735276153
Root relative squared error: 49.127994844877705
Weighted TruePositiveRate: 0.89086859688196
Weighted MatthewsCorrelation: 0.763626841356689
Weighted FMeasure: 0.8952975215039976
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9129895509163058
Mean absolute error: 0.04110838153544828
Coverage of cases: 91.98218262806236
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 13.0
Weighted Recall: 0.89086859688196
Weighted FalsePositiveRate: 0.05824719958766542
Kappa statistic: 0.7552344080279466
Training time: 0.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 19.30215293244252
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.9137390300342401
Weighted AreaUnderROC: 0.9344322958733208
Root mean squared error: 0.18427606492477253
Relative absolute error: 14.953841388169229
Root relative squared error: 49.446456935754966
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7679067771932615
Weighted FMeasure: 0.8975667981174611
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9128402087551121
Mean absolute error: 0.04153844830046966
Coverage of cases: 91.98218262806236
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.05780537341087265
Kappa statistic: 0.7595233313248979
Training time: 0.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 19.413511507052764
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.9137390300342401
Weighted AreaUnderROC: 0.9370024691451568
Root mean squared error: 0.18229212704636139
Relative absolute error: 14.980789339674887
Root relative squared error: 48.914110540643286
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7679067771932615
Weighted FMeasure: 0.8975667981174611
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9153920899330075
Mean absolute error: 0.04161330372131872
Coverage of cases: 92.65033407572383
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.05780537341087265
Kappa statistic: 0.7595233313248979
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 19.487750556792925
Incorrectly Classified Instances: 10.46770601336303
Correctly Classified Instances: 89.53229398663697
Weighted Precision: 0.9143773627668702
Weighted AreaUnderROC: 0.9386863252740414
Root mean squared error: 0.18053666372719432
Relative absolute error: 14.76505314592211
Root relative squared error: 48.44307030300348
Weighted TruePositiveRate: 0.8953229398663697
Weighted MatthewsCorrelation: 0.7715788949790374
Weighted FMeasure: 0.8992446251694713
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9169846091560636
Mean absolute error: 0.0410140365164499
Coverage of cases: 93.0957683741648
Instances selection time: 1.0
Test time: 27.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8953229398663697
Weighted FalsePositiveRate: 0.057701542572075215
Kappa statistic: 0.7636842105263157
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 19.413511507052764
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9224951015989685
Weighted AreaUnderROC: 0.9407704877378638
Root mean squared error: 0.1654943895214384
Relative absolute error: 12.776122487437464
Root relative squared error: 44.40680458377204
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.8071459440740497
Weighted FMeasure: 0.9151126299662539
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9164112669430309
Mean absolute error: 0.03548922913177038
Coverage of cases: 93.76391982182628
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 20.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.056767065022898335
Kappa statistic: 0.8025277192657576
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 19.33927245731259
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.933638125972327
Weighted AreaUnderROC: 0.9418719148687561
Root mean squared error: 0.1563156512664431
Relative absolute error: 11.861773968787476
Root relative squared error: 41.94389066147035
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.816684359952367
Weighted FMeasure: 0.921390732881428
Iteration time: 2.0
Weighted AreaUnderPRC: 0.926399041925093
Mean absolute error: 0.03294937213552044
Coverage of cases: 94.65478841870824
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.056221408007308134
Kappa statistic: 0.8123824778884323
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.19079435783227
Incorrectly Classified Instances: 8.240534521158128
Correctly Classified Instances: 91.75946547884188
Weighted Precision: 0.9324797309788111
Weighted AreaUnderROC: 0.9409938372065891
Root mean squared error: 0.15816684648985987
Relative absolute error: 11.888024122757937
Root relative squared error: 42.44061846457229
Weighted TruePositiveRate: 0.9175946547884187
Weighted MatthewsCorrelation: 0.8123299602739387
Weighted FMeasure: 0.9194250453555721
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9230821054189087
Mean absolute error: 0.03302228922988283
Coverage of cases: 94.43207126948775
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9175946547884187
Weighted FalsePositiveRate: 0.056494236515103234
Kappa statistic: 0.807824447349243
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 18.9309576837417
Incorrectly Classified Instances: 14.476614699331849
Correctly Classified Instances: 85.52338530066815
Weighted Precision: 0.9161629016862869
Weighted AreaUnderROC: 0.9084747522550584
Root mean squared error: 0.2136992055137497
Relative absolute error: 19.279094996424902
Root relative squared error: 57.34151403197335
Weighted TruePositiveRate: 0.8552338530066815
Weighted MatthewsCorrelation: 0.7136515916293776
Weighted FMeasure: 0.87314452107691
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9031701493456823
Mean absolute error: 0.053553041656735315
Coverage of cases: 87.75055679287306
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 26.0
Weighted Recall: 0.8552338530066815
Weighted FalsePositiveRate: 0.05940150000143131
Kappa statistic: 0.6942794590574359
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.11655530809211
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.915025062236719
Weighted AreaUnderROC: 0.9092719825383221
Root mean squared error: 0.21508191897003814
Relative absolute error: 19.6156248720708
Root relative squared error: 57.712534985773104
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.7101163060893534
Weighted FMeasure: 0.8710791301421399
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9036539205155582
Mean absolute error: 0.054487846866862796
Coverage of cases: 87.75055679287306
Instances selection time: 1.0
Test time: 25.0
Accumulative iteration time: 28.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.05967432850922641
Kappa statistic: 0.6905259200467855
Training time: 1.0
		
Time end:Tue Oct 31 11.10.54 EET 2017