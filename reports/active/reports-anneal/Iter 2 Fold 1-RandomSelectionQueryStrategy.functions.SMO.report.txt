Tue Oct 31 11.11.02 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.11.02 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9244669111399498
Weighted AreaUnderROC: 0.9130208936277773
Root mean squared error: 0.31353213614546893
Relative absolute error: 80.73051224944456
Root relative squared error: 84.1295003462389
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8007390770112465
Weighted FMeasure: 0.9220332380813614
Iteration time: 89.0
Weighted AreaUnderPRC: 0.8979492991001212
Mean absolute error: 0.2242514229151215
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.11088294965248138
Kappa statistic: 0.8077487705218861
Training time: 88.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9405027044225263
Weighted AreaUnderROC: 0.9188654873862768
Root mean squared error: 0.31341109040609866
Relative absolute error: 80.69487750556928
Root relative squared error: 84.09702036604466
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8318679250194657
Weighted FMeasure: 0.9320922805395638
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9098145538780285
Mean absolute error: 0.224152437515468
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 178.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.10308992874404874
Kappa statistic: 0.832704058820607
Training time: 88.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9444385717427466
Weighted AreaUnderROC: 0.9133644630063317
Root mean squared error: 0.3131741264696996
Relative absolute error: 80.64142538975636
Root relative squared error: 84.03343626964416
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.845447300848048
Weighted FMeasure: 0.9374841400878643
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9104609131670546
Mean absolute error: 0.22400395941598766
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 263.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.12277123241327112
Kappa statistic: 0.8443534305669679
Training time: 85.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9438642574156482
Weighted AreaUnderROC: 0.9108904980262228
Root mean squared error: 0.3130160508510799
Relative absolute error: 80.6057906458811
Root relative squared error: 83.99102013018589
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.843006340451019
Weighted FMeasure: 0.9395298652760277
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9115258797841794
Mean absolute error: 0.22390497401633414
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 359.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.12984981627552747
Kappa statistic: 0.8494337967859262
Training time: 95.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9566676266109221
Weighted AreaUnderROC: 0.9493330000435705
Root mean squared error: 0.31221918835782075
Relative absolute error: 80.42761692650467
Root relative squared error: 83.7771994857482
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8732536056209464
Weighted FMeasure: 0.9496077673714675
Iteration time: 99.0
Weighted AreaUnderPRC: 0.936170591049715
Mean absolute error: 0.22341004701806633
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 458.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.0671776810324388
Kappa statistic: 0.878978706721227
Training time: 98.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9651130576315794
Weighted AreaUnderROC: 0.95579967876712
Root mean squared error: 0.31187025138601876
Relative absolute error: 80.35634743875416
Root relative squared error: 83.68356987109073
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8998875726810761
Weighted FMeasure: 0.9600796625099679
Iteration time: 106.0
Weighted AreaUnderPRC: 0.9466905998311778
Mean absolute error: 0.22321207621875933
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 564.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.0657822101726613
Kappa statistic: 0.9047803198522991
Training time: 105.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9663810935813235
Weighted AreaUnderROC: 0.9583079307415688
Root mean squared error: 0.31187025138601876
Relative absolute error: 80.35634743875417
Root relative squared error: 83.68356987109073
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.9013443931142078
Weighted FMeasure: 0.9611725103222211
Iteration time: 103.0
Weighted AreaUnderPRC: 0.949057908599087
Mean absolute error: 0.22321207621875935
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 667.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.05868360679137388
Kappa statistic: 0.9055578926529906
Training time: 103.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9763870995055334
Weighted AreaUnderROC: 0.9639907836703647
Root mean squared error: 0.3114732576007845
Relative absolute error: 80.26726057906598
Root relative squared error: 83.57704526023927
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9296235369582214
Weighted FMeasure: 0.9720466038420282
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9612396333714034
Mean absolute error: 0.22296461271962548
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 765.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05728813593159638
Kappa statistic: 0.9321085658123536
Training time: 98.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9763870995055334
Weighted AreaUnderROC: 0.9639907836703647
Root mean squared error: 0.3114732576007845
Relative absolute error: 80.26726057906598
Root relative squared error: 83.57704526023927
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9296235369582214
Weighted FMeasure: 0.9720466038420282
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9612396333714034
Mean absolute error: 0.22296461271962548
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 863.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05728813593159638
Kappa statistic: 0.9321085658123536
Training time: 98.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9763870995055334
Weighted AreaUnderROC: 0.9639881878993948
Root mean squared error: 0.3114679609319759
Relative absolute error: 80.26726057906598
Root relative squared error: 83.57562401485802
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9296235369582214
Weighted FMeasure: 0.9720466038420282
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9612373768653322
Mean absolute error: 0.22296461271962548
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 976.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05728813593159638
Kappa statistic: 0.9321085658123536
Training time: 112.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9643470293514141
Weighted AreaUnderROC: 0.9415529100181117
Root mean squared error: 0.31169563641469156
Relative absolute error: 80.32071269487888
Root relative squared error: 83.63671575759751
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8983161652819657
Weighted FMeasure: 0.960245705674354
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9419942546555996
Mean absolute error: 0.2231130908191058
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 1079.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.0999798538144761
Kappa statistic: 0.9012190702971322
Training time: 102.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9643470293514141
Weighted AreaUnderROC: 0.9415595263065237
Root mean squared error: 0.31169563641469156
Relative absolute error: 80.32071269487888
Root relative squared error: 83.63671575759751
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8983161652819657
Weighted FMeasure: 0.960245705674354
Iteration time: 114.0
Weighted AreaUnderPRC: 0.9420484077154733
Mean absolute error: 0.2231130908191058
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 1193.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.0999798538144761
Kappa statistic: 0.9012190702971322
Training time: 114.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.962048500080875
Weighted AreaUnderROC: 0.9310523674026822
Root mean squared error: 0.3117856017946145
Relative absolute error: 80.33853006681653
Root relative squared error: 83.66085600221302
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.892020313931188
Weighted FMeasure: 0.9572744207042649
Iteration time: 114.0
Weighted AreaUnderPRC: 0.9317981159347488
Mean absolute error: 0.22316258351893262
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 1307.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12103660882444495
Kappa statistic: 0.893329461763852
Training time: 114.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9763870995055334
Weighted AreaUnderROC: 0.9659673352335576
Root mean squared error: 0.311298419945919
Relative absolute error: 80.2316258351907
Root relative squared error: 83.53013139448257
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9296235369582214
Weighted FMeasure: 0.9720466038420282
Iteration time: 253.0
Weighted AreaUnderPRC: 0.9633933389866128
Mean absolute error: 0.22286562731997195
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 1560.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05728813593159638
Kappa statistic: 0.9321085658123536
Training time: 253.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9662387183100171
Weighted AreaUnderROC: 0.9577062938481332
Root mean squared error: 0.31155799205540086
Relative absolute error: 80.2850779510036
Root relative squared error: 83.5997819003063
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9056615554972001
Weighted FMeasure: 0.962641656669576
Iteration time: 255.0
Weighted AreaUnderPRC: 0.9498505619597389
Mean absolute error: 0.22301410541945224
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1815.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.06506877801462327
Kappa statistic: 0.9101393440572385
Training time: 254.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9662387183100171
Weighted AreaUnderROC: 0.9577791452520839
Root mean squared error: 0.31155799205540086
Relative absolute error: 80.2850779510036
Root relative squared error: 83.5997819003063
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9056615554972001
Weighted FMeasure: 0.962641656669576
Iteration time: 248.0
Weighted AreaUnderPRC: 0.9498781652344975
Mean absolute error: 0.22301410541945224
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 2063.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.06506877801462327
Kappa statistic: 0.9101393440572385
Training time: 248.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9710678721400221
Weighted AreaUnderROC: 0.9623976854418443
Root mean squared error: 0.31139379810879003
Relative absolute error: 80.24944320712832
Root relative squared error: 83.55572404117241
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9174588097202617
Weighted FMeasure: 0.967254635278816
Iteration time: 254.0
Weighted AreaUnderPRC: 0.9560765822402775
Mean absolute error: 0.22291512001979868
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 2317.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.05810539892843183
Kappa statistic: 0.9213266583229036
Training time: 254.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9675898107787567
Weighted AreaUnderROC: 0.9613466650539799
Root mean squared error: 0.3114785541795236
Relative absolute error: 80.26726057906596
Root relative squared error: 83.57846648145232
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9115117344287338
Weighted FMeasure: 0.9651076314643366
Iteration time: 266.0
Weighted AreaUnderPRC: 0.9535337924950352
Mean absolute error: 0.22296461271962545
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 2583.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.058209229767229254
Kappa statistic: 0.9160454738101769
Training time: 266.0
		
Time end:Tue Oct 31 11.11.06 EET 2017