Tue Oct 31 11.09.20 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.09.20 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 31.66295471417958
Incorrectly Classified Instances: 12.249443207126948
Correctly Classified Instances: 87.75055679287306
Weighted Precision: 0.8816525297576449
Weighted AreaUnderROC: 0.9678917429727389
Root mean squared error: 0.1694804471447079
Relative absolute error: 24.962455272746354
Root relative squared error: 45.47637608071546
Weighted TruePositiveRate: 0.8775055679287305
Weighted MatthewsCorrelation: 0.674105033965281
Weighted FMeasure: 0.8583128501575279
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9670418245194965
Mean absolute error: 0.06934015353540585
Coverage of cases: 99.10913140311804
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8775055679287305
Weighted FalsePositiveRate: 0.3436475584189748
Kappa statistic: 0.6277341453487495
Training time: 3.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 28.470675575352598
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9672103331306762
Weighted AreaUnderROC: 0.9742984506326994
Root mean squared error: 0.1101808893017682
Relative absolute error: 13.94187946938682
Root relative squared error: 29.56463499601611
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9097280865110038
Weighted FMeasure: 0.9643885470229194
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9817122761798714
Mean absolute error: 0.038727442970518555
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.09310028604805104
Kappa statistic: 0.9127364602228558
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 27.09725315515956
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9535862382950449
Weighted AreaUnderROC: 0.9855493431980987
Root mean squared error: 0.11859619990191003
Relative absolute error: 14.538157706658366
Root relative squared error: 31.822699782459125
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8667014625481632
Weighted FMeasure: 0.9467066468272851
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9879581926892731
Mean absolute error: 0.04038377140738395
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14951110042571883
Kappa statistic: 0.8678740536636259
Training time: 6.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 28.804751299183383
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9721070297060761
Weighted AreaUnderROC: 0.9715885784648746
Root mean squared error: 0.11295899618817086
Relative absolute error: 14.342537098128943
Root relative squared error: 30.31007929762691
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9219495610436177
Weighted FMeasure: 0.9691536068854818
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9754282792070086
Mean absolute error: 0.039840380828135555
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.09254209770414004
Kappa statistic: 0.9240330053620698
Training time: 6.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 26.688938381588702
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9797932603853975
Weighted AreaUnderROC: 0.9885212083796246
Root mean squared error: 0.09678359630519377
Relative absolute error: 11.905916175586867
Root relative squared error: 25.96976405343742
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9463037075380122
Weighted FMeasure: 0.9797888585504235
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9903345406596339
Mean absolute error: 0.033071989376629855
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.04354901991777729
Kappa statistic: 0.9493139001078695
Training time: 7.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 23.38530066815145
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.980065361562608
Weighted AreaUnderROC: 0.983507751540665
Root mean squared error: 0.08967270558887092
Relative absolute error: 9.1742921729378
Root relative squared error: 24.061711850765
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9462951059475041
Weighted FMeasure: 0.9794590790568516
Iteration time: 9.0
Weighted AreaUnderPRC: 0.98563669296278
Mean absolute error: 0.02548414492482697
Coverage of cases: 99.55456570155901
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 43.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.05722807737450321
Kappa statistic: 0.9485301609944977
Training time: 7.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 24.758723088344443
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9844086833177572
Weighted AreaUnderROC: 0.9927477770670876
Root mean squared error: 0.08611263687638977
Relative absolute error: 9.121153078021912
Root relative squared error: 23.10644517328364
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9582295017348578
Weighted FMeasure: 0.9840288876564458
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9929917806410199
Mean absolute error: 0.02533653632783839
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 67.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.04299083157386629
Kappa statistic: 0.9602785430831839
Training time: 21.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 20.6384558277654
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9868939523727943
Weighted AreaUnderROC: 0.9992818537225576
Root mean squared error: 0.06782134336387619
Relative absolute error: 5.385576086734633
Root relative squared error: 18.198376090437858
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9646783306506868
Weighted FMeasure: 0.9866070915530673
Iteration time: 22.0
Weighted AreaUnderPRC: 0.999376593194435
Mean absolute error: 0.01495993357426272
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.0221931512168219
Kappa statistic: 0.9665640669215112
Training time: 20.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 22.49443207126947
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.995716977899606
Weighted AreaUnderROC: 0.9999604051445768
Root mean squared error: 0.05730775472128769
Relative absolute error: 5.834282376574676
Root relative squared error: 15.377284223362132
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9883332353379165
Weighted FMeasure: 0.9955827957501505
Iteration time: 25.0
Weighted AreaUnderPRC: 0.9999013341492989
Mean absolute error: 0.016206339934929495
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 114.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 5.581883439110024E-4
Kappa statistic: 0.9889430654058313
Training time: 22.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.08611729769856
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955715543585228
Weighted AreaUnderROC: 0.9998959265657847
Root mean squared error: 0.06343646920540313
Relative absolute error: 6.228944255246164
Root relative squared error: 17.0217908875025
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882671875643789
Weighted FMeasure: 0.9954349306747164
Iteration time: 29.0
Weighted AreaUnderPRC: 0.9999352566426685
Mean absolute error: 0.017302622931239173
Coverage of cases: 100.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 143.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.014237245800636927
Kappa statistic: 0.9887733159973995
Training time: 27.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.160356347438718
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9910847075661465
Weighted AreaUnderROC: 0.9984501924890853
Root mean squared error: 0.06857001587227748
Relative absolute error: 6.674442560608269
Root relative squared error: 18.39926600503833
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.9762802354334699
Weighted FMeasure: 0.991042001950597
Iteration time: 19.0
Weighted AreaUnderPRC: 0.9980652771394424
Mean absolute error: 0.018540118223911675
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 162.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.021634962872910897
Kappa statistic: 0.9775550501137246
Training time: 19.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 21.417965850037078
Incorrectly Classified Instances: 0.89086859688196
Correctly Classified Instances: 99.10913140311804
Weighted Precision: 0.9910847075661465
Weighted AreaUnderROC: 0.9996862668504409
Root mean squared error: 0.06073134177125088
Relative absolute error: 5.421748241840401
Root relative squared error: 16.295929027834813
Weighted TruePositiveRate: 0.9910913140311804
Weighted MatthewsCorrelation: 0.9762802354334699
Weighted FMeasure: 0.991042001950597
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9997133937700151
Mean absolute error: 0.015060411782889853
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 173.0
Weighted Recall: 0.9910913140311804
Weighted FalsePositiveRate: 0.021634962872910897
Kappa statistic: 0.9775550501137246
Training time: 11.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 22.123236822568657
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.997816498537054
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.06010868573655918
Relative absolute error: 6.101519939148204
Root relative squared error: 16.12885288141428
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.9941208001091942
Weighted FMeasure: 0.9977822236762314
Iteration time: 13.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.01694866649763373
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 186.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 2.790941719555012E-4
Kappa statistic: 0.9944515842024617
Training time: 12.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 22.234595397178875
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.995716977899606
Weighted AreaUnderROC: 0.9997542927944092
Root mean squared error: 0.06109799237303712
Relative absolute error: 6.2520499305213155
Root relative squared error: 16.394311708185043
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9883332353379165
Weighted FMeasure: 0.9955827957501505
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9997978055175331
Mean absolute error: 0.017366805362559037
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 200.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 5.581883439110024E-4
Kappa statistic: 0.9889430654058313
Training time: 13.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 20.6384558277654
Incorrectly Classified Instances: 1.55902004454343
Correctly Classified Instances: 98.44097995545657
Weighted Precision: 0.9845939731562229
Weighted AreaUnderROC: 0.999549458539048
Root mean squared error: 0.06766949537131801
Relative absolute error: 5.430832883187244
Root relative squared error: 18.157630998405025
Weighted TruePositiveRate: 0.9844097995545658
Weighted MatthewsCorrelation: 0.9583699350611097
Weighted FMeasure: 0.9836173060482463
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9995106934792191
Mean absolute error: 0.015085646897742194
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 212.0
Weighted Recall: 0.9844097995545658
Weighted FalsePositiveRate: 0.03615130284550332
Kappa statistic: 0.9605170594442491
Training time: 12.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 20.489977728285005
Incorrectly Classified Instances: 1.3363028953229399
Correctly Classified Instances: 98.66369710467706
Weighted Precision: 0.9872706716935723
Weighted AreaUnderROC: 0.9990113545420908
Root mean squared error: 0.0647516588305529
Relative absolute error: 4.951678823045561
Root relative squared error: 17.374693296118984
Weighted TruePositiveRate: 0.9866369710467706
Weighted MatthewsCorrelation: 0.9650732853500429
Weighted FMeasure: 0.9866010608880067
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9985135723241634
Mean absolute error: 0.013754663397348642
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 224.0
Weighted Recall: 0.9866369710467706
Weighted FalsePositiveRate: 0.015353622488458934
Kappa statistic: 0.966798126694602
Training time: 11.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 22.791388270230094
Incorrectly Classified Instances: 0.44543429844098
Correctly Classified Instances: 99.55456570155901
Weighted Precision: 0.9955715543585228
Weighted AreaUnderROC: 0.9998699203370018
Root mean squared error: 0.06486657009935907
Relative absolute error: 6.650613528387769
Root relative squared error: 17.405527225130747
Weighted TruePositiveRate: 0.9955456570155902
Weighted MatthewsCorrelation: 0.9882671875643789
Weighted FMeasure: 0.9954349306747164
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9998548891237432
Mean absolute error: 0.01847392646774362
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 238.0
Weighted Recall: 0.9955456570155902
Weighted FalsePositiveRate: 0.014237245800636927
Kappa statistic: 0.9887733159973995
Training time: 13.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.083890126206313
Incorrectly Classified Instances: 0.22271714922049
Correctly Classified Instances: 99.77728285077951
Weighted Precision: 0.9977793217191426
Weighted AreaUnderROC: 1.0
Root mean squared error: 0.05993609309665715
Relative absolute error: 5.428611124271942
Root relative squared error: 16.08254141638583
Weighted TruePositiveRate: 0.9977728285077951
Weighted MatthewsCorrelation: 0.9941480107478113
Weighted FMeasure: 0.997745900699748
Iteration time: 14.0
Weighted AreaUnderPRC: 1.0
Mean absolute error: 0.015079475345199688
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 252.0
Weighted Recall: 0.9977728285077951
Weighted FalsePositiveRate: 0.007118622900318464
Kappa statistic: 0.9944091644876106
Training time: 13.0
		
Time end:Tue Oct 31 11.09.22 EET 2017