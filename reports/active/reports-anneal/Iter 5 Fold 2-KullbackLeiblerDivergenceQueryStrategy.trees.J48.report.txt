Tue Oct 31 11.07.49 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.07.49 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 17.743132887899023
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9291689293036557
Weighted AreaUnderROC: 0.92211725186921
Root mean squared error: 0.15102279906305227
Relative absolute error: 9.977728285078047
Root relative squared error: 40.52366938287337
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8291385458388747
Weighted FMeasure: 0.9157206145988515
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9118716821675379
Mean absolute error: 0.0277159119029943
Coverage of cases: 92.65033407572383
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.08482784869727708
Kappa statistic: 0.8061432184049838
Training time: 2.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9586557466290205
Weighted AreaUnderROC: 0.9742855605746998
Root mean squared error: 0.10630537675542921
Relative absolute error: 5.4342984409800055
Root relative squared error: 28.52472585586403
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.904170043400984
Weighted FMeasure: 0.9573443181484615
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9707395417076161
Mean absolute error: 0.015095273447166532
Coverage of cases: 96.43652561247217
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.01852748921876688
Kappa statistic: 0.9037650925198258
Training time: 3.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.956262451172254
Weighted AreaUnderROC: 0.9696126633784382
Root mean squared error: 0.10974162211538238
Relative absolute error: 5.701559020044597
Root relative squared error: 29.446767241330857
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8979467719751015
Weighted FMeasure: 0.9550987320848953
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9664232688845207
Mean absolute error: 0.015837663944568165
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.025646112119085344
Kappa statistic: 0.89802773129333
Training time: 2.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.956262451172254
Weighted AreaUnderROC: 0.9696126633784382
Root mean squared error: 0.10974162211538238
Relative absolute error: 5.701559020044597
Root relative squared error: 29.446767241330857
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8979467719751015
Weighted FMeasure: 0.9550987320848953
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9664232688845207
Mean absolute error: 0.015837663944568165
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.025646112119085344
Kappa statistic: 0.89802773129333
Training time: 2.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.956262451172254
Weighted AreaUnderROC: 0.9696126633784382
Root mean squared error: 0.10974162211538238
Relative absolute error: 5.701559020044597
Root relative squared error: 29.446767241330857
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8979467719751015
Weighted FMeasure: 0.9550987320848953
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9664232688845207
Mean absolute error: 0.015837663944568165
Coverage of cases: 96.21380846325167
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.025646112119085344
Kappa statistic: 0.89802773129333
Training time: 3.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 21.640682999257642
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9300683048915837
Weighted AreaUnderROC: 0.9491392112500102
Root mean squared error: 0.141893992998209
Relative absolute error: 10.170749814402464
Root relative squared error: 38.07415367314513
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8182884662061377
Weighted FMeasure: 0.9204283939368085
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9347211800887086
Mean absolute error: 0.028252082817784345
Coverage of cases: 96.43652561247217
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.11808437898442795
Kappa statistic: 0.8241484738225188
Training time: 4.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9300683048915837
Weighted AreaUnderROC: 0.9491392112500102
Root mean squared error: 0.1421957089609894
Relative absolute error: 9.990454979319221
Root relative squared error: 38.155112561465984
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8182884662061377
Weighted FMeasure: 0.9204283939368085
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9347211800887086
Mean absolute error: 0.027751263831442004
Coverage of cases: 93.54120267260579
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.11808437898442795
Kappa statistic: 0.8241484738225188
Training time: 4.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 17.520415738678526
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9300683048915837
Weighted AreaUnderROC: 0.9491392112500102
Root mean squared error: 0.1425190233911803
Relative absolute error: 9.817371937639301
Root relative squared error: 38.2418669267476
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8182884662061377
Weighted FMeasure: 0.9204283939368085
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9347211800887086
Mean absolute error: 0.027270477604553346
Coverage of cases: 93.54120267260579
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.11808437898442795
Kappa statistic: 0.8241484738225188
Training time: 4.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 17.483296213808458
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9400781146232962
Weighted AreaUnderROC: 0.9556869609959875
Root mean squared error: 0.13686849583399704
Relative absolute error: 8.592363061231163
Root relative squared error: 36.72567127955588
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.827628513782622
Weighted FMeasure: 0.9293894994399178
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9336636424743369
Mean absolute error: 0.023867675170086325
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 40.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.15144474011037626
Kappa statistic: 0.834648569023569
Training time: 9.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 17.297698589458033
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9393833803255871
Weighted AreaUnderROC: 0.9568432679234525
Root mean squared error: 0.13001217965797984
Relative absolute error: 8.116449252306788
Root relative squared error: 34.88592859417916
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8509291528281447
Weighted FMeasure: 0.9326318029246045
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9397813057539406
Mean absolute error: 0.02254569236751863
Coverage of cases: 94.87750556792874
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 49.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.11640981395269494
Kappa statistic: 0.8549741602067183
Training time: 9.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 17.297698589458033
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9393833803255871
Weighted AreaUnderROC: 0.9568432679234525
Root mean squared error: 0.13010688649698943
Relative absolute error: 8.075195349364018
Root relative squared error: 34.91134110577431
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8509291528281447
Weighted FMeasure: 0.9326318029246045
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9397813057539406
Mean absolute error: 0.022431098192677603
Coverage of cases: 94.87750556792874
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.11640981395269494
Kappa statistic: 0.8549741602067183
Training time: 5.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 17.297698589458033
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9650965945883642
Weighted AreaUnderROC: 0.976504947265002
Root mean squared error: 0.09262145883455464
Relative absolute error: 4.0979955456570565
Root relative squared error: 24.852945375511638
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.9284874521590248
Weighted FMeasure: 0.9673297525537872
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9713114265510752
Mean absolute error: 0.011383320960158375
Coverage of cases: 97.55011135857461
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 60.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.02397154708735234
Kappa statistic: 0.9287371196952677
Training time: 5.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.634001484781013
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9643552590629885
Weighted AreaUnderROC: 0.9846730119045445
Root mean squared error: 0.0967356521308131
Relative absolute error: 5.038356842365801
Root relative squared error: 25.956899281472587
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9302173381113071
Weighted FMeasure: 0.9654381790186762
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9708696631199427
Mean absolute error: 0.013995435673238197
Coverage of cases: 97.32739420935413
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 69.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.017411112530944874
Kappa statistic: 0.9235242590880335
Training time: 8.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 17.297698589458033
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9393833803255871
Weighted AreaUnderROC: 0.9568432679234525
Root mean squared error: 0.13041888698131687
Relative absolute error: 7.945042190783987
Root relative squared error: 34.9950596208106
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8509291528281447
Weighted FMeasure: 0.9326318029246045
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9397813057539406
Mean absolute error: 0.02206956164106641
Coverage of cases: 94.87750556792874
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 80.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.11640981395269494
Kappa statistic: 0.8549741602067183
Training time: 11.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 17.297698589458033
Incorrectly Classified Instances: 12.026726057906458
Correctly Classified Instances: 87.97327394209354
Weighted Precision: 0.8469651273095674
Weighted AreaUnderROC: 0.7868180151591007
Root mean squared error: 0.19338261690755243
Relative absolute error: 17.53872645637822
Root relative squared error: 51.89000124865074
Weighted TruePositiveRate: 0.8797327394209354
Weighted MatthewsCorrelation: 0.64703977975595
Weighted FMeasure: 0.851503851662523
Iteration time: 9.0
Weighted AreaUnderPRC: 0.8043866677136796
Mean absolute error: 0.04871868460105013
Coverage of cases: 88.41870824053453
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 89.0
Weighted Recall: 0.8797327394209354
Weighted FalsePositiveRate: 0.3296894067902934
Kappa statistic: 0.6470947834187238
Training time: 8.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 20.081662954714126
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9642496739394146
Weighted AreaUnderROC: 0.9791884553758029
Root mean squared error: 0.0965200775036217
Relative absolute error: 5.819917276487483
Root relative squared error: 25.899054538997685
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9296153242111845
Weighted FMeasure: 0.9654155072773243
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9700002019162594
Mean absolute error: 0.016166436879131735
Coverage of cases: 97.55011135857461
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 100.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.024425904592465908
Kappa statistic: 0.9229203453011575
Training time: 11.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9556962562307774
Weighted AreaUnderROC: 0.968072264105173
Root mean squared error: 0.10783532358182718
Relative absolute error: 4.8898787428854735
Root relative squared error: 28.935253668558346
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9001119031315912
Weighted FMeasure: 0.9597724123693966
Iteration time: 11.0
Weighted AreaUnderPRC: 0.95256583911637
Mean absolute error: 0.013582996508015067
Coverage of cases: 96.43652561247217
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 111.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.08653985149164359
Kappa statistic: 0.9079587966996361
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 17.00074239049738
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9577432786929067
Weighted AreaUnderROC: 0.944770496970381
Root mean squared error: 0.10481156230981416
Relative absolute error: 4.575980812061034
Root relative squared error: 28.12389337832406
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9131872854125318
Weighted FMeasure: 0.9618300243044203
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9435288850739063
Mean absolute error: 0.012711057811280523
Coverage of cases: 96.65924276169265
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 123.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.07942122859132512
Kappa statistic: 0.9133873456790125
Training time: 12.0
		
Time end:Tue Oct 31 11.07.49 EET 2017