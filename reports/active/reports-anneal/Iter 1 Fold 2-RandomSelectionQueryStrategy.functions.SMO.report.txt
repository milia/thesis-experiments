Tue Oct 31 11.10.59 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.10.59 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9261830466140698
Weighted AreaUnderROC: 0.8952602927917803
Root mean squared error: 0.31022868579845314
Relative absolute error: 78.11644925230704
Root relative squared error: 83.24309160189219
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8188423775083905
Weighted FMeasure: 0.9277762304273446
Iteration time: 53.0
Weighted AreaUnderPRC: 0.894042418373463
Mean absolute error: 0.2169901368119618
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.15838809967753667
Kappa statistic: 0.8282144647616033
Training time: 52.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9401325152366112
Weighted AreaUnderROC: 0.9390998710654238
Root mean squared error: 0.3092565151128612
Relative absolute error: 77.90645879732767
Root relative squared error: 82.98223083324623
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8638578463052564
Weighted FMeasure: 0.9398541912701347
Iteration time: 52.0
Weighted AreaUnderPRC: 0.9206706125162105
Mean absolute error: 0.2164068299925747
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 105.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.08859734153412953
Kappa statistic: 0.8641143981236286
Training time: 51.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.013363028953229
Correctly Classified Instances: 93.98663697104676
Weighted Precision: 0.9322760668663441
Weighted AreaUnderROC: 0.9268305997869536
Root mean squared error: 0.3095258490514238
Relative absolute error: 77.96372892141297
Root relative squared error: 83.05450071267909
Weighted TruePositiveRate: 0.9398663697104677
Weighted MatthewsCorrelation: 0.8435007302563017
Weighted FMeasure: 0.9301777335669076
Iteration time: 56.0
Weighted AreaUnderPRC: 0.9067645777435019
Mean absolute error: 0.21656591367058942
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 161.0
Weighted Recall: 0.9398663697104677
Weighted FalsePositiveRate: 0.1099532102350849
Kappa statistic: 0.8452435661764705
Training time: 55.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9184256259701694
Weighted AreaUnderROC: 0.911606961017741
Root mean squared error: 0.30986829912350017
Relative absolute error: 78.04008908686002
Root relative squared error: 83.1463897094866
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.813768178979014
Weighted FMeasure: 0.9148044179333693
Iteration time: 60.0
Weighted AreaUnderPRC: 0.8916190735822399
Mean absolute error: 0.21677802524127568
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 221.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.13842770183635877
Kappa statistic: 0.8193463814764822
Training time: 60.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 66.66666666666661
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9184256259701694
Weighted AreaUnderROC: 0.911606961017741
Root mean squared error: 0.3098805224839563
Relative absolute error: 78.04008908686002
Root relative squared error: 83.14966958127381
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.813768178979014
Weighted FMeasure: 0.9148044179333693
Iteration time: 56.0
Weighted AreaUnderPRC: 0.8916190735822399
Mean absolute error: 0.21677802524127568
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 277.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.13842770183635877
Kappa statistic: 0.8193463814764822
Training time: 56.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9183535073980514
Weighted AreaUnderROC: 0.899533047089998
Root mean squared error: 0.3134847759876782
Relative absolute error: 80.71269487750685
Root relative squared error: 84.11679228236949
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7886457679292398
Weighted FMeasure: 0.9085829480439404
Iteration time: 92.0
Weighted AreaUnderPRC: 0.8806274105513678
Mean absolute error: 0.22420193021529455
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 369.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.1600626647092697
Kappa statistic: 0.7953829327361268
Training time: 91.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9183535073980514
Weighted AreaUnderROC: 0.8995405544096347
Root mean squared error: 0.3134847759876782
Relative absolute error: 80.71269487750685
Root relative squared error: 84.11679228236949
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.7886457679292398
Weighted FMeasure: 0.9085829480439404
Iteration time: 95.0
Weighted AreaUnderPRC: 0.8806274105513678
Mean absolute error: 0.22420193021529455
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 464.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.1600626647092697
Kappa statistic: 0.7953829327361268
Training time: 95.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9311453209003321
Weighted AreaUnderROC: 0.9072785879549282
Root mean squared error: 0.31316359057753806
Relative absolute error: 80.64142538975632
Root relative squared error: 84.03060919311494
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8104862412361128
Weighted FMeasure: 0.923602505827156
Iteration time: 100.0
Weighted AreaUnderPRC: 0.8936235018065365
Mean absolute error: 0.22400395941598755
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 564.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.14526723056472174
Kappa statistic: 0.8204139034397337
Training time: 99.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.67706013363029
Correctly Classified Instances: 95.32293986636971
Weighted Precision: 0.9521198919862618
Weighted AreaUnderROC: 0.9381651835417868
Root mean squared error: 0.3123723858876583
Relative absolute error: 80.46325167037998
Root relative squared error: 83.8183066966324
Weighted TruePositiveRate: 0.9532293986636972
Weighted MatthewsCorrelation: 0.8778680805866528
Weighted FMeasure: 0.9451964096751255
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9224425559208186
Mean absolute error: 0.2235090324177199
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 665.0
Weighted Recall: 0.9532293986636972
Weighted FalsePositiveRate: 0.10143911647498895
Kappa statistic: 0.8783449023301423
Training time: 101.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9540431003338773
Weighted AreaUnderROC: 0.9337489319442481
Root mean squared error: 0.3121452040193976
Relative absolute error: 80.40979955456704
Root relative squared error: 83.75734740455005
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8777072383071648
Weighted FMeasure: 0.949500245814778
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9274719629592343
Mean absolute error: 0.22336055431823956
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 767.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1074814017255475
Kappa statistic: 0.8839763301377297
Training time: 102.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9618301148044004
Weighted AreaUnderROC: 0.9454001348512011
Root mean squared error: 0.3119072783582182
Relative absolute error: 80.35634743875416
Root relative squared error: 83.69350525031139
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8977490045013272
Weighted FMeasure: 0.9591778419809331
Iteration time: 109.0
Weighted AreaUnderPRC: 0.9393809944348828
Mean absolute error: 0.2232120762187593
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 876.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.08612553302459212
Kappa statistic: 0.902595579602879
Training time: 108.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9629004966216911
Weighted AreaUnderROC: 0.9530456736417178
Root mean squared error: 0.3118861206266541
Relative absolute error: 80.35634743875416
Root relative squared error: 83.68782803518816
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8997176164211867
Weighted FMeasure: 0.9606761193547727
Iteration time: 113.0
Weighted AreaUnderPRC: 0.9435031651293132
Mean absolute error: 0.22321207621875933
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 989.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.06560694683950323
Kappa statistic: 0.9047969467172221
Training time: 113.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9642349709939524
Weighted AreaUnderROC: 0.9502027222006354
Root mean squared error: 0.3118861206266541
Relative absolute error: 80.35634743875416
Root relative squared error: 83.68782803518816
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8992441155388671
Weighted FMeasure: 0.9599473954938557
Iteration time: 118.0
Weighted AreaUnderPRC: 0.941984338537733
Mean absolute error: 0.2232120762187593
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 9.0
Accumulative iteration time: 1107.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.07262173890102426
Kappa statistic: 0.9040260524065785
Training time: 118.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9599051282792932
Weighted AreaUnderROC: 0.9519529753610836
Root mean squared error: 0.3119654547211448
Relative absolute error: 80.37416481069181
Root relative squared error: 83.7091156065739
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8940092318493374
Weighted FMeasure: 0.9585820965357189
Iteration time: 255.0
Weighted AreaUnderPRC: 0.9412395202921707
Mean absolute error: 0.22326156891858612
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 7.0
Accumulative iteration time: 1362.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.06571077767830066
Kappa statistic: 0.899599990061865
Training time: 255.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9599051282792932
Weighted AreaUnderROC: 0.9519529753610836
Root mean squared error: 0.3119654547211448
Relative absolute error: 80.37416481069181
Root relative squared error: 83.7091156065739
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8940092318493374
Weighted FMeasure: 0.9585820965357189
Iteration time: 261.0
Weighted AreaUnderPRC: 0.9412395202921707
Mean absolute error: 0.22326156891858612
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 1623.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.06571077767830066
Kappa statistic: 0.899599990061865
Training time: 260.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9599051282792932
Weighted AreaUnderROC: 0.9539295070260007
Root mean squared error: 0.31167975747551274
Relative absolute error: 80.33853006681653
Root relative squared error: 83.63245499110778
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8940092318493374
Weighted FMeasure: 0.9585820965357189
Iteration time: 258.0
Weighted AreaUnderPRC: 0.9439448731126158
Mean absolute error: 0.2231625835189326
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1881.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.06571077767830066
Kappa statistic: 0.899599990061865
Training time: 257.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9659199133392995
Weighted AreaUnderROC: 0.957412912129618
Root mean squared error: 0.31144147623673074
Relative absolute error: 80.28507795100363
Root relative squared error: 83.56851742538625
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9108786763823077
Weighted FMeasure: 0.9651150838760145
Iteration time: 258.0
Weighted AreaUnderPRC: 0.9512711720965114
Mean absolute error: 0.2230141054194523
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2139.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.06487349516243415
Kappa statistic: 0.9154128256009646
Training time: 257.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 83.33333333333299
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9689142712276974
Weighted AreaUnderROC: 0.9585296741857025
Root mean squared error: 0.3113620086341417
Relative absolute error: 80.26726057906598
Root relative squared error: 83.54719403001849
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9167562609666069
Weighted FMeasure: 0.9671991686629781
Iteration time: 269.0
Weighted AreaUnderPRC: 0.9535457569135105
Mean absolute error: 0.2229646127196255
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 2408.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.06476966432363672
Kappa statistic: 0.9207313997477932
Training time: 269.0
		
Time end:Tue Oct 31 11.11.02 EET 2017