Tue Oct 31 11.13.11 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.13.11 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 28.619153674832898
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9693817679133706
Weighted AreaUnderROC: 0.9745106889634468
Root mean squared error: 0.12072925288885848
Relative absolute error: 16.377876363367314
Root relative squared error: 32.39505795987021
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9157621696736914
Weighted FMeasure: 0.9665345231143624
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9748256613448559
Mean absolute error: 0.045494101009353194
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.07227754303436504
Kappa statistic: 0.9200813679994915
Training time: 4.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 25.167037861915286
Incorrectly Classified Instances: 2.89532293986637
Correctly Classified Instances: 97.10467706013362
Weighted Precision: 0.9717555013913722
Weighted AreaUnderROC: 0.9903448500656543
Root mean squared error: 0.10610435127389506
Relative absolute error: 12.016823311216566
Root relative squared error: 28.47078505883363
Weighted TruePositiveRate: 0.9710467706013363
Weighted MatthewsCorrelation: 0.921774521179292
Weighted FMeasure: 0.968466015713634
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9881217349330772
Mean absolute error: 0.03338006475337902
Coverage of cases: 99.55456570155901
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9710467706013363
Weighted FalsePositiveRate: 0.07200471452656995
Kappa statistic: 0.9254705176332387
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 26.317743132887855
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9681107611440828
Weighted AreaUnderROC: 0.9906777033703377
Root mean squared error: 0.10870583869129946
Relative absolute error: 13.310023326898868
Root relative squared error: 29.16883738378483
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9169081652572562
Weighted FMeasure: 0.9674488949254535
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9881197262658056
Mean absolute error: 0.03697228701916316
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.058080336271790226
Kappa statistic: 0.9213148407770879
Training time: 5.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 22.60579064587967
Incorrectly Classified Instances: 3.11804008908686
Correctly Classified Instances: 96.88195991091314
Weighted Precision: 0.9694860633056624
Weighted AreaUnderROC: 0.9946713696376537
Root mean squared error: 0.09989281015161677
Relative absolute error: 9.641540939141466
Root relative squared error: 26.80405367549967
Weighted TruePositiveRate: 0.9688195991091314
Weighted MatthewsCorrelation: 0.9156366237295479
Weighted FMeasure: 0.9661703470238927
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9908126370197137
Mean absolute error: 0.026782058164281586
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9688195991091314
Weighted FalsePositiveRate: 0.08596913181941178
Kappa statistic: 0.918826431126435
Training time: 5.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 23.49665924276162
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9402340977508015
Weighted AreaUnderROC: 0.9813479014428944
Root mean squared error: 0.12026115400145573
Relative absolute error: 12.7319666320009
Root relative squared error: 32.26945384797926
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8510780929064881
Weighted FMeasure: 0.9387779650086184
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9822660040859451
Mean absolute error: 0.035366573977779925
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.1640011552151198
Kappa statistic: 0.8539382192282146
Training time: 6.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 24.684484038604264
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9571554623765619
Weighted AreaUnderROC: 0.9845784746084559
Root mean squared error: 0.10900526923085492
Relative absolute error: 11.616302571615128
Root relative squared error: 29.249183028703083
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8775331922545303
Weighted FMeasure: 0.9481767698469189
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9828001632051735
Mean absolute error: 0.032267507143375035
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 38.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.12868086922132255
Kappa statistic: 0.8810485740399773
Training time: 6.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 24.64736451373413
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9615082925274466
Weighted AreaUnderROC: 0.9821873325980882
Root mean squared error: 0.11311945347042594
Relative absolute error: 12.26806152892997
Root relative squared error: 30.353134504499774
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8903939995718633
Weighted FMeasure: 0.9536254804509932
Iteration time: 8.0
Weighted AreaUnderPRC: 0.979491306583506
Mean absolute error: 0.0340779486914718
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.121289417813209
Kappa statistic: 0.8929848256137286
Training time: 7.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 22.90274684484026
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9692777601018135
Weighted AreaUnderROC: 0.9871875930065515
Root mean squared error: 0.10505905460779179
Relative absolute error: 10.566127359153231
Root relative squared error: 28.190302530586333
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9366505849014759
Weighted FMeasure: 0.972907753331817
Iteration time: 8.0
Weighted AreaUnderPRC: 0.986315012159368
Mean absolute error: 0.02935035377542535
Coverage of cases: 98.88641425389756
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 54.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.06434043461066127
Kappa statistic: 0.942453604019276
Training time: 7.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 23.42242019302146
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9634900924638279
Weighted AreaUnderROC: 0.9866765956487225
Root mean squared error: 0.11502741006348853
Relative absolute error: 11.64727609329483
Root relative squared error: 30.8650929813246
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8970551645883028
Weighted FMeasure: 0.9574543529001163
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9826952494845322
Mean absolute error: 0.03235354470359643
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.11417079491289053
Kappa statistic: 0.899397677698259
Training time: 8.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.531551596139433
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9574776098185092
Weighted AreaUnderROC: 0.9923870398349739
Root mean squared error: 0.11746305017467827
Relative absolute error: 11.214942542721635
Root relative squared error: 31.518643804206057
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8772930084756949
Weighted FMeasure: 0.9469601647218573
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9887293893968616
Mean absolute error: 0.031152618174226453
Coverage of cases: 98.88641425389756
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 71.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.13552666361384594
Kappa statistic: 0.8800715831085232
Training time: 8.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 22.605790645879615
Incorrectly Classified Instances: 3.7861915367483294
Correctly Classified Instances: 96.21380846325167
Weighted Precision: 0.9550223027340237
Weighted AreaUnderROC: 0.9962793108363055
Root mean squared error: 0.1022549946650459
Relative absolute error: 9.668795202139652
Root relative squared error: 27.43789429319065
Weighted TruePositiveRate: 0.9621380846325167
Weighted MatthewsCorrelation: 0.8943083899668494
Weighted FMeasure: 0.955043599327994
Iteration time: 8.0
Weighted AreaUnderPRC: 0.991043542073342
Mean absolute error: 0.026857764450387657
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9621380846325167
Weighted FalsePositiveRate: 0.12101658930541388
Kappa statistic: 0.8985539990962494
Training time: 7.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 22.56867112100952
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9397219245527139
Weighted AreaUnderROC: 0.9945053759943231
Root mean squared error: 0.12477296580097491
Relative absolute error: 12.27119447825451
Root relative squared error: 33.48009999422853
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8165604077547797
Weighted FMeasure: 0.9177981332253411
Iteration time: 9.0
Weighted AreaUnderPRC: 0.990103077144054
Mean absolute error: 0.03408665132848441
Coverage of cases: 99.55456570155901
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.1995942697167121
Kappa statistic: 0.8193560022752183
Training time: 8.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 21.93763919821813
Incorrectly Classified Instances: 4.008908685968819
Correctly Classified Instances: 95.99109131403118
Weighted Precision: 0.9619153674832962
Weighted AreaUnderROC: 0.9879813492471109
Root mean squared error: 0.11554291109970698
Relative absolute error: 11.165124210803015
Root relative squared error: 31.003416424459328
Weighted TruePositiveRate: 0.9599109131403119
Weighted MatthewsCorrelation: 0.8911600682650872
Weighted FMeasure: 0.9544091179553497
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9870782170600709
Mean absolute error: 0.031014233918896954
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 98.0
Weighted Recall: 0.9599109131403119
Weighted FalsePositiveRate: 0.12813521220573235
Kappa statistic: 0.8921896885213101
Training time: 9.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 22.160356347438615
Incorrectly Classified Instances: 4.8997772828507795
Correctly Classified Instances: 95.10022271714922
Weighted Precision: 0.9529388980342791
Weighted AreaUnderROC: 0.9893054403763393
Root mean squared error: 0.11569004240338744
Relative absolute error: 10.784088958345638
Root relative squared error: 31.042895896057047
Weighted TruePositiveRate: 0.9510022271714922
Weighted MatthewsCorrelation: 0.8657702347816726
Weighted FMeasure: 0.9447168271339483
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9841946666230215
Mean absolute error: 0.02995580266207092
Coverage of cases: 99.10913140311804
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 107.0
Weighted Recall: 0.9510022271714922
Weighted FalsePositiveRate: 0.14291811502195947
Kappa statistic: 0.868154456027015
Training time: 9.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 21.714922048997636
Incorrectly Classified Instances: 4.23162583518931
Correctly Classified Instances: 95.7683741648107
Weighted Precision: 0.9595357112576592
Weighted AreaUnderROC: 0.9845780853968176
Root mean squared error: 0.11789468835526713
Relative absolute error: 11.094186116516882
Root relative squared error: 31.63446448182378
Weighted TruePositiveRate: 0.9576837416481069
Weighted MatthewsCorrelation: 0.8833617199613416
Weighted FMeasure: 0.9491877318861827
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9821112168301056
Mean absolute error: 0.030817183656991036
Coverage of cases: 98.88641425389756
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 117.0
Weighted Recall: 0.9576837416481069
Weighted FalsePositiveRate: 0.12840804071352746
Kappa statistic: 0.88651208577776
Training time: 10.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 21.603563474387446
Incorrectly Classified Instances: 6.904231625835189
Correctly Classified Instances: 93.0957683741648
Weighted Precision: 0.9366957850928783
Weighted AreaUnderROC: 0.9897219947976497
Root mean squared error: 0.12987352595573873
Relative absolute error: 11.543496715822585
Root relative squared error: 34.848723901753985
Weighted TruePositiveRate: 0.9309576837416481
Weighted MatthewsCorrelation: 0.8107281978885136
Weighted FMeasure: 0.9221790772405486
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9861560553509353
Mean absolute error: 0.032065268655062415
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 129.0
Weighted Recall: 0.9309576837416481
Weighted FalsePositiveRate: 0.2206773099098724
Kappa statistic: 0.8047059153664833
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 20.824053452115727
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.9782309895004773
Weighted AreaUnderROC: 0.9977297538719078
Root mean squared error: 0.08864175124910052
Relative absolute error: 7.229540101904755
Root relative squared error: 23.785077772513993
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9398640485310787
Weighted FMeasure: 0.9749585798671612
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9954239772479911
Mean absolute error: 0.02008205583862412
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 141.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.06434043461066127
Kappa statistic: 0.9424794066026978
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.67780252412766
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.974179910158167
Weighted AreaUnderROC: 0.9894322767478065
Root mean squared error: 0.0982495406848954
Relative absolute error: 8.228651366972723
Root relative squared error: 26.36311820754701
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9269913818132451
Weighted FMeasure: 0.967777326735932
Iteration time: 14.0
Weighted AreaUnderPRC: 0.986572860861366
Mean absolute error: 0.02285736490825734
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 155.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.08542347480382158
Kappa statistic: 0.9298437500000001
Training time: 13.0
		
Time end:Tue Oct 31 11.13.12 EET 2017