Tue Oct 31 11.13.12 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.13.12 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 30.846325167037907
Incorrectly Classified Instances: 10.913140311804009
Correctly Classified Instances: 89.08685968819599
Weighted Precision: 0.8949190996573732
Weighted AreaUnderROC: 0.9705354329575463
Root mean squared error: 0.1557989026962332
Relative absolute error: 21.859403439222724
Root relative squared error: 41.80523246983853
Weighted TruePositiveRate: 0.89086859688196
Weighted MatthewsCorrelation: 0.7008882426228984
Weighted FMeasure: 0.8869798321426571
Iteration time: 7.0
Weighted AreaUnderPRC: 0.967626692346095
Mean absolute error: 0.0607205651089514
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.89086859688196
Weighted FalsePositiveRate: 0.23017829908261309
Kappa statistic: 0.7118854927843693
Training time: 6.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 29.584261321455088
Incorrectly Classified Instances: 3.56347438752784
Correctly Classified Instances: 96.43652561247217
Weighted Precision: 0.9646559521174947
Weighted AreaUnderROC: 0.9890279956015039
Root mean squared error: 0.13081050045802306
Relative absolute error: 17.82280231299131
Root relative squared error: 35.100140543389
Weighted TruePositiveRate: 0.9643652561247216
Weighted MatthewsCorrelation: 0.9031504484085989
Weighted FMeasure: 0.9617508376439383
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9847221626258398
Mean absolute error: 0.04950778420275315
Coverage of cases: 99.55456570155901
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9643652561247216
Weighted FalsePositiveRate: 0.08618932482532743
Kappa statistic: 0.9081541333196962
Training time: 5.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 28.6933927245731
Incorrectly Classified Instances: 5.3452115812917596
Correctly Classified Instances: 94.65478841870824
Weighted Precision: 0.9531794653760379
Weighted AreaUnderROC: 0.9800837668089485
Root mean squared error: 0.142501923473907
Relative absolute error: 19.09005185590077
Root relative squared error: 38.23727853745567
Weighted TruePositiveRate: 0.9465478841870824
Weighted MatthewsCorrelation: 0.8675576663117052
Weighted FMeasure: 0.9480589633337585
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9768884510715382
Mean absolute error: 0.053027921821946065
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9465478841870824
Weighted FalsePositiveRate: 0.06572416139184466
Kappa statistic: 0.8692106029711621
Training time: 6.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 27.43132887899031
Incorrectly Classified Instances: 8.017817371937639
Correctly Classified Instances: 91.98218262806236
Weighted Precision: 0.9270530632446
Weighted AreaUnderROC: 0.9793225882680945
Root mean squared error: 0.14663324614181095
Relative absolute error: 18.321759872724517
Root relative squared error: 39.34582873614596
Weighted TruePositiveRate: 0.9198218262806236
Weighted MatthewsCorrelation: 0.7809122078230406
Weighted FMeasure: 0.9158471118730965
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9821490454017135
Mean absolute error: 0.05089377742423427
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9198218262806236
Weighted FalsePositiveRate: 0.1933125591909335
Kappa statistic: 0.78540989047461
Training time: 6.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 25.315515961395697
Incorrectly Classified Instances: 2.6726057906458798
Correctly Classified Instances: 97.32739420935413
Weighted Precision: 0.9737939585844555
Weighted AreaUnderROC: 0.995649961762497
Root mean squared error: 0.10163849957772163
Relative absolute error: 11.46336407247588
Root relative squared error: 27.272471302424442
Weighted TruePositiveRate: 0.9732739420935412
Weighted MatthewsCorrelation: 0.9280254392902704
Weighted FMeasure: 0.9713273947512178
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9921770464923524
Mean absolute error: 0.03184267797909935
Coverage of cases: 99.77728285077951
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9732739420935412
Weighted FalsePositiveRate: 0.05771483322405358
Kappa statistic: 0.9321000100816615
Training time: 7.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 23.719376391982134
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9589909071981289
Weighted AreaUnderROC: 0.9875342950747225
Root mean squared error: 0.11890041788506737
Relative absolute error: 11.642140902780342
Root relative squared error: 31.90433003329725
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9064378309111917
Weighted FMeasure: 0.9617167164564397
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9846651852642172
Mean absolute error: 0.03233928028550063
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.09274975938173491
Kappa statistic: 0.9128482511419662
Training time: 7.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 24.907201187824764
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9450773116228515
Weighted AreaUnderROC: 0.9754421455750875
Root mean squared error: 0.13250069668773384
Relative absolute error: 14.73358020290479
Root relative squared error: 35.55366778318267
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8468980773578758
Weighted FMeasure: 0.9387313923319862
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9782457890283363
Mean absolute error: 0.040926611674735124
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 52.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.15683738500363215
Kappa statistic: 0.8504370303256409
Training time: 7.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 24.721603563474353
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9487229738241375
Weighted AreaUnderROC: 0.9877164827959573
Root mean squared error: 0.125575089604045
Relative absolute error: 13.531771258222618
Root relative squared error: 33.69533239623278
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8611871660859906
Weighted FMeasure: 0.9468997739839253
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9857100587204475
Mean absolute error: 0.03758825349506245
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.12866200709334483
Kappa statistic: 0.8653304470293673
Training time: 8.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 23.42242019302149
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9372386956485562
Weighted AreaUnderROC: 0.9870610859316029
Root mean squared error: 0.13342680298205448
Relative absolute error: 13.281018128218083
Root relative squared error: 35.80216817860163
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8211941749841225
Weighted FMeasure: 0.9274334051286134
Iteration time: 9.0
Weighted AreaUnderPRC: 0.976828888480387
Mean absolute error: 0.03689171702282764
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 70.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.18531187660490603
Kappa statistic: 0.8237642791402741
Training time: 8.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 22.5315515961395
Incorrectly Classified Instances: 5.122494432071269
Correctly Classified Instances: 94.87750556792874
Weighted Precision: 0.9516416065472448
Weighted AreaUnderROC: 0.9919706203820707
Root mean squared error: 0.12674904062535264
Relative absolute error: 12.483029825213308
Root relative squared error: 34.01033651054068
Weighted TruePositiveRate: 0.9487750556792873
Weighted MatthewsCorrelation: 0.8607769768354585
Weighted FMeasure: 0.9448056717786552
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9889891043430985
Mean absolute error: 0.0346750828478144
Coverage of cases: 99.77728285077951
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 79.0
Weighted Recall: 0.9487750556792873
Weighted FalsePositiveRate: 0.14969874258428265
Kappa statistic: 0.8620049174194239
Training time: 9.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 21.083890126206303
Incorrectly Classified Instances: 4.4543429844097995
Correctly Classified Instances: 95.5456570155902
Weighted Precision: 0.9487445154912276
Weighted AreaUnderROC: 0.9900364430004612
Root mean squared error: 0.11505495257059094
Relative absolute error: 9.717672976740742
Root relative squared error: 30.872483411502813
Weighted TruePositiveRate: 0.955456570155902
Weighted MatthewsCorrelation: 0.8754959236133136
Weighted FMeasure: 0.9490794111190857
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9881083001620417
Mean absolute error: 0.02699353604650179
Coverage of cases: 98.88641425389756
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 90.0
Weighted Recall: 0.955456570155902
Weighted FalsePositiveRate: 0.1283428738833272
Kappa statistic: 0.881586581570758
Training time: 11.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 22.494432071269422
Incorrectly Classified Instances: 5.79064587973274
Correctly Classified Instances: 94.20935412026726
Weighted Precision: 0.9457291038582798
Weighted AreaUnderROC: 0.9813983499465346
Root mean squared error: 0.1304667806740393
Relative absolute error: 12.158090621100158
Root relative squared error: 35.007910847125096
Weighted TruePositiveRate: 0.9420935412026726
Weighted MatthewsCorrelation: 0.8409959985431397
Weighted FMeasure: 0.9355123071048294
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9793583675769643
Mean absolute error: 0.0337724739475001
Coverage of cases: 99.10913140311804
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 100.0
Weighted Recall: 0.9420935412026726
Weighted FalsePositiveRate: 0.17105461128523802
Kappa statistic: 0.8424837747763552
Training time: 9.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 22.494432071269443
Incorrectly Classified Instances: 6.6815144766147
Correctly Classified Instances: 93.3184855233853
Weighted Precision: 0.9268743779496271
Weighted AreaUnderROC: 0.9834259224638147
Root mean squared error: 0.12972817027531547
Relative absolute error: 12.007501086508961
Root relative squared error: 34.809720879872934
Weighted TruePositiveRate: 0.933184855233853
Weighted MatthewsCorrelation: 0.8114458911389981
Weighted FMeasure: 0.9224309092188749
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9817343612116397
Mean absolute error: 0.03335416968474678
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 111.0
Weighted Recall: 0.933184855233853
Weighted FalsePositiveRate: 0.19268957415814889
Kappa statistic: 0.8167346938775509
Training time: 10.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 22.45731254639933
Incorrectly Classified Instances: 6.23608017817372
Correctly Classified Instances: 93.76391982182628
Weighted Precision: 0.9418412833287418
Weighted AreaUnderROC: 0.9914361541810777
Root mean squared error: 0.12696375152316086
Relative absolute error: 12.103433101441254
Root relative squared error: 34.06794949010179
Weighted TruePositiveRate: 0.9376391982182628
Weighted MatthewsCorrelation: 0.829603264112575
Weighted FMeasure: 0.9323142242863547
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9875870857879206
Mean absolute error: 0.03362064750400315
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 122.0
Weighted Recall: 0.9376391982182628
Weighted FalsePositiveRate: 0.18529185708587495
Kappa statistic: 0.8285838946306345
Training time: 10.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 22.420193021529272
Incorrectly Classified Instances: 5.567928730512249
Correctly Classified Instances: 94.43207126948775
Weighted Precision: 0.9476890794971379
Weighted AreaUnderROC: 0.9949915503362012
Root mean squared error: 0.11979492419844813
Relative absolute error: 10.880849969135149
Root relative squared error: 32.1443512640599
Weighted TruePositiveRate: 0.9443207126948775
Weighted MatthewsCorrelation: 0.8470663630542139
Weighted FMeasure: 0.937683651387864
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9895842034421363
Mean absolute error: 0.030224583247597334
Coverage of cases: 98.66369710467706
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 134.0
Weighted Recall: 0.9443207126948775
Weighted FalsePositiveRate: 0.16393598838491957
Kappa statistic: 0.8491709440756765
Training time: 11.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 23.088344469190737
Incorrectly Classified Instances: 3.34075723830735
Correctly Classified Instances: 96.65924276169265
Weighted Precision: 0.9675404915418184
Weighted AreaUnderROC: 0.9858498626498323
Root mean squared error: 0.11563472170317099
Relative absolute error: 11.909707035839501
Root relative squared error: 31.028051794507423
Weighted TruePositiveRate: 0.9665924276169265
Weighted MatthewsCorrelation: 0.9097254471875016
Weighted FMeasure: 0.9642823697652692
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9853849610195785
Mean absolute error: 0.03308251954399829
Coverage of cases: 99.33184855233853
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 147.0
Weighted Recall: 0.9665924276169265
Weighted FalsePositiveRate: 0.08591023065337194
Kappa statistic: 0.9134774733109802
Training time: 12.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 22.086117297698546
Incorrectly Classified Instances: 2.2271714922048997
Correctly Classified Instances: 97.7728285077951
Weighted Precision: 0.978357128909681
Weighted AreaUnderROC: 0.9986032184486425
Root mean squared error: 0.0829359795095658
Relative absolute error: 7.060852138852455
Root relative squared error: 22.254058555670362
Weighted TruePositiveRate: 0.977728285077951
Weighted MatthewsCorrelation: 0.9400800726505734
Weighted FMeasure: 0.9756182785123517
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9955978312883913
Mean absolute error: 0.01961347816347885
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 158.0
Weighted Recall: 0.977728285077951
Weighted FalsePositiveRate: 0.05031711615177961
Kappa statistic: 0.9433924203837716
Training time: 11.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 21.121009651076477
Incorrectly Classified Instances: 1.78173719376392
Correctly Classified Instances: 98.21826280623608
Weighted Precision: 0.9802323114981736
Weighted AreaUnderROC: 0.995621793543108
Root mean squared error: 0.08506417335124555
Relative absolute error: 6.757071120944413
Root relative squared error: 22.825112887585448
Weighted TruePositiveRate: 0.9821826280623608
Weighted MatthewsCorrelation: 0.9519773626633841
Weighted FMeasure: 0.9807940534559393
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9902404474296649
Mean absolute error: 0.018769642002623182
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 171.0
Weighted Recall: 0.9821826280623608
Weighted FalsePositiveRate: 0.035820795698218236
Kappa statistic: 0.9550921411246968
Training time: 13.0
		
Time end:Tue Oct 31 11.13.13 EET 2017