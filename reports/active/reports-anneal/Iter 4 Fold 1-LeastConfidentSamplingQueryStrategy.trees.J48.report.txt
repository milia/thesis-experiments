Tue Oct 31 11.09.11 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.09.11 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 32.10838901262047
Incorrectly Classified Instances: 17.14922048997773
Correctly Classified Instances: 82.85077951002228
Weighted Precision: 0.7610375998951919
Weighted AreaUnderROC: 0.7014807274911885
Root mean squared error: 0.2187720717468112
Relative absolute error: 27.06347438752812
Root relative squared error: 58.70270688052001
Weighted TruePositiveRate: 0.8285077951002228
Weighted MatthewsCorrelation: 0.4988671934013789
Weighted FMeasure: 0.7793961630506611
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7379493707408985
Mean absolute error: 0.07517631774313292
Coverage of cases: 93.98663697104676
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8285077951002228
Weighted FalsePositiveRate: 0.44875351598649804
Kappa statistic: 0.46186533013728504
Training time: 2.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 31.77431328878973
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21108032837354182
Relative absolute error: 26.035879487995494
Root relative squared error: 56.63879555474633
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.07232188746665343
Coverage of cases: 94.65478841870824
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 3.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 31.77431328878973
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21223292780375042
Relative absolute error: 24.522963943899597
Root relative squared error: 56.9480704359592
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.06811934428860932
Coverage of cases: 94.65478841870824
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 3.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 31.77431328878973
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21321966444041285
Relative absolute error: 23.472007344565757
Root relative squared error: 57.21283965941521
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.0652000204015709
Coverage of cases: 94.65478841870824
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 3.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 31.77431328878973
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21404091456771296
Relative absolute error: 22.699449844393232
Root relative squared error: 57.43320419275603
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.06305402734553613
Coverage of cases: 94.65478841870824
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 4.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 31.77431328878973
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.2147243822124484
Relative absolute error: 22.107607548939328
Root relative squared error: 57.61659780644204
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.0614100209692753
Coverage of cases: 94.65478841870824
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 4.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21529774703585927
Relative absolute error: 21.63971128394704
Root relative squared error: 57.77044773296851
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.06011030912207452
Coverage of cases: 85.30066815144767
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 27.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 4.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21578364145702125
Relative absolute error: 21.260515732697392
Root relative squared error: 57.90082688764124
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.05905698814638105
Coverage of cases: 85.30066815144767
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 32.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 5.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.2161996512157558
Relative absolute error: 20.946981748763694
Root relative squared error: 58.01245401962128
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.0581860604132319
Coverage of cases: 85.30066815144767
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 4.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.2165592879042181
Relative absolute error: 20.683413339958832
Root relative squared error: 58.108954669533844
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.05745392594432952
Coverage of cases: 85.30066815144767
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 43.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 5.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.699331848552339
Correctly Classified Instances: 85.30066815144767
Weighted Precision: 0.8570718927066366
Weighted AreaUnderROC: 0.698173609028248
Root mean squared error: 0.21687296172476223
Relative absolute error: 20.458748017324464
Root relative squared error: 58.19312218779369
Weighted TruePositiveRate: 0.8530066815144766
Weighted MatthewsCorrelation: 0.5664178611923505
Weighted FMeasure: 0.8118751073123761
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7439665441157249
Mean absolute error: 0.0568298556036785
Coverage of cases: 85.30066815144767
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8530066815144766
Weighted FalsePositiveRate: 0.46298958269265567
Kappa statistic: 0.5152139772281114
Training time: 6.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.25389755011136
Correctly Classified Instances: 85.74610244988864
Weighted Precision: 0.8691413016580055
Weighted AreaUnderROC: 0.7122159648603538
Root mean squared error: 0.21436695250313095
Relative absolute error: 19.112413550580417
Root relative squared error: 57.52068935117663
Weighted TruePositiveRate: 0.8574610244988864
Weighted MatthewsCorrelation: 0.5841016734338683
Weighted FMeasure: 0.8196765904668876
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7538481657807483
Mean absolute error: 0.05309003764050063
Coverage of cases: 85.74610244988864
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 55.0
Weighted Recall: 0.8574610244988864
Weighted FalsePositiveRate: 0.44875233689201877
Kappa statistic: 0.5350462753219856
Training time: 6.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.25389755011136
Correctly Classified Instances: 85.74610244988864
Weighted Precision: 0.8691413016580055
Weighted AreaUnderROC: 0.7184390614006317
Root mean squared error: 0.2144736189531693
Relative absolute error: 18.993620474878426
Root relative squared error: 57.54931096316115
Weighted TruePositiveRate: 0.8574610244988864
Weighted MatthewsCorrelation: 0.5841016734338683
Weighted FMeasure: 0.8196765904668876
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7568493638028935
Mean absolute error: 0.052760056874661766
Coverage of cases: 85.74610244988864
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 62.0
Weighted Recall: 0.8574610244988864
Weighted FalsePositiveRate: 0.44875233689201877
Kappa statistic: 0.5350462753219856
Training time: 7.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 14.25389755011136
Correctly Classified Instances: 85.74610244988864
Weighted Precision: 0.8691413016580055
Weighted AreaUnderROC: 0.7184390614006317
Root mean squared error: 0.21468005789965033
Relative absolute error: 18.873687559656535
Root relative squared error: 57.60470434526535
Weighted TruePositiveRate: 0.8574610244988864
Weighted MatthewsCorrelation: 0.5841016734338683
Weighted FMeasure: 0.8196765904668876
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7568493638028935
Mean absolute error: 0.052426909887934296
Coverage of cases: 85.74610244988864
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 70.0
Weighted Recall: 0.8574610244988864
Weighted FalsePositiveRate: 0.44875233689201877
Kappa statistic: 0.5350462753219856
Training time: 7.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 16.88938381588713
Incorrectly Classified Instances: 6.458797327394209
Correctly Classified Instances: 93.54120267260579
Weighted Precision: 0.9393171214562925
Weighted AreaUnderROC: 0.9249318810783713
Root mean squared error: 0.14313802303992743
Relative absolute error: 8.266647977115765
Root relative squared error: 38.40796196186525
Weighted TruePositiveRate: 0.9354120267260579
Weighted MatthewsCorrelation: 0.8296889992049892
Weighted FMeasure: 0.9256005717200337
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9116446978028035
Mean absolute error: 0.022962911047543565
Coverage of cases: 93.54120267260579
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.9354120267260579
Weighted FalsePositiveRate: 0.19276100665250953
Kappa statistic: 0.8199430278223354
Training time: 7.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.11655530809199
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9797732270020257
Weighted AreaUnderROC: 0.9632351904876163
Root mean squared error: 0.09001187848244702
Relative absolute error: 5.4342984409800055
Root relative squared error: 24.152721488304373
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9529967590808432
Weighted FMeasure: 0.9787645443707671
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9597676212779978
Mean absolute error: 0.015095273447166532
Coverage of cases: 98.21826280623608
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 92.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.05038854864614025
Kappa statistic: 0.9485196697920913
Training time: 15.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 2.0044543429844097
Correctly Classified Instances: 97.99554565701558
Weighted Precision: 0.9800590055823909
Weighted AreaUnderROC: 0.9615952003675557
Root mean squared error: 0.08166340777109682
Relative absolute error: 2.8878990348923823
Root relative squared error: 21.912591726054956
Weighted TruePositiveRate: 0.9799554565701559
Weighted MatthewsCorrelation: 0.9496037899876322
Weighted FMeasure: 0.9787474171320644
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9618716401177898
Mean absolute error: 0.008021941763589871
Coverage of cases: 97.99554565701558
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 107.0
Weighted Recall: 0.9799554565701559
Weighted FalsePositiveRate: 0.05722807737450321
Kappa statistic: 0.9483274512812644
Training time: 14.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 16.666666666666654
Incorrectly Classified Instances: 0.6681514476614699
Correctly Classified Instances: 99.33184855233853
Weighted Precision: 0.9942158473316147
Weighted AreaUnderROC: 0.9930799117925024
Root mean squared error: 0.04719291781830087
Relative absolute error: 0.8017817371937719
Root relative squared error: 12.66318867579388
Weighted TruePositiveRate: 0.9933184855233853
Weighted MatthewsCorrelation: 0.9899321024370988
Weighted FMeasure: 0.9936217073355769
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9887724219517804
Mean absolute error: 0.0022271714922048997
Coverage of cases: 99.33184855233853
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 117.0
Weighted Recall: 0.9933184855233853
Weighted FalsePositiveRate: 0.007158661938380574
Kappa statistic: 0.9832362604540024
Training time: 10.0
		
Time end:Tue Oct 31 11.09.12 EET 2017