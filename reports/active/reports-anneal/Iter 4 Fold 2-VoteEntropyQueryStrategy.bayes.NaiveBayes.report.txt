Tue Oct 31 11.12.10 EET 2017
Dataset: anneal
Test set size: 449
Initial Labelled set size: 89
Initial Unlabelled set size: 360
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.12.10 EET 2017
		
Iteration: 1
Labeled set size: 89
Unlabelled set size: 360
	
Mean region size: 21.529324424647328
Incorrectly Classified Instances: 10.244988864142538
Correctly Classified Instances: 89.75501113585746
Weighted Precision: 0.8883794383773486
Weighted AreaUnderROC: 0.9477318302888142
Root mean squared error: 0.16389061662964818
Relative absolute error: 15.391838215796959
Root relative squared error: 43.97646715899031
Weighted TruePositiveRate: 0.8975501113585747
Weighted MatthewsCorrelation: 0.7199437344349178
Weighted FMeasure: 0.8868086950250028
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9395137714484475
Mean absolute error: 0.042755106154991125
Coverage of cases: 95.5456570155902
Instances selection time: 18.0
Test time: 25.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8975501113585747
Weighted FalsePositiveRate: 0.26549478332137516
Kappa statistic: 0.7093033075299087
Training time: 0.0
		
Iteration: 2
Labeled set size: 109
Unlabelled set size: 340
	
Mean region size: 20.452858203414966
Incorrectly Classified Instances: 9.57683741648107
Correctly Classified Instances: 90.42316258351893
Weighted Precision: 0.8948851468341983
Weighted AreaUnderROC: 0.9513324278605869
Root mean squared error: 0.1572326760519675
Relative absolute error: 13.16123226327005
Root relative squared error: 42.189954232368514
Weighted TruePositiveRate: 0.9042316258351893
Weighted MatthewsCorrelation: 0.7439268655388229
Weighted FMeasure: 0.8947217066351928
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9421340600490211
Mean absolute error: 0.03655897850908311
Coverage of cases: 95.7683741648107
Instances selection time: 16.0
Test time: 26.0
Accumulative iteration time: 35.0
Weighted Recall: 0.9042316258351893
Weighted FalsePositiveRate: 0.24431963751198602
Kappa statistic: 0.7306463538832854
Training time: 1.0
		
Iteration: 3
Labeled set size: 129
Unlabelled set size: 320
	
Mean region size: 20.378619153674798
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.9001143682658159
Weighted AreaUnderROC: 0.9433005863975341
Root mean squared error: 0.15989864903883483
Relative absolute error: 13.23331870060027
Root relative squared error: 42.90530985134614
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7621688360032396
Weighted FMeasure: 0.8994735159239737
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9385606361160718
Mean absolute error: 0.036759218612778165
Coverage of cases: 95.5456570155902
Instances selection time: 16.0
Test time: 25.0
Accumulative iteration time: 52.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.229809563203554
Kappa statistic: 0.7504168982768205
Training time: 1.0
		
Iteration: 4
Labeled set size: 149
Unlabelled set size: 300
	
Mean region size: 20.304380103934637
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9003646444345516
Weighted AreaUnderROC: 0.9457542911352094
Root mean squared error: 0.16183909125594728
Relative absolute error: 13.25778273958904
Root relative squared error: 43.425985135811004
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7558361730774308
Weighted FMeasure: 0.8963114170701483
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9399273241453892
Mean absolute error: 0.03682717427663585
Coverage of cases: 95.10022271714922
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 67.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.23709718377287012
Kappa statistic: 0.7429233755533521
Training time: 1.0
		
Iteration: 5
Labeled set size: 169
Unlabelled set size: 280
	
Mean region size: 20.118782479584223
Incorrectly Classified Instances: 8.908685968819599
Correctly Classified Instances: 91.0913140311804
Weighted Precision: 0.9034723175918683
Weighted AreaUnderROC: 0.9458862498618039
Root mean squared error: 0.1604931265183775
Relative absolute error: 12.82913273983265
Root relative squared error: 43.06482489798816
Weighted TruePositiveRate: 0.910913140311804
Weighted MatthewsCorrelation: 0.7587379089055236
Weighted FMeasure: 0.8981928766024143
Iteration time: 15.0
Weighted AreaUnderPRC: 0.941145221244559
Mean absolute error: 0.03563647983286811
Coverage of cases: 95.32293986636971
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 82.0
Weighted Recall: 0.910913140311804
Weighted FalsePositiveRate: 0.24367014965759837
Kappa statistic: 0.7471241710431833
Training time: 1.0
		
Iteration: 6
Labeled set size: 189
Unlabelled set size: 260
	
Mean region size: 19.636228656273158
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9003646444345516
Weighted AreaUnderROC: 0.9456075137441728
Root mean squared error: 0.16433716295412634
Relative absolute error: 12.964336550352394
Root relative squared error: 44.09628811138661
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7558361730774308
Weighted FMeasure: 0.8963114170701483
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9397253314420311
Mean absolute error: 0.036012045973200735
Coverage of cases: 95.10022271714922
Instances selection time: 13.0
Test time: 26.0
Accumulative iteration time: 96.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.23709718377287012
Kappa statistic: 0.7429233755533521
Training time: 1.0
		
Iteration: 7
Labeled set size: 209
Unlabelled set size: 240
	
Mean region size: 19.48775055679285
Incorrectly Classified Instances: 9.35412026726058
Correctly Classified Instances: 90.64587973273942
Weighted Precision: 0.8980649057256592
Weighted AreaUnderROC: 0.9467921685957577
Root mean squared error: 0.1698140286933325
Relative absolute error: 13.534839866679444
Root relative squared error: 45.56588540296718
Weighted TruePositiveRate: 0.9064587973273942
Weighted MatthewsCorrelation: 0.7534245663053684
Weighted FMeasure: 0.8935589091076738
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9373032149576337
Mean absolute error: 0.03759677740744253
Coverage of cases: 94.43207126948775
Instances selection time: 12.0
Test time: 25.0
Accumulative iteration time: 109.0
Weighted Recall: 0.9064587973273942
Weighted FalsePositiveRate: 0.2442158066731886
Kappa statistic: 0.7344242902208202
Training time: 1.0
		
Iteration: 8
Labeled set size: 229
Unlabelled set size: 220
	
Mean region size: 19.339272457312525
Incorrectly Classified Instances: 9.131403118040089
Correctly Classified Instances: 90.86859688195992
Weighted Precision: 0.9004408897274784
Weighted AreaUnderROC: 0.9475885378867998
Root mean squared error: 0.17087684041722984
Relative absolute error: 13.56737930797594
Root relative squared error: 45.85106771439736
Weighted TruePositiveRate: 0.9086859688195991
Weighted MatthewsCorrelation: 0.7558675132229103
Weighted FMeasure: 0.8956250788819896
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9373409800714372
Mean absolute error: 0.037687164744377236
Coverage of cases: 94.43207126948775
Instances selection time: 11.0
Test time: 26.0
Accumulative iteration time: 121.0
Weighted Recall: 0.9086859688195991
Weighted FalsePositiveRate: 0.24394297816539348
Kappa statistic: 0.7408022753192627
Training time: 1.0
		
Iteration: 9
Labeled set size: 249
Unlabelled set size: 200
	
Mean region size: 19.00519673348179
Incorrectly Classified Instances: 10.022271714922049
Correctly Classified Instances: 89.97772828507794
Weighted Precision: 0.8904434330932872
Weighted AreaUnderROC: 0.9491746888299377
Root mean squared error: 0.1733659475852932
Relative absolute error: 13.78872873730462
Root relative squared error: 46.51896525412596
Weighted TruePositiveRate: 0.8997772828507795
Weighted MatthewsCorrelation: 0.7287932019442936
Weighted FMeasure: 0.8845057811751428
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9371568993077529
Mean absolute error: 0.03830202427029023
Coverage of cases: 94.20935412026726
Instances selection time: 10.0
Test time: 25.0
Accumulative iteration time: 132.0
Weighted Recall: 0.8997772828507795
Weighted FalsePositiveRate: 0.27241746976666736
Kappa statistic: 0.71074143534094
Training time: 1.0
		
Iteration: 10
Labeled set size: 269
Unlabelled set size: 180
	
Mean region size: 18.745360059391214
Incorrectly Classified Instances: 11.804008908685969
Correctly Classified Instances: 88.19599109131403
Weighted Precision: 0.8698574901378516
Weighted AreaUnderROC: 0.9511662639692058
Root mean squared error: 0.1805237907681845
Relative absolute error: 14.716229975486028
Root relative squared error: 48.43961612563338
Weighted TruePositiveRate: 0.8819599109131403
Weighted MatthewsCorrelation: 0.6875569897139008
Weighted FMeasure: 0.8664788978589469
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9351838307611708
Mean absolute error: 0.040878416598571894
Coverage of cases: 93.76391982182628
Instances selection time: 9.0
Test time: 26.0
Accumulative iteration time: 142.0
Weighted Recall: 0.8819599109131403
Weighted FalsePositiveRate: 0.3018142777301239
Kappa statistic: 0.6546705170437229
Training time: 1.0
		
Iteration: 11
Labeled set size: 289
Unlabelled set size: 160
	
Mean region size: 18.85671863400146
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.880063401855123
Weighted AreaUnderROC: 0.9524112217572169
Root mean squared error: 0.17589768074057394
Relative absolute error: 14.142454961192174
Root relative squared error: 47.198300546457695
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7119592583685782
Weighted FMeasure: 0.8776144354476519
Iteration time: 9.0
Weighted AreaUnderPRC: 0.93713807366194
Mean absolute error: 0.039284597114422316
Coverage of cases: 94.20935412026726
Instances selection time: 8.0
Test time: 25.0
Accumulative iteration time: 151.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.27991275201357835
Kappa statistic: 0.6915193587633293
Training time: 1.0
		
Iteration: 12
Labeled set size: 309
Unlabelled set size: 140
	
Mean region size: 18.85671863400146
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.880063401855123
Weighted AreaUnderROC: 0.9521986788022296
Root mean squared error: 0.17618182213546044
Relative absolute error: 14.088252866440197
Root relative squared error: 47.274543683360235
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7119592583685782
Weighted FMeasure: 0.8776144354476519
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9372360461175726
Mean absolute error: 0.03913403574011127
Coverage of cases: 94.43207126948775
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 159.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.27991275201357835
Kappa statistic: 0.6915193587633293
Training time: 1.0
		
Iteration: 13
Labeled set size: 329
Unlabelled set size: 120
	
Mean region size: 18.70824053452113
Incorrectly Classified Instances: 10.690423162583519
Correctly Classified Instances: 89.30957683741649
Weighted Precision: 0.880063401855123
Weighted AreaUnderROC: 0.9538806338321231
Root mean squared error: 0.1751006809323101
Relative absolute error: 13.95123573086578
Root relative squared error: 46.98444305653783
Weighted TruePositiveRate: 0.8930957683741648
Weighted MatthewsCorrelation: 0.7119592583685782
Weighted FMeasure: 0.8776144354476519
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9387696264826296
Mean absolute error: 0.03875343258573789
Coverage of cases: 93.76391982182628
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 167.0
Weighted Recall: 0.8930957683741648
Weighted FalsePositiveRate: 0.27991275201357835
Kappa statistic: 0.6915193587633293
Training time: 1.0
		
Iteration: 14
Labeled set size: 349
Unlabelled set size: 100
	
Mean region size: 18.70824053452113
Incorrectly Classified Instances: 11.35857461024499
Correctly Classified Instances: 88.64142538975501
Weighted Precision: 0.8721084451561719
Weighted AreaUnderROC: 0.9475374087641256
Root mean squared error: 0.1742150420962067
Relative absolute error: 13.845243843458146
Root relative squared error: 46.7468012196129
Weighted TruePositiveRate: 0.8864142538975501
Weighted MatthewsCorrelation: 0.690815370376325
Weighted FMeasure: 0.8675399641551106
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9349686297216684
Mean absolute error: 0.03845901067627224
Coverage of cases: 93.76391982182628
Instances selection time: 5.0
Test time: 26.0
Accumulative iteration time: 173.0
Weighted Recall: 0.8864142538975501
Weighted FalsePositiveRate: 0.30811441510705706
Kappa statistic: 0.6652192982456141
Training time: 1.0
		
Iteration: 15
Labeled set size: 369
Unlabelled set size: 80
	
Mean region size: 19.59910913140308
Incorrectly Classified Instances: 7.12694877505568
Correctly Classified Instances: 92.87305122494432
Weighted Precision: 0.9235643745572447
Weighted AreaUnderROC: 0.9692280935354302
Root mean squared error: 0.1354348577168329
Relative absolute error: 10.21987327006027
Root relative squared error: 36.340985805342214
Weighted TruePositiveRate: 0.9287305122494433
Weighted MatthewsCorrelation: 0.8351519112987056
Weighted FMeasure: 0.9203997948770558
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9553189584532352
Mean absolute error: 0.028388536861278246
Coverage of cases: 96.65924276169265
Instances selection time: 5.0
Test time: 25.0
Accumulative iteration time: 179.0
Weighted Recall: 0.9287305122494433
Weighted FalsePositiveRate: 0.12494001925334274
Kappa statistic: 0.8130432519648156
Training time: 1.0
		
Iteration: 16
Labeled set size: 389
Unlabelled set size: 60
	
Mean region size: 19.85894580549373
Incorrectly Classified Instances: 8.46325167037862
Correctly Classified Instances: 91.53674832962137
Weighted Precision: 0.9251844605318275
Weighted AreaUnderROC: 0.9629121161822727
Root mean squared error: 0.15162580255956304
Relative absolute error: 11.864512733556387
Root relative squared error: 40.68547219993754
Weighted TruePositiveRate: 0.9153674832962138
Weighted MatthewsCorrelation: 0.8152993236700239
Weighted FMeasure: 0.9096127455731785
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9521853126369592
Mean absolute error: 0.03295697981543408
Coverage of cases: 96.43652561247217
Instances selection time: 3.0
Test time: 26.0
Accumulative iteration time: 183.0
Weighted Recall: 0.9153674832962138
Weighted FalsePositiveRate: 0.09936281039901755
Kappa statistic: 0.7875456051002999
Training time: 1.0
		
Iteration: 17
Labeled set size: 409
Unlabelled set size: 40
	
Mean region size: 19.896065330363843
Incorrectly Classified Instances: 7.795100222717149
Correctly Classified Instances: 92.20489977728285
Weighted Precision: 0.9346758916652798
Weighted AreaUnderROC: 0.9685248184666323
Root mean squared error: 0.14816666267715992
Relative absolute error: 11.332547997162832
Root relative squared error: 39.75728756944946
Weighted TruePositiveRate: 0.9220489977728286
Weighted MatthewsCorrelation: 0.8438711344395947
Weighted FMeasure: 0.9226767666037838
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9574824254874124
Mean absolute error: 0.03147929999211867
Coverage of cases: 95.99109131403118
Instances selection time: 3.0
Test time: 25.0
Accumulative iteration time: 187.0
Weighted Recall: 0.9220489977728286
Weighted FalsePositiveRate: 0.028903397267414917
Kappa statistic: 0.8174605940226041
Training time: 1.0
		
Iteration: 18
Labeled set size: 429
Unlabelled set size: 20
	
Mean region size: 19.599109131403193
Incorrectly Classified Instances: 7.349665924276169
Correctly Classified Instances: 92.65033407572383
Weighted Precision: 0.9367692779536364
Weighted AreaUnderROC: 0.9716664347440845
Root mean squared error: 0.14695498734767196
Relative absolute error: 10.967691520502068
Root relative squared error: 39.432160961042115
Weighted TruePositiveRate: 0.9265033407572383
Weighted MatthewsCorrelation: 0.8365397742495332
Weighted FMeasure: 0.927062577235375
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9577928979286279
Mean absolute error: 0.03046580977917211
Coverage of cases: 95.7683741648107
Instances selection time: 1.0
Test time: 26.0
Accumulative iteration time: 189.0
Weighted Recall: 0.9265033407572383
Weighted FalsePositiveRate: 0.027512751906836375
Kappa statistic: 0.8312529895451335
Training time: 1.0
		
Time end:Tue Oct 31 11.12.12 EET 2017