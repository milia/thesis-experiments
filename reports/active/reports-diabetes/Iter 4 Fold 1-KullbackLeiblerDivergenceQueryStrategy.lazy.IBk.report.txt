Tue Oct 31 11.37.31 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.31 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6607980244068953
Weighted AreaUnderROC: 0.6166865671641791
Root mean squared error: 0.5633280668031083
Relative absolute error: 65.99893162393163
Root relative squared error: 112.66561336062166
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.2495608817460583
Weighted FMeasure: 0.6636525885001832
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6127956897574778
Mean absolute error: 0.3299946581196581
Coverage of cases: 67.44791666666667
Instances selection time: 14.0
Test time: 7.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.4411060323383085
Kappa statistic: 0.24570997548557405
Training time: 9.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6668369908204245
Weighted AreaUnderROC: 0.6091044776119403
Root mean squared error: 0.5556775977756533
Relative absolute error: 63.77551020408172
Root relative squared error: 111.13551955513066
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.2526810538082172
Weighted FMeasure: 0.6624503242254691
Iteration time: 13.0
Weighted AreaUnderPRC: 0.609706613843768
Mean absolute error: 0.3188775510204086
Coverage of cases: 68.48958333333333
Instances selection time: 12.0
Test time: 8.0
Accumulative iteration time: 36.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.4666868781094527
Kappa statistic: 0.23934254469255462
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6731021445499407
Weighted AreaUnderROC: 0.6044477611940299
Root mean squared error: 0.552010126741256
Relative absolute error: 62.623587570621574
Root relative squared error: 110.40202534825121
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.2564780431551445
Weighted FMeasure: 0.6598247808632616
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6080299414862277
Mean absolute error: 0.3131179378531079
Coverage of cases: 69.01041666666667
Instances selection time: 12.0
Test time: 9.0
Accumulative iteration time: 49.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.481208644278607
Kappa statistic: 0.23447028077464316
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.670819975956722
Weighted AreaUnderROC: 0.5937910447761193
Root mean squared error: 0.5549986706917864
Relative absolute error: 63.043478260869534
Root relative squared error: 110.99973413835728
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.24299089029899115
Weighted FMeasure: 0.6497698729841587
Iteration time: 13.0
Weighted AreaUnderPRC: 0.60165739737778
Mean absolute error: 0.31521739130434767
Coverage of cases: 68.75
Instances selection time: 12.0
Test time: 10.0
Accumulative iteration time: 62.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.4999179104477612
Kappa statistic: 0.2142954576456146
Training time: 1.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6719245821874339
Weighted AreaUnderROC: 0.5903283582089552
Root mean squared error: 0.5555037006902216
Relative absolute error: 62.97468354430366
Root relative squared error: 111.10074013804432
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.24046602203072176
Weighted FMeasure: 0.6462673611111112
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5998067281742884
Mean absolute error: 0.3148734177215183
Coverage of cases: 68.75
Instances selection time: 17.0
Test time: 11.0
Accumulative iteration time: 80.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.5068432835820895
Kappa statistic: 0.20802969888629175
Training time: 1.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6688763585705074
Weighted AreaUnderROC: 0.5848656716417912
Root mean squared error: 0.5582071587533529
Relative absolute error: 63.436329588014964
Root relative squared error: 111.64143175067058
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.2309431887870036
Weighted FMeasure: 0.6405374733850554
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5963047576292465
Mean absolute error: 0.3171816479400748
Coverage of cases: 68.48958333333333
Instances selection time: 13.0
Test time: 12.0
Accumulative iteration time: 94.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.5151644900497513
Kappa statistic: 0.19662493948405851
Training time: 1.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6617803465229936
Weighted AreaUnderROC: 0.5756716417910447
Root mean squared error: 0.5631185734845594
Relative absolute error: 64.42550505050507
Root relative squared error: 112.62371469691188
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21255843850954625
Weighted FMeasure: 0.6306345904223263
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5902748379698632
Mean absolute error: 0.3221275252525253
Coverage of cases: 67.96875
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 107.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5283442164179105
Kappa statistic: 0.17674126751725572
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6742758521796132
Weighted AreaUnderROC: 0.5762089552238806
Root mean squared error: 0.55877933457862
Relative absolute error: 63.36009174311924
Root relative squared error: 111.75586691572398
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.22586674902486611
Weighted FMeasure: 0.6303665628072704
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5918445428368772
Mean absolute error: 0.3168004587155962
Coverage of cases: 68.48958333333333
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 120.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.5324779228855722
Kappa statistic: 0.18018208765615093
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6799799465240642
Weighted AreaUnderROC: 0.5782089552238806
Root mean squared error: 0.5566790850214365
Relative absolute error: 62.81512605042025
Root relative squared error: 111.33581700428729
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.23406856180919378
Weighted FMeasure: 0.6323319367739478
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5936103045621657
Mean absolute error: 0.3140756302521012
Coverage of cases: 68.75
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 133.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.5310820895522388
Kappa statistic: 0.1852899575671854
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6818957115009746
Weighted AreaUnderROC: 0.5764776119402986
Root mean squared error: 0.5568595357138769
Relative absolute error: 62.790697674418595
Root relative squared error: 111.37190714277538
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.23358718843438483
Weighted FMeasure: 0.6301059582309582
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5927724856846978
Mean absolute error: 0.31395348837209297
Coverage of cases: 68.75
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 144.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.534544776119403
Kappa statistic: 0.18193438432040895
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6777488652255327
Weighted AreaUnderROC: 0.5727462686567164
Root mean squared error: 0.5593301078117324
Relative absolute error: 63.286870503597235
Root relative squared error: 111.86602156234649
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.2245557256021522
Weighted FMeasure: 0.6258769975106401
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5901370352896712
Mean absolute error: 0.3164343525179862
Coverage of cases: 68.48958333333333
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 155.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.5394032960199004
Kappa statistic: 0.17341492919661286
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6597335397653193
Weighted AreaUnderROC: 0.5825970149253731
Root mean squared error: 0.564069155717517
Relative absolute error: 64.30369127516794
Root relative squared error: 112.8138311435034
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21834792292780236
Weighted FMeasure: 0.638340560937148
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5939883071388798
Mean absolute error: 0.32151845637583965
Coverage of cases: 67.96875
Instances selection time: 9.0
Test time: 19.0
Accumulative iteration time: 164.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5144934701492537
Kappa statistic: 0.1898456260720411
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6556935541310541
Weighted AreaUnderROC: 0.5909850746268656
Root mean squared error: 0.5664765070893353
Relative absolute error: 64.80607966457023
Root relative squared error: 113.29530141786705
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.22221788489197483
Weighted FMeasure: 0.6463012069700215
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5981869602475071
Mean absolute error: 0.3240303983228512
Coverage of cases: 67.70833333333333
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 172.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.495113184079602
Kappa statistic: 0.20385232744783324
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6649872448979592
Weighted AreaUnderROC: 0.6122985074626865
Root mean squared error: 0.561993910609164
Relative absolute error: 63.75739644970423
Root relative squared error: 112.39878212183281
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.2527152577961949
Weighted FMeasure: 0.6639366684173669
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6111618214994331
Mean absolute error: 0.31878698224852114
Coverage of cases: 68.22916666666667
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 178.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.4576946517412935
Kappa statistic: 0.24311748739821634
Training time: 1.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6714600320745373
Weighted AreaUnderROC: 0.6264179104477612
Root mean squared error: 0.559778179904069
Relative absolute error: 63.22742085661089
Root relative squared error: 111.9556359808138
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.27201273333719816
Weighted FMeasure: 0.6734857108234603
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6198777673188525
Mean absolute error: 0.31613710428305447
Coverage of cases: 68.48958333333333
Instances selection time: 3.0
Test time: 22.0
Accumulative iteration time: 182.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.43206001243781095
Kappa statistic: 0.267175572519084
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6743852480778217
Weighted AreaUnderROC: 0.6365373134328358
Root mean squared error: 0.5621693121693122
Relative absolute error: 63.7345679012345
Root relative squared error: 112.43386243386244
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.2821122407500439
Weighted FMeasure: 0.677131398425003
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6257471727075458
Mean absolute error: 0.3186728395061725
Coverage of cases: 68.22916666666667
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 184.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.40921703980099505
Kappa statistic: 0.2808547218469851
Training time: 1.0
		
Time end:Tue Oct 31 11.37.32 EET 2017