Tue Oct 31 11.38.40 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.40 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7493862233445566
Weighted AreaUnderROC: 0.6384477611940299
Root mean squared error: 0.5153882032022076
Relative absolute error: 53.125
Root relative squared error: 103.07764064044152
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.37965094619502954
Weighted FMeasure: 0.696178008070433
Iteration time: 28.0
Weighted AreaUnderPRC: 0.6412795489618407
Mean absolute error: 0.265625
Coverage of cases: 73.4375
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.45747947761194024
Kappa statistic: 0.3214137214137215
Training time: 26.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6666666666666666
Weighted AreaUnderROC: 0.5327761194029851
Root mean squared error: 0.5773502691896257
Relative absolute error: 66.66666666666666
Root relative squared error: 115.47005383792515
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.14781997926077758
Weighted FMeasure: 0.5708589655958077
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5654839409722222
Mean absolute error: 0.3333333333333333
Coverage of cases: 66.66666666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.6011144278606965
Kappa statistic: 0.08202599731062303
Training time: 11.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 55.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 6.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 71.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 11.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 87.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 11.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 103.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 11.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 120.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 16.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 137.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 17.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 153.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 11.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 171.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 14.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 191.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 17.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 215.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 17.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 225.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7551715190294325
Weighted AreaUnderROC: 0.6404477611940298
Root mean squared error: 0.5128555677121321
Relative absolute error: 52.604166666666664
Root relative squared error: 102.57111354242643
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.38816403006677636
Weighted FMeasure: 0.6983492602955749
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6436732925548118
Mean absolute error: 0.2630208333333333
Coverage of cases: 73.69791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 235.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.456083644278607
Kappa statistic: 0.3267134226789806
Training time: 9.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7623256012421266
Weighted AreaUnderROC: 0.6806865671641792
Root mean squared error: 0.4921254921257382
Relative absolute error: 48.4375
Root relative squared error: 98.42509842514764
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.4389718200845121
Weighted FMeasure: 0.7352949249528534
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6715042091929008
Mean absolute error: 0.2421875
Coverage of cases: 75.78125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 246.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3964393656716418
Kappa statistic: 0.4040451238235098
Training time: 10.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7650049603174603
Weighted AreaUnderROC: 0.6988059701492538
Root mean squared error: 0.4841229182759271
Relative absolute error: 46.875
Root relative squared error: 96.82458365518542
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.45843899408824257
Weighted FMeasure: 0.749400542118432
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6837061321924603
Mean absolute error: 0.234375
Coverage of cases: 76.5625
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 256.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.3680130597014925
Kappa statistic: 0.43529411764705883
Training time: 10.0
		
Time end:Tue Oct 31 11.38.41 EET 2017