Tue Oct 31 11.38.24 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.24 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 89.84375
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.6994234800838575
Weighted AreaUnderROC: 0.7577014925373134
Root mean squared error: 0.4346334662678427
Relative absolute error: 67.50000000000004
Root relative squared error: 86.92669325356854
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3036657665514415
Weighted FMeasure: 0.6762089201877934
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7462109375003599
Mean absolute error: 0.3375000000000002
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.46797512437810945
Kappa statistic: 0.27243199350385716
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 89.84375
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7477796052631579
Weighted AreaUnderROC: 0.7682835820895523
Root mean squared error: 0.4240184744402851
Relative absolute error: 64.1666666666667
Root relative squared error: 84.80369488805702
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.41817168358472295
Weighted FMeasure: 0.7306420594486993
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7515216074714687
Mean absolute error: 0.32083333333333347
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3937014925373134
Kappa statistic: 0.3930453108535301
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 89.84375
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7649393344957861
Weighted AreaUnderROC: 0.782223880597015
Root mean squared error: 0.421369295669883
Relative absolute error: 62.2916666666667
Root relative squared error: 84.2738591339766
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.44571657982237545
Weighted FMeasure: 0.7386961996336997
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7691139447017745
Mean absolute error: 0.3114583333333335
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.39158084577114427
Kappa statistic: 0.4115922718187874
Training time: 8.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 89.58333333333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7431278935185186
Weighted AreaUnderROC: 0.784179104477612
Root mean squared error: 0.4218016911614589
Relative absolute error: 63.54166666666671
Root relative squared error: 84.36033823229178
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4226897995772629
Weighted FMeasure: 0.7381768223694846
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7695718565911834
Mean absolute error: 0.31770833333333354
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.36600000000000005
Kappa statistic: 0.41104294478527603
Training time: 10.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7368767011567866
Weighted AreaUnderROC: 0.8075373134328357
Root mean squared error: 0.4161142170926952
Relative absolute error: 60.46875000000003
Root relative squared error: 83.22284341853904
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.39868991444924456
Weighted FMeasure: 0.7238229925481111
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7878143898137591
Mean absolute error: 0.30234375000000013
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3978889925373134
Kappa statistic: 0.37764390020299915
Training time: 12.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 89.71354166666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7468502665159101
Weighted AreaUnderROC: 0.7913432835820896
Root mean squared error: 0.4190142201246476
Relative absolute error: 62.70833333333338
Root relative squared error: 83.80284402492953
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4184333792982942
Weighted FMeasure: 0.7316827697262479
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7777917553043271
Mean absolute error: 0.3135416666666669
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3902388059701493
Kappa statistic: 0.39535494029654905
Training time: 14.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7220665245466408
Weighted AreaUnderROC: 0.7925820895522389
Root mean squared error: 0.4212765817528116
Relative absolute error: 61.40624999999997
Root relative squared error: 84.25531635056231
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3549937590741254
Weighted FMeasure: 0.6998453948793486
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7786706258777306
Mean absolute error: 0.30703124999999987
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 81.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.4374281716417911
Kappa statistic: 0.32453260068350875
Training time: 17.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.36979166666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.746031746031746
Weighted AreaUnderROC: 0.8013283582089552
Root mean squared error: 0.41664583281247397
Relative absolute error: 59.375
Root relative squared error: 83.32916656249479
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.41879021622115126
Weighted FMeasure: 0.7326939115929942
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7848778258123762
Mean absolute error: 0.296875
Coverage of cases: 97.39583333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 99.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.38677611940298506
Kappa statistic: 0.3976470588235294
Training time: 18.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 87.890625
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7623456867336454
Weighted AreaUnderROC: 0.8204029850746268
Root mean squared error: 0.40764184647800816
Relative absolute error: 59.84375000000001
Root relative squared error: 81.52836929560164
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4517840455914362
Weighted FMeasure: 0.7461403264836172
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7988683077106891
Mean absolute error: 0.29921875000000003
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 120.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3728715796019901
Kappa statistic: 0.4279353022067973
Training time: 20.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7441798941798942
Weighted AreaUnderROC: 0.8063134328358209
Root mean squared error: 0.41082361381335736
Relative absolute error: 59.73958333333332
Root relative squared error: 82.16472276267147
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4203859870517246
Weighted FMeasure: 0.7355567226890757
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7881915387683675
Mean absolute error: 0.2986979166666666
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 143.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.37638805970149253
Kappa statistic: 0.4044203179526949
Training time: 22.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 89.58333333333333
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7523561507936508
Weighted AreaUnderROC: 0.7708208955223882
Root mean squared error: 0.4270782520022922
Relative absolute error: 63.333333333333364
Root relative squared error: 85.41565040045845
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.4320064755101818
Weighted FMeasure: 0.7382627884348069
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7566373280527898
Mean absolute error: 0.3166666666666668
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.38052176616915423
Kappa statistic: 0.4101960784313726
Training time: 25.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7604471421435708
Weighted AreaUnderROC: 0.796507462686567
Root mean squared error: 0.4174575826755736
Relative absolute error: 60.312500000000036
Root relative squared error: 83.49151653511471
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.4611956165059888
Weighted FMeasure: 0.7553115349974265
Iteration time: 27.0
Weighted AreaUnderPRC: 0.778889605970782
Mean absolute error: 0.3015625000000002
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 196.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.3437742537313433
Kappa statistic: 0.44989176111040374
Training time: 26.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 87.109375
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7490234375
Weighted AreaUnderROC: 0.7996865671641791
Root mean squared error: 0.41457809879442503
Relative absolute error: 60.62500000000001
Root relative squared error: 82.915619758885
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.43530740553479313
Weighted FMeasure: 0.7436314719034535
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7878221147998792
Mean absolute error: 0.30312500000000003
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 226.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.3597456467661691
Kappa statistic: 0.4233128834355829
Training time: 29.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7409239473552613
Weighted AreaUnderROC: 0.8061194029850748
Root mean squared error: 0.4115519408288582
Relative absolute error: 61.66666666666666
Root relative squared error: 82.31038816577164
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.41429900456441743
Weighted FMeasure: 0.7332599242889298
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7956467750796726
Mean absolute error: 0.3083333333333333
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 256.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.37778389303482585
Kappa statistic: 0.3993420628265498
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7345770747884566
Weighted AreaUnderROC: 0.8102238805970149
Root mean squared error: 0.41022097703554844
Relative absolute error: 61.6666666666667
Root relative squared error: 82.04419540710968
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.40232516404785823
Weighted FMeasure: 0.728674108560587
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7950924823061086
Mean absolute error: 0.3083333333333335
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 288.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.38057555970149254
Kappa statistic: 0.3892423366107577
Training time: 32.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7459553519044028
Weighted AreaUnderROC: 0.8075373134328357
Root mean squared error: 0.40904487325149713
Relative absolute error: 62.55208333333333
Root relative squared error: 81.80897465029943
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4294236512832011
Weighted FMeasure: 0.7413140281506202
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7985838358435923
Mean absolute error: 0.31276041666666665
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 322.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3611414800995025
Kappa statistic: 0.4182560438859474
Training time: 33.0
		
Time end:Tue Oct 31 11.38.24 EET 2017