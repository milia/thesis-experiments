Tue Oct 31 11.38.33 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.33 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 76.69270833333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7350659381909382
Weighted AreaUnderROC: 0.7711044776119403
Root mean squared error: 0.44673388170159073
Relative absolute error: 56.9272951096752
Root relative squared error: 89.34677634031814
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4129758594149399
Weighted FMeasure: 0.7357550142427908
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7597398044111591
Mean absolute error: 0.284636475548376
Coverage of cases: 92.1875
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.34941138059701493
Kappa statistic: 0.40906547285954115
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 80.72916666666667
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7612688004402054
Weighted AreaUnderROC: 0.7936716417910447
Root mean squared error: 0.42497880087538586
Relative absolute error: 55.52316041676716
Root relative squared error: 84.99576017507718
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.4599699616403387
Weighted FMeasure: 0.7537463450292398
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7850571715057636
Mean absolute error: 0.2776158020838358
Coverage of cases: 94.53125
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.35069962686567163
Kappa statistic: 0.44579858883899937
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 83.72395833333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7399626749955696
Weighted AreaUnderROC: 0.7680000000000001
Root mean squared error: 0.43381501296949637
Relative absolute error: 59.41473465670784
Root relative squared error: 86.76300259389927
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.41783564552971814
Weighted FMeasure: 0.7366861704972391
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7544911743481549
Mean absolute error: 0.29707367328353923
Coverage of cases: 94.53125
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3639331467661691
Kappa statistic: 0.4081982840800764
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 86.84895833333333
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7542283757474623
Weighted AreaUnderROC: 0.8054328358208954
Root mean squared error: 0.4138340860950598
Relative absolute error: 57.7103087623559
Root relative squared error: 82.76681721901195
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4503603291309713
Weighted FMeasure: 0.7513792425980205
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7885271645757066
Mean absolute error: 0.2885515438117795
Coverage of cases: 96.61458333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.3431032338308458
Kappa statistic: 0.4417899127796738
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.84375
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.743500525210084
Weighted AreaUnderROC: 0.8122388059701494
Root mean squared error: 0.4057023216320703
Relative absolute error: 58.97559891759314
Root relative squared error: 81.14046432641406
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43172915870294304
Weighted FMeasure: 0.7440893374450986
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7961592156918953
Mean absolute error: 0.2948779945879657
Coverage of cases: 96.875
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3382985074626866
Kappa statistic: 0.42800397219463754
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7521936116049782
Weighted AreaUnderROC: 0.7890149253731343
Root mean squared error: 0.4173573358203058
Relative absolute error: 64.83764325006686
Root relative squared error: 83.47146716406117
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45172765240378293
Weighted FMeasure: 0.7530055191865728
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7764709457591005
Mean absolute error: 0.3241882162503343
Coverage of cases: 97.39583333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.32372294776119404
Kappa statistic: 0.4488548675844187
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 93.22916666666667
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7597754343694856
Weighted AreaUnderROC: 0.8012537313432836
Root mean squared error: 0.41001792819908345
Relative absolute error: 63.474152111173524
Root relative squared error: 82.00358563981669
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.4645539108971713
Weighted FMeasure: 0.7581676136363636
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7840866772985554
Mean absolute error: 0.3173707605558676
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.32992350746268656
Kappa statistic: 0.45789936002007775
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 93.22916666666667
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7543284118557841
Weighted AreaUnderROC: 0.8032238805970149
Root mean squared error: 0.4110051723666655
Relative absolute error: 63.24815075984681
Root relative squared error: 82.2010344733331
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4545623402067196
Weighted FMeasure: 0.7541205210027949
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7838588825738816
Mean absolute error: 0.31624075379923405
Coverage of cases: 97.39583333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.32925248756218906
Kappa statistic: 0.44985673352435523
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 93.359375
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7543284118557841
Weighted AreaUnderROC: 0.8094029850746268
Root mean squared error: 0.4068895552919502
Relative absolute error: 63.61157476356624
Root relative squared error: 81.37791105839004
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4545623402067196
Weighted FMeasure: 0.7541205210027949
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7902085261990689
Mean absolute error: 0.3180578738178312
Coverage of cases: 97.39583333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.32925248756218906
Kappa statistic: 0.44985673352435523
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 93.75
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7521936116049782
Weighted AreaUnderROC: 0.8068358208955223
Root mean squared error: 0.40935438393903034
Relative absolute error: 64.30932072658774
Root relative squared error: 81.87087678780607
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45172765240378293
Weighted FMeasure: 0.7530055191865728
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7877582413104327
Mean absolute error: 0.3215466036329387
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.32372294776119404
Kappa statistic: 0.4488548675844187
Training time: 1.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 91.92708333333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7412843125370401
Weighted AreaUnderROC: 0.8033432835820896
Root mean squared error: 0.412254129461794
Relative absolute error: 62.38978697477337
Root relative squared error: 82.45082589235881
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4278707663464245
Weighted FMeasure: 0.7423821006569632
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7840926530972784
Mean absolute error: 0.3119489348738669
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3362316542288557
Kappa statistic: 0.4251497005988024
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.625
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7500524351132066
Weighted AreaUnderROC: 0.8044776119402984
Root mean squared error: 0.4121586477152084
Relative absolute error: 60.89773249819045
Root relative squared error: 82.43172954304168
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.44790098214403545
Weighted FMeasure: 0.7512323889504122
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7850861775748704
Mean absolute error: 0.30448866249095224
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.32165609452736316
Kappa statistic: 0.44590445781652965
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.741711582477032
Weighted AreaUnderROC: 0.8005373134328359
Root mean squared error: 0.4157651380536038
Relative absolute error: 59.739149601389784
Root relative squared error: 83.15302761072077
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4293695882982311
Weighted FMeasure: 0.7429945755340982
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7824591657737985
Mean absolute error: 0.2986957480069489
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.33276896766169156
Kappa statistic: 0.42720059051485526
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 87.63020833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7480839657375693
Weighted AreaUnderROC: 0.8216716417910447
Root mean squared error: 0.4093378667690877
Relative absolute error: 57.73466802544526
Root relative squared error: 81.86757335381753
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44427789981020976
Weighted FMeasure: 0.7494326434277414
Iteration time: 2.0
Weighted AreaUnderPRC: 0.815502651777667
Mean absolute error: 0.2886733401272263
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31958924129353233
Kappa statistic: 0.4429854027972881
Training time: 1.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 22.395833333333332
Correctly Classified Instances: 77.60416666666667
Weighted Precision: 0.7728688482216709
Weighted AreaUnderROC: 0.8351044776119402
Root mean squared error: 0.40271209568106914
Relative absolute error: 56.85646862078132
Root relative squared error: 80.54241913621382
Weighted TruePositiveRate: 0.7760416666666666
Weighted MatthewsCorrelation: 0.4992702945363435
Weighted FMeasure: 0.7738970588235293
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8321435600811489
Mean absolute error: 0.2842823431039066
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7760416666666666
Weighted FalsePositiveRate: 0.28625062189054723
Kappa statistic: 0.4984204131227216
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.28645833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7486396325888546
Weighted AreaUnderROC: 0.8312835820895522
Root mean squared error: 0.40862838621164926
Relative absolute error: 55.98832031609769
Root relative squared error: 81.72567724232985
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44587340283500776
Weighted FMeasure: 0.7499724764646011
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8291647713903707
Mean absolute error: 0.2799416015804885
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31612655472636814
Kappa statistic: 0.4449516158480919
Training time: 0.0
		
Time end:Tue Oct 31 11.38.34 EET 2017