Tue Oct 31 11.37.51 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.51 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7150716131185408
Weighted AreaUnderROC: 0.7388208955223879
Root mean squared error: 0.44863798509117275
Relative absolute error: 66.6145833333332
Root relative squared error: 89.72759701823455
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.34092008537141383
Weighted FMeasure: 0.6941281643056217
Iteration time: 7.0
Weighted AreaUnderPRC: 0.729789613909796
Mean absolute error: 0.333072916666666
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.44368252487562193
Kappa statistic: 0.31166655498224216
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 94.66145833333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7124904434250765
Weighted AreaUnderROC: 0.7631343283582089
Root mean squared error: 0.43053987426640666
Relative absolute error: 69.89583333333331
Root relative squared error: 86.10797485328133
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.36309675721581924
Weighted FMeasure: 0.713654957867921
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7568135489116594
Mean absolute error: 0.34947916666666656
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3778914800995025
Kappa statistic: 0.35900156006240247
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 94.40104166666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7373280152224825
Weighted AreaUnderROC: 0.8011194029850747
Root mean squared error: 0.42704776274635453
Relative absolute error: 68.38541666666656
Root relative squared error: 85.40955254927091
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.4216576613011032
Weighted FMeasure: 0.7356701956322587
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7851812899538059
Mean absolute error: 0.34192708333333277
Coverage of cases: 98.4375
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3085839552238806
Kappa statistic: 0.42141337745213897
Training time: 9.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 93.09895833333333
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.674033997160508
Weighted AreaUnderROC: 0.7346567164179104
Root mean squared error: 0.4591942671680475
Relative absolute error: 74.21874999999993
Root relative squared error: 91.8388534336095
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.2819601689334888
Weighted FMeasure: 0.669711163153786
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7320192430240985
Mean absolute error: 0.3710937499999996
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.3795024875621891
Kappa statistic: 0.2813194525675517
Training time: 12.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 96.61458333333333
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.7089071761026872
Weighted AreaUnderROC: 0.7331044776119403
Root mean squared error: 0.47365489370778535
Relative absolute error: 80.88541666666659
Root relative squared error: 94.73097874155707
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.34934723162237685
Weighted FMeasure: 0.6842737854251011
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7274750427785747
Mean absolute error: 0.40442708333333294
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 56.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.3115907960199005
Kappa statistic: 0.33962054809719294
Training time: 15.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 95.83333333333333
Incorrectly Classified Instances: 33.072916666666664
Correctly Classified Instances: 66.92708333333333
Weighted Precision: 0.7248690801457195
Weighted AreaUnderROC: 0.7421641791044776
Root mean squared error: 0.4850365106532363
Relative absolute error: 82.49999999999999
Root relative squared error: 97.00730213064726
Weighted TruePositiveRate: 0.6692708333333334
Weighted MatthewsCorrelation: 0.37039420610691115
Weighted FMeasure: 0.6767561585835259
Iteration time: 38.0
Weighted AreaUnderPRC: 0.7321522562419687
Mean absolute error: 0.4124999999999999
Coverage of cases: 98.4375
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 94.0
Weighted Recall: 0.6692708333333334
Weighted FalsePositiveRate: 0.28115143034825874
Kappa statistic: 0.3477772428181673
Training time: 33.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 95.83333333333333
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.717013888888889
Weighted AreaUnderROC: 0.7631492537313432
Root mean squared error: 0.4845799297673536
Relative absolute error: 82.03124999999999
Root relative squared error: 96.91598595347072
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.3481293292021605
Weighted FMeasure: 0.6583148219248208
Iteration time: 45.0
Weighted AreaUnderPRC: 0.7510727098821143
Mean absolute error: 0.41015624999999994
Coverage of cases: 98.17708333333333
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 139.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.2874595771144279
Kappa statistic: 0.321304210193099
Training time: 40.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 95.05208333333333
Incorrectly Classified Instances: 35.15625
Correctly Classified Instances: 64.84375
Weighted Precision: 0.7106536921040422
Weighted AreaUnderROC: 0.7510746268656715
Root mean squared error: 0.48527805603248403
Relative absolute error: 81.09374999999993
Root relative squared error: 97.0556112064968
Weighted TruePositiveRate: 0.6484375
Weighted MatthewsCorrelation: 0.3372117950898305
Weighted FMeasure: 0.6560164947426977
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7379833058407251
Mean absolute error: 0.40546874999999966
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 162.0
Weighted Recall: 0.6484375
Weighted FalsePositiveRate: 0.29578078358208953
Kappa statistic: 0.3130863412307203
Training time: 22.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 90.234375
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7336659799502278
Weighted AreaUnderROC: 0.7934477611940297
Root mean squared error: 0.43271189414358974
Relative absolute error: 68.85416666666663
Root relative squared error: 86.54237882871794
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.411042222025539
Weighted FMeasure: 0.7253698744019282
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7835231630906874
Mean absolute error: 0.34427083333333314
Coverage of cases: 98.95833333333333
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 187.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.2982496890547264
Kappa statistic: 0.40826084451869343
Training time: 23.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 93.09895833333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7435775842962263
Weighted AreaUnderROC: 0.7834776119402985
Root mean squared error: 0.43087241924572833
Relative absolute error: 69.53124999999993
Root relative squared error: 86.17448384914567
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.43559262380478253
Weighted FMeasure: 0.7428364003207436
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7686942654454386
Mean absolute error: 0.3476562499999996
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 213.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3043964552238806
Kappa statistic: 0.4355288946962048
Training time: 25.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 92.44791666666667
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.71875
Weighted AreaUnderROC: 0.779313432835821
Root mean squared error: 0.43069106290549697
Relative absolute error: 69.11458333333329
Root relative squared error: 86.1382125810994
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3810149253731343
Weighted FMeasure: 0.71875
Iteration time: 27.0
Weighted AreaUnderPRC: 0.7717960090413408
Mean absolute error: 0.3455729166666664
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 240.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.3377350746268657
Kappa statistic: 0.3810149253731344
Training time: 26.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7456514550264551
Weighted AreaUnderROC: 0.8232238805970149
Root mean squared error: 0.40246635470144143
Relative absolute error: 61.87499999999992
Root relative squared error: 80.49327094028828
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.43342768640930923
Weighted FMeasure: 0.7443759672601392
Iteration time: 28.0
Weighted AreaUnderPRC: 0.8130092067143594
Mean absolute error: 0.3093749999999996
Coverage of cases: 99.47916666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 268.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3472907338308458
Kappa statistic: 0.4267395813690363
Training time: 27.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 90.75520833333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7140110648671946
Weighted AreaUnderROC: 0.763179104477612
Root mean squared error: 0.4377975178854567
Relative absolute error: 68.6458333333333
Root relative squared error: 87.55950357709133
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3682285512030286
Weighted FMeasure: 0.7158235543329386
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7461417892958994
Mean absolute error: 0.3432291666666665
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 297.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.36750342039800993
Kappa statistic: 0.36588678313476125
Training time: 28.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.743500525210084
Weighted AreaUnderROC: 0.8221343283582091
Root mean squared error: 0.40379063263032705
Relative absolute error: 61.30208333333327
Root relative squared error: 80.75812652606541
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.43172915870294304
Weighted FMeasure: 0.7440893374450986
Iteration time: 38.0
Weighted AreaUnderPRC: 0.8101697277030272
Mean absolute error: 0.30651041666666634
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 335.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3382985074626866
Kappa statistic: 0.42800397219463754
Training time: 37.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.751821298358418
Weighted AreaUnderROC: 0.7970597014925374
Root mean squared error: 0.4155505885769703
Relative absolute error: 63.28124999999994
Root relative squared error: 83.11011771539405
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.44199789881200613
Weighted FMeasure: 0.7467600486106073
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7813105440400173
Mean absolute error: 0.3164062499999997
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 365.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.35488712686567164
Kappa statistic: 0.43050328506729596
Training time: 30.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 87.109375
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7629285399829735
Weighted AreaUnderROC: 0.822179104477612
Root mean squared error: 0.3980420832022662
Relative absolute error: 59.37499999999992
Root relative squared error: 79.60841664045324
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.46852394050049756
Weighted FMeasure: 0.7591307898553618
Iteration time: 32.0
Weighted AreaUnderPRC: 0.8147636380948521
Mean absolute error: 0.2968749999999996
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 397.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3354530472636816
Kappa statistic: 0.45900082314949653
Training time: 31.0
		
Time end:Tue Oct 31 11.37.52 EET 2017