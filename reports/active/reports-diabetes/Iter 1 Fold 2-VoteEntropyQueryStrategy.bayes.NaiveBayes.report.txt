Tue Oct 31 11.39.52 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.52 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 73.4375
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7120761145225322
Weighted AreaUnderROC: 0.7428760883084577
Root mean squared error: 0.46122432334339514
Relative absolute error: 58.65096566108782
Root relative squared error: 92.24486466867903
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.36146774283975586
Weighted FMeasure: 0.7128833614157749
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7306393859085115
Mean absolute error: 0.2932548283054391
Coverage of cases: 90.10416666666667
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.38135416666666666
Kappa statistic: 0.3566731383478424
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 72.00520833333333
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7034360718971993
Weighted AreaUnderROC: 0.7419402985074627
Root mean squared error: 0.46710280258999054
Relative absolute error: 58.559564040229496
Root relative squared error: 93.42056051799811
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3423363843251559
Weighted FMeasure: 0.7044270833333331
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7325790381589838
Mean absolute error: 0.2927978202011475
Coverage of cases: 90.36458333333333
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.39246703980099507
Kappa statistic: 0.33743255113565046
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 69.79166666666667
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.6983024691358025
Weighted AreaUnderROC: 0.7375626554726368
Root mean squared error: 0.4781610597556019
Relative absolute error: 58.71724072870216
Root relative squared error: 95.63221195112038
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3318974046757734
Weighted FMeasure: 0.6998737810179221
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7272625205777036
Mean absolute error: 0.2935862036435108
Coverage of cases: 88.80208333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.39525870646766176
Kappa statistic: 0.3278319579894975
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 69.27083333333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7225116478479937
Weighted AreaUnderROC: 0.7410305503731344
Root mean squared error: 0.4685266413626678
Relative absolute error: 56.65423977138553
Root relative squared error: 93.70532827253356
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3779438446024362
Weighted FMeasure: 0.7186343820974201
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7316954629288945
Mean absolute error: 0.28327119885692764
Coverage of cases: 88.28125
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3896215796019901
Kappa statistic: 0.36692489916127796
Training time: 1.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 69.79166666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7285875780586039
Weighted AreaUnderROC: 0.7457313432835821
Root mean squared error: 0.4695006288520059
Relative absolute error: 56.83747588304807
Root relative squared error: 93.90012577040117
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.38957191466744073
Weighted FMeasure: 0.7231927774203969
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7382147911229794
Mean absolute error: 0.28418737941524036
Coverage of cases: 88.54166666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.38682991293532343
Kappa statistic: 0.3769037979564294
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 69.79166666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7305094547625718
Weighted AreaUnderROC: 0.746597014925373
Root mean squared error: 0.46685407871145546
Relative absolute error: 56.70530579203869
Root relative squared error: 93.37081574229109
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3854163816636966
Weighted FMeasure: 0.7182436590642346
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7374538361067605
Mean absolute error: 0.2835265289601935
Coverage of cases: 88.02083333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.4041433457711443
Kappa statistic: 0.36507104970204957
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 69.53125
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7348310704165383
Weighted AreaUnderROC: 0.7506567164179104
Root mean squared error: 0.46723588301916946
Relative absolute error: 56.53549488729208
Root relative squared error: 93.4471766038339
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.38327541205132104
Weighted FMeasure: 0.7125245959165397
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7396526517663177
Mean absolute error: 0.2826774744364604
Coverage of cases: 87.76041666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.4214567786069652
Kappa statistic: 0.3527801882384353
Training time: 1.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 69.140625
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7319600249287749
Weighted AreaUnderROC: 0.75
Root mean squared error: 0.4697001937975848
Relative absolute error: 56.83714736848572
Root relative squared error: 93.94003875951697
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.376195631903737
Weighted FMeasure: 0.7090542186366307
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7388215692628322
Mean absolute error: 0.2841857368424286
Coverage of cases: 87.5
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 20.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.4263152985074627
Kappa statistic: 0.34510433386837885
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 68.75
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7331921064604185
Weighted AreaUnderROC: 0.752268656716418
Root mean squared error: 0.4712233326854293
Relative absolute error: 56.69225745700339
Root relative squared error: 94.24466653708586
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.37604013712229795
Weighted FMeasure: 0.707779255319149
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7413907735833862
Mean absolute error: 0.28346128728501696
Coverage of cases: 86.71875
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 22.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.42977798507462683
Kappa statistic: 0.3425540486101785
Training time: 1.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 68.359375
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7171562828601473
Weighted AreaUnderROC: 0.7527462686567165
Root mean squared error: 0.4721022790047702
Relative absolute error: 56.55085999456323
Root relative squared error: 94.42045580095404
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.34003048200605884
Weighted FMeasure: 0.6913763962375074
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7420565832025238
Mean absolute error: 0.28275429997281615
Coverage of cases: 86.19791666666667
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.4506078980099502
Kappa statistic: 0.3062740595664212
Training time: 1.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 69.921875
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7136082808878511
Weighted AreaUnderROC: 0.7925671641791044
Root mean squared error: 0.47052737785294013
Relative absolute error: 57.07990942647056
Root relative squared error: 94.10547557058803
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.3245437937996599
Weighted FMeasure: 0.6810956168613237
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7837512911523504
Mean absolute error: 0.28539954713235277
Coverage of cases: 88.02083333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.46725031094527364
Kappa statistic: 0.2849039841454247
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 75.0
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7550861941766674
Weighted AreaUnderROC: 0.7738805970149253
Root mean squared error: 0.4517236811934215
Relative absolute error: 57.194358125185275
Root relative squared error: 90.34473623868429
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.43874316347574127
Weighted FMeasure: 0.741526467799486
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7733368757959029
Mean absolute error: 0.28597179062592637
Coverage of cases: 92.1875
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3756632462686567
Kappa statistic: 0.41757453193293753
Training time: 1.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 76.5625
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7428169014084506
Weighted AreaUnderROC: 0.805641791044776
Root mean squared error: 0.4339445602054959
Relative absolute error: 56.164094214865045
Root relative squared error: 86.78891204109918
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4245776147366504
Weighted FMeasure: 0.7397964083357341
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8067959472829656
Mean absolute error: 0.28082047107432523
Coverage of cases: 94.53125
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.35907462686567165
Kappa statistic: 0.41537680791677245
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 79.03645833333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.735829663003071
Weighted AreaUnderROC: 0.8029850746268656
Root mean squared error: 0.4310541518728502
Relative absolute error: 57.06756748821046
Root relative squared error: 86.21083037457004
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.41594232331774544
Weighted FMeasure: 0.7370703913921582
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8026901321004848
Mean absolute error: 0.2853378374410523
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3424860074626866
Kappa statistic: 0.41329711710599415
Training time: 1.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 81.11979166666667
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7500524351132066
Weighted AreaUnderROC: 0.8049850746268655
Root mean squared error: 0.42667893630889975
Relative absolute error: 57.25403090640453
Root relative squared error: 85.33578726177996
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.44790098214403545
Weighted FMeasure: 0.7512323889504122
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8079530804284155
Mean absolute error: 0.28627015453202265
Coverage of cases: 95.3125
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.32165609452736316
Kappa statistic: 0.44590445781652965
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 80.078125
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7486396325888546
Weighted AreaUnderROC: 0.8022686567164179
Root mean squared error: 0.42956576496691073
Relative absolute error: 57.44952563181359
Root relative squared error: 85.91315299338214
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.44587340283500776
Weighted FMeasure: 0.7499724764646011
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8038610370689526
Mean absolute error: 0.28724762815906796
Coverage of cases: 95.05208333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.31612655472636814
Kappa statistic: 0.4449516158480919
Training time: 1.0
		
Time end:Tue Oct 31 11.39.53 EET 2017