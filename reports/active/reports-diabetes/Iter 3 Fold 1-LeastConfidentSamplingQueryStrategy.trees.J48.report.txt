Tue Oct 31 11.38.19 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.19 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 99.73958333333333
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7252899447450213
Weighted AreaUnderROC: 0.6952238805970149
Root mean squared error: 0.4530677566013262
Relative absolute error: 67.85416666666707
Root relative squared error: 90.61355132026524
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.394509758195921
Weighted FMeasure: 0.721318793911007
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6718591435944395
Mean absolute error: 0.33927083333333535
Coverage of cases: 99.47916666666667
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 6.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.31695895522388057
Kappa statistic: 0.3936132881038718
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 90.75520833333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7254488579980153
Weighted AreaUnderROC: 0.6769104477611941
Root mean squared error: 0.48774582672983463
Relative absolute error: 63.17107371794911
Root relative squared error: 97.54916534596693
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3937588060290041
Weighted FMeasure: 0.7270973327836302
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6694935653812496
Mean absolute error: 0.31585536858974556
Coverage of cases: 91.66666666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.35153202736318406
Kappa statistic: 0.3917696992064958
Training time: 2.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 98.828125
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7403374369081884
Weighted AreaUnderROC: 0.7041940298507464
Root mean squared error: 0.46471703783109153
Relative absolute error: 63.09819688109195
Root relative squared error: 92.94340756621831
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4274670648026431
Weighted FMeasure: 0.7418009440104166
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6843352038822821
Mean absolute error: 0.31549098440545975
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32723942786069654
Kappa statistic: 0.42641141324228743
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 86.45833333333333
Incorrectly Classified Instances: 35.677083333333336
Correctly Classified Instances: 64.32291666666667
Weighted Precision: 0.7076889667350194
Weighted AreaUnderROC: 0.7388358208955225
Root mean squared error: 0.46264273436778336
Relative absolute error: 66.79924242424244
Root relative squared error: 92.52854687355668
Weighted TruePositiveRate: 0.6432291666666666
Weighted MatthewsCorrelation: 0.3298503750671709
Weighted FMeasure: 0.6507549448350769
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7160164032338505
Mean absolute error: 0.33399621212121217
Coverage of cases: 96.35416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6432291666666666
Weighted FalsePositiveRate: 0.29857245024875617
Kappa statistic: 0.3050462351387054
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 77.47395833333333
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7346519262765736
Weighted AreaUnderROC: 0.7342985074626865
Root mean squared error: 0.4946133040279107
Relative absolute error: 62.2731621718455
Root relative squared error: 98.92266080558214
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.3971965360362401
Weighted FMeasure: 0.6973427823117438
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7115423039131322
Mean absolute error: 0.3113658108592275
Coverage of cases: 90.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 16.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.2734474502487562
Kappa statistic: 0.37923164701407375
Training time: 3.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7207923080597177
Weighted AreaUnderROC: 0.7532388059701492
Root mean squared error: 0.44999341551373867
Relative absolute error: 66.90325797872329
Root relative squared error: 89.99868310274773
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.3752316114991461
Weighted FMeasure: 0.6969484891446851
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7275980860107314
Mean absolute error: 0.3345162898936164
Coverage of cases: 97.13541666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.2976862562189055
Kappa statistic: 0.36522753792298707
Training time: 2.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 62.239583333333336
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7223314693312878
Weighted AreaUnderROC: 0.713820895522388
Root mean squared error: 0.5260846379588519
Relative absolute error: 60.86805555555554
Root relative squared error: 105.21692759177037
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.38027663817241514
Weighted FMeasure: 0.7017775318490465
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6901456396907242
Mean absolute error: 0.3043402777777777
Coverage of cases: 80.46875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.29835727611940294
Kappa statistic: 0.37184721212460153
Training time: 5.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 98.17708333333333
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.737869725418938
Weighted AreaUnderROC: 0.7042388059701491
Root mean squared error: 0.47257023608694737
Relative absolute error: 73.64454179318514
Root relative squared error: 94.51404721738948
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4229815474023096
Weighted FMeasure: 0.7386438792688793
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6785610418017652
Mean absolute error: 0.36822270896592574
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.3196430348258707
Kappa statistic: 0.4228688228928701
Training time: 3.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 98.17708333333333
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.737869725418938
Weighted AreaUnderROC: 0.7042388059701491
Root mean squared error: 0.47083773958361075
Relative absolute error: 71.95223410087742
Root relative squared error: 94.16754791672216
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4229815474023096
Weighted FMeasure: 0.7386438792688793
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6785610418017652
Mean absolute error: 0.3597611705043871
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.3196430348258707
Kappa statistic: 0.4228688228928701
Training time: 4.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 98.17708333333333
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.737869725418938
Weighted AreaUnderROC: 0.7042388059701491
Root mean squared error: 0.4695973834928108
Relative absolute error: 69.77576351264193
Root relative squared error: 93.91947669856216
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4229815474023096
Weighted FMeasure: 0.7386438792688793
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6785610418017652
Mean absolute error: 0.34887881756320965
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.3196430348258707
Kappa statistic: 0.4228688228928701
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 94.921875
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7681595886017959
Weighted AreaUnderROC: 0.748910447761194
Root mean squared error: 0.45795377783549535
Relative absolute error: 58.504432790430414
Root relative squared error: 91.59075556709907
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4804310887104238
Weighted FMeasure: 0.7500023631276468
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7197583406273348
Mean absolute error: 0.2925221639521521
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 40.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.2441349502487562
Kappa statistic: 0.47128245476003144
Training time: 4.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 94.01041666666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7540628857028043
Weighted AreaUnderROC: 0.7341343283582088
Root mean squared error: 0.46059005722391105
Relative absolute error: 57.81662911361297
Root relative squared error: 92.11801144478221
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4575133110731368
Weighted FMeasure: 0.7498674766953849
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7056090447326273
Mean absolute error: 0.28908314556806486
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.2808286691542289
Kappa statistic: 0.4562970747941847
Training time: 4.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 95.18229166666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7681595886017959
Weighted AreaUnderROC: 0.7597462686567166
Root mean squared error: 0.4594929385597921
Relative absolute error: 57.336609115261
Root relative squared error: 91.89858771195843
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4804310887104238
Weighted FMeasure: 0.7500023631276468
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7317631033569008
Mean absolute error: 0.286683045576305
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 51.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.2441349502487562
Kappa statistic: 0.47128245476003144
Training time: 5.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 79.296875
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7750109697235629
Weighted AreaUnderROC: 0.7615373134328358
Root mean squared error: 0.4577235571505684
Relative absolute error: 56.3997110460342
Root relative squared error: 91.54471143011368
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.49418330767514645
Weighted FMeasure: 0.7552414905356083
Iteration time: 6.0
Weighted AreaUnderPRC: 0.733018859874091
Mean absolute error: 0.281998555230171
Coverage of cases: 86.19791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.2344179104477612
Kappa statistic: 0.4837553215326014
Training time: 5.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7570015170260397
Weighted AreaUnderROC: 0.7301492537313433
Root mean squared error: 0.4450161475484139
Relative absolute error: 57.72130091350609
Root relative squared error: 89.00322950968278
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.46507964614057
Weighted FMeasure: 0.7560212088867804
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7069705989397104
Mean absolute error: 0.28860650456753045
Coverage of cases: 96.09375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.2870292288557214
Kappa statistic: 0.464959089292067
Training time: 6.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 86.328125
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7343907828282829
Weighted AreaUnderROC: 0.732044776119403
Root mean squared error: 0.45766542492673445
Relative absolute error: 58.800822023194144
Root relative squared error: 91.53308498534689
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4140307214002716
Weighted FMeasure: 0.7359530265428883
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7160357918217305
Mean absolute error: 0.2940041101159707
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 71.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.33695646766169157
Kappa statistic: 0.41262848751835546
Training time: 7.0
		
Time end:Tue Oct 31 11.38.20 EET 2017