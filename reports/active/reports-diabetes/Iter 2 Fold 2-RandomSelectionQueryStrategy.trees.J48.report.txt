Tue Oct 31 11.39.38 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.38 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 71.484375
Incorrectly Classified Instances: 34.375
Correctly Classified Instances: 65.625
Weighted Precision: 0.7151075708061002
Weighted AreaUnderROC: 0.6911492537313433
Root mean squared error: 0.4811464732201919
Relative absolute error: 67.6257586367882
Root relative squared error: 96.22929464403838
Weighted TruePositiveRate: 0.65625
Weighted MatthewsCorrelation: 0.3483014514509252
Weighted FMeasure: 0.6638657630383927
Iteration time: 2.0
Weighted AreaUnderPRC: 0.689888232027455
Mean absolute error: 0.338128793183941
Coverage of cases: 88.54166666666667
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 2.0
Weighted Recall: 0.65625
Weighted FalsePositiveRate: 0.2915932835820896
Kappa statistic: 0.3252396166134185
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 92.70833333333333
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7137186048199767
Weighted AreaUnderROC: 0.6172537313432835
Root mean squared error: 0.46592099602217674
Relative absolute error: 75.98824786324776
Root relative squared error: 93.18419920443534
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.31670005289068404
Weighted FMeasure: 0.6740708280216112
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6216906327126887
Mean absolute error: 0.37994123931623885
Coverage of cases: 95.83333333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.4790342039800995
Kappa statistic: 0.2711209276642738
Training time: 2.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 100.0
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7529757377547917
Weighted AreaUnderROC: 0.6074029850746269
Root mean squared error: 0.44977479162910755
Relative absolute error: 76.62391345427048
Root relative squared error: 89.95495832582151
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.34287743390550923
Weighted FMeasure: 0.6629126346183861
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6211373244346415
Mean absolute error: 0.3831195672713524
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.5039440298507463
Kappa statistic: 0.25762566232278394
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 79.42708333333333
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7043926952001769
Weighted AreaUnderROC: 0.7798358208955224
Root mean squared error: 0.4384890223394885
Relative absolute error: 61.76296027131757
Root relative squared error: 87.6978044678977
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3457997088196784
Weighted FMeasure: 0.7060136664163852
Iteration time: 4.0
Weighted AreaUnderPRC: 0.761446505598601
Mean absolute error: 0.30881480135658784
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.38554166666666667
Kappa statistic: 0.3422200074747726
Training time: 3.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 74.08854166666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7516714113181505
Weighted AreaUnderROC: 0.7535373134328358
Root mean squared error: 0.43551844575381804
Relative absolute error: 58.930501986909555
Root relative squared error: 87.10368915076361
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4115811187793749
Weighted FMeasure: 0.7214846077277417
Iteration time: 4.0
Weighted AreaUnderPRC: 0.72567707128549
Mean absolute error: 0.2946525099345478
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.41587344527363185
Kappa statistic: 0.3735620585267407
Training time: 3.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 74.08854166666667
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7516714113181505
Weighted AreaUnderROC: 0.7535373134328358
Root mean squared error: 0.4347064478677417
Relative absolute error: 59.352381651017474
Root relative squared error: 86.94128957354835
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4115811187793749
Weighted FMeasure: 0.7214846077277417
Iteration time: 4.0
Weighted AreaUnderPRC: 0.72567707128549
Mean absolute error: 0.29676190825508736
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.41587344527363185
Kappa statistic: 0.3735620585267407
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 91.796875
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7341534866205919
Weighted AreaUnderROC: 0.6498805970149254
Root mean squared error: 0.468524455320251
Relative absolute error: 67.8281914893614
Root relative squared error: 93.7048910640502
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.40534546275816186
Weighted FMeasure: 0.7312570193734708
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6504221132942445
Mean absolute error: 0.339140957446807
Coverage of cases: 92.44791666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.37018749999999995
Kappa statistic: 0.39599618684461396
Training time: 3.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 84.765625
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.68621552244017
Weighted AreaUnderROC: 0.670537313432836
Root mean squared error: 0.4862865042573755
Relative absolute error: 70.85927977086742
Root relative squared error: 97.25730085147511
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.2970622687653256
Weighted FMeasure: 0.6826681442931392
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6631927464567311
Mean absolute error: 0.3542963988543371
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.4375357587064676
Kappa statistic: 0.28520556814503095
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 88.93229166666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7426488181118646
Weighted AreaUnderROC: 0.7523134328358209
Root mean squared error: 0.4323127584766417
Relative absolute error: 66.07421063564614
Root relative squared error: 86.46255169532834
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.4327816859866996
Weighted FMeasure: 0.7392058531418813
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7240182422012101
Mean absolute error: 0.3303710531782307
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.29680006218905475
Kappa statistic: 0.4319526627218934
Training time: 3.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 87.76041666666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7247019262111397
Weighted AreaUnderROC: 0.7602238805970151
Root mean squared error: 0.43818947677509124
Relative absolute error: 64.90811941103915
Root relative squared error: 87.63789535501824
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3714183707167488
Weighted FMeasure: 0.7115636871469224
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7363321218015121
Mean absolute error: 0.3245405970551958
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.4138603855721393
Kappa statistic: 0.3500295799645042
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 91.27604166666667
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6953144297820594
Weighted AreaUnderROC: 0.594731343283582
Root mean squared error: 0.4905182409345128
Relative absolute error: 74.1780598958335
Root relative squared error: 98.10364818690256
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.27728359299775457
Weighted FMeasure: 0.6565362864751596
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6158965250931233
Mean absolute error: 0.37089029947916746
Coverage of cases: 92.70833333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.4998641169154228
Kappa statistic: 0.23338657037705735
Training time: 5.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7210838050037439
Weighted AreaUnderROC: 0.652223880597015
Root mean squared error: 0.4577102945943331
Relative absolute error: 70.1641285477056
Root relative squared error: 91.54205891886662
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.35542720803472544
Weighted FMeasure: 0.7011394313983829
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6509194552261577
Mean absolute error: 0.35082064273852803
Coverage of cases: 96.35416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.43396548507462684
Kappa statistic: 0.3271477204458982
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 82.68229166666667
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7347724780701754
Weighted AreaUnderROC: 0.7308059701492539
Root mean squared error: 0.44727655675954137
Relative absolute error: 63.8758085996958
Root relative squared error: 89.45531135190828
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.39126519456050485
Weighted FMeasure: 0.7194188119257285
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7123106878761086
Mean absolute error: 0.319379042998479
Coverage of cases: 94.53125
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.4062101990049751
Kappa statistic: 0.36775553213909395
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7083898275622306
Weighted AreaUnderROC: 0.7538955223880596
Root mean squared error: 0.44465417555722264
Relative absolute error: 63.28159994400902
Root relative squared error: 88.93083511144452
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.35139702933910116
Weighted FMeasure: 0.7081408500063718
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7316723202487206
Mean absolute error: 0.3164079997200451
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 58.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.39313805970149246
Kappa statistic: 0.3447098976109215
Training time: 5.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7412760416666666
Weighted AreaUnderROC: 0.767955223880597
Root mean squared error: 0.4333943740626336
Relative absolute error: 61.91768991442489
Root relative squared error: 86.67887481252671
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.40471843907261396
Weighted FMeasure: 0.725030435687214
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7395661376849029
Mean absolute error: 0.30958844957212445
Coverage of cases: 95.57291666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3999558457711443
Kappa statistic: 0.38040042149631187
Training time: 5.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 89.97395833333333
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.712939434674265
Weighted AreaUnderROC: 0.7573731343283582
Root mean squared error: 0.4258740135985662
Relative absolute error: 67.42380573770231
Root relative squared error: 85.17480271971324
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.36792304125681535
Weighted FMeasure: 0.7142989053632288
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7361927467518535
Mean absolute error: 0.33711902868851157
Coverage of cases: 99.21875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.35298165422885575
Kappa statistic: 0.3676195080679278
Training time: 5.0
		
Time end:Tue Oct 31 11.39.39 EET 2017