Tue Oct 31 11.40.22 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.40.22 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 89.84375
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.6994234800838575
Weighted AreaUnderROC: 0.7577014925373134
Root mean squared error: 0.4346334662678427
Relative absolute error: 67.50000000000004
Root relative squared error: 86.92669325356854
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.3036657665514415
Weighted FMeasure: 0.6762089201877934
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7462109375003599
Mean absolute error: 0.3375000000000002
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.46797512437810945
Kappa statistic: 0.27243199350385716
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 84.375
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7228190104166666
Weighted AreaUnderROC: 0.7650298507462686
Root mean squared error: 0.43547148777694583
Relative absolute error: 61.87500000000006
Root relative squared error: 87.09429755538916
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.34696122197190676
Weighted FMeasure: 0.6921130161261742
Iteration time: 7.0
Weighted AreaUnderPRC: 0.754411589466915
Mean absolute error: 0.3093750000000003
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.45267475124378104
Kappa statistic: 0.3086956521739132
Training time: 6.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 84.375
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.7412464985994398
Weighted AreaUnderROC: 0.7658656716417912
Root mean squared error: 0.4460182638263445
Relative absolute error: 62.44791666666667
Root relative squared error: 89.2036527652689
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.31032411453868153
Weighted FMeasure: 0.6458333333333334
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7556785634585967
Mean absolute error: 0.31223958333333335
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.5233781094527363
Kappa statistic: 0.22368231046931408
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 77.734375
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6868707365866458
Weighted AreaUnderROC: 0.7288955223880597
Root mean squared error: 0.48083044482090215
Relative absolute error: 64.58333333333336
Root relative squared error: 96.16608896418043
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.18435179860550951
Weighted FMeasure: 0.5858247162728691
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7108027559994148
Mean absolute error: 0.3229166666666668
Coverage of cases: 91.92708333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.586538868159204
Kappa statistic: 0.10932977065241584
Training time: 9.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 84.11458333333333
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7052813032926669
Weighted AreaUnderROC: 0.7606268656716418
Root mean squared error: 0.4635404962532041
Relative absolute error: 65.15625000000001
Root relative squared error: 92.70809925064081
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.20838136094920112
Weighted FMeasure: 0.5924515208125033
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7403423242796509
Mean absolute error: 0.3257812500000001
Coverage of cases: 96.875
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5802845149253731
Kappa statistic: 0.12358049432197729
Training time: 10.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 77.99479166666667
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7413782956685498
Weighted AreaUnderROC: 0.7366268656716418
Root mean squared error: 0.4699290726623895
Relative absolute error: 61.87500000000002
Root relative squared error: 93.98581453247789
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.2958349886353631
Weighted FMeasure: 0.6345526772734614
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7293608218212609
Mean absolute error: 0.3093750000000001
Coverage of cases: 92.44791666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.5365578358208956
Kappa statistic: 0.20314547837483618
Training time: 10.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 76.5625
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7419110004056383
Weighted AreaUnderROC: 0.7400298507462687
Root mean squared error: 0.4641861156906786
Relative absolute error: 60.20833333333337
Root relative squared error: 92.83722313813571
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.3245804724519634
Weighted FMeasure: 0.6566702760002081
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7335004253810619
Mean absolute error: 0.3010416666666669
Coverage of cases: 91.40625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.5101983830845772
Kappa statistic: 0.2438779894028354
Training time: 12.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 75.91145833333333
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.7576293498168498
Weighted AreaUnderROC: 0.7697014925373135
Root mean squared error: 0.46918035800034663
Relative absolute error: 61.40625000000003
Root relative squared error: 93.83607160006933
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.2709926836783308
Weighted FMeasure: 0.6074975852052399
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7554594449564744
Mean absolute error: 0.30703125000000014
Coverage of cases: 93.48958333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 76.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.5663799751243781
Kappa statistic: 0.157393632847367
Training time: 12.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 73.17708333333333
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7163816915655151
Weighted AreaUnderROC: 0.7153134328358209
Root mean squared error: 0.486028762866287
Relative absolute error: 61.5104166666667
Root relative squared error: 97.2057525732574
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.24741787053277706
Weighted FMeasure: 0.6144400771195951
Iteration time: 13.0
Weighted AreaUnderPRC: 0.7072509456509214
Mean absolute error: 0.3075520833333335
Coverage of cases: 88.54166666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 89.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.557387748756219
Kappa statistic: 0.1628929435040668
Training time: 13.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 74.47916666666667
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.7029803240740741
Weighted AreaUnderROC: 0.7526417910447761
Root mean squared error: 0.47724251347367064
Relative absolute error: 61.24999999999999
Root relative squared error: 95.44850269473413
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.21724644968444828
Weighted FMeasure: 0.6003428270042194
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7328955829872287
Mean absolute error: 0.30624999999999997
Coverage of cases: 91.14583333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 103.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.5719633084577115
Kappa statistic: 0.13628318584070778
Training time: 13.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 74.86979166666667
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7458777492478473
Weighted AreaUnderROC: 0.7556268656716418
Root mean squared error: 0.47145055414115267
Relative absolute error: 60.05208333333335
Root relative squared error: 94.29011082823054
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.2901567199635402
Weighted FMeasure: 0.6274000745273397
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7395299030644124
Mean absolute error: 0.30026041666666675
Coverage of cases: 91.14583333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 119.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.5448790422885572
Kappa statistic: 0.19103099582325803
Training time: 16.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 80.859375
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.731429814340979
Weighted AreaUnderROC: 0.7445671641791045
Root mean squared error: 0.4606946747394996
Relative absolute error: 63.54166666666664
Root relative squared error: 92.13893494789993
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.2844376185601682
Weighted FMeasure: 0.6326244409266798
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7345463863205516
Mean absolute error: 0.3177083333333332
Coverage of cases: 94.53125
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.5379536691542289
Kappa statistic: 0.19784930611058643
Training time: 21.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 85.15625
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7340498420661464
Weighted AreaUnderROC: 0.7284328358208955
Root mean squared error: 0.45127135221874354
Relative absolute error: 65.00000000000003
Root relative squared error: 90.25427044374871
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.31454454668845416
Weighted FMeasure: 0.6546470466799436
Iteration time: 25.0
Weighted AreaUnderPRC: 0.7256749981909992
Mean absolute error: 0.3250000000000001
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 165.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.5115942164179105
Kappa statistic: 0.23858520900321542
Training time: 24.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 85.41666666666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7316804679284964
Weighted AreaUnderROC: 0.7908059701492537
Root mean squared error: 0.42631536644445106
Relative absolute error: 60.78125000000004
Root relative squared error: 85.26307328889021
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.36882247871836893
Weighted FMeasure: 0.7029137272192828
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7785421872142478
Mean absolute error: 0.3039062500000002
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.43809919154228855
Kappa statistic: 0.3322077395826299
Training time: 28.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 87.5
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7247019262111397
Weighted AreaUnderROC: 0.7945074626865671
Root mean squared error: 0.4183300132670376
Relative absolute error: 62.187500000000064
Root relative squared error: 83.66600265340752
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3714183707167488
Weighted FMeasure: 0.7115636871469224
Iteration time: 30.0
Weighted AreaUnderPRC: 0.7784653212361926
Mean absolute error: 0.3109375000000003
Coverage of cases: 97.65625
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 223.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.4138603855721393
Kappa statistic: 0.3500295799645042
Training time: 30.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 87.890625
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7345770747884566
Weighted AreaUnderROC: 0.8050746268656717
Root mean squared error: 0.412815535883361
Relative absolute error: 61.3541666666667
Root relative squared error: 82.5631071766722
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.40232516404785823
Weighted FMeasure: 0.728674108560587
Iteration time: 32.0
Weighted AreaUnderPRC: 0.7897927400727275
Mean absolute error: 0.3067708333333335
Coverage of cases: 98.17708333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 255.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.38057555970149254
Kappa statistic: 0.3892423366107577
Training time: 32.0
		
Time end:Tue Oct 31 11.40.23 EET 2017