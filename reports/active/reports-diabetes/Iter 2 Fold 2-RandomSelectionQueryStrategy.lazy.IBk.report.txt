Tue Oct 31 11.39.30 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.30 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.6534976287262872
Weighted AreaUnderROC: 0.6194626865671641
Root mean squared error: 0.583245273713955
Relative absolute error: 70.56623931623935
Root relative squared error: 116.649054742791
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.23734362966775838
Weighted FMeasure: 0.6522004467109426
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6127056338922764
Mean absolute error: 0.3528311965811967
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 1.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.4121162935323383
Kappa statistic: 0.23728210601209526
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6862118561535132
Weighted AreaUnderROC: 0.657044776119403
Root mean squared error: 0.560249617122625
Relative absolute error: 64.79591836734686
Root relative squared error: 112.049923424525
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.3088111284383598
Weighted FMeasure: 0.682399207291598
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6386495258727146
Mean absolute error: 0.3239795918367343
Coverage of cases: 67.96875
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.365597947761194
Kappa statistic: 0.30821957935438515
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6961938545688545
Weighted AreaUnderROC: 0.6626567164179105
Root mean squared error: 0.5426555694273736
Relative absolute error: 60.57556497175135
Root relative squared error: 108.53111388547472
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.3309173299482369
Weighted FMeasure: 0.6979651820639049
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6444123016710906
Mean absolute error: 0.30287782485875675
Coverage of cases: 70.05208333333333
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.3752074004975124
Kappa statistic: 0.33046273273091165
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6921924667313183
Weighted AreaUnderROC: 0.654
Root mean squared error: 0.5433151929274437
Relative absolute error: 60.47705314009658
Root relative squared error: 108.66303858548874
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.320514779088312
Weighted FMeasure: 0.6945767172737192
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6389393835521782
Mean absolute error: 0.3023852657004829
Coverage of cases: 70.05208333333333
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.3925208333333334
Kappa statistic: 0.31847644916352863
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6671831244306489
Weighted AreaUnderROC: 0.6241492537313434
Root mean squared error: 0.5624037420532232
Relative absolute error: 64.51740506329115
Root relative squared error: 112.48074841064464
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.26397617953828695
Weighted FMeasure: 0.6699500322816854
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6178683450160157
Mean absolute error: 0.3225870253164558
Coverage of cases: 67.96875
Instances selection time: 0.0
Test time: 12.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.4313889925373134
Kappa statistic: 0.2604747291288283
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6623705518487628
Weighted AreaUnderROC: 0.6201492537313434
Root mean squared error: 0.567357786191091
Relative absolute error: 65.49625468164791
Root relative squared error: 113.4715572382182
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.2540351899519681
Weighted FMeasure: 0.6654847638643936
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6148809468864389
Mean absolute error: 0.32748127340823957
Coverage of cases: 67.44791666666667
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.4341806592039801
Kappa statistic: 0.25117004680187205
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6742520336270337
Weighted AreaUnderROC: 0.6333432835820897
Root mean squared error: 0.5585219815706103
Relative absolute error: 63.394360269360284
Root relative squared error: 111.70439631412205
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.2804017577149318
Weighted FMeasure: 0.6770339062967444
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6240773002247612
Mean absolute error: 0.3169718013468014
Coverage of cases: 68.48958333333333
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.41820926616915427
Kappa statistic: 0.27774668905055033
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8125
Correctly Classified Instances: 67.1875
Weighted Precision: 0.6635756499299096
Weighted AreaUnderROC: 0.6250746268656716
Root mean squared error: 0.5702067352275962
Relative absolute error: 65.940366972477
Root relative squared error: 114.04134704551925
Weighted TruePositiveRate: 0.671875
Weighted MatthewsCorrelation: 0.2584281348366165
Weighted FMeasure: 0.666545542635659
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6176303499302634
Mean absolute error: 0.32970183486238497
Coverage of cases: 67.1875
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 6.0
Weighted Recall: 0.671875
Weighted FalsePositiveRate: 0.4217257462686567
Kappa statistic: 0.2572761881370503
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6591540404040405
Weighted AreaUnderROC: 0.6210746268656716
Root mean squared error: 0.5749346632493343
Relative absolute error: 66.94677871148458
Root relative squared error: 114.98693264986686
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.24900780041510998
Weighted FMeasure: 0.6620198739748971
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6147268584280303
Mean absolute error: 0.33473389355742295
Coverage of cases: 66.66666666666667
Instances selection time: 1.0
Test time: 16.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.42451741293532336
Kappa statistic: 0.2481644640234948
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6601593459308389
Weighted AreaUnderROC: 0.6228059701492538
Root mean squared error: 0.5751211847435271
Relative absolute error: 66.92506459948314
Root relative squared error: 115.02423694870542
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.2514440241061015
Weighted FMeasure: 0.6627604166666666
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6157905223128624
Mean absolute error: 0.3346253229974157
Coverage of cases: 66.66666666666667
Instances selection time: 1.0
Test time: 17.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.42105472636815916
Kappa statistic: 0.25082307035727336
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8125
Correctly Classified Instances: 67.1875
Weighted Precision: 0.6645280934343435
Weighted AreaUnderROC: 0.6268059701492538
Root mean squared error: 0.5707690669421964
Relative absolute error: 65.87230215827343
Root relative squared error: 114.15381338843929
Weighted TruePositiveRate: 0.671875
Weighted MatthewsCorrelation: 0.26079515191405006
Weighted FMeasure: 0.6673008134440392
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6186962397411616
Mean absolute error: 0.32936151079136716
Coverage of cases: 67.1875
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 8.0
Weighted Recall: 0.671875
Weighted FalsePositiveRate: 0.41826305970149247
Kappa statistic: 0.2599118942731278
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6717420010639915
Weighted AreaUnderROC: 0.6374626865671642
Root mean squared error: 0.568636430865923
Relative absolute error: 65.33836689038037
Root relative squared error: 113.7272861731846
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.2774420477846189
Weighted FMeasure: 0.6729963514412277
Iteration time: 0.0
Weighted AreaUnderPRC: 0.625573947556857
Mean absolute error: 0.32669183445190186
Coverage of cases: 67.44791666666667
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.3995537935323383
Kappa statistic: 0.2773261065943992
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6781157715023994
Weighted AreaUnderROC: 0.6434626865671641
Root mean squared error: 0.5618897192191878
Relative absolute error: 63.7709643605871
Root relative squared error: 112.37794384383757
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.29127032008994624
Weighted FMeasure: 0.6799054865737938
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6300868488141381
Mean absolute error: 0.3188548218029355
Coverage of cases: 68.22916666666667
Instances selection time: 1.0
Test time: 20.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.3953662935323383
Kappa statistic: 0.29095532146749
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6784118077595869
Weighted AreaUnderROC: 0.642
Root mean squared error: 0.5596859817480977
Relative absolute error: 63.2396449704142
Root relative squared error: 111.93719634961954
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.2913870080514502
Weighted FMeasure: 0.6808563142605971
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6294376899783624
Mean absolute error: 0.316198224852071
Coverage of cases: 68.48958333333333
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.4008958333333333
Kappa statistic: 0.2905393025102303
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6829110181808572
Weighted AreaUnderROC: 0.646
Root mean squared error: 0.5551327536703662
Relative absolute error: 62.19157355679707
Root relative squared error: 111.02655073407324
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.30096004095261253
Weighted FMeasure: 0.6854305774076774
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6325434549925512
Mean absolute error: 0.3109578677839853
Coverage of cases: 69.01041666666667
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.3981041666666667
Kappa statistic: 0.2997854734906527
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6891306012946611
Weighted AreaUnderROC: 0.6534626865671642
Root mean squared error: 0.5505291958739739
Relative absolute error: 61.14417989417981
Root relative squared error: 110.10583917479477
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.31490868370664393
Weighted FMeasure: 0.6914065187478501
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6378859493609891
Mean absolute error: 0.30572089947089903
Coverage of cases: 69.53125
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.3883871268656716
Kappa statistic: 0.31399254870823917
Training time: 0.0
		
Time end:Tue Oct 31 11.39.31 EET 2017