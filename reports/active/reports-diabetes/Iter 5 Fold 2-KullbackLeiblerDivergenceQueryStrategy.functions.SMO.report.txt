Tue Oct 31 11.37.23 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.23 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.7853133378016087
Weighted AreaUnderROC: 0.5410447761194029
Root mean squared error: 0.5659615711335886
Relative absolute error: 64.0625
Root relative squared error: 113.19231422671771
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.23456301313414055
Weighted FMeasure: 0.5754507527536393
Iteration time: 18.0
Weighted AreaUnderPRC: 0.576776553947442
Mean absolute error: 0.3203125
Coverage of cases: 67.96875
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.597597947761194
Kappa statistic: 0.10430099370401277
Training time: 17.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7417589662447258
Weighted AreaUnderROC: 0.6562985074626866
Root mean squared error: 0.5103103630798288
Relative absolute error: 52.083333333333336
Root relative squared error: 102.06207261596576
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.39030959919418445
Weighted FMeasure: 0.7122233203885759
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6512549034150844
Mean absolute error: 0.2604166666666667
Coverage of cases: 73.95833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.42698631840796014
Kappa statistic: 0.35292531679698036
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7667100694444445
Weighted AreaUnderROC: 0.7149253731343284
Root mean squared error: 0.47871355387816905
Relative absolute error: 45.83333333333333
Root relative squared error: 95.74271077563381
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.4731602234073839
Weighted FMeasure: 0.7599954205053607
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6941460503472222
Mean absolute error: 0.22916666666666666
Coverage of cases: 77.08333333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.3409825870646766
Kappa statistic: 0.46012269938650313
Training time: 8.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7795148671174345
Weighted AreaUnderROC: 0.7631343283582089
Root mean squared error: 0.481425833678806
Relative absolute error: 46.35416666666667
Root relative squared error: 96.2851667357612
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.5112653008543989
Weighted FMeasure: 0.7715693347829123
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7245730181425935
Mean absolute error: 0.23177083333333334
Coverage of cases: 76.82291666666667
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.24196050995024876
Kappa statistic: 0.5078057491791002
Training time: 17.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7790304978763567
Weighted AreaUnderROC: 0.7400298507462687
Root mean squared error: 0.5519850541454905
Relative absolute error: 60.9375
Root relative squared error: 110.3970108290981
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.4629432063175498
Weighted FMeasure: 0.7005556644954473
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7033799900392776
Mean absolute error: 0.3046875
Coverage of cases: 69.53125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.21525279850746268
Kappa statistic: 0.417215794116121
Training time: 8.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.28125
Correctly Classified Instances: 61.71875
Weighted Precision: 0.759471525096525
Weighted AreaUnderROC: 0.686955223880597
Root mean squared error: 0.6187184335382291
Relative absolute error: 76.5625
Root relative squared error: 123.74368670764582
Weighted TruePositiveRate: 0.6171875
Weighted MatthewsCorrelation: 0.38035148421101267
Weighted FMeasure: 0.6142652671755725
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6634409785064083
Mean absolute error: 0.3828125
Coverage of cases: 61.71875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6171875
Weighted FalsePositiveRate: 0.243277052238806
Kappa statistic: 0.3073865030674847
Training time: 8.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.7697172619047619
Weighted AreaUnderROC: 0.7112238805970149
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.4183468327771462
Weighted FMeasure: 0.652603954687288
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6813623821924603
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 86.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.2285939054726368
Kappa statistic: 0.354864593781344
Training time: 8.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7790152211927147
Weighted AreaUnderROC: 0.7609850746268657
Root mean squared error: 0.5025974200756175
Relative absolute error: 50.520833333333336
Root relative squared error: 100.5194840151235
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.49872900262947334
Weighted FMeasure: 0.7530628649837915
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7207199144499846
Mean absolute error: 0.2526041666666667
Coverage of cases: 74.73958333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 96.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.22542568407960198
Kappa statistic: 0.48424259207975645
Training time: 9.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7887046737860587
Weighted AreaUnderROC: 0.7670149253731343
Root mean squared error: 0.4592793267718459
Relative absolute error: 42.1875
Root relative squared error: 91.85586535436919
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5349648602170413
Weighted FMeasure: 0.788878002422122
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7319693871788587
Mean absolute error: 0.2109375
Coverage of cases: 78.90625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 106.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.2550326492537313
Kappa statistic: 0.5349560432988457
Training time: 9.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.135416666666668
Correctly Classified Instances: 77.86458333333333
Weighted Precision: 0.783693881354302
Weighted AreaUnderROC: 0.7659402985074627
Root mean squared error: 0.4704829079431756
Relative absolute error: 44.27083333333333
Root relative squared error: 94.09658158863512
Weighted TruePositiveRate: 0.7786458333333334
Weighted MatthewsCorrelation: 0.5229420914764015
Weighted FMeasure: 0.7805197773966327
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7285915285899752
Mean absolute error: 0.22135416666666666
Coverage of cases: 77.86458333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 115.0
Weighted Recall: 0.7786458333333334
Weighted FalsePositiveRate: 0.246765236318408
Kappa statistic: 0.5219403597164451
Training time: 9.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.833333333333332
Correctly Classified Instances: 79.16666666666667
Weighted Precision: 0.7876055880441845
Weighted AreaUnderROC: 0.7534328358208954
Root mean squared error: 0.45643546458763845
Relative absolute error: 41.66666666666667
Root relative squared error: 91.28709291752769
Weighted TruePositiveRate: 0.7916666666666666
Weighted MatthewsCorrelation: 0.5287872495731752
Weighted FMeasure: 0.7872725392886682
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7243051113750812
Mean absolute error: 0.20833333333333334
Coverage of cases: 79.16666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 125.0
Weighted Recall: 0.7916666666666666
Weighted FalsePositiveRate: 0.2848009950248756
Kappa statistic: 0.5250463821892392
Training time: 9.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8006367370892019
Weighted AreaUnderROC: 0.7527761194029851
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5490719672556438
Weighted FMeasure: 0.7940054899324562
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7289739369375979
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 135.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.2965310945273632
Kappa statistic: 0.537173306267445
Training time: 9.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.052083333333332
Correctly Classified Instances: 79.94791666666667
Weighted Precision: 0.7995494521337947
Weighted AreaUnderROC: 0.7455820895522388
Root mean squared error: 0.4477955262542641
Relative absolute error: 40.10416666666667
Root relative squared error: 89.55910525085282
Weighted TruePositiveRate: 0.7994791666666666
Weighted MatthewsCorrelation: 0.5425482480447116
Weighted FMeasure: 0.7896587128301102
Iteration time: 10.0
Weighted AreaUnderPRC: 0.724087274500793
Mean absolute error: 0.20052083333333334
Coverage of cases: 79.94791666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 145.0
Weighted Recall: 0.7994791666666666
Weighted FalsePositiveRate: 0.30831498756218906
Kappa statistic: 0.5267302644215378
Training time: 10.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.797949858546754
Weighted AreaUnderROC: 0.7401194029850746
Root mean squared error: 0.45069390943299864
Relative absolute error: 40.625
Root relative squared error: 90.13878188659973
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5362833512971387
Weighted FMeasure: 0.7858707099239134
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7200696777440688
Mean absolute error: 0.203125
Coverage of cases: 79.6875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 156.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.3166361940298507
Kappa statistic: 0.517898532062838
Training time: 11.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.3125
Correctly Classified Instances: 79.6875
Weighted Precision: 0.797949858546754
Weighted AreaUnderROC: 0.7401194029850746
Root mean squared error: 0.45069390943299864
Relative absolute error: 40.625
Root relative squared error: 90.13878188659973
Weighted TruePositiveRate: 0.796875
Weighted MatthewsCorrelation: 0.5362833512971387
Weighted FMeasure: 0.7858707099239134
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7200696777440688
Mean absolute error: 0.203125
Coverage of cases: 79.6875
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 184.0
Weighted Recall: 0.796875
Weighted FalsePositiveRate: 0.3166361940298507
Kappa statistic: 0.517898532062838
Training time: 24.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8063510954135955
Weighted AreaUnderROC: 0.7423880597014926
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5497641672154274
Weighted FMeasure: 0.7899156024156025
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7244266860966079
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 213.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.3173072139303483
Kappa statistic: 0.5267254800207577
Training time: 28.0
		
Time end:Tue Oct 31 11.37.24 EET 2017