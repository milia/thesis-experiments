Tue Oct 31 11.39.03 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.03 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 59.635416666666664
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6859387300030687
Weighted AreaUnderROC: 0.6333432835820897
Root mean squared error: 0.5326785742615957
Relative absolute error: 61.556712962963
Root relative squared error: 106.53571485231915
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.292501652164861
Weighted FMeasure: 0.6791883246770988
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6365782747087537
Mean absolute error: 0.307783564814815
Coverage of cases: 77.34375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.4479238184079602
Kappa statistic: 0.27706109619540314
Training time: 2.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 61.458333333333336
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6759785353535354
Weighted AreaUnderROC: 0.6293134328358209
Root mean squared error: 0.5551427619448956
Relative absolute error: 65.81439393939394
Root relative squared error: 111.02855238897914
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.2868629748679112
Weighted FMeasure: 0.6765126211944202
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6249160968155109
Mean absolute error: 0.32907196969696967
Coverage of cases: 75.78125
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.3912325870646766
Kappa statistic: 0.286843997124371
Training time: 2.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 69.66145833333333
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6990255664239134
Weighted AreaUnderROC: 0.6809402985074628
Root mean squared error: 0.5045698785768312
Relative absolute error: 63.443627450980344
Root relative squared error: 100.91397571536625
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.3364173628003118
Weighted FMeasure: 0.6935247501608927
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6642974074606319
Mean absolute error: 0.3172181372549017
Coverage of cases: 86.45833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.346163868159204
Kappa statistic: 0.33523421588594693
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 68.09895833333333
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6939131741158552
Weighted AreaUnderROC: 0.6679402985074626
Root mean squared error: 0.5272062776472525
Relative absolute error: 64.56018518518502
Root relative squared error: 105.4412555294505
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.32520539722876873
Weighted FMeasure: 0.6883739056257817
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6499427944804892
Mean absolute error: 0.32280092592592513
Coverage of cases: 83.85416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 10.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.35241822139303486
Kappa statistic: 0.3240616816991563
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.911458333333336
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.673981482130999
Weighted AreaUnderROC: 0.6368955223880597
Root mean squared error: 0.5717184439622941
Relative absolute error: 67.81250000000001
Root relative squared error: 114.34368879245882
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.28020760620436924
Weighted FMeasure: 0.6641111849546494
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6296536420509147
Mean absolute error: 0.3390625000000001
Coverage of cases: 67.70833333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.36983924129353235
Kappa statistic: 0.2779468335534247
Training time: 4.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 92.70833333333333
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7013526897414512
Weighted AreaUnderROC: 0.6243283582089553
Root mean squared error: 0.490484331854826
Relative absolute error: 78.10979385198155
Root relative squared error: 98.0968663709652
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3388608533794078
Weighted FMeasure: 0.7029504703115813
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6448664292545998
Mean absolute error: 0.39054896925990773
Coverage of cases: 92.1875
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.3904001865671642
Kappa statistic: 0.33503900156006244
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 85.9375
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7307998208549543
Weighted AreaUnderROC: 0.7328805970149254
Root mean squared error: 0.44146019810965226
Relative absolute error: 68.41133958428304
Root relative squared error: 88.29203962193046
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.3993594331001041
Weighted FMeasure: 0.711820259297289
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7162891818020519
Mean absolute error: 0.34205669792141524
Coverage of cases: 94.79166666666667
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.28931125621890547
Kappa statistic: 0.3913482578690456
Training time: 3.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 87.109375
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7539711047254151
Weighted AreaUnderROC: 0.7446567164179104
Root mean squared error: 0.4337160806649413
Relative absolute error: 72.16894626611521
Root relative squared error: 86.74321613298825
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45758283975804454
Weighted FMeasure: 0.7552362138021884
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7162783463128717
Mean absolute error: 0.36084473133057604
Coverage of cases: 96.61458333333333
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3098722014925373
Kappa statistic: 0.4566368449881321
Training time: 3.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 77.47395833333333
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.7347948645546998
Weighted AreaUnderROC: 0.6980895522388059
Root mean squared error: 0.4961879431533012
Relative absolute error: 65.6022826810869
Root relative squared error: 99.23758863066024
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.4032031755191497
Weighted FMeasure: 0.7073270349346509
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6926186456713787
Mean absolute error: 0.3280114134054345
Coverage of cases: 88.54166666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.2782521766169154
Kappa statistic: 0.3904930160658092
Training time: 5.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 99.21875
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.7106670774056235
Weighted AreaUnderROC: 0.7193731343283583
Root mean squared error: 0.48011627690008374
Relative absolute error: 76.59070020311259
Root relative squared error: 96.02325538001675
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.32347989325813925
Weighted FMeasure: 0.6306372549019609
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7099950648417473
Mean absolute error: 0.3829535010155629
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.2910298507462687
Kappa statistic: 0.2880832217530127
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 100.0
Incorrectly Classified Instances: 29.166666666666668
Correctly Classified Instances: 70.83333333333333
Weighted Precision: 0.7517589075782238
Weighted AreaUnderROC: 0.7320597014925374
Root mean squared error: 0.43588951168665346
Relative absolute error: 72.92268786127178
Root relative squared error: 87.17790233733069
Weighted TruePositiveRate: 0.7083333333333334
Weighted MatthewsCorrelation: 0.4338093129464006
Weighted FMeasure: 0.7151457012568123
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6983101045349808
Mean absolute error: 0.3646134393063589
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 43.0
Weighted Recall: 0.7083333333333334
Weighted FalsePositiveRate: 0.25328855721393034
Kappa statistic: 0.41482529661478185
Training time: 3.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 82.8125
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7765458923295346
Weighted AreaUnderROC: 0.7515970149253732
Root mean squared error: 0.43636988575613256
Relative absolute error: 60.98620383597838
Root relative squared error: 87.27397715122652
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.5070593474583531
Weighted FMeasure: 0.772926424668228
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7305289714488343
Mean absolute error: 0.3049310191798919
Coverage of cases: 92.96875
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.2544154228855721
Kappa statistic: 0.505907123640192
Training time: 6.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7659600703379654
Weighted AreaUnderROC: 0.7575522388059701
Root mean squared error: 0.4251622777511405
Relative absolute error: 63.329429770812276
Root relative squared error: 85.03245555022811
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.4821595770612664
Weighted FMeasure: 0.7665649935649936
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7409842640667869
Mean absolute error: 0.31664714885406137
Coverage of cases: 95.3125
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.30635572139303485
Kappa statistic: 0.47941828937638664
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 79.55729166666667
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7596124939874939
Weighted AreaUnderROC: 0.7245223880597015
Root mean squared error: 0.4519616565818088
Relative absolute error: 61.78771052367856
Root relative squared error: 90.39233131636176
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4709154098708817
Weighted FMeasure: 0.759993235079731
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7081526912175363
Mean absolute error: 0.3089385526183928
Coverage of cases: 90.36458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 62.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.2911629353233831
Kappa statistic: 0.47088425593098476
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 22.65625
Correctly Classified Instances: 77.34375
Weighted Precision: 0.7699655211350963
Weighted AreaUnderROC: 0.7943880597014924
Root mean squared error: 0.4111190582640513
Relative absolute error: 59.59303675002404
Root relative squared error: 82.22381165281027
Weighted TruePositiveRate: 0.7734375
Weighted MatthewsCorrelation: 0.49271115052715464
Weighted FMeasure: 0.7710274258149504
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7730718598302522
Mean absolute error: 0.2979651837501202
Coverage of cases: 96.61458333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.7734375
Weighted FalsePositiveRate: 0.29110914179104475
Kappa statistic: 0.49169253240825267
Training time: 6.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 99.34895833333333
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7588595295780335
Weighted AreaUnderROC: 0.7584179104477612
Root mean squared error: 0.42651175947258624
Relative absolute error: 63.74608482536627
Root relative squared error: 85.30235189451724
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4691643492796162
Weighted FMeasure: 0.7595523689273689
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7401538879728404
Mean absolute error: 0.31873042412683134
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.29462562189054725
Kappa statistic: 0.4690393170614403
Training time: 7.0
		
Time end:Tue Oct 31 11.39.03 EET 2017