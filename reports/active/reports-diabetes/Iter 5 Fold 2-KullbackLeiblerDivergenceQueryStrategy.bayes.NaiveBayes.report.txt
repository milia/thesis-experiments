Tue Oct 31 11.37.13 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.13 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 84.24479166666667
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.704583794334221
Weighted AreaUnderROC: 0.7767761194029851
Root mean squared error: 0.458457736938823
Relative absolute error: 63.31559276333757
Root relative squared error: 91.6915473877646
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.337207669843337
Weighted FMeasure: 0.7002611520360139
Iteration time: 9.0
Weighted AreaUnderPRC: 0.7701801428052573
Mean absolute error: 0.31657796381668785
Coverage of cases: 95.57291666666667
Instances selection time: 4.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.4153100124378109
Kappa statistic: 0.3250338644133395
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 88.671875
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7173669781532684
Weighted AreaUnderROC: 0.7791940298507463
Root mean squared error: 0.4421824939034921
Relative absolute error: 63.45084263328783
Root relative squared error: 88.43649878069843
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.372138751521559
Weighted FMeasure: 0.717468174340154
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7774401318564222
Mean absolute error: 0.31725421316643915
Coverage of cases: 97.13541666666667
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.37856249999999997
Kappa statistic: 0.3663963794078823
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 91.92708333333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7260244899846731
Weighted AreaUnderROC: 0.7688358208955224
Root mean squared error: 0.44718154357003276
Relative absolute error: 66.29349327750768
Root relative squared error: 89.43630871400656
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3954754003685219
Weighted FMeasure: 0.7277256258234518
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7731540426290907
Mean absolute error: 0.3314674663875384
Coverage of cases: 95.83333333333333
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.34806934079601987
Kappa statistic: 0.3939319644498928
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 93.75
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.75
Weighted AreaUnderROC: 0.7789850746268657
Root mean squared error: 0.44121957154956215
Relative absolute error: 68.15734405493365
Root relative squared error: 88.24391430991243
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.44979104477611936
Weighted FMeasure: 0.75
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7787142107835239
Mean absolute error: 0.3407867202746682
Coverage of cases: 96.35416666666667
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 24.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.30020895522388064
Kappa statistic: 0.4497910447761195
Training time: 1.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 94.01041666666667
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7479443828529194
Weighted AreaUnderROC: 0.7832835820895522
Root mean squared error: 0.449926821309598
Relative absolute error: 71.98169675253766
Root relative squared error: 89.9853642619196
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.439278803149114
Weighted FMeasure: 0.7341555999809604
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7788878123880062
Mean absolute error: 0.3599084837626883
Coverage of cases: 95.83333333333333
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.27328606965174135
Kappa statistic: 0.4333711691259931
Training time: 1.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 93.359375
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7486167401080768
Weighted AreaUnderROC: 0.7914029850746269
Root mean squared error: 0.45590397116010356
Relative absolute error: 73.58176836225526
Root relative squared error: 91.18079423202072
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.4320957488201411
Weighted FMeasure: 0.7200878187894403
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7856777263675632
Mean absolute error: 0.3679088418112763
Coverage of cases: 95.57291666666667
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.26088495024875624
Kappa statistic: 0.4179252563113217
Training time: 1.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 94.66145833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7671806917211329
Weighted AreaUnderROC: 0.8089552238805972
Root mean squared error: 0.4403287219428564
Relative absolute error: 71.87113693341047
Root relative squared error: 88.06574438857128
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.47284145963180607
Weighted FMeasure: 0.7403748351525015
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8056940939712515
Mean absolute error: 0.3593556846670523
Coverage of cases: 96.875
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 35.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.239330223880597
Kappa statistic: 0.4585268745852688
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 92.05729166666667
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7712038866930171
Weighted AreaUnderROC: 0.8201791044776119
Root mean squared error: 0.4201383024731783
Relative absolute error: 65.04920368738122
Root relative squared error: 84.02766049463565
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4934010171681369
Weighted FMeasure: 0.7637442129629629
Iteration time: 4.0
Weighted AreaUnderPRC: 0.8158611445256475
Mean absolute error: 0.3252460184369061
Coverage of cases: 97.39583333333333
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.2530733830845771
Kappa statistic: 0.4903635314483554
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 21.354166666666668
Correctly Classified Instances: 78.64583333333333
Weighted Precision: 0.7864583333333334
Weighted AreaUnderROC: 0.8252835820895522
Root mean squared error: 0.40595954316851246
Relative absolute error: 62.71421646164197
Root relative squared error: 81.1919086337025
Weighted TruePositiveRate: 0.7864583333333334
Weighted MatthewsCorrelation: 0.5300298507462686
Weighted FMeasure: 0.7864583333333334
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8144644800278064
Mean absolute error: 0.31357108230820985
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 42.0
Weighted Recall: 0.7864583333333334
Weighted FalsePositiveRate: 0.2564284825870647
Kappa statistic: 0.5300298507462687
Training time: 1.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 21.875
Correctly Classified Instances: 78.125
Weighted Precision: 0.7805209836459838
Weighted AreaUnderROC: 0.8226268656716419
Root mean squared error: 0.4057837000921995
Relative absolute error: 59.27107965825022
Root relative squared error: 81.1567400184399
Weighted TruePositiveRate: 0.78125
Weighted MatthewsCorrelation: 0.5169285186216244
Weighted FMeasure: 0.780863388551059
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8128567337901402
Mean absolute error: 0.2963553982912511
Coverage of cases: 97.13541666666667
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 45.0
Weighted Recall: 0.78125
Weighted FalsePositiveRate: 0.2661455223880597
Kappa statistic: 0.5168943206326384
Training time: 1.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 21.875
Correctly Classified Instances: 78.125
Weighted Precision: 0.7786814015011689
Weighted AreaUnderROC: 0.8233432835820894
Root mean squared error: 0.40410444156596015
Relative absolute error: 57.92553497713141
Root relative squared error: 80.82088831319203
Weighted TruePositiveRate: 0.78125
Weighted MatthewsCorrelation: 0.5123594020059105
Weighted FMeasure: 0.7796070563294973
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8151767089265226
Mean absolute error: 0.28962767488565705
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 51.0
Weighted Recall: 0.78125
Weighted FalsePositiveRate: 0.2765335820895522
Kappa statistic: 0.511805303305485
Training time: 5.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.671875
Incorrectly Classified Instances: 20.833333333333332
Correctly Classified Instances: 79.16666666666667
Weighted Precision: 0.7888234077750207
Weighted AreaUnderROC: 0.8284776119402985
Root mean squared error: 0.3986649011636843
Relative absolute error: 57.612465574033635
Root relative squared error: 79.73298023273686
Weighted TruePositiveRate: 0.7916666666666666
Weighted MatthewsCorrelation: 0.5343238890820448
Weighted FMeasure: 0.789671682626539
Iteration time: 3.0
Weighted AreaUnderPRC: 0.8196257587242147
Mean absolute error: 0.28806232787016817
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 54.0
Weighted Recall: 0.7916666666666666
Weighted FalsePositiveRate: 0.26748756218905473
Kappa statistic: 0.5334143377885783
Training time: 1.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.02083333333333
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7906946976903969
Weighted AreaUnderROC: 0.8278208955223881
Root mean squared error: 0.3986950071176287
Relative absolute error: 56.826968299170254
Root relative squared error: 79.73900142352574
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.537248439492386
Weighted FMeasure: 0.7911681984471134
Iteration time: 2.0
Weighted AreaUnderPRC: 0.8162023385011964
Mean absolute error: 0.2841348414958513
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.27301710199004975
Kappa statistic: 0.5351517008887527
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 87.36979166666667
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7850788477544096
Weighted AreaUnderROC: 0.8268358208955225
Root mean squared error: 0.39947987447856215
Relative absolute error: 55.84415515015466
Root relative squared error: 79.89597489571243
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5243316743495029
Weighted FMeasure: 0.78538722286868
Iteration time: 5.0
Weighted AreaUnderPRC: 0.8185579877269964
Mean absolute error: 0.2792207757507733
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.2827341417910448
Kappa statistic: 0.5216829673371471
Training time: 4.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 21.09375
Correctly Classified Instances: 78.90625
Weighted Precision: 0.7849215088087926
Weighted AreaUnderROC: 0.8274029850746268
Root mean squared error: 0.40048272168659554
Relative absolute error: 55.06957280231833
Root relative squared error: 80.0965443373191
Weighted TruePositiveRate: 0.7890625
Weighted MatthewsCorrelation: 0.5232983105758581
Weighted FMeasure: 0.7848757747754022
Iteration time: 3.0
Weighted AreaUnderPRC: 0.820318482084759
Mean absolute error: 0.27534786401159167
Coverage of cases: 97.65625
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7890625
Weighted FalsePositiveRate: 0.286196828358209
Kappa statistic: 0.5199703685412679
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.80729166666667
Incorrectly Classified Instances: 20.833333333333332
Correctly Classified Instances: 79.16666666666667
Weighted Precision: 0.7875744047619048
Weighted AreaUnderROC: 0.828268656716418
Root mean squared error: 0.3992909871596286
Relative absolute error: 54.81946107530664
Root relative squared error: 79.85819743192572
Weighted TruePositiveRate: 0.7916666666666666
Weighted MatthewsCorrelation: 0.52789156992216
Weighted FMeasure: 0.7867411145375822
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8190755644632052
Mean absolute error: 0.2740973053765332
Coverage of cases: 97.65625
Instances selection time: 0.0
Test time: 10.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7916666666666666
Weighted FalsePositiveRate: 0.2882636815920398
Kappa statistic: 0.5233366434955312
Training time: 1.0
		
Time end:Tue Oct 31 11.37.13 EET 2017