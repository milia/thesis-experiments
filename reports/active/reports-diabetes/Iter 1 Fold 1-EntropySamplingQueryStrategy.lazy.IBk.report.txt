Tue Oct 31 11.36.34 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.36.34 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6703527576489533
Weighted AreaUnderROC: 0.6278805970149254
Root mean squared error: 0.5565306203264541
Relative absolute error: 64.47649572649574
Root relative squared error: 111.30612406529082
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.27113815439187894
Weighted FMeasure: 0.6730767971802366
Iteration time: 82.0
Weighted AreaUnderPRC: 0.6204344523701691
Mean absolute error: 0.3223824786324787
Coverage of cases: 68.22916666666667
Instances selection time: 81.0
Test time: 34.0
Accumulative iteration time: 82.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.426530472636816
Kappa statistic: 0.2678169542385596
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.072916666666664
Correctly Classified Instances: 66.92708333333333
Weighted Precision: 0.6500735648026604
Weighted AreaUnderROC: 0.5988358208955223
Root mean squared error: 0.5692835103168721
Relative absolute error: 66.83673469387756
Root relative squared error: 113.85670206337441
Weighted TruePositiveRate: 0.6692708333333334
Weighted MatthewsCorrelation: 0.22157066776171633
Weighted FMeasure: 0.65076299365664
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6016888718479715
Mean absolute error: 0.33418367346938777
Coverage of cases: 66.92708333333333
Instances selection time: 15.0
Test time: 22.0
Accumulative iteration time: 98.0
Weighted Recall: 0.6692708333333334
Weighted FalsePositiveRate: 0.4715991915422886
Kappa statistic: 0.21357156679352396
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.659839307947707
Weighted AreaUnderROC: 0.5999104477611941
Root mean squared error: 0.5612087784588262
Relative absolute error: 64.67161016949157
Root relative squared error: 112.24175569176525
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.23561332014790937
Weighted FMeasure: 0.6542039398553482
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6036357071068154
Mean absolute error: 0.3233580508474579
Coverage of cases: 67.96875
Instances selection time: 23.0
Test time: 25.0
Accumulative iteration time: 122.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.4798666044776119
Kappa statistic: 0.22085120422302862
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.072916666666664
Correctly Classified Instances: 66.92708333333333
Weighted Precision: 0.645473845233376
Weighted AreaUnderROC: 0.5832537313432836
Root mean squared error: 0.5709540264461411
Relative absolute error: 66.63647342995166
Root relative squared error: 114.19080528922822
Weighted TruePositiveRate: 0.6692708333333334
Weighted MatthewsCorrelation: 0.20226208594345021
Weighted FMeasure: 0.63852102654852
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5927495888416989
Mean absolute error: 0.3331823671497583
Coverage of cases: 66.92708333333333
Instances selection time: 21.0
Test time: 27.0
Accumulative iteration time: 143.0
Weighted Recall: 0.6692708333333334
Weighted FalsePositiveRate: 0.5027633706467661
Kappa statistic: 0.1861691475869436
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6594546198548444
Weighted AreaUnderROC: 0.5843283582089552
Root mean squared error: 0.5624037420532227
Relative absolute error: 64.517405063291
Root relative squared error: 112.48074841064455
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21991753837467956
Weighted FMeasure: 0.6401354208618607
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5949312658924703
Mean absolute error: 0.32258702531645495
Coverage of cases: 67.96875
Instances selection time: 19.0
Test time: 31.0
Accumulative iteration time: 162.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5110307835820895
Kappa statistic: 0.1930567894485067
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6600969405547508
Weighted AreaUnderROC: 0.5808656716417909
Root mean squared error: 0.5628010703770678
Relative absolute error: 64.46629213483145
Root relative squared error: 112.56021407541357
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21682297199846345
Weighted FMeasure: 0.6364945625051041
Iteration time: 18.0
Weighted AreaUnderPRC: 0.5930508676468862
Mean absolute error: 0.32233146067415724
Coverage of cases: 67.96875
Instances selection time: 18.0
Test time: 37.0
Accumulative iteration time: 180.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5179561567164179
Kappa statistic: 0.18660880347179168
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6611103787835604
Weighted AreaUnderROC: 0.5774029850746268
Root mean squared error: 0.5631185734845594
Relative absolute error: 64.42550505050507
Root relative squared error: 112.62371469691188
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.21392334324402784
Weighted FMeasure: 0.6326431585777795
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5911937601202225
Mean absolute error: 0.3221275252525253
Coverage of cases: 67.96875
Instances selection time: 15.0
Test time: 33.0
Accumulative iteration time: 195.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.5248815298507462
Kappa statistic: 0.18005694049024384
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.510416666666668
Correctly Classified Instances: 68.48958333333333
Weighted Precision: 0.6728660926615738
Weighted AreaUnderROC: 0.5779402985074626
Root mean squared error: 0.55877933457862
Relative absolute error: 63.36009174311924
Root relative squared error: 111.75586691572398
Weighted TruePositiveRate: 0.6848958333333334
Weighted MatthewsCorrelation: 0.22669951164110513
Weighted FMeasure: 0.6325173691178732
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5927168402599662
Mean absolute error: 0.3168004587155962
Coverage of cases: 68.48958333333333
Instances selection time: 13.0
Test time: 17.0
Accumulative iteration time: 208.0
Weighted Recall: 0.6848958333333334
Weighted FalsePositiveRate: 0.529015236318408
Kappa statistic: 0.18352428481057156
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8125
Correctly Classified Instances: 67.1875
Weighted Precision: 0.6533931016124125
Weighted AreaUnderROC: 0.5558208955223881
Root mean squared error: 0.5704255439400442
Relative absolute error: 65.91386554621856
Root relative squared error: 114.08510878800884
Weighted TruePositiveRate: 0.671875
Weighted MatthewsCorrelation: 0.17820478082359706
Weighted FMeasure: 0.6067314070547837
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5782054146384747
Mean absolute error: 0.3295693277310928
Coverage of cases: 67.1875
Instances selection time: 7.0
Test time: 26.0
Accumulative iteration time: 215.0
Weighted Recall: 0.671875
Weighted FalsePositiveRate: 0.5602332089552239
Kappa statistic: 0.1338966060432479
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.659703426409118
Weighted AreaUnderROC: 0.557820895522388
Root mean squared error: 0.5683418330162769
Relative absolute error: 65.37467700258397
Root relative squared error: 113.6683666032554
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.18679773823036663
Weighted FMeasure: 0.6085982421855868
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5798267193791188
Mean absolute error: 0.3268733850129198
Coverage of cases: 67.44791666666667
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 221.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.5588373756218905
Kappa statistic: 0.138982564396929
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6614713868003341
Weighted AreaUnderROC: 0.5650149253731344
Root mean squared error: 0.5662212151347854
Relative absolute error: 64.83812949640298
Root relative squared error: 113.24424302695708
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.19857646229902035
Weighted FMeasure: 0.6177761568386568
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5841267393744779
Mean absolute error: 0.3241906474820149
Coverage of cases: 67.70833333333333
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 226.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.5470534825870647
Kappa statistic: 0.1546655304644227
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6644209131823461
Weighted AreaUnderROC: 0.582865671641791
Root mean squared error: 0.5617715936159831
Relative absolute error: 63.78635346756164
Root relative squared error: 112.35431872319661
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.22381857098384392
Weighted FMeasure: 0.6385149183512414
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5946599271377758
Mean absolute error: 0.3189317673378082
Coverage of cases: 68.22916666666667
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 231.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.5165603233830846
Kappa statistic: 0.19160684704583097
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.072916666666664
Correctly Classified Instances: 66.92708333333333
Weighted Precision: 0.6458609762675297
Weighted AreaUnderROC: 0.5849850746268657
Root mean squared error: 0.5732878839326783
Relative absolute error: 66.35875262054515
Root relative squared error: 114.65757678653566
Weighted TruePositiveRate: 0.6692708333333334
Weighted MatthewsCorrelation: 0.20435496222717828
Weighted FMeasure: 0.6400424723175641
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5937295996409789
Mean absolute error: 0.33179376310272574
Coverage of cases: 66.92708333333333
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 239.0
Weighted Recall: 0.6692708333333334
Weighted FalsePositiveRate: 0.49930068407960193
Kappa statistic: 0.1893077997207262
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6608443586464686
Weighted AreaUnderROC: 0.6051044776119403
Root mean squared error: 0.5642924002307941
Relative absolute error: 64.27514792899419
Root relative squared error: 112.85848004615882
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.24133958422091628
Weighted FMeasure: 0.6581479090251265
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6066162152069056
Mean absolute error: 0.32137573964497096
Coverage of cases: 67.96875
Instances selection time: 7.0
Test time: 20.0
Accumulative iteration time: 246.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.4694785447761194
Kappa statistic: 0.22969534868549804
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.6787267791732078
Weighted AreaUnderROC: 0.6306865671641791
Root mean squared error: 0.5527954015258347
Relative absolute error: 61.673649906890205
Root relative squared error: 110.55908030516694
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.2857506947442993
Weighted FMeasure: 0.6791862347744039
Iteration time: 5.0
Weighted AreaUnderPRC: 0.623558152227237
Mean absolute error: 0.308368249534451
Coverage of cases: 69.27083333333333
Instances selection time: 5.0
Test time: 22.0
Accumulative iteration time: 251.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.43133519900497513
Kappa statistic: 0.278746975678085
Training time: 0.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7013526897414512
Weighted AreaUnderROC: 0.6602686567164179
Root mean squared error: 0.5362276265424193
Relative absolute error: 58.035714285714235
Root relative squared error: 107.24552530848386
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3388608533794078
Weighted FMeasure: 0.7029504703115813
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6445359545316989
Mean absolute error: 0.2901785714285712
Coverage of cases: 71.09375
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 252.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.3904001865671642
Kappa statistic: 0.33503900156006244
Training time: 0.0
		
Time end:Tue Oct 31 11.36.36 EET 2017