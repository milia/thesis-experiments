Tue Oct 31 11.40.19 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.40.19 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 90.625
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6944899980621981
Weighted AreaUnderROC: 0.6432388059701494
Root mean squared error: 0.521769658614954
Relative absolute error: 67.105736525379
Root relative squared error: 104.3539317229908
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.32666952153424084
Weighted FMeasure: 0.6966816209914767
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6345233675280922
Mean absolute error: 0.335528682626895
Coverage of cases: 89.32291666666667
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.3821327736318408
Kappa statistic: 0.3257191718072437
Training time: 3.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 68.48958333333333
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6970736326681449
Weighted AreaUnderROC: 0.7034029850746268
Root mean squared error: 0.5332159626843473
Relative absolute error: 67.15773809523805
Root relative squared error: 106.64319253686946
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.3288263015715474
Weighted FMeasure: 0.683031676900376
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6896271418239569
Mean absolute error: 0.33578869047619025
Coverage of cases: 82.29166666666667
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.33582960199004974
Kappa statistic: 0.32440408626560735
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 89.0625
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.6896565755208334
Weighted AreaUnderROC: 0.6680746268656717
Root mean squared error: 0.4932880702394051
Relative absolute error: 78.88152569037138
Root relative squared error: 98.65761404788101
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.3167936378073649
Weighted FMeasure: 0.691011613814883
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6565379244873392
Mean absolute error: 0.3944076284518569
Coverage of cases: 92.96875
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.3793949004975124
Kappa statistic: 0.3166023166023167
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 69.27083333333333
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7114470872694557
Weighted AreaUnderROC: 0.716134328358209
Root mean squared error: 0.5139253497272104
Relative absolute error: 68.10487531185977
Root relative squared error: 102.78506994544207
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.36224036169686347
Weighted FMeasure: 0.7024232914923291
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6988390297452131
Mean absolute error: 0.34052437655929885
Coverage of cases: 83.33333333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.32466293532338314
Kappa statistic: 0.3595583160800551
Training time: 3.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 89.19270833333333
Incorrectly Classified Instances: 37.5
Correctly Classified Instances: 62.5
Weighted Precision: 0.7670881013904269
Weighted AreaUnderROC: 0.7574776119402985
Root mean squared error: 0.49858716996757074
Relative absolute error: 73.5510964787463
Root relative squared error: 99.71743399351415
Weighted TruePositiveRate: 0.625
Weighted MatthewsCorrelation: 0.3952694605964689
Weighted FMeasure: 0.6224761181068171
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7264060478136866
Mean absolute error: 0.3677554823937315
Coverage of cases: 93.75
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.625
Weighted FalsePositiveRate: 0.2356268656716418
Kappa statistic: 0.3205544087289885
Training time: 3.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 36.71875
Correctly Classified Instances: 63.28125
Weighted Precision: 0.766025725643424
Weighted AreaUnderROC: 0.7746567164179105
Root mean squared error: 0.5142696076559659
Relative absolute error: 74.98432778775071
Root relative squared error: 102.85392153119317
Weighted TruePositiveRate: 0.6328125
Weighted MatthewsCorrelation: 0.4000477549208366
Weighted FMeasure: 0.6319234623288369
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7531875497585224
Mean absolute error: 0.3749216389387536
Coverage of cases: 90.625
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.6328125
Weighted FalsePositiveRate: 0.23490205223880598
Kappa statistic: 0.32993416167516465
Training time: 3.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 78.125
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.721173528786769
Weighted AreaUnderROC: 0.7044179104477611
Root mean squared error: 0.5276829924309678
Relative absolute error: 73.97144089960878
Root relative squared error: 105.53659848619357
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.36771554940967865
Weighted FMeasure: 0.6820705218886739
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6892506855172303
Mean absolute error: 0.36985720449804393
Coverage of cases: 85.9375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 41.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.2887478233830845
Kappa statistic: 0.34998104111369904
Training time: 4.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 100.0
Incorrectly Classified Instances: 38.020833333333336
Correctly Classified Instances: 61.979166666666664
Weighted Precision: 0.7078307748538012
Weighted AreaUnderROC: 0.671910447761194
Root mean squared error: 0.5169257669886281
Relative absolute error: 85.8533579951934
Root relative squared error: 103.38515339772563
Weighted TruePositiveRate: 0.6197916666666666
Weighted MatthewsCorrelation: 0.31635194735522626
Weighted FMeasure: 0.6251414668553648
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6575073349208268
Mean absolute error: 0.429266789975967
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.6197916666666666
Weighted FalsePositiveRate: 0.29382151741293533
Kappa statistic: 0.28034504004929134
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 79.42708333333333
Incorrectly Classified Instances: 32.8125
Correctly Classified Instances: 67.1875
Weighted Precision: 0.7554516321877434
Weighted AreaUnderROC: 0.713402985074627
Root mean squared error: 0.492802578571305
Relative absolute error: 68.93236245404891
Root relative squared error: 98.560515714261
Weighted TruePositiveRate: 0.671875
Weighted MatthewsCorrelation: 0.4152100486008088
Weighted FMeasure: 0.6773872995527436
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6975245978642826
Mean absolute error: 0.34466181227024456
Coverage of cases: 88.28125
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 54.0
Weighted Recall: 0.671875
Weighted FalsePositiveRate: 0.24166604477611942
Kappa statistic: 0.37332918868511034
Training time: 9.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 91.92708333333333
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.6963661807411808
Weighted AreaUnderROC: 0.7149104477611941
Root mean squared error: 0.4762866312734233
Relative absolute error: 76.8307694086341
Root relative squared error: 95.25732625468466
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.3286105219694802
Weighted FMeasure: 0.6983870364589432
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7011138098267002
Mean absolute error: 0.38415384704317046
Coverage of cases: 95.57291666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 59.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.3931918532338308
Kappa statistic: 0.3254989740720014
Training time: 5.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 97.52604166666667
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7652319470953849
Weighted AreaUnderROC: 0.8034626865671641
Root mean squared error: 0.4263331547583604
Relative absolute error: 62.327964497251884
Root relative squared error: 85.26663095167208
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4831189614027598
Weighted FMeasure: 0.7639941077441077
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7740508851034118
Mean absolute error: 0.31163982248625943
Coverage of cases: 97.91666666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 64.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.27591635572139306
Kappa statistic: 0.48292394199467303
Training time: 5.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 97.52604166666667
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.76481141216908
Weighted AreaUnderROC: 0.8048208955223881
Root mean squared error: 0.4138153660204772
Relative absolute error: 62.34335542929307
Root relative squared error: 82.76307320409543
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.47220943202122606
Weighted FMeasure: 0.7450432193079252
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7799396936628574
Mean absolute error: 0.31171677714646534
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 69.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.24692661691542286
Kappa statistic: 0.4622451265964599
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 98.30729166666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.729955038035281
Weighted AreaUnderROC: 0.7839253731343283
Root mean squared error: 0.4337531313438677
Relative absolute error: 64.28728310017317
Root relative squared error: 86.75062626877354
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.3861022090077879
Weighted FMeasure: 0.7192921854596568
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7579518469035168
Mean absolute error: 0.3214364155008659
Coverage of cases: 98.17708333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 75.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.40068065920398005
Kappa statistic: 0.3674734164002869
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.74609375
Weighted AreaUnderROC: 0.788134328358209
Root mean squared error: 0.42167769998585414
Relative absolute error: 64.81313759981808
Root relative squared error: 84.33553999717083
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.4336585079213105
Weighted FMeasure: 0.7294269130924974
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7652711790523185
Mean absolute error: 0.32406568799909036
Coverage of cases: 99.47916666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 80.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.2726150497512438
Kappa statistic: 0.4262516914749662
Training time: 5.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 82.8125
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6849970657276995
Weighted AreaUnderROC: 0.6758507462686567
Root mean squared error: 0.49186550481249214
Relative absolute error: 69.39927186186064
Root relative squared error: 98.37310096249843
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.300083262217657
Weighted FMeasure: 0.6855873267390121
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6656169531187094
Mean absolute error: 0.3469963593093032
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.42161815920398005
Kappa statistic: 0.29358030956609993
Training time: 7.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 86.58854166666667
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7613673941798941
Weighted AreaUnderROC: 0.7773582089552239
Root mean squared error: 0.4314607107673637
Relative absolute error: 61.40858434872673
Root relative squared error: 86.29214215347274
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.46669915614388663
Weighted FMeasure: 0.7447423708419786
Iteration time: 6.0
Weighted AreaUnderPRC: 0.747839986643303
Mean absolute error: 0.3070429217436336
Coverage of cases: 96.35416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 94.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.25385199004975123
Kappa statistic: 0.4587280108254398
Training time: 6.0
		
Time end:Tue Oct 31 11.40.20 EET 2017