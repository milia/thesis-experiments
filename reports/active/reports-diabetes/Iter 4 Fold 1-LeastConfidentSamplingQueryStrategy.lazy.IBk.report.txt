Tue Oct 31 11.38.13 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.13 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.552083333333336
Correctly Classified Instances: 67.44791666666667
Weighted Precision: 0.6607980244068953
Weighted AreaUnderROC: 0.6166865671641791
Root mean squared error: 0.5633280668031083
Relative absolute error: 65.99893162393163
Root relative squared error: 112.66561336062166
Weighted TruePositiveRate: 0.6744791666666666
Weighted MatthewsCorrelation: 0.2495608817460583
Weighted FMeasure: 0.6636525885001832
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6127956897574778
Mean absolute error: 0.3299946581196581
Coverage of cases: 67.44791666666667
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6744791666666666
Weighted FalsePositiveRate: 0.4411060323383085
Kappa statistic: 0.24570997548557405
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6729428218164211
Weighted AreaUnderROC: 0.6113731343283582
Root mean squared error: 0.5510676472570262
Relative absolute error: 62.755102040816425
Root relative squared error: 110.21352945140524
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.26264514415053775
Weighted FMeasure: 0.6654493401852557
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6119039768031561
Mean absolute error: 0.3137755102040821
Coverage of cases: 69.01041666666667
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.4673578980099502
Kappa statistic: 0.24618937644341785
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6847543834572135
Weighted AreaUnderROC: 0.6087164179104477
Root mean squared error: 0.5450092616452813
Relative absolute error: 61.087570621469034
Root relative squared error: 109.00185232905626
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.2747021166866244
Weighted FMeasure: 0.6646449530516433
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6121687281124929
Mean absolute error: 0.30543785310734517
Coverage of cases: 69.79166666666667
Instances selection time: 6.0
Test time: 9.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.4804838308457711
Kappa statistic: 0.24644742184328047
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.6799381655031381
Weighted AreaUnderROC: 0.5960597014925373
Root mean squared error: 0.5503550437927358
Relative absolute error: 62.01690821256036
Root relative squared error: 110.07100875854717
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.25572361496856
Weighted FMeasure: 0.6521629050925927
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6041308458317174
Mean absolute error: 0.3100845410628018
Coverage of cases: 69.27083333333333
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.5005889303482587
Kappa statistic: 0.22122920390485365
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6867634170324347
Weighted AreaUnderROC: 0.5945970149253731
Root mean squared error: 0.5485168674576879
Relative absolute error: 61.43196202531633
Root relative squared error: 109.70337349153758
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.26144353055932285
Weighted FMeasure: 0.6505630045008146
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6041382928398257
Mean absolute error: 0.30715981012658167
Coverage of cases: 69.53125
Instances selection time: 7.0
Test time: 11.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.5061184701492537
Kappa statistic: 0.2200541629053539
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.68310984321903
Weighted AreaUnderROC: 0.590865671641791
Root mean squared error: 0.5512445128818676
Relative absolute error: 61.89138576779024
Root relative squared error: 110.24890257637352
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.25315209544569783
Weighted FMeasure: 0.6466196929386986
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6014735273224309
Mean absolute error: 0.3094569288389512
Coverage of cases: 69.27083333333333
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 38.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.5109769900497513
Kappa statistic: 0.21180072362927915
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6822226124334448
Weighted AreaUnderROC: 0.5836716417910447
Root mean squared error: 0.5538872448358396
Relative absolute error: 62.363215488215495
Root relative squared error: 110.77744896716793
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.24336987021448397
Weighted FMeasure: 0.6385914621903049
Iteration time: 6.0
Weighted AreaUnderPRC: 0.597101290825027
Mean absolute error: 0.3118160774410775
Coverage of cases: 69.01041666666667
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 44.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.5227608830845771
Kappa statistic: 0.1970197511773388
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6985673842707815
Weighted AreaUnderROC: 0.5842089552238806
Root mean squared error: 0.5494663263883366
Relative absolute error: 61.295871559633
Root relative squared error: 109.89326527766733
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.2599391472809484
Weighted FMeasure: 0.6382447000722719
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5992400449604384
Mean absolute error: 0.306479357798165
Coverage of cases: 69.53125
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 50.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.5268945895522388
Kappa statistic: 0.20074005550416277
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.7015456614641398
Weighted AreaUnderROC: 0.5824776119402985
Root mean squared error: 0.5496769550934512
Relative absolute error: 61.265756302521076
Root relative squared error: 109.93539101869023
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.26028349281227875
Weighted FMeasure: 0.6359793194734541
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5984940669564057
Mean absolute error: 0.3063287815126054
Coverage of cases: 69.53125
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 57.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.530357276119403
Kappa statistic: 0.19742765273311896
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.6976520510090255
Weighted AreaUnderROC: 0.5787462686567164
Root mean squared error: 0.5521997680723378
Relative absolute error: 61.75710594315246
Root relative squared error: 110.43995361446757
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.2513926266377802
Weighted FMeasure: 0.631700841527496
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5957651195365581
Mean absolute error: 0.3087855297157623
Coverage of cases: 69.27083333333333
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.5352157960199004
Kappa statistic: 0.1888872977230418
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.729166666666668
Correctly Classified Instances: 69.27083333333333
Weighted Precision: 0.7010197158365262
Weighted AreaUnderROC: 0.5770149253731344
Root mean squared error: 0.5523530343364245
Relative absolute error: 61.735611510791486
Root relative squared error: 110.47060686728489
Weighted TruePositiveRate: 0.6927083333333334
Weighted MatthewsCorrelation: 0.25187579006634886
Weighted FMeasure: 0.6293154469145518
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5950336870410281
Mean absolute error: 0.30867805755395744
Coverage of cases: 69.27083333333333
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 68.0
Weighted Recall: 0.6927083333333334
Weighted FalsePositiveRate: 0.5386784825870647
Kappa statistic: 0.18550474547023313
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6903409090909091
Weighted AreaUnderROC: 0.5983283582089552
Root mean squared error: 0.5477838891074226
Relative absolute error: 60.68232662192401
Root relative squared error: 109.55677782148452
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.26963566553825513
Weighted FMeasure: 0.65447695035461
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6068221275252524
Mean absolute error: 0.30341163310962005
Coverage of cases: 69.79166666666667
Instances selection time: 4.0
Test time: 19.0
Accumulative iteration time: 72.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.5012599502487561
Kappa statistic: 0.22827442827442826
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6870919674061408
Weighted AreaUnderROC: 0.6159104477611941
Root mean squared error: 0.5455323443614607
Relative absolute error: 60.14806079664572
Root relative squared error: 109.10646887229214
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.28462539056056757
Weighted FMeasure: 0.671259242010715
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6166984662128622
Mean absolute error: 0.3007403039832286
Coverage of cases: 70.05208333333333
Instances selection time: 4.0
Test time: 31.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.4686999378109453
Kappa statistic: 0.26020237217717634
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.6892179821867321
Weighted AreaUnderROC: 0.6334925373134328
Root mean squared error: 0.5432560136714394
Relative absolute error: 59.615384615384684
Root relative squared error: 108.65120273428788
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.3027765216486935
Weighted FMeasure: 0.6848734036234037
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6271095536232255
Mean absolute error: 0.2980769230769234
Coverage of cases: 70.3125
Instances selection time: 3.0
Test time: 21.0
Accumulative iteration time: 79.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.43613992537313423
Kappa statistic: 0.2900882200311364
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.947916666666668
Correctly Classified Instances: 70.05208333333333
Weighted Precision: 0.6881066032840034
Weighted AreaUnderROC: 0.6418805970149254
Root mean squared error: 0.5457232826254578
Relative absolute error: 60.11987895716955
Root relative squared error: 109.14465652509155
Weighted TruePositiveRate: 0.7005208333333334
Weighted MatthewsCorrelation: 0.30720020543507587
Weighted FMeasure: 0.6887645037456922
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6315338871822668
Mean absolute error: 0.30059939478584774
Coverage of cases: 70.05208333333333
Instances selection time: 1.0
Test time: 22.0
Accumulative iteration time: 81.0
Weighted Recall: 0.7005208333333334
Weighted FalsePositiveRate: 0.41675963930348264
Kappa statistic: 0.30095611979991144
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6898347701149424
Weighted AreaUnderROC: 0.652
Root mean squared error: 0.548171512852127
Relative absolute error: 60.62610229276889
Root relative squared error: 109.63430257042539
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.31557282344421184
Weighted FMeasure: 0.6922902187902188
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6373164735991379
Mean absolute error: 0.30313051146384445
Coverage of cases: 69.79166666666667
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 82.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.3939166666666667
Kappa statistic: 0.313778654177964
Training time: 0.0
		
Time end:Tue Oct 31 11.38.14 EET 2017