Tue Oct 31 11.37.59 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.59 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.760416666666668
Correctly Classified Instances: 72.23958333333333
Weighted Precision: 0.7379487258347857
Weighted AreaUnderROC: 0.6463880597014925
Root mean squared error: 0.5255845776519031
Relative absolute error: 55.52083333333333
Root relative squared error: 105.11691553038062
Weighted TruePositiveRate: 0.7223958333333333
Weighted MatthewsCorrelation: 0.3588373346350616
Weighted FMeasure: 0.6905890690298909
Iteration time: 12.9
Weighted AreaUnderPRC: 0.6432450302619955
Mean absolute error: 0.27760416666666665
Coverage of cases: 72.23958333333333
Instances selection time: 2.5
Test time: 1.5
Accumulative iteration time: 12.9
Weighted Recall: 0.7223958333333333
Weighted FalsePositiveRate: 0.4296197139303482
Kappa statistic: 0.3181794725145709
Training time: 10.4
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.942708333333332
Correctly Classified Instances: 72.05729166666666
Weighted Precision: 0.7380730533951111
Weighted AreaUnderROC: 0.6453343283582089
Root mean squared error: 0.5272173492303007
Relative absolute error: 55.885416666666664
Root relative squared error: 105.44346984606015
Weighted TruePositiveRate: 0.7205729166666666
Weighted MatthewsCorrelation: 0.34341897856964715
Weighted FMeasure: 0.6831736205999555
Iteration time: 8.8
Weighted AreaUnderPRC: 0.6425285259147263
Mean absolute error: 0.2794270833333333
Coverage of cases: 72.05729166666666
Instances selection time: 1.9
Test time: 1.7
Accumulative iteration time: 21.7
Weighted Recall: 0.7205729166666666
Weighted FalsePositiveRate: 0.42990425995024867
Kappa statistic: 0.3096947014958026
Training time: 6.9
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.052083333333332
Correctly Classified Instances: 69.94791666666666
Weighted Precision: 0.6779375142277646
Weighted AreaUnderROC: 0.6235940298507463
Root mean squared error: 0.5472410647429131
Relative absolute error: 60.10416666666665
Root relative squared error: 109.44821294858261
Weighted TruePositiveRate: 0.6994791666666667
Weighted MatthewsCorrelation: 0.28511920839332516
Weighted FMeasure: 0.645172835081252
Iteration time: 9.2
Weighted AreaUnderPRC: 0.6269283406302072
Mean absolute error: 0.30052083333333335
Coverage of cases: 69.94791666666666
Instances selection time: 2.0
Test time: 2.1
Accumulative iteration time: 30.9
Weighted Recall: 0.6994791666666667
Weighted FalsePositiveRate: 0.4522911069651741
Kappa statistic: 0.24820843853432004
Training time: 7.2
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.34895833333333
Correctly Classified Instances: 65.65104166666666
Weighted Precision: 0.6602799716288035
Weighted AreaUnderROC: 0.5899014925373134
Root mean squared error: 0.5858381830422645
Relative absolute error: 68.69791666666666
Root relative squared error: 117.16763660845291
Weighted TruePositiveRate: 0.6565104166666667
Weighted MatthewsCorrelation: 0.2032569749390086
Weighted FMeasure: 0.5850595482916886
Iteration time: 10.3
Weighted AreaUnderPRC: 0.6025281091777017
Mean absolute error: 0.34348958333333335
Coverage of cases: 65.65104166666666
Instances selection time: 1.8
Test time: 1.5
Accumulative iteration time: 41.2
Weighted Recall: 0.6565104166666667
Weighted FalsePositiveRate: 0.4767074315920398
Kappa statistic: 0.1608583467160151
Training time: 8.5
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 38.567708333333336
Correctly Classified Instances: 61.43229166666667
Weighted Precision: 0.590701875938866
Weighted AreaUnderROC: 0.5632149253731342
Root mean squared error: 0.6180868849147573
Relative absolute error: 77.13541666666666
Root relative squared error: 123.61737698295141
Weighted TruePositiveRate: 0.6143229166666667
Weighted MatthewsCorrelation: 0.15091104787582235
Weighted FMeasure: 0.5224097638704945
Iteration time: 9.7
Weighted AreaUnderPRC: 0.5857479323294543
Mean absolute error: 0.38567708333333334
Coverage of cases: 61.43229166666667
Instances selection time: 2.1
Test time: 1.3
Accumulative iteration time: 50.9
Weighted Recall: 0.6143229166666667
Weighted FalsePositiveRate: 0.48789306592039805
Kappa statistic: 0.10773593517263365
Training time: 7.6
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 43.22916666666667
Correctly Classified Instances: 56.770833333333336
Weighted Precision: 0.5239166583854697
Weighted AreaUnderROC: 0.5300119402985074
Root mean squared error: 0.6518147566732607
Relative absolute error: 86.45833333333333
Root relative squared error: 130.3629513346521
Weighted TruePositiveRate: 0.5677083333333333
Weighted MatthewsCorrelation: 0.0879623736746997
Weighted FMeasure: 0.4502012480630292
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5643685581706259
Mean absolute error: 0.4322916666666667
Coverage of cases: 56.770833333333336
Instances selection time: 1.6
Test time: 1.9
Accumulative iteration time: 61.9
Weighted Recall: 0.5677083333333333
Weighted FalsePositiveRate: 0.5076844527363183
Kappa statistic: 0.04981630193233379
Training time: 9.4
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 40.46875
Correctly Classified Instances: 59.53125
Weighted Precision: 0.5605772948506014
Weighted AreaUnderROC: 0.545844776119403
Root mean squared error: 0.631987988324331
Relative absolute error: 80.93749999999999
Root relative squared error: 126.39759766486618
Weighted TruePositiveRate: 0.5953125
Weighted MatthewsCorrelation: 0.10830262752933055
Weighted FMeasure: 0.48848461809078214
Iteration time: 9.4
Weighted AreaUnderPRC: 0.5744870974081142
Mean absolute error: 0.4046875
Coverage of cases: 59.53125
Instances selection time: 1.1
Test time: 1.3
Accumulative iteration time: 71.3
Weighted Recall: 0.5953125
Weighted FalsePositiveRate: 0.503622947761194
Kappa statistic: 0.07425415658365073
Training time: 8.3
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.671875
Correctly Classified Instances: 66.328125
Weighted Precision: 0.5595684292534848
Weighted AreaUnderROC: 0.5893880597014926
Root mean squared error: 0.5799787579134945
Relative absolute error: 67.34374999999999
Root relative squared error: 115.9957515826989
Weighted TruePositiveRate: 0.66328125
Weighted MatthewsCorrelation: 0.17260827812004548
Weighted FMeasure: 0.582863271463304
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6034191693578792
Mean absolute error: 0.33671875
Coverage of cases: 66.328125
Instances selection time: 0.7
Test time: 1.5
Accumulative iteration time: 82.3
Weighted Recall: 0.66328125
Weighted FalsePositiveRate: 0.48450513059701494
Kappa statistic: 0.15571583030622987
Training time: 10.3
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.21354166666667
Correctly Classified Instances: 67.78645833333334
Weighted Precision: 0.556772176508682
Weighted AreaUnderROC: 0.5933164179104476
Root mean squared error: 0.5666187000817567
Relative absolute error: 64.42708333333333
Root relative squared error: 113.32374001635132
Weighted TruePositiveRate: 0.6778645833333334
Weighted MatthewsCorrelation: 0.17838528386107136
Weighted FMeasure: 0.5978141335878144
Iteration time: 11.2
Weighted AreaUnderPRC: 0.6063207797219113
Mean absolute error: 0.3221354166666667
Coverage of cases: 67.78645833333334
Instances selection time: 0.7
Test time: 1.5
Accumulative iteration time: 93.5
Weighted Recall: 0.6778645833333334
Weighted FalsePositiveRate: 0.49123174751243787
Kappa statistic: 0.1717407619913009
Training time: 10.5
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.458333333333332
Correctly Classified Instances: 68.54166666666666
Weighted Precision: 0.5571009342404635
Weighted AreaUnderROC: 0.595134328358209
Root mean squared error: 0.5593394258240793
Relative absolute error: 62.91666666666665
Root relative squared error: 111.86788516481582
Weighted TruePositiveRate: 0.6854166666666668
Weighted MatthewsCorrelation: 0.18319597967896
Weighted FMeasure: 0.6048245278407962
Iteration time: 10.7
Weighted AreaUnderPRC: 0.608195388484524
Mean absolute error: 0.3145833333333334
Coverage of cases: 68.54166666666666
Instances selection time: 0.7
Test time: 2.0
Accumulative iteration time: 104.2
Weighted Recall: 0.6854166666666668
Weighted FalsePositiveRate: 0.49514800995024866
Kappa statistic: 0.180388521428645
Training time: 10.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333336
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.5565180442535601
Weighted AreaUnderROC: 0.5931940298507463
Root mean squared error: 0.5545266723019772
Relative absolute error: 61.979166666666664
Root relative squared error: 110.90533446039547
Weighted TruePositiveRate: 0.6901041666666667
Weighted MatthewsCorrelation: 0.1840434310608669
Weighted FMeasure: 0.6083579593240943
Iteration time: 10.8
Weighted AreaUnderPRC: 0.6075504775072471
Mean absolute error: 0.30989583333333337
Coverage of cases: 69.01041666666667
Instances selection time: 1.0
Test time: 1.5
Accumulative iteration time: 115.0
Weighted Recall: 0.6901041666666667
Weighted FalsePositiveRate: 0.5037161069651741
Kappa statistic: 0.18332401514134558
Training time: 9.8
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333336
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.5546124081059993
Weighted AreaUnderROC: 0.589731343283582
Root mean squared error: 0.5544136294203085
Relative absolute error: 61.979166666666664
Root relative squared error: 110.88272588406168
Weighted TruePositiveRate: 0.6901041666666667
Weighted MatthewsCorrelation: 0.18009224103366378
Weighted FMeasure: 0.6076124415665797
Iteration time: 11.7
Weighted AreaUnderPRC: 0.605280347713918
Mean absolute error: 0.30989583333333337
Coverage of cases: 69.01041666666667
Instances selection time: 1.2
Test time: 1.5
Accumulative iteration time: 126.7
Weighted Recall: 0.6901041666666667
Weighted FalsePositiveRate: 0.5106414800995025
Kappa statistic: 0.17961094261949978
Training time: 10.5
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.338541666666668
Correctly Classified Instances: 69.66145833333334
Weighted Precision: 0.635748371731455
Weighted AreaUnderROC: 0.5943850746268657
Root mean squared error: 0.548596134197444
Relative absolute error: 60.677083333333336
Root relative squared error: 109.71922683948881
Weighted TruePositiveRate: 0.6966145833333333
Weighted MatthewsCorrelation: 0.2200407606087733
Weighted FMeasure: 0.6201330194178464
Iteration time: 11.5
Weighted AreaUnderPRC: 0.6086994641568285
Mean absolute error: 0.30338541666666663
Coverage of cases: 69.66145833333334
Instances selection time: 0.7
Test time: 1.5
Accumulative iteration time: 138.2
Weighted Recall: 0.6966145833333333
Weighted FalsePositiveRate: 0.507844434079602
Kappa statistic: 0.19670532915911482
Training time: 10.8
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.651041666666664
Correctly Classified Instances: 74.34895833333334
Weighted Precision: 0.7537851225146249
Weighted AreaUnderROC: 0.6676089552238806
Root mean squared error: 0.5056531489027389
Relative absolute error: 51.30208333333333
Root relative squared error: 101.13062978054778
Weighted TruePositiveRate: 0.7434895833333333
Weighted MatthewsCorrelation: 0.40649074954915754
Weighted FMeasure: 0.7155253321525237
Iteration time: 11.9
Weighted AreaUnderPRC: 0.6605588224671429
Mean absolute error: 0.25651041666666663
Coverage of cases: 74.34895833333334
Instances selection time: 1.4
Test time: 1.2
Accumulative iteration time: 150.1
Weighted Recall: 0.7434895833333333
Weighted FalsePositiveRate: 0.4082716728855721
Kappa statistic: 0.36748841338584837
Training time: 10.5
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.56770833333333
Correctly Classified Instances: 76.43229166666669
Weighted Precision: 0.7641188829062552
Weighted AreaUnderROC: 0.7017880597014925
Root mean squared error: 0.48502242448013944
Relative absolute error: 47.135416666666664
Root relative squared error: 97.00448489602786
Weighted TruePositiveRate: 0.7643229166666665
Weighted MatthewsCorrelation: 0.4583422677874921
Weighted FMeasure: 0.7496399794425541
Iteration time: 12.1
Weighted AreaUnderPRC: 0.6855637119793709
Mean absolute error: 0.23567708333333331
Coverage of cases: 76.43229166666669
Instances selection time: 0.8
Test time: 1.4
Accumulative iteration time: 162.2
Weighted Recall: 0.7643229166666665
Weighted FalsePositiveRate: 0.36074679726368164
Kappa statistic: 0.43725710027412185
Training time: 11.3
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.994791666666664
Correctly Classified Instances: 77.00520833333334
Weighted Precision: 0.7675064154123776
Weighted AreaUnderROC: 0.713113432835821
Root mean squared error: 0.4790327950162025
Relative absolute error: 45.98958333333333
Root relative squared error: 95.8065590032405
Weighted TruePositiveRate: 0.7700520833333333
Weighted MatthewsCorrelation: 0.4726167056210073
Weighted FMeasure: 0.7587264088624605
Iteration time: 12.9
Weighted AreaUnderPRC: 0.6937604221927021
Mean absolute error: 0.2299479166666667
Coverage of cases: 77.00520833333334
Instances selection time: 1.2
Test time: 1.2
Accumulative iteration time: 175.1
Weighted Recall: 0.7700520833333333
Weighted FalsePositiveRate: 0.3438252176616915
Kappa statistic: 0.45750708018580044
Training time: 11.7
		
Time end:Tue Oct 31 11.38.08 EET 2017