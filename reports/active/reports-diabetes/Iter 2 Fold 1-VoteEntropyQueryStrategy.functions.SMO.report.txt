Tue Oct 31 11.40.00 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.40.00 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7977240073145246
Weighted AreaUnderROC: 0.7329253731343285
Root mean squared error: 0.45357377202244253
Relative absolute error: 41.14583333333333
Root relative squared error: 90.7147544044885
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5304339929146479
Weighted FMeasure: 0.7812331109493439
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7153833509529562
Mean absolute error: 0.20572916666666666
Coverage of cases: 79.42708333333333
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.3284200870646767
Kappa statistic: 0.5071163969584714
Training time: 6.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.7958420219338894
Weighted AreaUnderROC: 0.6653731343283582
Root mean squared error: 0.48947250518628044
Relative absolute error: 47.91666666666667
Root relative squared error: 97.89450103725609
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4607301605680572
Weighted FMeasure: 0.724483150426782
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6689936076437158
Mean absolute error: 0.23958333333333334
Coverage of cases: 76.04166666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.42967039800995027
Kappa statistic: 0.38547175062621747
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7973341368227732
Weighted AreaUnderROC: 0.5783582089552238
Root mean squared error: 0.542467356928814
Relative absolute error: 58.854166666666664
Root relative squared error: 108.4934713857628
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.32852917266765946
Weighted FMeasure: 0.6255855435106737
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6057515630380509
Mean absolute error: 0.2942708333333333
Coverage of cases: 70.57291666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.549012748756219
Kappa statistic: 0.1948341126697839
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.7739202567449958
Weighted AreaUnderROC: 0.503731343283582
Root mean squared error: 0.5885186204927306
Relative absolute error: 69.27083333333334
Root relative squared error: 117.70372409854613
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.06979402073993957
Weighted FMeasure: 0.5194206746240712
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5484292628213846
Mean absolute error: 0.3463541666666667
Coverage of cases: 65.36458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.6461831467661692
Kappa statistic: 0.009695183432870725
Training time: 7.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 57.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 98.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 108.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 11.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7545774753664984
Weighted AreaUnderROC: 0.5594328358208955
Root mean squared error: 0.5566828839953077
Relative absolute error: 61.979166666666664
Root relative squared error: 111.33657679906155
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.26124959115614305
Weighted FMeasure: 0.602614046973803
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5881376152035518
Mean absolute error: 0.3098958333333333
Coverage of cases: 69.01041666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 129.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.5712384950248756
Kappa statistic: 0.14841595229221016
Training time: 10.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 21.614583333333332
Correctly Classified Instances: 78.38541666666667
Weighted Precision: 0.799010550255741
Weighted AreaUnderROC: 0.7076119402985075
Root mean squared error: 0.46491486675877913
Relative absolute error: 43.22916666666667
Root relative squared error: 92.98297335175583
Weighted TruePositiveRate: 0.7838541666666666
Weighted MatthewsCorrelation: 0.5098041698039525
Weighted FMeasure: 0.7627349311903422
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6990062104418143
Mean absolute error: 0.21614583333333334
Coverage of cases: 78.38541666666667
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7838541666666666
Weighted FalsePositiveRate: 0.36863028606965176
Kappa statistic: 0.46605910339744017
Training time: 11.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.270833333333332
Correctly Classified Instances: 80.72916666666667
Weighted Precision: 0.813983566151605
Weighted AreaUnderROC: 0.7463880597014926
Root mean squared error: 0.4389855730355308
Relative absolute error: 38.54166666666667
Root relative squared error: 87.79711460710617
Weighted TruePositiveRate: 0.8072916666666666
Weighted MatthewsCorrelation: 0.5633972191356217
Weighted FMeasure: 0.7947087204158372
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7296919100528069
Mean absolute error: 0.19270833333333334
Coverage of cases: 80.72916666666667
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 151.0
Weighted Recall: 0.8072916666666666
Weighted FalsePositiveRate: 0.3145155472636816
Kappa statistic: 0.5374397708034899
Training time: 11.0
		
Time end:Tue Oct 31 11.40.01 EET 2017