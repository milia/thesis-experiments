Tue Oct 31 11.38.22 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.22 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 100.0
Incorrectly Classified Instances: 42.1875
Correctly Classified Instances: 57.8125
Weighted Precision: 0.6700943732193733
Weighted AreaUnderROC: 0.6223283582089553
Root mean squared error: 0.48657812493901986
Relative absolute error: 84.7797192353644
Root relative squared error: 97.31562498780397
Weighted TruePositiveRate: 0.578125
Weighted MatthewsCorrelation: 0.23901545729325802
Weighted FMeasure: 0.5827105978260869
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6141886462784901
Mean absolute error: 0.42389859617682196
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.578125
Weighted FalsePositiveRate: 0.3334682835820895
Kappa statistic: 0.20854961832061067
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 100.0
Incorrectly Classified Instances: 42.1875
Correctly Classified Instances: 57.8125
Weighted Precision: 0.6700943732193733
Weighted AreaUnderROC: 0.6223283582089553
Root mean squared error: 0.5203676471432352
Relative absolute error: 86.47487593052077
Root relative squared error: 104.07352942864705
Weighted TruePositiveRate: 0.578125
Weighted MatthewsCorrelation: 0.23901545729325802
Weighted FMeasure: 0.5827105978260869
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6141886462784901
Mean absolute error: 0.43237437965260384
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.578125
Weighted FalsePositiveRate: 0.3334682835820895
Kappa statistic: 0.20854961832061067
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 95.44270833333333
Incorrectly Classified Instances: 35.9375
Correctly Classified Instances: 64.0625
Weighted Precision: 0.6803807564896275
Weighted AreaUnderROC: 0.6343880597014926
Root mean squared error: 0.5425572091867166
Relative absolute error: 79.60837601714587
Root relative squared error: 108.51144183734331
Weighted TruePositiveRate: 0.640625
Weighted MatthewsCorrelation: 0.285270097645394
Weighted FMeasure: 0.6489676339285714
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6265979017751054
Mean absolute error: 0.3980418800857293
Coverage of cases: 96.09375
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.640625
Weighted FalsePositiveRate: 0.3415205223880597
Kappa statistic: 0.2744002628984554
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 83.07291666666667
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.7213816732170638
Weighted AreaUnderROC: 0.6876865671641791
Root mean squared error: 0.541237086697457
Relative absolute error: 71.58471663313196
Root relative squared error: 108.24741733949139
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.35913313067718367
Weighted FMeasure: 0.6662085986021734
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6715033350381375
Mean absolute error: 0.3579235831656598
Coverage of cases: 90.88541666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.28327207711442787
Kappa statistic: 0.3334393385275878
Training time: 3.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 52.473958333333336
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.6982499672209713
Weighted AreaUnderROC: 0.6848059701492537
Root mean squared error: 0.5802410598191066
Relative absolute error: 69.11458333333333
Root relative squared error: 116.04821196382132
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.3207053274916482
Weighted FMeasure: 0.6617360508190079
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6631075102437046
Mean absolute error: 0.3455729166666666
Coverage of cases: 68.48958333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.31722792288557217
Kappa statistic: 0.30620007607455313
Training time: 3.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 71.22395833333333
Incorrectly Classified Instances: 38.28125
Correctly Classified Instances: 61.71875
Weighted Precision: 0.6954337333092746
Weighted AreaUnderROC: 0.6755820895522388
Root mean squared error: 0.5462364330377038
Relative absolute error: 74.62774161443059
Root relative squared error: 109.24728660754074
Weighted TruePositiveRate: 0.6171875
Weighted MatthewsCorrelation: 0.29713342716140706
Weighted FMeasure: 0.6237750656481261
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6626930372072651
Mean absolute error: 0.37313870807215294
Coverage of cases: 83.59375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6171875
Weighted FalsePositiveRate: 0.3090680970149254
Kappa statistic: 0.2677839464535879
Training time: 2.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 72.78645833333333
Incorrectly Classified Instances: 38.28125
Correctly Classified Instances: 61.71875
Weighted Precision: 0.6980810273514749
Weighted AreaUnderROC: 0.694044776119403
Root mean squared error: 0.5342208165364248
Relative absolute error: 73.22057787174074
Root relative squared error: 106.84416330728497
Weighted TruePositiveRate: 0.6171875
Weighted MatthewsCorrelation: 0.3009743737425669
Weighted FMeasure: 0.6234569557750108
Iteration time: 4.0
Weighted AreaUnderPRC: 0.673653700969476
Mean absolute error: 0.3661028893587037
Coverage of cases: 86.45833333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.6171875
Weighted FalsePositiveRate: 0.3056054104477612
Kappa statistic: 0.2699808597589365
Training time: 3.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 75.78125
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7059857617051013
Weighted AreaUnderROC: 0.7365522388059701
Root mean squared error: 0.49142901597187283
Relative absolute error: 67.66664287444635
Root relative squared error: 98.28580319437457
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.34957866850075314
Weighted FMeasure: 0.6951698251601699
Iteration time: 4.0
Weighted AreaUnderPRC: 0.7159428030381272
Mean absolute error: 0.3383332143722318
Coverage of cases: 90.10416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.3288504353233831
Kappa statistic: 0.34626609442060075
Training time: 4.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 70.3125
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.7284345432115532
Weighted AreaUnderROC: 0.711910447761194
Root mean squared error: 0.5119607040325702
Relative absolute error: 66.03919948165577
Root relative squared error: 102.39214080651405
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.387546054021698
Weighted FMeasure: 0.6972765294058783
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6915446207204788
Mean absolute error: 0.3301959974082789
Coverage of cases: 84.63541666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.28383550995024875
Kappa statistic: 0.3733062702287563
Training time: 5.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 72.00520833333333
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.7227105034722222
Weighted AreaUnderROC: 0.7142537313432836
Root mean squared error: 0.49969631748399546
Relative absolute error: 68.91957373596239
Root relative squared error: 99.93926349679909
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.37152368800154406
Weighted FMeasure: 0.6846222089590909
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6956071551847774
Mean absolute error: 0.3445978686798119
Coverage of cases: 85.15625
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.2873519900497512
Kappa statistic: 0.35416666666666674
Training time: 5.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 88.15104166666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7616522772772772
Weighted AreaUnderROC: 0.7935671641791044
Root mean squared error: 0.4273752976590222
Relative absolute error: 64.26146276739613
Root relative squared error: 85.47505953180445
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.46983385183055715
Weighted FMeasure: 0.7492925923805163
Iteration time: 5.0
Weighted AreaUnderPRC: 0.763245525391639
Mean absolute error: 0.32130731383698063
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.25798569651741293
Kappa statistic: 0.4642979159549026
Training time: 4.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.28125
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7489420082378944
Weighted AreaUnderROC: 0.7387462686567164
Root mean squared error: 0.45040887288528747
Relative absolute error: 63.12020717762026
Root relative squared error: 90.08177457705749
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.44627306082028434
Weighted FMeasure: 0.7447101050808568
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7168016246242983
Mean absolute error: 0.3156010358881013
Coverage of cases: 92.44791666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.2870830223880597
Kappa statistic: 0.44508670520231214
Training time: 5.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 84.24479166666667
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7486167401080768
Weighted AreaUnderROC: 0.7710298507462686
Root mean squared error: 0.4332300503063726
Relative absolute error: 67.7978493458699
Root relative squared error: 86.64601006127452
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.4320957488201411
Weighted FMeasure: 0.7200878187894403
Iteration time: 5.0
Weighted AreaUnderPRC: 0.7380844348717878
Mean absolute error: 0.33898924672934955
Coverage of cases: 94.79166666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.26088495024875624
Kappa statistic: 0.4179252563113217
Training time: 5.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 80.46875
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7509919289202388
Weighted AreaUnderROC: 0.7365223880597015
Root mean squared error: 0.46119697950007577
Relative absolute error: 63.1037313421654
Root relative squared error: 92.23939590001515
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.4294837412088086
Weighted FMeasure: 0.7100367239467852
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7106222542177552
Mean absolute error: 0.315518656710827
Coverage of cases: 90.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.25261753731343284
Kappa statistic: 0.4081124932395889
Training time: 5.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 71.61458333333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7404347437468991
Weighted AreaUnderROC: 0.7446716417910447
Root mean squared error: 0.4519109613475466
Relative absolute error: 62.38550709850564
Root relative squared error: 90.38219226950932
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.42840252520296923
Weighted FMeasure: 0.7384594166820332
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7161564977458413
Mean absolute error: 0.3119275354925282
Coverage of cases: 88.80208333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3037254353233831
Kappa statistic: 0.42806582905680396
Training time: 6.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 71.61458333333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7404347437468991
Weighted AreaUnderROC: 0.7446716417910447
Root mean squared error: 0.4525269653213993
Relative absolute error: 62.17750832930341
Root relative squared error: 90.50539306427986
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.42840252520296923
Weighted FMeasure: 0.7384594166820332
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7161564977458413
Mean absolute error: 0.31088754164651705
Coverage of cases: 88.80208333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3037254353233831
Kappa statistic: 0.42806582905680396
Training time: 6.0
		
Time end:Tue Oct 31 11.38.23 EET 2017