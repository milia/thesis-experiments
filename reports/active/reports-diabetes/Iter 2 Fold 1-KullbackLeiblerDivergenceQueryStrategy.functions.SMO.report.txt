Tue Oct 31 11.37.16 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.16 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 20.572916666666668
Correctly Classified Instances: 79.42708333333333
Weighted Precision: 0.7977240073145246
Weighted AreaUnderROC: 0.7329253731343285
Root mean squared error: 0.45357377202244253
Relative absolute error: 41.14583333333333
Root relative squared error: 90.7147544044885
Weighted TruePositiveRate: 0.7942708333333334
Weighted MatthewsCorrelation: 0.5304339929146479
Weighted FMeasure: 0.7812331109493439
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7153833509529562
Mean absolute error: 0.20572916666666666
Coverage of cases: 79.42708333333333
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7942708333333334
Weighted FalsePositiveRate: 0.3284200870646767
Kappa statistic: 0.5071163969584714
Training time: 19.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 43.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 19.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 84.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 108.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 132.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 21.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 21.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 153.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 20.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 177.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 22.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 202.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 24.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 227.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 24.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 25.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 252.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 24.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 276.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 19.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 300.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 19.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7426362627130784
Weighted AreaUnderROC: 0.574089552238806
Root mean squared error: 0.5496210815947049
Relative absolute error: 60.416666666666664
Root relative squared error: 109.92421631894098
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.28111316317450696
Weighted FMeasure: 0.6227933114035088
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5972447676058229
Mean absolute error: 0.3020833333333333
Coverage of cases: 69.79166666666667
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 330.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.5497375621890547
Kappa statistic: 0.18225877515053607
Training time: 26.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7902018229166666
Weighted AreaUnderROC: 0.6872238805970149
Root mean squared error: 0.47871355387816905
Relative absolute error: 45.83333333333333
Root relative squared error: 95.74271077563381
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.4789042218767164
Weighted FMeasure: 0.7443957115009746
Iteration time: 25.0
Weighted AreaUnderPRC: 0.6828016493055555
Mean absolute error: 0.22916666666666666
Coverage of cases: 77.08333333333333
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 355.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.39638557213930353
Kappa statistic: 0.4260869565217393
Training time: 23.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 19.791666666666668
Correctly Classified Instances: 80.20833333333333
Weighted Precision: 0.8077412075334269
Weighted AreaUnderROC: 0.7406567164179103
Root mean squared error: 0.44487826050130463
Relative absolute error: 39.58333333333333
Root relative squared error: 88.97565210026093
Weighted TruePositiveRate: 0.8020833333333334
Weighted MatthewsCorrelation: 0.5502917834590965
Weighted FMeasure: 0.789160307454103
Iteration time: 28.0
Weighted AreaUnderPRC: 0.723771871423183
Mean absolute error: 0.19791666666666666
Coverage of cases: 80.20833333333333
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 383.0
Weighted Recall: 0.8020833333333334
Weighted FalsePositiveRate: 0.3207699004975125
Kappa statistic: 0.5249381429873682
Training time: 28.0
		
Time end:Tue Oct 31 11.37.18 EET 2017