Tue Oct 31 11.38.05 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.05 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.723215115198811
Weighted AreaUnderROC: 0.5939402985074627
Root mean squared error: 0.542467356928814
Relative absolute error: 58.854166666666664
Root relative squared error: 108.4934713857628
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.296457528729729
Weighted FMeasure: 0.6484244709444472
Iteration time: 17.0
Weighted AreaUnderPRC: 0.6082778369303931
Mean absolute error: 0.2942708333333333
Coverage of cases: 70.57291666666667
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.5178485696517413
Kappa statistic: 0.22486602357984986
Training time: 14.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.7739202567449958
Weighted AreaUnderROC: 0.503731343283582
Root mean squared error: 0.5885186204927306
Relative absolute error: 69.27083333333334
Root relative squared error: 117.70372409854613
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.06979402073993957
Weighted FMeasure: 0.5194206746240712
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5484292628213846
Mean absolute error: 0.3463541666666667
Coverage of cases: 65.36458333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 24.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.6461831467661692
Kappa statistic: 0.009695183432870725
Training time: 6.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 31.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 7.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 48.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 8.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 4.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 16.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 8.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 85.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 114.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 10.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 124.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 133.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7493862233445566
Weighted AreaUnderROC: 0.6384477611940299
Root mean squared error: 0.5153882032022076
Relative absolute error: 53.125
Root relative squared error: 103.07764064044152
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.37965094619502954
Weighted FMeasure: 0.696178008070433
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6412795489618407
Mean absolute error: 0.265625
Coverage of cases: 73.4375
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 144.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.45747947761194024
Kappa statistic: 0.3214137214137215
Training time: 10.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.767535733549083
Weighted AreaUnderROC: 0.6881492537313433
Root mean squared error: 0.48680506023116343
Relative absolute error: 47.39583333333333
Root relative squared error: 97.36101204623269
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45242336737544947
Weighted FMeasure: 0.7420776770149476
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6776191627477751
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 155.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3867223258706467
Kappa statistic: 0.41911031318571723
Training time: 11.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 22.135416666666668
Correctly Classified Instances: 77.86458333333333
Weighted Precision: 0.7802173503180537
Weighted AreaUnderROC: 0.714
Root mean squared error: 0.4704829079431756
Relative absolute error: 44.27083333333333
Root relative squared error: 94.09658158863512
Weighted TruePositiveRate: 0.7786458333333334
Weighted MatthewsCorrelation: 0.4913841179436946
Weighted FMeasure: 0.7637607501393151
Iteration time: 14.0
Weighted AreaUnderPRC: 0.6977190129313465
Mean absolute error: 0.22135416666666666
Coverage of cases: 77.86458333333333
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 169.0
Weighted Recall: 0.7786458333333334
Weighted FalsePositiveRate: 0.3506458333333333
Kappa statistic: 0.46767564746558815
Training time: 11.0
		
Time end:Tue Oct 31 11.38.06 EET 2017