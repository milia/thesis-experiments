Tue Oct 31 11.39.18 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.18 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 71.74479166666667
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.7052087915972725
Weighted AreaUnderROC: 0.7768708799751245
Root mean squared error: 0.4737591587380488
Relative absolute error: 59.88985894678517
Root relative squared error: 94.75183174760976
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3501911971894347
Weighted FMeasure: 0.7072318254787295
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7804048616491256
Mean absolute error: 0.29944929473392584
Coverage of cases: 89.84375
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 1.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.3696240671641791
Kappa statistic: 0.3491724180052525
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 71.74479166666667
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7090506100082713
Weighted AreaUnderROC: 0.7769552238805971
Root mean squared error: 0.4742998750275914
Relative absolute error: 59.893771273425266
Root relative squared error: 94.85997500551828
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.35905591635353806
Weighted FMeasure: 0.7107985636114912
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7819004088552676
Mean absolute error: 0.29946885636712633
Coverage of cases: 89.84375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 2.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.3613028606965174
Kappa statistic: 0.3584447144592951
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 72.65625
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7403374369081884
Weighted AreaUnderROC: 0.7958156871890547
Root mean squared error: 0.44893781883280365
Relative absolute error: 55.423444987417106
Root relative squared error: 89.78756376656072
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4274670648026431
Weighted FMeasure: 0.7418009440104166
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7965032726831492
Mean absolute error: 0.2771172249370855
Coverage of cases: 90.625
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32723942786069654
Kappa statistic: 0.42641141324228743
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 73.56770833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7290167297979798
Weighted AreaUnderROC: 0.7965918065920398
Root mean squared error: 0.4476383333165241
Relative absolute error: 55.58581190230275
Root relative squared error: 89.52766666330481
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.40224336990133147
Weighted FMeasure: 0.7306720870737461
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7969386954109302
Mean absolute error: 0.27792905951151375
Coverage of cases: 91.92708333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 4.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3432108208955224
Kappa statistic: 0.4008810572687225
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 74.34895833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.727897420226454
Weighted AreaUnderROC: 0.7926216573383084
Root mean squared error: 0.44833961116295834
Relative absolute error: 56.204629235474954
Root relative squared error: 89.66792223259166
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.39886620025273917
Weighted FMeasure: 0.7294276061776063
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7926380141446586
Mean absolute error: 0.2810231461773748
Coverage of cases: 91.40625
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 5.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3501361940298507
Kappa statistic: 0.39659847177717533
Training time: 1.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 75.91145833333333
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.737365172202495
Weighted AreaUnderROC: 0.7920246424129352
Root mean squared error: 0.44809653249466835
Relative absolute error: 57.24058657632183
Root relative squared error: 89.61930649893367
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.420756224155016
Weighted FMeasure: 0.7388824389404883
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7926961829861069
Mean absolute error: 0.28620293288160914
Coverage of cases: 92.70833333333333
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.332097947761194
Kappa statistic: 0.4195321565992793
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 74.86979166666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7314136739601501
Weighted AreaUnderROC: 0.7936664334577115
Root mean squared error: 0.44709033470489923
Relative absolute error: 56.644172002795344
Root relative squared error: 89.41806694097984
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.4072898202955106
Weighted FMeasure: 0.7330125068754239
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7939097894430294
Mean absolute error: 0.2832208600139767
Coverage of cases: 92.70833333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.34181498756218903
Kappa statistic: 0.4057002758197977
Training time: 1.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 74.34895833333333
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7427245689700323
Weighted AreaUnderROC: 0.7978753886815921
Root mean squared error: 0.44420498872582254
Relative absolute error: 55.57725947357397
Root relative squared error: 88.8409977451645
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4325170619826129
Weighted FMeasure: 0.7441575411841151
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7973804643873178
Mean absolute error: 0.27788629736786985
Coverage of cases: 92.1875
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.32584359452736317
Kappa statistic: 0.4312587796982838
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 76.30208333333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7423502604166666
Weighted AreaUnderROC: 0.800054493159204
Root mean squared error: 0.4410260454802422
Relative absolute error: 56.185459746033914
Root relative squared error: 88.20520909604845
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4326937492003032
Weighted FMeasure: 0.7433825267276147
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7986347845486458
Mean absolute error: 0.2809272987301696
Coverage of cases: 93.48958333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.316851368159204
Kappa statistic: 0.43243243243243235
Training time: 1.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 77.47395833333333
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7326452161791733
Weighted AreaUnderROC: 0.8017313432835821
Root mean squared error: 0.43712827784256864
Relative absolute error: 56.31727894896331
Root relative squared error: 87.42565556851373
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.41074509206589765
Weighted FMeasure: 0.734181264451839
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8004468635767722
Mean absolute error: 0.28158639474481656
Coverage of cases: 93.75
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.33488961442786064
Kappa statistic: 0.4098959284279714
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 78.90625
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7409597291149711
Weighted AreaUnderROC: 0.8007164179104479
Root mean squared error: 0.43675958812840904
Relative absolute error: 57.13964701295008
Root relative squared error: 87.3519176256818
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4291631054449408
Weighted FMeasure: 0.7423478112175103
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7994222966959921
Mean absolute error: 0.2856982350647504
Coverage of cases: 94.01041666666667
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32377674129353234
Kappa statistic: 0.42843256379100836
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 78.38541666666667
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7416309062384644
Weighted AreaUnderROC: 0.7987462686567164
Root mean squared error: 0.4391843897987636
Relative absolute error: 57.266229319926175
Root relative squared error: 87.83687795975273
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.43090552972108154
Weighted FMeasure: 0.7428748990510802
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7963303373866024
Mean absolute error: 0.2863311465996309
Coverage of cases: 94.01041666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32031405472636815
Kappa statistic: 0.4304395205230657
Training time: 1.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 79.16666666666667
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7363379783437923
Weighted AreaUnderROC: 0.7946268656716416
Root mean squared error: 0.4413190669319156
Relative absolute error: 58.071837418682634
Root relative squared error: 88.26381338638312
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.4192692622518202
Weighted FMeasure: 0.7376274480113061
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7937457981364681
Mean absolute error: 0.2903591870934132
Coverage of cases: 94.01041666666667
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.326568407960199
Kappa statistic: 0.4188158372684346
Training time: 1.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 79.16666666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7333312419562419
Weighted AreaUnderROC: 0.7940298507462686
Root mean squared error: 0.4415631439338721
Relative absolute error: 58.193611511738574
Root relative squared error: 88.31262878677441
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.4125383414091344
Weighted FMeasure: 0.734734638160473
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7900023369104122
Mean absolute error: 0.29096805755869287
Coverage of cases: 94.79166666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3314269278606965
Kappa statistic: 0.41197161744193095
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 80.20833333333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7409597291149711
Weighted AreaUnderROC: 0.7953432835820896
Root mean squared error: 0.4370748264783373
Relative absolute error: 58.282858692865304
Root relative squared error: 87.41496529566746
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4291631054449408
Weighted FMeasure: 0.7423478112175103
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7890731469071298
Mean absolute error: 0.2914142934643265
Coverage of cases: 95.3125
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32377674129353234
Kappa statistic: 0.42843256379100836
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 79.6875
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7409597291149711
Weighted AreaUnderROC: 0.7939701492537313
Root mean squared error: 0.43985294058882957
Relative absolute error: 58.20277923628524
Root relative squared error: 87.97058811776591
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.4291631054449408
Weighted FMeasure: 0.7423478112175103
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7879958586980506
Mean absolute error: 0.2910138961814262
Coverage of cases: 95.05208333333333
Instances selection time: 0.0
Test time: 8.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.32377674129353234
Kappa statistic: 0.42843256379100836
Training time: 1.0
		
Time end:Tue Oct 31 11.39.18 EET 2017