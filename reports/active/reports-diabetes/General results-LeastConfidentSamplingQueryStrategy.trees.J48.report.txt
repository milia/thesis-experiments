Tue Oct 31 11.38.17 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.17 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 82.109375
Incorrectly Classified Instances: 30.755208333333332
Correctly Classified Instances: 69.24479166666667
Weighted Precision: 0.7107600314281972
Weighted AreaUnderROC: 0.6593208955223882
Root mean squared error: 0.48864799023884664
Relative absolute error: 66.98104607179087
Root relative squared error: 97.72959804776934
Weighted TruePositiveRate: 0.6924479166666666
Weighted MatthewsCorrelation: 0.34750679775911064
Weighted FMeasure: 0.6876337467450564
Iteration time: 3.8
Weighted AreaUnderPRC: 0.6507743417165968
Mean absolute error: 0.33490523035895436
Coverage of cases: 90.15624999999999
Instances selection time: 2.8
Test time: 2.3
Accumulative iteration time: 3.8
Weighted Recall: 0.6924479166666666
Weighted FalsePositiveRate: 0.35841209577114425
Kappa statistic: 0.3327738410221547
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 84.94791666666667
Incorrectly Classified Instances: 31.380208333333332
Correctly Classified Instances: 68.61979166666666
Weighted Precision: 0.6891190904986031
Weighted AreaUnderROC: 0.638423880597015
Root mean squared error: 0.508095351540993
Relative absolute error: 69.85443523044667
Root relative squared error: 101.61907030819859
Weighted TruePositiveRate: 0.6861979166666666
Weighted MatthewsCorrelation: 0.293667849531453
Weighted FMeasure: 0.6680692727074421
Iteration time: 2.3
Weighted AreaUnderPRC: 0.6370436841166247
Mean absolute error: 0.3492721761522334
Coverage of cases: 90.390625
Instances selection time: 0.9
Test time: 2.7
Accumulative iteration time: 6.1
Weighted Recall: 0.6861979166666666
Weighted FalsePositiveRate: 0.42235911069651744
Kappa statistic: 0.2754444358153296
Training time: 1.4
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 84.21875000000001
Incorrectly Classified Instances: 31.09375
Correctly Classified Instances: 68.90625
Weighted Precision: 0.6753182384570725
Weighted AreaUnderROC: 0.6580044776119404
Root mean squared error: 0.4993924418471214
Relative absolute error: 69.25610292978612
Root relative squared error: 99.8784883694243
Weighted TruePositiveRate: 0.6890625
Weighted MatthewsCorrelation: 0.2958119909127558
Weighted FMeasure: 0.6663510747840882
Iteration time: 2.4
Weighted AreaUnderPRC: 0.6534801569312954
Mean absolute error: 0.3462805146489306
Coverage of cases: 91.71875
Instances selection time: 0.4
Test time: 2.5
Accumulative iteration time: 8.5
Weighted Recall: 0.6890625
Weighted FalsePositiveRate: 0.41147444029850744
Kappa statistic: 0.2808802577186289
Training time: 2.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 83.91927083333333
Incorrectly Classified Instances: 32.10937500000001
Correctly Classified Instances: 67.890625
Weighted Precision: 0.6791617279224119
Weighted AreaUnderROC: 0.6437149253731345
Root mean squared error: 0.4987176722910843
Relative absolute error: 70.56046845186468
Root relative squared error: 99.74353445821687
Weighted TruePositiveRate: 0.67890625
Weighted MatthewsCorrelation: 0.2950240193866704
Weighted FMeasure: 0.655484860480625
Iteration time: 2.6
Weighted AreaUnderPRC: 0.6425367993739034
Mean absolute error: 0.35280234225932344
Coverage of cases: 91.40624999999999
Instances selection time: 0.5
Test time: 1.5
Accumulative iteration time: 11.1
Weighted Recall: 0.67890625
Weighted FalsePositiveRate: 0.4034137126865672
Kappa statistic: 0.27259121656993346
Training time: 2.1
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 72.60416666666667
Incorrectly Classified Instances: 31.067708333333332
Correctly Classified Instances: 68.93229166666667
Weighted Precision: 0.6805423108222505
Weighted AreaUnderROC: 0.6436179104477613
Root mean squared error: 0.5122695997421148
Relative absolute error: 68.02416217477762
Root relative squared error: 102.45391994842296
Weighted TruePositiveRate: 0.6893229166666667
Weighted MatthewsCorrelation: 0.30096576801973185
Weighted FMeasure: 0.664870244037915
Iteration time: 3.2
Weighted AreaUnderPRC: 0.6465767945072929
Mean absolute error: 0.3401208108738881
Coverage of cases: 84.55729166666667
Instances selection time: 0.9
Test time: 1.8
Accumulative iteration time: 14.3
Weighted Recall: 0.6893229166666667
Weighted FalsePositiveRate: 0.409603513681592
Kappa statistic: 0.2807833545120899
Training time: 2.3
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 81.79687500000001
Incorrectly Classified Instances: 29.557291666666668
Correctly Classified Instances: 70.44270833333334
Weighted Precision: 0.7209588763097056
Weighted AreaUnderROC: 0.6463044776119403
Root mean squared error: 0.4999782454886037
Relative absolute error: 67.30866225659501
Root relative squared error: 99.99564909772076
Weighted TruePositiveRate: 0.7044270833333334
Weighted MatthewsCorrelation: 0.3540737920911903
Weighted FMeasure: 0.6897784075819964
Iteration time: 3.2
Weighted AreaUnderPRC: 0.65384824668194
Mean absolute error: 0.3365433112829751
Coverage of cases: 88.93229166666667
Instances selection time: 0.7
Test time: 1.8
Accumulative iteration time: 17.5
Weighted Recall: 0.7044270833333334
Weighted FalsePositiveRate: 0.387656934079602
Kappa statistic: 0.326735059957804
Training time: 2.5
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 78.50260416666666
Incorrectly Classified Instances: 29.739583333333332
Correctly Classified Instances: 70.26041666666666
Weighted Precision: 0.7220530274884418
Weighted AreaUnderROC: 0.6509074626865672
Root mean squared error: 0.5026140406262082
Relative absolute error: 65.39664545784504
Root relative squared error: 100.52280812524164
Weighted TruePositiveRate: 0.7026041666666668
Weighted MatthewsCorrelation: 0.35759407192322934
Weighted FMeasure: 0.6901464001225261
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6551401066027436
Mean absolute error: 0.32698322728922513
Coverage of cases: 87.55208333333333
Instances selection time: 1.8
Test time: 1.4
Accumulative iteration time: 22.5
Weighted Recall: 0.7026041666666668
Weighted FalsePositiveRate: 0.3778996890547263
Kappa statistic: 0.331171503061494
Training time: 3.2
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 85.79427083333333
Incorrectly Classified Instances: 27.317708333333336
Correctly Classified Instances: 72.68229166666666
Weighted Precision: 0.7314839330069375
Weighted AreaUnderROC: 0.6739716417910449
Root mean squared error: 0.4821263511651904
Relative absolute error: 65.52454342208065
Root relative squared error: 96.42527023303808
Weighted TruePositiveRate: 0.7268229166666667
Weighted MatthewsCorrelation: 0.38837521376297185
Weighted FMeasure: 0.7152665222143485
Iteration time: 4.4
Weighted AreaUnderPRC: 0.669745246205572
Mean absolute error: 0.32762271711040325
Coverage of cases: 92.1875
Instances selection time: 0.6
Test time: 1.5
Accumulative iteration time: 26.9
Weighted Recall: 0.7268229166666667
Weighted FalsePositiveRate: 0.3725363495024876
Kappa statistic: 0.3701249259617194
Training time: 3.8
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 80.01302083333334
Incorrectly Classified Instances: 27.630208333333332
Correctly Classified Instances: 72.36979166666666
Weighted Precision: 0.7346368022189592
Weighted AreaUnderROC: 0.6860149253731344
Root mean squared error: 0.48953488198366363
Relative absolute error: 62.64110356131581
Root relative squared error: 97.90697639673272
Weighted TruePositiveRate: 0.7236979166666667
Weighted MatthewsCorrelation: 0.39273977600504534
Weighted FMeasure: 0.7132262098750851
Iteration time: 4.9
Weighted AreaUnderPRC: 0.6781235226393225
Mean absolute error: 0.31320551780657907
Coverage of cases: 89.79166666666666
Instances selection time: 0.5
Test time: 1.7
Accumulative iteration time: 31.8
Weighted Recall: 0.7236979166666667
Weighted FalsePositiveRate: 0.3613994092039801
Kappa statistic: 0.3720043493469552
Training time: 4.4
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 85.01302083333334
Incorrectly Classified Instances: 28.802083333333332
Correctly Classified Instances: 71.19791666666666
Weighted Precision: 0.7290530203520154
Weighted AreaUnderROC: 0.6865910447761194
Root mean squared error: 0.4824488214774731
Relative absolute error: 64.87947934380256
Root relative squared error: 96.48976429549462
Weighted TruePositiveRate: 0.7119791666666667
Weighted MatthewsCorrelation: 0.37002786623753214
Weighted FMeasure: 0.696264834947576
Iteration time: 4.5
Weighted AreaUnderPRC: 0.6792450317590903
Mean absolute error: 0.32439739671901274
Coverage of cases: 92.00520833333334
Instances selection time: 0.5
Test time: 1.6
Accumulative iteration time: 36.3
Weighted Recall: 0.7119791666666667
Weighted FalsePositiveRate: 0.382570211442786
Kappa statistic: 0.34075496150898277
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 84.15364583333333
Incorrectly Classified Instances: 27.317708333333336
Correctly Classified Instances: 72.68229166666666
Weighted Precision: 0.7405940477058129
Weighted AreaUnderROC: 0.6898089552238806
Root mean squared error: 0.47124186816175967
Relative absolute error: 62.6833815987928
Root relative squared error: 94.24837363235196
Weighted TruePositiveRate: 0.7268229166666667
Weighted MatthewsCorrelation: 0.39719950896916373
Weighted FMeasure: 0.711313172174197
Iteration time: 5.2
Weighted AreaUnderPRC: 0.6817239176771102
Mean absolute error: 0.313416907993964
Coverage of cases: 92.00520833333334
Instances selection time: 0.6
Test time: 1.1
Accumulative iteration time: 41.5
Weighted Recall: 0.7268229166666667
Weighted FalsePositiveRate: 0.3673423196517413
Kappa statistic: 0.368782797964107
Training time: 4.6
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 80.0
Incorrectly Classified Instances: 26.09375
Correctly Classified Instances: 73.90625
Weighted Precision: 0.7438297492727154
Weighted AreaUnderROC: 0.6965208955223879
Root mean squared error: 0.4710492964984213
Relative absolute error: 60.21256966102885
Root relative squared error: 94.20985929968425
Weighted TruePositiveRate: 0.7390625
Weighted MatthewsCorrelation: 0.4151538083762937
Weighted FMeasure: 0.727171547219619
Iteration time: 6.4
Weighted AreaUnderPRC: 0.6866412103064254
Mean absolute error: 0.30106284830514424
Coverage of cases: 90.05208333333334
Instances selection time: 0.8
Test time: 1.2
Accumulative iteration time: 47.9
Weighted Recall: 0.7390625
Weighted FalsePositiveRate: 0.3569729477611941
Kappa statistic: 0.3963911539516108
Training time: 5.6
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 80.74218750000001
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666666
Weighted Precision: 0.7457553344276847
Weighted AreaUnderROC: 0.7141194029850746
Root mean squared error: 0.4591746214414645
Relative absolute error: 61.30141780978562
Root relative squared error: 91.8349242882929
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.413535679991896
Weighted FMeasure: 0.7232375173298443
Iteration time: 6.0
Weighted AreaUnderPRC: 0.7013554546097722
Mean absolute error: 0.3065070890489281
Coverage of cases: 90.98958333333333
Instances selection time: 0.4
Test time: 1.0
Accumulative iteration time: 53.9
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3605134950248757
Kappa statistic: 0.3900949097012448
Training time: 5.6
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 79.75260416666667
Incorrectly Classified Instances: 27.161458333333332
Correctly Classified Instances: 72.83854166666666
Weighted Precision: 0.7389822286539088
Weighted AreaUnderROC: 0.6952850746268655
Root mean squared error: 0.4684620809938626
Relative absolute error: 62.87693794000629
Root relative squared error: 93.69241619877252
Weighted TruePositiveRate: 0.7283854166666667
Weighted MatthewsCorrelation: 0.406351825733422
Weighted FMeasure: 0.7204526688635164
Iteration time: 5.8
Weighted AreaUnderPRC: 0.6859112441494333
Mean absolute error: 0.31438468970003147
Coverage of cases: 89.37499999999999
Instances selection time: 0.6
Test time: 1.1
Accumulative iteration time: 59.7
Weighted Recall: 0.7283854166666667
Weighted FalsePositiveRate: 0.3429585509950249
Kappa statistic: 0.38921825219288814
Training time: 5.2
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 83.15104166666666
Incorrectly Classified Instances: 26.171875
Correctly Classified Instances: 73.828125
Weighted Precision: 0.736530286545874
Weighted AreaUnderROC: 0.7203880597014926
Root mean squared error: 0.4580250859885755
Relative absolute error: 61.11014564762036
Root relative squared error: 91.6050171977151
Weighted TruePositiveRate: 0.73828125
Weighted MatthewsCorrelation: 0.40876711806533095
Weighted FMeasure: 0.729131984242285
Iteration time: 6.7
Weighted AreaUnderPRC: 0.7037046829918414
Mean absolute error: 0.3055507282381018
Coverage of cases: 91.97916666666667
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 66.4
Weighted Recall: 0.73828125
Weighted FalsePositiveRate: 0.35496781716417913
Kappa statistic: 0.3980672417864043
Training time: 6.2
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.88541666666667
Incorrectly Classified Instances: 26.354166666666668
Correctly Classified Instances: 73.64583333333331
Weighted Precision: 0.7380173425668601
Weighted AreaUnderROC: 0.7304164179104478
Root mean squared error: 0.4514306770301575
Relative absolute error: 62.35046094368111
Root relative squared error: 90.28613540603149
Weighted TruePositiveRate: 0.7364583333333334
Weighted MatthewsCorrelation: 0.4148074944197816
Weighted FMeasure: 0.730919399505648
Iteration time: 7.7
Weighted AreaUnderPRC: 0.7115518003310422
Mean absolute error: 0.31175230471840554
Coverage of cases: 93.20312500000003
Instances selection time: 0.5
Test time: 1.0
Accumulative iteration time: 74.1
Weighted Recall: 0.7364583333333334
Weighted FalsePositiveRate: 0.3358613184079603
Kappa statistic: 0.40663322706387695
Training time: 7.2
		
Time end:Tue Oct 31 11.38.24 EET 2017