Tue Oct 31 11.37.47 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.47 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 87.23958333333333
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.6891480576643515
Weighted AreaUnderROC: 0.7520746268656716
Root mean squared error: 0.44350051672273566
Relative absolute error: 66.61458333333326
Root relative squared error: 88.70010334454713
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.2984445177935727
Weighted FMeasure: 0.6813732890499195
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7368074656075398
Mean absolute error: 0.3330729166666663
Coverage of cases: 97.91666666666667
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.4465279850746269
Kappa statistic: 0.281983991602152
Training time: 5.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 93.75
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7323875523012552
Weighted AreaUnderROC: 0.7910298507462686
Root mean squared error: 0.4310839052125855
Relative absolute error: 68.0208333333333
Root relative squared error: 86.2167810425171
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.4102415846142742
Weighted FMeasure: 0.7288773720781934
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7780715987138572
Mean absolute error: 0.3401041666666665
Coverage of cases: 99.21875
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3093087686567164
Kappa statistic: 0.4094557384732556
Training time: 8.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 95.3125
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6908885135135135
Weighted AreaUnderROC: 0.7557164179104477
Root mean squared error: 0.44648511546672387
Relative absolute error: 72.96874999999994
Root relative squared error: 89.29702309334478
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.3192571854538229
Weighted FMeasure: 0.6927124026215381
Iteration time: 12.0
Weighted AreaUnderPRC: 0.750019027953809
Mean absolute error: 0.3648437499999997
Coverage of cases: 98.95833333333333
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.38146175373134333
Kappa statistic: 0.3188186063436231
Training time: 10.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 94.27083333333333
Incorrectly Classified Instances: 29.6875
Correctly Classified Instances: 70.3125
Weighted Precision: 0.7011375681405209
Weighted AreaUnderROC: 0.7540746268656715
Root mean squared error: 0.4466892096301409
Relative absolute error: 72.70833333333331
Root relative squared error: 89.33784192602818
Weighted TruePositiveRate: 0.703125
Weighted MatthewsCorrelation: 0.3421616441170234
Weighted FMeasure: 0.7020540223665224
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7390852353709486
Mean absolute error: 0.3635416666666666
Coverage of cases: 98.95833333333333
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.703125
Weighted FalsePositiveRate: 0.3634235074626866
Kappa statistic: 0.3420704580978718
Training time: 13.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 94.01041666666667
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.7158721221221221
Weighted AreaUnderROC: 0.7845223880597015
Root mean squared error: 0.4473300422134274
Relative absolute error: 73.95833333333334
Root relative squared error: 89.46600844268548
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.3702664130320284
Weighted FMeasure: 0.7032442930218354
Iteration time: 16.0
Weighted AreaUnderPRC: 0.7837635799347061
Mean absolute error: 0.3697916666666667
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 60.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.31427487562189055
Kappa statistic: 0.3659036556200888
Training time: 15.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 95.83333333333333
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.7197124094202899
Weighted AreaUnderROC: 0.7585074626865672
Root mean squared error: 0.4717818616691408
Relative absolute error: 78.90625
Root relative squared error: 94.35637233382816
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3695681398517879
Weighted FMeasure: 0.6896182739343116
Iteration time: 17.0
Weighted AreaUnderPRC: 0.7437365325998235
Mean absolute error: 0.39453125
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 77.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.2949483830845771
Kappa statistic: 0.3564835164835164
Training time: 16.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 94.79166666666667
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.7261721079308289
Weighted AreaUnderROC: 0.7515820895522388
Root mean squared error: 0.49900943544853604
Relative absolute error: 83.43750000000001
Root relative squared error: 99.80188708970721
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.362925365939919
Weighted FMeasure: 0.6603606479930156
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7362307385269994
Mean absolute error: 0.41718750000000004
Coverage of cases: 96.35416666666667
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.27567568407960197
Kappa statistic: 0.33148332373422695
Training time: 16.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 94.01041666666667
Incorrectly Classified Instances: 29.427083333333332
Correctly Classified Instances: 70.57291666666667
Weighted Precision: 0.7459713494304044
Weighted AreaUnderROC: 0.7838358208955224
Root mean squared error: 0.4622465521775149
Relative absolute error: 76.61458333333334
Root relative squared error: 92.44931043550298
Weighted TruePositiveRate: 0.7057291666666666
Weighted MatthewsCorrelation: 0.42351445173450647
Weighted FMeasure: 0.7125769669029252
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7629705572585032
Mean absolute error: 0.3830729166666667
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 116.0
Weighted Recall: 0.7057291666666666
Weighted FalsePositiveRate: 0.261609763681592
Kappa statistic: 0.40679170995789343
Training time: 20.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 92.44791666666667
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.7296223958333333
Weighted AreaUnderROC: 0.7874029850746269
Root mean squared error: 0.43484312305320727
Relative absolute error: 69.73958333333333
Root relative squared error: 86.96862461064146
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.40080515979638154
Weighted FMeasure: 0.7183517394299491
Iteration time: 22.0
Weighted AreaUnderPRC: 0.7780759334718651
Mean absolute error: 0.34869791666666666
Coverage of cases: 99.21875
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 138.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.29897450248756224
Kappa statistic: 0.39670932358318084
Training time: 21.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 91.40625
Incorrectly Classified Instances: 28.645833333333332
Correctly Classified Instances: 71.35416666666667
Weighted Precision: 0.716684962919594
Weighted AreaUnderROC: 0.7714029850746269
Root mean squared error: 0.44385268764910446
Relative absolute error: 70.78125
Root relative squared error: 88.7705375298209
Weighted TruePositiveRate: 0.7135416666666666
Weighted MatthewsCorrelation: 0.37625201297254923
Weighted FMeasure: 0.7149384462700829
Iteration time: 24.0
Weighted AreaUnderPRC: 0.7624882231557738
Mean absolute error: 0.35390625000000003
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 162.0
Weighted Recall: 0.7135416666666666
Weighted FalsePositiveRate: 0.333601368159204
Kappa statistic: 0.37603403450720857
Training time: 23.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 90.75520833333333
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7525533987963763
Weighted AreaUnderROC: 0.8271194029850745
Root mean squared error: 0.40233692348577693
Relative absolute error: 62.08333333333324
Root relative squared error: 80.46738469715538
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45311010981104904
Weighted FMeasure: 0.7535927373677437
Iteration time: 27.0
Weighted AreaUnderPRC: 0.8073918387926633
Mean absolute error: 0.31041666666666623
Coverage of cases: 98.95833333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 189.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.32026026119402984
Kappa statistic: 0.45082118472042815
Training time: 27.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.625
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7379766883157338
Weighted AreaUnderROC: 0.812089552238806
Root mean squared error: 0.41196303434491116
Relative absolute error: 62.55208333333325
Root relative squared error: 82.39260686898223
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.4224545289889343
Weighted FMeasure: 0.7394450017894264
Iteration time: 29.0
Weighted AreaUnderPRC: 0.8073525732985583
Mean absolute error: 0.31276041666666626
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 218.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.32863526119402986
Kappa statistic: 0.4215811575680116
Training time: 28.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 87.5
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7240088646338646
Weighted AreaUnderROC: 0.8020298507462686
Root mean squared error: 0.4153625424453515
Relative absolute error: 62.135416666666664
Root relative squared error: 83.0725084890703
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.38887147728766563
Weighted FMeasure: 0.7250784491616914
Iteration time: 29.0
Weighted AreaUnderPRC: 0.7849037421172845
Mean absolute error: 0.3106770833333333
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 247.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.36192008706467665
Kappa statistic: 0.3851893303488156
Training time: 29.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7490097601540616
Weighted AreaUnderROC: 0.8021343283582091
Root mean squared error: 0.4173015995655899
Relative absolute error: 62.86458333333328
Root relative squared error: 83.46031991311797
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.44374946010534516
Weighted FMeasure: 0.7494208095816589
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7850366014063778
Mean absolute error: 0.3143229166666664
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 278.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.3320441542288557
Kappa statistic: 0.43992055610724934
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7383548570500325
Weighted AreaUnderROC: 0.8170000000000001
Root mean squared error: 0.41186820302939936
Relative absolute error: 59.99999999999995
Root relative squared error: 82.37364060587987
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.42116129347630277
Weighted FMeasure: 0.7394088606286187
Iteration time: 33.0
Weighted AreaUnderPRC: 0.8059499689864381
Mean absolute error: 0.29999999999999977
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 311.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3410901741293533
Kappa statistic: 0.41818181818181804
Training time: 32.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 86.97916666666667
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7573829651460232
Weighted AreaUnderROC: 0.8146268656716419
Root mean squared error: 0.40996697021426776
Relative absolute error: 59.68749999999988
Root relative squared error: 81.99339404285355
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4623903776408383
Weighted FMeasure: 0.7577227918644981
Iteration time: 33.0
Weighted AreaUnderPRC: 0.7956245640645917
Mean absolute error: 0.2984374999999994
Coverage of cases: 97.39583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 344.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.32093128109452734
Kappa statistic: 0.45876959296202224
Training time: 32.0
		
Time end:Tue Oct 31 11.37.48 EET 2017