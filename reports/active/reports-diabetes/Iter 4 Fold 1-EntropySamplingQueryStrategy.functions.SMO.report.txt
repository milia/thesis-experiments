Tue Oct 31 11.36.31 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.36.31 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7427884615384616
Weighted AreaUnderROC: 0.6989253731343283
Root mean squared error: 0.5
Relative absolute error: 50.0
Root relative squared error: 100.0
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42672382893015043
Weighted FMeasure: 0.7413191691771047
Iteration time: 31.0
Weighted AreaUnderPRC: 0.6773825620993589
Mean absolute error: 0.25
Coverage of cases: 75.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3521492537313433
Kappa statistic: 0.4196473551637279
Training time: 26.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7376761842487648
Weighted AreaUnderROC: 0.6614925373134328
Root mean squared error: 0.5103103630798288
Relative absolute error: 52.083333333333336
Root relative squared error: 102.06207261596576
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.39030862687585804
Weighted FMeasure: 0.71597413003663
Iteration time: 13.0
Weighted AreaUnderPRC: 0.653614757020125
Mean absolute error: 0.2604166666666667
Coverage of cases: 73.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.4165982587064676
Kappa statistic: 0.36042638241172564
Training time: 12.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.8125
Correctly Classified Instances: 67.1875
Weighted Precision: 0.7087958494208494
Weighted AreaUnderROC: 0.5350447761194029
Root mean squared error: 0.57282196186948
Relative absolute error: 65.625
Root relative squared error: 114.564392373896
Weighted TruePositiveRate: 0.671875
Weighted MatthewsCorrelation: 0.17824231297450457
Weighted FMeasure: 0.5706053836094158
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5687683498230373
Mean absolute error: 0.328125
Coverage of cases: 67.1875
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.671875
Weighted FalsePositiveRate: 0.601785447761194
Kappa statistic: 0.08847023360964576
Training time: 7.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.635416666666664
Correctly Classified Instances: 65.36458333333333
Weighted Precision: 0.6353864335971856
Weighted AreaUnderROC: 0.5071940298507462
Root mean squared error: 0.5885186204927306
Relative absolute error: 69.27083333333334
Root relative squared error: 117.70372409854613
Weighted TruePositiveRate: 0.6536458333333334
Weighted MatthewsCorrelation: 0.060495076125410496
Weighted FMeasure: 0.5284439777271983
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5497273655659082
Mean absolute error: 0.3463541666666667
Coverage of cases: 65.36458333333333
Instances selection time: 8.0
Test time: 2.0
Accumulative iteration time: 76.0
Weighted Recall: 0.6536458333333334
Weighted FalsePositiveRate: 0.6392577736318409
Kappa statistic: 0.01852563609808598
Training time: 16.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 19.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 95.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 14.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 111.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 15.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 121.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 138.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 16.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 147.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 9.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 159.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 12.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 173.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 13.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.895833333333336
Correctly Classified Instances: 65.10416666666667
Weighted Precision: 0.4238552517361111
Weighted AreaUnderROC: 0.5
Root mean squared error: 0.590726953281576
Relative absolute error: 69.79166666666666
Root relative squared error: 118.1453906563152
Weighted TruePositiveRate: 0.6510416666666666
Weighted MatthewsCorrelation: 0.0
Weighted FMeasure: 0.513439800210305
Iteration time: 24.0
Weighted AreaUnderPRC: 0.5456271701388888
Mean absolute error: 0.3489583333333333
Coverage of cases: 65.10416666666667
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 197.0
Weighted Recall: 0.6510416666666666
Weighted FalsePositiveRate: 0.6510416666666666
Kappa statistic: 0.0
Training time: 18.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.375
Correctly Classified Instances: 65.625
Weighted Precision: 0.6883223684210527
Weighted AreaUnderROC: 0.5091940298507462
Root mean squared error: 0.5863019699779287
Relative absolute error: 68.75
Root relative squared error: 117.26039399558574
Weighted TruePositiveRate: 0.65625
Weighted MatthewsCorrelation: 0.08632498561936641
Weighted FMeasure: 0.5298050379572119
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5514976813779239
Mean absolute error: 0.34375
Coverage of cases: 65.625
Instances selection time: 6.0
Test time: 3.0
Accumulative iteration time: 217.0
Weighted Recall: 0.65625
Weighted FalsePositiveRate: 0.6378619402985075
Kappa statistic: 0.023728813559322
Training time: 14.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.479166666666668
Correctly Classified Instances: 75.52083333333333
Weighted Precision: 0.7654344149037545
Weighted AreaUnderROC: 0.6717611940298508
Root mean squared error: 0.4947642536265799
Relative absolute error: 48.95833333333333
Root relative squared error: 98.95285072531598
Weighted TruePositiveRate: 0.7552083333333334
Weighted MatthewsCorrelation: 0.43400219094311837
Weighted FMeasure: 0.7282467723004696
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6664631463514866
Mean absolute error: 0.24479166666666666
Coverage of cases: 75.52083333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 230.0
Weighted Recall: 0.7552083333333334
Weighted FalsePositiveRate: 0.41168594527363184
Kappa statistic: 0.3893625659764516
Training time: 13.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7425206161059742
Weighted AreaUnderROC: 0.6796119402985075
Root mean squared error: 0.5025974200756175
Relative absolute error: 50.520833333333336
Root relative squared error: 100.5194840151235
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4124226862417646
Weighted FMeasure: 0.7304093266295716
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6658806546783466
Mean absolute error: 0.2526041666666667
Coverage of cases: 74.73958333333333
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 248.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.3881719527363184
Kappa statistic: 0.3925239741666124
Training time: 18.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7433969185619956
Weighted AreaUnderROC: 0.6902686567164179
Root mean squared error: 0.5
Relative absolute error: 50.0
Root relative squared error: 100.0
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42184871014324016
Weighted FMeasure: 0.7373294346978557
Iteration time: 24.0
Weighted AreaUnderPRC: 0.672509339717331
Mean absolute error: 0.25
Coverage of cases: 75.0
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 272.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3694626865671642
Kappa statistic: 0.40885182809493265
Training time: 21.0
		
Time end:Tue Oct 31 11.36.32 EET 2017