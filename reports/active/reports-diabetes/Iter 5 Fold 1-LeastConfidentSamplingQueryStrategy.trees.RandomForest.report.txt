Tue Oct 31 11.38.31 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.31 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 94.53125
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.684017995715105
Weighted AreaUnderROC: 0.7222089552238806
Root mean squared error: 0.45245280601038074
Relative absolute error: 74.42708333333323
Root relative squared error: 90.49056120207615
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.3005355157763378
Weighted FMeasure: 0.6860500307069689
Iteration time: 6.0
Weighted AreaUnderPRC: 0.713177756203828
Mean absolute error: 0.37213541666666616
Coverage of cases: 98.69791666666667
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.4126259328358209
Kappa statistic: 0.2965491325859586
Training time: 4.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 94.66145833333333
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6901402133132478
Weighted AreaUnderROC: 0.7254477611940299
Root mean squared error: 0.4676514906066984
Relative absolute error: 76.97916666666659
Root relative squared error: 93.53029812133968
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.3166217218966009
Weighted FMeasure: 0.6836037103628637
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7149617270232728
Mean absolute error: 0.38489583333333294
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.3552098880597015
Kappa statistic: 0.3152003711651104
Training time: 7.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 94.79166666666667
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7218457259953163
Weighted AreaUnderROC: 0.7494477611940299
Root mean squared error: 0.454204891357781
Relative absolute error: 75.10416666666666
Root relative squared error: 90.84097827155621
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.38760342505468776
Weighted FMeasure: 0.7201213836106269
Iteration time: 10.0
Weighted AreaUnderPRC: 0.735807390776979
Mean absolute error: 0.3755208333333333
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 24.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.32734701492537316
Kappa statistic: 0.38737887024344125
Training time: 9.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 91.66666666666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7332117096151384
Weighted AreaUnderROC: 0.7620298507462686
Root mean squared error: 0.44438042073580797
Relative absolute error: 69.21874999999996
Root relative squared error: 88.87608414716159
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.4127823732196146
Weighted FMeasure: 0.7324459518488545
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7449742714632909
Mean absolute error: 0.3460937499999998
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3169051616915423
Kappa statistic: 0.412721981350597
Training time: 11.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 92.05729166666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7290167297979798
Weighted AreaUnderROC: 0.77
Root mean squared error: 0.43424383511263975
Relative absolute error: 69.11458333333329
Root relative squared error: 86.84876702252795
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.40224336990133147
Weighted FMeasure: 0.7306720870737461
Iteration time: 14.0
Weighted AreaUnderPRC: 0.7630425845779518
Mean absolute error: 0.3455729166666664
Coverage of cases: 98.95833333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 50.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3432108208955224
Kappa statistic: 0.4008810572687225
Training time: 13.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 91.27604166666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.7435775842962263
Weighted AreaUnderROC: 0.7805671641791044
Root mean squared error: 0.43508260709892793
Relative absolute error: 68.80208333333327
Root relative squared error: 87.01652141978559
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.43559262380478253
Weighted FMeasure: 0.7428364003207436
Iteration time: 15.0
Weighted AreaUnderPRC: 0.7650589701438303
Mean absolute error: 0.3440104166666664
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3043964552238806
Kappa statistic: 0.4355288946962048
Training time: 14.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7295822883416934
Weighted AreaUnderROC: 0.7934328358208956
Root mean squared error: 0.4258875536257586
Relative absolute error: 64.63541666666663
Root relative squared error: 85.17751072515172
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.4046858273548936
Weighted FMeasure: 0.7305489935875716
Iteration time: 18.0
Weighted AreaUnderPRC: 0.7853549472579434
Mean absolute error: 0.3231770833333331
Coverage of cases: 98.69791666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.330755907960199
Kappa statistic: 0.40451671183378507
Training time: 17.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 90.234375
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7348404962760298
Weighted AreaUnderROC: 0.7939402985074627
Root mean squared error: 0.4260098296987993
Relative absolute error: 65.46874999999997
Root relative squared error: 85.20196593975986
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.41625344367946404
Weighted FMeasure: 0.735781051964512
Iteration time: 21.0
Weighted AreaUnderPRC: 0.7796490906672556
Mean absolute error: 0.3273437499999998
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 104.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.32450155472636816
Kappa statistic: 0.41607949412827455
Training time: 20.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 90.49479166666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7539711047254151
Weighted AreaUnderROC: 0.7801641791044777
Root mean squared error: 0.4262542766158872
Relative absolute error: 64.73958333333326
Root relative squared error: 85.25085532317745
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45758283975804454
Weighted FMeasure: 0.7552362138021884
Iteration time: 23.0
Weighted AreaUnderPRC: 0.7668110299993461
Mean absolute error: 0.32369791666666625
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 127.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3098722014925373
Kappa statistic: 0.4566368449881321
Training time: 22.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 26.041666666666668
Correctly Classified Instances: 73.95833333333333
Weighted Precision: 0.7324820553221288
Weighted AreaUnderROC: 0.7903880597014924
Root mean squared error: 0.42600982969879947
Relative absolute error: 64.21874999999996
Root relative squared error: 85.20196593975989
Weighted TruePositiveRate: 0.7395833333333334
Weighted MatthewsCorrelation: 0.40768855589813874
Weighted FMeasure: 0.7334263931719777
Iteration time: 24.0
Weighted AreaUnderPRC: 0.775842126043603
Mean absolute error: 0.3210937499999998
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 151.0
Weighted Recall: 0.7395833333333334
Weighted FalsePositiveRate: 0.35080721393034825
Kappa statistic: 0.40417080436941416
Training time: 23.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 90.88541666666667
Incorrectly Classified Instances: 25.78125
Correctly Classified Instances: 74.21875
Weighted Precision: 0.739344132019976
Weighted AreaUnderROC: 0.7918507462686568
Root mean squared error: 0.42078176251670735
Relative absolute error: 64.53124999999994
Root relative squared error: 84.15635250334147
Weighted TruePositiveRate: 0.7421875
Weighted MatthewsCorrelation: 0.42598750280646897
Weighted FMeasure: 0.7405100149629327
Iteration time: 26.0
Weighted AreaUnderPRC: 0.7732693062651039
Mean absolute error: 0.3226562499999997
Coverage of cases: 98.17708333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 177.0
Weighted Recall: 0.7421875
Weighted FalsePositiveRate: 0.3217098880597015
Kappa statistic: 0.42563606696077844
Training time: 26.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 24.21875
Correctly Classified Instances: 75.78125
Weighted Precision: 0.7518924705286875
Weighted AreaUnderROC: 0.7759402985074626
Root mean squared error: 0.4307515235028194
Relative absolute error: 62.55208333333327
Root relative squared error: 86.15030470056388
Weighted TruePositiveRate: 0.7578125
Weighted MatthewsCorrelation: 0.45040132260950255
Weighted FMeasure: 0.7523980180593223
Iteration time: 28.0
Weighted AreaUnderPRC: 0.7557913983054001
Mean absolute error: 0.3127604166666664
Coverage of cases: 95.83333333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 205.0
Weighted Recall: 0.7578125
Weighted FalsePositiveRate: 0.3271856343283582
Kappa statistic: 0.4468744191809677
Training time: 27.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 88.80208333333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7296470247778751
Weighted AreaUnderROC: 0.7742985074626866
Root mean squared error: 0.4360989471056619
Relative absolute error: 65.36458333333327
Root relative squared error: 87.21978942113238
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.40399732604310423
Weighted FMeasure: 0.73126220703125
Iteration time: 30.0
Weighted AreaUnderPRC: 0.761086630686195
Mean absolute error: 0.32682291666666635
Coverage of cases: 97.39583333333333
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 235.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3397481343283582
Kappa statistic: 0.4029996341909523
Training time: 30.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 88.41145833333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7376831729705818
Weighted AreaUnderROC: 0.7886716417910448
Root mean squared error: 0.42371128141695724
Relative absolute error: 63.229166666666615
Root relative squared error: 84.74225628339144
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.41830812974437254
Weighted FMeasure: 0.738084902807325
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7763646261830089
Mean absolute error: 0.31614583333333307
Coverage of cases: 97.65625
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 266.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3480155472636816
Kappa statistic: 0.413977824841161
Training time: 30.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7146070416786708
Weighted AreaUnderROC: 0.7860149253731343
Root mean squared error: 0.4259486960499663
Relative absolute error: 64.21874999999989
Root relative squared error: 85.18973920999326
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3700182845161861
Weighted FMeasure: 0.7164991709499846
Iteration time: 31.0
Weighted AreaUnderPRC: 0.7706096711928162
Mean absolute error: 0.32109374999999946
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 297.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3640407338308458
Kappa statistic: 0.3681491050009226
Training time: 31.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 90.234375
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7236281971087016
Weighted AreaUnderROC: 0.7747164179104478
Root mean squared error: 0.4296316251550082
Relative absolute error: 64.37499999999986
Root relative squared error: 85.92632503100164
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.3873326610522306
Weighted FMeasure: 0.7243594454242602
Iteration time: 34.0
Weighted AreaUnderPRC: 0.7594163084508616
Mean absolute error: 0.3218749999999993
Coverage of cases: 98.17708333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 331.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.36538277363184085
Kappa statistic: 0.3829641185647427
Training time: 33.0
		
Time end:Tue Oct 31 11.38.32 EET 2017