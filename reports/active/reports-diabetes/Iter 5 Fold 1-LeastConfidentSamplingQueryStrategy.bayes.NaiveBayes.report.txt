Tue Oct 31 11.37.58 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.58 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 76.69270833333333
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6884786853837368
Weighted AreaUnderROC: 0.7129954912935323
Root mean squared error: 0.5147521764893043
Relative absolute error: 70.9697638656533
Root relative squared error: 102.95043529786085
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.3096093492098084
Weighted FMeasure: 0.6730484330484331
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7052027084826804
Mean absolute error: 0.35484881932826656
Coverage of cases: 86.97916666666667
Instances selection time: 7.0
Test time: 4.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.34487562189054727
Kappa statistic: 0.3048987442018327
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 83.72395833333333
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.7037444502781769
Weighted AreaUnderROC: 0.7256469993781094
Root mean squared error: 0.49092818231122864
Relative absolute error: 70.56204281039577
Root relative squared error: 98.18563646224572
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.342695617325736
Weighted FMeasure: 0.6883742877492879
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7167310733874436
Mean absolute error: 0.3528102140519788
Coverage of cases: 92.44791666666667
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.32611256218905477
Kappa statistic: 0.3374816155673718
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 87.5
Incorrectly Classified Instances: 30.46875
Correctly Classified Instances: 69.53125
Weighted Precision: 0.6927748328013376
Weighted AreaUnderROC: 0.7317962531094527
Root mean squared error: 0.4676406471700515
Relative absolute error: 68.86613663001016
Root relative squared error: 93.5281294340103
Weighted TruePositiveRate: 0.6953125
Weighted MatthewsCorrelation: 0.3237125130829006
Weighted FMeasure: 0.6939245849489891
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7218738666510927
Mean absolute error: 0.34433068315005083
Coverage of cases: 94.53125
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6953125
Weighted FalsePositiveRate: 0.37453638059701494
Kappa statistic: 0.3235772357723577
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 90.88541666666667
Incorrectly Classified Instances: 28.90625
Correctly Classified Instances: 71.09375
Weighted Precision: 0.713553901776538
Weighted AreaUnderROC: 0.7440649098258706
Root mean squared error: 0.45651430253025077
Relative absolute error: 69.46371105176371
Root relative squared error: 91.30286050605015
Weighted TruePositiveRate: 0.7109375
Weighted MatthewsCorrelation: 0.3694299791876232
Weighted FMeasure: 0.712124680874681
Iteration time: 2.0
Weighted AreaUnderPRC: 0.730014805892138
Mean absolute error: 0.34731855525881855
Coverage of cases: 95.05208333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7109375
Weighted FalsePositiveRate: 0.33845988805970145
Kappa statistic: 0.36928085232317254
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 92.578125
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7147142999024428
Weighted AreaUnderROC: 0.7553783426616917
Root mean squared error: 0.4469516347460033
Relative absolute error: 68.99379734232188
Root relative squared error: 89.39032694920066
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.37207742426920715
Weighted FMeasure: 0.7153855952336295
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7393114542828011
Mean absolute error: 0.3449689867116094
Coverage of cases: 95.83333333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.34605628109452735
Kappa statistic: 0.3720218448058573
Training time: 1.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 93.61979166666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7318115234375
Weighted AreaUnderROC: 0.7609305814676617
Root mean squared error: 0.439760930647322
Relative absolute error: 69.36908033078227
Root relative squared error: 87.95218612946441
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.40951372692171556
Weighted FMeasure: 0.7329083441450684
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7441634330581768
Mean absolute error: 0.34684540165391137
Coverage of cases: 96.09375
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3293600746268657
Kappa statistic: 0.4092664092664093
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 93.88020833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.73104505044912
Weighted AreaUnderROC: 0.7611343283582089
Root mean squared error: 0.43756451260045304
Relative absolute error: 68.5889785855197
Root relative squared error: 87.5129025200906
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.407632994782559
Weighted FMeasure: 0.7323799969715324
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7474263420736996
Mean absolute error: 0.3429448929275985
Coverage of cases: 96.61458333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3328227611940298
Kappa statistic: 0.40719215401380315
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 93.88020833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7296470247778751
Weighted AreaUnderROC: 0.7638507462686567
Root mean squared error: 0.43429969358310916
Relative absolute error: 68.06984465790045
Root relative squared error: 86.85993871662183
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.40399732604310423
Weighted FMeasure: 0.73126220703125
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7559009374354321
Mean absolute error: 0.34034922328950223
Coverage of cases: 97.13541666666667
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 24.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3397481343283582
Kappa statistic: 0.4029996341909523
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 93.359375
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7308697661576876
Weighted AreaUnderROC: 0.7674029850746269
Root mean squared error: 0.4331206976987118
Relative absolute error: 66.6519774540026
Root relative squared error: 86.62413953974236
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.40562906678541316
Weighted FMeasure: 0.7323964137004527
Iteration time: 2.0
Weighted AreaUnderPRC: 0.756951256378032
Mean absolute error: 0.33325988727001304
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 26.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.34527767412935323
Kappa statistic: 0.4035799963092821
Training time: 1.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 91.53645833333333
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7303233560794045
Weighted AreaUnderROC: 0.759731343283582
Root mean squared error: 0.43982527720056847
Relative absolute error: 66.00867919426446
Root relative squared error: 87.96505544011369
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.4057940424144732
Weighted FMeasure: 0.7318313953488372
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7512816850782578
Mean absolute error: 0.3300433959713223
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 28.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.336285447761194
Kappa statistic: 0.4051032806804374
Training time: 1.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 91.015625
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7224598987819523
Weighted AreaUnderROC: 0.7848059701492537
Root mean squared error: 0.4337133930832901
Relative absolute error: 64.5557674775468
Root relative squared error: 86.74267861665803
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.38696714642294944
Weighted FMeasure: 0.7241222651222651
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7896759072174886
Mean absolute error: 0.322778837387734
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 30.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.3563905472636815
Kappa statistic: 0.3847670692630022
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.10416666666667
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7340631925508339
Weighted AreaUnderROC: 0.8046567164179105
Root mean squared error: 0.4253150926426918
Relative absolute error: 62.34731988632788
Root relative squared error: 85.06301852853836
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.41437461049653823
Weighted FMeasure: 0.735267793042992
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8083684820663285
Mean absolute error: 0.3117365994316394
Coverage of cases: 97.91666666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.32796424129353235
Kappa statistic: 0.4140327551822082
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 90.49479166666667
Incorrectly Classified Instances: 26.822916666666668
Correctly Classified Instances: 73.17708333333333
Weighted Precision: 0.7287822530816918
Weighted AreaUnderROC: 0.8068059701492537
Root mean squared error: 0.42480810423006615
Relative absolute error: 62.104357991460255
Root relative squared error: 84.96162084601323
Weighted TruePositiveRate: 0.7317708333333334
Weighted MatthewsCorrelation: 0.40276171818660744
Weighted FMeasure: 0.7300255711230511
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8112520873013079
Mean absolute error: 0.3105217899573013
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7317708333333334
Weighted FalsePositiveRate: 0.3342185945273632
Kappa statistic: 0.40242944340363823
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7219822719060525
Weighted AreaUnderROC: 0.7977910447761194
Root mean squared error: 0.43937877783300017
Relative absolute error: 61.83260754634047
Root relative squared error: 87.87575556660003
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3873262182198241
Weighted FMeasure: 0.7236537897766643
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8002117966297516
Mean absolute error: 0.30916303773170234
Coverage of cases: 97.13541666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3473983208955224
Kappa statistic: 0.3865254701478911
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 84.24479166666667
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7199513911937162
Weighted AreaUnderROC: 0.7956119402985075
Root mean squared error: 0.4427000726656348
Relative absolute error: 61.279317344534526
Root relative squared error: 88.54001453312696
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3836016806750288
Weighted FMeasure: 0.7206078778898934
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7988665036468764
Mean absolute error: 0.3063965867226726
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.3398019278606965
Kappa statistic: 0.3835443797635479
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 82.94270833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7251884824849893
Weighted AreaUnderROC: 0.7967462686567165
Root mean squared error: 0.4460989086826157
Relative absolute error: 60.75345272156335
Root relative squared error: 89.21978173652315
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.39512593708085025
Weighted FMeasure: 0.725830160546157
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7996567051720489
Mean absolute error: 0.30376726360781675
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.3335475746268657
Kappa statistic: 0.3950669147212386
Training time: 0.0
		
Time end:Tue Oct 31 11.37.59 EET 2017