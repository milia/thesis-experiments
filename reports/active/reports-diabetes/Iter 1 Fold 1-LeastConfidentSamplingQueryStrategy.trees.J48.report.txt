Tue Oct 31 11.38.17 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.17 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 75.26041666666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7428736256277997
Weighted AreaUnderROC: 0.6665373134328358
Root mean squared error: 0.48856023850410474
Relative absolute error: 53.81193693693699
Root relative squared error: 97.71204770082095
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.4278886529255667
Weighted FMeasure: 0.7420454545454546
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6700470089972189
Mean absolute error: 0.26905968468468494
Coverage of cases: 83.33333333333333
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 7.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.34868656716417906
Kappa statistic: 0.4217593173547496
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 91.92708333333333
Incorrectly Classified Instances: 27.604166666666668
Correctly Classified Instances: 72.39583333333333
Weighted Precision: 0.7138775849713349
Weighted AreaUnderROC: 0.638
Root mean squared error: 0.49370869775295095
Relative absolute error: 63.93939393939395
Root relative squared error: 98.7417395505902
Weighted TruePositiveRate: 0.7239583333333334
Weighted MatthewsCorrelation: 0.35477392071537434
Weighted FMeasure: 0.7069875507375506
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6374529669677415
Mean absolute error: 0.31969696969696976
Coverage of cases: 92.96875
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.7239583333333334
Weighted FalsePositiveRate: 0.4111225124378109
Kappa statistic: 0.3399065905552673
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 95.18229166666667
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7320218059173178
Weighted AreaUnderROC: 0.6949701492537314
Root mean squared error: 0.46505871996865583
Relative absolute error: 63.417994896251784
Root relative squared error: 93.01174399373117
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.361779562322941
Weighted FMeasure: 0.6964672232529375
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6877247432913638
Mean absolute error: 0.3170899744812589
Coverage of cases: 98.4375
Instances selection time: 1.0
Test time: 6.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.4498830845771144
Kappa statistic: 0.3190560632928659
Training time: 1.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 91.796875
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7230755649717514
Weighted AreaUnderROC: 0.6302388059701493
Root mean squared error: 0.47714760028871345
Relative absolute error: 66.99412128712872
Root relative squared error: 95.42952005774269
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3395558772527407
Weighted FMeasure: 0.6853856912217466
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6404671533030267
Mean absolute error: 0.33497060643564364
Coverage of cases: 94.79166666666667
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 15.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.46445864427860695
Kappa statistic: 0.29523156089193814
Training time: 2.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 65.234375
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7230755649717514
Weighted AreaUnderROC: 0.643223880597015
Root mean squared error: 0.49486037206482414
Relative absolute error: 60.36011904761905
Root relative squared error: 98.97207441296483
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3395558772527407
Weighted FMeasure: 0.6853856912217466
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6595506829288443
Mean absolute error: 0.30180059523809527
Coverage of cases: 83.85416666666667
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.46445864427860695
Kappa statistic: 0.29523156089193814
Training time: 2.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 89.32291666666667
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7345669676098288
Weighted AreaUnderROC: 0.6527014925373135
Root mean squared error: 0.487130513013887
Relative absolute error: 60.17893602540815
Root relative squared error: 97.4261026027774
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3759972533108224
Weighted FMeasure: 0.7064677867963475
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6664689380812313
Mean absolute error: 0.30089468012704074
Coverage of cases: 92.44791666666667
Instances selection time: 2.0
Test time: 5.0
Accumulative iteration time: 23.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.4332406716417911
Kappa statistic: 0.33998382313291986
Training time: 2.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.719460557764711
Weighted AreaUnderROC: 0.5855223880597015
Root mean squared error: 0.5010293364610545
Relative absolute error: 61.467313218390615
Root relative squared error: 100.20586729221091
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3565637124368342
Weighted FMeasure: 0.7036199660503545
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6211163339654561
Mean absolute error: 0.3073365660919531
Coverage of cases: 89.0625
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.4270401119402985
Kappa statistic: 0.3323176790090746
Training time: 3.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 86.06770833333333
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.719460557764711
Weighted AreaUnderROC: 0.5855223880597015
Root mean squared error: 0.5032748382806971
Relative absolute error: 60.47028186274501
Root relative squared error: 100.65496765613942
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3565637124368342
Weighted FMeasure: 0.7036199660503545
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6211163339654561
Mean absolute error: 0.30235140931372506
Coverage of cases: 89.0625
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.4270401119402985
Kappa statistic: 0.3323176790090746
Training time: 7.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 54.296875
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7195457175925926
Weighted AreaUnderROC: 0.655955223880597
Root mean squared error: 0.5043479539744296
Relative absolute error: 56.591319444444466
Root relative squared error: 100.86959079488591
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.372219375747142
Weighted FMeasure: 0.7163582242336082
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6679379748870719
Mean absolute error: 0.28295659722222233
Coverage of cases: 77.34375
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.39101741293532344
Kappa statistic: 0.36196319018404893
Training time: 8.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 92.83854166666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7441602573865366
Weighted AreaUnderROC: 0.6715970149253732
Root mean squared error: 0.463113434962555
Relative absolute error: 62.224311985596934
Root relative squared error: 92.622686992511
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.35863539445628995
Weighted FMeasure: 0.6829243729858484
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6789085366170898
Mean absolute error: 0.3111215599279847
Coverage of cases: 95.83333333333333
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 53.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.47551772388059704
Kappa statistic: 0.29436471823591176
Training time: 4.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 65.75520833333333
Incorrectly Classified Instances: 28.125
Correctly Classified Instances: 71.875
Weighted Precision: 0.7140925767105012
Weighted AreaUnderROC: 0.696089552238806
Root mean squared error: 0.47276279446245084
Relative absolute error: 58.397540788129476
Root relative squared error: 94.55255889249017
Weighted TruePositiveRate: 0.71875
Weighted MatthewsCorrelation: 0.3326294164162586
Weighted FMeasure: 0.6877728873239436
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6866377268990248
Mean absolute error: 0.2919877039406474
Coverage of cases: 84.89583333333333
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 62.0
Weighted Recall: 0.71875
Weighted FalsePositiveRate: 0.45546641791044773
Kappa statistic: 0.2984165651644336
Training time: 8.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 62.109375
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7254709507042253
Weighted AreaUnderROC: 0.642179104477612
Root mean squared error: 0.49027113409301826
Relative absolute error: 56.98604789137877
Root relative squared error: 98.05422681860365
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3872293089809524
Weighted FMeasure: 0.7235336838567176
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6566078189798513
Mean absolute error: 0.28493023945689383
Coverage of cases: 80.98958333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 78.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3778376865671642
Kappa statistic: 0.3788378584115707
Training time: 14.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 62.109375
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7254709507042253
Weighted AreaUnderROC: 0.642179104477612
Root mean squared error: 0.49096038023219674
Relative absolute error: 56.661667567166695
Root relative squared error: 98.19207604643935
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3872293089809524
Weighted FMeasure: 0.7235336838567176
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6566078189798513
Mean absolute error: 0.2833083378358335
Coverage of cases: 80.98958333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 84.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3778376865671642
Kappa statistic: 0.3788378584115707
Training time: 6.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 62.109375
Incorrectly Classified Instances: 26.5625
Correctly Classified Instances: 73.4375
Weighted Precision: 0.7254709507042253
Weighted AreaUnderROC: 0.642179104477612
Root mean squared error: 0.49148715703372753
Relative absolute error: 56.42738819140168
Root relative squared error: 98.2974314067455
Weighted TruePositiveRate: 0.734375
Weighted MatthewsCorrelation: 0.3872293089809524
Weighted FMeasure: 0.7235336838567176
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6566078189798513
Mean absolute error: 0.2821369409570084
Coverage of cases: 80.98958333333333
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 91.0
Weighted Recall: 0.734375
Weighted FalsePositiveRate: 0.3778376865671642
Kappa statistic: 0.3788378584115707
Training time: 6.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 77.47395833333333
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7457718633705476
Weighted AreaUnderROC: 0.7360298507462687
Root mean squared error: 0.45445204446446164
Relative absolute error: 56.851328037141215
Root relative squared error: 90.89040889289232
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.43032582830127447
Weighted FMeasure: 0.7421153216210072
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7150781600422992
Mean absolute error: 0.28425664018570607
Coverage of cases: 91.14583333333333
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 98.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.3576787935323383
Kappa statistic: 0.4204003813155386
Training time: 7.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 95.57291666666667
Incorrectly Classified Instances: 27.34375
Correctly Classified Instances: 72.65625
Weighted Precision: 0.7182451462738867
Weighted AreaUnderROC: 0.6652537313432836
Root mean squared error: 0.4723784502282945
Relative absolute error: 67.39660710431718
Root relative squared error: 94.4756900456589
Weighted TruePositiveRate: 0.7265625
Weighted MatthewsCorrelation: 0.3580263843923139
Weighted FMeasure: 0.705962982042979
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6697204269041478
Mean absolute error: 0.3369830355215859
Coverage of cases: 95.57291666666667
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 104.0
Weighted Recall: 0.7265625
Weighted FalsePositiveRate: 0.4201147388059702
Kappa statistic: 0.33740879510944594
Training time: 6.0
		
Time end:Tue Oct 31 11.38.18 EET 2017