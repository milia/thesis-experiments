Tue Oct 31 11.38.38 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.38.38 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 88.54166666666667
Incorrectly Classified Instances: 28.385416666666668
Correctly Classified Instances: 71.61458333333333
Weighted Precision: 0.7046295911391938
Weighted AreaUnderROC: 0.7244179104477612
Root mean squared error: 0.44266719475837657
Relative absolute error: 64.95964421574405
Root relative squared error: 88.53343895167531
Weighted TruePositiveRate: 0.7161458333333334
Weighted MatthewsCorrelation: 0.33855891714577074
Weighted FMeasure: 0.7012674528596361
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7093598110656038
Mean absolute error: 0.32479822107872025
Coverage of cases: 94.53125
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 8.0
Weighted Recall: 0.7161458333333334
Weighted FalsePositiveRate: 0.4118473258706467
Kappa statistic: 0.3275496433391171
Training time: 0.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 90.625
Incorrectly Classified Instances: 27.864583333333332
Correctly Classified Instances: 72.13541666666667
Weighted Precision: 0.7140110648671946
Weighted AreaUnderROC: 0.722865671641791
Root mean squared error: 0.44452871983337305
Relative absolute error: 68.0759604838725
Root relative squared error: 88.9057439666746
Weighted TruePositiveRate: 0.7213541666666666
Weighted MatthewsCorrelation: 0.3682285512030286
Weighted FMeasure: 0.7158235543329386
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7066274351348246
Mean absolute error: 0.3403798024193625
Coverage of cases: 94.27083333333333
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 11.0
Weighted Recall: 0.7213541666666666
Weighted FalsePositiveRate: 0.36750342039800993
Kappa statistic: 0.36588678313476125
Training time: 1.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 90.88541666666667
Incorrectly Classified Instances: 27.083333333333332
Correctly Classified Instances: 72.91666666666667
Weighted Precision: 0.7196889671361503
Weighted AreaUnderROC: 0.7270149253731342
Root mean squared error: 0.4382167215479943
Relative absolute error: 67.66278306310834
Root relative squared error: 87.64334430959886
Weighted TruePositiveRate: 0.7291666666666666
Weighted MatthewsCorrelation: 0.374779873729053
Weighted FMeasure: 0.7181127756970452
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7123681768588823
Mean absolute error: 0.33831391531554167
Coverage of cases: 94.53125
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.7291666666666666
Weighted FalsePositiveRate: 0.38409203980099504
Kappa statistic: 0.3666582085765034
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 91.53645833333333
Incorrectly Classified Instances: 25.520833333333332
Correctly Classified Instances: 74.47916666666667
Weighted Precision: 0.7380881519274376
Weighted AreaUnderROC: 0.753044776119403
Root mean squared error: 0.4289619576000339
Relative absolute error: 64.77145411501044
Root relative squared error: 85.79239152000679
Weighted TruePositiveRate: 0.7447916666666666
Weighted MatthewsCorrelation: 0.40748823864745304
Weighted FMeasure: 0.730047487745098
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7423147072808464
Mean absolute error: 0.3238572705750522
Coverage of cases: 96.09375
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7447916666666666
Weighted FalsePositiveRate: 0.3826424129353234
Kappa statistic: 0.3920124079100426
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 91.015625
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7433969185619956
Weighted AreaUnderROC: 0.7577014925373134
Root mean squared error: 0.4255216750904726
Relative absolute error: 64.50576618458817
Root relative squared error: 85.10433501809452
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42184871014324016
Weighted FMeasure: 0.7373294346978557
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7465538119439663
Mean absolute error: 0.3225288309229408
Coverage of cases: 96.09375
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 19.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.3694626865671642
Kappa statistic: 0.40885182809493265
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 90.88541666666667
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7427687387011543
Weighted AreaUnderROC: 0.7587761194029851
Root mean squared error: 0.42479824225400936
Relative absolute error: 65.04767785472424
Root relative squared error: 84.95964845080188
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.42561950418300376
Weighted FMeasure: 0.7405696444501083
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7457384103067765
Mean absolute error: 0.3252383892736212
Coverage of cases: 96.35416666666667
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.35561194029850746
Kappa statistic: 0.41751990898748575
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 92.578125
Incorrectly Classified Instances: 25.260416666666668
Correctly Classified Instances: 74.73958333333333
Weighted Precision: 0.7401342333299593
Weighted AreaUnderROC: 0.7608059701492538
Root mean squared error: 0.42529276761975815
Relative absolute error: 65.83003856377469
Root relative squared error: 85.05855352395163
Weighted TruePositiveRate: 0.7473958333333334
Weighted MatthewsCorrelation: 0.4223999699031738
Weighted FMeasure: 0.7397166921245811
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7546453100519349
Mean absolute error: 0.3291501928188735
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 23.0
Weighted Recall: 0.7473958333333334
Weighted FalsePositiveRate: 0.35008240049751244
Kappa statistic: 0.41679714410972646
Training time: 1.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 92.96875
Incorrectly Classified Instances: 26.302083333333332
Correctly Classified Instances: 73.69791666666667
Weighted Precision: 0.7285082244987274
Weighted AreaUnderROC: 0.761402985074627
Root mean squared error: 0.4243356785815467
Relative absolute error: 66.17073686208641
Root relative squared error: 84.86713571630933
Weighted TruePositiveRate: 0.7369791666666666
Weighted MatthewsCorrelation: 0.39533373640860797
Weighted FMeasure: 0.7274550148195825
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7527695108168805
Mean absolute error: 0.3308536843104321
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 25.0
Weighted Recall: 0.7369791666666666
Weighted FalsePositiveRate: 0.3695164800995025
Kappa statistic: 0.388303577061384
Training time: 1.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 93.48958333333333
Incorrectly Classified Instances: 23.4375
Correctly Classified Instances: 76.5625
Weighted Precision: 0.7598300137362637
Weighted AreaUnderROC: 0.7706567164179106
Root mean squared error: 0.4199519328922986
Relative absolute error: 65.58604889730599
Root relative squared error: 83.99038657845972
Weighted TruePositiveRate: 0.765625
Weighted MatthewsCorrelation: 0.46360752122663396
Weighted FMeasure: 0.7574867211035358
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7610326984017833
Mean absolute error: 0.3279302444865299
Coverage of cases: 96.61458333333333
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.765625
Weighted FalsePositiveRate: 0.33338619402985076
Kappa statistic: 0.4559193954659949
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 94.27083333333333
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7627371399532414
Weighted AreaUnderROC: 0.7693731343283582
Root mean squared error: 0.4197170164893983
Relative absolute error: 65.70860368866761
Root relative squared error: 83.94340329787966
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.4693263382514538
Weighted FMeasure: 0.7598365972172559
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7574096726173011
Mean absolute error: 0.32854301844333805
Coverage of cases: 96.61458333333333
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3319903606965174
Kappa statistic: 0.46098037978676415
Training time: 1.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 92.96875
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7571730063907451
Weighted AreaUnderROC: 0.7681194029850746
Root mean squared error: 0.4213585126514528
Relative absolute error: 64.56436558201233
Root relative squared error: 84.27170253029055
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45611442241854205
Weighted FMeasure: 0.7537179986161564
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7569971945121826
Mean absolute error: 0.32282182791006164
Coverage of cases: 96.35416666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 31.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.34170740049751247
Kappa statistic: 0.4468435382764517
Training time: 1.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 90.36458333333333
Incorrectly Classified Instances: 23.958333333333332
Correctly Classified Instances: 76.04166666666667
Weighted Precision: 0.754380868544601
Weighted AreaUnderROC: 0.7717313432835821
Root mean squared error: 0.421509399456461
Relative absolute error: 62.36383258666008
Root relative squared error: 84.3018798912922
Weighted TruePositiveRate: 0.7604166666666666
Weighted MatthewsCorrelation: 0.4494764852404491
Weighted FMeasure: 0.7506382246550786
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7647228610987039
Mean absolute error: 0.3118191629333004
Coverage of cases: 96.09375
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 32.0
Weighted Recall: 0.7604166666666666
Weighted FalsePositiveRate: 0.34656592039801
Kappa statistic: 0.43973610758690684
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 87.76041666666667
Incorrectly Classified Instances: 24.739583333333332
Correctly Classified Instances: 75.26041666666667
Weighted Precision: 0.7456226822259845
Weighted AreaUnderROC: 0.7669850746268657
Root mean squared error: 0.42446758455223577
Relative absolute error: 61.15066188508226
Root relative squared error: 84.89351691044715
Weighted TruePositiveRate: 0.7526041666666666
Weighted MatthewsCorrelation: 0.4323300373300309
Weighted FMeasure: 0.7436458060184193
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7572345296464537
Mean absolute error: 0.3057533094254113
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7526041666666666
Weighted FalsePositiveRate: 0.35075342039801
Kappa statistic: 0.4246419784240741
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 84.63541666666667
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333333
Weighted Precision: 0.7569652457757297
Weighted AreaUnderROC: 0.7717910447761195
Root mean squared error: 0.42676208692516854
Relative absolute error: 59.4693254587021
Root relative squared error: 85.3524173850337
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.4579432603644094
Weighted FMeasure: 0.7551390844281333
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7631819718961909
Mean absolute error: 0.2973466272935105
Coverage of cases: 95.57291666666667
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 34.0
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3347820273631841
Kappa statistic: 0.45087686215349804
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 85.28645833333333
Incorrectly Classified Instances: 23.177083333333332
Correctly Classified Instances: 76.82291666666667
Weighted Precision: 0.7628734597633587
Weighted AreaUnderROC: 0.8272537313432835
Root mean squared error: 0.4071598395481515
Relative absolute error: 55.81804800775453
Root relative squared error: 81.4319679096303
Weighted TruePositiveRate: 0.7682291666666666
Weighted MatthewsCorrelation: 0.47437943267217403
Weighted FMeasure: 0.763047565669674
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8241744450000624
Mean absolute error: 0.27909024003877264
Coverage of cases: 98.4375
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7682291666666666
Weighted FalsePositiveRate: 0.3146769278606965
Kappa statistic: 0.4706647667430765
Training time: 1.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 85.15625
Incorrectly Classified Instances: 22.916666666666668
Correctly Classified Instances: 77.08333333333333
Weighted Precision: 0.7655374649859944
Weighted AreaUnderROC: 0.8285970149253732
Root mean squared error: 0.406723697362169
Relative absolute error: 55.90044903738405
Root relative squared error: 81.3447394724338
Weighted TruePositiveRate: 0.7708333333333334
Weighted MatthewsCorrelation: 0.47981036431255153
Weighted FMeasure: 0.7654152259913406
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8261507265229011
Mean absolute error: 0.27950224518692024
Coverage of cases: 98.17708333333333
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.7708333333333334
Weighted FalsePositiveRate: 0.3132810945273632
Kappa statistic: 0.47567030784508446
Training time: 1.0
		
Time end:Tue Oct 31 11.38.39 EET 2017