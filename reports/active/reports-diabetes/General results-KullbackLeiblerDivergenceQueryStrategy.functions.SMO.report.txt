Tue Oct 31 11.37.14 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.37.14 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.760416666666668
Correctly Classified Instances: 72.23958333333333
Weighted Precision: 0.7379487258347857
Weighted AreaUnderROC: 0.6463880597014925
Root mean squared error: 0.5255845776519031
Relative absolute error: 55.52083333333333
Root relative squared error: 105.11691553038062
Weighted TruePositiveRate: 0.7223958333333333
Weighted MatthewsCorrelation: 0.3588373346350616
Weighted FMeasure: 0.6905890690298909
Iteration time: 19.4
Weighted AreaUnderPRC: 0.6432450302619955
Mean absolute error: 0.27760416666666665
Coverage of cases: 72.23958333333333
Instances selection time: 2.1
Test time: 2.0
Accumulative iteration time: 19.4
Weighted Recall: 0.7223958333333333
Weighted FalsePositiveRate: 0.4296197139303482
Kappa statistic: 0.3181794725145709
Training time: 17.3
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.921875000000007
Correctly Classified Instances: 70.07812500000001
Weighted Precision: 0.63858312927684
Weighted AreaUnderROC: 0.6034716417910448
Root mean squared error: 0.5455770804340783
Relative absolute error: 59.843750000000014
Root relative squared error: 109.11541608681564
Weighted TruePositiveRate: 0.7007812499999999
Weighted MatthewsCorrelation: 0.250316310001558
Weighted FMeasure: 0.6386199237814698
Iteration time: 15.5
Weighted AreaUnderPRC: 0.6140324516401847
Mean absolute error: 0.29921875
Coverage of cases: 70.07812500000001
Instances selection time: 1.8
Test time: 2.8
Accumulative iteration time: 34.9
Weighted Recall: 0.7007812499999999
Weighted FalsePositiveRate: 0.49383796641791033
Kappa statistic: 0.22480424642862823
Training time: 13.7
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.489583333333336
Correctly Classified Instances: 71.51041666666667
Weighted Precision: 0.6509665110370291
Weighted AreaUnderROC: 0.6355940298507464
Root mean squared error: 0.5317600792183883
Relative absolute error: 56.979166666666664
Root relative squared error: 106.35201584367765
Weighted TruePositiveRate: 0.7151041666666667
Weighted MatthewsCorrelation: 0.2967354890971727
Weighted FMeasure: 0.6647044823272139
Iteration time: 14.8
Weighted AreaUnderPRC: 0.6365106235312283
Mean absolute error: 0.28489583333333335
Coverage of cases: 71.51041666666667
Instances selection time: 1.7
Test time: 3.0
Accumulative iteration time: 49.7
Weighted Recall: 0.7151041666666667
Weighted FalsePositiveRate: 0.443916106965174
Kappa statistic: 0.28266347018051957
Training time: 13.1
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 28.359375
Correctly Classified Instances: 71.640625
Weighted Precision: 0.6554948854544285
Weighted AreaUnderROC: 0.6523492537313433
Root mean squared error: 0.5305095707716857
Relative absolute error: 56.718749999999986
Root relative squared error: 106.10191415433712
Weighted TruePositiveRate: 0.71640625
Weighted MatthewsCorrelation: 0.31362734077191073
Weighted FMeasure: 0.6725425215692313
Iteration time: 16.9
Weighted AreaUnderPRC: 0.6470980158575577
Mean absolute error: 0.28359375
Coverage of cases: 71.640625
Instances selection time: 2.5
Test time: 2.1
Accumulative iteration time: 66.6
Weighted Recall: 0.71640625
Weighted FalsePositiveRate: 0.4117077425373134
Kappa statistic: 0.30540251761707526
Training time: 14.4
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.583333333333336
Correctly Classified Instances: 70.41666666666666
Weighted Precision: 0.654993843342256
Weighted AreaUnderROC: 0.6516059701492537
Root mean squared error: 0.5423390304114042
Relative absolute error: 59.166666666666664
Root relative squared error: 108.46780608228084
Weighted TruePositiveRate: 0.7041666666666667
Weighted MatthewsCorrelation: 0.30695686316996545
Weighted FMeasure: 0.6620369990060009
Iteration time: 15.5
Weighted AreaUnderPRC: 0.6453597053010236
Mean absolute error: 0.29583333333333334
Coverage of cases: 70.41666666666666
Instances selection time: 1.6
Test time: 2.5
Accumulative iteration time: 82.1
Weighted Recall: 0.7041666666666667
Weighted FalsePositiveRate: 0.40095472636815915
Kappa statistic: 0.29372158241350554
Training time: 13.9
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.520833333333332
Correctly Classified Instances: 69.47916666666667
Weighted Precision: 0.6491664127256802
Weighted AreaUnderROC: 0.6449253731343283
Root mean squared error: 0.550532848862465
Relative absolute error: 61.041666666666664
Root relative squared error: 110.10656977249303
Weighted TruePositiveRate: 0.6947916666666666
Weighted MatthewsCorrelation: 0.29306137282832634
Weighted FMeasure: 0.6526828654940385
Iteration time: 15.2
Weighted AreaUnderPRC: 0.639686996748358
Mean absolute error: 0.3052083333333333
Coverage of cases: 69.47916666666667
Instances selection time: 1.9
Test time: 2.3
Accumulative iteration time: 97.3
Weighted Recall: 0.6947916666666666
Weighted FalsePositiveRate: 0.40494092039800994
Kappa statistic: 0.27983107339493385
Training time: 13.3
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 29.270833333333332
Correctly Classified Instances: 70.72916666666666
Weighted Precision: 0.6540980974404597
Weighted AreaUnderROC: 0.6495044776119403
Root mean squared error: 0.5389772278607998
Relative absolute error: 58.541666666666664
Root relative squared error: 107.79544557215993
Weighted TruePositiveRate: 0.7072916666666667
Weighted MatthewsCorrelation: 0.30662476563282703
Weighted FMeasure: 0.6641952176727501
Iteration time: 14.7
Weighted AreaUnderPRC: 0.6443350816475515
Mean absolute error: 0.29270833333333335
Coverage of cases: 70.72916666666666
Instances selection time: 1.3
Test time: 2.7
Accumulative iteration time: 112.0
Weighted Recall: 0.7072916666666667
Weighted FalsePositiveRate: 0.4082827114427861
Kappa statistic: 0.2953793065538625
Training time: 13.4
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.786458333333332
Correctly Classified Instances: 72.21354166666667
Weighted Precision: 0.6561872108228068
Weighted AreaUnderROC: 0.6508626865671642
Root mean squared error: 0.5251617811542161
Relative absolute error: 55.572916666666664
Root relative squared error: 105.03235623084319
Weighted TruePositiveRate: 0.7221354166666666
Weighted MatthewsCorrelation: 0.31609802718149277
Weighted FMeasure: 0.6769478773945152
Iteration time: 14.6
Weighted AreaUnderPRC: 0.6468773829366452
Mean absolute error: 0.2778645833333333
Coverage of cases: 72.21354166666667
Instances selection time: 1.4
Test time: 2.1
Accumulative iteration time: 126.6
Weighted Recall: 0.7221354166666666
Weighted FalsePositiveRate: 0.4204100435323383
Kappa statistic: 0.3088220909617053
Training time: 13.2
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.630208333333332
Correctly Classified Instances: 72.36979166666667
Weighted Precision: 0.6609537752578711
Weighted AreaUnderROC: 0.6563910447761193
Root mean squared error: 0.5234864405777258
Relative absolute error: 55.260416666666664
Root relative squared error: 104.69728811554515
Weighted TruePositiveRate: 0.7236979166666666
Weighted MatthewsCorrelation: 0.3249079814805701
Weighted FMeasure: 0.6789182723049189
Iteration time: 15.1
Weighted AreaUnderPRC: 0.6513235856026789
Mean absolute error: 0.2763020833333333
Coverage of cases: 72.36979166666667
Instances selection time: 1.4
Test time: 2.5
Accumulative iteration time: 141.7
Weighted Recall: 0.7236979166666666
Weighted FalsePositiveRate: 0.41091582711442787
Kappa statistic: 0.3160528797515682
Training time: 13.7
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.734375
Correctly Classified Instances: 72.26562500000001
Weighted Precision: 0.6547057611207902
Weighted AreaUnderROC: 0.6471074626865672
Root mean squared error: 0.5246048764726738
Relative absolute error: 55.46875
Root relative squared error: 104.92097529453478
Weighted TruePositiveRate: 0.72265625
Weighted MatthewsCorrelation: 0.31205746372214904
Weighted FMeasure: 0.6758210529875713
Iteration time: 15.5
Weighted AreaUnderPRC: 0.6445073022656385
Mean absolute error: 0.27734374999999994
Coverage of cases: 72.26562500000001
Instances selection time: 1.7
Test time: 1.9
Accumulative iteration time: 157.2
Weighted Recall: 0.72265625
Weighted FalsePositiveRate: 0.4284413246268657
Kappa statistic: 0.30416627660776896
Training time: 13.8
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.395833333333332
Correctly Classified Instances: 72.60416666666666
Weighted Precision: 0.6556303470467221
Weighted AreaUnderROC: 0.6441671641791045
Root mean squared error: 0.521085831057438
Relative absolute error: 54.791666666666664
Root relative squared error: 104.2171662114876
Weighted TruePositiveRate: 0.7260416666666668
Weighted MatthewsCorrelation: 0.3133318420786249
Weighted FMeasure: 0.6774625556938162
Iteration time: 15.6
Weighted AreaUnderPRC: 0.6434632330601172
Mean absolute error: 0.2739583333333334
Coverage of cases: 72.60416666666666
Instances selection time: 1.1
Test time: 2.5
Accumulative iteration time: 172.8
Weighted Recall: 0.7260416666666668
Weighted FalsePositiveRate: 0.43770733830845765
Kappa statistic: 0.30469467158726404
Training time: 14.5
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.135416666666668
Correctly Classified Instances: 72.86458333333334
Weighted Precision: 0.658754788680474
Weighted AreaUnderROC: 0.6437432835820895
Root mean squared error: 0.5183699471434429
Relative absolute error: 54.27083333333333
Root relative squared error: 103.67398942868856
Weighted TruePositiveRate: 0.7286458333333333
Weighted MatthewsCorrelation: 0.3178564045000761
Weighted FMeasure: 0.6788739353965104
Iteration time: 15.6
Weighted AreaUnderPRC: 0.644249448937204
Mean absolute error: 0.2713541666666667
Coverage of cases: 72.86458333333334
Instances selection time: 1.6
Test time: 1.9
Accumulative iteration time: 188.4
Weighted Recall: 0.7286458333333333
Weighted FalsePositiveRate: 0.44115926616915424
Kappa statistic: 0.3070267020464388
Training time: 14.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 27.109375
Correctly Classified Instances: 72.890625
Weighted Precision: 0.7122124803425883
Weighted AreaUnderROC: 0.6429044776119404
Root mean squared error: 0.5184504953254023
Relative absolute error: 54.21875
Root relative squared error: 103.69009906508047
Weighted TruePositiveRate: 0.72890625
Weighted MatthewsCorrelation: 0.3365310073418476
Weighted FMeasure: 0.6818740445506221
Iteration time: 15.4
Weighted AreaUnderPRC: 0.643480966323328
Mean absolute error: 0.27109374999999997
Coverage of cases: 72.890625
Instances selection time: 1.3
Test time: 2.2
Accumulative iteration time: 203.8
Weighted Recall: 0.72890625
Weighted FalsePositiveRate: 0.44309729477611925
Kappa statistic: 0.30897623456789525
Training time: 14.1
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 24.869791666666664
Correctly Classified Instances: 75.13020833333334
Weighted Precision: 0.7560372498511588
Weighted AreaUnderROC: 0.6782835820895522
Root mean squared error: 0.49797607620383555
Relative absolute error: 49.73958333333333
Root relative squared error: 99.59521524076712
Weighted TruePositiveRate: 0.7513020833333333
Weighted MatthewsCorrelation: 0.42348796594102167
Weighted FMeasure: 0.7277731545267111
Iteration time: 19.4
Weighted AreaUnderPRC: 0.6684688339225291
Mean absolute error: 0.24869791666666669
Coverage of cases: 75.13020833333334
Instances selection time: 1.3
Test time: 2.6
Accumulative iteration time: 223.2
Weighted Recall: 0.7513020833333333
Weighted FalsePositiveRate: 0.39473491915422887
Kappa statistic: 0.39122508708174747
Training time: 18.1
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.697916666666668
Correctly Classified Instances: 76.30208333333334
Weighted Precision: 0.7626609600695391
Weighted AreaUnderROC: 0.6992298507462686
Root mean squared error: 0.486513887202887
Relative absolute error: 47.39583333333333
Root relative squared error: 97.30277744057743
Weighted TruePositiveRate: 0.7630208333333334
Weighted MatthewsCorrelation: 0.45454455927009274
Weighted FMeasure: 0.7478960433543322
Iteration time: 20.3
Weighted AreaUnderPRC: 0.6834453922446462
Mean absolute error: 0.23697916666666666
Coverage of cases: 76.30208333333334
Instances selection time: 1.2
Test time: 3.1
Accumulative iteration time: 243.5
Weighted Recall: 0.7630208333333334
Weighted FalsePositiveRate: 0.3645611318407961
Kappa statistic: 0.43294329205952725
Training time: 19.1
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 23.046874999999996
Correctly Classified Instances: 76.95312500000001
Weighted Precision: 0.7671744560913937
Weighted AreaUnderROC: 0.7111552238805969
Root mean squared error: 0.47957493812468177
Relative absolute error: 46.09374999999999
Root relative squared error: 95.91498762493636
Weighted TruePositiveRate: 0.7695312499999999
Weighted MatthewsCorrelation: 0.47078579347098176
Weighted FMeasure: 0.7575641526780815
Iteration time: 22.8
Weighted AreaUnderPRC: 0.6924923643823067
Mean absolute error: 0.23046874999999994
Coverage of cases: 76.95312500000001
Instances selection time: 1.7
Test time: 2.7
Accumulative iteration time: 266.3
Weighted Recall: 0.7695312499999999
Weighted FalsePositiveRate: 0.34722080223880597
Kappa statistic: 0.4546353797012287
Training time: 21.1
		
Time end:Tue Oct 31 11.37.24 EET 2017