Tue Oct 31 11.39.29 EET 2017
Dataset: pima_diabetes
Test set size: 384
Initial Labelled set size: 76
Initial Unlabelled set size: 308
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.39.29 EET 2017
		
Iteration: 1
Labeled set size: 76
Unlabelled set size: 308
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.072916666666664
Correctly Classified Instances: 66.92708333333333
Weighted Precision: 0.693455145406174
Weighted AreaUnderROC: 0.6663582089552239
Root mean squared error: 0.5678144918658858
Relative absolute error: 67.0138888888888
Root relative squared error: 113.56289837317716
Weighted TruePositiveRate: 0.6692708333333334
Weighted MatthewsCorrelation: 0.3194729212420105
Weighted FMeasure: 0.6759255562653038
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6444031354311204
Mean absolute error: 0.335069444444444
Coverage of cases: 66.92708333333333
Instances selection time: 1.0
Test time: 18.0
Accumulative iteration time: 2.0
Weighted Recall: 0.6692708333333334
Weighted FalsePositiveRate: 0.3365544154228856
Kappa statistic: 0.3137067267098227
Training time: 1.0
		
Iteration: 2
Labeled set size: 96
Unlabelled set size: 288
	
Mean region size: 50.0
Incorrectly Classified Instances: 35.416666666666664
Correctly Classified Instances: 64.58333333333333
Weighted Precision: 0.659079145492189
Weighted AreaUnderROC: 0.6275820895522388
Root mean squared error: 0.5891034703616768
Relative absolute error: 71.42857142857136
Root relative squared error: 117.82069407233536
Weighted TruePositiveRate: 0.6458333333333334
Weighted MatthewsCorrelation: 0.2481520295806798
Weighted FMeasure: 0.6507523148148148
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6175870150946577
Mean absolute error: 0.35714285714285676
Coverage of cases: 64.58333333333333
Instances selection time: 1.0
Test time: 23.0
Accumulative iteration time: 3.0
Weighted Recall: 0.6458333333333334
Weighted FalsePositiveRate: 0.3906691542288557
Kappa statistic: 0.24662435083669948
Training time: 0.0
		
Iteration: 3
Labeled set size: 116
Unlabelled set size: 268
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.71875
Correctly Classified Instances: 63.28125
Weighted Precision: 0.6485280345969303
Weighted AreaUnderROC: 0.6158507462686568
Root mean squared error: 0.6008625479207574
Relative absolute error: 73.8877118644067
Root relative squared error: 120.17250958415148
Weighted TruePositiveRate: 0.6328125
Weighted MatthewsCorrelation: 0.2246407187934635
Weighted FMeasure: 0.6384708173939355
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6097312124300909
Mean absolute error: 0.3694385593220335
Coverage of cases: 63.28125
Instances selection time: 2.0
Test time: 24.0
Accumulative iteration time: 5.0
Weighted Recall: 0.6328125
Weighted FalsePositiveRate: 0.40111100746268663
Kappa statistic: 0.22282827122925875
Training time: 0.0
		
Iteration: 4
Labeled set size: 136
Unlabelled set size: 248
	
Mean region size: 50.0
Incorrectly Classified Instances: 36.458333333333336
Correctly Classified Instances: 63.541666666666664
Weighted Precision: 0.6460434472934473
Weighted AreaUnderROC: 0.6126567164179103
Root mean squared error: 0.5994597792055012
Relative absolute error: 73.30917874396134
Root relative squared error: 119.89195584110024
Weighted TruePositiveRate: 0.6354166666666666
Weighted MatthewsCorrelation: 0.22011818834181449
Weighted FMeasure: 0.6396604489194119
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6078229611823361
Mean absolute error: 0.36654589371980667
Coverage of cases: 63.541666666666664
Instances selection time: 1.0
Test time: 27.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6354166666666666
Weighted FalsePositiveRate: 0.41010323383084574
Kappa statistic: 0.21924015336354116
Training time: 0.0
		
Iteration: 5
Labeled set size: 156
Unlabelled set size: 228
	
Mean region size: 50.0
Incorrectly Classified Instances: 34.114583333333336
Correctly Classified Instances: 65.88541666666667
Weighted Precision: 0.6618758564576911
Weighted AreaUnderROC: 0.6289253731343284
Root mean squared error: 0.5804029542728338
Relative absolute error: 68.63132911392414
Root relative squared error: 116.08059085456676
Weighted TruePositiveRate: 0.6588541666666666
Weighted MatthewsCorrelation: 0.25574099697248676
Weighted FMeasure: 0.6602552540052541
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6190661338117007
Mean absolute error: 0.34315664556962067
Coverage of cases: 65.88541666666667
Instances selection time: 0.0
Test time: 12.0
Accumulative iteration time: 6.0
Weighted Recall: 0.6588541666666666
Weighted FalsePositiveRate: 0.40100342039800996
Kappa statistic: 0.255637762651672
Training time: 0.0
		
Iteration: 6
Labeled set size: 176
Unlabelled set size: 208
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.680245519113491
Weighted AreaUnderROC: 0.6483880597014925
Root mean squared error: 0.5628010703770678
Relative absolute error: 64.46629213483143
Root relative squared error: 112.56021407541357
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.2962680353096309
Weighted FMeasure: 0.6799619219852345
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6329919032321416
Mean absolute error: 0.3223314606741572
Coverage of cases: 67.96875
Instances selection time: 1.0
Test time: 13.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.38291138059701496
Kappa statistic: 0.2962631861255141
Training time: 0.0
		
Iteration: 7
Labeled set size: 196
Unlabelled set size: 188
	
Mean region size: 50.0
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6678328589500316
Weighted AreaUnderROC: 0.6349253731343284
Root mean squared error: 0.5744491601243706
Relative absolute error: 67.003367003367
Root relative squared error: 114.88983202487412
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.26893710921054953
Weighted FMeasure: 0.6672318905250632
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6233653439114221
Mean absolute error: 0.33501683501683505
Coverage of cases: 66.66666666666667
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.39681592039800995
Kappa statistic: 0.26891956211327933
Training time: 0.0
		
Iteration: 8
Labeled set size: 216
Unlabelled set size: 168
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.291666666666664
Correctly Classified Instances: 67.70833333333333
Weighted Precision: 0.6805596213895395
Weighted AreaUnderROC: 0.6498507462686568
Root mean squared error: 0.5656634780858653
Relative absolute error: 64.9082568807339
Root relative squared error: 113.13269561717306
Weighted TruePositiveRate: 0.6770833333333334
Weighted MatthewsCorrelation: 0.2967921283975797
Weighted FMeasure: 0.6786578848862751
Iteration time: 5.0
Weighted AreaUnderPRC: 0.633727122060402
Mean absolute error: 0.3245412844036695
Coverage of cases: 67.70833333333333
Instances selection time: 5.0
Test time: 18.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6770833333333334
Weighted FalsePositiveRate: 0.37738184079601994
Kappa statistic: 0.29662018435358073
Training time: 0.0
		
Iteration: 9
Labeled set size: 236
Unlabelled set size: 148
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6897239896312007
Weighted AreaUnderROC: 0.6595820895522388
Root mean squared error: 0.5566790850214359
Relative absolute error: 62.81512605042012
Root relative squared error: 111.33581700428718
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.3170512354332425
Weighted FMeasure: 0.6885377134724857
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6409740517666725
Mean absolute error: 0.3140756302521006
Coverage of cases: 68.75
Instances selection time: 0.0
Test time: 16.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.36833582089552236
Kappa statistic: 0.31696905016008536
Training time: 0.0
		
Iteration: 10
Labeled set size: 256
Unlabelled set size: 128
	
Mean region size: 50.0
Incorrectly Classified Instances: 32.03125
Correctly Classified Instances: 67.96875
Weighted Precision: 0.6862118561535132
Weighted AreaUnderROC: 0.657044776119403
Root mean squared error: 0.5637769776249928
Relative absolute error: 64.34108527131777
Root relative squared error: 112.75539552499856
Weighted TruePositiveRate: 0.6796875
Weighted MatthewsCorrelation: 0.3088111284383598
Weighted FMeasure: 0.682399207291598
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6386495258727146
Mean absolute error: 0.32170542635658883
Coverage of cases: 67.96875
Instances selection time: 0.0
Test time: 17.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6796875
Weighted FalsePositiveRate: 0.365597947761194
Kappa statistic: 0.30821957935438515
Training time: 0.0
		
Iteration: 11
Labeled set size: 276
Unlabelled set size: 108
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.686921026849804
Weighted AreaUnderROC: 0.6573134328358209
Root mean squared error: 0.5616365383051213
Relative absolute error: 63.80395683453237
Root relative squared error: 112.32730766102425
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3106464194010867
Weighted FMeasure: 0.6843131406857547
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6390298809760538
Mean absolute error: 0.31901978417266186
Coverage of cases: 68.22916666666667
Instances selection time: 0.0
Test time: 18.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.3676648009950249
Kappa statistic: 0.31032858320574713
Training time: 0.0
		
Iteration: 12
Labeled set size: 296
Unlabelled set size: 88
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6932870370370371
Weighted AreaUnderROC: 0.6647761194029851
Root mean squared error: 0.5571480461401432
Relative absolute error: 62.751677852348976
Root relative squared error: 111.42960922802865
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.3244589832949552
Weighted FMeasure: 0.6899317280869183
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6444137008101852
Mean absolute error: 0.3137583892617449
Coverage of cases: 68.75
Instances selection time: 0.0
Test time: 19.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.3579477611940298
Kappa statistic: 0.32394366197183105
Training time: 0.0
		
Iteration: 13
Labeled set size: 316
Unlabelled set size: 68
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.25
Correctly Classified Instances: 68.75
Weighted Precision: 0.6908811475409836
Weighted AreaUnderROC: 0.6613134328358209
Root mean squared error: 0.5572651792856804
Relative absolute error: 62.73584905660389
Root relative squared error: 111.45303585713609
Weighted TruePositiveRate: 0.6875
Weighted MatthewsCorrelation: 0.3194949525618567
Weighted FMeasure: 0.6890237595673631
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6421141244023224
Mean absolute error: 0.31367924528301944
Coverage of cases: 68.75
Instances selection time: 0.0
Test time: 20.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6875
Weighted FalsePositiveRate: 0.3648731343283582
Kappa statistic: 0.3193098558260458
Training time: 0.0
		
Iteration: 14
Labeled set size: 336
Unlabelled set size: 48
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.770833333333332
Correctly Classified Instances: 68.22916666666667
Weighted Precision: 0.6857203844652614
Weighted AreaUnderROC: 0.6555820895522388
Root mean squared error: 0.5619939106091648
Relative absolute error: 63.75739644970406
Root relative squared error: 112.39878212183297
Weighted TruePositiveRate: 0.6822916666666666
Weighted MatthewsCorrelation: 0.3081435404797182
Weighted FMeasure: 0.6838408222268191
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6378913492266783
Mean absolute error: 0.3187869822485203
Coverage of cases: 68.22916666666667
Instances selection time: 0.0
Test time: 21.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6822916666666666
Weighted FalsePositiveRate: 0.37112748756218905
Kappa statistic: 0.3079650200898132
Training time: 0.0
		
Iteration: 15
Labeled set size: 356
Unlabelled set size: 28
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.989583333333332
Correctly Classified Instances: 69.01041666666667
Weighted Precision: 0.6952448701022353
Weighted AreaUnderROC: 0.666776119402985
Root mean squared error: 0.5551327536703665
Relative absolute error: 62.19157355679697
Root relative squared error: 111.02655073407331
Weighted TruePositiveRate: 0.6901041666666666
Weighted MatthewsCorrelation: 0.3288589936680018
Weighted FMeasure: 0.6922983896662182
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6459755178731085
Mean absolute error: 0.3109578677839848
Coverage of cases: 69.01041666666667
Instances selection time: 0.0
Test time: 22.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6901041666666666
Weighted FalsePositiveRate: 0.3565519278606965
Kappa statistic: 0.3284345423549468
Training time: 0.0
		
Iteration: 16
Labeled set size: 376
Unlabelled set size: 8
	
Mean region size: 50.0
Incorrectly Classified Instances: 30.208333333333332
Correctly Classified Instances: 69.79166666666667
Weighted Precision: 0.6989790876027829
Weighted AreaUnderROC: 0.6693134328358209
Root mean squared error: 0.5481715128521267
Relative absolute error: 60.62610229276888
Root relative squared error: 109.63430257042535
Weighted TruePositiveRate: 0.6979166666666666
Weighted MatthewsCorrelation: 0.33748037244297285
Weighted FMeasure: 0.6984289007883385
Iteration time: 0.0
Weighted AreaUnderPRC: 0.6484335365110426
Mean absolute error: 0.3031305114638444
Coverage of cases: 69.79166666666667
Instances selection time: 0.0
Test time: 24.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6979166666666666
Weighted FalsePositiveRate: 0.3592898009950249
Kappa statistic: 0.3374583531651594
Training time: 0.0
		
Time end:Tue Oct 31 11.39.30 EET 2017