Wed Nov 01 17.29.05 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 17.29.05 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.89333333333448
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8332471311850611
Weighted AreaUnderROC: 0.9458768011182656
Root mean squared error: 0.3366390142219849
Relative absolute error: 30.463274437665877
Root relative squared error: 71.41191893049587
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7225086768066672
Weighted FMeasure: 0.7933306010095732
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9011067952120695
Mean absolute error: 0.1353923308340712
Coverage of cases: 88.52
Instances selection time: 97.0
Test time: 130.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09681289388112355
Kappa statistic: 0.70707596640371
Training time: 5.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 39.773333333334534
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.831416174870052
Weighted AreaUnderROC: 0.95159608433539
Root mean squared error: 0.33637970451445703
Relative absolute error: 30.47069332024092
Root relative squared error: 71.35691103470974
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7196374520669747
Weighted FMeasure: 0.7908519274905345
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9101167773481882
Mean absolute error: 0.13542530364551583
Coverage of cases: 88.44
Instances selection time: 93.0
Test time: 128.0
Accumulative iteration time: 201.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09780840332733196
Kappa statistic: 0.7040862218471585
Training time: 6.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 39.773333333334534
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.83335347211402
Weighted AreaUnderROC: 0.9510190115288626
Root mean squared error: 0.33507488452944617
Relative absolute error: 30.293107607194084
Root relative squared error: 71.08011691682105
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7226028519327868
Weighted FMeasure: 0.7930550164112141
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9091445048159822
Mean absolute error: 0.13463603380975211
Coverage of cases: 88.6
Instances selection time: 92.0
Test time: 129.0
Accumulative iteration time: 299.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09681289388112357
Kappa statistic: 0.7070786335128401
Training time: 6.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.01333333333452
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8330640267154419
Weighted AreaUnderROC: 0.9519142706807241
Root mean squared error: 0.33319531855478907
Relative absolute error: 30.104983685639436
Root relative squared error: 70.6814007629108
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7240884313030556
Weighted FMeasure: 0.7953033525430421
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9106480132038006
Mean absolute error: 0.13379992749173145
Coverage of cases: 89.08
Instances selection time: 92.0
Test time: 126.0
Accumulative iteration time: 397.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.0960425782269215
Kappa statistic: 0.7094614913671125
Training time: 6.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.0000000000012
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8351270347515197
Weighted AreaUnderROC: 0.9525133547898881
Root mean squared error: 0.3314857832328167
Relative absolute error: 29.837523135893964
Root relative squared error: 70.31875355725742
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7274784076309359
Weighted FMeasure: 0.7982157167060487
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9118090705856553
Mean absolute error: 0.13261121393730713
Coverage of cases: 89.2
Instances selection time: 94.0
Test time: 128.0
Accumulative iteration time: 496.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09484934014951166
Kappa statistic: 0.7130492500940527
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 39.96000000000119
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8353996370497001
Weighted AreaUnderROC: 0.9526025795568134
Root mean squared error: 0.33020475627363843
Relative absolute error: 29.670769844308715
Root relative squared error: 70.0470067023421
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7280134201574425
Weighted FMeasure: 0.7986922711405248
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9118821067146142
Mean absolute error: 0.13187008819692825
Coverage of cases: 89.16
Instances selection time: 90.0
Test time: 128.0
Accumulative iteration time: 591.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09465161151831022
Kappa statistic: 0.7136470084738918
Training time: 5.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.013333333334515
Incorrectly Classified Instances: 18.96
Correctly Classified Instances: 81.04
Weighted Precision: 0.8365909953771145
Weighted AreaUnderROC: 0.952765155673254
Root mean squared error: 0.32919009442663255
Relative absolute error: 29.561420741439857
Root relative squared error: 69.83176442055337
Weighted TruePositiveRate: 0.8104
Weighted MatthewsCorrelation: 0.7302109677992985
Weighted FMeasure: 0.8004985596360354
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9121007112142077
Mean absolute error: 0.13138409218417776
Coverage of cases: 89.36
Instances selection time: 89.0
Test time: 127.0
Accumulative iteration time: 686.0
Weighted Recall: 0.8104
Weighted FalsePositiveRate: 0.09386069699350448
Kappa statistic: 0.7160393892905873
Training time: 6.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 40.18666666666786
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8359488558531583
Weighted AreaUnderROC: 0.9536170765360071
Root mean squared error: 0.32785762912933425
Relative absolute error: 29.44452806053694
Root relative squared error: 69.54910584632876
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.730292974769818
Weighted FMeasure: 0.8012798025954329
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9136304112002541
Mean absolute error: 0.13086456915794256
Coverage of cases: 89.68
Instances selection time: 87.0
Test time: 131.0
Accumulative iteration time: 780.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09367670094270551
Kappa statistic: 0.7166307213036184
Training time: 7.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 40.14666666666788
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8364270776911855
Weighted AreaUnderROC: 0.953933904512309
Root mean squared error: 0.3274492146595083
Relative absolute error: 29.41648628555402
Root relative squared error: 69.46246805398417
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7305328597512344
Weighted FMeasure: 0.8011896499400467
Iteration time: 141.0
Weighted AreaUnderPRC: 0.9141227741521745
Mean absolute error: 0.13073993904690737
Coverage of cases: 89.68
Instances selection time: 134.0
Test time: 264.0
Accumulative iteration time: 921.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09366983465250428
Kappa statistic: 0.7166333016857264
Training time: 7.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 40.32000000000124
Incorrectly Classified Instances: 18.72
Correctly Classified Instances: 81.28
Weighted Precision: 0.8379709131808719
Weighted AreaUnderROC: 0.954433436375871
Root mean squared error: 0.32634054930062406
Relative absolute error: 29.348358581589082
Root relative squared error: 69.22728461598406
Weighted TruePositiveRate: 0.8128
Weighted MatthewsCorrelation: 0.7333114819609634
Weighted FMeasure: 0.8034721317402611
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9150398941420924
Mean absolute error: 0.13043714925150765
Coverage of cases: 89.88
Instances selection time: 86.0
Test time: 130.0
Accumulative iteration time: 1014.0
Weighted Recall: 0.8128
Weighted FalsePositiveRate: 0.09268119149649712
Kappa statistic: 0.7196236150724593
Training time: 7.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 40.413333333334585
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.8384452183591058
Weighted AreaUnderROC: 0.9547888982928956
Root mean squared error: 0.3257886878893807
Relative absolute error: 29.271779239416706
Root relative squared error: 69.11021713213445
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7347481874887878
Weighted FMeasure: 0.8051096348180952
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9157544414527843
Mean absolute error: 0.13009679661963042
Coverage of cases: 90.16
Instances selection time: 86.0
Test time: 130.0
Accumulative iteration time: 1107.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.09209487189309402
Kappa statistic: 0.7214132889575656
Training time: 7.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 40.426666666667906
Incorrectly Classified Instances: 18.6
Correctly Classified Instances: 81.4
Weighted Precision: 0.837540861720702
Weighted AreaUnderROC: 0.9548432735291502
Root mean squared error: 0.32461421328879814
Relative absolute error: 29.14759899697565
Root relative squared error: 68.86107344581347
Weighted TruePositiveRate: 0.814
Weighted MatthewsCorrelation: 0.7342989897607747
Weighted FMeasure: 0.805271772915069
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9158268617374912
Mean absolute error: 0.12954488443100348
Coverage of cases: 90.32
Instances selection time: 85.0
Test time: 130.0
Accumulative iteration time: 1199.0
Weighted Recall: 0.814
Weighted FalsePositiveRate: 0.09210860447349649
Kappa statistic: 0.7214082149707676
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 40.53333333333459
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8381420178681197
Weighted AreaUnderROC: 0.9546837344812807
Root mean squared error: 0.32403482205985296
Relative absolute error: 29.10541049867112
Root relative squared error: 68.73816600572934
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7353926463001893
Weighted FMeasure: 0.806223160696753
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9155962890081628
Mean absolute error: 0.12935737999409447
Coverage of cases: 90.44
Instances selection time: 85.0
Test time: 130.0
Accumulative iteration time: 1291.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09171314721109361
Kappa statistic: 0.7226039330568399
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 40.680000000001236
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8365935677448085
Weighted AreaUnderROC: 0.9545416631311294
Root mean squared error: 0.3233898788466705
Relative absolute error: 29.055182113902195
Root relative squared error: 68.60135288987286
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.734640508766598
Weighted FMeasure: 0.8064636009528867
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9154199114001094
Mean absolute error: 0.1291341427284548
Coverage of cases: 90.6
Instances selection time: 83.0
Test time: 130.0
Accumulative iteration time: 1381.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09174061237189853
Kappa statistic: 0.7225950911673558
Training time: 7.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 40.72000000000123
Incorrectly Classified Instances: 18.4
Correctly Classified Instances: 81.6
Weighted Precision: 0.8377954494391786
Weighted AreaUnderROC: 0.9546359719884271
Root mean squared error: 0.32232825334811344
Relative absolute error: 28.901442357323536
Root relative squared error: 68.37614811313979
Weighted TruePositiveRate: 0.816
Weighted MatthewsCorrelation: 0.7364139472925526
Weighted FMeasure: 0.8078848209125931
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9156067463515748
Mean absolute error: 0.12845085492143854
Coverage of cases: 90.68
Instances selection time: 81.0
Test time: 131.0
Accumulative iteration time: 1470.0
Weighted Recall: 0.816
Weighted FalsePositiveRate: 0.091140560188093
Kappa statistic: 0.7243900211932089
Training time: 8.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.720000000001235
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8392132777125307
Weighted AreaUnderROC: 0.9547735348601555
Root mean squared error: 0.3219680648682945
Relative absolute error: 28.856678350773517
Root relative squared error: 68.29974059816422
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7383389460944829
Weighted FMeasure: 0.8091119426116838
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9158796769526252
Mean absolute error: 0.12825190378121623
Coverage of cases: 90.72
Instances selection time: 81.0
Test time: 131.0
Accumulative iteration time: 1559.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09054050800428746
Kappa statistic: 0.7261874775767316
Training time: 8.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 40.773333333334556
Incorrectly Classified Instances: 18.4
Correctly Classified Instances: 81.6
Weighted Precision: 0.8371444097388214
Weighted AreaUnderROC: 0.9545928213297329
Root mean squared error: 0.32156620164380084
Relative absolute error: 28.864560449599804
Root relative squared error: 68.21449253481954
Weighted TruePositiveRate: 0.816
Weighted MatthewsCorrelation: 0.7361087789875804
Weighted FMeasure: 0.8080635742742752
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9156639385638417
Mean absolute error: 0.12828693533155527
Coverage of cases: 90.76
Instances selection time: 80.0
Test time: 132.0
Accumulative iteration time: 1647.0
Weighted Recall: 0.816
Weighted FalsePositiveRate: 0.09115429276849543
Kappa statistic: 0.7243850010796918
Training time: 8.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 40.826666666667876
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8366526220094167
Weighted AreaUnderROC: 0.954619827677799
Root mean squared error: 0.3207502869793651
Relative absolute error: 28.79569665477342
Root relative squared error: 68.04141089719191
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7362576883543032
Weighted FMeasure: 0.8086831532500987
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9156911414141469
Mean absolute error: 0.1279808740212158
Coverage of cases: 90.8
Instances selection time: 80.0
Test time: 131.0
Accumulative iteration time: 1735.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.09097029671769644
Kappa statistic: 0.7249779023639199
Training time: 8.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 40.90666666666787
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8367803605538522
Weighted AreaUnderROC: 0.9547671289727813
Root mean squared error: 0.3197793042255534
Relative absolute error: 28.68396137797453
Root relative squared error: 67.83543435030128
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7370935086695637
Weighted FMeasure: 0.8096870886354756
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9158676492637354
Mean absolute error: 0.1274842727909985
Coverage of cases: 91.0
Instances selection time: 77.0
Test time: 131.0
Accumulative iteration time: 1821.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.0905817057454948
Kappa statistic: 0.7261712679480654
Training time: 9.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 41.06666666666783
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8363729946776278
Weighted AreaUnderROC: 0.9547362918129969
Root mean squared error: 0.3188256242484812
Relative absolute error: 28.59490720664145
Root relative squared error: 67.6331282766404
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7368850489677555
Weighted FMeasure: 0.8097569743941098
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9157799598739228
Mean absolute error: 0.1270884764739626
Coverage of cases: 91.28
Instances selection time: 76.0
Test time: 129.0
Accumulative iteration time: 1906.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09058857203569604
Kappa statistic: 0.7261687739886941
Training time: 9.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 41.120000000001134
Incorrectly Classified Instances: 18.12
Correctly Classified Instances: 81.88
Weighted Precision: 0.8379166894049754
Weighted AreaUnderROC: 0.9548641805877689
Root mean squared error: 0.31815787177724797
Relative absolute error: 28.53565311947585
Root relative squared error: 67.49147658647148
Weighted TruePositiveRate: 0.8188
Weighted MatthewsCorrelation: 0.7392216565716572
Weighted FMeasure: 0.8116439157371413
Iteration time: 86.0
Weighted AreaUnderPRC: 0.915989759216999
Mean absolute error: 0.12682512497544882
Coverage of cases: 91.32
Instances selection time: 76.0
Test time: 129.0
Accumulative iteration time: 1992.0
Weighted Recall: 0.8188
Weighted FalsePositiveRate: 0.08979079122068909
Kappa statistic: 0.728561837927569
Training time: 10.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 41.17333333333445
Incorrectly Classified Instances: 17.96
Correctly Classified Instances: 82.04
Weighted Precision: 0.8381326864883318
Weighted AreaUnderROC: 0.9548905578390989
Root mean squared error: 0.3175880860692297
Relative absolute error: 28.527170152800586
Root relative squared error: 67.37060678508261
Weighted TruePositiveRate: 0.8204
Weighted MatthewsCorrelation: 0.7409217022839142
Weighted FMeasure: 0.8136159659670331
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9161017621598209
Mean absolute error: 0.12678742290133654
Coverage of cases: 91.24
Instances selection time: 75.0
Test time: 129.0
Accumulative iteration time: 2076.0
Weighted Recall: 0.8204
Weighted FalsePositiveRate: 0.08902047556648701
Kappa statistic: 0.7309476156805408
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 41.16000000000112
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.838119275054575
Weighted AreaUnderROC: 0.9548795101949954
Root mean squared error: 0.31704198147922574
Relative absolute error: 28.439678236613148
Root relative squared error: 67.25476050743394
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7405146041836148
Weighted FMeasure: 0.8130769796613491
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9160609329552161
Mean absolute error: 0.12639856994050347
Coverage of cases: 91.4
Instances selection time: 75.0
Test time: 131.0
Accumulative iteration time: 2160.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08921133790748723
Kappa statistic: 0.7303520739041448
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 41.20000000000111
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8377679487894013
Weighted AreaUnderROC: 0.9550870819061689
Root mean squared error: 0.3166294205268908
Relative absolute error: 28.40312646999158
Root relative squared error: 67.16724311331929
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7403379353343659
Weighted FMeasure: 0.8131467841811777
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9164043980886625
Mean absolute error: 0.12623611764440762
Coverage of cases: 91.48
Instances selection time: 74.0
Test time: 128.0
Accumulative iteration time: 2243.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.08921820419768844
Kappa statistic: 0.7303496178994162
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 41.18666666666778
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8393412773145664
Weighted AreaUnderROC: 0.9549730056498728
Root mean squared error: 0.31639832816320884
Relative absolute error: 28.368019535842393
Root relative squared error: 67.11822102008732
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7427006760497774
Weighted FMeasure: 0.8149272075558484
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9161935183428604
Mean absolute error: 0.12608008682596678
Coverage of cases: 91.44
Instances selection time: 72.0
Test time: 129.0
Accumulative iteration time: 2325.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08842042338268147
Kappa statistic: 0.7327440759668373
Training time: 10.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 41.28000000000108
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.8383460470434447
Weighted AreaUnderROC: 0.9550652313469998
Root mean squared error: 0.31592159570884215
Relative absolute error: 28.30848182770168
Root relative squared error: 67.01709079469899
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7413960431186303
Weighted FMeasure: 0.8141728659080039
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9163308942293694
Mean absolute error: 0.12581547478978583
Coverage of cases: 91.6
Instances selection time: 72.0
Test time: 128.0
Accumulative iteration time: 2407.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08882274693528557
Kappa statistic: 0.7315443963248427
Training time: 10.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 41.29333333333442
Incorrectly Classified Instances: 17.88
Correctly Classified Instances: 82.12
Weighted Precision: 0.8380062710542212
Weighted AreaUnderROC: 0.9553660896332751
Root mean squared error: 0.3155289383708623
Relative absolute error: 28.30088137860397
Root relative squared error: 66.93379559478853
Weighted TruePositiveRate: 0.8212
Weighted MatthewsCorrelation: 0.7416145246139492
Weighted FMeasure: 0.8147808520154053
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9168337817797033
Mean absolute error: 0.12578169501601824
Coverage of cases: 91.68
Instances selection time: 71.0
Test time: 128.0
Accumulative iteration time: 2488.0
Weighted Recall: 0.8212
Weighted FalsePositiveRate: 0.08863875088448658
Kappa statistic: 0.7321375281435369
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 41.426666666667714
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8376271415910592
Weighted AreaUnderROC: 0.9550491217292065
Root mean squared error: 0.31481488990584183
Relative absolute error: 28.243177169087286
Root relative squared error: 66.78232304127499
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7418113450984315
Weighted FMeasure: 0.8152865917730949
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9163172602214401
Mean absolute error: 0.12552523186261075
Coverage of cases: 91.88
Instances selection time: 69.0
Test time: 132.0
Accumulative iteration time: 2568.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08845475483368762
Kappa statistic: 0.7327319042120733
Training time: 11.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 41.4666666666677
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.8363580195116755
Weighted AreaUnderROC: 0.9550702881167644
Root mean squared error: 0.3141083893795367
Relative absolute error: 28.20697207073274
Root relative squared error: 66.63245164735633
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7403809384740302
Weighted FMeasure: 0.8145059314610067
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9163430900361862
Mean absolute error: 0.1253643203143683
Coverage of cases: 91.92
Instances selection time: 69.0
Test time: 128.0
Accumulative iteration time: 2647.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08886394467649293
Kappa statistic: 0.7315309471708035
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 41.54666666666768
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8367049850513552
Weighted AreaUnderROC: 0.9548770811183526
Root mean squared error: 0.31376419591585175
Relative absolute error: 28.17732557569372
Root relative squared error: 66.55943718769281
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7413488794296361
Weighted FMeasure: 0.8154039418404515
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9159802819157358
Mean absolute error: 0.1252325581141949
Coverage of cases: 91.92
Instances selection time: 68.0
Test time: 128.0
Accumulative iteration time: 2725.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08847535370429127
Kappa statistic: 0.7327258179189242
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.54666666666768
Incorrectly Classified Instances: 17.88
Correctly Classified Instances: 82.12
Weighted Precision: 0.8363531022334526
Weighted AreaUnderROC: 0.9549792635144854
Root mean squared error: 0.3134070504884948
Relative absolute error: 28.13729973102281
Root relative squared error: 66.48367520162665
Weighted TruePositiveRate: 0.8212
Weighted MatthewsCorrelation: 0.7407651635733213
Weighted FMeasure: 0.8150342693037185
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9161657150877381
Mean absolute error: 0.12505466547121308
Coverage of cases: 91.88
Instances selection time: 67.0
Test time: 129.0
Accumulative iteration time: 2803.0
Weighted Recall: 0.8212
Weighted FalsePositiveRate: 0.08867308233549272
Kappa statistic: 0.7321265484523748
Training time: 11.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 41.53333333333434
Incorrectly Classified Instances: 17.72
Correctly Classified Instances: 82.28
Weighted Precision: 0.8382621573632858
Weighted AreaUnderROC: 0.9550562178594377
Root mean squared error: 0.31285995411478346
Relative absolute error: 28.079315898841088
Root relative squared error: 66.3676185348825
Weighted TruePositiveRate: 0.8228
Weighted MatthewsCorrelation: 0.7433159120893209
Weighted FMeasure: 0.8166398951801009
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9163154591103281
Mean absolute error: 0.12479695955040543
Coverage of cases: 91.96
Instances selection time: 65.0
Test time: 129.0
Accumulative iteration time: 2879.0
Weighted Recall: 0.8228
Weighted FalsePositiveRate: 0.08786843523028454
Kappa statistic: 0.7345248354113907
Training time: 11.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 41.53333333333432
Incorrectly Classified Instances: 17.68
Correctly Classified Instances: 82.32
Weighted Precision: 0.8379220921734902
Weighted AreaUnderROC: 0.9547952850992107
Root mean squared error: 0.3125185808032219
Relative absolute error: 28.03750530160858
Root relative squared error: 66.29520231982612
Weighted TruePositiveRate: 0.8232
Weighted MatthewsCorrelation: 0.7435121980730223
Weighted FMeasure: 0.8173234453487772
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9158738346851151
Mean absolute error: 0.12461113467381649
Coverage of cases: 91.92
Instances selection time: 64.0
Test time: 131.0
Accumulative iteration time: 2955.0
Weighted Recall: 0.8232
Weighted FalsePositiveRate: 0.08768443917948555
Kappa statistic: 0.7351168638028446
Training time: 12.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 41.56000000000098
Incorrectly Classified Instances: 17.48
Correctly Classified Instances: 82.52
Weighted Precision: 0.8398021320490101
Weighted AreaUnderROC: 0.9548163114144484
Root mean squared error: 0.3121759623040796
Relative absolute error: 28.01190110446318
Root relative squared error: 66.22252196059507
Weighted TruePositiveRate: 0.8252
Weighted MatthewsCorrelation: 0.7464225778704833
Weighted FMeasure: 0.8195382035882344
Iteration time: 75.0
Weighted AreaUnderPRC: 0.915950039252056
Mean absolute error: 0.12449733824205916
Coverage of cases: 92.04
Instances selection time: 63.0
Test time: 130.0
Accumulative iteration time: 3030.0
Weighted Recall: 0.8252
Weighted FalsePositiveRate: 0.08668892973327716
Kappa statistic: 0.738109700991995
Training time: 12.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 41.54666666666764
Incorrectly Classified Instances: 17.48
Correctly Classified Instances: 82.52
Weighted Precision: 0.8389211193325169
Weighted AreaUnderROC: 0.9555951284032406
Root mean squared error: 0.3108218186656311
Relative absolute error: 27.88895716342647
Root relative squared error: 65.9352647157608
Weighted TruePositiveRate: 0.8252
Weighted MatthewsCorrelation: 0.745962047062441
Weighted FMeasure: 0.8197362589200784
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9173691520725089
Mean absolute error: 0.12395092072634045
Coverage of cases: 92.04
Instances selection time: 62.0
Test time: 130.0
Accumulative iteration time: 3103.0
Weighted Recall: 0.8252
Weighted FalsePositiveRate: 0.08670952860388084
Kappa statistic: 0.7381025439412392
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.40000000000102
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.8360000823308313
Weighted AreaUnderROC: 0.9536454577322017
Root mean squared error: 0.3147605272442544
Relative absolute error: 28.27869006778413
Root relative squared error: 66.77079097927944
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7401795041308413
Weighted FMeasure: 0.8146663251987617
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9139033079747183
Mean absolute error: 0.12568306696793005
Coverage of cases: 91.68
Instances selection time: 61.0
Test time: 130.0
Accumulative iteration time: 3176.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08887081096669416
Kappa statistic: 0.7315272789858251
Training time: 12.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 41.240000000001054
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8343785125184094
Weighted AreaUnderROC: 0.9530519993552914
Root mean squared error: 0.3164647342581274
Relative absolute error: 28.445666859792773
Root relative squared error: 67.13230788009602
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7369881442475751
Weighted FMeasure: 0.8120230578702768
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9128488376522429
Mean absolute error: 0.12642518604352404
Coverage of cases: 91.48
Instances selection time: 60.0
Test time: 130.0
Accumulative iteration time: 3248.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.09005031646370154
Kappa statistic: 0.7279390967098609
Training time: 12.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 41.226666666667725
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.835317716268237
Weighted AreaUnderROC: 0.9539828944087437
Root mean squared error: 0.31564535588466947
Relative absolute error: 28.336290842289557
Root relative squared error: 66.95849147882711
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7382361983643505
Weighted FMeasure: 0.8129692016646679
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9146550105317898
Mean absolute error: 0.1259390704101764
Coverage of cases: 91.56
Instances selection time: 60.0
Test time: 130.0
Accumulative iteration time: 3320.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.08964799291109744
Kappa statistic: 0.7291363692911595
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 41.106666666667735
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.8350436838479869
Weighted AreaUnderROC: 0.9528010812600147
Root mean squared error: 0.31714334203635725
Relative absolute error: 28.463380114820065
Root relative squared error: 67.2762623286217
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7381203352354241
Weighted FMeasure: 0.8128547917438665
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9127024216806944
Mean absolute error: 0.1265039116214231
Coverage of cases: 91.24
Instances selection time: 59.0
Test time: 130.0
Accumulative iteration time: 3391.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.08965485920129868
Kappa statistic: 0.7291363692911595
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.0933333333344
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.833997819374313
Weighted AreaUnderROC: 0.953969412484072
Root mean squared error: 0.31611535291279336
Relative absolute error: 28.339531967858466
Root relative squared error: 67.05819290454428
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7363577206464547
Weighted FMeasure: 0.8119320266497949
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9148755173550004
Mean absolute error: 0.12595347541270488
Coverage of cases: 91.32
Instances selection time: 57.0
Test time: 131.0
Accumulative iteration time: 3461.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09024804509490296
Kappa statistic: 0.7273361182203473
Training time: 13.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.08000000000105
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8365959714860601
Weighted AreaUnderROC: 0.9542730109260746
Root mean squared error: 0.31601155380575435
Relative absolute error: 28.31250266321241
Root relative squared error: 67.03617378880377
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7392669036772469
Weighted FMeasure: 0.8133998844862282
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9154464960312356
Mean absolute error: 0.12583334516983352
Coverage of cases: 91.32
Instances selection time: 57.0
Test time: 131.0
Accumulative iteration time: 3531.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.08942966540929233
Kappa statistic: 0.7297393176918514
Training time: 13.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.00000000000107
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8357144140976247
Weighted AreaUnderROC: 0.9541979722038119
Root mean squared error: 0.3162076431421166
Relative absolute error: 28.332516960140175
Root relative squared error: 67.07777061864181
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7376545336296604
Weighted FMeasure: 0.8120193237395972
Iteration time: 69.0
Weighted AreaUnderPRC: 0.915357793336476
Mean absolute error: 0.1259222976006236
Coverage of cases: 91.44
Instances selection time: 56.0
Test time: 132.0
Accumulative iteration time: 3600.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.09002285130289663
Kappa statistic: 0.7279452918356023
Training time: 13.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 40.97333333333442
Incorrectly Classified Instances: 18.12
Correctly Classified Instances: 81.88
Weighted Precision: 0.8357737463019932
Weighted AreaUnderROC: 0.9537702534962066
Root mean squared error: 0.31650088542491883
Relative absolute error: 28.354268598370265
Root relative squared error: 67.13997670065183
Weighted TruePositiveRate: 0.8188
Weighted MatthewsCorrelation: 0.738103972557033
Weighted FMeasure: 0.8123661865770024
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9146722380722112
Mean absolute error: 0.12601897154831287
Coverage of cases: 91.32
Instances selection time: 56.0
Test time: 131.0
Accumulative iteration time: 3669.0
Weighted Recall: 0.8188
Weighted FalsePositiveRate: 0.08983198896189643
Kappa statistic: 0.7285432950074019
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 40.82666666666775
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.8365139973330588
Weighted AreaUnderROC: 0.9538746290577065
Root mean squared error: 0.31686268451809335
Relative absolute error: 28.39778444989436
Root relative squared error: 67.21672587831509
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7388969235397033
Weighted FMeasure: 0.8126647910767827
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9148641268690629
Mean absolute error: 0.12621237533286442
Coverage of cases: 91.08
Instances selection time: 55.0
Test time: 129.0
Accumulative iteration time: 3737.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.08962739404049376
Kappa statistic: 0.7291450042593746
Training time: 13.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 40.85333333333442
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8356567933205544
Weighted AreaUnderROC: 0.953502225039409
Root mean squared error: 0.3176408997583084
Relative absolute error: 28.468045219339896
Root relative squared error: 67.38181026038872
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7373057416481196
Weighted FMeasure: 0.8111888014527212
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9142725472397624
Mean absolute error: 0.126524645419289
Coverage of cases: 90.92
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 3804.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09022057993409807
Kappa statistic: 0.7273522608789443
Training time: 14.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 40.85333333333442
Incorrectly Classified Instances: 18.0
Correctly Classified Instances: 82.0
Weighted Precision: 0.8372856779795628
Weighted AreaUnderROC: 0.9546866901662437
Root mean squared error: 0.31670123650501586
Relative absolute error: 28.39232822812806
Root relative squared error: 67.18247758285823
Weighted TruePositiveRate: 0.82
Weighted MatthewsCorrelation: 0.7401223790753378
Weighted FMeasure: 0.8134729282226193
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9163619519551277
Mean absolute error: 0.12618812545834754
Coverage of cases: 91.12
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 3871.0
Weighted Recall: 0.82
Weighted FalsePositiveRate: 0.0892319367780909
Kappa statistic: 0.7303422496167863
Training time: 14.0
		
Time end:Wed Nov 01 17.29.18 EET 2017