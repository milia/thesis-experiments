Wed Nov 01 15.19.20 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 15.19.20 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.6908038021388636
Weighted AreaUnderROC: 0.7676628087752634
Root mean squared error: 0.45296455815067116
Relative absolute error: 46.75944333995982
Root relative squared error: 96.08829321165213
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.5355997935847004
Weighted FMeasure: 0.6903742719022679
Iteration time: 768.0
Weighted AreaUnderPRC: 0.5803721036873329
Mean absolute error: 0.20781974817760018
Coverage of cases: 69.04
Instances selection time: 767.0
Test time: 948.0
Accumulative iteration time: 768.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.15507438244947314
Kappa statistic: 0.535518102551842
Training time: 1.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.6947696438208311
Weighted AreaUnderROC: 0.7710121128586823
Root mean squared error: 0.44978535513611223
Relative absolute error: 46.091013384319986
Root relative squared error: 95.41388240854312
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.5421120438467139
Weighted FMeasure: 0.6944384777943081
Iteration time: 779.0
Weighted AreaUnderPRC: 0.5848329835259416
Mean absolute error: 0.20484894837475648
Coverage of cases: 69.48
Instances selection time: 779.0
Test time: 982.0
Accumulative iteration time: 1547.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.15277577428263528
Kappa statistic: 0.5421735559445914
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.691941066227093
Weighted AreaUnderROC: 0.7689475278102822
Root mean squared error: 0.4518912238146332
Relative absolute error: 46.49723756906124
Root relative squared error: 95.86060461540428
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5380189704096816
Weighted FMeasure: 0.6913271918411872
Iteration time: 808.0
Weighted AreaUnderPRC: 0.5818280580834404
Mean absolute error: 0.2066543891958287
Coverage of cases: 69.2
Instances selection time: 808.0
Test time: 1015.0
Accumulative iteration time: 2355.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.15410494437943553
Kappa statistic: 0.5380175553328972
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.68
Correctly Classified Instances: 69.32
Weighted Precision: 0.6932684322942595
Weighted AreaUnderROC: 0.7698715859178892
Root mean squared error: 0.4510540626494345
Relative absolute error: 46.30763765541668
Root relative squared error: 95.68301591434687
Weighted TruePositiveRate: 0.6932
Weighted MatthewsCorrelation: 0.5400206679117336
Weighted FMeasure: 0.692182318840172
Iteration time: 978.0
Weighted AreaUnderPRC: 0.5830433028752683
Mean absolute error: 0.20581172291296398
Coverage of cases: 69.32
Instances selection time: 978.0
Test time: 1060.0
Accumulative iteration time: 3333.0
Weighted Recall: 0.6932
Weighted FalsePositiveRate: 0.15345682816422143
Kappa statistic: 0.5398405676555555
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.68
Correctly Classified Instances: 69.32
Weighted Precision: 0.6937423495429059
Weighted AreaUnderROC: 0.7699265162394991
Root mean squared error: 0.4510950348560859
Relative absolute error: 46.29777015437277
Root relative squared error: 95.69170743189588
Weighted TruePositiveRate: 0.6932
Weighted MatthewsCorrelation: 0.5405753808336573
Weighted FMeasure: 0.6914061405139131
Iteration time: 845.0
Weighted AreaUnderPRC: 0.5830883744640286
Mean absolute error: 0.20576786735276884
Coverage of cases: 69.32
Instances selection time: 845.0
Test time: 1090.0
Accumulative iteration time: 4178.0
Weighted Recall: 0.6932
Weighted FalsePositiveRate: 0.1533469675210018
Kappa statistic: 0.5399118934275184
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.28
Correctly Classified Instances: 69.72
Weighted Precision: 0.6985247801795043
Weighted AreaUnderROC: 0.7729288919759087
Root mean squared error: 0.44818283691604527
Relative absolute error: 45.691542288557564
Root relative squared error: 95.07393695842782
Weighted TruePositiveRate: 0.6972
Weighted MatthewsCorrelation: 0.5472347964149581
Weighted FMeasure: 0.6947268510972932
Iteration time: 848.0
Weighted AreaUnderPRC: 0.5874599070897144
Mean absolute error: 0.20307352128247902
Coverage of cases: 69.72
Instances selection time: 848.0
Test time: 1120.0
Accumulative iteration time: 5026.0
Weighted Recall: 0.6972
Weighted FalsePositiveRate: 0.15134221604818257
Kappa statistic: 0.5459207850711711
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.6978542084100049
Weighted AreaUnderROC: 0.772046031609509
Root mean squared error: 0.4491056631118911
Relative absolute error: 45.861958266453634
Root relative squared error: 95.26969795670956
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.5458794883961544
Weighted FMeasure: 0.6931342227291869
Iteration time: 865.0
Weighted AreaUnderPRC: 0.586265312847634
Mean absolute error: 0.20383092562868377
Coverage of cases: 69.6
Instances selection time: 865.0
Test time: 1148.0
Accumulative iteration time: 5891.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.151907936780982
Kappa statistic: 0.5441420453386724
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.44
Correctly Classified Instances: 69.56
Weighted Precision: 0.6983262065443291
Weighted AreaUnderROC: 0.7717506004390089
Root mean squared error: 0.4494344878508344
Relative absolute error: 45.913530326595414
Root relative squared error: 95.33945221752818
Weighted TruePositiveRate: 0.6956
Weighted MatthewsCorrelation: 0.5459166086014413
Weighted FMeasure: 0.6922874412353164
Iteration time: 879.0
Weighted AreaUnderPRC: 0.5860601396385521
Mean absolute error: 0.20406013478486948
Coverage of cases: 69.56
Instances selection time: 879.0
Test time: 1183.0
Accumulative iteration time: 6770.0
Weighted Recall: 0.6956
Weighted FalsePositiveRate: 0.15209879912198215
Kappa statistic: 0.5435567973689678
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.64
Correctly Classified Instances: 69.36
Weighted Precision: 0.6974437038444308
Weighted AreaUnderROC: 0.7702665782963072
Root mean squared error: 0.45094005479495913
Relative absolute error: 46.204524886878396
Root relative squared error: 95.65883119624445
Weighted TruePositiveRate: 0.6936
Weighted MatthewsCorrelation: 0.5437593304578995
Weighted FMeasure: 0.6897086956663774
Iteration time: 894.0
Weighted AreaUnderPRC: 0.5840783145778689
Mean absolute error: 0.20535344394168273
Coverage of cases: 69.36
Instances selection time: 894.0
Test time: 1216.0
Accumulative iteration time: 7664.0
Weighted Recall: 0.6936
Weighted FalsePositiveRate: 0.15306684340738566
Kappa statistic: 0.5405682990207027
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.64
Correctly Classified Instances: 69.36
Weighted Precision: 0.6982339806902315
Weighted AreaUnderROC: 0.770276877731609
Root mean squared error: 0.45096977266554433
Relative absolute error: 46.197364568082286
Root relative squared error: 95.66513530858842
Weighted TruePositiveRate: 0.6936
Weighted MatthewsCorrelation: 0.544352508560721
Weighted FMeasure: 0.6893181078447848
Iteration time: 907.0
Weighted AreaUnderPRC: 0.5842392437689248
Mean absolute error: 0.20532162030258888
Coverage of cases: 69.36
Instances selection time: 907.0
Test time: 1249.0
Accumulative iteration time: 8571.0
Weighted Recall: 0.6936
Weighted FalsePositiveRate: 0.15304624453678198
Kappa statistic: 0.5405829582427094
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6987735927458143
Weighted AreaUnderROC: 0.7705791751923105
Root mean squared error: 0.4507033304797803
Relative absolute error: 46.13086770981272
Root relative squared error: 95.60861438568404
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5450714628346902
Weighted FMeasure: 0.6896170910643885
Iteration time: 919.0
Weighted AreaUnderPRC: 0.5846760623456269
Mean absolute error: 0.20502607871027972
Coverage of cases: 69.4
Instances selection time: 919.0
Test time: 1281.0
Accumulative iteration time: 9490.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.15284164961537933
Kappa statistic: 0.5411848107582266
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.76
Correctly Classified Instances: 69.24
Weighted Precision: 0.6980346633181022
Weighted AreaUnderROC: 0.7693940173652092
Root mean squared error: 0.45190662056354464
Relative absolute error: 46.36348547717993
Root relative squared error: 95.86387075907332
Weighted TruePositiveRate: 0.6924
Weighted MatthewsCorrelation: 0.5433369693622734
Weighted FMeasure: 0.6875598165432278
Iteration time: 934.0
Weighted AreaUnderPRC: 0.5831210097462086
Mean absolute error: 0.20605993545413398
Coverage of cases: 69.24
Instances selection time: 934.0
Test time: 1312.0
Accumulative iteration time: 10424.0
Weighted Recall: 0.6924
Weighted FalsePositiveRate: 0.1536119652695814
Kappa statistic: 0.5388004926498352
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.6975175667824313
Weighted AreaUnderROC: 0.7685008575635077
Root mean squared error: 0.45281239467246587
Relative absolute error: 46.53674293404988
Root relative squared error: 96.05601446346574
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.5420404503017706
Weighted FMeasure: 0.6860400347343095
Iteration time: 944.0
Weighted AreaUnderPRC: 0.5819729699287364
Mean absolute error: 0.20682996859577824
Coverage of cases: 69.12
Instances selection time: 944.0
Test time: 1344.0
Accumulative iteration time: 11368.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.15419828487298445
Kappa statistic: 0.5370076057895641
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.36
Correctly Classified Instances: 68.64
Weighted Precision: 0.6936283811895358
Weighted AreaUnderROC: 0.7649179189213997
Root mean squared error: 0.45634206878895534
Relative absolute error: 47.248230668411935
Root relative squared error: 96.80477141441025
Weighted TruePositiveRate: 0.6864
Weighted MatthewsCorrelation: 0.5355112125709375
Weighted FMeasure: 0.6808503671466962
Iteration time: 956.0
Weighted AreaUnderPRC: 0.5772417850399992
Mean absolute error: 0.20999213630405403
Coverage of cases: 68.64
Instances selection time: 956.0
Test time: 1377.0
Accumulative iteration time: 12324.0
Weighted Recall: 0.6864
Weighted FalsePositiveRate: 0.15656416215720045
Kappa statistic: 0.5298429771494092
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.6921969562169639
Weighted AreaUnderROC: 0.7634270304884968
Root mean squared error: 0.4578177933424126
Relative absolute error: 47.541762452106575
Root relative squared error: 97.11781986608419
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5329327547022737
Weighted FMeasure: 0.678595959411584
Iteration time: 965.0
Weighted AreaUnderPRC: 0.575276770610655
Mean absolute error: 0.21129672200936356
Coverage of cases: 68.44
Instances selection time: 964.0
Test time: 1410.0
Accumulative iteration time: 13289.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.15754593902300643
Kappa statistic: 0.5268553088753309
Training time: 1.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.84
Correctly Classified Instances: 68.16
Weighted Precision: 0.6899149280396153
Weighted AreaUnderROC: 0.7613349802792918
Root mean squared error: 0.45986600462353333
Relative absolute error: 47.95516811955258
Root relative squared error: 97.55231109193916
Weighted TruePositiveRate: 0.6816
Weighted MatthewsCorrelation: 0.5290857950051141
Weighted FMeasure: 0.6756281936184688
Iteration time: 974.0
Weighted AreaUnderPRC: 0.5724500165269086
Mean absolute error: 0.2131340805313458
Coverage of cases: 68.16
Instances selection time: 974.0
Test time: 1443.0
Accumulative iteration time: 14263.0
Weighted Recall: 0.6816
Weighted FalsePositiveRate: 0.15893003944141645
Kappa statistic: 0.5226619249485421
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.84
Correctly Classified Instances: 68.16
Weighted Precision: 0.6900499645348421
Weighted AreaUnderROC: 0.7613349802792918
Root mean squared error: 0.459886795727999
Relative absolute error: 47.95042527339065
Root relative squared error: 97.55672155122598
Weighted TruePositiveRate: 0.6816
Weighted MatthewsCorrelation: 0.5291861401070892
Weighted FMeasure: 0.6755627930093506
Iteration time: 982.0
Weighted AreaUnderPRC: 0.5724943084700908
Mean absolute error: 0.21311300121507057
Coverage of cases: 68.16
Instances selection time: 982.0
Test time: 1486.0
Accumulative iteration time: 15245.0
Weighted Recall: 0.6816
Weighted FalsePositiveRate: 0.15893003944141645
Kappa statistic: 0.522664100406527
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.04
Correctly Classified Instances: 67.96
Weighted Precision: 0.6888626205684188
Weighted AreaUnderROC: 0.7598440918463888
Root mean squared error: 0.46134875309262346
Relative absolute error: 48.24483985765193
Root relative squared error: 97.86684954112545
Weighted TruePositiveRate: 0.6796
Weighted MatthewsCorrelation: 0.5267720203082408
Weighted FMeasure: 0.6732241741013846
Iteration time: 1007.0
Weighted AreaUnderPRC: 0.5705371514341012
Mean absolute error: 0.21442151047845404
Coverage of cases: 67.96
Instances selection time: 1007.0
Test time: 1512.0
Accumulative iteration time: 16252.0
Weighted Recall: 0.6796
Weighted FalsePositiveRate: 0.1599118163072224
Kappa statistic: 0.5196701375146617
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.04
Correctly Classified Instances: 67.96
Weighted Precision: 0.6889582372954325
Weighted AreaUnderROC: 0.7598475249914893
Root mean squared error: 0.46136770511660813
Relative absolute error: 48.240556199306006
Root relative squared error: 97.87086987252847
Weighted TruePositiveRate: 0.6796
Weighted MatthewsCorrelation: 0.5268627294254369
Weighted FMeasure: 0.6731477145203665
Iteration time: 1000.0
Weighted AreaUnderPRC: 0.5705519368656016
Mean absolute error: 0.2144024719969166
Coverage of cases: 67.96
Instances selection time: 1000.0
Test time: 1537.0
Accumulative iteration time: 17252.0
Weighted Recall: 0.6796
Weighted FalsePositiveRate: 0.15990495001702118
Kappa statistic: 0.5196745156208323
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.6888140674628137
Weighted AreaUnderROC: 0.759249796360288
Root mean squared error: 0.46196144713586257
Relative absolute error: 48.356058890148105
Root relative squared error: 97.99682157495553
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.526125428860706
Weighted FMeasure: 0.672080535102462
Iteration time: 1008.0
Weighted AreaUnderPRC: 0.569833648672235
Mean absolute error: 0.21491581728954812
Coverage of cases: 67.88
Instances selection time: 1008.0
Test time: 1572.0
Accumulative iteration time: 18260.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.16030040727942402
Kappa statistic: 0.518475201053094
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6885472830363593
Weighted AreaUnderROC: 0.7586555008741871
Root mean squared error: 0.46255371701990744
Relative absolute error: 48.47176079734232
Root relative squared error: 98.12246099034574
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.5253369760978142
Weighted FMeasure: 0.671015927424842
Iteration time: 1013.0
Weighted AreaUnderPRC: 0.5691671973171114
Mean absolute error: 0.2154300479881891
Coverage of cases: 67.8
Instances selection time: 1013.0
Test time: 1605.0
Accumulative iteration time: 19273.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.16068899825162566
Kappa statistic: 0.5172890859122289
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.6871948676006155
Weighted AreaUnderROC: 0.7571680455863848
Root mean squared error: 0.4640046218612517
Relative absolute error: 48.767063921993845
Root relative squared error: 98.43024438599701
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5228281604378335
Weighted FMeasure: 0.6687072420068025
Iteration time: 1016.0
Weighted AreaUnderPRC: 0.5671693312948417
Mean absolute error: 0.21674250631997366
Coverage of cases: 67.6
Instances selection time: 1016.0
Test time: 1636.0
Accumulative iteration time: 20289.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.1616639088272304
Kappa statistic: 0.5142975219099789
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6863548626048271
Weighted AreaUnderROC: 0.7556805902985824
Root mean squared error: 0.4654505043823769
Relative absolute error: 49.06256627783637
Root relative squared error: 98.73696238664303
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.520696457614774
Weighted FMeasure: 0.6661638885475544
Iteration time: 1021.0
Weighted AreaUnderPRC: 0.5653118451131444
Mean absolute error: 0.2180558501237182
Coverage of cases: 67.4
Instances selection time: 1021.0
Test time: 1668.0
Accumulative iteration time: 21310.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.16263881940283514
Kappa statistic: 0.5113104937646817
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.64
Correctly Classified Instances: 67.36
Weighted Precision: 0.6861518021966295
Weighted AreaUnderROC: 0.7553851591280824
Root mean squared error: 0.46575130168245443
Relative absolute error: 49.11900311526444
Root relative squared error: 98.80077112983727
Weighted TruePositiveRate: 0.6736
Weighted MatthewsCorrelation: 0.5202597028201408
Weighted FMeasure: 0.6656493982173519
Iteration time: 1025.0
Weighted AreaUnderPRC: 0.5649298145633468
Mean absolute error: 0.2183066805122874
Coverage of cases: 67.36
Instances selection time: 1025.0
Test time: 1704.0
Accumulative iteration time: 22335.0
Weighted Recall: 0.6736
Weighted FalsePositiveRate: 0.16282968174383533
Kappa statistic: 0.5107153341814268
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.72
Correctly Classified Instances: 67.28
Weighted Precision: 0.6856222928483828
Weighted AreaUnderROC: 0.7547908636419816
Root mean squared error: 0.4663364514116644
Relative absolute error: 49.235401831128364
Root relative squared error: 98.92490013229742
Weighted TruePositiveRate: 0.6728
Weighted MatthewsCorrelation: 0.5192758281132216
Weighted FMeasure: 0.6647078526489825
Iteration time: 1029.0
Weighted AreaUnderPRC: 0.564164695110822
Mean absolute error: 0.2188240081383493
Coverage of cases: 67.28
Instances selection time: 1029.0
Test time: 1732.0
Accumulative iteration time: 23364.0
Weighted Recall: 0.6728
Weighted FalsePositiveRate: 0.163218272716037
Kappa statistic: 0.5095228124657474
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6835245845967407
Weighted AreaUnderROC: 0.7526988134327764
Root mean squared error: 0.4683417151306847
Relative absolute error: 49.65104685942246
Root relative squared error: 99.3502808044334
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.5155788509275603
Weighted FMeasure: 0.6616315395367091
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.56150624222166
Mean absolute error: 0.22067131937521195
Coverage of cases: 67.0
Instances selection time: 1034.0
Test time: 1763.0
Accumulative iteration time: 24398.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.16460237313444703
Kappa statistic: 0.5053390999402091
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.6836398537727125
Weighted AreaUnderROC: 0.7527022465778771
Root mean squared error: 0.46835536699077635
Relative absolute error: 49.64809384164164
Root relative squared error: 99.35317680128739
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.5156876223312955
Weighted FMeasure: 0.6615450763533527
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5615244098949274
Mean absolute error: 0.22065819485174165
Coverage of cases: 67.0
Instances selection time: 1035.0
Test time: 1797.0
Accumulative iteration time: 25433.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.16459550684424581
Kappa statistic: 0.5053436081160584
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.92
Correctly Classified Instances: 67.08
Weighted Precision: 0.6846494019532157
Weighted AreaUnderROC: 0.7533102746443804
Root mean squared error: 0.4678004373285518
Relative absolute error: 49.525599232982984
Root relative squared error: 99.23545844311523
Weighted TruePositiveRate: 0.6708
Weighted MatthewsCorrelation: 0.5171147535445689
Weighted FMeasure: 0.6621407483373563
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5623432515410508
Mean absolute error: 0.22011377436881427
Coverage of cases: 67.08
Instances selection time: 1035.0
Test time: 1835.0
Accumulative iteration time: 26468.0
Weighted Recall: 0.6708
Weighted FalsePositiveRate: 0.16417945071123927
Kappa statistic: 0.5065517693746482
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.72
Correctly Classified Instances: 67.28
Weighted Precision: 0.6869432202452572
Weighted AreaUnderROC: 0.7548183288027864
Root mean squared error: 0.46638984456436805
Relative absolute error: 49.22370649106443
Root relative squared error: 98.93622653040111
Weighted TruePositiveRate: 0.6728
Weighted MatthewsCorrelation: 0.5204318112526813
Weighted FMeasure: 0.66385570014679
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5643836732740077
Mean absolute error: 0.2187720288491763
Coverage of cases: 67.28
Instances selection time: 1037.0
Test time: 1860.0
Accumulative iteration time: 27505.0
Weighted Recall: 0.6728
Weighted FalsePositiveRate: 0.16316334239442717
Kappa statistic: 0.5095585715371777
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.6862902286502498
Weighted AreaUnderROC: 0.754220600171585
Root mean squared error: 0.4669717839649998
Relative absolute error: 49.3407202216079
Root relative squared error: 99.05967451932902
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.5193391279776709
Weighted FMeasure: 0.6629955929526165
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5636169736656151
Mean absolute error: 0.21929208987381393
Coverage of cases: 67.2
Instances selection time: 1037.0
Test time: 1895.0
Accumulative iteration time: 28542.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.16355879965683004
Kappa statistic: 0.50836392868543
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.84
Correctly Classified Instances: 67.16
Weighted Precision: 0.6864910474051666
Weighted AreaUnderROC: 0.7539251690010847
Root mean squared error: 0.46726813537672
Relative absolute error: 49.39800543970899
Root relative squared error: 99.1225401471815
Weighted TruePositiveRate: 0.6716
Weighted MatthewsCorrelation: 0.5191997579233878
Weighted FMeasure: 0.6623047815566749
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5633089121315895
Mean absolute error: 0.2195466908431521
Coverage of cases: 67.16
Instances selection time: 1037.0
Test time: 1927.0
Accumulative iteration time: 29579.0
Weighted Recall: 0.6716
Weighted FalsePositiveRate: 0.16374966199783025
Kappa statistic: 0.5077688583283231
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.88
Correctly Classified Instances: 67.12
Weighted Precision: 0.6863421702153655
Weighted AreaUnderROC: 0.7536263046854842
Root mean squared error: 0.4675639117021091
Relative absolute error: 49.45538735529804
Root relative squared error: 99.18528378080062
Weighted TruePositiveRate: 0.6712
Weighted MatthewsCorrelation: 0.518788114067726
Weighted FMeasure: 0.6617920179341195
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5629766996277609
Mean absolute error: 0.21980172157910347
Coverage of cases: 67.12
Instances selection time: 1037.0
Test time: 1957.0
Accumulative iteration time: 30616.0
Weighted Recall: 0.6712
Weighted FalsePositiveRate: 0.16394739062903171
Kappa statistic: 0.5071737988170733
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.08
Correctly Classified Instances: 66.92
Weighted Precision: 0.685023804325085
Weighted AreaUnderROC: 0.7521319831074805
Root mean squared error: 0.4689947084499462
Relative absolute error: 49.752230971128064
Root relative squared error: 99.4888016056692
Weighted TruePositiveRate: 0.6692
Weighted MatthewsCorrelation: 0.5162166709378744
Weighted FMeasure: 0.6596498769362279
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5610774851729421
Mean absolute error: 0.221121026538348
Coverage of cases: 66.92
Instances selection time: 1035.0
Test time: 2008.0
Accumulative iteration time: 31651.0
Weighted Recall: 0.6692
Weighted FalsePositiveRate: 0.16493603378503885
Kappa statistic: 0.5041783317657295
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.16
Correctly Classified Instances: 66.84
Weighted Precision: 0.6843925105551165
Weighted AreaUnderROC: 0.7515342544762791
Root mean squared error: 0.46957203492028843
Relative absolute error: 49.86964746345582
Root relative squared error: 99.61127104431044
Weighted TruePositiveRate: 0.6684
Weighted MatthewsCorrelation: 0.5151359180396972
Weighted FMeasure: 0.6587840330728267
Iteration time: 1045.0
Weighted AreaUnderPRC: 0.5603023583663315
Mean absolute error: 0.22164287761536025
Coverage of cases: 66.84
Instances selection time: 1045.0
Test time: 2017.0
Accumulative iteration time: 32696.0
Weighted Recall: 0.6684
Weighted FalsePositiveRate: 0.16533149104744171
Kappa statistic: 0.5029815113918555
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.689954340408327
Weighted AreaUnderROC: 0.7556771571534818
Root mean squared error: 0.4656002805624761
Relative absolute error: 49.02958579881573
Root relative squared error: 98.76873471242554
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.5228489370945409
Weighted FMeasure: 0.6654902978059779
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5660037590558542
Mean absolute error: 0.21790927021695983
Coverage of cases: 67.4
Instances selection time: 1035.0
Test time: 2056.0
Accumulative iteration time: 33731.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.16264568569303636
Kappa statistic: 0.5112904498598394
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.6938594367939925
Weighted AreaUnderROC: 0.7592463632151873
Root mean squared error: 0.4621695759850915
Relative absolute error: 48.30922693266835
Root relative squared error: 98.04097237115064
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5293238168131026
Weighted FMeasure: 0.670500737564227
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5706081382337834
Mean absolute error: 0.21470767525630477
Coverage of cases: 67.88
Instances selection time: 1035.0
Test time: 2081.0
Accumulative iteration time: 34766.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.16030727356962524
Kappa statistic: 0.5184378916815093
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.92
Correctly Classified Instances: 68.08
Weighted Precision: 0.6963023399998618
Weighted AreaUnderROC: 0.7607166527774867
Root mean squared error: 0.4607378235467479
Relative absolute error: 48.007849550284504
Root relative squared error: 97.7372518137107
Weighted TruePositiveRate: 0.6808
Weighted MatthewsCorrelation: 0.5323862168391046
Weighted FMeasure: 0.6724032748084603
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5726960016818713
Mean absolute error: 0.2133682202234877
Coverage of cases: 68.08
Instances selection time: 1037.0
Test time: 2113.0
Accumulative iteration time: 35803.0
Weighted Recall: 0.6808
Weighted FalsePositiveRate: 0.15936669444502669
Kappa statistic: 0.521394964547242
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.693579479479874
Weighted AreaUnderROC: 0.7588857022877757
Root mean squared error: 0.46247576876399554
Relative absolute error: 48.36492357200286
Root relative squared error: 98.10592566824465
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5285094319919439
Weighted FMeasure: 0.6696753023959894
Iteration time: 1036.0
Weighted AreaUnderPRC: 0.5700820664814517
Mean absolute error: 0.21495521587556926
Coverage of cases: 67.84
Instances selection time: 1036.0
Test time: 2145.0
Accumulative iteration time: 36839.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16062859542444877
Kappa statistic: 0.5177634588979719
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.6948722953211488
Weighted AreaUnderROC: 0.7600639938246754
Root mean squared error: 0.46133269677214916
Relative absolute error: 48.12351543942939
Root relative squared error: 97.86344348119897
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.5305706751945782
Weighted FMeasure: 0.6713026070007309
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.571606748585344
Mean absolute error: 0.21388229084190938
Coverage of cases: 68.0
Instances selection time: 1035.0
Test time: 2177.0
Accumulative iteration time: 37874.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.15987201235064916
Kappa statistic: 0.520132021278306
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.8
Correctly Classified Instances: 68.2
Weighted Precision: 0.6969098050600154
Weighted AreaUnderROC: 0.7615445828222767
Root mean squared error: 0.4598972731261664
Relative absolute error: 47.82229150428593
Root relative squared error: 97.55894414301399
Weighted TruePositiveRate: 0.682
Weighted MatthewsCorrelation: 0.5334022747591084
Weighted FMeasure: 0.673388352022213
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.573644477396242
Mean absolute error: 0.21254351779682737
Coverage of cases: 68.2
Instances selection time: 1035.0
Test time: 2208.0
Accumulative iteration time: 38909.0
Weighted Recall: 0.682
Weighted FalsePositiveRate: 0.15891083435544687
Kappa statistic: 0.5231029334628414
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.76
Correctly Classified Instances: 68.24
Weighted Precision: 0.6987923665857958
Weighted AreaUnderROC: 0.7617747842358651
Root mean squared error: 0.4596161669430146
Relative absolute error: 47.76055257098946
Root relative squared error: 97.49931251651195
Weighted TruePositiveRate: 0.6824
Weighted MatthewsCorrelation: 0.5343255922710752
Weighted FMeasure: 0.6731716629444764
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.574218620902578
Mean absolute error: 0.21226912253773195
Coverage of cases: 68.24
Instances selection time: 1037.0
Test time: 2240.0
Accumulative iteration time: 39946.0
Weighted Recall: 0.6824
Weighted FalsePositiveRate: 0.15885043152826997
Kappa statistic: 0.5236159302832913
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.64
Correctly Classified Instances: 68.36
Weighted Precision: 0.7025599038271474
Weighted AreaUnderROC: 0.7626439120218623
Root mean squared error: 0.45875502078707314
Relative absolute error: 47.579138321996695
Root relative squared error: 97.31663583057427
Weighted TruePositiveRate: 0.6836
Weighted MatthewsCorrelation: 0.5371559553373201
Weighted FMeasure: 0.6740600783105297
Iteration time: 1038.0
Weighted AreaUnderPRC: 0.5759637508697173
Mean absolute error: 0.21146283698665297
Coverage of cases: 68.36
Instances selection time: 1038.0
Test time: 2271.0
Accumulative iteration time: 40984.0
Weighted Recall: 0.6836
Weighted FalsePositiveRate: 0.15831217595627545
Kappa statistic: 0.5253747499366966
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.16
Correctly Classified Instances: 68.84
Weighted Precision: 0.7067000470728139
Weighted AreaUnderROC: 0.7662302838090709
Root mean squared error: 0.4552695975525463
Relative absolute error: 46.858972449738495
Root relative squared error: 96.57726590924254
Weighted TruePositiveRate: 0.6884
Weighted MatthewsCorrelation: 0.5439233669364196
Weighted FMeasure: 0.6787682820107248
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.580726561383917
Mean absolute error: 0.20826209977661653
Coverage of cases: 68.84
Instances selection time: 1033.0
Test time: 2305.0
Accumulative iteration time: 42017.0
Weighted Recall: 0.6884
Weighted FalsePositiveRate: 0.15593943238185826
Kappa statistic: 0.5325495527477325
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.7088343307440552
Weighted AreaUnderROC: 0.7668039804245681
Root mean squared error: 0.45469222968450485
Relative absolute error: 46.73749082905417
Root relative squared error: 96.45478768882315
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5456118808076403
Weighted FMeasure: 0.6791449243995821
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5817627134249101
Mean absolute error: 0.20772218146246396
Coverage of cases: 68.92
Instances selection time: 1030.0
Test time: 2339.0
Accumulative iteration time: 43047.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.15559203915086395
Kappa statistic: 0.5337156584883074
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.711211555531415
Weighted AreaUnderROC: 0.7676834076458671
Root mean squared error: 0.4538208052806374
Relative absolute error: 46.556182212581255
Root relative squared error: 96.26993065724331
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.5478378620577438
Weighted FMeasure: 0.6800461254933338
Iteration time: 1046.0
Weighted AreaUnderPRC: 0.5832091793170868
Mean absolute error: 0.20691636538925098
Coverage of cases: 69.04
Instances selection time: 1046.0
Test time: 2404.0
Accumulative iteration time: 44093.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.1550331847082658
Kappa statistic: 0.535488442904448
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.7106302852801752
Weighted AreaUnderROC: 0.7664433194971564
Root mean squared error: 0.4549989691231393
Relative absolute error: 46.794012829650654
Root relative squared error: 96.51985694995788
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.5457303674428835
Weighted FMeasure: 0.6776337388839899
Iteration time: 1019.0
Weighted AreaUnderPRC: 0.5815813735528775
Mean absolute error: 0.20797339035400386
Coverage of cases: 68.88
Instances selection time: 1019.0
Test time: 2406.0
Accumulative iteration time: 45112.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.15591336100568748
Kappa statistic: 0.5330410098030177
Training time: 0.0
		
Time end:Wed Nov 01 15.21.26 EET 2017