Wed Nov 01 17.17.57 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.17.57 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7358068683007442
Weighted AreaUnderROC: 0.801966794620587
Root mean squared error: 0.4182800533946965
Relative absolute error: 39.96023856858833
Root relative squared error: 88.7305986571381
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6038339747867176
Weighted FMeasure: 0.7358064485302517
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6298750486648425
Mean absolute error: 0.17760106030483785
Coverage of cases: 73.6
Instances selection time: 10.0
Test time: 995.0
Accumulative iteration time: 10.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13206641075882625
Kappa statistic: 0.6040186586408048
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.44
Correctly Classified Instances: 73.56
Weighted Precision: 0.7353094791389849
Weighted AreaUnderROC: 0.801695395465791
Root mean squared error: 0.4186441073745946
Relative absolute error: 40.00611854684434
Root relative squared error: 88.8078261685093
Weighted TruePositiveRate: 0.7356
Weighted MatthewsCorrelation: 0.6032515005373341
Weighted FMeasure: 0.7351741520634864
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6294362312197715
Mean absolute error: 0.177804971319309
Coverage of cases: 73.56
Instances selection time: 10.0
Test time: 1025.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7356
Weighted FalsePositiveRate: 0.13220920906841788
Kappa statistic: 0.6034602740383462
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7369022222757969
Weighted AreaUnderROC: 0.802880553292892
Root mean squared error: 0.41741924648003387
Relative absolute error: 39.75469613259702
Root relative squared error: 88.54799393514304
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6056266530461298
Weighted FMeasure: 0.7368012201207894
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6312935934777452
Mean absolute error: 0.1766875383670987
Coverage of cases: 73.72
Instances selection time: 9.0
Test time: 1041.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.13143889341421583
Kappa statistic: 0.605849126964815
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.08
Correctly Classified Instances: 73.92
Weighted Precision: 0.7389502538103925
Weighted AreaUnderROC: 0.8043680085806946
Root mean squared error: 0.4158683819044765
Relative absolute error: 39.44440497335639
Root relative squared error: 88.21900587771947
Weighted TruePositiveRate: 0.7392
Weighted MatthewsCorrelation: 0.6086119626280302
Weighted FMeasure: 0.7389312803867207
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6335745855854551
Mean absolute error: 0.17530846654825144
Coverage of cases: 73.92
Instances selection time: 10.0
Test time: 1074.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7392
Weighted FalsePositiveRate: 0.1304639828386111
Kappa statistic: 0.6088309180042409
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.737779738488839
Weighted AreaUnderROC: 0.8034817150691941
Root mean squared error: 0.4168617895978935
Relative absolute error: 39.612349914235864
Root relative squared error: 88.42973947266889
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6068559126705407
Weighted FMeasure: 0.7376798068886136
Iteration time: 10.0
Weighted AreaUnderPRC: 0.632174477267831
Mean absolute error: 0.17605488850771578
Coverage of cases: 73.8
Instances selection time: 10.0
Test time: 1107.0
Accumulative iteration time: 49.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13103656986161172
Kappa statistic: 0.607041810751336
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.16
Correctly Classified Instances: 73.84
Weighted Precision: 0.7381884504352395
Weighted AreaUnderROC: 0.8037737130945936
Root mean squared error: 0.41657873052085637
Relative absolute error: 39.5422885572147
Root relative squared error: 88.36969357481406
Weighted TruePositiveRate: 0.7384
Weighted MatthewsCorrelation: 0.6074397897151624
Weighted FMeasure: 0.7381358157719196
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6326381125489299
Mean absolute error: 0.17574350469873282
Coverage of cases: 73.84
Instances selection time: 9.0
Test time: 1140.0
Accumulative iteration time: 58.0
Weighted Recall: 0.7384
Weighted FalsePositiveRate: 0.13085257381081275
Kappa statistic: 0.6076328018977613
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 25.68
Correctly Classified Instances: 74.32
Weighted Precision: 0.7430517599503732
Weighted AreaUnderROC: 0.8073600848818022
Root mean squared error: 0.4127720373912449
Relative absolute error: 38.8160513643666
Root relative squared error: 87.56217201706072
Weighted TruePositiveRate: 0.7432
Weighted MatthewsCorrelation: 0.6146363759562046
Weighted FMeasure: 0.7430552931693847
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6381455536750473
Mean absolute error: 0.17251578384163013
Coverage of cases: 74.32
Instances selection time: 9.0
Test time: 1186.0
Accumulative iteration time: 67.0
Weighted Recall: 0.7432
Weighted FalsePositiveRate: 0.12847983023639553
Kappa statistic: 0.6148128806572708
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7402257027698722
Weighted AreaUnderROC: 0.8052577352372953
Root mean squared error: 0.4150470322925982
Relative absolute error: 39.22488335925503
Root relative squared error: 88.04477131363424
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6104171898713868
Weighted FMeasure: 0.7402573782790796
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6349367893740002
Mean absolute error: 0.1743328149300232
Coverage of cases: 74.04
Instances selection time: 9.0
Test time: 1223.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.12988452952540924
Kappa statistic: 0.6106130211005744
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.739333751292822
Weighted AreaUnderROC: 0.8046909049119995
Root mean squared error: 0.41571508217696435
Relative absolute error: 39.335746606335995
Root relative squared error: 88.18648609465609
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.609249795470601
Weighted FMeasure: 0.7392263619137631
Iteration time: 8.0
Weighted AreaUnderPRC: 0.633993605824539
Mean absolute error: 0.17482554047260526
Coverage of cases: 73.96
Instances selection time: 8.0
Test time: 1241.0
Accumulative iteration time: 84.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.1302181901760011
Kappa statistic: 0.609452239668422
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7377178393219856
Weighted AreaUnderROC: 0.8034851482142948
Root mean squared error: 0.41701768607494394
Relative absolute error: 39.56661786237202
Root relative squared error: 88.46281010949451
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6068333629937706
Weighted FMeasure: 0.7376332498421431
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6321782379784476
Mean absolute error: 0.17585163494387648
Coverage of cases: 73.8
Instances selection time: 9.0
Test time: 1267.0
Accumulative iteration time: 93.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.1310297035714105
Kappa statistic: 0.6070489774154549
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.12
Correctly Classified Instances: 73.88
Weighted Precision: 0.738510010531354
Weighted AreaUnderROC: 0.8040691442650938
Root mean squared error: 0.41640638708893263
Relative absolute error: 39.439544807963664
Root relative squared error: 88.3331340119922
Weighted TruePositiveRate: 0.7388
Weighted MatthewsCorrelation: 0.6079922469752451
Weighted FMeasure: 0.7385155873341424
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6331234824155386
Mean absolute error: 0.17528686581317268
Coverage of cases: 73.88
Instances selection time: 8.0
Test time: 1303.0
Accumulative iteration time: 101.0
Weighted Recall: 0.7388
Weighted FalsePositiveRate: 0.13066171146981254
Kappa statistic: 0.6082345380431261
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7377569149842935
Weighted AreaUnderROC: 0.8034508167632887
Root mean squared error: 0.4170680217758266
Relative absolute error: 39.55186721991886
Root relative squared error: 88.47348792412349
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6067797668132766
Weighted FMeasure: 0.7378006669970016
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6322481578239478
Mean absolute error: 0.17578607653297354
Coverage of cases: 73.8
Instances selection time: 8.0
Test time: 1340.0
Accumulative iteration time: 109.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.13109836647342277
Kappa statistic: 0.6070167253681683
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.76
Correctly Classified Instances: 73.24
Weighted Precision: 0.7324279676506316
Weighted AreaUnderROC: 0.7992049197330676
Root mean squared error: 0.4215249694288701
Relative absolute error: 40.3816958277238
Root relative squared error: 89.41894929678163
Weighted TruePositiveRate: 0.7324
Weighted MatthewsCorrelation: 0.5984300088693639
Weighted FMeasure: 0.732389971301836
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6260434338867091
Mean absolute error: 0.17947420367877331
Coverage of cases: 73.24
Instances selection time: 8.0
Test time: 1367.0
Accumulative iteration time: 117.0
Weighted Recall: 0.7324
Weighted FalsePositiveRate: 0.1339901605338649
Kappa statistic: 0.5985749831929528
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7316325751050051
Weighted AreaUnderROC: 0.7986071911018662
Root mean squared error: 0.42217677588742564
Relative absolute error: 40.49488859763877
Root relative squared error: 89.55721832684138
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5972381877010244
Weighted FMeasure: 0.7315939445278173
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6251030023044063
Mean absolute error: 0.17997728265617316
Coverage of cases: 73.16
Instances selection time: 7.0
Test time: 1397.0
Accumulative iteration time: 124.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.1343856177962678
Kappa statistic: 0.5973730723110762
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7345406406607472
Weighted AreaUnderROC: 0.8006992413110712
Root mean squared error: 0.41998989569797995
Relative absolute error: 40.07049808429017
Root relative squared error: 89.09331098336153
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6014952903894784
Weighted FMeasure: 0.7344047655372373
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6282639726049629
Mean absolute error: 0.17809110259684605
Coverage of cases: 73.44
Instances selection time: 8.0
Test time: 1429.0
Accumulative iteration time: 132.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13300151737785773
Kappa statistic: 0.6015588171193376
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.68
Correctly Classified Instances: 73.32
Weighted Precision: 0.7333490081839372
Weighted AreaUnderROC: 0.7997992152191684
Root mean squared error: 0.42095755161975384
Relative absolute error: 40.244084682441816
Root relative squared error: 89.298581802604
Weighted TruePositiveRate: 0.7332
Weighted MatthewsCorrelation: 0.5996910513354323
Weighted FMeasure: 0.7332425718266522
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6268935959649306
Mean absolute error: 0.17886259858863113
Coverage of cases: 73.32
Instances selection time: 7.0
Test time: 1461.0
Accumulative iteration time: 139.0
Weighted Recall: 0.7332
Weighted FalsePositiveRate: 0.13360156956166327
Kappa statistic: 0.599756805754477
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.732088607658129
Weighted AreaUnderROC: 0.7989026222723662
Root mean squared error: 0.4219222009106628
Relative absolute error: 40.41798298906497
Root relative squared error: 89.50321481912457
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.5978636916728588
Weighted FMeasure: 0.732009067767725
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6255545121894263
Mean absolute error: 0.1796354799514007
Coverage of cases: 73.2
Instances selection time: 7.0
Test time: 1494.0
Accumulative iteration time: 146.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13419475545526757
Kappa statistic: 0.5979657788470955
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.73090342778715
Weighted AreaUnderROC: 0.7980025961804634
Root mean squared error: 0.4228839197509309
Relative absolute error: 40.592170818506105
Root relative squared error: 89.7072261931891
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.5960638027592569
Weighted FMeasure: 0.730843223968669
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6241876764578043
Mean absolute error: 0.18040964808225018
Coverage of cases: 73.08
Instances selection time: 7.0
Test time: 1530.0
Accumulative iteration time: 153.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.13479480763907312
Kappa statistic: 0.5961637839681463
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7328553789529909
Weighted AreaUnderROC: 0.7995106503388695
Root mean squared error: 0.4213274065424484
Relative absolute error: 40.28829663963106
Root relative squared error: 89.37703987977177
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5990505357988326
Weighted FMeasure: 0.7328243064185899
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6264295563941865
Mean absolute error: 0.17905909617613888
Coverage of cases: 73.28
Instances selection time: 7.0
Test time: 1563.0
Accumulative iteration time: 160.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13377869932226102
Kappa statistic: 0.5991713648023015
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.7299887213934234
Weighted AreaUnderROC: 0.7970682386375545
Root mean squared error: 0.42385938029121156
Relative absolute error: 40.76194790487048
Root relative squared error: 89.9141526220328
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5943603688079578
Weighted FMeasure: 0.7297147680329699
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6230321885308131
Mean absolute error: 0.18116421291053633
Coverage of cases: 72.96
Instances selection time: 7.0
Test time: 1593.0
Accumulative iteration time: 167.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.1354635227248909
Kappa statistic: 0.594335872614743
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7292631010298039
Weighted AreaUnderROC: 0.7964636437161519
Root mean squared error: 0.42450181189838804
Relative absolute error: 40.877076411960275
Root relative squared error: 90.05043294579771
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5931782598455522
Weighted FMeasure: 0.7289644993567379
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6222075341065618
Mean absolute error: 0.1816758951642687
Coverage of cases: 72.88
Instances selection time: 6.0
Test time: 1626.0
Accumulative iteration time: 173.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.1358727125676962
Kappa statistic: 0.5931338272856697
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7254691423483999
Weighted AreaUnderROC: 0.7937772980208462
Root mean squared error: 0.4273252835869589
Relative absolute error: 41.41105092091076
Root relative squared error: 90.64938173904073
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5877011377477567
Weighted FMeasure: 0.7253050476422442
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6180876587072536
Mean absolute error: 0.18404911520404868
Coverage of cases: 72.52
Instances selection time: 7.0
Test time: 1647.0
Accumulative iteration time: 180.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.13764540395830788
Kappa statistic: 0.5877479902864545
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.7299293746805885
Weighted AreaUnderROC: 0.7970751049277558
Root mean squared error: 0.4239049738917688
Relative absolute error: 40.74909862142021
Root relative squared error: 89.92382448527262
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5943236766669534
Weighted FMeasure: 0.7297303836168814
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6230855996458653
Mean absolute error: 0.18110710498409066
Coverage of cases: 72.96
Instances selection time: 7.0
Test time: 1679.0
Accumulative iteration time: 187.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.13544979014448844
Kappa statistic: 0.5943469730363153
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.72663578615728
Weighted AreaUnderROC: 0.7946876235480506
Root mean squared error: 0.42641990143492814
Relative absolute error: 41.223676012460395
Root relative squared error: 90.45732118126085
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5894948313028553
Weighted FMeasure: 0.7264961521708119
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6194312971502102
Mean absolute error: 0.18321633783315816
Coverage of cases: 72.64
Instances selection time: 6.0
Test time: 1711.0
Accumulative iteration time: 193.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13702475290389868
Kappa statistic: 0.5895575766711248
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7267862171705308
Weighted AreaUnderROC: 0.795007086734255
Root mean squared error: 0.42612152452909857
Relative absolute error: 41.16012207527832
Root relative squared error: 90.39402588122239
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5900045361903699
Weighted FMeasure: 0.7267918943268437
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6198344589071694
Mean absolute error: 0.18293387589012675
Coverage of cases: 72.68
Instances selection time: 6.0
Test time: 1763.0
Accumulative iteration time: 199.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.1367858265314899
Kappa statistic: 0.5901800663584276
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7315617754503514
Weighted AreaUnderROC: 0.7986140573920673
Root mean squared error: 0.42237439194435955
Relative absolute error: 40.438683948156324
Root relative squared error: 89.59913902302019
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5972090447049091
Weighted FMeasure: 0.7315801880913586
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6251648297741228
Mean absolute error: 0.17972748421402893
Coverage of cases: 73.16
Instances selection time: 6.0
Test time: 1781.0
Accumulative iteration time: 205.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13437188521586532
Kappa statistic: 0.5973804165834626
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7320902504236646
Weighted AreaUnderROC: 0.7989026222723662
Root mean squared error: 0.4220718286938175
Relative absolute error: 40.37536656891373
Root relative squared error: 89.53495566516133
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.5978501553103545
Weighted FMeasure: 0.73204141022767
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6256603674126069
Mean absolute error: 0.17944607363961745
Coverage of cases: 73.2
Instances selection time: 6.0
Test time: 1810.0
Accumulative iteration time: 211.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13419475545526757
Kappa statistic: 0.5979694456778715
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7343411310512942
Weighted AreaUnderROC: 0.8007129738914737
Root mean squared error: 0.42018948709033316
Relative absolute error: 40.013039309685006
Root relative squared error: 89.13565071146135
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6014009778511068
Weighted FMeasure: 0.7343635403943944
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6282895971632705
Mean absolute error: 0.17783573026526753
Coverage of cases: 73.44
Instances selection time: 6.0
Test time: 1848.0
Accumulative iteration time: 217.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13297405221705283
Kappa statistic: 0.6015806208813997
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7328033315196003
Weighted AreaUnderROC: 0.7995140834839701
Root mean squared error: 0.4214645597976874
Relative absolute error: 40.249106302918136
Root relative squared error: 89.40613447882416
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5990320156609021
Weighted FMeasure: 0.7328011345652697
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6264598318717383
Mean absolute error: 0.17888491690185923
Coverage of cases: 73.28
Instances selection time: 6.0
Test time: 1883.0
Accumulative iteration time: 223.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.1337718330320598
Kappa statistic: 0.5991750205872831
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7255662667919925
Weighted AreaUnderROC: 0.7941070606423521
Root mean squared error: 0.42711625816513465
Relative absolute error: 41.322991689752165
Root relative squared error: 90.60504075107704
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5882116032115189
Weighted FMeasure: 0.7255647734201004
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6183994718878721
Mean absolute error: 0.18365774084334383
Coverage of cases: 72.56
Instances selection time: 6.0
Test time: 1906.0
Accumulative iteration time: 229.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13738587871529542
Kappa statistic: 0.5883724703508171
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7243077275857391
Weighted AreaUnderROC: 0.7932104676955501
Root mean squared error: 0.42805986641165467
Relative absolute error: 41.499546690842976
Root relative squared error: 90.80521028804638
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5863840893970471
Weighted FMeasure: 0.7243366261511528
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6170653569193714
Mean absolute error: 0.18444242973708075
Coverage of cases: 72.44
Instances selection time: 5.0
Test time: 1933.0
Accumulative iteration time: 234.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13797906460889972
Kappa statistic: 0.5865817765247092
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7269636775689869
Weighted AreaUnderROC: 0.794989921008752
Root mean squared error: 0.42620225872274015
Relative absolute error: 41.13766696349006
Root relative squared error: 90.41115218996167
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5900697684682166
Weighted FMeasure: 0.7268684330844135
Iteration time: 5.0
Weighted AreaUnderPRC: 0.619796546219479
Mean absolute error: 0.18283407539329002
Coverage of cases: 72.68
Instances selection time: 5.0
Test time: 1967.0
Accumulative iteration time: 239.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13682015798249603
Kappa statistic: 0.5901576386935354
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7287222194274217
Weighted AreaUnderROC: 0.7965220071828624
Root mean squared error: 0.42464925548604016
Relative absolute error: 40.835695538056676
Root relative squared error: 90.0817104539991
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5930038718977924
Weighted FMeasure: 0.7287567704386234
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6219016742133812
Mean absolute error: 0.18149198016914161
Coverage of cases: 72.88
Instances selection time: 5.0
Test time: 2008.0
Accumulative iteration time: 244.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13575598563427535
Kappa statistic: 0.593185777774791
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7274102724954411
Weighted AreaUnderROC: 0.7956391468164625
Root mean squared error: 0.42559726853857094
Relative absolute error: 41.01255374032522
Root relative squared error: 90.28281439142846
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5911779929087687
Weighted FMeasure: 0.7274720183191168
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6205368296660838
Mean absolute error: 0.18227801662366847
Coverage of cases: 72.76
Instances selection time: 4.0
Test time: 2038.0
Accumulative iteration time: 249.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13632170636707475
Kappa statistic: 0.5914062106255985
Training time: 1.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7262894529090992
Weighted AreaUnderROC: 0.7947425538696605
Root mean squared error: 0.42654293601145166
Relative absolute error: 41.18951817413356
Root relative squared error: 90.48342075627491
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5894196627325771
Weighted FMeasure: 0.7263033554782005
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6191749413562746
Mean absolute error: 0.18306452521837224
Coverage of cases: 72.64
Instances selection time: 4.0
Test time: 2069.0
Accumulative iteration time: 253.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13691489226067907
Kappa statistic: 0.5896043666095393
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.724267108433735
Weighted AreaUnderROC: 0.7932482322916569
Root mean squared error: 0.42810807915765103
Relative absolute error: 41.4862842892777
Root relative squared error: 90.81543775593649
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.58641883196343
Weighted FMeasure: 0.7242964441636212
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6169002978313253
Mean absolute error: 0.18438348573012397
Coverage of cases: 72.44
Instances selection time: 5.0
Test time: 2102.0
Accumulative iteration time: 258.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13790353541668623
Kappa statistic: 0.5866062835844895
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7251025904557921
Weighted AreaUnderROC: 0.793849394067959
Root mean squared error: 0.4274949711829882
Relative absolute error: 41.36418642681752
Root relative squared error: 90.68537791399139
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5876386988364898
Weighted FMeasure: 0.7251321158129546
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6177558676081076
Mean absolute error: 0.18384082856363426
Coverage of cases: 72.52
Instances selection time: 5.0
Test time: 2135.0
Accumulative iteration time: 263.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.13750121186408215
Kappa statistic: 0.5877987469081905
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7279489951292668
Weighted AreaUnderROC: 0.7959380111320634
Root mean squared error: 0.4253198525179922
Relative absolute error: 40.94288012872006
Root relative squared error: 90.22396556662017
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.5918437871711525
Weighted FMeasure: 0.7279691737646592
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6209041510309498
Mean absolute error: 0.1819683561276456
Coverage of cases: 72.8
Instances selection time: 4.0
Test time: 2166.0
Accumulative iteration time: 267.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.1361239777358733
Kappa statistic: 0.5919875964229312
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.729550252564346
Weighted AreaUnderROC: 0.797143767829768
Root mean squared error: 0.4240751508820373
Relative absolute error: 40.70118764845585
Root relative squared error: 89.95992447641885
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5942530687751609
Weighted FMeasure: 0.729562492495712
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6226785677465649
Mean absolute error: 0.18089416732647132
Coverage of cases: 72.96
Instances selection time: 4.0
Test time: 2198.0
Accumulative iteration time: 271.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.13531246434046393
Kappa statistic: 0.5943913686483249
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.16
Correctly Classified Instances: 72.84
Weighted Precision: 0.7283149410070606
Weighted AreaUnderROC: 0.7962300091574627
Root mean squared error: 0.4250229393468054
Relative absolute error: 40.878565861260945
Root relative squared error: 90.16098077158922
Weighted TruePositiveRate: 0.7284
Weighted MatthewsCorrelation: 0.5924149867586437
Weighted FMeasure: 0.7283521077627112
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6214196517539947
Mean absolute error: 0.1816825149389384
Coverage of cases: 72.84
Instances selection time: 4.0
Test time: 2233.0
Accumulative iteration time: 275.0
Weighted Recall: 0.7284
Weighted FalsePositiveRate: 0.13593998168507435
Kappa statistic: 0.5925876146634859
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.96
Correctly Classified Instances: 73.04
Weighted Precision: 0.730298300074989
Weighted AreaUnderROC: 0.797727763880567
Root mean squared error: 0.4234627457609357
Relative absolute error: 40.57712970069058
Root relative squared error: 89.83001373222956
Weighted TruePositiveRate: 0.7304
Weighted MatthewsCorrelation: 0.5954059403655879
Weighted FMeasure: 0.7303380471176579
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6236683653231933
Mean absolute error: 0.18034279866973674
Coverage of cases: 73.04
Instances selection time: 5.0
Test time: 2261.0
Accumulative iteration time: 280.0
Weighted Recall: 0.7304
Weighted FalsePositiveRate: 0.13494447223886594
Kappa statistic: 0.5955877058662584
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7314730020622379
Weighted AreaUnderROC: 0.7986312231175704
Root mean squared error: 0.4225266044352991
Relative absolute error: 40.39546485260936
Root relative squared error: 89.63142816837758
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5972002547611602
Weighted FMeasure: 0.7315194344326992
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6250126177686123
Mean absolute error: 0.17953539934493132
Coverage of cases: 73.16
Instances selection time: 4.0
Test time: 2298.0
Accumulative iteration time: 284.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.1343375537648592
Kappa statistic: 0.5973914324896834
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7319076556317575
Weighted AreaUnderROC: 0.7989300874331712
Root mean squared error: 0.42221874550028676
Relative absolute error: 40.333581533878245
Root relative squared error: 89.56612142619875
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.5978212947627384
Weighted FMeasure: 0.731926924073676
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6254355594728594
Mean absolute error: 0.17926036237279305
Coverage of cases: 73.2
Instances selection time: 3.0
Test time: 2331.0
Accumulative iteration time: 287.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13413982513365777
Kappa statistic: 0.5979859455886578
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7326207240148936
Weighted AreaUnderROC: 0.7995484149349762
Root mean squared error: 0.4215949876257764
Relative absolute error: 40.211885546589215
Root relative squared error: 89.43380239933332
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5990151556477922
Weighted FMeasure: 0.7326448980598849
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6262886258063569
Mean absolute error: 0.17871949131817513
Coverage of cases: 73.28
Instances selection time: 4.0
Test time: 2361.0
Accumulative iteration time: 291.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13370317013004754
Kappa statistic: 0.5992024368491841
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.7307897092869862
Weighted AreaUnderROC: 0.7980609596471738
Root mean squared error: 0.4231765750709601
Relative absolute error: 40.50932754880617
Root relative squared error: 89.769307761592
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.596097611114917
Weighted FMeasure: 0.7307651134298372
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6240173413211664
Mean absolute error: 0.1800414557724727
Coverage of cases: 73.08
Instances selection time: 3.0
Test time: 2427.0
Accumulative iteration time: 294.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.13467808070565224
Kappa statistic: 0.5961987724442682
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7290024729137075
Weighted AreaUnderROC: 0.7965254403279629
Root mean squared error: 0.42475218175595675
Relative absolute error: 40.80684248039835
Root relative squared error: 90.10354441302518
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5931511199697949
Weighted FMeasure: 0.7288705113119087
Iteration time: 4.0
Weighted AreaUnderPRC: 0.621887376996352
Mean absolute error: 0.18136374435732686
Coverage of cases: 72.88
Instances selection time: 3.0
Test time: 2432.0
Accumulative iteration time: 298.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13574911934407413
Kappa statistic: 0.5931653702363145
Training time: 1.0
		
Time end:Wed Nov 01 17.19.20 EET 2017