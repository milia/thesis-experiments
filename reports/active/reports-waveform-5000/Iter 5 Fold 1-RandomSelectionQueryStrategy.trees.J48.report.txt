Wed Nov 01 17.26.17 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.26.17 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 35.480000000000885
Incorrectly Classified Instances: 30.48
Correctly Classified Instances: 69.52
Weighted Precision: 0.6966188801862315
Weighted AreaUnderROC: 0.7604097452172675
Root mean squared error: 0.44459831838185315
Relative absolute error: 46.85342619745833
Root relative squared error: 94.31354574958299
Weighted TruePositiveRate: 0.6952
Weighted MatthewsCorrelation: 0.5429288541439257
Weighted FMeasure: 0.6957692217306076
Iteration time: 96.0
Weighted AreaUnderPRC: 0.5893034370310732
Mean absolute error: 0.20823744976648245
Coverage of cases: 71.64
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 96.0
Weighted Recall: 0.6952
Weighted FalsePositiveRate: 0.15292136016149513
Kappa statistic: 0.5427026871318218
Training time: 86.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 37.346666666667616
Incorrectly Classified Instances: 29.28
Correctly Classified Instances: 70.72
Weighted Precision: 0.7072184418885521
Weighted AreaUnderROC: 0.8031688755401784
Root mean squared error: 0.432518652028367
Relative absolute error: 44.44080672268927
Root relative squared error: 91.75106155167667
Weighted TruePositiveRate: 0.7072
Weighted MatthewsCorrelation: 0.5605203308732922
Weighted FMeasure: 0.7071091528520641
Iteration time: 88.0
Weighted AreaUnderPRC: 0.6333175148039054
Mean absolute error: 0.19751469654528656
Coverage of cases: 75.12
Instances selection time: 9.0
Test time: 8.0
Accumulative iteration time: 184.0
Weighted Recall: 0.7072
Weighted FalsePositiveRate: 0.14668738445659818
Kappa statistic: 0.5608006675829853
Training time: 79.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 37.25333333333419
Incorrectly Classified Instances: 31.96
Correctly Classified Instances: 68.04
Weighted Precision: 0.6811063358806021
Weighted AreaUnderROC: 0.7756969750739211
Root mean squared error: 0.454260495941534
Relative absolute error: 48.40063534504707
Root relative squared error: 96.36320313162662
Weighted TruePositiveRate: 0.6804
Weighted MatthewsCorrelation: 0.5206042062932261
Weighted FMeasure: 0.6806238916895548
Iteration time: 100.0
Weighted AreaUnderPRC: 0.5916527831745141
Mean absolute error: 0.21511393486687688
Coverage of cases: 72.8
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 284.0
Weighted Recall: 0.6804
Weighted FalsePositiveRate: 0.1601205925825274
Kappa statistic: 0.5205504440939016
Training time: 90.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 37.160000000000935
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7127652964961269
Weighted AreaUnderROC: 0.8024927343085393
Root mean squared error: 0.4262760018823536
Relative absolute error: 43.6713809523811
Root relative squared error: 90.42679547643031
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5675413512832556
Weighted FMeasure: 0.712008497923426
Iteration time: 102.0
Weighted AreaUnderPRC: 0.6367822996560665
Mean absolute error: 0.19409502645502802
Coverage of cases: 75.76
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 386.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14461536386640078
Kappa statistic: 0.5673177903801723
Training time: 92.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 37.306666666667645
Incorrectly Classified Instances: 28.96
Correctly Classified Instances: 71.04
Weighted Precision: 0.7120832341665696
Weighted AreaUnderROC: 0.7970385214058405
Root mean squared error: 0.4298283852132483
Relative absolute error: 44.38076367663354
Root relative squared error: 91.18036977922522
Weighted TruePositiveRate: 0.7104
Weighted MatthewsCorrelation: 0.565952585197177
Weighted FMeasure: 0.710961690679198
Iteration time: 106.0
Weighted AreaUnderPRC: 0.6266182534067144
Mean absolute error: 0.1972478385628167
Coverage of cases: 75.12
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 492.0
Weighted Recall: 0.7104
Weighted FalsePositiveRate: 0.14527721266201735
Kappa statistic: 0.5654797943303036
Training time: 96.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 39.666666666667574
Incorrectly Classified Instances: 29.48
Correctly Classified Instances: 70.52
Weighted Precision: 0.7056716651202726
Weighted AreaUnderROC: 0.8074541241468635
Root mean squared error: 0.43105246709148665
Relative absolute error: 44.86997097020637
Root relative squared error: 91.44003675827419
Weighted TruePositiveRate: 0.7052
Weighted MatthewsCorrelation: 0.5579201207077789
Weighted FMeasure: 0.704758893276421
Iteration time: 105.0
Weighted AreaUnderPRC: 0.640105946604622
Mean absolute error: 0.19942209320091814
Coverage of cases: 77.96
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 597.0
Weighted Recall: 0.7052
Weighted FalsePositiveRate: 0.1475798995497882
Kappa statistic: 0.5578530930005926
Training time: 96.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 36.360000000001186
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7262621446948493
Weighted AreaUnderROC: 0.8081342858443838
Root mean squared error: 0.41981304398818814
Relative absolute error: 41.663138688897426
Root relative squared error: 89.05579507038405
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5889886331743033
Weighted FMeasure: 0.7257668616008369
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6458980804223876
Mean absolute error: 0.18516950528398943
Coverage of cases: 76.0
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 705.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.1371675512134903
Kappa statistic: 0.5890212393823487
Training time: 99.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 35.38666666666768
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7244307614186289
Weighted AreaUnderROC: 0.7940432642777276
Root mean squared error: 0.4204102392817969
Relative absolute error: 42.11035778358839
Root relative squared error: 89.18247932292509
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5860415623550529
Weighted FMeasure: 0.7239683491804616
Iteration time: 111.0
Weighted AreaUnderPRC: 0.6275842690997447
Mean absolute error: 0.18715714570483818
Coverage of cases: 75.08
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 816.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13814246178909503
Kappa statistic: 0.5860195067608415
Training time: 102.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 39.22666666666776
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7078777433295613
Weighted AreaUnderROC: 0.7972436808085744
Root mean squared error: 0.4239126980457718
Relative absolute error: 45.79395684674806
Root relative squared error: 89.92546302577495
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5627329493998579
Weighted FMeasure: 0.7046482078673907
Iteration time: 110.0
Weighted AreaUnderPRC: 0.6268069326020793
Mean absolute error: 0.203528697096659
Coverage of cases: 76.96
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 926.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14583875204091443
Kappa statistic: 0.5622601575431689
Training time: 101.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 37.76000000000118
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7252506129080235
Weighted AreaUnderROC: 0.8018975674540736
Root mean squared error: 0.41415274568243526
Relative absolute error: 42.2443154323529
Root relative squared error: 87.85506447572308
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5891796151853219
Weighted FMeasure: 0.72504484482433
Iteration time: 116.0
Weighted AreaUnderPRC: 0.6446152903987792
Mean absolute error: 0.18775251303268042
Coverage of cases: 77.72
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 1042.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13682563048806315
Kappa statistic: 0.5897203596352975
Training time: 107.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 41.066666666667615
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7292204106494001
Weighted AreaUnderROC: 0.8226039547663256
Root mean squared error: 0.4065213893831326
Relative absolute error: 42.882444883047
Root relative squared error: 86.2362093390568
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.5931222668392253
Weighted FMeasure: 0.7259545842669981
Iteration time: 121.0
Weighted AreaUnderPRC: 0.6581159192780056
Mean absolute error: 0.19058864392465424
Coverage of cases: 80.6
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1163.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.13584245983762308
Kappa statistic: 0.5922051697350028
Training time: 113.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 37.54666666666741
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7200144744715335
Weighted AreaUnderROC: 0.80779613420634
Root mean squared error: 0.42168805445850943
Relative absolute error: 42.35937362637369
Root relative squared error: 89.45354485589203
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5782558026042899
Weighted FMeasure: 0.7183201068211449
Iteration time: 134.0
Weighted AreaUnderPRC: 0.6400851190553655
Mean absolute error: 0.18826388278388392
Coverage of cases: 76.88
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1297.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14099992439853104
Kappa statistic: 0.5775929373539127
Training time: 126.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 37.17333333333418
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7389849982242794
Weighted AreaUnderROC: 0.8033653378829131
Root mean squared error: 0.4074436382218833
Relative absolute error: 40.99956581713937
Root relative squared error: 86.43184786140341
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6068732558371099
Weighted FMeasure: 0.7377599381155489
Iteration time: 134.0
Weighted AreaUnderPRC: 0.6431698091072714
Mean absolute error: 0.18222029252062028
Coverage of cases: 78.08
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1431.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13140595574784378
Kappa statistic: 0.6063808543647563
Training time: 126.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 36.69333333333419
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7290064168484078
Weighted AreaUnderROC: 0.8172848483381048
Root mean squared error: 0.41396565509513383
Relative absolute error: 41.74798850410741
Root relative squared error: 87.81537656882996
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5918546161646822
Weighted FMeasure: 0.7278153344693595
Iteration time: 135.0
Weighted AreaUnderPRC: 0.6477080405023949
Mean absolute error: 0.18554661557381158
Coverage of cases: 77.36
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1566.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13641783442989192
Kappa statistic: 0.5913801247292668
Training time: 127.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 37.693333333334316
Incorrectly Classified Instances: 28.52
Correctly Classified Instances: 71.48
Weighted Precision: 0.7142872243384334
Weighted AreaUnderROC: 0.817872691607442
Root mean squared error: 0.41592273937098984
Relative absolute error: 43.067715710882666
Root relative squared error: 88.23053683767337
Weighted TruePositiveRate: 0.7148
Weighted MatthewsCorrelation: 0.5718313287533929
Weighted FMeasure: 0.7144001037850123
Iteration time: 138.0
Weighted AreaUnderPRC: 0.6506707561100641
Mean absolute error: 0.1914120698261461
Coverage of cases: 77.44
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1704.0
Weighted Recall: 0.7148
Weighted FalsePositiveRate: 0.14274515062833784
Kappa statistic: 0.5722357610903728
Training time: 130.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 35.98666666666771
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7159265339816818
Weighted AreaUnderROC: 0.787343247003587
Root mean squared error: 0.4235051110401321
Relative absolute error: 43.81757395729267
Root relative squared error: 89.83900076509154
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5728719532994823
Weighted FMeasure: 0.715433084749017
Iteration time: 146.0
Weighted AreaUnderPRC: 0.6218203476321088
Mean absolute error: 0.19474477314352392
Coverage of cases: 75.44
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1850.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.14268474780116092
Kappa statistic: 0.5727383034110125
Training time: 138.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 38.373333333334195
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7196366008739513
Weighted AreaUnderROC: 0.8128536202334394
Root mean squared error: 0.4185866831781535
Relative absolute error: 43.003371761527866
Root relative squared error: 88.79564465689698
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.579706253589745
Weighted FMeasure: 0.7197087199824449
Iteration time: 143.0
Weighted AreaUnderPRC: 0.6420685849899611
Mean absolute error: 0.1911260967179025
Coverage of cases: 77.12
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1993.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.14014034697171304
Kappa statistic: 0.580015959393543
Training time: 136.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 37.04000000000105
Incorrectly Classified Instances: 29.48
Correctly Classified Instances: 70.52
Weighted Precision: 0.7047243660349586
Weighted AreaUnderROC: 0.7823251974058433
Root mean squared error: 0.43537151977184796
Relative absolute error: 45.14584793735304
Root relative squared error: 92.35624618984981
Weighted TruePositiveRate: 0.7052
Weighted MatthewsCorrelation: 0.5574641727925045
Weighted FMeasure: 0.7048238536993228
Iteration time: 154.0
Weighted AreaUnderPRC: 0.6069207984641752
Mean absolute error: 0.20064821305490335
Coverage of cases: 74.68
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 2147.0
Weighted Recall: 0.7052
Weighted FalsePositiveRate: 0.14752496922817837
Kappa statistic: 0.5578369648297402
Training time: 147.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 35.89333333333448
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7174137754473929
Weighted AreaUnderROC: 0.78488989578417
Root mean squared error: 0.4273023626526162
Relative absolute error: 43.57273364434402
Root relative squared error: 90.64451947460925
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5756363314063894
Weighted FMeasure: 0.7172963475626039
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6135617465226801
Mean absolute error: 0.1936565939748632
Coverage of cases: 74.4
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 2296.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14166863948434882
Kappa statistic: 0.5757580848987879
Training time: 142.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 39.69333333333433
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7223595997756321
Weighted AreaUnderROC: 0.8142745548294955
Root mean squared error: 0.4096364605024303
Relative absolute error: 43.26245614277302
Root relative squared error: 86.89701571275694
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5842649108239711
Weighted FMeasure: 0.7212541935304966
Iteration time: 148.0
Weighted AreaUnderPRC: 0.6487010173483302
Mean absolute error: 0.19227758285676988
Coverage of cases: 79.32
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2444.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13855025784726624
Kappa statistic: 0.5843465760234311
Training time: 141.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 37.36000000000105
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7353748257694123
Weighted AreaUnderROC: 0.8093227825677178
Root mean squared error: 0.41234676840916074
Relative absolute error: 40.41605882352935
Root relative squared error: 87.47195884274272
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6036898244479051
Weighted FMeasure: 0.735261950083559
Iteration time: 153.0
Weighted AreaUnderPRC: 0.6452437665559061
Mean absolute error: 0.17962692810457573
Coverage of cases: 77.56
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2597.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13208700962942993
Kappa statistic: 0.6040655984116552
Training time: 147.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 37.973333333334516
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7287552618595164
Weighted AreaUnderROC: 0.8108114737154798
Root mean squared error: 0.41327723367673874
Relative absolute error: 41.956532072936184
Root relative squared error: 87.6693403328516
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5933026662141203
Weighted FMeasure: 0.7280603794091927
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6453971186408815
Mean absolute error: 0.18647347587971727
Coverage of cases: 78.6
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2746.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13550879918703124
Kappa statistic: 0.5933118880333643
Training time: 142.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 37.14666666666773
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7374745065715425
Weighted AreaUnderROC: 0.8188053355411468
Root mean squared error: 0.40854332301355584
Relative absolute error: 40.635481254393426
Root relative squared error: 86.66512623341123
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6063802402336311
Weighted FMeasure: 0.7371145661112971
Iteration time: 156.0
Weighted AreaUnderPRC: 0.6571906814260338
Mean absolute error: 0.18060213890841606
Coverage of cases: 78.4
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2902.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13115876930059966
Kappa statistic: 0.6064777599946252
Training time: 149.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 36.693333333334245
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7326395902360747
Weighted AreaUnderROC: 0.8183476915170961
Root mean squared error: 0.4140697947584169
Relative absolute error: 41.15332232366405
Root relative squared error: 87.83746792745936
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5984974416722459
Weighted FMeasure: 0.7293367772972434
Iteration time: 174.0
Weighted AreaUnderPRC: 0.6576496106661325
Mean absolute error: 0.18290365477184106
Coverage of cases: 76.92
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3076.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13391184377238322
Kappa statistic: 0.5976336300090717
Training time: 168.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 35.226666666667704
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.729547376664022
Weighted AreaUnderROC: 0.7988828323230249
Root mean squared error: 0.4187114853771427
Relative absolute error: 41.51461073594852
Root relative squared error: 88.82211920126065
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.5950177610282575
Weighted FMeasure: 0.7287216212960408
Iteration time: 185.0
Weighted AreaUnderPRC: 0.627129951984759
Mean absolute error: 0.18450938104866096
Coverage of cases: 74.8
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3261.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.13492247958362816
Kappa statistic: 0.5951298499545226
Training time: 179.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 39.01333333333412
Incorrectly Classified Instances: 29.76
Correctly Classified Instances: 70.24
Weighted Precision: 0.701448957509156
Weighted AreaUnderROC: 0.7990253514998507
Root mean squared error: 0.4235913244648186
Relative absolute error: 46.118560640338046
Root relative squared error: 89.8572893942591
Weighted TruePositiveRate: 0.7024
Weighted MatthewsCorrelation: 0.5535277134322796
Weighted FMeasure: 0.700332990386415
Iteration time: 183.0
Weighted AreaUnderPRC: 0.6216754312546127
Mean absolute error: 0.2049713806237256
Coverage of cases: 77.48
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3444.0
Weighted Recall: 0.7024
Weighted FalsePositiveRate: 0.14873054610135655
Kappa statistic: 0.55378380537489
Training time: 177.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 36.146666666667656
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32
Weighted Precision: 0.7226104510342212
Weighted AreaUnderROC: 0.8047564171986836
Root mean squared error: 0.42067533459466605
Relative absolute error: 42.49046799321371
Root relative squared error: 89.23871453094225
Weighted TruePositiveRate: 0.7232
Weighted MatthewsCorrelation: 0.5844949336124086
Weighted FMeasure: 0.7226029806434102
Iteration time: 189.0
Weighted AreaUnderPRC: 0.6348312029178298
Mean absolute error: 0.18884652441428404
Coverage of cases: 75.68
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3633.0
Weighted Recall: 0.7232
Weighted FalsePositiveRate: 0.1384623898592844
Kappa statistic: 0.584863100808677
Training time: 183.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 37.16000000000106
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7211514578155072
Weighted AreaUnderROC: 0.7924458228742869
Root mean squared error: 0.42266162536331653
Relative absolute error: 43.0595825823311
Root relative squared error: 89.66007043251855
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.5826535435605228
Weighted FMeasure: 0.7210421333620122
Iteration time: 193.0
Weighted AreaUnderPRC: 0.6226978911000562
Mean absolute error: 0.19137592258813915
Coverage of cases: 75.72
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3826.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.13904184317248627
Kappa statistic: 0.5830899858574524
Training time: 187.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 37.253333333334446
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7322254392890581
Weighted AreaUnderROC: 0.8113471972164783
Root mean squared error: 0.41299736928158537
Relative absolute error: 41.25662945187998
Root relative squared error: 87.60997212936392
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5988995158624587
Weighted FMeasure: 0.7321907374959404
Iteration time: 199.0
Weighted AreaUnderPRC: 0.6460967119040176
Mean absolute error: 0.18336279756391188
Coverage of cases: 77.76
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4025.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13367570496924264
Kappa statistic: 0.5992572581229594
Training time: 194.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 38.506666666667655
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7124587255046229
Weighted AreaUnderROC: 0.8053182856535732
Root mean squared error: 0.4280642093405906
Relative absolute error: 43.82981383580646
Root relative squared error: 90.80613156239662
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5688750473432163
Weighted FMeasure: 0.7125587234484562
Iteration time: 210.0
Weighted AreaUnderPRC: 0.635968904828447
Mean absolute error: 0.19479917260358517
Coverage of cases: 77.08
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4235.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.14376812523535112
Kappa statistic: 0.5692163697779484
Training time: 205.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 35.42666666666772
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.721572336014147
Weighted AreaUnderROC: 0.7883236991813214
Root mean squared error: 0.4224935068869316
Relative absolute error: 42.277850098527495
Root relative squared error: 89.62440711811018
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.5814961164022429
Weighted FMeasure: 0.7206518959027629
Iteration time: 197.0
Weighted AreaUnderPRC: 0.614938515300477
Mean absolute error: 0.18790155599345643
Coverage of cases: 75.24
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4432.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.1397311571289077
Kappa statistic: 0.5811968170958098
Training time: 192.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 37.813333333334214
Incorrectly Classified Instances: 28.64
Correctly Classified Instances: 71.36
Weighted Precision: 0.7132949090731315
Weighted AreaUnderROC: 0.80312386100894
Root mean squared error: 0.41928992974567353
Relative absolute error: 43.18827107500308
Root relative squared error: 88.94482578191884
Weighted TruePositiveRate: 0.7136
Weighted MatthewsCorrelation: 0.5701145252248617
Weighted FMeasure: 0.7133963003790983
Iteration time: 195.0
Weighted AreaUnderPRC: 0.6365969092838035
Mean absolute error: 0.19194787144445905
Coverage of cases: 77.48
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4627.0
Weighted Recall: 0.7136
Weighted FalsePositiveRate: 0.14333833652194214
Kappa statistic: 0.5704143653436229
Training time: 190.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 36.626666666667624
Incorrectly Classified Instances: 27.52
Correctly Classified Instances: 72.48
Weighted Precision: 0.7245227899970784
Weighted AreaUnderROC: 0.8172501520341204
Root mean squared error: 0.4161210521549267
Relative absolute error: 41.75696699584885
Root relative squared error: 88.27260533196869
Weighted TruePositiveRate: 0.7248
Weighted MatthewsCorrelation: 0.5870048050885868
Weighted FMeasure: 0.724467702854492
Iteration time: 205.0
Weighted AreaUnderPRC: 0.6534137344529825
Mean absolute error: 0.18558651998155132
Coverage of cases: 76.68
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4832.0
Weighted Recall: 0.7248
Weighted FalsePositiveRate: 0.13766460904427744
Kappa statistic: 0.587242035271128
Training time: 200.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 37.160000000000935
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7085154068507215
Weighted AreaUnderROC: 0.8054033130211604
Root mean squared error: 0.42849902843328036
Relative absolute error: 44.02570034083557
Root relative squared error: 90.89837062110573
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.5629605239911446
Weighted FMeasure: 0.7084158333844036
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6354874592606887
Mean absolute error: 0.19566977929260346
Coverage of cases: 75.68
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5029.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14573854525716423
Kappa statistic: 0.5632325304411328
Training time: 192.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 37.36000000000092
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7278377527466003
Weighted AreaUnderROC: 0.8205916529039711
Root mean squared error: 0.4144933574509942
Relative absolute error: 41.43055984211883
Root relative squared error: 87.92731914311307
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5914363807540427
Weighted FMeasure: 0.7259112627541131
Iteration time: 210.0
Weighted AreaUnderPRC: 0.6572550407125509
Mean absolute error: 0.18413582152052899
Coverage of cases: 78.76
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 5239.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13623105080982473
Kappa statistic: 0.5909647593949157
Training time: 206.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.586666666667206
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7261871489645378
Weighted AreaUnderROC: 0.8275019996805367
Root mean squared error: 0.40488397174940227
Relative absolute error: 42.73766080376848
Root relative squared error: 85.88886060532326
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.588234123193088
Weighted FMeasure: 0.7214585247487
Iteration time: 202.0
Weighted AreaUnderPRC: 0.6662170014797303
Mean absolute error: 0.18994515912786078
Coverage of cases: 82.64
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 5441.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13742976139280166
Kappa statistic: 0.5868888473179553
Training time: 197.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 35.82666666666772
Incorrectly Classified Instances: 25.4
Correctly Classified Instances: 74.6
Weighted Precision: 0.7454767604329853
Weighted AreaUnderROC: 0.8001551298054094
Root mean squared error: 0.4039010743088551
Relative absolute error: 39.3878196258266
Root relative squared error: 85.68035657169672
Weighted TruePositiveRate: 0.746
Weighted MatthewsCorrelation: 0.6186883396489004
Weighted FMeasure: 0.7455010784586246
Iteration time: 229.0
Weighted AreaUnderPRC: 0.6374086844998088
Mean absolute error: 0.1750569761147857
Coverage of cases: 77.56
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5670.0
Weighted Recall: 0.746
Weighted FalsePositiveRate: 0.12710259610818672
Kappa statistic: 0.6190387970889044
Training time: 225.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 36.013333333334316
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.728790114025072
Weighted AreaUnderROC: 0.8014255819180945
Root mean squared error: 0.4148478403250235
Relative absolute error: 40.8955439790059
Root relative squared error: 88.00251631632524
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5934368904418572
Weighted FMeasure: 0.7288905539575217
Iteration time: 230.0
Weighted AreaUnderPRC: 0.6301228563041256
Mean absolute error: 0.1817579732400271
Coverage of cases: 77.0
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 5900.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13558572216387885
Kappa statistic: 0.5938191392421589
Training time: 225.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 38.41333333333442
Incorrectly Classified Instances: 27.16
Correctly Classified Instances: 72.84
Weighted Precision: 0.7278826381179072
Weighted AreaUnderROC: 0.8235538889583549
Root mean squared error: 0.4118261535931586
Relative absolute error: 41.45387952597054
Root relative squared error: 87.36151976270833
Weighted TruePositiveRate: 0.7284
Weighted MatthewsCorrelation: 0.5924008964271095
Weighted FMeasure: 0.7276297231912667
Iteration time: 223.0
Weighted AreaUnderPRC: 0.659583382719026
Mean absolute error: 0.18423946455986992
Coverage of cases: 79.28
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6123.0
Weighted Recall: 0.7284
Weighted FalsePositiveRate: 0.13580265588104984
Kappa statistic: 0.5926916280913447
Training time: 219.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 37.28000000000103
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7373908114707397
Weighted AreaUnderROC: 0.8160187454059098
Root mean squared error: 0.40633712328757543
Relative absolute error: 40.02377554323238
Root relative squared error: 86.19712059734343
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6061987248276285
Weighted FMeasure: 0.7372696193559685
Iteration time: 227.0
Weighted AreaUnderPRC: 0.654668160802978
Mean absolute error: 0.17788344685881144
Coverage of cases: 78.4
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6350.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13133042655563032
Kappa statistic: 0.6064239294250909
Training time: 223.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 38.026666666667516
Incorrectly Classified Instances: 26.48
Correctly Classified Instances: 73.52
Weighted Precision: 0.7352120520459892
Weighted AreaUnderROC: 0.8235979070101932
Root mean squared error: 0.4036700245533789
Relative absolute error: 40.654039607186505
Root relative squared error: 85.6313435170301
Weighted TruePositiveRate: 0.7352
Weighted MatthewsCorrelation: 0.6026912161610027
Weighted FMeasure: 0.7351541361181926
Iteration time: 233.0
Weighted AreaUnderPRC: 0.6617378203794262
Mean absolute error: 0.18068462047638528
Coverage of cases: 79.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6583.0
Weighted Recall: 0.7352
Weighted FalsePositiveRate: 0.13251679834283892
Kappa statistic: 0.6027861134025245
Training time: 229.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 36.69333333333417
Incorrectly Classified Instances: 25.76
Correctly Classified Instances: 74.24
Weighted Precision: 0.7423207323400851
Weighted AreaUnderROC: 0.8188935373031091
Root mean squared error: 0.40581633464118255
Relative absolute error: 39.42605738121536
Root relative squared error: 86.08664464231462
Weighted TruePositiveRate: 0.7424
Weighted MatthewsCorrelation: 0.6134086403684917
Weighted FMeasure: 0.7423259994050659
Iteration time: 233.0
Weighted AreaUnderPRC: 0.653712965352326
Mean absolute error: 0.1752269216942913
Coverage of cases: 78.28
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6816.0
Weighted Recall: 0.7424
Weighted FalsePositiveRate: 0.12896454927141432
Kappa statistic: 0.6135829667371737
Training time: 229.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 38.98666666666738
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7448974792015162
Weighted AreaUnderROC: 0.831350085973181
Root mean squared error: 0.3996410597843889
Relative absolute error: 39.33109904371151
Root relative squared error: 84.77667102423577
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6176025574230979
Weighted FMeasure: 0.7448775005008156
Iteration time: 229.0
Weighted AreaUnderPRC: 0.6713311515944267
Mean absolute error: 0.17480488463871866
Coverage of cases: 80.84
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 7045.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.12745685562938222
Kappa statistic: 0.6178336917817325
Training time: 225.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 38.10666666666771
Incorrectly Classified Instances: 23.72
Correctly Classified Instances: 76.28
Weighted Precision: 0.763200035539801
Weighted AreaUnderROC: 0.8369871658917396
Root mean squared error: 0.3873851939570265
Relative absolute error: 37.03827425907407
Root relative squared error: 82.17680927348363
Weighted TruePositiveRate: 0.7628
Weighted MatthewsCorrelation: 0.644601357567023
Weighted FMeasure: 0.7618879798714239
Iteration time: 252.0
Weighted AreaUnderPRC: 0.6940941633769466
Mean absolute error: 0.1646145522625522
Coverage of cases: 80.96
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 7297.0
Weighted Recall: 0.7628
Weighted FalsePositiveRate: 0.11846841166806762
Kappa statistic: 0.644317318375707
Training time: 248.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 40.45333333333425
Incorrectly Classified Instances: 24.64
Correctly Classified Instances: 75.36
Weighted Precision: 0.7531822978865534
Weighted AreaUnderROC: 0.8212835490191687
Root mean squared error: 0.3932914445539629
Relative absolute error: 38.480983702832155
Root relative squared error: 83.42971422802788
Weighted TruePositiveRate: 0.7536
Weighted MatthewsCorrelation: 0.630160771134198
Weighted FMeasure: 0.7531417169435046
Iteration time: 262.0
Weighted AreaUnderPRC: 0.6644978924586696
Mean absolute error: 0.1710265942348104
Coverage of cases: 81.2
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 7559.0
Weighted Recall: 0.7536
Weighted FalsePositiveRate: 0.1232496240525423
Kappa statistic: 0.6304511160016347
Training time: 258.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 38.44000000000082
Incorrectly Classified Instances: 25.88
Correctly Classified Instances: 74.12
Weighted Precision: 0.7410895459374033
Weighted AreaUnderROC: 0.8217046965424978
Root mean squared error: 0.40262617612532453
Relative absolute error: 39.73135304376981
Root relative squared error: 85.40990982642765
Weighted TruePositiveRate: 0.7412
Weighted MatthewsCorrelation: 0.6118023538665566
Weighted FMeasure: 0.7407138677885657
Iteration time: 236.0
Weighted AreaUnderPRC: 0.6570929039736962
Mean absolute error: 0.17658379130564442
Coverage of cases: 79.84
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 7795.0
Weighted Recall: 0.7412
Weighted FalsePositiveRate: 0.12934488016878062
Kappa statistic: 0.6118766931654305
Training time: 233.0
		
Time end:Wed Nov 01 17.26.29 EET 2017