Wed Nov 01 17.32.02 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 17.32.02 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.88
Correctly Classified Instances: 83.12
Weighted Precision: 0.8325268140432943
Weighted AreaUnderROC: 0.9140321744962854
Root mean squared error: 0.3341323756304475
Relative absolute error: 58.459999999998715
Root relative squared error: 70.88018058667788
Weighted TruePositiveRate: 0.8312
Weighted MatthewsCorrelation: 0.7476452341186224
Weighted FMeasure: 0.8306612829677302
Iteration time: 49.0
Weighted AreaUnderPRC: 0.7748539652138313
Mean absolute error: 0.25982222222221774
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 49.0
Weighted Recall: 0.8312
Weighted FalsePositiveRate: 0.08418304170873775
Kappa statistic: 0.7468869493951439
Training time: 44.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.72
Correctly Classified Instances: 83.28
Weighted Precision: 0.8334678726120163
Weighted AreaUnderROC: 0.9152254557229084
Root mean squared error: 0.3335998934185845
Relative absolute error: 58.37999999999874
Root relative squared error: 70.76722405181702
Weighted TruePositiveRate: 0.8328
Weighted MatthewsCorrelation: 0.7496301852798252
Weighted FMeasure: 0.8324536095383144
Iteration time: 55.0
Weighted AreaUnderPRC: 0.7764405669954499
Mean absolute error: 0.2594666666666623
Coverage of cases: 100.0
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 104.0
Weighted Recall: 0.8328
Weighted FalsePositiveRate: 0.08345392379574305
Kappa statistic: 0.7492609797297297
Training time: 47.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.28
Correctly Classified Instances: 83.72
Weighted Precision: 0.8372473847452504
Weighted AreaUnderROC: 0.9179421472407272
Root mean squared error: 0.33213116552216715
Relative absolute error: 58.159999999998895
Root relative squared error: 70.45565981523465
Weighted TruePositiveRate: 0.8372
Weighted MatthewsCorrelation: 0.7557840009928001
Weighted FMeasure: 0.8371397797153837
Iteration time: 53.0
Weighted AreaUnderPRC: 0.7819463798678924
Mean absolute error: 0.2584888888888852
Coverage of cases: 100.0
Instances selection time: 7.0
Test time: 10.0
Accumulative iteration time: 157.0
Weighted Recall: 0.8372
Weighted FalsePositiveRate: 0.08140250207614934
Kappa statistic: 0.7558092792473885
Training time: 46.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 15.96
Correctly Classified Instances: 84.04
Weighted Precision: 0.8404307245007342
Weighted AreaUnderROC: 0.9197749996205472
Root mean squared error: 0.3310589071449401
Relative absolute error: 57.99999999999898
Root relative squared error: 70.22819946431825
Weighted TruePositiveRate: 0.8404
Weighted MatthewsCorrelation: 0.7605451935065942
Weighted FMeasure: 0.8404047205500983
Iteration time: 53.0
Weighted AreaUnderPRC: 0.7859915215963809
Mean absolute error: 0.25777777777777444
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 210.0
Weighted Recall: 0.8404
Weighted FalsePositiveRate: 0.07987560334814767
Kappa statistic: 0.7605839878571079
Training time: 48.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 66.66666666666865
Incorrectly Classified Instances: 16.16
Correctly Classified Instances: 83.84
Weighted Precision: 0.8386385065141396
Weighted AreaUnderROC: 0.9188345363555611
Root mean squared error: 0.33177413111042303
Relative absolute error: 58.09999999999901
Root relative squared error: 70.37992137913629
Weighted TruePositiveRate: 0.8384
Weighted MatthewsCorrelation: 0.7576340920972081
Weighted FMeasure: 0.8384324358189115
Iteration time: 52.0
Weighted AreaUnderPRC: 0.783989781807724
Mean absolute error: 0.258222222222219
Coverage of cases: 99.96
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 262.0
Weighted Recall: 0.8384
Weighted FalsePositiveRate: 0.08093290940616708
Kappa statistic: 0.7575672036913155
Training time: 47.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.0
Correctly Classified Instances: 84.0
Weighted Precision: 0.8409517663898534
Weighted AreaUnderROC: 0.9200825489982082
Root mean squared error: 0.33119312928998235
Relative absolute error: 58.01999999999906
Root relative squared error: 70.25667228100168
Weighted TruePositiveRate: 0.84
Weighted MatthewsCorrelation: 0.7603942058030647
Weighted FMeasure: 0.8400821515472803
Iteration time: 57.0
Weighted AreaUnderPRC: 0.7864695528340466
Mean absolute error: 0.2578666666666637
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 319.0
Weighted Recall: 0.84
Weighted FalsePositiveRate: 0.08023812294417851
Kappa statistic: 0.759933588027792
Training time: 52.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.32
Correctly Classified Instances: 83.68
Weighted Precision: 0.8380789878017929
Weighted AreaUnderROC: 0.9185093833276354
Root mean squared error: 0.33226495451672616
Relative absolute error: 58.17999999999909
Root relative squared error: 70.4840407468249
Weighted TruePositiveRate: 0.8368
Weighted MatthewsCorrelation: 0.7557644284722144
Weighted FMeasure: 0.8368568183467358
Iteration time: 61.0
Weighted AreaUnderPRC: 0.7828979739385332
Mean absolute error: 0.25857777777777496
Coverage of cases: 100.0
Instances selection time: 6.0
Test time: 12.0
Accumulative iteration time: 380.0
Weighted Recall: 0.8368
Weighted FalsePositiveRate: 0.08187488231539979
Kappa statistic: 0.7551233242152783
Training time: 55.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.76
Correctly Classified Instances: 83.24
Weighted Precision: 0.8343663892632641
Weighted AreaUnderROC: 0.9165784621100038
Root mean squared error: 0.33373309362090525
Relative absolute error: 58.399999999999096
Root relative squared error: 70.79548008171194
Weighted TruePositiveRate: 0.8324
Weighted MatthewsCorrelation: 0.7495360769677036
Weighted FMeasure: 0.8324415468094097
Iteration time: 60.0
Weighted AreaUnderPRC: 0.7783248375056766
Mean absolute error: 0.25955555555555276
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 440.0
Weighted Recall: 0.8324
Weighted FalsePositiveRate: 0.08415289161163396
Kappa statistic: 0.7484971655210383
Training time: 55.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 16.76
Correctly Classified Instances: 83.24
Weighted Precision: 0.834806518741238
Weighted AreaUnderROC: 0.9169948254191509
Root mean squared error: 0.3337330936209052
Relative absolute error: 58.39999999999909
Root relative squared error: 70.79548008171193
Weighted TruePositiveRate: 0.8324
Weighted MatthewsCorrelation: 0.7498075749350103
Weighted FMeasure: 0.8323998656141303
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7789745251030422
Mean absolute error: 0.2595555555555527
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 499.0
Weighted Recall: 0.8324
Weighted FalsePositiveRate: 0.08418722306264008
Kappa statistic: 0.7484822494499025
Training time: 54.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.04
Correctly Classified Instances: 82.96
Weighted Precision: 0.8326687697023993
Weighted AreaUnderROC: 0.9157865727165431
Root mean squared error: 0.3346197399592147
Relative absolute error: 58.53999999999907
Root relative squared error: 70.98356617321178
Weighted TruePositiveRate: 0.8296
Weighted MatthewsCorrelation: 0.7459473718814541
Weighted FMeasure: 0.8296441869138849
Iteration time: 59.0
Weighted AreaUnderPRC: 0.7763692866503048
Mean absolute error: 0.26017777777777484
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 558.0
Weighted Recall: 0.8296
Weighted FalsePositiveRate: 0.08563998638306237
Kappa statistic: 0.7442662819535079
Training time: 54.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.72
Correctly Classified Instances: 82.28
Weighted Precision: 0.827908669850919
Weighted AreaUnderROC: 0.9127061694701609
Root mean squared error: 0.3368701255402201
Relative absolute error: 58.87999999999906
Root relative squared error: 71.4609450445958
Weighted TruePositiveRate: 0.8228
Weighted MatthewsCorrelation: 0.7368498339722866
Weighted FMeasure: 0.8228595859386172
Iteration time: 61.0
Weighted AreaUnderPRC: 0.7696725350665585
Mean absolute error: 0.2616888888888859
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 619.0
Weighted Recall: 0.8228
Weighted FalsePositiveRate: 0.0891730303685174
Kappa statistic: 0.7340221139898363
Training time: 56.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.8284112143755341
Weighted AreaUnderROC: 0.9130604645515121
Root mean squared error: 0.3366061551779852
Relative absolute error: 58.83999999999901
Root relative squared error: 71.40494847464522
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7378873711427432
Weighted FMeasure: 0.8236611356260594
Iteration time: 65.0
Weighted AreaUnderPRC: 0.7704268464804063
Mean absolute error: 0.2615111111111079
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 684.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08875697423551088
Kappa statistic: 0.735227749764887
Training time: 60.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 66.72000000000199
Incorrectly Classified Instances: 17.8
Correctly Classified Instances: 82.2
Weighted Precision: 0.8278948067567168
Weighted AreaUnderROC: 0.9125266144635873
Root mean squared error: 0.3370459909270575
Relative absolute error: 58.91999999999907
Root relative squared error: 71.49825172687841
Weighted TruePositiveRate: 0.822
Weighted MatthewsCorrelation: 0.7360915255434509
Weighted FMeasure: 0.8220372297120714
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7695419054233414
Mean absolute error: 0.26186666666666375
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 753.0
Weighted Recall: 0.822
Weighted FalsePositiveRate: 0.08961655166232886
Kappa statistic: 0.7328078995674729
Training time: 64.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8269574814891374
Weighted AreaUnderROC: 0.9109285407109575
Root mean squared error: 0.3384496242399307
Relative absolute error: 59.11999999999907
Root relative squared error: 71.796007317028
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7318729674451397
Weighted FMeasure: 0.8178764367825736
Iteration time: 67.0
Weighted AreaUnderPRC: 0.7662308740942164
Mean absolute error: 0.26275555555555263
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 820.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09175862893917261
Kappa statistic: 0.726754953842606
Training time: 62.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 18.72
Correctly Classified Instances: 81.28
Weighted Precision: 0.8233891160889332
Weighted AreaUnderROC: 0.9084903683294895
Root mean squared error: 0.3401524712591485
Relative absolute error: 59.37999999999906
Root relative squared error: 72.15723571941166
Weighted TruePositiveRate: 0.8128
Weighted MatthewsCorrelation: 0.7249998001812815
Weighted FMeasure: 0.812631791673288
Iteration time: 70.0
Weighted AreaUnderPRC: 0.7611046030318869
Mean absolute error: 0.26391111111110815
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 890.0
Weighted Recall: 0.8128
Weighted FalsePositiveRate: 0.09443209549780965
Kappa statistic: 0.7189248611681002
Training time: 65.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8210242227928899
Weighted AreaUnderROC: 0.9067127611087541
Root mean squared error: 0.34119615298947464
Relative absolute error: 59.539999999999075
Root relative squared error: 72.3786340480859
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.7206516266294317
Weighted FMeasure: 0.8094511223684553
Iteration time: 73.0
Weighted AreaUnderPRC: 0.7576059916155368
Mean absolute error: 0.26462222222221937
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 963.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.09606885486903093
Kappa statistic: 0.714109718443719
Training time: 68.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8232275613418838
Weighted AreaUnderROC: 0.9057790328071347
Root mean squared error: 0.34145657497344334
Relative absolute error: 59.5799999999991
Root relative squared error: 72.43387789333619
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7212399256403426
Weighted FMeasure: 0.8084627856678156
Iteration time: 69.0
Weighted AreaUnderPRC: 0.7570546613985997
Mean absolute error: 0.26479999999999726
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1032.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.0965741727746534
Kappa statistic: 0.7128704901745242
Training time: 64.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8224814909435859
Weighted AreaUnderROC: 0.9048197818543465
Root mean squared error: 0.34210676262979545
Relative absolute error: 59.679999999999104
Root relative squared error: 72.57180352359131
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.718968843546285
Weighted FMeasure: 0.8064045585296173
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7553164290756677
Mean absolute error: 0.26524444444444173
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1106.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09761774625227039
Kappa statistic: 0.7098524728979381
Training time: 69.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 66.70666666666865
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8231301555303351
Weighted AreaUnderROC: 0.9053274467302366
Root mean squared error: 0.34206345523515014
Relative absolute error: 59.679999999999104
Root relative squared error: 72.56261663786253
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7193401690926252
Weighted FMeasure: 0.8063894499087816
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7562336729065774
Mean absolute error: 0.26524444444444173
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 1185.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09763834512287407
Kappa statistic: 0.7098458494500046
Training time: 74.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 66.73333333333532
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8215626865317459
Weighted AreaUnderROC: 0.904365677655863
Root mean squared error: 0.34262602439481266
Relative absolute error: 59.77999999999911
Root relative squared error: 72.68195557816767
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.716593779872243
Weighted FMeasure: 0.8043305618974949
Iteration time: 74.0
Weighted AreaUnderPRC: 0.7543551157009649
Mean absolute error: 0.2656888888888862
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1259.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09865445343968614
Kappa statistic: 0.7068368294643403
Training time: 69.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 66.70666666666867
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8235566967835066
Weighted AreaUnderROC: 0.9049841077183606
Root mean squared error: 0.3421933609763615
Relative absolute error: 59.699999999999115
Root relative squared error: 72.59017380702024
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7192730418163499
Weighted FMeasure: 0.8058230122460018
Iteration time: 80.0
Weighted AreaUnderPRC: 0.7558465821189392
Mean absolute error: 0.26533333333333065
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1339.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09786353891488041
Kappa statistic: 0.7092344961310166
Training time: 75.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 66.72000000000199
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8211154255386713
Weighted AreaUnderROC: 0.9036810233808023
Root mean squared error: 0.34305814214049934
Relative absolute error: 59.839999999999094
Root relative squared error: 72.7736215946415
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7152143235910345
Weighted FMeasure: 0.8031482471320696
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7531453764090071
Mean absolute error: 0.2659555555555528
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 1418.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09928197078429657
Kappa statistic: 0.705026516373972
Training time: 74.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 66.72000000000199
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8221855185173372
Weighted AreaUnderROC: 0.9034784931167247
Root mean squared error: 0.34344658326377
Relative absolute error: 59.899999999999096
Root relative squared error: 72.85602240034842
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7147281471928448
Weighted FMeasure: 0.8018600516201323
Iteration time: 75.0
Weighted AreaUnderPRC: 0.7531001908634235
Mean absolute error: 0.2662222222222195
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 12.0
Accumulative iteration time: 1493.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09995068587011437
Kappa statistic: 0.7032011973849022
Training time: 70.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8250340559555943
Weighted AreaUnderROC: 0.9044571178493912
Root mean squared error: 0.34279893685820156
Relative absolute error: 59.77999999999913
Root relative squared error: 72.71863585079187
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7186505010295458
Weighted FMeasure: 0.8041794767995162
Iteration time: 77.0
Weighted AreaUnderPRC: 0.7553620979160174
Mean absolute error: 0.26568888888888625
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1570.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09875744779270454
Kappa statistic: 0.7068006859902638
Training time: 73.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.819467450308182
Weighted AreaUnderROC: 0.9001255720884579
Root mean squared error: 0.3455966392154866
Relative absolute error: 60.21999999999916
Root relative squared error: 73.31211814336523
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7073096053730492
Weighted FMeasure: 0.7952723039016194
Iteration time: 82.0
Weighted AreaUnderPRC: 0.7469648036049437
Mean absolute error: 0.26764444444444196
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1652.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10326540235376429
Kappa statistic: 0.693547700922746
Training time: 78.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.68
Correctly Classified Instances: 79.32
Weighted Precision: 0.8179813424123744
Weighted AreaUnderROC: 0.8984516749772868
Root mean squared error: 0.3464956843299227
Relative absolute error: 60.35999999999922
Root relative squared error: 73.50283441246835
Weighted TruePositiveRate: 0.7932
Weighted MatthewsCorrelation: 0.7039317169413877
Weighted FMeasure: 0.7923104873345077
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7439716924351475
Mean absolute error: 0.26826666666666443
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 13.0
Accumulative iteration time: 1736.0
Weighted Recall: 0.7932
Weighted FalsePositiveRate: 0.10470443309378413
Kappa statistic: 0.6893258864487918
Training time: 79.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.04
Correctly Classified Instances: 78.96
Weighted Precision: 0.8159534133662609
Weighted AreaUnderROC: 0.8966686498199224
Root mean squared error: 0.3476481831669216
Relative absolute error: 60.5399999999992
Root relative squared error: 73.74731633535379
Weighted TruePositiveRate: 0.7896
Weighted MatthewsCorrelation: 0.6994318031328746
Weighted FMeasure: 0.7886514454561496
Iteration time: 78.0
Weighted AreaUnderPRC: 0.7406261967223547
Mean absolute error: 0.2690666666666644
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1814.0
Weighted Recall: 0.7896
Weighted FalsePositiveRate: 0.1065526536766093
Kappa statistic: 0.6839017532996209
Training time: 74.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.813945861960598
Weighted AreaUnderROC: 0.8949009077958413
Root mean squared error: 0.3486694284997935
Relative absolute error: 60.69999999999922
Root relative squared error: 73.96395518539244
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.6952990314185977
Weighted FMeasure: 0.7854164513408588
Iteration time: 79.0
Weighted AreaUnderPRC: 0.7374084658049219
Mean absolute error: 0.26977777777777556
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1893.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10818941304783057
Kappa statistic: 0.6790824498485197
Training time: 75.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.6
Correctly Classified Instances: 78.4
Weighted Precision: 0.8113520219323038
Weighted AreaUnderROC: 0.8937097628527919
Root mean squared error: 0.3494334038178175
Relative absolute error: 60.81999999999918
Root relative squared error: 74.12601882380262
Weighted TruePositiveRate: 0.784
Weighted MatthewsCorrelation: 0.6915100995051131
Weighted FMeasure: 0.7831145085899199
Iteration time: 90.0
Weighted AreaUnderPRC: 0.7348201781668301
Mean absolute error: 0.2703111111111087
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 1983.0
Weighted Recall: 0.784
Weighted FalsePositiveRate: 0.10938951741544163
Kappa statistic: 0.6754795990850929
Training time: 86.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.64
Correctly Classified Instances: 78.36
Weighted Precision: 0.8118917303827692
Weighted AreaUnderROC: 0.8936330975425186
Root mean squared error: 0.3495605707064154
Relative absolute error: 60.83999999999919
Root relative squared error: 74.15299499458361
Weighted TruePositiveRate: 0.7836
Weighted MatthewsCorrelation: 0.6914999045714514
Weighted FMeasure: 0.7826261197564347
Iteration time: 84.0
Weighted AreaUnderPRC: 0.734827996078071
Mean absolute error: 0.2703999999999977
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2067.0
Weighted Recall: 0.7836
Weighted FalsePositiveRate: 0.10961471120744798
Kappa statistic: 0.6748682405047102
Training time: 80.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.48
Correctly Classified Instances: 78.52
Weighted Precision: 0.8134544028357135
Weighted AreaUnderROC: 0.8943262219286182
Root mean squared error: 0.349051625174614
Relative absolute error: 60.75999999999919
Root relative squared error: 74.0450313435462
Weighted TruePositiveRate: 0.7852
Weighted MatthewsCorrelation: 0.6939183659285288
Weighted FMeasure: 0.7841771635963977
Iteration time: 85.0
Weighted AreaUnderPRC: 0.7363875229448571
Mean absolute error: 0.2700444444444421
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2152.0
Weighted Recall: 0.7852
Weighted FalsePositiveRate: 0.10881006410223977
Kappa statistic: 0.6772721721830488
Training time: 81.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 22.08
Correctly Classified Instances: 77.92
Weighted Precision: 0.8094365689740445
Weighted AreaUnderROC: 0.8912157624726161
Root mean squared error: 0.3509563653367362
Relative absolute error: 61.059999999999185
Root relative squared error: 74.44908774905669
Weighted TruePositiveRate: 0.7792
Weighted MatthewsCorrelation: 0.6860116924404998
Weighted FMeasure: 0.7781115001875853
Iteration time: 84.0
Weighted AreaUnderPRC: 0.7305435826186838
Mean absolute error: 0.27137777777777544
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2236.0
Weighted Recall: 0.7792
Weighted FalsePositiveRate: 0.11187212163307846
Kappa statistic: 0.6682377293036238
Training time: 80.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 66.70666666666867
Incorrectly Classified Instances: 22.36
Correctly Classified Instances: 77.64
Weighted Precision: 0.8088744992971677
Weighted AreaUnderROC: 0.8900062275806591
Root mean squared error: 0.3517996062575472
Relative absolute error: 61.199999999999214
Root relative squared error: 74.62796616104052
Weighted TruePositiveRate: 0.7764
Weighted MatthewsCorrelation: 0.6831539375386249
Weighted FMeasure: 0.7751668200552779
Iteration time: 92.0
Weighted AreaUnderPRC: 0.7285896276638856
Mean absolute error: 0.2719999999999978
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2328.0
Weighted Recall: 0.7764
Weighted FalsePositiveRate: 0.113331751243702
Kappa statistic: 0.664007578161815
Training time: 88.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 66.70666666666867
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.80598646400234
Weighted AreaUnderROC: 0.8866711448852282
Root mean squared error: 0.35381518506868453
Relative absolute error: 61.51999999999922
Root relative squared error: 75.05553499465186
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.6755895344155013
Weighted FMeasure: 0.7685325466433449
Iteration time: 92.0
Weighted AreaUnderPRC: 0.7228443527918753
Mean absolute error: 0.27342222222222007
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 14.0
Accumulative iteration time: 2420.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.11662586885674824
Kappa statistic: 0.6543558852793357
Training time: 88.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 66.70666666666867
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.8093364264283402
Weighted AreaUnderROC: 0.888689613688058
Root mean squared error: 0.35255679868114664
Relative absolute error: 61.319999999999204
Root relative squared error: 74.7885909302576
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.6812859584240579
Weighted FMeasure: 0.772485768961162
Iteration time: 94.0
Weighted AreaUnderPRC: 0.7268198280830916
Mean absolute error: 0.2725333333333311
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2514.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.1146005185133253
Kappa statistic: 0.660373293631272
Training time: 90.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 66.70666666666867
Incorrectly Classified Instances: 22.08
Correctly Classified Instances: 77.92
Weighted Precision: 0.8128209807318055
Weighted AreaUnderROC: 0.8913231581176535
Root mean squared error: 0.3509141501004811
Relative absolute error: 61.059999999999235
Root relative squared error: 74.44013254510908
Weighted TruePositiveRate: 0.7792
Weighted MatthewsCorrelation: 0.6879600434763073
Weighted FMeasure: 0.7781378981034174
Iteration time: 100.0
Weighted AreaUnderPRC: 0.7320670822783504
Mean absolute error: 0.27137777777777566
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2614.0
Weighted Recall: 0.7792
Weighted FalsePositiveRate: 0.11195451711549317
Kappa statistic: 0.6682089341236052
Training time: 96.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.8185612631572677
Weighted AreaUnderROC: 0.8944445609224247
Root mean squared error: 0.34866942849979343
Relative absolute error: 60.69999999999928
Root relative squared error: 73.96395518539244
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.697923148419288
Weighted FMeasure: 0.785484877455823
Iteration time: 94.0
Weighted AreaUnderPRC: 0.7388161804193258
Mean absolute error: 0.26977777777777584
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2708.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10830613998125141
Kappa statistic: 0.6790443358778184
Training time: 90.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.8225246178488802
Weighted AreaUnderROC: 0.8974324552914162
Root mean squared error: 0.3467521262184837
Relative absolute error: 60.39999999999935
Root relative squared error: 73.55723395198288
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.7055830664353525
Weighted FMeasure: 0.7917278686703401
Iteration time: 103.0
Weighted AreaUnderPRC: 0.744786157413519
Mean absolute error: 0.2684444444444428
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 2811.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10524408245041272
Kappa statistic: 0.6880855907562753
Training time: 100.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8259131923298332
Weighted AreaUnderROC: 0.8990411286446448
Root mean squared error: 0.34559663921548667
Relative absolute error: 60.21999999999939
Root relative squared error: 73.31211814336524
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7105701957957719
Weighted FMeasure: 0.7953891438998407
Iteration time: 97.0
Weighted AreaUnderPRC: 0.7485439303613596
Mean absolute error: 0.26764444444444296
Coverage of cases: 100.0
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 2908.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10340959444799003
Kappa statistic: 0.6935113099134308
Training time: 93.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.64
Correctly Classified Instances: 79.36
Weighted Precision: 0.8208968663442778
Weighted AreaUnderROC: 0.8978062102342418
Root mean squared error: 0.34641016151377874
Relative absolute error: 60.3399999999994
Root relative squared error: 73.48469228349587
Weighted TruePositiveRate: 0.7936
Weighted MatthewsCorrelation: 0.7054606371243072
Weighted FMeasure: 0.7926494621309109
Iteration time: 108.0
Weighted AreaUnderPRC: 0.7447682422022234
Mean absolute error: 0.26817777777777635
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3016.0
Weighted Recall: 0.7936
Weighted FalsePositiveRate: 0.1045341696233876
Kappa statistic: 0.689929635660111
Training time: 104.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.8241017312107564
Weighted AreaUnderROC: 0.8990952554657476
Root mean squared error: 0.3455108938626683
Relative absolute error: 60.199999999999434
Root relative squared error: 73.2939288072353
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.709641093011534
Weighted FMeasure: 0.7954935144723193
Iteration time: 102.0
Weighted AreaUnderPRC: 0.7479686360552467
Mean absolute error: 0.26755555555555427
Coverage of cases: 99.96
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 3118.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10311573775397144
Kappa statistic: 0.6941471910333811
Training time: 99.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.36
Correctly Classified Instances: 79.64
Weighted Precision: 0.8239248876405593
Weighted AreaUnderROC: 0.8990315258125531
Root mean squared error: 0.34551089386266837
Relative absolute error: 60.19999999999948
Root relative squared error: 73.29392880723532
Weighted TruePositiveRate: 0.7964
Weighted MatthewsCorrelation: 0.7094447053443812
Weighted FMeasure: 0.794927031129225
Iteration time: 103.0
Weighted AreaUnderPRC: 0.7474470136095197
Mean absolute error: 0.2675555555555545
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3221.0
Weighted Recall: 0.7964
Weighted FalsePositiveRate: 0.10306767372256284
Kappa statistic: 0.6941597613268443
Training time: 99.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8233690547005837
Weighted AreaUnderROC: 0.8987366300681353
Root mean squared error: 0.34563950391508924
Relative absolute error: 60.2199999999995
Root relative squared error: 73.32121111929398
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7085362187866869
Weighted FMeasure: 0.7941655363733604
Iteration time: 111.0
Weighted AreaUnderPRC: 0.7465891920438192
Mean absolute error: 0.26764444444444346
Coverage of cases: 99.96
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 3332.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10321047203215448
Kappa statistic: 0.6935784865661203
Training time: 108.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8243307348596057
Weighted AreaUnderROC: 0.8993128718367182
Root mean squared error: 0.34525353003264475
Relative absolute error: 60.15999999999951
Root relative squared error: 73.23933369440277
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.7100995085246814
Weighted FMeasure: 0.7952926111209561
Iteration time: 107.0
Weighted AreaUnderPRC: 0.7477660859257715
Mean absolute error: 0.2673777777777769
Coverage of cases: 99.96
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 3439.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10258295468754404
Kappa statistic: 0.6953920934601945
Training time: 104.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 66.680000000002
Incorrectly Classified Instances: 20.32
Correctly Classified Instances: 79.68
Weighted Precision: 0.8227016819088345
Weighted AreaUnderROC: 0.8991893529135837
Root mean squared error: 0.3453822359196999
Relative absolute error: 60.179999999999524
Root relative squared error: 73.26663633605736
Weighted TruePositiveRate: 0.7968
Weighted MatthewsCorrelation: 0.7088203714456487
Weighted FMeasure: 0.7941623919379821
Iteration time: 118.0
Weighted AreaUnderPRC: 0.7464550117447641
Mean absolute error: 0.2674666666666658
Coverage of cases: 99.96
Instances selection time: 4.0
Test time: 13.0
Accumulative iteration time: 3557.0
Weighted Recall: 0.7968
Weighted FalsePositiveRate: 0.10269828783633077
Kappa statistic: 0.6948135851689975
Training time: 114.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 66.69333333333533
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8242729697314723
Weighted AreaUnderROC: 0.9002402203428664
Root mean squared error: 0.3446952491909588
Relative absolute error: 60.079999999999565
Root relative squared error: 73.12090444371397
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.7115219010563784
Weighted FMeasure: 0.7958076516986567
Iteration time: 129.0
Weighted AreaUnderPRC: 0.7482139767500948
Mean absolute error: 0.26702222222222155
Coverage of cases: 100.0
Instances selection time: 3.0
Test time: 13.0
Accumulative iteration time: 3686.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.10164784806851256
Kappa statistic: 0.6978339439530625
Training time: 126.0
		
Time end:Wed Nov 01 17.32.10 EET 2017