Wed Nov 01 16.57.38 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.57.38 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7112886525533404
Weighted AreaUnderROC: 0.7833719772964308
Root mean squared error: 0.43748477465084434
Relative absolute error: 43.65805168986066
Root relative squared error: 92.80453524644398
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5668246160552243
Weighted FMeasure: 0.71087625866048
Iteration time: 739.0
Weighted AreaUnderPRC: 0.6023248216482038
Mean absolute error: 0.1940357852882705
Coverage of cases: 71.12
Instances selection time: 738.0
Test time: 943.0
Accumulative iteration time: 739.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.1444560454071385
Kappa statistic: 0.5668302872263172
Training time: 1.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.56
Correctly Classified Instances: 71.44
Weighted Precision: 0.7150681403340526
Weighted AreaUnderROC: 0.7856907957741236
Root mean squared error: 0.4351036097610589
Relative absolute error: 43.16787762906172
Root relative squared error: 92.29941389423679
Weighted TruePositiveRate: 0.7144
Weighted MatthewsCorrelation: 0.5717776204504921
Weighted FMeasure: 0.7143754127967912
Iteration time: 773.0
Weighted AreaUnderPRC: 0.6060124843332712
Mean absolute error: 0.1918572339069419
Coverage of cases: 71.44
Instances selection time: 773.0
Test time: 968.0
Accumulative iteration time: 1512.0
Weighted Recall: 0.7144
Weighted FalsePositiveRate: 0.14301840845175276
Kappa statistic: 0.5715400841653697
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7131230835637683
Weighted AreaUnderROC: 0.7832003200414002
Root mean squared error: 0.43758015996241467
Relative absolute error: 43.63314917127113
Root relative squared error: 92.82476952663507
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.567532254514798
Weighted FMeasure: 0.7113175994897638
Iteration time: 780.0
Weighted AreaUnderPRC: 0.6028773332057262
Mean absolute error: 0.19392510742787258
Coverage of cases: 71.12
Instances selection time: 780.0
Test time: 998.0
Accumulative iteration time: 2292.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14479935991719978
Kappa statistic: 0.5666405930757571
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7138332682567052
Weighted AreaUnderROC: 0.7831591223001928
Root mean squared error: 0.4376227948749858
Relative absolute error: 43.62202486678438
Root relative squared error: 92.83381375737338
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5679088105562538
Weighted FMeasure: 0.7112622927238179
Iteration time: 793.0
Weighted AreaUnderPRC: 0.6030347026287679
Mean absolute error: 0.19387566607459814
Coverage of cases: 71.12
Instances selection time: 793.0
Test time: 1030.0
Accumulative iteration time: 3085.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14488175539961448
Kappa statistic: 0.5665931435755659
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7119369987624453
Weighted AreaUnderROC: 0.7806995448733748
Root mean squared error: 0.4400804877233106
Relative absolute error: 44.08919382504169
Root relative squared error: 93.35516914111061
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5637781141955465
Weighted FMeasure: 0.7080776415662238
Iteration time: 811.0
Weighted AreaUnderPRC: 0.5999409151388224
Mean absolute error: 0.1959519725557418
Coverage of cases: 70.8
Instances selection time: 811.0
Test time: 1062.0
Accumulative iteration time: 3896.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14660091025325045
Kappa statistic: 0.5617288679468666
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.12
Correctly Classified Instances: 69.88
Weighted Precision: 0.7052401067038858
Weighted AreaUnderROC: 0.7737158049713386
Root mean squared error: 0.4469971984686091
Relative absolute error: 45.45273631840845
Root relative squared error: 94.82242506256253
Weighted TruePositiveRate: 0.6988
Weighted MatthewsCorrelation: 0.5513053418662716
Weighted FMeasure: 0.6986712907777578
Iteration time: 827.0
Weighted AreaUnderPRC: 0.5907467935839027
Mean absolute error: 0.2020121614151496
Coverage of cases: 69.88
Instances selection time: 827.0
Test time: 1098.0
Accumulative iteration time: 4723.0
Weighted Recall: 0.6988
Weighted FalsePositiveRate: 0.15136839005732267
Kappa statistic: 0.5478357364692996
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.08
Correctly Classified Instances: 69.92
Weighted Precision: 0.7069557937166498
Weighted AreaUnderROC: 0.7739769046908327
Root mean squared error: 0.44673575654857994
Relative absolute error: 45.384269662922435
Root relative squared error: 94.76696485620081
Weighted TruePositiveRate: 0.6992
Weighted MatthewsCorrelation: 0.5526913549938799
Weighted FMeasure: 0.6988385916806146
Iteration time: 844.0
Weighted AreaUnderPRC: 0.5914565634825811
Mean absolute error: 0.20170786516854508
Coverage of cases: 69.92
Instances selection time: 844.0
Test time: 1130.0
Accumulative iteration time: 5567.0
Weighted Recall: 0.6992
Weighted FalsePositiveRate: 0.15124619061833472
Kappa statistic: 0.5483888170501479
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.08
Correctly Classified Instances: 69.92
Weighted Precision: 0.7077324506056966
Weighted AreaUnderROC: 0.773956305820229
Root mean squared error: 0.4467690190483295
Relative absolute error: 45.376049766719895
Root relative squared error: 94.77402089794046
Weighted TruePositiveRate: 0.6992
Weighted MatthewsCorrelation: 0.5531558262324404
Weighted FMeasure: 0.6986992731866524
Iteration time: 874.0
Weighted AreaUnderPRC: 0.5916274468660534
Mean absolute error: 0.2016713322965338
Coverage of cases: 69.92
Instances selection time: 874.0
Test time: 1167.0
Accumulative iteration time: 6441.0
Weighted Recall: 0.6992
Weighted FalsePositiveRate: 0.15128738835954209
Kappa statistic: 0.5483640810109919
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.36
Correctly Classified Instances: 69.64
Weighted Precision: 0.7060880954678492
Weighted AreaUnderROC: 0.7718264910149172
Root mean squared error: 0.44887494116660187
Relative absolute error: 45.78642533936723
Root relative squared error: 95.2207544410848
Weighted TruePositiveRate: 0.6964
Weighted MatthewsCorrelation: 0.5496072919985409
Weighted FMeasure: 0.6957150749407183
Iteration time: 873.0
Weighted AreaUnderPRC: 0.5889331095398349
Mean absolute error: 0.20349522373052195
Coverage of cases: 69.64
Instances selection time: 873.0
Test time: 1199.0
Accumulative iteration time: 7314.0
Weighted Recall: 0.6964
Weighted FalsePositiveRate: 0.1527470179701656
Kappa statistic: 0.5441288115396075
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7035400247299505
Weighted AreaUnderROC: 0.7684806200765989
Root mean squared error: 0.45214568151468715
Relative absolute error: 46.43631039531506
Root relative squared error: 95.91458324497425
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5440583870443998
Weighted FMeasure: 0.6910159357273177
Iteration time: 886.0
Weighted AreaUnderPRC: 0.5847325426227769
Mean absolute error: 0.2063836017569568
Coverage of cases: 69.2
Instances selection time: 886.0
Test time: 1230.0
Accumulative iteration time: 8200.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.1550387598468022
Kappa statistic: 0.5374734227423236
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.64
Correctly Classified Instances: 69.36
Weighted Precision: 0.7056892499657206
Weighted AreaUnderROC: 0.7696726441939012
Root mean squared error: 0.4509978057509229
Relative absolute error: 46.19061166429322
Root relative squared error: 95.67108202401904
Weighted TruePositiveRate: 0.6936
Weighted MatthewsCorrelation: 0.5468001794516762
Weighted FMeasure: 0.6925017558473991
Iteration time: 898.0
Weighted AreaUnderPRC: 0.5865220770016911
Mean absolute error: 0.20529160739685973
Coverage of cases: 69.36
Instances selection time: 898.0
Test time: 1285.0
Accumulative iteration time: 9098.0
Weighted Recall: 0.6936
Weighted FalsePositiveRate: 0.1542547116121977
Kappa statistic: 0.5398635545264091
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.7060938790079963
Weighted AreaUnderROC: 0.7690440172567942
Root mean squared error: 0.4516127030356747
Relative absolute error: 46.303734439835644
Root relative squared error: 95.8015214359534
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.5463431096235423
Weighted FMeasure: 0.691473092999328
Iteration time: 919.0
Weighted AreaUnderPRC: 0.5859460240741516
Mean absolute error: 0.2057943752881594
Coverage of cases: 69.28
Instances selection time: 919.0
Test time: 1295.0
Accumulative iteration time: 10017.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.1547119654864116
Kappa statistic: 0.5386284529274865
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.92
Correctly Classified Instances: 69.08
Weighted Precision: 0.7062873791611606
Weighted AreaUnderROC: 0.7674981985022814
Root mean squared error: 0.4531055668601564
Relative absolute error: 46.59650067294623
Root relative squared error: 96.11820567605713
Weighted TruePositiveRate: 0.6908
Weighted MatthewsCorrelation: 0.544637972711097
Weighted FMeasure: 0.6891571313610437
Iteration time: 931.0
Weighted AreaUnderPRC: 0.5843761516145469
Mean absolute error: 0.20709555854642864
Coverage of cases: 69.08
Instances selection time: 931.0
Test time: 1338.0
Accumulative iteration time: 10948.0
Weighted Recall: 0.6908
Weighted FalsePositiveRate: 0.15580360299543716
Kappa statistic: 0.5355738420768417
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.16
Correctly Classified Instances: 68.84
Weighted Precision: 0.7067490581818181
Weighted AreaUnderROC: 0.7656466491419667
Root mean squared error: 0.45488459292133426
Relative absolute error: 46.94941022280236
Root relative squared error: 96.49559409358707
Weighted TruePositiveRate: 0.6884
Weighted MatthewsCorrelation: 0.5426850478980436
Weighted FMeasure: 0.6862978069177254
Iteration time: 941.0
Weighted AreaUnderPRC: 0.5825576787878788
Mean absolute error: 0.20866404543467815
Coverage of cases: 68.84
Instances selection time: 941.0
Test time: 1363.0
Accumulative iteration time: 11889.0
Weighted Recall: 0.6884
Weighted FalsePositiveRate: 0.1571067017160666
Kappa statistic: 0.5319134105906543
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.7041376023508803
Weighted AreaUnderROC: 0.7626168082447522
Root mean squared error: 0.45781779334241285
Relative absolute error: 47.54176245210638
Root relative squared error: 97.11781986608425
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5374871166158588
Weighted FMeasure: 0.6819810950972951
Iteration time: 946.0
Weighted AreaUnderPRC: 0.5786902281234858
Mean absolute error: 0.2112967220093627
Coverage of cases: 68.44
Instances selection time: 946.0
Test time: 1392.0
Accumulative iteration time: 12835.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.15916638351049567
Kappa statistic: 0.5258742842143899
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.68
Correctly Classified Instances: 68.32
Weighted Precision: 0.7045106707434691
Weighted AreaUnderROC: 0.7616858838469439
Root mean squared error: 0.45870912417106335
Relative absolute error: 47.716064757161746
Root relative squared error: 97.30689968805005
Weighted TruePositiveRate: 0.6832
Weighted MatthewsCorrelation: 0.5366946018652283
Weighted FMeasure: 0.6803394885280576
Iteration time: 954.0
Weighted AreaUnderPRC: 0.5777630374194342
Mean absolute error: 0.21207139892071986
Coverage of cases: 68.32
Instances selection time: 954.0
Test time: 1422.0
Accumulative iteration time: 13789.0
Weighted Recall: 0.6832
Weighted FalsePositiveRate: 0.15982823230611226
Kappa statistic: 0.5240345714364012
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.7031587656715905
Weighted AreaUnderROC: 0.7592503384358301
Root mean squared error: 0.4610408261073869
Relative absolute error: 48.18955042527405
Root relative squared error: 97.8015283633141
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.5330151830871246
Weighted FMeasure: 0.6768231972197672
Iteration time: 964.0
Weighted AreaUnderPRC: 0.5748681812643708
Mean absolute error: 0.21417577966788567
Coverage of cases: 68.0
Instances selection time: 964.0
Test time: 1455.0
Accumulative iteration time: 14753.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.16149932312833964
Kappa statistic: 0.5191873111608165
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.92
Correctly Classified Instances: 68.08
Weighted Precision: 0.7056165256854762
Weighted AreaUnderROC: 0.7598240350513273
Root mean squared error: 0.46048400510246495
Relative absolute error: 48.06548042704689
Root relative squared error: 97.68340879076788
Weighted TruePositiveRate: 0.6808
Weighted MatthewsCorrelation: 0.535323375815577
Weighted FMeasure: 0.6771183619794757
Iteration time: 975.0
Weighted AreaUnderPRC: 0.5760114770366971
Mean absolute error: 0.21362435745354277
Coverage of cases: 68.08
Instances selection time: 975.0
Test time: 1486.0
Accumulative iteration time: 15728.0
Weighted Recall: 0.6808
Weighted FalsePositiveRate: 0.16115192989734534
Kappa statistic: 0.5203542886818038
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.705561365387996
Weighted AreaUnderROC: 0.7582885157321163
Root mean squared error: 0.4619433284075172
Relative absolute error: 48.36013904982744
Root relative squared error: 97.99297801225168
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5335456917544001
Weighted FMeasure: 0.6747448980328465
Iteration time: 983.0
Weighted AreaUnderPRC: 0.5743991833076392
Mean absolute error: 0.2149339513325674
Coverage of cases: 67.88
Instances selection time: 983.0
Test time: 1517.0
Accumulative iteration time: 16711.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.16222296853576723
Kappa statistic: 0.5173093005060377
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.7049198381486484
Weighted AreaUnderROC: 0.756766728993308
Root mean squared error: 0.4633974341338855
Relative absolute error: 48.6550396376001
Root relative squared error: 98.30144041815485
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5313617689373712
Weighted FMeasure: 0.6724366706925228
Iteration time: 995.0
Weighted AreaUnderPRC: 0.5726248491161697
Mean absolute error: 0.216244620611557
Coverage of cases: 67.68
Instances selection time: 995.0
Test time: 1561.0
Accumulative iteration time: 17706.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.1632665420133842
Kappa statistic: 0.5142793456429053
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.7051627502201016
Weighted AreaUnderROC: 0.7561518346366034
Root mean squared error: 0.4639879791367645
Relative absolute error: 48.770764119601424
Root relative squared error: 98.42671393099431
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5307996389864589
Weighted FMeasure: 0.6713647622602744
Iteration time: 1002.0
Weighted AreaUnderPRC: 0.572011110519543
Mean absolute error: 0.21675895164267403
Coverage of cases: 67.6
Instances selection time: 1002.0
Test time: 1590.0
Accumulative iteration time: 18708.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.1636963307267932
Kappa statistic: 0.5130592694638627
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.68
Correctly Classified Instances: 67.32
Weighted Precision: 0.7042054635050655
Weighted AreaUnderROC: 0.7540220198312916
Root mean squared error: 0.4660052452526743
Relative absolute error: 49.18569880823434
Root relative squared error: 98.85464069599962
Weighted TruePositiveRate: 0.6732
Weighted MatthewsCorrelation: 0.5277231413059924
Weighted FMeasure: 0.6680694768524631
Iteration time: 1003.0
Weighted AreaUnderPRC: 0.5695155680556363
Mean absolute error: 0.2186031058143759
Coverage of cases: 67.32
Instances selection time: 1003.0
Test time: 1619.0
Accumulative iteration time: 19711.0
Weighted Recall: 0.6732
Weighted FalsePositiveRate: 0.16515596033741675
Kappa statistic: 0.5088174777771473
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.7039307118626493
Weighted AreaUnderROC: 0.7531082611589864
Root mean squared error: 0.46687606876085735
Relative absolute error: 49.36161187698802
Root relative squared error: 99.03937025835549
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.5264805148354903
Weighted FMeasure: 0.6666140682274249
Iteration time: 1211.0
Weighted AreaUnderPRC: 0.5684728583403763
Mean absolute error: 0.21938494167550335
Coverage of cases: 67.2
Instances selection time: 1211.0
Test time: 1654.0
Accumulative iteration time: 20922.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.16578347768202717
Kappa statistic: 0.5069981018224478
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.84
Correctly Classified Instances: 67.16
Weighted Precision: 0.70425681470544
Weighted AreaUnderROC: 0.7527956642629834
Root mean squared error: 0.4671760424784623
Relative absolute error: 49.41806853582515
Root relative squared error: 99.10300429332433
Weighted TruePositiveRate: 0.6716
Weighted MatthewsCorrelation: 0.5263875468699291
Weighted FMeasure: 0.6660132878755322
Iteration time: 1016.0
Weighted AreaUnderPRC: 0.5682110851264172
Mean absolute error: 0.21963586015922393
Coverage of cases: 67.16
Instances selection time: 1016.0
Test time: 1692.0
Accumulative iteration time: 21938.0
Weighted Recall: 0.6716
Weighted FalsePositiveRate: 0.1660086714740335
Kappa statistic: 0.5063810915940247
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.12
Correctly Classified Instances: 66.88
Weighted Precision: 0.7025148412181358
Weighted AreaUnderROC: 0.7506761488929733
Root mean squared error: 0.4691782329029197
Relative absolute error: 49.83357070193182
Root relative squared error: 99.52773302123254
Weighted TruePositiveRate: 0.6688
Weighted MatthewsCorrelation: 0.5227731284484597
Weighted FMeasure: 0.6630911919534661
Iteration time: 1029.0
Weighted AreaUnderPRC: 0.565635624536887
Mean absolute error: 0.22148253645303134
Coverage of cases: 66.88
Instances selection time: 1029.0
Test time: 1720.0
Accumulative iteration time: 22967.0
Weighted Recall: 0.6688
Weighted FalsePositiveRate: 0.16744770221405336
Kappa statistic: 0.5021542052096315
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.0
Correctly Classified Instances: 67.0
Weighted Precision: 0.7047100793972738
Weighted AreaUnderROC: 0.7515624424044736
Root mean squared error: 0.4683417151306846
Relative absolute error: 49.65104685942246
Root relative squared error: 99.35028080443338
Weighted TruePositiveRate: 0.67
Weighted MatthewsCorrelation: 0.5253176928177667
Weighted FMeasure: 0.6639499366434922
Iteration time: 1026.0
Weighted AreaUnderPRC: 0.5670390703629725
Mean absolute error: 0.22067131937521198
Coverage of cases: 67.0
Instances selection time: 1026.0
Test time: 1743.0
Accumulative iteration time: 23993.0
Weighted Recall: 0.67
Weighted FalsePositiveRate: 0.1668751151910527
Kappa statistic: 0.5039375930643141
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.12
Correctly Classified Instances: 66.88
Weighted Precision: 0.7043951035733699
Weighted AreaUnderROC: 0.7506486837321684
Root mean squared error: 0.4692061420612576
Relative absolute error: 49.82756598240387
Root relative squared error: 99.53365344776792
Weighted TruePositiveRate: 0.6688
Weighted MatthewsCorrelation: 0.5240758611693225
Weighted FMeasure: 0.6625382779732865
Iteration time: 1027.0
Weighted AreaUnderPRC: 0.5659981205591662
Mean absolute error: 0.2214558488106849
Coverage of cases: 66.88
Instances selection time: 1027.0
Test time: 1770.0
Accumulative iteration time: 25020.0
Weighted Recall: 0.6688
Weighted FalsePositiveRate: 0.16750263253566314
Kappa statistic: 0.5021178032571597
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.7044533747412007
Weighted AreaUnderROC: 0.7500372225205645
Root mean squared error: 0.4697856390992962
Relative absolute error: 49.94439117929183
Root relative squared error: 99.6565833333503
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.5234036728321165
Weighted FMeasure: 0.661533787106259
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.5653542101229021
Mean absolute error: 0.22197507190796473
Coverage of cases: 66.8
Instances selection time: 1033.0
Test time: 1806.0
Accumulative iteration time: 26053.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.1679255549588709
Kappa statistic: 0.5009015041746283
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.28
Correctly Classified Instances: 66.72
Weighted Precision: 0.7044352489504186
Weighted AreaUnderROC: 0.7494257613089607
Root mean squared error: 0.47036399121091205
Relative absolute error: 50.061335841958126
Root relative squared error: 99.77927034336143
Weighted TruePositiveRate: 0.6672
Weighted MatthewsCorrelation: 0.5227053061548453
Weighted FMeasure: 0.6605571548330328
Iteration time: 1051.0
Weighted AreaUnderPRC: 0.5647006036064822
Mean absolute error: 0.22249482596425937
Coverage of cases: 66.72
Instances selection time: 1050.0
Test time: 1844.0
Accumulative iteration time: 27104.0
Weighted Recall: 0.6672
Weighted FalsePositiveRate: 0.1683484773820787
Kappa statistic: 0.4996851383875719
Training time: 1.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.705939696981549
Weighted AreaUnderROC: 0.7500200567950615
Root mean squared error: 0.4698105222298114
Relative absolute error: 49.939058171746545
Root relative squared error: 99.66186184244764
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.5243618786500316
Weighted FMeasure: 0.6612011194869906
Iteration time: 1038.0
Weighted AreaUnderPRC: 0.5656457515062869
Mean absolute error: 0.2219513696522079
Coverage of cases: 66.8
Instances selection time: 1038.0
Test time: 1872.0
Accumulative iteration time: 28142.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.16795988640987705
Kappa statistic: 0.5008786940388801
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.16
Correctly Classified Instances: 66.84
Weighted Precision: 0.707953514762494
Weighted AreaUnderROC: 0.7503017553851591
Root mean squared error: 0.46953917974388837
Relative absolute error: 49.87669990933735
Root relative squared error: 99.60430140890158
Weighted TruePositiveRate: 0.6684
Weighted MatthewsCorrelation: 0.5260287430617747
Weighted FMeasure: 0.6610743733800639
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5663444248483086
Mean absolute error: 0.22167422181927815
Coverage of cases: 66.84
Instances selection time: 1035.0
Test time: 1902.0
Accumulative iteration time: 29177.0
Weighted Recall: 0.6684
Weighted FalsePositiveRate: 0.16779648922968174
Kappa statistic: 0.5014549816855368
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.28
Correctly Classified Instances: 66.72
Weighted Precision: 0.707463444342327
Weighted AreaUnderROC: 0.7493914298579545
Root mean squared error: 0.47039935631040414
Relative absolute error: 50.053784505787796
Root relative squared error: 99.7867724138619
Weighted TruePositiveRate: 0.6672
Weighted MatthewsCorrelation: 0.524657141474672
Weighted FMeasure: 0.6597026448214497
Iteration time: 1038.0
Weighted AreaUnderPRC: 0.5652679027869837
Mean absolute error: 0.222461264470169
Coverage of cases: 66.72
Instances selection time: 1038.0
Test time: 1954.0
Accumulative iteration time: 30215.0
Weighted Recall: 0.6672
Weighted FalsePositiveRate: 0.16841714028409094
Kappa statistic: 0.4996394035894137
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.52
Correctly Classified Instances: 66.48
Weighted Precision: 0.7061580652296718
Weighted AreaUnderROC: 0.7475742119486459
Root mean squared error: 0.47210345354568584
Relative absolute error: 50.41049868766333
Root relative squared error: 100.14826602712256
Weighted TruePositiveRate: 0.6648
Weighted MatthewsCorrelation: 0.5217104852369133
Weighted FMeasure: 0.6570504224974395
Iteration time: 1043.0
Weighted AreaUnderPRC: 0.5630607260780056
Mean absolute error: 0.22404666083406027
Coverage of cases: 66.48
Instances selection time: 1043.0
Test time: 1969.0
Accumulative iteration time: 31258.0
Weighted Recall: 0.6648
Weighted FalsePositiveRate: 0.16965157610270812
Kappa statistic: 0.49601260569902683
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.68
Correctly Classified Instances: 66.32
Weighted Precision: 0.7050551955262265
Weighted AreaUnderROC: 0.7463650221058405
Root mean squared error: 0.47323949566311274
Relative absolute error: 50.64763542562265
Root relative squared error: 100.38925695260639
Weighted TruePositiveRate: 0.6632
Weighted MatthewsCorrelation: 0.5195970430777661
Weighted FMeasure: 0.6552483317367154
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5615341571150116
Mean absolute error: 0.2251006018916573
Coverage of cases: 66.32
Instances selection time: 1037.0
Test time: 1995.0
Accumulative iteration time: 32295.0
Weighted Recall: 0.6632
Weighted FalsePositiveRate: 0.1704699557883188
Kappa statistic: 0.49359767935423
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.88
Correctly Classified Instances: 66.12
Weighted Precision: 0.7041551826257363
Weighted AreaUnderROC: 0.7448466685121328
Root mean squared error: 0.47465283597027635
Relative absolute error: 50.944716821638856
Root relative squared error: 100.6890717072023
Weighted TruePositiveRate: 0.6612
Weighted MatthewsCorrelation: 0.5173154921950941
Weighted FMeasure: 0.652965939054088
Iteration time: 1036.0
Weighted AreaUnderPRC: 0.5597414677700824
Mean absolute error: 0.22642096365172934
Coverage of cases: 66.12
Instances selection time: 1036.0
Test time: 2023.0
Accumulative iteration time: 33331.0
Weighted Recall: 0.6612
Weighted FalsePositiveRate: 0.17150666297573453
Kappa statistic: 0.490569582902192
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.56
Correctly Classified Instances: 66.44
Weighted Precision: 0.7046919119943776
Weighted AreaUnderROC: 0.7472753476330452
Root mean squared error: 0.4724158939077023
Relative absolute error: 50.463840399002684
Root relative squared error: 100.21454463673203
Weighted TruePositiveRate: 0.6644
Weighted MatthewsCorrelation: 0.520603595551094
Weighted FMeasure: 0.6571609501790002
Iteration time: 1051.0
Weighted AreaUnderPRC: 0.5625458968952054
Mean absolute error: 0.22428373510667965
Coverage of cases: 66.44
Instances selection time: 1051.0
Test time: 2300.0
Accumulative iteration time: 34382.0
Weighted Recall: 0.6644
Weighted FalsePositiveRate: 0.16984930473390958
Kappa statistic: 0.4954158014360742
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.08
Correctly Classified Instances: 66.92
Weighted Precision: 0.7092448983706788
Weighted AreaUnderROC: 0.750885751435958
Root mean squared error: 0.4690348661170121
Relative absolute error: 49.743581357316266
Root relative squared error: 99.49732033327888
Weighted TruePositiveRate: 0.6692
Weighted MatthewsCorrelation: 0.5273761236919845
Weighted FMeasure: 0.6630308536574434
Iteration time: 1069.0
Weighted AreaUnderPRC: 0.5677696944286393
Mean absolute error: 0.22108258381029555
Coverage of cases: 66.92
Instances selection time: 1069.0
Test time: 2409.0
Accumulative iteration time: 35451.0
Weighted Recall: 0.6692
Weighted FalsePositiveRate: 0.16742849712808378
Kappa statistic: 0.5026554709822622
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.84
Correctly Classified Instances: 67.16
Weighted Precision: 0.7090166399073922
Weighted AreaUnderROC: 0.7527235682158704
Root mean squared error: 0.46733952192818395
Relative absolute error: 49.382461786001215
Root relative squared error: 99.13768352156919
Weighted TruePositiveRate: 0.6716
Weighted MatthewsCorrelation: 0.5292150206538062
Weighted FMeasure: 0.6662744974483162
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.5697520785489126
Mean absolute error: 0.2194776079377842
Coverage of cases: 67.16
Instances selection time: 1042.0
Test time: 2126.0
Accumulative iteration time: 36493.0
Weighted Recall: 0.6716
Weighted FalsePositiveRate: 0.16615286356825923
Kappa statistic: 0.506311159388254
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.7091032113479935
Weighted AreaUnderROC: 0.7530292988216724
Root mean squared error: 0.46706372391162865
Relative absolute error: 49.320665083134955
Root relative squared error: 99.07917792724598
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.529594316566599
Weighted FMeasure: 0.6667565078684856
Iteration time: 1055.0
Weighted AreaUnderPRC: 0.5700966468234935
Mean absolute error: 0.21920295592504527
Coverage of cases: 67.2
Instances selection time: 1055.0
Test time: 2158.0
Accumulative iteration time: 37548.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.16594140235665536
Kappa statistic: 0.5069192458798893
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.76
Correctly Classified Instances: 67.24
Weighted Precision: 0.7078265475314307
Weighted AreaUnderROC: 0.7533590614431785
Root mean squared error: 0.4667874643806633
Relative absolute error: 49.258924395946224
Root relative squared error: 99.02057443093209
Weighted TruePositiveRate: 0.6724
Weighted MatthewsCorrelation: 0.5289371763641866
Weighted FMeasure: 0.6676416716957132
Iteration time: 1049.0
Weighted AreaUnderPRC: 0.5701814561866846
Mean absolute error: 0.21892855287087315
Coverage of cases: 67.24
Instances selection time: 1049.0
Test time: 2191.0
Accumulative iteration time: 38597.0
Weighted Recall: 0.6724
Weighted FalsePositiveRate: 0.16568187711364288
Kappa statistic: 0.5075588217680407
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.84
Correctly Classified Instances: 67.16
Weighted Precision: 0.703744025071251
Weighted AreaUnderROC: 0.7528196962786875
Root mean squared error: 0.46736542853070234
Relative absolute error: 49.37682271680688
Root relative squared error: 99.14317914186468
Weighted TruePositiveRate: 0.6716
Weighted MatthewsCorrelation: 0.525779226921264
Weighted FMeasure: 0.6663994400076434
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5681262331038387
Mean absolute error: 0.21945254540803158
Coverage of cases: 67.16
Instances selection time: 1041.0
Test time: 2219.0
Accumulative iteration time: 39638.0
Weighted Recall: 0.6716
Weighted FalsePositiveRate: 0.16596060744262492
Kappa statistic: 0.5064104121070251
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.08
Correctly Classified Instances: 66.92
Weighted Precision: 0.7002053657832228
Weighted AreaUnderROC: 0.7510471092556869
Root mean squared error: 0.46907824548774774
Relative absolute error: 49.73424036281307
Root relative squared error: 99.50652248744211
Weighted TruePositiveRate: 0.6692
Weighted MatthewsCorrelation: 0.5215408027348373
Weighted FMeasure: 0.6632252912938534
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5650092521734053
Mean absolute error: 0.22104106827917022
Coverage of cases: 66.92
Instances selection time: 1037.0
Test time: 2280.0
Accumulative iteration time: 40675.0
Weighted Recall: 0.6692
Weighted FalsePositiveRate: 0.16710578148862618
Kappa statistic: 0.5028213535523325
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.48
Correctly Classified Instances: 67.52
Weighted Precision: 0.706093883492676
Weighted AreaUnderROC: 0.7555747048760055
Root mean squared error: 0.464812595539763
Relative absolute error: 48.83454951600813
Root relative squared error: 98.6016414861257
Weighted TruePositiveRate: 0.6752
Weighted MatthewsCorrelation: 0.5300400608954811
Weighted FMeasure: 0.6696808929173645
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5713092990491164
Mean absolute error: 0.2170424422933705
Coverage of cases: 67.52
Instances selection time: 1035.0
Test time: 2291.0
Accumulative iteration time: 41710.0
Weighted Recall: 0.6752
Weighted FalsePositiveRate: 0.1640505902479887
Kappa statistic: 0.5118792480632066
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.48
Correctly Classified Instances: 67.52
Weighted Precision: 0.7051001632643009
Weighted AreaUnderROC: 0.7555953037466093
Root mean squared error: 0.46482019550442405
Relative absolute error: 48.83286867204763
Root relative squared error: 98.60325368209027
Weighted TruePositiveRate: 0.6752
Weighted MatthewsCorrelation: 0.5295515828883195
Weighted FMeasure: 0.669202461863312
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5708219006845259
Mean absolute error: 0.21703497187576826
Coverage of cases: 67.52
Instances selection time: 1035.0
Test time: 2316.0
Accumulative iteration time: 42745.0
Weighted Recall: 0.6752
Weighted FalsePositiveRate: 0.16400939250678137
Kappa statistic: 0.5118926279195974
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.52
Correctly Classified Instances: 67.48
Weighted Precision: 0.7048340558267036
Weighted AreaUnderROC: 0.7553101720114109
Root mean squared error: 0.46511371045437677
Relative absolute error: 48.89110629067223
Root relative squared error: 98.66551760553763
Weighted TruePositiveRate: 0.6748
Weighted MatthewsCorrelation: 0.5289123165104105
Weighted FMeasure: 0.6683073318506234
Iteration time: 1029.0
Weighted AreaUnderPRC: 0.5702482755512281
Mean absolute error: 0.217293805736322
Coverage of cases: 67.48
Instances selection time: 1029.0
Test time: 2349.0
Accumulative iteration time: 43774.0
Weighted Recall: 0.6748
Weighted FalsePositiveRate: 0.1641796559771779
Kappa statistic: 0.511302673517005
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.7037058904858666
Weighted AreaUnderROC: 0.7547193096704108
Root mean squared error: 0.4656926356749587
Relative absolute error: 49.009265858873604
Root relative squared error: 98.78832619031967
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.5274418201885233
Weighted FMeasure: 0.6675399180293525
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5693353631870137
Mean absolute error: 0.2178189593727726
Coverage of cases: 67.4
Instances selection time: 1037.0
Test time: 2391.0
Accumulative iteration time: 44811.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.1645613806591783
Kappa statistic: 0.510111656072656
Training time: 0.0
		
Time end:Wed Nov 01 16.59.44 EET 2017