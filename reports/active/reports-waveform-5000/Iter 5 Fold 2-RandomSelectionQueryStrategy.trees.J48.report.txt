Wed Nov 01 17.26.29 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.26.29 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 36.933333333334254
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7254400234501466
Weighted AreaUnderROC: 0.7962617695264307
Root mean squared error: 0.4218571469022131
Relative absolute error: 43.11873628641738
Root relative squared error: 89.48941477996912
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5863870314560093
Weighted FMeasure: 0.724358343798185
Iteration time: 76.0
Weighted AreaUnderPRC: 0.6335112651139128
Mean absolute error: 0.19163882793963372
Coverage of cases: 76.4
Instances selection time: 10.0
Test time: 7.0
Accumulative iteration time: 76.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.1383988222445448
Kappa statistic: 0.5858737080460056
Training time: 66.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 35.133333333334406
Incorrectly Classified Instances: 28.96
Correctly Classified Instances: 71.04
Weighted Precision: 0.7109879734575056
Weighted AreaUnderROC: 0.7672799081821209
Root mean squared error: 0.43083760642470004
Relative absolute error: 44.719213920322154
Root relative squared error: 91.39445792792566
Weighted TruePositiveRate: 0.7104
Weighted MatthewsCorrelation: 0.5657684430206016
Weighted FMeasure: 0.7103158375745727
Iteration time: 85.0
Weighted AreaUnderPRC: 0.5918923142792676
Mean absolute error: 0.1987520618680994
Coverage of cases: 73.44
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 161.0
Weighted Recall: 0.7104
Weighted FalsePositiveRate: 0.14494974929734886
Kappa statistic: 0.5655863767887761
Training time: 75.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 38.120000000001085
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7259971817168338
Weighted AreaUnderROC: 0.8175138896763731
Root mean squared error: 0.4169432206250681
Relative absolute error: 41.819982039961076
Root relative squared error: 88.44701360212314
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5867021608821271
Weighted FMeasure: 0.7243079512707029
Iteration time: 83.0
Weighted AreaUnderPRC: 0.6594031218661499
Mean absolute error: 0.18586658684427232
Coverage of cases: 78.4
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 244.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13845010029785734
Kappa statistic: 0.5858343337334934
Training time: 74.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 35.98666666666785
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.7232279911045217
Weighted AreaUnderROC: 0.7814343961760769
Root mean squared error: 0.4257265977524714
Relative absolute error: 42.81305683111047
Root relative squared error: 90.31024926067482
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.5820962121041261
Weighted FMeasure: 0.7211336636508555
Iteration time: 90.0
Weighted AreaUnderPRC: 0.6120866335351274
Mean absolute error: 0.19028025258271408
Coverage of cases: 74.72
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 334.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.140078766226691
Kappa statistic: 0.5810229570607356
Training time: 81.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 37.586666666667696
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7241034241502136
Weighted AreaUnderROC: 0.8175897122409089
Root mean squared error: 0.4208303684502933
Relative absolute error: 42.140561005593575
Root relative squared error: 89.27160217813051
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5845958564777626
Weighted FMeasure: 0.7229455164056054
Iteration time: 93.0
Weighted AreaUnderPRC: 0.6606854288968874
Mean absolute error: 0.18729138224708342
Coverage of cases: 77.44
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 427.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.138971255022073
Kappa statistic: 0.5840745568863569
Training time: 84.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 35.68000000000098
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7297908009599703
Weighted AreaUnderROC: 0.8037283064640378
Root mean squared error: 0.4188806364558266
Relative absolute error: 41.76958080808068
Root relative squared error: 88.85800156369538
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5922647221648487
Weighted FMeasure: 0.7278053190558057
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6444261869330034
Mean absolute error: 0.18564258136924836
Coverage of cases: 75.92
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 527.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.1366077981557299
Kappa statistic: 0.5912384574383797
Training time: 91.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 37.50666666666767
Incorrectly Classified Instances: 28.76
Correctly Classified Instances: 71.24
Weighted Precision: 0.7152532660694787
Weighted AreaUnderROC: 0.7898560017951527
Root mean squared error: 0.43232040334486443
Relative absolute error: 44.06796128871134
Root relative squared error: 91.70900665513689
Weighted TruePositiveRate: 0.7124
Weighted MatthewsCorrelation: 0.569851245679622
Weighted FMeasure: 0.7121203306287877
Iteration time: 105.0
Weighted AreaUnderPRC: 0.6143751022278824
Mean absolute error: 0.19585760572760688
Coverage of cases: 74.32
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 632.0
Weighted Recall: 0.7124
Weighted FalsePositiveRate: 0.1442269086003015
Kappa statistic: 0.5684456071003964
Training time: 96.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 38.72000000000092
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7213337299872157
Weighted AreaUnderROC: 0.7985621278287501
Root mean squared error: 0.42496215518879304
Relative absolute error: 43.3901767563704
Root relative squared error: 90.14808650449345
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5797457866829675
Weighted FMeasure: 0.7188529829453537
Iteration time: 109.0
Weighted AreaUnderPRC: 0.6262361956415163
Mean absolute error: 0.1928452300283138
Coverage of cases: 76.2
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 741.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.14077268644659402
Kappa statistic: 0.5786658472057502
Training time: 101.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 39.32000000000096
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7266835905399465
Weighted AreaUnderROC: 0.8236497513409711
Root mean squared error: 0.41537092486775684
Relative absolute error: 41.41481268731266
Root relative squared error: 88.11347930451544
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5899735079303958
Weighted FMeasure: 0.7267057875659645
Iteration time: 115.0
Weighted AreaUnderPRC: 0.6639278012510641
Mean absolute error: 0.1840658341658349
Coverage of cases: 79.08
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 856.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13676194908714437
Kappa statistic: 0.5902007212467306
Training time: 106.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 36.74666666666784
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7179319071631499
Weighted AreaUnderROC: 0.7951444568755296
Root mean squared error: 0.426923405510642
Relative absolute error: 43.28595311808978
Root relative squared error: 90.56413052514856
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5737300124253829
Weighted FMeasure: 0.7157351849301122
Iteration time: 117.0
Weighted AreaUnderPRC: 0.6230508960708279
Mean absolute error: 0.19238201385817771
Coverage of cases: 74.96
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 973.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.14291340937847818
Kappa statistic: 0.5726333612001991
Training time: 109.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 36.41333333333434
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7243655145555956
Weighted AreaUnderROC: 0.7792750861459544
Root mean squared error: 0.42256370974917995
Relative absolute error: 42.90418773628158
Root relative squared error: 89.63929939409654
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.58638535533569
Weighted FMeasure: 0.7243664837121364
Iteration time: 117.0
Weighted AreaUnderPRC: 0.6131110331802829
Mean absolute error: 0.190685278827919
Coverage of cases: 75.16
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 1090.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13801134421123787
Kappa statistic: 0.5865721515001252
Training time: 109.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 35.93333333333444
Incorrectly Classified Instances: 27.52
Correctly Classified Instances: 72.48
Weighted Precision: 0.7257496835307978
Weighted AreaUnderROC: 0.7937514917983006
Root mean squared error: 0.4219528502968355
Relative absolute error: 42.372819770464616
Root relative squared error: 89.50971653576514
Weighted TruePositiveRate: 0.7248
Weighted MatthewsCorrelation: 0.5873610138531639
Weighted FMeasure: 0.7250629160714401
Iteration time: 123.0
Weighted AreaUnderPRC: 0.6231581059275907
Mean absolute error: 0.18832364342428806
Coverage of cases: 75.2
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1213.0
Weighted Recall: 0.7248
Weighted FalsePositiveRate: 0.1379555815347539
Kappa statistic: 0.5871018293309417
Training time: 115.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 36.200000000000955
Incorrectly Classified Instances: 28.68
Correctly Classified Instances: 71.32
Weighted Precision: 0.7138260552180935
Weighted AreaUnderROC: 0.7985318148604568
Root mean squared error: 0.42801234064369226
Relative absolute error: 43.35761904761956
Root relative squared error: 90.79512855020418
Weighted TruePositiveRate: 0.7132
Weighted MatthewsCorrelation: 0.5698060709961429
Weighted FMeasure: 0.7134033926809897
Iteration time: 127.0
Weighted AreaUnderPRC: 0.6269507066778751
Mean absolute error: 0.1927005291005323
Coverage of cases: 74.76
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1340.0
Weighted Recall: 0.7132
Weighted FalsePositiveRate: 0.1437284135350877
Kappa statistic: 0.5697197957699314
Training time: 119.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 39.920000000000705
Incorrectly Classified Instances: 27.88
Correctly Classified Instances: 72.12
Weighted Precision: 0.7233037452944161
Weighted AreaUnderROC: 0.8118793059557001
Root mean squared error: 0.4147433574766976
Relative absolute error: 42.64254386590579
Root relative squared error: 87.98035215715458
Weighted TruePositiveRate: 0.7212
Weighted MatthewsCorrelation: 0.5826720146605018
Weighted FMeasure: 0.7210975615954699
Iteration time: 122.0
Weighted AreaUnderPRC: 0.6482975882274926
Mean absolute error: 0.18952241718180443
Coverage of cases: 78.48
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1462.0
Weighted Recall: 0.7212
Weighted FalsePositiveRate: 0.13975114095815752
Kappa statistic: 0.5816832729004701
Training time: 114.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 39.61333333333408
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7281512730228009
Weighted AreaUnderROC: 0.8239058269709216
Root mean squared error: 0.4141485471452466
Relative absolute error: 41.88807165264356
Root relative squared error: 87.85417383148794
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5899382320213198
Weighted FMeasure: 0.7258525394387163
Iteration time: 128.0
Weighted AreaUnderPRC: 0.669732937004236
Mean absolute error: 0.18616920734508338
Coverage of cases: 78.2
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1590.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.13736831322762225
Kappa statistic: 0.5888529080163719
Training time: 121.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.306666666667326
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7156923468331886
Weighted AreaUnderROC: 0.8139326897891417
Root mean squared error: 0.4203932002076641
Relative absolute error: 43.60189613894864
Root relative squared error: 89.17886478946575
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5729170863654883
Weighted FMeasure: 0.7150524554763452
Iteration time: 138.0
Weighted AreaUnderPRC: 0.6534203330970033
Mean absolute error: 0.19378620506199484
Coverage of cases: 78.0
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1728.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.1426047932090995
Kappa statistic: 0.5727576859212136
Training time: 131.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 38.89333333333428
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7206054368988632
Weighted AreaUnderROC: 0.8148061898011448
Root mean squared error: 0.42008943954924316
Relative absolute error: 43.17920942226986
Root relative squared error: 89.11442742303763
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5797680821421207
Weighted FMeasure: 0.7195797090342043
Iteration time: 132.0
Weighted AreaUnderPRC: 0.6533316581167581
Mean absolute error: 0.19190759743231137
Coverage of cases: 77.88
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1860.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.1404822035724149
Kappa statistic: 0.5792797749885007
Training time: 124.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 39.640000000000775
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.713854323401104
Weighted AreaUnderROC: 0.8199705752247548
Root mean squared error: 0.42245078548565346
Relative absolute error: 43.965194490030406
Root relative squared error: 89.61534454034651
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5695112969726428
Weighted FMeasure: 0.7127524586122057
Iteration time: 133.0
Weighted AreaUnderPRC: 0.6585252173199484
Mean absolute error: 0.19540086440013607
Coverage of cases: 78.68
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1993.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.1439676183559543
Kappa statistic: 0.56907416965754
Training time: 126.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 39.186666666667456
Incorrectly Classified Instances: 28.56
Correctly Classified Instances: 71.44
Weighted Precision: 0.7166058605540142
Weighted AreaUnderROC: 0.820460250006686
Root mean squared error: 0.4233467293696635
Relative absolute error: 43.88112782490269
Root relative squared error: 89.80540293913035
Weighted TruePositiveRate: 0.7144
Weighted MatthewsCorrelation: 0.5724789444938967
Weighted FMeasure: 0.7143563719202471
Iteration time: 138.0
Weighted AreaUnderPRC: 0.6592622608051079
Mean absolute error: 0.19502723477734618
Coverage of cases: 78.0
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 2131.0
Weighted Recall: 0.7144
Weighted FalsePositiveRate: 0.14321123511298237
Kappa statistic: 0.5714526482785842
Training time: 130.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 38.93333333333417
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.71727957531474
Weighted AreaUnderROC: 0.8150177843644074
Root mean squared error: 0.4231270170394431
Relative absolute error: 43.32825367635656
Root relative squared error: 89.75879491554761
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5757308817723596
Weighted FMeasure: 0.7168590335919771
Iteration time: 144.0
Weighted AreaUnderPRC: 0.6494248917042243
Mean absolute error: 0.1925700163393634
Coverage of cases: 78.16
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 2275.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14164769889478707
Kappa statistic: 0.5757429119662341
Training time: 137.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 35.65333333333439
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7256996549990224
Weighted AreaUnderROC: 0.8115465208192922
Root mean squared error: 0.41670394419195866
Relative absolute error: 42.02701473513445
Root relative squared error: 88.39625540559418
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5883229058087809
Weighted FMeasure: 0.7252943139053963
Iteration time: 146.0
Weighted AreaUnderPRC: 0.638915648589821
Mean absolute error: 0.18678673215615396
Coverage of cases: 75.92
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2421.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.137430662464512
Kappa statistic: 0.5883691112181059
Training time: 139.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 37.240000000001004
Incorrectly Classified Instances: 28.52
Correctly Classified Instances: 71.48
Weighted Precision: 0.7153501185469955
Weighted AreaUnderROC: 0.8258296377591888
Root mean squared error: 0.4236292187574524
Relative absolute error: 43.222801198649
Root relative squared error: 89.86532798764598
Weighted TruePositiveRate: 0.7148
Weighted MatthewsCorrelation: 0.5722694008802206
Weighted FMeasure: 0.714589027712685
Iteration time: 145.0
Weighted AreaUnderPRC: 0.6656678801881543
Mean absolute error: 0.19210133866066315
Coverage of cases: 76.48
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2566.0
Weighted Recall: 0.7148
Weighted FalsePositiveRate: 0.14293376983280473
Kappa statistic: 0.57212599491208
Training time: 139.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 36.69333333333442
Incorrectly Classified Instances: 29.16
Correctly Classified Instances: 70.84
Weighted Precision: 0.7096112839130647
Weighted AreaUnderROC: 0.8016199889797504
Root mean squared error: 0.42940050790938894
Relative absolute error: 44.18255041628153
Root relative squared error: 91.08960329630277
Weighted TruePositiveRate: 0.7084
Weighted MatthewsCorrelation: 0.5630071354294579
Weighted FMeasure: 0.7081511366487448
Iteration time: 149.0
Weighted AreaUnderPRC: 0.6337930229607677
Mean absolute error: 0.19636689073902994
Coverage of cases: 75.68
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2715.0
Weighted Recall: 0.7084
Weighted FalsePositiveRate: 0.14616093467354863
Kappa statistic: 0.5625090018723895
Training time: 143.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 36.98666666666772
Incorrectly Classified Instances: 28.64
Correctly Classified Instances: 71.36
Weighted Precision: 0.7144617564544467
Weighted AreaUnderROC: 0.8106005990709547
Root mean squared error: 0.4246138967854655
Relative absolute error: 44.000067222700366
Root relative squared error: 90.07420974091403
Weighted TruePositiveRate: 0.7136
Weighted MatthewsCorrelation: 0.5705043313419039
Weighted FMeasure: 0.713815892827895
Iteration time: 162.0
Weighted AreaUnderPRC: 0.6428965062938141
Mean absolute error: 0.19555585432311365
Coverage of cases: 75.72
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 2877.0
Weighted Recall: 0.7136
Weighted FalsePositiveRate: 0.14354318947787723
Kappa statistic: 0.5703240332890855
Training time: 156.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 38.42666666666774
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.724830305997552
Weighted AreaUnderROC: 0.8216229433037865
Root mean squared error: 0.4157776361938444
Relative absolute error: 42.78069589567852
Root relative squared error: 88.199755805514
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.58612597206783
Weighted FMeasure: 0.7240955737434966
Iteration time: 159.0
Weighted AreaUnderPRC: 0.6630018538229567
Mean absolute error: 0.19013642620301652
Coverage of cases: 77.76
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 3036.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13832341939087947
Kappa statistic: 0.5859415018153764
Training time: 152.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 36.49333333333444
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7276273707567376
Weighted AreaUnderROC: 0.8126762993233319
Root mean squared error: 0.41919679154821726
Relative absolute error: 41.47558658008645
Root relative squared error: 88.92506818661622
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5896819255347353
Weighted FMeasure: 0.7256137871899334
Iteration time: 163.0
Weighted AreaUnderPRC: 0.6507061555730479
Mean absolute error: 0.18433594035594064
Coverage of cases: 76.6
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3199.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.13735458107433232
Kappa statistic: 0.5889048232446775
Training time: 157.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 35.880000000001004
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7410079824233359
Weighted AreaUnderROC: 0.8124586761726589
Root mean squared error: 0.4100655126504019
Relative absolute error: 39.38366921313969
Root relative squared error: 86.98803141775096
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6108629666697594
Weighted FMeasure: 0.7399306869559703
Iteration time: 166.0
Weighted AreaUnderPRC: 0.6507212176657633
Mean absolute error: 0.1750385298361772
Coverage of cases: 76.64
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3365.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.130007261176262
Kappa statistic: 0.6105758245471878
Training time: 160.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 35.986666666667695
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.7323135293605392
Weighted AreaUnderROC: 0.8070977838075868
Root mean squared error: 0.41626249871761883
Relative absolute error: 41.281251687380326
Root relative squared error: 88.30261067906524
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5951354721182851
Weighted FMeasure: 0.7290198014270368
Iteration time: 178.0
Weighted AreaUnderPRC: 0.6412293878605682
Mean absolute error: 0.18347222972169122
Coverage of cases: 76.2
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 3543.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13589562513659142
Kappa statistic: 0.5936302349669598
Training time: 173.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 35.72000000000107
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.730549168621117
Weighted AreaUnderROC: 0.8121444697557415
Root mean squared error: 0.41708375835957295
Relative absolute error: 41.47662409321466
Root relative squared error: 88.47682615764742
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5926045539957717
Weighted FMeasure: 0.7276729474130693
Iteration time: 179.0
Weighted AreaUnderPRC: 0.6475052312528563
Mean absolute error: 0.18434055152539938
Coverage of cases: 75.56
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3722.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13669090563475775
Kappa statistic: 0.5912264838178509
Training time: 173.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 35.693333333334394
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7260131793476137
Weighted AreaUnderROC: 0.7975233944409229
Root mean squared error: 0.420742763357117
Relative absolute error: 42.056602053723445
Root relative squared error: 89.25301833149507
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5867474909923214
Weighted FMeasure: 0.7240016071902484
Iteration time: 186.0
Weighted AreaUnderPRC: 0.6285721187031799
Mean absolute error: 0.18691823134988286
Coverage of cases: 75.24
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3908.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13842168814177402
Kappa statistic: 0.5858695323374675
Training time: 180.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 38.58666666666756
Incorrectly Classified Instances: 27.76
Correctly Classified Instances: 72.24
Weighted Precision: 0.7238179069835496
Weighted AreaUnderROC: 0.8068687661633398
Root mean squared error: 0.4185606357570609
Relative absolute error: 42.47153348612204
Root relative squared error: 88.79011916447087
Weighted TruePositiveRate: 0.7224
Weighted MatthewsCorrelation: 0.5840919863800669
Weighted FMeasure: 0.7223171377089462
Iteration time: 189.0
Weighted AreaUnderPRC: 0.6419715938462647
Mean absolute error: 0.1887623710494322
Coverage of cases: 78.64
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 4097.0
Weighted Recall: 0.7224
Weighted FalsePositiveRate: 0.13917505762078225
Kappa statistic: 0.5834849752107543
Training time: 183.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 37.25333333333435
Incorrectly Classified Instances: 28.68
Correctly Classified Instances: 71.32
Weighted Precision: 0.7155947006610505
Weighted AreaUnderROC: 0.7927632529236914
Root mean squared error: 0.4277074171739389
Relative absolute error: 43.57303862210162
Root relative squared error: 90.73044451424254
Weighted TruePositiveRate: 0.7132
Weighted MatthewsCorrelation: 0.5706274144780564
Weighted FMeasure: 0.7135002421214169
Iteration time: 202.0
Weighted AreaUnderPRC: 0.6286839123632911
Mean absolute error: 0.19365794943156367
Coverage of cases: 76.12
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4299.0
Weighted Recall: 0.7132
Weighted FalsePositiveRate: 0.1439089974423167
Kappa statistic: 0.5696276444790609
Training time: 197.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 35.586666666667725
Incorrectly Classified Instances: 26.68
Correctly Classified Instances: 73.32
Weighted Precision: 0.7342259800848715
Weighted AreaUnderROC: 0.7977085670063172
Root mean squared error: 0.414381781270608
Relative absolute error: 41.440906498610346
Root relative squared error: 87.90365026098208
Weighted TruePositiveRate: 0.7332
Weighted MatthewsCorrelation: 0.6000611796104235
Weighted FMeasure: 0.7333732701038516
Iteration time: 199.0
Weighted AreaUnderPRC: 0.6308640317402602
Mean absolute error: 0.18418180666049128
Coverage of cases: 75.6
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 4498.0
Weighted Recall: 0.7332
Weighted FalsePositiveRate: 0.13373854510447883
Kappa statistic: 0.5996936786410471
Training time: 194.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 37.1733333333344
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7292777506757203
Weighted AreaUnderROC: 0.8192744713303295
Root mean squared error: 0.41270833704004717
Relative absolute error: 41.564168755105364
Root relative squared error: 87.54865913197196
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5915610123241335
Weighted FMeasure: 0.7274846707270439
Iteration time: 191.0
Weighted AreaUnderPRC: 0.6556337500298697
Mean absolute error: 0.18472963891158026
Coverage of cases: 77.8
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 4689.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13685445962310075
Kappa statistic: 0.5906307503774361
Training time: 186.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 35.88000000000109
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7134554354004041
Weighted AreaUnderROC: 0.7908291762655222
Root mean squared error: 0.4274594227431248
Relative absolute error: 43.59616642389163
Root relative squared error: 90.67783695112499
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5679542437766087
Weighted FMeasure: 0.7119419362503626
Iteration time: 211.0
Weighted AreaUnderPRC: 0.6216757386435809
Mean absolute error: 0.1937607396617415
Coverage of cases: 74.6
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 4900.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.1446203003753567
Kappa statistic: 0.5672809157080028
Training time: 206.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 36.73333333333435
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7209899392844225
Weighted AreaUnderROC: 0.7946608054057124
Root mean squared error: 0.42281406181551046
Relative absolute error: 42.57476479584801
Root relative squared error: 89.69240708723245
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5798165655708324
Weighted FMeasure: 0.7196622227260651
Iteration time: 213.0
Weighted AreaUnderPRC: 0.6307392118199652
Mean absolute error: 0.1892211768704365
Coverage of cases: 76.12
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5113.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14057467808559712
Kappa statistic: 0.5793017922103736
Training time: 208.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 37.426666666667636
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.7298922183586212
Weighted AreaUnderROC: 0.8084238297882864
Root mean squared error: 0.41369813421666674
Relative absolute error: 41.31921179901175
Root relative squared error: 87.75862682064805
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.5948025373036254
Weighted FMeasure: 0.7298666240335577
Iteration time: 221.0
Weighted AreaUnderPRC: 0.6399584778541803
Mean absolute error: 0.18364094132894196
Coverage of cases: 77.0
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5334.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.13516717849848622
Kappa statistic: 0.5949872988016904
Training time: 217.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 38.18666666666763
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7337458504524652
Weighted AreaUnderROC: 0.8194938280559251
Root mean squared error: 0.4114344804009123
Relative absolute error: 41.10765696600094
Root relative squared error: 87.27843333163443
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.5994592561254304
Weighted FMeasure: 0.732808528256722
Iteration time: 239.0
Weighted AreaUnderPRC: 0.6589070295885658
Mean absolute error: 0.1827006976266717
Coverage of cases: 78.08
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5573.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13390908915889588
Kappa statistic: 0.5991170011839251
Training time: 235.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 37.60000000000101
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.736776493020542
Weighted AreaUnderROC: 0.8302223054693298
Root mean squared error: 0.40792770584287946
Relative absolute error: 40.58299406984669
Root relative squared error: 86.53453411061118
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.6022828512115701
Weighted FMeasure: 0.7338107280164707
Iteration time: 221.0
Weighted AreaUnderPRC: 0.6763964691191939
Mean absolute error: 0.1803688625326528
Coverage of cases: 78.92
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5794.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.13328276806975972
Kappa statistic: 0.6009231218301894
Training time: 217.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 36.7066666666676
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7278769080697236
Weighted AreaUnderROC: 0.8056188059851155
Root mean squared error: 0.41640972717383085
Relative absolute error: 41.017025746183336
Root relative squared error: 88.33384255099658
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.5919348229002895
Weighted FMeasure: 0.7274979172297119
Iteration time: 230.0
Weighted AreaUnderPRC: 0.6403292836895272
Mean absolute error: 0.1822978922052601
Coverage of cases: 77.68
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6024.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.13606212084334987
Kappa statistic: 0.5920494762395216
Training time: 226.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 39.50666666666754
Incorrectly Classified Instances: 26.88
Correctly Classified Instances: 73.12
Weighted Precision: 0.7316215542406208
Weighted AreaUnderROC: 0.8248060858679348
Root mean squared error: 0.4093895341410925
Relative absolute error: 41.199541894282554
Root relative squared error: 86.84463472139024
Weighted TruePositiveRate: 0.7312
Weighted MatthewsCorrelation: 0.5970049941563258
Weighted FMeasure: 0.7308334609830113
Iteration time: 232.0
Weighted AreaUnderPRC: 0.6677848159043327
Mean absolute error: 0.1831090750857011
Coverage of cases: 79.72
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6256.0
Weighted Recall: 0.7312
Weighted FalsePositiveRate: 0.13446648016859328
Kappa statistic: 0.5968312858922148
Training time: 228.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 37.626666666667674
Incorrectly Classified Instances: 26.16
Correctly Classified Instances: 73.84
Weighted Precision: 0.7397574753291789
Weighted AreaUnderROC: 0.8121229022034079
Root mean squared error: 0.40819108237597684
Relative absolute error: 40.391965775522145
Root relative squared error: 86.59040471037875
Weighted TruePositiveRate: 0.7384
Weighted MatthewsCorrelation: 0.6081490814348217
Weighted FMeasure: 0.738425070660098
Iteration time: 231.0
Weighted AreaUnderPRC: 0.6513892817835812
Mean absolute error: 0.1795198478912104
Coverage of cases: 77.64
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6487.0
Weighted Recall: 0.7384
Weighted FalsePositiveRate: 0.13096364093128599
Kappa statistic: 0.6075765801703045
Training time: 227.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 37.62666666666786
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7419687855673817
Weighted AreaUnderROC: 0.81760578594485
Root mean squared error: 0.4081089877855343
Relative absolute error: 39.991714541753176
Root relative squared error: 86.57298981789856
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6111981061377704
Weighted FMeasure: 0.740462181725913
Iteration time: 233.0
Weighted AreaUnderPRC: 0.656041696572244
Mean absolute error: 0.17774095351890384
Coverage of cases: 78.04
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6720.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.1300490944320151
Kappa statistic: 0.61054628654384
Training time: 229.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 38.2000000000011
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.7292412276683972
Weighted AreaUnderROC: 0.8025610121715212
Root mean squared error: 0.4159802255366653
Relative absolute error: 41.783426389599434
Root relative squared error: 88.24273149494545
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5937947913930739
Weighted FMeasure: 0.7287192279156164
Iteration time: 249.0
Weighted AreaUnderPRC: 0.6368543373848915
Mean absolute error: 0.18570411728710948
Coverage of cases: 77.76
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6969.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13550032869174283
Kappa statistic: 0.5938383416605472
Training time: 245.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 36.97333333333425
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.729924944015868
Weighted AreaUnderROC: 0.8132522830806924
Root mean squared error: 0.4161818726009641
Relative absolute error: 41.31701126963411
Root relative squared error: 88.28550729691706
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.5938867160442767
Weighted FMeasure: 0.729407014668241
Iteration time: 258.0
Weighted AreaUnderPRC: 0.6462601365297054
Mean absolute error: 0.18363116119837467
Coverage of cases: 76.72
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 7227.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13570511516272968
Kappa statistic: 0.5937215395037089
Training time: 255.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 36.013333333334316
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7453328790733096
Weighted AreaUnderROC: 0.8199173855947032
Root mean squared error: 0.4036168756773226
Relative absolute error: 38.87066820169542
Root relative squared error: 85.62006893782853
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.617683295425008
Weighted FMeasure: 0.7452550691283812
Iteration time: 259.0
Weighted AreaUnderPRC: 0.6598732178176586
Mean absolute error: 0.17275852534086936
Coverage of cases: 77.56
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 7486.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.1275913304282446
Kappa statistic: 0.6177630606221785
Training time: 256.0
		
Time end:Wed Nov 01 17.26.40 EET 2017