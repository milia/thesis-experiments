Wed Nov 01 17.04.19 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 17.04.19 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 37.706666666667815
Incorrectly Classified Instances: 28.12
Correctly Classified Instances: 71.88
Weighted Precision: 0.7212801635974433
Weighted AreaUnderROC: 0.8199852211951715
Root mean squared error: 0.41854648814998946
Relative absolute error: 43.09251557045095
Root relative squared error: 88.78711800380154
Weighted TruePositiveRate: 0.7188
Weighted MatthewsCorrelation: 0.5792185521044902
Weighted FMeasure: 0.7189180404484453
Iteration time: 76.0
Weighted AreaUnderPRC: 0.6554336938680987
Mean absolute error: 0.19152229142422733
Coverage of cases: 78.28
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 76.0
Weighted Recall: 0.7188
Weighted FalsePositiveRate: 0.14101633415000117
Kappa statistic: 0.5780304019791634
Training time: 73.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 38.906666666667405
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.728235049607787
Weighted AreaUnderROC: 0.8156152491398486
Root mean squared error: 0.4132783775885748
Relative absolute error: 41.563463745036394
Root relative squared error: 87.6695829931965
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5913463694727691
Weighted FMeasure: 0.7265801579697924
Iteration time: 78.0
Weighted AreaUnderPRC: 0.6545987697493356
Mean absolute error: 0.18472650553349595
Coverage of cases: 79.72
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 154.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13661792148980362
Kappa statistic: 0.5907402153306553
Training time: 75.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 39.89333333333394
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7352818355994108
Weighted AreaUnderROC: 0.8392998485151589
Root mean squared error: 0.40654390288502834
Relative absolute error: 40.67461525471413
Root relative squared error: 86.24098517401443
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.6018730951163094
Weighted FMeasure: 0.7333481767681216
Iteration time: 79.0
Weighted AreaUnderPRC: 0.683728564897602
Mean absolute error: 0.18077606779873034
Coverage of cases: 81.36
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 233.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.13290128154160608
Kappa statistic: 0.6010744235555414
Training time: 77.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 39.49333333333404
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7273289839194885
Weighted AreaUnderROC: 0.8251572573428717
Root mean squared error: 0.4175934116335621
Relative absolute error: 41.556253132831664
Root relative squared error: 88.58493994347492
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5907716540650517
Weighted FMeasure: 0.7255781896755041
Iteration time: 84.0
Weighted AreaUnderPRC: 0.6656010642193182
Mean absolute error: 0.18469445836814158
Coverage of cases: 79.76
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 317.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13656882885498722
Kappa statistic: 0.5902907915605901
Training time: 82.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 39.720000000000695
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7266510433487677
Weighted AreaUnderROC: 0.8264754659194412
Root mean squared error: 0.417670270632974
Relative absolute error: 41.58958646616501
Root relative squared error: 88.60124419937873
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.5898811183507372
Weighted FMeasure: 0.7255348377555987
Iteration time: 87.0
Weighted AreaUnderPRC: 0.6670618527797587
Mean absolute error: 0.1848426065162898
Coverage of cases: 80.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 404.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.1368320029493966
Kappa statistic: 0.5896572510203377
Training time: 85.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 36.38666666666767
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.712262772261533
Weighted AreaUnderROC: 0.8100118393135569
Root mean squared error: 0.4303202367924506
Relative absolute error: 43.973582365003274
Root relative squared error: 91.2847072553226
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5676194844128166
Weighted FMeasure: 0.7112350415728893
Iteration time: 97.0
Weighted AreaUnderPRC: 0.6423127426261819
Mean absolute error: 0.1954381438444599
Coverage of cases: 75.16
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 501.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.1444323375850285
Kappa statistic: 0.5673835259646688
Training time: 95.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 37.973333333334296
Incorrectly Classified Instances: 28.36
Correctly Classified Instances: 71.64
Weighted Precision: 0.7165431216524379
Weighted AreaUnderROC: 0.8018718906387176
Root mean squared error: 0.42816491069869367
Relative absolute error: 43.312270676691455
Root relative squared error: 90.82749354635344
Weighted TruePositiveRate: 0.7164
Weighted MatthewsCorrelation: 0.5744668655091806
Weighted FMeasure: 0.7162779243376555
Iteration time: 105.0
Weighted AreaUnderPRC: 0.6343802651148617
Mean absolute error: 0.19249898078529626
Coverage of cases: 75.72
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 606.0
Weighted Recall: 0.7164
Weighted FalsePositiveRate: 0.14205323817774532
Kappa statistic: 0.5745684840332972
Training time: 103.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 37.666666666667744
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7414697352124493
Weighted AreaUnderROC: 0.8227000595347989
Root mean squared error: 0.4083587116783801
Relative absolute error: 39.466211089220145
Root relative squared error: 86.62596425531524
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.6121891958244482
Weighted FMeasure: 0.7413522239718962
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6620584659399686
Mean absolute error: 0.17540538261875704
Coverage of cases: 78.32
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 709.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.12941895256757863
Kappa statistic: 0.6123768666514019
Training time: 101.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 37.080000000000965
Incorrectly Classified Instances: 28.32
Correctly Classified Instances: 71.68
Weighted Precision: 0.7167440260221752
Weighted AreaUnderROC: 0.8094162114847965
Root mean squared error: 0.42716214847916284
Relative absolute error: 42.90186043986962
Root relative squared error: 90.61477555674907
Weighted TruePositiveRate: 0.7168
Weighted MatthewsCorrelation: 0.5749707479518971
Weighted FMeasure: 0.7166835617771087
Iteration time: 102.0
Weighted AreaUnderPRC: 0.6429625893725214
Mean absolute error: 0.19067493528831034
Coverage of cases: 76.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 811.0
Weighted Recall: 0.7168
Weighted FalsePositiveRate: 0.14181467015276208
Kappa statistic: 0.575192387447583
Training time: 100.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 36.16000000000105
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7328101087437169
Weighted AreaUnderROC: 0.8087149690542085
Root mean squared error: 0.4163030338003574
Relative absolute error: 40.903103417480146
Root relative squared error: 88.31120946862936
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5985147889901308
Weighted FMeasure: 0.7298601282150967
Iteration time: 131.0
Weighted AreaUnderPRC: 0.6420824577051865
Mean absolute error: 0.1817915707443571
Coverage of cases: 76.76
Instances selection time: 24.0
Test time: 6.0
Accumulative iteration time: 942.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13421188078564486
Kappa statistic: 0.5974739359874379
Training time: 107.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 37.13333333333432
Incorrectly Classified Instances: 25.88
Correctly Classified Instances: 74.12
Weighted Precision: 0.7417731198511824
Weighted AreaUnderROC: 0.8221270127566062
Root mean squared error: 0.4072095921499551
Relative absolute error: 39.91284864121069
Root relative squared error: 86.38219919203226
Weighted TruePositiveRate: 0.7412
Weighted MatthewsCorrelation: 0.6118447063368893
Weighted FMeasure: 0.7413657573140531
Iteration time: 115.0
Weighted AreaUnderPRC: 0.6653158521902378
Mean absolute error: 0.17739043840538166
Coverage of cases: 77.96
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1057.0
Weighted Recall: 0.7412
Weighted FalsePositiveRate: 0.1296158428636188
Kappa statistic: 0.6117714263769813
Training time: 112.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 36.46666666666776
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.7211439302052289
Weighted AreaUnderROC: 0.7958741438186897
Root mean squared error: 0.42247936980942297
Relative absolute error: 42.60814572719765
Root relative squared error: 89.62140819109844
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.5811525206953164
Weighted FMeasure: 0.720821871356156
Iteration time: 116.0
Weighted AreaUnderPRC: 0.6283510254736633
Mean absolute error: 0.1893695365653238
Coverage of cases: 75.52
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1173.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.13985619353689885
Kappa statistic: 0.5811472580627353
Training time: 113.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 35.77333333333427
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.722012634321068
Weighted AreaUnderROC: 0.8030500500670179
Root mean squared error: 0.42332409691165673
Relative absolute error: 42.779668038612684
Root relative squared error: 89.8006018697709
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.5815149130839369
Weighted FMeasure: 0.7210097599456274
Iteration time: 126.0
Weighted AreaUnderPRC: 0.6299139860708216
Mean absolute error: 0.19013185794939058
Coverage of cases: 75.16
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1299.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.13997763923864276
Kappa statistic: 0.5810806917810192
Training time: 123.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 35.573333333334325
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7255872455879966
Weighted AreaUnderROC: 0.8004453594326882
Root mean squared error: 0.42266883629714724
Relative absolute error: 42.08613174213183
Root relative squared error: 89.66160010258167
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5877429236527252
Weighted FMeasure: 0.7253285030270353
Iteration time: 130.0
Weighted AreaUnderPRC: 0.6241168009319236
Mean absolute error: 0.1870494744094757
Coverage of cases: 75.0
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 1429.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.1376729588127473
Kappa statistic: 0.5877366103812122
Training time: 128.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 40.84000000000088
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7438877938171683
Weighted AreaUnderROC: 0.8314748497913428
Root mean squared error: 0.40235761378674806
Relative absolute error: 40.183293869575905
Root relative squared error: 85.35293915119405
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.6144974937985426
Weighted FMeasure: 0.7430701417286575
Iteration time: 137.0
Weighted AreaUnderPRC: 0.6759692338684962
Mean absolute error: 0.17859241719811597
Coverage of cases: 82.32
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1566.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.12890581352234554
Kappa statistic: 0.6141021363017661
Training time: 134.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 39.98666666666762
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7221691568340891
Weighted AreaUnderROC: 0.8135810964798745
Root mean squared error: 0.42022457194435026
Relative absolute error: 42.558404512802795
Root relative squared error: 89.14309333291908
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.5829403140749774
Weighted FMeasure: 0.7220313600948954
Iteration time: 146.0
Weighted AreaUnderPRC: 0.6467310177399782
Mean absolute error: 0.18914846450134665
Coverage of cases: 79.12
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1712.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.13910254005625824
Kappa statistic: 0.5830049372215432
Training time: 143.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 40.68000000000098
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.720234342197424
Weighted AreaUnderROC: 0.8166832386168147
Root mean squared error: 0.4208705567603076
Relative absolute error: 43.32603180831421
Root relative squared error: 89.28012740609117
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5799485051170788
Weighted FMeasure: 0.7200307192397546
Iteration time: 148.0
Weighted AreaUnderPRC: 0.6558048526710695
Mean absolute error: 0.1925601413702863
Coverage of cases: 80.4
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1860.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.14012551466327156
Kappa statistic: 0.5800079966477437
Training time: 145.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 37.98666666666791
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.7317426843890582
Weighted AreaUnderROC: 0.8123848375789631
Root mean squared error: 0.41444381964039817
Relative absolute error: 41.71854467645325
Root relative squared error: 87.9168105865738
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.5966100440325233
Weighted FMeasure: 0.7304690855365665
Iteration time: 142.0
Weighted AreaUnderPRC: 0.647250970393019
Mean absolute error: 0.18541575411757089
Coverage of cases: 78.16
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 2002.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.1349140442568888
Kappa statistic: 0.5960694496461256
Training time: 140.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 37.56000000000129
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7324745119990262
Weighted AreaUnderROC: 0.8098103962335111
Root mean squared error: 0.41494914098727337
Relative absolute error: 41.53751870242732
Root relative squared error: 88.02400543188992
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5976231911058021
Weighted FMeasure: 0.7317252027750619
Iteration time: 148.0
Weighted AreaUnderPRC: 0.6450884249655835
Mean absolute error: 0.18461119423301117
Coverage of cases: 77.76
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2150.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.13452382177260733
Kappa statistic: 0.597286273643677
Training time: 146.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 36.12000000000118
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.740200326488799
Weighted AreaUnderROC: 0.8095420876805065
Root mean squared error: 0.4097839559076933
Relative absolute error: 40.53878580899067
Root relative squared error: 86.92830421313353
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.6095864362216813
Weighted FMeasure: 0.7394018700499116
Iteration time: 154.0
Weighted AreaUnderPRC: 0.6475937636637596
Mean absolute error: 0.1801723813732927
Coverage of cases: 76.32
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2304.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.13047947307461455
Kappa statistic: 0.6093069212809261
Training time: 152.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 37.986666666667574
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7190150898617147
Weighted AreaUnderROC: 0.803676673953547
Root mean squared error: 0.4172844338744461
Relative absolute error: 43.454899316472876
Root relative squared error: 88.51939586286288
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5745419463730326
Weighted FMeasure: 0.7146184149572528
Iteration time: 158.0
Weighted AreaUnderPRC: 0.6420338673666586
Mean absolute error: 0.19313288585099148
Coverage of cases: 77.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2462.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.1430218374862206
Kappa statistic: 0.572556596628384
Training time: 156.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 35.93333333333442
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.715714825844032
Weighted AreaUnderROC: 0.7815081381712069
Root mean squared error: 0.4304034885447009
Relative absolute error: 44.18789931647321
Root relative squared error: 91.30236761889114
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.569303273820126
Weighted FMeasure: 0.7108878873619578
Iteration time: 174.0
Weighted AreaUnderPRC: 0.6138322289712135
Mean absolute error: 0.19639066362877075
Coverage of cases: 73.76
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 2636.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14485596842255535
Kappa statistic: 0.5671389674829509
Training time: 172.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 36.00000000000115
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7221769982313841
Weighted AreaUnderROC: 0.7966985767192084
Root mean squared error: 0.42558900971090385
Relative absolute error: 43.261212507765634
Root relative squared error: 90.28106242951405
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.580060339400854
Weighted FMeasure: 0.7190412291833556
Iteration time: 173.0
Weighted AreaUnderPRC: 0.6286988531813034
Mean absolute error: 0.19227205559007038
Coverage of cases: 75.16
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2809.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.1409571542038852
Kappa statistic: 0.578587338314407
Training time: 171.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 36.880000000001125
Incorrectly Classified Instances: 26.32
Correctly Classified Instances: 73.68
Weighted Precision: 0.7399162129714004
Weighted AreaUnderROC: 0.8155045918430087
Root mean squared error: 0.4095978929437454
Relative absolute error: 40.67162687780026
Root relative squared error: 86.88883429807296
Weighted TruePositiveRate: 0.7368
Weighted MatthewsCorrelation: 0.6067304456645225
Weighted FMeasure: 0.7360815884656756
Iteration time: 183.0
Weighted AreaUnderPRC: 0.6555754354938897
Mean absolute error: 0.18076278612355753
Coverage of cases: 77.68
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 2992.0
Weighted Recall: 0.7368
Weighted FalsePositiveRate: 0.13211317694124122
Kappa statistic: 0.6049949765926507
Training time: 181.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 37.30666666666759
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7193085868631129
Weighted AreaUnderROC: 0.7996416322205888
Root mean squared error: 0.4232562715648937
Relative absolute error: 43.084738038228664
Root relative squared error: 89.78621394098116
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5767302182049675
Weighted FMeasure: 0.7169268470438588
Iteration time: 177.0
Weighted AreaUnderPRC: 0.6349568102838842
Mean absolute error: 0.1914877246143505
Coverage of cases: 76.84
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3169.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14183407703972842
Kappa statistic: 0.5756149681261029
Training time: 175.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 37.0666666666675
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7264709492423659
Weighted AreaUnderROC: 0.804771211999037
Root mean squared error: 0.4181909995914232
Relative absolute error: 41.855120995671136
Root relative squared error: 88.71170749268262
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5887012983311679
Weighted FMeasure: 0.7252602071508618
Iteration time: 184.0
Weighted AreaUnderPRC: 0.6402832782725179
Mean absolute error: 0.18602275998076148
Coverage of cases: 76.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3353.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.1375354400124073
Kappa statistic: 0.5883025100343762
Training time: 182.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 36.666666666667474
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7223609241620418
Weighted AreaUnderROC: 0.8079177778796306
Root mean squared error: 0.4231734033136284
Relative absolute error: 42.46283658149501
Root relative squared error: 89.76863493025674
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5807721120905449
Weighted FMeasure: 0.7204250455725554
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6382545443764185
Mean absolute error: 0.18872371813997874
Coverage of cases: 75.92
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 3534.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.1405060533419217
Kappa statistic: 0.5798406083331773
Training time: 179.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 36.78666666666755
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7244718739015903
Weighted AreaUnderROC: 0.8053245990340845
Root mean squared error: 0.4233694054764997
Relative absolute error: 42.28085389751233
Root relative squared error: 89.8102132678048
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5848130968897951
Weighted FMeasure: 0.7227546129299851
Iteration time: 194.0
Weighted AreaUnderPRC: 0.6362483140266378
Mean absolute error: 0.1879149062111668
Coverage of cases: 75.68
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 3728.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13902618363523264
Kappa statistic: 0.5840489931464719
Training time: 192.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 36.85333333333435
Incorrectly Classified Instances: 27.04
Correctly Classified Instances: 72.96
Weighted Precision: 0.730168254844849
Weighted AreaUnderROC: 0.8106718790666588
Root mean squared error: 0.41294382239451005
Relative absolute error: 41.31961438568947
Root relative squared error: 87.5986131192752
Weighted TruePositiveRate: 0.7296
Weighted MatthewsCorrelation: 0.5944880620616031
Weighted FMeasure: 0.7296507049867966
Iteration time: 197.0
Weighted AreaUnderPRC: 0.6414187893155798
Mean absolute error: 0.18364273060306516
Coverage of cases: 77.6
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3925.0
Weighted Recall: 0.7296
Weighted FalsePositiveRate: 0.13543019706608556
Kappa statistic: 0.594349309889764
Training time: 195.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 36.14666666666761
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32
Weighted Precision: 0.7239684967362713
Weighted AreaUnderROC: 0.7944220280530482
Root mean squared error: 0.42379936334181506
Relative absolute error: 42.56016902099903
Root relative squared error: 89.90142110446149
Weighted TruePositiveRate: 0.7232
Weighted MatthewsCorrelation: 0.584885847655985
Weighted FMeasure: 0.723427837211113
Iteration time: 202.0
Weighted AreaUnderPRC: 0.6217280120076588
Mean absolute error: 0.18915630675999656
Coverage of cases: 75.08
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 4127.0
Weighted Recall: 0.7232
Weighted FalsePositiveRate: 0.13872403452222382
Kappa statistic: 0.5847176079734219
Training time: 200.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 36.37333333333437
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7303272364066257
Weighted AreaUnderROC: 0.810763806083823
Root mean squared error: 0.41760484036218093
Relative absolute error: 41.227266213266795
Root relative squared error: 88.58736434292693
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5937046982137711
Weighted FMeasure: 0.7290278453961506
Iteration time: 206.0
Weighted AreaUnderPRC: 0.6463400704400972
Mean absolute error: 0.18323229428118662
Coverage of cases: 75.88
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4333.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13598472412077253
Kappa statistic: 0.5930624225438136
Training time: 204.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 36.18666666666749
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7261174596363904
Weighted AreaUnderROC: 0.8037572713364829
Root mean squared error: 0.42193304434495404
Relative absolute error: 41.8925289914107
Root relative squared error: 89.50551505890017
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5870782311930492
Weighted FMeasure: 0.7248218605119785
Iteration time: 204.0
Weighted AreaUnderPRC: 0.6394669054583279
Mean absolute error: 0.186189017739604
Coverage of cases: 75.6
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 4537.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.1382432839351844
Kappa statistic: 0.5864657502411583
Training time: 202.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 38.84000000000062
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7264097302976609
Weighted AreaUnderROC: 0.8195416991429112
Root mean squared error: 0.4143623797072216
Relative absolute error: 42.20604392057221
Root relative squared error: 87.89953456787123
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5886320975241045
Weighted FMeasure: 0.7255934171746754
Iteration time: 205.0
Weighted AreaUnderPRC: 0.6581483734501243
Mean absolute error: 0.18758241742476628
Coverage of cases: 77.92
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 4742.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13746820836453463
Kappa statistic: 0.5883153576375303
Training time: 203.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 37.213333333334035
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7188121851825593
Weighted AreaUnderROC: 0.8098160041998735
Root mean squared error: 0.42387427380421344
Relative absolute error: 43.05740138160247
Root relative squared error: 89.9173120132446
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5776307651301192
Weighted FMeasure: 0.7184949682865115
Iteration time: 213.0
Weighted AreaUnderPRC: 0.6425395008550034
Mean absolute error: 0.19136622836267853
Coverage of cases: 75.4
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 4955.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14103034133413703
Kappa statistic: 0.5775184103554079
Training time: 211.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 37.146666666667336
Incorrectly Classified Instances: 27.96
Correctly Classified Instances: 72.04
Weighted Precision: 0.7209348110778571
Weighted AreaUnderROC: 0.8110589501151628
Root mean squared error: 0.42478178546009826
Relative absolute error: 42.774066068966505
Root relative squared error: 90.10982430700919
Weighted TruePositiveRate: 0.7204
Weighted MatthewsCorrelation: 0.5806470611486644
Weighted FMeasure: 0.7205160617083711
Iteration time: 216.0
Weighted AreaUnderPRC: 0.6457700637472031
Mean absolute error: 0.1901069603065187
Coverage of cases: 75.2
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 5171.0
Weighted Recall: 0.7204
Weighted FalsePositiveRate: 0.14007094781514934
Kappa statistic: 0.5805185870473742
Training time: 214.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 37.853333333334
Incorrectly Classified Instances: 26.36
Correctly Classified Instances: 73.64
Weighted Precision: 0.7369509252327292
Weighted AreaUnderROC: 0.8088652091070748
Root mean squared error: 0.4120086217929256
Relative absolute error: 40.55926061193685
Root relative squared error: 87.40022711313017
Weighted TruePositiveRate: 0.7364
Weighted MatthewsCorrelation: 0.604739898088583
Weighted FMeasure: 0.7362610548646255
Iteration time: 227.0
Weighted AreaUnderPRC: 0.6412968358930078
Mean absolute error: 0.18026338049749796
Coverage of cases: 77.68
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5398.0
Weighted Recall: 0.7364
Weighted FalsePositiveRate: 0.13204535023811603
Kappa statistic: 0.6045342577750126
Training time: 225.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 38.80000000000057
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7255009616669292
Weighted AreaUnderROC: 0.8136325119276449
Root mean squared error: 0.4189235759272097
Relative absolute error: 42.08459608160731
Root relative squared error: 88.86711040111405
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5864502862930423
Weighted FMeasure: 0.7243082560794044
Iteration time: 228.0
Weighted AreaUnderPRC: 0.6528527739635781
Mean absolute error: 0.18704264925158892
Coverage of cases: 78.32
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 5626.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.1383965230398695
Kappa statistic: 0.5858641634456102
Training time: 226.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 37.05333333333421
Incorrectly Classified Instances: 26.32
Correctly Classified Instances: 73.68
Weighted Precision: 0.7389484687751914
Weighted AreaUnderROC: 0.8193257224617784
Root mean squared error: 0.40885043200123766
Relative absolute error: 40.27739620455925
Root relative squared error: 86.73027388773717
Weighted TruePositiveRate: 0.7368
Weighted MatthewsCorrelation: 0.6059124972281056
Weighted FMeasure: 0.7372554984468228
Iteration time: 227.0
Weighted AreaUnderPRC: 0.6594249740167284
Mean absolute error: 0.17901064979804196
Coverage of cases: 78.16
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 5853.0
Weighted Recall: 0.7368
Weighted FalsePositiveRate: 0.13206784865245108
Kappa statistic: 0.6050499821852333
Training time: 225.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 36.33333333333423
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7358974324372123
Weighted AreaUnderROC: 0.8155704480966217
Root mean squared error: 0.41103140511176417
Relative absolute error: 40.29057563489167
Root relative squared error: 87.19292815054882
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6037864818747919
Weighted FMeasure: 0.735937830359408
Iteration time: 232.0
Weighted AreaUnderPRC: 0.6535277365988964
Mean absolute error: 0.1790692250439638
Coverage of cases: 77.08
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6085.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13216770776658932
Kappa statistic: 0.6039847296511753
Training time: 230.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 35.85333333333429
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7413443679740753
Weighted AreaUnderROC: 0.8057572676451403
Root mean squared error: 0.4105552752940557
Relative absolute error: 39.853038995060984
Root relative squared error: 87.09192576370079
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6108570717264985
Weighted FMeasure: 0.7405291783688817
Iteration time: 235.0
Weighted AreaUnderPRC: 0.6383863300404098
Mean absolute error: 0.1771246177558274
Coverage of cases: 76.32
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 6320.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.13008973185940645
Kappa statistic: 0.6105126301932049
Training time: 234.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 36.0933333333344
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7462263090459659
Weighted AreaUnderROC: 0.8086700358952766
Root mean squared error: 0.4066868929095255
Relative absolute error: 39.278819459434345
Root relative squared error: 86.27131793880362
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6181187417968543
Weighted FMeasure: 0.7451775104393339
Iteration time: 239.0
Weighted AreaUnderPRC: 0.6401101856768255
Mean absolute error: 0.17457253093082012
Coverage of cases: 77.08
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 6559.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.12769309421217623
Kappa statistic: 0.6177285611135009
Training time: 237.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 37.85333333333431
Incorrectly Classified Instances: 26.64
Correctly Classified Instances: 73.36
Weighted Precision: 0.734491335104149
Weighted AreaUnderROC: 0.8016468339051295
Root mean squared error: 0.4116850493151843
Relative absolute error: 40.6424464030503
Root relative squared error: 87.3315870251653
Weighted TruePositiveRate: 0.7336
Weighted MatthewsCorrelation: 0.6007322069479016
Weighted FMeasure: 0.7333502905454153
Iteration time: 265.0
Weighted AreaUnderPRC: 0.6359564939998014
Mean absolute error: 0.18063309512466882
Coverage of cases: 76.76
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 6824.0
Weighted Recall: 0.7336
Weighted FalsePositiveRate: 0.13343838414252512
Kappa statistic: 0.6003558153389439
Training time: 263.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 36.89333333333418
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.737793541480702
Weighted AreaUnderROC: 0.8056466680922945
Root mean squared error: 0.41082433991761336
Relative absolute error: 40.60759759526953
Root relative squared error: 87.14900298966928
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6063094730938762
Weighted FMeasure: 0.7374232821449643
Iteration time: 247.0
Weighted AreaUnderPRC: 0.6352253669632296
Mean absolute error: 0.1804782115345321
Coverage of cases: 77.28
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 7071.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.1314633948996234
Kappa statistic: 0.606364415343147
Training time: 245.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 38.78666666666743
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7340623703686114
Weighted AreaUnderROC: 0.8085224590627688
Root mean squared error: 0.4118835868278244
Relative absolute error: 41.107669149477736
Root relative squared error: 87.37370319161762
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6013040874190476
Weighted FMeasure: 0.7341891355109327
Iteration time: 252.0
Weighted AreaUnderPRC: 0.6464836480751479
Mean absolute error: 0.18270075177545744
Coverage of cases: 78.88
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7323.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13295290667131285
Kappa statistic: 0.6015984063936257
Training time: 250.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 35.733333333334315
Incorrectly Classified Instances: 25.96
Correctly Classified Instances: 74.04
Weighted Precision: 0.7404705018658798
Weighted AreaUnderROC: 0.8165582804132625
Root mean squared error: 0.40807288189004604
Relative absolute error: 39.5294046093818
Root relative squared error: 86.56533060083639
Weighted TruePositiveRate: 0.7404
Weighted MatthewsCorrelation: 0.6106329750810379
Weighted FMeasure: 0.7399657079874492
Iteration time: 270.0
Weighted AreaUnderPRC: 0.6494643725456053
Mean absolute error: 0.17568624270836436
Coverage of cases: 77.0
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7593.0
Weighted Recall: 0.7404
Weighted FalsePositiveRate: 0.12991121386663768
Kappa statistic: 0.6106001246079601
Training time: 268.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 35.76000000000096
Incorrectly Classified Instances: 27.0
Correctly Classified Instances: 73.0
Weighted Precision: 0.730208162791531
Weighted AreaUnderROC: 0.8042835342387192
Root mean squared error: 0.4171684721714514
Relative absolute error: 41.099148199125516
Root relative squared error: 88.49479667089925
Weighted TruePositiveRate: 0.73
Weighted MatthewsCorrelation: 0.5950673010895672
Weighted FMeasure: 0.7295589239131789
Iteration time: 269.0
Weighted AreaUnderPRC: 0.6332869953277046
Mean absolute error: 0.18266288088500313
Coverage of cases: 75.84
Instances selection time: 2.0
Test time: 9.0
Accumulative iteration time: 7862.0
Weighted Recall: 0.73
Weighted FalsePositiveRate: 0.13517718232852405
Kappa statistic: 0.5949768002711195
Training time: 267.0
		
Time end:Wed Nov 01 17.04.31 EET 2017