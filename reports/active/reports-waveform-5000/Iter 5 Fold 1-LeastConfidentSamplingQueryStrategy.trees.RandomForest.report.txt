Wed Nov 01 16.38.59 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.38.59 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.7733333333346
Incorrectly Classified Instances: 21.8
Correctly Classified Instances: 78.2
Weighted Precision: 0.7839145032854887
Weighted AreaUnderROC: 0.9297344961696943
Root mean squared error: 0.3209818271075986
Relative absolute error: 51.41399999999915
Root relative squared error: 68.09052797562912
Weighted TruePositiveRate: 0.782
Weighted MatthewsCorrelation: 0.6737174242175775
Weighted FMeasure: 0.7822388140673913
Iteration time: 93.0
Weighted AreaUnderPRC: 0.8399194809798358
Mean absolute error: 0.22850666666666394
Coverage of cases: 99.72
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 93.0
Weighted Recall: 0.782
Weighted FalsePositiveRate: 0.10936881591186863
Kappa statistic: 0.6728990697369581
Training time: 86.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 72.49333333333469
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8086180663592181
Weighted AreaUnderROC: 0.9380043754892231
Root mean squared error: 0.31307507086959
Relative absolute error: 50.21999999999903
Root relative squared error: 66.41325168970366
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7114129288504326
Weighted FMeasure: 0.8072736715028346
Iteration time: 100.0
Weighted AreaUnderPRC: 0.8569405393561821
Mean absolute error: 0.22319999999999673
Coverage of cases: 99.72
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 193.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09672652231074516
Kappa statistic: 0.7106949012437238
Training time: 92.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 71.3600000000014
Incorrectly Classified Instances: 21.04
Correctly Classified Instances: 78.96
Weighted Precision: 0.7909945085302493
Weighted AreaUnderROC: 0.9305513516111571
Root mean squared error: 0.31966649287447346
Relative absolute error: 51.01199999999907
Root relative squared error: 67.81150344889825
Weighted TruePositiveRate: 0.7896
Weighted MatthewsCorrelation: 0.6849656722052229
Weighted FMeasure: 0.7896341536834952
Iteration time: 103.0
Weighted AreaUnderPRC: 0.839716554635039
Mean absolute error: 0.22671999999999692
Coverage of cases: 99.72
Instances selection time: 7.0
Test time: 13.0
Accumulative iteration time: 296.0
Weighted Recall: 0.7896
Weighted FalsePositiveRate: 0.10553644272682791
Kappa statistic: 0.6842939480230198
Training time: 96.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 73.41333333333486
Incorrectly Classified Instances: 22.28
Correctly Classified Instances: 77.72
Weighted Precision: 0.778493997619425
Weighted AreaUnderROC: 0.9267516769287589
Root mean squared error: 0.32419747068723226
Relative absolute error: 52.48199999999912
Root relative squared error: 68.77266898994051
Weighted TruePositiveRate: 0.7772
Weighted MatthewsCorrelation: 0.6662771953058269
Weighted FMeasure: 0.7773511456820817
Iteration time: 108.0
Weighted AreaUnderPRC: 0.8305495660981049
Mean absolute error: 0.2332533333333305
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 404.0
Weighted Recall: 0.7772
Weighted FalsePositiveRate: 0.11166603029407238
Kappa statistic: 0.6657197192477746
Training time: 100.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 72.50666666666812
Incorrectly Classified Instances: 21.24
Correctly Classified Instances: 78.76
Weighted Precision: 0.7914350117241558
Weighted AreaUnderROC: 0.932100328425502
Root mean squared error: 0.3192031745873868
Relative absolute error: 51.25799999999911
Root relative squared error: 67.71321879810424
Weighted TruePositiveRate: 0.7876
Weighted MatthewsCorrelation: 0.6832522749895911
Weighted FMeasure: 0.7874542904884196
Iteration time: 112.0
Weighted AreaUnderPRC: 0.8424556887314527
Mean absolute error: 0.22781333333333045
Coverage of cases: 99.8
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 516.0
Weighted Recall: 0.7876
Weighted FalsePositiveRate: 0.10675853974967674
Kappa statistic: 0.681218779251629
Training time: 105.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 72.5200000000012
Incorrectly Classified Instances: 22.72
Correctly Classified Instances: 77.28
Weighted Precision: 0.7786139150889627
Weighted AreaUnderROC: 0.9251630557448806
Root mean squared error: 0.3259815945724531
Relative absolute error: 52.49399999999915
Root relative squared error: 69.15113881925548
Weighted TruePositiveRate: 0.7728
Weighted MatthewsCorrelation: 0.6619054755075353
Weighted FMeasure: 0.7733278033633596
Iteration time: 118.0
Weighted AreaUnderPRC: 0.8289654389133119
Mean absolute error: 0.23330666666666397
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 634.0
Weighted Recall: 0.7728
Weighted FalsePositiveRate: 0.11425988893956292
Kappa statistic: 0.6589719204203771
Training time: 110.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 70.8533333333347
Incorrectly Classified Instances: 21.16
Correctly Classified Instances: 78.84
Weighted Precision: 0.7950910223378782
Weighted AreaUnderROC: 0.9312602802458275
Root mean squared error: 0.319866638877307
Relative absolute error: 50.49599999999903
Root relative squared error: 67.85396082764754
Weighted TruePositiveRate: 0.7884
Weighted MatthewsCorrelation: 0.6859602641512824
Weighted FMeasure: 0.788406322917125
Iteration time: 123.0
Weighted AreaUnderPRC: 0.8416203267998927
Mean absolute error: 0.22442666666666342
Coverage of cases: 99.88
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 757.0
Weighted Recall: 0.7884
Weighted FalsePositiveRate: 0.10646607684029225
Kappa statistic: 0.6823701884787682
Training time: 115.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 71.7733333333348
Incorrectly Classified Instances: 21.44
Correctly Classified Instances: 78.56
Weighted Precision: 0.7960027421320051
Weighted AreaUnderROC: 0.9329389406109263
Root mean squared error: 0.32034564665893667
Relative absolute error: 51.47999999999906
Root relative squared error: 67.95557372283699
Weighted TruePositiveRate: 0.7856
Weighted MatthewsCorrelation: 0.6838568618751794
Weighted FMeasure: 0.7857215630546256
Iteration time: 128.0
Weighted AreaUnderPRC: 0.8435688671705822
Mean absolute error: 0.2287999999999969
Coverage of cases: 99.84
Instances selection time: 9.0
Test time: 13.0
Accumulative iteration time: 885.0
Weighted Recall: 0.7856
Weighted FalsePositiveRate: 0.1080630322549403
Kappa statistic: 0.6780907611087514
Training time: 119.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 69.78666666666797
Incorrectly Classified Instances: 20.88
Correctly Classified Instances: 79.12
Weighted Precision: 0.8010787775329636
Weighted AreaUnderROC: 0.9375211750607667
Root mean squared error: 0.3138109834491657
Relative absolute error: 49.07399999999892
Root relative squared error: 66.56936232231719
Weighted TruePositiveRate: 0.7912
Weighted MatthewsCorrelation: 0.6918269821363805
Weighted FMeasure: 0.7916283504665352
Iteration time: 127.0
Weighted AreaUnderPRC: 0.8560647420359001
Mean absolute error: 0.2181066666666629
Coverage of cases: 99.8
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 1012.0
Weighted Recall: 0.7912
Weighted FalsePositiveRate: 0.10524676738671164
Kappa statistic: 0.6865160085824951
Training time: 119.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 68.52000000000133
Incorrectly Classified Instances: 21.88
Correctly Classified Instances: 78.12
Weighted Precision: 0.7941683373553887
Weighted AreaUnderROC: 0.9308256773956666
Root mean squared error: 0.31931593550379817
Relative absolute error: 48.53399999999889
Root relative squared error: 67.73713900069842
Weighted TruePositiveRate: 0.7812
Weighted MatthewsCorrelation: 0.6786816934530705
Weighted FMeasure: 0.7812801888925582
Iteration time: 132.0
Weighted AreaUnderPRC: 0.8433413941312509
Mean absolute error: 0.21570666666666274
Coverage of cases: 99.4
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 1144.0
Weighted Recall: 0.7812
Weighted FalsePositiveRate: 0.1103753730021806
Kappa statistic: 0.6714454239014745
Training time: 125.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 72.00000000000153
Incorrectly Classified Instances: 23.2
Correctly Classified Instances: 76.8
Weighted Precision: 0.7855344812572417
Weighted AreaUnderROC: 0.9312418989701288
Root mean squared error: 0.32418101939091426
Relative absolute error: 51.70199999999918
Root relative squared error: 68.76917914298478
Weighted TruePositiveRate: 0.768
Weighted MatthewsCorrelation: 0.661595642826561
Weighted FMeasure: 0.7676239502789249
Iteration time: 142.0
Weighted AreaUnderPRC: 0.8412524170725928
Mean absolute error: 0.22978666666666409
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 13.0
Accumulative iteration time: 1286.0
Weighted Recall: 0.768
Weighted FalsePositiveRate: 0.11714760427907203
Kappa statistic: 0.651549272014336
Training time: 134.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 71.34666666666813
Incorrectly Classified Instances: 23.44
Correctly Classified Instances: 76.56
Weighted Precision: 0.7831168270729935
Weighted AreaUnderROC: 0.9281256690838418
Root mean squared error: 0.3254535297089262
Relative absolute error: 51.53999999999907
Root relative squared error: 69.03911934548361
Weighted TruePositiveRate: 0.7656
Weighted MatthewsCorrelation: 0.657739279915147
Weighted FMeasure: 0.7656902555079839
Iteration time: 146.0
Weighted AreaUnderPRC: 0.837020455344258
Mean absolute error: 0.22906666666666362
Coverage of cases: 99.72
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 1432.0
Weighted Recall: 0.7656
Weighted FalsePositiveRate: 0.11835457493688434
Kappa statistic: 0.6479558611143819
Training time: 137.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 70.76000000000143
Incorrectly Classified Instances: 23.56
Correctly Classified Instances: 76.44
Weighted Precision: 0.784460377345305
Weighted AreaUnderROC: 0.9275594424861463
Root mean squared error: 0.3277966849537468
Relative absolute error: 51.03599999999915
Root relative squared error: 69.53617763437924
Weighted TruePositiveRate: 0.7644
Weighted MatthewsCorrelation: 0.6576801589873551
Weighted FMeasure: 0.7636125204614426
Iteration time: 149.0
Weighted AreaUnderPRC: 0.835305518589366
Mean absolute error: 0.22682666666666396
Coverage of cases: 99.56
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 1581.0
Weighted Recall: 0.7644
Weighted FalsePositiveRate: 0.11903015631290337
Kappa statistic: 0.6461083454021003
Training time: 142.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.78666666666808
Incorrectly Classified Instances: 22.76
Correctly Classified Instances: 77.24
Weighted Precision: 0.7928239360382086
Weighted AreaUnderROC: 0.9328221434967919
Root mean squared error: 0.32228765619137917
Relative absolute error: 50.321999999999065
Root relative squared error: 68.36753615569268
Weighted TruePositiveRate: 0.7724
Weighted MatthewsCorrelation: 0.6699563440209128
Weighted FMeasure: 0.771724683250772
Iteration time: 153.0
Weighted AreaUnderPRC: 0.8419714584355584
Mean absolute error: 0.22365333333333023
Coverage of cases: 99.56
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 1734.0
Weighted Recall: 0.7724
Weighted FalsePositiveRate: 0.11502751965746609
Kappa statistic: 0.6581219176896972
Training time: 146.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 71.94666666666815
Incorrectly Classified Instances: 23.28
Correctly Classified Instances: 76.72
Weighted Precision: 0.7889019461451856
Weighted AreaUnderROC: 0.9268316545374251
Root mean squared error: 0.3289822690257525
Relative absolute error: 51.863999999999216
Root relative squared error: 69.78767799547384
Weighted TruePositiveRate: 0.7672
Weighted MatthewsCorrelation: 0.6628620874567738
Weighted FMeasure: 0.7663542800267352
Iteration time: 158.0
Weighted AreaUnderPRC: 0.8330784561295681
Mean absolute error: 0.23050666666666425
Coverage of cases: 99.68
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 1892.0
Weighted Recall: 0.7672
Weighted FalsePositiveRate: 0.11767352105529819
Kappa statistic: 0.6502950260175693
Training time: 151.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 72.24000000000143
Incorrectly Classified Instances: 23.76
Correctly Classified Instances: 76.24
Weighted Precision: 0.7881562744884997
Weighted AreaUnderROC: 0.9261541563822889
Root mean squared error: 0.33003434164745005
Relative absolute error: 51.65399999999921
Root relative squared error: 70.01085630100475
Weighted TruePositiveRate: 0.7624
Weighted MatthewsCorrelation: 0.6583715731164498
Weighted FMeasure: 0.7606220792428334
Iteration time: 170.0
Weighted AreaUnderPRC: 0.8310579771113382
Mean absolute error: 0.2295733333333309
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 19.0
Accumulative iteration time: 2062.0
Weighted Recall: 0.7624
Weighted FalsePositiveRate: 0.12019732301414238
Kappa statistic: 0.6430210372390357
Training time: 162.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 70.93333333333474
Incorrectly Classified Instances: 23.36
Correctly Classified Instances: 76.64
Weighted Precision: 0.7942320248926952
Weighted AreaUnderROC: 0.9308116638033033
Root mean squared error: 0.32743039972895904
Relative absolute error: 51.563999999999105
Root relative squared error: 69.45847680449049
Weighted TruePositiveRate: 0.7664
Weighted MatthewsCorrelation: 0.6655268500016925
Weighted FMeasure: 0.7649466424267961
Iteration time: 234.0
Weighted AreaUnderPRC: 0.8415007390803839
Mean absolute error: 0.22917333333333045
Coverage of cases: 99.48
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2296.0
Weighted Recall: 0.7664
Weighted FalsePositiveRate: 0.11824063557273173
Kappa statistic: 0.6490147531600091
Training time: 226.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 70.54666666666822
Incorrectly Classified Instances: 24.32
Correctly Classified Instances: 75.68
Weighted Precision: 0.7920275980209112
Weighted AreaUnderROC: 0.924019642432517
Root mean squared error: 0.33489898974665916
Relative absolute error: 52.39199999999925
Root relative squared error: 71.04280399871585
Weighted TruePositiveRate: 0.7568
Weighted MatthewsCorrelation: 0.6549179999681113
Weighted FMeasure: 0.7559265761012535
Iteration time: 186.0
Weighted AreaUnderPRC: 0.8268954834828725
Mean absolute error: 0.2328533333333311
Coverage of cases: 99.4
Instances selection time: 11.0
Test time: 19.0
Accumulative iteration time: 2482.0
Weighted Recall: 0.7568
Weighted FalsePositiveRate: 0.12324704174921274
Kappa statistic: 0.6345289366523236
Training time: 175.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 70.6800000000015
Incorrectly Classified Instances: 24.12
Correctly Classified Instances: 75.88
Weighted Precision: 0.7927380527619363
Weighted AreaUnderROC: 0.9262208907240757
Root mean squared error: 0.33463612078395333
Relative absolute error: 52.5179999999992
Root relative squared error: 70.98704107088803
Weighted TruePositiveRate: 0.7588
Weighted MatthewsCorrelation: 0.6579783770715554
Weighted FMeasure: 0.7564040516789873
Iteration time: 186.0
Weighted AreaUnderPRC: 0.8316778199313222
Mean absolute error: 0.23341333333333086
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2668.0
Weighted Recall: 0.7588
Weighted FalsePositiveRate: 0.12220346827159576
Kappa statistic: 0.6375212077144145
Training time: 178.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 70.81333333333474
Incorrectly Classified Instances: 25.6
Correctly Classified Instances: 74.4
Weighted Precision: 0.7805056140554556
Weighted AreaUnderROC: 0.9185377984216205
Root mean squared error: 0.341342447794976
Relative absolute error: 52.90199999999934
Root relative squared error: 72.40966786279262
Weighted TruePositiveRate: 0.744
Weighted MatthewsCorrelation: 0.6370041845693791
Weighted FMeasure: 0.7415931534759295
Iteration time: 192.0
Weighted AreaUnderPRC: 0.8164087679341333
Mean absolute error: 0.23511999999999816
Coverage of cases: 99.2
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 2860.0
Weighted Recall: 0.744
Weighted FalsePositiveRate: 0.1296979511712807
Kappa statistic: 0.6152514331884114
Training time: 184.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 71.17333333333481
Incorrectly Classified Instances: 26.2
Correctly Classified Instances: 73.8
Weighted Precision: 0.7711061998511043
Weighted AreaUnderROC: 0.9239070960579545
Root mean squared error: 0.33681250966474824
Relative absolute error: 53.06999999999917
Root relative squared error: 71.44872287172075
Weighted TruePositiveRate: 0.738
Weighted MatthewsCorrelation: 0.6265826399326739
Weighted FMeasure: 0.7345979331706205
Iteration time: 190.0
Weighted AreaUnderPRC: 0.8234423824410116
Mean absolute error: 0.23586666666666406
Coverage of cases: 99.44
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 3050.0
Weighted Recall: 0.738
Weighted FalsePositiveRate: 0.1326364154784973
Kappa statistic: 0.6062410848172333
Training time: 183.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 70.37333333333481
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7783749709973837
Weighted AreaUnderROC: 0.9176622854193968
Root mean squared error: 0.340211698799438
Relative absolute error: 52.33799999999932
Root relative squared error: 72.16979977802319
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.634581809955855
Weighted FMeasure: 0.7402526307949981
Iteration time: 197.0
Weighted AreaUnderPRC: 0.8154740820872362
Mean absolute error: 0.23261333333333142
Coverage of cases: 99.08
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 3247.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.13025680561387887
Kappa statistic: 0.613462052674001
Training time: 189.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 68.98666666666819
Incorrectly Classified Instances: 24.44
Correctly Classified Instances: 75.56
Weighted Precision: 0.7937396464024671
Weighted AreaUnderROC: 0.9286045128873041
Root mean squared error: 0.3318393185463899
Relative absolute error: 50.36399999999922
Root relative squared error: 70.3937497225424
Weighted TruePositiveRate: 0.7556
Weighted MatthewsCorrelation: 0.6555182711907289
Weighted FMeasure: 0.7532584727432564
Iteration time: 216.0
Weighted AreaUnderPRC: 0.8375847601554065
Mean absolute error: 0.2238399999999976
Coverage of cases: 99.4
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 3463.0
Weighted Recall: 0.7556
Weighted FalsePositiveRate: 0.12390889054482927
Kappa statistic: 0.6326752830263997
Training time: 209.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 70.62666666666823
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7763117598100452
Weighted AreaUnderROC: 0.9184346901604614
Root mean squared error: 0.34528056224853726
Relative absolute error: 54.071999999999306
Root relative squared error: 73.2450680933532
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.618680911960435
Weighted FMeasure: 0.7236646575232843
Iteration time: 205.0
Weighted AreaUnderPRC: 0.8162077824410174
Mean absolute error: 0.24031999999999804
Coverage of cases: 99.28
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 3668.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13838019964280837
Kappa statistic: 0.589884260768184
Training time: 198.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 70.49333333333479
Incorrectly Classified Instances: 24.68
Correctly Classified Instances: 75.32
Weighted Precision: 0.7954881923786395
Weighted AreaUnderROC: 0.9187598992751366
Root mean squared error: 0.34014898696502505
Relative absolute error: 52.70999999999936
Root relative squared error: 72.15649658901097
Weighted TruePositiveRate: 0.7532
Weighted MatthewsCorrelation: 0.6536955087934689
Weighted FMeasure: 0.7518516865210891
Iteration time: 210.0
Weighted AreaUnderPRC: 0.8175734641382957
Mean absolute error: 0.23426666666666493
Coverage of cases: 99.04
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 3878.0
Weighted Recall: 0.7532
Weighted FalsePositiveRate: 0.1252119892654587
Kappa statistic: 0.6290495226703047
Training time: 203.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 69.64000000000142
Incorrectly Classified Instances: 25.44
Correctly Classified Instances: 74.56
Weighted Precision: 0.7873491220015308
Weighted AreaUnderROC: 0.9199286771694406
Root mean squared error: 0.3400588184417507
Relative absolute error: 52.51799999999929
Root relative squared error: 72.1373689567339
Weighted TruePositiveRate: 0.7456
Weighted MatthewsCorrelation: 0.6422151937242789
Weighted FMeasure: 0.7434871343518483
Iteration time: 217.0
Weighted AreaUnderPRC: 0.8181426356064456
Mean absolute error: 0.23341333333333128
Coverage of cases: 99.04
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4095.0
Weighted Recall: 0.7456
Weighted FalsePositiveRate: 0.12901003099949335
Kappa statistic: 0.6176176763564112
Training time: 210.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 69.0133333333347
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7778318009126882
Weighted AreaUnderROC: 0.921512992322042
Root mean squared error: 0.3421266821904804
Relative absolute error: 52.16999999999929
Root relative squared error: 72.57602910052289
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.6246715933273057
Weighted FMeasure: 0.7285003585182503
Iteration time: 220.0
Weighted AreaUnderPRC: 0.8207254779533792
Mean absolute error: 0.2318666666666646
Coverage of cases: 99.16
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4315.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13593879316637889
Kappa statistic: 0.5971077386823955
Training time: 213.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 69.84000000000148
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.7817978710730346
Weighted AreaUnderROC: 0.9236627450271797
Root mean squared error: 0.3393483952125497
Relative absolute error: 52.553999999999284
Root relative squared error: 71.98666543186977
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.6336468211933141
Weighted FMeasure: 0.7369471335158809
Iteration time: 236.0
Weighted AreaUnderPRC: 0.8279799050577805
Mean absolute error: 0.23357333333333122
Coverage of cases: 99.48
Instances selection time: 7.0
Test time: 19.0
Accumulative iteration time: 4551.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.13203775707932588
Kappa statistic: 0.6085831286461594
Training time: 229.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 69.3200000000012
Incorrectly Classified Instances: 25.36
Correctly Classified Instances: 74.64
Weighted Precision: 0.7894037279344058
Weighted AreaUnderROC: 0.9256620183568463
Root mean squared error: 0.3365550970247409
Relative absolute error: 51.461999999999335
Root relative squared error: 71.39411740472706
Weighted TruePositiveRate: 0.7464
Weighted MatthewsCorrelation: 0.6446383302367401
Weighted FMeasure: 0.7427872259310463
Iteration time: 237.0
Weighted AreaUnderPRC: 0.8339530678483319
Mean absolute error: 0.22871999999999812
Coverage of cases: 99.36
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 4788.0
Weighted Recall: 0.7464
Weighted FalsePositiveRate: 0.1286214400272917
Kappa statistic: 0.6187957505226225
Training time: 230.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 69.48000000000127
Incorrectly Classified Instances: 25.8
Correctly Classified Instances: 74.2
Weighted Precision: 0.7845572446581044
Weighted AreaUnderROC: 0.9220287192345026
Root mean squared error: 0.33929338337197107
Relative absolute error: 51.749999999999325
Root relative squared error: 71.97499565821416
Weighted TruePositiveRate: 0.742
Weighted MatthewsCorrelation: 0.6370644182116874
Weighted FMeasure: 0.7394371355232275
Iteration time: 232.0
Weighted AreaUnderPRC: 0.8229048969456421
Mean absolute error: 0.2299999999999981
Coverage of cases: 99.4
Instances selection time: 7.0
Test time: 18.0
Accumulative iteration time: 5020.0
Weighted Recall: 0.742
Weighted FalsePositiveRate: 0.13081018755090992
Kappa statistic: 0.6122066057388131
Training time: 225.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 70.82666666666792
Incorrectly Classified Instances: 25.24
Correctly Classified Instances: 74.76
Weighted Precision: 0.7890966646788871
Weighted AreaUnderROC: 0.9223871801302572
Root mean squared error: 0.3393719689858502
Relative absolute error: 52.595999999999364
Root relative squared error: 71.99166618435744
Weighted TruePositiveRate: 0.7476
Weighted MatthewsCorrelation: 0.644543573802767
Weighted FMeasure: 0.7460714363247537
Iteration time: 238.0
Weighted AreaUnderPRC: 0.8275283084443772
Mean absolute error: 0.23375999999999827
Coverage of cases: 99.52
Instances selection time: 7.0
Test time: 17.0
Accumulative iteration time: 5258.0
Weighted Recall: 0.7476
Weighted FalsePositiveRate: 0.12798019010227882
Kappa statistic: 0.6206480941745015
Training time: 231.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 70.08000000000138
Incorrectly Classified Instances: 25.08
Correctly Classified Instances: 74.92
Weighted Precision: 0.7885074306898548
Weighted AreaUnderROC: 0.9233218887935638
Root mean squared error: 0.337223961188999
Relative absolute error: 52.42199999999933
Root relative squared error: 71.53600492059893
Weighted TruePositiveRate: 0.7492
Weighted MatthewsCorrelation: 0.6458380345342801
Weighted FMeasure: 0.746278350316113
Iteration time: 249.0
Weighted AreaUnderPRC: 0.8260835702142111
Mean absolute error: 0.23298666666666476
Coverage of cases: 99.12
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 5507.0
Weighted Recall: 0.7492
Weighted FalsePositiveRate: 0.12709314751465592
Kappa statistic: 0.6230735269929851
Training time: 239.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 69.36000000000143
Incorrectly Classified Instances: 25.36
Correctly Classified Instances: 74.64
Weighted Precision: 0.788540924072292
Weighted AreaUnderROC: 0.9265847986117084
Root mean squared error: 0.33579358739161896
Relative absolute error: 51.39599999999924
Root relative squared error: 71.23257681707123
Weighted TruePositiveRate: 0.7464
Weighted MatthewsCorrelation: 0.6430028672893957
Weighted FMeasure: 0.744164034955982
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8333734723522092
Mean absolute error: 0.22842666666666436
Coverage of cases: 99.44
Instances selection time: 10.0
Test time: 14.0
Accumulative iteration time: 5761.0
Weighted Recall: 0.7464
Weighted FalsePositiveRate: 0.12857337599588314
Kappa statistic: 0.6188445193449031
Training time: 244.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 70.33333333333468
Incorrectly Classified Instances: 25.56
Correctly Classified Instances: 74.44
Weighted Precision: 0.7777785441845979
Weighted AreaUnderROC: 0.9235124540771671
Root mean squared error: 0.33549962742155076
Relative absolute error: 51.977999999999284
Root relative squared error: 71.17021849060144
Weighted TruePositiveRate: 0.7444
Weighted MatthewsCorrelation: 0.6355017966463363
Weighted FMeasure: 0.7417705982626407
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8259664728026286
Mean absolute error: 0.23101333333333124
Coverage of cases: 99.28
Instances selection time: 10.0
Test time: 15.0
Accumulative iteration time: 6015.0
Weighted Recall: 0.7444
Weighted FalsePositiveRate: 0.1293972281870609
Kappa statistic: 0.6158982307034243
Training time: 244.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 69.62666666666813
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7881420912040095
Weighted AreaUnderROC: 0.924591943095078
Root mean squared error: 0.3354996274215511
Relative absolute error: 51.689999999999344
Root relative squared error: 71.17021849060151
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.6460661383265428
Weighted FMeasure: 0.7465429038760811
Iteration time: 254.0
Weighted AreaUnderPRC: 0.8282175670454246
Mean absolute error: 0.22973333333333149
Coverage of cases: 99.4
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 6269.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.12660842847963713
Kappa statistic: 0.6243067407425574
Training time: 245.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 69.17333333333478
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7841437342727317
Weighted AreaUnderROC: 0.9222403482004176
Root mean squared error: 0.33955264687526676
Relative absolute error: 52.57799999999923
Root relative squared error: 72.02999375260252
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.6359460791123498
Weighted FMeasure: 0.7377995114104864
Iteration time: 265.0
Weighted AreaUnderPRC: 0.8255796907389633
Mean absolute error: 0.23367999999999767
Coverage of cases: 99.36
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 6534.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.13095298586050155
Kappa statistic: 0.6116302200265102
Training time: 259.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 70.13333333333476
Incorrectly Classified Instances: 24.56
Correctly Classified Instances: 75.44
Weighted Precision: 0.7898081268864432
Weighted AreaUnderROC: 0.9287458791055898
Root mean squared error: 0.3320361426110108
Relative absolute error: 51.45599999999916
Root relative squared error: 70.43550241178063
Weighted TruePositiveRate: 0.7544
Weighted MatthewsCorrelation: 0.6510693132571491
Weighted FMeasure: 0.7518567191991682
Iteration time: 266.0
Weighted AreaUnderPRC: 0.8360310681345945
Mean absolute error: 0.22869333333333067
Coverage of cases: 99.52
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 6800.0
Weighted Recall: 0.7544
Weighted FalsePositiveRate: 0.12436475063440908
Kappa statistic: 0.6309459180695128
Training time: 260.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 69.49333333333477
Incorrectly Classified Instances: 24.96
Correctly Classified Instances: 75.04
Weighted Precision: 0.7859109505911416
Weighted AreaUnderROC: 0.9257535890098884
Root mean squared error: 0.3335146173708123
Relative absolute error: 51.97799999999907
Root relative squared error: 70.74913427032126
Weighted TruePositiveRate: 0.7504
Weighted MatthewsCorrelation: 0.6449833233387133
Weighted FMeasure: 0.7463451527478681
Iteration time: 278.0
Weighted AreaUnderPRC: 0.8302484029553443
Mean absolute error: 0.23101333333333027
Coverage of cases: 99.48
Instances selection time: 6.0
Test time: 15.0
Accumulative iteration time: 7078.0
Weighted Recall: 0.7504
Weighted FalsePositiveRate: 0.12632143807581975
Kappa statistic: 0.624945544978165
Training time: 272.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 69.89333333333471
Incorrectly Classified Instances: 25.12
Correctly Classified Instances: 74.88
Weighted Precision: 0.7874491622032997
Weighted AreaUnderROC: 0.9309654601028787
Root mean squared error: 0.33148554518510476
Relative absolute error: 51.40199999999919
Root relative squared error: 70.318703059712
Weighted TruePositiveRate: 0.7488
Weighted MatthewsCorrelation: 0.6440198647361493
Weighted FMeasure: 0.7448822282529751
Iteration time: 287.0
Weighted AreaUnderPRC: 0.8422454021652551
Mean absolute error: 0.2284533333333308
Coverage of cases: 99.6
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 7365.0
Weighted Recall: 0.7488
Weighted FalsePositiveRate: 0.12717414921243653
Kappa statistic: 0.6225258311344062
Training time: 281.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 69.6266666666681
Incorrectly Classified Instances: 24.36
Correctly Classified Instances: 75.64
Weighted Precision: 0.7968575712167881
Weighted AreaUnderROC: 0.9289836063346225
Root mean squared error: 0.3313246142380603
Relative absolute error: 50.77799999999921
Root relative squared error: 70.28456445052464
Weighted TruePositiveRate: 0.7564
Weighted MatthewsCorrelation: 0.6567968343025067
Weighted FMeasure: 0.752264737427821
Iteration time: 291.0
Weighted AreaUnderPRC: 0.8380939488378656
Mean absolute error: 0.22567999999999755
Coverage of cases: 99.44
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 7656.0
Weighted Recall: 0.7564
Weighted FalsePositiveRate: 0.12341730521960927
Kappa statistic: 0.633919470939698
Training time: 286.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 70.26666666666814
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.780297655690273
Weighted AreaUnderROC: 0.9190502534383852
Root mean squared error: 0.33911256734797146
Relative absolute error: 52.54799999999925
Root relative squared error: 71.93663878719896
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6369777793805615
Weighted FMeasure: 0.7411839707711945
Iteration time: 292.0
Weighted AreaUnderPRC: 0.8170972929674414
Mean absolute error: 0.23354666666666443
Coverage of cases: 99.2
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 7948.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.12891250915204208
Kappa statistic: 0.6171319104985435
Training time: 287.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 68.08000000000133
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7812856232109806
Weighted AreaUnderROC: 0.9234038570481745
Root mean squared error: 0.3353207419769903
Relative absolute error: 51.08999999999912
Root relative squared error: 71.13227115733017
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6378517279811207
Weighted FMeasure: 0.7418412044984377
Iteration time: 301.0
Weighted AreaUnderPRC: 0.827501627880233
Mean absolute error: 0.22706666666666384
Coverage of cases: 99.44
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 8249.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.129001770924658
Kappa statistic: 0.6170986779385123
Training time: 295.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 69.7600000000013
Incorrectly Classified Instances: 24.0
Correctly Classified Instances: 76.0
Weighted Precision: 0.7903277699076094
Weighted AreaUnderROC: 0.9253081703794023
Root mean squared error: 0.33146945560639446
Relative absolute error: 51.41399999999921
Root relative squared error: 70.31528994464827
Weighted TruePositiveRate: 0.76
Weighted MatthewsCorrelation: 0.6563650769435667
Weighted FMeasure: 0.7567946598402899
Iteration time: 304.0
Weighted AreaUnderPRC: 0.8295385229725464
Mean absolute error: 0.22850666666666425
Coverage of cases: 99.4
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 8553.0
Weighted Recall: 0.76
Weighted FalsePositiveRate: 0.12137682851114978
Kappa statistic: 0.6394349513008827
Training time: 299.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 69.33333333333482
Incorrectly Classified Instances: 25.92
Correctly Classified Instances: 74.08
Weighted Precision: 0.7762035567718368
Weighted AreaUnderROC: 0.9266421167399432
Root mean squared error: 0.33393013241295383
Relative absolute error: 51.56999999999903
Root relative squared error: 70.83727832151625
Weighted TruePositiveRate: 0.7408
Weighted MatthewsCorrelation: 0.6300998868708043
Weighted FMeasure: 0.7360365985862819
Iteration time: 305.0
Weighted AreaUnderPRC: 0.8343849843708644
Mean absolute error: 0.22919999999999674
Coverage of cases: 99.76
Instances selection time: 5.0
Test time: 16.0
Accumulative iteration time: 8858.0
Weighted Recall: 0.7408
Weighted FalsePositiveRate: 0.13111498925606277
Kappa statistic: 0.6105363851573974
Training time: 300.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 68.50666666666814
Incorrectly Classified Instances: 24.12
Correctly Classified Instances: 75.88
Weighted Precision: 0.7904962282251957
Weighted AreaUnderROC: 0.9307365334341353
Root mean squared error: 0.3266965972682703
Relative absolute error: 49.82399999999901
Root relative squared error: 69.30281379568916
Weighted TruePositiveRate: 0.7588
Weighted MatthewsCorrelation: 0.6549367674663723
Weighted FMeasure: 0.7542356500020901
Iteration time: 305.0
Weighted AreaUnderPRC: 0.8419301950016296
Mean absolute error: 0.22143999999999664
Coverage of cases: 99.52
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9163.0
Weighted Recall: 0.7588
Weighted FalsePositiveRate: 0.1219288166635467
Kappa statistic: 0.6376519848619052
Training time: 300.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 68.73333333333476
Incorrectly Classified Instances: 24.4
Correctly Classified Instances: 75.6
Weighted Precision: 0.7884381161663726
Weighted AreaUnderROC: 0.9240045742502553
Root mean squared error: 0.33297847778297335
Relative absolute error: 50.85599999999907
Root relative squared error: 70.6354018888542
Weighted TruePositiveRate: 0.756
Weighted MatthewsCorrelation: 0.651043921091279
Weighted FMeasure: 0.7511880855091904
Iteration time: 320.0
Weighted AreaUnderPRC: 0.8260021001671959
Mean absolute error: 0.2260266666666636
Coverage of cases: 99.24
Instances selection time: 5.0
Test time: 15.0
Accumulative iteration time: 9483.0
Weighted Recall: 0.756
Weighted FalsePositiveRate: 0.12333351595256045
Kappa statistic: 0.633443949133077
Training time: 315.0
		
Time end:Wed Nov 01 16.39.13 EET 2017