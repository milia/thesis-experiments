Wed Nov 01 17.09.41 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.09.41 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.82666666666714
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8342981063287792
Weighted AreaUnderROC: 0.9561185915628945
Root mean squared error: 0.33180401969671464
Relative absolute error: 29.69085252949065
Root relative squared error: 70.38626170575034
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7212137549328612
Weighted FMeasure: 0.7926486400769817
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9195206344412611
Mean absolute error: 0.13195934457551461
Coverage of cases: 89.68
Instances selection time: 10.0
Test time: 134.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09756950169852903
Kappa statistic: 0.704678515308076
Training time: 4.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 39.84000000000049
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8324984015158805
Weighted AreaUnderROC: 0.9554173448129816
Root mean squared error: 0.3323823584806695
Relative absolute error: 29.773831531334356
Root relative squared error: 70.50894588853764
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7191488431209077
Weighted FMeasure: 0.7913899922488323
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9184514250837353
Mean absolute error: 0.13232814013926442
Coverage of cases: 89.6
Instances selection time: 12.0
Test time: 129.0
Accumulative iteration time: 30.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09817677766573625
Kappa statistic: 0.7028800032970735
Training time: 4.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 39.986666666667176
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8332481036389776
Weighted AreaUnderROC: 0.9556638109314209
Root mean squared error: 0.3320962393502848
Relative absolute error: 29.791336871104406
Root relative squared error: 70.44825085534099
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7207161492377905
Weighted FMeasure: 0.7930816846835568
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9188557412081116
Mean absolute error: 0.13240594164935354
Coverage of cases: 89.52
Instances selection time: 10.0
Test time: 134.0
Accumulative iteration time: 45.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09758609208897258
Kappa statistic: 0.7046695989640791
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.14666666666714
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8348808894909331
Weighted AreaUnderROC: 0.95559992623709
Root mean squared error: 0.33003087292328703
Relative absolute error: 29.60173057264379
Root relative squared error: 70.01012047349143
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7235620212470923
Weighted FMeasure: 0.7951531617774017
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9190402004471911
Mean absolute error: 0.13156324698952857
Coverage of cases: 89.88
Instances selection time: 9.0
Test time: 130.0
Accumulative iteration time: 59.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09660137794127031
Kappa statistic: 0.7076614739037544
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 39.78666666666717
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8340849072991965
Weighted AreaUnderROC: 0.9548530423456439
Root mean squared error: 0.33309674851148385
Relative absolute error: 29.899742120305575
Root relative squared error: 70.66049089909792
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7203519838833566
Weighted FMeasure: 0.7914608734221075
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9180095151964505
Mean absolute error: 0.13288774275691428
Coverage of cases: 89.2
Instances selection time: 9.0
Test time: 132.0
Accumulative iteration time: 73.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09797281954021701
Kappa statistic: 0.7034838668866598
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 39.94666666666714
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8314263632099393
Weighted AreaUnderROC: 0.9553605073888679
Root mean squared error: 0.3322561328288315
Relative absolute error: 29.87568994507513
Root relative squared error: 70.48216938422533
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7170006398200348
Weighted FMeasure: 0.7898614009380355
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9188305694971416
Mean absolute error: 0.13278084420033454
Coverage of cases: 89.52
Instances selection time: 8.0
Test time: 130.0
Accumulative iteration time: 87.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.09897483863765122
Kappa statistic: 0.7004835872194192
Training time: 6.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 39.96000000000047
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8330386216126082
Weighted AreaUnderROC: 0.9537008794024999
Root mean squared error: 0.3341907220680221
Relative absolute error: 30.14163127284623
Root relative squared error: 70.89255773517799
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7206345610969943
Weighted FMeasure: 0.7931218675819921
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9162405843670481
Mean absolute error: 0.13396280565709498
Coverage of cases: 89.52
Instances selection time: 9.0
Test time: 130.0
Accumulative iteration time: 102.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09760046103814587
Kappa statistic: 0.7046670513383314
Training time: 6.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 39.89333333333382
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8330457810184376
Weighted AreaUnderROC: 0.9532310935043011
Root mean squared error: 0.3352846652647882
Relative absolute error: 30.211853784469692
Root relative squared error: 71.12461813097785
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.718576226085622
Weighted FMeasure: 0.7905281219209319
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9154623864737202
Mean absolute error: 0.13427490570875483
Coverage of cases: 89.24
Instances selection time: 9.0
Test time: 131.0
Accumulative iteration time: 116.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09856636335413432
Kappa statistic: 0.701685798710516
Training time: 5.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 39.920000000000464
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8330321225464777
Weighted AreaUnderROC: 0.9533428592180784
Root mean squared error: 0.335224713565587
Relative absolute error: 30.206059447101563
Root relative squared error: 71.1119004550632
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7190175755440239
Weighted FMeasure: 0.7907472783254789
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9157559809493152
Mean absolute error: 0.1342491530982298
Coverage of cases: 89.36
Instances selection time: 8.0
Test time: 131.0
Accumulative iteration time: 130.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09837692970459841
Kappa statistic: 0.7022845378689275
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 39.853333333333815
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8326058913189045
Weighted AreaUnderROC: 0.9532282031560455
Root mean squared error: 0.3360891105695906
Relative absolute error: 30.292933933071232
Root relative squared error: 71.2952667500137
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7180205495590091
Weighted FMeasure: 0.7900575826690897
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9157906219805083
Mean absolute error: 0.13463526192476166
Coverage of cases: 89.08
Instances selection time: 8.0
Test time: 129.0
Accumulative iteration time: 144.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.0987773893091327
Kappa statistic: 0.7010849120123371
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 39.8000000000005
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8323891317549588
Weighted AreaUnderROC: 0.9527912510221307
Root mean squared error: 0.3346849143685233
Relative absolute error: 30.128678869454298
Root relative squared error: 70.99739175324638
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7179684231071134
Weighted FMeasure: 0.7899353322564444
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9154235256038319
Mean absolute error: 0.1339052394197975
Coverage of cases: 89.0
Instances selection time: 8.0
Test time: 130.0
Accumulative iteration time: 158.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.0987874709025755
Kappa statistic: 0.7010840525286971
Training time: 6.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 39.8800000000005
Incorrectly Classified Instances: 20.0
Correctly Classified Instances: 80.0
Weighted Precision: 0.8313566808452518
Weighted AreaUnderROC: 0.9519054822540418
Root mean squared error: 0.3337416164832936
Relative absolute error: 30.044783382714083
Root relative squared error: 70.79728805384893
Weighted TruePositiveRate: 0.8
Weighted MatthewsCorrelation: 0.7170119349010143
Weighted FMeasure: 0.7899245524121458
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9141695912424104
Mean absolute error: 0.133532370589841
Coverage of cases: 89.28
Instances selection time: 8.0
Test time: 132.0
Accumulative iteration time: 173.0
Weighted Recall: 0.8
Weighted FalsePositiveRate: 0.09899063670540134
Kappa statistic: 0.7004802858520306
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 39.96000000000049
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8311506916566519
Weighted AreaUnderROC: 0.9516619795424043
Root mean squared error: 0.3334836415700524
Relative absolute error: 30.07002167605467
Root relative squared error: 70.74256331069024
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7177625163114064
Weighted FMeasure: 0.7908666800485509
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9136814494798396
Mean absolute error: 0.13364454078246582
Coverage of cases: 89.32
Instances selection time: 8.0
Test time: 130.0
Accumulative iteration time: 187.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09861105484704107
Kappa statistic: 0.7016725030228123
Training time: 6.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 40.02666666666718
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8320733084404552
Weighted AreaUnderROC: 0.9513842093412015
Root mean squared error: 0.33245217919602094
Relative absolute error: 29.947732449577558
Root relative squared error: 70.52375709892533
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7194183493909453
Weighted FMeasure: 0.7924697218542625
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9133363544929299
Mean absolute error: 0.1331010331092342
Coverage of cases: 89.32
Instances selection time: 8.0
Test time: 136.0
Accumulative iteration time: 201.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09802036927027741
Kappa statistic: 0.7034636879005037
Training time: 6.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 39.98666666666718
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8336313428313064
Weighted AreaUnderROC: 0.9518955510557157
Root mean squared error: 0.33192360323329245
Relative absolute error: 29.862230193166887
Root relative squared error: 70.41162920464008
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7217765917618147
Weighted FMeasure: 0.7940493534994013
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9141073804344072
Mean absolute error: 0.13272102308074235
Coverage of cases: 89.4
Instances selection time: 7.0
Test time: 130.0
Accumulative iteration time: 215.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09722080141638058
Kappa statistic: 0.7058606457598883
Training time: 7.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.01333333333383
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8328207776801415
Weighted AreaUnderROC: 0.9525571002163821
Root mean squared error: 0.33149912720301145
Relative absolute error: 29.844553229755682
Root relative squared error: 70.32158424080123
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7209699274998557
Weighted FMeasure: 0.7939075482554848
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9150938575321216
Mean absolute error: 0.13264245879891476
Coverage of cases: 89.56
Instances selection time: 8.0
Test time: 142.0
Accumulative iteration time: 230.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09742611089707166
Kappa statistic: 0.7052573462809227
Training time: 7.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 39.96000000000049
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8334420306992996
Weighted AreaUnderROC: 0.9525212558073106
Root mean squared error: 0.330999279950405
Relative absolute error: 29.773688209531137
Root relative squared error: 70.21555062623858
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7221009212130847
Weighted FMeasure: 0.7947377614423925
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9149863124107235
Mean absolute error: 0.13232750315347233
Coverage of cases: 89.56
Instances selection time: 7.0
Test time: 132.0
Accumulative iteration time: 244.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09703065320755624
Kappa statistic: 0.7064539396277597
Training time: 7.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 40.040000000000504
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.833607059285372
Weighted AreaUnderROC: 0.952472393604472
Root mean squared error: 0.33004047338547743
Relative absolute error: 29.617312351811595
Root relative squared error: 70.01215703906664
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7230036487652048
Weighted FMeasure: 0.7956742876972709
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9150433758267316
Mean absolute error: 0.1316324993413855
Coverage of cases: 89.68
Instances selection time: 7.0
Test time: 132.0
Accumulative iteration time: 258.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09664241887432999
Kappa statistic: 0.7076480235568595
Training time: 7.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 40.08000000000051
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8330028779028508
Weighted AreaUnderROC: 0.9526551757898453
Root mean squared error: 0.3297125413687323
Relative absolute error: 29.59023619711216
Root relative squared error: 69.94259215322404
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7231278153543687
Weighted FMeasure: 0.796212607809498
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9152402994062284
Mean absolute error: 0.13151216087605466
Coverage of cases: 89.76
Instances selection time: 7.0
Test time: 130.0
Accumulative iteration time: 272.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09645806490321797
Kappa statistic: 0.7082419317810926
Training time: 7.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 40.12000000000051
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8328191029676864
Weighted AreaUnderROC: 0.9528772421316929
Root mean squared error: 0.328777336891027
Relative absolute error: 29.499066041667273
Root relative squared error: 69.74420532482961
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7222120188427096
Weighted FMeasure: 0.7954020152511284
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9156757745109826
Mean absolute error: 0.1311069601851885
Coverage of cases: 89.84
Instances selection time: 6.0
Test time: 130.0
Accumulative iteration time: 286.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09684701379573264
Kappa statistic: 0.7070462739880575
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 40.173333333333844
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8329270242713345
Weighted AreaUnderROC: 0.9534439658480128
Root mean squared error: 0.32804369934215427
Relative absolute error: 29.408791117149374
Root relative squared error: 69.58857729910733
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7230904485775104
Weighted FMeasure: 0.7963051740457692
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9165047333782786
Mean absolute error: 0.13070573829844226
Coverage of cases: 89.84
Instances selection time: 7.0
Test time: 129.0
Accumulative iteration time: 301.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09645735034392958
Kappa statistic: 0.7082405334537275
Training time: 8.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 40.04000000000048
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8345120424220882
Weighted AreaUnderROC: 0.9527787040332478
Root mean squared error: 0.3291191390529005
Relative absolute error: 29.517548816066647
Root relative squared error: 69.8167125127751
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.72465759844118
Weighted FMeasure: 0.797002132251673
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9154832794105725
Mean absolute error: 0.13118910584918572
Coverage of cases: 89.72
Instances selection time: 6.0
Test time: 131.0
Accumulative iteration time: 316.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09604958961970107
Kappa statistic: 0.7094426234850878
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 40.05333333333384
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8364699692794637
Weighted AreaUnderROC: 0.9531141847456389
Root mean squared error: 0.3293681257060539
Relative absolute error: 29.499609654773472
Root relative squared error: 69.86953055803602
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.727208742768562
Weighted FMeasure: 0.7984709604013724
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9159758478383297
Mean absolute error: 0.13110937624343827
Coverage of cases: 89.64
Instances selection time: 6.0
Test time: 130.0
Accumulative iteration time: 331.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09524208385022669
Kappa statistic: 0.7118424256134102
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 39.986666666667155
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.83542422076406
Weighted AreaUnderROC: 0.9531510862514293
Root mean squared error: 0.3313655457721649
Relative absolute error: 29.692461517798666
Root relative squared error: 70.29324734012357
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7238625036911953
Weighted FMeasure: 0.7951606060116742
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9160975005385044
Mean absolute error: 0.13196649563466134
Coverage of cases: 89.44
Instances selection time: 6.0
Test time: 129.0
Accumulative iteration time: 346.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09661281088988494
Kappa statistic: 0.7076604931911252
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 39.81333333333381
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8367558456564762
Weighted AreaUnderROC: 0.9534329297740946
Root mean squared error: 0.33098797905968136
Relative absolute error: 29.61541711051767
Root relative squared error: 70.21315334529933
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7265261920307661
Weighted FMeasure: 0.797300974255172
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9165446565512566
Mean absolute error: 0.1316240760467458
Coverage of cases: 89.4
Instances selection time: 7.0
Test time: 129.0
Accumulative iteration time: 362.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09562166570858695
Kappa statistic: 0.710652585651627
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 39.92000000000049
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8378587662373888
Weighted AreaUnderROC: 0.9540018801345752
Root mean squared error: 0.3297690509397039
Relative absolute error: 29.482017180498392
Root relative squared error: 69.95457964347483
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7286726968278651
Weighted FMeasure: 0.7990992543267031
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9172638870459455
Mean absolute error: 0.13103118746888234
Coverage of cases: 89.56
Instances selection time: 6.0
Test time: 129.0
Accumulative iteration time: 377.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.0948300357702677
Kappa statistic: 0.7130452619279716
Training time: 9.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 39.96000000000049
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8360977861366178
Weighted AreaUnderROC: 0.9536427799560863
Root mean squared error: 0.33053749246551484
Relative absolute error: 29.590737097993387
Root relative squared error: 70.11759070762871
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.725378674851457
Weighted FMeasure: 0.7965791325707371
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9168391571743976
Mean absolute error: 0.13151438710219346
Coverage of cases: 89.52
Instances selection time: 6.0
Test time: 133.0
Accumulative iteration time: 393.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09601712339810237
Kappa statistic: 0.7094544597122965
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 40.106666666667145
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8331522142651476
Weighted AreaUnderROC: 0.952960477910662
Root mean squared error: 0.32983083158399124
Relative absolute error: 29.594410218243603
Root relative squared error: 69.96768529723133
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7223719093220902
Weighted FMeasure: 0.7950278784455388
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9159461244923095
Mean absolute error: 0.1315307120810833
Coverage of cases: 89.84
Instances selection time: 6.0
Test time: 127.0
Accumulative iteration time: 409.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09683621764300142
Kappa statistic: 0.7070525920274214
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 40.02666666666716
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.834198904226454
Weighted AreaUnderROC: 0.9528926727424701
Root mean squared error: 0.3299763861737053
Relative absolute error: 29.616306537340996
Root relative squared error: 69.9985620884572
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7236826734107277
Weighted FMeasure: 0.7957787803904338
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9157892864647176
Mean absolute error: 0.1316280290548495
Coverage of cases: 89.76
Instances selection time: 5.0
Test time: 131.0
Accumulative iteration time: 424.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09643353659719685
Kappa statistic: 0.7082522789870231
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 40.053333333333846
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.83361001434412
Weighted AreaUnderROC: 0.9518529804929773
Root mean squared error: 0.32984897943649744
Relative absolute error: 29.60539714946749
Root relative squared error: 69.97153503810266
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7238109729685901
Weighted FMeasure: 0.7963351669880371
Iteration time: 15.0
Weighted AreaUnderPRC: 0.914157508888396
Mean absolute error: 0.1315795428865228
Coverage of cases: 89.8
Instances selection time: 5.0
Test time: 128.0
Accumulative iteration time: 439.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09624989718537326
Kappa statistic: 0.7088461873947413
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 39.960000000000505
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8353829632546113
Weighted AreaUnderROC: 0.9522382374600064
Root mean squared error: 0.330658074656817
Relative absolute error: 29.675084267134494
Root relative squared error: 70.14317005317673
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7258689915862684
Weighted FMeasure: 0.797596104249509
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9146884605371459
Mean absolute error: 0.13188926340948726
Coverage of cases: 89.64
Instances selection time: 5.0
Test time: 129.0
Accumulative iteration time: 454.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09564333577745443
Kappa statistic: 0.710644681253849
Training time: 10.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 40.040000000000525
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8349037150860067
Weighted AreaUnderROC: 0.9517496647253257
Root mean squared error: 0.33033418745207527
Relative absolute error: 29.670765355610815
Root relative squared error: 70.074463201533
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7256384115487415
Weighted FMeasure: 0.7975631443996443
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9138377444527308
Mean absolute error: 0.1318700682471598
Coverage of cases: 89.64
Instances selection time: 5.0
Test time: 129.0
Accumulative iteration time: 469.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09564913001516677
Kappa statistic: 0.7106438491868552
Training time: 10.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 39.96000000000053
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.836341023028005
Weighted AreaUnderROC: 0.9517363719262881
Root mean squared error: 0.33139683547919707
Relative absolute error: 29.805942583045656
Root relative squared error: 70.29988488933071
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7263299430631741
Weighted FMeasure: 0.797166409067968
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9138119215850314
Mean absolute error: 0.13247085592464797
Coverage of cases: 89.48
Instances selection time: 5.0
Test time: 128.0
Accumulative iteration time: 485.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.0956281745055877
Kappa statistic: 0.7106531403299919
Training time: 11.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 40.02666666666716
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8367077657782751
Weighted AreaUnderROC: 0.9518022270741058
Root mean squared error: 0.33137749369395747
Relative absolute error: 29.824612440606835
Root relative squared error: 70.29578187707976
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7264829159600598
Weighted FMeasure: 0.7971835152074611
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9139184453512001
Mean absolute error: 0.13255383306936433
Coverage of cases: 89.6
Instances selection time: 5.0
Test time: 128.0
Accumulative iteration time: 501.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09562023659001012
Kappa statistic: 0.7106548043523268
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 40.040000000000504
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8341132077332352
Weighted AreaUnderROC: 0.9514916077142294
Root mean squared error: 0.33176080305502736
Relative absolute error: 29.876661272400877
Root relative squared error: 70.37709407163119
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7228258425036054
Weighted FMeasure: 0.7944778537129837
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9133732616102305
Mean absolute error: 0.1327851612106712
Coverage of cases: 89.56
Instances selection time: 4.0
Test time: 128.0
Accumulative iteration time: 516.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09682034181184627
Kappa statistic: 0.7070629810996317
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 39.97333333333384
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8348970849658388
Weighted AreaUnderROC: 0.9513037397786657
Root mean squared error: 0.3325098698087165
Relative absolute error: 29.947095942739114
Root relative squared error: 70.53599512595969
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7236088316153164
Weighted FMeasure: 0.7946116625723593
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9130256962149369
Mean absolute error: 0.13309820418995225
Coverage of cases: 89.48
Instances selection time: 5.0
Test time: 127.0
Accumulative iteration time: 532.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09661431777186678
Kappa statistic: 0.7076666575612018
Training time: 11.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 39.94666666666717
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8353677906092059
Weighted AreaUnderROC: 0.9515495069019031
Root mean squared error: 0.33251742344043406
Relative absolute error: 29.909333269799102
Root relative squared error: 70.5375974932227
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7238260633452721
Weighted FMeasure: 0.7945126914160363
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9133176422545847
Mean absolute error: 0.13293037008799663
Coverage of cases: 89.48
Instances selection time: 4.0
Test time: 132.0
Accumulative iteration time: 547.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09660709441557763
Kappa statistic: 0.7076694594616613
Training time: 11.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 39.92000000000049
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8367963988588656
Weighted AreaUnderROC: 0.9507699918606253
Root mean squared error: 0.33283776677050414
Relative absolute error: 29.951458750310515
Root relative squared error: 70.60555257552284
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7257377063991133
Weighted FMeasure: 0.79575911664785
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9121931795822482
Mean absolute error: 0.13311759444582513
Coverage of cases: 89.44
Instances selection time: 4.0
Test time: 129.0
Accumulative iteration time: 563.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09600847092323639
Kappa statistic: 0.7094661557424345
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 39.86666666666717
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8375133907952798
Weighted AreaUnderROC: 0.9519440882044106
Root mean squared error: 0.3327630488020404
Relative absolute error: 29.92170832256076
Root relative squared error: 70.58970250086968
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7272874302517692
Weighted FMeasure: 0.7973142989920116
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9137916140060315
Mean absolute error: 0.13298537032249289
Coverage of cases: 89.36
Instances selection time: 4.0
Test time: 133.0
Accumulative iteration time: 579.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09541421255003066
Kappa statistic: 0.7112581524643219
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 39.84000000000049
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8373090859366721
Weighted AreaUnderROC: 0.9517339799647321
Root mean squared error: 0.3329204358306878
Relative absolute error: 29.93624865190438
Root relative squared error: 70.6230893314379
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7267932075473205
Weighted FMeasure: 0.7967162211676415
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9135149545299417
Mean absolute error: 0.13304999400846454
Coverage of cases: 89.28
Instances selection time: 4.0
Test time: 129.0
Accumulative iteration time: 595.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09561229867443255
Kappa statistic: 0.7106617375727119
Training time: 12.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 39.85333333333383
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8361181048401133
Weighted AreaUnderROC: 0.9519405897027801
Root mean squared error: 0.3335462773756373
Relative absolute error: 29.98991587530015
Root relative squared error: 70.75585037155251
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.724591841470921
Weighted FMeasure: 0.7948874691606279
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9138605265025457
Mean absolute error: 0.13328851500133462
Coverage of cases: 89.32
Instances selection time: 4.0
Test time: 129.0
Accumulative iteration time: 611.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09640178493488655
Kappa statistic: 0.7082697557085679
Training time: 12.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 39.880000000000486
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8343944400368595
Weighted AreaUnderROC: 0.9526172967434462
Root mean squared error: 0.3340865887046226
Relative absolute error: 30.025757601672233
Root relative squared error: 70.87046771295574
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7213227262112825
Weighted FMeasure: 0.7920861223425749
Iteration time: 16.0
Weighted AreaUnderPRC: 0.91487225122712
Mean absolute error: 0.13344781156298832
Coverage of cases: 89.24
Instances selection time: 4.0
Test time: 130.0
Accumulative iteration time: 627.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09758672888485598
Kappa statistic: 0.7046824779548164
Training time: 12.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 39.893333333333814
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8351877338830367
Weighted AreaUnderROC: 0.9524014565503955
Root mean squared error: 0.33394792850201954
Relative absolute error: 30.012838965177924
Root relative squared error: 70.84105344209334
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7229128033988854
Weighted FMeasure: 0.7936762071240011
Iteration time: 16.0
Weighted AreaUnderPRC: 0.914554962790211
Mean absolute error: 0.1333903954007914
Coverage of cases: 89.2
Instances selection time: 4.0
Test time: 129.0
Accumulative iteration time: 643.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09699461418951548
Kappa statistic: 0.7064739154750308
Training time: 12.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 39.82666666666715
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8353892305472563
Weighted AreaUnderROC: 0.952187409419717
Root mean squared error: 0.33397547662178484
Relative absolute error: 30.01008332542874
Root relative squared error: 70.84689728078183
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7225866111816877
Weighted FMeasure: 0.7932281278093054
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9141314222096953
Mean absolute error: 0.13337814811301726
Coverage of cases: 89.08
Instances selection time: 4.0
Test time: 129.0
Accumulative iteration time: 660.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09718619151691664
Kappa statistic: 0.7058774197722256
Training time: 13.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 39.77333333333383
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8336201346204667
Weighted AreaUnderROC: 0.9526448065633918
Root mean squared error: 0.3340424954532202
Relative absolute error: 30.015885513492957
Root relative squared error: 70.86111412183438
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7192857410104367
Weighted FMeasure: 0.7905483121921799
Iteration time: 16.0
Weighted AreaUnderPRC: 0.9148684034169137
Mean absolute error: 0.13340393561552488
Coverage of cases: 89.0
Instances selection time: 4.0
Test time: 140.0
Accumulative iteration time: 676.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09837113546688606
Kappa statistic: 0.7022888179440414
Training time: 12.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 39.6800000000005
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.83474699880187
Weighted AreaUnderROC: 0.9529789657779567
Root mean squared error: 0.3344693778173043
Relative absolute error: 30.024589928388494
Root relative squared error: 70.95166954615821
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7206459654491034
Weighted FMeasure: 0.7911586353085609
Iteration time: 17.0
Weighted AreaUnderPRC: 0.9152967473653282
Mean absolute error: 0.13344262190394948
Coverage of cases: 88.92
Instances selection time: 3.0
Test time: 129.0
Accumulative iteration time: 693.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09796773986179308
Kappa statistic: 0.7034898347929222
Training time: 14.0
		
Time end:Wed Nov 01 17.09.52 EET 2017