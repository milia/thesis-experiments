Wed Nov 01 16.33.10 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.33.10 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.32
Correctly Classified Instances: 69.68
Weighted Precision: 0.697031147139408
Weighted AreaUnderROC: 0.7727027189321356
Root mean squared error: 0.4482584874713087
Relative absolute error: 45.805168986083295
Root relative squared error: 95.08998486461601
Weighted TruePositiveRate: 0.6968
Weighted MatthewsCorrelation: 0.5459141772676834
Weighted FMeasure: 0.6942765756143302
Iteration time: 746.0
Weighted AreaUnderPRC: 0.587058746947305
Mean absolute error: 0.2035785288270378
Coverage of cases: 69.68
Instances selection time: 746.0
Test time: 936.0
Accumulative iteration time: 746.0
Weighted Recall: 0.6968
Weighted FalsePositiveRate: 0.151394562135729
Kappa statistic: 0.5454436422077765
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.04
Correctly Classified Instances: 68.96
Weighted Precision: 0.689821042765898
Weighted AreaUnderROC: 0.7671218994287472
Root mean squared error: 0.45360075736746314
Relative absolute error: 46.86653919693907
Root relative squared error: 96.22325144576587
Weighted TruePositiveRate: 0.6896
Weighted MatthewsCorrelation: 0.5344557630601281
Weighted FMeasure: 0.6890787415119146
Iteration time: 773.0
Weighted AreaUnderPRC: 0.5798581347695215
Mean absolute error: 0.20829572976417463
Coverage of cases: 68.96
Instances selection time: 773.0
Test time: 974.0
Accumulative iteration time: 1519.0
Weighted Recall: 0.6896
Weighted FalsePositiveRate: 0.15535620114250545
Kappa statistic: 0.5344350104872113
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.6946117287502026
Weighted AreaUnderROC: 0.76941272077696
Root mean squared error: 0.45130399003372057
Relative absolute error: 46.37790055248665
Root relative squared error: 95.73603351881674
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.5397978152487831
Weighted FMeasure: 0.6929485748277074
Iteration time: 794.0
Weighted AreaUnderPRC: 0.5836029076254025
Mean absolute error: 0.2061240024554972
Coverage of cases: 69.28
Instances selection time: 794.0
Test time: 1008.0
Accumulative iteration time: 2313.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.15397455844608007
Kappa statistic: 0.5390948398786667
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.6968914832297156
Weighted AreaUnderROC: 0.7702520242315469
Root mean squared error: 0.4504656213491997
Relative absolute error: 46.18827708703304
Root relative squared error: 95.55818866422901
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5420537487753713
Weighted FMeasure: 0.6943632657240709
Iteration time: 811.0
Weighted AreaUnderPRC: 0.5851514331293983
Mean absolute error: 0.2052812314979256
Coverage of cases: 69.4
Instances selection time: 811.0
Test time: 1041.0
Accumulative iteration time: 3124.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.1534959515369059
Kappa statistic: 0.5408211773993263
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.6971654658069326
Weighted AreaUnderROC: 0.7692905605159276
Root mean squared error: 0.4513889951314588
Relative absolute error: 46.35746140651666
Root relative squared error: 95.75406582313057
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.5409682409848546
Weighted FMeasure: 0.6932169133743167
Iteration time: 828.0
Weighted AreaUnderPRC: 0.5842596125348934
Mean absolute error: 0.2060331618067417
Coverage of cases: 69.28
Instances selection time: 828.0
Test time: 1075.0
Accumulative iteration time: 3952.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.15421887896814473
Kappa statistic: 0.5389471955262509
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.92
Correctly Classified Instances: 69.08
Weighted Precision: 0.6959779917383945
Weighted AreaUnderROC: 0.7677617293809865
Root mean squared error: 0.45289435277494416
Relative absolute error: 46.6467661691547
Root relative squared error: 96.07340040247644
Weighted TruePositiveRate: 0.6908
Weighted MatthewsCorrelation: 0.5383853363664266
Weighted FMeasure: 0.6911416283239114
Iteration time: 845.0
Weighted AreaUnderPRC: 0.5823485304338676
Mean absolute error: 0.20731896075179965
Coverage of cases: 69.08
Instances selection time: 845.0
Test time: 1105.0
Accumulative iteration time: 4797.0
Weighted Recall: 0.6908
Weighted FalsePositiveRate: 0.155276541238027
Kappa statistic: 0.5359127857048169
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.6952760361302475
Weighted AreaUnderROC: 0.7662220632116117
Root mean squared error: 0.4543927625683352
Relative absolute error: 46.93675762439932
Root relative squared error: 96.39126112024738
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.536071656096433
Weighted FMeasure: 0.6889543601708381
Iteration time: 860.0
Weighted AreaUnderPRC: 0.5805436656848518
Mean absolute error: 0.20860781166399797
Coverage of cases: 68.88
Instances selection time: 860.0
Test time: 1138.0
Accumulative iteration time: 5657.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.15635587357677674
Kappa statistic: 0.5328642602010916
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6977156059019591
Weighted AreaUnderROC: 0.7670790677772774
Root mean squared error: 0.4535496376931624
Relative absolute error: 46.74961119751325
Root relative squared error: 96.21240732526084
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.5385871237115433
Weighted FMeasure: 0.6899171533503673
Iteration time: 876.0
Weighted AreaUnderPRC: 0.58205010846205
Mean absolute error: 0.2077760497667265
Coverage of cases: 69.0
Instances selection time: 876.0
Test time: 1170.0
Accumulative iteration time: 6533.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.1558418644454452
Kappa statistic: 0.5346107856922901
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.6997432086720867
Weighted AreaUnderROC: 0.7679555987339454
Root mean squared error: 0.4527026542880854
Relative absolute error: 46.56289592760257
Root relative squared error: 96.03273501247612
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.5408753724337384
Weighted FMeasure: 0.6909714911332252
Iteration time: 891.0
Weighted AreaUnderPRC: 0.5834704045528455
Mean absolute error: 0.20694620412267906
Coverage of cases: 69.12
Instances selection time: 891.0
Test time: 1201.0
Accumulative iteration time: 7424.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.1552888025321092
Kappa statistic: 0.5363808883230441
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.76
Correctly Classified Instances: 69.24
Weighted Precision: 0.7017274473810661
Weighted AreaUnderROC: 0.7688353840891137
Root mean squared error: 0.4518519911971053
Relative absolute error: 46.376573938506866
Root relative squared error: 95.85228212043498
Weighted TruePositiveRate: 0.6924
Weighted MatthewsCorrelation: 0.5431385884423452
Weighted FMeasure: 0.6920661898317092
Iteration time: 903.0
Weighted AreaUnderPRC: 0.5848926684443043
Mean absolute error: 0.2061181063933648
Coverage of cases: 69.24
Instances selection time: 903.0
Test time: 1235.0
Accumulative iteration time: 8327.0
Weighted Recall: 0.6924
Weighted FalsePositiveRate: 0.15472923182177242
Kappa statistic: 0.5381556688058448
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.6999380296015011
Weighted AreaUnderROC: 0.7663967634532126
Root mean squared error: 0.45422443389203737
Relative absolute error: 46.847795163581736
Root relative squared error: 96.35555321570386
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5391236073480142
Weighted FMeasure: 0.6886788884238
Iteration time: 916.0
Weighted AreaUnderPRC: 0.5818850760989497
Mean absolute error: 0.20821242294925313
Coverage of cases: 68.92
Instances selection time: 916.0
Test time: 1266.0
Accumulative iteration time: 9243.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.156406473093575
Kappa statistic: 0.5333084431169663
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.28
Correctly Classified Instances: 68.72
Weighted Precision: 0.6995005946656981
Weighted AreaUnderROC: 0.7648556292835583
Root mean squared error: 0.45571029817949094
Relative absolute error: 47.14024896265731
Root relative squared error: 96.67075262977826
Weighted TruePositiveRate: 0.6872
Weighted MatthewsCorrelation: 0.5370390792578459
Weighted FMeasure: 0.686470349838817
Iteration time: 950.0
Weighted AreaUnderPRC: 0.580168717632275
Mean absolute error: 0.20951221761181124
Coverage of cases: 68.72
Instances selection time: 950.0
Test time: 1312.0
Accumulative iteration time: 10193.0
Weighted Recall: 0.6872
Weighted FalsePositiveRate: 0.15748874143288344
Kappa statistic: 0.5302572028029829
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.6989571508753057
Weighted AreaUnderROC: 0.7633231864704725
Root mean squared error: 0.45719023833495165
Relative absolute error: 47.43310901749556
Root relative squared error: 96.98469534568122
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5348688479274434
Weighted FMeasure: 0.6842513896734221
Iteration time: 941.0
Weighted AreaUnderPRC: 0.5784223267221357
Mean absolute error: 0.2108138178555368
Coverage of cases: 68.52
Instances selection time: 941.0
Test time: 1334.0
Accumulative iteration time: 11134.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15855362705905487
Kappa statistic: 0.5272166988744034
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.52
Correctly Classified Instances: 68.48
Weighted Precision: 0.6997712786232634
Weighted AreaUnderROC: 0.762997036381336
Root mean squared error: 0.4575047064698714
Relative absolute error: 47.48728702489915
Root relative squared error: 97.05140411088188
Weighted TruePositiveRate: 0.6848
Weighted MatthewsCorrelation: 0.5350065948776251
Weighted FMeasure: 0.6835840157247627
Iteration time: 957.0
Weighted AreaUnderPRC: 0.578265077323539
Mean absolute error: 0.21105460899955278
Coverage of cases: 68.48
Instances selection time: 957.0
Test time: 1368.0
Accumulative iteration time: 12091.0
Weighted Recall: 0.6848
Weighted FalsePositiveRate: 0.15880592723732798
Kappa statistic: 0.5265834222491372
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.44
Correctly Classified Instances: 68.56
Weighted Precision: 0.7028251253705039
Weighted AreaUnderROC: 0.7635513862066562
Root mean squared error: 0.45694660195149256
Relative absolute error: 47.36245210727888
Root relative squared error: 96.9330122640149
Weighted TruePositiveRate: 0.6856
Weighted MatthewsCorrelation: 0.5376112535547635
Weighted FMeasure: 0.6839936286204478
Iteration time: 968.0
Weighted AreaUnderPRC: 0.5796107017919228
Mean absolute error: 0.21049978714346265
Coverage of cases: 68.56
Instances selection time: 968.0
Test time: 1398.0
Accumulative iteration time: 13059.0
Weighted Recall: 0.6856
Weighted FalsePositiveRate: 0.1584972275866875
Kappa statistic: 0.5277282406494997
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.44
Correctly Classified Instances: 68.56
Weighted Precision: 0.7034202451365289
Weighted AreaUnderROC: 0.7635409084518665
Root mean squared error: 0.45696831201062915
Relative absolute error: 47.35740971357525
Root relative squared error: 96.93761766302555
Weighted TruePositiveRate: 0.6856
Weighted MatthewsCorrelation: 0.5379704150882247
Weighted FMeasure: 0.6839149747583216
Iteration time: 976.0
Weighted AreaUnderPRC: 0.5797382992701281
Mean absolute error: 0.21047737650477988
Coverage of cases: 68.56
Instances selection time: 976.0
Test time: 1428.0
Accumulative iteration time: 14035.0
Weighted Recall: 0.6856
Weighted FalsePositiveRate: 0.15851818309626658
Kappa statistic: 0.5277155275597819
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.64
Correctly Classified Instances: 68.36
Weighted Precision: 0.7030303824267626
Weighted AreaUnderROC: 0.7620088229184252
Root mean squared error: 0.4584401725622966
Relative absolute error: 47.65151883353651
Root relative squared error: 97.24984643613905
Weighted TruePositiveRate: 0.6836
Weighted MatthewsCorrelation: 0.5359484712135048
Weighted FMeasure: 0.6815089258945725
Iteration time: 985.0
Weighted AreaUnderPRC: 0.5780109071738413
Mean absolute error: 0.21178452814905213
Coverage of cases: 68.36
Instances selection time: 985.0
Test time: 1463.0
Accumulative iteration time: 15020.0
Weighted Recall: 0.6836
Weighted FalsePositiveRate: 0.15958235416314961
Kappa statistic: 0.5246734729098733
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.92
Correctly Classified Instances: 68.08
Weighted Precision: 0.7019151583573157
Weighted AreaUnderROC: 0.7598757541417255
Root mean squared error: 0.460484005102466
Relative absolute error: 48.065480427046914
Root relative squared error: 97.68340879076811
Weighted TruePositiveRate: 0.6808
Weighted MatthewsCorrelation: 0.5327535768090654
Weighted FMeasure: 0.6784222680957002
Iteration time: 994.0
Weighted AreaUnderPRC: 0.5754948257512882
Mean absolute error: 0.21362435745354283
Coverage of cases: 68.08
Instances selection time: 994.0
Test time: 1495.0
Accumulative iteration time: 16014.0
Weighted Recall: 0.6808
Weighted FalsePositiveRate: 0.16104849171654878
Kappa statistic: 0.5204301561210178
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.08
Correctly Classified Instances: 67.92
Weighted Precision: 0.7018188431723316
Weighted AreaUnderROC: 0.7586466806282739
Root mean squared error: 0.4616556064778059
Relative absolute error: 48.300347624566804
Root relative squared error: 97.93194297397321
Weighted TruePositiveRate: 0.6792
Weighted MatthewsCorrelation: 0.5313065242545827
Weighted FMeasure: 0.6764436112231377
Iteration time: 1005.0
Weighted AreaUnderPRC: 0.5741751547885018
Mean absolute error: 0.21466821166474237
Coverage of cases: 67.92
Instances selection time: 1004.0
Test time: 1526.0
Accumulative iteration time: 17019.0
Weighted Recall: 0.6792
Weighted FalsePositiveRate: 0.1619066387434523
Kappa statistic: 0.5179913050920313
Training time: 1.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.7009406835600827
Weighted AreaUnderROC: 0.756819163710776
Root mean squared error: 0.4633974341338855
Relative absolute error: 48.65503963760011
Root relative squared error: 98.30144041815485
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.528614687696276
Weighted FMeasure: 0.6738316112078825
Iteration time: 1011.0
Weighted AreaUnderPRC: 0.5720429626533835
Mean absolute error: 0.21624462061155708
Coverage of cases: 67.68
Instances selection time: 1011.0
Test time: 1859.0
Accumulative iteration time: 18030.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16316167257844807
Kappa statistic: 0.514354899822475
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.6992798511052526
Weighted AreaUnderROC: 0.7546965726888659
Root mean squared error: 0.4654178213582961
Relative absolute error: 49.06976744186055
Root relative squared error: 98.73002927025588
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.525093427581569
Weighted FMeasure: 0.670821754756068
Iteration time: 1048.0
Weighted AreaUnderPRC: 0.569437400211625
Mean absolute error: 0.21808785529715902
Coverage of cases: 67.4
Instances selection time: 1048.0
Test time: 1673.0
Accumulative iteration time: 19078.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.1646068546222682
Kappa statistic: 0.5101233166875441
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.36
Correctly Classified Instances: 67.64
Weighted Precision: 0.702701148941794
Weighted AreaUnderROC: 0.7564832504261383
Root mean squared error: 0.4637181139235686
Relative absolute error: 48.70725893824529
Root relative squared error: 98.36946687431717
Weighted TruePositiveRate: 0.6764
Weighted MatthewsCorrelation: 0.5294336210542473
Weighted FMeasure: 0.6728506911327632
Iteration time: 1054.0
Weighted AreaUnderPRC: 0.572077164880385
Mean absolute error: 0.21647670639220232
Coverage of cases: 67.64
Instances selection time: 1054.0
Test time: 1809.0
Accumulative iteration time: 20132.0
Weighted Recall: 0.6764
Weighted FalsePositiveRate: 0.16343349914772345
Kappa statistic: 0.5137075561069675
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.7063698264440911
Weighted AreaUnderROC: 0.7567367707910365
Root mean squared error: 0.463447347448779
Relative absolute error: 48.643902439024075
Root relative squared error: 98.31202863118467
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5321420709747301
Weighted FMeasure: 0.6725029610324158
Iteration time: 1031.0
Weighted AreaUnderPRC: 0.5731319254213036
Mean absolute error: 0.21619512195121915
Coverage of cases: 67.68
Instances selection time: 1031.0
Test time: 1650.0
Accumulative iteration time: 21163.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16332645841792695
Kappa statistic: 0.5142488604085889
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.707606092977925
Weighted AreaUnderROC: 0.756719426959602
Root mean squared error: 0.46346260645687765
Relative absolute error: 48.64049844236726
Root relative squared error: 98.31526555561487
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5329480919991886
Weighted FMeasure: 0.6721724114547903
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5733760283689964
Mean absolute error: 0.21617999307718883
Coverage of cases: 67.68
Instances selection time: 1037.0
Test time: 1681.0
Accumulative iteration time: 22200.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16336114608079594
Kappa statistic: 0.5142264320460493
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7088015506651011
Weighted AreaUnderROC: 0.7573142586855036
Root mean squared error: 0.46290328572781175
Relative absolute error: 48.517599186163814
Root relative squared error: 98.19661571150068
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5344580062536098
Weighted FMeasure: 0.6728349670642619
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5742766559654857
Mean absolute error: 0.21563377416072907
Coverage of cases: 67.76
Instances selection time: 1041.0
Test time: 1715.0
Accumulative iteration time: 23241.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16297148262899283
Kappa statistic: 0.5154204515175155
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.36
Correctly Classified Instances: 67.64
Weighted Precision: 0.7079634104787528
Weighted AreaUnderROC: 0.7564073663033997
Root mean squared error: 0.4637780257620449
Relative absolute error: 48.69391824526491
Root relative squared error: 98.38217609449515
Weighted TruePositiveRate: 0.6764
Weighted MatthewsCorrelation: 0.5328640436133257
Weighted FMeasure: 0.6715816600326235
Iteration time: 1211.0
Weighted AreaUnderPRC: 0.5731239444631986
Mean absolute error: 0.21641741442340062
Coverage of cases: 67.64
Instances selection time: 1211.0
Test time: 1743.0
Accumulative iteration time: 24452.0
Weighted Recall: 0.6764
Weighted FalsePositiveRate: 0.16358526739320084
Kappa statistic: 0.513610255236175
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.64
Correctly Classified Instances: 67.36
Weighted Precision: 0.7059958171105117
Weighted AreaUnderROC: 0.7542912840784903
Root mean squared error: 0.4657937181974786
Relative absolute error: 49.10967741935409
Root relative squared error: 98.80976903145964
Weighted TruePositiveRate: 0.6736
Weighted MatthewsCorrelation: 0.5291397884839746
Weighted FMeasure: 0.6686080786714838
Iteration time: 1048.0
Weighted AreaUnderPRC: 0.5704370876147443
Mean absolute error: 0.21826523297490807
Coverage of cases: 67.36
Instances selection time: 1048.0
Test time: 1777.0
Accumulative iteration time: 25500.0
Weighted Recall: 0.6736
Weighted FalsePositiveRate: 0.16501743184301945
Kappa statistic: 0.5093858809914446
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.8
Correctly Classified Instances: 67.2
Weighted Precision: 0.7062950163933845
Weighted AreaUnderROC: 0.7530622105650385
Root mean squared error: 0.4669470529079656
Relative absolute error: 49.34611697027933
Root relative squared error: 99.05442826988859
Weighted TruePositiveRate: 0.672
Weighted MatthewsCorrelation: 0.5279884362128919
Weighted FMeasure: 0.6666022402101247
Iteration time: 1052.0
Weighted AreaUnderPRC: 0.5691979189295325
Mean absolute error: 0.21931607542346473
Coverage of cases: 67.2
Instances selection time: 1052.0
Test time: 1987.0
Accumulative iteration time: 26552.0
Weighted Recall: 0.672
Weighted FalsePositiveRate: 0.165875578869923
Kappa statistic: 0.5069462850446949
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.68
Correctly Classified Instances: 67.32
Weighted Precision: 0.7080468067867001
Weighted AreaUnderROC: 0.7539560853531412
Root mean squared error: 0.46610468033814756
Relative absolute error: 49.163875823143464
Root relative squared error: 98.87573406296742
Weighted TruePositiveRate: 0.6732
Weighted MatthewsCorrelation: 0.5302239819582841
Weighted FMeasure: 0.6676064338589541
Iteration time: 1055.0
Weighted AreaUnderPRC: 0.5705309442007942
Mean absolute error: 0.21850611476952753
Coverage of cases: 67.32
Instances selection time: 1055.0
Test time: 1855.0
Accumulative iteration time: 27607.0
Weighted Recall: 0.6732
Weighted FalsePositiveRate: 0.165287829293718
Kappa statistic: 0.5087395061467092
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.92
Correctly Classified Instances: 67.08
Weighted Precision: 0.7066210513582637
Weighted AreaUnderROC: 0.7521390461904326
Root mean squared error: 0.467825214105623
Relative absolute error: 49.520221606649585
Root relative squared error: 99.24071439124013
Weighted TruePositiveRate: 0.6708
Weighted MatthewsCorrelation: 0.5272156090707113
Weighted FMeasure: 0.664991486509538
Iteration time: 1071.0
Weighted AreaUnderPRC: 0.5682880107443212
Mean absolute error: 0.22008987380733253
Coverage of cases: 67.08
Instances selection time: 1071.0
Test time: 1884.0
Accumulative iteration time: 28678.0
Weighted Recall: 0.6708
Weighted FalsePositiveRate: 0.16652190761913468
Kappa statistic: 0.5051138634984974
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.04
Correctly Classified Instances: 66.96
Weighted Precision: 0.7068384992129302
Weighted AreaUnderROC: 0.7512184216550387
Root mean squared error: 0.4686888276868943
Relative absolute error: 49.69718948322661
Root relative squared error: 99.42391449713264
Weighted TruePositiveRate: 0.6696
Weighted MatthewsCorrelation: 0.5263367214754553
Weighted FMeasure: 0.6633785834868771
Iteration time: 1062.0
Weighted AreaUnderPRC: 0.5673471236779168
Mean absolute error: 0.2208763977032304
Coverage of cases: 66.96
Instances selection time: 1062.0
Test time: 1912.0
Accumulative iteration time: 29740.0
Weighted Recall: 0.6696
Weighted FalsePositiveRate: 0.1671631566899225
Kappa statistic: 0.5032845758435142
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.96
Correctly Classified Instances: 67.04
Weighted Precision: 0.7078708314317008
Weighted AreaUnderROC: 0.7518165077794406
Root mean squared error: 0.4681323745527437
Relative absolute error: 49.57506678539598
Root relative squared error: 99.30587296176152
Weighted TruePositiveRate: 0.6704
Weighted MatthewsCorrelation: 0.527733100351141
Weighted FMeasure: 0.664084162444609
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5682074665193118
Mean absolute error: 0.22033363015731652
Coverage of cases: 67.04
Instances selection time: 1060.0
Test time: 1942.0
Accumulative iteration time: 30800.0
Weighted Recall: 0.6704
Weighted FalsePositiveRate: 0.16676698444111868
Kappa statistic: 0.5044829857257425
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.04
Correctly Classified Instances: 66.96
Weighted Precision: 0.7082275251847091
Weighted AreaUnderROC: 0.7512010778236042
Root mean squared error: 0.46871107283687546
Relative absolute error: 49.69238845144298
Root relative squared error: 99.4286334060527
Weighted TruePositiveRate: 0.6696
Weighted MatthewsCorrelation: 0.5272642264562662
Weighted FMeasure: 0.6630084373461177
Iteration time: 1061.0
Weighted AreaUnderPRC: 0.5676126180168019
Mean absolute error: 0.22085505978419206
Coverage of cases: 66.96
Instances selection time: 1061.0
Test time: 1972.0
Accumulative iteration time: 31861.0
Weighted Recall: 0.6696
Weighted FalsePositiveRate: 0.16719784435279147
Kappa statistic: 0.5032618736048032
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.2
Correctly Classified Instances: 66.8
Weighted Precision: 0.7073809580553975
Weighted AreaUnderROC: 0.7499886335822985
Root mean squared error: 0.46985516360860075
Relative absolute error: 49.929492691314884
Root relative squared error: 99.67133170894667
Weighted TruePositiveRate: 0.668
Weighted MatthewsCorrelation: 0.525334194951219
Weighted FMeasure: 0.6612269058997987
Iteration time: 1062.0
Weighted AreaUnderPRC: 0.5661418633508792
Mean absolute error: 0.22190885640584498
Coverage of cases: 66.8
Instances selection time: 1062.0
Test time: 2007.0
Accumulative iteration time: 32923.0
Weighted Recall: 0.668
Weighted FalsePositiveRate: 0.16802273283540284
Kappa statistic: 0.5008429139419507
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.24
Correctly Classified Instances: 66.76
Weighted Precision: 0.708135193458534
Weighted AreaUnderROC: 0.7496733185275958
Root mean squared error: 0.47014834677710055
Relative absolute error: 49.987151310227176
Root relative squared error: 99.73352525091946
Weighted TruePositiveRate: 0.6676
Weighted MatthewsCorrelation: 0.5255336188153225
Weighted FMeasure: 0.6605378057564488
Iteration time: 1062.0
Weighted AreaUnderPRC: 0.5659686105363884
Mean absolute error: 0.22216511693434404
Coverage of cases: 66.76
Instances selection time: 1062.0
Test time: 2038.0
Accumulative iteration time: 33985.0
Weighted Recall: 0.6676
Weighted FalsePositiveRate: 0.1682533629448085
Kappa statistic: 0.5002220433472878
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.7073145388490895
Weighted AreaUnderROC: 0.7484608742862902
Root mean squared error: 0.47128841505660923
Relative absolute error: 50.22443890274318
Root relative squared error: 99.97537025435633
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.5236204301424834
Weighted FMeasure: 0.6587655873576352
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5645052302777214
Mean absolute error: 0.2232197284566374
Coverage of cases: 66.6
Instances selection time: 1060.0
Test time: 2070.0
Accumulative iteration time: 35045.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.1690782514274199
Kappa statistic: 0.4978030839581827
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.12
Correctly Classified Instances: 66.88
Weighted Precision: 0.708198099810236
Weighted AreaUnderROC: 0.7505895779438542
Root mean squared error: 0.4693183548199226
Relative absolute error: 49.8034341782484
Root relative squared error: 99.5574573685442
Weighted TruePositiveRate: 0.6688
Weighted MatthewsCorrelation: 0.5268355434717039
Weighted FMeasure: 0.6618223318890465
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5668359089098738
Mean absolute error: 0.2213485963477717
Coverage of cases: 66.88
Instances selection time: 1060.0
Test time: 2102.0
Accumulative iteration time: 36105.0
Weighted Recall: 0.6688
Weighted FalsePositiveRate: 0.1676208441122917
Kappa statistic: 0.5020401918516455
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.24
Correctly Classified Instances: 66.76
Weighted Precision: 0.7043434888394592
Weighted AreaUnderROC: 0.7497295984959271
Root mean squared error: 0.4701770510997915
Relative absolute error: 49.98101367658873
Root relative squared error: 99.73961435728671
Weighted TruePositiveRate: 0.6676
Weighted MatthewsCorrelation: 0.52361891805413
Weighted FMeasure: 0.6603965542521242
Iteration time: 1262.0
Weighted AreaUnderPRC: 0.5647049538147916
Mean absolute error: 0.22213783856261762
Coverage of cases: 66.76
Instances selection time: 1262.0
Test time: 2140.0
Accumulative iteration time: 37367.0
Weighted Recall: 0.6676
Weighted FalsePositiveRate: 0.16814080300814566
Kappa statistic: 0.5002718136464883
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.16
Correctly Classified Instances: 66.84
Weighted Precision: 0.7026528240082928
Weighted AreaUnderROC: 0.7503753509954971
Root mean squared error: 0.46961986912992626
Relative absolute error: 49.859382422802284
Root relative squared error: 99.62141821251272
Weighted TruePositiveRate: 0.6684
Weighted MatthewsCorrelation: 0.5232685962612305
Weighted FMeasure: 0.661186005062613
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5647510541441132
Mean absolute error: 0.22159725521245563
Coverage of cases: 66.84
Instances selection time: 1060.0
Test time: 2163.0
Accumulative iteration time: 38427.0
Weighted Recall: 0.6684
Weighted FalsePositiveRate: 0.1676492980090059
Kappa statistic: 0.5015210522872547
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.68
Correctly Classified Instances: 67.32
Weighted Precision: 0.7056139299931156
Weighted AreaUnderROC: 0.7540101438802022
Root mean squared error: 0.4662171711195959
Relative absolute error: 49.13920498830799
Root relative squared error: 98.89959696128236
Weighted TruePositiveRate: 0.6732
Weighted MatthewsCorrelation: 0.5293683400022383
Weighted FMeasure: 0.6661788424794332
Iteration time: 1069.0
Weighted AreaUnderPRC: 0.5691834303902024
Mean absolute error: 0.2183964666147032
Coverage of cases: 67.32
Instances selection time: 1069.0
Test time: 2220.0
Accumulative iteration time: 39496.0
Weighted Recall: 0.6732
Weighted FalsePositiveRate: 0.16517971223959566
Kappa statistic: 0.5087721154761916
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.44
Correctly Classified Instances: 67.56
Weighted Precision: 0.7077116168488925
Weighted AreaUnderROC: 0.7558419481534305
Root mean squared error: 0.4645104054773023
Relative absolute error: 48.778204144281844
Root relative squared error: 98.53753729341375
Weighted TruePositiveRate: 0.6756
Weighted MatthewsCorrelation: 0.5325268841518568
Weighted FMeasure: 0.6682600868567156
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5714359514042904
Mean absolute error: 0.21679201841903142
Coverage of cases: 67.56
Instances selection time: 1060.0
Test time: 2234.0
Accumulative iteration time: 40556.0
Weighted Recall: 0.6756
Weighted FalsePositiveRate: 0.16391610369313894
Kappa statistic: 0.5124127333023906
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.7068345671459328
Weighted AreaUnderROC: 0.7567849573169803
Root mean squared error: 0.4636585195628976
Relative absolute error: 48.59682539682666
Root relative squared error: 98.3568250013519
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5328292755467327
Weighted FMeasure: 0.6698349705859624
Iteration time: 1054.0
Weighted AreaUnderPRC: 0.5721947388419659
Mean absolute error: 0.21598589065256396
Coverage of cases: 67.68
Instances selection time: 1054.0
Test time: 2267.0
Accumulative iteration time: 41610.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16323008536603933
Kappa statistic: 0.5142680163282893
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.7059578168495273
Weighted AreaUnderROC: 0.7561955236674442
Root mean squared error: 0.46423981626062494
Relative absolute error: 48.71481757259789
Root relative squared error: 98.48013665240519
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5313937166246014
Weighted FMeasure: 0.669180283246106
Iteration time: 1050.0
Weighted AreaUnderPRC: 0.5713754908113362
Mean absolute error: 0.2165103003226583
Coverage of cases: 67.6
Instances selection time: 1050.0
Test time: 2292.0
Accumulative iteration time: 42660.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.16360895266511122
Kappa statistic: 0.513080462191239
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.7070958796632192
Weighted AreaUnderROC: 0.7583527634705209
Root mean squared error: 0.462237056729468
Relative absolute error: 48.294057226706485
Root relative squared error: 98.05528719873507
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5341815031322149
Weighted FMeasure: 0.6721580265172956
Iteration time: 1047.0
Weighted AreaUnderPRC: 0.5738527512602667
Mean absolute error: 0.21464025434091874
Coverage of cases: 67.88
Instances selection time: 1047.0
Test time: 2323.0
Accumulative iteration time: 43707.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.1620944730589582
Kappa statistic: 0.5173575766085156
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.28
Correctly Classified Instances: 67.72
Weighted Precision: 0.7052884896605246
Weighted AreaUnderROC: 0.7571731816121606
Root mean squared error: 0.4633942537067905
Relative absolute error: 48.531887201735124
Root relative squared error: 98.30076574768508
Weighted TruePositiveRate: 0.6772
Weighted MatthewsCorrelation: 0.531396258316039
Weighted FMeasure: 0.6706216343127699
Iteration time: 1044.0
Weighted AreaUnderPRC: 0.5721424926808608
Mean absolute error: 0.2156972764521571
Coverage of cases: 67.72
Instances selection time: 1044.0
Test time: 2383.0
Accumulative iteration time: 44751.0
Weighted Recall: 0.6772
Weighted FalsePositiveRate: 0.16285363677567874
Kappa statistic: 0.5149802001334737
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.6
Correctly Classified Instances: 67.4
Weighted Precision: 0.701783504848267
Weighted AreaUnderROC: 0.754802468301718
Root mean squared error: 0.46569263567495883
Relative absolute error: 49.00926585887357
Root relative squared error: 98.78832619031968
Weighted TruePositiveRate: 0.674
Weighted MatthewsCorrelation: 0.526162564860472
Weighted FMeasure: 0.6671426265167519
Iteration time: 1043.0
Weighted AreaUnderPRC: 0.5686397931359222
Mean absolute error: 0.21781895937277246
Coverage of cases: 67.4
Instances selection time: 1043.0
Test time: 2397.0
Accumulative iteration time: 45794.0
Weighted Recall: 0.674
Weighted FalsePositiveRate: 0.16439506339656412
Kappa statistic: 0.5102048077094125
Training time: 0.0
		
Time end:Wed Nov 01 16.35.17 EET 2017