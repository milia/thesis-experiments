Wed Nov 01 16.18.29 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.18.29 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.68
Correctly Classified Instances: 71.32
Weighted Precision: 0.7138555501066097
Weighted AreaUnderROC: 0.7847686556154017
Root mean squared error: 0.4359673691966528
Relative absolute error: 43.35984095427418
Root relative squared error: 92.48264494050349
Weighted TruePositiveRate: 0.7132
Weighted MatthewsCorrelation: 0.5700028751983565
Weighted FMeasure: 0.7132177917163112
Iteration time: 766.0
Weighted AreaUnderPRC: 0.6045010552949539
Mean absolute error: 0.1927104042412195
Coverage of cases: 71.32
Instances selection time: 766.0
Test time: 954.0
Accumulative iteration time: 766.0
Weighted Recall: 0.7132
Weighted FalsePositiveRate: 0.14366268876919683
Kappa statistic: 0.569676617679821
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7129729558067672
Weighted AreaUnderROC: 0.7845104906151286
Root mean squared error: 0.4363206382317099
Relative absolute error: 43.40650095602182
Root relative squared error: 92.55758461958511
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5692254486371038
Weighted FMeasure: 0.7126584343694411
Iteration time: 773.0
Weighted AreaUnderPRC: 0.6039036044276459
Mean absolute error: 0.19291778202676457
Coverage of cases: 71.28
Instances selection time: 773.0
Test time: 982.0
Accumulative iteration time: 1539.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.1437790187697427
Kappa statistic: 0.5691143054530685
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.717332795943263
Weighted AreaUnderROC: 0.7878231412501884
Root mean squared error: 0.4330109650346583
Relative absolute error: 42.73812154696172
Root relative squared error: 91.85549691124115
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5758430703217688
Weighted FMeasure: 0.7169202181834047
Iteration time: 792.0
Weighted AreaUnderPRC: 0.6087323182816275
Mean absolute error: 0.18994720687538633
Coverage of cases: 71.72
Instances selection time: 792.0
Test time: 1015.0
Accumulative iteration time: 2331.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14155371749962298
Kappa statistic: 0.5757357833681706
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.717622000194326
Weighted AreaUnderROC: 0.7878358404462483
Root mean squared error: 0.4330531433685777
Relative absolute error: 42.72682060390695
Root relative squared error: 91.86444428702123
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5760838782548029
Weighted FMeasure: 0.7166379300146195
Iteration time: 807.0
Weighted AreaUnderPRC: 0.6087472058437117
Mean absolute error: 0.18989698046180956
Coverage of cases: 71.72
Instances selection time: 807.0
Test time: 1047.0
Accumulative iteration time: 3138.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14152831910750338
Kappa statistic: 0.575750243988615
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7206819655486837
Weighted AreaUnderROC: 0.7899526761121485
Root mean squared error: 0.43094315408830336
Relative absolute error: 42.29845626071943
Root relative squared error: 91.41684796852735
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5805232707100745
Weighted FMeasure: 0.7191459353808864
Iteration time: 823.0
Weighted AreaUnderPRC: 0.6118791703606037
Mean absolute error: 0.18799313893653172
Coverage of cases: 72.0
Instances selection time: 823.0
Test time: 1079.0
Accumulative iteration time: 3961.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.1400946477757029
Kappa statistic: 0.5799723117747921
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.96
Correctly Classified Instances: 72.04
Weighted Precision: 0.7214322836726839
Weighted AreaUnderROC: 0.7902868029985652
Root mean squared error: 0.4306717279202685
Relative absolute error: 42.2288557213935
Root relative squared error: 91.35926978332466
Weighted TruePositiveRate: 0.7204
Weighted MatthewsCorrelation: 0.5814612497791939
Weighted FMeasure: 0.7192006778716626
Iteration time: 839.0
Weighted AreaUnderPRC: 0.6123657135641452
Mean absolute error: 0.1876838032061942
Coverage of cases: 72.04
Instances selection time: 839.0
Test time: 1112.0
Accumulative iteration time: 4800.0
Weighted Recall: 0.7204
Weighted FalsePositiveRate: 0.13982639400286961
Kappa statistic: 0.5806075826149064
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.720944425774459
Weighted AreaUnderROC: 0.789697765510376
Root mean squared error: 0.431321603615731
Relative absolute error: 42.33900481541007
Root relative squared error: 91.49712923668163
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5804997322626112
Weighted FMeasure: 0.7182237139014059
Iteration time: 854.0
Weighted AreaUnderPRC: 0.611533050300708
Mean absolute error: 0.18817335473515676
Coverage of cases: 71.96
Instances selection time: 854.0
Test time: 1145.0
Accumulative iteration time: 5654.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14020446897924804
Kappa statistic: 0.5794193130651442
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7243372658238408
Weighted AreaUnderROC: 0.7921049528819086
Root mean squared error: 0.4288853331111583
Relative absolute error: 41.85256609642428
Root relative squared error: 90.98031821830519
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5854867261202921
Weighted FMeasure: 0.7212057650011866
Iteration time: 869.0
Weighted AreaUnderPRC: 0.6151144678198465
Mean absolute error: 0.1860114048729977
Coverage of cases: 72.28
Instances selection time: 869.0
Test time: 1175.0
Accumulative iteration time: 6523.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13859009423618263
Kappa statistic: 0.5842290707033765
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7244591610421769
Weighted AreaUnderROC: 0.7921078500007649
Root mean squared error: 0.42891531893413304
Relative absolute error: 41.84434389140351
Root relative squared error: 90.98667917193465
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.585580458916452
Weighted FMeasure: 0.7211340548805191
Iteration time: 882.0
Weighted AreaUnderPRC: 0.6151353980177858
Mean absolute error: 0.18597486173957203
Coverage of cases: 72.28
Instances selection time: 882.0
Test time: 1207.0
Accumulative iteration time: 7405.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.1385842999984703
Kappa statistic: 0.5842322635763465
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.96
Correctly Classified Instances: 72.04
Weighted Precision: 0.7228320419496471
Weighted AreaUnderROC: 0.7903219257044833
Root mean squared error: 0.4307964095687761
Relative absolute error: 42.19502196193285
Root relative squared error: 91.38571875506943
Weighted TruePositiveRate: 0.7204
Weighted MatthewsCorrelation: 0.5825100564476792
Weighted FMeasure: 0.7184578488319922
Iteration time: 896.0
Weighted AreaUnderPRC: 0.6126265997125503
Mean absolute error: 0.18753343094192468
Coverage of cases: 72.04
Instances selection time: 896.0
Test time: 1240.0
Accumulative iteration time: 8301.0
Weighted Recall: 0.7204
Weighted FalsePositiveRate: 0.13975614859103322
Kappa statistic: 0.5806488460224153
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7214883921210414
Weighted AreaUnderROC: 0.7888426251063363
Root mean squared error: 0.43236124042761437
Relative absolute error: 42.48648648648433
Root relative squared error: 91.7176695085778
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5799964333647495
Weighted FMeasure: 0.7161723516840438
Iteration time: 908.0
Weighted AreaUnderPRC: 0.6105501401929108
Mean absolute error: 0.18882882882882016
Coverage of cases: 71.84
Instances selection time: 907.0
Test time: 1270.0
Accumulative iteration time: 9209.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14071474978732745
Kappa statistic: 0.5776720998191189
Training time: 1.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.32
Correctly Classified Instances: 71.68
Weighted Precision: 0.7202444841351249
Weighted AreaUnderROC: 0.7876529616545331
Root mean squared error: 0.4336132188767804
Relative absolute error: 42.718672199171664
Root relative squared error: 91.98325424396921
Weighted TruePositiveRate: 0.7168
Weighted MatthewsCorrelation: 0.5778512774193918
Weighted FMeasure: 0.7144417963949113
Iteration time: 920.0
Weighted AreaUnderPRC: 0.6088610192559486
Mean absolute error: 0.18986076532965274
Coverage of cases: 71.68
Instances selection time: 920.0
Test time: 1303.0
Accumulative iteration time: 10129.0
Weighted Recall: 0.7168
Weighted FalsePositiveRate: 0.1414940766909336
Kappa statistic: 0.5752831086013089
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.7217366613848297
Weighted AreaUnderROC: 0.7885555277992041
Root mean squared error: 0.43271761932630903
Relative absolute error: 42.532974427993324
Root relative squared error: 91.79326888935944
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.5798820232602193
Weighted FMeasure: 0.7154587479827019
Iteration time: 931.0
Weighted AreaUnderPRC: 0.6102320298437357
Mean absolute error: 0.18903544190219343
Coverage of cases: 71.8
Instances selection time: 931.0
Test time: 1336.0
Accumulative iteration time: 11060.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.1408889444015916
Kappa statistic: 0.5770853949170582
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7188419305019305
Weighted AreaUnderROC: 0.7855669224571182
Root mean squared error: 0.4357986114585551
Relative absolute error: 43.12450851900203
Root relative squared error: 92.44684601820751
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.5746174519356808
Weighted FMeasure: 0.7111305795558166
Iteration time: 941.0
Weighted AreaUnderPRC: 0.6061137720463321
Mean absolute error: 0.1916644823066766
Coverage of cases: 71.4
Instances selection time: 941.0
Test time: 1367.0
Accumulative iteration time: 12001.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.14286615508576367
Kappa statistic: 0.5711092470525908
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.76
Correctly Classified Instances: 71.24
Weighted Precision: 0.7176813100845498
Weighted AreaUnderROC: 0.7843772590053151
Root mean squared error: 0.4370377386792896
Relative absolute error: 43.357854406129334
Root relative squared error: 92.70970459636777
Weighted TruePositiveRate: 0.7124
Weighted MatthewsCorrelation: 0.5725364540296021
Weighted FMeasure: 0.709359576191866
Iteration time: 972.0
Weighted AreaUnderPRC: 0.6044311860727095
Mean absolute error: 0.1927015751383535
Coverage of cases: 71.24
Instances selection time: 972.0
Test time: 1413.0
Accumulative iteration time: 12973.0
Weighted Recall: 0.7124
Weighted FalsePositiveRate: 0.14364548198936983
Kappa statistic: 0.568714411440933
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7152827082134138
Weighted AreaUnderROC: 0.78168769672543
Root mean squared error: 0.4397853508562514
Relative absolute error: 43.89041095890507
Root relative squared error: 93.2925611570879
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.567950160532177
Weighted FMeasure: 0.7053714127994947
Iteration time: 961.0
Weighted AreaUnderPRC: 0.600769204568214
Mean absolute error: 0.1950684931506901
Coverage of cases: 70.88
Instances selection time: 961.0
Test time: 1439.0
Accumulative iteration time: 13934.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14542460654913997
Kappa statistic: 0.563326530504521
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.04
Correctly Classified Instances: 70.96
Weighted Precision: 0.7171800890903544
Weighted AreaUnderROC: 0.7822966567659682
Root mean squared error: 0.4392006852917068
Relative absolute error: 43.76573511543195
Root relative squared error: 93.16853486146317
Weighted TruePositiveRate: 0.7096
Weighted MatthewsCorrelation: 0.5699615833217677
Weighted FMeasure: 0.7056723122850084
Iteration time: 970.0
Weighted AreaUnderPRC: 0.6018544824230557
Mean absolute error: 0.1945143782908096
Coverage of cases: 70.96
Instances selection time: 970.0
Test time: 1467.0
Accumulative iteration time: 14904.0
Weighted Recall: 0.7096
Weighted FalsePositiveRate: 0.1450066864680636
Kappa statistic: 0.5645370488999703
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.08
Correctly Classified Instances: 70.92
Weighted Precision: 0.717067825881429
Weighted AreaUnderROC: 0.7820041225007681
Root mean squared error: 0.43952197218377065
Relative absolute error: 43.82064056939562
Root relative squared error: 93.23669010348858
Weighted TruePositiveRate: 0.7092
Weighted MatthewsCorrelation: 0.5695833495125265
Weighted FMeasure: 0.7051458698388056
Iteration time: 978.0
Weighted AreaUnderPRC: 0.6014764093725891
Mean absolute error: 0.19475840253064813
Coverage of cases: 70.92
Instances selection time: 978.0
Test time: 1498.0
Accumulative iteration time: 15882.0
Weighted Recall: 0.7092
Weighted FalsePositiveRate: 0.14519175499846407
Kappa statistic: 0.5639445610991045
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7171513657529537
Weighted AreaUnderROC: 0.781707619277779
Root mean squared error: 0.4398422030379873
Relative absolute error: 43.87578215527373
Root relative squared error: 93.3046213260571
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.5693290325352115
Weighted FMeasure: 0.7045530785680627
Iteration time: 986.0
Weighted AreaUnderPRC: 0.6011361916938802
Mean absolute error: 0.19500347624566194
Coverage of cases: 70.88
Instances selection time: 986.0
Test time: 1529.0
Accumulative iteration time: 16868.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14538476144444204
Kappa statistic: 0.5633474837538873
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.16
Correctly Classified Instances: 70.84
Weighted Precision: 0.7173669182116515
Weighted AreaUnderROC: 0.7814143704532904
Root mean squared error: 0.44016143569619737
Relative absolute error: 43.93114382786026
Root relative squared error: 93.37234079927607
Weighted TruePositiveRate: 0.7084
Weighted MatthewsCorrelation: 0.5691842939023353
Weighted FMeasure: 0.7038780972895596
Iteration time: 993.0
Weighted AreaUnderPRC: 0.6008064584350294
Mean absolute error: 0.1952495281238243
Coverage of cases: 70.84
Instances selection time: 993.0
Test time: 1563.0
Accumulative iteration time: 17861.0
Weighted Recall: 0.7084
Weighted FalsePositiveRate: 0.1455712590934193
Kappa statistic: 0.5627499942419752
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.36
Correctly Classified Instances: 70.64
Weighted Precision: 0.7161518675478146
Weighted AreaUnderROC: 0.7799253066596422
Root mean squared error: 0.441684852847871
Relative absolute error: 44.225913621262684
Root relative squared error: 93.69550637883337
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.5667361781525467
Weighted FMeasure: 0.7016273687309533
Iteration time: 999.0
Weighted AreaUnderPRC: 0.5988222359642728
Mean absolute error: 0.19655961609450173
Coverage of cases: 70.64
Instances selection time: 999.0
Test time: 1595.0
Accumulative iteration time: 18860.0
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.14654938668071577
Kappa statistic: 0.559765598572441
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.76
Correctly Classified Instances: 70.24
Weighted Precision: 0.7132521655155171
Weighted AreaUnderROC: 0.7769377731564887
Root mean squared error: 0.44469932974501475
Relative absolute error: 44.81993499458336
Root relative squared error: 94.33497349554352
Weighted TruePositiveRate: 0.7024
Weighted MatthewsCorrelation: 0.5614809891713755
Weighted FMeasure: 0.6973083434753212
Iteration time: 1005.0
Weighted AreaUnderPRC: 0.5947545474206807
Mean absolute error: 0.19919971108703807
Coverage of cases: 70.24
Instances selection time: 1005.0
Test time: 1628.0
Accumulative iteration time: 19865.0
Weighted Recall: 0.7024
Weighted FalsePositiveRate: 0.1485244536870226
Kappa statistic: 0.5537798445714439
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.68
Correctly Classified Instances: 70.32
Weighted Precision: 0.7147031703454423
Weighted AreaUnderROC: 0.7775517739937482
Root mean squared error: 0.444116466001594
Relative absolute error: 44.69650053022247
Root relative squared error: 94.21132942389936
Weighted TruePositiveRate: 0.7032
Weighted MatthewsCorrelation: 0.5632112031750883
Weighted FMeasure: 0.6977844759548881
Iteration time: 1009.0
Weighted AreaUnderPRC: 0.5957452274887223
Mean absolute error: 0.19865111346765638
Coverage of cases: 70.32
Instances selection time: 1008.0
Test time: 1657.0
Accumulative iteration time: 20874.0
Weighted Recall: 0.7032
Weighted FalsePositiveRate: 0.14809645201250343
Kappa statistic: 0.5549964423704801
Training time: 1.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.72
Correctly Classified Instances: 70.28
Weighted Precision: 0.7148323795016864
Weighted AreaUnderROC: 0.7772588824489037
Root mean squared error: 0.44443025569106476
Relative absolute error: 44.75264797507762
Root relative squared error: 94.2778942690867
Weighted TruePositiveRate: 0.7028
Weighted MatthewsCorrelation: 0.5630069035046364
Weighted FMeasure: 0.6971682377130437
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5954209077988433
Mean absolute error: 0.1989006576670126
Coverage of cases: 70.28
Instances selection time: 1030.0
Test time: 1711.0
Accumulative iteration time: 21904.0
Weighted Recall: 0.7028
Weighted FalsePositiveRate: 0.14828223510219227
Kappa statistic: 0.5544037622475995
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.72
Correctly Classified Instances: 70.28
Weighted Precision: 0.7151473100470174
Weighted AreaUnderROC: 0.7772617795677601
Root mean squared error: 0.4444442862216836
Relative absolute error: 44.7491353001006
Root relative squared error: 94.28087059408998
Weighted TruePositiveRate: 0.7028
Weighted MatthewsCorrelation: 0.5632440427531338
Weighted FMeasure: 0.6970279235632612
Iteration time: 1021.0
Weighted AreaUnderPRC: 0.5954764807640269
Mean absolute error: 0.1988850457782258
Coverage of cases: 70.28
Instances selection time: 1021.0
Test time: 1725.0
Accumulative iteration time: 22925.0
Weighted Recall: 0.7028
Weighted FalsePositiveRate: 0.14827644086447994
Kappa statistic: 0.5544050449745045
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7146595608081289
Weighted AreaUnderROC: 0.7766669478418585
Root mean squared error: 0.44505554414308224
Relative absolute error: 44.86540378863476
Root relative squared error: 94.41053798047247
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5622678542723948
Weighted FMeasure: 0.6961302330177407
Iteration time: 1024.0
Weighted AreaUnderPRC: 0.5946932570900038
Mean absolute error: 0.19940179461615543
Coverage of cases: 70.2
Instances selection time: 1024.0
Test time: 1754.0
Accumulative iteration time: 23949.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.14866610431628302
Kappa statistic: 0.5532133138354424
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.92
Correctly Classified Instances: 70.08
Weighted Precision: 0.7143701189929769
Weighted AreaUnderROC: 0.7757756128929679
Root mean squared error: 0.44596371097825416
Relative absolute error: 45.041642228738105
Root relative squared error: 94.60318925875211
Weighted TruePositiveRate: 0.7008
Weighted MatthewsCorrelation: 0.5611261594048719
Weighted FMeasure: 0.6945973711665973
Iteration time: 1074.0
Weighted AreaUnderPRC: 0.5936000245333637
Mean absolute error: 0.20018507657217027
Coverage of cases: 70.08
Instances selection time: 1073.0
Test time: 1783.0
Accumulative iteration time: 25023.0
Weighted Recall: 0.7008
Weighted FalsePositiveRate: 0.1492487742140641
Kappa statistic: 0.5514247182731339
Training time: 1.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.08
Correctly Classified Instances: 69.92
Weighted Precision: 0.7131207951517312
Weighted AreaUnderROC: 0.7745794406441642
Root mean squared error: 0.44716705393492046
Relative absolute error: 45.277852348994394
Root relative squared error: 94.85845684817765
Weighted TruePositiveRate: 0.6992
Weighted MatthewsCorrelation: 0.5589557614335077
Weighted FMeasure: 0.6929204599826371
Iteration time: 1024.0
Weighted AreaUnderPRC: 0.5919842809968126
Mean absolute error: 0.20123489932886493
Coverage of cases: 69.92
Instances selection time: 1024.0
Test time: 1815.0
Accumulative iteration time: 26047.0
Weighted Recall: 0.6992
Weighted FalsePositiveRate: 0.15004111871167175
Kappa statistic: 0.5490337087707666
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.24
Correctly Classified Instances: 69.76
Weighted Precision: 0.7123946315472002
Weighted AreaUnderROC: 0.7733894199127168
Root mean squared error: 0.44836682992227705
Relative absolute error: 45.51420507996375
Root relative squared error: 95.11296776914705
Weighted TruePositiveRate: 0.6976
Weighted MatthewsCorrelation: 0.5571423867775772
Weighted FMeasure: 0.6911146941574627
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5904851638457157
Mean absolute error: 0.20228535591095095
Coverage of cases: 69.76
Instances selection time: 1037.0
Test time: 1847.0
Accumulative iteration time: 27084.0
Weighted Recall: 0.6976
Weighted FalsePositiveRate: 0.15082116017456634
Kappa statistic: 0.5466460442588599
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.7114861759350131
Weighted AreaUnderROC: 0.7721997564609139
Root mean squared error: 0.4495630860614501
Relative absolute error: 45.75069252077681
Root relative squared error: 95.36673201756064
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.5552446441807378
Weighted FMeasure: 0.6892600605512389
Iteration time: 1025.0
Weighted AreaUnderPRC: 0.5889076299449487
Mean absolute error: 0.20333641120345344
Coverage of cases: 69.6
Instances selection time: 1025.0
Test time: 1881.0
Accumulative iteration time: 28109.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.1516004870781725
Kappa statistic: 0.5442521561670358
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.44
Correctly Classified Instances: 69.56
Weighted Precision: 0.7115034058573306
Weighted AreaUnderROC: 0.7719072221957135
Root mean squared error: 0.4498700123563417
Relative absolute error: 45.80779691749731
Root relative squared error: 95.43184091689332
Weighted TruePositiveRate: 0.6956
Weighted MatthewsCorrelation: 0.5549664866212228
Weighted FMeasure: 0.6886767315958526
Iteration time: 1024.0
Weighted AreaUnderPRC: 0.5885533122724667
Mean absolute error: 0.20359020852221124
Coverage of cases: 69.56
Instances selection time: 1024.0
Test time: 1912.0
Accumulative iteration time: 29133.0
Weighted Recall: 0.6956
Weighted FalsePositiveRate: 0.15178555560857293
Kappa statistic: 0.5436557718130143
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.84
Correctly Classified Instances: 69.16
Weighted Precision: 0.7089735662136386
Weighted AreaUnderROC: 0.768926554769205
Root mean squared error: 0.4528270561502972
Relative absolute error: 46.403561887800436
Root relative squared error: 96.05912463258477
Weighted TruePositiveRate: 0.6916
Weighted MatthewsCorrelation: 0.5499660680226803
Weighted FMeasure: 0.6842790753583833
Iteration time: 1024.0
Weighted AreaUnderPRC: 0.584642402789852
Mean absolute error: 0.2062380528346696
Coverage of cases: 69.16
Instances selection time: 1024.0
Test time: 1946.0
Accumulative iteration time: 30157.0
Weighted Recall: 0.6916
Weighted FalsePositiveRate: 0.15374689046158982
Kappa statistic: 0.5376713314489993
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.04
Correctly Classified Instances: 68.96
Weighted Precision: 0.7077877118597213
Weighted AreaUnderROC: 0.7674378482552011
Root mean squared error: 0.45430356821414886
Relative absolute error: 46.700262467190775
Root relative squared error: 96.37234014044074
Weighted TruePositiveRate: 0.6896
Weighted MatthewsCorrelation: 0.5475579008579884
Weighted FMeasure: 0.6819675313665646
Iteration time: 1027.0
Weighted AreaUnderPRC: 0.582699714485621
Mean absolute error: 0.2075567220764044
Coverage of cases: 68.96
Instances selection time: 1027.0
Test time: 2001.0
Accumulative iteration time: 31184.0
Weighted Recall: 0.6896
Weighted FalsePositiveRate: 0.15472430348959787
Kappa statistic: 0.5346777787531954
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.7022182702537628
Weighted AreaUnderROC: 0.7632176295604514
Root mean squared error: 0.45839363340942096
Relative absolute error: 47.53568357695515
Root relative squared error: 97.23997399096235
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.5393233206213219
Weighted FMeasure: 0.6757342613494722
Iteration time: 1031.0
Weighted AreaUnderPRC: 0.5767078584051087
Mean absolute error: 0.2112697047864683
Coverage of cases: 68.4
Instances selection time: 1031.0
Test time: 2013.0
Accumulative iteration time: 32215.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.15756474087909736
Kappa statistic: 0.5262630187719779
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6947728317848197
Weighted AreaUnderROC: 0.7586412566307732
Root mean squared error: 0.4627350398030077
Relative absolute error: 48.43110735418358
Root relative squared error: 98.16092536119989
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.5294720098073158
Weighted FMeasure: 0.6687111647725761
Iteration time: 1028.0
Weighted AreaUnderPRC: 0.5698690836878303
Mean absolute error: 0.21524936601859468
Coverage of cases: 67.8
Instances selection time: 1028.0
Test time: 2044.0
Accumulative iteration time: 33243.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.16071748673845354
Kappa statistic: 0.5171793002950424
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.08
Correctly Classified Instances: 67.92
Weighted Precision: 0.697670640152399
Weighted AreaUnderROC: 0.7594874650437078
Root mean squared error: 0.46188171137611594
Relative absolute error: 48.24937655860344
Root relative squared error: 97.97990706602957
Weighted TruePositiveRate: 0.6792
Weighted MatthewsCorrelation: 0.531805005723163
Weighted FMeasure: 0.6696639767080824
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5713833925180627
Mean absolute error: 0.21444167359379407
Coverage of cases: 67.92
Instances selection time: 1035.0
Test time: 2074.0
Accumulative iteration time: 34278.0
Weighted Recall: 0.6792
Weighted FalsePositiveRate: 0.16022506991258448
Kappa statistic: 0.5189079955331628
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.69840777207342
Weighted AreaUnderROC: 0.7600580868615298
Root mean squared error: 0.4613148243028247
Relative absolute error: 48.12755519214893
Root relative squared error: 97.8596521579222
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.5327224521924491
Weighted FMeasure: 0.6704356623962813
Iteration time: 1031.0
Weighted AreaUnderPRC: 0.5721372482878125
Mean absolute error: 0.21390024529844068
Coverage of cases: 68.0
Instances selection time: 1031.0
Test time: 2104.0
Accumulative iteration time: 35309.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.15988382627694028
Kappa statistic: 0.5200706456009676
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.92
Correctly Classified Instances: 68.08
Weighted Precision: 0.6989313676345063
Weighted AreaUnderROC: 0.7606290659589962
Root mean squared error: 0.4607468918169164
Relative absolute error: 48.00579243765028
Root relative squared error: 97.73917548430963
Weighted TruePositiveRate: 0.6808
Weighted MatthewsCorrelation: 0.5335495517882484
Weighted FMeasure: 0.6710796026094408
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.5728137858023817
Mean absolute error: 0.2133590775006689
Coverage of cases: 68.08
Instances selection time: 1033.0
Test time: 2135.0
Accumulative iteration time: 36342.0
Weighted Recall: 0.6808
Weighted FalsePositiveRate: 0.15954186808200765
Kappa statistic: 0.5212314072196863
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.92
Correctly Classified Instances: 68.08
Weighted Precision: 0.6992177919524002
Weighted AreaUnderROC: 0.7605947355757714
Root mean squared error: 0.46075567343522844
Relative absolute error: 48.00380047505893
Root relative squared error: 97.7410383468671
Weighted TruePositiveRate: 0.6808
Weighted MatthewsCorrelation: 0.533526023566453
Weighted FMeasure: 0.6704717169790065
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.5727191670350947
Mean absolute error: 0.21335022433359627
Coverage of cases: 68.08
Instances selection time: 1033.0
Test time: 2168.0
Accumulative iteration time: 37375.0
Weighted Recall: 0.6808
Weighted FalsePositiveRate: 0.15961052884845717
Kappa statistic: 0.5211868230613707
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.7024082190441373
Weighted AreaUnderROC: 0.7629628701652996
Root mean squared error: 0.45844878251387866
Relative absolute error: 47.52299298519001
Root relative squared error: 97.25167288268388
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.5378914142758872
Weighted FMeasure: 0.6740410557555179
Iteration time: 1032.0
Weighted AreaUnderPRC: 0.5760224884012741
Mean absolute error: 0.21121330215640105
Coverage of cases: 68.4
Instances selection time: 1032.0
Test time: 2202.0
Accumulative iteration time: 38407.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.15807425966940072
Kappa statistic: 0.5259409891343275
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.7036925031313751
Weighted AreaUnderROC: 0.763795664822886
Root mean squared error: 0.45758567724863236
Relative absolute error: 47.34151957022202
Root relative squared error: 97.06858060690382
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5394359380077318
Weighted FMeasure: 0.673783957835301
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5769441283003677
Mean absolute error: 0.21040675364543218
Coverage of cases: 68.52
Instances selection time: 1030.0
Test time: 2242.0
Accumulative iteration time: 39437.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15760867035422807
Kappa statistic: 0.5276543663942468
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.7030091489520399
Weighted AreaUnderROC: 0.7631842038248383
Root mean squared error: 0.4581746884504347
Relative absolute error: 47.45941043084018
Root relative squared error: 97.1935287514006
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5382712372482396
Weighted FMeasure: 0.6725911026707099
Iteration time: 1028.0
Weighted AreaUnderPRC: 0.5760657204087417
Mean absolute error: 0.21093071302595737
Coverage of cases: 68.44
Instances selection time: 1028.0
Test time: 2270.0
Accumulative iteration time: 40465.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.1580315923503233
Kappa statistic: 0.5264405809571734
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.64
Correctly Classified Instances: 68.36
Weighted Precision: 0.7024849515220287
Weighted AreaUnderROC: 0.7625759972252911
Root mean squared error: 0.45876274726696276
Relative absolute error: 47.5773641102002
Root relative squared error: 97.31827486447166
Weighted TruePositiveRate: 0.6836
Weighted MatthewsCorrelation: 0.5371559312576284
Weighted FMeasure: 0.6716557776872353
Iteration time: 1026.0
Weighted AreaUnderPRC: 0.5752657845293981
Mean absolute error: 0.21145495160089076
Coverage of cases: 68.36
Instances selection time: 1026.0
Test time: 2298.0
Accumulative iteration time: 41491.0
Weighted Recall: 0.6836
Weighted FalsePositiveRate: 0.1584480055494178
Kappa statistic: 0.5252312861898604
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.44
Correctly Classified Instances: 68.56
Weighted Precision: 0.7059290183595127
Weighted AreaUnderROC: 0.7640593056629295
Root mean squared error: 0.4573179870399982
Relative absolute error: 47.276302274395306
Root relative squared error: 97.01179493836909
Weighted TruePositiveRate: 0.6856
Weighted MatthewsCorrelation: 0.5407243887976918
Weighted FMeasure: 0.6733373259950495
Iteration time: 1023.0
Weighted AreaUnderPRC: 0.5775760571076823
Mean absolute error: 0.21011689899731345
Coverage of cases: 68.56
Instances selection time: 1023.0
Test time: 2329.0
Accumulative iteration time: 42514.0
Weighted Recall: 0.6856
Weighted FalsePositiveRate: 0.15748138867414094
Kappa statistic: 0.5282078862512816
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.7062345986642009
Weighted AreaUnderROC: 0.7637364099722934
Root mean squared error: 0.4576160728033094
Relative absolute error: 47.334490238611664
Root relative squared error: 97.07502847775285
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5402962899600269
Weighted FMeasure: 0.6726378898294516
Iteration time: 1022.0
Weighted AreaUnderPRC: 0.5772331818114819
Mean absolute error: 0.2103755121716084
Coverage of cases: 68.52
Instances selection time: 1022.0
Test time: 2359.0
Accumulative iteration time: 43536.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15772718005541328
Kappa statistic: 0.5275795119097986
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.7085324458093066
Weighted AreaUnderROC: 0.7658163365340545
Root mean squared error: 0.4555834224629484
Relative absolute error: 46.91375623663574
Root relative squared error: 96.64383822591772
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.5441356598888566
Weighted FMeasure: 0.6751168211874219
Iteration time: 1017.0
Weighted AreaUnderPRC: 0.5799576527829211
Mean absolute error: 0.2085055832739376
Coverage of cases: 68.8
Instances selection time: 1016.0
Test time: 2417.0
Accumulative iteration time: 44553.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.15636732693189132
Kappa statistic: 0.5317504417154166
Training time: 1.0
		
Time end:Wed Nov 01 16.20.34 EET 2017