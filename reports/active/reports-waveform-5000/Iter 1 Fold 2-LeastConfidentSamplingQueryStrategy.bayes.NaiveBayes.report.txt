Wed Nov 01 16.11.01 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.11.01 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.89333333333448
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8332471311850611
Weighted AreaUnderROC: 0.9458768011182656
Root mean squared error: 0.3366390142219849
Relative absolute error: 30.463274437665877
Root relative squared error: 71.41191893049587
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7225086768066672
Weighted FMeasure: 0.7933306010095732
Iteration time: 105.0
Weighted AreaUnderPRC: 0.9011067952120695
Mean absolute error: 0.1353923308340712
Coverage of cases: 88.52
Instances selection time: 100.0
Test time: 135.0
Accumulative iteration time: 105.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09681289388112355
Kappa statistic: 0.70707596640371
Training time: 5.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 40.06666666666783
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8335244783808472
Weighted AreaUnderROC: 0.9465588629712535
Root mean squared error: 0.33374601659911646
Relative absolute error: 30.180782715990198
Root relative squared error: 70.79822145936983
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7238357692613616
Weighted FMeasure: 0.7951361163840662
Iteration time: 102.0
Weighted AreaUnderPRC: 0.9020530733587373
Mean absolute error: 0.13413681207106817
Coverage of cases: 88.92
Instances selection time: 97.0
Test time: 132.0
Accumulative iteration time: 207.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09622657427772048
Kappa statistic: 0.7088637683715349
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 40.29333333333445
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8358890254343769
Weighted AreaUnderROC: 0.9466044823142434
Root mean squared error: 0.33240123821764606
Relative absolute error: 30.086313654579826
Root relative squared error: 70.51295088555058
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7282241429473636
Weighted FMeasure: 0.7987592172162077
Iteration time: 101.0
Weighted AreaUnderPRC: 0.9019043584542027
Mean absolute error: 0.13371694957591096
Coverage of cases: 89.08
Instances selection time: 96.0
Test time: 131.0
Accumulative iteration time: 308.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.094644745228109
Kappa statistic: 0.7136483122024158
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.4933333333344
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8356317159116748
Weighted AreaUnderROC: 0.9443302250480822
Root mean squared error: 0.3301642778293277
Relative absolute error: 29.92729144474236
Root relative squared error: 70.03841992760292
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7293146788201293
Weighted FMeasure: 0.8006698004540574
Iteration time: 100.0
Weighted AreaUnderPRC: 0.8989137661018757
Mean absolute error: 0.13301018419885555
Coverage of cases: 89.28
Instances selection time: 95.0
Test time: 132.0
Accumulative iteration time: 408.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09407215820510838
Kappa statistic: 0.7154325425353463
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.80000000000103
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8318643274324888
Weighted AreaUnderROC: 0.9436476215532126
Root mean squared error: 0.32762177948122867
Relative absolute error: 29.7499713757059
Root relative squared error: 69.49907458067399
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7275341467068209
Weighted FMeasure: 0.801482748069671
Iteration time: 100.0
Weighted AreaUnderPRC: 0.8981096280791516
Mean absolute error: 0.13222209500313795
Coverage of cases: 89.96
Instances selection time: 94.0
Test time: 133.0
Accumulative iteration time: 508.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09414082110712063
Kappa statistic: 0.7154079228996326
Training time: 6.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 40.85333333333439
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8319685219119348
Weighted AreaUnderROC: 0.9426664049766871
Root mean squared error: 0.3289393236081624
Relative absolute error: 30.008866717727937
Root relative squared error: 69.77856789667419
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7264051900292108
Weighted FMeasure: 0.8000447518200537
Iteration time: 98.0
Weighted AreaUnderPRC: 0.8971567398171646
Mean absolute error: 0.13337274096768034
Coverage of cases: 90.08
Instances selection time: 92.0
Test time: 130.0
Accumulative iteration time: 606.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09472027442032248
Kappa statistic: 0.7136183234425809
Training time: 6.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.73333333333437
Incorrectly Classified Instances: 18.76
Correctly Classified Instances: 81.24
Weighted Precision: 0.8368295580743054
Weighted AreaUnderROC: 0.9454810801903192
Root mean squared error: 0.3275644151639646
Relative absolute error: 29.840175383233873
Root relative squared error: 69.48690577135332
Weighted TruePositiveRate: 0.8124
Weighted MatthewsCorrelation: 0.7324651806599951
Weighted FMeasure: 0.8034946823128964
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9016749962329892
Mean absolute error: 0.13262300170326227
Coverage of cases: 90.04
Instances selection time: 91.0
Test time: 129.0
Accumulative iteration time: 703.0
Weighted Recall: 0.8124
Weighted FalsePositiveRate: 0.09290638528850345
Kappa statistic: 0.7190155647403716
Training time: 6.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 41.01333333333433
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8339369948883508
Weighted AreaUnderROC: 0.9477784887078436
Root mean squared error: 0.323981465606942
Relative absolute error: 29.57220542418105
Root relative squared error: 68.72684739282731
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7307339785771914
Weighted FMeasure: 0.8041514354844498
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9051713819556908
Mean absolute error: 0.13143202410747196
Coverage of cases: 90.56
Instances selection time: 89.0
Test time: 133.0
Accumulative iteration time: 798.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09315904424131467
Kappa statistic: 0.7183920883275798
Training time: 6.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 41.320000000000945
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8369550903100448
Weighted AreaUnderROC: 0.9483125572883502
Root mean squared error: 0.3232101735593709
Relative absolute error: 29.60359625950904
Root relative squared error: 68.56323164169348
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7350512585323595
Weighted FMeasure: 0.807351429330517
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9062154599504334
Mean absolute error: 0.1315715389311519
Coverage of cases: 90.88
Instances selection time: 89.0
Test time: 132.0
Accumulative iteration time: 893.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09176807753270343
Kappa statistic: 0.7225811956307078
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 41.213333333334305
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8326823761853773
Weighted AreaUnderROC: 0.9487031173680281
Root mean squared error: 0.324728353492641
Relative absolute error: 29.80953886567202
Root relative squared error: 68.88528623945646
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7274311037947032
Weighted FMeasure: 0.8011956016562691
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9070610599195851
Mean absolute error: 0.1324868394029874
Coverage of cases: 90.56
Instances selection time: 87.0
Test time: 131.0
Accumulative iteration time: 986.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.0945362783695235
Kappa statistic: 0.7142044350199147
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 41.50666666666757
Incorrectly Classified Instances: 18.76
Correctly Classified Instances: 81.24
Weighted Precision: 0.835531211231494
Weighted AreaUnderROC: 0.9499000502323335
Root mean squared error: 0.3234831627937051
Relative absolute error: 29.75642790250523
Root relative squared error: 68.62114140333006
Weighted TruePositiveRate: 0.8124
Weighted MatthewsCorrelation: 0.732104953596358
Weighted FMeasure: 0.8047604972675598
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9089277305366787
Mean absolute error: 0.13225079067780166
Coverage of cases: 90.84
Instances selection time: 86.0
Test time: 131.0
Accumulative iteration time: 1079.0
Weighted Recall: 0.8124
Weighted FalsePositiveRate: 0.09295444931991204
Kappa statistic: 0.7189899744730253
Training time: 7.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 41.60000000000089
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8328701876634675
Weighted AreaUnderROC: 0.9501009769646805
Root mean squared error: 0.3219719081997283
Relative absolute error: 29.63774418779237
Root relative squared error: 68.30055589187998
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.729116534226548
Weighted FMeasure: 0.8032250346936027
Iteration time: 92.0
Weighted AreaUnderPRC: 0.908968882213761
Mean absolute error: 0.13172330750130004
Coverage of cases: 91.04
Instances selection time: 85.0
Test time: 131.0
Accumulative iteration time: 1171.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09376596271532144
Kappa statistic: 0.7165894287976777
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 41.62666666666755
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8317020685705356
Weighted AreaUnderROC: 0.9495139338711204
Root mean squared error: 0.32345607629173906
Relative absolute error: 29.82849852849633
Root relative squared error: 68.61539548856442
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7266001493624535
Weighted FMeasure: 0.800838066852345
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9084953353564464
Mean absolute error: 0.13257110457109542
Coverage of cases: 90.92
Instances selection time: 84.0
Test time: 132.0
Accumulative iteration time: 1262.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.0947477395811274
Kappa statistic: 0.713601370495333
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 41.65333333333421
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8335606149305611
Weighted AreaUnderROC: 0.9498503340992258
Root mean squared error: 0.3231413786127277
Relative absolute error: 29.817105903120595
Root relative squared error: 68.54863802970864
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7295549629403564
Weighted FMeasure: 0.8031151056392144
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9090796131347575
Mean absolute error: 0.1325204706805366
Coverage of cases: 90.84
Instances selection time: 83.0
Test time: 131.0
Accumulative iteration time: 1352.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09375909642512022
Kappa statistic: 0.7165920099318622
Training time: 7.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 41.76000000000086
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8339326703475386
Weighted AreaUnderROC: 0.9497036008994116
Root mean squared error: 0.3234424254262157
Relative absolute error: 29.954851657699294
Root relative squared error: 68.61249970269024
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7297864927791621
Weighted FMeasure: 0.8034385478954191
Iteration time: 104.0
Weighted AreaUnderPRC: 0.9087255276911445
Mean absolute error: 0.13313267403421972
Coverage of cases: 90.84
Instances selection time: 89.0
Test time: 131.0
Accumulative iteration time: 1456.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.0937590964251202
Kappa statistic: 0.7165881382129545
Training time: 15.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.853333333334184
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8352192774539393
Weighted AreaUnderROC: 0.9486304049448853
Root mean squared error: 0.32421078060664393
Relative absolute error: 30.119074715309814
Root relative squared error: 68.77549245022242
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7316877736436117
Weighted FMeasure: 0.804839298595778
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9071000639842852
Mean absolute error: 0.13386255429026647
Coverage of cases: 90.96
Instances selection time: 82.0
Test time: 131.0
Accumulative iteration time: 1545.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.0931659105315159
Kappa statistic: 0.7183818291807238
Training time: 7.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 42.01333333333417
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8313235619268715
Weighted AreaUnderROC: 0.9474354604136687
Root mean squared error: 0.32497979118214176
Relative absolute error: 30.287230548056872
Root relative squared error: 68.93862422804402
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7270444340263639
Weighted FMeasure: 0.8021168198356876
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9054749871340627
Mean absolute error: 0.13460991354692006
Coverage of cases: 90.96
Instances selection time: 81.0
Test time: 131.0
Accumulative iteration time: 1634.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.0945912086911333
Kappa statistic: 0.7141797065434512
Training time: 8.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 41.88000000000086
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8334260406640928
Weighted AreaUnderROC: 0.9481028591955164
Root mean squared error: 0.3258458834252432
Relative absolute error: 30.37163467584403
Root relative squared error: 69.12235013751305
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7288842049850012
Weighted FMeasure: 0.8027035533693021
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9066220709302591
Mean absolute error: 0.1349850430037519
Coverage of cases: 90.76
Instances selection time: 79.0
Test time: 132.0
Accumulative iteration time: 1721.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09416828626792553
Kappa statistic: 0.7153871872704374
Training time: 8.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 41.93333333333419
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8333269524934717
Weighted AreaUnderROC: 0.9477118313407842
Root mean squared error: 0.326586133714642
Relative absolute error: 30.465873155445756
Root relative squared error: 69.2793809373358
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7288295591389738
Weighted FMeasure: 0.8026811950982438
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9059953755835866
Mean absolute error: 0.13540388069087067
Coverage of cases: 90.68
Instances selection time: 79.0
Test time: 129.0
Accumulative iteration time: 1808.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09416828626792553
Kappa statistic: 0.7153871872704374
Training time: 8.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 42.066666666667494
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.830560409194581
Weighted AreaUnderROC: 0.947426083518663
Root mean squared error: 0.3255758452984485
Relative absolute error: 30.441194745230185
Root relative squared error: 69.06506640032242
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7263209976544709
Weighted FMeasure: 0.8016140927412562
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9054854605743103
Mean absolute error: 0.13529419886769034
Coverage of cases: 90.88
Instances selection time: 78.0
Test time: 129.0
Accumulative iteration time: 1894.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.0948026699027372
Kappa statistic: 0.7135791982278821
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 41.90666666666751
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8296793279134373
Weighted AreaUnderROC: 0.9461653233552887
Root mean squared error: 0.3289084537979778
Relative absolute error: 30.79749269452229
Root relative squared error: 69.77201942103954
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.722686975624032
Weighted FMeasure: 0.7978977333069677
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9036698535165322
Mean absolute error: 0.1368777453089886
Coverage of cases: 90.56
Instances selection time: 77.0
Test time: 129.0
Accumulative iteration time: 1979.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09635016750134254
Kappa statistic: 0.7088041100082948
Training time: 8.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 42.093333333334144
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8291366212277517
Weighted AreaUnderROC: 0.9460774625949807
Root mean squared error: 0.3275960751069956
Relative absolute error: 30.683057523507106
Root relative squared error: 69.49362185947608
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7228369123653444
Weighted FMeasure: 0.7987826767959813
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9034660343521104
Mean absolute error: 0.1363691445489211
Coverage of cases: 90.8
Instances selection time: 76.0
Test time: 129.0
Accumulative iteration time: 2064.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09617303774074477
Kappa statistic: 0.7093913684562726
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 41.97333333333416
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8297438317838857
Weighted AreaUnderROC: 0.9458809936027858
Root mean squared error: 0.3276011577890687
Relative absolute error: 30.70946421329161
Root relative squared error: 69.49470005916422
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7239738037303948
Weighted FMeasure: 0.7997245789355013
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9032496441971356
Mean absolute error: 0.13648650761463002
Coverage of cases: 90.72
Instances selection time: 75.0
Test time: 129.0
Accumulative iteration time: 2148.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09578444676854314
Kappa statistic: 0.7105858007042293
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 41.88000000000082
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8305225933755696
Weighted AreaUnderROC: 0.945649842328294
Root mean squared error: 0.328677681306001
Relative absolute error: 30.808322425255785
Root relative squared error: 69.7230651828431
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7239288719135915
Weighted FMeasure: 0.7988548270557468
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9030919788754258
Mean absolute error: 0.1369258774455819
Coverage of cases: 90.36
Instances selection time: 74.0
Test time: 130.0
Accumulative iteration time: 2231.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09595471023893967
Kappa statistic: 0.7099998058676387
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 42.00000000000084
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.83026507748422
Weighted AreaUnderROC: 0.9447284928637563
Root mean squared error: 0.3293434797447615
Relative absolute error: 30.930581169518003
Root relative squared error: 69.8643023601284
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.723454547162078
Weighted FMeasure: 0.798690805439085
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9016219309819229
Mean absolute error: 0.1374692496423029
Coverage of cases: 90.36
Instances selection time: 73.0
Test time: 130.0
Accumulative iteration time: 2313.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09615930516034234
Kappa statistic: 0.7093966619323289
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 42.09333333333412
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8286993036601225
Weighted AreaUnderROC: 0.944235759061154
Root mean squared error: 0.32901087468465223
Relative absolute error: 30.954263996012585
Root relative squared error: 69.79374617209032
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.721512587333963
Weighted FMeasure: 0.7974254965394585
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9008803329211389
Mean absolute error: 0.13757450664894547
Coverage of cases: 90.44
Instances selection time: 72.0
Test time: 130.0
Accumulative iteration time: 2394.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09676622363434909
Kappa statistic: 0.7075977839746808
Training time: 9.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 42.186666666667456
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8279626389973557
Weighted AreaUnderROC: 0.943224766636479
Root mean squared error: 0.3308967028571422
Relative absolute error: 31.19796005528878
Root relative squared error: 70.19379073876642
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7191915747076651
Weighted FMeasure: 0.795038557292577
Iteration time: 82.0
Weighted AreaUnderPRC: 0.8995161208856646
Mean absolute error: 0.13865760024572857
Coverage of cases: 90.44
Instances selection time: 72.0
Test time: 128.0
Accumulative iteration time: 2476.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.0977411342099538
Kappa statistic: 0.7046126207223653
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 42.09333333333411
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8303914007056031
Weighted AreaUnderROC: 0.9430722471054973
Root mean squared error: 0.3303495988464125
Relative absolute error: 31.12333295945164
Root relative squared error: 70.07773245196603
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7231969121715492
Weighted FMeasure: 0.7981618134503696
Iteration time: 81.0
Weighted AreaUnderPRC: 0.8991753241027031
Mean absolute error: 0.13832592426423015
Coverage of cases: 90.24
Instances selection time: 71.0
Test time: 130.0
Accumulative iteration time: 2557.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09635703379154376
Kappa statistic: 0.7088001319147387
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 42.17333333333413
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8274931422197608
Weighted AreaUnderROC: 0.9421493715898931
Root mean squared error: 0.3324390742328027
Relative absolute error: 31.373972324934826
Root relative squared error: 70.52097711641768
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7178205168639615
Weighted FMeasure: 0.7936076460066359
Iteration time: 80.0
Weighted AreaUnderPRC: 0.8980010623026407
Mean absolute error: 0.139439876999711
Coverage of cases: 90.2
Instances selection time: 70.0
Test time: 128.0
Accumulative iteration time: 2637.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09832745381335689
Kappa statistic: 0.7028205445909385
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 42.266666666667426
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8289572020743492
Weighted AreaUnderROC: 0.9416323906760117
Root mean squared error: 0.331715521960156
Relative absolute error: 31.287956873877683
Root relative squared error: 70.36748850085827
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7209487188472318
Weighted FMeasure: 0.7964411689370325
Iteration time: 79.0
Weighted AreaUnderPRC: 0.8972108458770793
Mean absolute error: 0.13905758610612368
Coverage of cases: 90.24
Instances selection time: 69.0
Test time: 128.0
Accumulative iteration time: 2716.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09715481460655072
Kappa statistic: 0.7064047621387252
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 42.18666666666746
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8279551593892833
Weighted AreaUnderROC: 0.9417347647500561
Root mean squared error: 0.3325977807637162
Relative absolute error: 31.380478975184715
Root relative squared error: 70.55464385568595
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7184515645473317
Weighted FMeasure: 0.7942061205718044
Iteration time: 79.0
Weighted AreaUnderPRC: 0.8973249482473522
Mean absolute error: 0.13946879544526605
Coverage of cases: 90.16
Instances selection time: 68.0
Test time: 129.0
Accumulative iteration time: 2795.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09812972518215544
Kappa statistic: 0.7034169957040401
Training time: 11.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 42.04000000000081
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8275164249563408
Weighted AreaUnderROC: 0.9418896480195089
Root mean squared error: 0.33326946410901986
Relative absolute error: 31.438162905745987
Root relative squared error: 70.69712941016824
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7174248895685045
Weighted FMeasure: 0.7931575322066221
Iteration time: 76.0
Weighted AreaUnderPRC: 0.8973383607324315
Mean absolute error: 0.1397251684699828
Coverage of cases: 90.2
Instances selection time: 65.0
Test time: 131.0
Accumulative iteration time: 2871.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09851831615435709
Kappa statistic: 0.7022241043416325
Training time: 11.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 41.77333333333416
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8283914211702053
Weighted AreaUnderROC: 0.9434916480614295
Root mean squared error: 0.33310661399833214
Relative absolute error: 31.319428122239078
Root relative squared error: 70.66258368489295
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7173997210196013
Weighted FMeasure: 0.7924392968043399
Iteration time: 76.0
Weighted AreaUnderPRC: 0.900028365407592
Mean absolute error: 0.13919745832106323
Coverage of cases: 89.84
Instances selection time: 65.0
Test time: 131.0
Accumulative iteration time: 2947.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09868857962475362
Kappa statistic: 0.7016358265154922
Training time: 11.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 41.80000000000082
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8331583479940786
Weighted AreaUnderROC: 0.9467784833593651
Root mean squared error: 0.3318137887391939
Relative absolute error: 31.049898160920975
Root relative squared error: 70.38833403260519
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7239414194736536
Weighted FMeasure: 0.7970996132920374
Iteration time: 74.0
Weighted AreaUnderPRC: 0.905166804215448
Mean absolute error: 0.13799954738187165
Coverage of cases: 90.24
Instances selection time: 63.0
Test time: 130.0
Accumulative iteration time: 3021.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.0964723669403305
Kappa statistic: 0.7082275283672341
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 41.64000000000089
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8312309991829898
Weighted AreaUnderROC: 0.9490397386184012
Root mean squared error: 0.3319720446937839
Relative absolute error: 30.958463345615918
Root relative squared error: 70.4219051902013
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7201295874209873
Weighted FMeasure: 0.7932614547225617
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9081932541071183
Mean absolute error: 0.13759317042496028
Coverage of cases: 90.36
Instances selection time: 62.0
Test time: 131.0
Accumulative iteration time: 3095.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09784273477833808
Kappa statistic: 0.7040511917151587
Training time: 12.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.466666666667585
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8312629464385732
Weighted AreaUnderROC: 0.9506090262082681
Root mean squared error: 0.33301733898639513
Relative absolute error: 31.047252851479424
Root relative squared error: 70.6436455949936
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7192423226750544
Weighted FMeasure: 0.7918047264712277
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9102393937955625
Mean absolute error: 0.1379877904510203
Coverage of cases: 90.08
Instances selection time: 62.0
Test time: 132.0
Accumulative iteration time: 3168.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09821759317013727
Kappa statistic: 0.7028651942518792
Training time: 11.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 41.2933333333343
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8333850684822425
Weighted AreaUnderROC: 0.9515838866238162
Root mean squared error: 0.3328051165525538
Relative absolute error: 30.959865011647857
Root relative squared error: 70.59862641836686
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.721425081810571
Weighted FMeasure: 0.792868644479707
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9115323555952874
Mean absolute error: 0.1375994000517689
Coverage of cases: 90.0
Instances selection time: 60.0
Test time: 130.0
Accumulative iteration time: 3240.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09760380840592929
Kappa statistic: 0.7046677590165591
Training time: 12.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 41.320000000000974
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8330440825285088
Weighted AreaUnderROC: 0.9516483554512201
Root mean squared error: 0.3333064614818588
Relative absolute error: 31.02386627967183
Root relative squared error: 70.70497773813437
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7208310018401555
Weighted FMeasure: 0.7922790147557702
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9114289559574079
Mean absolute error: 0.13788385013187543
Coverage of cases: 89.88
Instances selection time: 60.0
Test time: 130.0
Accumulative iteration time: 3312.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09780153703713074
Kappa statistic: 0.704071402418803
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 41.25333333333433
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8338650278241934
Weighted AreaUnderROC: 0.9523775171639185
Root mean squared error: 0.33295024306196475
Relative absolute error: 30.965038527411913
Root relative squared error: 70.62941240004719
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.722429101120855
Weighted FMeasure: 0.793360978418957
Iteration time: 71.0
Weighted AreaUnderPRC: 0.912364726391585
Mean absolute error: 0.1376223934551647
Coverage of cases: 90.04
Instances selection time: 59.0
Test time: 132.0
Accumulative iteration time: 3383.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09720835114352643
Kappa statistic: 0.7058685396510774
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.22666666666766
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8338291811758991
Weighted AreaUnderROC: 0.9524652163134382
Root mean squared error: 0.333375273735534
Relative absolute error: 31.018444722049985
Root relative squared error: 70.71957502149512
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7223882308876387
Weighted FMeasure: 0.7931325171334364
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9123398374848057
Mean absolute error: 0.1378597543202228
Coverage of cases: 89.92
Instances selection time: 57.0
Test time: 131.0
Accumulative iteration time: 3453.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09720835114352643
Kappa statistic: 0.7058712178393239
Training time: 13.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.160000000001006
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8345192310352822
Weighted AreaUnderROC: 0.9528583792230394
Root mean squared error: 0.33341480089526687
Relative absolute error: 31.01005855829403
Root relative squared error: 70.72795999830156
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7231049534781281
Weighted FMeasure: 0.7935168928844449
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9128767167599632
Mean absolute error: 0.13782248248130746
Coverage of cases: 90.0
Instances selection time: 56.0
Test time: 131.0
Accumulative iteration time: 3522.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09700375622212376
Kappa statistic: 0.7064715944774488
Training time: 13.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.16000000000103
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8346050122333377
Weighted AreaUnderROC: 0.9523270899722529
Root mean squared error: 0.3338592242085992
Relative absolute error: 31.046240697477547
Root relative squared error: 70.82223641987397
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7231512831617544
Weighted FMeasure: 0.7931631029191815
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9117719167104479
Mean absolute error: 0.13798329198878975
Coverage of cases: 89.96
Instances selection time: 56.0
Test time: 130.0
Accumulative iteration time: 3591.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09700375622212377
Kappa statistic: 0.7064756034502176
Training time: 13.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 41.080000000001036
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8329847975165003
Weighted AreaUnderROC: 0.9527763235858514
Root mean squared error: 0.33381606700642175
Relative absolute error: 31.029618025105208
Root relative squared error: 70.81308139477896
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7203175118027388
Weighted FMeasure: 0.7912094837249513
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9124748633039391
Mean absolute error: 0.13790941344491267
Coverage of cases: 89.8
Instances selection time: 56.0
Test time: 129.0
Accumulative iteration time: 3660.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09799239937813094
Kappa statistic: 0.7034818064822831
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 41.09333333333437
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8341734981595944
Weighted AreaUnderROC: 0.9528864509302377
Root mean squared error: 0.33372955875717136
Relative absolute error: 31.025921153912815
Root relative squared error: 70.79473022387691
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7225067541497405
Weighted FMeasure: 0.7925379581561413
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9126747492764186
Mean absolute error: 0.13789298290627983
Coverage of cases: 89.96
Instances selection time: 54.0
Test time: 129.0
Accumulative iteration time: 3727.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.0972014848533252
Kappa statistic: 0.7058792521114394
Training time: 13.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 41.013333333334394
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8339834321044642
Weighted AreaUnderROC: 0.9530564686597219
Root mean squared error: 0.333441278193377
Relative absolute error: 30.972879165947507
Root relative squared error: 70.73357668141392
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7228335945751756
Weighted FMeasure: 0.793239861897565
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9128148249018398
Mean absolute error: 0.13765724073754512
Coverage of cases: 89.84
Instances selection time: 55.0
Test time: 130.0
Accumulative iteration time: 3795.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09701062251232499
Kappa statistic: 0.7064729308138727
Training time: 13.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.06666666666772
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8344376776617871
Weighted AreaUnderROC: 0.9533184099695643
Root mean squared error: 0.3326943857895079
Relative absolute error: 30.897782411895324
Root relative squared error: 70.57513687633615
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7230384320645713
Weighted FMeasure: 0.7932719277535262
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9131737360935227
Mean absolute error: 0.1373234773862021
Coverage of cases: 89.88
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 3861.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09700375622212376
Kappa statistic: 0.706474267138129
Training time: 13.0
		
Time end:Wed Nov 01 16.11.14 EET 2017