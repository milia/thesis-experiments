Wed Nov 01 15.21.26 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 15.21.26 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7028250058351176
Weighted AreaUnderROC: 0.7763944557982472
Root mean squared error: 0.444398110783266
Relative absolute error: 45.02982107355844
Root relative squared error: 94.27107530440117
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5532641527245132
Weighted FMeasure: 0.7019127565808656
Iteration time: 749.0
Weighted AreaUnderPRC: 0.5927310474964158
Mean absolute error: 0.2001325381047051
Coverage of cases: 70.2
Instances selection time: 749.0
Test time: 930.0
Accumulative iteration time: 749.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.1492110884035056
Kappa statistic: 0.5529563685415696
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.703402579886703
Weighted AreaUnderROC: 0.7763246843111625
Root mean squared error: 0.44444842125461664
Relative absolute error: 45.017208413000624
Root relative squared error: 94.28174776703818
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5534243329339648
Weighted FMeasure: 0.7022045172844914
Iteration time: 763.0
Weighted AreaUnderPRC: 0.5929551920048086
Mean absolute error: 0.20007648183555926
Coverage of cases: 70.2
Instances selection time: 763.0
Test time: 967.0
Accumulative iteration time: 1512.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.14935063137767493
Kappa statistic: 0.552879957210912
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.56
Correctly Classified Instances: 70.44
Weighted Precision: 0.7064438801961987
Weighted AreaUnderROC: 0.7780933036577119
Root mean squared error: 0.4427015740716875
Relative absolute error: 44.647513812155125
Root relative squared error: 93.91118552041443
Weighted TruePositiveRate: 0.7044
Weighted MatthewsCorrelation: 0.5573406076526656
Weighted FMeasure: 0.7046171345402392
Iteration time: 782.0
Weighted AreaUnderPRC: 0.5956580259445536
Mean absolute error: 0.19843339472069038
Coverage of cases: 70.44
Instances selection time: 782.0
Test time: 998.0
Accumulative iteration time: 2294.0
Weighted Recall: 0.7044
Weighted FalsePositiveRate: 0.148213392684576
Kappa statistic: 0.5564398215483576
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.76
Correctly Classified Instances: 70.24
Weighted Precision: 0.7077115408309732
Weighted AreaUnderROC: 0.7764756096877332
Root mean squared error: 0.44423993084775415
Relative absolute error: 44.9349911190046
Root relative squared error: 94.23752027288675
Weighted TruePositiveRate: 0.7024
Weighted MatthewsCorrelation: 0.5560471391604895
Weighted FMeasure: 0.7023181240055933
Iteration time: 800.0
Weighted AreaUnderPRC: 0.5942816120947817
Mean absolute error: 0.1997110716400214
Coverage of cases: 70.24
Instances selection time: 800.0
Test time: 1036.0
Accumulative iteration time: 3094.0
Weighted Recall: 0.7024
Weighted FalsePositiveRate: 0.1494487806245336
Kappa statistic: 0.553301891550345
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.68
Correctly Classified Instances: 70.32
Weighted Precision: 0.7095845571382029
Weighted AreaUnderROC: 0.7770422625477665
Root mean squared error: 0.4436827347581491
Relative absolute error: 44.805488850770885
Root relative squared error: 94.11932113286365
Weighted TruePositiveRate: 0.7032
Weighted MatthewsCorrelation: 0.5578504306923984
Weighted FMeasure: 0.7029607862554385
Iteration time: 819.0
Weighted AreaUnderPRC: 0.5953731094782474
Mean absolute error: 0.1991355060034271
Coverage of cases: 70.32
Instances selection time: 819.0
Test time: 1060.0
Accumulative iteration time: 3913.0
Weighted Recall: 0.7032
Weighted FalsePositiveRate: 0.14911547490446697
Kappa statistic: 0.5544618186988653
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.92
Correctly Classified Instances: 70.08
Weighted Precision: 0.7085070343758967
Weighted AreaUnderROC: 0.7752010134769788
Root mean squared error: 0.44551071323215397
Relative absolute error: 45.154228855721875
Root relative squared error: 94.5070939253132
Weighted TruePositiveRate: 0.7008
Weighted MatthewsCorrelation: 0.5549732424369025
Weighted FMeasure: 0.7004311607781183
Iteration time: 833.0
Weighted AreaUnderPRC: 0.5931394798552777
Mean absolute error: 0.20068546158098705
Coverage of cases: 70.08
Instances selection time: 833.0
Test time: 1092.0
Accumulative iteration time: 4746.0
Weighted Recall: 0.7008
Weighted FalsePositiveRate: 0.15039797304604255
Kappa statistic: 0.5508142033029328
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.36
Correctly Classified Instances: 69.64
Weighted Precision: 0.7057797975075366
Weighted AreaUnderROC: 0.7718431974242603
Root mean squared error: 0.44881010915715114
Relative absolute error: 45.8022471910121
Root relative squared error: 95.20700149502863
Weighted TruePositiveRate: 0.6964
Weighted MatthewsCorrelation: 0.5493219347231638
Weighted FMeasure: 0.6959510985612336
Iteration time: 849.0
Weighted AreaUnderPRC: 0.5889337212336616
Mean absolute error: 0.20356554307116584
Coverage of cases: 69.64
Instances selection time: 849.0
Test time: 1126.0
Accumulative iteration time: 5595.0
Weighted Recall: 0.6964
Weighted FalsePositiveRate: 0.15271360515147925
Kappa statistic: 0.5441474295680752
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.4
Correctly Classified Instances: 69.6
Weighted Precision: 0.7073133378729706
Weighted AreaUnderROC: 0.7714989889444008
Root mean squared error: 0.449139105813597
Relative absolute error: 45.85381026438704
Root relative squared error: 95.27679222505681
Weighted TruePositiveRate: 0.696
Weighted MatthewsCorrelation: 0.5498607197355107
Weighted FMeasure: 0.6951679321436471
Iteration time: 864.0
Weighted AreaUnderPRC: 0.5889217673494486
Mean absolute error: 0.2037947122861656
Coverage of cases: 69.6
Instances selection time: 864.0
Test time: 1157.0
Accumulative iteration time: 6459.0
Weighted Recall: 0.696
Weighted FalsePositiveRate: 0.1530020221111982
Kappa statistic: 0.5434935334657682
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.7060113901143763
Weighted AreaUnderROC: 0.7690531449522104
Root mean squared error: 0.4515283524639536
Relative absolute error: 46.323981900453084
Root relative squared error: 95.78362797757512
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.5461660423326231
Weighted FMeasure: 0.6916760230073085
Iteration time: 880.0
Weighted AreaUnderPRC: 0.585996483616122
Mean absolute error: 0.20588436400201468
Coverage of cases: 69.28
Instances selection time: 880.0
Test time: 1190.0
Accumulative iteration time: 7339.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.1546937100955791
Kappa statistic: 0.5386378763885918
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.7050228713288703
Weighted AreaUnderROC: 0.7672220163565681
Root mean squared error: 0.4533185400650947
Relative absolute error: 46.675256222547844
Root relative squared error: 96.16338411528399
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.5433905911574616
Weighted FMeasure: 0.6889735933979487
Iteration time: 893.0
Weighted AreaUnderPRC: 0.5837618409231794
Mean absolute error: 0.20744558321132472
Coverage of cases: 69.04
Instances selection time: 893.0
Test time: 1221.0
Accumulative iteration time: 8232.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.15595596728686403
Kappa statistic: 0.5349986230191782
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.7051634441038904
Weighted AreaUnderROC: 0.7669095984207214
Root mean squared error: 0.45363948127492065
Relative absolute error: 46.72830725462073
Root relative squared error: 96.23146602703305
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.5431255750060104
Weighted FMeasure: 0.6884921663043493
Iteration time: 909.0
Weighted AreaUnderPRC: 0.5834777199823878
Mean absolute error: 0.2076813655760931
Coverage of cases: 69.0
Instances selection time: 909.0
Test time: 1255.0
Accumulative iteration time: 9141.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.15618080315855734
Kappa statistic: 0.5343835237854919
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.7054158950995566
Weighted AreaUnderROC: 0.7659788534101822
Root mean squared error: 0.4545433259909775
Relative absolute error: 46.901244813279575
Root relative squared error: 96.42320044539208
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.5422207493162867
Weighted FMeasure: 0.6869764189025579
Iteration time: 922.0
Weighted AreaUnderPRC: 0.5825297292565946
Mean absolute error: 0.20844997694791018
Coverage of cases: 68.88
Instances selection time: 922.0
Test time: 1289.0
Accumulative iteration time: 10063.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.1568422931796358
Kappa statistic: 0.5325449646080059
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.32
Correctly Classified Instances: 68.68
Weighted Precision: 0.7043441541122091
Weighted AreaUnderROC: 0.7644601427503863
Root mean squared error: 0.45602692269883394
Relative absolute error: 47.19407806191005
Root relative squared error: 96.73791883319346
Weighted TruePositiveRate: 0.6868
Weighted MatthewsCorrelation: 0.5397478651798164
Weighted FMeasure: 0.684831282631642
Iteration time: 934.0
Weighted AreaUnderPRC: 0.5806616763458391
Mean absolute error: 0.20975145805293455
Coverage of cases: 68.68
Instances selection time: 934.0
Test time: 1323.0
Accumulative iteration time: 10997.0
Weighted Recall: 0.6868
Weighted FalsePositiveRate: 0.1578797144992274
Kappa statistic: 0.5295210800994267
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.705808398935239
Weighted AreaUnderROC: 0.7653540175384888
Root mean squared error: 0.4551764614422445
Relative absolute error: 47.009174311924525
Root relative squared error: 96.55750875669223
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.5417761296887632
Weighted FMeasure: 0.6858868886160484
Iteration time: 944.0
Weighted AreaUnderPRC: 0.5819344889024518
Mean absolute error: 0.20892966360855444
Coverage of cases: 68.8
Instances selection time: 944.0
Test time: 1355.0
Accumulative iteration time: 11941.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.15729196492302241
Kappa statistic: 0.5313135430677186
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.28
Correctly Classified Instances: 68.72
Weighted Precision: 0.7063464602221825
Weighted AreaUnderROC: 0.7647288243871513
Root mean squared error: 0.45578242310757794
Relative absolute error: 47.1233716475087
Root relative squared error: 96.68605263750113
Weighted TruePositiveRate: 0.6872
Weighted MatthewsCorrelation: 0.5414122305101492
Weighted FMeasure: 0.6847153212747775
Iteration time: 956.0
Weighted AreaUnderPRC: 0.5813479138585229
Mean absolute error: 0.20943720732226187
Coverage of cases: 68.72
Instances selection time: 956.0
Test time: 1387.0
Accumulative iteration time: 12897.0
Weighted Recall: 0.6872
Weighted FalsePositiveRate: 0.15774235122569744
Kappa statistic: 0.5300810616177903
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.72
Correctly Classified Instances: 68.28
Weighted Precision: 0.7045566168404853
Weighted AreaUnderROC: 0.7613818822505691
Root mean squared error: 0.4589986176454588
Relative absolute error: 47.77584059775919
Root relative squared error: 97.36831052770634
Weighted TruePositiveRate: 0.6828
Weighted MatthewsCorrelation: 0.5363250767737718
Weighted FMeasure: 0.6797265610380561
Iteration time: 968.0
Weighted AreaUnderPRC: 0.5773168337032368
Mean absolute error: 0.21233706932337518
Coverage of cases: 68.28
Instances selection time: 968.0
Test time: 1422.0
Accumulative iteration time: 13865.0
Weighted Recall: 0.6828
Weighted FalsePositiveRate: 0.1600362354988617
Kappa statistic: 0.5234189408337535
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.88
Correctly Classified Instances: 68.12
Weighted Precision: 0.7044625210071171
Weighted AreaUnderROC: 0.7601553485763294
Root mean squared error: 0.46017557464316705
Relative absolute error: 48.010206561361535
Root relative squared error: 97.61798080997967
Weighted TruePositiveRate: 0.6812
Weighted MatthewsCorrelation: 0.5348452214011581
Weighted FMeasure: 0.6777514142121785
Iteration time: 979.0
Weighted AreaUnderPRC: 0.5759657109267621
Mean absolute error: 0.21337869582827448
Coverage of cases: 68.12
Instances selection time: 979.0
Test time: 1452.0
Accumulative iteration time: 14844.0
Weighted Recall: 0.6812
Weighted FalsePositiveRate: 0.1608893028473413
Kappa statistic: 0.5209839098675633
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.0
Correctly Classified Instances: 68.0
Weighted Precision: 0.7047379737130339
Weighted AreaUnderROC: 0.7592282152439348
Root mean squared error: 0.4610606839724549
Relative absolute error: 48.185053380783565
Root relative squared error: 97.80574085262896
Weighted TruePositiveRate: 0.68
Weighted MatthewsCorrelation: 0.5339973124707673
Weighted FMeasure: 0.6762642387728433
Iteration time: 986.0
Weighted AreaUnderPRC: 0.5750601300391176
Mean absolute error: 0.21415579280348349
Coverage of cases: 68.0
Instances selection time: 986.0
Test time: 1486.0
Accumulative iteration time: 15830.0
Weighted Recall: 0.68
Weighted FalsePositiveRate: 0.16154356951213059
Kappa statistic: 0.5191489320778633
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7044065789321172
Weighted AreaUnderROC: 0.7580027534086275
Root mean squared error: 0.46223087124081413
Relative absolute error: 48.41993047508814
Root relative squared error: 98.05397505844346
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5324492935808581
Weighted FMeasure: 0.6743208030649884
Iteration time: 1007.0
Weighted AreaUnderPRC: 0.5736607226129788
Mean absolute error: 0.21519969100039274
Coverage of cases: 67.84
Instances selection time: 1007.0
Test time: 1529.0
Accumulative iteration time: 16837.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16239449318274493
Kappa statistic: 0.5167146986535816
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7051953752946216
Weighted AreaUnderROC: 0.7579893785349819
Root mean squared error: 0.4622490014096971
Relative absolute error: 48.415855039638316
Root relative squared error: 98.0578210480518
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5329927637577424
Weighted FMeasure: 0.6740989823878241
Iteration time: 1010.0
Weighted AreaUnderPRC: 0.573815725884159
Mean absolute error: 0.2151815779539491
Coverage of cases: 67.84
Instances selection time: 1010.0
Test time: 1549.0
Accumulative iteration time: 17847.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16242124293003635
Kappa statistic: 0.5166975002440557
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7057459373852102
Weighted AreaUnderROC: 0.7579821551786927
Root mean squared error: 0.46226633059096495
Relative absolute error: 48.4119601328905
Root relative squared error: 98.06149712252787
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5333302893392733
Weighted FMeasure: 0.6739818203023368
Iteration time: 1013.0
Weighted AreaUnderPRC: 0.5739372193887279
Mean absolute error: 0.21516426725729212
Coverage of cases: 67.84
Instances selection time: 1013.0
Test time: 1583.0
Accumulative iteration time: 18860.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16243568964261468
Kappa statistic: 0.5166884357248519
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.48
Correctly Classified Instances: 67.52
Weighted Precision: 0.7037383514326835
Weighted AreaUnderROC: 0.7555601638149375
Root mean squared error: 0.46457710766259946
Relative absolute error: 48.88667388949112
Root relative squared error: 98.55168696367681
Weighted TruePositiveRate: 0.6752
Weighted MatthewsCorrelation: 0.5292196387164758
Weighted FMeasure: 0.6705081767529787
Iteration time: 1016.0
Weighted AreaUnderPRC: 0.5708958275938231
Mean absolute error: 0.21727410617551712
Coverage of cases: 67.52
Instances selection time: 1016.0
Test time: 1619.0
Accumulative iteration time: 19876.0
Weighted Recall: 0.6752
Weighted FalsePositiveRate: 0.16407967237012513
Kappa statistic: 0.5118565945288502
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.44
Correctly Classified Instances: 67.56
Weighted Precision: 0.7051353955631383
Weighted AreaUnderROC: 0.7558454747238486
Root mean squared error: 0.46430690150845383
Relative absolute error: 48.823329798515104
Root relative squared error: 98.49436758250242
Weighted TruePositiveRate: 0.6756
Weighted MatthewsCorrelation: 0.5305041187029947
Weighted FMeasure: 0.6706297340714659
Iteration time: 1022.0
Weighted AreaUnderPRC: 0.5715029905289664
Mean absolute error: 0.21699257688229037
Coverage of cases: 67.56
Instances selection time: 1022.0
Test time: 1647.0
Accumulative iteration time: 20898.0
Weighted Recall: 0.6756
Weighted FalsePositiveRate: 0.163909050552303
Kappa statistic: 0.512438059794452
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.56
Correctly Classified Instances: 67.44
Weighted Precision: 0.7045769847668082
Weighted AreaUnderROC: 0.7549349706635999
Root mean squared error: 0.46518018354195323
Relative absolute error: 48.99937694704014
Root relative squared error: 98.67961867683515
Weighted TruePositiveRate: 0.6744
Weighted MatthewsCorrelation: 0.5290823632730906
Weighted FMeasure: 0.6692678967994488
Iteration time: 1028.0
Weighted AreaUnderPRC: 0.5704010140316804
Mean absolute error: 0.21777500865351276
Coverage of cases: 67.44
Instances selection time: 1028.0
Test time: 1677.0
Accumulative iteration time: 21926.0
Weighted Recall: 0.6744
Weighted FalsePositiveRate: 0.16453005867280016
Kappa statistic: 0.5106229685442392
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.96
Correctly Classified Instances: 67.04
Weighted Precision: 0.70163511322582
Weighted AreaUnderROC: 0.751920330133511
Root mean squared error: 0.4680435908161046
Relative absolute error: 49.594303153610724
Root relative squared error: 99.28703908709053
Weighted TruePositiveRate: 0.6704
Weighted MatthewsCorrelation: 0.5235592383939034
Weighted FMeasure: 0.6651420659488254
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.5665780773118868
Mean absolute error: 0.2204191251271598
Coverage of cases: 67.04
Instances selection time: 1034.0
Test time: 1715.0
Accumulative iteration time: 22960.0
Weighted Recall: 0.6704
Weighted FalsePositiveRate: 0.16655933973297815
Kappa statistic: 0.5045980991958136
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.28
Correctly Classified Instances: 66.72
Weighted Precision: 0.6995034651846332
Weighted AreaUnderROC: 0.7494986960493998
Root mean squared error: 0.4703244045847171
Relative absolute error: 50.069790628116365
Root relative squared error: 99.77087275181341
Weighted TruePositiveRate: 0.6672
Weighted MatthewsCorrelation: 0.5194089358217711
Weighted FMeasure: 0.6617793007627708
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5636036553386322
Mean absolute error: 0.22253240279162934
Coverage of cases: 66.72
Instances selection time: 1040.0
Test time: 1744.0
Accumulative iteration time: 24001.0
Weighted Recall: 0.6672
Weighted FalsePositiveRate: 0.1682026079012002
Kappa statistic: 0.49976647752388254
Training time: 1.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.4
Correctly Classified Instances: 66.6
Weighted Precision: 0.6990644549615177
Weighted AreaUnderROC: 0.7485852948702952
Root mean squared error: 0.4711853099565807
Relative absolute error: 50.24633431085002
Root relative squared error: 99.95349835973481
Weighted TruePositiveRate: 0.666
Weighted MatthewsCorrelation: 0.5180544037077657
Weighted FMeasure: 0.6603987219486823
Iteration time: 1044.0
Weighted AreaUnderPRC: 0.5625391316759893
Mean absolute error: 0.2233170413815567
Coverage of cases: 66.6
Instances selection time: 1044.0
Test time: 1777.0
Accumulative iteration time: 25045.0
Weighted Recall: 0.666
Weighted FalsePositiveRate: 0.16882941025940967
Kappa statistic: 0.4979487448357866
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.48
Correctly Classified Instances: 66.52
Weighted Precision: 0.6985069068647162
Weighted AreaUnderROC: 0.7479806999488925
Root mean squared error: 0.4717624871088036
Relative absolute error: 50.36318312560027
Root relative squared error: 100.07593612321963
Weighted TruePositiveRate: 0.6652
Weighted MatthewsCorrelation: 0.5169905504559287
Weighted FMeasure: 0.6595152906843451
Iteration time: 1047.0
Weighted AreaUnderPRC: 0.5617684603926735
Mean absolute error: 0.22383636944711338
Coverage of cases: 66.52
Instances selection time: 1047.0
Test time: 1835.0
Accumulative iteration time: 26092.0
Weighted Recall: 0.6652
Weighted FalsePositiveRate: 0.169238600102215
Kappa statistic: 0.4967413854390989
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.92
Correctly Classified Instances: 66.08
Weighted Precision: 0.6953553505747283
Weighted AreaUnderROC: 0.7446554278811778
Root mean squared error: 0.47486514875513586
Relative absolute error: 51.018626528693794
Root relative squared error: 100.73411005017432
Weighted TruePositiveRate: 0.6608
Weighted MatthewsCorrelation: 0.5111122091989714
Weighted FMeasure: 0.6549314765645898
Iteration time: 1059.0
Weighted AreaUnderPRC: 0.557665450488613
Mean absolute error: 0.22674945123864013
Coverage of cases: 66.08
Instances selection time: 1058.0
Test time: 1841.0
Accumulative iteration time: 27151.0
Weighted Recall: 0.6608
Weighted FalsePositiveRate: 0.17148914423764425
Kappa statistic: 0.4901027010125791
Training time: 1.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.52
Correctly Classified Instances: 66.48
Weighted Precision: 0.6991822700970174
Weighted AreaUnderROC: 0.7476704645726137
Root mean squared error: 0.4720692222763121
Relative absolute error: 50.41772853185712
Root relative squared error: 100.14100447831173
Weighted TruePositiveRate: 0.6648
Weighted MatthewsCorrelation: 0.5168717236987079
Weighted FMeasure: 0.6590677884848252
Iteration time: 1059.0
Weighted AreaUnderPRC: 0.5616619450431158
Mean absolute error: 0.22407879347492157
Coverage of cases: 66.48
Instances selection time: 1059.0
Test time: 1872.0
Accumulative iteration time: 28210.0
Weighted Recall: 0.6648
Weighted FalsePositiveRate: 0.1694590708547728
Kappa statistic: 0.49613163783433273
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.64
Correctly Classified Instances: 66.36
Weighted Precision: 0.6989786444607227
Weighted AreaUnderROC: 0.7467563488342205
Root mean squared error: 0.47292529830698404
Relative absolute error: 50.59474161377962
Root relative squared error: 100.32260562826156
Weighted TruePositiveRate: 0.6636
Weighted MatthewsCorrelation: 0.5156610064812058
Weighted FMeasure: 0.6576683987681167
Iteration time: 1057.0
Weighted AreaUnderPRC: 0.5606749307085863
Mean absolute error: 0.22486551828346601
Coverage of cases: 66.36
Instances selection time: 1057.0
Test time: 1905.0
Accumulative iteration time: 29267.0
Weighted Recall: 0.6636
Weighted FalsePositiveRate: 0.17008730233155908
Kappa statistic: 0.49431151565120884
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.76
Correctly Classified Instances: 66.24
Weighted Precision: 0.7002006693944857
Weighted AreaUnderROC: 0.7458277863832489
Root mean squared error: 0.4737794907829192
Relative absolute error: 50.771861086375424
Root relative squared error: 100.50380721591323
Weighted TruePositiveRate: 0.6624
Weighted MatthewsCorrelation: 0.5152855567507666
Weighted FMeasure: 0.6557827654568174
Iteration time: 1057.0
Weighted AreaUnderPRC: 0.5599095666291333
Mean absolute error: 0.22565271593944738
Coverage of cases: 66.24
Instances selection time: 1057.0
Test time: 1937.0
Accumulative iteration time: 30324.0
Weighted Recall: 0.6624
Weighted FalsePositiveRate: 0.17074442723350203
Kappa statistic: 0.4924710119165882
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.92
Correctly Classified Instances: 66.08
Weighted Precision: 0.6996436729539856
Weighted AreaUnderROC: 0.7446117304637988
Root mean squared error: 0.4749119269836456
Relative absolute error: 51.00892388451412
Root relative squared error: 100.74403321095166
Weighted TruePositiveRate: 0.6608
Weighted MatthewsCorrelation: 0.5135194055947674
Weighted FMeasure: 0.6538227638695512
Iteration time: 1058.0
Weighted AreaUnderPRC: 0.5584718853878673
Mean absolute error: 0.22670632837561935
Coverage of cases: 66.08
Instances selection time: 1058.0
Test time: 1971.0
Accumulative iteration time: 31382.0
Weighted Recall: 0.6608
Weighted FalsePositiveRate: 0.17157653907240256
Kappa statistic: 0.49004652603817633
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.88
Correctly Classified Instances: 66.12
Weighted Precision: 0.7013666438937287
Weighted AreaUnderROC: 0.7448995812119218
Root mean squared error: 0.4746425120107755
Relative absolute error: 50.94686156491764
Root relative squared error: 100.68688166467078
Weighted TruePositiveRate: 0.6612
Weighted MatthewsCorrelation: 0.5149025912767998
Weighted FMeasure: 0.6538832343758805
Iteration time: 1063.0
Weighted AreaUnderPRC: 0.5591268935560438
Mean absolute error: 0.22643049584407948
Coverage of cases: 66.12
Instances selection time: 1063.0
Test time: 2001.0
Accumulative iteration time: 32445.0
Weighted Recall: 0.6612
Weighted FalsePositiveRate: 0.17140083757615657
Kappa statistic: 0.4906307326594344
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.84
Correctly Classified Instances: 66.16
Weighted Precision: 0.7023689204611063
Weighted AreaUnderROC: 0.745194655316334
Root mean squared error: 0.4743725585178694
Relative absolute error: 50.88486897717563
Root relative squared error: 100.62961588103911
Weighted TruePositiveRate: 0.6616
Weighted MatthewsCorrelation: 0.5158644842245472
Weighted FMeasure: 0.65418420470566
Iteration time: 1056.0
Weighted AreaUnderPRC: 0.5596676590414063
Mean absolute error: 0.22615497323189274
Coverage of cases: 66.16
Instances selection time: 1056.0
Test time: 2032.0
Accumulative iteration time: 33501.0
Weighted Recall: 0.6616
Weighted FalsePositiveRate: 0.17121068936733222
Kappa statistic: 0.491225014697944
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.88
Correctly Classified Instances: 66.12
Weighted Precision: 0.7022856914461688
Weighted AreaUnderROC: 0.7448891034571321
Root mean squared error: 0.4746628172889656
Relative absolute error: 50.94264339152106
Root relative squared error: 100.6911890646414
Weighted TruePositiveRate: 0.6612
Weighted MatthewsCorrelation: 0.5154854358397741
Weighted FMeasure: 0.6536662514794118
Iteration time: 1055.0
Weighted AreaUnderPRC: 0.5593159228688359
Mean absolute error: 0.22641174840676132
Coverage of cases: 66.12
Instances selection time: 1055.0
Test time: 2060.0
Accumulative iteration time: 34556.0
Weighted Recall: 0.6612
Weighted FalsePositiveRate: 0.1714217930857356
Kappa statistic: 0.4906167638684547
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.48
Correctly Classified Instances: 66.52
Weighted Precision: 0.7051289079904085
Weighted AreaUnderROC: 0.7479236665395702
Root mean squared error: 0.4718620889710925
Relative absolute error: 50.342109566637916
Root relative squared error: 100.09706486889263
Weighted TruePositiveRate: 0.6652
Weighted MatthewsCorrelation: 0.5206406159604913
Weighted FMeasure: 0.6573530200152423
Iteration time: 1080.0
Weighted AreaUnderPRC: 0.5628498075304494
Mean absolute error: 0.22374270918505845
Coverage of cases: 66.52
Instances selection time: 1080.0
Test time: 2101.0
Accumulative iteration time: 35636.0
Weighted Recall: 0.6652
Weighted FalsePositiveRate: 0.16935266692085968
Kappa statistic: 0.4966607621025123
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 33.24
Correctly Classified Instances: 66.76
Weighted Precision: 0.706362462817722
Weighted AreaUnderROC: 0.7497501116181354
Root mean squared error: 0.4701770510997915
Relative absolute error: 49.98101367658869
Root relative squared error: 99.73961435728671
Weighted TruePositiveRate: 0.6676
Weighted MatthewsCorrelation: 0.5233127708286238
Weighted FMeasure: 0.6600652301157481
Iteration time: 1060.0
Weighted AreaUnderPRC: 0.5650508835199507
Mean absolute error: 0.2221378385626174
Coverage of cases: 66.76
Instances selection time: 1060.0
Test time: 2125.0
Accumulative iteration time: 36696.0
Weighted Recall: 0.6676
Weighted FalsePositiveRate: 0.16809977676372914
Kappa statistic: 0.5003011422237957
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.88
Correctly Classified Instances: 67.12
Weighted Precision: 0.7099880190326663
Weighted AreaUnderROC: 0.7524661052473702
Root mean squared error: 0.4676329636611525
Relative absolute error: 49.440380047505435
Root relative squared error: 99.19993191334875
Weighted TruePositiveRate: 0.6712
Weighted MatthewsCorrelation: 0.528493562981349
Weighted FMeasure: 0.6636517413344049
Iteration time: 1081.0
Weighted AreaUnderPRC: 0.5687262696892506
Mean absolute error: 0.21973502243335852
Coverage of cases: 67.12
Instances selection time: 1081.0
Test time: 2163.0
Accumulative iteration time: 37777.0
Weighted Recall: 0.6712
Weighted FalsePositiveRate: 0.1662677895052596
Kappa statistic: 0.5057308761142703
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.32
Correctly Classified Instances: 67.68
Weighted Precision: 0.712997381068589
Weighted AreaUnderROC: 0.7567206932070473
Root mean squared error: 0.46364217074592295
Relative absolute error: 48.60046765393526
Root relative squared error: 98.35335689354774
Weighted TruePositiveRate: 0.6768
Weighted MatthewsCorrelation: 0.5353103444551024
Weighted FMeasure: 0.6687631237382271
Iteration time: 1056.0
Weighted AreaUnderPRC: 0.5735803609132573
Mean absolute error: 0.21600207846193553
Coverage of cases: 67.68
Instances selection time: 1056.0
Test time: 2194.0
Accumulative iteration time: 38833.0
Weighted Recall: 0.6768
Weighted FalsePositiveRate: 0.16335861358590556
Kappa statistic: 0.5141981600135062
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.4
Correctly Classified Instances: 67.6
Weighted Precision: 0.7123611725123733
Weighted AreaUnderROC: 0.7561284013203576
Root mean squared error: 0.4642239374459147
Relative absolute error: 48.71834228702942
Root relative squared error: 98.47676824713756
Weighted TruePositiveRate: 0.676
Weighted MatthewsCorrelation: 0.5340071312012771
Weighted FMeasure: 0.6678384731193022
Iteration time: 1055.0
Weighted AreaUnderPRC: 0.572736866216819
Mean absolute error: 0.21652596572013177
Coverage of cases: 67.6
Instances selection time: 1055.0
Test time: 2226.0
Accumulative iteration time: 39888.0
Weighted Recall: 0.676
Weighted FalsePositiveRate: 0.16374319735928472
Kappa statistic: 0.5130050498370191
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7129195906547988
Weighted AreaUnderROC: 0.7573629117918774
Root mean squared error: 0.4630843322792981
Relative absolute error: 48.47709750567019
Root relative squared error: 98.2350214847806
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5355412041377725
Weighted FMeasure: 0.669180879507525
Iteration time: 1054.0
Weighted AreaUnderPRC: 0.5740018669126196
Mean absolute error: 0.2154537666918685
Coverage of cases: 67.76
Instances selection time: 1054.0
Test time: 2257.0
Accumulative iteration time: 40942.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16287417641624494
Kappa statistic: 0.5154463208308063
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.7146768007517438
Weighted AreaUnderROC: 0.7582748838524056
Root mean squared error: 0.4622294993877012
Relative absolute error: 48.295755770661856
Root relative squared error: 98.05368404445176
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5374353969120678
Weighted FMeasure: 0.6698835263680909
Iteration time: 1054.0
Weighted AreaUnderPRC: 0.575226722654234
Mean absolute error: 0.21464780342516482
Coverage of cases: 67.88
Instances selection time: 1054.0
Test time: 2291.0
Accumulative iteration time: 41996.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.16225023229518912
Kappa statistic: 0.5172603182450831
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.7136819262565175
Weighted AreaUnderROC: 0.7579982643000629
Root mean squared error: 0.4625247845683685
Relative absolute error: 48.3539251650777
Root relative squared error: 98.11632349054187
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.536459570241726
Weighted FMeasure: 0.6687441289065544
Iteration time: 1046.0
Weighted AreaUnderPRC: 0.5743559276439908
Mean absolute error: 0.21490633406701298
Coverage of cases: 67.84
Instances selection time: 1046.0
Test time: 2340.0
Accumulative iteration time: 43042.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.1624034713998742
Kappa statistic: 0.5166742572052807
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7128465613192402
Weighted AreaUnderROC: 0.7574095840915179
Root mean squared error: 0.4631070569041745
Relative absolute error: 48.47201735357899
Root relative squared error: 98.23984210568561
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5351315918183104
Weighted FMeasure: 0.667548429729289
Iteration time: 1058.0
Weighted AreaUnderPRC: 0.5733730419893027
Mean absolute error: 0.21543118823812984
Coverage of cases: 67.76
Instances selection time: 1058.0
Test time: 2364.0
Accumulative iteration time: 44100.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16278083181696423
Kappa statistic: 0.5154812749743554
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.7130471036067911
Weighted AreaUnderROC: 0.7577245418665766
Root mean squared error: 0.46282682109210393
Relative absolute error: 48.410548823948474
Root relative squared error: 98.18039511277168
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.5354484968333136
Weighted FMeasure: 0.6679183826371956
Iteration time: 1040.0
Weighted AreaUnderPRC: 0.573719650559042
Mean absolute error: 0.21515799477310535
Coverage of cases: 67.8
Instances selection time: 1040.0
Test time: 2386.0
Accumulative iteration time: 45140.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.16255091626684698
Kappa statistic: 0.5161007991490107
Training time: 0.0
		
Time end:Wed Nov 01 15.23.32 EET 2017