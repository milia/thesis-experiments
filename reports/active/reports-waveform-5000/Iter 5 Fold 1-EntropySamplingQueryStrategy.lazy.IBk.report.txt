Wed Nov 01 15.25.36 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 15.25.36 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7071662415691966
Weighted AreaUnderROC: 0.7810188273677318
Root mean squared error: 0.4399017364441522
Relative absolute error: 44.135188866799034
Root relative squared error: 93.31725026861902
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5617779231122522
Weighted FMeasure: 0.7067484328850145
Iteration time: 747.0
Weighted AreaUnderPRC: 0.598992950688409
Mean absolute error: 0.1961563949635522
Coverage of cases: 70.8
Instances selection time: 746.0
Test time: 944.0
Accumulative iteration time: 747.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14596234526453647
Kappa statistic: 0.5621304500963072
Training time: 1.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.709363298507708
Weighted AreaUnderROC: 0.7824616517692262
Root mean squared error: 0.4384423092684049
Relative absolute error: 43.824091778200945
Root relative squared error: 93.00765901283336
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.5646575837507706
Weighted FMeasure: 0.7094416214683599
Iteration time: 761.0
Weighted AreaUnderPRC: 0.6012305405957231
Mean absolute error: 0.19477374123644955
Coverage of cases: 71.0
Instances selection time: 761.0
Test time: 971.0
Accumulative iteration time: 1508.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.1450766964615477
Kappa statistic: 0.5650561947396395
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.96
Correctly Classified Instances: 71.04
Weighted Precision: 0.7099863162535294
Weighted AreaUnderROC: 0.782715885198519
Root mean squared error: 0.43818578609200076
Relative absolute error: 43.75248618784571
Root relative squared error: 92.95324222956329
Weighted TruePositiveRate: 0.7104
Weighted MatthewsCorrelation: 0.565271673589961
Weighted FMeasure: 0.7100987524798656
Iteration time: 781.0
Weighted AreaUnderPRC: 0.6017828026265273
Mean absolute error: 0.1944554941682041
Coverage of cases: 71.04
Instances selection time: 781.0
Test time: 1007.0
Accumulative iteration time: 2289.0
Weighted Recall: 0.7104
Weighted FalsePositiveRate: 0.14496822960296218
Kappa statistic: 0.5656026411359419
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7114377206371341
Weighted AreaUnderROC: 0.7835918792747173
Root mean squared error: 0.4373196369781189
Relative absolute error: 43.56234458259254
Root relative squared error: 92.76950425597991
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5671464621490688
Weighted FMeasure: 0.7114448504187105
Iteration time: 800.0
Weighted AreaUnderPRC: 0.6031477972575225
Mean absolute error: 0.19361042036707887
Coverage of cases: 71.16
Instances selection time: 800.0
Test time: 1038.0
Accumulative iteration time: 3089.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14441624145056522
Kappa statistic: 0.5673730386877711
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7120930255832114
Weighted AreaUnderROC: 0.7835403820982081
Root mean squared error: 0.4373593323447263
Relative absolute error: 43.551972555744726
Root relative squared error: 92.77792491485286
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5674165178243178
Weighted FMeasure: 0.7115719707699416
Iteration time: 815.0
Weighted AreaUnderPRC: 0.6033194902800145
Mean absolute error: 0.19356432246997748
Coverage of cases: 71.16
Instances selection time: 815.0
Test time: 1071.0
Accumulative iteration time: 3904.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14451923580358364
Kappa statistic: 0.5673138435325242
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7184821614036194
Weighted AreaUnderROC: 0.787707316791115
Root mean squared error: 0.4331291458266129
Relative absolute error: 42.70646766169233
Root relative squared error: 91.88056684306028
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5762136735964335
Weighted FMeasure: 0.7171910341391092
Iteration time: 829.0
Weighted AreaUnderPRC: 0.6095027497982628
Mean absolute error: 0.1898065229408557
Coverage of cases: 71.72
Instances selection time: 829.0
Test time: 1103.0
Accumulative iteration time: 4733.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14178536641776968
Kappa statistic: 0.5756690684477601
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.68
Correctly Classified Instances: 71.32
Weighted Precision: 0.7157953479854902
Weighted AreaUnderROC: 0.7846431444428945
Root mean squared error: 0.4362160550299849
Relative absolute error: 43.29438202247298
Root relative squared error: 92.53539917224374
Weighted TruePositiveRate: 0.7132
Weighted MatthewsCorrelation: 0.5707986038166246
Weighted FMeasure: 0.7134522304531904
Iteration time: 844.0
Weighted AreaUnderPRC: 0.6055287793261035
Mean absolute error: 0.19241947565543638
Coverage of cases: 71.32
Instances selection time: 844.0
Test time: 1134.0
Accumulative iteration time: 5577.0
Weighted Recall: 0.7132
Weighted FalsePositiveRate: 0.143913711114211
Kappa statistic: 0.5696004667763974
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.96
Correctly Classified Instances: 71.04
Weighted Precision: 0.7146178712082945
Weighted AreaUnderROC: 0.7824824313316773
Root mean squared error: 0.4383728166956693
Relative absolute error: 43.70388802488492
Root relative squared error: 92.99291741200632
Weighted TruePositiveRate: 0.7104
Weighted MatthewsCorrelation: 0.5674532464536806
Weighted FMeasure: 0.7105430788224976
Iteration time: 860.0
Weighted AreaUnderPRC: 0.602907245154453
Mean absolute error: 0.19423950233282278
Coverage of cases: 71.04
Instances selection time: 860.0
Test time: 1169.0
Accumulative iteration time: 6437.0
Weighted Recall: 0.7104
Weighted FalsePositiveRate: 0.14543513733664554
Kappa statistic: 0.5653330797376054
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.72
Correctly Classified Instances: 71.28
Weighted Precision: 0.7175857900086443
Weighted AreaUnderROC: 0.7842653177899797
Root mean squared error: 0.4365831553902629
Relative absolute error: 43.33755656108697
Root relative squared error: 92.61327291848231
Weighted TruePositiveRate: 0.7128
Weighted MatthewsCorrelation: 0.5713940695062074
Weighted FMeasure: 0.7128303133488675
Iteration time: 874.0
Weighted AreaUnderPRC: 0.6056259916011728
Mean absolute error: 0.19261136249372077
Coverage of cases: 71.28
Instances selection time: 874.0
Test time: 1201.0
Accumulative iteration time: 7311.0
Weighted Recall: 0.7128
Weighted FalsePositiveRate: 0.1442693644200406
Kappa statistic: 0.568911688137026
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7150355257237754
Weighted AreaUnderROC: 0.781218311167262
Root mean squared error: 0.43964179969779577
Relative absolute error: 43.92737920937066
Root relative squared error: 93.26210935781056
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.5661648337034414
Weighted FMeasure: 0.7087064401870941
Iteration time: 889.0
Weighted AreaUnderPRC: 0.6017114192920738
Mean absolute error: 0.19523279648609276
Coverage of cases: 70.88
Instances selection time: 888.0
Test time: 1232.0
Accumulative iteration time: 8200.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14636337766547577
Kappa statistic: 0.5628617949134886
Training time: 1.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.08
Correctly Classified Instances: 70.92
Weighted Precision: 0.7170557923025382
Weighted AreaUnderROC: 0.7814794108867562
Root mean squared error: 0.43936704702956014
Relative absolute error: 43.86059743954205
Root relative squared error: 93.203825515353
Weighted TruePositiveRate: 0.7092
Weighted MatthewsCorrelation: 0.5676993714958558
Weighted FMeasure: 0.7087826405019487
Iteration time: 908.0
Weighted AreaUnderPRC: 0.6024880584175453
Mean absolute error: 0.1949359886201878
Coverage of cases: 70.92
Instances selection time: 908.0
Test time: 1264.0
Accumulative iteration time: 9108.0
Weighted Recall: 0.7092
Weighted FalsePositiveRate: 0.14624117822648783
Kappa statistic: 0.5634164351432596
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.7135734375900346
Weighted AreaUnderROC: 0.7775289450270351
Root mean squared error: 0.4433039012865615
Relative absolute error: 44.63070539419259
Root relative squared error: 94.03895841785362
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.5608272108515943
Weighted FMeasure: 0.7033642235628397
Iteration time: 912.0
Weighted AreaUnderPRC: 0.5974356081346354
Mean absolute error: 0.1983586906408569
Coverage of cases: 70.4
Instances selection time: 912.0
Test time: 1297.0
Accumulative iteration time: 10020.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.14894210994592977
Kappa statistic: 0.5555629228007991
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.56
Correctly Classified Instances: 70.44
Weighted Precision: 0.7147078682963989
Weighted AreaUnderROC: 0.7778140767622335
Root mean squared error: 0.44302888889726605
Relative absolute error: 44.56473755046958
Root relative squared error: 93.98061948023928
Weighted TruePositiveRate: 0.7044
Weighted MatthewsCorrelation: 0.5618531023463436
Weighted FMeasure: 0.7036543968103711
Iteration time: 923.0
Weighted AreaUnderPRC: 0.5980138160512506
Mean absolute error: 0.1980655002243102
Coverage of cases: 70.44
Instances selection time: 923.0
Test time: 1329.0
Accumulative iteration time: 10943.0
Weighted Recall: 0.7044
Weighted FalsePositiveRate: 0.1487718464755332
Kappa statistic: 0.5561452797161637
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7139978212334348
Weighted AreaUnderROC: 0.7759728268372205
Root mean squared error: 0.4448471366272416
Relative absolute error: 44.91743119265807
Root relative squared error: 94.36632807016213
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5592060040692142
Weighted FMeasure: 0.7010669726760501
Iteration time: 935.0
Weighted AreaUnderPRC: 0.5959060250483347
Mean absolute error: 0.1996330275229257
Coverage of cases: 70.2
Instances selection time: 935.0
Test time: 1363.0
Accumulative iteration time: 11878.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.15005434632555903
Kappa statistic: 0.5524986911337556
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7144978433378371
Weighted AreaUnderROC: 0.7756533636510161
Root mean squared error: 0.44516783219703826
Relative absolute error: 44.97164750957751
Root relative squared error: 94.43435787379202
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5591456157550443
Weighted FMeasure: 0.7004560833443882
Iteration time: 947.0
Weighted AreaUnderPRC: 0.5956889765956801
Mean absolute error: 0.19987398893145653
Coverage of cases: 70.16
Instances selection time: 947.0
Test time: 1395.0
Accumulative iteration time: 12825.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.1502932726979678
Kappa statistic: 0.5518755151028764
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.96
Correctly Classified Instances: 70.04
Weighted Precision: 0.7138998863770757
Weighted AreaUnderROC: 0.774739604978711
Root mean squared error: 0.4460832134070339
Relative absolute error: 45.145703611458345
Root relative squared error: 94.62853955207963
Weighted TruePositiveRate: 0.7004
Weighted MatthewsCorrelation: 0.5576902618991098
Weighted FMeasure: 0.6991628450490219
Iteration time: 956.0
Weighted AreaUnderPRC: 0.5945601723460286
Mean absolute error: 0.20064757160648247
Coverage of cases: 70.04
Instances selection time: 956.0
Test time: 1425.0
Accumulative iteration time: 13781.0
Weighted Recall: 0.7004
Weighted FalsePositiveRate: 0.15092079004257825
Kappa statistic: 0.5500590269693058
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.88
Correctly Classified Instances: 70.12
Weighted Precision: 0.7158550890731956
Weighted AreaUnderROC: 0.7753201678844093
Root mean squared error: 0.4455073835953616
Relative absolute error: 45.02114216281959
Root relative squared error: 94.50638760268676
Weighted TruePositiveRate: 0.7012
Weighted MatthewsCorrelation: 0.5595862617972727
Weighted FMeasure: 0.6996998564985409
Iteration time: 965.0
Weighted AreaUnderPRC: 0.595640601355591
Mean absolute error: 0.20009396516808797
Coverage of cases: 70.12
Instances selection time: 965.0
Test time: 1456.0
Accumulative iteration time: 14746.0
Weighted Recall: 0.7012
Weighted FalsePositiveRate: 0.15055966423118147
Kappa statistic: 0.5512358855575814
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7168083475718311
Weighted AreaUnderROC: 0.7756087327647081
Root mean squared error: 0.445228255905606
Relative absolute error: 44.956583629893856
Root relative squared error: 94.44717567801383
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5605389802923816
Weighted FMeasure: 0.6999934998189542
Iteration time: 974.0
Weighted AreaUnderPRC: 0.5961981817195615
Mean absolute error: 0.19980703835508476
Coverage of cases: 70.16
Instances selection time: 974.0
Test time: 1489.0
Accumulative iteration time: 15720.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.15038253447058372
Kappa statistic: 0.5518223168935831
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7177575680638789
Weighted AreaUnderROC: 0.775897297645007
Root mean squared error: 0.4449480171057519
Relative absolute error: 44.89223638470609
Root relative squared error: 94.38772805129531
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5614974979907632
Weighted FMeasure: 0.7002462416901151
Iteration time: 982.0
Weighted AreaUnderPRC: 0.5967589009149213
Mean absolute error: 0.19952105059869465
Coverage of cases: 70.2
Instances selection time: 982.0
Test time: 1545.0
Accumulative iteration time: 16702.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.15020540470998597
Kappa statistic: 0.5524087857168777
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.0
Correctly Classified Instances: 70.0
Weighted Precision: 0.7171643885613056
Weighted AreaUnderROC: 0.7743720777610978
Root mean squared error: 0.44645611590230516
Relative absolute error: 45.18686296715846
Root relative squared error: 94.70764411701794
Weighted TruePositiveRate: 0.7
Weighted MatthewsCorrelation: 0.5592918442013349
Weighted FMeasure: 0.6981097798993063
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5949317445878045
Mean absolute error: 0.20083050207626077
Coverage of cases: 70.0
Instances selection time: 1030.0
Test time: 1559.0
Accumulative iteration time: 17732.0
Weighted Recall: 0.7
Weighted FalsePositiveRate: 0.15125584447780416
Kappa statistic: 0.5493780696365895
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.12
Correctly Classified Instances: 69.88
Weighted Precision: 0.7175123419547658
Weighted AreaUnderROC: 0.7734445865083902
Root mean squared error: 0.4473648881196309
Relative absolute error: 45.36212624584741
Root relative squared error: 94.90042381624542
Weighted TruePositiveRate: 0.6988
Weighted MatthewsCorrelation: 0.5584107515326847
Weighted FMeasure: 0.6966648321196872
Iteration time: 999.0
Weighted AreaUnderPRC: 0.5939987609526095
Mean absolute error: 0.20160944998154504
Coverage of cases: 69.88
Instances selection time: 999.0
Test time: 1591.0
Accumulative iteration time: 18731.0
Weighted Recall: 0.6988
Weighted FalsePositiveRate: 0.15191082698321953
Kappa statistic: 0.5475445911930186
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.16
Correctly Classified Instances: 69.84
Weighted Precision: 0.7186799907689587
Weighted AreaUnderROC: 0.7731216901770853
Root mean squared error: 0.44767788995368424
Relative absolute error: 45.41798483206998
Root relative squared error: 94.96682153206031
Weighted TruePositiveRate: 0.6984
Weighted MatthewsCorrelation: 0.5587268622677022
Weighted FMeasure: 0.6960606792794298
Iteration time: 1012.0
Weighted AreaUnderPRC: 0.5939021925350106
Mean absolute error: 0.20185771036475644
Coverage of cases: 69.84
Instances selection time: 1012.0
Test time: 1617.0
Accumulative iteration time: 19743.0
Weighted Recall: 0.6984
Weighted FalsePositiveRate: 0.15215661964582952
Kappa statistic: 0.5469168230624866
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.32
Correctly Classified Instances: 69.68
Weighted Precision: 0.7187900951505639
Weighted AreaUnderROC: 0.7718884683185756
Root mean squared error: 0.448879191252453
Relative absolute error: 45.65344644750767
Root relative squared error: 95.2216560204426
Weighted TruePositiveRate: 0.6968
Weighted MatthewsCorrelation: 0.5574023901913673
Weighted FMeasure: 0.6941542171586079
Iteration time: 1210.0
Weighted AreaUnderPRC: 0.5926076524909859
Mean absolute error: 0.20290420643336837
Coverage of cases: 69.68
Instances selection time: 1210.0
Test time: 1650.0
Accumulative iteration time: 20953.0
Weighted Recall: 0.6968
Weighted FalsePositiveRate: 0.15302306336284874
Kappa statistic: 0.5444757528890333
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.64
Correctly Classified Instances: 69.36
Weighted Precision: 0.7182111942008232
Weighted AreaUnderROC: 0.7694391903270595
Root mean squared error: 0.451256555131928
Relative absolute error: 46.1283489096571
Root relative squared error: 95.72597105660002
Weighted TruePositiveRate: 0.6936
Weighted MatthewsCorrelation: 0.5542473788059534
Weighted FMeasure: 0.6904620179121621
Iteration time: 1174.0
Weighted AreaUnderPRC: 0.5898418238195812
Mean absolute error: 0.2050148840429214
Coverage of cases: 69.36
Instances selection time: 1174.0
Test time: 1679.0
Accumulative iteration time: 22127.0
Weighted Recall: 0.6936
Weighted FalsePositiveRate: 0.15472161934588108
Kappa statistic: 0.539613438767866
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.88
Correctly Classified Instances: 69.12
Weighted Precision: 0.7168341574412902
Weighted AreaUnderROC: 0.7676185392726503
Root mean squared error: 0.45303471794960976
Relative absolute error: 46.48382502543105
Root relative squared error: 96.10317635253097
Weighted TruePositiveRate: 0.6912
Weighted MatthewsCorrelation: 0.5513089050626149
Weighted FMeasure: 0.6879866072636199
Iteration time: 1021.0
Weighted AreaUnderPRC: 0.5875239197701059
Mean absolute error: 0.20659477789080566
Coverage of cases: 69.12
Instances selection time: 1021.0
Test time: 1714.0
Accumulative iteration time: 23148.0
Weighted Recall: 0.6912
Weighted FalsePositiveRate: 0.15596292145469948
Kappa statistic: 0.5359839628364027
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.04
Correctly Classified Instances: 68.96
Weighted Precision: 0.7163530774319202
Weighted AreaUnderROC: 0.766399049994543
Root mean squared error: 0.45422062724696816
Relative absolute error: 46.719840478565
Root relative squared error: 96.35474570434125
Weighted TruePositiveRate: 0.6896
Weighted MatthewsCorrelation: 0.5496017117695777
Weighted FMeasure: 0.6861464699653637
Iteration time: 1026.0
Weighted AreaUnderPRC: 0.5860976761558185
Mean absolute error: 0.20764373546028983
Coverage of cases: 68.96
Instances selection time: 1026.0
Test time: 1745.0
Accumulative iteration time: 24174.0
Weighted Recall: 0.6896
Weighted FalsePositiveRate: 0.15680190001091376
Kappa statistic: 0.5335584274571553
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.04
Correctly Classified Instances: 68.96
Weighted Precision: 0.7170936055729215
Weighted AreaUnderROC: 0.7663887505592413
Root mean squared error: 0.4542338624464008
Relative absolute error: 46.7167155425209
Root relative squared error: 96.357553314122
Weighted TruePositiveRate: 0.6896
Weighted MatthewsCorrelation: 0.550031425046357
Weighted FMeasure: 0.6860719797377411
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5862957852699238
Mean absolute error: 0.20762984685564942
Coverage of cases: 68.96
Instances selection time: 1030.0
Test time: 1777.0
Accumulative iteration time: 25204.0
Weighted Recall: 0.6896
Weighted FalsePositiveRate: 0.15682249888151747
Kappa statistic: 0.5335477731234074
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.7167428874657243
Weighted AreaUnderROC: 0.7657807224927379
Root mean squared error: 0.4548315787019798
Relative absolute error: 46.833365292427054
Root relative squared error: 96.48434807938563
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.5491082460959762
Weighted FMeasure: 0.6851969699517364
Iteration time: 1030.0
Weighted AreaUnderPRC: 0.5855610201411849
Mean absolute error: 0.20814829018856568
Coverage of cases: 68.88
Instances selection time: 1030.0
Test time: 1807.0
Accumulative iteration time: 26234.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.15723855501452402
Kappa statistic: 0.5323370308763354
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.7136277930224992
Weighted AreaUnderROC: 0.7621497198192213
Root mean squared error: 0.45833818614927885
Relative absolute error: 47.548447789277105
Root relative squared error: 97.22821185086893
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.5428766011944177
Weighted FMeasure: 0.6802195554268663
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.5809354199227511
Mean absolute error: 0.21132643461901035
Coverage of cases: 68.4
Instances selection time: 1034.0
Test time: 1839.0
Accumulative iteration time: 27268.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.15970056036155716
Kappa statistic: 0.5250933462091149
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.7139391305205621
Weighted AreaUnderROC: 0.7624485841348222
Root mean squared error: 0.45805990842512634
Relative absolute error: 47.48587257617867
Root relative squared error: 97.16918023112852
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5434374108559449
Weighted FMeasure: 0.6808366323679705
Iteration time: 1040.0
Weighted AreaUnderPRC: 0.5814167244940515
Mean absolute error: 0.2110483225607951
Coverage of cases: 68.44
Instances selection time: 1039.0
Test time: 1875.0
Accumulative iteration time: 28308.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.1595028317303557
Kappa statistic: 0.5256944938721412
Training time: 1.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.76
Correctly Classified Instances: 68.24
Weighted Precision: 0.7130050688095569
Weighted AreaUnderROC: 0.7609302305411143
Root mean squared error: 0.4595205036709607
Relative absolute error: 47.782411604713765
Root relative squared error: 97.47901927199801
Weighted TruePositiveRate: 0.6824
Weighted MatthewsCorrelation: 0.5410879063261834
Weighted FMeasure: 0.678637062134571
Iteration time: 1043.0
Weighted AreaUnderPRC: 0.5795820812447809
Mean absolute error: 0.21236627379872886
Coverage of cases: 68.24
Instances selection time: 1043.0
Test time: 1939.0
Accumulative iteration time: 29351.0
Weighted Recall: 0.6824
Weighted FalsePositiveRate: 0.16053953891777145
Kappa statistic: 0.5226691284274941
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.88
Correctly Classified Instances: 68.12
Weighted Precision: 0.712655070224483
Weighted AreaUnderROC: 0.760016471868809
Root mean squared error: 0.46039890779465753
Relative absolute error: 47.95939447907367
Root relative squared error: 97.66535692574448
Weighted TruePositiveRate: 0.6812
Weighted MatthewsCorrelation: 0.5398159181959639
Weighted FMeasure: 0.6772878002552688
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.5785265694038152
Mean absolute error: 0.21315286435143954
Coverage of cases: 68.12
Instances selection time: 1042.0
Test time: 1945.0
Accumulative iteration time: 30393.0
Weighted Recall: 0.6812
Weighted FalsePositiveRate: 0.1611670562623819
Kappa statistic: 0.5208502869367316
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.88
Correctly Classified Instances: 68.12
Weighted Precision: 0.7131420750005264
Weighted AreaUnderROC: 0.7600096055786079
Root mean squared error: 0.46040963709438026
Relative absolute error: 47.95695538057658
Root relative squared error: 97.66763295392188
Weighted TruePositiveRate: 0.6812
Weighted MatthewsCorrelation: 0.5401394777082663
Weighted FMeasure: 0.677146618383369
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.5786312893809838
Mean absolute error: 0.2131420239136747
Coverage of cases: 68.12
Instances selection time: 1042.0
Test time: 1969.0
Accumulative iteration time: 31435.0
Weighted Recall: 0.6812
Weighted FalsePositiveRate: 0.16118078884278433
Kappa statistic: 0.5208415297287471
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.711056473332622
Weighted AreaUnderROC: 0.7572820621420946
Root mean squared error: 0.4630122933551985
Relative absolute error: 48.493207222698956
Root relative squared error: 98.21973972125853
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5356619905213316
Weighted FMeasure: 0.6731416898178555
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.57528290236698
Mean absolute error: 0.2155253654342186
Coverage of cases: 67.76
Instances selection time: 1042.0
Test time: 2004.0
Accumulative iteration time: 32477.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.16303587571581074
Kappa statistic: 0.5154019217981354
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.36
Correctly Classified Instances: 67.64
Weighted Precision: 0.7104824128314544
Weighted AreaUnderROC: 0.75637173661489
Root mean squared error: 0.46388325981957745
Relative absolute error: 48.670498732036464
Root relative squared error: 98.40449960920307
Weighted TruePositiveRate: 0.6764
Weighted MatthewsCorrelation: 0.534241754022381
Weighted FMeasure: 0.6717845521110163
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.5741818872529935
Mean absolute error: 0.21631332769794084
Coverage of cases: 67.64
Instances selection time: 1041.0
Test time: 2034.0
Accumulative iteration time: 33519.0
Weighted Recall: 0.6764
Weighted FalsePositiveRate: 0.16365652677022
Kappa statistic: 0.5135870937640542
Training time: 1.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.48
Correctly Classified Instances: 67.52
Weighted Precision: 0.7078135025594925
Weighted AreaUnderROC: 0.7554888762484903
Root mean squared error: 0.46475233396813753
Relative absolute error: 48.847880299252196
Root relative squared error: 98.5888580763433
Weighted TruePositiveRate: 0.6752
Weighted MatthewsCorrelation: 0.5314539642293232
Weighted FMeasure: 0.6710349647427394
Iteration time: 1039.0
Weighted AreaUnderPRC: 0.5727029922517842
Mean absolute error: 0.2171016902188997
Coverage of cases: 67.52
Instances selection time: 1039.0
Test time: 2067.0
Accumulative iteration time: 34558.0
Weighted Recall: 0.6752
Weighted FalsePositiveRate: 0.16422224750301934
Kappa statistic: 0.5118078764388141
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.24
Correctly Classified Instances: 67.76
Weighted Precision: 0.7095429311292366
Weighted AreaUnderROC: 0.7573026610126983
Root mean squared error: 0.4630415125519577
Relative absolute error: 48.486672117741314
Root relative squared error: 98.22593804890931
Weighted TruePositiveRate: 0.6776
Weighted MatthewsCorrelation: 0.5346653956840038
Weighted FMeasure: 0.6736414368478456
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5750005094201658
Mean absolute error: 0.21549632052329576
Coverage of cases: 67.76
Instances selection time: 1041.0
Test time: 2097.0
Accumulative iteration time: 35599.0
Weighted Recall: 0.6776
Weighted FalsePositiveRate: 0.1629946779746034
Kappa statistic: 0.5154284922755935
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.44
Correctly Classified Instances: 68.56
Weighted Precision: 0.7153125687124859
Weighted AreaUnderROC: 0.7633486102267248
Root mean squared error: 0.45726952836281387
Relative absolute error: 47.28753016894555
Root relative squared error: 97.00151530059577
Weighted TruePositiveRate: 0.6856
Weighted MatthewsCorrelation: 0.5452390836166785
Weighted FMeasure: 0.6824724210533939
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5828842059166301
Mean absolute error: 0.2101668007508701
Coverage of cases: 68.56
Instances selection time: 1041.0
Test time: 2132.0
Accumulative iteration time: 36640.0
Weighted Recall: 0.6856
Weighted FalsePositiveRate: 0.15890277954655016
Kappa statistic: 0.5275022542831379
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.2
Correctly Classified Instances: 68.8
Weighted Precision: 0.7162048566416168
Weighted AreaUnderROC: 0.7651761275713354
Root mean squared error: 0.4555295731650446
Relative absolute error: 46.92636579572397
Root relative squared error: 96.63241506480475
Weighted TruePositiveRate: 0.688
Weighted MatthewsCorrelation: 0.5479218822297187
Weighted FMeasure: 0.6851350161869884
Iteration time: 1042.0
Weighted AreaUnderPRC: 0.585006045348136
Mean absolute error: 0.20856162575877416
Coverage of cases: 68.8
Instances selection time: 1042.0
Test time: 2189.0
Accumulative iteration time: 37682.0
Weighted Recall: 0.688
Weighted FalsePositiveRate: 0.15764774485732932
Kappa statistic: 0.5311369544147302
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7177623993062978
Weighted AreaUnderROC: 0.7682265673391535
Root mean squared error: 0.45260846614568895
Relative absolute error: 46.32579890880622
Root relative squared error: 96.01275469021735
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5521194804816915
Weighted FMeasure: 0.6900652619945088
Iteration time: 1227.0
Weighted AreaUnderPRC: 0.5888879988016598
Mean absolute error: 0.20589243959469528
Coverage of cases: 69.2
Instances selection time: 1226.0
Test time: 2201.0
Accumulative iteration time: 38909.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.15554686532169293
Kappa statistic: 0.5372135592340198
Training time: 1.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.7185020335972543
Weighted AreaUnderROC: 0.7703735478699684
Root mean squared error: 0.45055453304732074
Relative absolute error: 45.90483499616218
Root relative squared error: 95.57704968362945
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.554940976079633
Weighted FMeasure: 0.6931687145338332
Iteration time: 1049.0
Weighted AreaUnderPRC: 0.5913430196042411
Mean absolute error: 0.20402148887183288
Coverage of cases: 69.48
Instances selection time: 1049.0
Test time: 2232.0
Accumulative iteration time: 39958.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.15405290426006327
Kappa statistic: 0.5414730697458523
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.44
Correctly Classified Instances: 69.56
Weighted Precision: 0.7189583207841395
Weighted AreaUnderROC: 0.7709850090815722
Root mean squared error: 0.4499714543022885
Relative absolute error: 45.78321995464989
Root relative squared error: 95.45336000325605
Weighted TruePositiveRate: 0.6956
Weighted MatthewsCorrelation: 0.5559547935297007
Weighted FMeasure: 0.6938368060171468
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5920097872586493
Mean absolute error: 0.20348097757622266
Coverage of cases: 69.56
Instances selection time: 1035.0
Test time: 2261.0
Accumulative iteration time: 40993.0
Weighted Recall: 0.6956
Weighted FalsePositiveRate: 0.1536299818368555
Kappa statistic: 0.5426833298999528
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.7114452022694989
Weighted AreaUnderROC: 0.7662306451927657
Root mean squared error: 0.4546847968482629
Relative absolute error: 46.73924050632829
Root relative squared error: 96.45321094615011
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5457192086198069
Weighted FMeasure: 0.6864959147352194
Iteration time: 1032.0
Weighted AreaUnderPRC: 0.5843509556258283
Mean absolute error: 0.20772995780590445
Coverage of cases: 68.92
Instances selection time: 1032.0
Test time: 2587.0
Accumulative iteration time: 42025.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.15673870961446867
Kappa statistic: 0.5330981157354228
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.96
Correctly Classified Instances: 69.04
Weighted Precision: 0.7117648414999634
Weighted AreaUnderROC: 0.7671581364454733
Root mean squared error: 0.4538136011152422
Relative absolute error: 46.55788701394063
Root relative squared error: 96.2684024229822
Weighted TruePositiveRate: 0.6904
Weighted MatthewsCorrelation: 0.5468982903177664
Weighted FMeasure: 0.6877911479243447
Iteration time: 1027.0
Weighted AreaUnderPRC: 0.5853911750961234
Mean absolute error: 0.20692394228418157
Coverage of cases: 69.04
Instances selection time: 1027.0
Test time: 2328.0
Accumulative iteration time: 43052.0
Weighted Recall: 0.6904
Weighted FalsePositiveRate: 0.1560837271090533
Kappa statistic: 0.5349326842099191
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7118517069415938
Weighted AreaUnderROC: 0.7684016577392847
Root mean squared error: 0.4526466286734669
Relative absolute error: 46.31670281995639
Root relative squared error: 96.02085018487104
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5483579112859939
Weighted FMeasure: 0.6893597074236589
Iteration time: 1022.0
Weighted AreaUnderPRC: 0.5866109023815762
Mean absolute error: 0.20585201253314048
Coverage of cases: 69.2
Instances selection time: 1022.0
Test time: 2359.0
Accumulative iteration time: 44074.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.15519668452143043
Kappa statistic: 0.5373826093351142
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.52
Correctly Classified Instances: 69.48
Weighted Precision: 0.7139495100174376
Weighted AreaUnderROC: 0.7705246062543952
Root mean squared error: 0.45059140582529716
Relative absolute error: 45.89593727726275
Root relative squared error: 95.58487158103394
Weighted TruePositiveRate: 0.6948
Weighted MatthewsCorrelation: 0.5521840366520994
Weighted FMeasure: 0.6919315171419428
Iteration time: 1017.0
Weighted AreaUnderPRC: 0.5892860495275197
Mean absolute error: 0.20398194345450207
Coverage of cases: 69.48
Instances selection time: 1017.0
Test time: 2390.0
Accumulative iteration time: 45091.0
Weighted Recall: 0.6948
Weighted FalsePositiveRate: 0.15375078749120935
Kappa statistic: 0.5416091525332491
Training time: 0.0
		
Time end:Wed Nov 01 15.27.42 EET 2017