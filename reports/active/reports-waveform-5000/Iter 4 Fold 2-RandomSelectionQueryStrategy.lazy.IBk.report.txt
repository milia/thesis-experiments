Wed Nov 01 17.20.42 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.20.42 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7192113648934165
Weighted AreaUnderROC: 0.787700450500914
Root mean squared error: 0.4329166027194479
Relative absolute error: 42.7634194831012
Root relative squared error: 91.83547964134902
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.576526673538004
Weighted FMeasure: 0.7174499178476562
Iteration time: 10.0
Weighted AreaUnderPRC: 0.609643062133897
Mean absolute error: 0.19005964214711735
Coverage of cases: 71.72
Instances selection time: 10.0
Test time: 954.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.1417990989981721
Kappa statistic: 0.5756555191911741
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7262613422133423
Weighted AreaUnderROC: 0.7930903076170287
Root mean squared error: 0.4274186770348481
Relative absolute error: 41.6764818355628
Root relative squared error: 90.66919348113697
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5872407476876582
Weighted FMeasure: 0.7246470851779678
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6176465428260628
Mean absolute error: 0.18522880815805773
Coverage of cases: 72.44
Instances selection time: 10.0
Test time: 1008.0
Accumulative iteration time: 20.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13821938476594264
Kappa statistic: 0.586455424816516
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32
Weighted Precision: 0.7252717210952031
Weighted AreaUnderROC: 0.7921834152349247
Root mean squared error: 0.42839303832784237
Relative absolute error: 41.84309392265231
Root relative squared error: 90.87588672441755
Weighted TruePositiveRate: 0.7232
Weighted MatthewsCorrelation: 0.5855575757152541
Weighted FMeasure: 0.7234135669258756
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6163358459907222
Mean absolute error: 0.1859693063229
Coverage of cases: 72.32
Instances selection time: 9.0
Test time: 1022.0
Accumulative iteration time: 29.0
Weighted Recall: 0.7232
Weighted FalsePositiveRate: 0.1388331695301506
Kappa statistic: 0.5846453219178822
Training time: 0.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7239593488959921
Weighted AreaUnderROC: 0.7912833891430219
Root mean squared error: 0.42936240693751276
Relative absolute error: 42.01065719360501
Root relative squared error: 91.08152085962774
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.5837186703709505
Weighted FMeasure: 0.7221874951267101
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6149103225870378
Mean absolute error: 0.1867140319715787
Coverage of cases: 72.2
Instances selection time: 9.0
Test time: 1047.0
Accumulative iteration time: 38.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.13943322171395614
Kappa statistic: 0.5828389424589044
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.52
Correctly Classified Instances: 72.48
Weighted Precision: 0.7255527838104652
Weighted AreaUnderROC: 0.7934475353993399
Root mean squared error: 0.4272335059518293
Relative absolute error: 41.58216123499027
Root relative squared error: 90.6299127625923
Weighted TruePositiveRate: 0.7248
Weighted MatthewsCorrelation: 0.5873362613841379
Weighted FMeasure: 0.724928463414016
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6177052975340896
Mean absolute error: 0.18480960548884648
Coverage of cases: 72.48
Instances selection time: 9.0
Test time: 1077.0
Accumulative iteration time: 47.0
Weighted Recall: 0.7248
Weighted FalsePositiveRate: 0.13790492920132036
Kappa statistic: 0.5871102542848714
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7261725412779565
Weighted AreaUnderROC: 0.7940589966109437
Root mean squared error: 0.4266482364442732
Relative absolute error: 41.452736318408704
Root relative squared error: 90.5057583513079
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5884507841191595
Weighted FMeasure: 0.725709050553934
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6185714697042404
Mean absolute error: 0.18423438363737288
Coverage of cases: 72.56
Instances selection time: 9.0
Test time: 1113.0
Accumulative iteration time: 56.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13748200677811256
Kappa statistic: 0.5883255363230101
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7258600292993255
Weighted AreaUnderROC: 0.7937601322953429
Root mean squared error: 0.42699293898190105
Relative absolute error: 41.50304975923047
Root relative squared error: 90.57888080186257
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5878959163385165
Weighted FMeasure: 0.725317471866123
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6181313765597615
Mean absolute error: 0.18445799892991405
Coverage of cases: 72.52
Instances selection time: 9.0
Test time: 1143.0
Accumulative iteration time: 65.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.13767973540931402
Kappa statistic: 0.5877254277753758
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7272740941764628
Weighted AreaUnderROC: 0.7949658889930477
Root mean squared error: 0.4257797599356152
Relative absolute error: 41.255365474340586
Root relative squared error: 90.32152666273592
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5902024074780713
Weighted FMeasure: 0.7269137740214053
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6198877700344837
Mean absolute error: 0.18335717988595904
Coverage of cases: 72.68
Instances selection time: 9.0
Test time: 1177.0
Accumulative iteration time: 74.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13686822201390458
Kappa statistic: 0.5901333392863015
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.68
Correctly Classified Instances: 72.32
Weighted Precision: 0.7239881311199182
Weighted AreaUnderROC: 0.7922417787016351
Root mean squared error: 0.42860575216182556
Relative absolute error: 41.78461538461652
Root relative squared error: 90.92101014275606
Weighted TruePositiveRate: 0.7232
Weighted MatthewsCorrelation: 0.584915706063559
Weighted FMeasure: 0.7234069580237641
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6160347600766838
Mean absolute error: 0.1857094017094076
Coverage of cases: 72.32
Instances selection time: 9.0
Test time: 1204.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7232
Weighted FalsePositiveRate: 0.13871644259672974
Kappa statistic: 0.5847135207435595
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.44
Correctly Classified Instances: 71.56
Weighted Precision: 0.7161999296090235
Weighted AreaUnderROC: 0.7865633567052215
Root mean squared error: 0.4344784124216677
Relative absolute error: 42.91185944363125
Root relative squared error: 92.1667895107578
Weighted TruePositiveRate: 0.7156
Weighted MatthewsCorrelation: 0.5734782176426918
Weighted FMeasure: 0.7157366710983367
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6073558877390407
Mean absolute error: 0.1907193753050287
Coverage of cases: 71.56
Instances selection time: 8.0
Test time: 1240.0
Accumulative iteration time: 91.0
Weighted Recall: 0.7156
Weighted FalsePositiveRate: 0.14247328658955702
Kappa statistic: 0.5733130924106622
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7156858181293921
Weighted AreaUnderROC: 0.786271358679822
Root mean squared error: 0.4348108459051543
Relative absolute error: 42.964438122330066
Root relative squared error: 92.23730930189785
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5728251626848815
Weighted FMeasure: 0.7153363529583
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6068784133632793
Mean absolute error: 0.19095305832146783
Coverage of cases: 71.52
Instances selection time: 9.0
Test time: 1271.0
Accumulative iteration time: 100.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.14265728264035601
Kappa statistic: 0.5727207653387243
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.48
Correctly Classified Instances: 71.52
Weighted Precision: 0.7156435273184758
Weighted AreaUnderROC: 0.7862747918249227
Root mean squared error: 0.4348363649105049
Relative absolute error: 42.95767634854968
Root relative squared error: 92.24272270041762
Weighted TruePositiveRate: 0.7152
Weighted MatthewsCorrelation: 0.5728068886298704
Weighted FMeasure: 0.7153198570496019
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6068811890742862
Mean absolute error: 0.19092300599355502
Coverage of cases: 71.52
Instances selection time: 9.0
Test time: 1303.0
Accumulative iteration time: 109.0
Weighted Recall: 0.7152
Weighted FalsePositiveRate: 0.14265041635015477
Kappa statistic: 0.5727266115227951
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.8
Correctly Classified Instances: 71.2
Weighted Precision: 0.7126237604255413
Weighted AreaUnderROC: 0.7838632784295132
Root mean squared error: 0.43729668113900244
Relative absolute error: 43.42934051143837
Root relative squared error: 92.7646345871278
Weighted TruePositiveRate: 0.712
Weighted MatthewsCorrelation: 0.5680863515739538
Weighted FMeasure: 0.7121491344101717
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6034231780877886
Mean absolute error: 0.19301929116194919
Coverage of cases: 71.2
Instances selection time: 8.0
Test time: 1335.0
Accumulative iteration time: 117.0
Weighted Recall: 0.712
Weighted FalsePositiveRate: 0.1442734431409736
Kappa statistic: 0.5679119923145945
Training time: 0.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.6
Correctly Classified Instances: 71.4
Weighted Precision: 0.7146412341552445
Weighted AreaUnderROC: 0.7853610331526173
Root mean squared error: 0.4357986114585542
Relative absolute error: 43.12450851900139
Root relative squared error: 92.44684601820731
Weighted TruePositiveRate: 0.714
Weighted MatthewsCorrelation: 0.5710888179180077
Weighted FMeasure: 0.7141574566643298
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6056379129042531
Mean absolute error: 0.19166448230667374
Coverage of cases: 71.4
Instances selection time: 8.0
Test time: 1366.0
Accumulative iteration time: 125.0
Weighted Recall: 0.714
Weighted FalsePositiveRate: 0.1432779336947652
Kappa statistic: 0.5709126034790765
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.88
Correctly Classified Instances: 72.12
Weighted Precision: 0.7213426628694558
Weighted AreaUnderROC: 0.7908023874452415
Root mean squared error: 0.4302996583931996
Relative absolute error: 42.042911877393585
Root relative squared error: 91.28034191762569
Weighted TruePositiveRate: 0.7212
Weighted MatthewsCorrelation: 0.5816907345688054
Weighted FMeasure: 0.721252451907615
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6134063970791014
Mean absolute error: 0.18685738612175015
Coverage of cases: 72.12
Instances selection time: 8.0
Test time: 1398.0
Accumulative iteration time: 133.0
Weighted Recall: 0.7212
Weighted FalsePositiveRate: 0.13959522510951733
Kappa statistic: 0.5817548629848133
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7186838298040867
Weighted AreaUnderROC: 0.788686305220332
Root mean squared error: 0.4324755081666417
Relative absolute error: 42.45579078455901
Root relative squared error: 91.74190935651893
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.577535208185617
Weighted FMeasure: 0.718501112337744
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6103973692464115
Mean absolute error: 0.1886924034869298
Coverage of cases: 71.84
Instances selection time: 7.0
Test time: 1434.0
Accumulative iteration time: 140.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14102738955933594
Kappa statistic: 0.5775409233227174
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.12
Correctly Classified Instances: 71.88
Weighted Precision: 0.7191097413241643
Weighted AreaUnderROC: 0.7889851695359328
Root mean squared error: 0.4321877668988576
Relative absolute error: 42.39076549210267
Root relative squared error: 91.6808702160157
Weighted TruePositiveRate: 0.7188
Weighted MatthewsCorrelation: 0.5781506716105849
Weighted FMeasure: 0.7189039980023814
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6108153340302862
Mean absolute error: 0.18840340218712387
Coverage of cases: 71.88
Instances selection time: 7.0
Test time: 1460.0
Accumulative iteration time: 147.0
Weighted Recall: 0.7188
Weighted FalsePositiveRate: 0.14082966092813448
Kappa statistic: 0.5781371593112511
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.44
Correctly Classified Instances: 71.56
Weighted Precision: 0.7159126515151515
Weighted AreaUnderROC: 0.7865873887209257
Root mean squared error: 0.43465858007496505
Relative absolute error: 42.86405693950259
Root relative squared error: 92.20500884157691
Weighted TruePositiveRate: 0.7156
Weighted MatthewsCorrelation: 0.5733642122844833
Weighted FMeasure: 0.7156575713810054
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6072765991245791
Mean absolute error: 0.1905069197311235
Coverage of cases: 71.56
Instances selection time: 7.0
Test time: 1493.0
Accumulative iteration time: 154.0
Weighted Recall: 0.7156
Weighted FalsePositiveRate: 0.14242522255814843
Kappa statistic: 0.573340335912574
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7176488301983686
Weighted AreaUnderROC: 0.7877759796931275
Root mean squared error: 0.43345199336140533
Relative absolute error: 42.620162224799095
Root relative squared error: 91.94905314740261
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.575821733499933
Weighted FMeasure: 0.7172663718723583
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6090977532668749
Mean absolute error: 0.1894229432213302
Coverage of cases: 71.72
Instances selection time: 7.0
Test time: 1527.0
Accumulative iteration time: 161.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14164804061374517
Kappa statistic: 0.5757309968893339
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7197003937943937
Weighted AreaUnderROC: 0.789270301271131
Root mean squared error: 0.4319335036036138
Relative absolute error: 42.316647791620326
Root relative squared error: 91.6269328259336
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5788342902570041
Weighted FMeasure: 0.7192951640518565
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6113440732265152
Mean absolute error: 0.1880739901849801
Coverage of cases: 71.92
Instances selection time: 7.0
Test time: 1557.0
Accumulative iteration time: 168.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.14065939745773798
Kappa statistic: 0.5787276422598345
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.8
Correctly Classified Instances: 72.2
Weighted Precision: 0.7223100152007239
Weighted AreaUnderROC: 0.7913863834960403
Root mean squared error: 0.42979071736167557
Relative absolute error: 41.893687707641355
Root relative squared error: 91.17237922124127
Weighted TruePositiveRate: 0.722
Weighted MatthewsCorrelation: 0.5829631860322538
Weighted FMeasure: 0.7220424029003454
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6143739703548253
Mean absolute error: 0.186194167589518
Coverage of cases: 72.2
Instances selection time: 7.0
Test time: 1590.0
Accumulative iteration time: 175.0
Weighted Recall: 0.722
Weighted FalsePositiveRate: 0.13922723300791937
Kappa statistic: 0.5829435806075846
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7240613582409393
Weighted AreaUnderROC: 0.79291503652505
Root mean squared error: 0.42825727988674067
Relative absolute error: 41.5904658721567
Root relative squared error: 90.84708801012566
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5858828480166508
Weighted FMeasure: 0.7239545620691346
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6164867541748991
Mean absolute error: 0.18484651498736396
Coverage of cases: 72.4
Instances selection time: 7.0
Test time: 1622.0
Accumulative iteration time: 182.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13816992694989993
Kappa statistic: 0.585970421726928
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7257280195842468
Weighted AreaUnderROC: 0.7941036274972517
Root mean squared error: 0.42702881635054946
Relative absolute error: 41.34718981972354
Root relative squared error: 90.58649154106129
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5882898973528683
Weighted FMeasure: 0.725629542388767
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6183010216592225
Mean absolute error: 0.18376528808766102
Coverage of cases: 72.56
Instances selection time: 6.0
Test time: 1652.0
Accumulative iteration time: 189.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13739274500549667
Kappa statistic: 0.5883574526263036
Training time: 1.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7274068611126622
Weighted AreaUnderROC: 0.7952990847596546
Root mean squared error: 0.42579603154679474
Relative absolute error: 41.10404984423612
Root relative squared error: 90.3249783927177
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5907302684104537
Weighted FMeasure: 0.7272263872457816
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6201015764673977
Mean absolute error: 0.18268466597438363
Coverage of cases: 72.72
Instances selection time: 7.0
Test time: 1683.0
Accumulative iteration time: 196.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.1366018304806909
Kappa statistic: 0.5907521016259742
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7267930882208303
Weighted AreaUnderROC: 0.7950379850401605
Root mean squared error: 0.4261215245290985
Relative absolute error: 41.16012207527832
Root relative squared error: 90.39402588122239
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5900800982628102
Weighted FMeasure: 0.7267105631839925
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6195567439960259
Mean absolute error: 0.18293387589012675
Coverage of cases: 72.68
Instances selection time: 7.0
Test time: 1713.0
Accumulative iteration time: 203.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13672402991967886
Kappa statistic: 0.5901912792704229
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7272498850407267
Weighted AreaUnderROC: 0.7953265499204595
Root mean squared error: 0.42582236510631716
Relative absolute error: 41.096709870389624
Root relative squared error: 90.33056458427102
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5906918736042371
Weighted FMeasure: 0.7271499557561306
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6200196661139559
Mean absolute error: 0.1826520438683992
Coverage of cases: 72.72
Instances selection time: 6.0
Test time: 1761.0
Accumulative iteration time: 209.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.1365469001590811
Kappa statistic: 0.5907782294018041
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.44
Correctly Classified Instances: 72.56
Weighted Precision: 0.7255753773294147
Weighted AreaUnderROC: 0.7941413920933584
Root mean squared error: 0.4270817053265642
Relative absolute error: 41.33255131964686
Root relative squared error: 90.5977109871383
Weighted TruePositiveRate: 0.7256
Weighted MatthewsCorrelation: 0.5882867037605022
Weighted FMeasure: 0.7254650328553466
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6182031655548365
Mean absolute error: 0.18370022808732026
Coverage of cases: 72.56
Instances selection time: 6.0
Test time: 1788.0
Accumulative iteration time: 215.0
Weighted Recall: 0.7256
Weighted FalsePositiveRate: 0.13731721581328316
Kappa statistic: 0.5883949948831378
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.16
Correctly Classified Instances: 72.84
Weighted Precision: 0.7284489142405463
Weighted AreaUnderROC: 0.7962334423025634
Root mean squared error: 0.42490904884220443
Relative absolute error: 40.91045062320376
Root relative squared error: 90.13682094715439
Weighted TruePositiveRate: 0.7284
Weighted MatthewsCorrelation: 0.5925084923930091
Weighted FMeasure: 0.7282999752586875
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6213733341883374
Mean absolute error: 0.18182422499201756
Coverage of cases: 72.84
Instances selection time: 5.0
Test time: 1825.0
Accumulative iteration time: 220.0
Weighted Recall: 0.7284
Weighted FalsePositiveRate: 0.13593311539487313
Kappa statistic: 0.5925876146634859
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.92
Correctly Classified Instances: 73.08
Weighted Precision: 0.7308802019260873
Weighted AreaUnderROC: 0.7980403607765701
Root mean squared error: 0.4230389409542608
Relative absolute error: 40.548259642523014
Root relative squared error: 89.74011115641977
Weighted TruePositiveRate: 0.7308
Weighted MatthewsCorrelation: 0.5961399804501081
Weighted FMeasure: 0.730696213578181
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6240318998022759
Mean absolute error: 0.18021448730010312
Coverage of cases: 73.08
Instances selection time: 6.0
Test time: 2127.0
Accumulative iteration time: 226.0
Weighted Recall: 0.7308
Weighted FalsePositiveRate: 0.13471927844685963
Kappa statistic: 0.5961877241068129
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7320227584924783
Weighted AreaUnderROC: 0.7989369537233725
Root mean squared error: 0.4221059705139891
Relative absolute error: 40.365650969530535
Root relative squared error: 89.54219823893096
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.5978981015846877
Weighted FMeasure: 0.7319351848183262
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6253659034992546
Mean absolute error: 0.17940289319791433
Coverage of cases: 73.2
Instances selection time: 5.0
Test time: 1908.0
Accumulative iteration time: 232.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13412609255325533
Kappa statistic: 0.5979822790588609
Training time: 1.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.76
Correctly Classified Instances: 73.24
Weighted Precision: 0.7323814478501331
Weighted AreaUnderROC: 0.7992461174742749
Root mean squared error: 0.4218014008891497
Relative absolute error: 40.30281051677229
Root relative squared error: 89.47758926481075
Weighted TruePositiveRate: 0.7324
Weighted MatthewsCorrelation: 0.5984829798064062
Weighted FMeasure: 0.7323417287330334
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6257939198041966
Mean absolute error: 0.17912360229676658
Coverage of cases: 73.24
Instances selection time: 5.0
Test time: 1914.0
Accumulative iteration time: 237.0
Weighted Recall: 0.7324
Weighted FalsePositiveRate: 0.1339077650514502
Kappa statistic: 0.5985896275559761
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.7291746590125425
Weighted AreaUnderROC: 0.7968414703690667
Root mean squared error: 0.4243261005473575
Relative absolute error: 40.77862867319618
Root relative squared error: 90.01315893944417
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.593666627692247
Weighted FMeasure: 0.7291612791134193
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6222047333093489
Mean absolute error: 0.18123834965865054
Coverage of cases: 72.92
Instances selection time: 6.0
Test time: 1935.0
Accumulative iteration time: 243.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13551705926186655
Kappa statistic: 0.5937876511445948
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7271657649861636
Weighted AreaUnderROC: 0.795347148791063
Root mean squared error: 0.42590005541379233
Relative absolute error: 41.075065616796785
Root relative squared error: 90.34704518724547
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.5906700533410887
Weighted FMeasure: 0.7271719131569441
Iteration time: 6.0
Weighted AreaUnderPRC: 0.619921770775217
Mean absolute error: 0.18255584718576431
Coverage of cases: 72.72
Instances selection time: 6.0
Test time: 1972.0
Accumulative iteration time: 249.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.13650570241787371
Kappa statistic: 0.5907875599418222
Training time: 0.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7259660834820651
Weighted AreaUnderROC: 0.7944642884246634
Root mean squared error: 0.42684534687569214
Relative absolute error: 41.251934651761076
Root relative squared error: 90.5475717881176
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5889106083995239
Weighted FMeasure: 0.7259195641116383
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6185133015343125
Mean absolute error: 0.18334193178560565
Coverage of cases: 72.6
Instances selection time: 5.0
Test time: 2008.0
Accumulative iteration time: 254.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.13707142315067314
Kappa statistic: 0.5889950021792265
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.64
Correctly Classified Instances: 72.36
Weighted Precision: 0.723552703525825
Weighted AreaUnderROC: 0.7926539368055561
Root mean squared error: 0.42871996594528416
Relative absolute error: 41.60845308537617
Root relative squared error: 90.94523854499263
Weighted TruePositiveRate: 0.7236
Weighted MatthewsCorrelation: 0.5852819973051135
Weighted FMeasure: 0.7235385415786099
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6158683327759399
Mean absolute error: 0.18492645815722827
Coverage of cases: 72.36
Instances selection time: 5.0
Test time: 2036.0
Accumulative iteration time: 259.0
Weighted Recall: 0.7236
Weighted FalsePositiveRate: 0.13829212638888788
Kappa statistic: 0.5853911771242493
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.84
Correctly Classified Instances: 72.16
Weighted Precision: 0.7215055446801771
Weighted AreaUnderROC: 0.7911699146628544
Root mean squared error: 0.43027728161922063
Relative absolute error: 41.90523690773162
Root relative squared error: 91.2755950870392
Weighted TruePositiveRate: 0.7216
Weighted MatthewsCorrelation: 0.582279972482313
Weighted FMeasure: 0.7214965398966852
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6136285135852844
Mean absolute error: 0.18624549736769697
Coverage of cases: 72.16
Instances selection time: 5.0
Test time: 2067.0
Accumulative iteration time: 264.0
Weighted Recall: 0.7216
Weighted FalsePositiveRate: 0.1392601706742914
Kappa statistic: 0.5824082516129482
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7239002440360747
Weighted AreaUnderROC: 0.7929802662819617
Root mean squared error: 0.4284273427305643
Relative absolute error: 41.5437448896139
Root relative squared error: 90.88316378715433
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5858987839794612
Weighted FMeasure: 0.7238454654994645
Iteration time: 4.0
Weighted AreaUnderPRC: 0.616293164217882
Mean absolute error: 0.18463886617606265
Coverage of cases: 72.4
Instances selection time: 4.0
Test time: 2097.0
Accumulative iteration time: 268.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.13803946743607665
Kappa statistic: 0.5860195067608415
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7259237332842111
Weighted AreaUnderROC: 0.7944608552795629
Root mean squared error: 0.4268806534130383
Relative absolute error: 41.24215607401371
Root relative squared error: 90.55506143571091
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.5888755283668518
Weighted FMeasure: 0.725907110274209
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6185576998838477
Mean absolute error: 0.18329847144006178
Coverage of cases: 72.6
Instances selection time: 5.0
Test time: 2129.0
Accumulative iteration time: 273.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.13707828944087436
Kappa statistic: 0.5890024988648069
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.56
Correctly Classified Instances: 72.44
Weighted Precision: 0.7243620774877761
Weighted AreaUnderROC: 0.7932516654367575
Root mean squared error: 0.42813335060429547
Relative absolute error: 41.479334916864396
Root relative squared error: 90.82079863932428
Weighted TruePositiveRate: 0.7244
Weighted MatthewsCorrelation: 0.5864917049799356
Weighted FMeasure: 0.7242846721128696
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6168015287915937
Mean absolute error: 0.1843525996305093
Coverage of cases: 72.44
Instances selection time: 5.0
Test time: 2157.0
Accumulative iteration time: 278.0
Weighted Recall: 0.7244
Weighted FalsePositiveRate: 0.13789666912648502
Kappa statistic: 0.5865968581361218
Training time: 0.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7287389427066144
Weighted AreaUnderROC: 0.7965735043593715
Root mean squared error: 0.4247098490062516
Relative absolute error: 40.81870615744171
Root relative squared error: 90.09456428071034
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.593108308311955
Weighted FMeasure: 0.7286612923386612
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6216844541437906
Mean absolute error: 0.18141647181085288
Coverage of cases: 72.88
Instances selection time: 4.0
Test time: 2190.0
Accumulative iteration time: 282.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.135652991281257
Kappa statistic: 0.5932136029371178
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.88
Correctly Classified Instances: 73.12
Weighted Precision: 0.7311309782142276
Weighted AreaUnderROC: 0.7983872891235795
Root mean squared error: 0.4228340001106713
Relative absolute error: 40.457405986185655
Root relative squared error: 89.6966366383465
Weighted TruePositiveRate: 0.7312
Weighted MatthewsCorrelation: 0.5967265189251127
Weighted FMeasure: 0.7310131315792785
Iteration time: 4.0
Weighted AreaUnderPRC: 0.624383482585077
Mean absolute error: 0.17981069327193708
Coverage of cases: 73.12
Instances selection time: 4.0
Test time: 2225.0
Accumulative iteration time: 286.0
Weighted Recall: 0.7312
Weighted FalsePositiveRate: 0.13442542175284103
Kappa statistic: 0.596828189772971
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 26.88
Correctly Classified Instances: 73.12
Weighted Precision: 0.7312178950702274
Weighted AreaUnderROC: 0.7983701233980764
Root mean squared error: 0.42284133364179427
Relative absolute error: 40.45532879818767
Root relative squared error: 89.69819231522263
Weighted TruePositiveRate: 0.7312
Weighted MatthewsCorrelation: 0.5967466627067107
Weighted FMeasure: 0.7310750027069312
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6244014215312194
Mean absolute error: 0.17980146132527938
Coverage of cases: 73.12
Instances selection time: 4.0
Test time: 2257.0
Accumulative iteration time: 290.0
Weighted Recall: 0.7312
Weighted FalsePositiveRate: 0.13445975320384715
Kappa statistic: 0.5968079670745705
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.08
Correctly Classified Instances: 72.92
Weighted Precision: 0.7292215000603992
Weighted AreaUnderROC: 0.7968586360945699
Root mean squared error: 0.42441862111520184
Relative absolute error: 40.752643335814156
Root relative squared error: 90.03278551572078
Weighted TruePositiveRate: 0.7292
Weighted MatthewsCorrelation: 0.593725129909374
Weighted FMeasure: 0.7291080065079395
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6221809306147625
Mean absolute error: 0.18112285927028599
Coverage of cases: 72.92
Instances selection time: 4.0
Test time: 2319.0
Accumulative iteration time: 294.0
Weighted Recall: 0.7292
Weighted FalsePositiveRate: 0.13548272781086046
Kappa statistic: 0.5938006174230614
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7269145179469658
Weighted AreaUnderROC: 0.7950448513303617
Root mean squared error: 0.42630215749230643
Relative absolute error: 41.109904622157856
Root relative squared error: 90.43234391917942
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5901520377046728
Weighted FMeasure: 0.7267301394904755
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6195509104515461
Mean absolute error: 0.18271068720959133
Coverage of cases: 72.68
Instances selection time: 4.0
Test time: 2320.0
Accumulative iteration time: 298.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13671029733927642
Kappa statistic: 0.5901912792704229
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.64
Correctly Classified Instances: 72.36
Weighted Precision: 0.72381088038208
Weighted AreaUnderROC: 0.7926230384996504
Root mean squared error: 0.42879832268944584
Relative absolute error: 41.586984815617434
Root relative squared error: 90.96186052053716
Weighted TruePositiveRate: 0.7236
Weighted MatthewsCorrelation: 0.5853549190721106
Weighted FMeasure: 0.7236389039115867
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6160422899400303
Mean absolute error: 0.18483104362496725
Coverage of cases: 72.36
Instances selection time: 4.0
Test time: 2353.0
Accumulative iteration time: 302.0
Weighted Recall: 0.7236
Weighted FalsePositiveRate: 0.13835392300069893
Kappa statistic: 0.5853684880050884
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7230336838814029
Weighted AreaUnderROC: 0.7920218767233485
Root mean squared error: 0.4294250374698083
Relative absolute error: 41.704918032786075
Root relative squared error: 91.09480680185641
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.584164918395137
Weighted FMeasure: 0.722845673800201
Iteration time: 4.0
Weighted AreaUnderPRC: 0.615156955646016
Mean absolute error: 0.18535519125682787
Coverage of cases: 72.28
Instances selection time: 4.0
Test time: 2371.0
Accumulative iteration time: 306.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13875624655330301
Kappa statistic: 0.5841665004532766
Training time: 0.0
		
Time end:Wed Nov 01 17.22.03 EET 2017