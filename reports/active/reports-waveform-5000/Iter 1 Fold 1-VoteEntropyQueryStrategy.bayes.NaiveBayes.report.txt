Wed Nov 01 17.28.52 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 17.28.52 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.89333333333457
Incorrectly Classified Instances: 21.0
Correctly Classified Instances: 79.0
Weighted Precision: 0.8295082477480763
Weighted AreaUnderROC: 0.953380326964151
Root mean squared error: 0.34277346046380947
Relative absolute error: 31.596317034601974
Root relative squared error: 72.71323149142142
Weighted TruePositiveRate: 0.79
Weighted MatthewsCorrelation: 0.7056916177764803
Weighted FMeasure: 0.7768862607335308
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9159133360616557
Mean absolute error: 0.14042807570934276
Coverage of cases: 88.48
Instances selection time: 115.0
Test time: 137.0
Accumulative iteration time: 120.0
Weighted Recall: 0.79
Weighted FalsePositiveRate: 0.10384197388102107
Kappa statistic: 0.6855713545801654
Training time: 5.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 40.080000000001235
Incorrectly Classified Instances: 20.48
Correctly Classified Instances: 79.52
Weighted Precision: 0.8334210063778551
Weighted AreaUnderROC: 0.9545810248461266
Root mean squared error: 0.3395785411706366
Relative absolute error: 31.21922994249412
Root relative squared error: 72.03548676215755
Weighted TruePositiveRate: 0.7952
Weighted MatthewsCorrelation: 0.7128906693176639
Weighted FMeasure: 0.7830895242056691
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9179135077917979
Mean absolute error: 0.1387521330777523
Coverage of cases: 88.76
Instances selection time: 95.0
Test time: 128.0
Accumulative iteration time: 220.0
Weighted Recall: 0.7952
Weighted FalsePositiveRate: 0.10126177458537226
Kappa statistic: 0.6933439832412487
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 40.18666666666788
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8340524547921635
Weighted AreaUnderROC: 0.9541596519533948
Root mean squared error: 0.336444596763509
Relative absolute error: 30.815465032279153
Root relative squared error: 71.37067675951508
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.7152381711733762
Weighted FMeasure: 0.7858066959469432
Iteration time: 128.0
Weighted AreaUnderPRC: 0.9177092451587949
Mean absolute error: 0.13695762236568576
Coverage of cases: 89.0
Instances selection time: 123.0
Test time: 127.0
Accumulative iteration time: 348.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10029000026826645
Kappa statistic: 0.6963254298881902
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.253333333334524
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.836061707405284
Weighted AreaUnderROC: 0.9549934898906985
Root mean squared error: 0.3344676286922821
Relative absolute error: 30.515121600927884
Root relative squared error: 70.95129850070892
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7194692876331129
Weighted FMeasure: 0.7897113360639688
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9189834664674738
Mean absolute error: 0.13562276267079124
Coverage of cases: 89.16
Instances selection time: 92.0
Test time: 128.0
Accumulative iteration time: 445.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09870674039162798
Kappa statistic: 0.7011065407233054
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.280000000001216
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.836546985581479
Weighted AreaUnderROC: 0.9555358127337901
Root mean squared error: 0.3333054987100777
Relative absolute error: 30.377407624775277
Root relative squared error: 70.70477350339783
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7205222180869365
Weighted FMeasure: 0.7906739579720297
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9196836881606664
Mean absolute error: 0.1350107005545574
Coverage of cases: 89.4
Instances selection time: 92.0
Test time: 129.0
Accumulative iteration time: 542.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09830628078709366
Kappa statistic: 0.7023027986651664
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 40.38666666666787
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8373641059545301
Weighted AreaUnderROC: 0.9555521101276218
Root mean squared error: 0.33200322908201396
Relative absolute error: 30.198833297349953
Root relative squared error: 70.42852039791669
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7229698797375051
Weighted FMeasure: 0.7930734310510872
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9198657835342916
Mean absolute error: 0.13421703687711153
Coverage of cases: 89.56
Instances selection time: 91.0
Test time: 129.0
Accumulative iteration time: 639.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09732736087710374
Kappa statistic: 0.7052889845168896
Training time: 6.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.42666666666787
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8380685881725396
Weighted AreaUnderROC: 0.9554793432442897
Root mean squared error: 0.3303606825510061
Relative absolute error: 29.98769805044983
Root relative squared error: 70.08008366076966
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7245182769410426
Weighted FMeasure: 0.7946472760862096
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9196474037030279
Mean absolute error: 0.13327865800199987
Coverage of cases: 89.76
Instances selection time: 90.0
Test time: 128.0
Accumulative iteration time: 735.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09673453162247483
Kappa statistic: 0.7070806689055528
Training time: 6.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 40.46666666666788
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.83578593492581
Weighted AreaUnderROC: 0.9558368234696952
Root mean squared error: 0.3293244347744335
Relative absolute error: 29.875680752972116
Root relative squared error: 69.86026231182848
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7222473336403101
Weighted FMeasure: 0.7933468780403404
Iteration time: 95.0
Weighted AreaUnderPRC: 0.920032496923313
Mean absolute error: 0.13278080334654335
Coverage of cases: 89.8
Instances selection time: 88.0
Test time: 130.0
Accumulative iteration time: 830.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09734331447166392
Kappa statistic: 0.7052822054667037
Training time: 7.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 40.50666666666787
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8372946799931918
Weighted AreaUnderROC: 0.9558425688968735
Root mean squared error: 0.32814924531062273
Relative absolute error: 29.707688934032703
Root relative squared error: 69.6109669801166
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7253934688584808
Weighted FMeasure: 0.7963540252513321
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9199821605384819
Mean absolute error: 0.13203417304014597
Coverage of cases: 89.96
Instances selection time: 87.0
Test time: 130.0
Accumulative iteration time: 924.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09615908508098292
Kappa statistic: 0.7088665588075175
Training time: 7.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 40.61333333333452
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8376976867092417
Weighted AreaUnderROC: 0.9557483086013563
Root mean squared error: 0.32649821470390344
Relative absolute error: 29.521561279955662
Root relative squared error: 69.26073049872927
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7276360307890676
Weighted FMeasure: 0.7988002913757982
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9198910152422793
Mean absolute error: 0.1312069390220258
Coverage of cases: 90.28
Instances selection time: 86.0
Test time: 130.0
Accumulative iteration time: 1016.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09518381573084007
Kappa statistic: 0.711851402067577
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 40.693333333334515
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8398018245403506
Weighted AreaUnderROC: 0.9559785229837969
Root mean squared error: 0.3251275145716053
Relative absolute error: 29.361302176587095
Root relative squared error: 68.96996109117289
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7302650578605387
Weighted FMeasure: 0.8004218239268612
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9203415639666337
Mean absolute error: 0.1304946763403877
Coverage of cases: 90.44
Instances selection time: 85.0
Test time: 130.0
Accumulative iteration time: 1108.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09437988275780775
Kappa statistic: 0.7142487437727792
Training time: 7.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 40.7333333333345
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8403253460480659
Weighted AreaUnderROC: 0.9561669028503714
Root mean squared error: 0.3238358933714467
Relative absolute error: 29.23981365207393
Root relative squared error: 68.69596685836595
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7321261327172569
Weighted FMeasure: 0.8025243492926013
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9206861039897174
Mean absolute error: 0.12995472734255142
Coverage of cases: 90.44
Instances selection time: 84.0
Test time: 130.0
Accumulative iteration time: 1199.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09359833441293133
Kappa statistic: 0.7166356104088306
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 40.80000000000115
Incorrectly Classified Instances: 18.76
Correctly Classified Instances: 81.24
Weighted Precision: 0.8409331134495757
Weighted AreaUnderROC: 0.9559461167990032
Root mean squared error: 0.32278191000819767
Relative absolute error: 29.095555886173372
Root relative squared error: 68.47238322334259
Weighted TruePositiveRate: 0.8124
Weighted MatthewsCorrelation: 0.7340374307006389
Weighted FMeasure: 0.8044991179578487
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9203131536067855
Mean absolute error: 0.1293135817163267
Coverage of cases: 90.68
Instances selection time: 83.0
Test time: 131.0
Accumulative iteration time: 1289.0
Weighted Recall: 0.8124
Weighted FalsePositiveRate: 0.09281464239018965
Kappa statistic: 0.7190241162379858
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 40.853333333334504
Incorrectly Classified Instances: 18.56
Correctly Classified Instances: 81.44
Weighted Precision: 0.8417599309630521
Weighted AreaUnderROC: 0.9560282692988327
Root mean squared error: 0.32183508296650754
Relative absolute error: 28.988956688198694
Root relative squared error: 68.27153087680563
Weighted TruePositiveRate: 0.8144
Weighted MatthewsCorrelation: 0.736446115641225
Weighted FMeasure: 0.8070304226610843
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9204196105143193
Mean absolute error: 0.12883980750310592
Coverage of cases: 90.8
Instances selection time: 82.0
Test time: 130.0
Accumulative iteration time: 1379.0
Weighted Recall: 0.8144
Weighted FalsePositiveRate: 0.09183072056518084
Kappa statistic: 0.7220100018884148
Training time: 8.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 40.960000000001145
Incorrectly Classified Instances: 18.68
Correctly Classified Instances: 81.32
Weighted Precision: 0.8404927960368986
Weighted AreaUnderROC: 0.9562470671718427
Root mean squared error: 0.32087793542328075
Relative absolute error: 28.947053527592804
Root relative squared error: 68.0684892212821
Weighted TruePositiveRate: 0.8132
Weighted MatthewsCorrelation: 0.7346261816579072
Weighted FMeasure: 0.8057007336922585
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9208068774276363
Mean absolute error: 0.1286535712337464
Coverage of cases: 90.96
Instances selection time: 81.0
Test time: 130.0
Accumulative iteration time: 1468.0
Weighted Recall: 0.8132
Weighted FalsePositiveRate: 0.09243220229467573
Kappa statistic: 0.7202137255597404
Training time: 8.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 40.973333333334466
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8415962414638105
Weighted AreaUnderROC: 0.9563910891505182
Root mean squared error: 0.3197939785418585
Relative absolute error: 28.80513480182721
Root relative squared error: 67.83854724287185
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7367672372246823
Weighted FMeasure: 0.8075811032974849
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9211194415179201
Mean absolute error: 0.12802282134145487
Coverage of cases: 91.0
Instances selection time: 80.0
Test time: 131.0
Accumulative iteration time: 1556.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09164271603422174
Kappa statistic: 0.7226048637492607
Training time: 8.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 41.12000000000112
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8415917740797934
Weighted AreaUnderROC: 0.9564726420754863
Root mean squared error: 0.31880085047282086
Relative absolute error: 28.716187516265247
Root relative squared error: 67.6278729652109
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7367661287919558
Weighted FMeasure: 0.8075786664461276
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9212129458599883
Mean absolute error: 0.1276275000722906
Coverage of cases: 91.4
Instances selection time: 79.0
Test time: 134.0
Accumulative iteration time: 1643.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09164200147493333
Kappa statistic: 0.7226049967048109
Training time: 8.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 41.1066666666678
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8431279009739016
Weighted AreaUnderROC: 0.9567229049513937
Root mean squared error: 0.3179558788458776
Relative absolute error: 28.58291144389267
Root relative squared error: 67.44862741501436
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7391180549694064
Weighted FMeasure: 0.809373242429584
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9216641382197369
Mean absolute error: 0.1270351619728569
Coverage of cases: 91.44
Instances selection time: 79.0
Test time: 131.0
Accumulative iteration time: 1730.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.09084314818032492
Kappa statistic: 0.7249991252695489
Training time: 8.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 41.186666666667755
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8446752849266576
Weighted AreaUnderROC: 0.956539190750529
Root mean squared error: 0.3172271885592074
Relative absolute error: 28.489618463437115
Root relative squared error: 67.29404886208758
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7414716879533763
Weighted FMeasure: 0.8111734833990537
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9213703344006335
Mean absolute error: 0.12662052650416555
Coverage of cases: 91.52
Instances selection time: 77.0
Test time: 131.0
Accumulative iteration time: 1816.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09004715312287016
Kappa statistic: 0.7273927724933914
Training time: 9.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 41.17333333333442
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.844507968347994
Weighted AreaUnderROC: 0.9566213581830061
Root mean squared error: 0.31642510919314454
Relative absolute error: 28.41679142148336
Root relative squared error: 67.12390213444972
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7417935656600975
Weighted FMeasure: 0.8117098509516223
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9215143035985089
Mean absolute error: 0.12629685076214886
Coverage of cases: 91.56
Instances selection time: 77.0
Test time: 129.0
Accumulative iteration time: 1901.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08985629035475741
Kappa statistic: 0.7279882586790919
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 41.08000000000112
Incorrectly Classified Instances: 18.04
Correctly Classified Instances: 81.96
Weighted Precision: 0.8449159240291585
Weighted AreaUnderROC: 0.9564807906483264
Root mean squared error: 0.31552667886991737
Relative absolute error: 28.277321534825276
Root relative squared error: 66.93331628225646
Weighted TruePositiveRate: 0.8196
Weighted MatthewsCorrelation: 0.7431958629239429
Weighted FMeasure: 0.8131784824593896
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9212951729460148
Mean absolute error: 0.12567698459922405
Coverage of cases: 91.4
Instances selection time: 76.0
Test time: 129.0
Accumulative iteration time: 1986.0
Weighted Recall: 0.8196
Weighted FalsePositiveRate: 0.08927068445641766
Kappa statistic: 0.7297792170379775
Training time: 9.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 41.05333333333445
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.8441937438824662
Weighted AreaUnderROC: 0.9565745338085597
Root mean squared error: 0.31502114148678156
Relative absolute error: 28.21782353014944
Root relative squared error: 66.82607560872887
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7424466168338258
Weighted FMeasure: 0.8127745455484674
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9213850187793196
Mean absolute error: 0.12541254902288698
Coverage of cases: 91.36
Instances selection time: 75.0
Test time: 129.0
Accumulative iteration time: 2070.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.08947313569995509
Kappa statistic: 0.729179408815426
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 41.133333333334434
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8459382983276086
Weighted AreaUnderROC: 0.956798464517329
Root mean squared error: 0.31421718905925733
Relative absolute error: 28.123138264269876
Root relative squared error: 66.65553154475273
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7457078054656641
Weighted FMeasure: 0.8154614825461144
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9218495218211148
Mean absolute error: 0.12499172561897781
Coverage of cases: 91.56
Instances selection time: 75.0
Test time: 128.0
Accumulative iteration time: 2154.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08828890630927407
Kappa statistic: 0.7327678397239767
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 41.160000000001084
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.845868546965916
Weighted AreaUnderROC: 0.9567177570132896
Root mean squared error: 0.31349838007249164
Relative absolute error: 28.078794400756113
Root relative squared error: 66.50304913207678
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7456640927615291
Weighted FMeasure: 0.815551824368479
Iteration time: 83.0
Weighted AreaUnderPRC: 0.921759345185293
Mean absolute error: 0.12479464178113886
Coverage of cases: 91.6
Instances selection time: 73.0
Test time: 128.0
Accumulative iteration time: 2237.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08828747719069724
Kappa statistic: 0.732766943055152
Training time: 10.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 41.346666666667666
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.845899070212965
Weighted AreaUnderROC: 0.9564730137938939
Root mean squared error: 0.3128435401420489
Relative absolute error: 28.027541013120533
Root relative squared error: 66.36413660545445
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7456756062177041
Weighted FMeasure: 0.8155650862992274
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9214155076696228
Mean absolute error: 0.12456684894720295
Coverage of cases: 91.76
Instances selection time: 72.0
Test time: 139.0
Accumulative iteration time: 2318.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08829033542785092
Kappa statistic: 0.7327663025737361
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 41.26666666666769
Incorrectly Classified Instances: 17.8
Correctly Classified Instances: 82.2
Weighted Precision: 0.8453830474454007
Weighted AreaUnderROC: 0.9563109600952697
Root mean squared error: 0.312310385020913
Relative absolute error: 27.989944325066837
Root relative squared error: 66.25103732498059
Weighted TruePositiveRate: 0.822
Weighted MatthewsCorrelation: 0.7458223474668036
Weighted FMeasure: 0.8161641646280854
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9211809895411328
Mean absolute error: 0.12439975255585319
Coverage of cases: 91.8
Instances selection time: 72.0
Test time: 130.0
Accumulative iteration time: 2400.0
Weighted Recall: 0.822
Weighted FalsePositiveRate: 0.08810741057531572
Kappa statistic: 0.7333590906454459
Training time: 10.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 41.2266666666677
Incorrectly Classified Instances: 17.72
Correctly Classified Instances: 82.28
Weighted Precision: 0.8459594222771116
Weighted AreaUnderROC: 0.9559949809376319
Root mean squared error: 0.3117388184820579
Relative absolute error: 27.921111397258514
Root relative squared error: 66.12978975232346
Weighted TruePositiveRate: 0.8228
Weighted MatthewsCorrelation: 0.7469022647690307
Weighted FMeasure: 0.8170880409584946
Iteration time: 80.0
Weighted AreaUnderPRC: 0.920782836367131
Mean absolute error: 0.12409382843226065
Coverage of cases: 91.72
Instances selection time: 70.0
Test time: 132.0
Accumulative iteration time: 2480.0
Weighted Recall: 0.8228
Weighted FalsePositiveRate: 0.08771266744508874
Kappa statistic: 0.7345549319094451
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 41.28000000000099
Incorrectly Classified Instances: 17.72
Correctly Classified Instances: 82.28
Weighted Precision: 0.8455764630085776
Weighted AreaUnderROC: 0.9557523096930578
Root mean squared error: 0.3112286755320883
Relative absolute error: 27.889708222784336
Root relative squared error: 66.02157209053405
Weighted TruePositiveRate: 0.8228
Weighted MatthewsCorrelation: 0.7467157581944806
Weighted FMeasure: 0.8171546813045589
Iteration time: 80.0
Weighted AreaUnderPRC: 0.920428649749807
Mean absolute error: 0.12395425876793097
Coverage of cases: 91.76
Instances selection time: 70.0
Test time: 128.0
Accumulative iteration time: 2560.0
Weighted Recall: 0.8228
Weighted FalsePositiveRate: 0.08771989080137789
Kappa statistic: 0.7345522597691958
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 41.25333333333434
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.8461587690537553
Weighted AreaUnderROC: 0.9558233558474635
Root mean squared error: 0.31083267539842435
Relative absolute error: 27.835616516747955
Root relative squared error: 65.93756777657468
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7477983275116526
Weighted FMeasure: 0.8180766899574866
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9205738983643449
Mean absolute error: 0.12371385118554704
Coverage of cases: 91.8
Instances selection time: 69.0
Test time: 128.0
Accumulative iteration time: 2639.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08732514767115088
Kappa statistic: 0.7357481359757401
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 41.26666666666766
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.8454377891017215
Weighted AreaUnderROC: 0.9559136008658518
Root mean squared error: 0.3103169760506626
Relative absolute error: 27.78839420535349
Root relative squared error: 65.82817142481794
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7474544768002203
Weighted FMeasure: 0.8181107953203984
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9206774390774155
Mean absolute error: 0.1235039742460161
Coverage of cases: 91.8
Instances selection time: 72.0
Test time: 129.0
Accumulative iteration time: 2722.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08733887982444077
Kappa statistic: 0.7357444623784004
Training time: 11.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 41.37333333333426
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.845420817132174
Weighted AreaUnderROC: 0.9558871421558799
Root mean squared error: 0.30960499027759797
Relative absolute error: 27.767162735230407
Root relative squared error: 65.67713643434524
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7474344773357234
Weighted FMeasure: 0.81821161612723
Iteration time: 88.0
Weighted AreaUnderPRC: 0.920721430792733
Mean absolute error: 0.12340961215658015
Coverage of cases: 91.92
Instances selection time: 77.0
Test time: 131.0
Accumulative iteration time: 2810.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08734030894301761
Kappa statistic: 0.7357429422392795
Training time: 11.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 41.373333333334266
Incorrectly Classified Instances: 17.64
Correctly Classified Instances: 82.36
Weighted Precision: 0.8450186935424283
Weighted AreaUnderROC: 0.9560170036452237
Root mean squared error: 0.30924073719749906
Relative absolute error: 27.708901138098202
Root relative squared error: 65.59986668744342
Weighted TruePositiveRate: 0.8236
Weighted MatthewsCorrelation: 0.7472368270710887
Weighted FMeasure: 0.8182653625315923
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9209042928339988
Mean absolute error: 0.12315067172488148
Coverage of cases: 91.92
Instances selection time: 65.0
Test time: 138.0
Accumulative iteration time: 2886.0
Weighted Recall: 0.8236
Weighted FalsePositiveRate: 0.08734610318072994
Kappa statistic: 0.7357406619978054
Training time: 11.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 41.426666666667586
Incorrectly Classified Instances: 17.6
Correctly Classified Instances: 82.4
Weighted Precision: 0.8446385691994099
Weighted AreaUnderROC: 0.9557062465794903
Root mean squared error: 0.3089628260377927
Relative absolute error: 27.693504631170853
Root relative squared error: 65.5409128277647
Weighted TruePositiveRate: 0.824
Weighted MatthewsCorrelation: 0.7474557588017297
Weighted FMeasure: 0.8187627844934282
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9204402625980538
Mean absolute error: 0.12308224280520436
Coverage of cases: 91.96
Instances selection time: 64.0
Test time: 130.0
Accumulative iteration time: 2961.0
Weighted Recall: 0.824
Weighted FalsePositiveRate: 0.08716317832819476
Kappa statistic: 0.7363348336680282
Training time: 11.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 41.45333333333425
Incorrectly Classified Instances: 17.6
Correctly Classified Instances: 82.4
Weighted Precision: 0.8446385691994099
Weighted AreaUnderROC: 0.9557479878041544
Root mean squared error: 0.3086099564429633
Relative absolute error: 27.672438355184635
Root relative squared error: 65.46605788275117
Weighted TruePositiveRate: 0.824
Weighted MatthewsCorrelation: 0.7474557588017297
Weighted FMeasure: 0.8187627844934282
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9204923080248464
Mean absolute error: 0.12298861491193229
Coverage of cases: 92.04
Instances selection time: 62.0
Test time: 130.0
Accumulative iteration time: 3035.0
Weighted Recall: 0.824
Weighted FalsePositiveRate: 0.08716317832819476
Kappa statistic: 0.7363348336680282
Training time: 12.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 41.48000000000093
Incorrectly Classified Instances: 17.56
Correctly Classified Instances: 82.44
Weighted Precision: 0.8452645354372551
Weighted AreaUnderROC: 0.9564296765168155
Root mean squared error: 0.30787824905649136
Relative absolute error: 27.591220112670396
Root relative squared error: 65.3108393063056
Weighted TruePositiveRate: 0.8244
Weighted MatthewsCorrelation: 0.7481633338598065
Weighted FMeasure: 0.8191482479917594
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9215222207609797
Mean absolute error: 0.12262764494520234
Coverage of cases: 92.24
Instances selection time: 68.0
Test time: 131.0
Accumulative iteration time: 3114.0
Weighted Recall: 0.8244
Weighted FalsePositiveRate: 0.08695715428821527
Kappa statistic: 0.7369355860116367
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.4266666666676
Incorrectly Classified Instances: 17.44
Correctly Classified Instances: 82.56
Weighted Precision: 0.8462645991022708
Weighted AreaUnderROC: 0.9566250374016058
Root mean squared error: 0.3074600415176457
Relative absolute error: 27.50809095270704
Root relative squared error: 65.22212409030728
Weighted TruePositiveRate: 0.8256
Weighted MatthewsCorrelation: 0.7498857926302666
Weighted FMeasure: 0.8202431652343048
Iteration time: 73.0
Weighted AreaUnderPRC: 0.921802742530508
Mean absolute error: 0.12225818201203187
Coverage of cases: 92.16
Instances selection time: 61.0
Test time: 131.0
Accumulative iteration time: 3187.0
Weighted Recall: 0.8256
Weighted FalsePositiveRate: 0.08636289591500954
Kappa statistic: 0.7387334179613345
Training time: 12.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 41.37333333333426
Incorrectly Classified Instances: 17.4
Correctly Classified Instances: 82.6
Weighted Precision: 0.8474375068105044
Weighted AreaUnderROC: 0.9555673982983058
Root mean squared error: 0.3094326342348609
Relative absolute error: 27.698348379172334
Root relative squared error: 65.64057419636588
Weighted TruePositiveRate: 0.826
Weighted MatthewsCorrelation: 0.750896560698143
Weighted FMeasure: 0.8203024551876044
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9201590264023893
Mean absolute error: 0.12310377057409982
Coverage of cases: 92.0
Instances selection time: 156.0
Test time: 212.0
Accumulative iteration time: 3377.0
Weighted Recall: 0.826
Weighted FalsePositiveRate: 0.08615107763731772
Kappa statistic: 0.7393401506769726
Training time: 34.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 41.41333333333426
Incorrectly Classified Instances: 17.52
Correctly Classified Instances: 82.48
Weighted Precision: 0.8465884257940332
Weighted AreaUnderROC: 0.9560081586222914
Root mean squared error: 0.3091825243563163
Relative absolute error: 27.676411798846033
Root relative squared error: 65.5875178790177
Weighted TruePositiveRate: 0.8248
Weighted MatthewsCorrelation: 0.7492719631419246
Weighted FMeasure: 0.8191321697181404
Iteration time: 72.0
Weighted AreaUnderPRC: 0.921060446037503
Mean absolute error: 0.1230062746615385
Coverage of cases: 92.0
Instances selection time: 60.0
Test time: 131.0
Accumulative iteration time: 3449.0
Weighted Recall: 0.8248
Weighted FalsePositiveRate: 0.08674819424767712
Kappa statistic: 0.7375423707282325
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 41.346666666667616
Incorrectly Classified Instances: 17.48
Correctly Classified Instances: 82.52
Weighted Precision: 0.8472908006775942
Weighted AreaUnderROC: 0.9566941474639269
Root mean squared error: 0.30871538526383235
Relative absolute error: 27.59216593957897
Root relative squared error: 65.48842271300188
Weighted TruePositiveRate: 0.8252
Weighted MatthewsCorrelation: 0.7500224942235676
Weighted FMeasure: 0.8195284624140806
Iteration time: 71.0
Weighted AreaUnderPRC: 0.922166231478128
Mean absolute error: 0.12263184862035155
Coverage of cases: 92.08
Instances selection time: 59.0
Test time: 130.0
Accumulative iteration time: 3520.0
Weighted Recall: 0.8252
Weighted FalsePositiveRate: 0.08654431388556286
Kappa statistic: 0.7381424677549714
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.36000000000092
Incorrectly Classified Instances: 17.76
Correctly Classified Instances: 82.24
Weighted Precision: 0.8452967551445072
Weighted AreaUnderROC: 0.9562200343204815
Root mean squared error: 0.30952274756186654
Relative absolute error: 27.674298877934444
Root relative squared error: 65.65969011974617
Weighted TruePositiveRate: 0.8224
Weighted MatthewsCorrelation: 0.7462455846168564
Weighted FMeasure: 0.8164178739173787
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9214789024656801
Mean absolute error: 0.12299688390193143
Coverage of cases: 91.92
Instances selection time: 58.0
Test time: 133.0
Accumulative iteration time: 3591.0
Weighted Recall: 0.8224
Weighted FalsePositiveRate: 0.08793020219708785
Kappa statistic: 0.7339548403920787
Training time: 13.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.26666666666764
Incorrectly Classified Instances: 17.88
Correctly Classified Instances: 82.12
Weighted Precision: 0.8444713831300927
Weighted AreaUnderROC: 0.9560265322378633
Root mean squared error: 0.31032791051964215
Relative absolute error: 27.730941416842914
Root relative squared error: 65.83049097596718
Weighted TruePositiveRate: 0.8212
Weighted MatthewsCorrelation: 0.7446405564466638
Weighted FMeasure: 0.815047979842722
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9212773311519095
Mean absolute error: 0.1232486285193024
Coverage of cases: 91.88
Instances selection time: 58.0
Test time: 314.0
Accumulative iteration time: 3662.0
Weighted Recall: 0.8212
Weighted FalsePositiveRate: 0.08852446057029359
Kappa statistic: 0.7321608329258823
Training time: 13.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.120000000001035
Incorrectly Classified Instances: 18.08
Correctly Classified Instances: 81.92
Weighted Precision: 0.843837234186839
Weighted AreaUnderROC: 0.9555532415537138
Root mean squared error: 0.3122211215842801
Relative absolute error: 27.91356414649589
Root relative squared error: 66.23210169057404
Weighted TruePositiveRate: 0.8192
Weighted MatthewsCorrelation: 0.7423156052700448
Weighted FMeasure: 0.8128069693129494
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9205666003767912
Mean absolute error: 0.12406028509553788
Coverage of cases: 91.72
Instances selection time: 70.0
Test time: 131.0
Accumulative iteration time: 3765.0
Weighted Recall: 0.8192
Weighted FalsePositiveRate: 0.08950115903901325
Kappa statistic: 0.7291730478769325
Training time: 33.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 41.06666666666771
Incorrectly Classified Instances: 18.16
Correctly Classified Instances: 81.84
Weighted Precision: 0.8437719784407917
Weighted AreaUnderROC: 0.9551201152749509
Root mean squared error: 0.31333833409348893
Relative absolute error: 28.03093129008457
Root relative squared error: 66.46909825296045
Weighted TruePositiveRate: 0.8184
Weighted MatthewsCorrelation: 0.7414869717834703
Weighted FMeasure: 0.8118271203016351
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9198375487477302
Mean absolute error: 0.1245819168448209
Coverage of cases: 91.68
Instances selection time: 55.0
Test time: 129.0
Accumulative iteration time: 3833.0
Weighted Recall: 0.8184
Weighted FalsePositiveRate: 0.08989153705010475
Kappa statistic: 0.7279792621934794
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 41.160000000001006
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8463866323445133
Weighted AreaUnderROC: 0.9556880378676124
Root mean squared error: 0.3124389692600633
Relative absolute error: 27.926835484182043
Root relative squared error: 66.27831416121766
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7460456625237396
Weighted FMeasure: 0.8150456492923169
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9207884123425116
Mean absolute error: 0.12411926881858744
Coverage of cases: 91.88
Instances selection time: 54.0
Test time: 127.0
Accumulative iteration time: 3900.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08831113541061991
Kappa statistic: 0.7327682240087738
Training time: 13.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 41.18666666666769
Incorrectly Classified Instances: 17.84
Correctly Classified Instances: 82.16
Weighted Precision: 0.8466228093699687
Weighted AreaUnderROC: 0.9557841089054436
Root mean squared error: 0.31303737962680417
Relative absolute error: 27.99416378335337
Root relative squared error: 66.40525616969408
Weighted TruePositiveRate: 0.8216
Weighted MatthewsCorrelation: 0.7462028996754517
Weighted FMeasure: 0.8148625627076325
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9210444085690777
Mean absolute error: 0.12441850570379333
Coverage of cases: 91.84
Instances selection time: 53.0
Test time: 130.0
Accumulative iteration time: 3967.0
Weighted Recall: 0.8216
Weighted FalsePositiveRate: 0.08831327908848516
Kappa statistic: 0.7327700173232136
Training time: 14.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.106666666667685
Incorrectly Classified Instances: 17.92
Correctly Classified Instances: 82.08
Weighted Precision: 0.846897528957529
Weighted AreaUnderROC: 0.9554900502103445
Root mean squared error: 0.3141863123697388
Relative absolute error: 28.106644765961175
Root relative squared error: 66.648981609791
Weighted TruePositiveRate: 0.8208
Weighted MatthewsCorrelation: 0.7455287828433129
Weighted FMeasure: 0.8138935354208167
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9205592567708994
Mean absolute error: 0.12491842118205025
Coverage of cases: 91.64
Instances selection time: 52.0
Test time: 130.0
Accumulative iteration time: 4033.0
Weighted Recall: 0.8208
Weighted FalsePositiveRate: 0.08869571918399907
Kappa statistic: 0.7315775951653289
Training time: 14.0
		
Time end:Wed Nov 01 17.29.05 EET 2017