Wed Nov 01 17.24.45 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 17.24.45 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 35.893333333334354
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.6847992442467077
Weighted AreaUnderROC: 0.7641174937173444
Root mean squared error: 0.45281169076934186
Relative absolute error: 49.10017561702764
Root relative squared error: 96.05586514306405
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5203892021266417
Weighted FMeasure: 0.6788618664225238
Iteration time: 89.0
Weighted AreaUnderPRC: 0.5802442340207798
Mean absolute error: 0.21822300274234607
Coverage of cases: 70.92
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 89.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16161047492322406
Kappa statistic: 0.5172330971538369
Training time: 79.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 35.32000000000101
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.6957274731566622
Weighted AreaUnderROC: 0.790277820798499
Root mean squared error: 0.44259627423369985
Relative absolute error: 47.09316279069743
Root relative squared error: 93.88884805156478
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.5374501047239746
Weighted FMeasure: 0.6905235391469816
Iteration time: 88.0
Weighted AreaUnderPRC: 0.6141091088087667
Mean absolute error: 0.20930294573643396
Coverage of cases: 72.36
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 177.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.15575275139476036
Kappa statistic: 0.5346760600859893
Training time: 78.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 35.720000000001086
Incorrectly Classified Instances: 31.36
Correctly Classified Instances: 68.64
Weighted Precision: 0.6911373978651499
Weighted AreaUnderROC: 0.7737790312170463
Root mean squared error: 0.44778420919270745
Relative absolute error: 47.45341324967157
Root relative squared error: 94.98937524852548
Weighted TruePositiveRate: 0.6864
Weighted MatthewsCorrelation: 0.5315053621451559
Weighted FMeasure: 0.6870209117694461
Iteration time: 92.0
Weighted AreaUnderPRC: 0.5949325835584582
Mean absolute error: 0.21090405888743016
Coverage of cases: 72.04
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 269.0
Weighted Recall: 0.6864
Weighted FalsePositiveRate: 0.15749797762456716
Kappa statistic: 0.5293088116032102
Training time: 82.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.18666666666766
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7031578352941177
Weighted AreaUnderROC: 0.8224860671407539
Root mean squared error: 0.42216827600676726
Relative absolute error: 45.80507640232128
Root relative squared error: 89.55541522986553
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5529928456531741
Weighted FMeasure: 0.7015600263569408
Iteration time: 95.0
Weighted AreaUnderPRC: 0.6630241066128923
Mean absolute error: 0.2035781173436511
Coverage of cases: 78.56
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 364.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.14954484706603421
Kappa statistic: 0.5522638882220195
Training time: 86.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 38.70666666666772
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7185205682936501
Weighted AreaUnderROC: 0.8031887906728314
Root mean squared error: 0.41820669963098683
Relative absolute error: 43.65820221242164
Root relative squared error: 88.7150379740147
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5761352586105146
Weighted FMeasure: 0.7174088361030329
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6366164843017308
Mean absolute error: 0.19403645427743044
Coverage of cases: 78.44
Instances selection time: 10.0
Test time: 6.0
Accumulative iteration time: 464.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14180596528837336
Kappa statistic: 0.5756826168391223
Training time: 90.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 38.386666666667516
Incorrectly Classified Instances: 30.84
Correctly Classified Instances: 69.16
Weighted Precision: 0.6936020274875496
Weighted AreaUnderROC: 0.8039426145821211
Root mean squared error: 0.4394848510518865
Relative absolute error: 46.597312702003684
Root relative squared error: 93.22881552226441
Weighted TruePositiveRate: 0.6916
Weighted MatthewsCorrelation: 0.538043033751754
Weighted FMeasure: 0.6919223419691842
Iteration time: 105.0
Weighted AreaUnderPRC: 0.6324764942589182
Mean absolute error: 0.2070991675644618
Coverage of cases: 76.36
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 569.0
Weighted Recall: 0.6916
Weighted FalsePositiveRate: 0.15467345268150315
Kappa statistic: 0.5372234414873962
Training time: 96.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 37.693333333334145
Incorrectly Classified Instances: 29.76
Correctly Classified Instances: 70.24
Weighted Precision: 0.7059301073703329
Weighted AreaUnderROC: 0.8035994945687644
Root mean squared error: 0.4357441314134225
Relative absolute error: 45.228824407659936
Root relative squared error: 92.43528905540173
Weighted TruePositiveRate: 0.7024
Weighted MatthewsCorrelation: 0.5547552473557139
Weighted FMeasure: 0.703304285767063
Iteration time: 110.0
Weighted AreaUnderPRC: 0.6326840655676516
Mean absolute error: 0.20101699736737844
Coverage of cases: 75.16
Instances selection time: 9.0
Test time: 7.0
Accumulative iteration time: 679.0
Weighted Recall: 0.7024
Weighted FalsePositiveRate: 0.14943090770188155
Kappa statistic: 0.5534011027151051
Training time: 101.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 35.28000000000106
Incorrectly Classified Instances: 29.12
Correctly Classified Instances: 70.88
Weighted Precision: 0.7121964286388671
Weighted AreaUnderROC: 0.784746124918779
Root mean squared error: 0.43488137795485526
Relative absolute error: 44.41232474226835
Root relative squared error: 92.25227140908822
Weighted TruePositiveRate: 0.7088
Weighted MatthewsCorrelation: 0.5644319455175065
Weighted FMeasure: 0.7094067903813459
Iteration time: 114.0
Weighted AreaUnderPRC: 0.6136980327872033
Mean absolute error: 0.19738810996563808
Coverage of cases: 73.64
Instances selection time: 9.0
Test time: 6.0
Accumulative iteration time: 793.0
Weighted Recall: 0.7088
Weighted FalsePositiveRate: 0.14616425524964025
Kappa statistic: 0.5630073736503051
Training time: 105.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 35.61333333333439
Incorrectly Classified Instances: 28.96
Correctly Classified Instances: 71.04
Weighted Precision: 0.7157062912268751
Weighted AreaUnderROC: 0.7892464646194521
Root mean squared error: 0.432397038943513
Relative absolute error: 44.03777622377649
Root relative squared error: 91.7252635205823
Weighted TruePositiveRate: 0.7104
Weighted MatthewsCorrelation: 0.5678165643724896
Weighted FMeasure: 0.7110981664550298
Iteration time: 115.0
Weighted AreaUnderPRC: 0.6185330963051496
Mean absolute error: 0.19572344988345197
Coverage of cases: 74.36
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 908.0
Weighted Recall: 0.7104
Weighted FalsePositiveRate: 0.14551066652885902
Kappa statistic: 0.5653211796174539
Training time: 107.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 39.00000000000079
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7255553768196628
Weighted AreaUnderROC: 0.8172011062677665
Root mean squared error: 0.4166696622521528
Relative absolute error: 43.15949261359893
Root relative squared error: 88.38898310796148
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5863811167840527
Weighted FMeasure: 0.7244149507548656
Iteration time: 111.0
Weighted AreaUnderPRC: 0.6504283439821317
Mean absolute error: 0.1918199671715517
Coverage of cases: 78.76
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 1019.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.1384308459775465
Kappa statistic: 0.5858835504543878
Training time: 103.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 40.12000000000078
Incorrectly Classified Instances: 29.0
Correctly Classified Instances: 71.0
Weighted Precision: 0.7133618171197148
Weighted AreaUnderROC: 0.8216201081115462
Root mean squared error: 0.42214190863990575
Relative absolute error: 44.869411659520374
Root relative squared error: 89.5498218666926
Weighted TruePositiveRate: 0.71
Weighted MatthewsCorrelation: 0.566226551155377
Weighted FMeasure: 0.7105021688247585
Iteration time: 121.0
Weighted AreaUnderPRC: 0.6678033567509006
Mean absolute error: 0.19941960737564704
Coverage of cases: 79.28
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1140.0
Weighted Recall: 0.71
Weighted FalsePositiveRate: 0.14555733677563348
Kappa statistic: 0.5648180939632765
Training time: 113.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 39.46666666666742
Incorrectly Classified Instances: 28.04
Correctly Classified Instances: 71.96
Weighted Precision: 0.7225849940193312
Weighted AreaUnderROC: 0.8133185947811856
Root mean squared error: 0.41907973792977316
Relative absolute error: 43.316697611981155
Root relative squared error: 88.90023736440693
Weighted TruePositiveRate: 0.7196
Weighted MatthewsCorrelation: 0.5804979197823815
Weighted FMeasure: 0.7199954408762548
Iteration time: 130.0
Weighted AreaUnderPRC: 0.66023103890675
Mean absolute error: 0.19251865605325047
Coverage of cases: 79.4
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1270.0
Weighted Recall: 0.7196
Weighted FalsePositiveRate: 0.14070198898357944
Kappa statistic: 0.5792413908346891
Training time: 122.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 39.240000000000805
Incorrectly Classified Instances: 29.68
Correctly Classified Instances: 70.32
Weighted Precision: 0.7064031291895791
Weighted AreaUnderROC: 0.8083658205469759
Root mean squared error: 0.4290505619124506
Relative absolute error: 44.69675202228321
Root relative squared error: 91.01536854005752
Weighted TruePositiveRate: 0.7032
Weighted MatthewsCorrelation: 0.5560299812596765
Weighted FMeasure: 0.7036356433436024
Iteration time: 130.0
Weighted AreaUnderPRC: 0.645503466935427
Mean absolute error: 0.19865223121014855
Coverage of cases: 78.24
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1400.0
Weighted Recall: 0.7032
Weighted FalsePositiveRate: 0.14897365382766767
Kappa statistic: 0.5545731886676697
Training time: 122.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 38.57333333333437
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7314867026266522
Weighted AreaUnderROC: 0.8173556060368424
Root mean squared error: 0.4115435845803967
Relative absolute error: 41.70919570203681
Root relative squared error: 87.30157782318518
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5943116312038832
Weighted FMeasure: 0.7289715432988707
Iteration time: 134.0
Weighted AreaUnderPRC: 0.6697182514332221
Mean absolute error: 0.18537420312016448
Coverage of cases: 78.56
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 1534.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.1360031720815195
Kappa statistic: 0.593048451243243
Training time: 126.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 37.69333333333446
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7167339201770118
Weighted AreaUnderROC: 0.7976945266271122
Root mean squared error: 0.4230789403383987
Relative absolute error: 43.772361359904544
Root relative squared error: 89.74859630714994
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.5741566464656627
Weighted FMeasure: 0.7161471481568685
Iteration time: 140.0
Weighted AreaUnderPRC: 0.6380858131109595
Mean absolute error: 0.19454382826624333
Coverage of cases: 76.2
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1674.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.14228929053875805
Kappa statistic: 0.5738937802415385
Training time: 133.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 37.69333333333433
Incorrectly Classified Instances: 29.96
Correctly Classified Instances: 70.04
Weighted Precision: 0.7014639363059757
Weighted AreaUnderROC: 0.7910961554557012
Root mean squared error: 0.43163889871717365
Relative absolute error: 45.706813764054864
Root relative squared error: 91.56443769204185
Weighted TruePositiveRate: 0.7004
Weighted MatthewsCorrelation: 0.5508915816757173
Weighted FMeasure: 0.7005301639869647
Iteration time: 142.0
Weighted AreaUnderPRC: 0.6275578550956176
Mean absolute error: 0.2031413945069115
Coverage of cases: 75.12
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1816.0
Weighted Recall: 0.7004
Weighted FalsePositiveRate: 0.15015863183024217
Kappa statistic: 0.550461290335746
Training time: 135.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 36.733333333334286
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7023200049603217
Weighted AreaUnderROC: 0.7891760896983242
Root mean squared error: 0.4342372266578365
Relative absolute error: 45.28673851350223
Root relative squared error: 92.11562628401859
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5529536285725196
Weighted FMeasure: 0.7020938419804026
Iteration time: 147.0
Weighted AreaUnderPRC: 0.613160618638621
Mean absolute error: 0.2012743933933442
Coverage of cases: 74.6
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 1963.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.1492235252112107
Kappa statistic: 0.5529415605207912
Training time: 140.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 40.82666666666743
Incorrectly Classified Instances: 27.76
Correctly Classified Instances: 72.24
Weighted Precision: 0.7241435185749894
Weighted AreaUnderROC: 0.816450609256771
Root mean squared error: 0.41676462570216877
Relative absolute error: 42.49330287009969
Root relative squared error: 88.40912789780285
Weighted TruePositiveRate: 0.7224
Weighted MatthewsCorrelation: 0.5841299060159101
Weighted FMeasure: 0.7227259228153704
Iteration time: 150.0
Weighted AreaUnderPRC: 0.6508759048162472
Mean absolute error: 0.18885912386711062
Coverage of cases: 80.24
Instances selection time: 7.0
Test time: 7.0
Accumulative iteration time: 2113.0
Weighted Recall: 0.7224
Weighted FalsePositiveRate: 0.13922862679255346
Kappa statistic: 0.583465776011662
Training time: 143.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 39.08000000000067
Incorrectly Classified Instances: 28.84
Correctly Classified Instances: 71.16
Weighted Precision: 0.7123077438897432
Weighted AreaUnderROC: 0.820077942584804
Root mean squared error: 0.4243965183856151
Relative absolute error: 43.22724998994822
Root relative squared error: 90.02809681872871
Weighted TruePositiveRate: 0.7116
Weighted MatthewsCorrelation: 0.5675465947686382
Weighted FMeasure: 0.7117082836983485
Iteration time: 157.0
Weighted AreaUnderPRC: 0.6621870543346684
Mean absolute error: 0.19212111106643742
Coverage of cases: 78.52
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2270.0
Weighted Recall: 0.7116
Weighted FalsePositiveRate: 0.14449177064277874
Kappa statistic: 0.5672941082141622
Training time: 150.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 39.60000000000096
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7201946780967857
Weighted AreaUnderROC: 0.8207850948017847
Root mean squared error: 0.4150911398968633
Relative absolute error: 42.16148586251046
Root relative squared error: 88.05412794945755
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5789942580459448
Weighted FMeasure: 0.7194784957837984
Iteration time: 161.0
Weighted AreaUnderPRC: 0.6589963552843864
Mean absolute error: 0.18738438161115847
Coverage of cases: 79.64
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 2431.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.1407486592303539
Kappa statistic: 0.5786949770838874
Training time: 153.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 39.38666666666756
Incorrectly Classified Instances: 28.12
Correctly Classified Instances: 71.88
Weighted Precision: 0.7197224751346087
Weighted AreaUnderROC: 0.8154458926936168
Root mean squared error: 0.4210673622341661
Relative absolute error: 42.70643137112728
Root relative squared error: 89.32187615163315
Weighted TruePositiveRate: 0.7188
Weighted MatthewsCorrelation: 0.5784716018319493
Weighted FMeasure: 0.7188199467067499
Iteration time: 164.0
Weighted AreaUnderPRC: 0.6541021189802181
Mean absolute error: 0.18980636164945547
Coverage of cases: 79.0
Instances selection time: 7.0
Test time: 6.0
Accumulative iteration time: 2595.0
Weighted Recall: 0.7188
Weighted FalsePositiveRate: 0.1409326552811529
Kappa statistic: 0.5780755829278937
Training time: 157.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 39.173333333334085
Incorrectly Classified Instances: 28.88
Correctly Classified Instances: 71.12
Weighted Precision: 0.7131273674195416
Weighted AreaUnderROC: 0.7982821923847782
Root mean squared error: 0.4298645024402192
Relative absolute error: 44.286637343048994
Root relative squared error: 91.18803140005784
Weighted TruePositiveRate: 0.7112
Weighted MatthewsCorrelation: 0.5674466472264524
Weighted FMeasure: 0.7115346426919356
Iteration time: 158.0
Weighted AreaUnderPRC: 0.6286290057421966
Mean absolute error: 0.1968294993024409
Coverage of cases: 76.68
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2753.0
Weighted Recall: 0.7112
Weighted FalsePositiveRate: 0.14484742394860833
Kappa statistic: 0.5666326855472361
Training time: 152.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 40.97333333333426
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.728325831617943
Weighted AreaUnderROC: 0.8166657250694037
Root mean squared error: 0.4166610181364041
Relative absolute error: 41.976564324318275
Root relative squared error: 88.38714941410251
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5915381513411532
Weighted FMeasure: 0.7277493147452231
Iteration time: 175.0
Weighted AreaUnderPRC: 0.6555049516606185
Mean absolute error: 0.18656250810808211
Coverage of cases: 79.72
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 2928.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.13649336362210537
Kappa statistic: 0.5913055752401035
Training time: 169.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 39.10666666666749
Incorrectly Classified Instances: 28.08
Correctly Classified Instances: 71.92
Weighted Precision: 0.7202008298993161
Weighted AreaUnderROC: 0.8158805642066796
Root mean squared error: 0.42262111507471634
Relative absolute error: 43.13146221520159
Root relative squared error: 89.65147690258543
Weighted TruePositiveRate: 0.7192
Weighted MatthewsCorrelation: 0.5790368193741584
Weighted FMeasure: 0.7193687961306018
Iteration time: 172.0
Weighted AreaUnderPRC: 0.6534152498525017
Mean absolute error: 0.19169538762311905
Coverage of cases: 77.6
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 3100.0
Weighted Recall: 0.7192
Weighted FalsePositiveRate: 0.14075552552055515
Kappa statistic: 0.5786834469887749
Training time: 166.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 38.040000000000866
Incorrectly Classified Instances: 27.24
Correctly Classified Instances: 72.76
Weighted Precision: 0.7287967648480711
Weighted AreaUnderROC: 0.8084765383923195
Root mean squared error: 0.41842174890097533
Relative absolute error: 42.026373955608854
Root relative squared error: 88.76065681314414
Weighted TruePositiveRate: 0.7276
Weighted MatthewsCorrelation: 0.5917333962390697
Weighted FMeasure: 0.7278466424521332
Iteration time: 178.0
Weighted AreaUnderPRC: 0.6430227868688385
Mean absolute error: 0.18678388424715134
Coverage of cases: 77.08
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3278.0
Weighted Recall: 0.7276
Weighted FalsePositiveRate: 0.1365482939437152
Kappa statistic: 0.5912832050886861
Training time: 172.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 36.9333333333344
Incorrectly Classified Instances: 26.16
Correctly Classified Instances: 73.84
Weighted Precision: 0.73866726875399
Weighted AreaUnderROC: 0.8248666205781994
Root mean squared error: 0.40733724799292004
Relative absolute error: 39.77188668854741
Root relative squared error: 86.40927908569785
Weighted TruePositiveRate: 0.7384
Weighted MatthewsCorrelation: 0.6075054208661651
Weighted FMeasure: 0.7384394867532147
Iteration time: 171.0
Weighted AreaUnderPRC: 0.6672673923878996
Mean absolute error: 0.17676394083798933
Coverage of cases: 77.72
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3449.0
Weighted Recall: 0.7384
Weighted FalsePositiveRate: 0.13103109735604462
Kappa statistic: 0.6075719649411753
Training time: 165.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 37.5733333333344
Incorrectly Classified Instances: 27.92
Correctly Classified Instances: 72.08
Weighted Precision: 0.7212973258320984
Weighted AreaUnderROC: 0.8009447331578936
Root mean squared error: 0.42174478397523607
Relative absolute error: 42.68309401278375
Root relative squared error: 89.46557900368329
Weighted TruePositiveRate: 0.7208
Weighted MatthewsCorrelation: 0.581153455263974
Weighted FMeasure: 0.7209519252624174
Iteration time: 180.0
Weighted AreaUnderPRC: 0.6308539802032069
Mean absolute error: 0.18970264005681756
Coverage of cases: 76.36
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 3629.0
Weighted Recall: 0.7208
Weighted FalsePositiveRate: 0.13990968067413959
Kappa statistic: 0.5811376062578282
Training time: 175.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 37.25333333333442
Incorrectly Classified Instances: 26.68
Correctly Classified Instances: 73.32
Weighted Precision: 0.7353481633084715
Weighted AreaUnderROC: 0.8060903081229659
Root mean squared error: 0.41044303724532183
Relative absolute error: 41.17158075277671
Root relative squared error: 87.06811647809073
Weighted TruePositiveRate: 0.7332
Weighted MatthewsCorrelation: 0.6004551046320771
Weighted FMeasure: 0.7337002171686945
Iteration time: 177.0
Weighted AreaUnderPRC: 0.6431974572468029
Mean absolute error: 0.18298480334567513
Coverage of cases: 77.52
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 3806.0
Weighted Recall: 0.7332
Weighted FalsePositiveRate: 0.1338762211697123
Kappa statistic: 0.5996655196413578
Training time: 172.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 37.32000000000117
Incorrectly Classified Instances: 25.92
Correctly Classified Instances: 74.08
Weighted Precision: 0.7421476985614727
Weighted AreaUnderROC: 0.8072072912774263
Root mean squared error: 0.4063543952812134
Relative absolute error: 39.8117908426135
Root relative squared error: 86.20078454049124
Weighted TruePositiveRate: 0.7408
Weighted MatthewsCorrelation: 0.611528056474182
Weighted FMeasure: 0.7411795644994411
Iteration time: 181.0
Weighted AreaUnderPRC: 0.6398307283807891
Mean absolute error: 0.1769412926338386
Coverage of cases: 78.56
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 3987.0
Weighted Recall: 0.7408
Weighted FalsePositiveRate: 0.1299820513728605
Kappa statistic: 0.6110977342602011
Training time: 175.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 37.33333333333442
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7375435257854064
Weighted AreaUnderROC: 0.8084557031767795
Root mean squared error: 0.410781393942266
Relative absolute error: 39.96051746873858
Root relative squared error: 87.13989277255145
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6057096811837013
Weighted FMeasure: 0.7373167677904013
Iteration time: 183.0
Weighted AreaUnderPRC: 0.645553357580517
Mean absolute error: 0.17760229986106119
Coverage of cases: 77.32
Instances selection time: 6.0
Test time: 6.0
Accumulative iteration time: 4170.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.13166548099085626
Kappa statistic: 0.6057556553961189
Training time: 177.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.78666666666781
Incorrectly Classified Instances: 25.84
Correctly Classified Instances: 74.16
Weighted Precision: 0.7423347195343243
Weighted AreaUnderROC: 0.8221308537111576
Root mean squared error: 0.40511708630750276
Relative absolute error: 39.36280816800674
Root relative squared error: 85.93831167077111
Weighted TruePositiveRate: 0.7416
Weighted MatthewsCorrelation: 0.6124875673534818
Weighted FMeasure: 0.7417939975312458
Iteration time: 188.0
Weighted AreaUnderPRC: 0.6642105765306326
Mean absolute error: 0.17494581408003076
Coverage of cases: 78.72
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 4358.0
Weighted Recall: 0.7416
Weighted FalsePositiveRate: 0.12949733233784172
Kappa statistic: 0.6123369504816264
Training time: 183.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 36.18666666666779
Incorrectly Classified Instances: 26.52
Correctly Classified Instances: 73.48
Weighted Precision: 0.7347597511045718
Weighted AreaUnderROC: 0.8177756217064394
Root mean squared error: 0.4107028682020328
Relative absolute error: 39.837087524665236
Root relative squared error: 87.12323494752647
Weighted TruePositiveRate: 0.7348
Weighted MatthewsCorrelation: 0.6019577462658895
Weighted FMeasure: 0.7347728282229027
Iteration time: 189.0
Weighted AreaUnderPRC: 0.6556852987757795
Mean absolute error: 0.17705372233184632
Coverage of cases: 76.96
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4547.0
Weighted Recall: 0.7348
Weighted FalsePositiveRate: 0.13282438761725998
Kappa statistic: 0.6021806500668192
Training time: 184.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 36.3466666666678
Incorrectly Classified Instances: 26.72
Correctly Classified Instances: 73.28
Weighted Precision: 0.7345250484166023
Weighted AreaUnderROC: 0.8095452935917276
Root mean squared error: 0.4125908577134366
Relative absolute error: 40.51356322248617
Root relative squared error: 87.52373800342329
Weighted TruePositiveRate: 0.7328
Weighted MatthewsCorrelation: 0.599761018834618
Weighted FMeasure: 0.7330354883182748
Iteration time: 200.0
Weighted AreaUnderPRC: 0.6431474421260611
Mean absolute error: 0.18006028098882826
Coverage of cases: 77.72
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 4747.0
Weighted Recall: 0.7328
Weighted FalsePositiveRate: 0.13399155431849902
Kappa statistic: 0.5990854346538067
Training time: 195.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 38.38666666666769
Incorrectly Classified Instances: 25.92
Correctly Classified Instances: 74.08
Weighted Precision: 0.7415146773065551
Weighted AreaUnderROC: 0.8157822670466496
Root mean squared error: 0.40565843428838694
Relative absolute error: 40.44129289206333
Root relative squared error: 86.05314891925057
Weighted TruePositiveRate: 0.7408
Weighted MatthewsCorrelation: 0.6113089014487608
Weighted FMeasure: 0.7409132388713072
Iteration time: 207.0
Weighted AreaUnderPRC: 0.6552122258556967
Mean absolute error: 0.1797390795202823
Coverage of cases: 78.6
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 4954.0
Weighted Recall: 0.7408
Weighted FalsePositiveRate: 0.12988592331004337
Kappa statistic: 0.6111385287786293
Training time: 202.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 37.69333333333423
Incorrectly Classified Instances: 27.6
Correctly Classified Instances: 72.4
Weighted Precision: 0.7263284150417274
Weighted AreaUnderROC: 0.7984191976414654
Root mean squared error: 0.4178214596616909
Relative absolute error: 42.265278313488956
Root relative squared error: 88.63331623561274
Weighted TruePositiveRate: 0.724
Weighted MatthewsCorrelation: 0.5868295251902927
Weighted FMeasure: 0.7243990034698093
Iteration time: 210.0
Weighted AreaUnderPRC: 0.6346500368063195
Mean absolute error: 0.18784568139328514
Coverage of cases: 77.48
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 5164.0
Weighted Recall: 0.724
Weighted FalsePositiveRate: 0.1384789100089551
Kappa statistic: 0.58582876504462
Training time: 205.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 36.253333333334275
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.7260294524051356
Weighted AreaUnderROC: 0.7966611572517782
Root mean squared error: 0.42198386028766016
Relative absolute error: 42.44629222501689
Root relative squared error: 89.51629474820413
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5879363542738664
Weighted FMeasure: 0.7253926340086803
Iteration time: 213.0
Weighted AreaUnderPRC: 0.6318669550386643
Mean absolute error: 0.1886501876667426
Coverage of cases: 75.36
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 5377.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.13773466573092383
Kappa statistic: 0.5877047433038929
Training time: 208.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 40.000000000000696
Incorrectly Classified Instances: 26.84
Correctly Classified Instances: 73.16
Weighted Precision: 0.7324913932267395
Weighted AreaUnderROC: 0.8143950830134485
Root mean squared error: 0.41167615178352235
Relative absolute error: 41.081179462642886
Root relative squared error: 87.32969957367311
Weighted TruePositiveRate: 0.7316
Weighted MatthewsCorrelation: 0.5975209885714967
Weighted FMeasure: 0.7317958556894566
Iteration time: 221.0
Weighted AreaUnderPRC: 0.6614061588562625
Mean absolute error: 0.18258301983396924
Coverage of cases: 79.24
Instances selection time: 5.0
Test time: 6.0
Accumulative iteration time: 5598.0
Weighted Recall: 0.7316
Weighted FalsePositiveRate: 0.1345778739219021
Kappa statistic: 0.5973087984966835
Training time: 216.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 40.14666666666732
Incorrectly Classified Instances: 25.24
Correctly Classified Instances: 74.76
Weighted Precision: 0.7495307026646041
Weighted AreaUnderROC: 0.8442737192381164
Root mean squared error: 0.3968048701703609
Relative absolute error: 38.65097302217336
Root relative squared error: 84.17502435159274
Weighted TruePositiveRate: 0.7476
Weighted MatthewsCorrelation: 0.6220594862106772
Weighted FMeasure: 0.7479089573689675
Iteration time: 223.0
Weighted AreaUnderPRC: 0.6986134422428278
Mean absolute error: 0.17178210232077132
Coverage of cases: 81.48
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 5821.0
Weighted Recall: 0.7476
Weighted FalsePositiveRate: 0.1266069320620337
Kappa statistic: 0.6212796881313256
Training time: 219.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 38.76000000000093
Incorrectly Classified Instances: 25.68
Correctly Classified Instances: 74.32
Weighted Precision: 0.745692063919309
Weighted AreaUnderROC: 0.8367687527419987
Root mean squared error: 0.40255864226239974
Relative absolute error: 39.04988438973302
Root relative squared error: 85.3955837306975
Weighted TruePositiveRate: 0.7432
Weighted MatthewsCorrelation: 0.6157729770681358
Weighted FMeasure: 0.7435333890132655
Iteration time: 229.0
Weighted AreaUnderPRC: 0.6857350457786483
Mean absolute error: 0.17355504173214759
Coverage of cases: 80.24
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6050.0
Weighted Recall: 0.7432
Weighted FalsePositiveRate: 0.12887807506806662
Kappa statistic: 0.61462837089126
Training time: 225.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 37.29333333333433
Incorrectly Classified Instances: 25.44
Correctly Classified Instances: 74.56
Weighted Precision: 0.7469879411642644
Weighted AreaUnderROC: 0.8305079925063478
Root mean squared error: 0.40135957534516953
Relative absolute error: 38.74270146968492
Root relative squared error: 85.14122322621652
Weighted TruePositiveRate: 0.7456
Weighted MatthewsCorrelation: 0.6187760782017443
Weighted FMeasure: 0.7459413011672638
Iteration time: 235.0
Weighted AreaUnderPRC: 0.6780827594020115
Mean absolute error: 0.17218978430971155
Coverage of cases: 79.52
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6285.0
Weighted Recall: 0.7456
Weighted FalsePositiveRate: 0.1275887089278396
Kappa statistic: 0.6182874406004842
Training time: 231.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 36.3333333333343
Incorrectly Classified Instances: 25.28
Correctly Classified Instances: 74.72
Weighted Precision: 0.7487357942699846
Weighted AreaUnderROC: 0.8178573199351533
Root mean squared error: 0.40281069774114814
Relative absolute error: 38.51801748213734
Root relative squared error: 85.44905277217497
Weighted TruePositiveRate: 0.7472
Weighted MatthewsCorrelation: 0.6213301389633814
Weighted FMeasure: 0.7473816392012482
Iteration time: 229.0
Weighted AreaUnderPRC: 0.6578894902061567
Mean absolute error: 0.1711911888095001
Coverage of cases: 77.88
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6514.0
Weighted Recall: 0.7472
Weighted FalsePositiveRate: 0.12668793375981427
Kappa statistic: 0.6207296681096542
Training time: 225.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 37.6666666666675
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7536205415348408
Weighted AreaUnderROC: 0.8284664679297268
Root mean squared error: 0.3959320098324756
Relative absolute error: 37.745673262032525
Root relative squared error: 83.9898627124085
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6286770872376852
Weighted FMeasure: 0.7519398815944456
Iteration time: 232.0
Weighted AreaUnderPRC: 0.6826620933572645
Mean absolute error: 0.16775854783125646
Coverage of cases: 80.0
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 6746.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12428772502459216
Kappa statistic: 0.6279072447179632
Training time: 228.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 37.4133333333342
Incorrectly Classified Instances: 24.96
Correctly Classified Instances: 75.04
Weighted Precision: 0.7536606040522552
Weighted AreaUnderROC: 0.8296796741186394
Root mean squared error: 0.3992856392312037
Relative absolute error: 37.99373213649091
Root relative squared error: 84.70127493923665
Weighted TruePositiveRate: 0.7504
Weighted MatthewsCorrelation: 0.6269271304748972
Weighted FMeasure: 0.7508496360259085
Iteration time: 240.0
Weighted AreaUnderPRC: 0.677935412811735
Mean absolute error: 0.1688610317177382
Coverage of cases: 78.76
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 6986.0
Weighted Recall: 0.7504
Weighted FalsePositiveRate: 0.12528462825543468
Kappa statistic: 0.6254331829223462
Training time: 237.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 37.053333333334194
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7532863426924998
Weighted AreaUnderROC: 0.8282527602847993
Root mean squared error: 0.39729198983831293
Relative absolute error: 37.59360348524048
Root relative squared error: 84.27835803773021
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6283710858130382
Weighted FMeasure: 0.752301967978585
Iteration time: 246.0
Weighted AreaUnderPRC: 0.6759528434016229
Mean absolute error: 0.16708268215662514
Coverage of cases: 78.96
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 7232.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12434952163640318
Kappa statistic: 0.6278936668942517
Training time: 242.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 36.33333333333424
Incorrectly Classified Instances: 24.8
Correctly Classified Instances: 75.2
Weighted Precision: 0.7536347703660784
Weighted AreaUnderROC: 0.8136079153870672
Root mean squared error: 0.4009810662251705
Relative absolute error: 37.95457736338044
Root relative squared error: 85.06092931656885
Weighted TruePositiveRate: 0.752
Weighted MatthewsCorrelation: 0.6285780382135999
Weighted FMeasure: 0.7522719340456889
Iteration time: 243.0
Weighted AreaUnderPRC: 0.6534664782614378
Mean absolute error: 0.16868701050391383
Coverage of cases: 77.8
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 7475.0
Weighted Recall: 0.752
Weighted FalsePositiveRate: 0.12434952163640318
Kappa statistic: 0.6278834828761581
Training time: 239.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 37.46666666666749
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7515787886100387
Weighted AreaUnderROC: 0.8279408176378371
Root mean squared error: 0.39949127960516867
Relative absolute error: 37.917712415288506
Root relative squared error: 84.74489785011157
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.6254856552823308
Weighted FMeasure: 0.7503404519699604
Iteration time: 252.0
Weighted AreaUnderPRC: 0.6780180506131785
Mean absolute error: 0.16852316629017192
Coverage of cases: 79.32
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 7727.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.12537249624341648
Kappa statistic: 0.6248928093692053
Training time: 249.0
		
Time end:Wed Nov 01 17.24.56 EET 2017