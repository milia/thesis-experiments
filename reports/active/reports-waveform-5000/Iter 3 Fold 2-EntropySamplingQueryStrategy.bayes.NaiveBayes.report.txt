Wed Nov 01 15.06.17 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 15.06.17 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.000000000000924
Incorrectly Classified Instances: 20.44
Correctly Classified Instances: 79.56
Weighted Precision: 0.8277443688554267
Weighted AreaUnderROC: 0.9512479553271933
Root mean squared error: 0.33950952344845914
Relative absolute error: 31.12232963079452
Root relative squared error: 72.02084589234542
Weighted TruePositiveRate: 0.7956
Weighted MatthewsCorrelation: 0.7108519498398488
Weighted FMeasure: 0.7833743885041751
Iteration time: 120.0
Weighted AreaUnderPRC: 0.9096524803963877
Mean absolute error: 0.13832146502575407
Coverage of cases: 88.28
Instances selection time: 115.0
Test time: 130.0
Accumulative iteration time: 120.0
Weighted Recall: 0.7956
Weighted FalsePositiveRate: 0.10114919118715267
Kappa statistic: 0.6939224621140698
Training time: 5.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 40.37333333333421
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8285975827716228
Weighted AreaUnderROC: 0.951936380424279
Root mean squared error: 0.3365723277609462
Relative absolute error: 30.89510051008686
Root relative squared error: 71.39777259585173
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.7145828655427604
Weighted FMeasure: 0.7873411884602459
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9108388872382307
Mean absolute error: 0.1373115578226089
Coverage of cases: 88.88
Instances selection time: 92.0
Test time: 125.0
Accumulative iteration time: 217.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.09958796100814488
Kappa statistic: 0.6986978235080868
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 40.426666666667536
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8289585107537332
Weighted AreaUnderROC: 0.9516618584806996
Root mean squared error: 0.33530313191626854
Relative absolute error: 30.798813887295946
Root relative squared error: 71.12853549932413
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.714709455916943
Weighted FMeasure: 0.7876382519237478
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9104087391718769
Mean absolute error: 0.1368836172768715
Coverage of cases: 89.08
Instances selection time: 91.0
Test time: 125.0
Accumulative iteration time: 313.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.09958109471794364
Kappa statistic: 0.6986964518302483
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.52000000000086
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8304662683177532
Weighted AreaUnderROC: 0.9518599953309226
Root mean squared error: 0.33362588361458245
Relative absolute error: 30.61951252773594
Root relative squared error: 70.77273740496737
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7182968129554276
Weighted FMeasure: 0.7909963241767552
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9104506330062333
Mean absolute error: 0.1360867223454937
Coverage of cases: 89.12
Instances selection time: 90.0
Test time: 128.0
Accumulative iteration time: 408.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09820386058973482
Kappa statistic: 0.7028787218034303
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.66666666666752
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8348551769413771
Weighted AreaUnderROC: 0.9524435366164801
Root mean squared error: 0.32947420275886485
Relative absolute error: 30.208890112051407
Root relative squared error: 69.8920328990473
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7272655403741255
Weighted FMeasure: 0.7992911113341052
Iteration time: 102.0
Weighted AreaUnderPRC: 0.911389063630954
Mean absolute error: 0.1342617338313402
Coverage of cases: 89.72
Instances selection time: 92.0
Test time: 125.0
Accumulative iteration time: 510.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09484934014951166
Kappa statistic: 0.7130374915618645
Training time: 10.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 41.00000000000079
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8325609160802987
Weighted AreaUnderROC: 0.9523069236056552
Root mean squared error: 0.3273034529990319
Relative absolute error: 30.092931350377494
Root relative squared error: 69.4315473364162
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7253615375543188
Weighted FMeasure: 0.7989114939201075
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9114241280125843
Mean absolute error: 0.13374636155723393
Coverage of cases: 90.16
Instances selection time: 88.0
Test time: 125.0
Accumulative iteration time: 604.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09527226257271944
Kappa statistic: 0.7118288217669051
Training time: 6.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.93333333333413
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8309037708275513
Weighted AreaUnderROC: 0.9519568795529827
Root mean squared error: 0.33106835523614214
Relative absolute error: 30.521031472070472
Root relative squared error: 70.23020370712572
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7197443950794433
Weighted FMeasure: 0.7927867042201963
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9106878723701509
Mean absolute error: 0.1356490287647583
Coverage of cases: 89.6
Instances selection time: 87.0
Test time: 125.0
Accumulative iteration time: 696.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09761754098633174
Kappa statistic: 0.7046664144250268
Training time: 5.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 41.13333333333411
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8319626641174955
Weighted AreaUnderROC: 0.9524199073267653
Root mean squared error: 0.3286949361786344
Relative absolute error: 30.319177895115644
Root relative squared error: 69.72672549407739
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7230717401765121
Weighted FMeasure: 0.7963321891685736
Iteration time: 91.0
Weighted AreaUnderPRC: 0.911257957190884
Mean absolute error: 0.13475190175607016
Coverage of cases: 90.2
Instances selection time: 85.0
Test time: 126.0
Accumulative iteration time: 787.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09624717314832416
Kappa statistic: 0.7088452106115497
Training time: 6.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 41.133333333334114
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8321116543712729
Weighted AreaUnderROC: 0.9532120962350323
Root mean squared error: 0.32726427077486314
Relative absolute error: 30.16853398255819
Root relative squared error: 69.4232355314927
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7239605954725994
Weighted FMeasure: 0.7973646151935428
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9126300276816455
Mean absolute error: 0.13408237325581482
Coverage of cases: 90.4
Instances selection time: 84.0
Test time: 129.0
Accumulative iteration time: 877.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09585858217612252
Kappa statistic: 0.7100380976390391
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 41.22666666666741
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.830717437069389
Weighted AreaUnderROC: 0.9538038033465575
Root mean squared error: 0.3259367277734937
Relative absolute error: 30.087463262109452
Root relative squared error: 69.14162113391717
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7224870152645757
Weighted FMeasure: 0.7964873661227216
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9136556726821536
Mean absolute error: 0.1337220589427093
Coverage of cases: 90.56
Instances selection time: 84.0
Test time: 127.0
Accumulative iteration time: 967.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09626777201892783
Kappa statistic: 0.70883858226676
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 41.373333333334045
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8317398677725291
Weighted AreaUnderROC: 0.9543341357978737
Root mean squared error: 0.3245562130468827
Relative absolute error: 29.980941505991982
Root relative squared error: 68.84876973650282
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7245632180312108
Weighted FMeasure: 0.7985939331946562
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9144330157288676
Mean absolute error: 0.13324862891552056
Coverage of cases: 90.88
Instances selection time: 83.0
Test time: 126.0
Accumulative iteration time: 1056.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.0954768574941221
Kappa statistic: 0.7112270835924501
Training time: 6.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 41.320000000000746
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8325872164976871
Weighted AreaUnderROC: 0.954613362523499
Root mean squared error: 0.32531854310932873
Relative absolute error: 30.10589795299369
Root relative squared error: 69.0104843635002
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7249710990891423
Weighted FMeasure: 0.7983131562316206
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9148589158355468
Mean absolute error: 0.1338039909021948
Coverage of cases: 90.84
Instances selection time: 82.0
Test time: 126.0
Accumulative iteration time: 1144.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09546312491371964
Kappa statistic: 0.7112336577421492
Training time: 6.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 41.46666666666738
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8325262516392464
Weighted AreaUnderROC: 0.9552530754113812
Root mean squared error: 0.3225561407843725
Relative absolute error: 29.86012138560818
Root relative squared error: 68.42449033859758
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.726115173913726
Weighted FMeasure: 0.8001354624443738
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9157757177452011
Mean absolute error: 0.13271165060270365
Coverage of cases: 91.16
Instances selection time: 81.0
Test time: 127.0
Accumulative iteration time: 1232.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.0948836716005178
Kappa statistic: 0.7130191985962265
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 41.50666666666739
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.831396000828662
Weighted AreaUnderROC: 0.9547448103494509
Root mean squared error: 0.32335379814770865
Relative absolute error: 30.007151195296746
Root relative squared error: 68.5936990178011
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7239852030689412
Weighted FMeasure: 0.7982241176552487
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9150041206708014
Mean absolute error: 0.1333651164235417
Coverage of cases: 91.04
Instances selection time: 79.0
Test time: 126.0
Accumulative iteration time: 1319.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09567458612532352
Kappa statistic: 0.7106279696579946
Training time: 8.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 41.813333333333944
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8301233351767369
Weighted AreaUnderROC: 0.9554081565745451
Root mean squared error: 0.32172753410170796
Relative absolute error: 29.91843683166391
Root relative squared error: 68.24871631732302
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7233533081071373
Weighted FMeasure: 0.7983770807575427
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9160113304542105
Mean absolute error: 0.13297083036295135
Coverage of cases: 91.36
Instances selection time: 79.0
Test time: 125.0
Accumulative iteration time: 1406.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.0956951849959272
Kappa statistic: 0.7106213815690144
Training time: 8.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.65333333333398
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8289812724770164
Weighted AreaUnderROC: 0.954137721013233
Root mean squared error: 0.32457753468482237
Relative absolute error: 30.246847621403344
Root relative squared error: 68.85329273893475
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7200033014821184
Weighted FMeasure: 0.795114766951537
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9139120990134306
Mean absolute error: 0.13443043387290438
Coverage of cases: 91.04
Instances selection time: 78.0
Test time: 125.0
Accumulative iteration time: 1494.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.0970655528339348
Kappa statistic: 0.7064395187525238
Training time: 10.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 41.920000000000556
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8303265721044975
Weighted AreaUnderROC: 0.9536375840488128
Root mean squared error: 0.3234521571141066
Relative absolute error: 30.200529538448098
Root relative squared error: 68.61456410544025
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7223136509987207
Weighted FMeasure: 0.79667664862293
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9133905799428655
Mean absolute error: 0.13422457572643662
Coverage of cases: 91.16
Instances selection time: 77.0
Test time: 126.0
Accumulative iteration time: 1578.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09627463830912907
Kappa statistic: 0.708834605115021
Training time: 7.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 42.12000000000053
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8314670821299956
Weighted AreaUnderROC: 0.9532082806014293
Root mean squared error: 0.3225941091443841
Relative absolute error: 30.184393100633248
Root relative squared error: 68.432544644048
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7236925527436577
Weighted FMeasure: 0.7977728561532196
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9129765525891583
Mean absolute error: 0.13415285822503728
Coverage of cases: 91.28
Instances selection time: 77.0
Test time: 126.0
Accumulative iteration time: 1662.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09587231475652497
Kappa statistic: 0.7100301760332511
Training time: 7.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 42.40000000000041
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8296423832209379
Weighted AreaUnderROC: 0.9517551353707182
Root mean squared error: 0.3231168670062864
Relative absolute error: 30.359968215244464
Root relative squared error: 68.54343833276893
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7220781087105184
Weighted FMeasure: 0.7971998028939503
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9110410980443434
Mean absolute error: 0.1349331920677538
Coverage of cases: 91.32
Instances selection time: 76.0
Test time: 124.0
Accumulative iteration time: 1746.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09629523717973273
Kappa statistic: 0.7088239988458406
Training time: 8.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 42.38666666666706
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8310096627776115
Weighted AreaUnderROC: 0.9503939401736521
Root mean squared error: 0.3246634361061444
Relative absolute error: 30.589035784689816
Root relative squared error: 68.87151518219386
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7236527961600159
Weighted FMeasure: 0.7977235578019696
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9091180791909774
Mean absolute error: 0.1359512701541776
Coverage of cases: 91.04
Instances selection time: 75.0
Test time: 124.0
Accumulative iteration time: 1828.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09589291362712864
Kappa statistic: 0.7100262150680444
Training time: 7.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 42.386666666667054
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8310031351245707
Weighted AreaUnderROC: 0.9501080298734218
Root mean squared error: 0.3243658604335322
Relative absolute error: 30.58651540938099
Root relative squared error: 68.80838984938781
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.723636125100549
Weighted FMeasure: 0.7977396314719708
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9088328824746598
Mean absolute error: 0.13594006848613838
Coverage of cases: 91.04
Instances selection time: 74.0
Test time: 133.0
Accumulative iteration time: 1912.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09589291362712864
Kappa statistic: 0.7100262150680444
Training time: 10.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 42.306666666667084
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8292715259909835
Weighted AreaUnderROC: 0.949168369241622
Root mean squared error: 0.3258583109691253
Relative absolute error: 30.743433006884334
Root relative squared error: 69.12498641767881
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.721275584696298
Weighted FMeasure: 0.7957794626251925
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9074964292382978
Mean absolute error: 0.13663748003059767
Coverage of cases: 90.8
Instances selection time: 74.0
Test time: 123.0
Accumulative iteration time: 1994.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09669756073233682
Kappa statistic: 0.707632399985717
Training time: 8.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 42.3200000000004
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8287969922730141
Weighted AreaUnderROC: 0.9489567498885132
Root mean squared error: 0.32582304616177665
Relative absolute error: 30.727351233269186
Root relative squared error: 69.11750562235477
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.720262866692534
Weighted FMeasure: 0.7950566693715432
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9072025305199948
Mean absolute error: 0.13656600548119702
Coverage of cases: 90.84
Instances selection time: 72.0
Test time: 124.0
Accumulative iteration time: 2074.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.0970930179947397
Kappa statistic: 0.7064341721168063
Training time: 8.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 42.346666666667055
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8284158806179864
Weighted AreaUnderROC: 0.9487185666655343
Root mean squared error: 0.32664870407676516
Relative absolute error: 30.86174863908887
Root relative squared error: 69.29265411554339
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7193371083481289
Weighted FMeasure: 0.7943083614938818
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9070322128451411
Mean absolute error: 0.13716332728484007
Coverage of cases: 90.84
Instances selection time: 71.0
Test time: 124.0
Accumulative iteration time: 2154.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09748847525714256
Kappa statistic: 0.7052359442478952
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 42.37333333333371
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8300774378122653
Weighted AreaUnderROC: 0.9485017733097904
Root mean squared error: 0.3254897101413886
Relative absolute error: 30.750939926450073
Root relative squared error: 69.04679437422574
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7221608736160526
Weighted FMeasure: 0.7965052674716891
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9067055568613428
Mean absolute error: 0.13667084411755653
Coverage of cases: 90.92
Instances selection time: 70.0
Test time: 125.0
Accumulative iteration time: 2233.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.0964998321011354
Kappa statistic: 0.7082275283672341
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 42.306666666667056
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8298602233240027
Weighted AreaUnderROC: 0.9474538928612987
Root mean squared error: 0.32750438224574596
Relative absolute error: 31.015901774901987
Root relative squared error: 69.47417086628326
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7216761554024071
Weighted FMeasure: 0.7961337211538643
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9053684316120363
Mean absolute error: 0.13784845233289836
Coverage of cases: 90.64
Instances selection time: 70.0
Test time: 129.0
Accumulative iteration time: 2316.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09669756073233683
Kappa statistic: 0.7076284062488917
Training time: 13.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 42.42666666666703
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8287447118622188
Weighted AreaUnderROC: 0.9466978959518522
Root mean squared error: 0.3278191594094574
Relative absolute error: 31.08773849809436
Root relative squared error: 69.54094518639018
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7207759743504053
Weighted FMeasure: 0.7960665157038346
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9043610619925488
Mean absolute error: 0.1381677266581978
Coverage of cases: 90.72
Instances selection time: 70.0
Test time: 126.0
Accumulative iteration time: 2399.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09691588823414193
Kappa statistic: 0.7070212799611183
Training time: 13.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 42.32000000000037
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8282642205349251
Weighted AreaUnderROC: 0.9464406751803847
Root mean squared error: 0.3297262783393374
Relative absolute error: 31.307634608233812
Root relative squared error: 69.94550620474438
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7189622571007389
Weighted FMeasure: 0.7941724266768994
Iteration time: 146.0
Weighted AreaUnderPRC: 0.9040165730187273
Mean absolute error: 0.13914504270326203
Coverage of cases: 90.64
Instances selection time: 132.0
Test time: 125.0
Accumulative iteration time: 2545.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09769307017854524
Kappa statistic: 0.7046327956571794
Training time: 14.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 42.42666666666702
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8263688920493071
Weighted AreaUnderROC: 0.9462542316946511
Root mean squared error: 0.32885583191780193
Relative absolute error: 31.217417945164517
Root relative squared error: 69.7608566345462
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7172150157575926
Weighted FMeasure: 0.793468210597564
Iteration time: 80.0
Weighted AreaUnderPRC: 0.903488780256915
Mean absolute error: 0.1387440797562874
Coverage of cases: 90.6
Instances selection time: 68.0
Test time: 123.0
Accumulative iteration time: 2625.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09810912631155176
Kappa statistic: 0.7034291498829535
Training time: 12.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 42.45333333333367
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8263459593328083
Weighted AreaUnderROC: 0.9453737323202062
Root mean squared error: 0.3290008795398385
Relative absolute error: 31.347820165601004
Root relative squared error: 69.79162588168731
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7176695892245516
Weighted FMeasure: 0.793952411490852
Iteration time: 75.0
Weighted AreaUnderPRC: 0.902231445220318
Mean absolute error: 0.13932364518044957
Coverage of cases: 90.68
Instances selection time: 66.0
Test time: 124.0
Accumulative iteration time: 2700.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09792513026075278
Kappa statistic: 0.7040242398156178
Training time: 9.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 42.413333333333654
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8265891946423893
Weighted AreaUnderROC: 0.9450575643750845
Root mean squared error: 0.3309021866694387
Relative absolute error: 31.55309596867945
Root relative squared error: 70.1949540310249
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7170181130395437
Weighted FMeasure: 0.7931349322179675
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9022121078786979
Mean absolute error: 0.14023598208302043
Coverage of cases: 90.6
Instances selection time: 64.0
Test time: 123.0
Accumulative iteration time: 2774.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09830685494275321
Kappa statistic: 0.7028300168524139
Training time: 10.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 42.22666666666702
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.827020287059777
Weighted AreaUnderROC: 0.9452583776329513
Root mean squared error: 0.3317950288289097
Relative absolute error: 31.54555457060094
Root relative squared error: 70.38435445467226
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7168322782472129
Weighted FMeasure: 0.7924782371440237
Iteration time: 75.0
Weighted AreaUnderPRC: 0.902499380011496
Mean absolute error: 0.14020246475822706
Coverage of cases: 90.28
Instances selection time: 63.0
Test time: 125.0
Accumulative iteration time: 2849.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09849085099355219
Kappa statistic: 0.7022376629934258
Training time: 12.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 42.28000000000036
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8289063181438687
Weighted AreaUnderROC: 0.9443920014224062
Root mean squared error: 0.33063750593726327
Relative absolute error: 31.483967918458216
Root relative squared error: 70.1388067688537
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7201864860942615
Weighted FMeasure: 0.7952918541399598
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9012163849712355
Mean absolute error: 0.1399287463042594
Coverage of cases: 90.48
Instances selection time: 62.0
Test time: 126.0
Accumulative iteration time: 2922.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09731134549654481
Kappa statistic: 0.7058243425024271
Training time: 11.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 42.17333333333371
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8273876630802021
Weighted AreaUnderROC: 0.9438540147198806
Root mean squared error: 0.33260313794298296
Relative absolute error: 31.645315664203792
Root relative squared error: 70.55578028502222
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7162295033447394
Weighted FMeasure: 0.7913874973066324
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9006632955492659
Mean absolute error: 0.14064584739646196
Coverage of cases: 90.28
Instances selection time: 61.0
Test time: 127.0
Accumulative iteration time: 2994.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.0988725756755526
Kappa statistic: 0.7010475915459373
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 42.1200000000004
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8274891485165992
Weighted AreaUnderROC: 0.9440397490262515
Root mean squared error: 0.3322559271722985
Relative absolute error: 31.55066656730316
Root relative squared error: 70.48212575788662
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7174939829980637
Weighted FMeasure: 0.7928584992017255
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9007914002015758
Mean absolute error: 0.14022518474357026
Coverage of cases: 90.28
Instances selection time: 61.0
Test time: 127.0
Accumulative iteration time: 3066.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09829312236235076
Kappa statistic: 0.702836782383781
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 42.06666666666708
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8276184978000584
Weighted AreaUnderROC: 0.9445219226190776
Root mean squared error: 0.33277016017887934
Relative absolute error: 31.590290962601003
Root relative squared error: 70.59121105170559
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7171746831983286
Weighted FMeasure: 0.7921825617597938
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9015313873621111
Mean absolute error: 0.14040129316711622
Coverage of cases: 90.2
Instances selection time: 66.0
Test time: 127.0
Accumulative iteration time: 3143.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.09848398470335097
Kappa statistic: 0.7022430861084239
Training time: 11.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 42.00000000000042
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8280402897494696
Weighted AreaUnderROC: 0.9451616068853713
Root mean squared error: 0.33170615959659633
Relative absolute error: 31.410090874950267
Root relative squared error: 70.36550244362998
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.717291071137399
Weighted FMeasure: 0.7921047491665663
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9022978376704341
Mean absolute error: 0.1396004038886685
Coverage of cases: 90.12
Instances selection time: 59.0
Test time: 126.0
Accumulative iteration time: 3214.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.0984702521229485
Kappa statistic: 0.7022471533150376
Training time: 12.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 42.133333333333724
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.829596573281604
Weighted AreaUnderROC: 0.9462203314466696
Root mean squared error: 0.33130048288190567
Relative absolute error: 31.386388967113362
Root relative squared error: 70.2794454168518
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7193192840412423
Weighted FMeasure: 0.7932199511301227
Iteration time: 70.0
Weighted AreaUnderPRC: 0.904043608010581
Mean absolute error: 0.13949506207606005
Coverage of cases: 90.4
Instances selection time: 58.0
Test time: 134.0
Accumulative iteration time: 3284.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09787019993914298
Kappa statistic: 0.7040458017279413
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 42.08000000000041
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8289510422031335
Weighted AreaUnderROC: 0.9475301745989183
Root mean squared error: 0.33011453877652547
Relative absolute error: 31.187524277523245
Root relative squared error: 70.02786868114504
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.718952269557198
Weighted FMeasure: 0.7936188298739729
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9058991172728822
Mean absolute error: 0.13861121901121506
Coverage of cases: 90.44
Instances selection time: 63.0
Test time: 126.0
Accumulative iteration time: 3359.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09787706622934421
Kappa statistic: 0.7040390639678225
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 42.053333333333775
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8290745064865052
Weighted AreaUnderROC: 0.9479630266896314
Root mean squared error: 0.3301536324147326
Relative absolute error: 31.20110739899512
Root relative squared error: 70.03616169414829
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7193798249726726
Weighted FMeasure: 0.7939751961077971
Iteration time: 68.0
Weighted AreaUnderPRC: 0.90650826344058
Mean absolute error: 0.13867158843997895
Coverage of cases: 90.52
Instances selection time: 56.0
Test time: 127.0
Accumulative iteration time: 3427.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09767933759814278
Kappa statistic: 0.7046381751743652
Training time: 12.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 42.00000000000042
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8287408948063767
Weighted AreaUnderROC: 0.9491103233589024
Root mean squared error: 0.32980862873204936
Relative absolute error: 31.10678999685451
Root relative squared error: 69.96297536108038
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7179765555922418
Weighted FMeasure: 0.7925536483691672
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9081204122769851
Mean absolute error: 0.13825239998602068
Coverage of cases: 90.56
Instances selection time: 56.0
Test time: 127.0
Accumulative iteration time: 3495.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09825879091134462
Kappa statistic: 0.7028476065932365
Training time: 12.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 42.05333333333382
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8308117577398568
Weighted AreaUnderROC: 0.9485454132820073
Root mean squared error: 0.3298296638479604
Relative absolute error: 31.116401024292113
Root relative squared error: 69.96743758301152
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7208956157914285
Weighted FMeasure: 0.7944709745248156
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9069974559194157
Mean absolute error: 0.13829511566352115
Coverage of cases: 90.44
Instances selection time: 54.0
Test time: 128.0
Accumulative iteration time: 3561.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.097256415174935
Kappa statistic: 0.7058457730816411
Training time: 12.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 42.16000000000042
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8309991823818065
Weighted AreaUnderROC: 0.9498693545904041
Root mean squared error: 0.32898853868408295
Relative absolute error: 31.03339179027038
Root relative squared error: 69.7890079908502
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7210356989132428
Weighted FMeasure: 0.7945785058298674
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9090357556529937
Mean absolute error: 0.13792618573453566
Coverage of cases: 90.72
Instances selection time: 53.0
Test time: 129.0
Accumulative iteration time: 3627.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.097256415174935
Kappa statistic: 0.7058444337619179
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 42.1333333333338
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8303001627050577
Weighted AreaUnderROC: 0.9498048874976421
Root mean squared error: 0.3302130910836969
Relative absolute error: 31.162904748958848
Root relative squared error: 70.04877478255578
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7194294328157668
Weighted FMeasure: 0.7929416459476314
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9086677193342934
Mean absolute error: 0.1385017988842622
Coverage of cases: 90.52
Instances selection time: 53.0
Test time: 126.0
Accumulative iteration time: 3693.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09784273477833808
Kappa statistic: 0.7040552340767338
Training time: 13.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 42.08000000000047
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8286403331355141
Weighted AreaUnderROC: 0.949498093411903
Root mean squared error: 0.3308938481997562
Relative absolute error: 31.215545610178843
Root relative squared error: 70.19318517448774
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7165658234188164
Weighted FMeasure: 0.7906539257917222
Iteration time: 65.0
Weighted AreaUnderPRC: 0.9082309393290867
Mean absolute error: 0.13873575826746218
Coverage of cases: 90.48
Instances selection time: 52.0
Test time: 126.0
Accumulative iteration time: 3758.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09883137793434525
Kappa statistic: 0.7010652859037965
Training time: 13.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 42.01333333333381
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.832640054144997
Weighted AreaUnderROC: 0.9498698037180598
Root mean squared error: 0.33146419707227803
Relative absolute error: 31.207619894988195
Root relative squared error: 70.31417444110843
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7201003071196522
Weighted FMeasure: 0.7920468142622765
Iteration time: 64.0
Weighted AreaUnderPRC: 0.9086387386906896
Mean absolute error: 0.13870053286661485
Coverage of cases: 90.48
Instances selection time: 51.0
Test time: 129.0
Accumulative iteration time: 3822.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09799926566833217
Kappa statistic: 0.7034723566747115
Training time: 13.0
		
Time end:Wed Nov 01 15.06.30 EET 2017