Wed Nov 01 16.49.13 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.49.13 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7035221000814861
Weighted AreaUnderROC: 0.7776523575588099
Root mean squared error: 0.4432035379259692
Relative absolute error: 44.791252485089224
Root relative squared error: 94.0176681339964
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5552922089703185
Weighted FMeasure: 0.7034098142100442
Iteration time: 769.0
Weighted AreaUnderPRC: 0.5940269508530682
Mean absolute error: 0.19907223326706414
Coverage of cases: 70.36
Instances selection time: 769.0
Test time: 949.0
Accumulative iteration time: 769.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14829528488238045
Kappa statistic: 0.5553986484118911
Training time: 0.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.6
Correctly Classified Instances: 70.4
Weighted Precision: 0.704081638001319
Weighted AreaUnderROC: 0.7779615213097123
Root mean squared error: 0.4429545281470154
Relative absolute error: 44.718929254301
Root relative squared error: 93.96484518301239
Weighted TruePositiveRate: 0.704
Weighted MatthewsCorrelation: 0.55607991843888
Weighted FMeasure: 0.7034665035115848
Iteration time: 783.0
Weighted AreaUnderPRC: 0.594499273757921
Mean absolute error: 0.19875079668578316
Coverage of cases: 70.4
Instances selection time: 783.0
Test time: 1004.0
Accumulative iteration time: 1552.0
Weighted Recall: 0.704
Weighted FalsePositiveRate: 0.14807695738057533
Kappa statistic: 0.5560168713588882
Training time: 0.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.36
Correctly Classified Instances: 70.64
Weighted Precision: 0.7065395690461521
Weighted AreaUnderROC: 0.779778739219021
Root mean squared error: 0.44120144699980346
Relative absolute error: 44.34917127071865
Root relative squared error: 93.59296051286323
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.5597921665239309
Weighted FMeasure: 0.7056531936813346
Iteration time: 800.0
Weighted AreaUnderPRC: 0.5970774225950609
Mean absolute error: 0.1971074278698616
Coverage of cases: 70.64
Instances selection time: 799.0
Test time: 1025.0
Accumulative iteration time: 2352.0
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.14684252156195818
Kappa statistic: 0.5596368143623193
Training time: 1.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.24
Correctly Classified Instances: 70.76
Weighted Precision: 0.7079827442470951
Weighted AreaUnderROC: 0.7807130967619298
Root mean squared error: 0.44034182399797966
Relative absolute error: 44.15914742451083
Root relative squared error: 93.41060693670717
Weighted TruePositiveRate: 0.7076
Weighted MatthewsCorrelation: 0.5618774551269033
Weighted FMeasure: 0.7064757964038119
Iteration time: 818.0
Weighted AreaUnderPRC: 0.5983715478885099
Mean absolute error: 0.19626287744227128
Coverage of cases: 70.76
Instances selection time: 818.0
Test time: 1051.0
Accumulative iteration time: 3170.0
Weighted Recall: 0.7076
Weighted FalsePositiveRate: 0.14617380647614037
Kappa statistic: 0.5614726552104847
Training time: 0.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.4
Correctly Classified Instances: 70.6
Weighted Precision: 0.7066814150821646
Weighted AreaUnderROC: 0.7795348052250299
Root mean squared error: 0.44158499515342997
Relative absolute error: 44.387650085762225
Root relative squared error: 93.6743233629655
Weighted TruePositiveRate: 0.706
Weighted MatthewsCorrelation: 0.5597349747521678
Weighted FMeasure: 0.70465943017809
Iteration time: 834.0
Weighted AreaUnderPRC: 0.5966570300542029
Mean absolute error: 0.19727844482561083
Coverage of cases: 70.6
Instances selection time: 834.0
Test time: 1084.0
Accumulative iteration time: 4004.0
Weighted Recall: 0.706
Weighted FalsePositiveRate: 0.14693038954993995
Kappa statistic: 0.559091144678572
Training time: 0.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.2
Correctly Classified Instances: 70.8
Weighted Precision: 0.7092348608346675
Weighted AreaUnderROC: 0.7810668913991403
Root mean squared error: 0.44011779809687
Relative absolute error: 44.079601990050186
Root relative squared error: 93.36308386655635
Weighted TruePositiveRate: 0.708
Weighted MatthewsCorrelation: 0.5632218562599067
Weighted FMeasure: 0.7061831754192558
Iteration time: 850.0
Weighted AreaUnderPRC: 0.5989101987300419
Mean absolute error: 0.19590934217800174
Coverage of cases: 70.8
Instances selection time: 850.0
Test time: 1118.0
Accumulative iteration time: 4854.0
Weighted Recall: 0.708
Weighted FalsePositiveRate: 0.14586621720171933
Kappa statistic: 0.5621304500963072
Training time: 0.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.36
Correctly Classified Instances: 70.64
Weighted Precision: 0.7083613964415576
Weighted AreaUnderROC: 0.7798885998622405
Root mean squared error: 0.4413569418777951
Relative absolute error: 44.30947030497683
Root relative squared error: 93.62594595766353
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.5613695416791711
Weighted FMeasure: 0.7041912282739288
Iteration time: 866.0
Weighted AreaUnderPRC: 0.5973210971189412
Mean absolute error: 0.19693097913323124
Coverage of cases: 70.64
Instances selection time: 866.0
Test time: 1150.0
Accumulative iteration time: 5720.0
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.1466228002755189
Kappa statistic: 0.5597592614895636
Training time: 0.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.36
Correctly Classified Instances: 70.64
Weighted Precision: 0.7091836949925324
Weighted AreaUnderROC: 0.7799229313132466
Root mean squared error: 0.4413897953110534
Relative absolute error: 44.30108864696866
Root relative squared error: 93.63291522329618
Weighted TruePositiveRate: 0.7064
Weighted MatthewsCorrelation: 0.5620344617130121
Weighted FMeasure: 0.7036718060815265
Iteration time: 882.0
Weighted AreaUnderPRC: 0.5974501552234583
Mean absolute error: 0.1968937273198616
Coverage of cases: 70.64
Instances selection time: 882.0
Test time: 1183.0
Accumulative iteration time: 6602.0
Weighted Recall: 0.7064
Weighted FalsePositiveRate: 0.14655413737350667
Kappa statistic: 0.5597953804183552
Training time: 0.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.48
Correctly Classified Instances: 70.52
Weighted Precision: 0.7085594040633608
Weighted AreaUnderROC: 0.7790400709468469
Root mean squared error: 0.442321819655858
Relative absolute error: 44.472398190045936
Root relative squared error: 93.83062744362887
Weighted TruePositiveRate: 0.7052
Weighted MatthewsCorrelation: 0.5606725034477095
Weighted FMeasure: 0.7021643744318159
Iteration time: 896.0
Weighted AreaUnderPRC: 0.5962639765289256
Mean absolute error: 0.19765510306687176
Coverage of cases: 70.52
Instances selection time: 896.0
Test time: 1215.0
Accumulative iteration time: 7498.0
Weighted Recall: 0.7052
Weighted FalsePositiveRate: 0.1471198581063061
Kappa statistic: 0.5580163244901417
Training time: 0.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7073273293358668
Weighted AreaUnderROC: 0.7778480468295448
Root mean squared error: 0.4435497173578321
Relative absolute error: 44.703953147877264
Root relative squared error: 94.09110388112965
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5585442698601906
Weighted FMeasure: 0.7003628001629866
Iteration time: 909.0
Weighted AreaUnderPRC: 0.5946317146130002
Mean absolute error: 0.1986842362127888
Coverage of cases: 70.36
Instances selection time: 909.0
Test time: 1249.0
Accumulative iteration time: 8407.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14790390634091058
Kappa statistic: 0.5556316758694687
Training time: 0.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7065603810800459
Weighted AreaUnderROC: 0.7766663221475443
Root mean squared error: 0.444772884036268
Relative absolute error: 44.93598862019675
Root relative squared error: 94.35057671698269
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5567891576674234
Weighted FMeasure: 0.6982440744633419
Iteration time: 923.0
Weighted AreaUnderPRC: 0.5931086775181922
Mean absolute error: 0.19971550497865317
Coverage of cases: 70.2
Instances selection time: 923.0
Test time: 1281.0
Accumulative iteration time: 9330.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.1486673557049114
Kappa statistic: 0.553263467337503
Training time: 0.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7074302923631817
Weighted AreaUnderROC: 0.7766903541632486
Root mean squared error: 0.4447989992409617
Relative absolute error: 44.92946058091441
Root relative squared error: 94.356116588482
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5574625161098523
Weighted FMeasure: 0.6977608328706616
Iteration time: 940.0
Weighted AreaUnderPRC: 0.5932558406727505
Mean absolute error: 0.19968649147073164
Coverage of cases: 70.2
Instances selection time: 940.0
Test time: 1315.0
Accumulative iteration time: 10270.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.14861929167350285
Kappa statistic: 0.553289932702079
Training time: 0.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.8
Correctly Classified Instances: 70.2
Weighted Precision: 0.7082614757790591
Weighted AreaUnderROC: 0.7767075198887516
Root mean squared error: 0.44482371351069977
Relative absolute error: 44.923283983847945
Root relative squared error: 94.36135927679915
Weighted TruePositiveRate: 0.702
Weighted MatthewsCorrelation: 0.5580687416052147
Weighted FMeasure: 0.6974074079687164
Iteration time: 953.0
Weighted AreaUnderPRC: 0.5934127067690579
Mean absolute error: 0.19965903992821404
Coverage of cases: 70.2
Instances selection time: 952.0
Test time: 1347.0
Accumulative iteration time: 11223.0
Weighted Recall: 0.702
Weighted FalsePositiveRate: 0.1485849602224967
Kappa statistic: 0.5533082530407416
Training time: 1.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.76
Correctly Classified Instances: 70.24
Weighted Precision: 0.7086912047302292
Weighted AreaUnderROC: 0.7770063842043524
Root mean squared error: 0.44454848643492356
Relative absolute error: 44.85766710353652
Root relative squared error: 94.30297479730488
Weighted TruePositiveRate: 0.7024
Weighted MatthewsCorrelation: 0.5587014060159056
Weighted FMeasure: 0.6977692421360907
Iteration time: 961.0
Weighted AreaUnderPRC: 0.5938497695787138
Mean absolute error: 0.19936740934905212
Coverage of cases: 70.24
Instances selection time: 961.0
Test time: 1378.0
Accumulative iteration time: 12184.0
Weighted Recall: 0.7024
Weighted FalsePositiveRate: 0.14838723159129527
Kappa statistic: 0.5539078392782709
Training time: 0.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7082796621688475
Weighted AreaUnderROC: 0.7764155218633522
Root mean squared error: 0.4451678321970382
Relative absolute error: 44.97164750957766
Root relative squared error: 94.43435787379201
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5577859852483061
Weighted FMeasure: 0.696788516642923
Iteration time: 969.0
Weighted AreaUnderPRC: 0.593066470288498
Mean absolute error: 0.1998739889314572
Coverage of cases: 70.16
Instances selection time: 969.0
Test time: 1410.0
Accumulative iteration time: 13153.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.1487689562732957
Kappa statistic: 0.5527188577806653
Training time: 0.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.44
Correctly Classified Instances: 69.56
Weighted Precision: 0.7044156970822323
Weighted AreaUnderROC: 0.7719325571293415
Root mean squared error: 0.449642389144336
Relative absolute error: 45.86301369863122
Root relative squared error: 95.38355474186392
Weighted TruePositiveRate: 0.6956
Weighted MatthewsCorrelation: 0.5503242583418331
Weighted FMeasure: 0.6900225495608707
Iteration time: 975.0
Weighted AreaUnderPRC: 0.5871174501895389
Mean absolute error: 0.20383561643836196
Coverage of cases: 69.56
Instances selection time: 975.0
Test time: 1445.0
Accumulative iteration time: 14128.0
Weighted Recall: 0.6956
Weighted FalsePositiveRate: 0.15173488574131722
Kappa statistic: 0.5437419015686406
Training time: 0.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.703399939365826
Weighted AreaUnderROC: 0.7707439661571397
Root mean squared error: 0.45084291263770654
Relative absolute error: 46.09720534629466
Root relative squared error: 95.63822423280473
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5483457354170268
Weighted FMeasure: 0.6881813407552744
Iteration time: 989.0
Weighted AreaUnderPRC: 0.5855402926399709
Mean absolute error: 0.204876468205755
Coverage of cases: 69.4
Instances selection time: 989.0
Test time: 1475.0
Accumulative iteration time: 15117.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.1525120676857205
Kappa statistic: 0.5413562377829891
Training time: 0.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.700342714139146
Weighted AreaUnderROC: 0.7677553230011326
Root mean squared error: 0.4537995289011304
Relative absolute error: 46.690391459075414
Root relative squared error: 96.26541725557476
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.5429852705305405
Weighted FMeasure: 0.6839415922606464
Iteration time: 991.0
Weighted AreaUnderPRC: 0.5815127925247715
Mean absolute error: 0.20751285092922503
Coverage of cases: 69.0
Instances selection time: 991.0
Test time: 1509.0
Accumulative iteration time: 16108.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.15448935399773486
Kappa statistic: 0.5353672457289756
Training time: 0.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.08
Correctly Classified Instances: 68.92
Weighted Precision: 0.7002318406749429
Weighted AreaUnderROC: 0.7671610275150319
Root mean squared error: 0.4544033530870506
Relative absolute error: 46.80556199304885
Root relative squared error: 96.39350770852735
Weighted TruePositiveRate: 0.6892
Weighted MatthewsCorrelation: 0.5423100985251419
Weighted FMeasure: 0.6828032149417435
Iteration time: 999.0
Weighted AreaUnderPRC: 0.5808260441056748
Mean absolute error: 0.20802471996910699
Coverage of cases: 68.92
Instances selection time: 999.0
Test time: 1566.0
Accumulative iteration time: 17107.0
Weighted Recall: 0.6892
Weighted FalsePositiveRate: 0.15487794496993648
Kappa statistic: 0.5341788057951274
Training time: 0.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.7001893906698495
Weighted AreaUnderROC: 0.766862163199431
Root mean squared error: 0.45471349444437975
Relative absolute error: 46.861155152888756
Root relative squared error: 96.45929862559551
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.5419790199065427
Weighted FMeasure: 0.6822522275795169
Iteration time: 1024.0
Weighted AreaUnderPRC: 0.5804833510888759
Mean absolute error: 0.20827180067950657
Coverage of cases: 68.88
Instances selection time: 1024.0
Test time: 1586.0
Accumulative iteration time: 18131.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.15507567360113791
Kappa statistic: 0.5335814184592407
Training time: 0.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.0
Correctly Classified Instances: 69.0
Weighted Precision: 0.701724580708915
Weighted AreaUnderROC: 0.7677621892913339
Root mean squared error: 0.4538529709080058
Relative absolute error: 46.67774086378761
Root relative squared error: 96.2767540172133
Weighted TruePositiveRate: 0.69
Weighted MatthewsCorrelation: 0.54406221349795
Weighted FMeasure: 0.6832535567161798
Iteration time: 1013.0
Weighted AreaUnderPRC: 0.581805161645252
Mean absolute error: 0.20745662606127926
Coverage of cases: 69.0
Instances selection time: 1013.0
Test time: 1619.0
Accumulative iteration time: 19144.0
Weighted Recall: 0.69
Weighted FalsePositiveRate: 0.15447562141733237
Kappa statistic: 0.5353820646206211
Training time: 0.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.28
Correctly Classified Instances: 68.72
Weighted Precision: 0.6998772720321085
Weighted AreaUnderROC: 0.7656735722272293
Root mean squared error: 0.4559143462658232
Relative absolute error: 47.09252437703186
Root relative squared error: 96.71403776543836
Weighted TruePositiveRate: 0.6872
Weighted MatthewsCorrelation: 0.5404873745194325
Weighted FMeasure: 0.6802641510257781
Iteration time: 1020.0
Weighted AreaUnderPRC: 0.5790662058936472
Mean absolute error: 0.2093001083423648
Coverage of cases: 68.72
Instances selection time: 1020.0
Test time: 1811.0
Accumulative iteration time: 20164.0
Weighted Recall: 0.6872
Weighted FalsePositiveRate: 0.1558528555455412
Kappa statistic: 0.5311919235519106
Training time: 0.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.32
Correctly Classified Instances: 68.68
Weighted Precision: 0.7002568916894184
Weighted AreaUnderROC: 0.7653850073469306
Root mean squared error: 0.45622142222651396
Relative absolute error: 47.14867444326595
Root relative squared error: 96.7791784136815
Weighted TruePositiveRate: 0.6868
Weighted MatthewsCorrelation: 0.5404286284375691
Weighted FMeasure: 0.6796524982577199
Iteration time: 1218.0
Weighted AreaUnderPRC: 0.5787958164878172
Mean absolute error: 0.2095496641922941
Coverage of cases: 68.68
Instances selection time: 1218.0
Test time: 1671.0
Accumulative iteration time: 21382.0
Weighted Recall: 0.6868
Weighted FalsePositiveRate: 0.15602998530613899
Kappa statistic: 0.5306009794193407
Training time: 0.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.52
Correctly Classified Instances: 68.48
Weighted Precision: 0.6991960328574535
Weighted AreaUnderROC: 0.763897552059128
Root mean squared error: 0.45769080064608636
Relative absolute error: 47.444236760124305
Root relative squared error: 97.09088064706415
Weighted TruePositiveRate: 0.6848
Weighted MatthewsCorrelation: 0.5380696355582071
Weighted FMeasure: 0.6773667357267824
Iteration time: 1029.0
Weighted AreaUnderPRC: 0.5769248872232653
Mean absolute error: 0.21086327448944234
Coverage of cases: 68.48
Instances selection time: 1029.0
Test time: 1886.0
Accumulative iteration time: 22411.0
Weighted Recall: 0.6848
Weighted FalsePositiveRate: 0.15700489588174368
Kappa statistic: 0.5276186054965532
Training time: 0.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.56
Correctly Classified Instances: 68.44
Weighted Precision: 0.699942360388016
Weighted AreaUnderROC: 0.7636158534690305
Root mean squared error: 0.457995582732676
Relative absolute error: 47.50071210579752
Root relative squared error: 97.15553469112767
Weighted TruePositiveRate: 0.6844
Weighted MatthewsCorrelation: 0.5382962010888864
Weighted FMeasure: 0.6765337052732899
Iteration time: 1031.0
Weighted AreaUnderPRC: 0.576741619001698
Mean absolute error: 0.21111427602576777
Coverage of cases: 68.44
Instances selection time: 1031.0
Test time: 1735.0
Accumulative iteration time: 23442.0
Weighted Recall: 0.6844
Weighted FalsePositiveRate: 0.157168293061939
Kappa statistic: 0.5270406845717435
Training time: 0.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.6
Correctly Classified Instances: 68.4
Weighted Precision: 0.7007738407721233
Weighted AreaUnderROC: 0.7633341548789329
Root mean squared error: 0.45829962366280025
Relative absolute error: 47.557328015952855
Root relative squared error: 97.2200315121624
Weighted TruePositiveRate: 0.684
Weighted MatthewsCorrelation: 0.5385722103277676
Weighted FMeasure: 0.6757302342172099
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.5765624746132282
Mean absolute error: 0.21136590229312477
Coverage of cases: 68.4
Instances selection time: 1034.0
Test time: 1770.0
Accumulative iteration time: 24476.0
Weighted Recall: 0.684
Weighted FalsePositiveRate: 0.1573316902421343
Kappa statistic: 0.5264585018201976
Training time: 0.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.72
Correctly Classified Instances: 68.28
Weighted Precision: 0.6998564295687515
Weighted AreaUnderROC: 0.7624375619321306
Root mean squared error: 0.45918236126147055
Relative absolute error: 47.733724340175165
Root relative squared error: 97.40728843477083
Weighted TruePositiveRate: 0.6828
Weighted MatthewsCorrelation: 0.5369627120272328
Weighted FMeasure: 0.674455733453204
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5753740262849276
Mean absolute error: 0.21214988595633508
Coverage of cases: 68.28
Instances selection time: 1037.0
Test time: 1796.0
Accumulative iteration time: 25513.0
Weighted Recall: 0.6828
Weighted FalsePositiveRate: 0.15792487613573858
Kappa statistic: 0.5246624084090996
Training time: 0.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.84
Correctly Classified Instances: 68.16
Weighted Precision: 0.698925541710456
Weighted AreaUnderROC: 0.7615409689853285
Root mean squared error: 0.4600629942264809
Relative absolute error: 47.91025886864937
Root relative squared error: 97.59409889715941
Weighted TruePositiveRate: 0.6816
Weighted MatthewsCorrelation: 0.5353468762763383
Weighted FMeasure: 0.6731595546592725
Iteration time: 1039.0
Weighted AreaUnderPRC: 0.5741917452605435
Mean absolute error: 0.21293448386066485
Coverage of cases: 68.16
Instances selection time: 1039.0
Test time: 1828.0
Accumulative iteration time: 26552.0
Weighted Recall: 0.6816
Weighted FalsePositiveRate: 0.1585180620293429
Kappa statistic: 0.5228685049576121
Training time: 0.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.88
Correctly Classified Instances: 68.12
Weighted Precision: 0.6986103896078428
Weighted AreaUnderROC: 0.7612421046697279
Root mean squared error: 0.46036430296033126
Relative absolute error: 47.96726246472389
Root relative squared error: 97.6580161318403
Weighted TruePositiveRate: 0.6812
Weighted MatthewsCorrelation: 0.5348059659073184
Weighted FMeasure: 0.6727201120927466
Iteration time: 1036.0
Weighted AreaUnderPRC: 0.5737994546171385
Mean absolute error: 0.2131878331765516
Coverage of cases: 68.12
Instances selection time: 1036.0
Test time: 1858.0
Accumulative iteration time: 27588.0
Weighted Recall: 0.6812
Weighted FalsePositiveRate: 0.15871579066054434
Kappa statistic: 0.5222712698377305
Training time: 0.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.96
Correctly Classified Instances: 68.04
Weighted Precision: 0.6983206982883925
Weighted AreaUnderROC: 0.7606443760385264
Root mean squared error: 0.4609535328911456
Relative absolute error: 48.084210526317094
Root relative squared error: 97.78301067576737
Weighted TruePositiveRate: 0.6804
Weighted MatthewsCorrelation: 0.5339903927096453
Weighted FMeasure: 0.6717012050993801
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5730821009545846
Mean absolute error: 0.2137076023391881
Coverage of cases: 68.04
Instances selection time: 1037.0
Test time: 1889.0
Accumulative iteration time: 28625.0
Weighted Recall: 0.6804
Weighted FalsePositiveRate: 0.15911124792294723
Kappa statistic: 0.521076815922068
Training time: 0.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.08
Correctly Classified Instances: 67.92
Weighted Precision: 0.6979836526478684
Weighted AreaUnderROC: 0.7597546493819256
Root mean squared error: 0.4618296485395223
Relative absolute error: 48.261106074341946
Root relative squared error: 97.96886287058861
Weighted TruePositiveRate: 0.6792
Weighted MatthewsCorrelation: 0.5328235236845887
Weighted FMeasure: 0.6701824025879267
Iteration time: 1044.0
Weighted AreaUnderPRC: 0.572001573718735
Mean absolute error: 0.2144938047748541
Coverage of cases: 67.92
Instances selection time: 1044.0
Test time: 1922.0
Accumulative iteration time: 29669.0
Weighted Recall: 0.6792
Weighted FalsePositiveRate: 0.15969070123614906
Kappa statistic: 0.5192873656971616
Training time: 0.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.6976702440190861
Weighted AreaUnderROC: 0.7594557850663247
Root mean squared error: 0.4621286401351396
Relative absolute error: 48.318432769367575
Root relative squared error: 98.03228856602225
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5322840732697361
Weighted FMeasure: 0.6697416047644211
Iteration time: 1035.0
Weighted AreaUnderPRC: 0.5716112467926671
Mean absolute error: 0.2147485900860791
Coverage of cases: 67.88
Instances selection time: 1035.0
Test time: 1953.0
Accumulative iteration time: 30704.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.15988842986735052
Kappa statistic: 0.5186901659350117
Training time: 0.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.12
Correctly Classified Instances: 67.88
Weighted Precision: 0.6976702440190861
Weighted AreaUnderROC: 0.7594557850663247
Root mean squared error: 0.4621394101938427
Relative absolute error: 48.31601049868693
Root relative squared error: 98.03457324048507
Weighted TruePositiveRate: 0.6788
Weighted MatthewsCorrelation: 0.5322840732697361
Weighted FMeasure: 0.6697416047644211
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.5716112467926671
Mean absolute error: 0.2147378244386096
Coverage of cases: 67.88
Instances selection time: 1033.0
Test time: 1987.0
Accumulative iteration time: 31738.0
Weighted Recall: 0.6788
Weighted FalsePositiveRate: 0.15988842986735052
Kappa statistic: 0.5186901659350117
Training time: 1.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.2
Correctly Classified Instances: 67.8
Weighted Precision: 0.6963724395434244
Weighted AreaUnderROC: 0.758844323854721
Root mean squared error: 0.4627249777217089
Relative absolute error: 48.43336199484004
Root relative squared error: 98.15879087142412
Weighted TruePositiveRate: 0.678
Weighted MatthewsCorrelation: 0.5308029577575012
Weighted FMeasure: 0.668953008240371
Iteration time: 1033.0
Weighted AreaUnderPRC: 0.5706705616681793
Mean absolute error: 0.21525938664373453
Coverage of cases: 67.8
Instances selection time: 1033.0
Test time: 2042.0
Accumulative iteration time: 32771.0
Weighted Recall: 0.678
Weighted FalsePositiveRate: 0.16031135229055826
Kappa statistic: 0.5174803965172275
Training time: 0.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 32.16
Correctly Classified Instances: 67.84
Weighted Precision: 0.6954712224072019
Weighted AreaUnderROC: 0.7591019904291143
Root mean squared error: 0.46244753934003613
Relative absolute error: 48.371259509720254
Root relative squared error: 98.09993730311145
Weighted TruePositiveRate: 0.6784
Weighted MatthewsCorrelation: 0.5306225694248976
Weighted FMeasure: 0.6695388452083895
Iteration time: 1053.0
Weighted AreaUnderPRC: 0.5707290886989764
Mean absolute error: 0.2149833755987577
Coverage of cases: 67.84
Instances selection time: 1053.0
Test time: 2064.0
Accumulative iteration time: 33824.0
Weighted Recall: 0.6784
Weighted FalsePositiveRate: 0.16019601914177156
Kappa statistic: 0.518024909240014
Training time: 0.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.84
Correctly Classified Instances: 68.16
Weighted Precision: 0.6987765353447285
Weighted AreaUnderROC: 0.761482605518618
Root mean squared error: 0.460150741962529
Relative absolute error: 47.890274314214594
Root relative squared error: 97.61271300291742
Weighted TruePositiveRate: 0.6816
Weighted MatthewsCorrelation: 0.5352436897338287
Weighted FMeasure: 0.6733032992322745
Iteration time: 1041.0
Weighted AreaUnderPRC: 0.5741186926244862
Mean absolute error: 0.21284566361873253
Coverage of cases: 68.16
Instances selection time: 1041.0
Test time: 2088.0
Accumulative iteration time: 34865.0
Weighted Recall: 0.6816
Weighted FalsePositiveRate: 0.15863478896276373
Kappa statistic: 0.5227902430508428
Training time: 0.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.32
Correctly Classified Instances: 68.68
Weighted Precision: 0.7043854619573975
Weighted AreaUnderROC: 0.765354109041025
Root mean squared error: 0.4563870659697231
Relative absolute error: 47.11005723630237
Root relative squared error: 96.81431675790681
Weighted TruePositiveRate: 0.6868
Weighted MatthewsCorrelation: 0.5429779623197799
Weighted FMeasure: 0.6791126585787611
Iteration time: 1039.0
Weighted AreaUnderPRC: 0.5796865979607012
Mean absolute error: 0.20937803216134485
Coverage of cases: 68.68
Instances selection time: 1039.0
Test time: 2114.0
Accumulative iteration time: 35904.0
Weighted Recall: 0.6868
Weighted FalsePositiveRate: 0.15609178191795
Kappa statistic: 0.5305389508774664
Training time: 0.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.32
Correctly Classified Instances: 68.68
Weighted Precision: 0.7030368494039283
Weighted AreaUnderROC: 0.7653060450096164
Root mean squared error: 0.45639604772795805
Relative absolute error: 47.10796460176931
Root relative squared error: 96.81622207655327
Weighted TruePositiveRate: 0.6868
Weighted MatthewsCorrelation: 0.5419777945637397
Weighted FMeasure: 0.6792671282519026
Iteration time: 1038.0
Weighted AreaUnderPRC: 0.5792888957653036
Mean absolute error: 0.20936873156342012
Coverage of cases: 68.68
Instances selection time: 1038.0
Test time: 2141.0
Accumulative iteration time: 36942.0
Weighted Recall: 0.6868
Weighted FalsePositiveRate: 0.15618790998076715
Kappa statistic: 0.5304790456935634
Training time: 0.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.24
Correctly Classified Instances: 68.76
Weighted Precision: 0.7036182625406069
Weighted AreaUnderROC: 0.7658625758996105
Root mean squared error: 0.4558214840067163
Relative absolute error: 46.98622327790922
Root relative squared error: 96.69433870549912
Weighted TruePositiveRate: 0.6876
Weighted MatthewsCorrelation: 0.5427687380898425
Weighted FMeasure: 0.6801896527257704
Iteration time: 1039.0
Weighted AreaUnderPRC: 0.5800246184863925
Mean absolute error: 0.20882765901293082
Coverage of cases: 68.76
Instances selection time: 1038.0
Test time: 2176.0
Accumulative iteration time: 37981.0
Weighted Recall: 0.6876
Weighted FalsePositiveRate: 0.15587484820077901
Kappa statistic: 0.5316228351199633
Training time: 1.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.48
Correctly Classified Instances: 68.52
Weighted Precision: 0.7013939236417222
Weighted AreaUnderROC: 0.7640075933941951
Root mean squared error: 0.45757748721304237
Relative absolute error: 47.3434138737325
Root relative squared error: 97.06684323799267
Weighted TruePositiveRate: 0.6852
Weighted MatthewsCorrelation: 0.5390373034975017
Weighted FMeasure: 0.6773093870875929
Iteration time: 1191.0
Weighted AreaUnderPRC: 0.577405455408695
Mean absolute error: 0.21041517277214544
Coverage of cases: 68.52
Instances selection time: 1191.0
Test time: 2206.0
Accumulative iteration time: 39172.0
Weighted Recall: 0.6852
Weighted FalsePositiveRate: 0.15718481321160965
Kappa statistic: 0.5279600026774925
Training time: 0.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 31.12
Correctly Classified Instances: 68.88
Weighted Precision: 0.7049813526666401
Weighted AreaUnderROC: 0.7666870727992998
Root mean squared error: 0.45496173216325486
Relative absolute error: 46.80276285494954
Root relative squared error: 96.51195779790437
Weighted TruePositiveRate: 0.6888
Weighted MatthewsCorrelation: 0.5442047769345331
Weighted FMeasure: 0.6810974208946246
Iteration time: 1037.0
Weighted AreaUnderPRC: 0.5811397204316974
Mean absolute error: 0.20801227935533229
Coverage of cases: 68.88
Instances selection time: 1037.0
Test time: 2240.0
Accumulative iteration time: 40209.0
Weighted Recall: 0.6888
Weighted FalsePositiveRate: 0.15542585440140042
Kappa statistic: 0.5333262632462161
Training time: 0.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.84
Correctly Classified Instances: 69.16
Weighted Precision: 0.7076426901362014
Weighted AreaUnderROC: 0.7687585241379011
Root mean squared error: 0.45291823709185497
Relative absolute error: 46.38185941043205
Root relative squared error: 96.07846703121191
Weighted TruePositiveRate: 0.6916
Weighted MatthewsCorrelation: 0.5480843653160805
Weighted FMeasure: 0.6839506590613498
Iteration time: 1038.0
Weighted AreaUnderPRC: 0.5839928417976877
Mean absolute error: 0.20614159737969895
Coverage of cases: 69.16
Instances selection time: 1038.0
Test time: 2271.0
Accumulative iteration time: 41247.0
Weighted Recall: 0.6916
Weighted FalsePositiveRate: 0.15408295172419775
Kappa statistic: 0.5374850657467078
Training time: 0.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.72
Correctly Classified Instances: 69.28
Weighted Precision: 0.7090652453229541
Weighted AreaUnderROC: 0.7696345182140997
Root mean squared error: 0.4520438315216127
Relative absolute error: 46.20044676098194
Root relative squared error: 95.89297759874424
Weighted TruePositiveRate: 0.6928
Weighted MatthewsCorrelation: 0.5498627732774171
Weighted FMeasure: 0.6849174207314481
Iteration time: 1034.0
Weighted AreaUnderPRC: 0.5852246889623137
Mean absolute error: 0.20533531893769846
Coverage of cases: 69.28
Instances selection time: 1034.0
Test time: 2302.0
Accumulative iteration time: 42281.0
Weighted Recall: 0.6928
Weighted FalsePositiveRate: 0.1535309635718008
Kappa statistic: 0.539253225467394
Training time: 0.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.76
Correctly Classified Instances: 69.24
Weighted Precision: 0.7093850388416758
Weighted AreaUnderROC: 0.7693047555925934
Root mean squared error: 0.45234542766199304
Relative absolute error: 46.258547322084254
Root relative squared error: 95.95695580155703
Weighted TruePositiveRate: 0.6924
Weighted MatthewsCorrelation: 0.5493967638182335
Weighted FMeasure: 0.6844290409813252
Iteration time: 1032.0
Weighted AreaUnderPRC: 0.5849221711386621
Mean absolute error: 0.20559354365370874
Coverage of cases: 69.24
Instances selection time: 1032.0
Test time: 2358.0
Accumulative iteration time: 43313.0
Weighted Recall: 0.6924
Weighted FalsePositiveRate: 0.15379048881481325
Kappa statistic: 0.5386175325337637
Training time: 0.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.8
Correctly Classified Instances: 69.2
Weighted Precision: 0.7096515211565766
Weighted AreaUnderROC: 0.7689784261161876
Root mean squared error: 0.4526466286734672
Relative absolute error: 46.316702819956554
Root relative squared error: 96.02085018487112
Weighted TruePositiveRate: 0.692
Weighted MatthewsCorrelation: 0.5488838240560626
Weighted FMeasure: 0.684168693769842
Iteration time: 1039.0
Weighted AreaUnderPRC: 0.584666314497365
Mean absolute error: 0.20585201253314123
Coverage of cases: 69.2
Instances selection time: 1039.0
Test time: 2369.0
Accumulative iteration time: 44352.0
Weighted Recall: 0.692
Weighted FalsePositiveRate: 0.15404314776762448
Kappa statistic: 0.5379880616115119
Training time: 0.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 33.33333333333432
Incorrectly Classified Instances: 30.6
Correctly Classified Instances: 69.4
Weighted Precision: 0.7112871887955182
Weighted AreaUnderROC: 0.7704624482588895
Root mean squared error: 0.45118156868776443
Relative absolute error: 46.01568068424797
Root relative squared error: 95.71006402965047
Weighted TruePositiveRate: 0.694
Weighted MatthewsCorrelation: 0.5516131756706227
Weighted FMeasure: 0.6860057496929776
Iteration time: 1025.0
Weighted AreaUnderPRC: 0.586615374789916
Mean absolute error: 0.20451413637443636
Coverage of cases: 69.4
Instances selection time: 1025.0
Test time: 2598.0
Accumulative iteration time: 45377.0
Weighted Recall: 0.694
Weighted FalsePositiveRate: 0.153075103482221
Kappa statistic: 0.5409651133486144
Training time: 0.0
		
Time end:Wed Nov 01 16.51.20 EET 2017