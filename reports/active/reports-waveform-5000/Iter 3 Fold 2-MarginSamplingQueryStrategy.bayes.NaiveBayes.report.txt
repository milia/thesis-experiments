Wed Nov 01 16.40.33 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.40.33 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.000000000000924
Incorrectly Classified Instances: 20.44
Correctly Classified Instances: 79.56
Weighted Precision: 0.8277443688554267
Weighted AreaUnderROC: 0.9512479553271933
Root mean squared error: 0.33950952344845914
Relative absolute error: 31.12232963079452
Root relative squared error: 72.02084589234542
Weighted TruePositiveRate: 0.7956
Weighted MatthewsCorrelation: 0.7108519498398488
Weighted FMeasure: 0.7833743885041751
Iteration time: 103.0
Weighted AreaUnderPRC: 0.9096524803963877
Mean absolute error: 0.13832146502575407
Coverage of cases: 88.28
Instances selection time: 99.0
Test time: 130.0
Accumulative iteration time: 103.0
Weighted Recall: 0.7956
Weighted FalsePositiveRate: 0.10114919118715267
Kappa statistic: 0.6939224621140698
Training time: 4.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 40.29333333333421
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8291308633156786
Weighted AreaUnderROC: 0.9518417171218534
Root mean squared error: 0.336501140900727
Relative absolute error: 30.852788597394877
Root relative squared error: 71.38267158237403
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.7143719908177946
Weighted FMeasure: 0.7870402337406045
Iteration time: 99.0
Weighted AreaUnderPRC: 0.9106643418174637
Mean absolute error: 0.1371235048773112
Coverage of cases: 88.8
Instances selection time: 94.0
Test time: 126.0
Accumulative iteration time: 202.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.09977195705894384
Kappa statistic: 0.6981015619889741
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 40.440000000000865
Incorrectly Classified Instances: 20.04
Correctly Classified Instances: 79.96
Weighted Precision: 0.8295341448671592
Weighted AreaUnderROC: 0.9521094415826872
Root mean squared error: 0.33391982421299105
Relative absolute error: 30.604434308185905
Root relative squared error: 70.83509162208759
Weighted TruePositiveRate: 0.7996
Weighted MatthewsCorrelation: 0.7158022911150856
Weighted FMeasure: 0.788481144846599
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9111048319025824
Mean absolute error: 0.13601970803638244
Coverage of cases: 89.08
Instances selection time: 93.0
Test time: 127.0
Accumulative iteration time: 300.0
Weighted Recall: 0.7996
Weighted FalsePositiveRate: 0.09918563745554076
Kappa statistic: 0.6998931116304179
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.56000000000085
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8327171307545257
Weighted AreaUnderROC: 0.9522113991981618
Root mean squared error: 0.3316721014426301
Relative absolute error: 30.34730048103747
Root relative squared error: 70.35827761814271
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.721811426123604
Weighted FMeasure: 0.793696157241814
Iteration time: 97.0
Weighted AreaUnderPRC: 0.9112462541644927
Mean absolute error: 0.13487689102683384
Coverage of cases: 89.28
Instances selection time: 92.0
Test time: 127.0
Accumulative iteration time: 397.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09701062251232499
Kappa statistic: 0.7064689217680972
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.94666666666747
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8340665503707713
Weighted AreaUnderROC: 0.9518942276182429
Root mean squared error: 0.32730623972217465
Relative absolute error: 29.9449146690562
Root relative squared error: 69.43213848966566
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7268913987091113
Weighted FMeasure: 0.7997159903859021
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9109586801417671
Mean absolute error: 0.1330885096402504
Coverage of cases: 90.0
Instances selection time: 91.0
Test time: 128.0
Accumulative iteration time: 493.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09486307272991412
Kappa statistic: 0.713029652005018
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 40.89333333333413
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8349164048016315
Weighted AreaUnderROC: 0.9516906201416481
Root mean squared error: 0.32936030730780524
Relative absolute error: 30.228218105177156
Root relative squared error: 69.86787202531012
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7269867729429313
Weighted FMeasure: 0.7985769237717354
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9106496691661443
Mean absolute error: 0.13434763602301023
Coverage of cases: 89.8
Instances selection time: 91.0
Test time: 128.0
Accumulative iteration time: 589.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.0950470687807131
Kappa statistic: 0.7124423327061382
Training time: 5.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.94666666666749
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8315520560585427
Weighted AreaUnderROC: 0.9526550844156172
Root mean squared error: 0.3295292465133689
Relative absolute error: 30.29701769366267
Root relative squared error: 69.90370944266883
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7212081843963036
Weighted FMeasure: 0.7943571915814744
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9116547779321936
Mean absolute error: 0.13465341197183472
Coverage of cases: 89.88
Instances selection time: 103.0
Test time: 130.0
Accumulative iteration time: 697.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09702435509272744
Kappa statistic: 0.7064582304439478
Training time: 5.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 41.20000000000076
Incorrectly Classified Instances: 19.52
Correctly Classified Instances: 80.48
Weighted Precision: 0.8302434851530627
Weighted AreaUnderROC: 0.9535184534801611
Root mean squared error: 0.3268102153469591
Relative absolute error: 30.10110498690998
Root relative squared error: 69.32691582986104
Weighted TruePositiveRate: 0.8048
Weighted MatthewsCorrelation: 0.7213641081154285
Weighted FMeasure: 0.7960143189577092
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9130600930126962
Mean absolute error: 0.13378268883071165
Coverage of cases: 90.44
Instances selection time: 88.0
Test time: 131.0
Accumulative iteration time: 791.0
Weighted Recall: 0.8048
Weighted FalsePositiveRate: 0.09665636299112948
Kappa statistic: 0.7076390559713045
Training time: 6.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 41.24000000000076
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8317110379692374
Weighted AreaUnderROC: 0.9535025605480311
Root mean squared error: 0.3268180871251259
Relative absolute error: 30.136641723443308
Root relative squared error: 69.32858568617756
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7225604275865342
Weighted FMeasure: 0.7956165637842229
Iteration time: 93.0
Weighted AreaUnderPRC: 0.9130171048133839
Mean absolute error: 0.1339406298819709
Coverage of cases: 90.6
Instances selection time: 87.0
Test time: 131.0
Accumulative iteration time: 884.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.0964449017795256
Kappa statistic: 0.7082501117276266
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 41.37333333333406
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8312301336117313
Weighted AreaUnderROC: 0.9539077913601832
Root mean squared error: 0.3257352293311486
Relative absolute error: 30.111127402245913
Root relative squared error: 69.09887685942294
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7235328444731463
Weighted FMeasure: 0.7975340521283765
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9137460290314211
Mean absolute error: 0.13382723289887136
Coverage of cases: 90.8
Instances selection time: 86.0
Test time: 131.0
Accumulative iteration time: 976.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09587231475652497
Kappa statistic: 0.7100328166166054
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 41.48000000000071
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8339438124784557
Weighted AreaUnderROC: 0.9547288165189933
Root mean squared error: 0.3241040796145866
Relative absolute error: 29.968456792052745
Root relative squared error: 68.7528577517095
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7268457322921544
Weighted FMeasure: 0.7995441851382001
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9151537773336335
Mean absolute error: 0.13319314129801282
Coverage of cases: 91.12
Instances selection time: 85.0
Test time: 128.0
Accumulative iteration time: 1068.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09486307272991412
Kappa statistic: 0.7130309586275715
Training time: 7.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 41.53333333333404
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8318750916939683
Weighted AreaUnderROC: 0.9549766392906472
Root mean squared error: 0.32465952161664174
Relative absolute error: 30.030421149520055
Root relative squared error: 68.87068479357221
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7242283430288065
Weighted FMeasure: 0.7978112250472688
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9153465403329722
Mean absolute error: 0.133468538442312
Coverage of cases: 91.24
Instances selection time: 84.0
Test time: 128.0
Accumulative iteration time: 1159.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.0956677198351223
Kappa statistic: 0.7106345574470083
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 41.373333333334074
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8296954344080907
Weighted AreaUnderROC: 0.9546299506133042
Root mean squared error: 0.3247702360868293
Relative absolute error: 30.107076279252155
Root relative squared error: 68.89417087936575
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7203036616765283
Weighted FMeasure: 0.7949481154599517
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9147426713701556
Mean absolute error: 0.133809227907788
Coverage of cases: 91.0
Instances selection time: 82.0
Test time: 129.0
Accumulative iteration time: 1248.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09705182025353234
Kappa statistic: 0.7064448651934913
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 41.53333333333404
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8335329167730225
Weighted AreaUnderROC: 0.9553440301943305
Root mean squared error: 0.3230873832066862
Relative absolute error: 29.98543490723002
Root relative squared error: 68.53718387437918
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7274146563229835
Weighted FMeasure: 0.8008818191688645
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9157527784974722
Mean absolute error: 0.1332685995876896
Coverage of cases: 91.36
Instances selection time: 82.0
Test time: 128.0
Accumulative iteration time: 1337.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09448134804791371
Kappa statistic: 0.7142187495506586
Training time: 7.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 41.866666666667236
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8305617238041574
Weighted AreaUnderROC: 0.9552664234795324
Root mean squared error: 0.3205197465225123
Relative absolute error: 29.82600807470524
Root relative squared error: 67.99250588107837
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7247415065929387
Weighted FMeasure: 0.7999960127534149
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9157528035050371
Mean absolute error: 0.13256003588757947
Coverage of cases: 91.68
Instances selection time: 80.0
Test time: 127.0
Accumulative iteration time: 1424.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09510886539252413
Kappa statistic: 0.712410907295656
Training time: 7.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.960000000000555
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8298424825269219
Weighted AreaUnderROC: 0.9538707055872088
Root mean squared error: 0.32266448328631536
Relative absolute error: 30.137407843977996
Root relative squared error: 68.44747325394194
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.7220585610302506
Weighted FMeasure: 0.7969845543958031
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9138460864293251
Mean absolute error: 0.13394403486212506
Coverage of cases: 91.36
Instances selection time: 79.0
Test time: 128.0
Accumulative iteration time: 1510.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09628150459933028
Kappa statistic: 0.7088293020770176
Training time: 7.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 41.84000000000058
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8323154269808374
Weighted AreaUnderROC: 0.9541954523475846
Root mean squared error: 0.32422849938978143
Relative absolute error: 30.27357900965886
Root relative squared error: 68.7792511717357
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7248747259728807
Weighted FMeasure: 0.798516817889364
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9144672412025276
Mean absolute error: 0.1345492400429289
Coverage of cases: 91.2
Instances selection time: 80.0
Test time: 140.0
Accumulative iteration time: 1598.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09546999120392087
Kappa statistic: 0.7112297132882501
Training time: 8.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 42.026666666667204
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8303177133505198
Weighted AreaUnderROC: 0.953643164246721
Root mean squared error: 0.32119918044320234
Relative absolute error: 30.009506682234612
Root relative squared error: 68.13663558088481
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7243355878312557
Weighted FMeasure: 0.7993953031145006
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9136364544119877
Mean absolute error: 0.13337558525437668
Coverage of cases: 91.28
Instances selection time: 77.0
Test time: 129.0
Accumulative iteration time: 1683.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09531346031392679
Kappa statistic: 0.7118130756099437
Training time: 8.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 42.42666666666704
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8316846068874747
Weighted AreaUnderROC: 0.952039510513013
Root mean squared error: 0.3215015734539892
Relative absolute error: 30.139579934265367
Root relative squared error: 68.20078282543805
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7258815086884791
Weighted FMeasure: 0.8003725375670783
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9116143802589337
Mean absolute error: 0.1339536885967356
Coverage of cases: 91.48
Instances selection time: 77.0
Test time: 126.0
Accumulative iteration time: 1768.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09491113676132269
Kappa statistic: 0.71301005123879
Training time: 8.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 42.440000000000374
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8319876482050506
Weighted AreaUnderROC: 0.9498447234005339
Root mean squared error: 0.32343490417053417
Relative absolute error: 30.446635434735867
Root relative squared error: 68.6109042034216
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7261925425136978
Weighted FMeasure: 0.8000532043700931
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9085759489329376
Mean absolute error: 0.13531837970993782
Coverage of cases: 91.28
Instances selection time: 76.0
Test time: 132.0
Accumulative iteration time: 1852.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09491800305152392
Kappa statistic: 0.7130126648289891
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 42.20000000000045
Incorrectly Classified Instances: 19.48
Correctly Classified Instances: 80.52
Weighted Precision: 0.8303252538658932
Weighted AreaUnderROC: 0.9495449826571963
Root mean squared error: 0.325887036036082
Relative absolute error: 30.648906122114465
Root relative squared error: 69.13107992456935
Weighted TruePositiveRate: 0.8052
Weighted MatthewsCorrelation: 0.7221957844693763
Weighted FMeasure: 0.7961577594572209
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9081647750135741
Mean absolute error: 0.1362173605427316
Coverage of cases: 90.76
Instances selection time: 75.0
Test time: 126.0
Accumulative iteration time: 1935.0
Weighted Recall: 0.8052
Weighted FalsePositiveRate: 0.09648609952073293
Kappa statistic: 0.7082341708949746
Training time: 8.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 42.293333333333756
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8307697041001338
Weighted AreaUnderROC: 0.9498372973992663
Root mean squared error: 0.3245879029230325
Relative absolute error: 30.548488385330696
Root relative squared error: 68.85549217439896
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7235960376067675
Weighted FMeasure: 0.7977805940218871
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9087374484080337
Mean absolute error: 0.13577105949035928
Coverage of cases: 90.96
Instances selection time: 75.0
Test time: 125.0
Accumulative iteration time: 2019.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09589977991732987
Kappa statistic: 0.7100235743644544
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 42.266666666667085
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8276441111119087
Weighted AreaUnderROC: 0.948730850820088
Root mean squared error: 0.3258619494811336
Relative absolute error: 30.73874105453606
Root relative squared error: 69.12575826263316
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7188901426034839
Weighted FMeasure: 0.7943678677388685
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9069681862983693
Mean absolute error: 0.1366166269090498
Coverage of cases: 90.84
Instances selection time: 73.0
Test time: 126.0
Accumulative iteration time: 2101.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09749534154734378
Kappa statistic: 0.7052332599452021
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 42.30666666666707
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8279314499006863
Weighted AreaUnderROC: 0.9485655433656821
Root mean squared error: 0.32743493288001785
Relative absolute error: 30.92431778530743
Root relative squared error: 69.45943843104664
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7183260432408338
Weighted FMeasure: 0.7934675893258795
Iteration time: 81.0
Weighted AreaUnderPRC: 0.90710505913235
Mean absolute error: 0.13744141237914478
Coverage of cases: 90.8
Instances selection time: 72.0
Test time: 125.0
Accumulative iteration time: 2182.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09788393251954543
Kappa statistic: 0.7040390639678225
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 42.38666666666704
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8310773285080159
Weighted AreaUnderROC: 0.9484867768983306
Root mean squared error: 0.3253880095323113
Relative absolute error: 30.75829999514086
Root relative squared error: 69.02522041712693
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7242802678019552
Weighted FMeasure: 0.7984941361613792
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9069456587715397
Mean absolute error: 0.13670355553396002
Coverage of cases: 90.96
Instances selection time: 71.0
Test time: 126.0
Accumulative iteration time: 2262.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09571578386653089
Kappa statistic: 0.7106161108818407
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 42.306666666667056
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8289570033732527
Weighted AreaUnderROC: 0.9474783820996537
Root mean squared error: 0.3273378428791708
Relative absolute error: 30.968073363465695
Root relative squared error: 69.43884253165132
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7204440677075841
Weighted FMeasure: 0.7955974313958465
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9055411539276209
Mean absolute error: 0.13763588161540374
Coverage of cases: 90.68
Instances selection time: 70.0
Test time: 125.0
Accumulative iteration time: 2342.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09709988428494092
Kappa statistic: 0.70642615179805
Training time: 10.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 42.29333333333372
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.828296305333656
Weighted AreaUnderROC: 0.9470051130010674
Root mean squared error: 0.32764054082839134
Relative absolute error: 31.03438345079665
Root relative squared error: 69.50305446341486
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7201454817499724
Weighted FMeasure: 0.7956641615545226
Iteration time: 79.0
Weighted AreaUnderPRC: 0.9049414128297943
Mean absolute error: 0.13793059311465242
Coverage of cases: 90.68
Instances selection time: 69.0
Test time: 126.0
Accumulative iteration time: 2421.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09711361686534338
Kappa statistic: 0.7064221414743312
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 42.30666666666704
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.827482408786984
Weighted AreaUnderROC: 0.9465103581962907
Root mean squared error: 0.3286798214185924
Relative absolute error: 31.19385801556199
Root relative squared error: 69.72351916928089
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7185761118751552
Weighted FMeasure: 0.7943440850389805
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9041935085004592
Mean absolute error: 0.13863936895805393
Coverage of cases: 90.6
Instances selection time: 68.0
Test time: 127.0
Accumulative iteration time: 2499.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09770680275894769
Kappa statistic: 0.7046274159440323
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 42.346666666667055
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8272600921741381
Weighted AreaUnderROC: 0.9459728862487
Root mean squared error: 0.328973942018249
Relative absolute error: 31.235133004671034
Root relative squared error: 69.78591157043202
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7185073176286465
Weighted FMeasure: 0.7943236430858273
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9034581632592948
Mean absolute error: 0.13882281335409413
Coverage of cases: 90.6
Instances selection time: 68.0
Test time: 128.0
Accumulative iteration time: 2577.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09771366904914891
Kappa statistic: 0.7046260709851254
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 42.29333333333369
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8268961307233714
Weighted AreaUnderROC: 0.945476340859992
Root mean squared error: 0.3293924742295466
Relative absolute error: 31.312574224841104
Root relative squared error: 69.87469565985808
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7179781459313102
Weighted FMeasure: 0.794068318119857
Iteration time: 77.0
Weighted AreaUnderPRC: 0.9030268431601586
Mean absolute error: 0.13916699655485001
Coverage of cases: 90.64
Instances selection time: 67.0
Test time: 124.0
Accumulative iteration time: 2654.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09791826397055156
Kappa statistic: 0.7040242398156178
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 42.29333333333369
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8257262409849231
Weighted AreaUnderROC: 0.9448542139145732
Root mean squared error: 0.3314614149937217
Relative absolute error: 31.510238283123204
Root relative squared error: 70.31358427312453
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7154264032882333
Weighted FMeasure: 0.7916956841758395
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9021214977362262
Mean absolute error: 0.14004550348054823
Coverage of cases: 90.4
Instances selection time: 66.0
Test time: 125.0
Accumulative iteration time: 2730.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09890004083635752
Kappa statistic: 0.7010367016691379
Training time: 10.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 42.22666666666704
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.827937803494469
Weighted AreaUnderROC: 0.9446615904785515
Root mean squared error: 0.3307240343223555
Relative absolute error: 31.41512575231988
Root relative squared error: 70.15716221121285
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7188722691616241
Weighted FMeasure: 0.7942903190037636
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9017452249701655
Mean absolute error: 0.13962278112142235
Coverage of cases: 90.36
Instances selection time: 64.0
Test time: 149.0
Accumulative iteration time: 2805.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09770680275894768
Kappa statistic: 0.7046287608906909
Training time: 11.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 42.30666666666702
Incorrectly Classified Instances: 20.04
Correctly Classified Instances: 79.96
Weighted Precision: 0.8259609890321561
Weighted AreaUnderROC: 0.9444185621120997
Root mean squared error: 0.3317691153487221
Relative absolute error: 31.557448985616507
Root relative squared error: 70.37885737540282
Weighted TruePositiveRate: 0.7996
Weighted MatthewsCorrelation: 0.7147510730041106
Weighted FMeasure: 0.7906859219862182
Iteration time: 84.0
Weighted AreaUnderPRC: 0.9016103739743568
Mean absolute error: 0.14025532882496292
Coverage of cases: 90.32
Instances selection time: 70.0
Test time: 136.0
Accumulative iteration time: 2889.0
Weighted Recall: 0.7996
Weighted FalsePositiveRate: 0.09928176551835793
Kappa statistic: 0.6998452855220223
Training time: 14.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 42.25333333333369
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8277099352935738
Weighted AreaUnderROC: 0.9446170230512203
Root mean squared error: 0.33150896722408374
Relative absolute error: 31.491082719149706
Root relative squared error: 70.3236716244894
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7179721163855818
Weighted FMeasure: 0.7934467561353509
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9017358128618121
Mean absolute error: 0.13996036764066602
Coverage of cases: 90.32
Instances selection time: 63.0
Test time: 127.0
Accumulative iteration time: 2963.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09809539373114932
Kappa statistic: 0.7034332010545556
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 42.20000000000037
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8282215796139714
Weighted AreaUnderROC: 0.9444680423339316
Root mean squared error: 0.332361719187608
Relative absolute error: 31.568380230712986
Root relative squared error: 70.50456763331283
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7182663317320525
Weighted FMeasure: 0.7931352728595034
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9015572434477567
Mean absolute error: 0.1403039121365028
Coverage of cases: 90.36
Instances selection time: 61.0
Test time: 130.0
Accumulative iteration time: 3036.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09808852744094809
Kappa statistic: 0.7034386024445287
Training time: 12.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 42.133333333333724
Incorrectly Classified Instances: 19.96
Correctly Classified Instances: 80.04
Weighted Precision: 0.8270990948008438
Weighted AreaUnderROC: 0.9447077345506665
Root mean squared error: 0.3321275559335758
Relative absolute error: 31.51648270571155
Root relative squared error: 70.45489410586357
Weighted TruePositiveRate: 0.8004
Weighted MatthewsCorrelation: 0.7161260878363852
Weighted FMeasure: 0.7915661285008164
Iteration time: 73.0
Weighted AreaUnderPRC: 0.9018914708696324
Mean absolute error: 0.14007325646982977
Coverage of cases: 90.2
Instances selection time: 62.0
Test time: 131.0
Accumulative iteration time: 3109.0
Weighted Recall: 0.8004
Weighted FalsePositiveRate: 0.09887944196575384
Kappa statistic: 0.7010435079351081
Training time: 11.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 42.13333333333373
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8271980411154763
Weighted AreaUnderROC: 0.945449579530071
Root mean squared error: 0.3308742926983355
Relative absolute error: 31.32516845552292
Root relative squared error: 70.18903682618851
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7173080911072403
Weighted FMeasure: 0.7929256147606849
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9029258155863865
Mean absolute error: 0.13922297091343586
Coverage of cases: 90.24
Instances selection time: 59.0
Test time: 133.0
Accumulative iteration time: 3180.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09829312236235074
Kappa statistic: 0.7028354293021521
Training time: 12.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 42.20000000000037
Incorrectly Classified Instances: 19.72
Correctly Classified Instances: 80.28
Weighted Precision: 0.8288352682335881
Weighted AreaUnderROC: 0.9461799754403643
Root mean squared error: 0.330201854106992
Relative absolute error: 31.27950803617982
Root relative squared error: 70.04639105982736
Weighted TruePositiveRate: 0.8028
Weighted MatthewsCorrelation: 0.7193842176109587
Weighted FMeasure: 0.794060751732751
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9039766043382409
Mean absolute error: 0.1390200357163554
Coverage of cases: 90.48
Instances selection time: 59.0
Test time: 128.0
Accumulative iteration time: 3251.0
Weighted Recall: 0.8028
Weighted FalsePositiveRate: 0.09769307017854523
Kappa statistic: 0.7046341405548469
Training time: 12.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 42.0800000000004
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8293178095723608
Weighted AreaUnderROC: 0.947355347864331
Root mean squared error: 0.32992127712886454
Relative absolute error: 31.187729574095417
Root relative squared error: 69.98687169466373
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7199718480036353
Weighted FMeasure: 0.7945474210231285
Iteration time: 70.0
Weighted AreaUnderPRC: 0.90576764921603
Mean absolute error: 0.13861213144042472
Coverage of cases: 90.4
Instances selection time: 58.0
Test time: 141.0
Accumulative iteration time: 3321.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09748847525714256
Kappa statistic: 0.7052332599452021
Training time: 12.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.946666666667106
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8297535836224499
Weighted AreaUnderROC: 0.9468904628674639
Root mean squared error: 0.33103960106202035
Relative absolute error: 31.303101349913543
Root relative squared error: 70.22410402567304
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7193040868719148
Weighted FMeasure: 0.7934920669960236
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9049700005994213
Mean absolute error: 0.1391248948885053
Coverage of cases: 90.32
Instances selection time: 58.0
Test time: 130.0
Accumulative iteration time: 3391.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09786333364894176
Kappa statistic: 0.7040444542004599
Training time: 12.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.93333333333377
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8300122657047606
Weighted AreaUnderROC: 0.9478144222450458
Root mean squared error: 0.3308077019898374
Relative absolute error: 31.239831971025623
Root relative squared error: 70.1749108037256
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7194070120763147
Weighted FMeasure: 0.7934901930960672
Iteration time: 74.0
Weighted AreaUnderPRC: 0.9062335625365263
Mean absolute error: 0.13884369764900342
Coverage of cases: 90.2
Instances selection time: 62.0
Test time: 129.0
Accumulative iteration time: 3465.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09785646735874053
Kappa statistic: 0.7040458017279413
Training time: 12.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 42.01333333333378
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8298354590541901
Weighted AreaUnderROC: 0.9489847263349693
Root mean squared error: 0.33021177184600353
Relative absolute error: 31.113003825624613
Root relative squared error: 70.0484949299801
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7188856202859746
Weighted FMeasure: 0.7928687492381565
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9078074531959228
Mean absolute error: 0.1382800170027767
Coverage of cases: 90.44
Instances selection time: 56.0
Test time: 127.0
Accumulative iteration time: 3533.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09804732969974075
Kappa statistic: 0.703450754852624
Training time: 12.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 41.98666666666713
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8292801623651791
Weighted AreaUnderROC: 0.9492748574522016
Root mean squared error: 0.3308110426499817
Relative absolute error: 31.168500982145527
Root relative squared error: 70.1756194647581
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7181779569648538
Weighted FMeasure: 0.7917872697553621
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9080147642999468
Mean absolute error: 0.13852667103175856
Coverage of cases: 90.4
Instances selection time: 59.0
Test time: 125.0
Accumulative iteration time: 3604.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09824505833094217
Kappa statistic: 0.702859782886353
Training time: 12.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 42.04000000000047
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8300016728784729
Weighted AreaUnderROC: 0.9500268425689754
Root mean squared error: 0.33043318236006336
Relative absolute error: 31.144654491899182
Root relative squared error: 70.0954631927554
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7176874858767671
Weighted FMeasure: 0.7909234880421211
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9090675488584667
Mean absolute error: 0.13842068663066368
Coverage of cases: 90.6
Instances selection time: 54.0
Test time: 125.0
Accumulative iteration time: 3670.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09861991672274137
Kappa statistic: 0.7016684283006195
Training time: 12.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 41.96000000000049
Incorrectly Classified Instances: 19.92
Correctly Classified Instances: 80.08
Weighted Precision: 0.8306328627832634
Weighted AreaUnderROC: 0.9491457645469585
Root mean squared error: 0.3316724663846967
Relative absolute error: 31.258921936595684
Root relative squared error: 70.35835503404572
Weighted TruePositiveRate: 0.8008
Weighted MatthewsCorrelation: 0.7178781819732016
Weighted FMeasure: 0.790777023445224
Iteration time: 65.0
Weighted AreaUnderPRC: 0.9075175266945702
Mean absolute error: 0.13892854194042592
Coverage of cases: 90.32
Instances selection time: 52.0
Test time: 126.0
Accumulative iteration time: 3735.0
Weighted Recall: 0.8008
Weighted FalsePositiveRate: 0.09860618414233892
Kappa statistic: 0.7016738612388081
Training time: 13.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 42.01333333333382
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8308339114280799
Weighted AreaUnderROC: 0.9495111523730982
Root mean squared error: 0.33090814449681155
Relative absolute error: 31.161635449311085
Root relative squared error: 70.19621787706585
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7192610445344157
Weighted FMeasure: 0.792167268757727
Iteration time: 65.0
Weighted AreaUnderPRC: 0.9080753899572046
Mean absolute error: 0.13849615755249436
Coverage of cases: 90.56
Instances selection time: 52.0
Test time: 128.0
Accumulative iteration time: 3800.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09802673082913707
Kappa statistic: 0.7034642563602425
Training time: 13.0
		
Time end:Wed Nov 01 16.40.46 EET 2017