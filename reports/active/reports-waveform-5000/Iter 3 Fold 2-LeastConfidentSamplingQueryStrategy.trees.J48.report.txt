Wed Nov 01 16.36.15 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 16.36.15 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 35.92000000000089
Incorrectly Classified Instances: 28.52
Correctly Classified Instances: 71.48
Weighted Precision: 0.7149546071945311
Weighted AreaUnderROC: 0.7929306859568465
Root mean squared error: 0.42707067516036873
Relative absolute error: 44.276490948010256
Root relative squared error: 90.59537113554397
Weighted TruePositiveRate: 0.7148
Weighted MatthewsCorrelation: 0.5724490346029576
Weighted FMeasure: 0.713793088214418
Iteration time: 83.0
Weighted AreaUnderPRC: 0.6144811095187168
Mean absolute error: 0.19678440421337984
Coverage of cases: 74.04
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 83.0
Weighted Recall: 0.7148
Weighted FalsePositiveRate: 0.14256662708310597
Kappa statistic: 0.5723196136504541
Training time: 81.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 35.62666666666763
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.7011133922238967
Weighted AreaUnderROC: 0.7806482696587309
Root mean squared error: 0.4364950403980781
Relative absolute error: 45.426513744481326
Root relative squared error: 92.5945809059329
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5520023318215335
Weighted FMeasure: 0.7012526786945881
Iteration time: 93.0
Weighted AreaUnderPRC: 0.5956476480565877
Mean absolute error: 0.20189561664214017
Coverage of cases: 73.48
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 176.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.14938005610120478
Kappa statistic: 0.5524312940039233
Training time: 90.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 36.29333333333429
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7171430683401372
Weighted AreaUnderROC: 0.7944828110736637
Root mean squared error: 0.4271928807307738
Relative absolute error: 43.326373688458595
Root relative squared error: 90.62129485180364
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5755499073444149
Weighted FMeasure: 0.7171596788384377
Iteration time: 95.0
Weighted AreaUnderPRC: 0.6169078687545885
Mean absolute error: 0.19256166083759466
Coverage of cases: 74.36
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 271.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14162744174314149
Kappa statistic: 0.5757793659083577
Training time: 92.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 37.200000000000905
Incorrectly Classified Instances: 30.04
Correctly Classified Instances: 69.96
Weighted Precision: 0.6994520732099718
Weighted AreaUnderROC: 0.7892181524475435
Root mean squared error: 0.4390985318813756
Relative absolute error: 45.791731114281006
Root relative squared error: 93.14686485071321
Weighted TruePositiveRate: 0.6996
Weighted MatthewsCorrelation: 0.5491604231494371
Weighted FMeasure: 0.6994045056976947
Iteration time: 99.0
Weighted AreaUnderPRC: 0.6072371498321978
Mean absolute error: 0.203518804952361
Coverage of cases: 74.8
Instances selection time: 2.0
Test time: 13.0
Accumulative iteration time: 370.0
Weighted Recall: 0.6996
Weighted FalsePositiveRate: 0.15041676328862053
Kappa statistic: 0.5493760268046259
Training time: 97.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 36.52000000000104
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.7170540167988105
Weighted AreaUnderROC: 0.7858470533134909
Root mean squared error: 0.4287030661599551
Relative absolute error: 43.79920297435141
Root relative squared error: 90.9416535591506
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.575576496294182
Weighted FMeasure: 0.717086280078046
Iteration time: 267.0
Weighted AreaUnderPRC: 0.6037812695724302
Mean absolute error: 0.1946631243304516
Coverage of cases: 74.4
Instances selection time: 6.0
Test time: 17.0
Accumulative iteration time: 637.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14154504626072675
Kappa statistic: 0.575804513439977
Training time: 261.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 35.880000000001004
Incorrectly Classified Instances: 28.76
Correctly Classified Instances: 71.24
Weighted Precision: 0.7134181235007878
Weighted AreaUnderROC: 0.7813235369200423
Root mean squared error: 0.43188266802397957
Relative absolute error: 44.715329373353185
Root relative squared error: 91.61614897100812
Weighted TruePositiveRate: 0.7124
Weighted MatthewsCorrelation: 0.5687467240026707
Weighted FMeasure: 0.7126528060433004
Iteration time: 107.0
Weighted AreaUnderPRC: 0.5983110106969062
Mean absolute error: 0.19873479721490397
Coverage of cases: 72.8
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 744.0
Weighted Recall: 0.7124
Weighted FalsePositiveRate: 0.144130644831382
Kappa statistic: 0.5685593410322989
Training time: 104.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 38.666666666667574
Incorrectly Classified Instances: 28.16
Correctly Classified Instances: 71.84
Weighted Precision: 0.7180106063113659
Weighted AreaUnderROC: 0.7869612715213025
Root mean squared error: 0.4250530012712701
Relative absolute error: 43.93016353821611
Root relative squared error: 90.16735786878257
Weighted TruePositiveRate: 0.7184
Weighted MatthewsCorrelation: 0.5773778471436465
Weighted FMeasure: 0.7180604078070725
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6121773837328229
Mean absolute error: 0.1952451712809614
Coverage of cases: 75.2
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 852.0
Weighted Recall: 0.7184
Weighted FalsePositiveRate: 0.14086946488470775
Kappa statistic: 0.5776160505900776
Training time: 105.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 38.21333333333441
Incorrectly Classified Instances: 30.28
Correctly Classified Instances: 69.72
Weighted Precision: 0.6971703347996718
Weighted AreaUnderROC: 0.8033115110825538
Root mean squared error: 0.4376807437822675
Relative absolute error: 45.92709673659661
Root relative squared error: 92.84610657696373
Weighted TruePositiveRate: 0.6972
Weighted MatthewsCorrelation: 0.5456115304408314
Weighted FMeasure: 0.6971825553541925
Iteration time: 116.0
Weighted AreaUnderPRC: 0.6308756958640513
Mean absolute error: 0.20412042994043034
Coverage of cases: 76.0
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 968.0
Weighted Recall: 0.6972
Weighted FalsePositiveRate: 0.15156880362482303
Kappa statistic: 0.5457820493065887
Training time: 113.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 38.0933333333344
Incorrectly Classified Instances: 30.12
Correctly Classified Instances: 69.88
Weighted Precision: 0.6988613083827914
Weighted AreaUnderROC: 0.8020023541256647
Root mean squared error: 0.4371134145522219
Relative absolute error: 45.737096736596605
Root relative squared error: 92.72575787324456
Weighted TruePositiveRate: 0.6988
Weighted MatthewsCorrelation: 0.5480460678632866
Weighted FMeasure: 0.6988279024746865
Iteration time: 119.0
Weighted AreaUnderPRC: 0.631616397349984
Mean absolute error: 0.20327598549598588
Coverage of cases: 75.56
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1087.0
Weighted Recall: 0.6988
Weighted FalsePositiveRate: 0.15077788910001727
Kappa statistic: 0.5481739025246098
Training time: 116.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 39.72000000000082
Incorrectly Classified Instances: 29.84
Correctly Classified Instances: 70.16
Weighted Precision: 0.701677197808374
Weighted AreaUnderROC: 0.8150238639722862
Root mean squared error: 0.4321848628714798
Relative absolute error: 45.88297887864821
Root relative squared error: 91.68025417878023
Weighted TruePositiveRate: 0.7016
Weighted MatthewsCorrelation: 0.5523166260175724
Weighted FMeasure: 0.701292682487245
Iteration time: 315.0
Weighted AreaUnderPRC: 0.6467304292490653
Mean absolute error: 0.20392435057177077
Coverage of cases: 77.24
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 1402.0
Weighted Recall: 0.7016
Weighted FalsePositiveRate: 0.14943498642281458
Kappa statistic: 0.5523516897943626
Training time: 309.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 37.386666666667544
Incorrectly Classified Instances: 29.56
Correctly Classified Instances: 70.44
Weighted Precision: 0.7042126693983242
Weighted AreaUnderROC: 0.8025075908645093
Root mean squared error: 0.4341974852866532
Relative absolute error: 45.11756824757375
Root relative squared error: 92.10719586610138
Weighted TruePositiveRate: 0.7044
Weighted MatthewsCorrelation: 0.5564759231680965
Weighted FMeasure: 0.7038454985344704
Iteration time: 126.0
Weighted AreaUnderPRC: 0.6296597845184985
Mean absolute error: 0.20052252554477318
Coverage of cases: 75.16
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1528.0
Weighted Recall: 0.7044
Weighted FalsePositiveRate: 0.14798908939259353
Kappa statistic: 0.5565723655898237
Training time: 123.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 37.12000000000092
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7039343306831565
Weighted AreaUnderROC: 0.7914843923441587
Root mean squared error: 0.4340992615693039
Relative absolute error: 44.83405275212345
Root relative squared error: 92.08635946911807
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5554053636015195
Weighted FMeasure: 0.7035335014416585
Iteration time: 135.0
Weighted AreaUnderPRC: 0.6152640117068929
Mean absolute error: 0.19926245667610515
Coverage of cases: 75.52
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1663.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14842574439620373
Kappa statistic: 0.5553398463744176
Training time: 132.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 36.86666666666757
Incorrectly Classified Instances: 29.64
Correctly Classified Instances: 70.36
Weighted Precision: 0.7051684550586411
Weighted AreaUnderROC: 0.8009619956619501
Root mean squared error: 0.43538336482924184
Relative absolute error: 44.85788696264971
Root relative squared error: 92.35875890597184
Weighted TruePositiveRate: 0.7036
Weighted MatthewsCorrelation: 0.5563220612477994
Weighted FMeasure: 0.7026453804432616
Iteration time: 146.0
Weighted AreaUnderPRC: 0.6272205586673295
Mean absolute error: 0.1993683865006663
Coverage of cases: 75.04
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 1809.0
Weighted Recall: 0.7036
Weighted FalsePositiveRate: 0.14837768036479515
Kappa statistic: 0.5553540413937185
Training time: 144.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 37.2666666666675
Incorrectly Classified Instances: 28.24
Correctly Classified Instances: 71.76
Weighted Precision: 0.7181296728093179
Weighted AreaUnderROC: 0.8053647452136538
Root mean squared error: 0.4263104859246808
Relative absolute error: 42.99234410550685
Root relative squared error: 90.43411064648198
Weighted TruePositiveRate: 0.7176
Weighted MatthewsCorrelation: 0.5765265415049738
Weighted FMeasure: 0.7173073300729675
Iteration time: 141.0
Weighted AreaUnderPRC: 0.6343046854509299
Mean absolute error: 0.19107708491336467
Coverage of cases: 76.44
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1950.0
Weighted Recall: 0.7176
Weighted FalsePositiveRate: 0.14145717827274493
Kappa statistic: 0.5763562121780907
Training time: 138.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 35.786666666667486
Incorrectly Classified Instances: 28.56
Correctly Classified Instances: 71.44
Weighted Precision: 0.7146262044014207
Weighted AreaUnderROC: 0.7922163024521329
Root mean squared error: 0.42899384255896783
Relative absolute error: 43.05384188719604
Root relative squared error: 91.00333654821587
Weighted TruePositiveRate: 0.7144
Weighted MatthewsCorrelation: 0.5718182180822395
Weighted FMeasure: 0.7135280995527328
Iteration time: 143.0
Weighted AreaUnderPRC: 0.620686833992507
Mean absolute error: 0.1913504083875389
Coverage of cases: 74.8
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2093.0
Weighted Recall: 0.7144
Weighted FalsePositiveRate: 0.14293601296933803
Kappa statistic: 0.5716006511670103
Training time: 141.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 36.306666666667574
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7174549684902373
Weighted AreaUnderROC: 0.8034830464790026
Root mean squared error: 0.4272419009065997
Relative absolute error: 42.83168939393953
Root relative squared error: 90.63169360142606
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.5749919334460929
Weighted FMeasure: 0.7146839713245374
Iteration time: 140.0
Weighted AreaUnderPRC: 0.6354776524220143
Mean absolute error: 0.19036306397306546
Coverage of cases: 75.28
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2233.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.1421450984445323
Kappa statistic: 0.5740006475190157
Training time: 137.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 36.426666666667536
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7165695696068054
Weighted AreaUnderROC: 0.7974213357319573
Root mean squared error: 0.42907844904036524
Relative absolute error: 43.27564234449774
Root relative squared error: 91.0212842932344
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.5743753313467022
Weighted FMeasure: 0.7151405361810604
Iteration time: 146.0
Weighted AreaUnderPRC: 0.623162747633558
Mean absolute error: 0.19233618819776865
Coverage of cases: 74.68
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2379.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.14216569731513595
Kappa statistic: 0.57398899187555
Training time: 144.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 36.6266666666676
Incorrectly Classified Instances: 28.12
Correctly Classified Instances: 71.88
Weighted Precision: 0.7200738657258091
Weighted AreaUnderROC: 0.8089470499164843
Root mean squared error: 0.42668768647062816
Relative absolute error: 42.580619359854616
Root relative squared error: 90.51412696565399
Weighted TruePositiveRate: 0.7188
Weighted MatthewsCorrelation: 0.579227444408218
Weighted FMeasure: 0.7171959093763349
Iteration time: 150.0
Weighted AreaUnderPRC: 0.6412191541734114
Mean absolute error: 0.18924719715491028
Coverage of cases: 76.04
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 2529.0
Weighted Recall: 0.7188
Weighted FalsePositiveRate: 0.14069920141431122
Kappa statistic: 0.5782083346033082
Training time: 148.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 36.34666666666757
Incorrectly Classified Instances: 28.4
Correctly Classified Instances: 71.6
Weighted Precision: 0.7174209807351024
Weighted AreaUnderROC: 0.8111828904335231
Root mean squared error: 0.4287211867528922
Relative absolute error: 42.88732901294664
Root relative squared error: 90.94549751739409
Weighted TruePositiveRate: 0.716
Weighted MatthewsCorrelation: 0.5751805188090477
Weighted FMeasure: 0.7144313279410472
Iteration time: 154.0
Weighted AreaUnderPRC: 0.6433975673554614
Mean absolute error: 0.19061035116865263
Coverage of cases: 76.04
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 2683.0
Weighted Recall: 0.716
Weighted FalsePositiveRate: 0.1419803074797029
Kappa statistic: 0.5740763936580334
Training time: 151.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 36.586666666667575
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7289624461683369
Weighted AreaUnderROC: 0.8154544858919419
Root mean squared error: 0.4199590625431274
Relative absolute error: 41.62551038549561
Root relative squared error: 89.08677028349705
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.591380941801138
Weighted FMeasure: 0.7260157197083318
Iteration time: 155.0
Weighted AreaUnderPRC: 0.6479644396078884
Mean absolute error: 0.18500226837998135
Coverage of cases: 76.56
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 2838.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13680642540209356
Kappa statistic: 0.5901333392863015
Training time: 153.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 37.3066666666675
Incorrectly Classified Instances: 27.36
Correctly Classified Instances: 72.64
Weighted Precision: 0.7289967084042621
Weighted AreaUnderROC: 0.8325803633496222
Root mean squared error: 0.4127756042144943
Relative absolute error: 41.55841580539317
Root relative squared error: 87.5629286545328
Weighted TruePositiveRate: 0.7264
Weighted MatthewsCorrelation: 0.591055449020555
Weighted FMeasure: 0.7253371235190565
Iteration time: 158.0
Weighted AreaUnderPRC: 0.6718404507991865
Mean absolute error: 0.18470407024619273
Coverage of cases: 78.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 2996.0
Weighted Recall: 0.7264
Weighted FalsePositiveRate: 0.13703848548430111
Kappa statistic: 0.5895163926988649
Training time: 156.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 36.24000000000091
Incorrectly Classified Instances: 27.32
Correctly Classified Instances: 72.68
Weighted Precision: 0.7294181439787913
Weighted AreaUnderROC: 0.818136565669561
Root mean squared error: 0.41915496689848286
Relative absolute error: 41.71320377358245
Root relative squared error: 88.91619583858181
Weighted TruePositiveRate: 0.7268
Weighted MatthewsCorrelation: 0.5916783198939609
Weighted FMeasure: 0.7259133682639766
Iteration time: 162.0
Weighted AreaUnderPRC: 0.6521117293430796
Mean absolute error: 0.18539201677147843
Coverage of cases: 76.56
Instances selection time: 6.0
Test time: 14.0
Accumulative iteration time: 3158.0
Weighted Recall: 0.7268
Weighted FalsePositiveRate: 0.13675836137068498
Kappa statistic: 0.5901595077593667
Training time: 156.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 36.7200000000008
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7310034737407939
Weighted AreaUnderROC: 0.8281216153706604
Root mean squared error: 0.41522770929769526
Relative absolute error: 41.37782445008003
Root relative squared error: 88.08309869428683
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.5936978793439229
Weighted FMeasure: 0.7269849585299593
Iteration time: 165.0
Weighted AreaUnderPRC: 0.6661893327600397
Mean absolute error: 0.18390144200035652
Coverage of cases: 77.88
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3323.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.13617890805748314
Kappa statistic: 0.5919466592672994
Training time: 163.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 36.68000000000072
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7316764047332838
Weighted AreaUnderROC: 0.8207351129071078
Root mean squared error: 0.4166674057309078
Relative absolute error: 41.343720237130434
Root relative squared error: 88.38850442751924
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5948787626194595
Weighted FMeasure: 0.7278814286710655
Iteration time: 175.0
Weighted AreaUnderPRC: 0.657617140802666
Mean absolute error: 0.18374986772058058
Coverage of cases: 76.72
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3498.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13569418902246436
Kappa statistic: 0.5931783570885971
Training time: 173.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 36.066666666667544
Incorrectly Classified Instances: 26.28
Correctly Classified Instances: 73.72
Weighted Precision: 0.7407937877293918
Weighted AreaUnderROC: 0.8222314521625561
Root mean squared error: 0.4100958212366526
Relative absolute error: 40.02094157768292
Root relative squared error: 86.99446083981077
Weighted TruePositiveRate: 0.7372
Weighted MatthewsCorrelation: 0.6078225169080465
Weighted FMeasure: 0.7365213700415089
Iteration time: 170.0
Weighted AreaUnderPRC: 0.6600699147123771
Mean absolute error: 0.17787085145636936
Coverage of cases: 77.16
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 3668.0
Weighted Recall: 0.7372
Weighted FalsePositiveRate: 0.13145949228481948
Kappa statistic: 0.605771836339988
Training time: 168.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 36.53333333333431
Incorrectly Classified Instances: 26.56
Correctly Classified Instances: 73.44
Weighted Precision: 0.7372889498279385
Weighted AreaUnderROC: 0.8111113268491462
Root mean squared error: 0.41329496812319433
Relative absolute error: 40.79895723253313
Root relative squared error: 87.67310237705641
Weighted TruePositiveRate: 0.7344
Weighted MatthewsCorrelation: 0.6032347199409528
Weighted FMeasure: 0.7337719690987597
Iteration time: 171.0
Weighted AreaUnderPRC: 0.6427019265418471
Mean absolute error: 0.1813286988112592
Coverage of cases: 77.04
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 3839.0
Weighted Recall: 0.7344
Weighted FalsePositiveRate: 0.13286419157383322
Kappa statistic: 0.6015733532258638
Training time: 169.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 38.18666666666766
Incorrectly Classified Instances: 26.8
Correctly Classified Instances: 73.2
Weighted Precision: 0.7346075315610311
Weighted AreaUnderROC: 0.8280345231289179
Root mean squared error: 0.40553889788138536
Relative absolute error: 41.65379075100287
Root relative squared error: 86.02779141805371
Weighted TruePositiveRate: 0.732
Weighted MatthewsCorrelation: 0.5997193760120751
Weighted FMeasure: 0.7308508859771675
Iteration time: 174.0
Weighted AreaUnderPRC: 0.6634161768281134
Mean absolute error: 0.185127958893347
Coverage of cases: 79.12
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4013.0
Weighted Recall: 0.732
Weighted FalsePositiveRate: 0.13395443529822468
Kappa statistic: 0.5980665923269832
Training time: 172.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 38.30666666666775
Incorrectly Classified Instances: 27.12
Correctly Classified Instances: 72.88
Weighted Precision: 0.7293639644568645
Weighted AreaUnderROC: 0.8101127971748469
Root mean squared error: 0.4128910455845756
Relative absolute error: 41.4915808011676
Root relative squared error: 87.587417467217
Weighted TruePositiveRate: 0.7288
Weighted MatthewsCorrelation: 0.5936354624132322
Weighted FMeasure: 0.7280715394153714
Iteration time: 191.0
Weighted AreaUnderPRC: 0.6419850441073662
Mean absolute error: 0.18440702578296797
Coverage of cases: 78.52
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4204.0
Weighted Recall: 0.7288
Weighted FalsePositiveRate: 0.13559119466944594
Kappa statistic: 0.5932636786384724
Training time: 189.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 36.65333333333436
Incorrectly Classified Instances: 26.44
Correctly Classified Instances: 73.56
Weighted Precision: 0.736874705670626
Weighted AreaUnderROC: 0.8003763723726502
Root mean squared error: 0.41222675345028986
Relative absolute error: 41.0763348418535
Root relative squared error: 87.4464998253643
Weighted TruePositiveRate: 0.7356
Weighted MatthewsCorrelation: 0.6041267955697216
Weighted FMeasure: 0.735006280211403
Iteration time: 188.0
Weighted AreaUnderPRC: 0.6318439217013124
Mean absolute error: 0.18256148818601642
Coverage of cases: 77.16
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4392.0
Weighted Recall: 0.7356
Weighted FalsePositiveRate: 0.13230533713123505
Kappa statistic: 0.6033969858170922
Training time: 186.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 35.96000000000106
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.7377855748921607
Weighted AreaUnderROC: 0.8062362947040665
Root mean squared error: 0.4119685446170237
Relative absolute error: 40.040954646802476
Root relative squared error: 87.39172546027487
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6064021706123017
Weighted FMeasure: 0.7374067355139455
Iteration time: 193.0
Weighted AreaUnderPRC: 0.6409266792582302
Mean absolute error: 0.17795979843023407
Coverage of cases: 76.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4585.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13137162429683769
Kappa statistic: 0.6063629036400391
Training time: 191.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 37.42666666666751
Incorrectly Classified Instances: 25.36
Correctly Classified Instances: 74.64
Weighted Precision: 0.7468773314742818
Weighted AreaUnderROC: 0.8189760902042612
Root mean squared error: 0.4041345939261459
Relative absolute error: 38.987866379234745
Root relative squared error: 85.72989356317464
Weighted TruePositiveRate: 0.7464
Weighted MatthewsCorrelation: 0.6196814193485082
Weighted FMeasure: 0.7463825237227875
Iteration time: 195.0
Weighted AreaUnderPRC: 0.6606355081312287
Mean absolute error: 0.17327940612993303
Coverage of cases: 78.84
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4780.0
Weighted Recall: 0.7464
Weighted FalsePositiveRate: 0.12702846070060733
Kappa statistic: 0.6195398568605723
Training time: 193.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 37.34666666666757
Incorrectly Classified Instances: 26.96
Correctly Classified Instances: 73.04
Weighted Precision: 0.7304623819781387
Weighted AreaUnderROC: 0.8109337296026008
Root mean squared error: 0.4146973219442942
Relative absolute error: 40.90632034632016
Root relative squared error: 87.97058654601318
Weighted TruePositiveRate: 0.7304
Weighted MatthewsCorrelation: 0.595431363733001
Weighted FMeasure: 0.7304001478422452
Iteration time: 206.0
Weighted AreaUnderPRC: 0.648643044738556
Mean absolute error: 0.18180586820586822
Coverage of cases: 78.2
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 4986.0
Weighted Recall: 0.7304
Weighted FalsePositiveRate: 0.13500626885067699
Kappa statistic: 0.5955729519190244
Training time: 204.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 37.30666666666752
Incorrectly Classified Instances: 26.24
Correctly Classified Instances: 73.76
Weighted Precision: 0.738846678634
Weighted AreaUnderROC: 0.8203921711281893
Root mean squared error: 0.41149645197694107
Relative absolute error: 40.07900631800613
Root relative squared error: 87.29157948812966
Weighted TruePositiveRate: 0.7376
Weighted MatthewsCorrelation: 0.6068050983767435
Weighted FMeasure: 0.7376828073414392
Iteration time: 210.0
Weighted AreaUnderPRC: 0.6611042248385783
Mean absolute error: 0.17812891696891697
Coverage of cases: 78.36
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 5196.0
Weighted Recall: 0.7376
Weighted FalsePositiveRate: 0.13152954897146588
Kappa statistic: 0.606298267472315
Training time: 208.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 36.08000000000095
Incorrectly Classified Instances: 27.2
Correctly Classified Instances: 72.8
Weighted Precision: 0.7302207882409182
Weighted AreaUnderROC: 0.8021402058875187
Root mean squared error: 0.42094575406828133
Relative absolute error: 41.3009956709956
Root relative squared error: 89.29607916400973
Weighted TruePositiveRate: 0.728
Weighted MatthewsCorrelation: 0.5931676246454647
Weighted FMeasure: 0.7273419634693893
Iteration time: 211.0
Weighted AreaUnderPRC: 0.634354348069488
Mean absolute error: 0.18355998075998128
Coverage of cases: 76.0
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 5407.0
Weighted Recall: 0.728
Weighted FalsePositiveRate: 0.13623383837909295
Kappa statistic: 0.5919112978396984
Training time: 209.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 37.10666666666737
Incorrectly Classified Instances: 27.48
Correctly Classified Instances: 72.52
Weighted Precision: 0.726813094525851
Weighted AreaUnderROC: 0.8028438437926206
Root mean squared error: 0.4199958101145934
Relative absolute error: 41.88940263559523
Root relative squared error: 89.09456562058976
Weighted TruePositiveRate: 0.7252
Weighted MatthewsCorrelation: 0.5886368637386381
Weighted FMeasure: 0.7246366590690292
Iteration time: 227.0
Weighted AreaUnderPRC: 0.6360058471317117
Mean absolute error: 0.18617512282486856
Coverage of cases: 76.64
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5634.0
Weighted Recall: 0.7252
Weighted FalsePositiveRate: 0.1376042062171005
Kappa statistic: 0.5877367093396177
Training time: 225.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 35.92000000000089
Incorrectly Classified Instances: 28.28
Correctly Classified Instances: 71.72
Weighted Precision: 0.718244834348249
Weighted AreaUnderROC: 0.8005489417601266
Root mean squared error: 0.426693928792736
Relative absolute error: 43.24445371333406
Root relative squared error: 90.51545116214183
Weighted TruePositiveRate: 0.7172
Weighted MatthewsCorrelation: 0.5764133565269828
Weighted FMeasure: 0.7165320279034645
Iteration time: 233.0
Weighted AreaUnderPRC: 0.6338017290120138
Mean absolute error: 0.19219757205926338
Coverage of cases: 74.92
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 5867.0
Weighted Recall: 0.7172
Weighted FalsePositiveRate: 0.14154504626072675
Kappa statistic: 0.5757522806165237
Training time: 231.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 36.04000000000086
Incorrectly Classified Instances: 28.2
Correctly Classified Instances: 71.8
Weighted Precision: 0.720234800596911
Weighted AreaUnderROC: 0.7776686123805717
Root mean squared error: 0.42830087901658187
Relative absolute error: 43.31797781385257
Root relative squared error: 90.85633678223503
Weighted TruePositiveRate: 0.718
Weighted MatthewsCorrelation: 0.5782381757926053
Weighted FMeasure: 0.717060820097677
Iteration time: 235.0
Weighted AreaUnderPRC: 0.604620080076076
Mean absolute error: 0.19252434583934566
Coverage of cases: 74.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6102.0
Weighted Recall: 0.718
Weighted FalsePositiveRate: 0.1412594496415435
Kappa statistic: 0.5768810189425266
Training time: 233.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 37.306666666667326
Incorrectly Classified Instances: 28.64
Correctly Classified Instances: 71.36
Weighted Precision: 0.714857473039001
Weighted AreaUnderROC: 0.7865587566521705
Root mean squared error: 0.4293274859358289
Relative absolute error: 44.07914781599611
Root relative squared error: 91.07411299649881
Weighted TruePositiveRate: 0.7136
Weighted MatthewsCorrelation: 0.5710794701133742
Weighted FMeasure: 0.7127591518749224
Iteration time: 240.0
Weighted AreaUnderPRC: 0.616076373960052
Mean absolute error: 0.19590732362665028
Coverage of cases: 75.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6342.0
Weighted Recall: 0.7136
Weighted FalsePositiveRate: 0.14346879603576543
Kappa statistic: 0.5703085616619217
Training time: 238.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 36.680000000000774
Incorrectly Classified Instances: 26.76
Correctly Classified Instances: 73.24
Weighted Precision: 0.7345355849966024
Weighted AreaUnderROC: 0.7956657168298555
Root mean squared error: 0.41511426611480795
Relative absolute error: 41.336237783237465
Root relative squared error: 88.05903376111712
Weighted TruePositiveRate: 0.7324
Weighted MatthewsCorrelation: 0.5996706134449701
Weighted FMeasure: 0.7319709701463833
Iteration time: 243.0
Weighted AreaUnderPRC: 0.6280216047910013
Mean absolute error: 0.18371661236994516
Coverage of cases: 76.76
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 6585.0
Weighted Recall: 0.7324
Weighted FalsePositiveRate: 0.134024491984871
Kappa statistic: 0.5985182261923109
Training time: 241.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 36.52000000000088
Incorrectly Classified Instances: 26.6
Correctly Classified Instances: 73.4
Weighted Precision: 0.7363868180871987
Weighted AreaUnderROC: 0.8048163114144485
Root mean squared error: 0.4137795838226766
Relative absolute error: 40.57233600712987
Root relative squared error: 87.77590489126841
Weighted TruePositiveRate: 0.734
Weighted MatthewsCorrelation: 0.6021525311710793
Weighted FMeasure: 0.7336008643089921
Iteration time: 245.0
Weighted AreaUnderPRC: 0.6451001725087011
Mean absolute error: 0.1803214933650225
Coverage of cases: 77.12
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 6830.0
Weighted Recall: 0.734
Weighted FalsePositiveRate: 0.1332679089110714
Kappa statistic: 0.6009150747279021
Training time: 243.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 36.00000000000091
Incorrectly Classified Instances: 28.52
Correctly Classified Instances: 71.48
Weighted Precision: 0.715219755457056
Weighted AreaUnderROC: 0.7816859069755002
Root mean squared error: 0.42983298195086483
Relative absolute error: 43.75688642059698
Root relative squared error: 91.18134489452721
Weighted TruePositiveRate: 0.7148
Weighted MatthewsCorrelation: 0.5723800856622144
Weighted FMeasure: 0.7142428786020791
Iteration time: 258.0
Weighted AreaUnderPRC: 0.6098373856103662
Mean absolute error: 0.1944750507582097
Coverage of cases: 74.56
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7088.0
Weighted Recall: 0.7148
Weighted FalsePositiveRate: 0.14282067982055133
Kappa statistic: 0.5721557780212163
Training time: 256.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 35.1866666666676
Incorrectly Classified Instances: 27.72
Correctly Classified Instances: 72.28
Weighted Precision: 0.7247375372340035
Weighted AreaUnderROC: 0.7819985157248887
Root mean squared error: 0.4255190823618137
Relative absolute error: 42.362667859488276
Root relative squared error: 90.26622859869444
Weighted TruePositiveRate: 0.7228
Weighted MatthewsCorrelation: 0.5851172238606134
Weighted FMeasure: 0.7223064733758318
Iteration time: 258.0
Weighted AreaUnderPRC: 0.6123704395855512
Mean absolute error: 0.18827852381994875
Coverage of cases: 74.24
Instances selection time: 2.0
Test time: 6.0
Accumulative iteration time: 7346.0
Weighted Recall: 0.7228
Weighted FalsePositiveRate: 0.13890730493773
Kappa statistic: 0.5840963235315298
Training time: 256.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 36.56000000000092
Incorrectly Classified Instances: 26.4
Correctly Classified Instances: 73.6
Weighted Precision: 0.7374041509433963
Weighted AreaUnderROC: 0.819704813269431
Root mean squared error: 0.4096523810469545
Relative absolute error: 40.18388199575565
Root relative squared error: 86.90039297025491
Weighted TruePositiveRate: 0.736
Weighted MatthewsCorrelation: 0.6047541034142605
Weighted FMeasure: 0.7354151063193122
Iteration time: 251.0
Weighted AreaUnderPRC: 0.6595941868826108
Mean absolute error: 0.17859503109224817
Coverage of cases: 77.96
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7597.0
Weighted Recall: 0.736
Weighted FalsePositiveRate: 0.13216253882164342
Kappa statistic: 0.6039735137485995
Training time: 249.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 36.40000000000076
Incorrectly Classified Instances: 29.04
Correctly Classified Instances: 70.96
Weighted Precision: 0.7094608455680276
Weighted AreaUnderROC: 0.7906644991113576
Root mean squared error: 0.4310510747313356
Relative absolute error: 44.118953461277734
Root relative squared error: 91.43974139408277
Weighted TruePositiveRate: 0.7096
Weighted MatthewsCorrelation: 0.564330187890742
Weighted FMeasure: 0.7092721648959905
Iteration time: 253.0
Weighted AreaUnderPRC: 0.6227082448642333
Mean absolute error: 0.19608423760567972
Coverage of cases: 75.4
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 7850.0
Weighted Recall: 0.7096
Weighted FalsePositiveRate: 0.14528129138295034
Kappa statistic: 0.5643847708915904
Training time: 251.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 35.97333333333409
Incorrectly Classified Instances: 28.0
Correctly Classified Instances: 72.0
Weighted Precision: 0.7204050378053621
Weighted AreaUnderROC: 0.8007477064783809
Root mean squared error: 0.4244257482998958
Relative absolute error: 42.561521843152796
Root relative squared error: 90.03429741990912
Weighted TruePositiveRate: 0.72
Weighted MatthewsCorrelation: 0.5801627695354221
Weighted FMeasure: 0.7196488224860818
Iteration time: 254.0
Weighted AreaUnderPRC: 0.6289333114236149
Mean absolute error: 0.1891623193029022
Coverage of cases: 76.08
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 8104.0
Weighted Recall: 0.72
Weighted FalsePositiveRate: 0.14019527729332285
Kappa statistic: 0.5799355116997561
Training time: 253.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 35.520000000000834
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7275941584271303
Weighted AreaUnderROC: 0.7862675864122621
Root mean squared error: 0.421487157130356
Relative absolute error: 41.6654468905912
Root relative squared error: 89.41092809697417
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.590845057837114
Weighted FMeasure: 0.7271356001863082
Iteration time: 264.0
Weighted AreaUnderPRC: 0.6105783394376205
Mean absolute error: 0.18517976395818397
Coverage of cases: 74.64
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 8368.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.1366292956414958
Kappa statistic: 0.5907371699703338
Training time: 263.0
		
Time end:Wed Nov 01 16.36.27 EET 2017