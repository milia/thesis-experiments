Wed Nov 01 15.07.09 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 15.07.09 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 40.02666666666796
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.839884876278365
Weighted AreaUnderROC: 0.9594105937712233
Root mean squared error: 0.3311340943730951
Relative absolute error: 29.83770510413495
Root relative squared error: 70.24414908398435
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7288734231951811
Weighted FMeasure: 0.7962995820544271
Iteration time: 108.0
Weighted AreaUnderPRC: 0.9254551971390003
Mean absolute error: 0.13261202268504485
Coverage of cases: 89.52
Instances selection time: 99.0
Test time: 133.0
Accumulative iteration time: 108.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.0951665107811081
Kappa statistic: 0.7118817797631513
Training time: 9.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 40.026666666667936
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8425019714831323
Weighted AreaUnderROC: 0.9595382119034952
Root mean squared error: 0.33025439144291835
Relative absolute error: 29.745374594302977
Root relative squared error: 70.05753591177707
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7325449880642991
Weighted FMeasure: 0.7990196298962985
Iteration time: 98.0
Weighted AreaUnderPRC: 0.9255055076340848
Mean absolute error: 0.1322016648635694
Coverage of cases: 89.56
Instances selection time: 93.0
Test time: 127.0
Accumulative iteration time: 206.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09397140747429085
Kappa statistic: 0.7154720887104117
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 40.18666666666794
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8406535524228591
Weighted AreaUnderROC: 0.9599129381146173
Root mean squared error: 0.3291195290442549
Relative absolute error: 29.663321331431007
Root relative squared error: 69.8167952424345
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.7312703295357975
Weighted FMeasure: 0.7989572217812347
Iteration time: 96.0
Weighted AreaUnderPRC: 0.9261249177129434
Mean absolute error: 0.13183698369524954
Coverage of cases: 89.64
Instances selection time: 91.0
Test time: 125.0
Accumulative iteration time: 302.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.09419259278613706
Kappa statistic: 0.714864474365837
Training time: 5.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 40.2933333333346
Incorrectly Classified Instances: 18.68
Correctly Classified Instances: 81.32
Weighted Precision: 0.8412707987437399
Weighted AreaUnderROC: 0.9604373632921932
Root mean squared error: 0.32496989393198333
Relative absolute error: 29.216420315778105
Root relative squared error: 68.93652470423339
Weighted TruePositiveRate: 0.8132
Weighted MatthewsCorrelation: 0.7352600559590268
Weighted FMeasure: 0.8034247280483372
Iteration time: 95.0
Weighted AreaUnderPRC: 0.9269547781146059
Mean absolute error: 0.1298507569590144
Coverage of cases: 90.16
Instances selection time: 90.0
Test time: 124.0
Accumulative iteration time: 397.0
Weighted Recall: 0.8132
Weighted FalsePositiveRate: 0.09244156932883013
Kappa statistic: 0.7202359842421571
Training time: 5.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 40.37333333333459
Incorrectly Classified Instances: 18.48
Correctly Classified Instances: 81.52
Weighted Precision: 0.8436033855413593
Weighted AreaUnderROC: 0.9608179220130649
Root mean squared error: 0.3226093934502798
Relative absolute error: 28.98511964925934
Root relative squared error: 68.43578693495138
Weighted TruePositiveRate: 0.8152
Weighted MatthewsCorrelation: 0.7384011065915974
Weighted FMeasure: 0.8055151219565408
Iteration time: 100.0
Weighted AreaUnderPRC: 0.9275068199937576
Mean absolute error: 0.12882275399670878
Coverage of cases: 90.2
Instances selection time: 95.0
Test time: 123.0
Accumulative iteration time: 497.0
Weighted Recall: 0.8152
Weighted FalsePositiveRate: 0.09143597743495384
Kappa statistic: 0.7232319795263554
Training time: 5.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 40.56000000000123
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8459706304850528
Weighted AreaUnderROC: 0.9606637416145042
Root mean squared error: 0.32056008639513944
Relative absolute error: 28.77248706781973
Root relative squared error: 68.00106326032444
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7423567197160406
Weighted FMeasure: 0.8087761996437963
Iteration time: 94.0
Weighted AreaUnderPRC: 0.9271379285143255
Mean absolute error: 0.1278777203014216
Coverage of cases: 90.68
Instances selection time: 89.0
Test time: 127.0
Accumulative iteration time: 591.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09004072208927444
Kappa statistic: 0.7274203396426437
Training time: 5.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 40.69333333333456
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.8441365511226937
Weighted AreaUnderROC: 0.9607100043823308
Root mean squared error: 0.3202187836964832
Relative absolute error: 28.832419914179805
Root relative squared error: 67.9286620245273
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7398051004034312
Weighted FMeasure: 0.807533944105141
Iteration time: 92.0
Weighted AreaUnderPRC: 0.9271884902019363
Mean absolute error: 0.1281440885074664
Coverage of cases: 90.64
Instances selection time: 87.0
Test time: 126.0
Accumulative iteration time: 683.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.0908367171467292
Kappa statistic: 0.725020872054069
Training time: 5.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 40.89333333333454
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8450590629579919
Weighted AreaUnderROC: 0.9605538134472227
Root mean squared error: 0.31993551893388233
Relative absolute error: 28.897069633153656
Root relative squared error: 67.86857249417542
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7411144839004361
Weighted FMeasure: 0.8080689810677868
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9270316562603639
Mean absolute error: 0.12843142059179463
Coverage of cases: 90.92
Instances selection time: 86.0
Test time: 126.0
Accumulative iteration time: 774.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.09043268474575282
Kappa statistic: 0.7262234993932587
Training time: 5.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 40.74666666666789
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.840398063454137
Weighted AreaUnderROC: 0.9591173201650927
Root mean squared error: 0.32423911159842805
Relative absolute error: 29.400539249947816
Root relative squared error: 68.7815023611449
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7315446813317712
Weighted FMeasure: 0.800074543626332
Iteration time: 91.0
Weighted AreaUnderPRC: 0.9246627535501278
Mean absolute error: 0.13066906333310202
Coverage of cases: 90.6
Instances selection time: 85.0
Test time: 126.0
Accumulative iteration time: 865.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09398243691723716
Kappa statistic: 0.7154592714990589
Training time: 6.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 40.68000000000123
Incorrectly Classified Instances: 18.96
Correctly Classified Instances: 81.04
Weighted Precision: 0.8417158005606652
Weighted AreaUnderROC: 0.9597897102449815
Root mean squared error: 0.32343976317205314
Relative absolute error: 29.362887467185423
Root relative squared error: 68.61193495329877
Weighted TruePositiveRate: 0.8104
Weighted MatthewsCorrelation: 0.7325892701516102
Weighted FMeasure: 0.8001007980489052
Iteration time: 90.0
Weighted AreaUnderPRC: 0.9256143323230557
Mean absolute error: 0.13050172207638028
Coverage of cases: 90.36
Instances selection time: 84.0
Test time: 127.0
Accumulative iteration time: 955.0
Weighted Recall: 0.8104
Weighted FalsePositiveRate: 0.09377347687669899
Kappa statistic: 0.7160644277368394
Training time: 6.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 40.906666666667874
Incorrectly Classified Instances: 18.56
Correctly Classified Instances: 81.44
Weighted Precision: 0.8443113755185936
Weighted AreaUnderROC: 0.9604821076562661
Root mean squared error: 0.3206847288816314
Relative absolute error: 29.133651494596457
Root relative squared error: 68.02750392455116
Weighted TruePositiveRate: 0.8144
Weighted MatthewsCorrelation: 0.737878795697879
Weighted FMeasure: 0.8048932139538573
Iteration time: 89.0
Weighted AreaUnderPRC: 0.9266759872971412
Mean absolute error: 0.1294828955315404
Coverage of cases: 90.68
Instances selection time: 83.0
Test time: 125.0
Accumulative iteration time: 1044.0
Weighted Recall: 0.8144
Weighted FalsePositiveRate: 0.09179904666627556
Kappa statistic: 0.7220414426624213
Training time: 6.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 41.09333333333451
Incorrectly Classified Instances: 18.28
Correctly Classified Instances: 81.72
Weighted Precision: 0.8466851002626968
Weighted AreaUnderROC: 0.9606436618003572
Root mean squared error: 0.3190359901633167
Relative absolute error: 28.9749198072374
Root relative squared error: 67.67775362611361
Weighted TruePositiveRate: 0.8172
Weighted MatthewsCorrelation: 0.7418467388463865
Weighted FMeasure: 0.8080310914218595
Iteration time: 88.0
Weighted AreaUnderPRC: 0.9269636778900562
Mean absolute error: 0.12877742136550016
Coverage of cases: 91.08
Instances selection time: 81.0
Test time: 125.0
Accumulative iteration time: 1132.0
Weighted Recall: 0.8172
Weighted FalsePositiveRate: 0.0904102223541919
Kappa statistic: 0.726229141296435
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 41.120000000001184
Incorrectly Classified Instances: 18.52
Correctly Classified Instances: 81.48
Weighted Precision: 0.8440732714904144
Weighted AreaUnderROC: 0.9612377023648322
Root mean squared error: 0.3181596474539867
Relative absolute error: 28.93537482995775
Root relative squared error: 67.49185326439043
Weighted TruePositiveRate: 0.8148
Weighted MatthewsCorrelation: 0.7381647497899808
Weighted FMeasure: 0.8054651082032968
Iteration time: 87.0
Weighted AreaUnderPRC: 0.9280495019674421
Mean absolute error: 0.12860166591092392
Coverage of cases: 91.12
Instances selection time: 81.0
Test time: 125.0
Accumulative iteration time: 1219.0
Weighted Recall: 0.8148
Weighted FalsePositiveRate: 0.09160889845745124
Kappa statistic: 0.7226362377253056
Training time: 6.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 41.25333333333448
Incorrectly Classified Instances: 18.2
Correctly Classified Instances: 81.8
Weighted Precision: 0.8464486652982872
Weighted AreaUnderROC: 0.9612368127628839
Root mean squared error: 0.3181204746693786
Relative absolute error: 29.01302630725922
Root relative squared error: 67.48354346190011
Weighted TruePositiveRate: 0.818
Weighted MatthewsCorrelation: 0.7425829186367889
Weighted FMeasure: 0.8086890529765003
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9280181293037029
Mean absolute error: 0.12894678358781936
Coverage of cases: 91.32
Instances selection time: 80.0
Test time: 125.0
Accumulative iteration time: 1305.0
Weighted Recall: 0.818
Weighted FalsePositiveRate: 0.09003349873298529
Kappa statistic: 0.7274234748894116
Training time: 6.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 41.10666666666788
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8463680303030303
Weighted AreaUnderROC: 0.96093019728486
Root mean squared error: 0.31973305982061107
Relative absolute error: 29.199314196584005
Root relative squared error: 67.82562443060328
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7401136012557602
Weighted FMeasure: 0.8054207923733203
Iteration time: 87.0
Weighted AreaUnderPRC: 0.927556016924191
Mean absolute error: 0.12977472976259619
Coverage of cases: 91.2
Instances selection time: 80.0
Test time: 125.0
Accumulative iteration time: 1392.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09120034541052931
Kappa statistic: 0.7238446763483568
Training time: 7.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 41.28000000000118
Incorrectly Classified Instances: 18.36
Correctly Classified Instances: 81.64
Weighted Precision: 0.846088883055481
Weighted AreaUnderROC: 0.9610556874550783
Root mean squared error: 0.31780808321795
Relative absolute error: 29.028846186132927
Root relative squared error: 67.41727522779316
Weighted TruePositiveRate: 0.8164
Weighted MatthewsCorrelation: 0.7407385115590259
Weighted FMeasure: 0.8070680689071906
Iteration time: 86.0
Weighted AreaUnderPRC: 0.9276633974606804
Mean absolute error: 0.1290170941605914
Coverage of cases: 91.52
Instances selection time: 79.0
Test time: 126.0
Accumulative iteration time: 1478.0
Weighted Recall: 0.8164
Weighted FalsePositiveRate: 0.09081282563659145
Kappa statistic: 0.725031678267654
Training time: 7.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 41.440000000001156
Incorrectly Classified Instances: 18.48
Correctly Classified Instances: 81.52
Weighted Precision: 0.8448029068667549
Weighted AreaUnderROC: 0.9608123140073023
Root mean squared error: 0.318423518578263
Relative absolute error: 29.15140772805927
Root relative squared error: 67.54782878279096
Weighted TruePositiveRate: 0.8152
Weighted MatthewsCorrelation: 0.7390154362739908
Weighted FMeasure: 0.805229494814956
Iteration time: 85.0
Weighted AreaUnderPRC: 0.9272806298977957
Mean absolute error: 0.12956181212470846
Coverage of cases: 91.64
Instances selection time: 78.0
Test time: 125.0
Accumulative iteration time: 1563.0
Weighted Recall: 0.8152
Weighted FalsePositiveRate: 0.09143153455241329
Kappa statistic: 0.7232358260799555
Training time: 7.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 41.57333333333442
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.8441289216031604
Weighted AreaUnderROC: 0.9607953772348053
Root mean squared error: 0.31773932640783
Relative absolute error: 29.163773400451063
Root relative squared error: 67.40268970578657
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7391168703822719
Weighted FMeasure: 0.8058823818685948
Iteration time: 83.0
Weighted AreaUnderPRC: 0.9271797579016972
Mean absolute error: 0.129616770668672
Coverage of cases: 91.72
Instances selection time: 76.0
Test time: 127.0
Accumulative iteration time: 1646.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.0912478951405897
Kappa statistic: 0.7238282650328483
Training time: 7.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 41.88000000000102
Incorrectly Classified Instances: 18.44
Correctly Classified Instances: 81.56
Weighted Precision: 0.843481216368232
Weighted AreaUnderROC: 0.9598510518215954
Root mean squared error: 0.3176097474635443
Relative absolute error: 29.249355607106448
Root relative squared error: 67.37520186072555
Weighted TruePositiveRate: 0.8156
Weighted MatthewsCorrelation: 0.7388370231213439
Weighted FMeasure: 0.8063644618631437
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9257385407547816
Mean absolute error: 0.12999713603158483
Coverage of cases: 91.92
Instances selection time: 75.0
Test time: 125.0
Accumulative iteration time: 1728.0
Weighted Recall: 0.8156
Weighted FalsePositiveRate: 0.09127091656462898
Kappa statistic: 0.7238186026203803
Training time: 7.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 41.62666666666775
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8417003191158848
Weighted AreaUnderROC: 0.9589335392414982
Root mean squared error: 0.32140170246023414
Relative absolute error: 29.631005008311046
Root relative squared error: 68.17959698835963
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7331440334700031
Weighted FMeasure: 0.8007047271369682
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9243850333982843
Mean absolute error: 0.13169335559249415
Coverage of cases: 91.36
Instances selection time: 74.0
Test time: 126.0
Accumulative iteration time: 1810.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09362699454787288
Kappa statistic: 0.7166475608446945
Training time: 8.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 41.61333333333446
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.8428351470312376
Weighted AreaUnderROC: 0.959300221395179
Root mean squared error: 0.320376677821698
Relative absolute error: 29.532520537328896
Root relative squared error: 67.96215642652197
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7348799797257713
Weighted FMeasure: 0.8020221153675448
Iteration time: 82.0
Weighted AreaUnderPRC: 0.9248471947200271
Mean absolute error: 0.13125564683257349
Coverage of cases: 91.48
Instances selection time: 74.0
Test time: 128.0
Accumulative iteration time: 1892.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09302408369980117
Kappa statistic: 0.7184440475237694
Training time: 8.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 41.64000000000111
Incorrectly Classified Instances: 18.76
Correctly Classified Instances: 81.24
Weighted Precision: 0.8434408129543952
Weighted AreaUnderROC: 0.9591827760804273
Root mean squared error: 0.32085198754818794
Relative absolute error: 29.60910041774794
Root relative squared error: 68.06298484575147
Weighted TruePositiveRate: 0.8124
Weighted MatthewsCorrelation: 0.735648094058195
Weighted FMeasure: 0.8021465606936025
Iteration time: 81.0
Weighted AreaUnderPRC: 0.9247159670805113
Mean absolute error: 0.13159600185665812
Coverage of cases: 91.36
Instances selection time: 72.0
Test time: 125.0
Accumulative iteration time: 1973.0
Weighted Recall: 0.8124
Weighted FalsePositiveRate: 0.09282885581255292
Kappa statistic: 0.7190448531413302
Training time: 9.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 41.666666666667766
Incorrectly Classified Instances: 18.64
Correctly Classified Instances: 81.36
Weighted Precision: 0.8442536825668072
Weighted AreaUnderROC: 0.9588177415912512
Root mean squared error: 0.3213652203212917
Relative absolute error: 29.69180329034701
Root relative squared error: 68.17185795800812
Weighted TruePositiveRate: 0.8136
Weighted MatthewsCorrelation: 0.7372400996719385
Weighted FMeasure: 0.803735198512838
Iteration time: 80.0
Weighted AreaUnderPRC: 0.9241457002856382
Mean absolute error: 0.13196357017932067
Coverage of cases: 91.52
Instances selection time: 72.0
Test time: 125.0
Accumulative iteration time: 2053.0
Weighted Recall: 0.8136
Weighted FalsePositiveRate: 0.0922367411172124
Kappa statistic: 0.7208367899250118
Training time: 8.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 41.68000000000108
Incorrectly Classified Instances: 18.84
Correctly Classified Instances: 81.16
Weighted Precision: 0.8426717906976744
Weighted AreaUnderROC: 0.9582317993150337
Root mean squared error: 0.3223146683081877
Relative absolute error: 29.79786499804506
Root relative squared error: 68.37326629098355
Weighted TruePositiveRate: 0.8116
Weighted MatthewsCorrelation: 0.7345130998758572
Weighted FMeasure: 0.8013195364516033
Iteration time: 156.0
Weighted AreaUnderPRC: 0.9233325806393791
Mean absolute error: 0.13243495554686757
Coverage of cases: 91.16
Instances selection time: 136.0
Test time: 245.0
Accumulative iteration time: 2209.0
Weighted Recall: 0.8116
Weighted FalsePositiveRate: 0.09323510965479952
Kappa statistic: 0.7178458036302642
Training time: 20.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 41.720000000001065
Incorrectly Classified Instances: 18.96
Correctly Classified Instances: 81.04
Weighted Precision: 0.8410210021738654
Weighted AreaUnderROC: 0.9580888937288834
Root mean squared error: 0.3225809466751046
Relative absolute error: 29.84227910152409
Root relative squared error: 68.4297524626626
Weighted TruePositiveRate: 0.8104
Weighted MatthewsCorrelation: 0.7325532469758851
Weighted FMeasure: 0.8000655672060074
Iteration time: 155.0
Weighted AreaUnderPRC: 0.9232388789719084
Mean absolute error: 0.13263235156232991
Coverage of cases: 91.0
Instances selection time: 138.0
Test time: 159.0
Accumulative iteration time: 2364.0
Weighted Recall: 0.8104
Weighted FalsePositiveRate: 0.09384381474058359
Kappa statistic: 0.7160467379880582
Training time: 17.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 41.693333333334394
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8394315357389062
Weighted AreaUnderROC: 0.9570970316166858
Root mean squared error: 0.3235353779649791
Relative absolute error: 29.98122968026911
Root relative squared error: 68.63221791383667
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7298273050483444
Weighted FMeasure: 0.7981494654320462
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9218194908906837
Mean absolute error: 0.13324990969008554
Coverage of cases: 90.92
Instances selection time: 69.0
Test time: 173.0
Accumulative iteration time: 2442.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09484432695603598
Kappa statistic: 0.71304993769769
Training time: 9.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 41.56000000000113
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8395762795233775
Weighted AreaUnderROC: 0.9565131322283201
Root mean squared error: 0.32593297569012747
Relative absolute error: 30.20921141144374
Root relative squared error: 69.14082519683963
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7286876974864104
Weighted FMeasure: 0.7966097062837161
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9209563030079296
Mean absolute error: 0.13426316182863948
Coverage of cases: 90.68
Instances selection time: 68.0
Test time: 124.0
Accumulative iteration time: 2520.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09542413861666339
Kappa statistic: 0.7112603664716977
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 41.840000000001034
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8391107856613551
Weighted AreaUnderROC: 0.9564532710713199
Root mean squared error: 0.3233007419868562
Relative absolute error: 30.022073148218173
Root relative squared error: 68.58244410646437
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7305083940600513
Weighted FMeasure: 0.7993020890715635
Iteration time: 78.0
Weighted AreaUnderPRC: 0.9207506531463318
Mean absolute error: 0.13343143621430362
Coverage of cases: 90.92
Instances selection time: 69.0
Test time: 124.0
Accumulative iteration time: 2598.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09446331597909889
Kappa statistic: 0.7142399789891499
Training time: 9.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 41.84000000000105
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8398582706586909
Weighted AreaUnderROC: 0.9551920369167285
Root mean squared error: 0.3254472950482184
Relative absolute error: 30.26762805433474
Root relative squared error: 69.03779677422413
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.730894061533398
Weighted FMeasure: 0.7991428791042892
Iteration time: 76.0
Weighted AreaUnderPRC: 0.9188855010935327
Mean absolute error: 0.13452279135259948
Coverage of cases: 90.88
Instances selection time: 67.0
Test time: 123.0
Accumulative iteration time: 2674.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09445895085996338
Kappa statistic: 0.7142434027967484
Training time: 9.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 41.92000000000102
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8405490165980922
Weighted AreaUnderROC: 0.955143831884131
Root mean squared error: 0.3244541686054263
Relative absolute error: 30.196047071134338
Root relative squared error: 68.82712284154195
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7328530743246666
Weighted FMeasure: 0.8011619722023843
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9188416926192553
Mean absolute error: 0.13420465364948658
Coverage of cases: 91.0
Instances selection time: 65.0
Test time: 125.0
Accumulative iteration time: 2749.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.0936752588372217
Kappa statistic: 0.7166318077860232
Training time: 10.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 42.09333333333429
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8352343760136751
Weighted AreaUnderROC: 0.954276079271559
Root mean squared error: 0.3243336187667008
Relative absolute error: 30.310809833355147
Root relative squared error: 68.80155035901184
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7263437248258353
Weighted FMeasure: 0.7969106885328039
Iteration time: 75.0
Weighted AreaUnderPRC: 0.9174547273262664
Mean absolute error: 0.13471471037046795
Coverage of cases: 91.08
Instances selection time: 65.0
Test time: 128.0
Accumulative iteration time: 2824.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09569588742253374
Kappa statistic: 0.7106374698475855
Training time: 10.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 41.93333333333435
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.839760998300555
Weighted AreaUnderROC: 0.9541732316219792
Root mean squared error: 0.3265944187236134
Relative absolute error: 30.482247424069154
Root relative squared error: 69.28113845314357
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7309024453885107
Weighted FMeasure: 0.7991767727122159
Iteration time: 76.0
Weighted AreaUnderPRC: 0.917346676205258
Mean absolute error: 0.13547665521808577
Coverage of cases: 90.8
Instances selection time: 63.0
Test time: 126.0
Accumulative iteration time: 2900.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09446903245340618
Kappa statistic: 0.7142413485220347
Training time: 13.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 42.06666666666764
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8372452641411735
Weighted AreaUnderROC: 0.9534331202882158
Root mean squared error: 0.32545913722595227
Relative absolute error: 30.433131029793724
Root relative squared error: 69.04030887947802
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7280943198312394
Weighted FMeasure: 0.7976442729924677
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9162259134482199
Mean absolute error: 0.13525836013241718
Coverage of cases: 91.12
Instances selection time: 62.0
Test time: 127.0
Accumulative iteration time: 2972.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09528090334201608
Kappa statistic: 0.7118409064658346
Training time: 10.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 41.97333333333434
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8374367547619508
Weighted AreaUnderROC: 0.953480605790888
Root mean squared error: 0.3273882441800425
Relative absolute error: 30.5471300119857
Root relative squared error: 69.44953426213942
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7265681482605363
Weighted FMeasure: 0.7961330771738675
Iteration time: 72.0
Weighted AreaUnderPRC: 0.9165509652908082
Mean absolute error: 0.13576502227549264
Coverage of cases: 90.68
Instances selection time: 61.0
Test time: 126.0
Accumulative iteration time: 3044.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09605300688933309
Kappa statistic: 0.7094490290924794
Training time: 11.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 42.04000000000099
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8375707270193743
Weighted AreaUnderROC: 0.9538884836202378
Root mean squared error: 0.32692300261731055
Relative absolute error: 30.544420248202233
Root relative squared error: 69.35084162297015
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7270237077165649
Weighted FMeasure: 0.7960878861777412
Iteration time: 71.0
Weighted AreaUnderPRC: 0.9170895336965481
Mean absolute error: 0.13575297888089946
Coverage of cases: 90.88
Instances selection time: 60.0
Test time: 131.0
Accumulative iteration time: 3115.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09585206252777753
Kappa statistic: 0.7100529668530635
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 41.89333333333437
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8375958563988787
Weighted AreaUnderROC: 0.9542480318199698
Root mean squared error: 0.3270165178345923
Relative absolute error: 30.520967374881852
Root relative squared error: 69.37067919625537
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7270379435829875
Weighted FMeasure: 0.7962181372726925
Iteration time: 70.0
Weighted AreaUnderPRC: 0.9176644402758272
Mean absolute error: 0.13564874388836443
Coverage of cases: 90.72
Instances selection time: 60.0
Test time: 125.0
Accumulative iteration time: 3185.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09585277708706594
Kappa statistic: 0.7100514383126049
Training time: 10.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 41.866666666667705
Incorrectly Classified Instances: 19.16
Correctly Classified Instances: 80.84
Weighted Precision: 0.8395997745984665
Weighted AreaUnderROC: 0.955450053524379
Root mean squared error: 0.3257711753412331
Relative absolute error: 30.322700863459232
Root relative squared error: 69.10650215966915
Weighted TruePositiveRate: 0.8084
Weighted MatthewsCorrelation: 0.7299686992521153
Weighted FMeasure: 0.7982576694461782
Iteration time: 69.0
Weighted AreaUnderPRC: 0.9194126097236373
Mean absolute error: 0.13476755939315277
Coverage of cases: 90.8
Instances selection time: 58.0
Test time: 125.0
Accumulative iteration time: 3254.0
Weighted Recall: 0.8084
Weighted FalsePositiveRate: 0.09484647063390121
Kappa statistic: 0.7130477373544511
Training time: 11.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 41.72000000000105
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.8381368339229186
Weighted AreaUnderROC: 0.9551230379541199
Root mean squared error: 0.3270741132278241
Relative absolute error: 30.4571496349869
Root relative squared error: 69.38289702419115
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7267554616826759
Weighted FMeasure: 0.7953784850388396
Iteration time: 75.0
Weighted AreaUnderPRC: 0.918694705994871
Mean absolute error: 0.1353651094888313
Coverage of cases: 90.6
Instances selection time: 57.0
Test time: 125.0
Accumulative iteration time: 3329.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09602347666829306
Kappa statistic: 0.7094635102942171
Training time: 18.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 41.760000000001064
Incorrectly Classified Instances: 19.08
Correctly Classified Instances: 80.92
Weighted Precision: 0.8409008630977672
Weighted AreaUnderROC: 0.9560592249839152
Root mean squared error: 0.3262968364612177
Relative absolute error: 30.343958996431017
Root relative squared error: 69.21801172243332
Weighted TruePositiveRate: 0.8092
Weighted MatthewsCorrelation: 0.7313580742672595
Weighted FMeasure: 0.7987481124590963
Iteration time: 68.0
Weighted AreaUnderPRC: 0.9200796469034561
Mean absolute error: 0.13486203998413848
Coverage of cases: 90.8
Instances selection time: 57.0
Test time: 132.0
Accumulative iteration time: 3397.0
Weighted Recall: 0.8092
Weighted FalsePositiveRate: 0.09443370799465384
Kappa statistic: 0.7142520304280253
Training time: 11.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 41.680000000001115
Incorrectly Classified Instances: 19.04
Correctly Classified Instances: 80.96
Weighted Precision: 0.8414282284742083
Weighted AreaUnderROC: 0.9569928244284592
Root mean squared error: 0.32532205705946354
Relative absolute error: 30.24861273927868
Root relative squared error: 69.01122978489092
Weighted TruePositiveRate: 0.8096
Weighted MatthewsCorrelation: 0.7319685736394824
Weighted FMeasure: 0.7992344978037423
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9213707090176898
Mean absolute error: 0.1344382788412392
Coverage of cases: 91.0
Instances selection time: 55.0
Test time: 126.0
Accumulative iteration time: 3464.0
Weighted Recall: 0.8096
Weighted FalsePositiveRate: 0.09422696939538594
Kappa statistic: 0.7148516293952984
Training time: 12.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 41.64000000000111
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8397559895647427
Weighted AreaUnderROC: 0.9568071538983615
Root mean squared error: 0.32682955848648565
Relative absolute error: 30.44777775326537
Root relative squared error: 69.33101912939965
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7282642888573617
Weighted FMeasure: 0.7959782061951178
Iteration time: 67.0
Weighted AreaUnderPRC: 0.9209857158972898
Mean absolute error: 0.13532345668118007
Coverage of cases: 90.88
Instances selection time: 55.0
Test time: 125.0
Accumulative iteration time: 3531.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09560206155417968
Kappa statistic: 0.7106681158420168
Training time: 12.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 41.65333333333443
Incorrectly Classified Instances: 19.2
Correctly Classified Instances: 80.8
Weighted Precision: 0.8400581536453028
Weighted AreaUnderROC: 0.9569121957118825
Root mean squared error: 0.32580324535434746
Relative absolute error: 30.347323665773132
Root relative squared error: 69.11330523679293
Weighted TruePositiveRate: 0.808
Weighted MatthewsCorrelation: 0.7296401107556553
Weighted FMeasure: 0.7976514560241244
Iteration time: 66.0
Weighted AreaUnderPRC: 0.9211920916879486
Mean absolute error: 0.13487699407010345
Coverage of cases: 90.8
Instances selection time: 54.0
Test time: 126.0
Accumulative iteration time: 3597.0
Weighted Recall: 0.808
Weighted FalsePositiveRate: 0.09501645565583995
Kappa statistic: 0.7124570782284481
Training time: 12.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 41.666666666667766
Incorrectly Classified Instances: 19.32
Correctly Classified Instances: 80.68
Weighted Precision: 0.8394537904952631
Weighted AreaUnderROC: 0.9568279900269435
Root mean squared error: 0.32643616104805645
Relative absolute error: 30.396424202968632
Root relative squared error: 69.24756693047523
Weighted TruePositiveRate: 0.8068
Weighted MatthewsCorrelation: 0.7281809888237454
Weighted FMeasure: 0.7960812026639625
Iteration time: 65.0
Weighted AreaUnderPRC: 0.9209831840159348
Mean absolute error: 0.1350952186798612
Coverage of cases: 90.8
Instances selection time: 53.0
Test time: 124.0
Accumulative iteration time: 3662.0
Weighted Recall: 0.8068
Weighted FalsePositiveRate: 0.09561142858833409
Kappa statistic: 0.7106646494261935
Training time: 12.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 41.65333333333445
Incorrectly Classified Instances: 19.28
Correctly Classified Instances: 80.72
Weighted Precision: 0.8403454646803653
Weighted AreaUnderROC: 0.9566562904698472
Root mean squared error: 0.32701766852542935
Relative absolute error: 30.455564892063453
Root relative squared error: 69.37092329464355
Weighted TruePositiveRate: 0.8072
Weighted MatthewsCorrelation: 0.7290232543898816
Weighted FMeasure: 0.7963564211548603
Iteration time: 64.0
Weighted AreaUnderPRC: 0.9207201280921985
Mean absolute error: 0.13535806618694932
Coverage of cases: 90.8
Instances selection time: 52.0
Test time: 124.0
Accumulative iteration time: 3726.0
Weighted Recall: 0.8072
Weighted FalsePositiveRate: 0.09540754822621984
Kappa statistic: 0.7112661780795201
Training time: 12.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 41.61333333333446
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8399250018314133
Weighted AreaUnderROC: 0.9562042090725202
Root mean squared error: 0.3279615288153585
Relative absolute error: 30.550734131810486
Root relative squared error: 69.57114629809404
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7280204475300182
Weighted FMeasure: 0.7954109579517079
Iteration time: 63.0
Weighted AreaUnderPRC: 0.9199985976560671
Mean absolute error: 0.13578104058582502
Coverage of cases: 90.8
Instances selection time: 51.0
Test time: 124.0
Accumulative iteration time: 3789.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09580443503431209
Kappa statistic: 0.7100704744397172
Training time: 12.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 41.60000000000111
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.839512825410285
Weighted AreaUnderROC: 0.9565435212659861
Root mean squared error: 0.3280812662944681
Relative absolute error: 30.534570013249883
Root relative squared error: 69.59654645312621
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7274043860911731
Weighted FMeasure: 0.7948891504487132
Iteration time: 64.0
Weighted AreaUnderPRC: 0.9205157340314392
Mean absolute error: 0.135709200058889
Coverage of cases: 90.64
Instances selection time: 51.0
Test time: 125.0
Accumulative iteration time: 3853.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09600037748084876
Kappa statistic: 0.7094731172180728
Training time: 13.0
		
Time end:Wed Nov 01 15.07.22 EET 2017