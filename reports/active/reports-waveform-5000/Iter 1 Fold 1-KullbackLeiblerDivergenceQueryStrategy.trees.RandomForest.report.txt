Wed Nov 01 16.08.38 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 16.08.38 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 71.92000000000154
Incorrectly Classified Instances: 22.24
Correctly Classified Instances: 77.76
Weighted Precision: 0.7774382297077796
Weighted AreaUnderROC: 0.9237522055509231
Root mean squared error: 0.32663129060149604
Relative absolute error: 52.10999999999915
Root relative squared error: 69.28896015960933
Weighted TruePositiveRate: 0.7776
Weighted MatthewsCorrelation: 0.666490193300409
Weighted FMeasure: 0.7765678788887083
Iteration time: 107.0
Weighted AreaUnderPRC: 0.8280819472991683
Mean absolute error: 0.2315999999999973
Coverage of cases: 99.8
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 107.0
Weighted Recall: 0.7776
Weighted FalsePositiveRate: 0.11112256798735266
Kappa statistic: 0.6665054376408355
Training time: 94.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 70.70666666666811
Incorrectly Classified Instances: 20.96
Correctly Classified Instances: 79.04
Weighted Precision: 0.7908995660163872
Weighted AreaUnderROC: 0.9329206539246039
Root mean squared error: 0.3176622944784799
Relative absolute error: 50.339999999999
Root relative squared error: 67.38634876590316
Weighted TruePositiveRate: 0.7904
Weighted MatthewsCorrelation: 0.6858036825555384
Weighted FMeasure: 0.7902674100710516
Iteration time: 103.0
Weighted AreaUnderPRC: 0.844559255747892
Mean absolute error: 0.22373333333332993
Coverage of cases: 99.72
Instances selection time: 13.0
Test time: 12.0
Accumulative iteration time: 210.0
Weighted Recall: 0.7904
Weighted FalsePositiveRate: 0.10488573131723308
Kappa statistic: 0.6855927057507734
Training time: 90.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 71.81333333333474
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.7968936541201127
Weighted AreaUnderROC: 0.9314165936243644
Root mean squared error: 0.3207366520995051
Relative absolute error: 51.62999999999919
Root relative squared error: 68.038518502389
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.6944550335075038
Weighted FMeasure: 0.795647141858351
Iteration time: 109.0
Weighted AreaUnderPRC: 0.841942558531427
Mean absolute error: 0.22946666666666415
Coverage of cases: 99.88
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 319.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10211324013489469
Kappa statistic: 0.6939943694963988
Training time: 96.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 71.56000000000148
Incorrectly Classified Instances: 21.32
Correctly Classified Instances: 78.68
Weighted Precision: 0.7910224097492046
Weighted AreaUnderROC: 0.9320314633365142
Root mean squared error: 0.3202707188197704
Relative absolute error: 51.58799999999906
Root relative squared error: 67.93967912788473
Weighted TruePositiveRate: 0.7868
Weighted MatthewsCorrelation: 0.68229320157735
Weighted FMeasure: 0.7866781426698849
Iteration time: 114.0
Weighted AreaUnderPRC: 0.84166440406994
Mean absolute error: 0.22927999999999693
Coverage of cases: 99.8
Instances selection time: 13.0
Test time: 13.0
Accumulative iteration time: 433.0
Weighted Recall: 0.7868
Weighted FalsePositiveRate: 0.10710579166426205
Kappa statistic: 0.6800239363707488
Training time: 101.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 72.6266666666681
Incorrectly Classified Instances: 21.36
Correctly Classified Instances: 78.64
Weighted Precision: 0.7914918616929897
Weighted AreaUnderROC: 0.9305760772764956
Root mean squared error: 0.3210150567600619
Relative absolute error: 51.60599999999913
Root relative squared error: 68.0975770494071
Weighted TruePositiveRate: 0.7864
Weighted MatthewsCorrelation: 0.6823347801487445
Weighted FMeasure: 0.7852317773321407
Iteration time: 118.0
Weighted AreaUnderPRC: 0.8388312213338506
Mean absolute error: 0.22935999999999718
Coverage of cases: 99.8
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 551.0
Weighted Recall: 0.7864
Weighted FalsePositiveRate: 0.10714583403640092
Kappa statistic: 0.6794896418734898
Training time: 105.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 71.17333333333474
Incorrectly Classified Instances: 22.32
Correctly Classified Instances: 77.68
Weighted Precision: 0.7848426766541715
Weighted AreaUnderROC: 0.9300568908911202
Root mean squared error: 0.32161674914925165
Relative absolute error: 51.455999999999115
Root relative squared error: 68.22521527998242
Weighted TruePositiveRate: 0.7768
Weighted MatthewsCorrelation: 0.6692169782664666
Weighted FMeasure: 0.7761238170965662
Iteration time: 119.0
Weighted AreaUnderPRC: 0.8402413457093378
Mean absolute error: 0.22869333333333047
Coverage of cases: 99.76
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 670.0
Weighted Recall: 0.7768
Weighted FalsePositiveRate: 0.11224177573571767
Kappa statistic: 0.6649902571001653
Training time: 106.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 71.96000000000139
Incorrectly Classified Instances: 23.84
Correctly Classified Instances: 76.16
Weighted Precision: 0.7760265499870106
Weighted AreaUnderROC: 0.9264009023471975
Root mean squared error: 0.3278983582351894
Relative absolute error: 52.913999999999184
Root relative squared error: 69.55774579441132
Weighted TruePositiveRate: 0.7616
Weighted MatthewsCorrelation: 0.6499892597924961
Weighted FMeasure: 0.7593775953316099
Iteration time: 126.0
Weighted AreaUnderPRC: 0.8315631026743926
Mean absolute error: 0.23517333333333082
Coverage of cases: 99.88
Instances selection time: 14.0
Test time: 13.0
Accumulative iteration time: 796.0
Weighted Recall: 0.7616
Weighted FalsePositiveRate: 0.12008726495673855
Kappa statistic: 0.6420611321149959
Training time: 112.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 71.62666666666797
Incorrectly Classified Instances: 24.44
Correctly Classified Instances: 75.56
Weighted Precision: 0.7747189485528942
Weighted AreaUnderROC: 0.9212346672720946
Root mean squared error: 0.3322890308150405
Relative absolute error: 53.14199999999934
Root relative squared error: 70.48914810096608
Weighted TruePositiveRate: 0.7556
Weighted MatthewsCorrelation: 0.6428417051900626
Weighted FMeasure: 0.7545029232071946
Iteration time: 132.0
Weighted AreaUnderPRC: 0.823944668475644
Mean absolute error: 0.23618666666666485
Coverage of cases: 99.76
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 928.0
Weighted Recall: 0.7556
Weighted FalsePositiveRate: 0.12316499677945451
Kappa statistic: 0.6330499590169055
Training time: 119.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 71.73333333333484
Incorrectly Classified Instances: 23.68
Correctly Classified Instances: 76.32
Weighted Precision: 0.7836677500202118
Weighted AreaUnderROC: 0.9285723425638566
Root mean squared error: 0.32905116116899785
Relative absolute error: 53.43599999999927
Root relative squared error: 69.80229222597161
Weighted TruePositiveRate: 0.7632
Weighted MatthewsCorrelation: 0.6552876779200556
Weighted FMeasure: 0.7620884609494736
Iteration time: 134.0
Weighted AreaUnderPRC: 0.834904591999283
Mean absolute error: 0.2374933333333312
Coverage of cases: 99.92
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 1062.0
Weighted Recall: 0.7632
Weighted FalsePositiveRate: 0.11946407141774527
Kappa statistic: 0.6444001920238962
Training time: 121.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 70.24000000000123
Incorrectly Classified Instances: 23.0
Correctly Classified Instances: 77.0
Weighted Precision: 0.7825466532886372
Weighted AreaUnderROC: 0.9260761601226896
Root mean squared error: 0.3264066992776534
Relative absolute error: 51.25799999999913
Root relative squared error: 69.24131714518391
Weighted TruePositiveRate: 0.77
Weighted MatthewsCorrelation: 0.6617683262966573
Weighted FMeasure: 0.7682580649170894
Iteration time: 137.0
Weighted AreaUnderPRC: 0.8305757427428336
Mean absolute error: 0.2278133333333305
Coverage of cases: 99.52
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 1199.0
Weighted Recall: 0.77
Weighted FalsePositiveRate: 0.1158680070851932
Kappa statistic: 0.6546670990835166
Training time: 125.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 70.4000000000014
Incorrectly Classified Instances: 22.68
Correctly Classified Instances: 77.32
Weighted Precision: 0.792486587080727
Weighted AreaUnderROC: 0.9324031096170377
Root mean squared error: 0.32519942599375207
Relative absolute error: 50.93999999999916
Root relative squared error: 68.98521580744631
Weighted TruePositiveRate: 0.7732
Weighted MatthewsCorrelation: 0.6705164280146663
Weighted FMeasure: 0.7697606704931506
Iteration time: 142.0
Weighted AreaUnderPRC: 0.8445220486549362
Mean absolute error: 0.22639999999999733
Coverage of cases: 99.76
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 1341.0
Weighted Recall: 0.7732
Weighted FalsePositiveRate: 0.1144690594390358
Kappa statistic: 0.6593835214193099
Training time: 129.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 70.58666666666792
Incorrectly Classified Instances: 23.6
Correctly Classified Instances: 76.4
Weighted Precision: 0.7865669725378043
Weighted AreaUnderROC: 0.9330508047824645
Root mean squared error: 0.3248712565514722
Relative absolute error: 51.233999999999114
Root relative squared error: 68.91560055604201
Weighted TruePositiveRate: 0.764
Weighted MatthewsCorrelation: 0.6583679060292947
Weighted FMeasure: 0.7608156652631793
Iteration time: 146.0
Weighted AreaUnderPRC: 0.8455492706571209
Mean absolute error: 0.2277066666666638
Coverage of cases: 99.84
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 1487.0
Weighted Recall: 0.764
Weighted FalsePositiveRate: 0.11920363605645364
Kappa statistic: 0.6455254010489564
Training time: 134.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 71.01333333333466
Incorrectly Classified Instances: 24.48
Correctly Classified Instances: 75.52
Weighted Precision: 0.7830362150552419
Weighted AreaUnderROC: 0.92901507248524
Root mean squared error: 0.33028876254978146
Relative absolute error: 52.037999999999215
Root relative squared error: 70.06482712459899
Weighted TruePositiveRate: 0.7552
Weighted MatthewsCorrelation: 0.6476073947393788
Weighted FMeasure: 0.7524554446030057
Iteration time: 154.0
Weighted AreaUnderPRC: 0.8386464020193675
Mean absolute error: 0.2312799999999976
Coverage of cases: 99.6
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 1641.0
Weighted Recall: 0.7552
Weighted FalsePositiveRate: 0.12377267053447326
Kappa statistic: 0.6322647539947344
Training time: 141.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 70.53333333333454
Incorrectly Classified Instances: 24.68
Correctly Classified Instances: 75.32
Weighted Precision: 0.7786462509105758
Weighted AreaUnderROC: 0.9272150223366566
Root mean squared error: 0.33155391718391647
Relative absolute error: 51.85199999999924
Root relative squared error: 70.33320695091294
Weighted TruePositiveRate: 0.7532
Weighted MatthewsCorrelation: 0.6433020819970477
Weighted FMeasure: 0.7501326247386316
Iteration time: 148.0
Weighted AreaUnderPRC: 0.8389635603392233
Mean absolute error: 0.230453333333331
Coverage of cases: 99.68
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 1789.0
Weighted Recall: 0.7532
Weighted FalsePositiveRate: 0.12469737639059199
Kappa statistic: 0.6292904968588828
Training time: 136.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 71.04000000000124
Incorrectly Classified Instances: 24.32
Correctly Classified Instances: 75.68
Weighted Precision: 0.7779168711062581
Weighted AreaUnderROC: 0.923124176081171
Root mean squared error: 0.33436506994600834
Relative absolute error: 52.5659999999993
Root relative squared error: 70.92954250522087
Weighted TruePositiveRate: 0.7568
Weighted MatthewsCorrelation: 0.6464089910388677
Weighted FMeasure: 0.7518914214200032
Iteration time: 155.0
Weighted AreaUnderPRC: 0.8255884596421338
Mean absolute error: 0.23362666666666465
Coverage of cases: 99.44
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 1944.0
Weighted Recall: 0.7568
Weighted FalsePositiveRate: 0.12261479310327213
Kappa statistic: 0.6347953532514026
Training time: 142.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 70.45333333333468
Incorrectly Classified Instances: 27.28
Correctly Classified Instances: 72.72
Weighted Precision: 0.7686902579285999
Weighted AreaUnderROC: 0.9212703865945816
Root mean squared error: 0.3429168995543949
Relative absolute error: 53.71199999999938
Root relative squared error: 72.74365951751348
Weighted TruePositiveRate: 0.7272
Weighted MatthewsCorrelation: 0.613262778019486
Weighted FMeasure: 0.7200084272006675
Iteration time: 160.0
Weighted AreaUnderPRC: 0.8224568147801877
Mean absolute error: 0.23871999999999838
Coverage of cases: 99.52
Instances selection time: 13.0
Test time: 15.0
Accumulative iteration time: 2104.0
Weighted Recall: 0.7272
Weighted FalsePositiveRate: 0.1381153483056436
Kappa statistic: 0.5900322780451585
Training time: 147.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 68.4400000000012
Incorrectly Classified Instances: 25.48
Correctly Classified Instances: 74.52
Weighted Precision: 0.7775694739422478
Weighted AreaUnderROC: 0.9275983322991491
Root mean squared error: 0.33265397437377187
Relative absolute error: 51.44999999999915
Root relative squared error: 70.56656432050487
Weighted TruePositiveRate: 0.7452
Weighted MatthewsCorrelation: 0.6352625735095073
Weighted FMeasure: 0.7406454157782517
Iteration time: 165.0
Weighted AreaUnderPRC: 0.8366237549518075
Mean absolute error: 0.22866666666666396
Coverage of cases: 99.68
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 2269.0
Weighted Recall: 0.7452
Weighted FalsePositiveRate: 0.12890778451983043
Kappa statistic: 0.6171808742843625
Training time: 153.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 70.22666666666791
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7734391643155829
Weighted AreaUnderROC: 0.9240683829295544
Root mean squared error: 0.33572210730503366
Relative absolute error: 52.259999999999266
Root relative squared error: 71.21741360088795
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.6308372235121157
Weighted FMeasure: 0.7382722169791336
Iteration time: 162.0
Weighted AreaUnderPRC: 0.8263619313189488
Mean absolute error: 0.2322666666666645
Coverage of cases: 99.36
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 2431.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.13001978034761985
Kappa statistic: 0.6135925055935941
Training time: 150.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 70.65333333333463
Incorrectly Classified Instances: 26.04
Correctly Classified Instances: 73.96
Weighted Precision: 0.7741868387993298
Weighted AreaUnderROC: 0.9262060647779808
Root mean squared error: 0.3368916739843821
Relative absolute error: 53.17199999999936
Root relative squared error: 71.46551615989308
Weighted TruePositiveRate: 0.7396
Weighted MatthewsCorrelation: 0.6279477686107803
Weighted FMeasure: 0.7353700855978065
Iteration time: 176.0
Weighted AreaUnderPRC: 0.8319013854130919
Mean absolute error: 0.23631999999999825
Coverage of cases: 99.36
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 2607.0
Weighted Recall: 0.7396
Weighted FalsePositiveRate: 0.1317403617571574
Kappa statistic: 0.6087490209710525
Training time: 164.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 70.2000000000015
Incorrectly Classified Instances: 25.72
Correctly Classified Instances: 74.28
Weighted Precision: 0.7713783274290423
Weighted AreaUnderROC: 0.923279422589748
Root mean squared error: 0.3380256400531362
Relative absolute error: 53.01599999999939
Root relative squared error: 71.70606668894852
Weighted TruePositiveRate: 0.7428
Weighted MatthewsCorrelation: 0.6299695798331696
Weighted FMeasure: 0.7379860807430383
Iteration time: 168.0
Weighted AreaUnderPRC: 0.8229752531939232
Mean absolute error: 0.23562666666666504
Coverage of cases: 99.32
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 2775.0
Weighted Recall: 0.7428
Weighted FalsePositiveRate: 0.12999588883748212
Kappa statistic: 0.6135969639658437
Training time: 157.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 69.77333333333465
Incorrectly Classified Instances: 27.16
Correctly Classified Instances: 72.84
Weighted Precision: 0.7649629726774765
Weighted AreaUnderROC: 0.9280296862923667
Root mean squared error: 0.3377809941367322
Relative absolute error: 52.559999999999256
Root relative squared error: 71.65416945300487
Weighted TruePositiveRate: 0.7284
Weighted MatthewsCorrelation: 0.6128764502885239
Weighted FMeasure: 0.7218163456319983
Iteration time: 174.0
Weighted AreaUnderPRC: 0.834557438115451
Mean absolute error: 0.2335999999999978
Coverage of cases: 99.6
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 2949.0
Weighted Recall: 0.7284
Weighted FalsePositiveRate: 0.13743719013071654
Kappa statistic: 0.5918566566916995
Training time: 163.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 71.26666666666787
Incorrectly Classified Instances: 25.52
Correctly Classified Instances: 74.48
Weighted Precision: 0.7813704414572314
Weighted AreaUnderROC: 0.9260048823957759
Root mean squared error: 0.33909683965891796
Relative absolute error: 53.42999999999937
Root relative squared error: 71.93330244052432
Weighted TruePositiveRate: 0.7448
Weighted MatthewsCorrelation: 0.6377461119490774
Weighted FMeasure: 0.7409541326611397
Iteration time: 177.0
Weighted AreaUnderPRC: 0.8303160458528797
Mean absolute error: 0.23746666666666497
Coverage of cases: 99.28
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 3126.0
Weighted Recall: 0.7448
Weighted FalsePositiveRate: 0.12925899024488152
Kappa statistic: 0.6164917676772049
Training time: 165.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 69.36000000000126
Incorrectly Classified Instances: 26.12
Correctly Classified Instances: 73.88
Weighted Precision: 0.7761330487683943
Weighted AreaUnderROC: 0.9285210124707548
Root mean squared error: 0.3362181831291488
Relative absolute error: 51.53999999999938
Root relative squared error: 71.32264717465232
Weighted TruePositiveRate: 0.7388
Weighted MatthewsCorrelation: 0.6291391111905416
Weighted FMeasure: 0.7333056479847695
Iteration time: 181.0
Weighted AreaUnderPRC: 0.8361900939682653
Mean absolute error: 0.229066666666665
Coverage of cases: 99.56
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 3307.0
Weighted Recall: 0.7388
Weighted FalsePositiveRate: 0.1322528222658762
Kappa statistic: 0.607472662544789
Training time: 170.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 71.41333333333455
Incorrectly Classified Instances: 27.4
Correctly Classified Instances: 72.6
Weighted Precision: 0.7653071876392518
Weighted AreaUnderROC: 0.9259323237929109
Root mean squared error: 0.34134244779497624
Relative absolute error: 54.383999999999354
Root relative squared error: 72.40966786279266
Weighted TruePositiveRate: 0.726
Weighted MatthewsCorrelation: 0.6113667686564294
Weighted FMeasure: 0.722576620782037
Iteration time: 195.0
Weighted AreaUnderPRC: 0.8290377878744448
Mean absolute error: 0.24170666666666493
Coverage of cases: 99.68
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 3502.0
Weighted Recall: 0.726
Weighted FalsePositiveRate: 0.1388249470892608
Kappa statistic: 0.5881682976404509
Training time: 184.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 70.77333333333458
Incorrectly Classified Instances: 25.0
Correctly Classified Instances: 75.0
Weighted Precision: 0.7804120819856828
Weighted AreaUnderROC: 0.927484025713289
Root mean squared error: 0.33599603172259734
Relative absolute error: 52.8179999999993
Root relative squared error: 71.2755217448455
Weighted TruePositiveRate: 0.75
Weighted MatthewsCorrelation: 0.642147626513269
Weighted FMeasure: 0.7469660941540249
Iteration time: 195.0
Weighted AreaUnderPRC: 0.8351450915969908
Mean absolute error: 0.23474666666666466
Coverage of cases: 99.56
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 3697.0
Weighted Recall: 0.75
Weighted FalsePositiveRate: 0.12650456031219445
Kappa statistic: 0.6243640031810659
Training time: 184.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 69.1600000000013
Incorrectly Classified Instances: 25.28
Correctly Classified Instances: 74.72
Weighted Precision: 0.780421067877662
Weighted AreaUnderROC: 0.9277301562309663
Root mean squared error: 0.33425339689123934
Relative absolute error: 51.23399999999925
Root relative squared error: 70.90585307292997
Weighted TruePositiveRate: 0.7472
Weighted MatthewsCorrelation: 0.639640830268532
Weighted FMeasure: 0.7453957369969266
Iteration time: 191.0
Weighted AreaUnderPRC: 0.8384363042471825
Mean absolute error: 0.2277066666666644
Coverage of cases: 99.6
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 3888.0
Weighted Recall: 0.7472
Weighted FalsePositiveRate: 0.1280261855112316
Kappa statistic: 0.6201070144114087
Training time: 181.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 70.38666666666792
Incorrectly Classified Instances: 23.64
Correctly Classified Instances: 76.36
Weighted Precision: 0.7824139152126404
Weighted AreaUnderROC: 0.9324874865575586
Root mean squared error: 0.3259202356405614
Relative absolute error: 51.587999999999184
Root relative squared error: 69.13812262420738
Weighted TruePositiveRate: 0.7636
Weighted MatthewsCorrelation: 0.6556810764655469
Weighted FMeasure: 0.7625335546927862
Iteration time: 202.0
Weighted AreaUnderPRC: 0.8464501902588276
Mean absolute error: 0.22927999999999746
Coverage of cases: 99.68
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 4090.0
Weighted Recall: 0.7636
Weighted FalsePositiveRate: 0.11937410234746569
Kappa statistic: 0.6449391025502383
Training time: 192.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 68.53333333333454
Incorrectly Classified Instances: 25.32
Correctly Classified Instances: 74.68
Weighted Precision: 0.7732875531361242
Weighted AreaUnderROC: 0.9342951499411857
Root mean squared error: 0.32553136049644427
Relative absolute error: 49.43999999999902
Root relative squared error: 69.05562974877535
Weighted TruePositiveRate: 0.7468
Weighted MatthewsCorrelation: 0.634954883065091
Weighted FMeasure: 0.7450904763211942
Iteration time: 202.0
Weighted AreaUnderPRC: 0.8515264456624111
Mean absolute error: 0.21973333333332998
Coverage of cases: 99.68
Instances selection time: 11.0
Test time: 17.0
Accumulative iteration time: 4292.0
Weighted Recall: 0.7468
Weighted FalsePositiveRate: 0.12806456547457853
Kappa statistic: 0.619588602802424
Training time: 191.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 70.22666666666805
Incorrectly Classified Instances: 24.92
Correctly Classified Instances: 75.08
Weighted Precision: 0.7828704066946719
Weighted AreaUnderROC: 0.9317209481476312
Root mean squared error: 0.32943588147012676
Relative absolute error: 51.34199999999917
Root relative squared error: 69.88390372610813
Weighted TruePositiveRate: 0.7508
Weighted MatthewsCorrelation: 0.6440827173268084
Weighted FMeasure: 0.7491911319697175
Iteration time: 209.0
Weighted AreaUnderPRC: 0.8436841848544755
Mean absolute error: 0.22818666666666404
Coverage of cases: 99.48
Instances selection time: 10.0
Test time: 18.0
Accumulative iteration time: 4501.0
Weighted Recall: 0.7508
Weighted FalsePositiveRate: 0.1261877672191663
Kappa statistic: 0.6255467738360214
Training time: 199.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 68.40000000000114
Incorrectly Classified Instances: 24.44
Correctly Classified Instances: 75.56
Weighted Precision: 0.775157969832253
Weighted AreaUnderROC: 0.9310033639595345
Root mean squared error: 0.3267374073064373
Relative absolute error: 50.21999999999906
Root relative squared error: 69.31147091210768
Weighted TruePositiveRate: 0.7556
Weighted MatthewsCorrelation: 0.6444325806814742
Weighted FMeasure: 0.754180881252834
Iteration time: 220.0
Weighted AreaUnderPRC: 0.8416491704752826
Mean absolute error: 0.22319999999999687
Coverage of cases: 99.6
Instances selection time: 10.0
Test time: 27.0
Accumulative iteration time: 4721.0
Weighted Recall: 0.7556
Weighted FalsePositiveRate: 0.12344417517984134
Kappa statistic: 0.632881687681937
Training time: 210.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 67.42666666666778
Incorrectly Classified Instances: 22.96
Correctly Classified Instances: 77.04
Weighted Precision: 0.7903512099572081
Weighted AreaUnderROC: 0.9339341060256057
Root mean squared error: 0.3220807352202228
Relative absolute error: 48.56399999999903
Root relative squared error: 68.32364158913037
Weighted TruePositiveRate: 0.7704
Weighted MatthewsCorrelation: 0.6669801208868659
Weighted FMeasure: 0.7691753814690279
Iteration time: 215.0
Weighted AreaUnderPRC: 0.8477555761066895
Mean absolute error: 0.2158399999999967
Coverage of cases: 99.28
Instances selection time: 10.0
Test time: 17.0
Accumulative iteration time: 4936.0
Weighted Recall: 0.7704
Weighted FalsePositiveRate: 0.11601640877245584
Kappa statistic: 0.6551079355938705
Training time: 205.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 69.33333333333458
Incorrectly Classified Instances: 23.56
Correctly Classified Instances: 76.44
Weighted Precision: 0.7864895063439682
Weighted AreaUnderROC: 0.9353874610096878
Root mean squared error: 0.32395061352002286
Relative absolute error: 50.4899999999989
Root relative squared error: 68.72030267686503
Weighted TruePositiveRate: 0.7644
Weighted MatthewsCorrelation: 0.6592663142969277
Weighted FMeasure: 0.7628371404027046
Iteration time: 234.0
Weighted AreaUnderPRC: 0.8483295830728906
Mean absolute error: 0.22439999999999619
Coverage of cases: 99.48
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 5170.0
Weighted Recall: 0.7644
Weighted FalsePositiveRate: 0.11908390347491893
Kappa statistic: 0.6460637724578244
Training time: 222.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 68.33333333333441
Incorrectly Classified Instances: 23.2
Correctly Classified Instances: 76.8
Weighted Precision: 0.7890779098034043
Weighted AreaUnderROC: 0.9316057430968684
Root mean squared error: 0.32466598220324755
Relative absolute error: 49.21199999999898
Root relative squared error: 68.87205529095203
Weighted TruePositiveRate: 0.768
Weighted MatthewsCorrelation: 0.6635608055406834
Weighted FMeasure: 0.7676543978047166
Iteration time: 229.0
Weighted AreaUnderPRC: 0.8468080474460603
Mean absolute error: 0.21871999999999647
Coverage of cases: 99.48
Instances selection time: 13.0
Test time: 14.0
Accumulative iteration time: 5399.0
Weighted Recall: 0.768
Weighted FalsePositiveRate: 0.11726286789599066
Kappa statistic: 0.6515079850335899
Training time: 216.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 68.74666666666782
Incorrectly Classified Instances: 22.44
Correctly Classified Instances: 77.56
Weighted Precision: 0.7918333519454746
Weighted AreaUnderROC: 0.9348787024689936
Root mean squared error: 0.3207491231476703
Relative absolute error: 49.00199999999903
Root relative squared error: 68.04116401120685
Weighted TruePositiveRate: 0.7756
Weighted MatthewsCorrelation: 0.6723248857613027
Weighted FMeasure: 0.7750408251733765
Iteration time: 231.0
Weighted AreaUnderPRC: 0.852239195756198
Mean absolute error: 0.21778666666666335
Coverage of cases: 99.52
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 5630.0
Weighted Recall: 0.7756
Weighted FalsePositiveRate: 0.11327714011163548
Kappa statistic: 0.6629801541568424
Training time: 219.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 69.56000000000127
Incorrectly Classified Instances: 23.36
Correctly Classified Instances: 76.64
Weighted Precision: 0.7882667763832933
Weighted AreaUnderROC: 0.9347917850913465
Root mean squared error: 0.32322128642773407
Relative absolute error: 50.255999999998984
Root relative squared error: 68.56558903706689
Weighted TruePositiveRate: 0.7664
Weighted MatthewsCorrelation: 0.6616851558829651
Weighted FMeasure: 0.7657923373542017
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8510076914104393
Mean absolute error: 0.22335999999999653
Coverage of cases: 99.76
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 5877.0
Weighted Recall: 0.7664
Weighted FalsePositiveRate: 0.11808188437748468
Kappa statistic: 0.6490909291927985
Training time: 235.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 69.6933333333345
Incorrectly Classified Instances: 23.04
Correctly Classified Instances: 76.96
Weighted Precision: 0.7883157927866232
Weighted AreaUnderROC: 0.9367069881226802
Root mean squared error: 0.32026239242221227
Relative absolute error: 49.949999999999044
Root relative squared error: 67.93791283223189
Weighted TruePositiveRate: 0.7696
Weighted MatthewsCorrelation: 0.6645438230469699
Weighted FMeasure: 0.7694476382852963
Iteration time: 248.0
Weighted AreaUnderPRC: 0.8557093548296576
Mean absolute error: 0.22199999999999678
Coverage of cases: 99.6
Instances selection time: 12.0
Test time: 14.0
Accumulative iteration time: 6125.0
Weighted Recall: 0.7696
Weighted FalsePositiveRate: 0.11638868951112193
Kappa statistic: 0.6539436485682398
Training time: 236.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 68.8533333333345
Incorrectly Classified Instances: 22.6
Correctly Classified Instances: 77.4
Weighted Precision: 0.790063407679725
Weighted AreaUnderROC: 0.9375685516246466
Root mean squared error: 0.31937856325474645
Relative absolute error: 49.34999999999899
Root relative squared error: 67.75042435291422
Weighted TruePositiveRate: 0.774
Weighted MatthewsCorrelation: 0.669671210864315
Weighted FMeasure: 0.7733913426853866
Iteration time: 247.0
Weighted AreaUnderPRC: 0.8573257331490464
Mean absolute error: 0.21933333333332988
Coverage of cases: 99.8
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 6372.0
Weighted Recall: 0.774
Weighted FalsePositiveRate: 0.11404702221768223
Kappa statistic: 0.6605900470824682
Training time: 236.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 67.54666666666779
Incorrectly Classified Instances: 22.76
Correctly Classified Instances: 77.24
Weighted Precision: 0.7891282581381375
Weighted AreaUnderROC: 0.9374044491919176
Root mean squared error: 0.31769167442663526
Relative absolute error: 48.227999999998964
Root relative squared error: 67.39258119407465
Weighted TruePositiveRate: 0.7724
Weighted MatthewsCorrelation: 0.6677176656483113
Weighted FMeasure: 0.7716551898107645
Iteration time: 246.0
Weighted AreaUnderPRC: 0.8582590307061418
Mean absolute error: 0.21434666666666308
Coverage of cases: 99.6
Instances selection time: 11.0
Test time: 14.0
Accumulative iteration time: 6618.0
Weighted Recall: 0.7724
Weighted FalsePositiveRate: 0.11488278461642985
Kappa statistic: 0.6581721956663202
Training time: 235.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 68.72000000000104
Incorrectly Classified Instances: 22.72
Correctly Classified Instances: 77.28
Weighted Precision: 0.7904296376328923
Weighted AreaUnderROC: 0.9367852775999068
Root mean squared error: 0.3205828026163173
Relative absolute error: 49.145999999999006
Root relative squared error: 68.00588209853576
Weighted TruePositiveRate: 0.7728
Weighted MatthewsCorrelation: 0.6686189662190052
Weighted FMeasure: 0.7718051854869925
Iteration time: 259.0
Weighted AreaUnderPRC: 0.8561551175462566
Mean absolute error: 0.21842666666666327
Coverage of cases: 99.68
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 6877.0
Weighted Recall: 0.7728
Weighted FalsePositiveRate: 0.11466652345619753
Kappa statistic: 0.658780491086962
Training time: 250.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 68.32000000000119
Incorrectly Classified Instances: 23.12
Correctly Classified Instances: 76.88
Weighted Precision: 0.7895248225720758
Weighted AreaUnderROC: 0.9355037489582037
Root mean squared error: 0.3216333316060377
Relative absolute error: 49.50599999999898
Root relative squared error: 68.22873295027507
Weighted TruePositiveRate: 0.7688
Weighted MatthewsCorrelation: 0.6643469410559415
Weighted FMeasure: 0.7671541680991745
Iteration time: 265.0
Weighted AreaUnderPRC: 0.8519220385761195
Mean absolute error: 0.22002666666666315
Coverage of cases: 99.68
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 7142.0
Weighted Recall: 0.7688
Weighted FalsePositiveRate: 0.11673192129782133
Kappa statistic: 0.6527387259043934
Training time: 257.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 69.02666666666771
Incorrectly Classified Instances: 23.52
Correctly Classified Instances: 76.48
Weighted Precision: 0.7836686756847603
Weighted AreaUnderROC: 0.9348522327698431
Root mean squared error: 0.3231635292954111
Relative absolute error: 49.31999999999897
Root relative squared error: 68.55333689908865
Weighted TruePositiveRate: 0.7648
Weighted MatthewsCorrelation: 0.6573069156679779
Weighted FMeasure: 0.7622461830840729
Iteration time: 269.0
Weighted AreaUnderPRC: 0.8504446177610876
Mean absolute error: 0.21919999999999645
Coverage of cases: 99.48
Instances selection time: 9.0
Test time: 14.0
Accumulative iteration time: 7411.0
Weighted Recall: 0.7648
Weighted FalsePositiveRate: 0.11863181874067787
Kappa statistic: 0.6467635127374194
Training time: 260.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 69.69333333333437
Incorrectly Classified Instances: 24.08
Correctly Classified Instances: 75.92
Weighted Precision: 0.7796163934202857
Weighted AreaUnderROC: 0.9350341806933359
Root mean squared error: 0.3260879227038818
Relative absolute error: 50.77799999999893
Root relative squared error: 69.17369442208471
Weighted TruePositiveRate: 0.7592
Weighted MatthewsCorrelation: 0.6495194240308362
Weighted FMeasure: 0.7565712933135763
Iteration time: 274.0
Weighted AreaUnderPRC: 0.8524022019563996
Mean absolute error: 0.22567999999999633
Coverage of cases: 99.76
Instances selection time: 8.0
Test time: 14.0
Accumulative iteration time: 7685.0
Weighted Recall: 0.7592
Weighted FalsePositiveRate: 0.1214035176003228
Kappa statistic: 0.6383602460592079
Training time: 266.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 68.84000000000121
Incorrectly Classified Instances: 23.08
Correctly Classified Instances: 76.92
Weighted Precision: 0.7893409693061249
Weighted AreaUnderROC: 0.9356743174011255
Root mean squared error: 0.32240760950490294
Relative absolute error: 49.69199999999902
Root relative squared error: 68.39298209611822
Weighted TruePositiveRate: 0.7692
Weighted MatthewsCorrelation: 0.6648369606283715
Weighted FMeasure: 0.7660956961978346
Iteration time: 274.0
Weighted AreaUnderPRC: 0.8512593001293429
Mean absolute error: 0.22085333333333002
Coverage of cases: 99.48
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 7959.0
Weighted Recall: 0.7692
Weighted FalsePositiveRate: 0.11649335327283811
Kappa statistic: 0.6533405236636848
Training time: 266.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 66.9600000000011
Incorrectly Classified Instances: 24.2
Correctly Classified Instances: 75.8
Weighted Precision: 0.7783450252182367
Weighted AreaUnderROC: 0.9332691907078108
Root mean squared error: 0.3224985788082371
Relative absolute error: 48.431999999999
Root relative squared error: 68.41227959949845
Weighted TruePositiveRate: 0.758
Weighted MatthewsCorrelation: 0.647656861606471
Weighted FMeasure: 0.7560271651081634
Iteration time: 276.0
Weighted AreaUnderPRC: 0.8493318125556935
Mean absolute error: 0.2152533333333299
Coverage of cases: 99.4
Instances selection time: 8.0
Test time: 15.0
Accumulative iteration time: 8235.0
Weighted Recall: 0.758
Weighted FalsePositiveRate: 0.12206651450338309
Kappa statistic: 0.636546002342446
Training time: 268.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 68.29333333333452
Incorrectly Classified Instances: 23.24
Correctly Classified Instances: 76.76
Weighted Precision: 0.7855105490228277
Weighted AreaUnderROC: 0.935728410195063
Root mean squared error: 0.32169136347333405
Relative absolute error: 48.959999999998985
Root relative squared error: 68.24104336834213
Weighted TruePositiveRate: 0.7676
Weighted MatthewsCorrelation: 0.6608243248252034
Weighted FMeasure: 0.7651235281082827
Iteration time: 285.0
Weighted AreaUnderPRC: 0.8544780379564061
Mean absolute error: 0.2175999999999965
Coverage of cases: 99.64
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 8520.0
Weighted Recall: 0.7676
Weighted FalsePositiveRate: 0.11713091576100466
Kappa statistic: 0.6510077885931324
Training time: 278.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 67.65333333333442
Incorrectly Classified Instances: 24.28
Correctly Classified Instances: 75.72
Weighted Precision: 0.7817701558199633
Weighted AreaUnderROC: 0.9288288585897433
Root mean squared error: 0.3286457058900959
Relative absolute error: 50.02799999999901
Root relative squared error: 69.71628217281778
Weighted TruePositiveRate: 0.7572
Weighted MatthewsCorrelation: 0.6483402627479669
Weighted FMeasure: 0.7534207429025555
Iteration time: 284.0
Weighted AreaUnderPRC: 0.8389409098250042
Mean absolute error: 0.2223466666666633
Coverage of cases: 99.12
Instances selection time: 7.0
Test time: 15.0
Accumulative iteration time: 8804.0
Weighted Recall: 0.7572
Weighted FalsePositiveRate: 0.12243506691879706
Kappa statistic: 0.6353550164336543
Training time: 277.0
		
Time end:Wed Nov 01 16.08.50 EET 2017