Wed Nov 01 15.34.36 EET 2017
Dataset: waveform
Test set size: 2500
Initial Labelled set size: 500
Initial Unlabelled set size: 2000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 15.34.36 EET 2017
		
Iteration: 1
Labeled set size: 500
Unlabelled set size: 2000
	
Mean region size: 39.133333333334356
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.841292016764236
Weighted AreaUnderROC: 0.9545876650833277
Root mean squared error: 0.3315731038286152
Relative absolute error: 29.383559486363655
Root relative squared error: 70.33727705288536
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7283245366860054
Weighted FMeasure: 0.7942388843498575
Iteration time: 204.0
Weighted AreaUnderPRC: 0.9157420603037623
Mean absolute error: 0.1305935977171724
Coverage of cases: 88.48
Instances selection time: 198.0
Test time: 139.0
Accumulative iteration time: 204.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09573616308488768
Kappa statistic: 0.710097843175871
Training time: 6.0
		
Iteration: 2
Labeled set size: 520
Unlabelled set size: 1980
	
Mean region size: 39.25333333333433
Incorrectly Classified Instances: 19.24
Correctly Classified Instances: 80.76
Weighted Precision: 0.8421711558104941
Weighted AreaUnderROC: 0.9552569596415739
Root mean squared error: 0.332003288056777
Relative absolute error: 29.46314653117282
Root relative squared error: 70.42853290835315
Weighted TruePositiveRate: 0.8076
Weighted MatthewsCorrelation: 0.7300328007844363
Weighted FMeasure: 0.7955303311557025
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9167782160899586
Mean absolute error: 0.13094731791632427
Coverage of cases: 88.64
Instances selection time: 185.0
Test time: 128.0
Accumulative iteration time: 394.0
Weighted Recall: 0.8076
Weighted FalsePositiveRate: 0.09514762118598925
Kappa statistic: 0.7118903395932396
Training time: 5.0
		
Iteration: 3
Labeled set size: 540
Unlabelled set size: 1960
	
Mean region size: 39.146666666667684
Incorrectly Classified Instances: 19.64
Correctly Classified Instances: 80.36
Weighted Precision: 0.8394447364786016
Weighted AreaUnderROC: 0.9551626969329718
Root mean squared error: 0.3342682343652615
Relative absolute error: 29.732684368657907
Root relative squared error: 70.909000576479
Weighted TruePositiveRate: 0.8036
Weighted MatthewsCorrelation: 0.7246137125695512
Weighted FMeasure: 0.7910283931326165
Iteration time: 190.0
Weighted AreaUnderPRC: 0.9166494534963382
Mean absolute error: 0.13214526386070244
Coverage of cases: 88.32
Instances selection time: 184.0
Test time: 127.0
Accumulative iteration time: 584.0
Weighted Recall: 0.8036
Weighted FalsePositiveRate: 0.09712633875214315
Kappa statistic: 0.7059098347105458
Training time: 6.0
		
Iteration: 4
Labeled set size: 560
Unlabelled set size: 1940
	
Mean region size: 39.05333333333433
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8402652504168953
Weighted AreaUnderROC: 0.9555015424675528
Root mean squared error: 0.33472534797255665
Relative absolute error: 29.771494856192408
Root relative squared error: 71.00596901592631
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7254035261922182
Weighted FMeasure: 0.7914096875264484
Iteration time: 189.0
Weighted AreaUnderPRC: 0.9171963749408817
Mean absolute error: 0.13231775491641132
Coverage of cases: 88.32
Instances selection time: 183.0
Test time: 128.0
Accumulative iteration time: 773.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09692174383074048
Kappa statistic: 0.7065100620329917
Training time: 6.0
		
Iteration: 5
Labeled set size: 580
Unlabelled set size: 1920
	
Mean region size: 39.026666666667666
Incorrectly Classified Instances: 19.56
Correctly Classified Instances: 80.44
Weighted Precision: 0.8413200487751689
Weighted AreaUnderROC: 0.9549006456159815
Root mean squared error: 0.3362070533639079
Relative absolute error: 29.93474375774407
Root relative squared error: 71.32028619490984
Weighted TruePositiveRate: 0.8044
Weighted MatthewsCorrelation: 0.7263295223261542
Weighted FMeasure: 0.7913843438089049
Iteration time: 187.0
Weighted AreaUnderPRC: 0.9162104021158229
Mean absolute error: 0.13304330558997426
Coverage of cases: 88.12
Instances selection time: 181.0
Test time: 128.0
Accumulative iteration time: 960.0
Weighted Recall: 0.8044
Weighted FalsePositiveRate: 0.09671786346862624
Kappa statistic: 0.7071142137172609
Training time: 6.0
		
Iteration: 6
Labeled set size: 600
Unlabelled set size: 1900
	
Mean region size: 39.05333333333433
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8410773992451192
Weighted AreaUnderROC: 0.9549010201770736
Root mean squared error: 0.33711882148854383
Relative absolute error: 30.067087400636794
Root relative squared error: 71.5137014220498
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7258055663291724
Weighted FMeasure: 0.7908896449950176
Iteration time: 184.0
Weighted AreaUnderPRC: 0.9162088234474147
Mean absolute error: 0.13363149955838638
Coverage of cases: 88.16
Instances selection time: 178.0
Test time: 127.0
Accumulative iteration time: 1144.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09691594959302816
Kappa statistic: 0.7065165309056062
Training time: 6.0
		
Iteration: 7
Labeled set size: 620
Unlabelled set size: 1880
	
Mean region size: 39.05333333333433
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.8412722937901179
Weighted AreaUnderROC: 0.9549448483202665
Root mean squared error: 0.3375758195196317
Relative absolute error: 30.124481471534654
Root relative squared error: 71.61064534408115
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.72594128907131
Weighted FMeasure: 0.7905986511766802
Iteration time: 182.0
Weighted AreaUnderPRC: 0.9162226223707587
Mean absolute error: 0.13388658431793243
Coverage of cases: 88.16
Instances selection time: 175.0
Test time: 130.0
Accumulative iteration time: 1326.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09691809327089339
Kappa statistic: 0.7065184996363225
Training time: 7.0
		
Iteration: 8
Labeled set size: 640
Unlabelled set size: 1860
	
Mean region size: 38.98666666666767
Incorrectly Classified Instances: 19.76
Correctly Classified Instances: 80.24
Weighted Precision: 0.8402411627849075
Weighted AreaUnderROC: 0.9547797477080032
Root mean squared error: 0.3384060647730215
Relative absolute error: 30.21797622813155
Root relative squared error: 71.78676695869709
Weighted TruePositiveRate: 0.8024
Weighted MatthewsCorrelation: 0.7238123800137237
Weighted FMeasure: 0.7887505341469416
Iteration time: 180.0
Weighted AreaUnderPRC: 0.9159826050245272
Mean absolute error: 0.1343021165694742
Coverage of cases: 87.96
Instances selection time: 174.0
Test time: 130.0
Accumulative iteration time: 1506.0
Weighted Recall: 0.8024
Weighted FalsePositiveRate: 0.09771043776850107
Kappa statistic: 0.7041264182766023
Training time: 6.0
		
Iteration: 9
Labeled set size: 660
Unlabelled set size: 1840
	
Mean region size: 38.94666666666767
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8399305205033625
Weighted AreaUnderROC: 0.9550108908903323
Root mean squared error: 0.3386578971700386
Relative absolute error: 30.260564645730806
Root relative squared error: 71.84018867739306
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7232523429826017
Weighted FMeasure: 0.7883907628407228
Iteration time: 178.0
Weighted AreaUnderPRC: 0.9162784736397496
Mean absolute error: 0.1344913984254709
Coverage of cases: 88.0
Instances selection time: 171.0
Test time: 129.0
Accumulative iteration time: 1684.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09790852389290296
Kappa statistic: 0.7035273418465108
Training time: 7.0
		
Iteration: 10
Labeled set size: 680
Unlabelled set size: 1820
	
Mean region size: 39.053333333334315
Incorrectly Classified Instances: 19.84
Correctly Classified Instances: 80.16
Weighted Precision: 0.8397871578245737
Weighted AreaUnderROC: 0.9547460743146085
Root mean squared error: 0.3398719636886455
Relative absolute error: 30.417867760240423
Root relative squared error: 72.0977310778286
Weighted TruePositiveRate: 0.8016
Weighted MatthewsCorrelation: 0.7227919162844463
Weighted FMeasure: 0.7877499916836732
Iteration time: 177.0
Weighted AreaUnderPRC: 0.9158100204918743
Mean absolute error: 0.13519052337884696
Coverage of cases: 87.92
Instances selection time: 170.0
Test time: 128.0
Accumulative iteration time: 1861.0
Weighted Recall: 0.8016
Weighted FalsePositiveRate: 0.09810732457659331
Kappa statistic: 0.7029308269639147
Training time: 7.0
		
Iteration: 11
Labeled set size: 700
Unlabelled set size: 1800
	
Mean region size: 38.90666666666767
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8401455024427797
Weighted AreaUnderROC: 0.9544211598908163
Root mean squared error: 0.3402589105389392
Relative absolute error: 30.45858026144117
Root relative squared error: 72.17981490036905
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7233648623139195
Weighted FMeasure: 0.7879774952219738
Iteration time: 174.0
Weighted AreaUnderPRC: 0.9152694354409344
Mean absolute error: 0.13537146782862805
Coverage of cases: 87.64
Instances selection time: 167.0
Test time: 130.0
Accumulative iteration time: 2035.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09790852389290296
Kappa statistic: 0.7035316034112912
Training time: 7.0
		
Iteration: 12
Labeled set size: 720
Unlabelled set size: 1780
	
Mean region size: 38.90666666666765
Incorrectly Classified Instances: 19.8
Correctly Classified Instances: 80.2
Weighted Precision: 0.8396319552394929
Weighted AreaUnderROC: 0.9542798991807478
Root mean squared error: 0.3405115626554036
Relative absolute error: 30.490152894974898
Root relative squared error: 72.23341050781899
Weighted TruePositiveRate: 0.802
Weighted MatthewsCorrelation: 0.7231575239753009
Weighted FMeasure: 0.7879414387533707
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9150989504991015
Mean absolute error: 0.13551179064433352
Coverage of cases: 87.6
Instances selection time: 165.0
Test time: 130.0
Accumulative iteration time: 2207.0
Weighted Recall: 0.802
Weighted FalsePositiveRate: 0.09791574724919212
Kappa statistic: 0.7035300408517636
Training time: 7.0
		
Iteration: 13
Labeled set size: 740
Unlabelled set size: 1760
	
Mean region size: 38.86666666666763
Incorrectly Classified Instances: 19.88
Correctly Classified Instances: 80.12
Weighted Precision: 0.8396690334257418
Weighted AreaUnderROC: 0.9538747324425629
Root mean squared error: 0.341441070045909
Relative absolute error: 30.596546543831586
Root relative squared error: 72.4305888015158
Weighted TruePositiveRate: 0.8012
Weighted MatthewsCorrelation: 0.7223196841274039
Weighted FMeasure: 0.7869806491443987
Iteration time: 172.0
Weighted AreaUnderPRC: 0.9143167955614683
Mean absolute error: 0.1359846513059188
Coverage of cases: 87.56
Instances selection time: 165.0
Test time: 128.0
Accumulative iteration time: 2379.0
Weighted Recall: 0.8012
Weighted FalsePositiveRate: 0.0983046961417068
Kappa statistic: 0.7023363185147529
Training time: 7.0
		
Iteration: 14
Labeled set size: 760
Unlabelled set size: 1740
	
Mean region size: 38.78666666666765
Incorrectly Classified Instances: 20.08
Correctly Classified Instances: 79.92
Weighted Precision: 0.8383168537068061
Weighted AreaUnderROC: 0.9536215990705332
Root mean squared error: 0.3420316329993206
Relative absolute error: 30.650751401240132
Root relative squared error: 72.55586612223826
Weighted TruePositiveRate: 0.7992
Weighted MatthewsCorrelation: 0.7196098197848547
Weighted FMeasure: 0.7847712592483622
Iteration time: 169.0
Weighted AreaUnderPRC: 0.9138985122692825
Mean absolute error: 0.13622556178329012
Coverage of cases: 87.48
Instances selection time: 162.0
Test time: 128.0
Accumulative iteration time: 2548.0
Weighted Recall: 0.7992
Weighted FalsePositiveRate: 0.09929441220442796
Kappa statistic: 0.6993456035463325
Training time: 7.0
		
Iteration: 15
Labeled set size: 780
Unlabelled set size: 1720
	
Mean region size: 38.813333333334306
Incorrectly Classified Instances: 20.08
Correctly Classified Instances: 79.92
Weighted Precision: 0.8383883655420897
Weighted AreaUnderROC: 0.9536435901294557
Root mean squared error: 0.342314262808563
Relative absolute error: 30.6874471879232
Root relative squared error: 72.61582095864249
Weighted TruePositiveRate: 0.7992
Weighted MatthewsCorrelation: 0.719646580072834
Weighted FMeasure: 0.7846294036186937
Iteration time: 168.0
Weighted AreaUnderPRC: 0.9140475595324926
Mean absolute error: 0.1363886541685482
Coverage of cases: 87.44
Instances selection time: 160.0
Test time: 127.0
Accumulative iteration time: 2716.0
Weighted Recall: 0.7992
Weighted FalsePositiveRate: 0.09929441220442796
Kappa statistic: 0.6993470440663804
Training time: 8.0
		
Iteration: 16
Labeled set size: 800
Unlabelled set size: 1700
	
Mean region size: 38.773333333334314
Incorrectly Classified Instances: 20.12
Correctly Classified Instances: 79.88
Weighted Precision: 0.8374618279569891
Weighted AreaUnderROC: 0.953649353186462
Root mean squared error: 0.34239511036105685
Relative absolute error: 30.69917602833807
Root relative squared error: 72.6329713144257
Weighted TruePositiveRate: 0.7988
Weighted MatthewsCorrelation: 0.7188163172492245
Weighted FMeasure: 0.7843827536760067
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9141092581591082
Mean absolute error: 0.13644078234816984
Coverage of cases: 87.44
Instances selection time: 158.0
Test time: 127.0
Accumulative iteration time: 2882.0
Weighted Recall: 0.7988
Weighted FalsePositiveRate: 0.09949900712583061
Kappa statistic: 0.6987452470025451
Training time: 8.0
		
Iteration: 17
Labeled set size: 820
Unlabelled set size: 1680
	
Mean region size: 38.82666666666766
Incorrectly Classified Instances: 20.16
Correctly Classified Instances: 79.84
Weighted Precision: 0.8377280797511998
Weighted AreaUnderROC: 0.9538442531927805
Root mean squared error: 0.342793956825288
Relative absolute error: 30.768114297905058
Root relative squared error: 72.71757942627875
Weighted TruePositiveRate: 0.7984
Weighted MatthewsCorrelation: 0.718518925474539
Weighted FMeasure: 0.7840484785313764
Iteration time: 166.0
Weighted AreaUnderPRC: 0.9142975307760327
Mean absolute error: 0.13674717465735645
Coverage of cases: 87.4
Instances selection time: 158.0
Test time: 128.0
Accumulative iteration time: 3048.0
Weighted Recall: 0.7984
Weighted FalsePositiveRate: 0.09969129901252019
Kappa statistic: 0.6981471987580913
Training time: 8.0
		
Iteration: 18
Labeled set size: 840
Unlabelled set size: 1660
	
Mean region size: 38.90666666666766
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8370969008580487
Weighted AreaUnderROC: 0.9537625810278265
Root mean squared error: 0.34324099466827523
Relative absolute error: 30.82009462457409
Root relative squared error: 72.81241047334574
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.7169911434686322
Weighted FMeasure: 0.782405754435539
Iteration time: 164.0
Weighted AreaUnderPRC: 0.9142423343651881
Mean absolute error: 0.13697819833144104
Coverage of cases: 87.44
Instances selection time: 155.0
Test time: 128.0
Accumulative iteration time: 3212.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10028555738572593
Kappa statistic: 0.6963558388556245
Training time: 9.0
		
Iteration: 19
Labeled set size: 860
Unlabelled set size: 1640
	
Mean region size: 38.853333333334334
Incorrectly Classified Instances: 20.32
Correctly Classified Instances: 79.68
Weighted Precision: 0.8369648863951302
Weighted AreaUnderROC: 0.9533918097432099
Root mean squared error: 0.34379119311323236
Relative absolute error: 30.884198264494547
Root relative squared error: 72.92912518877398
Weighted TruePositiveRate: 0.7968
Weighted MatthewsCorrelation: 0.7165345034480104
Weighted FMeasure: 0.7817551468633688
Iteration time: 161.0
Weighted AreaUnderPRC: 0.913536573406342
Mean absolute error: 0.1372631033977542
Coverage of cases: 87.28
Instances selection time: 152.0
Test time: 130.0
Accumulative iteration time: 3373.0
Weighted Recall: 0.7968
Weighted FalsePositiveRate: 0.10048435806941626
Kappa statistic: 0.6957594132516658
Training time: 9.0
		
Iteration: 20
Labeled set size: 880
Unlabelled set size: 1620
	
Mean region size: 38.94666666666765
Incorrectly Classified Instances: 20.28
Correctly Classified Instances: 79.72
Weighted Precision: 0.8371571303397419
Weighted AreaUnderROC: 0.9539767441244789
Root mean squared error: 0.343671642143664
Relative absolute error: 30.893576853233597
Root relative squared error: 72.90376459839023
Weighted TruePositiveRate: 0.7972
Weighted MatthewsCorrelation: 0.7170497530810468
Weighted FMeasure: 0.7823944682775631
Iteration time: 159.0
Weighted AreaUnderPRC: 0.9145162236239872
Mean absolute error: 0.13730478601437218
Coverage of cases: 87.32
Instances selection time: 150.0
Test time: 129.0
Accumulative iteration time: 3532.0
Weighted Recall: 0.7972
Weighted FalsePositiveRate: 0.10028698650430276
Kappa statistic: 0.6963552569229804
Training time: 9.0
		
Iteration: 21
Labeled set size: 900
Unlabelled set size: 1600
	
Mean region size: 38.973333333334296
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8364231707455969
Weighted AreaUnderROC: 0.9542319275807587
Root mean squared error: 0.344062949158541
Relative absolute error: 30.970533994208726
Root relative squared error: 72.98677335151385
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7154857255531075
Weighted FMeasure: 0.7810280184030457
Iteration time: 158.0
Weighted AreaUnderPRC: 0.9148679633890692
Mean absolute error: 0.13764681775203944
Coverage of cases: 87.24
Instances selection time: 149.0
Test time: 129.0
Accumulative iteration time: 3690.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10088195943679691
Kappa statistic: 0.6945607376154561
Training time: 9.0
		
Iteration: 22
Labeled set size: 920
Unlabelled set size: 1580
	
Mean region size: 39.00000000000097
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.8356606221577435
Weighted AreaUnderROC: 0.9541173777024087
Root mean squared error: 0.34472440768603946
Relative absolute error: 31.059308146432517
Root relative squared error: 73.12708989459418
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.7138919120626075
Weighted FMeasure: 0.7796664669418635
Iteration time: 156.0
Weighted AreaUnderPRC: 0.9146353868008313
Mean absolute error: 0.13804136953970073
Coverage of cases: 87.24
Instances selection time: 146.0
Test time: 129.0
Accumulative iteration time: 3846.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10147621781000264
Kappa statistic: 0.6927665385030003
Training time: 10.0
		
Iteration: 23
Labeled set size: 940
Unlabelled set size: 1560
	
Mean region size: 38.94666666666764
Incorrectly Classified Instances: 20.52
Correctly Classified Instances: 79.48
Weighted Precision: 0.8357322890779293
Weighted AreaUnderROC: 0.9537746406392343
Root mean squared error: 0.3450784784328897
Relative absolute error: 31.098583087823815
Root relative squared error: 73.20219964242946
Weighted TruePositiveRate: 0.7948
Weighted MatthewsCorrelation: 0.7139265086560119
Weighted FMeasure: 0.7795219131165653
Iteration time: 154.0
Weighted AreaUnderPRC: 0.914021809905048
Mean absolute error: 0.13821592483477316
Coverage of cases: 87.16
Instances selection time: 145.0
Test time: 130.0
Accumulative iteration time: 4000.0
Weighted Recall: 0.7948
Weighted FalsePositiveRate: 0.10147621781000264
Kappa statistic: 0.6927680105021835
Training time: 9.0
		
Iteration: 24
Labeled set size: 960
Unlabelled set size: 1540
	
Mean region size: 38.920000000000975
Incorrectly Classified Instances: 20.64
Correctly Classified Instances: 79.36
Weighted Precision: 0.8348644575199548
Weighted AreaUnderROC: 0.953517521396048
Root mean squared error: 0.3458013437085461
Relative absolute error: 31.19309781179674
Root relative squared error: 73.35554252391974
Weighted TruePositiveRate: 0.7936
Weighted MatthewsCorrelation: 0.7122643691648185
Weighted FMeasure: 0.7783094874757445
Iteration time: 153.0
Weighted AreaUnderPRC: 0.9136114086319136
Mean absolute error: 0.13863599027465281
Coverage of cases: 87.08
Instances selection time: 144.0
Test time: 130.0
Accumulative iteration time: 4153.0
Weighted Recall: 0.7936
Weighted FalsePositiveRate: 0.10206976162391998
Kappa statistic: 0.6909726647355253
Training time: 9.0
		
Iteration: 25
Labeled set size: 980
Unlabelled set size: 1520
	
Mean region size: 38.86666666666765
Incorrectly Classified Instances: 20.68
Correctly Classified Instances: 79.32
Weighted Precision: 0.8347448998134515
Weighted AreaUnderROC: 0.9532289391396703
Root mean squared error: 0.3462841307239206
Relative absolute error: 31.256819134171455
Root relative squared error: 73.45795711565178
Weighted TruePositiveRate: 0.7932
Weighted MatthewsCorrelation: 0.7118144136719188
Weighted FMeasure: 0.7776548055616268
Iteration time: 151.0
Weighted AreaUnderPRC: 0.9130935306719228
Mean absolute error: 0.13891919615187379
Coverage of cases: 86.92
Instances selection time: 142.0
Test time: 128.0
Accumulative iteration time: 4304.0
Weighted Recall: 0.7932
Weighted FalsePositiveRate: 0.10226856230761032
Kappa statistic: 0.6903762964070236
Training time: 9.0
		
Iteration: 26
Labeled set size: 1000
Unlabelled set size: 1500
	
Mean region size: 38.826666666667656
Incorrectly Classified Instances: 20.68
Correctly Classified Instances: 79.32
Weighted Precision: 0.8347789402286606
Weighted AreaUnderROC: 0.9529205382121388
Root mean squared error: 0.3465619398141941
Relative absolute error: 31.280645356197557
Root relative squared error: 73.51688932313407
Weighted TruePositiveRate: 0.7932
Weighted MatthewsCorrelation: 0.7118133727775073
Weighted FMeasure: 0.7775130276472718
Iteration time: 149.0
Weighted AreaUnderPRC: 0.9126240380027146
Mean absolute error: 0.1390250904719898
Coverage of cases: 86.8
Instances selection time: 140.0
Test time: 127.0
Accumulative iteration time: 4453.0
Weighted Recall: 0.7932
Weighted FalsePositiveRate: 0.1022678477483219
Kappa statistic: 0.6903780765160216
Training time: 9.0
		
Iteration: 27
Labeled set size: 1020
Unlabelled set size: 1480
	
Mean region size: 38.800000000001
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.8344420133537176
Weighted AreaUnderROC: 0.9526260467782065
Root mean squared error: 0.3469095814600613
Relative absolute error: 31.321318525876872
Root relative squared error: 73.59063525269873
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.7112252099570989
Weighted FMeasure: 0.7771609739526665
Iteration time: 148.0
Weighted AreaUnderPRC: 0.9121892279065491
Mean absolute error: 0.139205860115009
Coverage of cases: 86.72
Instances selection time: 138.0
Test time: 127.0
Accumulative iteration time: 4601.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10246521931343541
Kappa statistic: 0.6897793432808361
Training time: 10.0
		
Iteration: 28
Labeled set size: 1040
Unlabelled set size: 1460
	
Mean region size: 38.78666666666767
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.8345135962172955
Weighted AreaUnderROC: 0.9523470323991585
Root mean squared error: 0.3473329340316906
Relative absolute error: 31.369000434290047
Root relative squared error: 73.68044189496828
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.7112594657025831
Weighted FMeasure: 0.7770130534805119
Iteration time: 146.0
Weighted AreaUnderPRC: 0.911720064029982
Mean absolute error: 0.1394177797079564
Coverage of cases: 86.72
Instances selection time: 136.0
Test time: 127.0
Accumulative iteration time: 4747.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10246521931343541
Kappa statistic: 0.6897808295572033
Training time: 10.0
		
Iteration: 29
Labeled set size: 1060
Unlabelled set size: 1440
	
Mean region size: 38.786666666667664
Incorrectly Classified Instances: 20.72
Correctly Classified Instances: 79.28
Weighted Precision: 0.8345135962172955
Weighted AreaUnderROC: 0.9522005265567522
Root mean squared error: 0.34771643653495043
Relative absolute error: 31.430092031227353
Root relative squared error: 73.76179506116539
Weighted TruePositiveRate: 0.7928
Weighted MatthewsCorrelation: 0.7112594657025831
Weighted FMeasure: 0.7770130534805119
Iteration time: 145.0
Weighted AreaUnderPRC: 0.9113703366471334
Mean absolute error: 0.13968929791656667
Coverage of cases: 86.6
Instances selection time: 135.0
Test time: 127.0
Accumulative iteration time: 4892.0
Weighted Recall: 0.7928
Weighted FalsePositiveRate: 0.10246521931343541
Kappa statistic: 0.6897808295572033
Training time: 10.0
		
Iteration: 30
Labeled set size: 1080
Unlabelled set size: 1420
	
Mean region size: 38.78666666666766
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.834213334480182
Weighted AreaUnderROC: 0.9522977893184201
Root mean squared error: 0.34803685915425336
Relative absolute error: 31.490418779362034
Root relative squared error: 73.82976696325179
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.7107053109667846
Weighted FMeasure: 0.7766563561328741
Iteration time: 143.0
Weighted AreaUnderPRC: 0.9114916044823159
Mean absolute error: 0.13995741679716525
Coverage of cases: 86.56
Instances selection time: 132.0
Test time: 127.0
Accumulative iteration time: 5035.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10266330543783733
Kappa statistic: 0.6891818019358643
Training time: 11.0
		
Iteration: 31
Labeled set size: 1100
Unlabelled set size: 1400
	
Mean region size: 38.786666666667664
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.8339133214995783
Weighted AreaUnderROC: 0.9522000288125512
Root mean squared error: 0.34828183647124955
Relative absolute error: 31.52055747331704
Root relative squared error: 73.88173449987725
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.7101514189486557
Weighted FMeasure: 0.7762995748976432
Iteration time: 141.0
Weighted AreaUnderPRC: 0.911380351688998
Mean absolute error: 0.14009136654807638
Coverage of cases: 86.56
Instances selection time: 130.0
Test time: 130.0
Accumulative iteration time: 5176.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10286139156223921
Kappa statistic: 0.688582773740533
Training time: 11.0
		
Iteration: 32
Labeled set size: 1120
Unlabelled set size: 1380
	
Mean region size: 38.73333333333433
Incorrectly Classified Instances: 20.76
Correctly Classified Instances: 79.24
Weighted Precision: 0.8342506929231022
Weighted AreaUnderROC: 0.9521579506832917
Root mean squared error: 0.3485182231215239
Relative absolute error: 31.56073428734498
Root relative squared error: 73.93187968089455
Weighted TruePositiveRate: 0.7924
Weighted MatthewsCorrelation: 0.7107400427854668
Weighted FMeasure: 0.7766515387823862
Iteration time: 139.0
Weighted AreaUnderPRC: 0.9112725979159941
Mean absolute error: 0.14026993016597836
Coverage of cases: 86.48
Instances selection time: 127.0
Test time: 129.0
Accumulative iteration time: 5315.0
Weighted Recall: 0.7924
Weighted FalsePositiveRate: 0.10266401999712572
Kappa statistic: 0.6891815041075953
Training time: 12.0
		
Iteration: 33
Labeled set size: 1140
Unlabelled set size: 1360
	
Mean region size: 38.746666666667664
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.8340308404553171
Weighted AreaUnderROC: 0.9524631410968044
Root mean squared error: 0.3487435549717287
Relative absolute error: 31.606791217406787
Root relative squared error: 73.9796797846837
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.7102610185462499
Weighted FMeasure: 0.7762839449668051
Iteration time: 138.0
Weighted AreaUnderPRC: 0.9117958425049209
Mean absolute error: 0.1404746276329197
Coverage of cases: 86.48
Instances selection time: 126.0
Test time: 129.0
Accumulative iteration time: 5453.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10286353524010447
Kappa statistic: 0.6885818785316014
Training time: 12.0
		
Iteration: 34
Labeled set size: 1160
Unlabelled set size: 1340
	
Mean region size: 38.8133333333343
Incorrectly Classified Instances: 20.8
Correctly Classified Instances: 79.2
Weighted Precision: 0.8342388441254373
Weighted AreaUnderROC: 0.9523807099748419
Root mean squared error: 0.3477325227877588
Relative absolute error: 31.49840938112602
Root relative squared error: 73.7652074706988
Weighted TruePositiveRate: 0.792
Weighted MatthewsCorrelation: 0.7103848013725266
Weighted FMeasure: 0.776332071826537
Iteration time: 135.0
Weighted AreaUnderPRC: 0.9119198341004868
Mean absolute error: 0.13999293058278298
Coverage of cases: 86.44
Instances selection time: 123.0
Test time: 130.0
Accumulative iteration time: 5588.0
Weighted Recall: 0.792
Weighted FalsePositiveRate: 0.10286710803654653
Kappa statistic: 0.6885811325202267
Training time: 12.0
		
Iteration: 35
Labeled set size: 1180
Unlabelled set size: 1320
	
Mean region size: 39.00000000000093
Incorrectly Classified Instances: 20.4
Correctly Classified Instances: 79.6
Weighted Precision: 0.8363343190215399
Weighted AreaUnderROC: 0.9518036674566254
Root mean squared error: 0.3447795857465944
Relative absolute error: 31.124251106265405
Root relative squared error: 73.13879492883152
Weighted TruePositiveRate: 0.796
Weighted MatthewsCorrelation: 0.7155538527979619
Weighted FMeasure: 0.7814069534526383
Iteration time: 134.0
Weighted AreaUnderPRC: 0.9114573638210833
Mean absolute error: 0.13833000491673578
Coverage of cases: 86.68
Instances selection time: 123.0
Test time: 129.0
Accumulative iteration time: 5722.0
Weighted Recall: 0.796
Weighted FalsePositiveRate: 0.10089918662312386
Kappa statistic: 0.6945541520751392
Training time: 11.0
		
Iteration: 36
Labeled set size: 1200
Unlabelled set size: 1300
	
Mean region size: 39.14666666666758
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8413044613182344
Weighted AreaUnderROC: 0.9504970231698519
Root mean squared error: 0.34117319109255995
Relative absolute error: 30.63423838891838
Root relative squared error: 72.37376309418072
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7253532335295498
Weighted FMeasure: 0.7906522873931546
Iteration time: 132.0
Weighted AreaUnderPRC: 0.9097488233858472
Mean absolute error: 0.13615217061741566
Coverage of cases: 87.0
Instances selection time: 120.0
Test time: 130.0
Accumulative iteration time: 5854.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09734649845108084
Kappa statistic: 0.7053077665059995
Training time: 12.0
		
Iteration: 37
Labeled set size: 1220
Unlabelled set size: 1280
	
Mean region size: 39.34666666666756
Incorrectly Classified Instances: 19.68
Correctly Classified Instances: 80.32
Weighted Precision: 0.8413044613182344
Weighted AreaUnderROC: 0.9504826922532047
Root mean squared error: 0.34001399996319126
Relative absolute error: 30.494646805249648
Root relative squared error: 72.12786152170034
Weighted TruePositiveRate: 0.8032
Weighted MatthewsCorrelation: 0.7253532335295498
Weighted FMeasure: 0.7906522873931546
Iteration time: 132.0
Weighted AreaUnderPRC: 0.9099196462317864
Mean absolute error: 0.13553176357888796
Coverage of cases: 87.56
Instances selection time: 119.0
Test time: 127.0
Accumulative iteration time: 5986.0
Weighted Recall: 0.8032
Weighted FalsePositiveRate: 0.09734649845108084
Kappa statistic: 0.7053077665059995
Training time: 13.0
		
Iteration: 38
Labeled set size: 1240
Unlabelled set size: 1260
	
Mean region size: 39.453333333334236
Incorrectly Classified Instances: 19.4
Correctly Classified Instances: 80.6
Weighted Precision: 0.842158602545191
Weighted AreaUnderROC: 0.9501140000900398
Root mean squared error: 0.33813922121581036
Relative absolute error: 30.30303415223827
Root relative squared error: 71.73016089205112
Weighted TruePositiveRate: 0.806
Weighted MatthewsCorrelation: 0.7286705612519274
Weighted FMeasure: 0.7941989285131936
Iteration time: 129.0
Weighted AreaUnderPRC: 0.910039388229694
Mean absolute error: 0.13468015178772627
Coverage of cases: 87.72
Instances selection time: 116.0
Test time: 128.0
Accumulative iteration time: 6115.0
Weighted Recall: 0.806
Weighted FalsePositiveRate: 0.09597720052999942
Kappa statistic: 0.7094870391685747
Training time: 13.0
		
Iteration: 39
Labeled set size: 1260
Unlabelled set size: 1240
	
Mean region size: 39.44000000000089
Incorrectly Classified Instances: 19.44
Correctly Classified Instances: 80.56
Weighted Precision: 0.8419935301923293
Weighted AreaUnderROC: 0.950507677272344
Root mean squared error: 0.33758937896357705
Relative absolute error: 30.19417171907425
Root relative squared error: 71.61352173651
Weighted TruePositiveRate: 0.8056
Weighted MatthewsCorrelation: 0.728195336946611
Weighted FMeasure: 0.7938716286135996
Iteration time: 128.0
Weighted AreaUnderPRC: 0.9103160160279454
Mean absolute error: 0.13419631875144175
Coverage of cases: 87.88
Instances selection time: 115.0
Test time: 128.0
Accumulative iteration time: 6243.0
Weighted Recall: 0.8056
Weighted FalsePositiveRate: 0.09617671577297816
Kappa statistic: 0.708887624872833
Training time: 13.0
		
Iteration: 40
Labeled set size: 1280
Unlabelled set size: 1220
	
Mean region size: 39.4000000000009
Incorrectly Classified Instances: 19.6
Correctly Classified Instances: 80.4
Weighted Precision: 0.839895053286219
Weighted AreaUnderROC: 0.9513200466392416
Root mean squared error: 0.3372651967165275
Relative absolute error: 30.11414084122685
Root relative squared error: 71.5447522969413
Weighted TruePositiveRate: 0.804
Weighted MatthewsCorrelation: 0.7255213107299668
Weighted FMeasure: 0.7918919226317223
Iteration time: 126.0
Weighted AreaUnderPRC: 0.9111258195449873
Mean absolute error: 0.13384062596100885
Coverage of cases: 87.88
Instances selection time: 113.0
Test time: 128.0
Accumulative iteration time: 6369.0
Weighted Recall: 0.804
Weighted FalsePositiveRate: 0.09696842347470244
Kappa statistic: 0.7064955763492459
Training time: 13.0
		
Iteration: 41
Labeled set size: 1300
Unlabelled set size: 1200
	
Mean region size: 39.48000000000089
Incorrectly Classified Instances: 19.36
Correctly Classified Instances: 80.64
Weighted Precision: 0.8415335940011851
Weighted AreaUnderROC: 0.9523905607882723
Root mean squared error: 0.336218083768692
Relative absolute error: 29.97888537337843
Root relative squared error: 71.32262609711647
Weighted TruePositiveRate: 0.8064
Weighted MatthewsCorrelation: 0.7287672401658254
Weighted FMeasure: 0.7946994728345073
Iteration time: 125.0
Weighted AreaUnderPRC: 0.912910732336672
Mean absolute error: 0.1332394905483492
Coverage of cases: 88.08
Instances selection time: 112.0
Test time: 126.0
Accumulative iteration time: 6494.0
Weighted Recall: 0.8064
Weighted FalsePositiveRate: 0.09578205040615619
Kappa statistic: 0.7100828396355574
Training time: 13.0
		
Iteration: 42
Labeled set size: 1320
Unlabelled set size: 1180
	
Mean region size: 39.560000000000905
Incorrectly Classified Instances: 19.12
Correctly Classified Instances: 80.88
Weighted Precision: 0.8432848199650088
Weighted AreaUnderROC: 0.952519391271729
Root mean squared error: 0.3345555234853615
Relative absolute error: 29.82405467250604
Root relative squared error: 70.96994380197415
Weighted TruePositiveRate: 0.8088
Weighted MatthewsCorrelation: 0.7320609611887103
Weighted FMeasure: 0.7976757499097613
Iteration time: 123.0
Weighted AreaUnderPRC: 0.9136837714547715
Mean absolute error: 0.13255135410002747
Coverage of cases: 88.32
Instances selection time: 110.0
Test time: 128.0
Accumulative iteration time: 6617.0
Weighted Recall: 0.8088
Weighted FalsePositiveRate: 0.09459782101547519
Kappa statistic: 0.7136686213874923
Training time: 13.0
		
Iteration: 43
Labeled set size: 1340
Unlabelled set size: 1160
	
Mean region size: 39.56000000000088
Incorrectly Classified Instances: 19.0
Correctly Classified Instances: 81.0
Weighted Precision: 0.8441679147086241
Weighted AreaUnderROC: 0.9523082707273984
Root mean squared error: 0.3334576133550545
Relative absolute error: 29.66791695345265
Root relative squared error: 70.7370418924921
Weighted TruePositiveRate: 0.81
Weighted MatthewsCorrelation: 0.7337101718495542
Weighted FMeasure: 0.7991594116833736
Iteration time: 121.0
Weighted AreaUnderPRC: 0.9133773565305846
Mean absolute error: 0.1318574086820124
Coverage of cases: 88.44
Instances selection time: 108.0
Test time: 129.0
Accumulative iteration time: 6738.0
Weighted Recall: 0.81
Weighted FalsePositiveRate: 0.09400570632013469
Kappa statistic: 0.7154615895909022
Training time: 13.0
		
Iteration: 44
Labeled set size: 1360
Unlabelled set size: 1140
	
Mean region size: 39.72000000000088
Incorrectly Classified Instances: 18.92
Correctly Classified Instances: 81.08
Weighted Precision: 0.8442825668213689
Weighted AreaUnderROC: 0.9519919771135593
Root mean squared error: 0.3320261911520547
Relative absolute error: 29.504962044356848
Root relative squared error: 70.43339138854745
Weighted TruePositiveRate: 0.8108
Weighted MatthewsCorrelation: 0.7345999368927351
Weighted FMeasure: 0.8002436484676622
Iteration time: 119.0
Weighted AreaUnderPRC: 0.9131036661813876
Mean absolute error: 0.1311331646415866
Coverage of cases: 88.76
Instances selection time: 105.0
Test time: 129.0
Accumulative iteration time: 6857.0
Weighted Recall: 0.8108
Weighted FalsePositiveRate: 0.09361818654619683
Kappa statistic: 0.716654214627783
Training time: 14.0
		
Iteration: 45
Labeled set size: 1380
Unlabelled set size: 1120
	
Mean region size: 39.81333333333421
Incorrectly Classified Instances: 18.88
Correctly Classified Instances: 81.12
Weighted Precision: 0.8441135991239844
Weighted AreaUnderROC: 0.9523783221565202
Root mean squared error: 0.3308765696341328
Relative absolute error: 29.419590474101447
Root relative squared error: 70.1895198372113
Weighted TruePositiveRate: 0.8112
Weighted MatthewsCorrelation: 0.7349440614072844
Weighted FMeasure: 0.8008321467960734
Iteration time: 117.0
Weighted AreaUnderPRC: 0.914182889628486
Mean absolute error: 0.1307537354404515
Coverage of cases: 88.96
Instances selection time: 104.0
Test time: 129.0
Accumulative iteration time: 6974.0
Weighted Recall: 0.8112
Weighted FalsePositiveRate: 0.09342803833737251
Kappa statistic: 0.7172491892479932
Training time: 13.0
		
Iteration: 46
Labeled set size: 1400
Unlabelled set size: 1100
	
Mean region size: 39.906666666667526
Incorrectly Classified Instances: 18.8
Correctly Classified Instances: 81.2
Weighted Precision: 0.844462830160504
Weighted AreaUnderROC: 0.9529749287695849
Root mean squared error: 0.33031149781292446
Relative absolute error: 29.335832967246972
Root relative squared error: 70.06965000222115
Weighted TruePositiveRate: 0.812
Weighted MatthewsCorrelation: 0.7359062840617863
Weighted FMeasure: 0.8017534060672289
Iteration time: 116.0
Weighted AreaUnderPRC: 0.9149349234798286
Mean absolute error: 0.1303814798544316
Coverage of cases: 89.2
Instances selection time: 102.0
Test time: 129.0
Accumulative iteration time: 7090.0
Weighted Recall: 0.812
Weighted FalsePositiveRate: 0.09303043696999184
Kappa statistic: 0.7184451269920308
Training time: 14.0
		
Time end:Wed Nov 01 15.34.52 EET 2017