Wed Nov 01 01.14.22 EET 2017
Dataset: letter
Test set size: 10000
Initial Labelled set size: 2000
Initial Unlabelled set size: 8000
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 01.14.22 EET 2017
		
Iteration: 1
Labeled set size: 2000
Unlabelled set size: 8000
	
Mean region size: 11.159615384615867
Incorrectly Classified Instances: 37.86
Correctly Classified Instances: 62.14
Weighted Precision: 0.6329912040340411
Weighted AreaUnderROC: 0.9498678491443338
Root mean squared error: 0.14449254562988112
Relative absolute error: 44.843975340546336
Root relative squared error: 75.13612372754343
Weighted TruePositiveRate: 0.6214
Weighted MatthewsCorrelation: 0.6088940234444633
Weighted FMeasure: 0.6200636490341684
Iteration time: 107.0
Weighted AreaUnderPRC: 0.6646945441072593
Mean absolute error: 0.03316862081400894
Coverage of cases: 84.71
Instances selection time: 100.0
Test time: 2081.0
Accumulative iteration time: 107.0
Weighted Recall: 0.6214
Weighted FalsePositiveRate: 0.015095548722676117
Kappa statistic: 0.6062495538326956
Training time: 7.0
		
Iteration: 2
Labeled set size: 2020
Unlabelled set size: 7980
	
Mean region size: 11.253846153846585
Incorrectly Classified Instances: 38.23
Correctly Classified Instances: 61.77
Weighted Precision: 0.6284883355216606
Weighted AreaUnderROC: 0.9501913154004796
Root mean squared error: 0.1443951096916825
Relative absolute error: 44.95463480698862
Root relative squared error: 75.08545703968015
Weighted TruePositiveRate: 0.6177
Weighted MatthewsCorrelation: 0.6047898406100906
Weighted FMeasure: 0.6162902693459049
Iteration time: 110.0
Weighted AreaUnderPRC: 0.6650234754056124
Mean absolute error: 0.033250469531791685
Coverage of cases: 84.73
Instances selection time: 103.0
Test time: 2053.0
Accumulative iteration time: 217.0
Weighted Recall: 0.6177
Weighted FalsePositiveRate: 0.015246590713299833
Kappa statistic: 0.6024012220541126
Training time: 7.0
		
Iteration: 3
Labeled set size: 2040
Unlabelled set size: 7960
	
Mean region size: 11.244230769231205
Incorrectly Classified Instances: 37.91
Correctly Classified Instances: 62.09
Weighted Precision: 0.6316871842841116
Weighted AreaUnderROC: 0.9500928935792606
Root mean squared error: 0.14434815453405697
Relative absolute error: 44.898108108383354
Root relative squared error: 75.06104035771486
Weighted TruePositiveRate: 0.6209
Weighted MatthewsCorrelation: 0.6081868992380927
Weighted FMeasure: 0.6196504259984584
Iteration time: 107.0
Weighted AreaUnderPRC: 0.6656358544518013
Mean absolute error: 0.03320865984347419
Coverage of cases: 84.7
Instances selection time: 100.0
Test time: 2055.0
Accumulative iteration time: 324.0
Weighted Recall: 0.6209
Weighted FalsePositiveRate: 0.015118294497480912
Kappa statistic: 0.6057283110712312
Training time: 7.0
		
Iteration: 4
Labeled set size: 2060
Unlabelled set size: 7940
	
Mean region size: 11.204615384615835
Incorrectly Classified Instances: 38.08
Correctly Classified Instances: 61.92
Weighted Precision: 0.6295597089217307
Weighted AreaUnderROC: 0.9497183484532133
Root mean squared error: 0.1445780293759485
Relative absolute error: 44.92252103396116
Root relative squared error: 75.18057527549846
Weighted TruePositiveRate: 0.6192
Weighted MatthewsCorrelation: 0.6061468848158307
Weighted FMeasure: 0.6175991325180958
Iteration time: 106.0
Weighted AreaUnderPRC: 0.6642856931663489
Mean absolute error: 0.0332267167410909
Coverage of cases: 84.46
Instances selection time: 99.0
Test time: 2052.0
Accumulative iteration time: 430.0
Weighted Recall: 0.6192
Weighted FalsePositiveRate: 0.01518325584314207
Kappa statistic: 0.6039615699200492
Training time: 7.0
		
Iteration: 5
Labeled set size: 2080
Unlabelled set size: 7920
	
Mean region size: 11.240000000000423
Incorrectly Classified Instances: 37.9
Correctly Classified Instances: 62.1
Weighted Precision: 0.6310634005665275
Weighted AreaUnderROC: 0.9502992352131807
Root mean squared error: 0.14398260831601356
Relative absolute error: 44.762350177487164
Root relative squared error: 74.87095632433227
Weighted TruePositiveRate: 0.621
Weighted MatthewsCorrelation: 0.6079823711312522
Weighted FMeasure: 0.619496575777143
Iteration time: 108.0
Weighted AreaUnderPRC: 0.6678530460543739
Mean absolute error: 0.03310824717269299
Coverage of cases: 84.7
Instances selection time: 101.0
Test time: 2049.0
Accumulative iteration time: 538.0
Weighted Recall: 0.621
Weighted FalsePositiveRate: 0.015107810043299304
Kappa statistic: 0.6058336618052819
Training time: 7.0
		
Iteration: 6
Labeled set size: 2100
Unlabelled set size: 7900
	
Mean region size: 11.201538461538904
Incorrectly Classified Instances: 37.91
Correctly Classified Instances: 62.09
Weighted Precision: 0.6313285534925747
Weighted AreaUnderROC: 0.950158904183305
Root mean squared error: 0.14414732268298672
Relative absolute error: 44.789453785897855
Root relative squared error: 74.95660779515832
Weighted TruePositiveRate: 0.6209
Weighted MatthewsCorrelation: 0.6080590756622702
Weighted FMeasure: 0.6195771071924097
Iteration time: 106.0
Weighted AreaUnderPRC: 0.6673240448994505
Mean absolute error: 0.03312829422033403
Coverage of cases: 84.57
Instances selection time: 98.0
Test time: 2051.0
Accumulative iteration time: 644.0
Weighted Recall: 0.6209
Weighted FalsePositiveRate: 0.0151120862135696
Kappa statistic: 0.6057297134391348
Training time: 8.0
		
Iteration: 7
Labeled set size: 2120
Unlabelled set size: 7880
	
Mean region size: 11.171923076923559
Incorrectly Classified Instances: 37.85
Correctly Classified Instances: 62.15
Weighted Precision: 0.6324049270034116
Weighted AreaUnderROC: 0.9504857283359426
Root mean squared error: 0.14391223643481638
Relative absolute error: 44.69831805581754
Root relative squared error: 74.83436294610974
Weighted TruePositiveRate: 0.6215
Weighted MatthewsCorrelation: 0.6088987876331431
Weighted FMeasure: 0.6203769955056764
Iteration time: 106.0
Weighted AreaUnderPRC: 0.6686039159474713
Mean absolute error: 0.03306088613595511
Coverage of cases: 84.66
Instances selection time: 98.0
Test time: 2061.0
Accumulative iteration time: 750.0
Weighted Recall: 0.6215
Weighted FalsePositiveRate: 0.015094584258591419
Kappa statistic: 0.6063514757773971
Training time: 8.0
		
Iteration: 8
Labeled set size: 2140
Unlabelled set size: 7860
	
Mean region size: 11.16538461538509
Incorrectly Classified Instances: 37.87
Correctly Classified Instances: 62.13
Weighted Precision: 0.6324655052608731
Weighted AreaUnderROC: 0.9505902133609705
Root mean squared error: 0.14400311056118398
Relative absolute error: 44.719855889467816
Root relative squared error: 74.88161749182089
Weighted TruePositiveRate: 0.6213
Weighted MatthewsCorrelation: 0.6087142860713647
Weighted FMeasure: 0.6200781411677972
Iteration time: 105.0
Weighted AreaUnderPRC: 0.6680818762212146
Mean absolute error: 0.03307681648628815
Coverage of cases: 84.71
Instances selection time: 98.0
Test time: 2044.0
Accumulative iteration time: 855.0
Weighted Recall: 0.6213
Weighted FalsePositiveRate: 0.015108523883359643
Kappa statistic: 0.6061424922797636
Training time: 7.0
		
Iteration: 9
Labeled set size: 2160
Unlabelled set size: 7840
	
Mean region size: 11.15846153846202
Incorrectly Classified Instances: 37.8
Correctly Classified Instances: 62.2
Weighted Precision: 0.633020677860724
Weighted AreaUnderROC: 0.950539907688984
Root mean squared error: 0.14389911181408646
Relative absolute error: 44.665648095560435
Root relative squared error: 74.82753814333019
Weighted TruePositiveRate: 0.622
Weighted MatthewsCorrelation: 0.6093821710520342
Weighted FMeasure: 0.6207218835047507
Iteration time: 105.0
Weighted AreaUnderPRC: 0.6680513056770591
Mean absolute error: 0.033036721964167313
Coverage of cases: 84.83
Instances selection time: 98.0
Test time: 2046.0
Accumulative iteration time: 960.0
Weighted Recall: 0.622
Weighted FalsePositiveRate: 0.015077517491979919
Kappa statistic: 0.6068703095103813
Training time: 7.0
		
Iteration: 10
Labeled set size: 2180
Unlabelled set size: 7820
	
Mean region size: 11.166538461538945
Incorrectly Classified Instances: 37.61
Correctly Classified Instances: 62.39
Weighted Precision: 0.6350778558053229
Weighted AreaUnderROC: 0.9508237856434923
Root mean squared error: 0.14361496203456056
Relative absolute error: 44.562996461573924
Root relative squared error: 74.6797802579767
Weighted TruePositiveRate: 0.6239
Weighted MatthewsCorrelation: 0.6114468248679915
Weighted FMeasure: 0.6227219911750728
Iteration time: 105.0
Weighted AreaUnderPRC: 0.670030767375027
Mean absolute error: 0.032960796199384405
Coverage of cases: 84.97
Instances selection time: 97.0
Test time: 2059.0
Accumulative iteration time: 1065.0
Weighted Recall: 0.6239
Weighted FalsePositiveRate: 0.014996406679328061
Kappa statistic: 0.6088481563103089
Training time: 8.0
		
Iteration: 11
Labeled set size: 2200
Unlabelled set size: 7800
	
Mean region size: 11.144615384615879
Incorrectly Classified Instances: 37.71
Correctly Classified Instances: 62.29
Weighted Precision: 0.6340757803561456
Weighted AreaUnderROC: 0.950680693982138
Root mean squared error: 0.1437071464204127
Relative absolute error: 44.5749846517157
Root relative squared error: 74.72771613861983
Weighted TruePositiveRate: 0.6229
Weighted MatthewsCorrelation: 0.6101649869942406
Weighted FMeasure: 0.6211909422801843
Iteration time: 104.0
Weighted AreaUnderPRC: 0.6696033988684807
Mean absolute error: 0.032969663203927135
Coverage of cases: 84.98
Instances selection time: 96.0
Test time: 2055.0
Accumulative iteration time: 1169.0
Weighted Recall: 0.6229
Weighted FalsePositiveRate: 0.01503927387786127
Kappa statistic: 0.6078081232183468
Training time: 8.0
		
Iteration: 12
Labeled set size: 2220
Unlabelled set size: 7780
	
Mean region size: 11.150769230769725
Incorrectly Classified Instances: 37.57
Correctly Classified Instances: 62.43
Weighted Precision: 0.6352129441627227
Weighted AreaUnderROC: 0.9507144044876743
Root mean squared error: 0.14373463970892753
Relative absolute error: 44.598919759184376
Root relative squared error: 74.74201264864753
Weighted TruePositiveRate: 0.6243
Weighted MatthewsCorrelation: 0.6115361019622445
Weighted FMeasure: 0.622560431993402
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6691607162900943
Mean absolute error: 0.032987366685782664
Coverage of cases: 84.9
Instances selection time: 96.0
Test time: 2045.0
Accumulative iteration time: 1272.0
Weighted Recall: 0.6243
Weighted FalsePositiveRate: 0.014980708710003841
Kappa statistic: 0.6092644247312551
Training time: 7.0
		
Iteration: 13
Labeled set size: 2240
Unlabelled set size: 7760
	
Mean region size: 11.129230769231294
Incorrectly Classified Instances: 37.67
Correctly Classified Instances: 62.33
Weighted Precision: 0.6346347566640553
Weighted AreaUnderROC: 0.9506240658391231
Root mean squared error: 0.14392762851971527
Relative absolute error: 44.662343681540676
Root relative squared error: 74.84236683025716
Weighted TruePositiveRate: 0.6233
Weighted MatthewsCorrelation: 0.6105530619621649
Weighted FMeasure: 0.6214288654600422
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6690638294412594
Mean absolute error: 0.03303427787095743
Coverage of cases: 84.69
Instances selection time: 95.0
Test time: 2046.0
Accumulative iteration time: 1375.0
Weighted Recall: 0.6233
Weighted FalsePositiveRate: 0.015021388482610618
Kappa statistic: 0.6082241723189629
Training time: 8.0
		
Iteration: 14
Labeled set size: 2260
Unlabelled set size: 7740
	
Mean region size: 11.128076923077465
Incorrectly Classified Instances: 37.68
Correctly Classified Instances: 62.32
Weighted Precision: 0.6342356706246255
Weighted AreaUnderROC: 0.9509551589342585
Root mean squared error: 0.14382400493527694
Relative absolute error: 44.60023305143561
Root relative squared error: 74.78848256634923
Weighted TruePositiveRate: 0.6232
Weighted MatthewsCorrelation: 0.6103732262967662
Weighted FMeasure: 0.6213389317886719
Iteration time: 104.0
Weighted AreaUnderPRC: 0.670259439606703
Mean absolute error: 0.032988338055790975
Coverage of cases: 84.69
Instances selection time: 96.0
Test time: 2084.0
Accumulative iteration time: 1479.0
Weighted Recall: 0.6232
Weighted FalsePositiveRate: 0.015022914147569347
Kappa statistic: 0.608121144314547
Training time: 8.0
		
Iteration: 15
Labeled set size: 2280
Unlabelled set size: 7720
	
Mean region size: 11.077692307692875
Incorrectly Classified Instances: 37.8
Correctly Classified Instances: 62.2
Weighted Precision: 0.6342271331068711
Weighted AreaUnderROC: 0.9508595710955309
Root mean squared error: 0.14408431452167042
Relative absolute error: 44.68849798409093
Root relative squared error: 74.92384355127385
Weighted TruePositiveRate: 0.622
Weighted MatthewsCorrelation: 0.6095750110072019
Weighted FMeasure: 0.6204190022548542
Iteration time: 102.0
Weighted AreaUnderPRC: 0.6688446872357278
Mean absolute error: 0.03305362276929342
Coverage of cases: 84.62
Instances selection time: 94.0
Test time: 2043.0
Accumulative iteration time: 1581.0
Weighted Recall: 0.622
Weighted FalsePositiveRate: 0.015071696967888472
Kappa statistic: 0.6068733187334265
Training time: 8.0
		
Iteration: 16
Labeled set size: 2300
Unlabelled set size: 7700
	
Mean region size: 11.19384615384661
Incorrectly Classified Instances: 37.61
Correctly Classified Instances: 62.39
Weighted Precision: 0.6358017273687123
Weighted AreaUnderROC: 0.9509228297403495
Root mean squared error: 0.14391943548495928
Relative absolute error: 44.73406267452242
Root relative squared error: 74.83810645218405
Weighted TruePositiveRate: 0.6239
Weighted MatthewsCorrelation: 0.6113783051620262
Weighted FMeasure: 0.6221448110903328
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6687034901527305
Mean absolute error: 0.03308732446339954
Coverage of cases: 84.67
Instances selection time: 94.0
Test time: 2072.0
Accumulative iteration time: 1684.0
Weighted Recall: 0.6239
Weighted FalsePositiveRate: 0.015001745191714027
Kappa statistic: 0.6088469684329624
Training time: 9.0
		
Iteration: 17
Labeled set size: 2320
Unlabelled set size: 7680
	
Mean region size: 11.234615384615806
Incorrectly Classified Instances: 37.67
Correctly Classified Instances: 62.33
Weighted Precision: 0.6351575124627429
Weighted AreaUnderROC: 0.9510652054596647
Root mean squared error: 0.1438912196570542
Relative absolute error: 44.74831421981095
Root relative squared error: 74.82343422167341
Weighted TruePositiveRate: 0.6233
Weighted MatthewsCorrelation: 0.6106544130749542
Weighted FMeasure: 0.6213484491739056
Iteration time: 103.0
Weighted AreaUnderPRC: 0.6691091494437079
Mean absolute error: 0.03309786554719284
Coverage of cases: 84.66
Instances selection time: 95.0
Test time: 2048.0
Accumulative iteration time: 1787.0
Weighted Recall: 0.6233
Weighted FalsePositiveRate: 0.015027280604100602
Kappa statistic: 0.6082227095500451
Training time: 8.0
		
Iteration: 18
Labeled set size: 2340
Unlabelled set size: 7660
	
Mean region size: 11.195000000000492
Incorrectly Classified Instances: 37.66
Correctly Classified Instances: 62.34
Weighted Precision: 0.6347666548818109
Weighted AreaUnderROC: 0.9508128566109406
Root mean squared error: 0.14400809111902216
Relative absolute error: 44.76484899352793
Root relative squared error: 74.88420738189676
Weighted TruePositiveRate: 0.6234
Weighted MatthewsCorrelation: 0.6105519955654585
Weighted FMeasure: 0.6212926441302541
Iteration time: 102.0
Weighted AreaUnderPRC: 0.6688602494032109
Mean absolute error: 0.03311009540940953
Coverage of cases: 84.56
Instances selection time: 94.0
Test time: 2044.0
Accumulative iteration time: 1889.0
Weighted Recall: 0.6234
Weighted FalsePositiveRate: 0.015024552261274996
Kappa statistic: 0.6083253840512093
Training time: 8.0
		
Iteration: 19
Labeled set size: 2360
Unlabelled set size: 7640
	
Mean region size: 11.191923076923558
Incorrectly Classified Instances: 37.76
Correctly Classified Instances: 62.24
Weighted Precision: 0.63362343704808
Weighted AreaUnderROC: 0.9508762874470441
Root mean squared error: 0.14416358367487095
Relative absolute error: 44.80631703247804
Root relative squared error: 74.96506351093814
Weighted TruePositiveRate: 0.6224
Weighted MatthewsCorrelation: 0.609399484591627
Weighted FMeasure: 0.6201294875167479
Iteration time: 101.0
Weighted AreaUnderPRC: 0.6682188912642268
Mean absolute error: 0.03314076703585192
Coverage of cases: 84.48
Instances selection time: 92.0
Test time: 2055.0
Accumulative iteration time: 1990.0
Weighted Recall: 0.6224
Weighted FalsePositiveRate: 0.015068769597347069
Kappa statistic: 0.6072840039400978
Training time: 9.0
		
Iteration: 20
Labeled set size: 2380
Unlabelled set size: 7620
	
Mean region size: 11.154230769231292
Incorrectly Classified Instances: 37.8
Correctly Classified Instances: 62.2
Weighted Precision: 0.633226751760006
Weighted AreaUnderROC: 0.9508251049209204
Root mean squared error: 0.14433955787015276
Relative absolute error: 44.83851270922237
Root relative squared error: 75.05657009248468
Weighted TruePositiveRate: 0.622
Weighted MatthewsCorrelation: 0.608943472440548
Weighted FMeasure: 0.6196464889744321
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6675507752452635
Mean absolute error: 0.03316458040622494
Coverage of cases: 84.46
Instances selection time: 92.0
Test time: 2057.0
Accumulative iteration time: 2090.0
Weighted Recall: 0.622
Weighted FalsePositiveRate: 0.015093042141389077
Kappa statistic: 0.6068649982828425
Training time: 8.0
		
Iteration: 21
Labeled set size: 2400
Unlabelled set size: 7600
	
Mean region size: 11.157307692308201
Incorrectly Classified Instances: 37.91
Correctly Classified Instances: 62.09
Weighted Precision: 0.6322427271751032
Weighted AreaUnderROC: 0.9509264485011678
Root mean squared error: 0.1443761919267231
Relative absolute error: 44.822707489662065
Root relative squared error: 75.07561980190125
Weighted TruePositiveRate: 0.6209
Weighted MatthewsCorrelation: 0.607820000810031
Weighted FMeasure: 0.6185151318467269
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6676543380206189
Mean absolute error: 0.03315289015507087
Coverage of cases: 84.55
Instances selection time: 92.0
Test time: 2064.0
Accumulative iteration time: 2190.0
Weighted Recall: 0.6209
Weighted FalsePositiveRate: 0.015133401013027617
Kappa statistic: 0.6057222176258393
Training time: 8.0
		
Iteration: 22
Labeled set size: 2420
Unlabelled set size: 7580
	
Mean region size: 11.14461538461592
Incorrectly Classified Instances: 37.98
Correctly Classified Instances: 62.02
Weighted Precision: 0.6315211187645832
Weighted AreaUnderROC: 0.9508629058786761
Root mean squared error: 0.144340306796987
Relative absolute error: 44.80286690244152
Root relative squared error: 75.05695953443848
Weighted TruePositiveRate: 0.6202
Weighted MatthewsCorrelation: 0.6070596600169466
Weighted FMeasure: 0.6177521842256429
Iteration time: 99.0
Weighted AreaUnderPRC: 0.6678381676391101
Mean absolute error: 0.03313821516452314
Coverage of cases: 84.5
Instances selection time: 91.0
Test time: 2067.0
Accumulative iteration time: 2289.0
Weighted Recall: 0.6202
Weighted FalsePositiveRate: 0.015161323909217136
Kappa statistic: 0.6049939046024918
Training time: 8.0
		
Iteration: 23
Labeled set size: 2440
Unlabelled set size: 7560
	
Mean region size: 11.174230769231269
Incorrectly Classified Instances: 37.73
Correctly Classified Instances: 62.27
Weighted Precision: 0.6339208610193133
Weighted AreaUnderROC: 0.9513153401430684
Root mean squared error: 0.14398812955186885
Relative absolute error: 44.7279714686712
Root relative squared error: 74.87382736697703
Weighted TruePositiveRate: 0.6227
Weighted MatthewsCorrelation: 0.6097081356640733
Weighted FMeasure: 0.620423854000261
Iteration time: 101.0
Weighted AreaUnderPRC: 0.6696507614627997
Mean absolute error: 0.033082819133627935
Coverage of cases: 84.63
Instances selection time: 92.0
Test time: 2063.0
Accumulative iteration time: 2390.0
Weighted Recall: 0.6227
Weighted FalsePositiveRate: 0.015060103481548246
Kappa statistic: 0.6075940422770462
Training time: 9.0
		
Iteration: 24
Labeled set size: 2460
Unlabelled set size: 7540
	
Mean region size: 11.143461538462061
Incorrectly Classified Instances: 37.51
Correctly Classified Instances: 62.49
Weighted Precision: 0.6366900238396325
Weighted AreaUnderROC: 0.9514143978452603
Root mean squared error: 0.14378489875054026
Relative absolute error: 44.65621142521673
Root relative squared error: 74.76814735028616
Weighted TruePositiveRate: 0.6249
Weighted MatthewsCorrelation: 0.6123225079207102
Weighted FMeasure: 0.6230167370798998
Iteration time: 99.0
Weighted AreaUnderPRC: 0.6701011723805077
Mean absolute error: 0.03302974217841014
Coverage of cases: 84.53
Instances selection time: 90.0
Test time: 2075.0
Accumulative iteration time: 2489.0
Weighted Recall: 0.6249
Weighted FalsePositiveRate: 0.014973030023741722
Kappa statistic: 0.609882423908352
Training time: 9.0
		
Iteration: 25
Labeled set size: 2480
Unlabelled set size: 7520
	
Mean region size: 11.106153846154402
Incorrectly Classified Instances: 37.54
Correctly Classified Instances: 62.46
Weighted Precision: 0.6365909158298977
Weighted AreaUnderROC: 0.9514189110094691
Root mean squared error: 0.14389619755568359
Relative absolute error: 44.64458657005674
Root relative squared error: 74.82602272896068
Weighted TruePositiveRate: 0.6246
Weighted MatthewsCorrelation: 0.612146496959588
Weighted FMeasure: 0.6229013042354042
Iteration time: 100.0
Weighted AreaUnderPRC: 0.6698079792017652
Mean absolute error: 0.03302114391275926
Coverage of cases: 84.38
Instances selection time: 91.0
Test time: 2060.0
Accumulative iteration time: 2589.0
Weighted Recall: 0.6246
Weighted FalsePositiveRate: 0.014987509314229435
Kappa statistic: 0.6095695237749748
Training time: 9.0
		
Iteration: 26
Labeled set size: 2500
Unlabelled set size: 7500
	
Mean region size: 11.106923076923627
Incorrectly Classified Instances: 37.6
Correctly Classified Instances: 62.4
Weighted Precision: 0.6358480180183517
Weighted AreaUnderROC: 0.9516332960168163
Root mean squared error: 0.14401326846086956
Relative absolute error: 44.6963162874786
Root relative squared error: 74.8868995996574
Weighted TruePositiveRate: 0.624
Weighted MatthewsCorrelation: 0.6114548069061541
Weighted FMeasure: 0.6222348619039473
Iteration time: 99.0
Weighted AreaUnderPRC: 0.6695902485220171
Mean absolute error: 0.03305940553807128
Coverage of cases: 84.46
Instances selection time: 90.0
Test time: 2048.0
Accumulative iteration time: 2688.0
Weighted Recall: 0.624
Weighted FalsePositiveRate: 0.015011730154512373
Kappa statistic: 0.6089454965705768
Training time: 9.0
		
Iteration: 27
Labeled set size: 2520
Unlabelled set size: 7480
	
Mean region size: 11.113461538462072
Incorrectly Classified Instances: 37.6
Correctly Classified Instances: 62.4
Weighted Precision: 0.6361690433762753
Weighted AreaUnderROC: 0.9514129945998772
Root mean squared error: 0.144093395392745
Relative absolute error: 44.74234087969825
Root relative squared error: 74.92856560423262
Weighted TruePositiveRate: 0.624
Weighted MatthewsCorrelation: 0.6114916239880508
Weighted FMeasure: 0.6221170726753157
Iteration time: 97.0
Weighted AreaUnderPRC: 0.6685775208119116
Mean absolute error: 0.0330934473962219
Coverage of cases: 84.45
Instances selection time: 88.0
Test time: 2051.0
Accumulative iteration time: 2785.0
Weighted Recall: 0.624
Weighted FalsePositiveRate: 0.015010206152178341
Kappa statistic: 0.6089462083148749
Training time: 9.0
		
Iteration: 28
Labeled set size: 2540
Unlabelled set size: 7460
	
Mean region size: 11.105384615385162
Incorrectly Classified Instances: 37.52
Correctly Classified Instances: 62.48
Weighted Precision: 0.6374971300906664
Weighted AreaUnderROC: 0.9516146442668049
Root mean squared error: 0.14393078269103932
Relative absolute error: 44.68479587913881
Root relative squared error: 74.84400699934567
Weighted TruePositiveRate: 0.6248
Weighted MatthewsCorrelation: 0.6125242000839464
Weighted FMeasure: 0.6230425973294946
Iteration time: 98.0
Weighted AreaUnderPRC: 0.6693751228775906
Mean absolute error: 0.03305088452598564
Coverage of cases: 84.52
Instances selection time: 88.0
Test time: 2066.0
Accumulative iteration time: 2883.0
Weighted Recall: 0.6248
Weighted FalsePositiveRate: 0.01497966310076213
Kappa statistic: 0.6097774219088387
Training time: 10.0
		
Iteration: 29
Labeled set size: 2560
Unlabelled set size: 7440
	
Mean region size: 11.064230769231354
Incorrectly Classified Instances: 37.67
Correctly Classified Instances: 62.33
Weighted Precision: 0.6366686095274302
Weighted AreaUnderROC: 0.9514969110268088
Root mean squared error: 0.14431017222870754
Relative absolute error: 44.7670317914771
Root relative squared error: 75.04128955893316
Weighted TruePositiveRate: 0.6233
Weighted MatthewsCorrelation: 0.611191437040447
Weighted FMeasure: 0.6216398425079984
Iteration time: 98.0
Weighted AreaUnderPRC: 0.6682616460877236
Mean absolute error: 0.03311170990493407
Coverage of cases: 84.5
Instances selection time: 89.0
Test time: 2051.0
Accumulative iteration time: 2981.0
Weighted Recall: 0.6233
Weighted FalsePositiveRate: 0.01503670673454329
Kappa statistic: 0.6082184923138906
Training time: 9.0
		
Iteration: 30
Labeled set size: 2580
Unlabelled set size: 7420
	
Mean region size: 11.115769230769757
Incorrectly Classified Instances: 37.8
Correctly Classified Instances: 62.2
Weighted Precision: 0.6349666154518294
Weighted AreaUnderROC: 0.9516181583267309
Root mean squared error: 0.14430586160201261
Relative absolute error: 44.81501639754946
Root relative squared error: 75.0390480330518
Weighted TruePositiveRate: 0.622
Weighted MatthewsCorrelation: 0.60971053667373
Weighted FMeasure: 0.6202964910636103
Iteration time: 97.0
Weighted AreaUnderPRC: 0.668203775742054
Mean absolute error: 0.0331472014774728
Coverage of cases: 84.62
Instances selection time: 87.0
Test time: 2050.0
Accumulative iteration time: 3078.0
Weighted Recall: 0.622
Weighted FalsePositiveRate: 0.015088217102516234
Kappa statistic: 0.6068666664898607
Training time: 10.0
		
Iteration: 31
Labeled set size: 2600
Unlabelled set size: 7400
	
Mean region size: 11.120769230769753
Incorrectly Classified Instances: 37.89
Correctly Classified Instances: 62.11
Weighted Precision: 0.634146598009777
Weighted AreaUnderROC: 0.9518252731032497
Root mean squared error: 0.14436514465080577
Relative absolute error: 44.858529686650385
Root relative squared error: 75.06987521842424
Weighted TruePositiveRate: 0.6211
Weighted MatthewsCorrelation: 0.6087155441764768
Weighted FMeasure: 0.6192238554922075
Iteration time: 97.0
Weighted AreaUnderPRC: 0.6687727944855951
Mean absolute error: 0.033179385862902463
Coverage of cases: 84.68
Instances selection time: 87.0
Test time: 2059.0
Accumulative iteration time: 3175.0
Weighted Recall: 0.6211
Weighted FalsePositiveRate: 0.015122415654299151
Kappa statistic: 0.6059318028011603
Training time: 10.0
		
Iteration: 32
Labeled set size: 2620
Unlabelled set size: 7380
	
Mean region size: 11.147307692308196
Incorrectly Classified Instances: 37.97
Correctly Classified Instances: 62.03
Weighted Precision: 0.6343715883337204
Weighted AreaUnderROC: 0.951867726149888
Root mean squared error: 0.14448280893761675
Relative absolute error: 44.940427752672754
Root relative squared error: 75.13106064756596
Weighted TruePositiveRate: 0.6203
Weighted MatthewsCorrelation: 0.6082689585743434
Weighted FMeasure: 0.6186565498609217
Iteration time: 96.0
Weighted AreaUnderPRC: 0.667717782200832
Mean absolute error: 0.033239961355522554
Coverage of cases: 84.67
Instances selection time: 87.0
Test time: 2065.0
Accumulative iteration time: 3271.0
Weighted Recall: 0.6203
Weighted FalsePositiveRate: 0.015152575386241634
Kappa statistic: 0.6051009557794504
Training time: 9.0
		
Iteration: 33
Labeled set size: 2640
Unlabelled set size: 7360
	
Mean region size: 11.151923076923559
Incorrectly Classified Instances: 37.85
Correctly Classified Instances: 62.15
Weighted Precision: 0.6353721556893159
Weighted AreaUnderROC: 0.9519263368509805
Root mean squared error: 0.14436242178004827
Relative absolute error: 44.86998749305245
Root relative squared error: 75.06845932563034
Weighted TruePositiveRate: 0.6215
Weighted MatthewsCorrelation: 0.6093495873611826
Weighted FMeasure: 0.619606629425347
Iteration time: 98.0
Weighted AreaUnderPRC: 0.6683232606822958
Mean absolute error: 0.033187860571779734
Coverage of cases: 84.73
Instances selection time: 88.0
Test time: 2048.0
Accumulative iteration time: 3369.0
Weighted Recall: 0.6215
Weighted FalsePositiveRate: 0.015105582891766153
Kappa statistic: 0.6063486918150606
Training time: 10.0
		
Iteration: 34
Labeled set size: 2660
Unlabelled set size: 7340
	
Mean region size: 11.133846153846632
Incorrectly Classified Instances: 37.83
Correctly Classified Instances: 62.17
Weighted Precision: 0.6349897076958251
Weighted AreaUnderROC: 0.951844430581977
Root mean squared error: 0.1444317274799843
Relative absolute error: 44.854271795071384
Root relative squared error: 75.10449828959709
Weighted TruePositiveRate: 0.6217
Weighted MatthewsCorrelation: 0.6093650366227727
Weighted FMeasure: 0.6197336491496003
Iteration time: 95.0
Weighted AreaUnderPRC: 0.6676270183055812
Mean absolute error: 0.03317623653481149
Coverage of cases: 84.67
Instances selection time: 86.0
Test time: 2046.0
Accumulative iteration time: 3464.0
Weighted Recall: 0.6217
Weighted FalsePositiveRate: 0.015099102117381016
Kappa statistic: 0.6065559162305441
Training time: 9.0
		
Iteration: 35
Labeled set size: 2680
Unlabelled set size: 7320
	
Mean region size: 11.156153846154321
Incorrectly Classified Instances: 37.93
Correctly Classified Instances: 62.07
Weighted Precision: 0.6335954150153024
Weighted AreaUnderROC: 0.9518039219422925
Root mean squared error: 0.1444337824170327
Relative absolute error: 44.899154069397284
Root relative squared error: 75.10556685686224
Weighted TruePositiveRate: 0.6207
Weighted MatthewsCorrelation: 0.6082009255112627
Weighted FMeasure: 0.6186905318137559
Iteration time: 95.0
Weighted AreaUnderPRC: 0.6680705367696581
Mean absolute error: 0.03320943348327739
Coverage of cases: 84.71
Instances selection time: 86.0
Test time: 2047.0
Accumulative iteration time: 3559.0
Weighted Recall: 0.6207
Weighted FalsePositiveRate: 0.015133790828377116
Kappa statistic: 0.6055175253857481
Training time: 9.0
		
Iteration: 36
Labeled set size: 2700
Unlabelled set size: 7300
	
Mean region size: 11.191538461538887
Incorrectly Classified Instances: 38.01
Correctly Classified Instances: 61.99
Weighted Precision: 0.6328830470246116
Weighted AreaUnderROC: 0.9518417731497201
Root mean squared error: 0.14445326938720063
Relative absolute error: 44.959314232210495
Root relative squared error: 75.11570008134957
Weighted TruePositiveRate: 0.6199
Weighted MatthewsCorrelation: 0.6073452657862725
Weighted FMeasure: 0.6177903469004062
Iteration time: 94.0
Weighted AreaUnderPRC: 0.6674918264656955
Mean absolute error: 0.03325393064512147
Coverage of cases: 84.83
Instances selection time: 84.0
Test time: 2057.0
Accumulative iteration time: 3653.0
Weighted Recall: 0.6199
Weighted FalsePositiveRate: 0.015167481525874266
Kappa statistic: 0.6046853840631778
Training time: 10.0
		
Iteration: 37
Labeled set size: 2720
Unlabelled set size: 7280
	
Mean region size: 11.195384615385043
Incorrectly Classified Instances: 37.97
Correctly Classified Instances: 62.03
Weighted Precision: 0.6334353605115304
Weighted AreaUnderROC: 0.9518893561321662
Root mean squared error: 0.14436842809732994
Relative absolute error: 44.89755898529958
Root relative squared error: 75.07158261061682
Weighted TruePositiveRate: 0.6203
Weighted MatthewsCorrelation: 0.607851115341306
Weighted FMeasure: 0.6182949193250573
Iteration time: 95.0
Weighted AreaUnderPRC: 0.6681200163533776
Mean absolute error: 0.03320825368734714
Coverage of cases: 84.96
Instances selection time: 85.0
Test time: 2047.0
Accumulative iteration time: 3748.0
Weighted Recall: 0.6203
Weighted FalsePositiveRate: 0.015151527226095871
Kappa statistic: 0.6051015061253833
Training time: 10.0
		
Iteration: 38
Labeled set size: 2740
Unlabelled set size: 7260
	
Mean region size: 11.196153846154278
Incorrectly Classified Instances: 37.8
Correctly Classified Instances: 62.2
Weighted Precision: 0.6349267998613187
Weighted AreaUnderROC: 0.9518561722386089
Root mean squared error: 0.14433681387676195
Relative absolute error: 44.873100996637234
Root relative squared error: 75.05514321592145
Weighted TruePositiveRate: 0.622
Weighted MatthewsCorrelation: 0.609522026383472
Weighted FMeasure: 0.6199048154935225
Iteration time: 94.0
Weighted AreaUnderPRC: 0.6681845580395049
Mean absolute error: 0.03319016345904658
Coverage of cases: 84.85
Instances selection time: 84.0
Test time: 2055.0
Accumulative iteration time: 3842.0
Weighted Recall: 0.622
Weighted FalsePositiveRate: 0.015082247342761337
Kappa statistic: 0.6068697411872702
Training time: 10.0
		
Iteration: 39
Labeled set size: 2760
Unlabelled set size: 7240
	
Mean region size: 11.191923076923523
Incorrectly Classified Instances: 37.84
Correctly Classified Instances: 62.16
Weighted Precision: 0.6353834115706822
Weighted AreaUnderROC: 0.9518074191427637
Root mean squared error: 0.14439397312387145
Relative absolute error: 44.900588377659496
Root relative squared error: 75.08486602441839
Weighted TruePositiveRate: 0.6216
Weighted MatthewsCorrelation: 0.6092630702808064
Weighted FMeasure: 0.6193315074979194
Iteration time: 94.0
Weighted AreaUnderPRC: 0.6679574569843003
Mean absolute error: 0.03321049436216955
Coverage of cases: 84.89
Instances selection time: 84.0
Test time: 2057.0
Accumulative iteration time: 3936.0
Weighted Recall: 0.6216
Weighted FalsePositiveRate: 0.015099239901037044
Kappa statistic: 0.6064531573122278
Training time: 10.0
		
Iteration: 40
Labeled set size: 2780
Unlabelled set size: 7220
	
Mean region size: 11.241538461538832
Incorrectly Classified Instances: 37.95
Correctly Classified Instances: 62.05
Weighted Precision: 0.6338556025176273
Weighted AreaUnderROC: 0.9519282074918539
Root mean squared error: 0.14425007059747916
Relative absolute error: 44.92371149432797
Root relative squared error: 75.0100367106944
Weighted TruePositiveRate: 0.6205
Weighted MatthewsCorrelation: 0.6080712640315687
Weighted FMeasure: 0.6183866587028034
Iteration time: 92.0
Weighted AreaUnderPRC: 0.6687688133347756
Mean absolute error: 0.033227597259113686
Coverage of cases: 84.94
Instances selection time: 82.0
Test time: 2084.0
Accumulative iteration time: 4028.0
Weighted Recall: 0.6205
Weighted FalsePositiveRate: 0.015138749274353156
Kappa statistic: 0.6053113383205063
Training time: 10.0
		
Iteration: 41
Labeled set size: 2800
Unlabelled set size: 7200
	
Mean region size: 11.221923076923444
Incorrectly Classified Instances: 37.91
Correctly Classified Instances: 62.09
Weighted Precision: 0.634089691782504
Weighted AreaUnderROC: 0.9520571041798676
Root mean squared error: 0.14429651391807405
Relative absolute error: 44.89091908687667
Root relative squared error: 75.03418723740374
Weighted TruePositiveRate: 0.6209
Weighted MatthewsCorrelation: 0.6084318856312378
Weighted FMeasure: 0.6187522535660451
Iteration time: 92.0
Weighted AreaUnderPRC: 0.6684282698920899
Mean absolute error: 0.033203342519874564
Coverage of cases: 84.94
Instances selection time: 82.0
Test time: 2058.0
Accumulative iteration time: 4120.0
Weighted Recall: 0.6209
Weighted FalsePositiveRate: 0.015119733279344908
Kappa statistic: 0.6057280609402351
Training time: 10.0
		
Iteration: 42
Labeled set size: 2820
Unlabelled set size: 7180
	
Mean region size: 11.229230769231135
Incorrectly Classified Instances: 37.86
Correctly Classified Instances: 62.14
Weighted Precision: 0.6339274737784208
Weighted AreaUnderROC: 0.9520516114658382
Root mean squared error: 0.14429637899849007
Relative absolute error: 44.91367637075863
Root relative squared error: 75.03411707922008
Weighted TruePositiveRate: 0.6214
Weighted MatthewsCorrelation: 0.608757820658746
Weighted FMeasure: 0.6192205217645558
Iteration time: 92.0
Weighted AreaUnderPRC: 0.667795669768332
Mean absolute error: 0.03322017483043815
Coverage of cases: 84.93
Instances selection time: 82.0
Test time: 2051.0
Accumulative iteration time: 4212.0
Weighted Recall: 0.6214
Weighted FalsePositiveRate: 0.015097661516073382
Kappa statistic: 0.6062484727308034
Training time: 10.0
		
Iteration: 43
Labeled set size: 2840
Unlabelled set size: 7160
	
Mean region size: 11.256153846154213
Incorrectly Classified Instances: 37.83
Correctly Classified Instances: 62.17
Weighted Precision: 0.6340050462792821
Weighted AreaUnderROC: 0.952091480968057
Root mean squared error: 0.1441937792582375
Relative absolute error: 44.919624517487144
Root relative squared error: 74.98076521428874
Weighted TruePositiveRate: 0.6217
Weighted MatthewsCorrelation: 0.609107478129802
Weighted FMeasure: 0.6197371081374766
Iteration time: 92.0
Weighted AreaUnderPRC: 0.6682114150511035
Mean absolute error: 0.033224574347249174
Coverage of cases: 84.82
Instances selection time: 82.0
Test time: 2054.0
Accumulative iteration time: 4304.0
Weighted Recall: 0.6217
Weighted FalsePositiveRate: 0.015083047632346256
Kappa statistic: 0.6065610883799518
Training time: 10.0
		
Iteration: 44
Labeled set size: 2860
Unlabelled set size: 7140
	
Mean region size: 11.275000000000357
Incorrectly Classified Instances: 37.89
Correctly Classified Instances: 62.11
Weighted Precision: 0.6324186997783422
Weighted AreaUnderROC: 0.9521778018631414
Root mean squared error: 0.14409566936329923
Relative absolute error: 44.905097453950575
Root relative squared error: 74.92974806892083
Weighted TruePositiveRate: 0.6211
Weighted MatthewsCorrelation: 0.6081055405621766
Weighted FMeasure: 0.6188945766534097
Iteration time: 91.0
Weighted AreaUnderPRC: 0.6686381276550463
Mean absolute error: 0.03321382947776946
Coverage of cases: 84.85
Instances selection time: 81.0
Test time: 2054.0
Accumulative iteration time: 4395.0
Weighted Recall: 0.6211
Weighted FalsePositiveRate: 0.015106275590673561
Kappa statistic: 0.6059373520055032
Training time: 10.0
		
Iteration: 45
Labeled set size: 2880
Unlabelled set size: 7120
	
Mean region size: 11.278846153846503
Incorrectly Classified Instances: 37.89
Correctly Classified Instances: 62.11
Weighted Precision: 0.6324416379425671
Weighted AreaUnderROC: 0.9521911215903323
Root mean squared error: 0.14413182603588123
Relative absolute error: 44.92883790346166
Root relative squared error: 74.94854953866347
Weighted TruePositiveRate: 0.6211
Weighted MatthewsCorrelation: 0.6081272928548842
Weighted FMeasure: 0.6189342857473371
Iteration time: 92.0
Weighted AreaUnderPRC: 0.6688055663931766
Mean absolute error: 0.03323138898184571
Coverage of cases: 84.78
Instances selection time: 81.0
Test time: 2053.0
Accumulative iteration time: 4487.0
Weighted Recall: 0.6211
Weighted FalsePositiveRate: 0.01510716246105712
Kappa statistic: 0.6059374872500393
Training time: 11.0
		
Iteration: 46
Labeled set size: 2900
Unlabelled set size: 7100
	
Mean region size: 11.2992307692311
Incorrectly Classified Instances: 37.75
Correctly Classified Instances: 62.25
Weighted Precision: 0.634290857346413
Weighted AreaUnderROC: 0.9521171797303678
Root mean squared error: 0.14397160544054255
Relative absolute error: 44.87229091474785
Root relative squared error: 74.86523482908736
Weighted TruePositiveRate: 0.6225
Weighted MatthewsCorrelation: 0.6097576210804031
Weighted FMeasure: 0.6204597385524913
Iteration time: 92.0
Weighted AreaUnderPRC: 0.669407526108059
Mean absolute error: 0.03318956428605148
Coverage of cases: 84.79
Instances selection time: 82.0
Test time: 2061.0
Accumulative iteration time: 4579.0
Weighted Recall: 0.6225
Weighted FalsePositiveRate: 0.015057468274974301
Kappa statistic: 0.6073906408210522
Training time: 10.0
		
Time end:Wed Nov 01 01.16.20 EET 2017