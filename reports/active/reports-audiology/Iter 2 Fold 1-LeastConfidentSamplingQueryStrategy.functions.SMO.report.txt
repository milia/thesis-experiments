Tue Oct 31 11.16.49 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.49 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 50.0
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.4563744081703477
Weighted AreaUnderROC: 0.8113860723653782
Root mean squared error: 0.19335334915351346
Relative absolute error: 94.4715000274834
Root relative squared error: 96.76070473132282
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3829648383799821
Weighted FMeasure: 0.4444510852112047
Iteration time: 1105.0
Weighted AreaUnderPRC: 0.3975161036184053
Mean absolute error: 0.07544598960528172
Coverage of cases: 90.26548672566372
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 1105.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.09550526453402272
Kappa statistic: 0.3973051370824366
Training time: 1101.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 54.166666666666586
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.5200682759024768
Weighted AreaUnderROC: 0.835736122428016
Root mean squared error: 0.19407843650648451
Relative absolute error: 95.39333310014364
Root relative squared error: 97.12356352623071
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.4744611878123897
Weighted FMeasure: 0.5250118584477953
Iteration time: 633.0
Weighted AreaUnderPRC: 0.4659597187005705
Mean absolute error: 0.07618217573969788
Coverage of cases: 90.26548672566372
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 1738.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.07135167622935604
Kappa statistic: 0.48411441349110224
Training time: 629.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 58.333333333333414
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.5584201152632645
Weighted AreaUnderROC: 0.8652085090974037
Root mean squared error: 0.19409895648412645
Relative absolute error: 95.62139284340144
Root relative squared error: 97.13383243290544
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.5238113482363548
Weighted FMeasure: 0.5614199479399714
Iteration time: 758.0
Weighted AreaUnderPRC: 0.5197550833690315
Mean absolute error: 0.0763643067846607
Coverage of cases: 91.15044247787611
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 2496.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.0679594816507149
Kappa statistic: 0.5321791494166352
Training time: 754.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 58.333333333333414
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.613189622007694
Weighted AreaUnderROC: 0.8771409454736556
Root mean squared error: 0.19403723910575435
Relative absolute error: 95.59061177375926
Root relative squared error: 97.10294692173329
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5677302456284232
Weighted FMeasure: 0.5965680492462048
Iteration time: 769.0
Weighted AreaUnderPRC: 0.5575351304776612
Mean absolute error: 0.07633972468043258
Coverage of cases: 91.15044247787611
Instances selection time: 3.0
Test time: 9.0
Accumulative iteration time: 3265.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.06671878432358402
Kappa statistic: 0.5735044347990187
Training time: 766.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 66.70353982300885
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6297504118300579
Weighted AreaUnderROC: 0.9035131580297695
Root mean squared error: 0.19396558177506074
Relative absolute error: 96.01924701366141
Root relative squared error: 97.06708711455934
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.608683749343093
Weighted FMeasure: 0.6422606096119025
Iteration time: 1277.0
Weighted AreaUnderPRC: 0.6101539918262843
Mean absolute error: 0.07668203754563221
Coverage of cases: 93.80530973451327
Instances selection time: 2.0
Test time: 12.0
Accumulative iteration time: 4542.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.04918030457839184
Kappa statistic: 0.6241337891527302
Training time: 1275.0
		
Time end:Tue Oct 31 11.16.54 EET 2017