Tue Oct 31 11.18.42 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.42 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 20.28023598820058
Incorrectly Classified Instances: 60.176991150442475
Correctly Classified Instances: 39.823008849557525
Weighted Precision: 0.36651384071998344
Weighted AreaUnderROC: 0.7630898093869011
Root mean squared error: 0.17822250730545847
Relative absolute error: 73.24014386137517
Root relative squared error: 89.1887080381929
Weighted TruePositiveRate: 0.39823008849557523
Weighted MatthewsCorrelation: 0.2901301831467641
Weighted FMeasure: 0.34461549355072063
Iteration time: 6.0
Weighted AreaUnderPRC: 0.4342242800080814
Mean absolute error: 0.058490392667070394
Coverage of cases: 77.87610619469027
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 6.0
Weighted Recall: 0.39823008849557523
Weighted FalsePositiveRate: 0.0869139422060787
Kappa statistic: 0.29800840489676594
Training time: 5.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 19.505899705014745
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.461149050258496
Weighted AreaUnderROC: 0.8579755484723052
Root mean squared error: 0.16330620801285728
Relative absolute error: 65.70919460575455
Root relative squared error: 81.72407586164084
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.38759383634902084
Weighted FMeasure: 0.45007201422189186
Iteration time: 6.0
Weighted AreaUnderPRC: 0.6165042782211125
Mean absolute error: 0.05247609291431781
Coverage of cases: 85.84070796460178
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.10861595500393695
Kappa statistic: 0.4047596651302794
Training time: 5.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 18.252212389380542
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.4403233697100039
Weighted AreaUnderROC: 0.8453761666424087
Root mean squared error: 0.16432931269794487
Relative absolute error: 64.05660782953652
Root relative squared error: 82.23607283907305
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3751185209831766
Weighted FMeasure: 0.42989204443216883
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6010578419080207
Mean absolute error: 0.0511563187527548
Coverage of cases: 88.49557522123894
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.10038911455791125
Kappa statistic: 0.3978685612788632
Training time: 7.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 18.58407079646018
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.6402233459755584
Weighted AreaUnderROC: 0.9009331685335573
Root mean squared error: 0.14502524144582069
Relative absolute error: 56.99080593831911
Root relative squared error: 72.57564778454658
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5954697638180995
Weighted FMeasure: 0.6417908436178491
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7295351447301578
Mean absolute error: 0.04551349085351869
Coverage of cases: 90.26548672566372
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 30.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.05534004275362335
Kappa statistic: 0.6056570851317844
Training time: 9.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 18.436578171091444
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.7466099329304636
Weighted AreaUnderROC: 0.9503535519037579
Root mean squared error: 0.1325209406677753
Relative absolute error: 52.930872898219086
Root relative squared error: 66.31806310472045
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.7207440825456922
Weighted FMeasure: 0.749721858420513
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8252098226374865
Mean absolute error: 0.042271183217327694
Coverage of cases: 96.46017699115045
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 44.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.03637898420845552
Kappa statistic: 0.7304092494035602
Training time: 13.0
		
Time end:Tue Oct 31 11.18.42 EET 2017