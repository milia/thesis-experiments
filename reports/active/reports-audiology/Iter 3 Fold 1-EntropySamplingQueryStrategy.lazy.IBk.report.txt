Tue Oct 31 11.15.13 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.13 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.23303834808269
Incorrectly Classified Instances: 49.557522123893804
Correctly Classified Instances: 50.442477876106196
Weighted Precision: 0.39981732680847726
Weighted AreaUnderROC: 0.7592329340604417
Root mean squared error: 0.1725059511861159
Relative absolute error: 73.34897034913476
Root relative squared error: 86.32794559903505
Weighted TruePositiveRate: 0.504424778761062
Weighted MatthewsCorrelation: 0.3714232959284068
Weighted FMeasure: 0.4165037570719918
Iteration time: 6.0
Weighted AreaUnderPRC: 0.396898481623544
Mean absolute error: 0.05857730270937839
Coverage of cases: 93.80530973451327
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 6.0
Weighted Recall: 0.504424778761062
Weighted FalsePositiveRate: 0.08809916906363491
Kappa statistic: 0.40565417488494415
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 79.38790560471978
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5555225257312986
Weighted AreaUnderROC: 0.7997873032087353
Root mean squared error: 0.1620835339491503
Relative absolute error: 59.874035311373326
Root relative squared error: 81.11220746329688
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.49116758058241
Weighted FMeasure: 0.5257888392077175
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5067262380547972
Mean absolute error: 0.0478160698667217
Coverage of cases: 87.61061946902655
Instances selection time: 6.0
Test time: 7.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08690617291854943
Kappa statistic: 0.5057055914796501
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 66.55604719764011
Incorrectly Classified Instances: 34.51327433628319
Correctly Classified Instances: 65.48672566371681
Weighted Precision: 0.5861814736523633
Weighted AreaUnderROC: 0.8446273765682947
Root mean squared error: 0.15345580156297509
Relative absolute error: 50.48967435002142
Root relative squared error: 76.79459171174992
Weighted TruePositiveRate: 0.6548672566371682
Weighted MatthewsCorrelation: 0.5635023780507137
Weighted FMeasure: 0.6021212664063152
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5779857131803804
Mean absolute error: 0.04032161493230872
Coverage of cases: 92.92035398230088
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 16.0
Weighted Recall: 0.6548672566371682
Weighted FalsePositiveRate: 0.06514053525020765
Kappa statistic: 0.5866241440765407
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 59.439528023598776
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.6991510479225213
Weighted AreaUnderROC: 0.8804981801082148
Root mean squared error: 0.13808600036481336
Relative absolute error: 42.42065371214965
Root relative squared error: 69.10301149332972
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6570353538457349
Weighted FMeasure: 0.685795718991755
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6685827686020148
Mean absolute error: 0.03387760539511947
Coverage of cases: 92.92035398230088
Instances selection time: 2.0
Test time: 19.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.04811393220379453
Kappa statistic: 0.6650921552283042
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 54.86725663716813
Incorrectly Classified Instances: 25.663716814159294
Correctly Classified Instances: 74.33628318584071
Weighted Precision: 0.7104842674532941
Weighted AreaUnderROC: 0.8937746138875108
Root mean squared error: 0.12950876929650584
Relative absolute error: 37.43754118282601
Root relative squared error: 64.81066834827305
Weighted TruePositiveRate: 0.7433628318584071
Weighted MatthewsCorrelation: 0.6851031976905997
Weighted FMeasure: 0.7142339719656906
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7381203148561919
Mean absolute error: 0.029898036361284627
Coverage of cases: 93.80530973451327
Instances selection time: 2.0
Test time: 30.0
Accumulative iteration time: 21.0
Weighted Recall: 0.7433628318584071
Weighted FalsePositiveRate: 0.04758702343708789
Kappa statistic: 0.6951344311098707
Training time: 0.0
		
Time end:Tue Oct 31 11.15.13 EET 2017