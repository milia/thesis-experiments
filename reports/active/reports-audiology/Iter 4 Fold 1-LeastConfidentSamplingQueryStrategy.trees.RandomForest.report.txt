Tue Oct 31 11.17.37 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.37 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 20.79646017699115
Incorrectly Classified Instances: 53.097345132743364
Correctly Classified Instances: 46.902654867256636
Weighted Precision: 0.5646419951729685
Weighted AreaUnderROC: 0.831453769511679
Root mean squared error: 0.1692621436581119
Relative absolute error: 69.50393297748926
Root relative squared error: 84.70463209659746
Weighted TruePositiveRate: 0.4690265486725664
Weighted MatthewsCorrelation: 0.39205078985255154
Weighted FMeasure: 0.4121355439330482
Iteration time: 4.0
Weighted AreaUnderPRC: 0.566634234469397
Mean absolute error: 0.05550661314174485
Coverage of cases: 83.1858407079646
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 4.0
Weighted Recall: 0.4690265486725664
Weighted FalsePositiveRate: 0.11642553249040029
Kappa statistic: 0.35237367465851566
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 21.49705014749263
Incorrectly Classified Instances: 38.93805309734513
Correctly Classified Instances: 61.06194690265487
Weighted Precision: 0.6140018605538726
Weighted AreaUnderROC: 0.8825454119227862
Root mean squared error: 0.15706323205498207
Relative absolute error: 64.96583702639063
Root relative squared error: 78.59987472445187
Weighted TruePositiveRate: 0.6106194690265486
Weighted MatthewsCorrelation: 0.530263795497695
Weighted FMeasure: 0.5610996337523425
Iteration time: 7.0
Weighted AreaUnderPRC: 0.6829548245270507
Mean absolute error: 0.051882439291909145
Coverage of cases: 88.49557522123894
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6106194690265486
Weighted FalsePositiveRate: 0.09531539575816607
Kappa statistic: 0.5226116178588573
Training time: 6.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 21.607669616519168
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.5834365614042147
Weighted AreaUnderROC: 0.8590373139234627
Root mean squared error: 0.15915449608046572
Relative absolute error: 65.92680610950313
Root relative squared error: 79.64641558744152
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.48318774865714176
Weighted FMeasure: 0.5299309316301253
Iteration time: 11.0
Weighted AreaUnderPRC: 0.6439039822119209
Mean absolute error: 0.05264987987911704
Coverage of cases: 87.61061946902655
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 22.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.08647423778812123
Kappa statistic: 0.4809711286089239
Training time: 10.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 19.24778761061947
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6986428724202361
Weighted AreaUnderROC: 0.900412071071664
Root mean squared error: 0.14378638798275523
Relative absolute error: 57.52205070504141
Root relative squared error: 71.9556826550578
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6427880804669401
Weighted FMeasure: 0.6832252051627925
Iteration time: 11.0
Weighted AreaUnderPRC: 0.7384144616520768
Mean absolute error: 0.04593774882694276
Coverage of cases: 89.38053097345133
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.05116652765780656
Kappa statistic: 0.6492605440934818
Training time: 10.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 16.88790560471976
Incorrectly Classified Instances: 27.43362831858407
Correctly Classified Instances: 72.56637168141593
Weighted Precision: 0.6807739580969668
Weighted AreaUnderROC: 0.9179435752281294
Root mean squared error: 0.13321022394830337
Relative absolute error: 50.04285731901109
Root relative squared error: 66.66300430318122
Weighted TruePositiveRate: 0.7256637168141593
Weighted MatthewsCorrelation: 0.6605752573099516
Weighted FMeasure: 0.6886469358381633
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8032665860134968
Mean absolute error: 0.03996478188671022
Coverage of cases: 92.03539823008849
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7256637168141593
Weighted FalsePositiveRate: 0.042069919552249424
Kappa statistic: 0.6759481961147086
Training time: 13.0
		
Time end:Tue Oct 31 11.17.37 EET 2017