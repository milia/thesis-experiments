Tue Oct 31 11.21.24 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.24 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 15.671091445427722
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.2632835024915421
Weighted AreaUnderROC: 0.7201824930834073
Root mean squared error: 0.18738197138455553
Relative absolute error: 67.36390453204608
Root relative squared error: 93.77242072346426
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.267321909234297
Weighted FMeasure: 0.32633253339646684
Iteration time: 4.0
Weighted AreaUnderPRC: 0.29310439995959586
Mean absolute error: 0.053797562647120205
Coverage of cases: 73.45132743362832
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.1494942535361661
Kappa statistic: 0.29644506929102227
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 11.910029498525082
Incorrectly Classified Instances: 47.78761061946903
Correctly Classified Instances: 52.21238938053097
Weighted Precision: 0.3962350566682192
Weighted AreaUnderROC: 0.7776991446977631
Root mean squared error: 0.18458594808769838
Relative absolute error: 59.06413709414848
Root relative squared error: 92.37319394082243
Weighted TruePositiveRate: 0.5221238938053098
Weighted MatthewsCorrelation: 0.394241015731118
Weighted FMeasure: 0.43125322830967333
Iteration time: 5.0
Weighted AreaUnderPRC: 0.38276993910434237
Mean absolute error: 0.04716927615157698
Coverage of cases: 70.79646017699115
Instances selection time: 3.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.5221238938053098
Weighted FalsePositiveRate: 0.07816702383730006
Kappa statistic: 0.4290793413173653
Training time: 2.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 12.278761061946913
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.451887570740142
Weighted AreaUnderROC: 0.8296764175459691
Root mean squared error: 0.16548153018869577
Relative absolute error: 52.65930683398924
Root relative squared error: 82.81268233095358
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4676001451684196
Weighted FMeasure: 0.4972668766859477
Iteration time: 3.0
Weighted AreaUnderPRC: 0.45801623655997414
Mean absolute error: 0.04205430754103314
Coverage of cases: 81.41592920353982
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.0689723184957545
Kappa statistic: 0.5132958801498128
Training time: 3.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 9.587020648967552
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.5297890564240662
Weighted AreaUnderROC: 0.8708716128908245
Root mean squared error: 0.14941842276826706
Relative absolute error: 39.93666202977474
Root relative squared error: 74.77414769485789
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.548623613274029
Weighted FMeasure: 0.5797936576697639
Iteration time: 3.0
Weighted AreaUnderPRC: 0.5547450410648356
Mean absolute error: 0.03189386203766737
Coverage of cases: 77.87610619469027
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.06268823741699792
Kappa statistic: 0.5986165638437091
Training time: 3.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 10.10324483775812
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.6937763486319609
Weighted AreaUnderROC: 0.9026140871932825
Root mean squared error: 0.12840855355812503
Relative absolute error: 33.390597288870765
Root relative squared error: 64.2600823322134
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.6976821529333601
Weighted FMeasure: 0.7206417830227289
Iteration time: 4.0
Weighted AreaUnderPRC: 0.673225826796274
Mean absolute error: 0.026666102001528778
Coverage of cases: 88.49557522123894
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.034253166441685745
Kappa statistic: 0.729863920559029
Training time: 3.0
		
Time end:Tue Oct 31 11.21.24 EET 2017