Tue Oct 31 11.21.24 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.24 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 12.094395280235993
Incorrectly Classified Instances: 51.32743362831859
Correctly Classified Instances: 48.67256637168141
Weighted Precision: 0.25326012806217746
Weighted AreaUnderROC: 0.764844361394617
Root mean squared error: 0.17764677520687408
Relative absolute error: 63.23644292093702
Root relative squared error: 88.90059177933651
Weighted TruePositiveRate: 0.48672566371681414
Weighted MatthewsCorrelation: 0.2990727411266837
Weighted FMeasure: 0.3301033121859889
Iteration time: 2.0
Weighted AreaUnderPRC: 0.3138596013409896
Mean absolute error: 0.050501325943803764
Coverage of cases: 78.76106194690266
Instances selection time: 1.0
Test time: 8.0
Accumulative iteration time: 2.0
Weighted Recall: 0.48672566371681414
Weighted FalsePositiveRate: 0.1309066759835908
Kappa statistic: 0.3508963058334158
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 10.435103244837764
Incorrectly Classified Instances: 51.32743362831859
Correctly Classified Instances: 48.67256637168141
Weighted Precision: 0.2622041830239315
Weighted AreaUnderROC: 0.7700524617194615
Root mean squared error: 0.18066191514122037
Relative absolute error: 60.122955416901426
Root relative squared error: 90.40947210744126
Weighted TruePositiveRate: 0.48672566371681414
Weighted MatthewsCorrelation: 0.30711397205956487
Weighted FMeasure: 0.335889872173058
Iteration time: 2.0
Weighted AreaUnderPRC: 0.32435985936857703
Mean absolute error: 0.048014860228775345
Coverage of cases: 69.02654867256638
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.48672566371681414
Weighted FalsePositiveRate: 0.12313549411634847
Kappa statistic: 0.35549218212213585
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 9.365781710914456
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.40153581060700855
Weighted AreaUnderROC: 0.7990304245860241
Root mean squared error: 0.16883580232854078
Relative absolute error: 49.9202192103763
Root relative squared error: 84.49127614654026
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.4296531873413882
Weighted FMeasure: 0.4529446773275371
Iteration time: 2.0
Weighted AreaUnderPRC: 0.4342762070378327
Mean absolute error: 0.039866841730508765
Coverage of cases: 77.87610619469027
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.09297759024683608
Kappa statistic: 0.4670837343599615
Training time: 2.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 5.936578171091448
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.49022271538641854
Weighted AreaUnderROC: 0.8110546578326239
Root mean squared error: 0.16393211104657884
Relative absolute error: 40.08928143144232
Root relative squared error: 82.03729939204005
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5225848297958755
Weighted FMeasure: 0.5510861315428058
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5077262859576935
Mean absolute error: 0.03201574558761012
Coverage of cases: 69.02654867256638
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.06222800463209194
Kappa statistic: 0.5762632417736946
Training time: 2.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.333333333333332
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.5662336341311013
Weighted AreaUnderROC: 0.8807686250478065
Root mean squared error: 0.14109033212091518
Relative absolute error: 38.13022011798739
Root relative squared error: 70.60648303514567
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.5919787417052754
Weighted FMeasure: 0.6184222367828978
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6028854146639409
Mean absolute error: 0.030451217455337083
Coverage of cases: 85.84070796460178
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.05276187536621581
Kappa statistic: 0.6434338747099768
Training time: 3.0
		
Time end:Tue Oct 31 11.21.25 EET 2017