Tue Oct 31 11.15.27 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.27 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 5.199115044247787
Incorrectly Classified Instances: 70.79646017699115
Correctly Classified Instances: 29.20353982300885
Weighted Precision: 0.08606830730724535
Weighted AreaUnderROC: 0.8153890317109433
Root mean squared error: 0.23424281401031066
Relative absolute error: 72.69970245633374
Root relative squared error: 117.22320746505657
Weighted TruePositiveRate: 0.2920353982300885
Weighted MatthewsCorrelation: 0.06509122624943038
Weighted FMeasure: 0.13294670744403958
Iteration time: 99.0
Weighted AreaUnderPRC: 0.5056616883664868
Mean absolute error: 0.0580587901560998
Coverage of cases: 36.283185840707965
Instances selection time: 93.0
Test time: 37.0
Accumulative iteration time: 99.0
Weighted Recall: 0.2920353982300885
Weighted FalsePositiveRate: 0.21917349395905114
Kappa statistic: 0.07961718590918346
Training time: 6.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 4.535398230088494
Incorrectly Classified Instances: 70.79646017699115
Correctly Classified Instances: 29.20353982300885
Weighted Precision: 0.08606830730724535
Weighted AreaUnderROC: 0.775381938732055
Root mean squared error: 0.23964392336440646
Relative absolute error: 73.51188166218562
Root relative squared error: 119.92610943040249
Weighted TruePositiveRate: 0.2920353982300885
Weighted MatthewsCorrelation: 0.06509122624943038
Weighted FMeasure: 0.13294670744403958
Iteration time: 36.0
Weighted AreaUnderPRC: 0.45321376705931005
Mean absolute error: 0.058707405494106496
Coverage of cases: 30.97345132743363
Instances selection time: 35.0
Test time: 30.0
Accumulative iteration time: 135.0
Weighted Recall: 0.2920353982300885
Weighted FalsePositiveRate: 0.21917349395905114
Kappa statistic: 0.07961718590918346
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 6.6371681415929205
Incorrectly Classified Instances: 57.52212389380531
Correctly Classified Instances: 42.47787610619469
Weighted Precision: 0.22180270565226315
Weighted AreaUnderROC: 0.8763123346929128
Root mean squared error: 0.2053883466229551
Relative absolute error: 60.69235930948393
Root relative squared error: 102.78343380056837
Weighted TruePositiveRate: 0.4247787610619469
Weighted MatthewsCorrelation: 0.2320595712743523
Weighted FMeasure: 0.28488809057896947
Iteration time: 26.0
Weighted AreaUnderPRC: 0.5858899542531054
Mean absolute error: 0.048469592504101704
Coverage of cases: 47.78761061946903
Instances selection time: 25.0
Test time: 31.0
Accumulative iteration time: 161.0
Weighted Recall: 0.4247787610619469
Weighted FalsePositiveRate: 0.1495587017747714
Kappa statistic: 0.2884820304175143
Training time: 1.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.227138643067846
Incorrectly Classified Instances: 41.5929203539823
Correctly Classified Instances: 58.4070796460177
Weighted Precision: 0.42498298162014975
Weighted AreaUnderROC: 0.929278608824646
Root mean squared error: 0.17128582778472617
Relative absolute error: 46.86666070757647
Root relative squared error: 85.7173536403516
Weighted TruePositiveRate: 0.584070796460177
Weighted MatthewsCorrelation: 0.43979793422622626
Weighted FMeasure: 0.48235148733715355
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6746900132090247
Mean absolute error: 0.03742823598174506
Coverage of cases: 67.2566371681416
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 177.0
Weighted Recall: 0.584070796460177
Weighted FalsePositiveRate: 0.09708446318321479
Kappa statistic: 0.48695904173106647
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.227138643067846
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5359655094168368
Weighted AreaUnderROC: 0.9420951162308461
Root mean squared error: 0.16425349351999088
Relative absolute error: 43.64923907221946
Root relative squared error: 82.19813029955617
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4787030110822034
Weighted FMeasure: 0.5074711744584773
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7412039439802691
Mean absolute error: 0.03485876731461967
Coverage of cases: 75.22123893805309
Instances selection time: 6.0
Test time: 30.0
Accumulative iteration time: 187.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.10052639172649405
Kappa statistic: 0.4949965996308171
Training time: 4.0
		
Time end:Tue Oct 31 11.15.27 EET 2017