Tue Oct 31 11.21.07 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.07 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 45.83333333333339
Incorrectly Classified Instances: 53.097345132743364
Correctly Classified Instances: 46.902654867256636
Weighted Precision: 0.38902285030603617
Weighted AreaUnderROC: 0.7303313712419608
Root mean squared error: 0.1959049851302076
Relative absolute error: 95.3420313174068
Root relative squared error: 98.03763164468437
Weighted TruePositiveRate: 0.4690265486725664
Weighted MatthewsCorrelation: 0.33195684279689697
Weighted FMeasure: 0.3825170422151162
Iteration time: 1162.0
Weighted AreaUnderPRC: 0.34688180601864577
Mean absolute error: 0.07614120556598451
Coverage of cases: 78.76106194690266
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 1162.0
Weighted Recall: 0.4690265486725664
Weighted FalsePositiveRate: 0.12293301980380315
Kappa statistic: 0.34795152914021926
Training time: 1159.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 45.83333333333339
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.3339319235336935
Weighted AreaUnderROC: 0.7302568489123465
Root mean squared error: 0.19589543084693115
Relative absolute error: 95.33736751897618
Root relative squared error: 98.03285035081447
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.29477505702130885
Weighted FMeasure: 0.35217959205465715
Iteration time: 1132.0
Weighted AreaUnderPRC: 0.33269866583077473
Mean absolute error: 0.07613748100473783
Coverage of cases: 78.76106194690266
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 2294.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.12850072743153335
Kappa statistic: 0.3239409437421596
Training time: 1129.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 50.0
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5776124492647547
Weighted AreaUnderROC: 0.8209152472623552
Root mean squared error: 0.19356346267773705
Relative absolute error: 94.88369177842507
Root relative squared error: 96.86585280745626
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.48974940872703426
Weighted FMeasure: 0.5127956657532811
Iteration time: 1637.0
Weighted AreaUnderPRC: 0.4772399801823269
Mean absolute error: 0.07577517051749216
Coverage of cases: 91.15044247787611
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 3931.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08532397469665805
Kappa statistic: 0.5107764705882353
Training time: 1634.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 50.0
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6356618004212748
Weighted AreaUnderROC: 0.8693973330790705
Root mean squared error: 0.19338478367336506
Relative absolute error: 94.79594483781618
Root relative squared error: 96.77643565249403
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.5867675824342321
Weighted FMeasure: 0.6036407054106169
Iteration time: 1631.0
Weighted AreaUnderPRC: 0.5728038546084547
Mean absolute error: 0.0757050948357559
Coverage of cases: 91.15044247787611
Instances selection time: 2.0
Test time: 8.0
Accumulative iteration time: 5562.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.06861535413464204
Kappa statistic: 0.6137852463685559
Training time: 1629.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 62.5
Incorrectly Classified Instances: 25.663716814159294
Correctly Classified Instances: 74.33628318584071
Weighted Precision: 0.6905131266308853
Weighted AreaUnderROC: 0.912759705576383
Root mean squared error: 0.1933064106245302
Relative absolute error: 95.41883354640125
Root relative squared error: 96.73721506764042
Weighted TruePositiveRate: 0.7433628318584071
Weighted MatthewsCorrelation: 0.6715461029583607
Weighted FMeasure: 0.6939808563233519
Iteration time: 2501.0
Weighted AreaUnderPRC: 0.6703692670085786
Mean absolute error: 0.07620254067941758
Coverage of cases: 94.69026548672566
Instances selection time: 1.0
Test time: 10.0
Accumulative iteration time: 8063.0
Weighted Recall: 0.7433628318584071
Weighted FalsePositiveRate: 0.053850353323203155
Kappa statistic: 0.6918085206432805
Training time: 2500.0
		
Time end:Tue Oct 31 11.21.15 EET 2017