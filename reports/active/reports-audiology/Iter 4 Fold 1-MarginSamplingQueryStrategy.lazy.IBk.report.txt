Tue Oct 31 11.18.38 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.38 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 88.27433628318593
Incorrectly Classified Instances: 45.13274336283186
Correctly Classified Instances: 54.86725663716814
Weighted Precision: 0.5919649639830479
Weighted AreaUnderROC: 0.8131212218332358
Root mean squared error: 0.16159789876741565
Relative absolute error: 68.1304504640792
Root relative squared error: 80.86917881842116
Weighted TruePositiveRate: 0.5486725663716814
Weighted MatthewsCorrelation: 0.4734513585212986
Weighted FMeasure: 0.4888815237487803
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5291995097358313
Mean absolute error: 0.05440973474561876
Coverage of cases: 95.57522123893806
Instances selection time: 8.0
Test time: 10.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5486725663716814
Weighted FalsePositiveRate: 0.07722284525434683
Kappa statistic: 0.4642060245444403
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 79.64601769911508
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.6306474149976712
Weighted AreaUnderROC: 0.8200567675067596
Root mean squared error: 0.15309769447260563
Relative absolute error: 57.6490811341604
Root relative squared error: 76.61538253546658
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5785700521147454
Weighted FMeasure: 0.5995971470083212
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5755136651010756
Mean absolute error: 0.046039196739086394
Coverage of cases: 94.69026548672566
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 12.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.054291533740163565
Kappa statistic: 0.568782576321668
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 69.83775811209438
Incorrectly Classified Instances: 38.05309734513274
Correctly Classified Instances: 61.94690265486726
Weighted Precision: 0.6019889157057299
Weighted AreaUnderROC: 0.8058900676243388
Root mean squared error: 0.15559637788894426
Relative absolute error: 54.28533914163567
Root relative squared error: 77.86581015579932
Weighted TruePositiveRate: 0.6194690265486725
Weighted MatthewsCorrelation: 0.550303280301951
Weighted FMeasure: 0.5869411952449963
Iteration time: 3.0
Weighted AreaUnderPRC: 0.567810726862197
Mean absolute error: 0.043352875008945115
Coverage of cases: 92.03539823008849
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 15.0
Weighted Recall: 0.6194690265486725
Weighted FalsePositiveRate: 0.0683213653325194
Kappa statistic: 0.5433270676691728
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 64.26991150442477
Incorrectly Classified Instances: 32.743362831858406
Correctly Classified Instances: 67.2566371681416
Weighted Precision: 0.6566969313871968
Weighted AreaUnderROC: 0.8405058828952328
Root mean squared error: 0.15040669675685925
Relative absolute error: 47.9632466091006
Root relative squared error: 75.2687141868399
Weighted TruePositiveRate: 0.672566371681416
Weighted MatthewsCorrelation: 0.6148568549848059
Weighted FMeasure: 0.6412115820288039
Iteration time: 2.0
Weighted AreaUnderPRC: 0.6104181747044202
Mean absolute error: 0.038303981666990033
Coverage of cases: 93.80530973451327
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 17.0
Weighted Recall: 0.672566371681416
Weighted FalsePositiveRate: 0.05080582385780988
Kappa statistic: 0.6117199108469539
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 53.42920353982299
Incorrectly Classified Instances: 26.548672566371682
Correctly Classified Instances: 73.45132743362832
Weighted Precision: 0.6964419986283599
Weighted AreaUnderROC: 0.8805156908163053
Root mean squared error: 0.13262440035638876
Relative absolute error: 39.850386410407005
Root relative squared error: 66.36983791195989
Weighted TruePositiveRate: 0.7345132743362832
Weighted MatthewsCorrelation: 0.6734620780577736
Weighted FMeasure: 0.7033853069251299
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7104321217512658
Mean absolute error: 0.031824961369422235
Coverage of cases: 93.80530973451327
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7345132743362832
Weighted FalsePositiveRate: 0.04387999183460812
Kappa statistic: 0.6868360277136258
Training time: 0.0
		
Time end:Tue Oct 31 11.18.38 EET 2017