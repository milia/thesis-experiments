Tue Oct 31 11.16.26 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.26 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.41740412979357
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5149633784938863
Weighted AreaUnderROC: 0.7824460835143099
Root mean squared error: 0.16261883561333343
Relative absolute error: 70.0041530317667
Root relative squared error: 81.38009093413908
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4869235813125953
Weighted FMeasure: 0.5237783294817943
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4547076382190571
Mean absolute error: 0.055906094435091556
Coverage of cases: 96.46017699115045
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08081000159536497
Kappa statistic: 0.5078583601590608
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 78.53982300884952
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.4410185667187228
Weighted AreaUnderROC: 0.7778269691826908
Root mean squared error: 0.16957205587478666
Relative absolute error: 63.46649237759973
Root relative squared error: 84.85972289084314
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.4400896299042627
Weighted FMeasure: 0.47689820715184655
Iteration time: 8.0
Weighted AreaUnderPRC: 0.45430089453886074
Mean absolute error: 0.05068504599599986
Coverage of cases: 89.38053097345133
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 15.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.07663815266224183
Kappa statistic: 0.48126288176878396
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 66.6297935103245
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.5402066881303167
Weighted AreaUnderROC: 0.8066992522609058
Root mean squared error: 0.16579827348973322
Relative absolute error: 58.072240788894845
Root relative squared error: 82.97119163612716
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.5031728936359607
Weighted FMeasure: 0.5359177274402
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5139636194278868
Mean absolute error: 0.046377136741131364
Coverage of cases: 84.95575221238938
Instances selection time: 7.0
Test time: 9.0
Accumulative iteration time: 23.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.07130676290677518
Kappa statistic: 0.524588631264024
Training time: 1.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 56.932153392330356
Incorrectly Classified Instances: 32.743362831858406
Correctly Classified Instances: 67.2566371681416
Weighted Precision: 0.6026235484958964
Weighted AreaUnderROC: 0.8369371963133979
Root mean squared error: 0.14999922612518285
Relative absolute error: 46.51364332317168
Root relative squared error: 75.06480178681711
Weighted TruePositiveRate: 0.672566371681416
Weighted MatthewsCorrelation: 0.5789272727567297
Weighted FMeasure: 0.6014132960187286
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6212204824978843
Mean absolute error: 0.0371463123761441
Coverage of cases: 84.95575221238938
Instances selection time: 13.0
Test time: 16.0
Accumulative iteration time: 36.0
Weighted Recall: 0.672566371681416
Weighted FalsePositiveRate: 0.06502902247643998
Kappa statistic: 0.6070488721804511
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 48.23008849557522
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.6410029498525073
Weighted AreaUnderROC: 0.8538901080878606
Root mean squared error: 0.14227389527214576
Relative absolute error: 42.305048526305995
Root relative squared error: 71.19877897989376
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6246216644884899
Weighted FMeasure: 0.6494997618421239
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6845473882452194
Mean absolute error: 0.033785281809202754
Coverage of cases: 85.84070796460178
Instances selection time: 7.0
Test time: 25.0
Accumulative iteration time: 46.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.05513894316558848
Kappa statistic: 0.6524373194146705
Training time: 3.0
		
Time end:Tue Oct 31 11.16.26 EET 2017