Tue Oct 31 11.17.31 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.31 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.41740412979357
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5149633784938863
Weighted AreaUnderROC: 0.7824460835143099
Root mean squared error: 0.16261883561333343
Relative absolute error: 70.0041530317667
Root relative squared error: 81.38009093413908
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4869235813125953
Weighted FMeasure: 0.5237783294817943
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4547076382190571
Mean absolute error: 0.055906094435091556
Coverage of cases: 96.46017699115045
Instances selection time: 8.0
Test time: 8.0
Accumulative iteration time: 8.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08081000159536497
Kappa statistic: 0.5078583601590608
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 80.64159292035397
Incorrectly Classified Instances: 42.47787610619469
Correctly Classified Instances: 57.52212389380531
Weighted Precision: 0.4984875261042488
Weighted AreaUnderROC: 0.7798553290579019
Root mean squared error: 0.1667561274437903
Relative absolute error: 62.46893506647239
Root relative squared error: 83.45053489048507
Weighted TruePositiveRate: 0.5752212389380531
Weighted MatthewsCorrelation: 0.4723032283351491
Weighted FMeasure: 0.5046399268639222
Iteration time: 4.0
Weighted AreaUnderPRC: 0.47670608441754003
Mean absolute error: 0.049888385643363446
Coverage of cases: 92.03539823008849
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 12.0
Weighted Recall: 0.5752212389380531
Weighted FalsePositiveRate: 0.06836128351563897
Kappa statistic: 0.4968460111317254
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 69.06342182890855
Incorrectly Classified Instances: 37.16814159292036
Correctly Classified Instances: 62.83185840707964
Weighted Precision: 0.6029874625041616
Weighted AreaUnderROC: 0.8042636944741456
Root mean squared error: 0.15890770882704625
Relative absolute error: 55.02276355151318
Root relative squared error: 79.52291470853703
Weighted TruePositiveRate: 0.6283185840707964
Weighted MatthewsCorrelation: 0.5424160733223046
Weighted FMeasure: 0.5676747110195924
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5506768685604189
Mean absolute error: 0.04394179033627795
Coverage of cases: 84.070796460177
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6283185840707964
Weighted FalsePositiveRate: 0.06836323318682694
Kappa statistic: 0.5569454817027633
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 59.07079646017694
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.6716593964381575
Weighted AreaUnderROC: 0.8451549132760011
Root mean squared error: 0.14195832056531707
Relative absolute error: 44.618002488184594
Root relative squared error: 71.04085447968826
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6338551661797545
Weighted FMeasure: 0.6536771996186094
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6606361426924757
Mean absolute error: 0.035632432542647474
Coverage of cases: 85.84070796460178
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 22.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.05781874848914794
Kappa statistic: 0.6516255605381166
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 50.11061946902654
Incorrectly Classified Instances: 26.548672566371682
Correctly Classified Instances: 73.45132743362832
Weighted Precision: 0.6972505861886393
Weighted AreaUnderROC: 0.8629372242640705
Root mean squared error: 0.13536683324732773
Relative absolute error: 40.52454773747094
Root relative squared error: 67.74224620158783
Weighted TruePositiveRate: 0.7345132743362832
Weighted MatthewsCorrelation: 0.6671255749358528
Weighted FMeasure: 0.6900441266935012
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7102159539419056
Mean absolute error: 0.032363354095896975
Coverage of cases: 86.72566371681415
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 24.0
Weighted Recall: 0.7345132743362832
Weighted FalsePositiveRate: 0.05084381753852548
Kappa statistic: 0.685119821660784
Training time: 0.0
		
Time end:Tue Oct 31 11.17.32 EET 2017