Tue Oct 31 11.16.29 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.29 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 8.1858407079646
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.46600204844608806
Weighted AreaUnderROC: 0.842732227003619
Root mean squared error: 0.16360386361205098
Relative absolute error: 50.419392073874675
Root relative squared error: 81.8730330204969
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.46071021619140207
Weighted FMeasure: 0.49012297135661775
Iteration time: 6.0
Weighted AreaUnderPRC: 0.49312780802578843
Mean absolute error: 0.04026548672566371
Coverage of cases: 80.53097345132744
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.05855554872372249
Kappa statistic: 0.48986548737792524
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.1858407079646
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.46600204844608806
Weighted AreaUnderROC: 0.842732227003619
Root mean squared error: 0.16531903544609125
Relative absolute error: 51.80454020777232
Root relative squared error: 82.73136434045395
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.46071021619140207
Weighted FMeasure: 0.49012297135661775
Iteration time: 8.0
Weighted AreaUnderPRC: 0.49312780802578843
Mean absolute error: 0.041371681415929194
Coverage of cases: 80.53097345132744
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.05855554872372249
Kappa statistic: 0.48986548737792524
Training time: 4.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.517699115044248
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.42466383174347777
Weighted AreaUnderROC: 0.7871734904274236
Root mean squared error: 0.17262033797108198
Relative absolute error: 50.17134974811013
Root relative squared error: 86.38518870329847
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.44035146756017707
Weighted FMeasure: 0.4798647541439435
Iteration time: 8.0
Weighted AreaUnderPRC: 0.44692293222519547
Mean absolute error: 0.040067397368282305
Coverage of cases: 69.02654867256638
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 22.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.08089462542499351
Kappa statistic: 0.4719626168224299
Training time: 5.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 6.674041297935108
Incorrectly Classified Instances: 45.13274336283186
Correctly Classified Instances: 54.86725663716814
Weighted Precision: 0.4337808460387161
Weighted AreaUnderROC: 0.7569433633253854
Root mean squared error: 0.1856400884679896
Relative absolute error: 49.252219399239614
Root relative squared error: 92.90072225377527
Weighted TruePositiveRate: 0.5486725663716814
Weighted MatthewsCorrelation: 0.4299434293485281
Weighted FMeasure: 0.4790159548911161
Iteration time: 8.0
Weighted AreaUnderPRC: 0.4218069229829909
Mean absolute error: 0.03933336965911488
Coverage of cases: 61.94690265486726
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 30.0
Weighted Recall: 0.5486725663716814
Weighted FalsePositiveRate: 0.07884996192415958
Kappa statistic: 0.45673076923076916
Training time: 4.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 6.93215339233039
Incorrectly Classified Instances: 23.893805309734514
Correctly Classified Instances: 76.10619469026548
Weighted Precision: 0.7208055695822483
Weighted AreaUnderROC: 0.8737471088004053
Root mean squared error: 0.1391495471659304
Relative absolute error: 30.6876134700733
Root relative squared error: 69.63524710466702
Weighted TruePositiveRate: 0.7610619469026548
Weighted MatthewsCorrelation: 0.7044250957820847
Weighted FMeasure: 0.7238150062919984
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6462479149713055
Mean absolute error: 0.024507469090683482
Coverage of cases: 79.64601769911505
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 38.0
Weighted Recall: 0.7610619469026548
Weighted FalsePositiveRate: 0.034473796097443506
Kappa statistic: 0.7193708609271523
Training time: 5.0
		
Time end:Tue Oct 31 11.16.29 EET 2017