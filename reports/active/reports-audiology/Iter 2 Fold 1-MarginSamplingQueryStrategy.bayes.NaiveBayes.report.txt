Tue Oct 31 11.17.39 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.39 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 5.309734513274338
Incorrectly Classified Instances: 68.14159292035399
Correctly Classified Instances: 31.858407079646017
Weighted Precision: 0.2030807303508342
Weighted AreaUnderROC: 0.8118985010700761
Root mean squared error: 0.2302082714446043
Relative absolute error: 71.11302417801616
Root relative squared error: 115.2041827952728
Weighted TruePositiveRate: 0.3185840707964602
Weighted MatthewsCorrelation: 0.14009137392664364
Weighted FMeasure: 0.18580511396752938
Iteration time: 23.0
Weighted AreaUnderPRC: 0.47559601653976974
Mean absolute error: 0.05679165125327667
Coverage of cases: 35.39823008849557
Instances selection time: 23.0
Test time: 30.0
Accumulative iteration time: 23.0
Weighted Recall: 0.3185840707964602
Weighted FalsePositiveRate: 0.19743561141270666
Kappa statistic: 0.12482397907865621
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.628318584070794
Incorrectly Classified Instances: 53.982300884955755
Correctly Classified Instances: 46.017699115044245
Weighted Precision: 0.3628122355372314
Weighted AreaUnderROC: 0.8930019855901009
Root mean squared error: 0.1852542197392061
Relative absolute error: 56.8905859476316
Root relative squared error: 92.7076201932507
Weighted TruePositiveRate: 0.46017699115044247
Weighted MatthewsCorrelation: 0.31110508080167787
Weighted FMeasure: 0.3672516322537245
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6177904015388006
Mean absolute error: 0.045433454055400134
Coverage of cases: 65.48672566371681
Instances selection time: 18.0
Test time: 31.0
Accumulative iteration time: 41.0
Weighted Recall: 0.46017699115044247
Weighted FalsePositiveRate: 0.1249631303252135
Kappa statistic: 0.3393079651107064
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.407079646017698
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.5201529185912319
Weighted AreaUnderROC: 0.9178700727123921
Root mean squared error: 0.17883635746769155
Relative absolute error: 54.45236528255094
Root relative squared error: 89.49589989474542
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.44037331822632314
Weighted FMeasure: 0.48790771175726927
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6321970826999828
Mean absolute error: 0.04348626394092601
Coverage of cases: 70.79646017699115
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 54.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.10586178391014356
Kappa statistic: 0.4355969647488233
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.964601769911499
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5091791992742838
Weighted AreaUnderROC: 0.9385907713574988
Root mean squared error: 0.16468870949229303
Relative absolute error: 45.464686248473626
Root relative squared error: 82.41592742784306
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.47882589825900085
Weighted FMeasure: 0.5216007037274991
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6852730038415272
Mean absolute error: 0.0363086036012115
Coverage of cases: 76.10619469026548
Instances selection time: 8.0
Test time: 31.0
Accumulative iteration time: 62.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.09555623979730103
Kappa statistic: 0.49734068271927284
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.042772861356932
Incorrectly Classified Instances: 38.05309734513274
Correctly Classified Instances: 61.94690265486726
Weighted Precision: 0.5594753909909888
Weighted AreaUnderROC: 0.9445156761461067
Root mean squared error: 0.16041799475933624
Relative absolute error: 41.38962112978652
Root relative squared error: 80.2787140354893
Weighted TruePositiveRate: 0.6194690265486725
Weighted MatthewsCorrelation: 0.517097047430385
Weighted FMeasure: 0.5446465039385393
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7318367487782665
Mean absolute error: 0.03305421131892666
Coverage of cases: 77.87610619469027
Instances selection time: 3.0
Test time: 34.0
Accumulative iteration time: 65.0
Weighted Recall: 0.6194690265486725
Weighted FalsePositiveRate: 0.08396368833348637
Kappa statistic: 0.5326536500913724
Training time: 0.0
		
Time end:Tue Oct 31 11.17.40 EET 2017