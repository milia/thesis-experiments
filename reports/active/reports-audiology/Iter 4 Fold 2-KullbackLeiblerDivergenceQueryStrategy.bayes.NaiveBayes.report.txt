Tue Oct 31 11.15.26 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.26 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.452802359882003
Incorrectly Classified Instances: 71.68141592920354
Correctly Classified Instances: 28.31858407079646
Weighted Precision: 0.29432972795804657
Weighted AreaUnderROC: 0.7997821906425161
Root mean squared error: 0.230841126175663
Relative absolute error: 73.92496442434461
Root relative squared error: 115.52088519550468
Weighted TruePositiveRate: 0.2831858407079646
Weighted MatthewsCorrelation: 0.10768157383876559
Weighted FMeasure: 0.16299381109375902
Iteration time: 48.0
Weighted AreaUnderPRC: 0.4398928030360764
Mean absolute error: 0.05903729797777516
Coverage of cases: 42.47787610619469
Instances selection time: 44.0
Test time: 30.0
Accumulative iteration time: 48.0
Weighted Recall: 0.2831858407079646
Weighted FalsePositiveRate: 0.23559826635237965
Kappa statistic: 0.05179736869367037
Training time: 4.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 5.420353982300888
Incorrectly Classified Instances: 74.33628318584071
Correctly Classified Instances: 25.663716814159294
Weighted Precision: 0.15371360144864046
Weighted AreaUnderROC: 0.7474665520179998
Root mean squared error: 0.23863247903571233
Relative absolute error: 77.2683417033862
Root relative squared error: 119.41994769868492
Weighted TruePositiveRate: 0.25663716814159293
Weighted MatthewsCorrelation: 0.07142651503477272
Weighted FMeasure: 0.14350442477876107
Iteration time: 35.0
Weighted AreaUnderPRC: 0.32437300529129415
Mean absolute error: 0.06170735622145421
Coverage of cases: 32.743362831858406
Instances selection time: 34.0
Test time: 31.0
Accumulative iteration time: 83.0
Weighted Recall: 0.25663716814159293
Weighted FalsePositiveRate: 0.20415864864390618
Kappa statistic: 0.0516535118393446
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 5.936578171091449
Incorrectly Classified Instances: 58.4070796460177
Correctly Classified Instances: 41.5929203539823
Weighted Precision: 0.23095812741830438
Weighted AreaUnderROC: 0.7931529993382403
Root mean squared error: 0.2069645539564584
Relative absolute error: 60.795924481649585
Root relative squared error: 103.57222247715539
Weighted TruePositiveRate: 0.415929203539823
Weighted MatthewsCorrelation: 0.23258117659616492
Weighted FMeasure: 0.2831910643474024
Iteration time: 24.0
Weighted AreaUnderPRC: 0.4361667751740281
Mean absolute error: 0.04855230080131734
Coverage of cases: 47.78761061946903
Instances selection time: 24.0
Test time: 31.0
Accumulative iteration time: 107.0
Weighted Recall: 0.415929203539823
Weighted FalsePositiveRate: 0.1493606762310446
Kappa statistic: 0.2641341884558461
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.1902654867256635
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.3940692058534328
Weighted AreaUnderROC: 0.8833965150099803
Root mean squared error: 0.17870178395768593
Relative absolute error: 50.5426052823537
Root relative squared error: 89.42855465493801
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.40343667348314827
Weighted FMeasure: 0.4450883369810005
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5585015396457758
Mean absolute error: 0.04036388616299077
Coverage of cases: 63.716814159292035
Instances selection time: 15.0
Test time: 31.0
Accumulative iteration time: 123.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.09826199220592358
Kappa statistic: 0.43347473968376393
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.411504424778758
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.5664074372038973
Weighted AreaUnderROC: 0.9339705180424293
Root mean squared error: 0.15896981760650145
Relative absolute error: 42.28757439204216
Root relative squared error: 79.55399609035125
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5419056982359368
Weighted FMeasure: 0.5671641453956846
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6717241581717158
Mean absolute error: 0.033771326771422534
Coverage of cases: 76.10619469026548
Instances selection time: 7.0
Test time: 30.0
Accumulative iteration time: 136.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.07279171789696016
Kappa statistic: 0.5601860641731536
Training time: 6.0
		
Time end:Tue Oct 31 11.15.27 EET 2017