Tue Oct 31 11.18.43 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.43 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.8215339233038295
Incorrectly Classified Instances: 75.22123893805309
Correctly Classified Instances: 24.778761061946902
Weighted Precision: 0.15723270440251574
Weighted AreaUnderROC: 0.7785524327771273
Root mean squared error: 0.23516231584245306
Relative absolute error: 79.4114960382321
Root relative squared error: 117.68335799086522
Weighted TruePositiveRate: 0.24778761061946902
Weighted MatthewsCorrelation: 0.08378088751067697
Weighted FMeasure: 0.13151803948264126
Iteration time: 7.0
Weighted AreaUnderPRC: 0.3976479111488375
Mean absolute error: 0.06341890308608808
Coverage of cases: 35.39823008849557
Instances selection time: 6.0
Test time: 36.0
Accumulative iteration time: 7.0
Weighted Recall: 0.24778761061946902
Weighted FalsePositiveRate: 0.19835732252299343
Kappa statistic: 0.05388100866824272
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 9.439528023598818
Incorrectly Classified Instances: 60.176991150442475
Correctly Classified Instances: 39.823008849557525
Weighted Precision: 0.42361286697569883
Weighted AreaUnderROC: 0.90183883830207
Root mean squared error: 0.18687207069660425
Relative absolute error: 61.69956619433738
Root relative squared error: 93.51724877984321
Weighted TruePositiveRate: 0.39823008849557523
Weighted MatthewsCorrelation: 0.29418523679568115
Weighted FMeasure: 0.34105357549048576
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6274808193147805
Mean absolute error: 0.04927395911353327
Coverage of cases: 71.68141592920354
Instances selection time: 2.0
Test time: 30.0
Accumulative iteration time: 10.0
Weighted Recall: 0.39823008849557523
Weighted FalsePositiveRate: 0.15053397721589373
Kappa statistic: 0.24858204576569531
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.558997050147488
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.48291192583227976
Weighted AreaUnderROC: 0.9201512318519017
Root mean squared error: 0.17353859046911388
Relative absolute error: 48.85998742151337
Root relative squared error: 86.84471401909903
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.4389977865939314
Weighted FMeasure: 0.4589126406895387
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6543163957177709
Mean absolute error: 0.03902012884356967
Coverage of cases: 69.91150442477876
Instances selection time: 1.0
Test time: 30.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.10029276111127546
Kappa statistic: 0.4504425639529228
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.743362831858404
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.5437733202987045
Weighted AreaUnderROC: 0.9451925663573535
Root mean squared error: 0.15666687624357425
Relative absolute error: 42.81699815082396
Root relative squared error: 78.4015245649947
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5423525212459641
Weighted FMeasure: 0.5745501440449249
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7330000499789422
Mean absolute error: 0.034194130467671884
Coverage of cases: 76.99115044247787
Instances selection time: 1.0
Test time: 30.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.07124531412987556
Kappa statistic: 0.5690312738367659
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.112094395280236
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.5552033979936115
Weighted AreaUnderROC: 0.9514092625017417
Root mean squared error: 0.15235101652649305
Relative absolute error: 41.70982893707303
Root relative squared error: 76.24171906085137
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.542715738896623
Weighted FMeasure: 0.5824313635349553
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7466646646875265
Mean absolute error: 0.03330993283169023
Coverage of cases: 81.41592920353982
Instances selection time: 0.0
Test time: 32.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.07476157706451259
Kappa statistic: 0.5702196443852809
Training time: 1.0
		
Time end:Tue Oct 31 11.18.44 EET 2017