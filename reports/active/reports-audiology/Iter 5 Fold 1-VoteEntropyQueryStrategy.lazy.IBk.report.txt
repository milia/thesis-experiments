Tue Oct 31 11.21.23 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.21.23 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 88.97492625368739
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.3848808969325971
Weighted AreaUnderROC: 0.6993085249141734
Root mean squared error: 0.1780735064217211
Relative absolute error: 76.0192273362881
Root relative squared error: 89.11414284147334
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.3236261237161699
Weighted FMeasure: 0.3682212124449985
Iteration time: 5.0
Weighted AreaUnderPRC: 0.3527465494861667
Mean absolute error: 0.060709799608841125
Coverage of cases: 93.80530973451327
Instances selection time: 5.0
Test time: 10.0
Accumulative iteration time: 5.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.11280249495455763
Kappa statistic: 0.3345364741641337
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 78.83480825958702
Incorrectly Classified Instances: 59.29203539823009
Correctly Classified Instances: 40.70796460176991
Weighted Precision: 0.34940953621215265
Weighted AreaUnderROC: 0.659648546963035
Root mean squared error: 0.19152732111171758
Relative absolute error: 72.31806447211191
Root relative squared error: 95.84689713007432
Weighted TruePositiveRate: 0.40707964601769914
Weighted MatthewsCorrelation: 0.25953929965020106
Weighted FMeasure: 0.309023455940984
Iteration time: 12.0
Weighted AreaUnderPRC: 0.29688424294564014
Mean absolute error: 0.05775400982147821
Coverage of cases: 83.1858407079646
Instances selection time: 10.0
Test time: 13.0
Accumulative iteration time: 17.0
Weighted Recall: 0.40707964601769914
Weighted FalsePositiveRate: 0.1472308639214027
Kappa statistic: 0.2670861568247822
Training time: 2.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 73.56194690265484
Incorrectly Classified Instances: 44.24778761061947
Correctly Classified Instances: 55.75221238938053
Weighted Precision: 0.5055682944597709
Weighted AreaUnderROC: 0.7590197157995885
Root mean squared error: 0.16827513739420372
Relative absolute error: 58.15868255031165
Root relative squared error: 84.21070001790268
Weighted TruePositiveRate: 0.5575221238938053
Weighted MatthewsCorrelation: 0.43964778888261746
Weighted FMeasure: 0.4693498023881505
Iteration time: 11.0
Weighted AreaUnderPRC: 0.4622993410955571
Mean absolute error: 0.04644617009226272
Coverage of cases: 83.1858407079646
Instances selection time: 7.0
Test time: 14.0
Accumulative iteration time: 28.0
Weighted Recall: 0.5575221238938053
Weighted FalsePositiveRate: 0.0949722848978704
Kappa statistic: 0.46858540255831455
Training time: 4.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 69.94837758112095
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.6293620342292908
Weighted AreaUnderROC: 0.804270277839074
Root mean squared error: 0.15555992239616373
Relative absolute error: 48.97708953779223
Root relative squared error: 77.84756656608019
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5549486798561424
Weighted FMeasure: 0.5710848685246418
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5132409694143398
Mean absolute error: 0.03911364789476458
Coverage of cases: 86.72566371681415
Instances selection time: 6.0
Test time: 23.0
Accumulative iteration time: 35.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.07463560680761784
Kappa statistic: 0.571482745544179
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 64.34365781710915
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6613375143567752
Weighted AreaUnderROC: 0.8464830221273228
Root mean squared error: 0.1435746466537461
Relative absolute error: 42.289508198597595
Root relative squared error: 71.84971996909776
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6236520744031709
Weighted FMeasure: 0.6478107826431843
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6123242643258285
Mean absolute error: 0.03377287113082443
Coverage of cases: 87.61061946902655
Instances selection time: 4.0
Test time: 23.0
Accumulative iteration time: 40.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.06211083566599941
Kappa statistic: 0.6383659638554217
Training time: 1.0
		
Time end:Tue Oct 31 11.21.23 EET 2017