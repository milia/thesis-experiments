Tue Oct 31 11.15.13 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.13 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 88.82743362831864
Incorrectly Classified Instances: 47.78761061946903
Correctly Classified Instances: 52.21238938053097
Weighted Precision: 0.5137429825829146
Weighted AreaUnderROC: 0.7713149627625931
Root mean squared error: 0.16607197733151488
Relative absolute error: 71.52432635194394
Root relative squared error: 83.10816250699348
Weighted TruePositiveRate: 0.5221238938053098
Weighted MatthewsCorrelation: 0.41047799839042426
Weighted FMeasure: 0.4353507495028468
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4791977474598946
Mean absolute error: 0.05712012173939956
Coverage of cases: 96.46017699115045
Instances selection time: 14.0
Test time: 13.0
Accumulative iteration time: 14.0
Weighted Recall: 0.5221238938053098
Weighted FalsePositiveRate: 0.10428340434697615
Kappa statistic: 0.42001710863986313
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 79.90412979351034
Incorrectly Classified Instances: 41.5929203539823
Correctly Classified Instances: 58.4070796460177
Weighted Precision: 0.5734800597632458
Weighted AreaUnderROC: 0.7795454475114081
Root mean squared error: 0.16245973152470045
Relative absolute error: 61.87771225452949
Root relative squared error: 81.30046974418218
Weighted TruePositiveRate: 0.584070796460177
Weighted MatthewsCorrelation: 0.48615796267120365
Weighted FMeasure: 0.506426570660552
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5305211850029419
Mean absolute error: 0.04941622853660331
Coverage of cases: 92.03539823008849
Instances selection time: 9.0
Test time: 16.0
Accumulative iteration time: 24.0
Weighted Recall: 0.584070796460177
Weighted FalsePositiveRate: 0.07802992484450108
Kappa statistic: 0.5035984671464623
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 71.38643067846606
Incorrectly Classified Instances: 35.39823008849557
Correctly Classified Instances: 64.60176991150442
Weighted Precision: 0.5895883614910163
Weighted AreaUnderROC: 0.8301239037348207
Root mean squared error: 0.15383145014395994
Relative absolute error: 53.42766370327838
Root relative squared error: 76.98257925676297
Weighted TruePositiveRate: 0.6460176991150443
Weighted MatthewsCorrelation: 0.5505482494076968
Weighted FMeasure: 0.5781728832901765
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5880525122419581
Mean absolute error: 0.042667925874145834
Coverage of cases: 92.92035398230088
Instances selection time: 5.0
Test time: 9.0
Accumulative iteration time: 29.0
Weighted Recall: 0.6460176991150443
Weighted FalsePositiveRate: 0.07064781148929458
Kappa statistic: 0.5770562365490783
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 62.94247787610617
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.6778318584070796
Weighted AreaUnderROC: 0.8661712627549158
Root mean squared error: 0.1377643606655167
Relative absolute error: 44.9695686606084
Root relative squared error: 68.94205186108253
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.64241381964249
Weighted FMeasure: 0.6679043433343942
Iteration time: 4.0
Weighted AreaUnderPRC: 0.6802357506630717
Mean absolute error: 0.0359131971942358
Coverage of cases: 91.15044247787611
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 33.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.06158086562971381
Kappa statistic: 0.6610105934189556
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 56.12094395280236
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.6805883687716066
Weighted AreaUnderROC: 0.8739753228310633
Root mean squared error: 0.13456745535528575
Relative absolute error: 42.14815863787396
Root relative squared error: 67.34220985094152
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6425121171530073
Weighted FMeasure: 0.6677395785505678
Iteration time: 2.0
Weighted AreaUnderPRC: 0.7045236436971335
Mean absolute error: 0.03365998780107982
Coverage of cases: 89.38053097345133
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 35.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.05690023059875935
Kappa statistic: 0.6525344763324636
Training time: 0.0
		
Time end:Tue Oct 31 11.15.13 EET 2017