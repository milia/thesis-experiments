Tue Oct 31 11.18.37 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.37 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 89.41740412979357
Incorrectly Classified Instances: 40.70796460176991
Correctly Classified Instances: 59.29203539823009
Weighted Precision: 0.5149633784938863
Weighted AreaUnderROC: 0.7824460835143099
Root mean squared error: 0.16261883561333343
Relative absolute error: 70.0041530317667
Root relative squared error: 81.38009093413908
Weighted TruePositiveRate: 0.5929203539823009
Weighted MatthewsCorrelation: 0.4869235813125953
Weighted FMeasure: 0.5237783294817943
Iteration time: 6.0
Weighted AreaUnderPRC: 0.4547076382190571
Mean absolute error: 0.055906094435091556
Coverage of cases: 96.46017699115045
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 6.0
Weighted Recall: 0.5929203539823009
Weighted FalsePositiveRate: 0.08081000159536497
Kappa statistic: 0.5078583601590608
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 81.48967551622418
Incorrectly Classified Instances: 41.5929203539823
Correctly Classified Instances: 58.4070796460177
Weighted Precision: 0.5054758756315059
Weighted AreaUnderROC: 0.7752041498127011
Root mean squared error: 0.16502113114973915
Relative absolute error: 62.364684307964986
Root relative squared error: 82.58228272493643
Weighted TruePositiveRate: 0.584070796460177
Weighted MatthewsCorrelation: 0.48009613219988695
Weighted FMeasure: 0.5098069850748783
Iteration time: 4.0
Weighted AreaUnderPRC: 0.4669862075436072
Mean absolute error: 0.04980512982927767
Coverage of cases: 92.92035398230088
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 10.0
Weighted Recall: 0.584070796460177
Weighted FalsePositiveRate: 0.07204750910642038
Kappa statistic: 0.5031340630554777
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 70.98082595870204
Incorrectly Classified Instances: 36.283185840707965
Correctly Classified Instances: 63.716814159292035
Weighted Precision: 0.6501909927545098
Weighted AreaUnderROC: 0.812093760773742
Root mean squared error: 0.15409363447161245
Relative absolute error: 52.32132980321012
Root relative squared error: 77.11378536425592
Weighted TruePositiveRate: 0.6371681415929203
Weighted MatthewsCorrelation: 0.5594971114857867
Weighted FMeasure: 0.5742615914076092
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5785615959884777
Mean absolute error: 0.04178439532895259
Coverage of cases: 85.84070796460178
Instances selection time: 4.0
Test time: 8.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6371681415929203
Weighted FalsePositiveRate: 0.06721859895666257
Kappa statistic: 0.5669283978313703
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 58.886430678466056
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6647984267453294
Weighted AreaUnderROC: 0.847247121736613
Root mean squared error: 0.14333581953139127
Relative absolute error: 44.5435531399398
Root relative squared error: 71.73020261514873
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6234293315633974
Weighted FMeasure: 0.646295004625795
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6522255444516033
Mean absolute error: 0.0355729764659242
Coverage of cases: 86.72566371681415
Instances selection time: 2.0
Test time: 10.0
Accumulative iteration time: 17.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.05967639519703065
Kappa statistic: 0.6413368185212845
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 49.41002949852506
Incorrectly Classified Instances: 27.43362831858407
Correctly Classified Instances: 72.56637168141593
Weighted Precision: 0.6928249175776506
Weighted AreaUnderROC: 0.8614143163053908
Root mean squared error: 0.1371125417629613
Relative absolute error: 40.79149537468783
Root relative squared error: 68.6158591333921
Weighted TruePositiveRate: 0.7256637168141593
Weighted MatthewsCorrelation: 0.6585293027903937
Weighted FMeasure: 0.6846555693359274
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7016821918299081
Mean absolute error: 0.032576541445063244
Coverage of cases: 85.84070796460178
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 18.0
Weighted Recall: 0.7256637168141593
Weighted FalsePositiveRate: 0.05227585102607782
Kappa statistic: 0.6750765235135887
Training time: 0.0
		
Time end:Tue Oct 31 11.18.37 EET 2017