Tue Oct 31 11.16.34 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.34 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 21.68141592920354
Incorrectly Classified Instances: 59.29203539823009
Correctly Classified Instances: 40.70796460176991
Weighted Precision: 0.30727996334457985
Weighted AreaUnderROC: 0.7540294718362408
Root mean squared error: 0.17771091916135104
Relative absolute error: 75.6083932740339
Root relative squared error: 88.93269163313586
Weighted TruePositiveRate: 0.40707964601769914
Weighted MatthewsCorrelation: 0.27018303731670207
Weighted FMeasure: 0.3466273853480042
Iteration time: 5.0
Weighted AreaUnderPRC: 0.4249020503198901
Mean absolute error: 0.060381702961902026
Coverage of cases: 68.14159292035399
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.40707964601769914
Weighted FalsePositiveRate: 0.10941207188707656
Kappa statistic: 0.2885735763954144
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 13.421828908554568
Incorrectly Classified Instances: 57.52212389380531
Correctly Classified Instances: 42.47787610619469
Weighted Precision: 0.3245369263068378
Weighted AreaUnderROC: 0.746770719140933
Root mean squared error: 0.18880209181694002
Relative absolute error: 68.19021789428108
Root relative squared error: 94.48309811510225
Weighted TruePositiveRate: 0.4247787610619469
Weighted MatthewsCorrelation: 0.2612589457308659
Weighted FMeasure: 0.29834898561194384
Iteration time: 6.0
Weighted AreaUnderPRC: 0.42340363351255367
Mean absolute error: 0.05445746567946055
Coverage of cases: 65.48672566371681
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.4247787610619469
Weighted FalsePositiveRate: 0.15745527077910437
Kappa statistic: 0.2633637548891786
Training time: 5.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 13.790560471976404
Incorrectly Classified Instances: 47.78761061946903
Correctly Classified Instances: 52.21238938053097
Weighted Precision: 0.4431444661533157
Weighted AreaUnderROC: 0.8195705950202877
Root mean squared error: 0.16971429438880126
Relative absolute error: 61.64106217074064
Root relative squared error: 84.93090396381785
Weighted TruePositiveRate: 0.5221238938053098
Weighted MatthewsCorrelation: 0.3973369055216251
Weighted FMeasure: 0.4337092949274104
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5714813616468786
Mean absolute error: 0.04922723715024422
Coverage of cases: 72.56637168141593
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 18.0
Weighted Recall: 0.5221238938053098
Weighted FalsePositiveRate: 0.11574272540332177
Kappa statistic: 0.40786026200873365
Training time: 6.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 14.380530973451327
Incorrectly Classified Instances: 37.16814159292036
Correctly Classified Instances: 62.83185840707964
Weighted Precision: 0.5329578032227503
Weighted AreaUnderROC: 0.846726252544735
Root mean squared error: 0.15746284429439908
Relative absolute error: 57.30698420044247
Root relative squared error: 78.79985451313684
Weighted TruePositiveRate: 0.6283185840707964
Weighted MatthewsCorrelation: 0.5151742995112946
Weighted FMeasure: 0.5405130023728602
Iteration time: 9.0
Weighted AreaUnderPRC: 0.6096142685842857
Mean absolute error: 0.04576599432674222
Coverage of cases: 77.87610619469027
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.6283185840707964
Weighted FalsePositiveRate: 0.07712621619799234
Kappa statistic: 0.5480860788421252
Training time: 8.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 15.671091445427727
Incorrectly Classified Instances: 29.20353982300885
Correctly Classified Instances: 70.79646017699115
Weighted Precision: 0.5905433264751329
Weighted AreaUnderROC: 0.9150033074845738
Root mean squared error: 0.1388280496571555
Relative absolute error: 51.72033571815557
Root relative squared error: 69.4743586294758
Weighted TruePositiveRate: 0.7079646017699115
Weighted MatthewsCorrelation: 0.6079115384936989
Weighted FMeasure: 0.6403820050850215
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7320616367734981
Mean absolute error: 0.041304434774915876
Coverage of cases: 87.61061946902655
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 39.0
Weighted Recall: 0.7079646017699115
Weighted FalsePositiveRate: 0.05795321493761885
Kappa statistic: 0.6496288640420934
Training time: 12.0
		
Time end:Tue Oct 31 11.16.34 EET 2017