Tue Oct 31 11.14.27 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.14.27 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 37.5
Incorrectly Classified Instances: 41.5929203539823
Correctly Classified Instances: 58.4070796460177
Weighted Precision: 0.46276113492409304
Weighted AreaUnderROC: 0.8230634299001375
Root mean squared error: 0.1930282562745409
Relative absolute error: 92.92642919341465
Root relative squared error: 96.59801700850737
Weighted TruePositiveRate: 0.584070796460177
Weighted MatthewsCorrelation: 0.4663997285045093
Weighted FMeasure: 0.5071613315267252
Iteration time: 616.0
Weighted AreaUnderPRC: 0.432600593480521
Mean absolute error: 0.07421207886974078
Coverage of cases: 84.95575221238938
Instances selection time: 3.0
Test time: 7.0
Accumulative iteration time: 616.0
Weighted Recall: 0.584070796460177
Weighted FalsePositiveRate: 0.07151561544116267
Kappa statistic: 0.5014549892049188
Training time: 613.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 54.166666666666586
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.6464653826132223
Weighted AreaUnderROC: 0.8908812909689231
Root mean squared error: 0.19310809312405045
Relative absolute error: 94.92162320173107
Root relative squared error: 96.63797012985691
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.6171516796343168
Weighted FMeasure: 0.6526212935821026
Iteration time: 2000.0
Weighted AreaUnderPRC: 0.5743065463559247
Mean absolute error: 0.07580546297360458
Coverage of cases: 93.80530973451327
Instances selection time: 11.0
Test time: 19.0
Accumulative iteration time: 2616.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.04648516154790662
Kappa statistic: 0.6249999999999999
Training time: 1989.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 75.0
Incorrectly Classified Instances: 33.6283185840708
Correctly Classified Instances: 66.3716814159292
Weighted Precision: 0.6384838244351518
Weighted AreaUnderROC: 0.9057016859544038
Root mean squared error: 0.1936198327351184
Relative absolute error: 96.03693728357078
Root relative squared error: 96.89406233422082
Weighted TruePositiveRate: 0.6637168141592921
Weighted MatthewsCorrelation: 0.5974287400725063
Weighted FMeasure: 0.6386592758274174
Iteration time: 1793.0
Weighted AreaUnderPRC: 0.5831160480141689
Mean absolute error: 0.07669616519174047
Coverage of cases: 97.34513274336283
Instances selection time: 19.0
Test time: 27.0
Accumulative iteration time: 4409.0
Weighted Recall: 0.6637168141592921
Weighted FalsePositiveRate: 0.0538016581158903
Kappa statistic: 0.6036551596824812
Training time: 1774.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 75.0
Incorrectly Classified Instances: 30.088495575221238
Correctly Classified Instances: 69.91150442477876
Weighted Precision: 0.6668912577098418
Weighted AreaUnderROC: 0.9158061890515571
Root mean squared error: 0.19354805791558843
Relative absolute error: 96.00273609507947
Root relative squared error: 96.85814373156879
Weighted TruePositiveRate: 0.6991150442477876
Weighted MatthewsCorrelation: 0.6363555179234668
Weighted FMeasure: 0.6774524317210974
Iteration time: 1631.0
Weighted AreaUnderPRC: 0.6387043224049131
Mean absolute error: 0.0766688517425981
Coverage of cases: 97.34513274336283
Instances selection time: 4.0
Test time: 15.0
Accumulative iteration time: 6040.0
Weighted Recall: 0.6991150442477876
Weighted FalsePositiveRate: 0.04808278617047432
Kappa statistic: 0.6469399007535379
Training time: 1627.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 79.16666666666674
Incorrectly Classified Instances: 23.008849557522122
Correctly Classified Instances: 76.99115044247787
Weighted Precision: 0.7486175115207373
Weighted AreaUnderROC: 0.9392002459472336
Root mean squared error: 0.1934280685652075
Relative absolute error: 96.04701108818081
Root relative squared error: 96.79809690975814
Weighted TruePositiveRate: 0.7699115044247787
Weighted MatthewsCorrelation: 0.7246148690406209
Weighted FMeasure: 0.7547069449724317
Iteration time: 1615.0
Weighted AreaUnderPRC: 0.7205687871554456
Mean absolute error: 0.0767042102440332
Coverage of cases: 98.23008849557522
Instances selection time: 2.0
Test time: 19.0
Accumulative iteration time: 7655.0
Weighted Recall: 0.7699115044247787
Weighted FalsePositiveRate: 0.03490934334317442
Kappa statistic: 0.7306563989732306
Training time: 1613.0
		
Time end:Tue Oct 31 11.14.35 EET 2017