Tue Oct 31 11.15.22 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.22 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.268436578171098
Incorrectly Classified Instances: 62.83185840707964
Correctly Classified Instances: 37.16814159292036
Weighted Precision: 0.1751051791672711
Weighted AreaUnderROC: 0.794544735028287
Root mean squared error: 0.21238752519782528
Relative absolute error: 65.34216383519554
Root relative squared error: 106.28606488717607
Weighted TruePositiveRate: 0.37168141592920356
Weighted MatthewsCorrelation: 0.1572570789054859
Weighted FMeasure: 0.23804792216529524
Iteration time: 491.0
Weighted AreaUnderPRC: 0.43279744382932867
Mean absolute error: 0.05218297806282984
Coverage of cases: 49.557522123893804
Instances selection time: 481.0
Test time: 69.0
Accumulative iteration time: 491.0
Weighted Recall: 0.37168141592920356
Weighted FalsePositiveRate: 0.19083604407259575
Kappa statistic: 0.1772969647251846
Training time: 10.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.333333333333337
Incorrectly Classified Instances: 50.442477876106196
Correctly Classified Instances: 49.557522123893804
Weighted Precision: 0.3358773669911162
Weighted AreaUnderROC: 0.896870600461468
Root mean squared error: 0.18255375628589798
Relative absolute error: 56.472867685100844
Root relative squared error: 91.35621486209283
Weighted TruePositiveRate: 0.49557522123893805
Weighted MatthewsCorrelation: 0.3387497844320963
Weighted FMeasure: 0.38201917645525246
Iteration time: 61.0
Weighted AreaUnderPRC: 0.565531557667325
Mean absolute error: 0.04509985960962921
Coverage of cases: 57.52212389380531
Instances selection time: 57.0
Test time: 34.0
Accumulative iteration time: 552.0
Weighted Recall: 0.49557522123893805
Weighted FalsePositiveRate: 0.10758069758678054
Kappa statistic: 0.3789412785652299
Training time: 4.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.554572271386425
Incorrectly Classified Instances: 52.21238938053097
Correctly Classified Instances: 47.78761061946903
Weighted Precision: 0.3910029498525074
Weighted AreaUnderROC: 0.9060008486962863
Root mean squared error: 0.1826229902858835
Relative absolute error: 57.871454251740865
Root relative squared error: 91.39086195074835
Weighted TruePositiveRate: 0.4778761061946903
Weighted MatthewsCorrelation: 0.3483710325366369
Weighted FMeasure: 0.39989611496281774
Iteration time: 26.0
Weighted AreaUnderPRC: 0.6115670058268252
Mean absolute error: 0.04621678638159867
Coverage of cases: 65.48672566371681
Instances selection time: 25.0
Test time: 32.0
Accumulative iteration time: 578.0
Weighted Recall: 0.4778761061946903
Weighted FalsePositiveRate: 0.1202753589033007
Kappa statistic: 0.35472319008904374
Training time: 1.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.558997050147501
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.4533145584300343
Weighted AreaUnderROC: 0.918132264021932
Root mean squared error: 0.16674118794154094
Relative absolute error: 45.78944495217503
Root relative squared error: 83.4430586467463
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.46479936394480464
Weighted FMeasure: 0.5038272672832524
Iteration time: 34.0
Weighted AreaUnderPRC: 0.6513103144194445
Mean absolute error: 0.036567959510417615
Coverage of cases: 71.68141592920354
Instances selection time: 28.0
Test time: 31.0
Accumulative iteration time: 612.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.09749490356056627
Kappa statistic: 0.5060709082078679
Training time: 6.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.259587020648972
Incorrectly Classified Instances: 37.16814159292036
Correctly Classified Instances: 62.83185840707964
Weighted Precision: 0.538170992153293
Weighted AreaUnderROC: 0.948200562325194
Root mean squared error: 0.1527175600834131
Relative absolute error: 42.08330747384153
Root relative squared error: 76.42515013684552
Weighted TruePositiveRate: 0.6283185840707964
Weighted MatthewsCorrelation: 0.5203529852430386
Weighted FMeasure: 0.5624326638532506
Iteration time: 7.0
Weighted AreaUnderPRC: 0.7292207600834981
Mean absolute error: 0.03360819694091516
Coverage of cases: 81.41592920353982
Instances selection time: 6.0
Test time: 46.0
Accumulative iteration time: 619.0
Weighted Recall: 0.6283185840707964
Weighted FalsePositiveRate: 0.08622845007651779
Kappa statistic: 0.5433464832098527
Training time: 1.0
		
Time end:Tue Oct 31 11.15.23 EET 2017