Tue Oct 31 11.17.34 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.34 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 8.1858407079646
Incorrectly Classified Instances: 43.36283185840708
Correctly Classified Instances: 56.63716814159292
Weighted Precision: 0.46600204844608806
Weighted AreaUnderROC: 0.842732227003619
Root mean squared error: 0.16360386361205098
Relative absolute error: 50.419392073874675
Root relative squared error: 81.8730330204969
Weighted TruePositiveRate: 0.5663716814159292
Weighted MatthewsCorrelation: 0.46071021619140207
Weighted FMeasure: 0.49012297135661775
Iteration time: 5.0
Weighted AreaUnderPRC: 0.49312780802578843
Mean absolute error: 0.04026548672566371
Coverage of cases: 80.53097345132744
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5663716814159292
Weighted FalsePositiveRate: 0.05855554872372249
Kappa statistic: 0.48986548737792524
Training time: 1.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.333333333333332
Incorrectly Classified Instances: 37.16814159292036
Correctly Classified Instances: 62.83185840707964
Weighted Precision: 0.53795161872449
Weighted AreaUnderROC: 0.8531872752177101
Root mean squared error: 0.1556867227374052
Relative absolute error: 42.79354935844903
Root relative squared error: 77.9110218433351
Weighted TruePositiveRate: 0.6283185840707964
Weighted MatthewsCorrelation: 0.5334542862745335
Weighted FMeasure: 0.5634837091067962
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5346670770068653
Mean absolute error: 0.03417540400153908
Coverage of cases: 83.1858407079646
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.6283185840707964
Weighted FalsePositiveRate: 0.05544006278678939
Kappa statistic: 0.5612867443150305
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 6.231563421828913
Incorrectly Classified Instances: 26.548672566371682
Correctly Classified Instances: 73.45132743362832
Weighted Precision: 0.6568312141204549
Weighted AreaUnderROC: 0.8611276154527946
Root mean squared error: 0.14726248918954024
Relative absolute error: 33.69639210347182
Root relative squared error: 73.69524395026387
Weighted TruePositiveRate: 0.7345132743362832
Weighted MatthewsCorrelation: 0.6588735357099476
Weighted FMeasure: 0.684138402487475
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6159798884082053
Mean absolute error: 0.026910313138189243
Coverage of cases: 77.87610619469027
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.7345132743362832
Weighted FalsePositiveRate: 0.03940604265244038
Kappa statistic: 0.6870095097405595
Training time: 2.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 6.637168141592927
Incorrectly Classified Instances: 24.778761061946902
Correctly Classified Instances: 75.22123893805309
Weighted Precision: 0.7119560120601244
Weighted AreaUnderROC: 0.8653448615536891
Root mean squared error: 0.1420306931861631
Relative absolute error: 30.87762564484387
Root relative squared error: 71.07707224279908
Weighted TruePositiveRate: 0.7522123893805309
Weighted MatthewsCorrelation: 0.6953510487513626
Weighted FMeasure: 0.7149654487698746
Iteration time: 3.0
Weighted AreaUnderPRC: 0.6408712205038445
Mean absolute error: 0.02465921492470165
Coverage of cases: 77.87610619469027
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.7522123893805309
Weighted FalsePositiveRate: 0.03463324758432862
Kappa statistic: 0.708950418544752
Training time: 2.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 6.67404129793511
Incorrectly Classified Instances: 23.893805309734514
Correctly Classified Instances: 76.10619469026548
Weighted Precision: 0.7208055695822483
Weighted AreaUnderROC: 0.869769640314751
Root mean squared error: 0.13970395494589768
Relative absolute error: 30.262004252000466
Root relative squared error: 69.91269193679956
Weighted TruePositiveRate: 0.7610619469026548
Weighted MatthewsCorrelation: 0.7044250957820847
Weighted FMeasure: 0.7238150062919984
Iteration time: 3.0
Weighted AreaUnderPRC: 0.649642463357631
Mean absolute error: 0.02416757284013921
Coverage of cases: 78.76106194690266
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.7610619469026548
Weighted FalsePositiveRate: 0.034473796097443506
Kappa statistic: 0.7193708609271523
Training time: 3.0
		
Time end:Tue Oct 31 11.17.34 EET 2017