Tue Oct 31 11.18.45 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.45 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.415929203539824
Incorrectly Classified Instances: 73.45132743362832
Correctly Classified Instances: 26.548672566371682
Weighted Precision: 0.1290783945651202
Weighted AreaUnderROC: 0.7702334913323334
Root mean squared error: 0.23450402341316479
Relative absolute error: 76.23718713109578
Root relative squared error: 117.3539256864542
Weighted TruePositiveRate: 0.26548672566371684
Weighted MatthewsCorrelation: 0.07273354080527995
Weighted FMeasure: 0.13649008645872976
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4100224283942266
Mean absolute error: 0.06088386472275004
Coverage of cases: 34.51327433628319
Instances selection time: 1.0
Test time: 29.0
Accumulative iteration time: 1.0
Weighted Recall: 0.26548672566371684
Weighted FalsePositiveRate: 0.2183327184297553
Kappa statistic: 0.04743042860044692
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.70206489675516
Incorrectly Classified Instances: 55.75221238938053
Correctly Classified Instances: 44.24778761061947
Weighted Precision: 0.3629548814321298
Weighted AreaUnderROC: 0.8845084476436154
Root mean squared error: 0.19086933395714858
Relative absolute error: 61.276751937003255
Root relative squared error: 95.51761759569348
Weighted TruePositiveRate: 0.4424778761061947
Weighted MatthewsCorrelation: 0.29776939826856136
Weighted FMeasure: 0.3595725068913784
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5344109027941857
Mean absolute error: 0.04893629494969005
Coverage of cases: 64.60176991150442
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 2.0
Weighted Recall: 0.4424778761061947
Weighted FalsePositiveRate: 0.13915115215818852
Kappa statistic: 0.30274240940254654
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.259587020648965
Incorrectly Classified Instances: 48.67256637168141
Correctly Classified Instances: 51.32743362831859
Weighted Precision: 0.40009050683829445
Weighted AreaUnderROC: 0.9044770325583281
Root mean squared error: 0.1832918037150957
Relative absolute error: 54.796229605140894
Root relative squared error: 91.72555932748207
Weighted TruePositiveRate: 0.5132743362831859
Weighted MatthewsCorrelation: 0.36942350782491595
Weighted FMeasure: 0.41263506385838505
Iteration time: 1.0
Weighted AreaUnderPRC: 0.5789163336395098
Mean absolute error: 0.043760877809661085
Coverage of cases: 65.48672566371681
Instances selection time: 1.0
Test time: 30.0
Accumulative iteration time: 3.0
Weighted Recall: 0.5132743362831859
Weighted FalsePositiveRate: 0.11670415808645716
Kappa statistic: 0.39164056382145657
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.632743362831856
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.41991150442477876
Weighted AreaUnderROC: 0.9181693967403362
Root mean squared error: 0.1808961367164113
Relative absolute error: 51.55220471552681
Root relative squared error: 90.52668468626636
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.4058682909759717
Weighted FMeasure: 0.445846741073412
Iteration time: 1.0
Weighted AreaUnderPRC: 0.594247129816779
Mean absolute error: 0.04117016348809429
Coverage of cases: 63.716814159292035
Instances selection time: 1.0
Test time: 30.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.10509464603066537
Kappa statistic: 0.4282934423039501
Training time: 0.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.743362831858402
Incorrectly Classified Instances: 46.017699115044245
Correctly Classified Instances: 53.982300884955755
Weighted Precision: 0.43802529382956246
Weighted AreaUnderROC: 0.9229048190704917
Root mean squared error: 0.17121514008590233
Relative absolute error: 48.69650599411253
Root relative squared error: 85.68197907050852
Weighted TruePositiveRate: 0.5398230088495575
Weighted MatthewsCorrelation: 0.41704498097946713
Weighted FMeasure: 0.4620494260785776
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6291832037319816
Mean absolute error: 0.03888957075918705
Coverage of cases: 72.56637168141593
Instances selection time: 0.0
Test time: 30.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5398230088495575
Weighted FalsePositiveRate: 0.10009966426767304
Kappa statistic: 0.43216080402010043
Training time: 1.0
		
Time end:Tue Oct 31 11.18.46 EET 2017