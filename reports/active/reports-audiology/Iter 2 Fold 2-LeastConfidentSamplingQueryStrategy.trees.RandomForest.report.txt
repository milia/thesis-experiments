Tue Oct 31 11.17.36 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.36 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 14.085545722713865
Incorrectly Classified Instances: 46.902654867256636
Correctly Classified Instances: 53.097345132743364
Weighted Precision: 0.4091675286365552
Weighted AreaUnderROC: 0.8201144755298065
Root mean squared error: 0.16573810250571167
Relative absolute error: 64.01452606646919
Root relative squared error: 82.94107999418365
Weighted TruePositiveRate: 0.5309734513274337
Weighted MatthewsCorrelation: 0.3924733121290515
Weighted FMeasure: 0.44425852937045024
Iteration time: 4.0
Weighted AreaUnderPRC: 0.5439768798279717
Mean absolute error: 0.05112271178919404
Coverage of cases: 74.33628318584071
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.5309734513274337
Weighted FalsePositiveRate: 0.10484498714285638
Kappa statistic: 0.42612111920275975
Training time: 2.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 18.21533923303835
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.518067559647683
Weighted AreaUnderROC: 0.8522015302463413
Root mean squared error: 0.15912002476981527
Relative absolute error: 64.38156197447017
Root relative squared error: 79.62916495110059
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.4928167602275402
Weighted FMeasure: 0.5353321911190363
Iteration time: 5.0
Weighted AreaUnderPRC: 0.6297742911208231
Mean absolute error: 0.051415830743500375
Coverage of cases: 83.1858407079646
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.07063740210531262
Kappa statistic: 0.5271526873721406
Training time: 4.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 19.874631268436577
Incorrectly Classified Instances: 48.67256637168141
Correctly Classified Instances: 51.32743362831859
Weighted Precision: 0.47648090568444557
Weighted AreaUnderROC: 0.8420879605269
Root mean squared error: 0.16200797801939504
Relative absolute error: 65.8211904348136
Root relative squared error: 81.07439666228544
Weighted TruePositiveRate: 0.5132743362831859
Weighted MatthewsCorrelation: 0.3918595614593256
Weighted FMeasure: 0.4433954413439143
Iteration time: 8.0
Weighted AreaUnderPRC: 0.6190457169789816
Mean absolute error: 0.05256553402780242
Coverage of cases: 83.1858407079646
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 17.0
Weighted Recall: 0.5132743362831859
Weighted FalsePositiveRate: 0.10851005428950135
Kappa statistic: 0.40611562350692787
Training time: 7.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 16.629793510324482
Incorrectly Classified Instances: 27.43362831858407
Correctly Classified Instances: 72.56637168141593
Weighted Precision: 0.6806350858927641
Weighted AreaUnderROC: 0.928377249346298
Root mean squared error: 0.134424939168823
Relative absolute error: 50.298575596503944
Root relative squared error: 67.2708898210681
Weighted TruePositiveRate: 0.7256637168141593
Weighted MatthewsCorrelation: 0.6556920767288141
Weighted FMeasure: 0.6802088324182167
Iteration time: 10.0
Weighted AreaUnderPRC: 0.7899405507168875
Mean absolute error: 0.04016900134443015
Coverage of cases: 89.38053097345133
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.7256637168141593
Weighted FalsePositiveRate: 0.0571386586102778
Kappa statistic: 0.6713883677298311
Training time: 9.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 17.625368731563416
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.6293510324483776
Weighted AreaUnderROC: 0.9406260474693452
Root mean squared error: 0.13544922804620338
Relative absolute error: 51.74632350488197
Root relative squared error: 67.78347940928926
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.614611978398875
Weighted FMeasure: 0.652783990837088
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7939347958195203
Mean absolute error: 0.041325188910148705
Coverage of cases: 92.03539823008849
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.0509800008618101
Kappa statistic: 0.6352149049990776
Training time: 11.0
		
Time end:Tue Oct 31 11.17.37 EET 2017