Tue Oct 31 11.16.36 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.36 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 7.81710914454277
Incorrectly Classified Instances: 54.86725663716814
Correctly Classified Instances: 45.13274336283186
Weighted Precision: 0.25035719428046116
Weighted AreaUnderROC: 0.8508234638305091
Root mean squared error: 0.19101312240306279
Relative absolute error: 59.801601662318895
Root relative squared error: 95.58957430820863
Weighted TruePositiveRate: 0.45132743362831856
Weighted MatthewsCorrelation: 0.2656749670271782
Weighted FMeasure: 0.3216952560091037
Iteration time: 30.0
Weighted AreaUnderPRC: 0.5148430094850402
Mean absolute error: 0.047758223549768455
Coverage of cases: 53.097345132743364
Instances selection time: 30.0
Test time: 30.0
Accumulative iteration time: 30.0
Weighted Recall: 0.45132743362831856
Weighted FalsePositiveRate: 0.14044397912292092
Kappa statistic: 0.3050981948026185
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 7.817109144542768
Incorrectly Classified Instances: 48.67256637168141
Correctly Classified Instances: 51.32743362831859
Weighted Precision: 0.33415136865661255
Weighted AreaUnderROC: 0.9112710039937055
Root mean squared error: 0.18680402237485672
Relative absolute error: 56.08850536182474
Root relative squared error: 93.48319504559501
Weighted TruePositiveRate: 0.5132743362831859
Weighted MatthewsCorrelation: 0.3516373352284085
Weighted FMeasure: 0.3812643486450954
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6315136938759027
Mean absolute error: 0.04479290358756827
Coverage of cases: 58.4070796460177
Instances selection time: 17.0
Test time: 31.0
Accumulative iteration time: 48.0
Weighted Recall: 0.5132743362831859
Weighted FalsePositiveRate: 0.11771091144927948
Kappa statistic: 0.39128305582761996
Training time: 1.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.890855457227134
Incorrectly Classified Instances: 45.13274336283186
Correctly Classified Instances: 54.86725663716814
Weighted Precision: 0.4206349456240986
Weighted AreaUnderROC: 0.9190659012510977
Root mean squared error: 0.17784909709685648
Relative absolute error: 52.6760785257257
Root relative squared error: 89.00184065215396
Weighted TruePositiveRate: 0.5486725663716814
Weighted MatthewsCorrelation: 0.39277720132445637
Weighted FMeasure: 0.4183346751961983
Iteration time: 13.0
Weighted AreaUnderPRC: 0.6606814047069349
Mean absolute error: 0.04206770160040585
Coverage of cases: 61.06194690265487
Instances selection time: 13.0
Test time: 31.0
Accumulative iteration time: 61.0
Weighted Recall: 0.5486725663716814
Weighted FalsePositiveRate: 0.11707626843595213
Kappa statistic: 0.43361179361179353
Training time: 0.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 8.4070796460177
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.542731700624271
Weighted AreaUnderROC: 0.9520042025805374
Root mean squared error: 0.15882708940947562
Relative absolute error: 45.05021140378606
Root relative squared error: 79.48256996305783
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.48813967449534534
Weighted FMeasure: 0.5007627142786847
Iteration time: 9.0
Weighted AreaUnderPRC: 0.756976299575026
Mean absolute error: 0.03597759938496795
Coverage of cases: 74.33628318584071
Instances selection time: 8.0
Test time: 30.0
Accumulative iteration time: 70.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.08845718557558499
Kappa statistic: 0.5103043143297381
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 8.333333333333334
Incorrectly Classified Instances: 30.97345132743363
Correctly Classified Instances: 69.02654867256638
Weighted Precision: 0.5977392727002305
Weighted AreaUnderROC: 0.9637353436831592
Root mean squared error: 0.14123481221377343
Relative absolute error: 38.22525595110526
Root relative squared error: 70.67878587171826
Weighted TruePositiveRate: 0.6902654867256637
Weighted MatthewsCorrelation: 0.5973164898621234
Weighted FMeasure: 0.6174789392569111
Iteration time: 3.0
Weighted AreaUnderPRC: 0.7973976918902855
Mean absolute error: 0.03052711412761872
Coverage of cases: 81.41592920353982
Instances selection time: 3.0
Test time: 31.0
Accumulative iteration time: 73.0
Weighted Recall: 0.6902654867256637
Weighted FalsePositiveRate: 0.06707120872923782
Kappa statistic: 0.622866406026509
Training time: 0.0
		
Time end:Tue Oct 31 11.16.36 EET 2017