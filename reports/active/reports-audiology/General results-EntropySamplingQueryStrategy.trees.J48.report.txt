Tue Oct 31 11.15.15 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.15.15 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 10.272861356932152
Incorrectly Classified Instances: 45.92920353982301
Correctly Classified Instances: 54.07079646017699
Weighted Precision: 0.39106644671764246
Weighted AreaUnderROC: 0.770457569037696
Root mean squared error: 0.17572596042498567
Relative absolute error: 55.92528441162533
Root relative squared error: 87.93934961431816
Weighted TruePositiveRate: 0.54070796460177
Weighted MatthewsCorrelation: 0.39909328145576933
Weighted FMeasure: 0.43918204111495723
Iteration time: 12.2
Weighted AreaUnderPRC: 0.3966684144013171
Mean absolute error: 0.04466255352317297
Coverage of cases: 71.15044247787611
Instances selection time: 5.5
Test time: 5.8
Accumulative iteration time: 12.2
Weighted Recall: 0.54070796460177
Weighted FalsePositiveRate: 0.09767803262436742
Kappa statistic: 0.4361401213321427
Training time: 6.7
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 8.491887905604717
Incorrectly Classified Instances: 37.34513274336283
Correctly Classified Instances: 62.65486725663716
Weighted Precision: 0.4993593306366056
Weighted AreaUnderROC: 0.8212452486971522
Root mean squared error: 0.16268341778992074
Relative absolute error: 46.95540631516787
Root relative squared error: 81.41241008944223
Weighted TruePositiveRate: 0.6265486725663717
Weighted MatthewsCorrelation: 0.5100728614488875
Weighted FMeasure: 0.5431459565827959
Iteration time: 7.8
Weighted AreaUnderPRC: 0.4882323251337093
Mean absolute error: 0.03749910921002987
Coverage of cases: 75.22123893805309
Instances selection time: 3.2
Test time: 5.2
Accumulative iteration time: 20.0
Weighted Recall: 0.6265486725663717
Weighted FalsePositiveRate: 0.06971032219843241
Kappa statistic: 0.5499782035926795
Training time: 4.6
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 8.182153392330385
Incorrectly Classified Instances: 32.83185840707964
Correctly Classified Instances: 67.16814159292036
Weighted Precision: 0.5811164393608286
Weighted AreaUnderROC: 0.8474309380050344
Root mean squared error: 0.15355464449852776
Relative absolute error: 39.69097036294213
Root relative squared error: 76.844056136047
Weighted TruePositiveRate: 0.6716814159292035
Weighted MatthewsCorrelation: 0.5806578558502757
Weighted FMeasure: 0.6061007128549091
Iteration time: 9.2
Weighted AreaUnderPRC: 0.5510138741077775
Mean absolute error: 0.03169764994262737
Coverage of cases: 77.5221238938053
Instances selection time: 3.7
Test time: 5.2
Accumulative iteration time: 29.2
Weighted Recall: 0.6716814159292035
Weighted FalsePositiveRate: 0.05618880320666371
Kappa statistic: 0.6076374816522654
Training time: 5.5
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.754424778761065
Incorrectly Classified Instances: 29.646017699115042
Correctly Classified Instances: 70.35398230088495
Weighted Precision: 0.6171998588846852
Weighted AreaUnderROC: 0.8761103562361356
Root mean squared error: 0.1459267249666551
Relative absolute error: 36.22380086641205
Root relative squared error: 73.02678132405592
Weighted TruePositiveRate: 0.7035398230088495
Weighted MatthewsCorrelation: 0.6169844307081113
Weighted FMeasure: 0.6434713412116075
Iteration time: 10.6
Weighted AreaUnderPRC: 0.5979929414898004
Mean absolute error: 0.02892872985859294
Coverage of cases: 81.68141592920354
Instances selection time: 3.7
Test time: 4.9
Accumulative iteration time: 39.8
Weighted Recall: 0.7035398230088495
Weighted FalsePositiveRate: 0.05112111847991323
Kappa statistic: 0.6482000874802225
Training time: 6.9
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.861356932153394
Incorrectly Classified Instances: 25.92920353982301
Correctly Classified Instances: 74.070796460177
Weighted Precision: 0.6903549427525131
Weighted AreaUnderROC: 0.8988415171804766
Root mean squared error: 0.133568101714677
Relative absolute error: 32.4673809279722
Root relative squared error: 66.8420987178039
Weighted TruePositiveRate: 0.7407079646017699
Weighted MatthewsCorrelation: 0.6777892948422547
Weighted FMeasure: 0.7013528769977931
Iteration time: 13.5
Weighted AreaUnderPRC: 0.668979580504349
Mean absolute error: 0.02592881115775556
Coverage of cases: 84.86725663716814
Instances selection time: 4.4
Test time: 5.1
Accumulative iteration time: 53.3
Weighted Recall: 0.7407079646017699
Weighted FalsePositiveRate: 0.03808552241454447
Kappa statistic: 0.695171638584062
Training time: 9.1
		
Time end:Tue Oct 31 11.15.18 EET 2017