Tue Oct 31 11.20.28 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Tue Oct 31 11.20.28 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 40.00000000000001
Incorrectly Classified Instances: 46.19469026548673
Correctly Classified Instances: 53.80530973451327
Weighted Precision: 0.455207181015418
Weighted AreaUnderROC: 0.7907170602450662
Root mean squared error: 0.19397500698693754
Relative absolute error: 93.60823570580374
Root relative squared error: 97.07180381663576
Weighted TruePositiveRate: 0.5380530973451327
Weighted MatthewsCorrelation: 0.41708590030749393
Weighted FMeasure: 0.4639760668178837
Iteration time: 725.6
Weighted AreaUnderPRC: 0.4109695457350499
Mean absolute error: 0.07475657712616264
Coverage of cases: 82.0353982300885
Instances selection time: 3.5
Test time: 7.1
Accumulative iteration time: 725.6
Weighted Recall: 0.5380530973451327
Weighted FalsePositiveRate: 0.09335349390567932
Kappa statistic: 0.44069140569926396
Training time: 722.1
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 40.833333333333336
Incorrectly Classified Instances: 43.98230088495575
Correctly Classified Instances: 56.01769911504425
Weighted Precision: 0.4795785815753552
Weighted AreaUnderROC: 0.8063024280831537
Root mean squared error: 0.19393023700781
Relative absolute error: 93.713877326279
Root relative squared error: 97.04939937031948
Weighted TruePositiveRate: 0.5601769911504425
Weighted MatthewsCorrelation: 0.4444130255447429
Weighted FMeasure: 0.4814178419622194
Iteration time: 788.2
Weighted AreaUnderPRC: 0.4482346231855242
Mean absolute error: 0.07484094369806998
Coverage of cases: 82.47787610619469
Instances selection time: 3.1
Test time: 6.3
Accumulative iteration time: 1513.8
Weighted Recall: 0.5601769911504425
Weighted FalsePositiveRate: 0.08893548867496667
Kappa statistic: 0.4668611032844023
Training time: 785.1
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 42.112831858407084
Incorrectly Classified Instances: 38.230088495575224
Correctly Classified Instances: 61.76991150442477
Weighted Precision: 0.5396829883539379
Weighted AreaUnderROC: 0.8311280629355379
Root mean squared error: 0.19364424303462976
Relative absolute error: 93.7753440376384
Root relative squared error: 96.90627809253981
Weighted TruePositiveRate: 0.6176991150442477
Weighted MatthewsCorrelation: 0.5146290066395457
Weighted FMeasure: 0.5431197319479059
Iteration time: 896.6
Weighted AreaUnderPRC: 0.4950061694868907
Mean absolute error: 0.07489003169672505
Coverage of cases: 84.15929203539824
Instances selection time: 2.5
Test time: 6.8
Accumulative iteration time: 2410.4
Weighted Recall: 0.6176991150442477
Weighted FalsePositiveRate: 0.07307879724316566
Kappa statistic: 0.5398086566883615
Training time: 894.1
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 44.188790560471986
Incorrectly Classified Instances: 32.21238938053098
Correctly Classified Instances: 67.78761061946904
Weighted Precision: 0.6098288338305131
Weighted AreaUnderROC: 0.8655496405160836
Root mean squared error: 0.19336392695975302
Relative absolute error: 93.92592606222054
Root relative squared error: 96.7659982314909
Weighted TruePositiveRate: 0.6778761061946903
Weighted MatthewsCorrelation: 0.5850682754514067
Weighted FMeasure: 0.6073484571769594
Iteration time: 924.3
Weighted AreaUnderPRC: 0.5643246197536345
Mean absolute error: 0.07501028817468997
Coverage of cases: 86.01769911504427
Instances selection time: 1.6
Test time: 7.4
Accumulative iteration time: 3334.7
Weighted Recall: 0.6778761061946903
Weighted FalsePositiveRate: 0.06196339023111767
Kappa statistic: 0.6125537410496074
Training time: 922.7
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 55.859144542772846
Incorrectly Classified Instances: 24.247787610619472
Correctly Classified Instances: 75.75221238938052
Weighted Precision: 0.6909666412950567
Weighted AreaUnderROC: 0.9133313887190205
Root mean squared error: 0.19335735050794448
Relative absolute error: 95.11483619664114
Root relative squared error: 96.7627071475019
Weighted TruePositiveRate: 0.7575221238938054
Weighted MatthewsCorrelation: 0.68885812416368
Weighted FMeasure: 0.7128918204674546
Iteration time: 1767.2
Weighted AreaUnderPRC: 0.6740714823824043
Mean absolute error: 0.07595976501815085
Coverage of cases: 91.76991150442478
Instances selection time: 1.1
Test time: 10.4
Accumulative iteration time: 5101.9
Weighted Recall: 0.7575221238938054
Weighted FalsePositiveRate: 0.04061441389138212
Kappa statistic: 0.7127858591245351
Training time: 1766.1
		
Time end:Tue Oct 31 11.21.21 EET 2017