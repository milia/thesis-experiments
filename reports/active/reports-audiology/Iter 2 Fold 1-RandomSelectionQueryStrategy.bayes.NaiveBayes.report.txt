Tue Oct 31 11.18.44 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Tue Oct 31 11.18.44 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 7.522123893805319
Incorrectly Classified Instances: 69.91150442477876
Correctly Classified Instances: 30.088495575221238
Weighted Precision: 0.10390936497131187
Weighted AreaUnderROC: 0.8278496221128224
Root mean squared error: 0.21963332686203035
Relative absolute error: 70.73020253055574
Root relative squared error: 109.9121146993013
Weighted TruePositiveRate: 0.3008849557522124
Weighted MatthewsCorrelation: 0.09512206740599446
Weighted FMeasure: 0.15353982300884955
Iteration time: 3.0
Weighted AreaUnderPRC: 0.47725083226916293
Mean absolute error: 0.056485925632041106
Coverage of cases: 46.902654867256636
Instances selection time: 3.0
Test time: 30.0
Accumulative iteration time: 3.0
Weighted Recall: 0.3008849557522124
Weighted FalsePositiveRate: 0.20564686051411715
Kappa statistic: 0.09718851132686086
Training time: 0.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 7.448377581120949
Incorrectly Classified Instances: 65.48672566371681
Correctly Classified Instances: 34.51327433628319
Weighted Precision: 0.24146521787834657
Weighted AreaUnderROC: 0.8820344317254597
Root mean squared error: 0.21215135897580212
Relative absolute error: 66.86338524061644
Root relative squared error: 106.16787913980355
Weighted TruePositiveRate: 0.34513274336283184
Weighted MatthewsCorrelation: 0.16818752946545934
Weighted FMeasure: 0.22454932808915112
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5626803604836693
Mean absolute error: 0.05339784237965902
Coverage of cases: 56.63716814159292
Instances selection time: 2.0
Test time: 31.0
Accumulative iteration time: 5.0
Weighted Recall: 0.34513274336283184
Weighted FalsePositiveRate: 0.17933144706862095
Kappa statistic: 0.16713147410358561
Training time: 0.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.817109144542782
Incorrectly Classified Instances: 53.982300884955755
Correctly Classified Instances: 46.017699115044245
Weighted Precision: 0.32620158602459487
Weighted AreaUnderROC: 0.9093507583274675
Root mean squared error: 0.18669630809117646
Relative absolute error: 56.038856542385545
Root relative squared error: 93.42929109180177
Weighted TruePositiveRate: 0.46017699115044247
Weighted MatthewsCorrelation: 0.30129883816590897
Weighted FMeasure: 0.3625765827093261
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6431361448528017
Mean absolute error: 0.04475325348871073
Coverage of cases: 67.2566371681416
Instances selection time: 0.0
Test time: 31.0
Accumulative iteration time: 6.0
Weighted Recall: 0.46017699115044247
Weighted FalsePositiveRate: 0.13355533125052746
Kappa statistic: 0.32235548564687383
Training time: 1.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.817109144542776
Incorrectly Classified Instances: 47.78761061946903
Correctly Classified Instances: 52.21238938053097
Weighted Precision: 0.42172773663225455
Weighted AreaUnderROC: 0.9228377029978918
Root mean squared error: 0.1794059367776629
Relative absolute error: 51.79467594469977
Root relative squared error: 89.78093708533189
Weighted TruePositiveRate: 0.5221238938053098
Weighted MatthewsCorrelation: 0.39499568647556127
Weighted FMeasure: 0.43365881070609646
Iteration time: 1.0
Weighted AreaUnderPRC: 0.6580492906323062
Mean absolute error: 0.04136380370583667
Coverage of cases: 69.91150442477876
Instances selection time: 0.0
Test time: 35.0
Accumulative iteration time: 7.0
Weighted Recall: 0.5221238938053098
Weighted FalsePositiveRate: 0.10108954791903331
Kappa statistic: 0.40745775878811424
Training time: 1.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.853982300884959
Incorrectly Classified Instances: 39.823008849557525
Correctly Classified Instances: 60.176991150442475
Weighted Precision: 0.5142415475380078
Weighted AreaUnderROC: 0.9444194198767005
Root mean squared error: 0.16014061417362904
Relative absolute error: 43.14429449613963
Root relative squared error: 80.13990319477035
Weighted TruePositiveRate: 0.6017699115044248
Weighted MatthewsCorrelation: 0.49511122704318244
Weighted FMeasure: 0.5290454219657759
Iteration time: 1.0
Weighted AreaUnderPRC: 0.7196187247525717
Mean absolute error: 0.0344555129656671
Coverage of cases: 77.87610619469027
Instances selection time: 1.0
Test time: 30.0
Accumulative iteration time: 8.0
Weighted Recall: 0.6017699115044248
Weighted FalsePositiveRate: 0.08004624597319755
Kappa statistic: 0.5140017203478926
Training time: 0.0
		
Time end:Tue Oct 31 11.18.44 EET 2017