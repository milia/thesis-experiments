Tue Oct 31 11.16.34 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.16.34 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 6.692477876106194
Incorrectly Classified Instances: 64.60176991150442
Correctly Classified Instances: 35.39823008849557
Weighted Precision: 0.2263121506898813
Weighted AreaUnderROC: 0.8270362419197411
Root mean squared error: 0.21573476781448972
Relative absolute error: 68.30493795257254
Root relative squared error: 107.96114088618567
Weighted TruePositiveRate: 0.35398230088495575
Weighted MatthewsCorrelation: 0.16725176079258514
Weighted FMeasure: 0.2278743772918642
Iteration time: 26.1
Weighted AreaUnderPRC: 0.4784627593822971
Mean absolute error: 0.05454908239267943
Coverage of cases: 45.48672566371682
Instances selection time: 25.7
Test time: 34.5
Accumulative iteration time: 26.1
Weighted Recall: 0.35398230088495575
Weighted FalsePositiveRate: 0.1879496997085408
Kappa statistic: 0.1672079490089871
Training time: 0.4
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 7.809734513274336
Incorrectly Classified Instances: 54.60176991150441
Correctly Classified Instances: 45.398230088495566
Weighted Precision: 0.3341418973843031
Weighted AreaUnderROC: 0.8722787470602601
Root mean squared error: 0.19387512005158483
Relative absolute error: 59.28691755412634
Root relative squared error: 97.02181693872289
Weighted TruePositiveRate: 0.45398230088495567
Weighted MatthewsCorrelation: 0.29260941100930304
Weighted FMeasure: 0.33745187212222133
Iteration time: 18.8
Weighted AreaUnderPRC: 0.5659088380883892
Mean absolute error: 0.04734719110225363
Coverage of cases: 59.46902654867257
Instances selection time: 18.2
Test time: 30.6
Accumulative iteration time: 44.9
Weighted Recall: 0.45398230088495567
Weighted FalsePositiveRate: 0.13918781973582406
Kappa statistic: 0.314333836766468
Training time: 0.6
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 7.7064896755162255
Incorrectly Classified Instances: 43.89380530973451
Correctly Classified Instances: 56.10619469026549
Weighted Precision: 0.4453621332616948
Weighted AreaUnderROC: 0.9112124518400595
Root mean squared error: 0.17488790379350022
Relative absolute error: 50.39721511621428
Root relative squared error: 87.51995708441218
Weighted TruePositiveRate: 0.5610619469026549
Weighted MatthewsCorrelation: 0.4257063800557551
Weighted FMeasure: 0.4613340616703444
Iteration time: 13.6
Weighted AreaUnderPRC: 0.6469750971081074
Mean absolute error: 0.04024777596086553
Coverage of cases: 67.78761061946902
Instances selection time: 13.1
Test time: 32.6
Accumulative iteration time: 58.5
Weighted Recall: 0.5610619469026549
Weighted FalsePositiveRate: 0.10837139246512575
Kappa statistic: 0.45424920525171536
Training time: 0.5
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 7.7544247787610585
Incorrectly Classified Instances: 39.20353982300885
Correctly Classified Instances: 60.79646017699115
Weighted Precision: 0.5033063197952001
Weighted AreaUnderROC: 0.9359445214302486
Root mean squared error: 0.16305032532162625
Relative absolute error: 45.06214242603001
Root relative squared error: 81.59602331101031
Weighted TruePositiveRate: 0.6079646017699116
Weighted MatthewsCorrelation: 0.4883179259855367
Weighted FMeasure: 0.5182909959752007
Iteration time: 9.1
Weighted AreaUnderPRC: 0.7035836820686014
Mean absolute error: 0.03598712763189894
Coverage of cases: 73.80530973451327
Instances selection time: 8.5
Test time: 30.8
Accumulative iteration time: 67.6
Weighted Recall: 0.6079646017699116
Weighted FalsePositiveRate: 0.09045556876839952
Kappa statistic: 0.5168988906688282
Training time: 0.6
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 7.787610619469025
Incorrectly Classified Instances: 33.982300884955755
Correctly Classified Instances: 66.01769911504424
Weighted Precision: 0.5692479647948542
Weighted AreaUnderROC: 0.9520192657128466
Root mean squared error: 0.1516485746031055
Relative absolute error: 40.184686378492785
Root relative squared error: 75.89019282229707
Weighted TruePositiveRate: 0.6601769911504424
Weighted MatthewsCorrelation: 0.5597303205233473
Weighted FMeasure: 0.5886786757576565
Iteration time: 3.8
Weighted AreaUnderPRC: 0.7505187846710902
Mean absolute error: 0.032091937038379634
Coverage of cases: 79.38053097345133
Instances selection time: 3.2
Test time: 30.8
Accumulative iteration time: 71.4
Weighted Recall: 0.6601769911504424
Weighted FalsePositiveRate: 0.07451873401604034
Kappa statistic: 0.5851169721111154
Training time: 0.6
		
Time end:Tue Oct 31 11.16.38 EET 2017