Tue Oct 31 11.17.37 EET 2017
Dataset: audiology
Test set size: 113
Initial Labelled set size: 22
Initial Unlabelled set size: 91
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Tue Oct 31 11.17.37 EET 2017
		
Iteration: 1
Labeled set size: 22
Unlabelled set size: 91
	
Mean region size: 20.28023598820058
Incorrectly Classified Instances: 60.176991150442475
Correctly Classified Instances: 39.823008849557525
Weighted Precision: 0.36651384071998344
Weighted AreaUnderROC: 0.7630898093869011
Root mean squared error: 0.17822250730545847
Relative absolute error: 73.24014386137517
Root relative squared error: 89.1887080381929
Weighted TruePositiveRate: 0.39823008849557523
Weighted MatthewsCorrelation: 0.2901301831467641
Weighted FMeasure: 0.34461549355072063
Iteration time: 5.0
Weighted AreaUnderPRC: 0.4342242800080814
Mean absolute error: 0.058490392667070394
Coverage of cases: 77.87610619469027
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.39823008849557523
Weighted FalsePositiveRate: 0.0869139422060787
Kappa statistic: 0.29800840489676594
Training time: 3.0
		
Iteration: 2
Labeled set size: 42
Unlabelled set size: 71
	
Mean region size: 23.7094395280236
Incorrectly Classified Instances: 56.63716814159292
Correctly Classified Instances: 43.36283185840708
Weighted Precision: 0.40952122193389073
Weighted AreaUnderROC: 0.805762681965836
Root mean squared error: 0.17287106261723206
Relative absolute error: 73.5655909082006
Root relative squared error: 86.51065998973439
Weighted TruePositiveRate: 0.4336283185840708
Weighted MatthewsCorrelation: 0.3305171352887652
Weighted FMeasure: 0.39378543297045043
Iteration time: 9.0
Weighted AreaUnderPRC: 0.503799880125705
Mean absolute error: 0.05875029829474347
Coverage of cases: 86.72566371681415
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 14.0
Weighted Recall: 0.4336283185840708
Weighted FalsePositiveRate: 0.08808558706519376
Kappa statistic: 0.33523301774060116
Training time: 8.0
		
Iteration: 3
Labeled set size: 62
Unlabelled set size: 51
	
Mean region size: 21.20206489675516
Incorrectly Classified Instances: 38.05309734513274
Correctly Classified Instances: 61.94690265486726
Weighted Precision: 0.6192189303578299
Weighted AreaUnderROC: 0.8981673576045487
Root mean squared error: 0.14897368638796052
Relative absolute error: 61.1430936802215
Root relative squared error: 74.55158622505915
Weighted TruePositiveRate: 0.6194690265486725
Weighted MatthewsCorrelation: 0.5529487418314404
Weighted FMeasure: 0.6018766055818274
Iteration time: 8.0
Weighted AreaUnderPRC: 0.7267004922474499
Mean absolute error: 0.048829553980732396
Coverage of cases: 92.03539823008849
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 22.0
Weighted Recall: 0.6194690265486725
Weighted FalsePositiveRate: 0.060408214716687544
Kappa statistic: 0.5549143537601905
Training time: 7.0
		
Iteration: 4
Labeled set size: 82
Unlabelled set size: 31
	
Mean region size: 20.095870206489682
Incorrectly Classified Instances: 31.858407079646017
Correctly Classified Instances: 68.14159292035399
Weighted Precision: 0.656735840364159
Weighted AreaUnderROC: 0.9159642682591916
Root mean squared error: 0.1433354982978185
Relative absolute error: 57.83320430958051
Root relative squared error: 71.73004185875625
Weighted TruePositiveRate: 0.6814159292035398
Weighted MatthewsCorrelation: 0.6143071188461606
Weighted FMeasure: 0.6581648328742159
Iteration time: 12.0
Weighted AreaUnderPRC: 0.7694359256361227
Mean absolute error: 0.04618623955278994
Coverage of cases: 92.03539823008849
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 34.0
Weighted Recall: 0.6814159292035398
Weighted FalsePositiveRate: 0.05925548120836426
Kappa statistic: 0.6222490481938898
Training time: 11.0
		
Iteration: 5
Labeled set size: 102
Unlabelled set size: 11
	
Mean region size: 18.58407079646018
Incorrectly Classified Instances: 28.31858407079646
Correctly Classified Instances: 71.68141592920354
Weighted Precision: 0.7079477454698693
Weighted AreaUnderROC: 0.9355857684055365
Root mean squared error: 0.1340951712474242
Relative absolute error: 52.616367827424654
Root relative squared error: 67.10586254529537
Weighted TruePositiveRate: 0.7168141592920354
Weighted MatthewsCorrelation: 0.6677376578826645
Weighted FMeasure: 0.6984049839672053
Iteration time: 14.0
Weighted AreaUnderPRC: 0.810680350226808
Mean absolute error: 0.04202001597329048
Coverage of cases: 92.92035398230088
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 48.0
Weighted Recall: 0.7168141592920354
Weighted FalsePositiveRate: 0.03774766343961785
Kappa statistic: 0.6702835780067475
Training time: 13.0
		
Time end:Tue Oct 31 11.17.37 EET 2017