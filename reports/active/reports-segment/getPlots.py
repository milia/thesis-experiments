import matplotlib.pyplot as plt
import pylab
import numpy as np

def getList(inputfile):
	f = open(inputfile,'r')
	alist = []
	for line in f:
		alist.append(round(float(line[32:]),4))
	return alist

alist = getList('EntropySamplingRF.res')
alist2 = getList('LeastRF.res')
alist3 = getList('MarginRF.res')
alist4 = getList('VoteEntropyRF.res')
alist5 = getList('KullbackRF.res')
alist6 = getList('RandomSelectionRF.res')

print len(alist)

t = range(len(alist))
plt.ylabel('Correctly Classified Instances (%)')
plt.xlabel('Iteration')
plt.title('All strategies - Random Forest - segment.arff')
pylab.plot(t, alist, 'r--', label = 'Entropy Sampling')
pylab.plot(t, alist2, 'b--', label = 'Least Confident')
pylab.plot(t, alist3, 'g--', label = 'Margin Sampling')
pylab.plot(t, alist4, 'k-', label = 'Vote Entropy')
pylab.plot(t, alist5, '-r', label = 'Kullback-Leibler')
pylab.plot(t, alist6, '-b', label = 'Random')
#pylab.legend(loc='center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=3)
plt.show()

