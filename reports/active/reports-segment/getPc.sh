less General*EntropySampling*RandomForest* | grep "Correctly Classified Instances" > EntropySamplingRF.res
#less General*EntropySampling*SMO* | grep "Correctly Classified Instances"
#less General*Least*SMO* | grep "Correctly Classified Instances"
less General*Least*RandomForest* | grep "Correctly Classified Instances" > LeastRF.res
#less General*Margin*SMO* | grep "Correctly Classified Instances"
less General*Margin*RandomForest* | grep "Correctly Classified Instances" > MarginRF.res
#less General*VoteEntropy*SMO* | grep "Correctly Classified Instances"
less General*VoteEntropy*RandomForest* | grep "Correctly Classified Instances" > VoteEntropyRF.res
#less General*Kullback*SMO* | grep "Correctly Classified Instances"
less General*Kullback*RandomForest* | grep "Correctly Classified Instances" > KullbackRF.res
less General*RandomSelection*RandomForest* | grep "Correctly Classified Instances" > RandomSelectionRF.res
