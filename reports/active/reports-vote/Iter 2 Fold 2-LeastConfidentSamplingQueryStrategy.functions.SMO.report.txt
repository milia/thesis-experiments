Wed Nov 01 14.46.23 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.23 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9030488830623409
Weighted AreaUnderROC: 0.8969298245614035
Root mean squared error: 0.3110855084191276
Relative absolute error: 19.35483870967742
Root relative squared error: 62.21710168382552
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7956420835574817
Weighted FMeasure: 0.903117288438708
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8640318024236352
Mean absolute error: 0.0967741935483871
Coverage of cases: 90.3225806451613
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.10936615732880588
Kappa statistic: 0.7956043956043956
Training time: 6.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.909554264392974
Weighted AreaUnderROC: 0.8919172932330828
Root mean squared error: 0.3035883703594581
Relative absolute error: 18.433179723502306
Root relative squared error: 60.71767407189162
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.8053895820753685
Weighted FMeasure: 0.9065871310975789
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8653559085400916
Mean absolute error: 0.09216589861751152
Coverage of cases: 90.78341013824885
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.12399951491632306
Kappa statistic: 0.8014093529788596
Training time: 6.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9086274212501422
Weighted AreaUnderROC: 0.881578947368421
Root mean squared error: 0.3110855084191276
Relative absolute error: 19.35483870967742
Root relative squared error: 62.21710168382552
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7982234123311392
Weighted FMeasure: 0.9010649145434778
Iteration time: 7.0
Weighted AreaUnderPRC: 0.8577238506207424
Mean absolute error: 0.0967741935483871
Coverage of cases: 90.3225806451613
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.1400679117147708
Kappa statistic: 0.7891156462585034
Training time: 6.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.8988760850612183
Weighted AreaUnderROC: 0.8718671679197995
Root mean squared error: 0.32556225734894123
Relative absolute error: 21.19815668202765
Root relative squared error: 65.11245146978824
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.777907381311217
Weighted FMeasure: 0.8916425254523803
Iteration time: 8.0
Weighted AreaUnderPRC: 0.8455185477422278
Mean absolute error: 0.10599078341013825
Coverage of cases: 89.40092165898618
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.15027488075026274
Kappa statistic: 0.7690314220926466
Training time: 7.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 50.0
Incorrectly Classified Instances: 8.294930875576037
Correctly Classified Instances: 91.70506912442396
Weighted Precision: 0.9222295369760807
Weighted AreaUnderROC: 0.8972431077694235
Root mean squared error: 0.2880092164423916
Relative absolute error: 16.589861751152075
Root relative squared error: 57.601843288478314
Weighted TruePositiveRate: 0.9170506912442397
Weighted MatthewsCorrelation: 0.8278356837283092
Weighted FMeasure: 0.9153540008378718
Iteration time: 21.0
Weighted AreaUnderPRC: 0.8765806188686266
Mean absolute error: 0.08294930875576037
Coverage of cases: 91.70506912442396
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9170506912442397
Weighted FalsePositiveRate: 0.12256447570539251
Kappa statistic: 0.8196509372979962
Training time: 15.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 50.0
Incorrectly Classified Instances: 11.52073732718894
Correctly Classified Instances: 88.47926267281106
Weighted Precision: 0.8961737187543639
Weighted AreaUnderROC: 0.8555764411027569
Root mean squared error: 0.3394221166510653
Relative absolute error: 23.04147465437788
Root relative squared error: 67.88442333021307
Weighted TruePositiveRate: 0.8847926267281107
Weighted MatthewsCorrelation: 0.7631287279913898
Weighted FMeasure: 0.8807784325374525
Iteration time: 16.0
Weighted AreaUnderPRC: 0.8323852039475644
Mean absolute error: 0.1152073732718894
Coverage of cases: 88.47926267281106
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 66.0
Weighted Recall: 0.8847926267281107
Weighted FalsePositiveRate: 0.17363974452259684
Kappa statistic: 0.7454844006568145
Training time: 11.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 50.0
Incorrectly Classified Instances: 8.755760368663594
Correctly Classified Instances: 91.2442396313364
Weighted Precision: 0.9149451318309029
Weighted AreaUnderROC: 0.8956766917293233
Root mean squared error: 0.29590134113693356
Relative absolute error: 17.51152073732719
Root relative squared error: 59.18026822738671
Weighted TruePositiveRate: 0.9124423963133641
Weighted MatthewsCorrelation: 0.8158179393208759
Weighted FMeasure: 0.9111136001020651
Iteration time: 12.0
Weighted AreaUnderPRC: 0.8711508875498656
Mean absolute error: 0.08755760368663594
Coverage of cases: 91.2442396313364
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9124423963133641
Weighted FalsePositiveRate: 0.12108901285471745
Kappa statistic: 0.8109149277688604
Training time: 7.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9327214902317129
Weighted AreaUnderROC: 0.9172932330827068
Root mean squared error: 0.26291524102480945
Relative absolute error: 13.82488479262673
Root relative squared error: 52.58304820496189
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8547950478087362
Weighted FMeasure: 0.9300503513528466
Iteration time: 10.0
Weighted AreaUnderPRC: 0.8968569376718166
Mean absolute error: 0.06912442396313365
Coverage of cases: 93.08755760368663
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 88.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.09628910987145282
Kappa statistic: 0.8513902205177372
Training time: 9.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9404282138228559
Weighted AreaUnderROC: 0.931390977443609
Root mean squared error: 0.24476076912238712
Relative absolute error: 11.981566820276496
Root relative squared error: 48.95215382447742
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8734004833134899
Weighted FMeasure: 0.9397259893833848
Iteration time: 24.0
Weighted AreaUnderPRC: 0.9111749863515047
Mean absolute error: 0.059907834101382486
Coverage of cases: 94.00921658986175
Instances selection time: 7.0
Test time: 1.0
Accumulative iteration time: 112.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.07731021101139947
Kappa statistic: 0.8723471650300919
Training time: 17.0
		
Time end:Wed Nov 01 14.46.23 EET 2017