Wed Nov 01 14.45.47 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.47 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 51.61290322580645
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.2080482560204758
Relative absolute error: 12.153038259564894
Root relative squared error: 41.60965120409516
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.06076519129782448
Coverage of cases: 96.31336405529954
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 69.5852534562212
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9481178120617109
Weighted AreaUnderROC: 0.9721177944862155
Root mean squared error: 0.21750366846054478
Relative absolute error: 18.438969469796348
Root relative squared error: 43.50073369210896
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8880604771841482
Weighted FMeasure: 0.9450999113414718
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9578402438389036
Mean absolute error: 0.09219484734898174
Coverage of cases: 98.61751152073732
Instances selection time: 0.0
Test time: 4.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.043697954563828925
Kappa statistic: 0.8854679802955665
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9443288241415192
Weighted AreaUnderROC: 0.9721177944862155
Root mean squared error: 0.22041648674068887
Relative absolute error: 16.329873005396248
Root relative squared error: 44.083297348137776
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8794269798371535
Weighted FMeasure: 0.9405662765688124
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9578402438389036
Mean absolute error: 0.08164936502698124
Coverage of cases: 96.31336405529954
Instances selection time: 1.0
Test time: 7.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04660845662543455
Kappa statistic: 0.8761904761904763
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9443288241415192
Weighted AreaUnderROC: 0.9721177944862155
Root mean squared error: 0.2193213147052247
Relative absolute error: 15.275892660343015
Root relative squared error: 43.86426294104494
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8794269798371535
Weighted FMeasure: 0.9405662765688124
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9578402438389036
Mean absolute error: 0.07637946330171508
Coverage of cases: 97.23502304147465
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04660845662543455
Kappa statistic: 0.8761904761904763
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9458046329764258
Weighted AreaUnderROC: 0.930764411027569
Root mean squared error: 0.23086867437792524
Relative absolute error: 16.51305651249317
Root relative squared error: 46.17373487558505
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8851286016561534
Weighted FMeasure: 0.9449213448400267
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9265095469317969
Mean absolute error: 0.08256528256246584
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.052469884388390335
Kappa statistic: 0.884472049689441
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.948121050881277
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20643848097487055
Relative absolute error: 17.64732421278906
Root relative squared error: 41.28769619497411
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8839314341336624
Weighted FMeasure: 0.9407044181284223
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.0882366210639453
Coverage of cases: 100.0
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.03783652680087315
Kappa statistic: 0.8772464209564423
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.948121050881277
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20425882223640096
Relative absolute error: 17.129559672398635
Root relative squared error: 40.85176444728019
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8839314341336624
Weighted FMeasure: 0.9407044181284223
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.08564779836199318
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.03783652680087315
Kappa statistic: 0.8772464209564423
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20260836679259656
Relative absolute error: 16.778813131773624
Root relative squared error: 40.52167335851931
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.08389406565886812
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20162956128779072
Relative absolute error: 16.49717920999094
Root relative squared error: 40.325912257558144
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.0824858960499547
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 25.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Time end:Wed Nov 01 14.45.47 EET 2017