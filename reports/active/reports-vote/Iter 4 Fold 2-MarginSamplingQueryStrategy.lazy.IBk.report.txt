Wed Nov 01 14.46.49 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.49 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 52.07373271889401
Incorrectly Classified Instances: 11.059907834101383
Correctly Classified Instances: 88.94009216589862
Weighted Precision: 0.9043148255094464
Weighted AreaUnderROC: 0.9299140708915143
Root mean squared error: 0.3121463024493946
Relative absolute error: 23.853898730364946
Root relative squared error: 62.42926048987892
Weighted TruePositiveRate: 0.8894009216589862
Weighted MatthewsCorrelation: 0.7869771402327321
Weighted FMeasure: 0.8907388137356919
Iteration time: 5.0
Weighted AreaUnderPRC: 0.910421174688469
Mean absolute error: 0.11926949365182472
Coverage of cases: 91.2442396313364
Instances selection time: 5.0
Test time: 4.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8894009216589862
Weighted FalsePositiveRate: 0.08300994421537715
Kappa statistic: 0.7757685352622062
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 52.30414746543779
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9261186264308013
Weighted AreaUnderROC: 0.9583780880773363
Root mean squared error: 0.2592463248994357
Relative absolute error: 17.247651991097097
Root relative squared error: 51.849264979887145
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8411910241920598
Weighted FMeasure: 0.9222789770515238
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9460891453823325
Mean absolute error: 0.0862382599554855
Coverage of cases: 94.47004608294931
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.06702239469641845
Kappa statistic: 0.8380952380952381
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 51.61290322580645
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9225036365192093
Weighted AreaUnderROC: 0.9479054779806659
Root mean squared error: 0.27086683947143214
Relative absolute error: 17.548510413447804
Root relative squared error: 54.17336789428643
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8363292510119198
Weighted FMeasure: 0.9218993761848506
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9349073310829118
Mean absolute error: 0.08774255206723902
Coverage of cases: 93.54838709677419
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.08018028943326055
Kappa statistic: 0.8359788359788359
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9262672811059908
Weighted AreaUnderROC: 0.9548424633011099
Root mean squared error: 0.25606494370715965
Relative absolute error: 18.170272659402155
Root relative squared error: 51.21298874143193
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8446115288220551
Weighted FMeasure: 0.9262672811059908
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9413672437646011
Mean absolute error: 0.09085136329701077
Coverage of cases: 95.39170506912443
Instances selection time: 5.0
Test time: 7.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.08165575228393564
Kappa statistic: 0.8446115288220551
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 55.29953917050691
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.923426739760783
Weighted AreaUnderROC: 0.9540570897289307
Root mean squared error: 0.2564910876779164
Relative absolute error: 17.511563555050362
Root relative squared error: 51.29821753558328
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.837657259920533
Weighted FMeasure: 0.9220406888978905
Iteration time: 4.0
Weighted AreaUnderPRC: 0.942393199726963
Mean absolute error: 0.0875578177752518
Coverage of cases: 94.93087557603687
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.07579432452097987
Kappa statistic: 0.8366904245429192
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 56.22119815668203
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9298962131837307
Weighted AreaUnderROC: 0.9644647332617258
Root mean squared error: 0.2521016598024641
Relative absolute error: 17.37767336574037
Root relative squared error: 50.42033196049282
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8497713606435244
Weighted FMeasure: 0.9267998817886289
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9543088552427047
Mean absolute error: 0.08688836682870185
Coverage of cases: 95.39170506912443
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 26.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06411189263481283
Kappa statistic: 0.8472906403940886
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 55.99078341013825
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9298962131837307
Weighted AreaUnderROC: 0.9562298603651987
Root mean squared error: 0.26151410738382636
Relative absolute error: 18.20217190597236
Root relative squared error: 52.302821476765274
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8497713606435244
Weighted FMeasure: 0.9267998817886289
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9429314398417236
Mean absolute error: 0.09101085952986179
Coverage of cases: 94.93087557603687
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06411189263481283
Kappa statistic: 0.8472906403940886
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9337508369766434
Weighted AreaUnderROC: 0.9577515216612961
Root mean squared error: 0.2565444070728621
Relative absolute error: 17.087459648325673
Root relative squared error: 51.30888141457242
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8584521129178453
Weighted FMeasure: 0.9313239506787893
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9460148440874537
Mean absolute error: 0.08543729824162836
Coverage of cases: 94.93087557603687
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.061201390573207205
Kappa statistic: 0.8565257636531934
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 56.22119815668203
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9337508369766434
Weighted AreaUnderROC: 0.956811672037236
Root mean squared error: 0.2607447964315412
Relative absolute error: 17.852453097500483
Root relative squared error: 52.14895928630824
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8584521129178453
Weighted FMeasure: 0.9313239506787893
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9447163497180988
Mean absolute error: 0.08926226548750242
Coverage of cases: 94.93087557603687
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.061201390573207205
Kappa statistic: 0.8565257636531934
Training time: 0.0
		
Time end:Wed Nov 01 14.46.49 EET 2017