Wed Nov 01 14.46.03 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.03 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9356481908046279
Weighted AreaUnderROC: 0.9299928926794598
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8639864565185936
Weighted FMeasure: 0.9356308004699737
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9067901597585912
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07579403115484178
Kappa statistic: 0.8638229519900055
Training time: 8.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.936216226700545
Weighted AreaUnderROC: 0.9344349680170575
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8651882672018902
Weighted FMeasure: 0.9359161530377482
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9093865647587341
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 2.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.06690988047964633
Kappa statistic: 0.8650274190695207
Training time: 18.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.9278437159171105
Weighted AreaUnderROC: 0.9269722814498934
Root mean squared error: 0.27091418459143857
Relative absolute error: 14.678899082568808
Root relative squared error: 54.18283691828771
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.8470493386130592
Weighted FMeasure: 0.9269029100477789
Iteration time: 8.0
Weighted AreaUnderPRC: 0.89849542343861
Mean absolute error: 0.07339449541284404
Coverage of cases: 92.66055045871559
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.07266094168736918
Kappa statistic: 0.8464247974638958
Training time: 7.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9340627587394906
Weighted AreaUnderROC: 0.9351457000710731
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8588833378427607
Weighted FMeasure: 0.9316461035537277
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9071486553909027
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 46.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.060901260408312306
Kappa statistic: 0.8569678943224563
Training time: 8.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9392937869204323
Weighted AreaUnderROC: 0.9410980810234542
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8693166946660285
Weighted FMeasure: 0.9362513633155836
Iteration time: 22.0
Weighted AreaUnderPRC: 0.9142388860339151
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.053583654466853155
Kappa statistic: 0.8667946927374303
Training time: 21.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9328807307486082
Weighted AreaUnderROC: 0.9329246624022743
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8573230619003901
Weighted FMeasure: 0.9315329241029795
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9054623323424001
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 78.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06534333574591003
Kappa statistic: 0.856339513223794
Training time: 9.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9379778287461773
Weighted AreaUnderROC: 0.9388770433546552
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8676381962794484
Weighted FMeasure: 0.9361517154441897
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9124962592000299
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.05802572980445088
Kappa statistic: 0.8662107662633702
Training time: 10.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.9448294243070361
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9195987134596851
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 99.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 9.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.9448294243070361
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9195987134596851
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 112.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 12.0
		
Time end:Wed Nov 01 14.46.04 EET 2017