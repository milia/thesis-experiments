Wed Nov 01 14.45.43 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.43 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9827694235588973
Root mean squared error: 0.20562085219496554
Relative absolute error: 14.893355083475445
Root relative squared error: 41.124170438993104
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9756419658130933
Mean absolute error: 0.07446677541737723
Coverage of cases: 97.6958525345622
Instances selection time: 11.0
Test time: 9.0
Accumulative iteration time: 12.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9584752104228179
Weighted AreaUnderROC: 0.9767275331185106
Root mean squared error: 0.18942964208857582
Relative absolute error: 11.896316768474016
Root relative squared error: 37.885928417715164
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9124451045059361
Weighted FMeasure: 0.9584788379023034
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9731457929640189
Mean absolute error: 0.059481583842370074
Coverage of cases: 97.6958525345622
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.048124343115854146
Kappa statistic: 0.9124018838304552
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9399997679693255
Weighted AreaUnderROC: 0.9696663894759942
Root mean squared error: 0.22184536288745993
Relative absolute error: 14.381319333035458
Root relative squared error: 44.369072577491984
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8735107641897845
Weighted FMeasure: 0.9400249880811048
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9629220852718744
Mean absolute error: 0.07190659666517729
Coverage of cases: 96.31336405529954
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.06853828118683805
Kappa statistic: 0.8734693877551021
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9399997679693255
Weighted AreaUnderROC: 0.9755639097744361
Root mean squared error: 0.2255093699950566
Relative absolute error: 14.578753424160093
Root relative squared error: 45.101873999011325
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8735107641897845
Weighted FMeasure: 0.9400249880811048
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9679139320490051
Mean absolute error: 0.07289376712080047
Coverage of cases: 96.31336405529954
Instances selection time: 5.0
Test time: 8.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.06853828118683805
Kappa statistic: 0.8734693877551021
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 53.91705069124424
Incorrectly Classified Instances: 6.912442396313364
Correctly Classified Instances: 93.08755760368663
Weighted Precision: 0.9307663925785367
Weighted AreaUnderROC: 0.966031149301826
Root mean squared error: 0.24460393145219086
Relative absolute error: 16.094926952643597
Root relative squared error: 48.92078629043817
Weighted TruePositiveRate: 0.9308755760368663
Weighted MatthewsCorrelation: 0.8537258064222453
Weighted FMeasure: 0.9306326243608891
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9600383291714896
Mean absolute error: 0.08047463476321799
Coverage of cases: 95.39170506912443
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 31.0
Weighted Recall: 0.9308755760368663
Weighted FalsePositiveRate: 0.08313121513461072
Kappa statistic: 0.8533585619678334
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9215243255158332
Weighted AreaUnderROC: 0.9632563551736485
Root mean squared error: 0.24959411987720365
Relative absolute error: 16.46458656802765
Root relative squared error: 49.91882397544073
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8345764238736332
Weighted FMeasure: 0.9215711382599063
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9579638677922783
Mean absolute error: 0.08232293284013825
Coverage of cases: 95.39170506912443
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 34.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.08895221925782199
Kappa statistic: 0.8345368916797489
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9215243255158332
Weighted AreaUnderROC: 0.9685821697099891
Root mean squared error: 0.2347242768370377
Relative absolute error: 14.966203931312622
Root relative squared error: 46.94485536740754
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8345764238736332
Weighted FMeasure: 0.9215711382599063
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9627570905503836
Mean absolute error: 0.07483101965656311
Coverage of cases: 96.31336405529954
Instances selection time: 13.0
Test time: 32.0
Accumulative iteration time: 47.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.08895221925782199
Kappa statistic: 0.8345368916797489
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 54.60829493087557
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9215243255158332
Weighted AreaUnderROC: 0.970640887934121
Root mean squared error: 0.2338494974890411
Relative absolute error: 15.126703615715945
Root relative squared error: 46.76989949780822
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8345764238736332
Weighted FMeasure: 0.9215711382599063
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9656165081144779
Mean absolute error: 0.07563351807857972
Coverage of cases: 96.7741935483871
Instances selection time: 5.0
Test time: 21.0
Accumulative iteration time: 52.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.08895221925782199
Kappa statistic: 0.8345368916797489
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9262672811059908
Weighted AreaUnderROC: 0.9679556032939491
Root mean squared error: 0.23154075819096298
Relative absolute error: 14.695268375381055
Root relative squared error: 46.3081516381926
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8446115288220551
Weighted FMeasure: 0.9262672811059908
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9610522379891734
Mean absolute error: 0.07347634187690527
Coverage of cases: 96.7741935483871
Instances selection time: 6.0
Test time: 23.0
Accumulative iteration time: 58.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.08165575228393564
Kappa statistic: 0.8446115288220551
Training time: 0.0
		
Time end:Wed Nov 01 14.45.44 EET 2017