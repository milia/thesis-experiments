Wed Nov 01 14.46.37 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.37 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 57.6036866359447
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9767722878625134
Root mean squared error: 0.20771792791432264
Relative absolute error: 13.609100964512688
Root relative squared error: 41.54358558286453
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9646467861106518
Mean absolute error: 0.06804550482256344
Coverage of cases: 97.23502304147465
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 75.80645161290323
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9468122936245872
Weighted AreaUnderROC: 0.9905119942713928
Root mean squared error: 0.20422279109778435
Relative absolute error: 23.140697674773026
Root relative squared error: 40.84455821955687
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8864394897105831
Weighted FMeasure: 0.9450158295542056
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9902345836637634
Mean absolute error: 0.11570348837386513
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.04808391947610963
Kappa statistic: 0.8849721706864564
Training time: 2.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 80.87557603686636
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9634584218573727
Weighted AreaUnderROC: 0.9909595417114213
Root mean squared error: 0.20813493079785525
Relative absolute error: 27.094140557928558
Root relative squared error: 41.62698615957105
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.922817617517183
Weighted FMeasure: 0.9632108561899833
Iteration time: 4.0
Weighted AreaUnderPRC: 0.99161351738598
Mean absolute error: 0.1354707027896428
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.036441911229687124
Kappa statistic: 0.9226450405489708
Training time: 3.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 68.89400921658986
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9468122936245872
Weighted AreaUnderROC: 0.9806211958467597
Root mean squared error: 0.21323940731525284
Relative absolute error: 20.19394919865189
Root relative squared error: 42.64788146305057
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8864394897105831
Weighted FMeasure: 0.9450158295542056
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9738217045429185
Mean absolute error: 0.10096974599325945
Coverage of cases: 99.07834101382488
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.04808391947610963
Kappa statistic: 0.8849721706864564
Training time: 4.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 70.27649769585254
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9402622045885802
Weighted AreaUnderROC: 0.9932867883995703
Root mean squared error: 0.18914025680343793
Relative absolute error: 18.672675607011644
Root relative squared error: 37.82805136068759
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.874064173275429
Weighted FMeasure: 0.9401563743620956
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9935420910200939
Mean absolute error: 0.09336337803505822
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.06415231627455736
Kappa statistic: 0.8740231322288216
Training time: 4.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 69.12442396313364
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9634584218573727
Weighted AreaUnderROC: 0.994092373791622
Root mean squared error: 0.18416849577260924
Relative absolute error: 18.134176421934658
Root relative squared error: 36.833699154521845
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.922817617517183
Weighted FMeasure: 0.9632108561899833
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9942373099970013
Mean absolute error: 0.09067088210967329
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.036441911229687124
Kappa statistic: 0.9226450405489708
Training time: 4.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 65.43778801843318
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9931077694235588
Root mean squared error: 0.18879990405748323
Relative absolute error: 17.097009089263558
Root relative squared error: 37.759980811496646
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 5.0
Weighted AreaUnderPRC: 0.993239549177464
Mean absolute error: 0.0854850454463178
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 5.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 64.74654377880184
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9390070126227209
Weighted AreaUnderROC: 0.9930182599355531
Root mean squared error: 0.19302200821852494
Relative absolute error: 16.26429702190438
Root relative squared error: 38.60440164370499
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.8689159189138362
Weighted FMeasure: 0.9359498965650503
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9932592349305691
Mean absolute error: 0.08132148510952189
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.05390492359932088
Kappa statistic: 0.8663793103448275
Training time: 4.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 63.59447004608295
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9913623344074471
Root mean squared error: 0.18349865385468533
Relative absolute error: 15.400940693550794
Root relative squared error: 36.699730770937066
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9882038833705146
Mean absolute error: 0.07700470346775397
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 37.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 5.0
		
Time end:Wed Nov 01 14.46.38 EET 2017