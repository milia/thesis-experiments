Wed Nov 01 14.47.13 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.13 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 64.51612903225806
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9634584218573727
Weighted AreaUnderROC: 0.9942266380236305
Root mean squared error: 0.17289312771379176
Relative absolute error: 14.318366771059404
Root relative squared error: 34.57862554275835
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.922817617517183
Weighted FMeasure: 0.9632108561899833
Iteration time: 2.0
Weighted AreaUnderPRC: 0.99374789295119
Mean absolute error: 0.07159183385529702
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.036441911229687124
Kappa statistic: 0.9226450405489708
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 67.74193548387096
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9447004608294931
Weighted AreaUnderROC: 0.9902882205513786
Root mean squared error: 0.19879766856996994
Relative absolute error: 19.390467511577683
Root relative squared error: 39.75953371399399
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834586466165414
Weighted FMeasure: 0.9447004608294931
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9895124601639781
Mean absolute error: 0.09695233755788842
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06124181421295173
Kappa statistic: 0.8834586466165414
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 69.35483870967742
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9641046625292686
Weighted AreaUnderROC: 0.9884980307912639
Root mean squared error: 0.18861632926009228
Relative absolute error: 16.985432443721127
Root relative squared error: 37.72326585201846
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.923666504256351
Weighted FMeasure: 0.9632808965600179
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9881765410121278
Mean absolute error: 0.08492716221860563
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.032055946317406415
Kappa statistic: 0.9229813664596275
Training time: 2.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 66.12903225806451
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9678540624460927
Weighted AreaUnderROC: 0.9970461868958109
Root mean squared error: 0.14713878261625257
Relative absolute error: 12.262106527287706
Root relative squared error: 29.427756523250515
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9322100732143521
Weighted FMeasure: 0.9677765092718976
Iteration time: 3.0
Weighted AreaUnderPRC: 0.997143479299144
Mean absolute error: 0.06131053263643853
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.03353140916808149
Kappa statistic: 0.9321663019693655
Training time: 3.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 67.05069124423963
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9944504117436448
Root mean squared error: 0.17210175868120103
Relative absolute error: 15.243106890563732
Root relative squared error: 34.42035173624021
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9947321972165095
Mean absolute error: 0.07621553445281866
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 13.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 3.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 64.51612903225806
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9678540624460927
Weighted AreaUnderROC: 0.9953455066237021
Root mean squared error: 0.16213091662900983
Relative absolute error: 13.45397256349633
Root relative squared error: 32.426183325801965
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9322100732143521
Weighted FMeasure: 0.9677765092718976
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9953659348469124
Mean absolute error: 0.06726986281748165
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 18.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.03353140916808149
Kappa statistic: 0.9321663019693655
Training time: 4.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 64.51612903225806
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9591597501497389
Weighted AreaUnderROC: 0.9932492521626647
Root mean squared error: 0.1750164693139704
Relative absolute error: 15.88372276660179
Root relative squared error: 35.00329386279408
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.9135480351708758
Weighted FMeasure: 0.9586526109213916
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9933422298741691
Mean absolute error: 0.07941861383300895
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 23.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03935241329129274
Kappa statistic: 0.9131652661064424
Training time: 5.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 63.59447004608295
Incorrectly Classified Instances: 2.7649769585253456
Correctly Classified Instances: 97.23502304147465
Weighted Precision: 0.9723502304147466
Weighted AreaUnderROC: 0.9957035445757252
Root mean squared error: 0.15425591799814667
Relative absolute error: 13.175251912129918
Root relative squared error: 30.851183599629334
Weighted TruePositiveRate: 0.9723502304147466
Weighted MatthewsCorrelation: 0.9417293233082706
Weighted FMeasure: 0.9723502304147466
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9960058012789538
Mean absolute error: 0.06587625956064959
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9723502304147466
Weighted FalsePositiveRate: 0.030620907106475866
Kappa statistic: 0.9417293233082707
Training time: 5.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 64.74654377880184
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9683237785573716
Weighted AreaUnderROC: 0.9961510920157536
Root mean squared error: 0.16165756235196027
Relative absolute error: 14.509546036272894
Root relative squared error: 32.331512470392056
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9328527312106148
Weighted FMeasure: 0.9678409196055267
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9962703619335666
Mean absolute error: 0.07254773018136447
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 35.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.029145444255800793
Kappa statistic: 0.9324618736383443
Training time: 6.0
		
Time end:Wed Nov 01 14.47.14 EET 2017