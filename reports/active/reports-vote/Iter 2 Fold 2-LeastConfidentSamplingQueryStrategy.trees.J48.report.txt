Wed Nov 01 14.46.32 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.32 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 70.96774193548387
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9611976369495165
Root mean squared error: 0.20528661780429514
Relative absolute error: 19.31197084985534
Root relative squared error: 41.057323560859025
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9424316479990247
Mean absolute error: 0.0965598542492767
Coverage of cases: 99.07834101382488
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 85.9447004608295
Incorrectly Classified Instances: 16.589861751152075
Correctly Classified Instances: 83.41013824884793
Weighted Precision: 0.8416233090530697
Weighted AreaUnderROC: 0.8678839957035447
Root mean squared error: 0.35672434331405045
Relative absolute error: 40.53374998523028
Root relative squared error: 71.34486866281009
Weighted TruePositiveRate: 0.8341013824884793
Weighted MatthewsCorrelation: 0.649223557852128
Weighted FMeasure: 0.8279385771100309
Iteration time: 1.0
Weighted AreaUnderPRC: 0.8388557745114833
Mean absolute error: 0.20266874992615141
Coverage of cases: 96.7741935483871
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.8341013824884793
Weighted FalsePositiveRate: 0.2319710566739429
Kappa statistic: 0.6326530612244899
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 56.68202764976959
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.910863922877271
Weighted AreaUnderROC: 0.9715345391127587
Root mean squared error: 0.26405332991866637
Relative absolute error: 21.32818785652734
Root relative squared error: 52.81066598373327
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.8063112281239649
Weighted FMeasure: 0.9062784511844617
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9594448215760194
Mean absolute error: 0.1066409392826367
Coverage of cases: 97.6958525345622
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.12838547982860377
Kappa statistic: 0.8005148005148004
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 57.83410138248848
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9108104638099823
Weighted AreaUnderROC: 0.9774883637665592
Root mean squared error: 0.25259732551850284
Relative absolute error: 21.277191640784316
Root relative squared error: 50.51946510370057
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.8000068189462377
Weighted FMeasure: 0.9006848978994099
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9707384862352815
Mean absolute error: 0.10638595820392158
Coverage of cases: 98.61751152073732
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.1444538766270515
Kappa statistic: 0.7881548974943052
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 56.45161290322581
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9448551919001648
Weighted AreaUnderROC: 0.9745547624822425
Root mean squared error: 0.22315402995850117
Relative absolute error: 15.214618157934467
Root relative squared error: 44.630805991700235
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831057605697007
Weighted FMeasure: 0.9444357024215403
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9654372878534399
Mean absolute error: 0.07607309078967234
Coverage of cases: 98.15668202764977
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.07001374403751313
Kappa statistic: 0.8824273072060683
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 56.45161290322581
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9404282138228559
Weighted AreaUnderROC: 0.9749936477137545
Root mean squared error: 0.22306640533669722
Relative absolute error: 14.686706852276373
Root relative squared error: 44.61328106733944
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8734004833134899
Weighted FMeasure: 0.9397259893833848
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9651678209821749
Mean absolute error: 0.07343353426138187
Coverage of cases: 98.15668202764977
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.07731021101139947
Kappa statistic: 0.8723471650300919
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9454272810039809
Weighted AreaUnderROC: 0.9761240659251815
Root mean squared error: 0.21760607007017718
Relative absolute error: 13.61981540072421
Root relative squared error: 43.521214014035436
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834462279296256
Weighted FMeasure: 0.9442863166772084
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9684643627852586
Mean absolute error: 0.06809907700362106
Coverage of cases: 97.6958525345622
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.07439970894979384
Kappa statistic: 0.881904761904762
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9454272810039809
Weighted AreaUnderROC: 0.9760230068258203
Root mean squared error: 0.21774780025466506
Relative absolute error: 13.350142177522487
Root relative squared error: 43.54956005093301
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834462279296256
Weighted FMeasure: 0.9442863166772084
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9665057505226318
Mean absolute error: 0.06675071088761243
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.07439970894979384
Kappa statistic: 0.881904761904762
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9454272810039809
Weighted AreaUnderROC: 0.9763204093182264
Root mean squared error: 0.2186945658219386
Relative absolute error: 13.182369771484275
Root relative squared error: 43.738913164387725
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834462279296256
Weighted FMeasure: 0.9442863166772084
Iteration time: 2.0
Weighted AreaUnderPRC: 0.967546379454589
Mean absolute error: 0.06591184885742138
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.07439970894979384
Kappa statistic: 0.881904761904762
Training time: 1.0
		
Time end:Wed Nov 01 14.46.33 EET 2017