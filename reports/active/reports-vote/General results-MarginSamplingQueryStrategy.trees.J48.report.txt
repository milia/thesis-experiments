Wed Nov 01 14.46.50 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.50 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 62.759903606307866
Incorrectly Classified Instances: 5.330613452838962
Correctly Classified Instances: 94.66938654716103
Weighted Precision: 0.9482693184913625
Weighted AreaUnderROC: 0.9537219247479038
Root mean squared error: 0.22010790240557018
Relative absolute error: 16.949061453626246
Root relative squared error: 44.021580481114036
Weighted TruePositiveRate: 0.9466938654716103
Weighted MatthewsCorrelation: 0.889461784465712
Weighted FMeasure: 0.9467700502302077
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9353738240692377
Mean absolute error: 0.08474530726813123
Coverage of cases: 96.64672557392296
Instances selection time: 2.0
Test time: 1.7
Accumulative iteration time: 2.0
Weighted Recall: 0.9466938654716103
Weighted FalsePositiveRate: 0.05432955918350204
Kappa statistic: 0.8880624280549977
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 70.06849025493594
Incorrectly Classified Instances: 8.183105737115799
Correctly Classified Instances: 91.81689426288419
Weighted Precision: 0.9215042050311706
Weighted AreaUnderROC: 0.9415474761364004
Root mean squared error: 0.2538878974353507
Relative absolute error: 24.250670693328654
Root relative squared error: 50.777579487070135
Weighted TruePositiveRate: 0.918168942628842
Weighted MatthewsCorrelation: 0.8295158724201153
Weighted FMeasure: 0.9171992383140548
Iteration time: 1.1
Weighted AreaUnderPRC: 0.9252172627831236
Mean absolute error: 0.12125335346664327
Coverage of cases: 97.29104130554265
Instances selection time: 0.5
Test time: 1.6
Accumulative iteration time: 3.1
Weighted Recall: 0.918168942628842
Weighted FalsePositiveRate: 0.09830959276163669
Kappa statistic: 0.8246705644706545
Training time: 0.6
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 63.7374751617131
Incorrectly Classified Instances: 6.98727434152116
Correctly Classified Instances: 93.01272565847884
Weighted Precision: 0.9344012096533205
Weighted AreaUnderROC: 0.9718128470378089
Root mean squared error: 0.2320592659778669
Relative absolute error: 19.517973323487087
Root relative squared error: 46.411853195573386
Weighted TruePositiveRate: 0.9301272565847883
Weighted MatthewsCorrelation: 0.8560012548178124
Weighted FMeasure: 0.9292913262436159
Iteration time: 1.3
Weighted AreaUnderPRC: 0.9605556861466683
Mean absolute error: 0.09758986661743543
Coverage of cases: 97.42654208768445
Instances selection time: 0.8
Test time: 1.2
Accumulative iteration time: 4.4
Weighted Recall: 0.9301272565847883
Weighted FalsePositiveRate: 0.08376321214533154
Kappa statistic: 0.8505056322002119
Training time: 0.5
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 58.90722107132287
Incorrectly Classified Instances: 5.8863569103285
Correctly Classified Instances: 94.11364308967148
Weighted Precision: 0.943763712867313
Weighted AreaUnderROC: 0.973230602841683
Root mean squared error: 0.21404712451930727
Relative absolute error: 16.633610296119016
Root relative squared error: 42.80942490386147
Weighted TruePositiveRate: 0.941136430896715
Weighted MatthewsCorrelation: 0.8786923328073805
Weighted FMeasure: 0.9410610467684067
Iteration time: 1.1
Weighted AreaUnderPRC: 0.962170080021116
Mean absolute error: 0.08316805148059507
Coverage of cases: 97.65653405487674
Instances selection time: 0.3
Test time: 1.4
Accumulative iteration time: 5.5
Weighted Recall: 0.941136430896715
Weighted FalsePositiveRate: 0.06130345292940868
Kappa statistic: 0.8760169543227644
Training time: 0.8
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 54.2765188348201
Incorrectly Classified Instances: 4.873377584238787
Correctly Classified Instances: 95.1266224157612
Weighted Precision: 0.9530587250844371
Weighted AreaUnderROC: 0.9694526014530125
Root mean squared error: 0.1995981807315514
Relative absolute error: 15.195992678694713
Root relative squared error: 39.91963614631028
Weighted TruePositiveRate: 0.951266224157612
Weighted MatthewsCorrelation: 0.8995550261599359
Weighted FMeasure: 0.9514426750646388
Iteration time: 1.3
Weighted AreaUnderPRC: 0.9608452000589784
Mean absolute error: 0.07597996339347356
Coverage of cases: 97.60960554686508
Instances selection time: 0.3
Test time: 0.8
Accumulative iteration time: 6.8
Weighted Recall: 0.951266224157612
Weighted FalsePositiveRate: 0.04566453336346259
Kappa statistic: 0.8981260711134045
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 56.14308967150045
Incorrectly Classified Instances: 4.8279288039572155
Correctly Classified Instances: 95.17207119604278
Weighted Precision: 0.9537524813331201
Weighted AreaUnderROC: 0.9719004763730748
Root mean squared error: 0.1967800892798465
Relative absolute error: 14.621046267063928
Root relative squared error: 39.3560178559693
Weighted TruePositiveRate: 0.9517207119604277
Weighted MatthewsCorrelation: 0.9007727993786141
Weighted FMeasure: 0.9518904005524773
Iteration time: 1.4
Weighted AreaUnderPRC: 0.9615280137391643
Mean absolute error: 0.07310523133531965
Coverage of cases: 97.97826914133513
Instances selection time: 0.6
Test time: 0.9
Accumulative iteration time: 8.2
Weighted Recall: 0.9517207119604277
Weighted FalsePositiveRate: 0.044945378103397216
Kappa statistic: 0.8990985599480353
Training time: 0.8
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 56.005052213249904
Incorrectly Classified Instances: 4.5059823278231095
Correctly Classified Instances: 95.4940176721769
Weighted Precision: 0.956766770240475
Weighted AreaUnderROC: 0.9728392030809125
Root mean squared error: 0.19550798762786104
Relative absolute error: 14.096784449397433
Root relative squared error: 39.1015975255722
Weighted TruePositiveRate: 0.954940176721769
Weighted MatthewsCorrelation: 0.9072606234522456
Weighted FMeasure: 0.9550730633798181
Iteration time: 1.4
Weighted AreaUnderPRC: 0.9622576809347768
Mean absolute error: 0.07048392224698717
Coverage of cases: 97.93218619202638
Instances selection time: 0.5
Test time: 0.9
Accumulative iteration time: 9.6
Weighted Recall: 0.954940176721769
Weighted FalsePositiveRate: 0.04291851809759654
Kappa statistic: 0.9057169080704235
Training time: 0.9
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 57.49524373229612
Incorrectly Classified Instances: 4.551431108104681
Correctly Classified Instances: 95.44856889189532
Weighted Precision: 0.9553942544030155
Weighted AreaUnderROC: 0.9699947270817111
Root mean squared error: 0.19802166267427548
Relative absolute error: 14.393758543653453
Root relative squared error: 39.60433253485509
Weighted TruePositiveRate: 0.9544856889189534
Weighted MatthewsCorrelation: 0.9051438009014532
Weighted FMeasure: 0.9545545113177315
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9586959357968933
Mean absolute error: 0.07196879271826727
Coverage of cases: 97.83959751405742
Instances selection time: 1.2
Test time: 0.5
Accumulative iteration time: 11.6
Weighted Recall: 0.9544856889189534
Weighted FalsePositiveRate: 0.046724681923853505
Kappa statistic: 0.9043755823420113
Training time: 0.8
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 55.91267492495666
Incorrectly Classified Instances: 4.597302667737708
Correctly Classified Instances: 95.4026973322623
Weighted Precision: 0.9548463009123654
Weighted AreaUnderROC: 0.9717212699412411
Root mean squared error: 0.19610038592607257
Relative absolute error: 13.697662407508755
Root relative squared error: 39.22007718521452
Weighted TruePositiveRate: 0.954026973322623
Weighted MatthewsCorrelation: 0.9040483426227688
Weighted FMeasure: 0.9540818566360562
Iteration time: 1.3
Weighted AreaUnderPRC: 0.9616790649335203
Mean absolute error: 0.06848831203754377
Coverage of cases: 97.74785439479136
Instances selection time: 0.3
Test time: 1.0
Accumulative iteration time: 12.9
Weighted Recall: 0.954026973322623
Weighted FalsePositiveRate: 0.0479006500517592
Kappa statistic: 0.9033413217413067
Training time: 1.0
		
Time end:Wed Nov 01 14.46.53 EET 2017