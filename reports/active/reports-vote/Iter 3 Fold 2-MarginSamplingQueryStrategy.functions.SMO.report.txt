Wed Nov 01 14.46.44 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.44 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9602130325814537
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9391320764472826
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 9.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9602130325814537
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9391320764472826
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 22.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 7.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9602130325814537
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9391320764472826
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 29.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 6.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9602130325814537
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9391320764472826
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 6.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9572286115007013
Weighted AreaUnderROC: 0.9602130325814537
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9072050354544602
Weighted FMeasure: 0.954249926117893
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9391320764472826
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03349098552833697
Kappa statistic: 0.9045566502463054
Training time: 7.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9547994358855347
Weighted AreaUnderROC: 0.9448621553884712
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9031631703816188
Weighted FMeasure: 0.9535719305643404
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9301119555351133
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 52.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.06419273991430188
Kappa statistic: 0.9015873015873015
Training time: 7.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9541676477513538
Weighted AreaUnderROC: 0.9470551378446116
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9027162753101454
Weighted FMeasure: 0.953696418684617
Iteration time: 9.0
Weighted AreaUnderPRC: 0.930961066038483
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.05980677500202118
Kappa statistic: 0.9020227560050569
Training time: 8.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 50.0
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9631336405529954
Weighted AreaUnderROC: 0.9611528822055138
Root mean squared error: 0.19200614429492774
Relative absolute error: 7.373271889400922
Root relative squared error: 38.40122885898555
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.9223057644110276
Weighted FMeasure: 0.9631336405529954
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9461326126375849
Mean absolute error: 0.03686635944700461
Coverage of cases: 96.31336405529954
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 76.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.04082787614196782
Kappa statistic: 0.9223057644110276
Training time: 14.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 50.0
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9683237785573716
Weighted AreaUnderROC: 0.969298245614035
Root mean squared error: 0.1796053020267749
Relative absolute error: 6.451612903225806
Root relative squared error: 35.92106040535498
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9328527312106148
Weighted FMeasure: 0.9678409196055267
Iteration time: 13.0
Weighted AreaUnderPRC: 0.9546397936867634
Mean absolute error: 0.03225806451612903
Coverage of cases: 96.7741935483871
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 89.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.029145444255800793
Kappa statistic: 0.9324618736383443
Training time: 13.0
		
Time end:Wed Nov 01 14.46.44 EET 2017