Wed Nov 01 14.47.16 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.16 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 61.23853211009175
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9693941009239516
Root mean squared error: 0.20560585436561984
Relative absolute error: 14.535091557898063
Root relative squared error: 41.12117087312397
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9558709643552361
Mean absolute error: 0.07267545778949032
Coverage of cases: 97.24770642201835
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 66.05504587155963
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9838752665245203
Root mean squared error: 0.20185683707233412
Relative absolute error: 18.101390938508626
Root relative squared error: 40.371367414466825
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 2.0
Weighted AreaUnderPRC: 0.978274168324853
Mean absolute error: 0.09050695469254312
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 2.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 62.84403669724771
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.956144877675841
Weighted AreaUnderROC: 0.9750799573560767
Root mean squared error: 0.21604762726075658
Relative absolute error: 16.686721971159063
Root relative squared error: 43.20952545215132
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9059266834836831
Weighted FMeasure: 0.9543940824601355
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9669613452195496
Mean absolute error: 0.08343360985579532
Coverage of cases: 98.62385321100918
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.037639456713809714
Kappa statistic: 0.9044362616166931
Training time: 2.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 68.57798165137615
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9601639405831254
Weighted AreaUnderROC: 0.9861407249466951
Root mean squared error: 0.20452332037679
Relative absolute error: 18.776875994239766
Root relative squared error: 40.904664075358
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9148532571069216
Weighted FMeasure: 0.9589197544617876
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9830282798280261
Mean absolute error: 0.09388437997119883
Coverage of cases: 99.54128440366972
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03476392610994829
Kappa statistic: 0.9138037079342765
Training time: 3.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 63.30275229357798
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.9642715083082056
Weighted AreaUnderROC: 0.988095238095238
Root mean squared error: 0.18026759120242866
Relative absolute error: 13.3167694659629
Root relative squared error: 36.05351824048573
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9238935982125793
Weighted FMeasure: 0.9634514550238894
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9845319514300168
Mean absolute error: 0.0665838473298145
Coverage of cases: 99.54128440366972
Instances selection time: 5.0
Test time: 1.0
Accumulative iteration time: 27.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.03188839550608687
Kappa statistic: 0.9232123987319478
Training time: 3.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 64.90825688073394
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.9642715083082056
Weighted AreaUnderROC: 0.9930259417199717
Root mean squared error: 0.17787333666260832
Relative absolute error: 14.69785984372676
Root relative squared error: 35.57466733252166
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9238935982125793
Weighted FMeasure: 0.9634514550238894
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9928876948347992
Mean absolute error: 0.0734892992186338
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 36.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.03188839550608687
Kappa statistic: 0.9232123987319478
Training time: 8.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 66.97247706422019
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9502283314537143
Weighted AreaUnderROC: 0.9862295664534471
Root mean squared error: 0.20362710832329475
Relative absolute error: 17.865015371760578
Root relative squared error: 40.72542166465895
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8945566834252947
Weighted FMeasure: 0.9496982893446182
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9860758447604361
Mean absolute error: 0.08932507685880288
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04939913799286659
Kappa statistic: 0.8941840967257965
Training time: 9.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 60.091743119266056
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9593495149616872
Weighted AreaUnderROC: 0.9860518834399431
Root mean squared error: 0.17937154159203428
Relative absolute error: 13.518875731221744
Root relative squared error: 35.874308318406854
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9138039551373668
Weighted FMeasure: 0.9588440549183241
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9796300383422369
Mean absolute error: 0.06759437865610872
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.03920600144754602
Kappa statistic: 0.9134233518665609
Training time: 5.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 63.53211009174312
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9588474694561553
Weighted AreaUnderROC: 0.9868070362473348
Root mean squared error: 0.18998391352010865
Relative absolute error: 15.66332744807783
Root relative squared error: 37.99678270402173
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9130822475220538
Weighted FMeasure: 0.9587604279190397
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9838174718802165
Mean absolute error: 0.07831663724038915
Coverage of cases: 99.54128440366972
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.04364807678514374
Kappa statistic: 0.9130396241467955
Training time: 7.0
		
Time end:Wed Nov 01 14.47.16 EET 2017