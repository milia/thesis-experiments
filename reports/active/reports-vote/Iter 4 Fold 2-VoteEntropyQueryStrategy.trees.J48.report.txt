Wed Nov 01 14.47.31 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.31 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 51.61290322580645
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.2080482560204758
Relative absolute error: 12.153038259564894
Root relative squared error: 41.60965120409516
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.06076519129782448
Coverage of cases: 96.31336405529954
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 1.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20143867332581228
Relative absolute error: 14.27174975562072
Root relative squared error: 40.28773466516245
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.0713587487781036
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20156902318117645
Relative absolute error: 13.800657174151215
Root relative squared error: 40.31380463623529
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.06900328587075608
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 54.83870967741935
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9223292836196061
Weighted AreaUnderROC: 0.9758771929824561
Root mean squared error: 0.22988993609360847
Relative absolute error: 17.21521898773807
Root relative squared error: 45.977987218721694
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8343167065096193
Weighted FMeasure: 0.9209602926292969
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9626897402978337
Mean absolute error: 0.08607609493869035
Coverage of cases: 97.6958525345622
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.10211011399466409
Kappa statistic: 0.832325803372574
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9223292836196061
Weighted AreaUnderROC: 0.9758771929824561
Root mean squared error: 0.23014733270046703
Relative absolute error: 19.784238838988905
Root relative squared error: 46.0294665400934
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8343167065096193
Weighted FMeasure: 0.9209602926292969
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9626897402978337
Mean absolute error: 0.09892119419494452
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.10211011399466409
Kappa statistic: 0.832325803372574
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 55.29953917050691
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9281300936279898
Weighted AreaUnderROC: 0.9528284998209812
Root mean squared error: 0.254375866578452
Relative absolute error: 16.88652663910685
Root relative squared error: 50.875173315690404
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8388554743709837
Weighted FMeasure: 0.9199096927256725
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9410726276295115
Mean absolute error: 0.08443263319553426
Coverage of cases: 96.7741935483871
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.11965397364378688
Kappa statistic: 0.829284094590217
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 69.5852534562212
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9499957217421067
Weighted AreaUnderROC: 0.9612871464375222
Root mean squared error: 0.21450489148769802
Relative absolute error: 15.701987119063245
Root relative squared error: 42.9009782975396
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.894243339131137
Weighted FMeasure: 0.9494643022372562
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9443498320654533
Mean absolute error: 0.07850993559531623
Coverage of cases: 98.61751152073732
Instances selection time: 0.0
Test time: 0.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0495593823267847
Kappa statistic: 0.8938686585745409
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20170648613514044
Relative absolute error: 18.393346473727213
Root relative squared error: 40.341297227028086
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.09196673236863606
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 72.35023041474655
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9559393785454238
Weighted AreaUnderROC: 0.9687611886860008
Root mean squared error: 0.20129704355153585
Relative absolute error: 18.988125848642067
Root relative squared error: 40.25940871030717
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9056425813814624
Weighted FMeasure: 0.954179857961838
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9505417305205944
Mean absolute error: 0.09494062924321034
Coverage of cases: 100.0
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.03787695044061767
Kappa statistic: 0.904143475572047
Training time: 2.0
		
Time end:Wed Nov 01 14.47.31 EET 2017