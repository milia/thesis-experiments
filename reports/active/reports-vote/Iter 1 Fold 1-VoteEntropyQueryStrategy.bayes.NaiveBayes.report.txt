Wed Nov 01 14.47.17 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.17 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.895501230405877
Weighted AreaUnderROC: 0.966950959488273
Root mean squared error: 0.30864606218722546
Relative absolute error: 21.66774560564188
Root relative squared error: 61.72921243744509
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7790730531528622
Weighted FMeasure: 0.8948236959023835
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9692959266257998
Mean absolute error: 0.1083387280282094
Coverage of cases: 92.66055045871559
Instances selection time: 2.0
Test time: 7.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.11055795726479006
Kappa statistic: 0.7787485658812108
Training time: 6.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.211009174311926
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.8965031176359185
Weighted AreaUnderROC: 0.9717484008528785
Root mean squared error: 0.30543651335717215
Relative absolute error: 20.933976327525347
Root relative squared error: 61.08730267143443
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7806161349583483
Weighted FMeasure: 0.8950171502912354
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9737445205037556
Mean absolute error: 0.10466988163762674
Coverage of cases: 93.11926605504587
Instances selection time: 3.0
Test time: 3.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.10611588192719233
Kappa statistic: 0.7797205869431508
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9016437308868501
Weighted AreaUnderROC: 0.9737917555081735
Root mean squared error: 0.2962216028453924
Relative absolute error: 20.06710442974466
Root relative squared error: 59.24432056907848
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7910612218709788
Weighted FMeasure: 0.8996669814122983
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9754921815459769
Mean absolute error: 0.10033552214872331
Coverage of cases: 94.03669724770643
Instances selection time: 2.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.09879827598573318
Kappa statistic: 0.7897597755567246
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 54.58715596330275
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.8965031176359185
Weighted AreaUnderROC: 0.974235963041933
Root mean squared error: 0.29679909227614304
Relative absolute error: 20.26950365392694
Root relative squared error: 59.359818455228606
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7806161349583483
Weighted FMeasure: 0.8950171502912354
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9760875057666667
Mean absolute error: 0.1013475182696347
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.10611588192719233
Kappa statistic: 0.7797205869431508
Training time: 2.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9068398913085758
Weighted AreaUnderROC: 0.9750355366027007
Root mean squared error: 0.2911090997165862
Relative absolute error: 19.522380038006627
Root relative squared error: 58.22181994331724
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.8015426168393753
Weighted FMeasure: 0.9043045449752187
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9767744348946933
Mean absolute error: 0.09761190019003313
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.09148067004427403
Kappa statistic: 0.7997550520514392
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9068398913085758
Weighted AreaUnderROC: 0.9747690120824449
Root mean squared error: 0.29010964329093203
Relative absolute error: 19.42679064290166
Root relative squared error: 58.021928658186404
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.8015426168393753
Weighted FMeasure: 0.9043045449752187
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9764791919781167
Mean absolute error: 0.09713395321450831
Coverage of cases: 93.57798165137615
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.09148067004427403
Kappa statistic: 0.7997550520514392
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9016437308868501
Weighted AreaUnderROC: 0.9761904761904762
Root mean squared error: 0.28836636147993006
Relative absolute error: 19.21758044311661
Root relative squared error: 57.67327229598601
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7910612218709788
Weighted FMeasure: 0.8996669814122983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9777561883856296
Mean absolute error: 0.09608790221558305
Coverage of cases: 94.03669724770643
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.09879827598573318
Kappa statistic: 0.7897597755567246
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9016437308868501
Weighted AreaUnderROC: 0.9754797441364605
Root mean squared error: 0.28842849869362525
Relative absolute error: 19.145298652296916
Root relative squared error: 57.68569973872505
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7910612218709788
Weighted FMeasure: 0.8996669814122983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9770971923584628
Mean absolute error: 0.09572649326148458
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.09879827598573318
Kappa statistic: 0.7897597755567246
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9016437308868501
Weighted AreaUnderROC: 0.9753909026297086
Root mean squared error: 0.29053406136407706
Relative absolute error: 19.37747099886175
Root relative squared error: 58.10681227281541
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7910612218709788
Weighted FMeasure: 0.8996669814122983
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9769880886756469
Mean absolute error: 0.09688735499430874
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 22.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.09879827598573318
Kappa statistic: 0.7897597755567246
Training time: 0.0
		
Time end:Wed Nov 01 14.47.17 EET 2017