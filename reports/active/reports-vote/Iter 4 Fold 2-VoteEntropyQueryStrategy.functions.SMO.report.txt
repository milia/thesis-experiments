Wed Nov 01 14.47.23 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.23 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9519870810193392
Weighted AreaUnderROC: 0.9542606516290727
Root mean squared error: 0.22514716129596513
Relative absolute error: 10.138248847926267
Root relative squared error: 45.02943225919302
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8967979565784406
Weighted FMeasure: 0.9496375638311122
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9318533571543827
Mean absolute error: 0.05069124423963134
Coverage of cases: 94.93087557603687
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0407874525022233
Kappa statistic: 0.8947855600123419
Training time: 6.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9447004608294931
Weighted AreaUnderROC: 0.9417293233082706
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8834586466165414
Weighted FMeasure: 0.9447004608294931
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9202730328124459
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06124181421295173
Kappa statistic: 0.8834586466165414
Training time: 6.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9539170506912442
Weighted AreaUnderROC: 0.9514411027568921
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9028822055137845
Weighted FMeasure: 0.9539170506912442
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9331133132370095
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.05103484517745977
Kappa statistic: 0.9028822055137844
Training time: 6.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9399997679693255
Weighted AreaUnderROC: 0.9357769423558896
Root mean squared error: 0.24476076912238712
Relative absolute error: 11.981566820276496
Root relative squared error: 48.95215382447742
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8735107641897845
Weighted FMeasure: 0.9400249880811048
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9133018587822286
Mean absolute error: 0.059907834101382486
Coverage of cases: 94.00921658986175
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 28.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.06853828118683805
Kappa statistic: 0.8734693877551021
Training time: 7.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9411395056556346
Weighted AreaUnderROC: 0.9291979949874687
Root mean squared error: 0.24476076912238712
Relative absolute error: 11.981566820276496
Root relative squared error: 48.95215382447742
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8738638286836228
Weighted FMeasure: 0.9395578708341682
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9103264804171597
Mean absolute error: 0.059907834101382486
Coverage of cases: 94.00921658986175
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 37.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.08169617592368017
Kappa statistic: 0.8717785555202037
Training time: 9.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9369047189008899
Weighted AreaUnderROC: 0.9232456140350876
Root mean squared error: 0.254000254000381
Relative absolute error: 12.903225806451612
Root relative squared error: 50.8000508000762
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.8643140504984893
Weighted FMeasure: 0.9348128090416764
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9035638307582893
Mean absolute error: 0.06451612903225806
Coverage of cases: 93.54838709677419
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 45.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.0889926428975665
Kappa statistic: 0.8616071428571428
Training time: 8.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9446163708948918
Weighted AreaUnderROC: 0.9395363408521303
Root mean squared error: 0.23515854050088617
Relative absolute error: 11.059907834101383
Root relative squared error: 47.031708100177234
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8831133236786068
Weighted FMeasure: 0.944573672639523
Iteration time: 8.0
Weighted AreaUnderPRC: 0.919087716866961
Mean absolute error: 0.055299539170506916
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 53.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.06562777912523243
Kappa statistic: 0.8829452485840151
Training time: 7.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.608294930875576
Correctly Classified Instances: 95.39170506912443
Weighted Precision: 0.9549546477528472
Weighted AreaUnderROC: 0.9558270676691729
Root mean squared error: 0.2146693953705459
Relative absolute error: 9.216589861751153
Root relative squared error: 42.933879074109186
Weighted TruePositiveRate: 0.9539170506912442
Weighted MatthewsCorrelation: 0.9043975529562521
Weighted FMeasure: 0.9541011207000223
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9358442328428775
Mean absolute error: 0.04608294930875576
Coverage of cases: 95.39170506912443
Instances selection time: 0.0
Test time: 3.0
Accumulative iteration time: 63.0
Weighted Recall: 0.9539170506912442
Weighted FalsePositiveRate: 0.042262915352898375
Kappa statistic: 0.9037267080745341
Training time: 10.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.147465437788019
Correctly Classified Instances: 95.85253456221199
Weighted Precision: 0.9599775733961581
Weighted AreaUnderROC: 0.9617794486215538
Root mean squared error: 0.2036532699906392
Relative absolute error: 8.294930875576037
Root relative squared error: 40.73065399812784
Weighted TruePositiveRate: 0.9585253456221198
Weighted MatthewsCorrelation: 0.914597630461382
Weighted FMeasure: 0.9587274235341773
Iteration time: 11.0
Weighted AreaUnderPRC: 0.9430706356073045
Mean absolute error: 0.041474654377880185
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 74.0
Weighted Recall: 0.9585253456221198
Weighted FalsePositiveRate: 0.03496644837901204
Kappa statistic: 0.9135419894638983
Training time: 10.0
		
Time end:Wed Nov 01 14.47.24 EET 2017