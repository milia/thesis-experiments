Wed Nov 01 14.46.05 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.05 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9111615611879956
Weighted AreaUnderROC: 0.9401208244491827
Root mean squared error: 0.3008312462246739
Relative absolute error: 22.368720232528393
Root relative squared error: 60.166249244934775
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.8033117706560523
Weighted FMeasure: 0.9002665552326501
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9189915407040701
Mean absolute error: 0.11184360116264196
Coverage of cases: 91.28440366972477
Instances selection time: 8.0
Test time: 4.0
Accumulative iteration time: 14.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.07658789929774457
Kappa statistic: 0.7942690459849006
Training time: 6.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 52.06422018348624
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9227833993438825
Weighted AreaUnderROC: 0.9484719260838664
Root mean squared error: 0.2744198000311514
Relative absolute error: 18.688207483864602
Root relative squared error: 54.88396000623028
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8332275460793623
Weighted FMeasure: 0.9181503704964484
Iteration time: 7.0
Weighted AreaUnderPRC: 0.930595274160933
Mean absolute error: 0.09344103741932301
Coverage of cases: 93.11926605504587
Instances selection time: 7.0
Test time: 5.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.06952785221989657
Kappa statistic: 0.8294802711628717
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 9.174311926605505
Correctly Classified Instances: 90.8256880733945
Weighted Precision: 0.9088048179684621
Weighted AreaUnderROC: 0.9621090973702914
Root mean squared error: 0.2625562680434365
Relative absolute error: 18.568944125329846
Root relative squared error: 52.511253608687305
Weighted TruePositiveRate: 0.908256880733945
Weighted MatthewsCorrelation: 0.8073321192724388
Weighted FMeasure: 0.9084516471967831
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9480232023935787
Mean absolute error: 0.09284472062664922
Coverage of cases: 94.95412844036697
Instances selection time: 8.0
Test time: 6.0
Accumulative iteration time: 29.0
Weighted Recall: 0.908256880733945
Weighted FalsePositiveRate: 0.09748929011560806
Kappa statistic: 0.8071820272421723
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9034908236411497
Weighted AreaUnderROC: 0.9566453447050463
Root mean squared error: 0.2706500469463316
Relative absolute error: 19.166373300022048
Root relative squared error: 54.13000938926632
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.7962266195569134
Weighted FMeasure: 0.903560363329138
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9432954015758198
Mean absolute error: 0.09583186650011025
Coverage of cases: 94.4954128440367
Instances selection time: 8.0
Test time: 7.0
Accumulative iteration time: 37.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.10924897139466495
Kappa statistic: 0.7961891194016562
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 55.5045871559633
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.8990119971771348
Weighted AreaUnderROC: 0.9515675228053702
Root mean squared error: 0.2812571729765297
Relative absolute error: 21.677506315065173
Root relative squared error: 56.25143459530594
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.785443697059286
Weighted FMeasure: 0.8983186628669787
Iteration time: 7.0
Weighted AreaUnderPRC: 0.936911686644851
Mean absolute error: 0.10838753157532587
Coverage of cases: 94.4954128440367
Instances selection time: 7.0
Test time: 8.0
Accumulative iteration time: 44.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.12545072801131954
Kappa statistic: 0.7840806771114714
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 56.42201834862385
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.8945752666228949
Weighted AreaUnderROC: 0.951048329779673
Root mean squared error: 0.28439903875376543
Relative absolute error: 22.373437054303558
Root relative squared error: 56.87980775075309
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.7755977098570924
Weighted FMeasure: 0.8935446205170977
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9353853032431787
Mean absolute error: 0.11186718527151779
Coverage of cases: 94.4954128440367
Instances selection time: 6.0
Test time: 16.0
Accumulative iteration time: 51.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.1327683339527787
Kappa statistic: 0.7737568811479107
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 8.715596330275229
Correctly Classified Instances: 91.28440366972477
Weighted Precision: 0.9125898147528094
Weighted AreaUnderROC: 0.9566453447050463
Root mean squared error: 0.26547546774026975
Relative absolute error: 18.311138495794545
Root relative squared error: 53.09509354805395
Weighted TruePositiveRate: 0.9128440366972477
Weighted MatthewsCorrelation: 0.8151237397161437
Weighted FMeasure: 0.9125341228391434
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9452817755975053
Mean absolute error: 0.09155569247897273
Coverage of cases: 94.4954128440367
Instances selection time: 9.0
Test time: 10.0
Accumulative iteration time: 61.0
Weighted Recall: 0.9128440366972477
Weighted FalsePositiveRate: 0.10349791018694209
Kappa statistic: 0.8147750648421428
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9319859644377685
Weighted AreaUnderROC: 0.9523809523809522
Root mean squared error: 0.26503795917473605
Relative absolute error: 18.128327698812875
Root relative squared error: 53.007591834947206
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8560621400011506
Weighted FMeasure: 0.9314067581972066
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9400836559043995
Mean absolute error: 0.09064163849406437
Coverage of cases: 94.03669724770643
Instances selection time: 4.0
Test time: 11.0
Accumulative iteration time: 66.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06978541108350775
Kappa statistic: 0.8557055864442679
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 55.04587155963303
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9328807307486082
Weighted AreaUnderROC: 0.9613983653162759
Root mean squared error: 0.2520219619569355
Relative absolute error: 17.037398103709634
Root relative squared error: 50.4043923913871
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8573230619003901
Weighted FMeasure: 0.9315329241029795
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9482870953408914
Mean absolute error: 0.08518699051854817
Coverage of cases: 94.95412844036697
Instances selection time: 1.0
Test time: 12.0
Accumulative iteration time: 68.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06534333574591003
Kappa statistic: 0.856339513223794
Training time: 1.0
		
Time end:Wed Nov 01 14.46.06 EET 2017