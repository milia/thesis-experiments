Wed Nov 01 14.45.40 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.40 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 51.8348623853211
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9111615611879956
Weighted AreaUnderROC: 0.9401208244491827
Root mean squared error: 0.3008312462246739
Relative absolute error: 22.368720232528393
Root relative squared error: 60.166249244934775
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.8033117706560523
Weighted FMeasure: 0.9002665552326501
Iteration time: 20.0
Weighted AreaUnderPRC: 0.9189915407040701
Mean absolute error: 0.11184360116264196
Coverage of cases: 91.28440366972477
Instances selection time: 20.0
Test time: 8.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.07658789929774457
Kappa statistic: 0.7942690459849006
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 52.75229357798165
Incorrectly Classified Instances: 8.715596330275229
Correctly Classified Instances: 91.28440366972477
Weighted Precision: 0.9146919241922635
Weighted AreaUnderROC: 0.956956289978678
Root mean squared error: 0.2652527545784253
Relative absolute error: 18.348170037762888
Root relative squared error: 53.05055091568506
Weighted TruePositiveRate: 0.9128440366972477
Weighted MatthewsCorrelation: 0.8189695984293691
Weighted FMeasure: 0.9132750371971075
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9402971249165057
Mean absolute error: 0.09174085018881444
Coverage of cases: 94.03669724770643
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9128440366972477
Weighted FalsePositiveRate: 0.08572960883655119
Kappa statistic: 0.8180300500834724
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.970637882018479
Root mean squared error: 0.23141336251479844
Relative absolute error: 15.075059184024015
Root relative squared error: 46.282672502959684
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9588151731240568
Mean absolute error: 0.07537529592012007
Coverage of cases: 95.87155963302752
Instances selection time: 8.0
Test time: 17.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 55.27522935779817
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9211604333263688
Weighted AreaUnderROC: 0.9633528784648188
Root mean squared error: 0.27457815557771886
Relative absolute error: 20.08230022696881
Root relative squared error: 54.915631115543775
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8311473010230891
Weighted FMeasure: 0.918037467120036
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9532889297511973
Mean absolute error: 0.10041150113484404
Coverage of cases: 94.03669724770643
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.07396992755749432
Kappa statistic: 0.8287360335195532
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9211604333263688
Weighted AreaUnderROC: 0.9678393745557926
Root mean squared error: 0.28869414774986424
Relative absolute error: 20.760525783043455
Root relative squared error: 57.73882954997285
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.8311473010230891
Weighted FMeasure: 0.918037467120036
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9620107727443168
Mean absolute error: 0.10380262891521728
Coverage of cases: 92.66055045871559
Instances selection time: 4.0
Test time: 21.0
Accumulative iteration time: 48.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.07396992755749432
Kappa statistic: 0.8287360335195532
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 56.42201834862385
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.9288943042813457
Weighted AreaUnderROC: 0.9639303482587065
Root mean squared error: 0.2725259481968929
Relative absolute error: 19.982931108046493
Root relative squared error: 54.505189639378585
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.848493952677331
Weighted FMeasure: 0.927030531936217
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9591092832433149
Mean absolute error: 0.09991465554023246
Coverage of cases: 94.03669724770643
Instances selection time: 6.0
Test time: 19.0
Accumulative iteration time: 54.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.06821886634977145
Kappa statistic: 0.8470980185867087
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 55.5045871559633
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9328807307486082
Weighted AreaUnderROC: 0.9657515991471215
Root mean squared error: 0.26964595474892616
Relative absolute error: 19.263689981068715
Root relative squared error: 53.92919094978523
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8573230619003901
Weighted FMeasure: 0.9315329241029795
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9599501447258227
Mean absolute error: 0.09631844990534358
Coverage of cases: 94.03669724770643
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 57.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06534333574591003
Kappa statistic: 0.856339513223794
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9328807307486082
Weighted AreaUnderROC: 0.9643745557924663
Root mean squared error: 0.267819885966021
Relative absolute error: 18.532505939046683
Root relative squared error: 53.5639771932042
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8573230619003901
Weighted FMeasure: 0.9315329241029795
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9581966440618985
Mean absolute error: 0.0926625296952334
Coverage of cases: 94.03669724770643
Instances selection time: 3.0
Test time: 11.0
Accumulative iteration time: 60.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06534333574591003
Kappa statistic: 0.856339513223794
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 54.81651376146789
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9328807307486082
Weighted AreaUnderROC: 0.9648187633262258
Root mean squared error: 0.2674709525468711
Relative absolute error: 18.389684690736765
Root relative squared error: 53.49419050937422
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.8573230619003901
Weighted FMeasure: 0.9315329241029795
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9582988025023543
Mean absolute error: 0.09194842345368381
Coverage of cases: 94.03669724770643
Instances selection time: 5.0
Test time: 14.0
Accumulative iteration time: 65.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.06534333574591003
Kappa statistic: 0.856339513223794
Training time: 0.0
		
Time end:Wed Nov 01 14.45.40 EET 2017