Wed Nov 01 14.46.19 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.19 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9082988033923247
Weighted AreaUnderROC: 0.9684926602219837
Root mean squared error: 0.3082385159011211
Relative absolute error: 21.133539201784284
Root relative squared error: 61.64770318022422
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7996936534161513
Weighted FMeasure: 0.8997006577651738
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9677800731696539
Mean absolute error: 0.10566769600892142
Coverage of cases: 92.62672811059907
Instances selection time: 2.0
Test time: 3.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.0815749050044466
Kappa statistic: 0.7927051671732522
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 8.755760368663594
Correctly Classified Instances: 91.2442396313364
Weighted Precision: 0.9187842689749227
Weighted AreaUnderROC: 0.9779806659505907
Root mean squared error: 0.2821532102929092
Relative absolute error: 18.482539042396173
Root relative squared error: 56.43064205858184
Weighted TruePositiveRate: 0.9124423963133641
Weighted MatthewsCorrelation: 0.8243196581109568
Weighted FMeasure: 0.9132442164106588
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9777518597739523
Mean absolute error: 0.09241269521198087
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9124423963133641
Weighted FalsePositiveRate: 0.07284339881962971
Kappa statistic: 0.8198225757112267
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 5.529953917050691
Correctly Classified Instances: 94.47004608294931
Weighted Precision: 0.9468122936245872
Weighted AreaUnderROC: 0.9849624060150377
Root mean squared error: 0.23489963582948994
Relative absolute error: 15.0264450077703
Root relative squared error: 46.97992716589799
Weighted TruePositiveRate: 0.9447004608294931
Weighted MatthewsCorrelation: 0.8864394897105831
Weighted FMeasure: 0.9450158295542056
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9852189717258618
Mean absolute error: 0.0751322250388515
Coverage of cases: 97.23502304147465
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9447004608294931
Weighted FalsePositiveRate: 0.04808391947610963
Kappa statistic: 0.8849721706864564
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 55.76036866359447
Incorrectly Classified Instances: 8.294930875576037
Correctly Classified Instances: 91.70506912442396
Weighted Precision: 0.9207854137447405
Weighted AreaUnderROC: 0.9795023272466881
Root mean squared error: 0.25637301372800747
Relative absolute error: 17.127376015342673
Root relative squared error: 51.27460274560149
Weighted TruePositiveRate: 0.9170506912442397
Weighted MatthewsCorrelation: 0.8306268023732123
Weighted FMeasure: 0.9176498670122075
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9796688452145129
Mean absolute error: 0.08563688007671336
Coverage of cases: 96.31336405529954
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9170506912442397
Weighted FalsePositiveRate: 0.07431886167030478
Kappa statistic: 0.8282019704433498
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 55.06912442396313
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9285581237829142
Weighted AreaUnderROC: 0.9795918367346939
Root mean squared error: 0.2603097699252714
Relative absolute error: 17.334133397652653
Root relative squared error: 52.061953985054274
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8480333063688249
Weighted FMeasure: 0.9266877727389408
Iteration time: 2.0
Weighted AreaUnderPRC: 0.980250377752836
Mean absolute error: 0.08667066698826327
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 9.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06849785754709353
Kappa statistic: 0.8466295609152752
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 55.06912442396313
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.9092045738707405
Weighted AreaUnderROC: 0.9761009667024705
Root mean squared error: 0.2715237215271366
Relative absolute error: 18.352447158536272
Root relative squared error: 54.30474430542732
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.8080527964557579
Weighted FMeasure: 0.9082022414000447
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9769113764362307
Mean absolute error: 0.09176223579268136
Coverage of cases: 95.39170506912443
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.09329776053035815
Kappa statistic: 0.8074534161490682
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 54.83870967741935
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9051513229430954
Weighted AreaUnderROC: 0.9749373433583959
Root mean squared error: 0.28354818615760347
Relative absolute error: 19.351485704015275
Root relative squared error: 56.709637231520695
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7991870746501084
Weighted FMeasure: 0.9036973215797471
Iteration time: 1.0
Weighted AreaUnderPRC: 0.975748327866426
Mean absolute error: 0.09675742852007638
Coverage of cases: 94.93087557603687
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.09620826259196376
Kappa statistic: 0.7982646420824294
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.8972783488912522
Weighted AreaUnderROC: 0.97234156820623
Root mean squared error: 0.2967584924679688
Relative absolute error: 20.2671539765836
Root relative squared error: 59.351698493593766
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.7817604255966546
Weighted FMeasure: 0.8946967243741436
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9729638144186068
Mean absolute error: 0.101335769882918
Coverage of cases: 94.00921658986175
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.10202926671517502
Kappa statistic: 0.7800061709348967
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 53.225806451612904
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9025638148667602
Weighted AreaUnderROC: 0.9712674543501612
Root mean squared error: 0.3006777635179469
Relative absolute error: 20.357081072899177
Root relative squared error: 60.135552703589376
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7923376858325885
Weighted FMeasure: 0.8993498374593648
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9716415696266572
Mean absolute error: 0.10178540536449589
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.0947327997412887
Kappa statistic: 0.7900246305418719
Training time: 0.0
		
Time end:Wed Nov 01 14.46.19 EET 2017