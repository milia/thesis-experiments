Wed Nov 01 14.46.29 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.29 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 52.06422018348624
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9237863274704359
Weighted AreaUnderROC: 0.9431414356787491
Root mean squared error: 0.26498432223404983
Relative absolute error: 18.75177033017555
Root relative squared error: 52.996864446809965
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8381463301648796
Weighted FMeasure: 0.9224039806500435
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9221880900262259
Mean absolute error: 0.09375885165087774
Coverage of cases: 94.03669724770643
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.0755364722912306
Kappa statistic: 0.8371847816536333
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 54.12844036697248
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9356481908046279
Weighted AreaUnderROC: 0.964952025586354
Root mean squared error: 0.233961205805475
Relative absolute error: 16.402495554168357
Root relative squared error: 46.792241161095
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8639864565185936
Weighted FMeasure: 0.9356308004699737
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9530194004830732
Mean absolute error: 0.0820124777708418
Coverage of cases: 96.3302752293578
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07579403115484178
Kappa statistic: 0.8638229519900055
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.669724770642205
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9343341148327433
Weighted AreaUnderROC: 0.9550498653521383
Root mean squared error: 0.23359995368228917
Relative absolute error: 15.604895996226368
Root relative squared error: 46.71999073645783
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.856225786863442
Weighted FMeasure: 0.9301386294391045
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9465759977808043
Mean absolute error: 0.07802447998113184
Coverage of cases: 95.87155963302752
Instances selection time: 4.0
Test time: 6.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.10087993844669184
Kappa statistic: 0.8511064566068663
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.211009174311926
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9424417784050811
Weighted AreaUnderROC: 0.9630863539445629
Root mean squared error: 0.2213323490343445
Relative absolute error: 13.796550676584618
Root relative squared error: 44.266469806868905
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8750108236272491
Weighted FMeasure: 0.9396480128781567
Iteration time: 4.0
Weighted AreaUnderPRC: 0.955451803413681
Mean absolute error: 0.06898275338292309
Coverage of cases: 96.3302752293578
Instances selection time: 4.0
Test time: 7.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.08624472656377354
Kappa statistic: 0.8715438310216662
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9320296464547879
Weighted AreaUnderROC: 0.9529584221748401
Root mean squared error: 0.23915311083091503
Relative absolute error: 15.653044902447535
Root relative squared error: 47.830622166183005
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.854478186063692
Weighted FMeasure: 0.930572578598107
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9440951472394357
Mean absolute error: 0.07826522451223768
Coverage of cases: 95.87155963302752
Instances selection time: 3.0
Test time: 8.0
Accumulative iteration time: 17.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.09199578777149638
Kappa statistic: 0.8524501398790723
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9356481908046279
Weighted AreaUnderROC: 0.9575781805259416
Root mean squared error: 0.23506955163028112
Relative absolute error: 15.165475701137618
Root relative squared error: 47.01391032605623
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8639864565185936
Weighted FMeasure: 0.9356308004699737
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9512556633685594
Mean absolute error: 0.07582737850568809
Coverage of cases: 95.87155963302752
Instances selection time: 4.0
Test time: 9.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07579403115484178
Kappa statistic: 0.8638229519900055
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9402732233725933
Weighted AreaUnderROC: 0.960820895522388
Root mean squared error: 0.23238334457898605
Relative absolute error: 14.315694728014844
Root relative squared error: 46.47666891579721
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8738725167000166
Weighted FMeasure: 0.9402992725370855
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9549348993760872
Mean absolute error: 0.07157847364007422
Coverage of cases: 95.87155963302752
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 24.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.06847642521338262
Kappa statistic: 0.8738313596295967
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 52.981651376146786
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.9270790904565175
Weighted AreaUnderROC: 0.9578447050461976
Root mean squared error: 0.24302283905227234
Relative absolute error: 14.735450566916775
Root relative squared error: 48.60456781045447
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.8459028845587397
Weighted FMeasure: 0.9267613177574264
Iteration time: 3.0
Weighted AreaUnderPRC: 0.949349303684923
Mean absolute error: 0.07367725283458387
Coverage of cases: 95.41284403669725
Instances selection time: 3.0
Test time: 10.0
Accumulative iteration time: 27.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.0771030170249669
Kappa statistic: 0.8457456217937378
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 52.981651376146786
Incorrectly Classified Instances: 7.339449541284404
Correctly Classified Instances: 92.66055045871559
Weighted Precision: 0.9270790904565175
Weighted AreaUnderROC: 0.9556236673773987
Root mean squared error: 0.2430599204152207
Relative absolute error: 14.703400744479323
Root relative squared error: 48.61198408304414
Weighted TruePositiveRate: 0.926605504587156
Weighted MatthewsCorrelation: 0.8459028845587397
Weighted FMeasure: 0.9267613177574264
Iteration time: 2.0
Weighted AreaUnderPRC: 0.947633112827431
Mean absolute error: 0.07351700372239661
Coverage of cases: 95.41284403669725
Instances selection time: 2.0
Test time: 11.0
Accumulative iteration time: 29.0
Weighted Recall: 0.926605504587156
Weighted FalsePositiveRate: 0.0771030170249669
Kappa statistic: 0.8457456217937378
Training time: 0.0
		
Time end:Wed Nov 01 14.46.29 EET 2017