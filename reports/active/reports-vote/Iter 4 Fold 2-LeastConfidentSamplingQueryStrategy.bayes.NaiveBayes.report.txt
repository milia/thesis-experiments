Wed Nov 01 14.46.21 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.21 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.9049323193664363
Weighted AreaUnderROC: 0.9726996061582528
Root mean squared error: 0.3075610999934312
Relative absolute error: 21.435247387644353
Root relative squared error: 61.512219998686234
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.7916559663717887
Weighted FMeasure: 0.8951860458746714
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9742205792191488
Mean absolute error: 0.10717623693822176
Coverage of cases: 92.62672811059907
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.08448540706605222
Kappa statistic: 0.78374279648165
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9278827687940103
Weighted AreaUnderROC: 0.9817400644468315
Root mean squared error: 0.2651482020141264
Relative absolute error: 16.340223625005567
Root relative squared error: 53.02964040282529
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8433897397538223
Weighted FMeasure: 0.9223764041569051
Iteration time: 1.0
Weighted AreaUnderPRC: 0.982363468499188
Mean absolute error: 0.08170111812502784
Coverage of cases: 94.00921658986175
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.06263642978413776
Kappa statistic: 0.8387886203732028
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 54.60829493087557
Incorrectly Classified Instances: 6.451612903225806
Correctly Classified Instances: 93.54838709677419
Weighted Precision: 0.9390070126227209
Weighted AreaUnderROC: 0.9881847475832438
Root mean squared error: 0.22979772401270931
Relative absolute error: 13.963287783007603
Root relative squared error: 45.95954480254186
Weighted TruePositiveRate: 0.9354838709677419
Weighted MatthewsCorrelation: 0.8689159189138362
Weighted FMeasure: 0.9359498965650503
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9884683490246782
Mean absolute error: 0.06981643891503801
Coverage of cases: 97.6958525345622
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9354838709677419
Weighted FalsePositiveRate: 0.05390492359932088
Kappa statistic: 0.8663793103448275
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9261186264308013
Weighted AreaUnderROC: 0.984514858575009
Root mean squared error: 0.24831767539866614
Relative absolute error: 15.602568949532413
Root relative squared error: 49.66353507973323
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8411910241920598
Weighted FMeasure: 0.9222789770515238
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9851724483448785
Mean absolute error: 0.07801284474766207
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.06702239469641845
Kappa statistic: 0.8380952380952381
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 54.14746543778802
Incorrectly Classified Instances: 7.373271889400922
Correctly Classified Instances: 92.62672811059907
Weighted Precision: 0.9298962131837307
Weighted AreaUnderROC: 0.9838882921589689
Root mean squared error: 0.2527934388284767
Relative absolute error: 15.884962228402099
Root relative squared error: 50.55868776569534
Weighted TruePositiveRate: 0.9262672811059908
Weighted MatthewsCorrelation: 0.8497713606435244
Weighted FMeasure: 0.9267998817886289
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9846330247637622
Mean absolute error: 0.0794248111420105
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9262672811059908
Weighted FalsePositiveRate: 0.06411189263481283
Kappa statistic: 0.8472906403940886
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 54.3778801843318
Incorrectly Classified Instances: 7.8341013824884795
Correctly Classified Instances: 92.16589861751152
Weighted Precision: 0.9246327149552955
Weighted AreaUnderROC: 0.9829036877909059
Root mean squared error: 0.26190546472867515
Relative absolute error: 16.48275080850868
Root relative squared error: 52.381092945735034
Weighted TruePositiveRate: 0.9216589861751152
Weighted MatthewsCorrelation: 0.8392791910875476
Weighted FMeasure: 0.9221671441026281
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9837784861105443
Mean absolute error: 0.0824137540425434
Coverage of cases: 95.85253456221199
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9216589861751152
Weighted FalsePositiveRate: 0.07140835960869917
Kappa statistic: 0.8373958654736193
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 8.294930875576037
Correctly Classified Instances: 91.70506912442396
Weighted Precision: 0.9207854137447405
Weighted AreaUnderROC: 0.9797708557107053
Root mean squared error: 0.2769986768791021
Relative absolute error: 17.72706410121461
Root relative squared error: 55.39973537582043
Weighted TruePositiveRate: 0.9170506912442397
Weighted MatthewsCorrelation: 0.8306268023732123
Weighted FMeasure: 0.9176498670122075
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9808013763591219
Mean absolute error: 0.08863532050607305
Coverage of cases: 94.47004608294931
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9170506912442397
Weighted FalsePositiveRate: 0.07431886167030478
Kappa statistic: 0.8282019704433498
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.9116746143057504
Weighted AreaUnderROC: 0.9783387039026138
Root mean squared error: 0.28491709155236833
Relative absolute error: 18.47266419461139
Root relative squared error: 56.98341831047367
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.8114822441029004
Weighted FMeasure: 0.9084998522357862
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9795092502456458
Mean absolute error: 0.09236332097305695
Coverage of cases: 94.00921658986175
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.08452583070579675
Kappa statistic: 0.8091133004926108
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 52.764976958525345
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.9133144728839191
Weighted AreaUnderROC: 0.9766380236305048
Root mean squared error: 0.28867808717954
Relative absolute error: 18.8799632815012
Root relative squared error: 57.735617435908004
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.813614934148821
Weighted FMeasure: 0.9086229159115622
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9779222376408612
Mean absolute error: 0.09439981640750601
Coverage of cases: 93.08755760368663
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.08013986579351604
Kappa statistic: 0.8099325567136725
Training time: 0.0
		
Time end:Wed Nov 01 14.46.21 EET 2017