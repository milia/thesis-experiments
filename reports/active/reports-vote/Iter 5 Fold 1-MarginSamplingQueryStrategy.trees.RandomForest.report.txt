Wed Nov 01 14.46.56 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: RandomForest-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.56 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 66.28440366972477
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9248165787660398
Weighted AreaUnderROC: 0.9828091684434966
Root mean squared error: 0.22294078221652358
Relative absolute error: 21.261843893576877
Root relative squared error: 44.58815644330472
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8362539035404984
Weighted FMeasure: 0.9208237800309854
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9812103056353586
Mean absolute error: 0.10630921946788438
Coverage of cases: 99.54128440366972
Instances selection time: 6.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.11107307499201241
Kappa statistic: 0.8312539841544486
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 70.87155963302752
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9483604637174641
Weighted AreaUnderROC: 0.9756130063965885
Root mean squared error: 0.22945554463485726
Relative absolute error: 21.906715254972315
Root relative squared error: 45.891108926971455
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8884013914874982
Weighted FMeasure: 0.9453583114133572
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9671954805530549
Mean absolute error: 0.10953357627486157
Coverage of cases: 98.62385321100918
Instances selection time: 1.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.04339051792153257
Kappa statistic: 0.8858240223463687
Training time: 3.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 63.99082568807339
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9419751340267805
Weighted AreaUnderROC: 0.9747245913290689
Root mean squared error: 0.22103041777031612
Relative absolute error: 18.53126168120819
Root relative squared error: 44.20608355406323
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8764997936359007
Weighted FMeasure: 0.9406618675559155
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9657511121212459
Mean absolute error: 0.09265630840604094
Coverage of cases: 98.62385321100918
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05515019920058945
Kappa statistic: 0.8754942447939549
Training time: 3.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 63.07339449541284
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.936216226700545
Weighted AreaUnderROC: 0.9817874911158492
Root mean squared error: 0.20533437632147433
Relative absolute error: 17.402759382475878
Root relative squared error: 41.066875264294865
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8651882672018902
Weighted FMeasure: 0.9359161530377482
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9755249601935934
Mean absolute error: 0.0870137969123794
Coverage of cases: 99.08256880733946
Instances selection time: 4.0
Test time: 3.0
Accumulative iteration time: 27.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.06690988047964633
Kappa statistic: 0.8650274190695207
Training time: 4.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 61.00917431192661
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9369506640148841
Weighted AreaUnderROC: 0.9796825994535839
Root mean squared error: 0.22193793507265547
Relative absolute error: 17.246861485654293
Root relative squared error: 44.38758701453109
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8662604035129392
Weighted FMeasure: 0.9360400462918065
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9758816905748534
Mean absolute error: 0.08623430742827146
Coverage of cases: 98.62385321100918
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.062467805142048596
Kappa statistic: 0.8656216977809089
Training time: 4.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 62.15596330275229
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9419751340267805
Weighted AreaUnderROC: 0.9808546552949537
Root mean squared error: 0.21736642389598823
Relative absolute error: 17.368011815571197
Root relative squared error: 43.47328477919765
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8764997936359007
Weighted FMeasure: 0.9406618675559155
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9783222704355384
Mean absolute error: 0.08684005907785598
Coverage of cases: 99.08256880733946
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 38.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05515019920058945
Kappa statistic: 0.8754942447939549
Training time: 5.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 62.61467889908257
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.9431370478831289
Weighted AreaUnderROC: 0.9800106609808102
Root mean squared error: 0.217859260077851
Relative absolute error: 17.399197787200922
Root relative squared error: 43.5718520155702
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.877996911510556
Weighted FMeasure: 0.9407599564132306
Iteration time: 6.0
Weighted AreaUnderPRC: 0.974134042610345
Mean absolute error: 0.08699598893600462
Coverage of cases: 99.08256880733946
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.05070812386299173
Kappa statistic: 0.876038841746129
Training time: 6.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 62.61467889908257
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9379778287461773
Weighted AreaUnderROC: 0.98431947405828
Root mean squared error: 0.22310599432451175
Relative absolute error: 17.79615156784032
Root relative squared error: 44.621198864902354
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8676381962794484
Weighted FMeasure: 0.9361517154441897
Iteration time: 6.0
Weighted AreaUnderPRC: 0.9846050985827475
Mean absolute error: 0.08898075783920159
Coverage of cases: 100.0
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 50.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.05802572980445088
Kappa statistic: 0.8662107662633702
Training time: 5.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 63.30275229357798
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9369506640148841
Weighted AreaUnderROC: 0.9819207533759773
Root mean squared error: 0.21996332647622507
Relative absolute error: 17.541223192018492
Root relative squared error: 43.99266529524502
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8662604035129392
Weighted FMeasure: 0.9360400462918065
Iteration time: 5.0
Weighted AreaUnderPRC: 0.9791276607315317
Mean absolute error: 0.08770611596009247
Coverage of cases: 99.08256880733946
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 55.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.062467805142048596
Kappa statistic: 0.8656216977809089
Training time: 4.0
		
Time end:Wed Nov 01 14.46.57 EET 2017