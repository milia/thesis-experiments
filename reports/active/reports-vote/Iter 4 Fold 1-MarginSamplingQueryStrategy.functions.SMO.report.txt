Wed Nov 01 14.46.44 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.44 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9357798165137615
Weighted AreaUnderROC: 0.9322139303482587
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8644278606965174
Weighted FMeasure: 0.9357798165137615
Iteration time: 7.0
Weighted AreaUnderPRC: 0.9080229586014879
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.07135195581724404
Kappa statistic: 0.8644278606965176
Training time: 6.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.128440366972477
Correctly Classified Instances: 95.87155963302752
Weighted Precision: 0.9592899829480278
Weighted AreaUnderROC: 0.9508706467661692
Root mean squared error: 0.20318563844357893
Relative absolute error: 8.256880733944955
Root relative squared error: 40.637127688715786
Weighted TruePositiveRate: 0.9587155963302753
Weighted MatthewsCorrelation: 0.9129627418338602
Weighted FMeasure: 0.9584604622596404
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9372924605577572
Mean absolute error: 0.04128440366972477
Coverage of cases: 95.87155963302752
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 15.0
Weighted Recall: 0.9587155963302753
Weighted FalsePositiveRate: 0.056974302797936915
Kappa statistic: 0.9118677567154794
Training time: 7.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 7.798165137614679
Correctly Classified Instances: 92.20183486238533
Weighted Precision: 0.9248165787660398
Weighted AreaUnderROC: 0.9054726368159204
Root mean squared error: 0.2792519496371454
Relative absolute error: 15.59633027522936
Root relative squared error: 55.85038992742908
Weighted TruePositiveRate: 0.9220183486238532
Weighted MatthewsCorrelation: 0.8362539035404984
Weighted FMeasure: 0.9208237800309854
Iteration time: 6.0
Weighted AreaUnderPRC: 0.8840070821984204
Mean absolute error: 0.0779816513761468
Coverage of cases: 92.20183486238533
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 21.0
Weighted Recall: 0.9220183486238532
Weighted FalsePositiveRate: 0.11107307499201241
Kappa statistic: 0.8312539841544486
Training time: 6.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9418546987354327
Weighted AreaUnderROC: 0.9166666666666666
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8686221874350808
Weighted FMeasure: 0.9344548352941874
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9023799070512311
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.10244648318042814
Kappa statistic: 0.86007702182285
Training time: 13.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 9.63302752293578
Correctly Classified Instances: 90.36697247706422
Weighted Precision: 0.9111541750509861
Weighted AreaUnderROC: 0.8794420753375978
Root mean squared error: 0.3103711894318766
Relative absolute error: 19.26605504587156
Root relative squared error: 62.074237886375315
Weighted TruePositiveRate: 0.9036697247706422
Weighted MatthewsCorrelation: 0.8004637145150094
Weighted FMeasure: 0.9011201678173196
Iteration time: 13.0
Weighted AreaUnderPRC: 0.8578022416741048
Mean absolute error: 0.0963302752293578
Coverage of cases: 90.36697247706422
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 49.0
Weighted Recall: 0.9036697247706422
Weighted FalsePositiveRate: 0.14478557409544676
Kappa statistic: 0.7886621733911919
Training time: 12.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 10.091743119266056
Correctly Classified Instances: 89.90825688073394
Weighted Precision: 0.9074448004916379
Weighted AreaUnderROC: 0.8734896943852167
Root mean squared error: 0.3176750402418493
Relative absolute error: 20.18348623853211
Root relative squared error: 63.53500804836985
Weighted TruePositiveRate: 0.8990825688073395
Weighted MatthewsCorrelation: 0.7912375673819233
Weighted FMeasure: 0.8962032462949894
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8515044258004048
Mean absolute error: 0.10091743119266056
Coverage of cases: 89.90825688073394
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.8990825688073395
Weighted FalsePositiveRate: 0.15210318003690587
Kappa statistic: 0.7780862483805294
Training time: 13.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 8.256880733944953
Correctly Classified Instances: 91.74311926605505
Weighted Precision: 0.9225390528142822
Weighted AreaUnderROC: 0.8972992181947407
Root mean squared error: 0.28734788556634544
Relative absolute error: 16.51376146788991
Root relative squared error: 57.46957711326909
Weighted TruePositiveRate: 0.9174311926605505
Weighted MatthewsCorrelation: 0.828246998338951
Weighted FMeasure: 0.9157276453782408
Iteration time: 14.0
Weighted AreaUnderPRC: 0.8769787522922781
Mean absolute error: 0.08256880733944955
Coverage of cases: 91.74311926605505
Instances selection time: 1.0
Test time: 3.0
Accumulative iteration time: 77.0
Weighted Recall: 0.9174311926605505
Weighted FalsePositiveRate: 0.1228327562710693
Kappa statistic: 0.8200990280579499
Training time: 13.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9383626746012067
Weighted AreaUnderROC: 0.9211087420042644
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8656038695451582
Weighted FMeasure: 0.9349025714975575
Iteration time: 18.0
Weighted AreaUnderPRC: 0.9032443351524236
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 95.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.09356233250523269
Kappa statistic: 0.861348355442486
Training time: 17.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9507568363707345
Weighted AreaUnderROC: 0.9389658848614072
Root mean squared error: 0.224630175168721
Relative absolute error: 10.091743119266056
Root relative squared error: 44.9260350337442
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8939184241669917
Weighted FMeasure: 0.9490865576386117
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9235030451535619
Mean absolute error: 0.05045871559633028
Coverage of cases: 94.95412844036697
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 103.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.07160951468085522
Kappa statistic: 0.8917967692446529
Training time: 8.0
		
Time end:Wed Nov 01 14.46.45 EET 2017