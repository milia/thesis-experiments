Wed Nov 01 14.47.18 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.18 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9051513229430954
Weighted AreaUnderROC: 0.9648227712137487
Root mean squared error: 0.2953701987717769
Relative absolute error: 19.51492142155106
Root relative squared error: 59.074039754355375
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7991870746501084
Weighted FMeasure: 0.9036973215797471
Iteration time: 3.0
Weighted AreaUnderPRC: 0.9655076227665516
Mean absolute error: 0.09757460710775528
Coverage of cases: 93.08755760368663
Instances selection time: 3.0
Test time: 4.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.09620826259196376
Kappa statistic: 0.7982646420824294
Training time: 0.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.900054559094319
Weighted AreaUnderROC: 0.9658968850698175
Root mean squared error: 0.299164912602475
Relative absolute error: 19.90683308930345
Root relative squared error: 59.832982520495
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7887838451556592
Weighted FMeasure: 0.899022465540049
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9668837496674751
Mean absolute error: 0.09953416544651726
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.1035047295658501
Kappa statistic: 0.7881987577639752
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9034730607785637
Weighted AreaUnderROC: 0.9659863945578231
Root mean squared error: 0.29973400595326233
Relative absolute error: 19.869973637084684
Root relative squared error: 59.946801190652465
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7965363066901977
Weighted FMeasure: 0.9033295278156929
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9670906589300692
Mean absolute error: 0.09934986818542342
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.10498019241652518
Kappa statistic: 0.7964989059080962
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9034730607785637
Weighted AreaUnderROC: 0.9669709989258861
Root mean squared error: 0.2992901754542808
Relative absolute error: 19.717698471308974
Root relative squared error: 59.85803509085616
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.7965363066901977
Weighted FMeasure: 0.9033295278156929
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9681453694019194
Mean absolute error: 0.09858849235654488
Coverage of cases: 93.08755760368663
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.10498019241652518
Kappa statistic: 0.7964989059080962
Training time: 0.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.8986175115207373
Weighted AreaUnderROC: 0.9669709989258861
Root mean squared error: 0.30171959228324274
Relative absolute error: 19.908091755173185
Root relative squared error: 60.34391845664855
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7863408521303259
Weighted FMeasure: 0.8986175115207373
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9680051247490067
Mean absolute error: 0.09954045877586593
Coverage of cases: 93.08755760368663
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.11227665939041151
Kappa statistic: 0.7863408521303258
Training time: 0.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 52.30414746543779
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.8986175115207373
Weighted AreaUnderROC: 0.9669709989258861
Root mean squared error: 0.30245105362597796
Relative absolute error: 19.992763423019568
Root relative squared error: 60.490210725195595
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7863408521303259
Weighted FMeasure: 0.8986175115207373
Iteration time: 1.0
Weighted AreaUnderPRC: 0.96805339849263
Mean absolute error: 0.09996381711509783
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 9.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.11227665939041151
Kappa statistic: 0.7863408521303258
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.8942757748260595
Weighted AreaUnderROC: 0.9656283566058004
Root mean squared error: 0.3078607907855236
Relative absolute error: 20.646129704068432
Root relative squared error: 61.57215815710472
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.77715434004389
Weighted FMeasure: 0.8941228161790922
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9670058762900937
Mean absolute error: 0.10323064852034217
Coverage of cases: 92.16589861751152
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.11518716145201714
Kappa statistic: 0.777117849327915
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 52.30414746543779
Incorrectly Classified Instances: 10.599078341013826
Correctly Classified Instances: 89.40092165898618
Weighted Precision: 0.8942757748260595
Weighted AreaUnderROC: 0.96437522377372
Root mean squared error: 0.31318373010405876
Relative absolute error: 21.1009455530111
Root relative squared error: 62.63674602081175
Weighted TruePositiveRate: 0.8940092165898618
Weighted MatthewsCorrelation: 0.77715434004389
Weighted FMeasure: 0.8941228161790922
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9658294109566514
Mean absolute error: 0.1055047277650555
Coverage of cases: 91.70506912442396
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 11.0
Weighted Recall: 0.8940092165898618
Weighted FalsePositiveRate: 0.11518716145201714
Kappa statistic: 0.777117849327915
Training time: 0.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.899199990837404
Weighted AreaUnderROC: 0.9660759040458289
Root mean squared error: 0.3070963899966157
Relative absolute error: 20.473450030447786
Root relative squared error: 61.41927799932314
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7874211178544184
Weighted FMeasure: 0.8988298545224542
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9671994442739791
Mean absolute error: 0.10236725015223894
Coverage of cases: 92.16589861751152
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.1078906944781308
Kappa statistic: 0.7872738615096694
Training time: 4.0
		
Time end:Wed Nov 01 14.47.18 EET 2017