Wed Nov 01 14.47.17 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.17 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 53.68663594470046
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9082988033923247
Weighted AreaUnderROC: 0.9684926602219837
Root mean squared error: 0.3082385159011211
Relative absolute error: 21.133539201784284
Root relative squared error: 61.64770318022422
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7996936534161513
Weighted FMeasure: 0.8997006577651738
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9677800731696539
Mean absolute error: 0.10566769600892142
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.0815749050044466
Kappa statistic: 0.7927051671732522
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 52.764976958525345
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.9117281897794035
Weighted AreaUnderROC: 0.969298245614035
Root mean squared error: 0.30603637807126155
Relative absolute error: 20.61978049406092
Root relative squared error: 61.20727561425231
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.8078151161943748
Weighted FMeasure: 0.9042148292843742
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9690934499434719
Mean absolute error: 0.1030989024703046
Coverage of cases: 92.16589861751152
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.07866440294284097
Kappa statistic: 0.8017057569296374
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9082988033923247
Weighted AreaUnderROC: 0.9698353025420695
Root mean squared error: 0.3070182092991414
Relative absolute error: 20.711614418593904
Root relative squared error: 61.403641859828284
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7996936534161513
Weighted FMeasure: 0.8997006577651738
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9702186732148192
Mean absolute error: 0.10355807209296952
Coverage of cases: 92.16589861751152
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 6.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.0815749050044466
Kappa statistic: 0.7927051671732522
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 52.07373271889401
Incorrectly Classified Instances: 9.216589861751151
Correctly Classified Instances: 90.78341013824885
Weighted Precision: 0.9173992177516168
Weighted AreaUnderROC: 0.9718940207662013
Root mean squared error: 0.30155968750618745
Relative absolute error: 20.07559411564132
Root relative squared error: 60.31193750123749
Weighted TruePositiveRate: 0.9078341013824884
Weighted MatthewsCorrelation: 0.8187047755449094
Weighted FMeasure: 0.9088187797865218
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9725240813494851
Mean absolute error: 0.1003779705782066
Coverage of cases: 92.16589861751152
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 8.0
Weighted Recall: 0.9078341013824884
Weighted FalsePositiveRate: 0.07136793596895465
Kappa statistic: 0.8115501519756838
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 52.764976958525345
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9061250333244469
Weighted AreaUnderROC: 0.9714464733261726
Root mean squared error: 0.3022839153003726
Relative absolute error: 20.13292362843357
Root relative squared error: 60.456783060074514
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7969741788128749
Weighted FMeasure: 0.8996020658707984
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9722321740131651
Mean absolute error: 0.10066461814216786
Coverage of cases: 92.62672811059907
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 10.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.08596086991672729
Kappa statistic: 0.7918192918192918
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9061250333244469
Weighted AreaUnderROC: 0.9718045112781954
Root mean squared error: 0.30078903378745203
Relative absolute error: 20.002368815281972
Root relative squared error: 60.157806757490405
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7969741788128749
Weighted FMeasure: 0.8996020658707984
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9725763883655285
Mean absolute error: 0.10001184407640987
Coverage of cases: 92.16589861751152
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 12.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.08596086991672729
Kappa statistic: 0.7918192918192918
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 52.534562211981566
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9061250333244469
Weighted AreaUnderROC: 0.9712674543501612
Root mean squared error: 0.30280200384157435
Relative absolute error: 20.083333634479683
Root relative squared error: 60.56040076831487
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7969741788128749
Weighted FMeasure: 0.8996020658707984
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9717255573692534
Mean absolute error: 0.10041666817239842
Coverage of cases: 92.16589861751152
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.08596086991672729
Kappa statistic: 0.7918192918192918
Training time: 1.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 53.45622119815668
Incorrectly Classified Instances: 9.67741935483871
Correctly Classified Instances: 90.3225806451613
Weighted Precision: 0.909685769155835
Weighted AreaUnderROC: 0.9721625492302183
Root mean squared error: 0.30067159156335255
Relative absolute error: 20.10069996636407
Root relative squared error: 60.13431831267051
Weighted TruePositiveRate: 0.9032258064516129
Weighted MatthewsCorrelation: 0.8052495764680914
Weighted FMeasure: 0.9041120286644121
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9729121412191691
Mean absolute error: 0.10050349983182034
Coverage of cases: 92.62672811059907
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 14.0
Weighted Recall: 0.9032258064516129
Weighted FalsePositiveRate: 0.08305036785512168
Kappa statistic: 0.8008565310492505
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 52.995391705069125
Incorrectly Classified Instances: 10.138248847926267
Correctly Classified Instances: 89.86175115207374
Weighted Precision: 0.9061250333244469
Weighted AreaUnderROC: 0.9713569638381667
Root mean squared error: 0.3027708494364234
Relative absolute error: 20.237074461314506
Root relative squared error: 60.55416988728468
Weighted TruePositiveRate: 0.8986175115207373
Weighted MatthewsCorrelation: 0.7969741788128749
Weighted FMeasure: 0.8996020658707984
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9717065468396345
Mean absolute error: 0.10118537230657253
Coverage of cases: 92.62672811059907
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8986175115207373
Weighted FalsePositiveRate: 0.08596086991672729
Kappa statistic: 0.7918192918192918
Training time: 1.0
		
Time end:Wed Nov 01 14.47.17 EET 2017