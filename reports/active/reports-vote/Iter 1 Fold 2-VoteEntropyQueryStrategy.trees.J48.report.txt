Wed Nov 01 14.47.29 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 175
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.29 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 175
	
Mean region size: 71.88940092165899
Incorrectly Classified Instances: 3.686635944700461
Correctly Classified Instances: 96.31336405529954
Weighted Precision: 0.9641046625292686
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.19283716866780873
Relative absolute error: 16.306933876326216
Root relative squared error: 38.56743373356174
Weighted TruePositiveRate: 0.9631336405529954
Weighted MatthewsCorrelation: 0.923666504256351
Weighted FMeasure: 0.9632808965600179
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.08153466938163108
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 2.0
Weighted Recall: 0.9631336405529954
Weighted FalsePositiveRate: 0.032055946317406415
Kappa statistic: 0.9229813664596275
Training time: 2.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 155
	
Mean region size: 71.88940092165899
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9460797684321857
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.204611605849206
Relative absolute error: 18.353280104432184
Root relative squared error: 40.922321169841204
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8815299030395531
Weighted FMeasure: 0.9406407796493981
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.09176640052216091
Coverage of cases: 99.53917050691244
Instances selection time: 1.0
Test time: 0.0
Accumulative iteration time: 3.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04222249171315385
Kappa statistic: 0.8767207096971552
Training time: 0.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 135
	
Mean region size: 79.72350230414746
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9460797684321857
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.21416396760282247
Relative absolute error: 18.86509968046342
Root relative squared error: 42.83279352056449
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8815299030395531
Weighted FMeasure: 0.9406407796493981
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.0943254984023171
Coverage of cases: 97.23502304147465
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 4.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04222249171315385
Kappa statistic: 0.8767207096971552
Training time: 1.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 115
	
Mean region size: 79.72350230414746
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9460797684321857
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.21962280140715534
Relative absolute error: 18.278996293402844
Root relative squared error: 43.92456028143107
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8815299030395531
Weighted FMeasure: 0.9406407796493981
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.09139498146701422
Coverage of cases: 97.23502304147465
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 5.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04222249171315385
Kappa statistic: 0.8767207096971552
Training time: 1.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 95
	
Mean region size: 51.61290322580645
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9460797684321857
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.21129788555241608
Relative absolute error: 18.326134545084724
Root relative squared error: 42.259577110483214
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8815299030395531
Weighted FMeasure: 0.9406407796493981
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.09163067272542362
Coverage of cases: 96.7741935483871
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 6.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04222249171315385
Kappa statistic: 0.8767207096971552
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 75
	
Mean region size: 71.88940092165899
Incorrectly Classified Instances: 5.990783410138249
Correctly Classified Instances: 94.00921658986175
Weighted Precision: 0.9460797684321857
Weighted AreaUnderROC: 0.9706856426781239
Root mean squared error: 0.20250273818046385
Relative absolute error: 17.801671236206428
Root relative squared error: 40.50054763609277
Weighted TruePositiveRate: 0.9400921658986175
Weighted MatthewsCorrelation: 0.8815299030395531
Weighted FMeasure: 0.9406407796493981
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9551265655029816
Mean absolute error: 0.08900835618103213
Coverage of cases: 99.53917050691244
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 7.0
Weighted Recall: 0.9400921658986175
Weighted FalsePositiveRate: 0.04222249171315385
Kappa statistic: 0.8767207096971552
Training time: 1.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 55
	
Mean region size: 53.225806451612904
Incorrectly Classified Instances: 5.0691244239631335
Correctly Classified Instances: 94.93087557603687
Weighted Precision: 0.9519870810193392
Weighted AreaUnderROC: 0.9711331901181527
Root mean squared error: 0.19557197853200203
Relative absolute error: 13.772122350838693
Root relative squared error: 39.11439570640041
Weighted TruePositiveRate: 0.9493087557603687
Weighted MatthewsCorrelation: 0.8967979565784406
Weighted FMeasure: 0.9496375638311122
Iteration time: 4.0
Weighted AreaUnderPRC: 0.9642745788416132
Mean absolute error: 0.06886061175419346
Coverage of cases: 97.23502304147465
Instances selection time: 0.0
Test time: 5.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9493087557603687
Weighted FalsePositiveRate: 0.0407874525022233
Kappa statistic: 0.8947855600123419
Training time: 4.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 35
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9678540624460927
Weighted AreaUnderROC: 0.9829931972789115
Root mean squared error: 0.1686541032566287
Relative absolute error: 12.366072845567793
Root relative squared error: 33.730820651325736
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9322100732143521
Weighted FMeasure: 0.9677765092718976
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9754507845758225
Mean absolute error: 0.06183036422783896
Coverage of cases: 98.61751152073732
Instances selection time: 3.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.03353140916808149
Kappa statistic: 0.9321663019693655
Training time: 5.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 15
	
Mean region size: 55.52995391705069
Incorrectly Classified Instances: 3.225806451612903
Correctly Classified Instances: 96.7741935483871
Weighted Precision: 0.9678540624460927
Weighted AreaUnderROC: 0.9829931972789115
Root mean squared error: 0.16778184329063553
Relative absolute error: 11.900790386418375
Root relative squared error: 33.556368658127106
Weighted TruePositiveRate: 0.967741935483871
Weighted MatthewsCorrelation: 0.9322100732143521
Weighted FMeasure: 0.9677765092718976
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9754507845758225
Mean absolute error: 0.059503951932091875
Coverage of cases: 98.61751152073732
Instances selection time: 0.0
Test time: 1.0
Accumulative iteration time: 21.0
Weighted Recall: 0.967741935483871
Weighted FalsePositiveRate: 0.03353140916808149
Kappa statistic: 0.9321663019693655
Training time: 2.0
		
Time end:Wed Nov 01 14.47.29 EET 2017