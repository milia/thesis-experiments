Wed Nov 01 14.47.20 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: VoteEntropyQueryStrategy-1cores
Time begining: Wed Nov 01 14.47.20 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 54.357798165137616
Incorrectly Classified Instances: 11.926605504587156
Correctly Classified Instances: 88.07339449541284
Weighted Precision: 0.8865584051683426
Weighted AreaUnderROC: 0.9663290689410092
Root mean squared error: 0.3081542298483304
Relative absolute error: 22.28314179973221
Root relative squared error: 61.63084596966608
Weighted TruePositiveRate: 0.8807339449541285
Weighted MatthewsCorrelation: 0.7570986252724465
Weighted FMeasure: 0.8817727573837588
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9681427743407082
Mean absolute error: 0.11141570899866104
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 2.0
Weighted Recall: 0.8807339449541285
Weighted FalsePositiveRate: 0.1103003984011789
Kappa statistic: 0.7536937250130368
Training time: 1.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 11.46788990825688
Correctly Classified Instances: 88.53211009174312
Weighted Precision: 0.8920100654410696
Weighted AreaUnderROC: 0.9658848614072496
Root mean squared error: 0.31629159114237504
Relative absolute error: 22.758568395370148
Root relative squared error: 63.258318228475005
Weighted TruePositiveRate: 0.8853211009174312
Weighted MatthewsCorrelation: 0.7678427368202981
Weighted FMeasure: 0.8863905365565509
Iteration time: 2.0
Weighted AreaUnderPRC: 0.9677401457997457
Mean absolute error: 0.11379284197685073
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 4.0
Weighted Recall: 0.8853211009174312
Weighted FalsePositiveRate: 0.10298279245971975
Kappa statistic: 0.7636805133986645
Training time: 1.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.89908256880734
Incorrectly Classified Instances: 11.009174311926605
Correctly Classified Instances: 88.9908256880734
Weighted Precision: 0.8975221837870356
Weighted AreaUnderROC: 0.9657960199004975
Root mean squared error: 0.31812713699546524
Relative absolute error: 22.862228357667806
Root relative squared error: 63.62542739909305
Weighted TruePositiveRate: 0.8899082568807339
Weighted MatthewsCorrelation: 0.7786278700775608
Weighted FMeasure: 0.8909977064220183
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9673177838089243
Mean absolute error: 0.11431114178833904
Coverage of cases: 92.20183486238533
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.8899082568807339
Weighted FalsePositiveRate: 0.0956651865182606
Kappa statistic: 0.7736240913811007
Training time: 0.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 11.009174311926605
Correctly Classified Instances: 88.9908256880734
Weighted Precision: 0.8975221837870356
Weighted AreaUnderROC: 0.9675728500355366
Root mean squared error: 0.3144906308905827
Relative absolute error: 22.369200995004277
Root relative squared error: 62.89812617811654
Weighted TruePositiveRate: 0.8899082568807339
Weighted MatthewsCorrelation: 0.7786278700775608
Weighted FMeasure: 0.8909977064220183
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9691820476033743
Mean absolute error: 0.11184600497502138
Coverage of cases: 92.66055045871559
Instances selection time: 5.0
Test time: 2.0
Accumulative iteration time: 15.0
Weighted Recall: 0.8899082568807339
Weighted FalsePositiveRate: 0.0956651865182606
Kappa statistic: 0.7736240913811007
Training time: 5.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 11.926605504587156
Correctly Classified Instances: 88.07339449541284
Weighted Precision: 0.8865584051683426
Weighted AreaUnderROC: 0.9665067519545132
Root mean squared error: 0.3149864579240899
Relative absolute error: 22.436771476374304
Root relative squared error: 62.99729158481798
Weighted TruePositiveRate: 0.8807339449541285
Weighted MatthewsCorrelation: 0.7570986252724465
Weighted FMeasure: 0.8817727573837588
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9679626973174701
Mean absolute error: 0.11218385738187152
Coverage of cases: 92.66055045871559
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 16.0
Weighted Recall: 0.8807339449541285
Weighted FalsePositiveRate: 0.1103003984011789
Kappa statistic: 0.7536937250130368
Training time: 1.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 53.440366972477065
Incorrectly Classified Instances: 11.926605504587156
Correctly Classified Instances: 88.07339449541284
Weighted Precision: 0.8865584051683426
Weighted AreaUnderROC: 0.9665067519545132
Root mean squared error: 0.31310733679516983
Relative absolute error: 22.260625537979987
Root relative squared error: 62.621467359033964
Weighted TruePositiveRate: 0.8807339449541285
Weighted MatthewsCorrelation: 0.7570986252724465
Weighted FMeasure: 0.8817727573837588
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9679165961634423
Mean absolute error: 0.11130312768989993
Coverage of cases: 92.66055045871559
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 17.0
Weighted Recall: 0.8807339449541285
Weighted FalsePositiveRate: 0.1103003984011789
Kappa statistic: 0.7536937250130368
Training time: 0.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 52.981651376146786
Incorrectly Classified Instances: 11.926605504587156
Correctly Classified Instances: 88.07339449541284
Weighted Precision: 0.8865584051683426
Weighted AreaUnderROC: 0.965174129353234
Root mean squared error: 0.31964041109804986
Relative absolute error: 22.841618569480772
Root relative squared error: 63.92808221960997
Weighted TruePositiveRate: 0.8807339449541285
Weighted MatthewsCorrelation: 0.7570986252724465
Weighted FMeasure: 0.8817727573837588
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9668133928470423
Mean absolute error: 0.11420809284740385
Coverage of cases: 91.74311926605505
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.8807339449541285
Weighted FalsePositiveRate: 0.1103003984011789
Kappa statistic: 0.7536937250130368
Training time: 0.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 53.211009174311926
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.9030971607861283
Weighted AreaUnderROC: 0.9663290689410092
Root mean squared error: 0.3151384054250148
Relative absolute error: 22.196889899904498
Root relative squared error: 63.02768108500296
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.789456455588754
Weighted FMeasure: 0.8955949189955267
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9678905417651639
Mean absolute error: 0.11098444949952249
Coverage of cases: 91.28440366972477
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 19.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.08834758057680145
Kappa statistic: 0.7835247387963042
Training time: 1.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 53.211009174311926
Incorrectly Classified Instances: 10.55045871559633
Correctly Classified Instances: 89.44954128440367
Weighted Precision: 0.9030971607861283
Weighted AreaUnderROC: 0.9657960199004975
Root mean squared error: 0.3144775091274968
Relative absolute error: 22.005491827475414
Root relative squared error: 62.89550182549936
Weighted TruePositiveRate: 0.8944954128440367
Weighted MatthewsCorrelation: 0.789456455588754
Weighted FMeasure: 0.8955949189955267
Iteration time: 1.0
Weighted AreaUnderPRC: 0.9666069953903776
Mean absolute error: 0.11002745913737706
Coverage of cases: 91.28440366972477
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 20.0
Weighted Recall: 0.8944954128440367
Weighted FalsePositiveRate: 0.08834758057680145
Kappa statistic: 0.7835247387963042
Training time: 1.0
		
Time end:Wed Nov 01 14.47.20 EET 2017