Wed Nov 01 14.45.54 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: KullbackLeiblerDivergenceQueryStrategy-1cores
Time begining: Wed Nov 01 14.45.54 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 53.49384856043631
Incorrectly Classified Instances: 10.113304866190337
Correctly Classified Instances: 89.88669513380965
Weighted Precision: 0.9049896378415149
Weighted AreaUnderROC: 0.9709951183929976
Root mean squared error: 0.30107513276003267
Relative absolute error: 20.645696122583878
Root relative squared error: 60.21502655200654
Weighted TruePositiveRate: 0.8988669513380968
Weighted MatthewsCorrelation: 0.7954381332088503
Weighted FMeasure: 0.8997117891633766
Iteration time: 11.5
Weighted AreaUnderPRC: 0.9723514044523126
Mean absolute error: 0.10322848061291938
Coverage of cases: 93.01209148945165
Instances selection time: 7.8
Test time: 3.9
Accumulative iteration time: 11.5
Weighted Recall: 0.8988669513380968
Weighted FalsePositiveRate: 0.09013055811623602
Kappa statistic: 0.7913211962811035
Training time: 3.7
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 53.12539635564199
Incorrectly Classified Instances: 10.205470764807846
Correctly Classified Instances: 89.79452923519216
Weighted Precision: 0.9027632359839883
Weighted AreaUnderROC: 0.9719826618785771
Root mean squared error: 0.30056685610185574
Relative absolute error: 20.383754331166223
Root relative squared error: 60.11337122037114
Weighted TruePositiveRate: 0.8979452923519217
Weighted MatthewsCorrelation: 0.7917865544363767
Weighted FMeasure: 0.8986985782063659
Iteration time: 6.4
Weighted AreaUnderPRC: 0.9733190290018436
Mean absolute error: 0.10191877165583114
Coverage of cases: 92.96558576079143
Instances selection time: 4.3
Test time: 4.3
Accumulative iteration time: 17.9
Weighted Recall: 0.8979452923519217
Weighted FalsePositiveRate: 0.09424387462850851
Kappa statistic: 0.7886999803964968
Training time: 2.1
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 53.355176933158575
Incorrectly Classified Instances: 9.975478797615525
Correctly Classified Instances: 90.02452120238448
Weighted Precision: 0.9043536899743131
Weighted AreaUnderROC: 0.9729020712762717
Root mean squared error: 0.29876322285731083
Relative absolute error: 20.20083702016637
Root relative squared error: 59.752644571462156
Weighted TruePositiveRate: 0.9002452120238449
Weighted MatthewsCorrelation: 0.7955798555942681
Weighted FMeasure: 0.9008774160346558
Iteration time: 3.6
Weighted AreaUnderPRC: 0.9742283326500673
Mean absolute error: 0.10100418510083187
Coverage of cases: 93.19494355895657
Instances selection time: 2.7
Test time: 3.8
Accumulative iteration time: 21.5
Weighted Recall: 0.9002452120238449
Weighted FalsePositiveRate: 0.09456122593923025
Kappa statistic: 0.7929308934659521
Training time: 0.9
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 53.124867881452666
Incorrectly Classified Instances: 9.791569779731956
Correctly Classified Instances: 90.20843022026806
Weighted Precision: 0.9058679638576482
Weighted AreaUnderROC: 0.973114556113097
Root mean squared error: 0.2987601760732764
Relative absolute error: 20.196391966683052
Root relative squared error: 59.752035214655294
Weighted TruePositiveRate: 0.9020843022026804
Weighted MatthewsCorrelation: 0.798953534880846
Weighted FMeasure: 0.9026540486809665
Iteration time: 3.5
Weighted AreaUnderPRC: 0.9744965516841237
Mean absolute error: 0.10098195983341525
Coverage of cases: 93.24060372891387
Instances selection time: 1.7
Test time: 3.3
Accumulative iteration time: 25.0
Weighted Recall: 0.9020843022026804
Weighted FalsePositiveRate: 0.09384822693989658
Kappa statistic: 0.7964828886510179
Training time: 1.8
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 53.240286644400285
Incorrectly Classified Instances: 10.251553714116602
Correctly Classified Instances: 89.74844628588342
Weighted Precision: 0.900782288770643
Weighted AreaUnderROC: 0.9728827666180375
Root mean squared error: 0.2999010386511817
Relative absolute error: 20.284856938848726
Root relative squared error: 59.98020773023633
Weighted TruePositiveRate: 0.897484462858834
Weighted MatthewsCorrelation: 0.7887037873621143
Weighted FMeasure: 0.8980582296518976
Iteration time: 2.4
Weighted AreaUnderPRC: 0.9742445227947323
Mean absolute error: 0.10142428469424362
Coverage of cases: 93.24081511858961
Instances selection time: 1.0
Test time: 2.6
Accumulative iteration time: 27.4
Weighted Recall: 0.897484462858834
Weighted FalsePositiveRate: 0.09983174898459615
Kappa statistic: 0.7867029299635322
Training time: 1.4
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 53.17126791527502
Incorrectly Classified Instances: 10.52741724094195
Correctly Classified Instances: 89.47258275905806
Weighted Precision: 0.8976107352520695
Weighted AreaUnderROC: 0.9728830338105393
Root mean squared error: 0.3002960772817251
Relative absolute error: 20.317171718971977
Root relative squared error: 60.05921545634502
Weighted TruePositiveRate: 0.8947258275905803
Weighted MatthewsCorrelation: 0.78235027955135
Weighted FMeasure: 0.8952687455977559
Iteration time: 2.1
Weighted AreaUnderPRC: 0.9742411933214571
Mean absolute error: 0.10158585859485988
Coverage of cases: 93.28668667822264
Instances selection time: 1.0
Test time: 2.4
Accumulative iteration time: 29.5
Weighted Recall: 0.8947258275905803
Weighted FalsePositiveRate: 0.10421035981666808
Kappa statistic: 0.7806830231887935
Training time: 1.1
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 53.217033780070174
Incorrectly Classified Instances: 10.297636663425358
Correctly Classified Instances: 89.70236333657463
Weighted Precision: 0.9003467222586858
Weighted AreaUnderROC: 0.9729067471450483
Root mean squared error: 0.29904581111136913
Relative absolute error: 20.186539529186938
Root relative squared error: 59.80916222227383
Weighted TruePositiveRate: 0.8970236333657466
Weighted MatthewsCorrelation: 0.7877904620567386
Weighted FMeasure: 0.897610298000207
Iteration time: 3.1
Weighted AreaUnderPRC: 0.9741848008887158
Mean absolute error: 0.10093269764593471
Coverage of cases: 93.3321354585042
Instances selection time: 1.1
Test time: 3.0
Accumulative iteration time: 32.6
Weighted Recall: 0.8970236333657466
Weighted FalsePositiveRate: 0.10011718814822498
Kappa statistic: 0.7857934853947863
Training time: 2.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 53.33234684817993
Incorrectly Classified Instances: 9.838075508392171
Correctly Classified Instances: 90.16192449160783
Weighted Precision: 0.90577207744001
Weighted AreaUnderROC: 0.973228513714991
Root mean squared error: 0.29827115100523793
Relative absolute error: 20.000383429434372
Root relative squared error: 59.65423020104758
Weighted TruePositiveRate: 0.9016192449160784
Weighted MatthewsCorrelation: 0.7985293309515805
Weighted FMeasure: 0.9022567057505215
Iteration time: 3.6
Weighted AreaUnderPRC: 0.9745140481430046
Mean absolute error: 0.10000191714717185
Coverage of cases: 93.0566947110303
Instances selection time: 2.2
Test time: 3.2
Accumulative iteration time: 36.2
Weighted Recall: 0.9016192449160784
Weighted FalsePositiveRate: 0.0923582192175035
Kappa statistic: 0.7958310134978512
Training time: 1.4
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 53.263328119054655
Incorrectly Classified Instances: 10.021984526275737
Correctly Classified Instances: 89.97801547372428
Weighted Precision: 0.9039575251157898
Weighted AreaUnderROC: 0.9730587796784071
Root mean squared error: 0.299170449877327
Relative absolute error: 20.075192460728886
Root relative squared error: 59.834089975465396
Weighted TruePositiveRate: 0.8997801547372427
Weighted MatthewsCorrelation: 0.7947171166632302
Weighted FMeasure: 0.9004337325292104
Iteration time: 2.4
Weighted AreaUnderPRC: 0.9742449711912464
Mean absolute error: 0.10037596230364443
Coverage of cases: 93.05690610070603
Instances selection time: 1.2
Test time: 3.3
Accumulative iteration time: 38.6
Weighted Recall: 0.8997801547372427
Weighted FalsePositiveRate: 0.09440384081811645
Kappa statistic: 0.7920348585615737
Training time: 1.2
		
Time end:Wed Nov 01 14.45.57 EET 2017