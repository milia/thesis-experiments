Wed Nov 01 14.46.22 EET 2017
Dataset: vote
Test set size: 217
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.22 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.299412336701474
Correctly Classified Instances: 93.70058766329853
Weighted Precision: 0.938482517702601
Weighted AreaUnderROC: 0.9360972393670745
Root mean squared error: 0.2491703468017013
Relative absolute error: 12.59882467340295
Root relative squared error: 49.834069360340266
Weighted TruePositiveRate: 0.9370058766329852
Weighted MatthewsCorrelation: 0.8689453426058067
Weighted FMeasure: 0.9370861267021551
Iteration time: 11.1
Weighted AreaUnderPRC: 0.911904233664897
Mean absolute error: 0.06299412336701475
Coverage of cases: 93.70058766329853
Instances selection time: 2.4
Test time: 1.8
Accumulative iteration time: 11.1
Weighted Recall: 0.9370058766329852
Weighted FalsePositiveRate: 0.06481139789883643
Kappa statistic: 0.8676460483727638
Training time: 8.7
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.023971589227582
Correctly Classified Instances: 93.97602841077244
Weighted Precision: 0.941550263302809
Weighted AreaUnderROC: 0.9376753450791157
Root mean squared error: 0.24386396266322946
Relative absolute error: 12.047943178455165
Root relative squared error: 48.7727925326459
Weighted TruePositiveRate: 0.9397602841077243
Weighted MatthewsCorrelation: 0.8748932801967179
Weighted FMeasure: 0.9397222287653259
Iteration time: 9.7
Weighted AreaUnderPRC: 0.9150557783866091
Mean absolute error: 0.060239715892275815
Coverage of cases: 93.97602841077244
Instances selection time: 2.4
Test time: 1.2
Accumulative iteration time: 20.8
Weighted Recall: 0.9397602841077243
Weighted FalsePositiveRate: 0.06440959394949278
Kappa statistic: 0.8730783853466766
Training time: 7.3
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.8863569103285
Correctly Classified Instances: 94.11364308967151
Weighted Precision: 0.9436941609944018
Weighted AreaUnderROC: 0.938139658848614
Root mean squared error: 0.23954859033948836
Relative absolute error: 11.772713820657001
Root relative squared error: 47.90971806789768
Weighted TruePositiveRate: 0.941136430896715
Weighted MatthewsCorrelation: 0.8783649955263343
Weighted FMeasure: 0.9409331303943154
Iteration time: 9.1
Weighted AreaUnderPRC: 0.916855865701786
Mean absolute error: 0.05886356910328501
Coverage of cases: 94.11364308967151
Instances selection time: 1.6
Test time: 1.4
Accumulative iteration time: 29.9
Weighted Recall: 0.941136430896715
Weighted FalsePositiveRate: 0.06485711319948687
Kappa statistic: 0.8755092106471425
Training time: 7.5
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.300680674755844
Correctly Classified Instances: 93.69931932524415
Weighted Precision: 0.9401181804901837
Weighted AreaUnderROC: 0.9332239180039654
Root mean squared error: 0.24798915936039045
Relative absolute error: 12.601361349511691
Root relative squared error: 49.59783187207809
Weighted TruePositiveRate: 0.9369931932524416
Weighted MatthewsCorrelation: 0.8701505277455409
Weighted FMeasure: 0.9367288224761292
Iteration time: 12.1
Weighted AreaUnderPRC: 0.9111534032452437
Mean absolute error: 0.06300680674755846
Coverage of cases: 93.69931932524415
Instances selection time: 2.7
Test time: 1.6
Accumulative iteration time: 42.0
Weighted Recall: 0.9369931932524416
Weighted FalsePositiveRate: 0.07054535724451129
Kappa statistic: 0.8666250482190309
Training time: 9.4
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.896376780958017
Correctly Classified Instances: 93.10362321904198
Weighted Precision: 0.9348703433681737
Weighted AreaUnderROC: 0.9235093330340776
Root mean squared error: 0.26114762928789464
Relative absolute error: 13.792753561916035
Root relative squared error: 52.229525857578935
Weighted TruePositiveRate: 0.9310362321904199
Weighted MatthewsCorrelation: 0.857880595447962
Weighted FMeasure: 0.93051165635421
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9013576201719067
Mean absolute error: 0.06896376780958018
Coverage of cases: 93.10362321904198
Instances selection time: 2.7
Test time: 2.0
Accumulative iteration time: 54.0
Weighted Recall: 0.9310362321904199
Weighted FalsePositiveRate: 0.08401756612226437
Kappa statistic: 0.8531791711996742
Training time: 9.3
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.62157020251131
Correctly Classified Instances: 93.37842979748868
Weighted Precision: 0.9371034874519939
Weighted AreaUnderROC: 0.9231193655781244
Root mean squared error: 0.2535007271157518
Relative absolute error: 13.24314040502262
Root relative squared error: 50.70014542315037
Weighted TruePositiveRate: 0.933784297974887
Weighted MatthewsCorrelation: 0.862430146619886
Weighted FMeasure: 0.9328764910819901
Iteration time: 12.3
Weighted AreaUnderPRC: 0.9033613300739944
Mean absolute error: 0.0662157020251131
Coverage of cases: 93.37842979748868
Instances selection time: 2.6
Test time: 1.1
Accumulative iteration time: 66.3
Weighted Recall: 0.933784297974887
Weighted FalsePositiveRate: 0.08754556681863808
Kappa statistic: 0.8575929316004842
Training time: 9.7
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.701179554390564
Correctly Classified Instances: 94.29882044560944
Weighted Precision: 0.9446303746338465
Weighted AreaUnderROC: 0.9356764111771968
Root mean squared error: 0.23589016119605635
Relative absolute error: 11.402359108781129
Root relative squared error: 47.17803223921127
Weighted TruePositiveRate: 0.9429882044560942
Weighted MatthewsCorrelation: 0.8808564406913153
Weighted FMeasure: 0.9426075591429551
Iteration time: 10.1
Weighted AreaUnderPRC: 0.9167235598979337
Mean absolute error: 0.05701179554390564
Coverage of cases: 94.29882044560944
Instances selection time: 1.6
Test time: 1.6
Accumulative iteration time: 76.4
Weighted Recall: 0.9429882044560942
Weighted FalsePositiveRate: 0.07163538210170092
Kappa statistic: 0.8785165942342591
Training time: 8.5
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.1025239927281945
Correctly Classified Instances: 94.89747600727182
Weighted Precision: 0.9498573166413525
Weighted AreaUnderROC: 0.9443057270040771
Root mean squared error: 0.22420524632804936
Relative absolute error: 10.205047985456392
Root relative squared error: 44.841049265609875
Weighted TruePositiveRate: 0.948974760072718
Weighted MatthewsCorrelation: 0.8930169477878549
Weighted FMeasure: 0.9488035457442127
Iteration time: 12.8
Weighted AreaUnderPRC: 0.9257531744779517
Mean absolute error: 0.05102523992728195
Coverage of cases: 94.89747600727182
Instances selection time: 1.6
Test time: 1.0
Accumulative iteration time: 89.2
Weighted Recall: 0.948974760072718
Weighted FalsePositiveRate: 0.06036330606456334
Kappa statistic: 0.8918084642368014
Training time: 11.2
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.597302667737709
Correctly Classified Instances: 95.40269733226229
Weighted Precision: 0.955010173380073
Weighted AreaUnderROC: 0.9532768488385142
Root mean squared error: 0.21371293230931382
Relative absolute error: 9.194605335475417
Root relative squared error: 42.74258646186276
Weighted TruePositiveRate: 0.9540269733226229
Weighted MatthewsCorrelation: 0.9042043787242229
Weighted FMeasure: 0.9540696120262346
Iteration time: 15.0
Weighted AreaUnderPRC: 0.9346661547598109
Mean absolute error: 0.04597302667737707
Coverage of cases: 95.40269733226229
Instances selection time: 2.2
Test time: 1.5
Accumulative iteration time: 104.2
Weighted Recall: 0.9540269733226229
Weighted FalsePositiveRate: 0.047473275645594525
Kappa statistic: 0.9033081674106433
Training time: 12.8
		
Time end:Wed Nov 01 14.46.27 EET 2017