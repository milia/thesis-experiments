Wed Nov 01 14.46.42 EET 2017
Dataset: vote
Test set size: 218
Initial Labelled set size: 43
Initial Unlabelled set size: 174
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: MarginSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.46.42 EET 2017
		
Iteration: 1
Labeled set size: 43
Unlabelled set size: 174
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.8807339449541285
Correctly Classified Instances: 93.11926605504587
Weighted Precision: 0.9320296464547879
Weighted AreaUnderROC: 0.919598436389481
Root mean squared error: 0.2623115312935009
Relative absolute error: 13.761467889908257
Root relative squared error: 52.462306258700174
Weighted TruePositiveRate: 0.9311926605504587
Weighted MatthewsCorrelation: 0.854478186063692
Weighted FMeasure: 0.930572578598107
Iteration time: 11.0
Weighted AreaUnderPRC: 0.8979963116632249
Mean absolute error: 0.06880733944954129
Coverage of cases: 93.11926605504587
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.9311926605504587
Weighted FalsePositiveRate: 0.09199578777149638
Kappa statistic: 0.8524501398790723
Training time: 10.0
		
Iteration: 2
Labeled set size: 63
Unlabelled set size: 154
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.045871559633028
Correctly Classified Instances: 94.95412844036697
Weighted Precision: 0.9502283314537143
Weighted AreaUnderROC: 0.9500710732054015
Root mean squared error: 0.224630175168721
Relative absolute error: 10.091743119266056
Root relative squared error: 44.9260350337442
Weighted TruePositiveRate: 0.9495412844036697
Weighted MatthewsCorrelation: 0.8945566834252947
Weighted FMeasure: 0.9496982893446182
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9290339105166626
Mean absolute error: 0.05045871559633028
Coverage of cases: 94.95412844036697
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 19.0
Weighted Recall: 0.9495412844036697
Weighted FalsePositiveRate: 0.04939913799286659
Kappa statistic: 0.8941840967257965
Training time: 7.0
		
Iteration: 3
Labeled set size: 83
Unlabelled set size: 134
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.504587155963303
Correctly Classified Instances: 94.4954128440367
Weighted Precision: 0.9483604637174641
Weighted AreaUnderROC: 0.9507818052594172
Root mean squared error: 0.2346185661017325
Relative absolute error: 11.009174311926607
Root relative squared error: 46.9237132203465
Weighted TruePositiveRate: 0.944954128440367
Weighted MatthewsCorrelation: 0.8884013914874982
Weighted FMeasure: 0.9453583114133572
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9267712557162647
Mean absolute error: 0.05504587155963303
Coverage of cases: 94.4954128440367
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 28.0
Weighted Recall: 0.944954128440367
Weighted FalsePositiveRate: 0.04339051792153257
Kappa statistic: 0.8858240223463687
Training time: 8.0
		
Iteration: 4
Labeled set size: 103
Unlabelled set size: 114
	
Mean region size: 50.0
Incorrectly Classified Instances: 5.963302752293578
Correctly Classified Instances: 94.03669724770643
Weighted Precision: 0.94633047151331
Weighted AreaUnderROC: 0.9492714996446341
Root mean squared error: 0.24419874594873697
Relative absolute error: 11.926605504587156
Root relative squared error: 48.839749189747394
Weighted TruePositiveRate: 0.9403669724770642
Weighted MatthewsCorrelation: 0.8818943266182712
Weighted FMeasure: 0.9409230790094063
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9233274653435471
Mean absolute error: 0.05963302752293578
Coverage of cases: 94.03669724770643
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 36.0
Weighted Recall: 0.9403669724770642
Weighted FalsePositiveRate: 0.04182397318779627
Kappa statistic: 0.8771138669673055
Training time: 7.0
		
Iteration: 5
Labeled set size: 123
Unlabelled set size: 94
	
Mean region size: 50.0
Incorrectly Classified Instances: 6.422018348623853
Correctly Classified Instances: 93.57798165137615
Weighted Precision: 0.9408958964316524
Weighted AreaUnderROC: 0.9433191186922532
Root mean squared error: 0.2534170149895988
Relative absolute error: 12.844036697247708
Root relative squared error: 50.68340299791976
Weighted TruePositiveRate: 0.9357798165137615
Weighted MatthewsCorrelation: 0.8712920064828202
Weighted FMeasure: 0.9363391770527931
Iteration time: 8.0
Weighted AreaUnderPRC: 0.9161053300410956
Mean absolute error: 0.06422018348623854
Coverage of cases: 93.57798165137615
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 44.0
Weighted Recall: 0.9357798165137615
Weighted FalsePositiveRate: 0.049141579129255424
Kappa statistic: 0.8673735442377891
Training time: 7.0
		
Iteration: 6
Labeled set size: 143
Unlabelled set size: 74
	
Mean region size: 50.0
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.9642715083082056
Weighted AreaUnderROC: 0.9657071783937455
Root mean squared error: 0.19156525704423027
Relative absolute error: 7.339449541284404
Root relative squared error: 38.31305140884606
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9238935982125793
Weighted FMeasure: 0.9634514550238894
Iteration time: 9.0
Weighted AreaUnderPRC: 0.9490760354871121
Mean absolute error: 0.03669724770642202
Coverage of cases: 96.3302752293578
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 53.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.03188839550608687
Kappa statistic: 0.9232123987319478
Training time: 8.0
		
Iteration: 7
Labeled set size: 163
Unlabelled set size: 54
	
Mean region size: 50.0
Incorrectly Classified Instances: 3.669724770642202
Correctly Classified Instances: 96.3302752293578
Weighted Precision: 0.963302752293578
Weighted AreaUnderROC: 0.9612651030561479
Root mean squared error: 0.19156525704423027
Relative absolute error: 7.339449541284404
Root relative squared error: 38.31305140884606
Weighted TruePositiveRate: 0.963302752293578
Weighted MatthewsCorrelation: 0.9225302061122957
Weighted FMeasure: 0.963302752293578
Iteration time: 10.0
Weighted AreaUnderPRC: 0.9463755925483982
Mean absolute error: 0.03669724770642202
Coverage of cases: 96.3302752293578
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 63.0
Weighted Recall: 0.963302752293578
Weighted FalsePositiveRate: 0.04077254618128232
Kappa statistic: 0.9225302061122956
Training time: 9.0
		
Iteration: 8
Labeled set size: 183
Unlabelled set size: 34
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9560234541577826
Root mean squared error: 0.2141764684390597
Relative absolute error: 9.174311926605505
Root relative squared error: 42.83529368781194
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 14.0
Weighted AreaUnderPRC: 0.9361686447127487
Mean absolute error: 0.045871559633027525
Coverage of cases: 95.41284403669725
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 77.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 13.0
		
Iteration: 9
Labeled set size: 203
Unlabelled set size: 14
	
Mean region size: 50.0
Incorrectly Classified Instances: 4.587155963302752
Correctly Classified Instances: 95.41284403669725
Weighted Precision: 0.9551645602104318
Weighted AreaUnderROC: 0.9560234541577826
Root mean squared error: 0.2141764684390597
Relative absolute error: 9.174311926605505
Root relative squared error: 42.83529368781194
Weighted TruePositiveRate: 0.9541284403669725
Weighted MatthewsCorrelation: 0.9046825333126992
Weighted FMeasure: 0.9543143187798617
Iteration time: 12.0
Weighted AreaUnderPRC: 0.9361686447127487
Mean absolute error: 0.045871559633027525
Coverage of cases: 95.41284403669725
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 89.0
Weighted Recall: 0.9541284403669725
Weighted FalsePositiveRate: 0.04208153205140744
Kappa statistic: 0.904015498414935
Training time: 11.0
		
Time end:Wed Nov 01 14.46.42 EET 2017