Wed Nov 01 14.41.51 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: LeastConfidentSamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.41.51 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 36.347517730496456
Incorrectly Classified Instances: 41.13475177304964
Correctly Classified Instances: 58.86524822695036
Weighted Precision: 0.591539705519762
Weighted AreaUnderROC: 0.7013982975684749
Root mean squared error: 0.4457625287142173
Relative absolute error: 58.856904463913196
Root relative squared error: 102.94444637912066
Weighted TruePositiveRate: 0.5886524822695035
Weighted MatthewsCorrelation: 0.452788227734768
Weighted FMeasure: 0.583549756409754
Iteration time: 5.0
Weighted AreaUnderPRC: 0.4515670569172998
Mean absolute error: 0.2207133917396745
Coverage of cases: 61.22931442080378
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 5.0
Weighted Recall: 0.5886524822695035
Weighted FalsePositiveRate: 0.13617029398367334
Kappa statistic: 0.45216226274655746
Training time: 5.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 37.05673758865248
Incorrectly Classified Instances: 43.73522458628842
Correctly Classified Instances: 56.26477541371158
Weighted Precision: 0.5711475008920375
Weighted AreaUnderROC: 0.7091344873275821
Root mean squared error: 0.4508340497868131
Relative absolute error: 60.07946440987754
Root relative squared error: 104.11566400170628
Weighted TruePositiveRate: 0.5626477541371159
Weighted MatthewsCorrelation: 0.4184565594214368
Weighted FMeasure: 0.542018337175298
Iteration time: 6.0
Weighted AreaUnderPRC: 0.45324706381309204
Mean absolute error: 0.22529799153704078
Coverage of cases: 63.593380614657214
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 11.0
Weighted Recall: 0.5626477541371159
Weighted FalsePositiveRate: 0.14472606073447133
Kappa statistic: 0.41772387365601404
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 34.9290780141844
Incorrectly Classified Instances: 42.5531914893617
Correctly Classified Instances: 57.4468085106383
Weighted Precision: 0.5632251878282022
Weighted AreaUnderROC: 0.7407699929443161
Root mean squared error: 0.4437332392433744
Relative absolute error: 57.49210554778924
Root relative squared error: 102.47580205021873
Weighted TruePositiveRate: 0.574468085106383
Weighted MatthewsCorrelation: 0.42662826176596425
Weighted FMeasure: 0.5497533727856835
Iteration time: 7.0
Weighted AreaUnderPRC: 0.4801295617996679
Mean absolute error: 0.21559539580420964
Coverage of cases: 68.32151300236407
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 18.0
Weighted Recall: 0.574468085106383
Weighted FalsePositiveRate: 0.14086355605064024
Kappa statistic: 0.433452635181892
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 34.33806146572104
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6516681304585477
Weighted AreaUnderROC: 0.7725794956636161
Root mean squared error: 0.4059415110462047
Relative absolute error: 50.494818201674015
Root relative squared error: 93.74817627110788
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5369106584385508
Weighted FMeasure: 0.6370233593982816
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5289426168345968
Mean absolute error: 0.18935556825627756
Coverage of cases: 70.44917257683215
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 25.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11448524926588878
Kappa statistic: 0.5405457683609094
Training time: 7.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 33.51063829787234
Incorrectly Classified Instances: 35.22458628841608
Correctly Classified Instances: 64.77541371158392
Weighted Precision: 0.6857311058281923
Weighted AreaUnderROC: 0.816885079502043
Root mean squared error: 0.36918416309544205
Relative absolute error: 48.150399639761275
Root relative squared error: 85.25943037748007
Weighted TruePositiveRate: 0.6477541371158393
Weighted MatthewsCorrelation: 0.5237623312686867
Weighted FMeasure: 0.5798770365452312
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5782623818683693
Mean absolute error: 0.18056399864910477
Coverage of cases: 81.08747044917257
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 32.0
Weighted Recall: 0.6477541371158393
Weighted FalsePositiveRate: 0.11652826051369893
Kappa statistic: 0.5312653389061595
Training time: 6.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 31.087470449172578
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6590064355094402
Weighted AreaUnderROC: 0.7932220648286026
Root mean squared error: 0.3870122887228695
Relative absolute error: 46.92082033759127
Root relative squared error: 89.37665962953676
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5476788568284903
Weighted FMeasure: 0.6279311120939494
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5630081766820874
Mean absolute error: 0.1759530762659673
Coverage of cases: 74.94089834515367
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 40.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11045465571111515
Kappa statistic: 0.5562672975627175
Training time: 8.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 33.451536643026
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.6457343994759837
Weighted AreaUnderROC: 0.8120132401094339
Root mean squared error: 0.36809501033136066
Relative absolute error: 48.33859188469119
Root relative squared error: 85.00790132086766
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.5253799564928002
Weighted FMeasure: 0.5917884092063702
Iteration time: 9.0
Weighted AreaUnderPRC: 0.568734495255509
Mean absolute error: 0.18126971956759197
Coverage of cases: 81.56028368794327
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 49.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11426699210953116
Kappa statistic: 0.5406619561175158
Training time: 8.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 30.61465721040189
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.6759236782956779
Weighted AreaUnderROC: 0.8040326141670944
Root mean squared error: 0.38006466067489597
Relative absolute error: 44.34394604607368
Root relative squared error: 87.772173660046
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.5801948299679452
Weighted FMeasure: 0.6741980469105727
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5777967872818158
Mean absolute error: 0.1662897976727763
Coverage of cases: 76.35933806146572
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 63.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10285001195639423
Kappa statistic: 0.587492183545246
Training time: 14.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 33.156028368794324
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6651730133264027
Weighted AreaUnderROC: 0.7994002467366431
Root mean squared error: 0.38130459011351664
Relative absolute error: 45.834914456681986
Root relative squared error: 88.0585230981115
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5696317006560653
Weighted FMeasure: 0.6652613258140282
Iteration time: 15.0
Weighted AreaUnderPRC: 0.5725664073494385
Mean absolute error: 0.17188092921255743
Coverage of cases: 76.59574468085107
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 78.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10484953060788552
Kappa statistic: 0.5780956917855122
Training time: 15.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 43.321513002364064
Incorrectly Classified Instances: 32.860520094562645
Correctly Classified Instances: 67.13947990543736
Weighted Precision: 0.6599980149172657
Weighted AreaUnderROC: 0.8128685777026594
Root mean squared error: 0.37255115453855686
Relative absolute error: 45.91586085646648
Root relative squared error: 86.03700374389668
Weighted TruePositiveRate: 0.6713947990543735
Weighted MatthewsCorrelation: 0.5566728015044969
Weighted FMeasure: 0.6581486670871296
Iteration time: 16.0
Weighted AreaUnderPRC: 0.6181924463776616
Mean absolute error: 0.1721844782117493
Coverage of cases: 81.56028368794327
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 94.0
Weighted Recall: 0.6713947990543735
Weighted FalsePositiveRate: 0.1088029844462933
Kappa statistic: 0.5625906666369094
Training time: 15.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 39.125295508274235
Incorrectly Classified Instances: 34.2789598108747
Correctly Classified Instances: 65.72104018912529
Weighted Precision: 0.6425071624876576
Weighted AreaUnderROC: 0.8050044786595206
Root mean squared error: 0.3826010385631773
Relative absolute error: 46.28123753389991
Root relative squared error: 88.35792504267232
Weighted TruePositiveRate: 0.6572104018912529
Weighted MatthewsCorrelation: 0.5359253196002289
Weighted FMeasure: 0.6461334550675082
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6013293632781184
Mean absolute error: 0.17355464075212468
Coverage of cases: 80.1418439716312
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 112.0
Weighted Recall: 0.6572104018912529
Weighted FalsePositiveRate: 0.1141269496388146
Kappa statistic: 0.5433529884749396
Training time: 17.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 33.86524822695036
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6589251665346233
Weighted AreaUnderROC: 0.7876404856481092
Root mean squared error: 0.39443905777200033
Relative absolute error: 46.74180619019081
Root relative squared error: 91.0917984734267
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.549560519659052
Weighted FMeasure: 0.6612238227525806
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5796211124783684
Mean absolute error: 0.17528177321321553
Coverage of cases: 74.70449172576832
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 132.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11227251436407631
Kappa statistic: 0.5523042178777195
Training time: 19.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 32.505910165484636
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6523420753895456
Weighted AreaUnderROC: 0.804810414127427
Root mean squared error: 0.3901826778413802
Relative absolute error: 46.24058247639122
Root relative squared error: 90.10882963393998
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5420523515922958
Weighted FMeasure: 0.6509806786033635
Iteration time: 20.0
Weighted AreaUnderPRC: 0.5966110347836143
Mean absolute error: 0.1734021842864671
Coverage of cases: 77.30496453900709
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 152.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11350609413662603
Kappa statistic: 0.5465326633165828
Training time: 20.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 32.97872340425532
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6669755869005148
Weighted AreaUnderROC: 0.7997154206807334
Root mean squared error: 0.38900688475871464
Relative absolute error: 46.3291930904223
Root relative squared error: 89.83729185282465
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5539020156038589
Weighted FMeasure: 0.6615515454463072
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6072508369485103
Mean absolute error: 0.17373447408908363
Coverage of cases: 75.41371158392435
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 173.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11074787013425182
Kappa statistic: 0.5530171676055394
Training time: 20.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.97872340425532
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6730992171447935
Weighted AreaUnderROC: 0.8172756879923161
Root mean squared error: 0.38146573000308526
Relative absolute error: 43.29830683880706
Root relative squared error: 88.09573676155937
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.564002259925927
Weighted FMeasure: 0.6694813831643568
Iteration time: 23.0
Weighted AreaUnderPRC: 0.6054922259461256
Mean absolute error: 0.16236865064552647
Coverage of cases: 78.25059101654847
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 196.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10934079625312973
Kappa statistic: 0.5649413079932923
Training time: 23.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 33.51063829787234
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6922862980918069
Weighted AreaUnderROC: 0.8244878402886197
Root mean squared error: 0.37099311156296927
Relative absolute error: 41.097494394752005
Root relative squared error: 85.67718913135087
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.595123058962229
Weighted FMeasure: 0.688759052856798
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6284204318879298
Mean absolute error: 0.15411560398032
Coverage of cases: 80.37825059101655
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 220.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10047654597633685
Kappa statistic: 0.599929996499825
Training time: 23.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 34.515366430260045
Incorrectly Classified Instances: 30.73286052009456
Correctly Classified Instances: 69.26713947990544
Weighted Precision: 0.6856188978314365
Weighted AreaUnderROC: 0.8177755766002468
Root mean squared error: 0.3725047210080035
Relative absolute error: 42.61002291548517
Root relative squared error: 86.02628037935091
Weighted TruePositiveRate: 0.6926713947990544
Weighted MatthewsCorrelation: 0.5858895549127036
Weighted FMeasure: 0.6833958060720151
Iteration time: 24.0
Weighted AreaUnderPRC: 0.6367210626870601
Mean absolute error: 0.15978758593306938
Coverage of cases: 79.90543735224587
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 244.0
Weighted Recall: 0.6926713947990544
Weighted FalsePositiveRate: 0.1029081569185861
Kappa statistic: 0.5904917227050334
Training time: 23.0
		
Time end:Wed Nov 01 14.41.52 EET 2017