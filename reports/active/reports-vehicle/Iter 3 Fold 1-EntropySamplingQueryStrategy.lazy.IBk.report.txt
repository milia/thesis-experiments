Wed Nov 01 14.39.26 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: IBk-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.39.26 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.952718676122934
Correctly Classified Instances: 60.047281323877066
Weighted Precision: 0.5985178358753507
Weighted AreaUnderROC: 0.7331458813797094
Root mean squared error: 0.43711649104025646
Relative absolute error: 55.39436922415646
Root relative squared error: 100.94772950772669
Weighted TruePositiveRate: 0.6004728132387707
Weighted MatthewsCorrelation: 0.4658992517567654
Weighted FMeasure: 0.5938932237427128
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4866487574978023
Mean absolute error: 0.20772888459058672
Coverage of cases: 60.047281323877066
Instances selection time: 13.0
Test time: 13.0
Accumulative iteration time: 14.0
Weighted Recall: 0.6004728132387707
Weighted FalsePositiveRate: 0.13418105047935214
Kappa statistic: 0.4667338032896945
Training time: 1.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.6024806612553707
Weighted AreaUnderROC: 0.739508573444391
Root mean squared error: 0.4336693752695354
Relative absolute error: 53.78688381052441
Root relative squared error: 100.15165222046525
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.47602462023007414
Weighted FMeasure: 0.6013796350898116
Iteration time: 12.0
Weighted AreaUnderPRC: 0.4922985821582472
Mean absolute error: 0.2017008142894665
Coverage of cases: 60.99290780141844
Instances selection time: 12.0
Test time: 15.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.13091193112540245
Kappa statistic: 0.47952632010678675
Training time: 0.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.00709219858156
Correctly Classified Instances: 60.99290780141844
Weighted Precision: 0.6004249721894623
Weighted AreaUnderROC: 0.7393100369616518
Root mean squared error: 0.43488355718011396
Relative absolute error: 53.50916075650119
Root relative squared error: 100.43205552163234
Weighted TruePositiveRate: 0.6099290780141844
Weighted MatthewsCorrelation: 0.47547387150961845
Weighted FMeasure: 0.5949956367565535
Iteration time: 13.0
Weighted AreaUnderPRC: 0.48929733830335076
Mean absolute error: 0.20065935283687944
Coverage of cases: 60.99290780141844
Instances selection time: 13.0
Test time: 18.0
Accumulative iteration time: 39.0
Weighted Recall: 0.6099290780141844
Weighted FalsePositiveRate: 0.13130900409088078
Kappa statistic: 0.4792000895422154
Training time: 0.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 25.0
Incorrectly Classified Instances: 39.716312056737586
Correctly Classified Instances: 60.283687943262414
Weighted Precision: 0.5984269181470924
Weighted AreaUnderROC: 0.7343294709947593
Root mean squared error: 0.4397174688051872
Relative absolute error: 54.22656699252457
Root relative squared error: 101.54839959282229
Weighted TruePositiveRate: 0.6028368794326241
Weighted MatthewsCorrelation: 0.46855510280710394
Weighted FMeasure: 0.5859565544776851
Iteration time: 14.0
Weighted AreaUnderPRC: 0.47889415525994317
Mean absolute error: 0.20334962622196714
Coverage of cases: 60.283687943262414
Instances selection time: 14.0
Test time: 20.0
Accumulative iteration time: 53.0
Weighted Recall: 0.6028368794326241
Weighted FalsePositiveRate: 0.13417793744310538
Kappa statistic: 0.46944991936928865
Training time: 0.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.42553191489362
Correctly Classified Instances: 59.57446808510638
Weighted Precision: 0.5936278190039327
Weighted AreaUnderROC: 0.7294042240940161
Root mean squared error: 0.4443212675174047
Relative absolute error: 54.998311381290115
Root relative squared error: 102.61160136313974
Weighted TruePositiveRate: 0.5957446808510638
Weighted MatthewsCorrelation: 0.4602675133225086
Weighted FMeasure: 0.5766452983315002
Iteration time: 20.0
Weighted AreaUnderPRC: 0.4738897297813927
Mean absolute error: 0.20624366767983793
Coverage of cases: 59.57446808510638
Instances selection time: 20.0
Test time: 22.0
Accumulative iteration time: 73.0
Weighted Recall: 0.5957446808510638
Weighted FalsePositiveRate: 0.13693623266303181
Kappa statistic: 0.45983062998476565
Training time: 0.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 25.0
Incorrectly Classified Instances: 40.66193853427896
Correctly Classified Instances: 59.33806146572104
Weighted Precision: 0.5932009358140707
Weighted AreaUnderROC: 0.7277917426838695
Root mean squared error: 0.44617130630024293
Relative absolute error: 55.19004744898823
Root relative squared error: 103.0388495188529
Weighted TruePositiveRate: 0.5933806146572104
Weighted MatthewsCorrelation: 0.4582041181411808
Weighted FMeasure: 0.5733018355134029
Iteration time: 14.0
Weighted AreaUnderPRC: 0.4701277472501091
Mean absolute error: 0.20696267793370587
Coverage of cases: 59.33806146572104
Instances selection time: 14.0
Test time: 24.0
Accumulative iteration time: 87.0
Weighted Recall: 0.5933806146572104
Weighted FalsePositiveRate: 0.13779712928947166
Kappa statistic: 0.45658246567975735
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 25.0
Incorrectly Classified Instances: 38.297872340425535
Correctly Classified Instances: 61.702127659574465
Weighted Precision: 0.6003845867067751
Weighted AreaUnderROC: 0.7439387551696761
Root mean squared error: 0.4334469006615037
Relative absolute error: 52.00490998363349
Root relative squared error: 100.10027391053126
Weighted TruePositiveRate: 0.6170212765957447
Weighted MatthewsCorrelation: 0.48222181851774926
Weighted FMeasure: 0.5921426789278212
Iteration time: 14.0
Weighted AreaUnderPRC: 0.48770496124394674
Mean absolute error: 0.19501841243862558
Coverage of cases: 61.702127659574465
Instances selection time: 14.0
Test time: 26.0
Accumulative iteration time: 101.0
Weighted Recall: 0.6170212765957447
Weighted FalsePositiveRate: 0.1291437662563925
Kappa statistic: 0.48877598979431974
Training time: 0.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 25.0
Incorrectly Classified Instances: 36.87943262411348
Correctly Classified Instances: 63.12056737588652
Weighted Precision: 0.6040078162331706
Weighted AreaUnderROC: 0.7538177086281527
Root mean squared error: 0.4256992309550486
Relative absolute error: 50.0642860105348
Root relative squared error: 98.31102623428559
Weighted TruePositiveRate: 0.6312056737588653
Weighted MatthewsCorrelation: 0.49606433523060056
Weighted FMeasure: 0.6026002878698171
Iteration time: 17.0
Weighted AreaUnderPRC: 0.5018553431781831
Mean absolute error: 0.1877410725395055
Coverage of cases: 63.12056737588652
Instances selection time: 17.0
Test time: 28.0
Accumulative iteration time: 118.0
Weighted Recall: 0.6312056737588653
Weighted FalsePositiveRate: 0.12357025650255986
Kappa statistic: 0.508194522079374
Training time: 0.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6115107898884882
Weighted AreaUnderROC: 0.7618087915177901
Root mean squared error: 0.41911448604017
Relative absolute error: 48.44174991738489
Root relative squared error: 96.79034453462552
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.5096783634485798
Weighted FMeasure: 0.6121584260057134
Iteration time: 14.0
Weighted AreaUnderPRC: 0.5156677056624509
Mean absolute error: 0.18165656219019333
Coverage of cases: 64.30260047281324
Instances selection time: 14.0
Test time: 30.0
Accumulative iteration time: 132.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.11940842169255214
Kappa statistic: 0.5241596638655462
Training time: 0.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 25.0
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6123624941448874
Weighted AreaUnderROC: 0.7635600836768125
Root mean squared error: 0.417971758080367
Relative absolute error: 48.06816978935114
Root relative squared error: 96.52644281654442
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5117163106270184
Weighted FMeasure: 0.6114278637240623
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5197175051663779
Mean absolute error: 0.18025563671006675
Coverage of cases: 64.53900709219859
Instances selection time: 12.0
Test time: 32.0
Accumulative iteration time: 144.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11826990356836094
Kappa statistic: 0.5275045238928564
Training time: 0.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6400923739032675
Weighted AreaUnderROC: 0.7757885039872466
Root mean squared error: 0.4068828293304032
Relative absolute error: 45.52688030820405
Root relative squared error: 93.96556441701794
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5408651939635296
Weighted FMeasure: 0.6431137123926468
Iteration time: 12.0
Weighted AreaUnderPRC: 0.5475196917414916
Mean absolute error: 0.1707258011557652
Coverage of cases: 66.43026004728132
Instances selection time: 12.0
Test time: 33.0
Accumulative iteration time: 156.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11272559249832009
Kappa statistic: 0.5523175648985249
Training time: 0.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.6434366301504055
Weighted AreaUnderROC: 0.7757900878934232
Root mean squared error: 0.40706322695601777
Relative absolute error: 45.47706037067733
Root relative squared error: 94.00722546410184
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5420540039633626
Weighted FMeasure: 0.6430073889937786
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5508825557014012
Mean absolute error: 0.17053897639003998
Coverage of cases: 66.43026004728132
Instances selection time: 10.0
Test time: 35.0
Accumulative iteration time: 166.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.11272242468596673
Kappa statistic: 0.55234759278581
Training time: 0.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6621629566347439
Weighted AreaUnderROC: 0.7837676850675853
Root mean squared error: 0.3999894489761134
Relative absolute error: 43.87649195640902
Root relative squared error: 92.37360641574766
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5607007329300658
Weighted FMeasure: 0.6568924238085306
Iteration time: 9.0
Weighted AreaUnderPRC: 0.5658160058790214
Mean absolute error: 0.16453684483653383
Coverage of cases: 67.61229314420804
Instances selection time: 9.0
Test time: 37.0
Accumulative iteration time: 175.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.1085875613069097
Kappa statistic: 0.5681775843697141
Training time: 0.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.38770685579196
Correctly Classified Instances: 67.61229314420804
Weighted Precision: 0.6654443378537951
Weighted AreaUnderROC: 0.783852856336626
Root mean squared error: 0.4001274095993593
Relative absolute error: 43.836671104951904
Root relative squared error: 92.4054670569351
Weighted TruePositiveRate: 0.6761229314420804
Weighted MatthewsCorrelation: 0.5613168515315426
Weighted FMeasure: 0.6491554204356126
Iteration time: 8.0
Weighted AreaUnderPRC: 0.5669481600361731
Mean absolute error: 0.16438751664356963
Coverage of cases: 67.61229314420804
Instances selection time: 8.0
Test time: 38.0
Accumulative iteration time: 183.0
Weighted Recall: 0.6761229314420804
Weighted FalsePositiveRate: 0.1084172187688282
Kappa statistic: 0.5682773109243697
Training time: 0.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6605581172798215
Weighted AreaUnderROC: 0.7820960520044
Root mean squared error: 0.401708439514281
Relative absolute error: 44.11296124987164
Root relative squared error: 92.77059027572585
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5582732008363197
Weighted FMeasure: 0.6609849260908188
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5642971635123205
Mean absolute error: 0.16542360468701867
Coverage of cases: 67.37588652482269
Instances selection time: 7.0
Test time: 40.0
Accumulative iteration time: 190.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10956676123942677
Kappa statistic: 0.5648959086470733
Training time: 0.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 25.0
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6458722111852432
Weighted AreaUnderROC: 0.774114526563211
Root mean squared error: 0.40903309855614367
Relative absolute error: 45.64110063123007
Root relative squared error: 94.46214782354251
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5412641869596113
Weighted FMeasure: 0.651184971331293
Iteration time: 5.0
Weighted AreaUnderPRC: 0.5569765088803585
Mean absolute error: 0.17115412736711275
Coverage of cases: 66.19385342789599
Instances selection time: 5.0
Test time: 42.0
Accumulative iteration time: 195.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.1137094811525378
Kappa statistic: 0.5490539593552907
Training time: 0.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 25.0
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6642044488832141
Weighted AreaUnderROC: 0.7820363475986168
Root mean squared error: 0.40191960395301846
Relative absolute error: 44.05275112409026
Root relative squared error: 92.8193566139452
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.560000216757295
Weighted FMeasure: 0.6682911431881825
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5726732667511921
Mean absolute error: 0.16519781671533845
Coverage of cases: 67.37588652482269
Instances selection time: 2.0
Test time: 44.0
Accumulative iteration time: 197.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10968617005099335
Kappa statistic: 0.5647661440042946
Training time: 0.0
		
Time end:Wed Nov 01 14.39.28 EET 2017