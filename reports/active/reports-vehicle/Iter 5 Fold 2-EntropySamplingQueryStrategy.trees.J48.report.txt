Wed Nov 01 14.39.44 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: J48-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.39.44 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 37.05673758865248
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.6450993857048332
Weighted AreaUnderROC: 0.7831729436448834
Root mean squared error: 0.395779256809689
Relative absolute error: 48.870501707381095
Root relative squared error: 91.4013041834976
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5390241022365714
Weighted FMeasure: 0.6494661652106593
Iteration time: 13.0
Weighted AreaUnderPRC: 0.5688115324853236
Mean absolute error: 0.1832643814026791
Coverage of cases: 75.177304964539
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 13.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11432157254722755
Kappa statistic: 0.5458632490102664
Training time: 12.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 38.77068557919622
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.6445721487046908
Weighted AreaUnderROC: 0.7740473104500637
Root mean squared error: 0.398443707073979
Relative absolute error: 50.30470186498539
Root relative squared error: 92.01663261442967
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5256661572474862
Weighted FMeasure: 0.6368708980148896
Iteration time: 6.0
Weighted AreaUnderPRC: 0.5512546511095325
Mean absolute error: 0.18864263199369521
Coverage of cases: 75.177304964539
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 19.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.12080240181664506
Kappa statistic: 0.5261742961690689
Training time: 5.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 41.016548463356976
Incorrectly Classified Instances: 34.751773049645394
Correctly Classified Instances: 65.2482269503546
Weighted Precision: 0.6423676911739891
Weighted AreaUnderROC: 0.7837488980486691
Root mean squared error: 0.3960444389624586
Relative absolute error: 49.608303000737884
Root relative squared error: 91.46254537841192
Weighted TruePositiveRate: 0.6524822695035462
Weighted MatthewsCorrelation: 0.5312983384535112
Weighted FMeasure: 0.6427460342910434
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5571860615916423
Mean absolute error: 0.18603113625276704
Coverage of cases: 74.94089834515367
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 26.0
Weighted Recall: 0.6524822695035462
Weighted FalsePositiveRate: 0.11735888837160541
Kappa statistic: 0.5361392305913422
Training time: 6.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 41.78486997635934
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6666173795803096
Weighted AreaUnderROC: 0.8022549392564837
Root mean squared error: 0.3858605830278982
Relative absolute error: 47.054110848436956
Root relative squared error: 89.11068459232919
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5651099612680114
Weighted FMeasure: 0.6674203931029684
Iteration time: 7.0
Weighted AreaUnderPRC: 0.5734229313860707
Mean absolute error: 0.1764529156816386
Coverage of cases: 77.54137115839244
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 33.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10851260483005953
Kappa statistic: 0.5710355009730891
Training time: 7.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 37.94326241134752
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6499287487175696
Weighted AreaUnderROC: 0.7828858195510898
Root mean squared error: 0.3985975237328695
Relative absolute error: 47.662273371493114
Root relative squared error: 92.05215505019619
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5470642856829762
Weighted FMeasure: 0.649960604292148
Iteration time: 14.0
Weighted AreaUnderPRC: 0.548090050918285
Mean absolute error: 0.17873352514309918
Coverage of cases: 73.28605200945627
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 47.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11246514452325299
Kappa statistic: 0.555354267310789
Training time: 13.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 35.1063829787234
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.6539160573254643
Weighted AreaUnderROC: 0.7965149195042414
Root mean squared error: 0.39617618602797655
Relative absolute error: 46.03894791838047
Root relative squared error: 91.49297105990861
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5509926952613752
Weighted FMeasure: 0.6552624458253264
Iteration time: 10.0
Weighted AreaUnderPRC: 0.5753242079155803
Mean absolute error: 0.17264605469392677
Coverage of cases: 72.81323877068557
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 57.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.11133522901547203
Kappa statistic: 0.5585736008825546
Training time: 9.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 36.643026004728135
Incorrectly Classified Instances: 34.988179669030735
Correctly Classified Instances: 65.01182033096927
Weighted Precision: 0.6787757459655291
Weighted AreaUnderROC: 0.8104540529269274
Root mean squared error: 0.37052874523673834
Relative absolute error: 46.666611560228404
Root relative squared error: 85.56994832197007
Weighted TruePositiveRate: 0.6501182033096927
Weighted MatthewsCorrelation: 0.5451314743108114
Weighted FMeasure: 0.6547859846866083
Iteration time: 10.0
Weighted AreaUnderPRC: 0.6296199034070592
Mean absolute error: 0.1749997933508565
Coverage of cases: 78.25059101654847
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 67.0
Weighted Recall: 0.6501182033096927
Weighted FalsePositiveRate: 0.1181456134011051
Kappa statistic: 0.5333248850159152
Training time: 9.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 32.68321513002364
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.6782680176161627
Weighted AreaUnderROC: 0.7962931395470974
Root mean squared error: 0.3865168013128365
Relative absolute error: 45.219198103358714
Root relative squared error: 89.2622317137117
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5595503603104839
Weighted FMeasure: 0.6680121959266757
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6044556651729018
Mean absolute error: 0.16957199288759517
Coverage of cases: 73.75886524822695
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 79.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.11233156442021397
Kappa statistic: 0.5554404376798198
Training time: 11.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 34.86997635933806
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6848879573008959
Weighted AreaUnderROC: 0.8318709535799315
Root mean squared error: 0.3751941362090026
Relative absolute error: 44.889500657821934
Root relative squared error: 86.6473742154547
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5690163444526666
Weighted FMeasure: 0.6706169772923926
Iteration time: 12.0
Weighted AreaUnderPRC: 0.6277636671236372
Mean absolute error: 0.16833562746683223
Coverage of cases: 83.68794326241135
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 91.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10965864817125726
Kappa statistic: 0.5649964230356503
Training time: 12.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 28.664302600472812
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.683662070858786
Weighted AreaUnderROC: 0.8046255397393858
Root mean squared error: 0.38771757530100925
Relative absolute error: 42.49677911616448
Root relative squared error: 89.53953858783468
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5768766919631393
Weighted FMeasure: 0.6812768419992212
Iteration time: 13.0
Weighted AreaUnderPRC: 0.616821363484038
Mean absolute error: 0.1593629216856168
Coverage of cases: 73.28605200945627
Instances selection time: 1.0
Test time: 2.0
Accumulative iteration time: 104.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.10653326722491799
Kappa statistic: 0.5774729780096907
Training time: 12.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 31.97399527186761
Incorrectly Classified Instances: 30.260047281323878
Correctly Classified Instances: 69.73995271867612
Weighted Precision: 0.692012893607191
Weighted AreaUnderROC: 0.8145198072779456
Root mean squared error: 0.36984476777483216
Relative absolute error: 42.25749559082871
Root relative squared error: 85.4119904932696
Weighted TruePositiveRate: 0.6973995271867612
Weighted MatthewsCorrelation: 0.5928299394797881
Weighted FMeasure: 0.6920732658247079
Iteration time: 15.0
Weighted AreaUnderPRC: 0.6293365937469877
Mean absolute error: 0.15846560846560767
Coverage of cases: 76.8321513002364
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 119.0
Weighted Recall: 0.6973995271867612
Weighted FalsePositiveRate: 0.10217551819112732
Kappa statistic: 0.5963860810448162
Training time: 15.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 28.19148936170213
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6834055864658773
Weighted AreaUnderROC: 0.7846727628073712
Root mean squared error: 0.38316845820256906
Relative absolute error: 43.621879758498125
Root relative squared error: 88.48896499529086
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5807163780017992
Weighted FMeasure: 0.6824120189140273
Iteration time: 16.0
Weighted AreaUnderPRC: 0.5933327575938446
Mean absolute error: 0.16358204909436797
Coverage of cases: 73.04964539007092
Instances selection time: 0.0
Test time: 6.0
Accumulative iteration time: 135.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.10511082345695848
Kappa statistic: 0.5838507002153935
Training time: 16.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 30.260047281323878
Incorrectly Classified Instances: 30.969267139479907
Correctly Classified Instances: 69.03073286052009
Weighted Precision: 0.7072737101279599
Weighted AreaUnderROC: 0.80837277850673
Root mean squared error: 0.3734976451737799
Relative absolute error: 42.154019807245724
Root relative squared error: 86.25558639310927
Weighted TruePositiveRate: 0.6903073286052009
Weighted MatthewsCorrelation: 0.593617386961223
Weighted FMeasure: 0.6869127097887132
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6246291563604999
Mean absolute error: 0.15807757427717148
Coverage of cases: 74.23167848699764
Instances selection time: 4.0
Test time: 2.0
Accumulative iteration time: 171.0
Weighted Recall: 0.6903073286052009
Weighted FalsePositiveRate: 0.10427715314519301
Kappa statistic: 0.5869448544210385
Training time: 32.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 27.836879432624112
Incorrectly Classified Instances: 30.023640661938533
Correctly Classified Instances: 69.97635933806147
Weighted Precision: 0.6972437788730586
Weighted AreaUnderROC: 0.8063830817003983
Root mean squared error: 0.3736321042227632
Relative absolute error: 40.6571060135823
Root relative squared error: 86.28663838035948
Weighted TruePositiveRate: 0.6997635933806147
Weighted MatthewsCorrelation: 0.5974588175251605
Weighted FMeasure: 0.6965160694402717
Iteration time: 36.0
Weighted AreaUnderPRC: 0.6131816814969936
Mean absolute error: 0.15246414755093363
Coverage of cases: 74.94089834515367
Instances selection time: 2.0
Test time: 2.0
Accumulative iteration time: 207.0
Weighted Recall: 0.6997635933806147
Weighted FalsePositiveRate: 0.10112814452046098
Kappa statistic: 0.5995691647162301
Training time: 34.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 32.15130023640662
Incorrectly Classified Instances: 26.95035460992908
Correctly Classified Instances: 73.04964539007092
Weighted Precision: 0.7349718901102607
Weighted AreaUnderROC: 0.851937072938066
Root mean squared error: 0.3494421497321484
Relative absolute error: 37.429486059320034
Root relative squared error: 80.70020768562296
Weighted TruePositiveRate: 0.7304964539007093
Weighted MatthewsCorrelation: 0.6414637558020132
Weighted FMeasure: 0.7289968826331343
Iteration time: 19.0
Weighted AreaUnderPRC: 0.6818660088691028
Mean absolute error: 0.14036057272245012
Coverage of cases: 82.50591016548464
Instances selection time: 0.0
Test time: 2.0
Accumulative iteration time: 226.0
Weighted Recall: 0.7304964539007093
Weighted FalsePositiveRate: 0.09114778651417353
Kappa statistic: 0.6404697110904009
Training time: 19.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 35.7565011820331
Incorrectly Classified Instances: 29.314420803782507
Correctly Classified Instances: 70.68557919621749
Weighted Precision: 0.7111291229853499
Weighted AreaUnderROC: 0.851948389998298
Root mean squared error: 0.3390761011025108
Relative absolute error: 42.04484977226275
Root relative squared error: 78.30627129892135
Weighted TruePositiveRate: 0.706855791962175
Weighted MatthewsCorrelation: 0.6097921432834615
Weighted FMeasure: 0.7014789530149964
Iteration time: 18.0
Weighted AreaUnderPRC: 0.6636593496913618
Mean absolute error: 0.1576681866459853
Coverage of cases: 86.28841607565012
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 244.0
Weighted Recall: 0.706855791962175
Weighted FalsePositiveRate: 0.09902272665725634
Kappa statistic: 0.6090369034220079
Training time: 17.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 32.68321513002364
Incorrectly Classified Instances: 29.078014184397162
Correctly Classified Instances: 70.92198581560284
Weighted Precision: 0.7129027944627865
Weighted AreaUnderROC: 0.8403327088716928
Root mean squared error: 0.3516895784641667
Relative absolute error: 39.77917339934672
Root relative squared error: 81.21922911898908
Weighted TruePositiveRate: 0.7092198581560284
Weighted MatthewsCorrelation: 0.6123216027716637
Weighted FMeasure: 0.7094851448062507
Iteration time: 21.0
Weighted AreaUnderPRC: 0.6620579842444392
Mean absolute error: 0.1491719002475502
Coverage of cases: 81.56028368794327
Instances selection time: 1.0
Test time: 1.0
Accumulative iteration time: 265.0
Weighted Recall: 0.7092198581560284
Weighted FalsePositiveRate: 0.09873019839707782
Kappa statistic: 0.6120076362063566
Training time: 20.0
		
Time end:Wed Nov 01 14.39.45 EET 2017