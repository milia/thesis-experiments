Wed Nov 01 14.43.18 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: NaiveBayes-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: RandomSelectionQueryStrategy-1cores
Time begining: Wed Nov 01 14.43.18 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 37.05673758865248
Incorrectly Classified Instances: 51.06382978723404
Correctly Classified Instances: 48.93617021276596
Weighted Precision: 0.49994111108727135
Weighted AreaUnderROC: 0.733381245575987
Root mean squared error: 0.468593461712851
Relative absolute error: 68.70701237985752
Root relative squared error: 108.21702450416525
Weighted TruePositiveRate: 0.48936170212765956
Weighted MatthewsCorrelation: 0.32514828327042383
Weighted FMeasure: 0.4761575401424347
Iteration time: 2.0
Weighted AreaUnderPRC: 0.4981539108399474
Mean absolute error: 0.25765129642446566
Coverage of cases: 63.829787234042556
Instances selection time: 2.0
Test time: 15.0
Accumulative iteration time: 2.0
Weighted Recall: 0.48936170212765956
Weighted FalsePositiveRate: 0.16741855743962544
Kappa statistic: 0.3217480383933012
Training time: 0.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 38.12056737588652
Incorrectly Classified Instances: 52.24586288416076
Correctly Classified Instances: 47.75413711583924
Weighted Precision: 0.5146036493876794
Weighted AreaUnderROC: 0.7479170031279541
Root mean squared error: 0.47362243205510846
Relative absolute error: 70.4410560880446
Root relative squared error: 109.37841545650484
Weighted TruePositiveRate: 0.47754137115839246
Weighted MatthewsCorrelation: 0.3220486533126652
Weighted FMeasure: 0.461708745127907
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5075016985740751
Mean absolute error: 0.26415396033016725
Coverage of cases: 67.61229314420804
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 4.0
Weighted Recall: 0.47754137115839246
Weighted FalsePositiveRate: 0.1698894874392949
Kappa statistic: 0.3071176038956708
Training time: 1.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 40.484633569739955
Incorrectly Classified Instances: 52.009456264775416
Correctly Classified Instances: 47.990543735224584
Weighted Precision: 0.5012272130502471
Weighted AreaUnderROC: 0.750085350546018
Root mean squared error: 0.4579103464221164
Relative absolute error: 68.50106386030775
Root relative squared error: 105.7498647086095
Weighted TruePositiveRate: 0.4799054373522459
Weighted MatthewsCorrelation: 0.3185334470338682
Weighted FMeasure: 0.46289607421053475
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5093697011703252
Mean absolute error: 0.25687898947615406
Coverage of cases: 73.04964539007092
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 6.0
Weighted Recall: 0.4799054373522459
Weighted FalsePositiveRate: 0.16956269926075745
Kappa statistic: 0.30997664330997665
Training time: 1.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 42.08037825059102
Incorrectly Classified Instances: 53.427895981087474
Correctly Classified Instances: 46.572104018912526
Weighted Precision: 0.5039411439640986
Weighted AreaUnderROC: 0.7546547180697348
Root mean squared error: 0.4680513732666
Relative absolute error: 71.5113556085871
Root relative squared error: 108.09183454001823
Weighted TruePositiveRate: 0.4657210401891253
Weighted MatthewsCorrelation: 0.3064417281767301
Weighted FMeasure: 0.4493955804762659
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5170440360381598
Mean absolute error: 0.2681675835322016
Coverage of cases: 73.04964539007092
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 8.0
Weighted Recall: 0.4657210401891253
Weighted FalsePositiveRate: 0.17378342823219436
Kappa statistic: 0.291473040578099
Training time: 1.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 44.621749408983455
Incorrectly Classified Instances: 54.37352245862884
Correctly Classified Instances: 45.62647754137116
Weighted Precision: 0.484884494906447
Weighted AreaUnderROC: 0.7445107456199651
Root mean squared error: 0.46874151085652493
Relative absolute error: 73.91781600729537
Root relative squared error: 108.25121498934662
Weighted TruePositiveRate: 0.4562647754137116
Weighted MatthewsCorrelation: 0.2901042239524843
Weighted FMeasure: 0.4408608783338773
Iteration time: 1.0
Weighted AreaUnderPRC: 0.49133866829782563
Mean absolute error: 0.27719181002735765
Coverage of cases: 73.99527186761229
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 9.0
Weighted Recall: 0.4562647754137116
Weighted FalsePositiveRate: 0.1770006670311067
Kappa statistic: 0.2787830715286478
Training time: 1.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 43.557919621749406
Incorrectly Classified Instances: 53.427895981087474
Correctly Classified Instances: 46.572104018912526
Weighted Precision: 0.5037822605478206
Weighted AreaUnderROC: 0.7508754531220462
Root mean squared error: 0.4647470148664437
Relative absolute error: 72.6974556414897
Root relative squared error: 107.32872565528652
Weighted TruePositiveRate: 0.4657210401891253
Weighted MatthewsCorrelation: 0.3067503626866828
Weighted FMeasure: 0.4451992975875921
Iteration time: 1.0
Weighted AreaUnderPRC: 0.49418842488365355
Mean absolute error: 0.2726154586555864
Coverage of cases: 72.57683215130024
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 10.0
Weighted Recall: 0.4657210401891253
Weighted FalsePositiveRate: 0.1734364908688231
Kappa statistic: 0.2916672841244202
Training time: 0.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 43.43971631205674
Incorrectly Classified Instances: 53.191489361702125
Correctly Classified Instances: 46.808510638297875
Weighted Precision: 0.4934364469938778
Weighted AreaUnderROC: 0.7467321959553938
Root mean squared error: 0.46269041483570467
Relative absolute error: 72.59707591117889
Root relative squared error: 106.85377422274149
Weighted TruePositiveRate: 0.46808510638297873
Weighted MatthewsCorrelation: 0.30478362616139787
Weighted FMeasure: 0.45086247883382946
Iteration time: 2.0
Weighted AreaUnderPRC: 0.48935856973104497
Mean absolute error: 0.27223903466692084
Coverage of cases: 72.81323877068557
Instances selection time: 1.0
Test time: 15.0
Accumulative iteration time: 12.0
Weighted Recall: 0.46808510638297873
Weighted FalsePositiveRate: 0.17308124303329578
Kappa statistic: 0.2943518491058454
Training time: 1.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 45.449172576832154
Incorrectly Classified Instances: 56.973995271867615
Correctly Classified Instances: 43.026004728132385
Weighted Precision: 0.44525655589485374
Weighted AreaUnderROC: 0.7522441101015039
Root mean squared error: 0.46069794369141803
Relative absolute error: 73.84569796345815
Root relative squared error: 106.3936327221389
Weighted TruePositiveRate: 0.4302600472813239
Weighted MatthewsCorrelation: 0.2500006991575275
Weighted FMeasure: 0.41301645757332767
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5026252400192696
Mean absolute error: 0.2769213673629681
Coverage of cases: 74.94089834515367
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 14.0
Weighted Recall: 0.4302600472813239
Weighted FalsePositiveRate: 0.18570335542193878
Kappa statistic: 0.24425087108013938
Training time: 1.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 46.98581560283688
Incorrectly Classified Instances: 59.57446808510638
Correctly Classified Instances: 40.42553191489362
Weighted Precision: 0.41676637447954157
Weighted AreaUnderROC: 0.7464142806171105
Root mean squared error: 0.47079652342249145
Relative absolute error: 78.24388021780848
Root relative squared error: 108.72579981260617
Weighted TruePositiveRate: 0.40425531914893614
Weighted MatthewsCorrelation: 0.21353638487516086
Weighted FMeasure: 0.377640097658561
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4841321565588174
Mean absolute error: 0.2934145508167818
Coverage of cases: 73.99527186761229
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 15.0
Weighted Recall: 0.40425531914893614
Weighted FalsePositiveRate: 0.19377907260754232
Kappa statistic: 0.2102478995954776
Training time: 1.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 46.572104018912526
Incorrectly Classified Instances: 58.628841607565015
Correctly Classified Instances: 41.371158392434985
Weighted Precision: 0.4285093620512895
Weighted AreaUnderROC: 0.7494422020659708
Root mean squared error: 0.4686704711084631
Relative absolute error: 77.30109726176893
Root relative squared error: 108.23480906227996
Weighted TruePositiveRate: 0.41371158392434987
Weighted MatthewsCorrelation: 0.22700006787957308
Weighted FMeasure: 0.3821654559224541
Iteration time: 2.0
Weighted AreaUnderPRC: 0.49117036301370476
Mean absolute error: 0.2898791147316335
Coverage of cases: 73.04964539007092
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 17.0
Weighted Recall: 0.41371158392434987
Weighted FalsePositiveRate: 0.19039931512129443
Kappa statistic: 0.22305994578661253
Training time: 1.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 46.74940898345154
Incorrectly Classified Instances: 58.392434988179666
Correctly Classified Instances: 41.607565011820334
Weighted Precision: 0.43078021306527947
Weighted AreaUnderROC: 0.7531715440378147
Root mean squared error: 0.4690015278298884
Relative absolute error: 77.42925608479057
Root relative squared error: 108.31126333717273
Weighted TruePositiveRate: 0.4160756501182033
Weighted MatthewsCorrelation: 0.22983721875003543
Weighted FMeasure: 0.3771507850546949
Iteration time: 1.0
Weighted AreaUnderPRC: 0.4937743758656939
Mean absolute error: 0.29035971031796465
Coverage of cases: 73.75886524822695
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 18.0
Weighted Recall: 0.4160756501182033
Weighted FalsePositiveRate: 0.18938829635915916
Kappa statistic: 0.2265364741416325
Training time: 1.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 48.3451536643026
Incorrectly Classified Instances: 56.737588652482266
Correctly Classified Instances: 43.262411347517734
Weighted Precision: 0.46747233883840555
Weighted AreaUnderROC: 0.7672879006892896
Root mean squared error: 0.4625513240088251
Relative absolute error: 76.09756522174715
Root relative squared error: 106.8216525722052
Weighted TruePositiveRate: 0.4326241134751773
Weighted MatthewsCorrelation: 0.2604007598327551
Weighted FMeasure: 0.3997925095964312
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5246304184982297
Mean absolute error: 0.2853658695815518
Coverage of cases: 76.59574468085107
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 20.0
Weighted Recall: 0.4326241134751773
Weighted FalsePositiveRate: 0.183528630591836
Kappa statistic: 0.24855662472242784
Training time: 1.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 46.45390070921986
Incorrectly Classified Instances: 57.4468085106383
Correctly Classified Instances: 42.5531914893617
Weighted Precision: 0.476842997174246
Weighted AreaUnderROC: 0.7654237789245418
Root mean squared error: 0.4720361755162571
Relative absolute error: 76.15732896430953
Root relative squared error: 109.012085200621
Weighted TruePositiveRate: 0.425531914893617
Weighted MatthewsCorrelation: 0.254641944384542
Weighted FMeasure: 0.37867904357266063
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5307306988755229
Mean absolute error: 0.2855899836161607
Coverage of cases: 74.46808510638297
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 22.0
Weighted Recall: 0.425531914893617
Weighted FalsePositiveRate: 0.1855220049788955
Kappa statistic: 0.23967009394185965
Training time: 2.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 46.808510638297875
Incorrectly Classified Instances: 56.501182033096924
Correctly Classified Instances: 43.498817966903076
Weighted Precision: 0.4994371931556322
Weighted AreaUnderROC: 0.7658732827542515
Root mean squared error: 0.4712758706534906
Relative absolute error: 75.99358510037771
Root relative squared error: 108.83650031374722
Weighted TruePositiveRate: 0.43498817966903075
Weighted MatthewsCorrelation: 0.2712510158428514
Weighted FMeasure: 0.39025685418136324
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5350087865951025
Mean absolute error: 0.2849759441264164
Coverage of cases: 75.177304964539
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 24.0
Weighted Recall: 0.43498817966903075
Weighted FalsePositiveRate: 0.18239835172224875
Kappa statistic: 0.25215815364130634
Training time: 1.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 47.635933806146575
Incorrectly Classified Instances: 56.737588652482266
Correctly Classified Instances: 43.262411347517734
Weighted Precision: 0.49440421055070355
Weighted AreaUnderROC: 0.7667723178842416
Root mean squared error: 0.4701119634013474
Relative absolute error: 76.54544619878455
Root relative squared error: 108.56770744761259
Weighted TruePositiveRate: 0.4326241134751773
Weighted MatthewsCorrelation: 0.2692589444057262
Weighted FMeasure: 0.3920453539550258
Iteration time: 2.0
Weighted AreaUnderPRC: 0.540499656383708
Mean absolute error: 0.2870454232454421
Coverage of cases: 76.35933806146572
Instances selection time: 0.0
Test time: 14.0
Accumulative iteration time: 26.0
Weighted Recall: 0.4326241134751773
Weighted FalsePositiveRate: 0.18304190533754525
Kappa statistic: 0.24899577597111977
Training time: 2.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 47.104018912529554
Incorrectly Classified Instances: 57.21040189125296
Correctly Classified Instances: 42.78959810874704
Weighted Precision: 0.48873713902732147
Weighted AreaUnderROC: 0.7694093657580742
Root mean squared error: 0.4691127493129261
Relative absolute error: 76.0909053505934
Root relative squared error: 108.33694883844133
Weighted TruePositiveRate: 0.42789598108747046
Weighted MatthewsCorrelation: 0.26202161224127996
Weighted FMeasure: 0.3818436369792931
Iteration time: 2.0
Weighted AreaUnderPRC: 0.5445824338155533
Mean absolute error: 0.28534089506472526
Coverage of cases: 75.177304964539
Instances selection time: 0.0
Test time: 15.0
Accumulative iteration time: 28.0
Weighted Recall: 0.42789598108747046
Weighted FalsePositiveRate: 0.18441698527063666
Kappa statistic: 0.24297261520030175
Training time: 2.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 46.15839243498818
Incorrectly Classified Instances: 56.737588652482266
Correctly Classified Instances: 43.262411347517734
Weighted Precision: 0.4960850465990069
Weighted AreaUnderROC: 0.7663179162523786
Root mean squared error: 0.4749561486610286
Relative absolute error: 76.28492333629224
Root relative squared error: 109.68642411308512
Weighted TruePositiveRate: 0.4326241134751773
Weighted MatthewsCorrelation: 0.26939492571362217
Weighted FMeasure: 0.38884197793148223
Iteration time: 2.0
Weighted AreaUnderPRC: 0.538307632991301
Mean absolute error: 0.28606846251109586
Coverage of cases: 74.46808510638297
Instances selection time: 1.0
Test time: 14.0
Accumulative iteration time: 30.0
Weighted Recall: 0.4326241134751773
Weighted FalsePositiveRate: 0.18295316719394727
Kappa statistic: 0.24913464098695307
Training time: 1.0
		
Time end:Wed Nov 01 14.43.19 EET 2017