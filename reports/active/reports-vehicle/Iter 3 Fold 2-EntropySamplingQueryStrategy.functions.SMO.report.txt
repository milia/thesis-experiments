Wed Nov 01 14.39.10 EET 2017
Dataset: vehicle
Test set size: 423
Initial Labelled set size: 84
Initial Unlabelled set size: 339
Batch mode: QBestBatchMode
Batch size: 20
Classifiers: SMO-1cores
Scenario: PoolBasedSamplingScenario
Query strategy: EntropySamplingQueryStrategy-1cores
Time begining: Wed Nov 01 14.39.10 EET 2017
		
Iteration: 1
Labeled set size: 84
Unlabelled set size: 339
	
Mean region size: 75.65011820330969
Incorrectly Classified Instances: 34.04255319148936
Correctly Classified Instances: 65.95744680851064
Weighted Precision: 0.641423247386051
Weighted AreaUnderROC: 0.8176498962257258
Root mean squared error: 0.37197250012038535
Relative absolute error: 77.8040451799316
Root relative squared error: 85.90336923025704
Weighted TruePositiveRate: 0.6595744680851063
Weighted MatthewsCorrelation: 0.5369230624456783
Weighted FMeasure: 0.6360708520215251
Iteration time: 100.0
Weighted AreaUnderPRC: 0.5713984536793135
Mean absolute error: 0.2917651694247435
Coverage of cases: 96.45390070921985
Instances selection time: 4.0
Test time: 10.0
Accumulative iteration time: 100.0
Weighted Recall: 0.6595744680851063
Weighted FalsePositiveRate: 0.11272009966845355
Kappa statistic: 0.5468598890062638
Training time: 96.0
		
Iteration: 2
Labeled set size: 104
Unlabelled set size: 319
	
Mean region size: 75.23640661938535
Incorrectly Classified Instances: 32.6241134751773
Correctly Classified Instances: 67.37588652482269
Weighted Precision: 0.6668886567469059
Weighted AreaUnderROC: 0.8326496429146353
Root mean squared error: 0.3713098800940043
Relative absolute error: 77.64644076700812
Root relative squared error: 85.75034369001641
Weighted TruePositiveRate: 0.6737588652482269
Weighted MatthewsCorrelation: 0.5604629852176208
Weighted FMeasure: 0.6548465733529328
Iteration time: 96.0
Weighted AreaUnderPRC: 0.5946742239484828
Mean absolute error: 0.2911741528762804
Coverage of cases: 96.92671394799055
Instances selection time: 5.0
Test time: 5.0
Accumulative iteration time: 196.0
Weighted Recall: 0.6737588652482269
Weighted FalsePositiveRate: 0.10886305896889543
Kappa statistic: 0.5652717889139613
Training time: 91.0
		
Iteration: 3
Labeled set size: 124
Unlabelled set size: 299
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 32.15130023640662
Correctly Classified Instances: 67.84869976359339
Weighted Precision: 0.6628952265686342
Weighted AreaUnderROC: 0.829014676706363
Root mean squared error: 0.37294221322484117
Relative absolute error: 77.90911478854741
Root relative squared error: 86.12731487901476
Weighted TruePositiveRate: 0.6784869976359338
Weighted MatthewsCorrelation: 0.5631796403797819
Weighted FMeasure: 0.6575973135288905
Iteration time: 101.0
Weighted AreaUnderPRC: 0.5862816797098002
Mean absolute error: 0.2921591804570528
Coverage of cases: 96.6903073286052
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 297.0
Weighted Recall: 0.6784869976359338
Weighted FalsePositiveRate: 0.10689538016569042
Kappa statistic: 0.5718049869743208
Training time: 94.0
		
Iteration: 4
Labeled set size: 144
Unlabelled set size: 279
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 31.44208037825059
Correctly Classified Instances: 68.5579196217494
Weighted Precision: 0.6684135175416792
Weighted AreaUnderROC: 0.8349616846150026
Root mean squared error: 0.37051317184003113
Relative absolute error: 77.48883635408458
Root relative squared error: 85.56635180005762
Weighted TruePositiveRate: 0.6855791962174941
Weighted MatthewsCorrelation: 0.5723153562997003
Weighted FMeasure: 0.6685187350401919
Iteration time: 103.0
Weighted AreaUnderPRC: 0.5968392122904295
Mean absolute error: 0.2905831363278172
Coverage of cases: 97.16312056737588
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 400.0
Weighted Recall: 0.6855791962174941
Weighted FalsePositiveRate: 0.10485675235400527
Kappa statistic: 0.5810352916644946
Training time: 99.0
		
Iteration: 5
Labeled set size: 164
Unlabelled set size: 259
	
Mean region size: 76.06382978723404
Incorrectly Classified Instances: 31.20567375886525
Correctly Classified Instances: 68.79432624113475
Weighted Precision: 0.6694939210330575
Weighted AreaUnderROC: 0.8254135435132722
Root mean squared error: 0.37360193815801995
Relative absolute error: 78.17178881008672
Root relative squared error: 86.2796718261195
Weighted TruePositiveRate: 0.6879432624113475
Weighted MatthewsCorrelation: 0.5747890432727168
Weighted FMeasure: 0.670935077358086
Iteration time: 105.0
Weighted AreaUnderPRC: 0.5865102409925371
Mean absolute error: 0.2931442080378252
Coverage of cases: 96.92671394799055
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 505.0
Weighted Recall: 0.6879432624113475
Weighted FalsePositiveRate: 0.1050858314506796
Kappa statistic: 0.58387241019526
Training time: 99.0
		
Iteration: 6
Labeled set size: 184
Unlabelled set size: 239
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 31.678486997635932
Correctly Classified Instances: 68.32151300236407
Weighted Precision: 0.6615137840908792
Weighted AreaUnderROC: 0.8222174031217406
Root mean squared error: 0.3750930188459536
Relative absolute error: 78.38192802731814
Root relative squared error: 86.62402216074427
Weighted TruePositiveRate: 0.6832151300236406
Weighted MatthewsCorrelation: 0.5669304101828527
Weighted FMeasure: 0.6569617247045768
Iteration time: 109.0
Weighted AreaUnderPRC: 0.5784049336139043
Mean absolute error: 0.293932230102443
Coverage of cases: 96.6903073286052
Instances selection time: 6.0
Test time: 2.0
Accumulative iteration time: 614.0
Weighted Recall: 0.6832151300236406
Weighted FalsePositiveRate: 0.1059027616967854
Kappa statistic: 0.5780265920223933
Training time: 103.0
		
Iteration: 7
Labeled set size: 204
Unlabelled set size: 219
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 33.80614657210402
Correctly Classified Instances: 66.19385342789599
Weighted Precision: 0.6435849901638973
Weighted AreaUnderROC: 0.8122083525933591
Root mean squared error: 0.37957436796270766
Relative absolute error: 79.32755450485946
Root relative squared error: 87.65894540830053
Weighted TruePositiveRate: 0.6619385342789598
Weighted MatthewsCorrelation: 0.5398879134304612
Weighted FMeasure: 0.6266143867155742
Iteration time: 121.0
Weighted AreaUnderPRC: 0.556537672481395
Mean absolute error: 0.297478329393223
Coverage of cases: 95.74468085106383
Instances selection time: 3.0
Test time: 5.0
Accumulative iteration time: 735.0
Weighted Recall: 0.6619385342789598
Weighted FalsePositiveRate: 0.11145902263888637
Kappa statistic: 0.5503746320578004
Training time: 118.0
		
Iteration: 8
Labeled set size: 224
Unlabelled set size: 199
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 35.46099290780142
Correctly Classified Instances: 64.53900709219859
Weighted Precision: 0.623021355544152
Weighted AreaUnderROC: 0.8127829068237645
Root mean squared error: 0.3801794054811265
Relative absolute error: 79.32755450485944
Root relative squared error: 87.79867283795211
Weighted TruePositiveRate: 0.6453900709219859
Weighted MatthewsCorrelation: 0.5144363232680659
Weighted FMeasure: 0.5971459948026183
Iteration time: 117.0
Weighted AreaUnderPRC: 0.5464673337132689
Mean absolute error: 0.2974783293932229
Coverage of cases: 95.27186761229315
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 852.0
Weighted Recall: 0.6453900709219859
Weighted FalsePositiveRate: 0.11672783658453198
Kappa statistic: 0.5285542329811422
Training time: 110.0
		
Iteration: 9
Labeled set size: 244
Unlabelled set size: 179
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 35.69739952718676
Correctly Classified Instances: 64.30260047281324
Weighted Precision: 0.6316559573610621
Weighted AreaUnderROC: 0.8270784047868088
Root mean squared error: 0.38022258560568856
Relative absolute error: 79.43262411347516
Root relative squared error: 87.80864486056794
Weighted TruePositiveRate: 0.6430260047281324
Weighted MatthewsCorrelation: 0.4994537633826067
Weighted FMeasure: 0.5579187456096988
Iteration time: 121.0
Weighted AreaUnderPRC: 0.5600270954467059
Mean absolute error: 0.29787234042553185
Coverage of cases: 95.50827423167848
Instances selection time: 4.0
Test time: 5.0
Accumulative iteration time: 973.0
Weighted Recall: 0.6430260047281324
Weighted FalsePositiveRate: 0.1169758381436975
Kappa statistic: 0.5257812325990601
Training time: 117.0
		
Iteration: 10
Labeled set size: 264
Unlabelled set size: 159
	
Mean region size: 75.76832151300236
Incorrectly Classified Instances: 34.515366430260045
Correctly Classified Instances: 65.48463356973996
Weighted Precision: 0.49237305205466847
Weighted AreaUnderROC: 0.8331931666757297
Root mean squared error: 0.3787083518493365
Relative absolute error: 79.11741528762805
Root relative squared error: 87.45894755382957
Weighted TruePositiveRate: 0.6548463356973995
Weighted MatthewsCorrelation: 0.49817841112649164
Weighted FMeasure: 0.561036850443588
Iteration time: 122.0
Weighted AreaUnderPRC: 0.5736085381237157
Mean absolute error: 0.2966903073286052
Coverage of cases: 95.50827423167848
Instances selection time: 1.0
Test time: 4.0
Accumulative iteration time: 1095.0
Weighted Recall: 0.6548463356973995
Weighted FalsePositiveRate: 0.11332741953931709
Kappa statistic: 0.5414259619525669
Training time: 121.0
		
Iteration: 11
Labeled set size: 284
Unlabelled set size: 139
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 33.56973995271868
Correctly Classified Instances: 66.43026004728132
Weighted Precision: 0.512162829365936
Weighted AreaUnderROC: 0.8390936785069868
Root mean squared error: 0.3775795614045312
Relative absolute error: 78.8547412660888
Root relative squared error: 87.19826456696276
Weighted TruePositiveRate: 0.6643026004728132
Weighted MatthewsCorrelation: 0.5159158588069455
Weighted FMeasure: 0.5741065470272505
Iteration time: 141.0
Weighted AreaUnderPRC: 0.5933128633307679
Mean absolute error: 0.29570527974783295
Coverage of cases: 95.74468085106383
Instances selection time: 6.0
Test time: 5.0
Accumulative iteration time: 1236.0
Weighted Recall: 0.6643026004728132
Weighted FalsePositiveRate: 0.1101746280054461
Kappa statistic: 0.5540128154676606
Training time: 135.0
		
Iteration: 12
Labeled set size: 304
Unlabelled set size: 119
	
Mean region size: 75.59101654846336
Incorrectly Classified Instances: 33.096926713947994
Correctly Classified Instances: 66.903073286052
Weighted Precision: 0.5179313614682217
Weighted AreaUnderROC: 0.8474537036267349
Root mean squared error: 0.3734700864023139
Relative absolute error: 78.06671920147092
Root relative squared error: 86.24922196745949
Weighted TruePositiveRate: 0.6690307328605201
Weighted MatthewsCorrelation: 0.5227788235150196
Weighted FMeasure: 0.5789927991269097
Iteration time: 109.0
Weighted AreaUnderPRC: 0.6059382487420537
Mean absolute error: 0.29275019700551597
Coverage of cases: 96.45390070921985
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 1345.0
Weighted Recall: 0.6690307328605201
Weighted FalsePositiveRate: 0.10876793602308174
Kappa statistic: 0.5602355509679718
Training time: 102.0
		
Iteration: 13
Labeled set size: 324
Unlabelled set size: 99
	
Mean region size: 75.35460992907801
Incorrectly Classified Instances: 33.333333333333336
Correctly Classified Instances: 66.66666666666667
Weighted Precision: 0.5200660509357784
Weighted AreaUnderROC: 0.8506042617444208
Root mean squared error: 0.3696703392291634
Relative absolute error: 77.27869713685304
Root relative squared error: 85.37170794615112
Weighted TruePositiveRate: 0.6666666666666666
Weighted MatthewsCorrelation: 0.5219512122743377
Weighted FMeasure: 0.5779199529086462
Iteration time: 116.0
Weighted AreaUnderPRC: 0.6117971502164319
Mean absolute error: 0.28979511426319887
Coverage of cases: 97.16312056737588
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1461.0
Weighted Recall: 0.6666666666666666
Weighted FalsePositiveRate: 0.10933171747299686
Kappa statistic: 0.5571897365842069
Training time: 113.0
		
Iteration: 14
Labeled set size: 344
Unlabelled set size: 79
	
Mean region size: 75.177304964539
Incorrectly Classified Instances: 31.914893617021278
Correctly Classified Instances: 68.08510638297872
Weighted Precision: 0.7836670487379709
Weighted AreaUnderROC: 0.8562607889109625
Root mean squared error: 0.3662347911497341
Relative absolute error: 76.5957446808509
Root relative squared error: 84.57830210276215
Weighted TruePositiveRate: 0.6808510638297872
Weighted MatthewsCorrelation: 0.5826351285396221
Weighted FMeasure: 0.6117992539747309
Iteration time: 116.0
Weighted AreaUnderPRC: 0.62881583174434
Mean absolute error: 0.28723404255319085
Coverage of cases: 96.6903073286052
Instances selection time: 3.0
Test time: 6.0
Accumulative iteration time: 1577.0
Weighted Recall: 0.6808510638297872
Weighted FalsePositiveRate: 0.10462668497726636
Kappa statistic: 0.5759634662508353
Training time: 113.0
		
Iteration: 15
Labeled set size: 364
Unlabelled set size: 59
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7289949066343495
Weighted AreaUnderROC: 0.8634114814013107
Root mean squared error: 0.3587163730603183
Relative absolute error: 75.1773049645389
Root relative squared error: 82.8419978196404
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6317780977775134
Weighted FMeasure: 0.7044554004445192
Iteration time: 121.0
Weighted AreaUnderPRC: 0.6492105914829693
Mean absolute error: 0.2819148936170209
Coverage of cases: 98.58156028368795
Instances selection time: 5.0
Test time: 3.0
Accumulative iteration time: 1698.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09098593949014239
Kappa statistic: 0.6320946171973151
Training time: 116.0
		
Iteration: 16
Labeled set size: 384
Unlabelled set size: 39
	
Mean region size: 75.41371158392435
Incorrectly Classified Instances: 26.004728132387708
Correctly Classified Instances: 73.99527186761229
Weighted Precision: 0.733057239714281
Weighted AreaUnderROC: 0.8593146325868786
Root mean squared error: 0.35715692868822735
Relative absolute error: 74.8620961386918
Root relative squared error: 82.48185956843521
Weighted TruePositiveRate: 0.7399527186761229
Weighted MatthewsCorrelation: 0.6496483449497109
Weighted FMeasure: 0.7322575644468059
Iteration time: 124.0
Weighted AreaUnderPRC: 0.6524320299623926
Mean absolute error: 0.2807328605200942
Coverage of cases: 98.58156028368795
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 1822.0
Weighted Recall: 0.7399527186761229
Weighted FalsePositiveRate: 0.08626750781676752
Kappa statistic: 0.6537328094302554
Training time: 117.0
		
Iteration: 17
Labeled set size: 404
Unlabelled set size: 19
	
Mean region size: 75.53191489361703
Incorrectly Classified Instances: 27.659574468085108
Correctly Classified Instances: 72.34042553191489
Weighted Precision: 0.7121766235019503
Weighted AreaUnderROC: 0.854668291040198
Root mean squared error: 0.3594022121411269
Relative absolute error: 75.3349093774625
Root relative squared error: 83.00038557081065
Weighted TruePositiveRate: 0.723404255319149
Weighted MatthewsCorrelation: 0.6253837749542484
Weighted FMeasure: 0.7139603949470701
Iteration time: 124.0
Weighted AreaUnderPRC: 0.6365705249940761
Mean absolute error: 0.2825059101654843
Coverage of cases: 98.3451536643026
Instances selection time: 7.0
Test time: 2.0
Accumulative iteration time: 1946.0
Weighted Recall: 0.723404255319149
Weighted FalsePositiveRate: 0.09236719744510419
Kappa statistic: 0.6314727389161094
Training time: 117.0
		
Time end:Wed Nov 01 14.39.13 EET 2017